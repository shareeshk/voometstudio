<?php
	class model_search extends CI_Model{
		public function get_parent_cat_info($pcat_id){
			$query=$this->db->select("*")->from("parent_category")->where(array("pcat_id"=>$pcat_id,"view"=>"1","active"=>"1"))->get();
			return $query->row();
		}
		
		public function get_parent_cat_info_all_combo($pcat_id=''){
			if($pcat_id!=''){
				$query=$this->db->select("*")->from("parent_category")->where(array("pcat_id"=>$pcat_id,"view"=>"1","active"=>"1","combo_pack_status"=>"yes"))->get();
				return $query->result();
			}else{
				$query=$this->db->select("*")->from("parent_category")->where(array("view"=>"1","active"=>"1","combo_pack_status"=>"yes"))->get();
				return $query->result();
			}
		}
		public function get_cat_info($cat_id){
			$query=$this->db->select("*")->from("category")->where(array("cat_id"=>$cat_id,"view"=>"1","active"=>"1"))->get();
			return $query->row();
		}
		
		public function get_subcat_info($subcat_id){
			$query=$this->db->select("*")->from("subcategory")->where(array("subcat_id"=>$subcat_id,"view"=>"1","active"=>"1"))->get();
			return $query->row();
		}
		
		
		
		public function get_brand_info($brand_id){
			$query=$this->db->select("*")->from("brands")->where(array("brand_id"=>$brand_id,"view"=>"1","active"=>"1"))->get();
			return $query->row();
		}
		
		public function get_product_info($product_id){
			$query=$this->db->select("*")->from("products")->where(array("product_id"=>$product_id,"view"=>"1","active"=>"1"))->get();
			return $query->row();
		}
		
		public function get_inventory_info($inventory_id){
			$query=$this->db->select("*")->from("inventory")->where(array("id"=>$inventory_id,"active"=>"1"))->get();
			return $query->row();
		}
		
		/*public function get_filter_box_info($cat_current,$cat_current_id,$category_tree){
			
			extract($category_tree);
			
			if($cat_current=="subcat"){
	
				if(isset($subcat_id)){
					$sql="select filterbox_to_category.*,filterbox.* from filterbox_to_category,filterbox where filterbox_to_category.filterbox_id=filterbox.filterbox_id and ((filterbox_to_category.subcat_id='$cat_current_id' and filterbox_to_category.subcat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.cat_id='$cat_id' and filterbox_to_category.pcat_id='$pcat_id') or (filterbox_to_category.cat_id='$cat_id' and filterbox_to_category.cat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.subcat_id='0' and filterbox_to_category.pcat_id='$pcat_id' and filterbox_to_category.pcat_view=1) or (filterbox_to_category.pcat_id='$pcat_id' and filterbox_to_category.pcat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.cat_id='0' and filterbox_to_category.subcat_id='0')) order by filterbox.sort_order asc";	
				}
			
			}
			
			if($cat_current=="cat"){
				if(isset($cat_id)){
					$sql="select filterbox_to_category.*,filterbox.* from filterbox_to_category,filterbox where filterbox_to_category.filterbox_id=filterbox.filterbox_id and ((filterbox_to_category.cat_id='$cat_id' and filterbox_to_category.cat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.pcat_id='$pcat_id') or (filterbox_to_category.pcat_id='$pcat_id' and filterbox_to_category.pcat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.cat_id='0')) order by filterbox.sort_order asc";	
				}
				
			}
			if($cat_current=="pcat"){
				if($cat_current_id!=0){
					//0 means all_ids_arr
					$sql="select filterbox_to_category.*,filterbox.* from filterbox_to_category,filterbox where filterbox_to_category.filterbox_id=filterbox.filterbox_id and filterbox_to_category.pcat_id='$cat_current_id' and filterbox_to_category.pcat_view='1' and filterbox.view_filterbox='1' order by filterbox.sort_order asc";
				}else{
					$sql="select filterbox_to_category.*,filterbox.* from filterbox_to_category,filterbox where filterbox_to_category.filterbox_id=filterbox.filterbox_id and filterbox_to_category.pcat_id='$cat_current_id' and filterbox_to_category.pcat_view='1' and filterbox.view_filterbox='1' order by filterbox.sort_order asc";
				}
			}
			$query=$this->db->query($sql);
			return $query->result();
		}*/
		public function get_filter_box_info($cat_current,$cat_current_id,$category_tree){
			//echo "cat_current=".$cat_current."<br>";
			//echo "cat_current_id=".$cat_current_id."<br>";
			//print_r($category_tree);
			
			extract($category_tree);
			if($cat_current=="pcat_id"){
	
				if(isset($pcat_id)){
					$sql="select filterbox_to_category.*,filterbox.* from filterbox_to_category,filterbox where filterbox_to_category.filterbox_id=filterbox.filterbox_id and ((filterbox_to_category.pcat_id='$cat_current_id' and filterbox_to_category.pcat_view='1' and filterbox.view_filterbox='1') or (filterbox_to_category.pcat_id='$pcat_id' and filterbox_to_category.pcat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.pcat_id='$pcat_id' and filterbox_to_category.pcat_view=1)) order by filterbox.sort_order asc";	
				}
			
			}
			
			if($cat_current=="cat_id"){
	
				if(isset($cat_id)){
					$sql="select filterbox_to_category.*,filterbox.* from filterbox_to_category,filterbox where filterbox_to_category.filterbox_id=filterbox.filterbox_id and ((filterbox_to_category.cat_id='$cat_current_id' and filterbox_to_category.cat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.cat_id='$cat_id' and filterbox_to_category.pcat_id='$pcat_id') or (filterbox_to_category.cat_id='$cat_id' and filterbox_to_category.cat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.cat_id='0' and filterbox_to_category.pcat_id='$pcat_id' and filterbox_to_category.pcat_view=1) or (filterbox_to_category.pcat_id='$pcat_id' and filterbox_to_category.pcat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.cat_id='0' and filterbox_to_category.cat_id='0')) order by filterbox.sort_order asc";	
				}
			
			}
			
			if($cat_current=="subcat_id"){
	
				if(isset($subcat_id)){
					$sql="select filterbox_to_category.*,filterbox.* from filterbox_to_category,filterbox where filterbox_to_category.filterbox_id=filterbox.filterbox_id and ((filterbox_to_category.subcat_id='$cat_current_id' and filterbox_to_category.subcat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.cat_id='$cat_id' and filterbox_to_category.pcat_id='$pcat_id') or 
					
					(filterbox_to_category.subcat_id='0' and filterbox_to_category.subcat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.cat_id='$cat_id' and filterbox_to_category.pcat_id='$pcat_id')
					or 
					
					(filterbox_to_category.cat_id='$cat_id' and filterbox_to_category.cat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.subcat_id='0' and filterbox_to_category.pcat_id='$pcat_id' and filterbox_to_category.pcat_view=1) or (filterbox_to_category.pcat_id='$pcat_id' and filterbox_to_category.pcat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.cat_id='0' and filterbox_to_category.subcat_id='0')) order by filterbox.sort_order asc";	
				}
			
			}
			
			if($cat_current=="brand_id"){
				if(isset($cat_id)){
					$sql="select filterbox_to_category.*,filterbox.* from filterbox_to_category,filterbox where filterbox_to_category.filterbox_id=filterbox.filterbox_id and ((filterbox_to_category.cat_id='$cat_id' and filterbox_to_category.cat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.pcat_id='$pcat_id') or (filterbox_to_category.pcat_id='$pcat_id' and filterbox_to_category.pcat_view='1' and filterbox.view_filterbox='1' and cat_id='0')) order by filterbox.sort_order asc";	
				}
				
			}
			
			if($cat_current=="product_id"){
				    
				$sql="select filterbox_to_category.*,filterbox.* from filterbox_to_category,filterbox where filterbox_to_category.filterbox_id=filterbox.filterbox_id and ((filterbox_to_category.subcat_id='$subcat_id' and filterbox_to_category.subcat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.cat_id='$cat_id' and filterbox_to_category.pcat_id='$pcat_id') or (filterbox_to_category.cat_id='$cat_id' and filterbox_to_category.cat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.subcat_id='0' and filterbox_to_category.pcat_id='$pcat_id' and filterbox_to_category.pcat_view=1) or (filterbox_to_category.pcat_id='$pcat_id' and filterbox_to_category.pcat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.cat_id='0' and filterbox_to_category.subcat_id='0')) order by filterbox.sort_order asc";  
             
                
			}

			
			if($cat_current=="inventory_id"){
					
				$sql="select filterbox_to_category.*,filterbox.* from filterbox_to_category,filterbox where filterbox_to_category.filterbox_id=filterbox.filterbox_id and ((filterbox_to_category.subcat_id='$subcat_id' and filterbox_to_category.subcat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.cat_id='$cat_id' and filterbox_to_category.pcat_id='$pcat_id') or (filterbox_to_category.cat_id='$cat_id' and filterbox_to_category.cat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.subcat_id='0' and filterbox_to_category.pcat_id='$pcat_id' and filterbox_to_category.pcat_view=1) or (filterbox_to_category.pcat_id='$pcat_id' and filterbox_to_category.pcat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.cat_id='0' and filterbox_to_category.subcat_id='0')) order by filterbox.sort_order asc";  
			
				
			}
			if($cat_current=="all"){
				    
				$sql="select filterbox_to_category.*,filterbox.* from filterbox_to_category,filterbox where filterbox_to_category.filterbox_id=filterbox.filterbox_id and ((filterbox_to_category.pcat_view='1' and filterbox.view_filterbox='1') or ( filterbox_to_category.pcat_view='1' and filterbox.view_filterbox='1' and filterbox_to_category.pcat_view=1)) order by filterbox.sort_order asc";	
             
                
			}
			$query=$this->db->query($sql);
			return $query->result();
		}
		public function get_filter_info($filterbox_id){
			$sql="select * from filter where filterbox_id='$filterbox_id' and view_filter='1' order by sort_order asc";
			$query=$this->db->query($sql);
			return $query->result();
		}
		public function get_filter_id_by_filter_name($colorFilterName){
			$sql="select filter_id from filter where lower(filter_options)=lower('$colorFilterName')";
			$query=$this->db->query($sql);
			return $query->result();
		}
		public function get_filter_info_by_filterboxname($filterbox_name){
			$sql="select * from filter where filterbox_id in (SELECT filterbox_id FROM `filterbox` where lower(filterbox_name)=lower('$filterbox_name')) and view_filter='1' order by sort_order asc";
			$query=$this->db->query($sql);
			return $query->result();
		}
		
		public function get_filter_availability_for_products($cat_current,$cat_current_id,$filter_id,$filter_prod_arr,$filter_prod_arr_with_filter_arr,$type_filter,$filter_values){
			
			if($type_filter!=""){
				$filter_prod_arr_with_filter_in="'".implode("','",$filter_prod_arr_with_filter_arr)."'";
				$sql="select count(*) as count from products_filter where inventory_id in ($filter_prod_arr_with_filter_in) and filter_id='$filter_id'";
				$query=$this->db->query($sql);
				return $query->row()->count;
			}
			else{
				$filter_prod_in="'".implode("','",$filter_prod_arr)."'";
				$sql="select count(*) as count from products_filter where inventory_id in ($filter_prod_in) and filter_id='$filter_id'";
				$query=$this->db->query($sql);
				return $query->row()->count;
			}
		

			/*
			if($cat_current=="pcat"){
				
				$sql="select count(*) as count from products,products_filter where products.pcat_id='$cat_current_id' and products.product_id=products_filter.product_id and products_filter.filter_id='$filter_id'";
				
			}
			if($cat_current=="cat"){
				$sql="select count(*) as count from products,products_filter where products.cat_id='$cat_current_id' and products.product_id=products_filter.product_id and products_filter.filter_id='$filter_id'";
			}
			if($cat_current=="subcat"){
				$sql="select count(*) as count from products,products_filter where products.subcat_id='$cat_current_id' and products.product_id=products_filter.product_id and products_filter.filter_id='$filter_id'";
			}*/
			
		}
		public function get_filter_id_constant_by_type($type_filter,$filter_prod_arr){
			$filter_prod_in="'".implode("','",$filter_prod_arr)."'";
			$filter_id_constant_arr=array();
			$sql_type_filter="select filter_id from filter where filterbox_id = (select filterbox_id from filterbox where filterbox_name='$type_filter')";
			$query_type_filter=$this->db->query($sql_type_filter);
			foreach($query_type_filter->result() as $query_type_filterObj){
				$filter_id_constant_arr[]=$query_type_filterObj->filter_id;
			}
			$filter_id_constant_in="'".implode("','",$filter_id_constant_arr)."'";
			
			$sql="select filter_id from products_filter where inventory_id in ($filter_prod_in) and filter_id in ($filter_id_constant_in)";
			$query=$this->db->query($sql);
			foreach($query->result() as $queryObj){
				$filter_id_constant_filtered_arr[$queryObj->filter_id]=$queryObj->filter_id;
			}
			
			return $filter_id_constant_filtered_arr;
		}
		public function testing_search(){
			$sql="select inventory.id as inventory_id,inventory.sku_id,inventory.sku_name,products.product_id,products.product_name,products.product_description,products.pcat_id,products.cat_id,products.subcat_id,products.brand_id,inventory.image,inventory.thumbnail,inventory.attribute_1_value,inventory.attribute_2_value,inventory.selling_price from products,inventory where products.product_id=inventory.product_id";
			$query=$this->db->query($sql);
			return $query->result();
		}
		public function get_subcat_name($pcat_id,$cat_id,$subcat_id){
            
            $sql="select subcat_name from subcategory where pcat_id='$pcat_id' and cat_id='$cat_id' and subcat_id='$subcat_id'";
            $query=$this->db->query($sql);
            $str= ($query->num_rows() > 0) ? $query->row()->subcat_name: '';
            return $str;
        }
		public function get_cat_name($pcat_id,$cat_id){
            
            $sql="select cat_name from category where pcat_id='$pcat_id' and cat_id='$cat_id'";
            $query=$this->db->query($sql);
			if(empty($query->row())){
				return "";
			}
			else{
				return $query->row()->cat_name;
			}
        }
		public function get_pcat_name($pcat_id){
            
            $sql="select pcat_name from parent_category where pcat_id='$pcat_id'";
            $query=$this->db->query($sql);
			if(empty($query->row())){
				return "";
			}
			else{
				return $query->row()->pcat_name;
			}
        }
		public function testing_search_inv(){
			$sql="select inventory.id as inventory_id,inventory.sku_id,inventory.sku_name,products.product_id,products.product_name,products.product_description,products.pcat_id,products.cat_id,products.subcat_id,products.brand_id,inventory.image,inventory.thumbnail,inventory.tax_percent,inventory.base_price,inventory.selling_price,inventory.max_selling_price,inventory.selling_discount,inventory.common_image,inventory.inventory_type from products,inventory where products.product_id=inventory.product_id and products.view=1 and products.active=1 and inventory.active=1";
			$query=$this->db->query($sql);
			return $query->result();
		}
        public function testing_cat_subCat(){
            $sql="select subcat.subcat_name ,subcat.subcat_id,subcat.cat_id,subcat.pcat_id, category.cat_name, brands.brand_name,brands.brand_id,products.product_name,products.product_id from subcategory as subcat,category,brands,products where subcat.cat_id=category.cat_id and brands.subcat_id=subcat.subcat_id and products.brand_id=brands.brand_id and subcat.view=1 and category.view=1 and brands.view=1 and products.view=1 and subcat.active=1 and category.active=1 and brands.active=1 and products.active=1";
            $query=$this->db->query($sql);
            return $query->result();
        }
		public function pcat_table_empty_or_not(){
			$sql="SELECT * from parent_category where view=1 and active=1";
            $query=$this->db->query($sql);
            return $query->result();
		}
        public function testing_pcat_Cat(){
			$pcat_table_empty_or_not_obj=$this->pcat_table_empty_or_not();
			if(!empty($pcat_table_empty_or_not_obj)){
				$sql="SELECT parent_category.pcat_name, parent_category.pcat_id, subcategory.subcat_name, subcategory.subcat_id, category.cat_name,category.cat_id FROM subcategory ,category,parent_category where category.cat_id = subcategory.cat_id and parent_category.pcat_id = category.pcat_id and subcategory.view=1 and parent_category.view=1 and category.view=1 and subcategory.active=1 and parent_category.active=1 and category.active=1";
			}
			else{
				$sql="SELECT '' pcat_name, 0 as pcat_id, subcategory.subcat_name, subcategory.subcat_id, category.cat_name,category.cat_id FROM subcategory ,category where category.cat_id = subcategory.cat_id and subcategory.view=1 and category.view=1 and subcategory.active=1 and category.active=1";
			}
            $query=$this->db->query($sql);
            return $query->result();
        }
		
		public function get_filter_id_new($inventory_id){
			$sql="select * from  products_filter where inventory_id='$inventory_id' and active=1";
			$query=$this->db->query($sql);
			return $query->result();
		}
		public function get_filterbox_name($filter_id){
			$sql="select filterbox_name from  filterbox where view_filterbox=1 and active=1 and filterbox_id = (select filterbox_id from filter where filter_id='$filter_id')";
			$query=$this->db->query($sql);
			
			if(!empty($query->row())){
				return $query->row()->filterbox_name;
			}
			else{
				return "";
			}
			
		}
		public function get_filterbox_type($filter_id){
			$sql="select type from  filterbox where view_filterbox=1 and active=1 and filterbox_id = (select filterbox_id from filter where filter_id='$filter_id')";
			$query=$this->db->query($sql);
			
			if(!empty($query->row())){
				return $query->row()->type;
			}
			else{
				return "";
			}
			
		}
		public function get_filterbox_units($filterbox_name){
			$sql="select filterbox_units from filterbox where lower(filterbox_name)=lower('$filterbox_name')";
			$query=$this->db->query($sql);
			if(!empty($query->row())){
				return $query->row()->filterbox_units;
			}
			else{
				return "";
			}
		}
		public function get_filter_options($filter_id){
			$sql="select filter_options from filter where filter_id='$filter_id'";
			$query=$this->db->query($sql);
			
			if(!empty($query->row())){
				return $query->row()->filter_options;
			}
			else{
				return "";
			}
			
		}
		public function get_color_images($product_id){
			$sql="select filter.filter_id,inventory.image from inventory,filter where lower(filter.filter_options)=lower(inventory.attribute_1_value) and  inventory.product_id='$product_id'";
			$query=$this->db->query($sql);
			return $query->result();
		}
		public function get_product_filters($product_id){
			$sql="select products.brand_id,inventory.attribute_1_value,inventory.attribute_2_value,inventory.selling_price from inventory,products where inventory.product_id='$product_id' and products.product_id=inventory.product_id";
			$query=$this->db->query($sql);
			return $query->result();
		}
		public function get_filter_id($filter_options,$type_of_filter){
			if($type_of_filter=="color" || $type_of_filter=="size"){
				$sql="select filter_id from filter where lower(filter_options)=lower('$filter_options')";
				$query=$this->db->query($sql);
				return $query->row()->filter_id;
			}
			if($type_of_filter=="price"){
				$sql="select filterbox_id from filterbox where lower(filterbox_name)=lower('price')";
				$query=$this->db->query($sql);
				$filterbox_id=$query->row()->filterbox_id;
				$sql_filter="select filter_id,filter_options from filter where filterbox_id='$filterbox_id'";
				$query_filter=$this->db->query($sql_filter);
				foreach($query_filter->result() as $qfilterObj){
					$leftprice=trim(explode("-",$qfilterObj->filter_options)[0]);
					$rightprice=trim(explode("-",$qfilterObj->filter_options)[1]);
					if(($leftprice<=$filter_options) && ($rightprice>=$filter_options)){
						$filter_id=$qfilterObj->filter_id;
						break;
					}
				}
				return $filter_id;
			}
		}
		public function search_keywords($searchTerm){
			$sugg_arr=array("sugg1","sugg2","generalsugg1","generalsugg2","generalsugg3","generalsugg4");
			foreach($sugg_arr as $sugg){
				$sql="SELECT $sugg FROM products WHERE $sugg LIKE '%".$searchTerm."%'";
				$query=$this->db->query($sql);
				$sugg_res=$query->result();
				foreach($sugg_res as $sugObj){
					$product_name_arr[$sugObj->$sugg]=$sugObj->$sugg;
				}
			}
			//print_r($product_name_arr);
			return $product_name_arr;
		}
		public function get_inventory_product_info_by_inventory_id($inventory_id){
			//$sql="select products.*,inventory.*,logistics_inventory.*,(inventory.stock-inventory.cutoff_stock) as stock_available from products,inventory,logistics_inventory where products.product_id=inventory.product_id and id='$inventory_id' and inventory.id=logistics_inventory.inventory_id";
			

			$sql='select products.*,inventory.*,logistics_inventory.*,(inventory.stock-inventory.cutoff_stock) as stock_available from inventory ';
			$sql.= ' LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`'
            .' LEFT JOIN `logistics_inventory` ON `logistics_inventory`.`inventory_id` = `inventory`.`id`'
            ." where  inventory.id='$inventory_id'";
    

			$query=$this->db->query($sql);
			return $query->row();
		}
		public function get_product_filter_attributes($product_id){
			$att_total_arr=array();
			$attribute_arr=array("attribute_1"=>"attribute_1_value","attribute_2"=>"attribute_2_value","attribute_3"=>"attribute_3_value","attribute_4"=>"attribute_4_value");
			foreach($attribute_arr as $k_att => $v_att){
				$sql_att="select distinct $k_att from inventory where product_id='$product_id' and $k_att!='' and active=1";
				$query_att=$this->db->query($sql_att);
				if(!empty($query_att->row())){
					if($query_att->row()->$k_att!=""){
						$sql_att_val="select distinct $v_att from inventory where product_id='$product_id' and active=1";
						$query_att_val=$this->db->query($sql_att_val);
						$att_arr=array();
						$att_values_arr=array();
						foreach($query_att_val->result() as $vattObj){
							//$att_values_arr[]=strtolower($vattObj->$v_att);
							if($vattObj->$v_att!=""){
								$att_values_arr[]=$vattObj->$v_att;
							}
						}
						$att_arr[strtolower($query_att->row()->$k_att)]=$att_values_arr;
						$att_total_arr[$v_att]=$att_arr;
					}
				}
			}
			
			return $att_total_arr;
		}
		public function get_attribute_value_exists_or_not($att_total_key,$inventory_id){
			$sql="select $att_total_key from inventory where id='$inventory_id' and active=1";
			$query=$this->db->query($sql);
			if(!empty($query->row())){
				if($query->row()->$att_total_key!=""){
					return "yes";
				}
				else{
					return "no";
				}
			}
			else{
				return "no";
			}
		}
		public function get_availability_of_colors_based_on_remaining_attributes($available_remaining_attributes_for_color_arr){
			
			$attribute_2="";
			$attribute_2_value="";
			$attribute_3="";
			$attribute_3_value="";
			$attribute_4="";
			$attribute_4_value="";
			$ii=2;
			foreach($available_remaining_attributes_for_color_arr as $att_name =>$att_value){
				if($ii==2){
					
					$attribute_2_value=$att_value;
				}
				if($ii==3){
					
					$attribute_3_value=$att_value;
				}
				if($ii==4){
					
					$attribute_4_value=$att_value;
				}
				$ii++;
			}
			if($attribute_2_value!=""){
				$sql="select attribute_1_value from inventory where lower(attribute_2_value)=lower('$attribute_2_value')";
			}
			if($attribute_3_value!=""){
				$sql.=" and lower(attribute_3_value)=lower('$attribute_3_value')";
			}
			if($attribute_4_value!=""){
				$sql.=" and lower(attribute_4_value)=lower('$attribute_4_value')";
			}
			$sql.=" and lower(attribute_1)='color'";
			$query=$this->db->query($sql);
			return $query->result_array();
		}
		public function get_available_remaining_attributes($attribute_n_value,$attribute_value,$product_id,$val1=''){
			$att_total_arr=array();
			$attribute_arr=array("attribute_1"=>"attribute_1_value","attribute_2"=>"attribute_2_value","attribute_3"=>"attribute_3_value","attribute_4"=>"attribute_4_value");
			$attribute_arr=array_flip($attribute_arr);
                        if($val1==''){
			unset($attribute_arr[$attribute_n_value]);
                        }
			$attribute_arr=array_flip($attribute_arr);
			foreach($attribute_arr as $k_att => $v_att){
				$sql_att="select distinct $k_att from inventory where product_id='$product_id' and $attribute_n_value='$attribute_value'";
				$query_att=$this->db->query($sql_att);
				if($query_att->row()->$k_att!=""){
					$sql_att_val="select distinct $v_att from inventory where product_id='$product_id' and $attribute_n_value='$attribute_value' and product_status='In Stock'";
					$query_att_val=$this->db->query($sql_att_val);
					$att_arr=array();
					$att_values_arr=array();
					foreach($query_att_val->result() as $vattObj){
						$att_values_arr[]=strtolower($vattObj->$v_att);
					}
					$att_arr[strtolower($query_att->row()->$k_att)]=$att_values_arr;
					$att_total_arr[]=$att_arr;
				}
			}
			
			return $att_total_arr;
		}
		
		public function get_inventory_id_by_series_filter($input_params){
			$whereClause_arr=array();
			foreach($input_params as $fieldname => $fieldval){
				$whereClause_arr[]='lower('.$fieldname.')'."="."lower('".$fieldval."')";
			}
			$whereClause=implode(" and ",$whereClause_arr);

                        $sql="select id from inventory where $whereClause limit 0,1";
                        //echo $sql;
			$query=$this->db->query($sql);
                        
                        if($query->num_rows()>0){
                            return $query->row()->id;    
                        }else{
                        
                            unset($whereClause_arr[count($whereClause_arr)-1]);
                            if(!empty($whereClause_arr)){
                                $whereClause=implode(" and ",$whereClause_arr);
                                $sql="select id from inventory where $whereClause limit 0,1";
                                //echo "||".$sql;
                                //print_r($whereClause_arr);
                                
                                $query=$this->db->query($sql);
                                if($query->num_rows()>0){
                                    return $query->row()->id;    
                                }else{
                                    unset($whereClause_arr[count($whereClause_arr)-1]);
                                    if(!empty($whereClause_arr) && count($whereClause_arr)>0){
                                        $whereClause=implode(" and ",$whereClause_arr);
                                        $sql="select id from inventory where $whereClause limit 0,1";
                                        //echo "||".$sql;
                                        $query=$this->db->query($sql);
                                        if($query->num_rows()>0){
                                            return $query->row()->id;    
                                        }
                                    }

                                }
                            }
                        
                        }
		
		}
		//addedd////////////
		public function get_specification_group_by_category($all_ids_arr){
			
			//print_r($all_ids_arr);
			
			//first priority is for subcat
			//next check the category where subcat!=subcat idate
			//next check the parent_category where cat_id!=cat_id and subcat_id!=subcat
			
			$allowed_specification_group_arr=array();	
			//direct array

			$brand_id=$all_ids_arr["brand_id"];
			$subcat_id=$all_ids_arr["subcat_id"];
			$cat_id=$all_ids_arr["cat_id"];
			$pcat_id=$all_ids_arr["pcat_id"];
			
			if($subcat_id!=0){
				$sql="select specification_group_id from specification_group_to_categories where (subcat_id='$subcat_id' and subcat_view=1) or (cat_id='$cat_id' and cat_view=1 and subcat_id=0) or (pcat_id='$pcat_id' and pcat_view=1 and cat_id=0 and subcat_id=0)";
				
			}
			else if($cat_id!=0){
				$sql="select specification_group_id from specification_group_to_categories where (cat_id='$cat_id' and cat_view=1 and subcat_id=0) or (pcat_id='$pcat_id' and pcat_view=1  and cat_id=0 and subcat_id=0)";
			}
			else if($pcat_id!=0){
				$sql="select specification_group_id from specification_group_to_categories where pcat_id='$pcat_id' and pcat_view=1 and pcat_view=1 and subcat_id=0 and cat_id=0";
			}
			
			$query =$this->db->query($sql);
			$specification_group_ids = $query->result();
			if(count($specification_group_ids)>0){
					
				foreach($specification_group_ids as $group_ids_k => $group_ids_v){
					$allowed_specification_group_arr[]=$group_ids_v->specification_group_id;
				}
			}
			/*foreach($all_ids_arr as $key=>$val){
				
				$sql="select specification_group_id from specification_group_to_categories where subcat_id='$val'";
				
				$query =$this->db->query($sql);
				$specification_group_ids = $query->result();
				
				if(count($specification_group_ids)>0)
				{
					foreach($specification_group_ids as $group_ids_k => $group_ids_v)
					{
						$allowed_specification_group_arr[]=$group_ids_v->specification_group_id;
					}
					
				}
			}*/
			
			//print_r($allowed_specification_group_arr);
			return $allowed_specification_group_arr;
			
		}
		public function get_specification_groups($allowed_specification_group_arr)
		{
			$allowed_specification_group_in="'".implode("','",$allowed_specification_group_arr)."'";
			//echo $allowed_specification_group_in;
			$sql="select specification_group_id,specification_group_name from specification_group where specification_group_id in ($allowed_specification_group_in) and view_specification_group=1";
			$query =$this->db->query($sql);
			return $query->result_array();
			
		}
		
		public function get_specification_values($specification_group_id,$inventory_id){
			$sql="select specification.specification_name,inventory_specification.specification_value from specification,inventory_specification where specification.specification_group_id='$specification_group_id' and inventory_specification.inventory_id='$inventory_id' and specification.specification_id=inventory_specification.specification_id";
			$query =$this->db->query($sql);
			return $query->result_array();
		}
		
		public function get_info_for_wishlist($inventory_id){
			$sql="select inventory.image,inventory.common_image,,inventory.inventory_type,inventory.selling_price,inventory.product_status,products.product_name from inventory,products where inventory.product_id=products.product_id and inventory.id='$inventory_id'";
			$res=$this->db->query($sql);
			return $res->row();
		}
		public function add_to_wishlist($inventory_id,$product_name,$image,$selling_price,$product_status){
			$customer_id=$this->session->userdata("customer_id");
			$sql="insert into wishlist set customer_id='$customer_id',inventory_id='$inventory_id',product_name='$product_name',product_image='$image',product_price='$selling_price',status='$product_status'";
			$res=$this->db->query($sql);
			return $res;
		}
		public function check_wishlist($inventory_id){
			$customer_id=$this->session->userdata("customer_id");
			$sql="select count(*) as count from wishlist where customer_id='$customer_id' and inventory_id='$inventory_id'";
			$res=$this->db->query($sql);
			return $res->row()->count;
		}
		
		public function delete_wishlist($inventory_id){
			$customer_id=$this->session->userdata("customer_id");
			$sql="delete from wishlist where customer_id='$customer_id' and inventory_id='$inventory_id'";
			$res=$this->db->query($sql);
			return $res;
		}

		//////////adddeddd////////
		
		public function get_average_value_of_ratings($inventory_id){
			$sql1 = "SELECT SUM(`rating`) as sum_rating  FROM `rated_inventory` WHERE    `inventory_id` = '$inventory_id' AND `rating`!='' AND `status`='1' ";
            $query=$this->db->query($sql1);   
			foreach($query->result_array() as $num_rows1){
				$sql2 = "SELECT *  FROM `rated_inventory` WHERE    `inventory_id` = '$inventory_id' AND `rating`!='' AND `status`='1' ";
				$result2=$this->db->query($sql2); 
				$num_rows2 = $result2->num_rows($result2);
				$rate2 = $num_rows1['sum_rating']; 	
				if($rate2 !=0){	
				
							$avg1= $rate2/$num_rows2;
							$avg = round($avg1 , 11);	
				}
			
			if(isset($avg)){
                        $n=floor($avg*10);
                        $n1=floor($n/10);
                        if(($n%10)==4 || ($n%10)==5 || ($n%10)==6){
                            $avg_value=(float)($n1."."."5");
                        }
                        else if(($n%10)<4){
                            $avg_value=$n1;
                        }
                        else{
                            $avg_value=$n1+1;
                        }
                        $avg_value;
                    }
                    else{
                        $avg_value=0;
                    }
			
			}
			
				return $avg_value;
                    
		}
		public function number_of_ratings($inventory_id){
			$sql = "SELECT count(*)  FROM `rated_inventory` WHERE    `inventory_id` = '$inventory_id' AND `rating`!='' AND `status`=1 ";
			$query =$this->db->query($sql);
			$result = $query->row_array();
			$count = $result['count(*)'];
			return $count;	
		}
		public function number_of_reviews($inventory_id){
			$sql = "SELECT count(*) FROM `rated_inventory` WHERE    `inventory_id` = '$inventory_id' AND `review`!='' AND `status` = 1 ";        
			$query =$this->db->query($sql);
			$result = $query->row_array();
			$count = $result['count(*)'];
			return $count;	
		}
		public function get_data_for_reviews($inventory_id){
			$sql="select * from rated_inventory where inventory_id='$inventory_id' and status=1 ORDER BY `rating` DESC limit 0,3";
			$res=$this->db->query($sql);
			return $res->result();
		}
		public function rated_inventory($data){
			return $this->db->insert('rated_inventory',$data);
		}
		public function check_review_exists($inventory_id,$customer_id,$order_item_id){
			
			$sql = "SELECT count(*) FROM `rated_inventory` WHERE `inventory_id` = '$inventory_id' AND `customer_id`='$customer_id' AND order_item_id='$order_item_id'";    
			$query =$this->db->query($sql);
			$result = $query->row_array();
			$count = $result['count(*)'];
			if($count!=0){
				return "yes";
			}else{
				return "no";
			}
		}
		/*public function check_review_exists_in_rateorder($order_item_id,$customer_id){
			
			$sql = "SELECT count(*) FROM `rateorder` WHERE `order_item_id` = '$order_item_id' AND `customer_id`='$customer_id'";        
			$query =$this->db->query($sql);
			$result = $query->row_array();
			$count = $result['count(*)'];
			if($count!=0){
				return "yes";
			}else{
				return "no";
			}
		}
		public function update_ratedorder($data,$order_item_id,$customer_id){
			$this->db->where(array("order_item_id"=>$order_item_id,"customer_id"=>$customer_id));
			$query=$this->db->update("rateorder",$data);	
			if($query==true){
				return true;
			}else{
				return false;
			}
		}
		public function add_to_ratedorder($data){
			return $this->db->insert('rateorder',$data);
		}*/
		public function update_rated_inventory($data,$inventory_id,$customer_id){
			$this->db->where(array("inventory_id"=>$inventory_id,"customer_id"=>$customer_id));
			$query=$this->db->update("rated_inventory",$data);	
			if($query==true){
				return true;
			}else{
				return false;
			}
		}
		public function total_show_all_reviews($inventory_id,$records_per_page,$rating_value){
			if($rating_value==0){
			$sql="select * from rated_inventory where inventory_id='$inventory_id' and status=1";
		}else{
			$sql="select * from rated_inventory where inventory_id='$inventory_id' and status=1 and rating='$rating_value'";
		}
			$res=$this->db->query($sql);
			
			$total_no_of_records=$res->num_rows();
			
			$total_pages=ceil($total_no_of_records/$records_per_page);
			return $total_pages;
		}
		public function show_all_reviews($inventory_id,$records_per_page,$pageno,$rating_value){
			
			//$limit="limit ".($pageno-1)*$records_per_page.", ".$records_per_page;	
			$limit='';
			if($rating_value==0){	
			  $sql_select="select * from rated_inventory where inventory_id='$inventory_id' and status=1 ORDER BY `rating` DESC ".$limit;
			}else{
				$sql_select="select * from rated_inventory where inventory_id='$inventory_id' and status=1 and rating='$rating_value' ORDER BY `rating` DESC ".$limit;
			}
			
			//exit;
			$result=$this->db->query($sql_select);
			
			return $result->result();
		}
		public function get_product_name_by_inventory_id($inventory_id){
			$sql="select products.product_name from products,inventory where inventory.id='$inventory_id' and products.product_id=inventory.product_id";
			$res=$this->db->query($sql);
			return $res->row()->product_name;
		}
		public function get_total_reviews($inventory_id){
			$sql="select count(*) as count from rated_inventory where  inventory_id='$inventory_id'and status=1";
			$res=$this->db->query($sql);
			return $res->row()->count;	
		}
		public function get_total_rate_count($inventory_id){
			$sql="select count(*) as count from rated_inventory where  inventory_id='$inventory_id'and status=1 and rating!=''";
			$res=$this->db->query($sql);
			return $res->row()->count;
		}
		public function get_total_review_count($inventory_id){
			$sql="select count(*) as count from rated_inventory where  inventory_id='$inventory_id'and status=1 and (review!='' or review_title!='')";
			$res=$this->db->query($sql);
			return $res->row()->count;
		}
		public function number_ratings_one_to_five($inventory_id){	
			$temp=array();
			$sql_1="select count(*) as count from rated_inventory where  inventory_id='$inventory_id' and rating=1 and status=1";
			$res_1=$this->db->query($sql_1);
			$temp["one"]=$res_1->row()->count;	
			$sql_2="select count(*) as count from rated_inventory where  inventory_id='$inventory_id' and rating=2 and status=1";
			$res_2=$this->db->query($sql_2);
			$temp["two"]=$res_2->row()->count;
			$sql_3="select count(*) as count from rated_inventory where  inventory_id='$inventory_id' and rating=3 and status=1";
			$res_3=$this->db->query($sql_3);
			$temp["three"]=$res_3->row()->count;	
			$sql_4="select count(*) as count from rated_inventory where  inventory_id='$inventory_id' and rating=4 and status=1";
			$res_4=$this->db->query($sql_4);
			$temp["four"]=$res_4->row()->count;
			$sql_5="select count(*) as count from rated_inventory where  inventory_id='$inventory_id' and rating=5 and status=1";
			$res_5=$this->db->query($sql_5);
			$temp["five"]=$res_5->row()->count;			
			
			return $temp;
		}
		////added for shipping charge calculation//////
		public function get_default_logistics($vendor,$logistics_id){
			$sql="select logistics_id,logistics_name,logistics_weblink from logistics where vendor_id='$vendor' and logistics_id='$logistics_id' and default_logistics=1 order by logistics_priority asc limit 0,1";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row_array();
			}
		}
		public function get_logistics_service_details($vendor_id,$default_logistics_id,$pincode,$territory_name_id){
			$pin=substr($pincode,0,3);
			$sql="select logistics_service_id,city from logistics_service where logistics_id='$default_logistics_id' and pin='$pin' and territory_name_id='$territory_name_id' and pincode like '%$pincode%'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row_array();
			}
		}
		public function get_imp_info_json_name_by_pincode($pincode){
			$pin=substr($pincode,0,3);
			$sql="select imp_info_json from logistics_all_pincodes where pin='$pin' and pincodes like '%$pincode%'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row_array()["imp_info_json"];
			}
			else{
				return "None";
			}
		}
		public function get_territory_of_pincode($logistics_service_id){
			$sql="select logistics_territory_id from logistics_territory where logistics_service_id='$logistics_service_id'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row()->logistics_territory_id;
			}
		}
		public function default_delivery_mode($logistics_id){
			$sql="select logistics_delivery_mode_id,delivery_mode,speed_duration from logistics_delivery_mode where default_delivery_mode=1 and logistics_id='$logistics_id'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row_array();
			}
		}
		public function get_default_parcel_category($logistics_id){
			$sql="select * from logistics_parcel_category where default_parcel_category='1' and logistics_id='$logistics_id'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row_array();
			}
		}
		public function get_logistics_inventory_data($inventory_id){
			$sql="select * from logistics_inventory where inventory_id='$inventory_id'";
			$result=$this->db->query($sql);
			return (isset($result->result()[0])) ? $result->result()[0]: 0;
		}
		public function available_parcel_category_of_inventory($inventory_id,$parcel_cat_arr){
			$sql="select * from logistics_parcel_category where logistics_parcel_category_id in ($parcel_cat_arr)";
			$query=$this->db->query($sql);
			return $query->result();
		}
		public function get_logistics_by_logistics_id($logistics_id){
			$sql="select * from logistics where logistics_id='$logistics_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		
		public function get_logistics_parcel_box_dimension_info($logistics_id){
			$get_logistics_by_logistics_id_row=$this->get_logistics_by_logistics_id($logistics_id);
			$vendor_id=$get_logistics_by_logistics_id_row->vendor_id;
			$sql="select * from logistics_parcel_box_dimension where vendor_id='$vendor_id' order by lbh asc";
			$query=$this->db->query($sql);
			return $query->result();
		}
		public function get_inventory_type($inventory_id){
			$sql="select inventory_type from inventory where id='$inventory_id'";
			$query=$this->db->query($sql);
			return $query->row()->inventory_type;
		}
		public function get_inventory_for_addons_weight_in_kg($addons_inv_list_arr){
			$inventory_addon_weight_in_kg=0;
			foreach($addons_inv_list_arr as $inv_id){
				$inventory_type=$this->get_inventory_type($inv_id);
				if($inventory_type=='2'){
					$sql_inventory_type="select inventory_addon_weight_in_kg from inventory where id='$inv_id'";
					$query_inventory_type=$this->db->query($sql_inventory_type);
					if(!empty($query_inventory_type->row())){
						$inventory_addon_weight_in_kg=$inventory_addon_weight_in_kg+(float)($query_inventory_type->row()->inventory_addon_weight_in_kg);
					}
					
				}
				else{
					$logistics_inventory_obj=$this->get_logistics_inventory_data($inv_id);
					$inventory_addon_weight_in_kg=$inventory_addon_weight_in_kg+(float)($logistics_inventory_obj->inventory_weight_in_kg);
				}
			}
			return $inventory_addon_weight_in_kg;
		}
		public function get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id($logistics_parcel_box_dimension_id){
			$sql="select * from logistics_parcel_box_dimension where logistics_parcel_box_dimension_id='$logistics_parcel_box_dimension_id'";
			$query=$this->db->query($sql);
			return $query->row();
		} 
		public function get_logistics_price($current_quantity,$inventory_id,$logistics_id,$inventory_weight_in_kg,$inventory_volume_in_kg,$logistics_territory_id="",$default_delivery_mode_id="",$logistics_parcel_category_id=""){
			
			
			/* new code - nanthini  */
			
			$weight=($inventory_weight_in_kg=='') ? 0 : $inventory_weight_in_kg;

			$sql="SELECT logistics_price,SUBSTRING_INDEX(weight_in_kg,'-',1) as 'weight_in_kg_from',SUBSTRING_INDEX(weight_in_kg,'-',-1) as 'weight_in_kg_to'
FROM logistics_weight where $weight between SUBSTRING_INDEX(weight_in_kg,'-',1) and SUBSTRING_INDEX(weight_in_kg,'-',-1) and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id'";
			
			
			/*$sql="SELECT logistics_price,
			REPLACE(weight_in_kg, '-', ',') as dashRep
			FROM logistics_weight
			WHERE FIND_IN_SET($weight, REPLACE(weight_in_kg, '-', ',')) and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id'";*/

			$query=$this->db->query($sql);

			$res=$query->row_array();
			if(!empty($res)){
				$logistics_price_with_tax=$res["logistics_price"];
			}else{
				$logistics_price_with_tax=0;// need to fix some values when there is no data
			}
			$required_volume_in_kg=$inventory_weight_in_kg;

			//print_r($res);

			/* new code - nanthini  */

			return array("resultant_inventory_volume_in_kg"=>$required_volume_in_kg,"logistics_price_with_tax"=>round($logistics_price_with_tax));
			
			/*
			
			$get_logistics_inventory_data_obj=$this->get_logistics_inventory_data($inventory_id);
			$inventory_weight_in_kg_per_unit=$inventory_weight_in_kg/$current_quantity;

			$inventory_weight_in_kg_per_unit;

			$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
			$logistics_volumetric_value=$get_logistics_by_logistics_id_obj->logistics_volumetric_value;
			
			/////// for all the skus code 12-21-2019 based on packings not volumetric weight starts/////
			
			$inventory_length=$get_logistics_inventory_data_obj->inventory_length;
			$inventory_breadth=$get_logistics_inventory_data_obj->inventory_breadth;
			$inventory_height=$get_logistics_inventory_data_obj->inventory_height;
			
			$inventory_moq=$get_logistics_inventory_data_obj->inventory_moq;
			$suitable_box_arr=array();
			$get_logistics_parcel_box_dimension_info_result=$this->get_logistics_parcel_box_dimension_info($logistics_id);
			foreach($get_logistics_parcel_box_dimension_info_result as $get_logistics_parcel_box_dimension_info_row){
				$available_space_for_filling_length=$get_logistics_parcel_box_dimension_info_row->available_space_for_filling_length;
				$available_space_for_filling_breadth=$get_logistics_parcel_box_dimension_info_row->available_space_for_filling_breadth;
				$available_space_for_filling_height=$get_logistics_parcel_box_dimension_info_row->available_space_for_filling_height;
			
				if($inventory_length<=$available_space_for_filling_length && $inventory_breadth<=$available_space_for_filling_breadth & $inventory_height<=$available_space_for_filling_height){
						$suitable_box_arr[$get_logistics_parcel_box_dimension_info_row->logistics_parcel_box_dimension_id]=0;
				}
			}
			
			foreach($suitable_box_arr as $logistics_parcel_box_dimension_id => $val){
				$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row=$this->get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id($logistics_parcel_box_dimension_id);
				$box_available_space_for_filling_length=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->available_space_for_filling_length;
				$box_available_space_for_filling_breadth=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->available_space_for_filling_breadth;
				$box_available_space_for_filling_height=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->available_space_for_filling_height;
				
				$box_available_space_for_filling_length_temp=$box_available_space_for_filling_length;
				$box_available_space_for_filling_breadth_temp=$box_available_space_for_filling_breadth;
				$box_available_space_for_filling_height_temp=$box_available_space_for_filling_height;
				$flag=true;
				
				while($flag){
					if($inventory_length<=$box_available_space_for_filling_length_temp){
						$suitable_box_arr[$logistics_parcel_box_dimension_id]++;
						
						$box_available_space_for_filling_length_temp=$box_available_space_for_filling_length_temp-$inventory_length;
						$box_available_space_for_filling_breadth_temp=$box_available_space_for_filling_breadth_temp-$inventory_breadth;
						$box_available_space_for_filling_height_temp=$box_available_space_for_filling_height_temp-$inventory_height;
								
								
					}
					else{
						$flag=false;
					}
				}
				
			}
			arsort($suitable_box_arr);
			
			//print_r($suitable_box_arr);
			$no_of_boxes_arr=[];
			$extra_box_arr=[];
			$quantity_filled=0;
			$current_quantity_temp=$current_quantity;
			foreach($suitable_box_arr as $box_id => $box_fitted_quantity){
				if($current_quantity_temp>=$box_fitted_quantity){
					$no_of_boxes_arr[$box_id]["no_of_boxes"]=(int)($current_quantity_temp/$box_fitted_quantity);
					$quantity_filled+=($no_of_boxes_arr[$box_id]["no_of_boxes"]*$suitable_box_arr[$box_id]);
					$current_quantity_temp=$current_quantity_temp%$box_fitted_quantity;
				}
			}
			
			$remaining_quantity=$current_quantity-$quantity_filled;
			if($remaining_quantity>0){
				foreach($suitable_box_arr as $box_id => $box_fitted_quantity){
					if((100-(($remaining_quantity/$box_fitted_quantity))*100)<=50){ // remaining space
						$extra_box_arr[$box_id]["no_of_quantities"]=$remaining_quantity;
					}
				}
			}
			//print_r($no_of_boxes_arr);
			//print_r($extra_box_arr);
			$total_arr=array("no_of_boxes_arr"=>$no_of_boxes_arr,"extra_box_arr"=>$extra_box_arr);
			$max_actual_volumetric_arr=array();
			foreach($total_arr as $k=>$v_arr){
				if($k=="no_of_boxes_arr"){
					if(!empty($v_arr)){
						foreach($v_arr as $box_id => $boxes_arr){
							$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row=$this->get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id($box_id);
							
							$parcel_lbh=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_length*
							$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_breadth*
							$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_height;
							for($i=1;$i<=$boxes_arr["no_of_boxes"];$i++){
								$box_actual_weight_without_packing=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->box_actual_weight_without_packing;
								
								$total_actual_weight_parcel_sku=($inventory_weight_in_kg_per_unit*$suitable_box_arr[$box_id])+$box_actual_weight_without_packing;
								
								$volumetric_weight_of_parcel_in_kg=$parcel_lbh/$logistics_volumetric_value;
								
								$max_actual_volumetric_arr[]=max($total_actual_weight_parcel_sku,$volumetric_weight_of_parcel_in_kg);
							}
							//$boxes_arr["no_of_boxes"]
						}
					}
				}
				if($k=="extra_box_arr"){
					if(!empty($v_arr)){
						foreach($v_arr as $box_id => $boxes_arr){
							$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row=$this->get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id($box_id);
							
							$parcel_lbh=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_length*
							$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_breadth*
							$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_height;
							
							$box_actual_weight_without_packing=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->box_actual_weight_without_packing;
							
							$total_actual_weight_parcel_sku=($inventory_weight_in_kg_per_unit*$boxes_arr["no_of_quantities"])+$box_actual_weight_without_packing;
							
							$volumetric_weight_of_parcel_in_kg=$parcel_lbh/$logistics_volumetric_value;
							
							$max_actual_volumetric_arr[]=max($total_actual_weight_parcel_sku,$volumetric_weight_of_parcel_in_kg);
							
							//$boxes_arr["no_of_boxes"]
						}
					}
				}
			}
			//print_r($max_actual_volumetric_arr);
			$required_volume_in_kg=array_sum($max_actual_volumetric_arr);
			//$required_volume_in_kg=2;
			
			
			
			/////// for all the skus code 12-21-2019 based on packings not volumetric weight ends/////
			
			
			//$inventory_volume_in_lbh_cms=$get_logistics_inventory_data_obj->inventory_volume_in_kg;
			//$quantity_volumetric_breakup_point=$get_logistics_inventory_data_obj->volumetric_breakup_point;
			
			
			//$net_volumetric_volume_weight=$inventory_volume_in_lbh_cms/$logistics_volumetric_value;
			
			//if($current_quantity<($quantity_volumetric_breakup_point+1)){
				//$resultant_inventory_volume_in_kg=$net_volumetric_volume_weight;
			//}
			//else{
				//$resultant_inventory_volume_in_kg=(intval(($current_quantity-1)/$quantity_volumetric_breakup_point)+1)*$net_volumetric_volume_weight;
			//}
			
			
			//$required_volume_in_kg=max($inventory_weight_in_kg,$resultant_inventory_volume_in_kg);
			
			
			
			
			$logistics_price=0;
			
			$sql="SELECT logistics_price,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg!='yes'";
			
			$query=$this->db->query($sql);
			if(!empty($query->result())){
				$logistics_price=0;
				foreach($query->result() as $resObj){
					$logistics_price=$resObj->logistics_price;
					break;
				}
				//////////////////// Tax caculation starts /////
				$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
				$logistics_gst=$get_logistics_by_logistics_id_obj->logistics_gst;
				$logistics_fuelsurcharge=$get_logistics_by_logistics_id_obj->logistics_fuelsurcharge;
				$logistics_tax=$logistics_gst+$logistics_fuelsurcharge;
				$logistics_price_with_tax=$logistics_price+($logistics_price*($logistics_tax/100));
				//////////////////// Tax caculation ends /////
				return array("resultant_inventory_volume_in_kg"=>$required_volume_in_kg,"logistics_price_with_tax"=>round($logistics_price_with_tax));
			}
			
			/// if parcel category id is there no problem
			$sql_additional_weight_in_kg="SELECT logistics_price,volume_in_kg,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg='yes'";
			
			$query_additional_weight_in_kg=$this->db->query($sql_additional_weight_in_kg);
			if(!empty($query_additional_weight_in_kg->result())){
				//////////////////
				$sql_take_latest_price_before_breakpoint="SELECT logistics_price,volume_in_kg,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where  territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg!='yes' order by logistics_weight_id desc limit 0,1";
				
				$query_take_latest_price_before_breakpoint=$this->db->query($sql_take_latest_price_before_breakpoint);
				$logistics_price_latest_price_before_breakpoint=$query_take_latest_price_before_breakpoint->row_array()["logistics_price"];
				$logistics_weight_latest_price_before_breakpoint=$query_take_latest_price_before_breakpoint->row_array()["volume_in_kg"];
				$logistics_weight_latest_price_before_breakpoint_max=explode("-",$logistics_weight_latest_price_before_breakpoint)[1];
				/////////////////
				$logistics_price=0;
				foreach($query_additional_weight_in_kg->result() as $resObj){
					$additional_weight_value_in_kg=$resObj->additional_weight_value_in_kg;
					$do_u_want_to_roundup=$resObj->do_u_want_to_roundup;
					$do_u_want_to_include_breakupweight=$resObj->do_u_want_to_include_breakupweight;
					if($do_u_want_to_roundup=="yes" && $do_u_want_to_include_breakupweight=="yes"){
						$result_value_of_weights=(ceil($required_volume_in_kg-$logistics_weight_latest_price_before_breakpoint_max))/$additional_weight_value_in_kg;
						$logistics_price=$logistics_price_latest_price_before_breakpoint+(($result_value_of_weights)*($resObj->logistics_price));
					}
					if($do_u_want_to_roundup=="yes" && $do_u_want_to_include_breakupweight=="no"){
						$logistics_price=(ceil($required_volume_in_kg)/$additional_weight_value_in_kg)*$resObj->logistics_price;
					}
					if($do_u_want_to_roundup=="no"){
						$logistics_price_part_1=0;
						$logistics_price_part_2=0;
						
						$decimalpart_of_required_volume_in_kg=floor($required_volume_in_kg);
						$fractionalpart_of_required_volume_in_kg=$required_volume_in_kg-$decimalpart_of_required_volume_in_kg;
						$logistics_price_part_1=($decimalpart_of_required_volume_in_kg/$additional_weight_value_in_kg)*$resObj->logistics_price;
						
						$sql_fractional_part="SELECT logistics_price,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$fractionalpart_of_required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$fractionalpart_of_required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg!='yes'";
						$query_fractional_part=$this->db->query($sql_fractional_part);
						if(!empty($query_fractional_part->result())){
							foreach($query_fractional_part->result() as $resObj_fractional_part){
								$logistics_price_part_2=$resObj_fractional_part->logistics_price;
								break;
							}
						}
						$logistics_price=$logistics_price_part_1+$logistics_price_part_2;
						
					}
					break;
				}
				//////////////////// Tax caculation starts /////
				$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
				$logistics_gst=$get_logistics_by_logistics_id_obj->logistics_gst;
				$logistics_fuelsurcharge=$get_logistics_by_logistics_id_obj->logistics_fuelsurcharge;
				$logistics_tax=$logistics_gst+$logistics_fuelsurcharge;
				$logistics_price_with_tax=$logistics_price+($logistics_price*($logistics_tax/100));
				//////////////////// Tax caculation ends /////

				
				return array("resultant_inventory_volume_in_kg"=>$required_volume_in_kg,"logistics_price_with_tax"=>round($logistics_price_with_tax));
			}
			//////////////////// Tax caculation starts /////
			$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
			$logistics_gst=$get_logistics_by_logistics_id_obj->logistics_gst;
			$logistics_fuelsurcharge=$get_logistics_by_logistics_id_obj->logistics_fuelsurcharge;
			$logistics_tax=$logistics_gst+$logistics_fuelsurcharge;
			$logistics_price_with_tax=$logistics_price+($logistics_price*($logistics_tax/100));
			//////////////////// Tax caculation ends /////
			

			// new code - nanthini  
			
			$weight=$inventory_weight_in_kg;
			$weight=0.75;
			$sql="SELECT logistics_price,
			REPLACE(weight_in_kg, '-', ',') as dashRep
			FROM logistics_weight
			WHERE FIND_IN_SET($weight, REPLACE(weight_in_kg, '-', ',')) and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id'";

			$query=$this->db->query($sql);

			$res=$query->row();

			//print_r($res);

			// new code - nanthini  

			return array("resultant_inventory_volume_in_kg"=>$required_volume_in_kg,"logistics_price_with_tax"=>round($logistics_price_with_tax));*/
			
		}
		
		
		public function get_default_logistics_id_for_all_vendors($vendor_id){
			$sql="select logistics_id from logistics where vendor_id='$vendor_id' and default_logistics='1' order by logistics_priority";
			$result=$this->db->query($sql);
			return $result->result();
		}
		public function check_availability_pincode_for_storing_in_localstorage($logistics_id_arr,$pincode){
			$pin=substr($pincode,0,3);
			$check_logistics_yes=0;
			$territory_name_id=0;
			
			foreach($logistics_id_arr as $logistics_id){
				$sql="select * from logistics_service where logistics_id ='$logistics_id' and pin='$pin' and pincode like '%$pincode%'";
				
				$result=$this->db->query($sql);
				
				if(empty($result->row())){
					continue;
				}
				else{
					$territory_name_id=$result->row()->territory_name_id;
					$check_logistics_yes=true;
					break;
				}
			}
			if($check_logistics_yes==0){
				$logistics_id=0;
			}
			return array("check_logistics_yes"=>$check_logistics_yes,"logistics_id"=>$logistics_id,"territory_name_id"=>$territory_name_id);
		}
		public function validate_pincode($pincode){
            $pin=substr($pincode,0,3);
            $sql="select * from  logistics_all_pincodes where pin='$pin' and pincodes like '%$pincode%'";
            $result=$this->db->query($sql);
            $pincode_valid=($result->num_rows() > 0) ? "valid" : "not_valid" ;
            return $pincode_valid;
        }

		public function check_availability_pincode($logistics_id_arr,$pincode,$vendor_id){
			$pin=substr($pincode,0,3);
			$check_logistics_yes=0;
			$territory_name_id=0;

			/*added new*/
            $sql="select * from  logistics_all_pincodes where pin='$pin' and pincodes like '%$pincode%'";
            $result=$this->db->query($sql);
            $pincode_valid=($result->num_rows() > 0) ? "valid" : 'not_valid' ;
			/*added new*/

			foreach($logistics_id_arr as $logistics_id){
				$sql="select * from logistics_service where logistics_id ='$logistics_id' and pin='$pin' and pincode like '%$pincode%'";
				
				$result=$this->db->query($sql);
				
				if(empty($result->row())){
					continue;
				}
				else{
					$territory_name_id=$result->row()->territory_name_id;
					$check_logistics_yes=true;
					break;
				}
			}
			if($check_logistics_yes==0){
				$logistics_id=0;
			}
			
			
			/** getting info of pincode like officename and deliverytype etc starts */
			$imp_json_info_arr=$this->get_imp_json_info_of_pincode($pincode);
			$office_name=$imp_json_info_arr["office_name"];
			$deliverytype=$imp_json_info_arr["deliverytype"];
			/** getting info of pincode like officename and deliverytype etc ends */
			
			
			
			
			echo json_encode(array("check_logistics_yes"=>$check_logistics_yes,"logistics_id"=>$logistics_id,"territory_name_id"=>$territory_name_id,"vendor_id"=>$vendor_id,"pincode_valid"=>$pincode_valid,"office_name"=>$office_name,"deliverytype"=>$deliverytype));
		}
		public function get_imp_json_info_of_pincode($pincode){
			$imp_info_json_name_by_pincode_json=$this->get_imp_info_json_name_by_pincode($pincode);
			$office_name="";
			$deliverytype="";
			if($imp_info_json_name_by_pincode_json=="None"){
				$office_name="";
				$deliverytype="";
			}
			else{
				$imp_info_json_name_by_pincode_arr=json_decode($imp_info_json_name_by_pincode_json,true);
				foreach($imp_info_json_name_by_pincode_arr as $arr){
					if($arr["Pincode"]==$pincode){
						$office_name=$arr["Office_Name"];
						$deliverytype=$arr["DeliveryType"];
						break;
					}
				}
			}
			return array("office_name"=>$office_name,"deliverytype"=>$deliverytype);
		}
		public function get_logistics_id_territory_name_id_by_pincode_vendor_id($logistics_id_arr,$pincode,$vendor_id){
			$pin=substr($pincode,0,3);
			$check_logistics_yes=false;
			$territory_name_id=0;
			
			foreach($logistics_id_arr as $logistics_id){
			$sql="select * from logistics_service where logistics_id ='$logistics_id' and pin='$pin' and pincode like '%$pincode%'";
				
				$result=$this->db->query($sql);
				
				if(empty($result->row())){
					continue;
				}
				else{
					$territory_name_id=$result->row()->territory_name_id;
					$check_logistics_yes=true;
					break;
				}
			}
			if($check_logistics_yes==false){
				$logistics_id=0;
			}
			
			return array("check_logistics_yes"=>$check_logistics_yes,"logistics_id"=>$logistics_id,"territory_name_id"=>$territory_name_id,"vendor_id"=>$vendor_id);
		}
		public function check_availability_pincode_foreach_inventory($pincode,$logistics_id){
			
			$pin=substr($pincode,0,3);
			$sql="select count(*) as count from logistics_service where logistics_id=$logistics_id and pin='$pin' and pincode like '%$pincode%'";
			
			$result=$this->db->query($sql);
			
			if($result->row()->count==0){
				return false;
			}
			else{
				return true;
			}
		}
		public function get_territory_name_by_territory_id($logistics_territory_id){
			$sql="select territory_name from logistics_all_territory_name where territory_name_id='$logistics_territory_id'";
			$result=$this->db->query($sql);
			
			$count=count($result->result());
			if($count>0){
				return $result->row()->territory_name;
			}else{
				return '';
			}
		}
		public function get_free_shipping_territories_by_inventory_id($inventory_id){
			
			$sql_in="select free_shipping_territories from logistics_inventory where inventory_id='$inventory_id'";
			
			$res=$this->db->query($sql_in);
			
			if($res->num_rows()>0){
				$free_shipping_territories=$res->row()->free_shipping_territories;
				
				if($free_shipping_territories!=0 && $free_shipping_territories!=''){
					$sql="select territory_name from logistics_all_territory_name where territory_name_id in ('$free_shipping_territories')";
					$result=$this->db->query($sql);
					return $result->result_array();
				}else{
					return false;
				}
			}else{
				return false;
				
			}
		}
		public function get_order_item_id($inventory_id,$customer_id){
			
			$sql_select="select orders_status.order_delivered,orders_status.order_delivered_date,orders_status.order_delivered_time,orders_status.order_item_id,history_orders.customer_id,history_orders.inventory_id from orders_status,history_orders where history_orders.inventory_id='$inventory_id' and history_orders.customer_id='$customer_id' and history_orders.order_item_id=orders_status.order_item_id and orders_status.order_delivered=1 order by orders_status.id desc limit 1";
			
			$result=$this->db->query($sql_select);
			return $result->row_array();
		}
		public function get_data_from_rated_inventory($inventory_id,$customer_id){
			
			$sql_select="select review_title,review,rating from rated_inventory where customer_id='$customer_id' and inventory_id='$inventory_id'";
			
			$result=$this->db->query($sql_select);
			return $result->row_array();
		}
		public function dump_search_keywords($search_keyword){
            $search_keyword=addslashes($search_keyword);
            $sql_select="select count(*) as count from searched_keyword where LOWER(searched_keyword)='$search_keyword'";
            $query=$this->db->query($sql_select);
            $count=($query->row()->count);
            
            if($count>0){
                $sql="update searched_keyword set number_of_hits=number_of_hits+1 where  LOWER(searched_keyword)='$search_keyword'";
            }else{      
                 $sql="insert into searched_keyword (searched_keyword,number_of_hits) VALUES ('$search_keyword',1)";
            }
            
            $result=$this->db->query($sql);
            return $result;
        }
		public function add_mail_id_to_notify($inventory_id,$email,$sku_id,$customer_id){
			
			$sql_count="select count(*) as count from request_for_out_of_stock where (email='$email' or mobile='$email') and sku_id='$sku_id'";
			$query=$this->db->query($sql_count);
			$exist_count=$query->row()->count;
			$email_input="";
			$mobile_input="";
			if(!preg_match('/^[0-9]{10}+$/', $email))  {  
			$email_input=$email;
			}
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$mobile_input=$email;
			}
			if($exist_count==0){
				$sql="insert into request_for_out_of_stock (inventory_id,email,mobile,sku_id,customer_id) VALUES ('$inventory_id','$email_input','$mobile_input','$sku_id','$customer_id')";
				$result=$this->db->query($sql);
				return $result;
			}else{
				return "exists";
			}
			 
		}
		public function get_inventory_info_by_inventory_id($inventory_id){
			$sql="select * from inventory where id='$inventory_id'";
			$result=$this->db->query($sql);
			return $result->row_array();
		}
		public function get_product_info_by_inventory_id($inventory_id){
			$sql="select products.* from products,inventory where inventory.product_id=products.product_id and inventory.id='$inventory_id'";
			$result=$this->db->query($sql);
			return $result->row_array();
		}
		
		public function add_to_compare_inventories($inventory_id_list_for_compare_arr){
			$subcat_id_arr=[];
			foreach($inventory_id_list_for_compare_arr as $inventory_id){
				$sql="select subcat_id from products where product_id=(select product_id from inventory where id='$inventory_id')";
				$result=$this->db->query($sql);
				$subcat_id_arr[]=$result->row_array()["subcat_id"];
			}
			if(count(array_unique($subcat_id_arr))==1){
				return "similar";
			}
			else{
				return "dissimilar";
			}
		}
		public function get_attribute_id($product_id){
			
			$sql="select attribute.attribute_id from attribute,products where products.subcat_id=attribute.subcat_id and products.product_id='$product_id' and attribute.active=1";
			$query=$this->db->query($sql);
			if(isset($query->row()->attribute_id)){
				return $query->row()->attribute_id;
			}else{
				return '';
			}
            
		}

		public function get_attribute_expansion_info($att_k,$attribute_id){
			$sql="select * from attribute_expansions where attribute_id='$attribute_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function attribute_expansions_sub_options($attribute_expansions_id){
			$sql="select * from attribute_expansions_sub_options where attribute_expansions_id='$attribute_expansions_id'";
			$query = $this->db->query($sql);
			return $query->result();
		}
		public function inventory_comparision_yes_or_no($inventory_id){
			$sql="select subcategory.subcat_comparision from subcategory,products,inventory where subcategory.subcat_id=products.subcat_id and products.product_id=inventory.product_id and inventory.id='$inventory_id'";
			$result=$this->db->query($sql);
			return $result->row_array()["subcat_comparision"];
		}
		public function get_count_of_invs($subcat_id){
			$sql="select inventory.id from subcategory,products,inventory where subcategory.subcat_id=products.subcat_id and products.product_id=inventory.product_id and products.subcat_id='$subcat_id'";
			$result=$this->db->query($sql);
			return $result->result();
		}
		
		public function get_all_states(){
			$sql="select * from logistics_all_states order by state_id desc";
			$result=$this->db->query($sql);
			return $result->result_array();
		}
		
		public function get_mobile_db($cust_id){
			$sql="select mobile from customer where  id='$cust_id'";
			$query =$this->db->query($sql);
			$result = $query->row_array();
			$mobile = $result['mobile'];
			return $mobile;
		}
		
		public function get_email_db($cust_id){
			$sql="select email from customer where  id='$cust_id'";
			$query =$this->db->query($sql);
			$result = $query->row_array();
			$email = $result['email'];
			return $email;
		}
		
		 function enquiries_server($connect_name,$connect_mobile,$connect_email){
		 
			$sql="insert into enquiries set name='$connect_name',mobile='$connect_mobile',email='$connect_email'";
			$this->db->query($sql);
			
			 
		}
		
		function get_shipping_module_info(){
			$sql="select * from shipping_module";
			$query =$this->db->query($sql);
			return $query->row();
		}
		
		public function get_order_placed_date_customer_id($id){
			$sql="select customer_id,timestamp from active_orders where order_id='$id' union select customer_id,timestamp from cancelled_orders where order_id='$id' union select customer_id,timestamp from completed_orders where order_id='$id' union select customer_id,timestamp from returned_orders where order_id='$id'";
			$result=$this->db->query($sql);
			return array("order_placed_date"=>$result->row()->timestamp,"customer_id"=>$result->row()->customer_id);
		}
		
		public function get_customer_name($customer_id){
			$sql="select name from customer where id='$customer_id'";
			$query=$this->db->query($sql);
			return $query->row()->name;
		}
		
		//////////// url changes in detail and searchcategory pages starts//////////
		
		public function get_parentcategory_name($pcat_id){
            
            $sql="select pcat_name from parent_category where pcat_id='$pcat_id'";
            $query=$this->db->query($sql);
            return $query->row()->pcat_name;
        }
		public function get_category_name($cat_id){
            
            $sql="select cat_name from category where cat_id='$cat_id'";
            $query=$this->db->query($sql);
            return $query->row()->cat_name;
        }
		public function get_subcategory_name($subcat_id){
            
            $sql="select subcat_name from subcategory where  subcat_id='$subcat_id'";
            $query=$this->db->query($sql);
            return $query->row()->subcat_name;
        }
		public function get_brand_name($brand_id){
            
            $sql="select brand_name from brands where  brand_id='$brand_id'";
            $query=$this->db->query($sql);
            return $query->row()->brand_name;
        }
		public function get_sku_id_product_id($inventory_id){
            
            $sql="select sku_id,product_id from inventory where  id='$inventory_id'";
            $query=$this->db->query($sql);
            return $query->row();
        }
		public function get_product_stuff_by_product_id_seo($product_id){
            
            $sql="select product_name,pcat_id,cat_id,subcat_id,brand_id from products where  product_id='$product_id'";
            $query=$this->db->query($sql);
            return $query->row();
        }
		
		public function get_pcat_id_by_parentcategory_name($parentcategory_name){
			$sql="select pcat_id from parent_category where lower(pcat_name)=lower('$parentcategory_name')";
            $query=$this->db->query($sql);
            return ($query->num_rows()>0) ? $query->row()->pcat_id : '';
		}
		public function get_cat_id_by_category_name($category_name,$parentcategory_name){
			$pcat_id=$this->get_pcat_id_by_parentcategory_name($parentcategory_name);
			$sql="select cat_id from category where lower(cat_name)=lower('$category_name')";
			if($pcat_id==0 || $pcat_id==""){
				
			}
			else{
				$sql.=" and pcat_id='$pcat_id'";
			}
			
            $query=$this->db->query($sql);
            return $query->row()->cat_id;
		}
		public function get_subcat_id_by_subcategory_name($subcategory_name,$cat_id=''){
		    if($cat_id!=''){
                $sql="select subcat_id from subcategory where lower(subcat_name)=lower('$subcategory_name') and cat_id='$cat_id'";
            }else{
                $sql="select subcat_id from subcategory where lower(subcat_name)=lower('$subcategory_name')";
            }
            $query=$this->db->query($sql);
            return $query->row()->subcat_id;
		}
		public function get_inventory_id_by_sku_id_seo($sku_id){
			$sql="select id from inventory where lower(sku_id)=lower('$sku_id')";
            $query=$this->db->query($sql);
            return $query->row()->id;
		}
		public function get_inventory_id_by_sku_id($sku_id){
			$sql="select id from inventory where lower(sku_id)=lower('$sku_id')";
            $query=$this->db->query($sql);
			if(!empty($query->row())){
				return $query->row()->id;
			}
			else{
				return "";
			}
		}
		
		//////////// url changes in detail and searchcategory pages starts//////////
		public function get_emi_options(){
			$sql="select * from emioptions order by emioptions_id desc";
			$query=$this->db->query($sql);
            return $query->result_array();
		}
		public function get_minimum_emistartvalue(){
			$sql="select min(emistartvalue) as min_emistartvalue from emioptions";
			$query=$this->db->query($sql);
			if(!empty($query->row_array())){
				return $query->row_array()["min_emistartvalue"];
			}
			else{
				return 0;
			}
		}
		public function get_skus_under_chain($chain_id,$chain_name){
			if($chain_name=="pcat"){
				$cat_id_arr=[];
				$sql_cat="select cat_id from category where pcat_id='$chain_id' and view='1' and active='1'";
				$query_cat=$this->db->query($sql_cat);
				$result_cat_arr=$query_cat->result_array();
				foreach($result_cat_arr as $arr){
					$cat_id_arr[]=$arr["cat_id"];
				}
				$cat_id_arr=array_unique($cat_id_arr);
				$cat_id_in=implode(",",$cat_id_arr);
				if($cat_id_in==""){
					$cat_id_in="'testfield'";
				}
				$sql_subcat="select subcat_id from subcategory where pcat_id='$chain_id' and view='1' and active='1' and cat_id in ($cat_id_in)";
				$query_subcat=$this->db->query($sql_subcat);
				$result_subcat_arr=$query_subcat->result_array();
				$subcat_id_arr=[];
				foreach($result_subcat_arr as $arr){
					$subcat_id_arr[]=$arr["subcat_id"];
				}
				$subcat_id_arr=array_unique($subcat_id_arr);
				$subcat_id_in=implode(",",$subcat_id_arr);
				if($subcat_id_in==""){
					$subcat_id_in="'testfield'";
				}
				$sql_brand="select brand_id from brands where pcat_id='$chain_id' and view='1' and active='1' and subcat_id in ($subcat_id_in)";
				$query_brand=$this->db->query($sql_brand);
				$result_brand_arr=$query_brand->result_array();
				$brand_id_arr=[];
				foreach($result_brand_arr as $arr){
					$brand_id_arr[]=$arr["brand_id"];
				}
				$brand_id_arr=array_unique($brand_id_arr);
				$brand_id_in=implode(",",$brand_id_arr);
				if($brand_id_in==""){
					$brand_id_in="'testfield'";
				}
				$sql_product="select product_id from products where pcat_id='$chain_id' and view='1' and active='1' and brand_id in ($brand_id_in)";
				$query_product=$this->db->query($sql_product);
				$result_product_arr=$query_product->result_array();
				$product_id_arr=[];
				foreach($result_product_arr as $arr){
					$product_id_arr[]=$arr["product_id"];
				}
				$product_id_arr=array_unique($product_id_arr);
				$product_id_in=implode(",",$product_id_arr);
				if($product_id_in==""){
					$product_id_in="'testfield'";
				}
				$sql="select count(id) as count_number_of_skus from inventory where inventory_type!=2 and  product_id in ($product_id_in) and active=1";
				
				
				
			}
			if($chain_name=="cat"){
				$sql_subcat="select subcat_id from subcategory where cat_id='$chain_id' and view='1' and active='1'";
				$query_subcat=$this->db->query($sql_subcat);
				$result_subcat_arr=$query_subcat->result_array();
				$subcat_id_arr=[];
				foreach($result_subcat_arr as $arr){
					$subcat_id_arr[]=$arr["subcat_id"];
				}
				$subcat_id_arr=array_unique($subcat_id_arr);
				$subcat_id_in=implode(",",$subcat_id_arr);
				if($subcat_id_in==""){
					$subcat_id_in="'test'";
				}
				$sql_brand="select brand_id from brands where cat_id='$chain_id' and view='1' and active='1' and subcat_id in ($subcat_id_in)";
				$query_brand=$this->db->query($sql_brand);
				$result_brand_arr=$query_brand->result_array();
				$brand_id_arr=[];
				foreach($result_brand_arr as $arr){
					$brand_id_arr[]=$arr["brand_id"];
				}
				$brand_id_arr=array_unique($brand_id_arr);
				$brand_id_in=implode(",",$brand_id_arr);
				if($brand_id_in==""){
					$brand_id_in="'testfield'";
				}
				$sql_product="select product_id from products where cat_id='$chain_id' and view='1' and active='1' and brand_id in ($brand_id_in)";
				$query_product=$this->db->query($sql_product);
				$result_product_arr=$query_product->result_array();
				$product_id_arr=[];
				foreach($result_product_arr as $arr){
					$product_id_arr[]=$arr["product_id"];
				}
				$product_id_arr=array_unique($product_id_arr);
				$product_id_in=implode(",",$product_id_arr);
				if($product_id_in==""){
					$product_id_in="'testfield'";
				}
				$sql="select count(id) as count_number_of_skus from inventory where inventory_type!=2 and  product_id in ($product_id_in) and active=1";
			}
			if($chain_name=="subcat"){
				
				$sql_brand="select brand_id from brands where subcat_id='$chain_id' and view='1' and active='1'";
				$query_brand=$this->db->query($sql_brand);
				$result_brand_arr=$query_brand->result_array();
				$brand_id_arr=[];
				foreach($result_brand_arr as $arr){
					$brand_id_arr[]=$arr["brand_id"];
				}
				$brand_id_arr=array_unique($brand_id_arr);
				$brand_id_in=implode(",",$brand_id_arr);
				if($brand_id_in==""){
					$brand_id_in="'testfield'";
				}
				if(!empty($brand_id_arr)){
					$sql_product="select product_id from products where subcat_id='$chain_id' and view='1' and active='1' and brand_id in ($brand_id_in)";
					$query_product=$this->db->query($sql_product);
					$result_product_arr=$query_product->result_array();
					$product_id_arr=[];
					foreach($result_product_arr as $arr){
						$product_id_arr[]=$arr["product_id"];
					}
					$product_id_arr=array_unique($product_id_arr);
					$product_id_in=implode(",",$product_id_arr);
					if($product_id_in==""){
					$product_id_in="'testfield'";
				}
					$sql="select count(id) as count_number_of_skus from inventory where inventory_type!=2 and  product_id in ($product_id_in) and active=1";
				}
				else{
					$sql="select count(id) as count_number_of_skus from inventory where inventory_type!=2 and 1=2";
				}
				
				
				
				
			}
			if($chain_name=="brand"){
				
				$sql_product="select product_id from products where brand_id='$chain_id' and view='1' and active='1'";
				$query_product=$this->db->query($sql_product);
				$result_product_arr=$query_product->result_array();
				$product_id_arr=[];
				foreach($result_product_arr as $arr){
					$product_id_arr[]=$arr["product_id"];
				}
				$product_id_arr=array_unique($product_id_arr);
				$product_id_in=implode(",",$product_id_arr);
				if($product_id_in==""){
					$product_id_in="'testfield'";
				}
				$sql="select count(id) as count_number_of_skus from inventory where inventory_type!=2 and  product_id in ($product_id_in) and active=1";
				
			}
			if($chain_name=="product"){
				$sql="select count(id) as count_number_of_skus from inventory where inventory_type!=2 and  product_id='$chain_id' and active=1";
			}
			$query=$this->db->query($sql);
			return $query->row_array()["count_number_of_skus"];
		}
                public function save_contact($name,$email,$subject,$mobile,$message){

            $name=$this->db->escape_str($name);
            $email=$this->db->escape_str($email);
            $subject=$this->db->escape_str($subject);
            $mobile=$this->db->escape_str($mobile);
            $message=$this->db->escape_str($message);
            $sql="insert into contactus set name='$name',mobile='$mobile',email='$email',message='$message',subject='$subject'";
            return $this->db->query($sql);
        }
		
		
        public function get_product_count_info($dup_pcat_id,$dup_cat_id,$dup_subcat_id){
                $query=$this->db->select("*")->from("products")->where(array("pcat_id"=>$dup_pcat_id,"cat_id"=>$dup_cat_id,"subcat_id"=>$dup_subcat_id,"view"=>"1","active"=>"1"))->get();
                return $query->result_array();
        }
        public function get_active_products($product_id,$inventory_id){
            $sql="select id from inventory 
                     LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`
                     where inventory.id='$inventory_id' and inventory.product_id='$product_id' and products.active=1 and inventory.active=1";
            //echo $sql;
            $query=$this->db->query($sql);

            if($query->num_rows()>0){
                return $query->row()->id;    
            }else{
                return '';
            }
        }
		
		
		
		
		function webinar_registration($webinar_title,$name,$postal_code,$email,$mobile,$webinar_registration_unique_id){
			$name=addslashes($name);
			$postal_code=addslashes($postal_code);
			$email=addslashes($email);
			$mobile=addslashes($mobile);
			
			
			$sql="insert into webinar_registrations set 
			                webinar_title='$webinar_title',
							name='$name',
							postal_code='$postal_code',
							email='$email',
							mobile='$mobile',
							webinar_registration_unique_id='$webinar_registration_unique_id'
							";
			$flag=$this->db->query($sql);
			if($flag==true){
				return array("status"=>$flag,"webinar_registration_unique_id"=>$webinar_registration_unique_id);
			}
			else{
				return array("status"=>$flag,"webinar_registration_unique_id"=>"1199");
			}
			 
		}
		
		
		function get_registered_name_by_webinar_registration_unique_id($webinar_registration_unique_id){
			$sql="select name from webinar_registrations where webinar_registration_unique_id='$webinar_registration_unique_id'";
			$result=$this->db->query($sql);
			$row_arr=$result->row_array();
			if(!empty($row_arr)){
				$name=$row_arr["name"];
				return $name;
			}
			else{
				return "";
			}
		}
		
		function get_partner_name_by_partner_unique_id($partner_unique_id){
			$sql="select name from partner_with_us where partner_unique_id='$partner_unique_id'";
			$result=$this->db->query($sql);
			$row_arr=$result->row_array();
			if(!empty($row_arr)){
				$name=$row_arr["name"];
				return $name;
			}
			else{
				return "";
			}
		}
		
		
		
	 
	
	public function update_carttable_combo_dump($insert_arr){
		$customer_id=$this->session->userdata("customer_id");
		$this->db->where(array("combo_customer_id"=>$customer_id));
		$query=$this->db->update("carttable_combo",$insert_arr);	
		if($query==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	
	
	
	
	
	public function get_expertise_area_details_from_master_db($expertise_area_unique_id){
		$sql="select * from expertise_area_db where expertise_area_unique_id='$expertise_area_unique_id'";
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	public function get_expertise_area_list_from_master_db(){
		$sql="select expertise_area_unique_id,expertise_area_title from expertise_area_db";
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	
	public function show_available_active_categories($pcat_id){
		$sql="select cat_id,cat_name from category where trim(pcat_id) = '$pcat_id' and active=1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_active_subcategories($cat_id){
		$sql="select subcat_id,subcat_name from subcategory where trim(cat_id) = trim('$cat_id') and active=1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_active_brands($subcat_id){
		$sql="select brand_id,brand_name from brands where trim(subcat_id) = '$subcat_id'  and active=1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_active_products($brand_id){
		$sql="select product_id,product_name from  products where trim(brand_id) = '$brand_id' and active=1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_active_skus($product_id){
		$sql="select id,sku_id from  inventory where trim(product_id) = '$product_id' and active=1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function get_all_parent_categories(){
		$sql="select pcat_id,pcat_name from  parent_category where view = 1 and active=1 order by pcat_id";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	public function get_categories_by_pcat_id($pcat_id){
		$sql="select cat_id,cat_name from category where pcat_id='$pcat_id' and view = 1 and active=1";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	public function get_sub_categories_by_cat_id($cat_id){
		$sql="select subcat_id,subcat_name from subcategory where cat_id='$cat_id' and view = 1 and active=1";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_inventories_of_catalog_tree_view_by_subcat_id_fun($result_catalog_tree_view_subcat_id_arr){
		$result_catalog_tree_view_subcat_id_in=implode(",",$result_catalog_tree_view_subcat_id_arr);
		$sql="select id from inventory where product_id in (select product_id from products where subcat_id in ($result_catalog_tree_view_subcat_id_in))";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_all_brand_names(){
		$product_id_arr=[];
		$brand_id_arr=[];
		$sql="select product_id from inventory where inventory_type!=2 and active=1";
		$query=$this->db->query($sql);
		$product_id_result_array=$query->result_array();
		foreach($product_id_result_array as $arr){
			$product_id_arr[]=$arr["product_id"];
		}
		$product_id_in=implode(",",$product_id_arr);
		if($product_id_in!=""){
			$sql="select brand_id from products where active=1 and view=1 and product_id in ($product_id_in)";
		}
		else{
			$sql="select brand_id from products where active=1 and view=1 and 1=2";
		}
		$query=$this->db->query($sql);
		$brand_id_result_array=$query->result_array();
		foreach($brand_id_result_array as $arr){
			$brand_id_arr[]=$arr["brand_id"];
		}
		$brand_id_in=implode(",",$brand_id_arr);
		if($brand_id_in!=""){
			$sql="select distinct brand_name from brands where active='1' and view=1 and brand_id in ($brand_id_in)";
		}
		else{
			$sql="select distinct brand_name from brands where active='1' and view=1 and 1=2";
		}
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	public function is_offer_discount_exists($inventory_id){
		$sql="select count(*) as count from tagged_inventory where tagged_main_inventory_id='$inventory_id'";
		$query=$this->db->query($sql);
		return $query->row_array()["count"];
	}
	public function get_total_count_skus_indexed_in_elasticsearch(){
		$sql="select inventory.id as inventory_id,inventory.sku_id,inventory.sku_name,products.product_id,products.product_name,products.product_description,products.pcat_id,products.cat_id,products.subcat_id,products.brand_id,inventory.image,inventory.thumbnail,inventory.tax_percent,inventory.base_price,inventory.selling_price,inventory.max_selling_price,inventory.selling_discount,inventory.common_image,inventory.inventory_type from products,inventory where products.product_id=inventory.product_id and products.view=1 and products.active=1 and inventory.active=1";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function get_all_tagged_inventories_for_combo($tagged_main_inventory_id_list_in){
		
		$sql="select * from tagged_inventory_combo where tagged_main_inventory_id in ($tagged_main_inventory_id_list_in)";
		$query=$this->db->query($sql);
		return $query->result_array();
		
	}
	public function get_inventory_details_list($tagged_inventory_id_list){
		$tagged_inventory_id_list_in=implode(",",$tagged_inventory_id_list);
		$sql="select * from inventory where id in ($tagged_inventory_id_list_in)";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	public function get_pcat_cat_subcat_id_by_sku_id($sku_id){
		$sql="select products.pcat_id,products.cat_id,products.subcat_id,inventory.sku_id from products,inventory where products.product_id=inventory.product_id and lower(inventory.sku_id)=lower('$sku_id') and products.view=1 and products.active=1 and inventory.active=1";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	public function get_inventory_id_list_by_sku_id_arr($skuid_list_arr){
		$skuid_list_in="'".implode("','",$skuid_list_arr)."'";
		$sql="select id as inventory_id from inventory where sku_id in ($skuid_list_in) and inventory.active=1";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	}
?>