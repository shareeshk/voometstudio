<?php
class Customer_blogs extends CI_model{
    public function get_blogs_of_tag($tag){
        $sql="select tags_id from  blog_tag where tags='$tag'";
        $result=$this->db->query($sql);
        $tag_id=$result->row()->tags_id;
        
       $sql="select blog.*,date_format(blog.timestamp,'%a,%e %b %Y') as timestamp,admin_country.name from blog join admin_country on admin_country.user_id=blog.customer_id where  tag_sel LIKE '%$tag_id%'  and admin_publish=1 order by timestamp desc";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }else{
            return array();
        } 
    }
    public function get_blogs_of_topic($topic){
        $sql="select topic_id from  blog_topic where topic_sel='$topic'";
        $result=$this->db->query($sql);
        $topic_id=$result->row()->topic_id;
		
        $sql="select blog.*,date_format(blog.timestamp,'%a,%e %b %Y') as timestamp,admin_country.name from blog join admin_country on admin_country.user_id=blog.customer_id where  topic_sel LIKE '%$topic_id%' and admin_publish=1 order by timestamp desc";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }else{
            return array();
        }
    }
    public function all_blogs(){
        $sql="select blog.*,date_format(blog.timestamp,'%a,%e %b %Y') as timestamp,admin_country.name from blog join admin_country on admin_country.user_id=blog.customer_id where admin_publish=1 order by timestamp desc";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }else{
            return array();
        }
    }
    public function get_popular_blogs(){
        $sql="select *,date_format(blog.timestamp,'%a,%e %b %Y') as timestamp from blog where count_likes>0 order by count_likes desc limit 5 ";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }else{
            return array();
        }
    }
    
    public function get_blog_topics(){
        $sql="select * from blog_topic";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }else{
            return array();
        }
    }
    public function get_blog_tags(){
        $sql="select * from blog_tag";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }else{
            return array();
        }
    }
    
    public function blog_like_feed_back($customer_id,$blog_id){
        $s='","';
        $sql="update blog set likes =CONCAT(likes, '$s','$customer_id'),count_likes = count_likes + 1 where blog_id='$blog_id'";
        $result=$this->db->query($sql);
        if($this->db->affected_rows()>0){
            return true;
        }else{
            return false;
        }
    }
    public function check_slugg($blog_title){
        $slug = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($blog_title)));
        $sql="select COUNT(*) AS NumHits  from blog where blog_title  LIKE '%$slug%'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            $numHits=$result->row()->NumHits;
            return ($numHits > 0) ? ($slug . '-' . $numHits) : $slug;
        }else{
            return $slug;
        }
    }
    
	public function add_blog($topic_sel,$tag_sel,$blog_title,$blog_id,$customer_id){
	    $slug_field=$this->check_slugg($blog_title);
	    $sql="insert into blog set slug='$slug_field', topic_sel=".$this->db->escape($topic_sel).", tag_sel='$tag_sel',blog_title=".$this->db->escape($blog_title).",blog_id='$blog_id',customer_id='$customer_id'";
        $result=$this->db->query($sql);
        if($this->db->affected_rows()>0){
            return true;
        }else{
            return false;
        }
    }
    public function get_blog_post($id){
        $sql="select blog.*,date_format(blog.timestamp,'%a,%e %b %Y') as timestamp,admin_country.name from blog join admin_country on admin_country.user_id=blog.customer_id where slug='$id' and blog.admin_publish='1'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->row_array();
        }else{
            return array();
        }
    }
    public function get_blog_post_preview($id,$customer_id){
        $sql="select blog.*,date_format(blog.timestamp,'%a,%e %b %Y') as timestamp,admin_country.name from blog join admin_country on admin_country.user_id=blog.customer_id where slug='$id' and blog.customer_id='$customer_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->row_array();
        }else{
            return array();
        }
    }
    
    public function add_blogs_comments($customer_image,$customer_id,$blog_id,$customer_name,$message){
        $sql="insert into blogs_comment set customer_image='$customer_image',customer_id='$customer_id',blog_id='$blog_id',customer_name='$customer_name',message=".$this->db->escape($message);
        $result=$this->db->query($sql);
        if($this->db->affected_rows()>0){
            return true;
        }else{
            return false;
        }
    }
    public function get_blog_post_comments_live($blog_id){
        $sql="select *,date_format(blogs_comment.timestamp,'%a,%e %b %Y') as timestamp from blogs_comment where blog_id='$blog_id' and active=1"; 
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }else{
            return array();
        }
    }
    
    public function get_all_blog_data($blog_id,$customer_id){
        $sql="select * from blog where blog_id='$blog_id' and customer_id='$customer_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->row_array();
        }else{
            return array();
        }
    }
   
    public function update_blog_content($blog_content,$blog_id,$customer_id){
        $sql="update blog set blog_content=".$this->db->escape($blog_content)." where blog_id='$blog_id' and customer_id='$customer_id'";
        $result=$this->db->query($sql);
        if($this->db->affected_rows()>0){
            return 1;
        }else{
            if($this->db->_error_message()){
                return false;
            }else{
                return 2;
            }
            
        }
    }
    public function blogger_request_publish_admin($blogger_request_publish,$blog_id,$customer_id){
        $sql="update blog set blogger_request_publish=".$this->db->escape($blogger_request_publish)." where blog_id='$blog_id' and customer_id='$customer_id'";
        $result=$this->db->query($sql);
        if($this->db->affected_rows()>0){
            return 1;
        }else{
            if($this->db->_error_message()){
                return false;
            }else{
                return 2;
            }
            
        }
    }
    public function blogger_request_unpublish_admin($blogger_request_unpublish,$blog_id,$customer_id){
        $sql="update blog set blogger_request_unpublish=".$this->db->escape($blogger_request_unpublish)." where blog_id='$blog_id' and customer_id='$customer_id'";
        $result=$this->db->query($sql);
        if($this->db->affected_rows()>0){
            return 1;
        }else{
            if($this->db->_error_message()){
                return false;
            }else{
                return 2;
            }
            
        }
    }
    
    public function getAllBlogs_processing($params,$fg){
        //define index of column
        $columns = array( 
            0 =>'blog.id', 
            1 => 'blog.blog_id',
            2 => 'blog.blog_title',
            3 => 'blog.blogger_request_publish',
            4 => 'blog.admin_publish',
            5 => 'blog.blogger_request_unpublish',
            6 => 'blog.topic_sel',
            7 => 'blog.slug',
        );

        $where = $sqlTot = $sqlRec = "";
        $customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT *,date_format(blog.timestamp,'%a,%e %b %Y') as timestamp from `blog` where customer_id='$customer_id'";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" (  date_format(blog.timestamp,'%a,%e %b %Y')  LIKE '%".$params['search']['value']."%' ";
            $where .="  or blog.blog_title LIKE '%".addslashes($params['search']['value'])."%' ";
            $where .="  or blog.topic_sel LIKE '%".addslashes($params['search']['value'])."%' )";
            //$where .="  blog.villa_type LIKE '%".addslashes($params['search']['value'])."%' ";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            //echo $sqlTot; exit;
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
           // echo $sqlRec; exit;
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }
}
?>