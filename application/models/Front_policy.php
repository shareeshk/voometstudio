<?php
class Front_policy extends CI_model{
    
    public function getAllActivePolicyList(){
        $sql="select * from policys_list where active=1";
            $result=$this->db->query($sql);
            if($result->num_rows()>0){
                return $result->result_array();
            }
            else{
                return false;
            }
    }
    
    public function check_policy_exist($id,$name){
        $sql="select * from policy_items where policy_type='$id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function getStoreLevelPaymentPolicy($id){
        $sql="select * from policy_items where policy_type='$id' and pcat_id='' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id=''";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    
    public function getParentCategoryLevelPaymentPolicy($id,$pcat_id){
        $sql="select * from policy_items where policy_type='$id' and pcat_id='$pcat_id' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id=''";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    
    public function getCategoryLevelPaymentPolicy($id,$cat_id){
        $sql="select * from policy_items where policy_type='$id' and pcat_id!='' and cat_id='$cat_id' and subcat_id='' and brand_id='' and product_id='' and inv_id=''";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    
    public function getSubCategoryLevelPaymentPolicy($id,$subcat_id){
        $sql="select * from policy_items where policy_type='$id' and pcat_id!='' and cat_id!='' and subcat_id='$subcat_id' and brand_id='' and product_id='' and inv_id=''";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    
    public function getBrandLevelPaymentPolicy($id,$brand_id){
        $sql="select * from policy_items where policy_type='$id' and pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id='$brand_id' and product_id='' and inv_id=''";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    
    public function getProductLevelPaymentPolicy($id,$product_id){
        $sql="select * from policy_items where policy_type='$id' and pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id='$product_id' and inv_id=''";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    
    public function getInvLevelPaymentPolicy($id,$inventory_id){
        $sql="select * from policy_items where policy_type='$id' and pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id!='' and inv_id='$inventory_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    
    public function store_level_policy($id,$name,$payment_policy_uid){
        if($name=="Payment"){
            $sql="select * from payment_policy join policy_items on payment_policy.payment_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id='' and policy_items.cat_id='' and policy_items.subcat_id='' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id='' ";
        }
        if($name=="Replacement"){
            $sql="select * from replacement_policy join policy_items on replacement_policy.replacement_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id='' and policy_items.cat_id='' and policy_items.subcat_id='' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id='' ";
        }
        if($name=="Refund"){
            $sql="select * from refund_policy join policy_items on refund_policy.refund_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id='' and policy_items.cat_id='' and policy_items.subcat_id='' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id='' ";
        }
        if($name=="Cancellation"){
           $sql="select * from cancellation_policy join policy_items on cancellation_policy.cancellation_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id='' and policy_items.cat_id='' and policy_items.subcat_id='' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id='' ";
        }
        if($name=="Exchange"){
            $sql="select * from exchange_policy join policy_items on exchange_policy.exchange_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id='' and policy_items.cat_id='' and policy_items.subcat_id='' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id='' and now() > exchange_policy.exchange_start_date and  not exchange_policy.exchange_end_date < now()";
        }
		if(isset($sql)){
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->result_array();
			}
		}
        
    }


    public function parent_category_level_policy($id,$name,$payment_policy_uid,$pcat_id){
		//echo $name;
        if($name=="Payment"){
            $sql="select * from payment_policy join policy_items on payment_policy.payment_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id='$pcat_id' and policy_items.cat_id='' and policy_items.subcat_id='' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id=''";
        }
        if($name=="Replacement"){
            $sql="select * from replacement_policy join policy_items on replacement_policy.replacement_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id='$pcat_id' and policy_items.cat_id='' and policy_items.subcat_id='' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id=''";
        }
        if($name=="Refund"){
            $sql="select * from refund_policy join policy_items on refund_policy.refund_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id='$pcat_id' and policy_items.cat_id='' and policy_items.subcat_id='' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id=''";
        }
        if($name=="Cancellation"){
            $sql="select * from cancellation_policy join policy_items on cancellation_policy.cancellation_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id='$pcat_id' and policy_items.cat_id='' and policy_items.subcat_id='' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id=''";
        }
        if($name=="Exchange"){
            $sql="select * from exchange_policy join policy_items on exchange_policy.exchange_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id='$pcat_id' and policy_items.cat_id='' and policy_items.subcat_id='' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id='' and now() > exchange_policy.exchange_start_date and  not exchange_policy.exchange_end_date < now()";
        }
		if($name=="Shipping Charge For Replacement"){
			  $sql="select * from shipping_charge_on_replacement join policy_items on shipping_charge_on_replacement.shipping_charge_on_replacement_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id='$pcat_id' and policy_items.cat_id='' and policy_items.subcat_id='' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id=''";
		}
		if(isset($sql)){
			
			$result=$this->db->query($sql);
			
			if($result->num_rows()>0){
				return $result->result_array();
			}
		}
        
    }
    
    public function category_level_policy($id,$name,$payment_policy_uid,$cat_id){
        if($name=="Payment"){
            $sql="select * from payment_policy join policy_items on payment_policy.payment_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id='$cat_id' and policy_items.subcat_id='' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id=''";
        }
        if($name=="Replacement"){
            $sql="select * from replacement_policy join policy_items on replacement_policy.replacement_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id='$cat_id' and policy_items.subcat_id='' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id=''";
        }
        if($name=="Refund"){
            $sql="select * from refund_policy join policy_items on refund_policy.refund_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id='$cat_id' and policy_items.subcat_id='' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id=''";
        }
        if($name=="Cancellation"){
            $sql="select * from cancellation_policy join policy_items on cancellation_policy.cancellation_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id='$cat_id' and policy_items.subcat_id='' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id=''";
        }
        if($name=="Exchange"){
            $sql="select * from exchange_policy join policy_items on exchange_policy.exchange_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id='$cat_id' and policy_items.subcat_id='' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id='' and now() > exchange_policy.exchange_start_date and  not exchange_policy.exchange_end_date < now()";
        }
		if(isset($sql)){
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->result_array();
			}
		}
        
    }
    
    public function sub_category_level_policy($id,$name,$payment_policy_uid,$subcat_id){
        if($name=="Payment"){
            $sql="select * from payment_policy join policy_items on payment_policy.payment_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id='$subcat_id' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id=''";
        }
        if($name=="Replacement"){
            $sql="select * from replacement_policy join policy_items on replacement_policy.replacement_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id='$subcat_id' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id=''";
        }
        if($name=="Refund"){
            $sql="select * from refund_policy join policy_items on refund_policy.refund_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id='$subcat_id' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id=''";
        }
        if($name=="Cancellation"){
            $sql="select * from cancellation_policy join policy_items on cancellation_policy.cancellation_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id='$subcat_id' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id=''";
        }
        if($name=="Exchange"){
            $sql="select * from exchange_policy join policy_items on exchange_policy.exchange_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id='$subcat_id' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id='' and now() > exchange_policy.exchange_start_date and  not exchange_policy.exchange_end_date < now()";
        }
		if(isset($sql)){
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->result_array();
			}
		}
        
    }
    
    public function brand_level_policy($id,$name,$payment_policy_uid,$brand_id){
        if($name=="Payment"){
            $sql="select * from payment_policy join policy_items on payment_policy.payment_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id!='' and policy_items.brand_id='$brand_id' and policy_items.product_id='' and policy_items.inv_id=''";
        }
        if($name=="Replacement"){
            $sql="select * from replacement_policy join policy_items on replacement_policy.replacement_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id!='' and policy_items.brand_id='$brand_id' and policy_items.product_id='' and policy_items.inv_id=''";
        }
        if($name=="Refund"){
            $sql="select * from refund_policy join policy_items on refund_policy.refund_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id!='' and policy_items.brand_id='$brand_id' and policy_items.product_id='' and policy_items.inv_id=''";
        }
        if($name=="Cancellation"){
            $sql="select * from cancellation_policy join policy_items on cancellation_policy.cancellation_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id!='' and policy_items.brand_id='$brand_id' and policy_items.product_id='' and policy_items.inv_id=''";
        }
        if($name=="Exchange"){
            $sql="select * from exchange_policy join policy_items on exchange_policy.exchange_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id!='' and policy_items.brand_id='$brand_id' and policy_items.product_id='' and policy_items.inv_id=''and now() > exchange_policy.exchange_start_date and  not exchange_policy.exchange_end_date < now()";
        }
		if(isset($sql)){
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->result_array();
			}
		}
        
        
    }
    public function product_level_policy($id,$name,$payment_policy_uid,$product_id){
        if($name=="Payment"){
            $sql="select * from payment_policy join policy_items on payment_policy.payment_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id!='' and policy_items.brand_id!='' and policy_items.product_id='$product_id' and policy_items.inv_id=''";
        }
        if($name=="Replacement"){
            $sql="select * from replacement_policy join policy_items on replacement_policy.replacement_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id!='' and policy_items.brand_id!='' and policy_items.product_id='$product_id' and policy_items.inv_id=''";
        }
        if($name=="Refund"){
            $sql="select * from refund_policy join policy_items on refund_policy.refund_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id!='' and policy_items.brand_id!='' and policy_items.product_id='$product_id' and policy_items.inv_id=''";
        }
        if($name=="Cancellation"){
            $sql="select * from cancellation_policy join policy_items on cancellation_policy.cancellation_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id!='' and policy_items.brand_id!='' and policy_items.product_id='$product_id' and policy_items.inv_id=''";
        }
        if($name=="Exchange"){
            $sql="select * from exchange_policy join policy_items on exchange_policy.exchange_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id!='' and policy_items.brand_id!='' and policy_items.product_id='$product_id' and policy_items.inv_id='' and now() > exchange_policy.exchange_start_date and  not exchange_policy.exchange_end_date < now()";
        }
		if(isset($sql)){
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->result_array();
			}
		}
        
    }

public function inventory_level_policy($id,$name,$payment_policy_uid,$inventory_id){
        if($name=="Payment"){
            $sql="select * from payment_policy join policy_items on payment_policy.payment_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id!='' and policy_items.brand_id!='' and policy_items.product_id!='' and policy_items.inv_id='$inventory_id'";
        }
        if($name=="Replacement"){
            $sql="select * from replacement_policy join policy_items on replacement_policy.replacement_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id!='' and policy_items.brand_id!='' and policy_items.product_id!='' and policy_items.inv_id='$inventory_id'";
        }
        if($name=="Refund"){
            $sql="select * from refund_policy join policy_items on refund_policy.refund_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id!='' and policy_items.brand_id!='' and policy_items.product_id!='' and policy_items.inv_id='$inventory_id'";
        }
        if($name=="Cancellation"){
            $sql="select * from cancellation_policy join policy_items on cancellation_policy.cancellation_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id!='' and policy_items.brand_id!='' and policy_items.product_id!='' and policy_items.inv_id='$inventory_id'";
        }
        if($name=="Exchange"){
            $sql="select * from exchange_policy join policy_items on exchange_policy.exchange_policy_uid=policy_items.policy_uid where policy_items.policy_type=$id and policy_items.pcat_id!='' and policy_items.cat_id!='' and policy_items.subcat_id!='' and policy_items.brand_id!='' and policy_items.product_id!='' and policy_items.inv_id='$inventory_id' and now() > exchange_policy.exchange_start_date and  not exchange_policy.exchange_end_date < now()";
        }
		if(isset($sql)){
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->result_array();
			}
		}
        
    }
    
	public function get_promotions($inventory_id){
		$sql="select * from promotion_items_taged_for_purchasing where inv_id='$inventory_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		
	}
	public function get_all_promotions_data($promo_uid,$inventory_id){
		$sqls="SELECT * ,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id FROM (`promotions`) 
				LEFT JOIN `promotion_items_taged_for_purchasing_free` 
				ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
				LEFT JOIN `promotion_items_taged_for_purchasing` 
				ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid' and promotion_items_taged_for_purchasing.inv_id='$inventory_id' and now() > promotions.promo_start_date and  not promotions.promo_end_date < now()";
		$result=$this->db->query($sqls);
        return $result->result();
	}
    
    public function product_level_promotion_uid($product_id){
        $sql="select * from promotion_items_taged_for_purchasing where  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='$product_id' and promotion_items_taged_for_purchasing.brand_id  is not NULL and promotion_items_taged_for_purchasing.subcat_id is not NULL and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    public function brand_level_promotion_uid($brand_id){
        $sql="select * from  promotion_items_taged_for_purchasing where  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='$brand_id' and promotion_items_taged_for_purchasing.subcat_id is not NULL and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    public function subcat_level_promotion_uid($subcat_id){
        $sql="select *  from  promotion_items_taged_for_purchasing where  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='$subcat_id' and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    public function cat_level_promotion_uid($cat_id){
        $sql="select *  from  promotion_items_taged_for_purchasing where  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.cat_id='$cat_id' and promotion_items_taged_for_purchasing.pcat_id is not NULL";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    public function pcat_level_promotion_uid($pcat_id){
        $sql="select *  from  promotion_items_taged_for_purchasing where  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id is not NULL and promotion_items_taged_for_purchasing.subcat_id  is not NULL and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id='$pcat_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    public function store_level_promotion_uid(){
        $sql="select * from promotion_items_taged_for_purchasing where promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.cat_id='' and promotion_items_taged_for_purchasing.pcat_id=''";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    
    public function product_level_promotion($promo_uid,$product_id){
        $sqls="SELECT *,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id
         FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid' and  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='$product_id' and promotion_items_taged_for_purchasing.brand_id is not NULL and promotion_items_taged_for_purchasing.subcat_id is not NULL and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL and now() > promotions.promo_start_date  and not promotions.promo_end_date < now()";
        $result=$this->db->query($sqls);
        return $result->result();
    }
    
    public function brand_level_promotion($promo_uid,$brand_id){
      $sqls="SELECT * ,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id
         FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid'  and promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='$brand_id' and promotion_items_taged_for_purchasing.subcat_id is not NULL and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL and now() > promotions.promo_start_date and  not promotions.promo_end_date < now()";
        $result=$this->db->query($sqls);
        return $result->result();
    }
    
    public function subcat_level_promotion($promo_uid,$subcat_id){
        $sqls="SELECT *,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id
         FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid'  and promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='$subcat_id' and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL and now() > promotions.promo_start_date  and not promotions.promo_end_date < now()";
        $result=$this->db->query($sqls);
        return $result->result();
    }
    
    public function cat_level_promotion($promo_uid,$cat_id){
        $sqls="SELECT * ,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid'  and promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.cat_id='$cat_id' and promotion_items_taged_for_purchasing.pcat_id  is not NULL and now() > promotions.promo_start_date  and not promotions.promo_end_date < now()";
        $result=$this->db->query($sqls);
        return $result->result();
    }
    
    public function pcat_level_promotion($promo_uid,$pcat_id){
        $sqls="SELECT * ,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid'  and promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.cat_id='' and promotion_items_taged_for_purchasing.pcat_id='$pcat_id' and now() > promotions.promo_start_date  and not promotions.promo_end_date < now()";
        $result=$this->db->query($sqls);
        return $result->result();
    }
    
    public function store_level_promotion($promo_uid){
        $sqls="SELECT * ,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid' and promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.cat_id='' and promotion_items_taged_for_purchasing.pcat_id='' and promotion_items_taged_for_purchasing.inv_id='' and now() > promotions.promo_start_date   and not promotions.promo_end_date < now()";
        $result=$this->db->query($sqls);
        return $result->result();
    }
    
    public function get_product_id($inventory_id){
        $sql="select product_id from inventory where id='$inventory_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
          return  $result->row()-> product_id;
        }
        else{
            return false;
        }
    }
    public function get_a_TO_z_id($product_id){
        $sql="select * from products where product_id='$product_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()==1){
          return  $result->result_array();
        }
        else{
            return false;
        }
    }
    
    public function get_free_item_data($ids){
        $sql="select * from inventory where id='$ids'";
        $result=$this->db->query($sql);
        if($result->num_rows()==1){
          return  $result->result_array();
        }
        else{
            return false;
        }
    }
    public function get_product_name($product_id){
       $sql="select product_name from products where product_id='$product_id'";
        $result=$this->db->query($sql);
         return  $result->row()->product_name;
 
    }
}
?>