<?php
	class Customer_frontend extends CI_Model{
	//new functions are created//////

	function __construct(){
		parent::__construct();	
		$this->load->helper('user_analytics');			
	}
	public function get_all_tree_of_products($p_id)
	{
		$sql="select A.pcat_name,C.cat_name,S.subcat_name,P.product_name,P.pcat_id,P.cat_id,P.subcat_id from products P inner join parent_category A on P.pcat_id = A.pcat_id inner join category C on P.cat_id = C.cat_id inner join subcategory S on P.subcat_id = S.subcat_id where P.product_id = '$p_id' and P.view=1";
		
		$result = $this->db->query($sql);
		$c=count($result->result());
		if(empty($c)){
			$sql="select C.cat_name,S.subcat_name,P.product_name,P.pcat_id,P.cat_id,P.subcat_id from products P inner join category C on P.cat_id = C.cat_id inner join subcategory S on P.subcat_id = S.subcat_id where P.product_id = '$p_id' and P.view=1";
		
			$result = $this->db->query($sql);
		}
        return $result->result();
	}
	public function get_count_of_products($f_id)
	{
		$sql="select count(*) from products_filter where filter_id='$f_id'";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$count = $result['count(*)'];
		return $count;
	}
	public function get_list_of_filter_options($filterbox_id){
	
	$qry = "SELECT * from filter where filterbox_id='$filterbox_id' and view_filter='1' ORDER BY filter.sort_order ASC";
		
		$result = $this->db->query($qry);
		
        return $result->result();
		
	}
	public function get_list_of_filters($arr)
	{
		$l=count($arr);	$i=0; $value='';
		if($l==1)
		{
			$value .= "A.pcat_view = 1 and ";
		}
		if($l==2)
		{
			$value .= "A.cat_view = 1 and ";
		}
		if($l==3)
		{
			$value .= "A.subcat_view = 1 and ";
		}
		foreach($arr as $key=>$val)
		{
			$i++;
			$value .= "A.$key = $val ";
			if($i != $l)
			$value.= "and ";
		}
		//echo $value;
		
		$sql="select A.*,B.* from filterbox_to_category A inner join filterbox B on A.filterbox_id = B.filterbox_id where B.view_filterbox = '1' and $value GROUP BY A.filterbox_id";
		
		$result = $this->db->query($sql);
        return $result->result();
	}
	
	public function to_return_products($arr)
	{
		$l=count($arr);	$i=0; $value='';
		foreach($arr as $key=>$val)
		{
			$i++;
			$value .= "A.$key = $val ";
			if($i != $l)
			$value.= "and ";
		}
		
		$sql="select A.*,B.* from products A inner join inventory B on A.product_id = B.product_id where A.view = '1' and $value GROUP BY A.product_id";
		
		$result = $this->db->query($sql);
        return $result->result();
		
	}
	public function get_name_of_parent($pcat_id)
	{
		$qry = "SELECT pcat_name from `parent_category` where pcat_id=$pcat_id and view=1";
		$result = $this->db->query($qry);
        return $result->result();
	}
	public function get_name_of_category($pcat_id,$cat_id)
	{
		$qry = "SELECT cat_name from `category` where pcat_id=$pcat_id and cat_id=$cat_id and view=1";
		$result = $this->db->query($qry);
        return $result->result();
	}
	public function get_name_of_subcategory($pcat_id,$cat_id,$subcat_id)
	{
		$qry = "SELECT subcat_name from `subcategory` where pcat_id=$pcat_id and cat_id=$cat_id and subcat_id=$subcat_id and view=1";
		$result = $this->db->query($qry);
        return $result->result();
	}
	
	public function provide_parent_category_level() {
		$qry_bp = 'SELECT pcat_id,pcat_name,menu_level,menu_sort_order,show_sub_menu from `parent_category` where menu_level=1 and view=1 order by menu_sort_order';
		$result = $this->db->query($qry_bp);
        return $result->result();
    }
	public function provide_category_level() {
		$qry_bp = 'SELECT pcat_id,cat_id,cat_name,menu_level,menu_sort_order from `category` where menu_level=1 and view=1';
		$result = $this->db->query($qry_bp);
        return $result->result();
    }
	public function provide_parent_category() {
		$qry_bp = 'SELECT pcat_id,pcat_name from `parent_category` where `view` = "1" ';
		$result = $this->db->query($qry_bp);
        return $result->result();
    }
	public function provide_category_1($pcat_id) {
		$qry_bp = "SELECT cat_id,cat_name from `category` where `view` = 1 and pcat_id=$pcat_id";
		$result = $this->db->query($qry_bp);
        return $result->result();
    }
	public function provide_category($pcat_id) {
		$qry_bp = "SELECT cat_id,cat_name,menu_level from `category` where `view` = 1 and pcat_id=$pcat_id";
		$result = $this->db->query($qry_bp);
        return $result->result();
    }
	public function provide_sub_category($cat_id) {
		$qry_bp = "SELECT subcat_id,subcat_name,menu_sort_order from `subcategory` where `view` = 1 and cat_id=$cat_id";
		$result = $this->db->query($qry_bp);
        return $result->result();
    }
	public function provide_brands($subcat_id) {
		$qry_bp = "SELECT brand_id,brand_name from `brands` where `view` = 1 and subcat_id=$subcat_id";
		$result = $this->db->query($qry_bp);
        return $result->result();
    }	
	public function check_email($cust_id,$email){
		$sql="select count(*) from customer where email='$email' and id!='$cust_id'";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$count = $result['count(*)'];
		return $count;
	}
	public function get_email_db($cust_id){
		$sql="select email from customer where  id='$cust_id'";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$email = $result['email'];
		return $email;
	}
	public function update_verification_code_db($cust_id,$verif_code){
		$verif_code="1234";
		$sql="update customer set random_no='$verif_code' where id='$cust_id'";
		$this->db->query($sql);
	}
	public function get_verif_code_db($cust_id,$rn){
		
		$sql="select random_no from customer where  id='$cust_id'";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		if(count($result)>0)
		{
			$random_no = $result['random_no'];
			if($rn==$random_no)
			{
				$valid=true;
			}
			else
			{
				$valid=false;
			}
		}else{
			$valid=false;
		}
			echo json_encode(array(
			'data' => $valid,
			));
		//return $random_no;
	}
	public function update_email_db($cust_id,$email){
		$sql="update customer set email='$email' where id='$cust_id'";
		return $this->db->query($sql);
	}

	function get_product_name_by_product_id($product_id){
		$query =$this->db->query("SELECT product_name from products where product_id='$product_id'");
		return $query->result();
	}
	
	function get_product_price_by_product_id($product_id){
		$query =$this->db->query("SELECT selling_price from  inventory where product_id='$product_id'");
		return $query->result();
	}
	public function get_product_all_details($product_id){
		$sql="select cat_id,subcat_id,brand_id from products where product_id='$product_id'";
		$query=$this->db->query($sql);
		$queryObj=$query->result();
		return $queryObj[0];
	}
	
	public function get_feedback_questions(){
		$sql="select * from feedback_questions";
		$result=$this->db->query($sql);
		if($result->num_rows>0){
			return $result->result();
		}
		else{
			return false;
		}
	}
	
	
	/*-------------------------*/
	
	
	public function checkemailverification($email) {
		$qry_bp = 'SELECT active from `customer` where `email` = "'.$email.'" ';
		$result = $this->db->query($qry_bp);
        return $result->result();
    }
	function verify_email($email){
		$sql="select count(*) as count from customer where email='$email'";
	 	$valid = true;
		$query=$this->db->query($sql);
		if($query->row()->count>0){
			
			$valid=false;
		}
		echo json_encode(array(
			'valid' => $valid,
			));
	 }
	  function verify_mobile($mobile){
		$sql="select count(*) as count from customer where mobile like '%$mobile%'";
	 	$valid = true;
		$query=$this->db->query($sql);
		if($query->row()->count>0){
			
			$valid=false;
		}
		echo json_encode(array(
			'valid' => $valid,
			));
	 }
	 function add_customer($name,$email,$mobile,$password,$rand)
	 {
		 $rand="1234";
		 $temp_arr=array();
		 $pass=md5($password);		 
		 $image="assets/pictures/cust_uploads/user.jpg";
		 $data=array(
					"name"=>$name,
					"email"=>$email,
					"mobile"=>$mobile,
					"password"=>$pass,
					"random_no"=>$rand,
					"image"=>$image
					);
					
					
		$result=$this->db->insert("customer",$data);
		if($result){
			$valid=true;
			$c_id=$this->db->insert_id();
			$temp_arr['c_id']=$c_id;	
			echo json_encode(array(
			'data' => $valid,
			'c_id'=>$c_id
			));
		}
		else{
			$valid=false;
			echo json_encode(array(
			'data' => $valid,
			));
		}
	 }
	 function update_new_customer($c_id,$name,$email,$mobile,$password,$rand)
	 {
		 $pass=md5($password);		 
		 $rand="1234";
		 $sql="update customer set name='$name',email='$email',mobile='$mobile',password='$pass',random_no='$rand' where id='$c_id'";
		 
		$result=$this->db->query($sql);
		
		if($result)
		{
			$valid=true;
			echo json_encode(array(
			'data' => $valid,
			'c_id'=>$c_id
			));
		}else{
			$valid=false;
			echo json_encode(array(
			'data' => $valid,
			));
		}
		 
	}
	function resend_code_to_newuser($c_id,$mobile,$rand){
		$rand="1234";
		$sql="update customer set random_no='$rand' where id='$c_id' and mobile='$mobile'";
		 
		$result=$this->db->query($sql);
		
		if($result)
		{
			$valid=true;
			echo json_encode(array(
			'data' => $valid,
			'c_id'=>$c_id
			));
		}else{
			$valid=false;
			echo json_encode(array(
			'data' => $valid,
			));
		}
	}
	function resend_code_to_setpassword($val,$rand){
		$temp=array();
		$rand="1234";
		if(is_numeric($val))
		{	 
			$sql="update customer set random_no='$rand' where mobile='$val'";
			$sql_cid="select id from customer where mobile='$val' and active=1 and mobile_verification=1";
			$temp['field']="mobile";
			$temp['value']=$val;
			
		}else
		{
			$sql="update customer set random_no='$rand' where email='$val'";
			$sql_cid="select * from customer where email='$val' and active=1 and mobile_verification=1";
			$temp['field']="email";
			$temp['value']="$val";
		}
			$result=$this->db->query($sql);

			$query_res=$this->db->query($sql_cid);
			if($result){
				if($query_res->result()){
					$values=$query_res->result();
					$c_id=$values[0]->id;
					$valid=true;
					$temp['valid']=$valid;
					$temp['c_id']=$c_id;			
					echo json_encode($temp);
				}
				else{
				$valid=false;
				echo json_encode(array(
				'valid' => $valid,
				));
				}
			}
			else{
				$valid=false;
				echo json_encode(array(
				'valid' => $valid,
				));
			}
	}
	function resend_code_to_newpassword($c_id,$val,$rand){
		
		$temp=array();
		$rand="1234";
		if(is_numeric($val))
		{	 
			$sql="update customer set random_no='$rand' where mobile='$val' and id='$c_id'";
			
		}else
		{
			$sql="update customer set random_no='$rand' where email='$val' and id='$c_id'";
		}
			$result=$this->db->query($sql);

			
			if($result){
				
					$valid=true;
					$temp['valid']=$valid;

					echo json_encode($temp);
			}
			else{
				$valid=false;
				echo json_encode(array(
				'valid' => $valid,
				));
			}
	}
	function activate_new_user($c_id){
		 
		$sql="update customer set active=1,mobile_verification=1 where id='$c_id'";
		 
		$result=$this->db->query($sql);
		return $result;
		
	}
	 
	 
	function customer_verify_user($val){
		if(is_numeric($val)){	 
			 $sql="select block_user,active,deactivated_by,mobile_verification from customer where mobile='$val'";
		}else{
			 $sql="select block_user,active,deactivated_by,mobile_verification from customer where email='$val'";
		}
	 	$valid = true;
		
		$query=$this->db->query($sql);
		if(!empty($query->row())){
			$user_blocked=$query->row()->block_user;
			$active=$query->row()->active;
			$deactivated_by=$query->row()->deactivated_by;
			$mobile_verification=$query->row()->mobile_verification;
			$valid=false;
			
			echo json_encode(array(
				'valid' => $valid,
				'is_user_blocked'=>$user_blocked,
				'active'=>$active,
				'deactivated_by'=>$deactivated_by,
				'mobile_verification'=>$mobile_verification
				));
		}
		else{
			$valid=true;
			echo json_encode(array(
					'valid' => $valid
					));
		}
	}
	function customer_login($un,$pw,$type){
		$customer_id="";
		$mobile="";
		 $password_login=md5($pw);
		 
		 				
						
		 if (is_numeric($un)){
		 $sql_exists_admin="select count(*) as count from customer where mobile='$un' and password='$password_login' and block_user=0";
		 }
		 if(!is_numeric($un)){
			$sql_exists_admin="select count(*) as count from customer where email='$un' and password='$password_login' and block_user=0";
		 }
		 $query=$this->db->query($sql_exists_admin);
		 
		 if($query->row()->count>0){
			 
			if($type=="re_activate"){
				if (is_numeric($un)){
				 $sql_update_customer="update customer set active=1 where mobile='$un' and password='$password_login' and block_user=0";
				}
				if(!is_numeric($un)){
					$sql_update_customer="update customer set active=1 where email='$un' and password='$password_login' and block_user=0";
				}
				$query_update=$this->db->query($sql_update_customer);
				
			}
			if(is_numeric($un)){	 
			 $sql_select_admin="select * from customer where mobile='$un' and password='$password_login' and active=1 and mobile_verification=1";
			}else{
				$sql_select_admin="select * from customer where email='$un' and password='$password_login' and active=1 and mobile_verification=1";
			}
			$query_res=$this->db->query($sql_select_admin);
			$values=$query_res->result();
			if($query_res->num_rows()>0){
				$this->session->set_userdata("user_type","customer");
				$this->session->set_userdata("logged_in",true);
				$this->session->set_userdata("customer_id", $values[0]->id);
				$this->session->set_userdata("customer_email",$values[0]->email);
				$this->session->set_userdata("customer_mobile",$values[0]->mobile);
				$this->session->set_userdata("customer_name",$values[0]->name);
				$this->session->set_userdata("customer_gender",$values[0]->gender);
				$this->session->set_userdata("customer_image",$values[0]->image);
				$this->session->set_userdata("user_category",$values[0]->user_category);
			
				$current_login_customer = date("D j M Y - h:i a");
				$sql_current_login_customer="UPDATE `customer` SET `current_login` = '$current_login_customer' WHERE `id` = '".$this->session->userdata('customer_id')."'";
				//$resu=mysql_query($sql_current_login_customer);
				$resu=$this->db->query($sql_current_login_customer);
				if($resu){
					$valid=true;
				}else{
					$valid=false;
				}
			}
			else{
				$valid="verifyfalse";
				
					if(is_numeric($un)){	 
					 $sql_select_admin="select * from customer where mobile='$un' and password='$password_login' and  mobile_verification=0";
					}else{
						$sql_select_admin="select * from customer where email='$un' and password='$password_login' and mobile_verification=0";
					}
					$query_res=$this->db->query($sql_select_admin);
					$values=$query_res->result();
					if($query_res->num_rows()>0){
									$mobile=$values[0]->mobile;
									$customer_id=$values[0]->id;
									$rand=mt_rand(1000,10000);
									
										$mobile_stuff_arr=mobile_stuff();
										$authKey = $mobile_stuff_arr["authKey_smstemplate"];
										$mobileNumber = "+91".$mobile;
										$senderId = $mobile_stuff_arr["senderId_smstemplate"];
										
										

										$message = urlencode("Your verification code is ".$rand);
										$route = "4";
										$postData = array(
											'authkey' => $authKey,
											'mobiles' => $mobileNumber,
											'message' => $message,
											'sender' => $senderId,
											'route' => $route
										);

										$url="https://control.msg91.com/api/sendhttp.php";
										$ch = curl_init();
										curl_setopt_array($ch, array(
											CURLOPT_URL => $url,
											CURLOPT_RETURNTRANSFER => true,
											CURLOPT_POST => true,
											CURLOPT_POSTFIELDS => $postData
										));
										curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
										curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

										$output = curl_exec($ch);
							
										if(curl_errno($ch))
										{
											echo 'error:' . curl_error($ch);
										}else{
											//no error - code has been sent
											$rand="1234";
											$sql_update="update customer set random_no='$rand' where id='$customer_id'";
											$query_update=$this->db->query($sql_update);
										}

										curl_close($ch);
					}
					else{
						
						if(is_numeric($un)){	 
							 $sql_select_admin="select * from customer where mobile='$un' and password='$password_login' and  active=0";
						}else{
							$sql_select_admin="select * from customer where email='$un' and password='$password_login' and active=0";
						}
						$query_res=$this->db->query($sql_select_admin);
						$values=$query_res->result();
						if($query_res->num_rows()>0){
							$valid="active_false";
						}
					}
					
					
			}
		 }else{
			 $valid=false;
		 }
		
		 if($valid===true){ 
			//$userID=get_cookie('userID');
			$customer_id=$this->session->userdata("customer_id");
			//$user_type=$customer_id;
			//$log_activity=array("login"=>time());
			//$log_activity=json_encode($log_activity);
			
			//$today=date('Y-m-d');
			
			//$sql="update visitor_data set log_actitivity=concat(log_actitivity,',','$log_activity') where visitor_id='$userID' and date(timestamp)='$today' and user_type='$user_type'";
			//$res=$this->db->query($sql);


			/* update login activity */
			$stat=update_login_activity($customer_id);
			/* update login activity */

		}
		
		echo json_encode(array(
			'valid' => $valid,
			'login_type'=>$type,
			'customer_id'=>$customer_id,
			'mobile'=>$mobile
			));
	}
	function reset_password($c_id,$pw){

		$passw=md5($pw);
		$sql="update customer set password='$passw' where id='$c_id'";

		$result=$this->db->query($sql);
		
		if($result)
		{
			$valid=true;
			echo json_encode(array(
			'data' => $valid,
			));
		}else{
			$valid=false;
			echo json_encode(array(
			'data' => $valid,
			));
		}
	}

	// from edata///////////////////////////////////
	
	public function get_category_name($parent_cat){
		$query=$this->db->select("cat_name")->from("category")->where("cat_id",$parent_cat)->get();
		return $query->result();
	}
	public function get_subcategory_name($parent_cat){
		$query=$this->db->select("cat_name")->from("category")->where("cat_id",$parent_cat)->get();
		return $query->result();
	}
	public function get_brand_name($parent_cat){
		$query=$this->db->select("cat_name")->from("category")->where("cat_id",$parent_cat)->get();
		return $query->result();
	}
	function get_customer_comment_1($id=""){
				
		$query =$this->db->query("SELECT *, DATE_FORMAT(timestamp,'%b %d %Y %h:%i %p') as tstamp FROM `complaints` where `id`='$id' and view_status=0");
		return $query->result();
	}
	public function get_close_complaints_from_cutomer($c_id,$close_response){
	
		$sql="update complaints set closed_status='1', close_response='$close_response' where complaint_id='$c_id'";
		
		$query=$this->db->query($sql);
		return 1;
	}

	public function delete_complaint_from_cutomer($i=""){
	
		$sql="update complaints set view_status='1' where i='$i'";		
		$query=$this->db->query($sql);
	}
	public function get_cart_table_details($customer_id)
	{
		$sql_cart="select * from carttable where customer_id = '$customer_id'";
		$query=$this->db->query($sql_cart);
		
		return $query->result();
		//echo $customer_id;
	}
	
	public function get_customer_info_by_cid($c_id){
		$sql="select * from customer where id='$c_id'";
		$query=$this->db->query($sql);
		return $query->row();
	}
	
	public function get_all_categories(){
		$sql="select cat_id,pcat_id,cat_name from category where active='1' order by cat_id";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_all_parent_categories(){
		$sql="select pcat_id,pcat_name from parent_category where active='1' order by pcat_id";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	public function create_wallet_account($c_id){
		$sql="insert into wallet (customer_id) values ('$c_id')";	
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	public function get_products_under_this_category($id){
		$sql="select products.*,inventory.*,inventory.id as inventory_id from products,inventory where products.cat_id='$id' and inventory.product_id=products.product_id limit 0,4";
		$result=$this->db->query($sql);
		return $result->result();	
	}
	public function get_category($id){
		$sql="select * from category where cat_id='$id'";
		$result=$this->db->query($sql);
		return $result->row();
	}
	public function bg_image_index(){
		$sql="select * from ui_front where type='banner' and cat_id='' and page='common'";
		$result=$this->db->query($sql);
		return $result->row();
	}
	public function foot_image_index(){
		$sql="select * from ui_front where type='footer' and cat_id='' and page='common'";
		$result=$this->db->query($sql);
		return $result->row();
	}
	public function testimonial_index(){
		$sql="select * from testimonial where page='common'";
		$result=$this->db->query($sql);
		return $result->result();
	}
	public function inv_index($section_name_in_page){
		$sql="select inv_to_index.*,products.*,inventory.*,inventory.id as inventory_id from inv_to_index,products,inventory where inv_to_index.page='common' and inventory.product_id=products.product_id and inventory.id=inv_to_index.inventory_id and  inventory.product_id=inv_to_index.product_id and lower(inv_to_index.section_name_in_page)=lower('$section_name_in_page')  and inventory.inventory_type!=2 order by inv_to_index.view_order asc";
		$result=$this->db->query($sql);
		return $result->result();
	}

	public function inv_index_copy($section_name_in_page , $pcat_id){
		$sql="select inv_to_index.*,products.*,inventory.*,inventory.id as inventory_id from inv_to_index,products,inventory where inv_to_index.page='common' and inventory.product_id=products.product_id and inventory.id=inv_to_index.inventory_id and  inventory.product_id=inv_to_index.product_id and lower(inv_to_index.section_name_in_page)=lower('$section_name_in_page') and inventory.inventory_type!=2 and products.pcat_id='$pcat_id' order by inv_to_index.view_order asc";
		$result=$this->db->query($sql);
		return $result->result();
	}
	
	public function brand_index_shopbyparentcategory($section_name_in_page){
		$sql="select brand_to_index_shopbyparentcategory.*,parent_category.pcat_name from brand_to_index_shopbyparentcategory,parent_category where brand_to_index_shopbyparentcategory.pcat_id=parent_category.pcat_id and  lower(brand_to_index_shopbyparentcategory.section_name_in_page)=lower('$section_name_in_page') order by brand_to_index_shopbyparentcategory.view_order asc";
		$result=$this->db->query($sql);
		return $result->result();
	}
	
	public function brand_index_shopbybrand($section_name_in_page){
		$sql="select brand_to_index_shopbybrand.* from brand_to_index_shopbybrand where lower(brand_to_index_shopbybrand.section_name_in_page)=lower('$section_name_in_page') order by brand_to_index_shopbybrand.view_order asc";
		$result=$this->db->query($sql);
		return $result->result();
	}
	
	public function brand_index_shopbycategory($section_name_in_page){
		$sql="select brand_to_index_shopbycategory.*,category.cat_name from brand_to_index_shopbycategory,category where brand_to_index_shopbycategory.cat_id=category.cat_id and  lower(brand_to_index_shopbycategory.section_name_in_page)=lower('$section_name_in_page') order by brand_to_index_shopbycategory.view_order asc";
		$result=$this->db->query($sql);
		return $result->result();
	}
	
	public function site_description(){
		$sql="select * from site_description where active=1 order by view_order asc";
		$result=$this->db->query($sql);
		return $result->result();
	}
	public function bg_image_cat_index($cat_id){
		$sql="select * from ui_front where type='banner' and cat_id='$cat_id' and page='category'";
		$result=$this->db->query($sql);
		return $result->row();
	}
	public function testimonial_cat_index($cat_id){
		$sql="select * from testimonial where page='category' and cat_id='$cat_id' ";
		$result=$this->db->query($sql);
		return $result->result();
	}
	public function inv_cat_index($cat_id){
		$sql="select inv_to_index.*,products.*,inventory.*,inventory.id as inventory_id from inv_to_index,products,inventory where inv_to_index.page='category' and inventory.product_id=products.product_id and inventory.id=inv_to_index.inventory_id and  inventory.product_id=inv_to_index.product_id and inv_to_index.cat_id='$cat_id' order by inv_to_index.view_order asc";
		$result=$this->db->query($sql);
		return $result->result();
	}
	/* user activity */
	public function update_section_activity($data,$userID,$user_type){
		$today=date('Y-m-d');
		$sql="update visitor_data set user_behaviour=concat(user_behaviour,',','$data') where visitor_id='$userID' and date(timestamp)='$today' and user_type='$user_type'";
		$res=$this->db->query($sql);
		return $res;
		return true;
	}
	public function user_btn_activity($data,$userID,$user_type){
		$today=date('Y-m-d');
		$sql="update visitor_data set user_btn_activity=concat(user_btn_activity,',','$data') where visitor_id='$userID' and date(timestamp)='$today' and user_type='$user_type'";
		$res=$this->db->query($sql);
		return $res;
		return true;
	}
	public function user_reviews_read($data,$userID,$user_type){
		$today=date('Y-m-d');
		$sql="update visitor_data set reviews_read=concat(reviews_read,',','$data') where visitor_id='$userID' and date(timestamp)='$today' and user_type='$user_type'";
		$res=$this->db->query($sql);
		return $res;
		return true;
	}
	public function user_filter_checked($data,$userID,$user_type){
		$today=date('Y-m-d');
		$sql="update visitor_data set filter_selection=concat(filter_selection,',','$data') where visitor_id='$userID' and date(timestamp)='$today' and user_type='$user_type'";
		$res=$this->db->query($sql);
		return $res;
		return true;
	}

	/* user activity */
	public function update_visitor_logout($data="",$userID="",$user_type=""){
		//$userID=get_cookie('userID');
		$customer_id=$this->session->userdata("customer_id");
		//$user_type=$customer_id;
		//$log_activity=array("logout"=>time());
		//$log_activity=json_encode($log_activity);
		
		//$today=date('Y-m-d');
		
		//$sql="update visitor_data set log_actitivity=concat(log_actitivity,',','$log_activity') where visitor_id='$userID' and date(timestamp)='$today' and user_type='$user_type'";
		//$res=$this->db->query($sql);
		
		$last_login = date("D j M Y - h:i a");
		$last_login="UPDATE `customer` SET `last_login` = '$last_login' WHERE `id` = '$customer_id'";
		//$resu=mysql_query($sql_current_login_customer);
		$resu=$this->db->query($last_login);
		
		/* update logout activity */
		$stat=update_logout_activity($customer_id);
		/* update logout activity */

		//return $res;
		return true;
	}
	public function get_hot_searched_key_words(){
		$sql="select searched_keyword,number_of_hits from searched_keyword order by number_of_hits desc limit 0,4";
		$res=$this->db->query($sql);
		return $res->result();
	}
	public function post_to_frontend_blogs(){
		$sql="select * from blog where post_to_frontend='1' and admin_publish='1'";
		$res=$this->db->query($sql);
		return $res->result_array();
	}
	public function add_partner($details_arr){
		extract($details_arr);
		
		//generate a user_id
										$date = new DateTime();
										$mt=microtime();
										$salt="business";
										$rand=mt_rand(1, 1000000);
										$ts=$date->getTimestamp();
										$str=$salt.$rand.$ts.$mt;
										$key=md5($str);
										$partner_unique_id=$key;
										
										
		$sql="insert into partner_with_us set name=".$this->db->escape($name).",city=".$this->db->escape($city).",email=".$this->db->escape($email).",mobile=".$this->db->escape($mobile).",company_name=".$this->db->escape($company_name).",interested_in=".$this->db->escape($interested_in).",comment=".$this->db->escape($comment).",partner_unique_id='$partner_unique_id'";
		$flag=$this->db->query($sql);
		
		if($flag==true){
			return array("status"=>$flag,"partner_unique_id"=>$partner_unique_id);
		}
		else{
			return array("status"=>$flag,"partner_unique_id"=>"1199");
		}
		
	}
	 public function add_news_letter_subscription($email_address_nl){
        $sql="select * from newsletter where email_address=".$this->db->escape($email_address_nl);
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            $sql="update newsletter set subscribed=1 where email_address=".$this->db->escape($email_address_nl);
            $result=$this->db->query($sql);
            if($this->db->affected_rows()>0){
                return true;
            }else{
                return true;
            }
        }else{
			if($this->session->userdata("customer_id")){
				$customer_id=$this->session->userdata("customer_id");
			}
			else{
				$customer_id=0;
			}
            $sql="INSERT INTO newsletter set email_address=".$this->db->escape($email_address_nl).",customer_id='$customer_id'";
            $result=$this->db->query($sql);
            if($this->db->affected_rows()>0){
                return true;
            }else{
                return false;
            }
        }
        
    }
	
	public function ganesh_image_yes_or_noFun(){
		$sql="select * from test";
		$result=$this->db->query($sql);
		if(!empty($result->row_array())){
			return "yes";
		}
		else{
			return "no";
		}
	}
	
	public function test_ganesh($ganesh_text){
		$sql="select * from test where BINARY(name)=BINARY('$ganesh_text')";
		$result=$this->db->query($sql);
		if(!empty($result->row_array())>0){
			$sql1="delete from test where name='$ganesh_text'";
			$result1=$this->db->query($sql1);
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function dump_failed_order_id($order_id){
		$sql_seqgen_order_id="update sequence_generator set order_id='$order_id'";
		$this->db->query($sql_seqgen_order_id);
	}
	public function get_order_id(){
		$customer_id=$this->session->userdata("customer_id");
		$generalstuff_arr=generalstuff();
		$company_gstin=$generalstuff_arr["company_gstin"];
		$company_shortcode=$generalstuff_arr["company_shortcode"];
		$firsthalf=date("dmy").strtoupper($company_shortcode);
		$random_numerics_3=sample_code_random_numerics(3);
		$random_alphabets_2=sample_code_random_alphabets(2);
		
		$sql="select order_id from sequence_generator";
		$result=$this->db->query($sql);
		$row_arr=$result->row_array();
		if(!empty($row_arr)){
			$framed_order_id=$customer_id."".$firsthalf."".$random_numerics_3."".$random_alphabets_2;
			return $framed_order_id;
		}
		else{
			$sql="insert into sequence_generator set sequence_generator=1";
			$result=$this->db->query($sql);
			$framed_order_id=$customer_id."".$firsthalf."".$random_numerics_3."".$random_alphabets_2;
			return $framed_order_id;
		}
	}
	
        
	/** Doctor Module Starts **/
	
	 

	  
	 
	 public function get_customer_profile_pic($customer_id){
		$sql="select image from customer where id='$customer_id'";
		$query=$this->db->query($sql);
		return $query->row_array()["image"];
	 }
	 
	 /** Doctor Module Ends **/




/** Become a reseller starts */
public function add_save_becomeareseller(
								$reseller_companyname,
								$reseller_name,
								$reseller_email,
								$reseller_mobile,
								$reseller_city,
								$reseller_type,
								$secure_id){
	$timestamp=date("Y-m-d H:i:s");					
	$data=array(
	"becomeareseller_companyname"=>$reseller_companyname,
	"becomeareseller_name"=>$reseller_name,
	"becomeareseller_email"=>$reseller_email,
	"becomeareseller_mobile"=>$reseller_mobile,
	"becomeareseller_city"=>$reseller_city,
	"becomeareseller_type"=>$reseller_type,
	"becomeareseller_secure_id"=>$secure_id,
	"becomeareseller_timestamp"=>$timestamp
	);

	$result=$this->db->insert("become_a_reseller",$data);
	return $result;
}
/** Become a reseller ends */

public function get_customer_info(){

	$user_type=$this->session->userdata('user_type');
	$customer_id=$this->session->userdata('customer_id');

	if($user_type=='franchise_customer'){
		$sql="select * from franchise_customer where id='$customer_id'";
	}else{
		$sql="select * from customer where id='$customer_id'";
	}
	//echo $sql;
	
	$result=$this->db->query($sql);
	if($result->num_rows()>0){
		return $result->row();
	}else{
		return false;
	}
}
public function get_order_id_for_combo(){
	$customer_id=$this->session->userdata("customer_id");
	$generalstuff_arr=generalstuff();
	$company_gstin=$generalstuff_arr["company_gstin"];
	$company_shortcode=$generalstuff_arr["company_shortcode"];
	$firsthalf=date("dmy").strtoupper($company_shortcode);
	$random_numerics_3=sample_code_random_numerics(3);
	$random_alphabets_2=sample_code_random_alphabets(2);
	
	$sql="select order_id from sequence_generator_combo";
	$result=$this->db->query($sql);
	$row_arr=$result->row_array();
	if(!empty($row_arr)){
		$framed_order_id=$customer_id."".$firsthalf."".$random_numerics_3."".$random_alphabets_2;
		return $framed_order_id;
	}
	else{
		$sql="insert into sequence_generator_combo set sequence_generator=1";
		$result=$this->db->query($sql);
		$framed_order_id=$customer_id."".$firsthalf."".$random_numerics_3."".$random_alphabets_2;
		return $framed_order_id;
	}
}
	
public function add_vendor($name,$email,$mobile,$password,$comp_data)
        {
                $data='';
		$temp_arr=array();
		$pass=md5($password);		 
		$image="assets/pictures/cust_uploads/user.jpg";


								/* new fields */
								$city=(isset($comp_data->city)) ? $this->db->escape_str($comp_data->city) : '';
								$state=(isset($comp_data->state)) ? $this->db->escape_str($comp_data->state) : '';
								$country=(isset($comp_data->country)) ? $this->db->escape_str($comp_data->country) : '';

								$about_furniture_operations='';
								if(isset($comp_data->about_furniture_operations)){
									$about_furniture_operations=(array) $comp_data->about_furniture_operations;//take keys and put comma separated values
									if(!empty($about_furniture_operations)){
										$about_furniture_operations=$this->db->escape_str(implode(',',array_keys($about_furniture_operations)));
									}else{
										$about_furniture_operations='';
									}
								}
								$applicable_media='';
								if(isset($comp_data->applicable_media)){
									$applicable_media=(array) $comp_data->applicable_media;
								}


								$website_url=isset($applicable_media["website_url"]) ? $applicable_media["website_url"] : '';
								$instagram_url=isset($applicable_media["instagram_url"]) ? $applicable_media["instagram_url"] : '';
								$facebook_url=isset($applicable_media["facebook_url"]) ? $applicable_media["facebook_url"] : '';

								$size_of_team=(isset($comp_data->size_of_team)) ? $this->db->escape_str($comp_data->size_of_team) : '';

								$kind_of_furniture=(isset($comp_data->size_of_team)) ? (array)$comp_data->kind_of_furniture : '';

								if(!empty($kind_of_furniture)){
									$kind_of_furniture=$this->db->escape_str(implode(',',array_keys($kind_of_furniture)));
								}else{
									$kind_of_furniture='';
								}

								$years_of_business=(isset($comp_data->years_of_business)) ? $this->db->escape_str($comp_data->years_of_business) : '';
								$shipping_capabilities=(isset($comp_data->years_of_business)) ? $this->db->escape_str($comp_data->shipping_capabilities) : '';

								if(isset($comp_data->shipping_capabilities_description)){
									$shipping_capabilities_description=$this->db->escape_str($comp_data->shipping_capabilities_description);
								}else{
									$shipping_capabilities_description='';
								}
	
								/* new fields */
								$address1=(isset($comp_data->address1)) ? $this->db->escape_str($comp_data->address1) : '';
								$address2=(isset($comp_data->address2)) ? $this->db->escape_str($comp_data->address2) : '';
								$pincode=(isset($comp_data->pincode)) ? $this->db->escape_str($comp_data->pincode) : '';


		$data=array(
                            "name"=>$name,
                            "email"=>$email,
                            "mobile"=>$mobile,
                            "password"=>$pass,
                            "password_org"=>$password,
                            "image"=>$image,
                            "address1"=>$address1,
                            "address2"=>$address2,
                            "pincode"=>$pincode,
							"city"=>$city,
							"state"=>$state,
							"country"=>$country,
							"about_furniture_operations"=>$about_furniture_operations,
							"website_url"=>$this->db->escape_str($website_url),
							"instagram_url"=>$this->db->escape_str($instagram_url),
							"facebook_url"=>$this->db->escape_str($facebook_url),
							"size_of_team"=>$size_of_team,
							"kind_of_furniture"=>$kind_of_furniture,
							"years_of_business"=>$years_of_business,
							"shipping_capabilities"=>$shipping_capabilities,
							"shipping_capabilities_description"=>$shipping_capabilities_description,
							"whatsapp_number"=>$this->db->escape_str($comp_data->whatsapp_number)
                            );
                 
		$result=$this->db->insert("product_vendors",$data);
                $c_id=$this->db->insert_id();
		if($result){
			$valid=true;
			$temp_arr['c_id']=$c_id;	
			$data=json_encode(array(
			'data' => $valid,
			'c_id'=>$c_id
			));
		}else{
			$valid=false;
			$data=json_encode(array(
			'data' => $valid,
			));
		}
                return $data;
	 }
	 function verify_email_vendor($email){
		$sql="select count(*) as count from product_vendors where email='$email'";
	 	$valid = true;
		$query=$this->db->query($sql);
		if($query->row()->count>0){
			
			$valid=false;
		}
		echo json_encode(array(
			'valid' => $valid,
			));
	 }
	  function verify_mobile_vendor($mobile){
		$sql="select count(*) as count from product_vendors where mobile like '%$mobile%'";
	 	$valid = true;
		$query=$this->db->query($sql);
		if($query->row()->count>0){
			
			$valid=false;
		}
		echo json_encode(array(
			'valid' => $valid,
			));
	 }
	 public function front_product_display(){
		
		// $sql="select inventory.*,inventory.id as inventory_id from inventory where inv_to_index.page='common' and inventory.product_id=products.product_id and inventory.id=inv_to_index.inventory_id and  inventory.product_id=inv_to_index.product_id and lower(inv_to_index.section_name_in_page)=lower('$section_name_in_page') order by inv_to_index.view_order asc";


		$sql = "select * from inventory ";
 
		$sql.= ' LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`'
				.' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`'
				.' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`'
				.' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`'
				.' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id` '
				." where  inventory.active='1'";
		

		$result=$this->db->query($sql);
		return $result->result();
		
	 }
	public function parent_category() {
		$query = $this->db->query("SELECT * FROM `parent_category` where active=1");
		return $query -> result();
	}
	public function get_positioning($inventory_id){
		$query = $this->db->query("SELECT * FROM `positioning` where position_inventory_id='$inventory_id'");
		return $query -> result();
	}
	 
}
?>