<?php
	class Franchise_frontend extends CI_Model{
	//new functions are created//////
	public function get_all_tree_of_products($p_id)
	{
		$sql="select A.pcat_name,C.cat_name,S.subcat_name,P.product_name,P.pcat_id,P.cat_id,P.subcat_id from products P inner join parent_category A on P.pcat_id = A.pcat_id inner join category C on P.cat_id = C.cat_id inner join subcategory S on P.subcat_id = S.subcat_id where P.product_id = '$p_id' and P.view=1";
		
		$result = $this->db->query($sql);
		$c=count($result->result());
		if(empty($c)){
			$sql="select C.cat_name,S.subcat_name,P.product_name,P.pcat_id,P.cat_id,P.subcat_id from products P inner join category C on P.cat_id = C.cat_id inner join subcategory S on P.subcat_id = S.subcat_id where P.product_id = '$p_id' and P.view=1";
		
			$result = $this->db->query($sql);
		}
        return $result->result();
	}
	public function get_count_of_products($f_id)
	{
		$sql="select count(*) from products_filter where filter_id='$f_id'";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$count = $result['count(*)'];
		return $count;
	}
	public function get_list_of_filter_options($filterbox_id){
	
	$qry = "SELECT * from filter where filterbox_id='$filterbox_id' and view_filter='1' ORDER BY filter.sort_order ASC";
		
		$result = $this->db->query($qry);
		
        return $result->result();
		
	}
	public function get_list_of_filters($arr)
	{
		$l=count($arr);	$i=0; $value='';
		if($l==1)
		{
			$value .= "A.pcat_view = 1 and ";
		}
		if($l==2)
		{
			$value .= "A.cat_view = 1 and ";
		}
		if($l==3)
		{
			$value .= "A.subcat_view = 1 and ";
		}
		foreach($arr as $key=>$val)
		{
			$i++;
			$value .= "A.$key = $val ";
			if($i != $l)
			$value.= "and ";
		}
		//echo $value;
		
		$sql="select A.*,B.* from filterbox_to_category A inner join filterbox B on A.filterbox_id = B.filterbox_id where B.view_filterbox = '1' and $value GROUP BY A.filterbox_id";
		
		$result = $this->db->query($sql);
        return $result->result();
	}
	
	public function to_return_products($arr)
	{
		$l=count($arr);	$i=0; $value='';
		foreach($arr as $key=>$val)
		{
			$i++;
			$value .= "A.$key = $val ";
			if($i != $l)
			$value.= "and ";
		}
		
		$sql="select A.*,B.* from products A inner join inventory B on A.product_id = B.product_id where A.view = '1' and $value GROUP BY A.product_id";
		
		$result = $this->db->query($sql);
        return $result->result();
		
	}
	public function get_name_of_parent($pcat_id)
	{
		$qry = "SELECT pcat_name from `parent_category` where pcat_id=$pcat_id and view=1";
		$result = $this->db->query($qry);
        return $result->result();
	}
	public function get_name_of_category($pcat_id,$cat_id)
	{
		$qry = "SELECT cat_name from `category` where pcat_id=$pcat_id and cat_id=$cat_id and view=1";
		$result = $this->db->query($qry);
        return $result->result();
	}
	public function get_name_of_subcategory($pcat_id,$cat_id,$subcat_id)
	{
		$qry = "SELECT subcat_name from `subcategory` where pcat_id=$pcat_id and cat_id=$cat_id and subcat_id=$subcat_id and view=1";
		$result = $this->db->query($qry);
        return $result->result();
	}
	
	public function provide_parent_category_level() {
		$qry_bp = 'SELECT pcat_id,pcat_name,menu_level,menu_sort_order,show_sub_menu from `parent_category` where menu_level=1 and view=1';
		$result = $this->db->query($qry_bp);
        return $result->result();
    }
	public function provide_category_level() {
		$qry_bp = 'SELECT pcat_id,cat_id,cat_name,menu_level,menu_sort_order from `category` where menu_level=1 and view=1';
		$result = $this->db->query($qry_bp);
        return $result->result();
    }
	public function provide_parent_category() {
		$qry_bp = 'SELECT pcat_id,pcat_name from `parent_category` where `view` = "1" ';
		$result = $this->db->query($qry_bp);
        return $result->result();
    }
	public function provide_category_1($pcat_id) {
		$qry_bp = "SELECT cat_id,cat_name from `category` where `view` = 1 and pcat_id=$pcat_id";
		$result = $this->db->query($qry_bp);
        return $result->result();
    }
	public function provide_category($pcat_id) {
		$qry_bp = "SELECT cat_id,cat_name,menu_level from `category` where `view` = 1 and pcat_id=$pcat_id";
		$result = $this->db->query($qry_bp);
        return $result->result();
    }
	public function provide_sub_category($cat_id) {
		$qry_bp = "SELECT subcat_id,subcat_name,menu_sort_order from `subcategory` where `view` = 1 and cat_id=$cat_id";
		$result = $this->db->query($qry_bp);
        return $result->result();
    }
	public function provide_brands($subcat_id) {
		$qry_bp = "SELECT brand_id,brand_name from `brands` where `view` = 1 and subcat_id=$subcat_id";
		$result = $this->db->query($qry_bp);
        return $result->result();
    }	
	public function check_email($cust_id,$email){
		$sql="select count(*) from franchise_customer where email='$email' and id!='$cust_id'";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$count = $result['count(*)'];
		return $count;
	}
	public function get_email_db($cust_id){
		$sql="select email from franchise_customer where  id='$cust_id'";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$email = $result['email'];
		return $email;
	}
	public function update_verification_code_db($cust_id,$verif_code){
		$verif_code="1234";
		$sql="update franchise_customer set random_no='$verif_code' where id='$cust_id'";
		$this->db->query($sql);
	}
	public function get_verif_code_db($cust_id,$rn){
		$sql="select random_no from franchise_customer where  id='$cust_id'";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		if(count($result)>0)
		{
			$random_no = $result['random_no'];
			if($rn==$random_no)
			{
				$valid=true;
			}
			else
			{
				$valid=false;
			}
		}else{
			$valid=false;
		}
			echo json_encode(array(
			'data' => $valid,
			));
		//return $random_no;
	}
	public function update_email_db($cust_id,$email){
		$sql="update franchise_customer set email='$email' where id='$cust_id'";
		return $this->db->query($sql);
	}

	function get_product_name_by_product_id($product_id){
		$query =$this->db->query("SELECT product_name from products where product_id='$product_id'");
		return $query->result();
	}
	
	function get_product_price_by_product_id($product_id){
		$query =$this->db->query("SELECT selling_price from  inventory where product_id='$product_id'");
		return $query->result();
	}
	public function get_product_all_details($product_id){
		$sql="select cat_id,subcat_id,brand_id from products where product_id='$product_id'";
		$query=$this->db->query($sql);
		$queryObj=$query->result();
		return $queryObj[0];
	}
	
	public function get_feedback_questions(){
		$sql="select * from feedback_questions";
		$result=$this->db->query($sql);
		if($result->num_rows>0){
			return $result->result();
		}
		else{
			return false;
		}
	}
	
	
	/*-------------------------*/
	
	
	public function checkemailverification($email) {
		$qry_bp = 'SELECT active from  `franchise_customer` where `email` = "'.$email.'" ';
		$result = $this->db->query($qry_bp);
        return $result->result();
    }
	function verify_email($email){
		$sql="select count(*) as count from franchise_customer where email='$email'";
	 	$valid = true;
		$query=$this->db->query($sql);
		if($query->row()->count>0){
			
			$valid=false;
		}
		echo json_encode(array(
			'valid' => $valid,
			));
	 }
	  function verify_mobile($mobile){
		$sql="select count(*) as count from franchise_customer where mobile like '%$mobile%'";
	 	$valid = true;
		$query=$this->db->query($sql);
		if($query->row()->count>0){
			
			$valid=false;
		}
		echo json_encode(array(
			'valid' => $valid,
			));
	 }
	 function add_customer($name,$email,$mobile,$password,$rand,$pincode,$city,$state,$country)
	 {
		 $rand="1234";
		 $temp_arr=array();
		 //$pass=md5($password);
		 $pass='';		 
		 $image="assets/pictures/franchise_uploads/user.jpg";
		 $data=array(
					"name"=>$name,
					"email"=>$email,
					"mobile"=>$mobile,
					"password"=>$pass,
					"random_no"=>$rand,
					"image"=>$image,
					"pincode"=>$pincode,
					"city"=>$city,
					"state"=>$state,
					"country"=>$country
					);
					
					
		$result=$this->db->insert( "franchise_customer",$data);
		if($result){
			$valid=true;
			$c_id=$this->db->insert_id();
			$temp_arr['c_id']=$c_id;	
			echo json_encode(array(
			'data' => $valid,
			'c_id'=>$c_id
			));
		}
		else{
			$valid=false;
			echo json_encode(array(
			'data' => $valid,
			));
		}
	 }
	 function update_new_customer($c_id,$name,$email,$mobile,$password,$rand,$pincode,$city,$state,$country)
	 {
		 //$pass=md5($password);		 
		 
		 $pass='';
		 $rand="1234";
		 $sql="update franchise_customer set name='$name',email='$email',mobile='$mobile',password='$pass',random_no='$rand',pincode='$pincode',state='$state',city='$city',country='$country' where id='$c_id'";
		 
		$result=$this->db->query($sql);
		
		if($result)
		{
			$valid=true;
			echo json_encode(array(
			'data' => $valid,
			'c_id'=>$c_id
			));
		}else{
			$valid=false;
			echo json_encode(array(
			'data' => $valid,
			));
		}
		 
	}
	
	function resend_code_to_newuser($c_id,$mobile,$rand){
		$rand="1234";
		$sql="update franchise_customer set random_no='$rand' where id='$c_id' and mobile='$mobile'";
		 
		$result=$this->db->query($sql);
		
		if($result)
		{
			$valid=true;
			echo json_encode(array(
			'data' => $valid,
			'c_id'=>$c_id
			));
		}else{
			$valid=false;
			echo json_encode(array(
			'data' => $valid,
			));
		}
	}
	function resend_code_to_setpassword($val,$rand){
		$temp=array();
		$rand="1234";
		if(is_numeric($val))
		{	 
			$sql="update franchise_customer set random_no='$rand' where mobile='$val'";
			$sql_cid="select id from franchise_customer where mobile='$val' and active=1 and mobile_verification=1";
			$temp['field']="mobile";
			$temp['value']=$val;
			
		}else
		{
			$sql="update franchise_customer set random_no='$rand' where email='$val'";
			$sql_cid="select * from franchise_customer where email='$val' and active=1 and mobile_verification=1";
			$temp['field']="email";
			$temp['value']="$val";
		}
			$result=$this->db->query($sql);

			$query_res=$this->db->query($sql_cid);
			if($result){
				if($query_res->result()){
					$values=$query_res->result();
					$c_id=$values[0]->id;
					$valid=true;
					$temp['valid']=$valid;
					$temp['c_id']=$c_id;			
					echo json_encode($temp);
				}
				else{
				$valid=false;
				echo json_encode(array(
				'valid' => $valid,
				));
				}
			}
			else{
				$valid=false;
				echo json_encode(array(
				'valid' => $valid,
				));
			}
	}
	function resend_code_to_newpassword($c_id,$val,$rand){
		
		$temp=array();
		$rand="1234";
		if(is_numeric($val))
		{	 
			$sql="update franchise_customer set random_no='$rand' where mobile='$val' and id='$c_id'";
			
		}else
		{
			$sql="update franchise_customer set random_no='$rand' where email='$val' and id='$c_id'";
		}
			$result=$this->db->query($sql);

			
			if($result){
				
					$valid=true;
					$temp['valid']=$valid;

					echo json_encode($temp);
			}
			else{
				$valid=false;
				echo json_encode(array(
				'valid' => $valid,
				));
			}
	}
	function activate_new_user($c_id){
		 
		$sql="update franchise_customer set active=1,mobile_verification=1 where id='$c_id'";
		 
		$result=$this->db->query($sql);
		return $result;
		
	}
	 
	 
	function customer_verify_user($val){
		if(is_numeric($val)){	 
			 $sql="select block_user,active,deactivated_by,mobile_verification from franchise_customer where mobile='$val'";
		}else{
			 $sql="select block_user,active,deactivated_by,mobile_verification from franchise_customer where email='$val'";
		}
	 	$valid = true;
		
		$query=$this->db->query($sql);
		if(!empty($query->row())){
			$user_blocked=$query->row()->block_user;
			$active=$query->row()->active;
			$deactivated_by=$query->row()->deactivated_by;
			$mobile_verification=$query->row()->mobile_verification;
			$valid=false;
			
			
			echo json_encode(array(
				'valid' => $valid,
				'is_user_blocked'=>$user_blocked,
				'active'=>$active,
				'deactivated_by'=>$deactivated_by,
				'mobile_verification'=>$mobile_verification
				));
		}
		else{
			$valid=true;
			echo json_encode(array(
					'valid' => $valid
					));
		}
	}
	 
	function customer_login($un,$pw,$type){


		$customer_id="";
		$mobile="";
		 $password_login=md5($pw);
		 
		 				
						
		 if (is_numeric($un)){
		 $sql_exists_admin="select count(*) as count from franchise_customer where mobile='$un' and password='$password_login' and block_user=0";
		 }
		 if(!is_numeric($un)){
			$sql_exists_admin="select count(*) as count from franchise_customer where email='$un' and password='$password_login' and block_user=0";
		 }
		 $query=$this->db->query($sql_exists_admin);
		 
		 if($query->row()->count>0){
			 
			
			if(is_numeric($un)){	 
				$sql_select_admin="select * from franchise_customer where mobile='$un' and password='$password_login' and  approval_status!='1'";
			}else{
				$sql_select_admin="select * from franchise_customer where email='$un' and password='$password_login' and approval_status!='1'";
			}
			
			//echo $sql_select_admin.'||';

			$query_res=$this->db->query($sql_select_admin);
			$values=$query_res->result();


			if($query_res->num_rows()>0){
				$valid="approve_false";
			}else{

			
			
			if($type=="re_activate"){
				if (is_numeric($un)){
				 $sql_update_customer="update franchise_customer set active=1 where mobile='$un' and password='$password_login' and block_user=0";
				}
				if(!is_numeric($un)){
					$sql_update_customer="update franchise_customer set active=1 where email='$un' and password='$password_login' and block_user=0";
				}
				$query_update=$this->db->query($sql_update_customer);
				
			}
			if(is_numeric($un)){	 
			 $sql_select_admin="select * from franchise_customer where mobile='$un' and password='$password_login' and active=1 and mobile_verification=1";//and mobile_verification=1 //when comes from becomeourfranchise page, no verication
			}else{
				$sql_select_admin="select * from franchise_customer where email='$un' and password='$password_login' and active=1 and mobile_verification=1";//and mobile_verification=1 //when comes from becomeourfranchise page, no verication
			}
			$query_res=$this->db->query($sql_select_admin);
			$values=$query_res->result();

			if($query_res->num_rows()>0){
				/*echo "session";
				echo "<br>";
				echo $sql_select_admin;
				echo '<br>';*/
				$this->session->set_userdata("user_type","franchise_customer");
				$this->session->set_userdata("logged_in",true);
				$this->session->set_userdata("customer_id", $values[0]->id);//normal user 
				$this->session->set_userdata("franchise_customer_id", $values[0]->id);//franchise user
				$this->session->set_userdata("customer_email",$values[0]->email);
				$this->session->set_userdata("customer_mobile",$values[0]->mobile);
				$this->session->set_userdata("customer_name",$values[0]->name);
				$this->session->set_userdata("customer_gender",$values[0]->gender);
				$this->session->set_userdata("customer_image",$values[0]->image);
				$this->session->set_userdata("user_category",'franchise_customer');
			
				$current_login_customer = date("D j M Y - h:i a");
				$sql_current_login_customer="UPDATE  `franchise_customer` SET `current_login` = '$current_login_customer' WHERE `id` = '".$this->session->userdata('customer_id')."'";
				//$resu=mysql_query($sql_current_login_customer);
				$resu=$this->db->query($sql_current_login_customer);
				if($resu){
					$valid=true;
				}else{
					$valid=false;
				}
			}
			else{
				$valid="verifyfalse";
				
					if(is_numeric($un)){	 
					 $sql_select_admin="select * from franchise_customer where mobile='$un' and password='$password_login' and  mobile_verification=0";
					}else{
						$sql_select_admin="select * from franchise_customer where email='$un' and password='$password_login' and mobile_verification=0";
					}
					$query_res=$this->db->query($sql_select_admin);
					$values=$query_res->result();
					if($query_res->num_rows()>0){
									$mobile=$values[0]->mobile;
									$customer_id=$values[0]->id;
									$rand=mt_rand(1000,10000);
									
										$mobile_stuff_arr=mobile_stuff();
										$authKey = $mobile_stuff_arr["authKey_smstemplate"];
										$mobileNumber = "+91".$mobile;
										$senderId = $mobile_stuff_arr["senderId_smstemplate"];
										
										

										$message = urlencode("Your verification code is ".$rand);
										$route = "4";
										$postData = array(
											'authkey' => $authKey,
											'mobiles' => $mobileNumber,
											'message' => $message,
											'sender' => $senderId,
											'route' => $route
										);

										$url="https://control.msg91.com/api/sendhttp.php";
										$ch = curl_init();
										curl_setopt_array($ch, array(
											CURLOPT_URL => $url,
											CURLOPT_RETURNTRANSFER => true,
											CURLOPT_POST => true,
											CURLOPT_POSTFIELDS => $postData
										));
										curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
										curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

										$output = curl_exec($ch);
							
										if(curl_errno($ch))
										{
											echo 'error:' . curl_error($ch);
										}else{
											//no error - code has been sent
											$rand="1234";
											$sql_update="update franchise_customer set random_no='$rand' where id='$customer_id'";
											$query_update=$this->db->query($sql_update);
										}

										curl_close($ch);
					}
					else{
						
						if(is_numeric($un)){	 
							 $sql_select_admin="select * from franchise_customer where mobile='$un' and password='$password_login' and  active=0";
						}else{
							$sql_select_admin="select * from franchise_customer where email='$un' and password='$password_login' and active=0";
						}
						$query_res=$this->db->query($sql_select_admin);
						$values=$query_res->result();
						if($query_res->num_rows()>0){
							$valid="active_false";
						}
					}
					
					
			}

		}//for admin approve

		 }else{
			 $valid=false;
		 }
		
		 if($valid===true){ 
			//$userID=get_cookie('userID');
			$customer_id=$this->session->userdata("franchise_customer_id");
			//$user_type=$customer_id;
			//$log_activity=array("login"=>time());
			//$log_activity=json_encode($log_activity);
			
			//$today=date('Y-m-d');
			
			//$sql="update visitor_data set log_actitivity=concat(log_actitivity,',','$log_activity') where visitor_id='$userID' and date(timestamp)='$today' and user_type='$user_type'";
			//$res=$this->db->query($sql);

		}
		
		echo json_encode(array(
			'valid' => $valid,
			'login_type'=>$type,
			'customer_id'=>$customer_id,
			'mobile'=>$mobile
			));
	}
	function reset_password($c_id,$pw){

		$passw=md5($pw);
		$sql="update franchise_customer set password='$passw' where id='$c_id'";

		$result=$this->db->query($sql);
		
		if($result)
		{
			$valid=true;
			echo json_encode(array(
			'data' => $valid,
			));
		}else{
			$valid=false;
			echo json_encode(array(
			'data' => $valid,
			));
		}
	}

	public function update_visitor_logout($data="",$userID="",$user_type=""){
		//$userID=get_cookie('userID');
		$customer_id=$this->session->userdata("franchise_customer_id");
		//$user_type=$customer_id;
		//$log_activity=array("logout"=>time());
		//$log_activity=json_encode($log_activity);
		
		//$today=date('Y-m-d');
		
		//$sql="update visitor_data set log_actitivity=concat(log_actitivity,',','$log_activity') where visitor_id='$userID' and date(timestamp)='$today' and user_type='$user_type'";
		//$res=$this->db->query($sql);
		
		$last_login = date("D j M Y - h:i a");
		$last_login="UPDATE  `franchise_customer` SET `last_login` = '$last_login' WHERE `id` = '$customer_id'";
		//$resu=mysql_query($sql_current_login_customer);
		$resu=$this->db->query($last_login);
		
		//return $res;
		return true;
	}
	public function get_hot_searched_key_words(){
		$sql="select searched_keyword,number_of_hits from searched_keyword order by number_of_hits desc limit 0,4";
		$res=$this->db->query($sql);
		return $res->result();
	}
		 
	public function get_customer_profile_pic($customer_id){
		$sql="select image from franchise_customer where id='$customer_id'";
		$query=$this->db->query($sql);
		return $query->row_array()["image"];
	}
	public function get_customer_info_by_cid($c_id){
		$sql="select * from franchise_customer where id='$c_id'";
		$query=$this->db->query($sql);
		return $query->row();
	}
	public function get_franchise_all_projects($customer_id){
		$sql="select * from franchise_projects where franchise_customer_id='$customer_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	public function get_franchise_projects($customer_id,$project_id){
		$sql="select * from franchise_projects where franchise_customer_id='$customer_id' and project_id='$project_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row_array();
		}
		else{
			return false;
		}
	}
	public function update_project_details_action($customer_id,$project_id,$project_name,$client_name,$client_email,$client_mobile,$project_location,$project_type,$project_deadline,$project_requirements,$project_tentative_budget,$project_status,$project_file){


		$project_deadline=date('yy-m-d',strtotime($project_deadline));

		$sql="update franchise_projects set
		franchise_customer_id='$customer_id', 
		project_name='$project_name',
		client_name='$client_name',
		client_email='$client_email',
		client_mobile='$client_mobile',
		project_location='$project_location',
		project_deadline='$project_deadline',
		project_type='$project_type',
		project_requirements='$project_requirements',
		project_tentative_budget='$project_tentative_budget',
		project_file='$project_file',
		project_status='$project_status' where project_id='$project_id'";

		$result=$this->db->query($sql);

		//echo $result;
		//echo 'ff';
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	public function create_project_details_action($customer_id,$project_name,$client_name,$client_email,$client_mobile,$project_location,$project_type,$project_deadline,$project_requirements,$project_tentative_budget,$project_status,$project_file){


		$project_deadline=date('Y-m-d',strtotime($project_deadline));

		$project_last_updated_on=date('Y-m-d H:i:s');
		$sql="insert into franchise_projects set
		franchise_customer_id='$customer_id', 
		project_name='$project_name',
		client_name='$client_name',
		client_email='$client_email',
		client_mobile='$client_mobile',
		project_location='$project_location',
		project_deadline='$project_deadline',
		project_type='$project_type',
		project_requirements='$project_requirements',
		project_tentative_budget='$project_tentative_budget',
		project_last_updated_on='$project_last_updated_on',
		project_file='$project_file',
		project_status='$project_status'";

		$result=$this->db->query($sql);
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}


}
?>