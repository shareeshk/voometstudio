<?php
	class Dialyorders_cron_model extends CI_Model{
		function get_dialyorders(){
			$sql = "select orders_status.type_of_order,orders_status.id as orders_status_id,history_orders.*,history_orders.image,
				history_orders.sku_id,
				history_orders.prev_order_item_id,
				products.product_name,
				history_orders.timestamp,
				history_orders.order_id,
				history_orders.payment_type,
				orders_status.order_item_id,
				DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') AS 'order_status_timestamp',
				history_orders.quantity,
				history_orders.product_price,
				history_orders.subtotal,
				history_orders.grandtotal,
				shipping_address.customer_name,
				shipping_address.address1,
				shipping_address.address2,
				shipping_address.country,
				shipping_address.state,
				shipping_address.city,
				shipping_address.pincode,
				shipping_address.mobile,invoices_offers.* from history_orders,invoices_offers,orders_status,products,inventory,shipping_address
				where history_orders.customer_id=orders_status.customer_id and 
				history_orders.order_item_id=orders_status.order_item_id and
				history_orders.customer_id=shipping_address.customer_id and
				history_orders.shipping_address_id=shipping_address.shipping_address_id and
				history_orders.product_id=products.product_id and
				history_orders.inventory_id=inventory.id and	
				orders_status.customer_id=shipping_address.customer_id and invoices_offers.order_id=history_orders.order_id and history_orders.timestamp between DATE_ADD(now(), interval -24 hour) and now()";
				$query=$this->db->query($sql);
				return $query->result();
		}
		
		function get_inventory_attr_details($inventory_id){
			$sql="select attribute_1,attribute_1_value,attribute_2,attribute_2_value,attribute_3,attribute_3_value,attribute_4,attribute_4_value from inventory where id='$inventory_id'";
			$query=$this->db->query($sql);
			$get_inventory_details_row=$query->row_array();
			
			$attribute_1=$get_inventory_details_row["attribute_1"];
			$attribute_1_value=$get_inventory_details_row["attribute_1_value"];
			
			$attribute_2=$get_inventory_details_row["attribute_2"];
			$attribute_2_value=$get_inventory_details_row["attribute_2_value"];
			
			$attribute_3=$get_inventory_details_row["attribute_3"];
			$attribute_3_value=$get_inventory_details_row["attribute_3_value"];
			
			$attribute_4=$get_inventory_details_row["attribute_4"];
			$attribute_4_value=$get_inventory_details_row["attribute_4_value"];
			
			$get_inventory_attr_details_str="";
			if($attribute_1!=""){
				$get_inventory_attr_details_str.=$attribute_1." : ".$attribute_1_value."<br>";
			}
			if($attribute_2!=""){
				$get_inventory_attr_details_str.=$attribute_2." : ".$attribute_2_value."<br>";
			}
			if($attribute_3!=""){
				$get_inventory_attr_details_str.=$attribute_3." : ".$attribute_3_value."<br>";
			}
			if($attribute_4!=""){
				$get_inventory_attr_details_str.=$attribute_4." : ".$attribute_4_value."<br>";
			}
			return $get_inventory_attr_details_str;
		}
		
		public function check_date_sent(){
			$today=date("Y-m-d");
			$sql="select * from dialy_order_report_status where date_sent='$today'";
			$query=$this->db->query($sql);
			$row_arr=$query->row_array();
			if(!empty($row_arr)){
				return "yes";
			}
			else{
				return "no";
			}
		}
		public function update_date_sent(){
			$today=date("Y-m-d");
			$sql="insert into dialy_order_report_status set date_sent='$today'";
			$query=$this->db->query($sql);
		}
	}
?>