<?php

class Customer_account extends CI_Model{
	
	public function get_all_orders($customer_id){
		$sql="select * from orders_status where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}else{
			return false;
		}
	}
	
	public function get_all_distinct_orders_id($customer_id){
		$order_id=array();
		$sql="select distinct order_id from active_orders where customer_id='$customer_id' order by timestamp desc";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			foreach($result->result() as $res){
				$order_id[]=$res->order_id;
			}
		}
		$sql="select distinct order_id from cancelled_orders where customer_id='$customer_id' order by timestamp desc";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			foreach($result->result() as $res){
				$order_id[]=$res->order_id;
			}
		}
		$sql="select distinct order_id from completed_orders where customer_id='$customer_id' order by timestamp desc";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			foreach($result->result() as $res){
				$order_id[]=$res->order_id;
			}
		}
		
		$sql="select distinct order_id from returned_orders where customer_id='$customer_id' order by timestamp desc";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			foreach($result->result() as $res){
				$order_id[]=$res->order_id;
			}
		}
		
		return $order_id;
	}

	public function all_orders($order_id,$status=''){
		//$order_item_id_in="'".implode("','",$order_item_id_arr)."'";

		if($status==''){
			$sql="select order_item_id,product_id,vendor_id,grandtotal,'active' as status from active_orders where  order_id='$order_id' and prev_order_item_id='' 
			union select order_item_id,product_id,vendor_id,grandtotal,'cancelled' as status from cancelled_orders where  order_id='$order_id' and prev_order_item_id='' 
			union select order_item_id,product_id,vendor_id,grandtotal,'completed' as status from completed_orders where order_id='$order_id'  
			union select order_item_id,product_id,vendor_id,grandtotal,'returned' as status from  returned_orders where  order_id='$order_id' ";
		}elseif($status=='active'){
			$sql="select order_item_id,product_id,vendor_id,grandtotal,'active' as status from active_orders where  order_id='$order_id' and prev_order_item_id=''";
		}
		elseif($status=='cancelled'){
			$sql="select order_item_id,product_id,vendor_id,grandtotal,'cancelled' as status from cancelled_orders where  order_id='$order_id' and prev_order_item_id='' ";
		}
		elseif($status=='completed'){
			$sql="select order_item_id,product_id,vendor_id,grandtotal,'completed' as status from completed_orders where order_id='$order_id' ";
		}
		elseif($status=='returned'){
			$sql="select order_item_id,product_id,vendor_id,grandtotal,'returned' as status from  returned_orders where  order_id='$order_id'";
		}else{
			$sql="select order_item_id,product_id,vendor_id,grandtotal,'active' as status from active_orders where  order_id='$order_id' and prev_order_item_id='' 
			union select order_item_id,product_id,vendor_id,grandtotal,'cancelled' as status from cancelled_orders where  order_id='$order_id' and prev_order_item_id='' 
			union select order_item_id,product_id,vendor_id,grandtotal,'completed' as status from completed_orders where order_id='$order_id'  
			union select order_item_id,product_id,vendor_id,grandtotal,'returned' as status from  returned_orders where  order_id='$order_id' ";
		}

		//echo $sql;
		
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
		
	}
	public function get_all_order_items($id,$order_id){
		$sql="select *,'active' as status from active_orders where order_id='$order_id' and order_item_id='$id'";
		$result=$this->db->query($sql);
		
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			$sql="select *,'cancelled' as status from cancelled_orders where order_id='$order_id' and  order_item_id='$id'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->result_array();
			}
			else{
				$sql="select *,'completed' as status from completed_orders where order_id='$order_id' and  order_item_id='$id'";
				$result=$this->db->query($sql);
				if($result->num_rows()>0){
					return $result->result_array();
				}
				else{
					$sql="select *,'replaced' as status from replaced_orders where order_id='$order_id' and  order_item_id='$id'";
					$result=$this->db->query($sql);
					if($result->num_rows()>0){
						return $result->result_array();
					}
					else{
						$sql="select *,'returned' as status from returned_orders where order_id='$order_id' and  order_item_id='$id'";
						$result=$this->db->query($sql);
						if($result->num_rows()>0){
							return $result->result_array();
						}
						else{
							$sql="select *,'replaced' as status from replaced_orders where order_id='$order_id' and  order_item_id='$id'";
							$result=$this->db->query($sql);
							if($result->num_rows()>0){
								return $result->result_array();
							}
							else{
								return false;
							}
						}
					}
				}
			}
		}
		//echo $sql;exit;
	}
	
	
	
	public function get_order_item_discount_price($id){
		$sql="select discount_price from active_orders where order_item_id='$id' union select discount_price from cancelled_orders where order_item_id='$id' union select discount_price from completed_orders where order_item_id='$id' union select discount_price from returned_orders where order_item_id='$id'";
		$result=$this->db->query($sql);
		return $result->row()->discount_price;
	}
	
		/*order details functions*/
	
	public function get_all_order_items_in_order_id($order_id){
		$order_item_id=array();
		
		$sql="select order_item_id from history_orders where order_id='$order_id' and prev_order_item_id=''";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			$order_item_id[]=$result->result_array();
			
			/*$arr=$result->result_array();
			foreach($arr as $arr_val){
				$order_item_id[]=$arr_val;
				$order_item_id_v=$arr_val['order_item_id'];
				$sql2="select order_item_id from history_orders where order_id='$order_id' and prev_order_item_id='$order_item_id_v'";
				$result2=$this->db->query($sql2);
				if($result2->num_rows()>0){
					$order_item_id[]=$result2->row_array();
				}
				
			}*/
		}
		
		//"select parent.order_item_id from history_orders child left join history_orders parent on child.parent = parent.order_item_id";
		
		/*$sql="select order_item_id,'replaced' as status from replaced_orders where order_id='$order_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			$order_item_id[]=$result->result_array();
		}
		$sql="select order_item_id,'active' as status,prev_order_item_id from active_orders where order_id='$order_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			$order_item_id[]=$result->result_array();
		}
		$sql="select order_item_id,'cancelled' as status from cancelled_orders where order_id='$order_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			$order_item_id[]=$result->result_array();
		}
		$sql="select order_item_id,'completed' as status from completed_orders where order_id='$order_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			$order_item_id[]=$result->result_array();
		}
		$sql="select order_item_id,'returned' as status from returned_orders where order_id='$order_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			$order_item_id[]=$result->result_array();
		}
		*/
		return $order_item_id;
	}
	public function get_order_items_in_order_id($order_id,$order_item_id){
		$order_item=array();
		
		$sql="select order_item_id,'replaced' as status from replaced_orders where order_id='$order_id' and order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			$order_item[]=$result->row_array();
		}
		$sql="select order_item_id,'active' as status from active_orders where order_id='$order_id' and order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			$order_item[]=$result->row_array();
		}
		$sql="select order_item_id,'cancelled' as status from cancelled_orders where order_id='$order_id' and order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			$order_item[]=$result->row_array();
		}
		$sql="select order_item_id,'completed' as status from completed_orders where order_id='$order_id' and order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			$order_item[]=$result->row_array();
		}
		$sql="select order_item_id,'returned' as status from returned_orders where order_id='$order_id' and order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			$order_item[]=$result->row_array();
		}
		
		return $order_item;
	}
	
public function get_all_order_invoice_offers($id){
    
    $sql="select * from invoices_offers where order_id='$id'";
    $result=$this->db->query($sql);
    return $result->result_array();
}
public function get_all_order_invoice_offers_obj($id){
    
    $sql="select * from invoices_offers where order_id='$id'";
    $result=$this->db->query($sql);
    return $result->row();
}
	public function get_order_placed_date($id){
		$sql="select timestamp from active_orders where order_id='$id' union select timestamp from cancelled_orders where order_id='$id' union select timestamp from completed_orders where order_id='$id' union select timestamp from returned_orders where order_id='$id'";
		$result=$this->db->query($sql);
		return $result->row()->timestamp;
	}
	public function get_order_grand_total($id){
		//$sql="select grandtotal from active_orders where order_id='$id' union select grandtotal from cancelled_orders where order_id='$id' union select grandtotal from completed_orders where order_id='$id' union select grandtotal from returned_orders where order_id='$id'";
		$sql="select grandtotal from active_orders where order_id='$id' union select grandtotal from cancelled_orders where order_id='$id' union select grandtotal from completed_orders where order_id='$id'";
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	public function get_order_grand_total_from_active($id){
		$sql="select grandtotal from active_orders where order_id='$id'";
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	public function get_discount_price($order_id){
		$sql="select discount_price from active_orders where order_id='$order_id' union select discount_price from cancelled_orders where order_id='$order_id' union select discount_price from completed_orders where order_id='$order_id' union select discount_price from returned_orders where order_id='$order_id'";
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	public function get_product_name($product_id){
		$sql="select product_name from products where product_id='$product_id'";
		$result=$this->db->query($sql);
		return (!empty($result->row())) ? $result->row()->product_name : '';
	}
	public function get_order_item_status_summary($id){
		 $sql="select 
		order_placed,
		order_placed_timestamp,
		order_confirmed,
		order_confirmed_timestamp,	
		order_packed,
		order_packed_timestamp,
		order_shipped,
		order_shipped_timestamp,
		order_delivered,
		order_delivered_date,
		order_delivered_time,
        order_delivered_timestamp,
		order_cancelled,
		order_cancelled_timestamp,
		order_delayed,
		order_delayed_timestamp,
		return_order_id,
		order_return_pickup,
		order_replacement_pickup,
		timestamp
                from orders_status where order_item_id='$id'";
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	public function get_shipping_address_order_item_id($id){
		$sql="select shipping_address_id from active_orders where order_id='$id' union select shipping_address_id from cancelled_orders where order_id='$id' union select shipping_address_id from completed_orders where order_id='$id' union select shipping_address_id from returned_orders where order_id='$id'";
		$result=$this->db->query($sql);
		return $result->row()->shipping_address_id;
	}
	public function get_shipping_address($shipping_address_id){
		
		$sql="select * from shipping_address where shipping_address_id='$shipping_address_id'";
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	public function get_total_num_of_order_items_in_order_id($id){
		$sql="select active_orders.*,history_orders.* from  active_orders,history_orders where active_orders.order_id='$id' and history_orders.order_id='$id'";
		
		$result=$this->db->query($sql);
		return $result->num_rows();
	}
	public function get_order_payment_method($id){
		
		$sql="select payment_type from active_orders where order_id='$id' union select payment_type from cancelled_orders where order_id='$id' union select payment_type from completed_orders where order_id='$id' union select payment_type from returned_orders where order_id='$id'";	
		
		$result=$this->db->query($sql);
		return $result->row()->payment_type;
	}
	public function get_expected_order_item_expected_delivery($id){
		$sql="select expected_delivery_date from active_orders where order_item_id='$id' union select expected_delivery_date from cancelled_orders where order_item_id='$id' union select expected_delivery_date from completed_orders where order_item_id='$id' union select expected_delivery_date from returned_orders where order_item_id='$id'";
		
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->expected_delivery_date;
		}else{
			return false;
		}
		
	}
	public function get_order_id_from_item_order_id($id){
		$sql="select order_id from active_orders where order_item_id='$id' union select order_id from cancelled_orders where order_item_id='$id' union select order_id from completed_orders where order_item_id='$id' union select order_id from returned_orders where order_item_id='$id'";
		$result=$this->db->query($sql);
		if(isset($result->row()->order_id)){
			return $result->row()->order_id;
		}else{
			return '';
		}
		
	}
	public function get_order_item_grand_total($id){
		$sql="select grandtotal from active_orders where order_item_id='$id' union select grandtotal from cancelled_orders where order_item_id='$id' union select grandtotal from completed_orders where order_item_id='$id' union select grandtotal from returned_orders where order_item_id='$id'";
		$result=$this->db->query($sql);
		return $result->row()->grandtotal;
	}
	public function get_order_item_data($id){
	 	$sql="select quantity,image,product_id,inventory_id,order_item_id,product_price,grandtotal,shipping_charge,customer_id,logistics_id, 	logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id,delivery_mode,order_id from active_orders where order_item_id='$id' union select quantity,image,product_id,inventory_id,order_item_id,product_price,grandtotal,shipping_charge,customer_id,logistics_id, 	logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id,delivery_mode,order_id from cancelled_orders where order_item_id='$id' union select quantity,image,product_id,inventory_id,order_item_id,product_price,grandtotal,shipping_charge,customer_id,logistics_id, 	logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id,delivery_mode,order_id from completed_orders where order_item_id='$id' union select quantity,image,product_id,inventory_id,order_item_id,product_price,grandtotal,shipping_charge,customer_id,logistics_id, 	logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id,delivery_mode,order_id from returned_orders where order_item_id='$id'";
	
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	
	public function get_order_details_return_data($order_item_id){
		$sql="select return_order_id,timestamp from replacement_desired where order_item_id='$order_item_id' union select return_order_id,timestamp from returns_desired where order_item_id='$order_item_id' union select return_order_id,timestamp from  return_stock_notification where order_item_id='$order_item_id'";
		//$sql="select return_order_id,timestamp from replacement_desired where order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function get_data_from_replacement_desired_return_order_id($return_order_id){
		$sql="select * from replacement_desired where return_order_id='$return_order_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	public function get_data_from_returns_desired_return_order_id($return_order_id){
		$sql="select * from returns_desired where return_order_id='$return_order_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	public function get_data_from_return_stock_notification_return_order_id($return_order_id){
		$sql="select * from  return_stock_notification where return_order_id='$return_order_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	public function get_replacement_item_data($replacement_with_inventory_id){
		$sql="select * from inventory where id='$replacement_with_inventory_id' ";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	public function get_reason_for_return_order_id($return_order_id){
		$sql="select reason_return_id,sub_issue_id,return_reason_comments from replacement_desired where return_order_id='$return_order_id' union select reason_return_id,sub_issue_id,return_reason_comments from returns_desired where return_order_id='$return_order_id' union select reason_return_id,sub_issue_id,return_reason_comments from  return_stock_notification where return_order_id='$return_order_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function get_primary_return_reason($reason_return_id){
		$sql="select reason_name from reason_return where reason_id='$reason_return_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->reason_name;
		}
		else{
			return false;
		}
	}
	
	public function get_primary_return_reason_admin($reason_return_id){
		$sql="select return_options from admin_return_reason where id='$reason_return_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->return_options;
		}
		else{
			return false;
		}
	}
	
	public function get_sub_return_reason($sub_issue_id){
		$sql="select sub_issue_name from sub_issue_return where sub_issue_id='$sub_issue_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->sub_issue_name;
		}
		else{
			return false;
		}		
	}
	
	public function get_indivisual_price_from_orders($order_item_id){
		$sql="select product_price from active_orders where order_item_id='$order_item_id' union select product_price from cancelled_orders where order_item_id='$order_item_id' union select product_price from completed_orders where order_item_id='$order_item_id' union select product_price from returned_orders where order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->product_price;
		}
		else{
			return false;
		}
	}
	
	public function get_refund_medium_from_return_desired_or_rep_desired($return_order_id){
		$sql="select refund_method,refund_bank_id from replacement_desired where return_order_id='$return_order_id' union select refund_method,refund_bank_id from returns_desired where return_order_id='$return_order_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function get_payment_method_from_return_rep_desired($return_order_id){
		$sql="select balance_amount_paid_by from replacement_desired where return_order_id='$return_order_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->balance_amount_paid_by;
		}
		else{
			return false;
		}
	}
	
	public function get_bank_details($id){
		$sql="select * from delivered_refund_bank_details where id='$id'";
		$result=$this->db->query($sql);
		
		if($result->num_rows()>0){
			//print_r($result->result_array());exit;
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	/*order details functions ends*/
	/* cancels related functions start */
	public function get_order_cancel_reasons(){
		$sql="select * from reason_cancel";
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	
	
	public function add_cancel_orders_to_cancels_table($order_item_id,$cancel_reason,$cancel_reason_comment,$user_type){
		$cancel_reason_comment=$this->db->escape_str($cancel_reason_comment);
		$sql="insert into cancels (order_item_id,reason_cancel_id,cancel_reason_comments,cancelled_by) values('$order_item_id','$cancel_reason','$cancel_reason_comment','$user_type')";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function get_cancel_id_of_this_transaction($order_item_id){
		$sql="select cancel_id from cancels where order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		return $result->row()->cancel_id;
	}
	
	public function get_all_order_data_from_order_item($id){
		$sql="select active_orders.*,invoices_offers.* from active_orders,invoices_offers where active_orders.order_item_id='$id' and active_orders.order_id=invoices_offers.order_id";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
	}
	
	
	
	
	public function active_order_add_to_cancel_orders($purchased_price,$order_item_id,$customer_id,$order_id,$shipping_address_id,$inventory_id,$product_id,$sku_id,$tax_percent,$product_price,$shipping_charge,$subtotal,$wallet_status,$wallet_amount,$grandtotal,$quantity,$timestamp,$image,$random_number,$invoice_email,$invoice_number,$logistics_name,$logistics_weblink,$tracking_number,$vendor_id,$return_period,$delivery_mode,$payment_type,$payment_status,$logistics_id,$logistics_delivery_mode_id,$logistics_territory_id,$logistics_parcel_category_id,$promotion_available,$promotion_residual,$promotion_discount,$promotion_cashback,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$default_discount,$promotion_item_multiplier,$promotion_item,$promotion_item_num,$promotion_clubed_with_default_discount,$promotion_id_selected,$promotion_type_selected,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_without_promotion,$total_price_of_product_with_promotion,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$order_item_invoice_discount_value,$prev_order_item_id,$notify_cancel,$razorpayOrderId,$razorpayPaymentId,$ord_max_selling_price,$ord_selling_discount,$ord_tax_percent_price,$ord_taxable_price,$ord_sku_name,$ord_coupon_code,$ord_coupon_name,$ord_coupon_type,$ord_coupon_value,$ord_coupon_reduce_price,$ord_coupon_status,$ord_addon_products_status,$ord_addon_products,$ord_addon_inventories,$ord_addon_total_price){
		
		$cancelled_timestamp=date("Y-m-d H:i:s");
		$sql="insert into cancelled_orders (purchased_price,order_item_id,customer_id,order_id,shipping_address_id,inventory_id,product_id,sku_id,tax_percent,product_price,shipping_charge,subtotal,wallet_status,wallet_amount,grandtotal,quantity,timestamp,image,random_number,invoice_email,invoice_number,logistics_name,logistics_weblink,tracking_number,vendor_id,return_period,delivery_mode,prev_order_item_id,payment_type,payment_status,logistics_id,logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id,promotion_available,promotion_residual,promotion_discount,promotion_cashback,promotion_surprise_gift,promotion_surprise_gift_type,promotion_surprise_gift_skus,promotion_surprise_gift_skus_nums,default_discount,promotion_item_multiplier,promotion_item,promotion_item_num,promotion_clubed_with_default_discount,promotion_id_selected,promotion_type_selected,promotion_minimum_quantity,promotion_quote,promotion_default_discount_promo,individual_price_of_product_with_promotion,individual_price_of_product_without_promotion,total_price_of_product_without_promotion,total_price_of_product_with_promotion,quantity_without_promotion,quantity_with_promotion,cash_back_value,order_item_invoice_discount_value,notify_cancel,razorpayOrderId,razorpayPaymentId,cancelled_timestamp,ordered_timestamp,ord_max_selling_price,ord_selling_discount,ord_tax_percent_price,ord_taxable_price,ord_sku_name,ord_coupon_code,ord_coupon_name,ord_coupon_type,ord_coupon_value,ord_coupon_reduce_price,ord_coupon_status,ord_addon_products_status,ord_addon_products,ord_addon_inventories,ord_addon_total_price) values('$purchased_price','$order_item_id','$customer_id','$order_id','$shipping_address_id','$inventory_id','$product_id','$sku_id','$tax_percent','$product_price','$shipping_charge','$subtotal','$wallet_status','$wallet_amount','$grandtotal','$quantity','$timestamp','$image','$random_number','$invoice_email','$invoice_number','$logistics_name','$logistics_weblink','$tracking_number','$vendor_id','$return_period','$delivery_mode','$prev_order_item_id','$payment_type','$payment_status','$logistics_id','$logistics_delivery_mode_id','$logistics_territory_id','$logistics_parcel_category_id','$promotion_available','$promotion_residual','$promotion_discount','$promotion_cashback','$promotion_surprise_gift','$promotion_surprise_gift_type','$promotion_surprise_gift_skus','$promotion_surprise_gift_skus_nums','$default_discount','$promotion_item_multiplier','$promotion_item','$promotion_item_num','$promotion_clubed_with_default_discount','$promotion_id_selected','$promotion_type_selected','$promotion_minimum_quantity','$promotion_quote','$promotion_default_discount_promo','$individual_price_of_product_with_promotion','$individual_price_of_product_without_promotion','$total_price_of_product_without_promotion','$total_price_of_product_with_promotion','$quantity_without_promotion','$quantity_with_promotion','$cash_back_value','$order_item_invoice_discount_value','$notify_cancel','$razorpayOrderId','$razorpayPaymentId','$cancelled_timestamp','$timestamp','$ord_max_selling_price','$ord_selling_discount','$ord_tax_percent_price','$ord_taxable_price','$ord_sku_name','$ord_coupon_code','$ord_coupon_name','$ord_coupon_type','$ord_coupon_value','$ord_coupon_reduce_price','$ord_coupon_status','$ord_addon_products_status','$ord_addon_products','$ord_addon_inventories','$ord_addon_total_price')";
		$result=$this->db->query($sql);
		//for both invoice offer --update -customer-response as reject
		//item promotion --update -customer-response as reject
		
		
		if($promotion_available==1){
			
			//reject that customer response is reject
			
		}

		if($this->db->affected_rows() > 0){
			
			return true;
		}
		else{
			return false;
		}
	}
	
	/*public function add_this_transaction_to_refund_table($return_id,$cancel_id,$refund_method,$product_id,$sku_id,$product_price,$grandtotal,$quantity,$refund_bank_id,$order_item_id){
		$sql="insert into refund (return_id,order_item_id,cancel_id,refund_type,product_id,sku_id,product_price,grand_total,quantity,refund_bank_id) values('$return_id','$order_item_id','$cancel_id','$refund_method','$product_id','$sku_id','$product_price','$grandtotal','$quantity','$refund_bank_id') ";
		$result=$this->db->query($sql);
		if($result){
			return true;
		}
		else{
			return false;
		}
	}*/
	
	public function get_bank_account_details($bank_account_number,$customer_id){
		$sql="select id from refund_bank_details where account_number='$bank_account_number' and 	customer_id='$customer_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->id;
		}
		else{
			return false;
		}
		
	}
	
	public function get_bank_account_details_delivered($bank_account_number,$customer_id){
		$sql="select id from delivered_refund_bank_details where account_number='$bank_account_number' and 	customer_id='$customer_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->id;
		}
		else{
			return false;
		}
		
	}
	
	public function get_refund_id_of_this_transaction($cancel_id){
		$sql="select refund_id from refund where cancel_id='$cancel_id'";
		$result=$this->db->query($sql);
		return $result->row()->refund_id;
	}
	
	public function add_bank_details($bank_name,$bank_branch_name,$bank_city,$bank_ifsc_code,$bank_account_number,$bank_account_confirm_number,$account_holder_name,$bank_return_refund_phone_number,$customer_id){
		$sql="insert into refund_bank_details (bank_name,branch_name,city,ifsc_code,account_number,confirm_account_number,account_holder_name,mobile_number,customer_id) values('$bank_name','$bank_branch_name','$bank_city','$bank_ifsc_code','$bank_account_number','$bank_account_confirm_number','$account_holder_name','$bank_return_refund_phone_number','$customer_id')";
		$result=$this->db->query($sql);
		
		$sql="insert into delivered_refund_bank_details (bank_name,branch_name,city,ifsc_code,account_number,confirm_account_number,account_holder_name,mobile_number,customer_id) values('$bank_name','$bank_branch_name','$bank_city','$bank_ifsc_code','$bank_account_number','$bank_account_confirm_number','$account_holder_name','$bank_return_refund_phone_number','$customer_id')";
		$result=$this->db->query($sql);
		
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}


	public function update_order_item_id_to_cancel($order_item_id){
		$sql_get_status="select * from  orders_status where order_placed='0' and order_confirmed='0' and order_packed='0' and order_shipped='0' and order_delivered='0' and order_item_id='$order_item_id'";
		$result_get_status=$this->db->query($sql_get_status);
		
		if($result_get_status->num_rows()==0){
			$sql="update orders_status set order_cancelled='1',order_cancelled_timestamp=now() where order_item_id='$order_item_id'"; 
			$result=$this->db->query($sql);
			if($result){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			$sql="update orders_status set order_placed='1',order_placed_timestamp=now(),order_cancelled=1,order_cancelled_timestamp=now() where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			if($result){
				return true;
			}
			else{
				return false;
			}
		}
	}
	public function remove_data_from_active_order_table($order_item_id){
		$sql="delete from active_orders where order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

	/*return related functions*/
	public function get_product_id_by_order_item_id_and_order_id($id,$order_id){
		$sql="select product_id from history_orders where order_item_id='$id' and order_id='$order_id'";
		$result=$this->db->query($sql);
		$row_arr=$result->row_array();
		if(!empty($row_arr)){
			return $row_arr["product_id"];
		}
		else{
			return "";
		}
	}
	public function get_product_info_by_product_id($product_id){
		$sql="select * from products where product_id='$product_id'";
		$result=$this->db->query($sql);
		$row_arr=$result->row_array();
		if(!empty($row_arr)){
			return $row_arr;
		}
		else{
			return array();
		}
	}
	public function get_order_return_primary_reasons($primary_reasons_pcat_id,$primary_reasons_cat_id,$primary_reasons_subcat_id){
		if($primary_reasons_cat_id!=""){
			$sql="select * from reason_return where pcat_id='$primary_reasons_pcat_id' and cat_id='$primary_reasons_cat_id' and subcat_id='$primary_reasons_subcat_id'";
		}
		else{
			$sql="select * from reason_return";
		}
		//$sql="select * from reason_return";
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	public function get_order_return_issue_reason(){
		$sql="select * from sub_issue_return";
		$result=$this->db->query($sql);
		return $result->result_array();
	}

	public function show_available_replacement_options($product_id,$desired_quantity_replacement,$inventory_id,$free_inventory_available,$no_refund){
		
		if($free_inventory_available==1 || $no_refund==1){
			
			$sql="select stock,(stock-$desired_quantity_replacement) status_quantity,product_id,id,image,thumbnail,selling_price,attribute_1,attribute_1_value,attribute_2,attribute_2_value from inventory where product_id='$product_id'  and id='$inventory_id' and stock>$desired_quantity_replacement union select stock,(stock-$desired_quantity_replacement) status_quantity,product_id,id,image,thumbnail,selling_price,attribute_1,attribute_1_value,attribute_2,attribute_2_value from inventory where product_id='$product_id'  and id!='$inventory_id' and stock>$desired_quantity_replacement";
			
		}else{
			 $sql="select stock,(stock-$desired_quantity_replacement) status_quantity,product_id,id,image,thumbnail,selling_price,attribute_1,attribute_1_value,attribute_2,attribute_2_value from inventory where product_id='$product_id'  and id='$inventory_id' union select stock,(stock-$desired_quantity_replacement) status_quantity,product_id,id,image,thumbnail,selling_price,attribute_1,attribute_1_value,attribute_2,attribute_2_value from inventory where product_id='$product_id'  and id!='$inventory_id'";
		}
		
		
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	
	public function insert_into_returns_refund_desired_table($return_order_id,$order_item_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_refund,$refund_method,$refund_bank_id,$original_inventory_id,$order_item_invoice_discount_value_each){
		$cancel_reason_comment=$this->db->escape_str($cancel_reason_comment);
		$sql="insert into returns_desired (quantity_refund,return_order_id,order_item_id,reason_return_id,sub_issue_id,return_reason_comments,refund_method,refund_bank_id,order_return_decision_status,order_return_decision_customer_decision,original_inventory_id,order_item_invoice_discount_value_each) values('$desired_quantity_refund','$return_order_id','$order_item_id','$cancel_reason','$sub_cancel_reason','$cancel_reason_comment','$refund_method','$refund_bank_id','pending','pending','$original_inventory_id','$order_item_invoice_discount_value_each')";
		
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	public function insert_into_returns_refund_desired_table_with_promo($return_order_id,$order_item_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_refund,$refund_method_rep,$refund_bank_id,$promotion_available,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$promotion_item,$promotion_item_num,$promotion_cashback,$promotion_item_multiplier,$promotion_discount,$default_discount,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_with_promotion,$total_price_of_product_without_promotion,$original_inventory_id,$order_item_invoice_discount_value_each){
		
		$cancel_reason_comment=$this->db->escape_str($cancel_reason_comment);
		$sql="insert into returns_desired (quantity_refund,return_order_id,order_item_id,reason_return_id,sub_issue_id,return_reason_comments,refund_method,refund_bank_id,order_return_decision_status,order_return_decision_customer_decision,promotion_available,promotion_minimum_quantity,promotion_quote,promotion_default_discount_promo,promotion_surprise_gift,promotion_surprise_gift_type,promotion_surprise_gift_skus,promotion_surprise_gift_skus_nums,promotion_item,promotion_item_num,promotion_cashback,promotion_item_multiplier,promotion_discount,default_discount,quantity_without_promotion,quantity_with_promotion,cash_back_value,individual_price_of_product_with_promotion,individual_price_of_product_without_promotion,total_price_of_product_with_promotion,total_price_of_product_without_promotion,original_inventory_id,order_item_invoice_discount_value_each) values('$desired_quantity_refund','$return_order_id','$order_item_id','$cancel_reason','$sub_cancel_reason','$cancel_reason_comment','$refund_method_rep','$refund_bank_id','pending','pending','$promotion_available','$promotion_minimum_quantity','$promotion_quote','$promotion_default_discount_promo','$promotion_surprise_gift','$promotion_surprise_gift_type','$promotion_surprise_gift_skus','$promotion_surprise_gift_skus_nums','$promotion_item','$promotion_item_num','$promotion_cashback','$promotion_item_multiplier','$promotion_discount','$default_discount','$quantity_without_promotion','$quantity_with_promotion','$cash_back_value','$individual_price_of_product_with_promotion','$individual_price_of_product_without_promotion','$total_price_of_product_with_promotion','$total_price_of_product_without_promotion','$original_inventory_id','$order_item_invoice_discount_value_each')";
		$customer_id=$this->session->userdata("customer_id");
		if($promotion_cashback>0 && $promotion_cashback!=''){
			if($cash_back_value==0){
				$sql_cb_rej="update order_item_offers set customer_response='reject' where order_item_id='$order_item_id' and customer_id='$customer_id'";
				$result_cb_rej=$this->db->query($sql_cb_rej);
			}
		}
		
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function insert_into_returns_replacement_desired_table($return_order_id,$order_item_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_replacement,$selected_inventory_id_for_rep,$replacement_value_each,$balance_amount_paid_by,$refund_method,$refund_bank_id,$paid_by="",$amount_paid_item_price="",$payment_status="",$return_value_each="",$shipping_charge_purchased="",$shipping_charge_requested="",$original_inventory_id="",$shipping_charge_percent_paid_by_customer="",$order_item_invoice_discount_value_each=""){
		
		if($amount_paid_item_price==0){
			$amount_paid=$shipping_charge_requested;
		}
		if($amount_paid_item_price!=0 && $paid_by=="customer"){
			$amount_paid=$amount_paid_item_price+$shipping_charge_requested;
		}
		if($amount_paid_item_price!=0 && $paid_by=="admin"){
			if($amount_paid_item_price<$shipping_charge_requested){
				$paid_by="customer";
				$amount_paid=abs($amount_paid_item_price-$shipping_charge_requested);
			}
			if($amount_paid_item_price>$shipping_charge_requested){
				$paid_by="admin";
				$amount_paid=abs($amount_paid_item_price-$shipping_charge_requested);
			}
			if($amount_paid_item_price==$shipping_charge_requested){
				$paid_by="";
				$amount_paid=abs($amount_paid_item_price-$shipping_charge_requested);
			}
		}
		if($amount_paid_item_price!=0 && $paid_by==""){
			if($amount_paid_item_price<$shipping_charge_requested){
				$paid_by="customer";
				$amount_paid=abs($amount_paid_item_price-$shipping_charge_requested);
			}
			if($amount_paid_item_price>$shipping_charge_requested){
				$paid_by="admin";
				$amount_paid=abs($amount_paid_item_price-$shipping_charge_requested);
			}
			if($amount_paid_item_price==$shipping_charge_requested){
				$paid_by="";
				$amount_paid=abs($amount_paid_item_price-$shipping_charge_requested);
			}
		}
		if($balance_amount_paid_by!=""){
			$payment_status="pending";
		}
		
		$cancel_reason_comment=$this->db->escape_str($cancel_reason_comment);
		
		$sql="insert into replacement_desired (return_order_id,order_item_id,quantity_replacement,	reason_return_id,sub_issue_id,return_reason_comments,replacement_with_inventory_id,replacement_value_each_inventory_id,balance_amount_paid_by,refund_method,refund_bank_id,paid_by,amount_paid_item_price,amount_paid,payment_status,return_value_each_inventory_id,order_replacement_decision_status,order_replacement_decision_customer_decision,shipping_charge_purchased,shipping_charge_requested,amount_paid_original,paid_by_original,amount_paid_item_price_original,original_inventory_id,shipping_charge_percent_paid_by_customer,order_item_invoice_discount_value_each) values('$return_order_id','$order_item_id','$desired_quantity_replacement','$cancel_reason','sub_issue_id','$cancel_reason_comment','$selected_inventory_id_for_rep','$replacement_value_each','$balance_amount_paid_by','$refund_method','$refund_bank_id','$paid_by','$amount_paid_item_price','$amount_paid','$payment_status','$return_value_each','pending','pending','$shipping_charge_purchased','$shipping_charge_requested','$amount_paid','$paid_by','$amount_paid_item_price','$original_inventory_id','$shipping_charge_percent_paid_by_customer','$order_item_invoice_discount_value_each')";
		
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
		
	}
	
	public function save_data_to_return_stock_notification($order_item_id,$return_order_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_replacement,$selected_inventory_id_for_rep,$replacement_value_each,$timelimitdefined,$refund_method,$refund_bank_id,$return_value_each,$shipping_charge_purchased,$shipping_charge_requested,$shipping_charge_percent_paid_by_customer,$original_inventory_id,$order_item_invoice_discount_value_each){
		
		$cancel_reason_comment=$this->db->escape_str($cancel_reason_comment);
		$sql="insert into return_stock_notification set order_item_id='$order_item_id',return_order_id='$return_order_id',reason_return_id='$cancel_reason',sub_issue_id='$sub_cancel_reason',return_reason_comments='$cancel_reason_comment',quantity_replacement='$desired_quantity_replacement',replacement_value_each_inventory_id='$replacement_value_each',replacement_with_inventory_id='$selected_inventory_id_for_rep',timelimitdefined='$timelimitdefined',refund_method='$refund_method',refund_bank_id='$refund_bank_id',return_value_each_inventory_id='$return_value_each',shipping_charge_purchased='$shipping_charge_purchased',shipping_charge_requested='$shipping_charge_requested',shipping_charge_percent_paid_by_customer='$shipping_charge_percent_paid_by_customer',original_inventory_id='$original_inventory_id',order_item_invoice_discount_value_each='$order_item_invoice_discount_value_each'";
		
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	public function save_data_to_return_stock_notification_with_promo($order_item_id,$return_order_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_replacement,$selected_inventory_id_for_rep,$replacement_value_each,$timelimitdefined,$refund_method_rep,$refund_bank_id,$return_value_each,$promotion_available,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$promotion_item,$promotion_item_num,$promotion_cashback,$promotion_item_multiplier,$promotion_discount,$default_discount,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_with_promotion,$total_price_of_product_without_promotion,$shipping_charge_purchased,$shipping_charge_requested,$shipping_charge_percent_paid_by_customer,$original_inventory_id,$order_item_invoice_discount_value_each){
		
		$cancel_reason_comment=$this->db->escape_str($cancel_reason_comment);
		$sql="insert into return_stock_notification set order_item_id='$order_item_id',return_order_id='$return_order_id',reason_return_id='$cancel_reason',sub_issue_id='$sub_cancel_reason',return_reason_comments='$cancel_reason_comment',quantity_replacement='$desired_quantity_replacement',replacement_value_each_inventory_id='$replacement_value_each',replacement_with_inventory_id='$selected_inventory_id_for_rep',timelimitdefined='$timelimitdefined',refund_method='$refund_method_rep',refund_bank_id='$refund_bank_id',return_value_each_inventory_id='$return_value_each',promotion_available='$promotion_available',promotion_minimum_quantity='$promotion_minimum_quantity',promotion_quote='$promotion_quote',promotion_default_discount_promo='$promotion_default_discount_promo',promotion_surprise_gift='$promotion_surprise_gift',promotion_surprise_gift_type='$promotion_surprise_gift_type',promotion_surprise_gift_skus='$promotion_surprise_gift_skus',promotion_surprise_gift_skus_nums='$promotion_surprise_gift_skus_nums',promotion_item='$promotion_item',promotion_item_num='$promotion_item_num',promotion_cashback='$promotion_cashback',promotion_item_multiplier='$promotion_item_multiplier',promotion_discount='$promotion_discount',default_discount='$default_discount',quantity_without_promotion='$quantity_without_promotion',quantity_with_promotion='$quantity_with_promotion',cash_back_value='$cash_back_value',individual_price_of_product_with_promotion='$individual_price_of_product_with_promotion',individual_price_of_product_without_promotion='$individual_price_of_product_without_promotion',total_price_of_product_with_promotion='$total_price_of_product_with_promotion',total_price_of_product_without_promotion='$total_price_of_product_without_promotion',shipping_charge_purchased='$shipping_charge_purchased',shipping_charge_requested='$shipping_charge_requested',shipping_charge_percent_paid_by_customer='$shipping_charge_percent_paid_by_customer',original_inventory_id='$original_inventory_id',order_item_invoice_discount_value_each='$order_item_invoice_discount_value_each'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	/*return related functions ends*/
	
	
	public function update_basic_info($name,$gender,$address1,$address2,$city,$state,$pin,$country,$customer_id){
		$user_type=$this->session->userdata('user_type');
		if($user_type=='franchise_customer'){
			$sql="update franchise_customer set name='$name',gender='$gender',address1='$address1',address2='$address2',city='$city',state='$state',pincode='$pin',country='$country' where id='$customer_id'";
		}else{
			$sql="update customer set name='$name',gender='$gender',address1='$address1',address2='$address2',city='$city',state='$state',pincode='$pin',country='$country' where id='$customer_id'";
		}
		
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	public function check_current_password($current_password,$customer_id){
		$user_type=$this->session->userdata('user_type');
		if($user_type=='franchise_customer'){
			$sql="select * from franchise_customer where password='$current_password' and id='$customer_id'";
		}else{
			$sql="select * from customer where password='$current_password' and id='$customer_id'";
		}
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return true;
		}
		else{
			return false;
		}
	}
	public function update_password($password,$customer_id,$new_password){
		$user_type=$this->session->userdata('user_type');
		if($user_type=='franchise_customer'){
			$sql="update franchise_customer set password='$password',password_org='$new_password' where id='$customer_id'";
		}else{
			$sql="update customer set password='$password',password_org='$new_password' where id='$customer_id'";
		}
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	/*addresss related function starts*/
	public function get_all_customer_addresses($customer_id){
		$sql="select * from customer_address where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function add_new_address($customer_id,$name,$mobile,$door_address,$street_address,$city,$state,$pin,$country,$make_default){
		if($make_default==1){
			$sql="update customer_address set make_default=0 where customer_id='$customer_id'";
			$result=$this->db->query($sql);
		}
		$sql="insert into customer_address (customer_id,customer_name,mobile,address1,address2,city,state,pincode,country,make_default) values('$customer_id','$name','$mobile','$door_address','$street_address','$city','$state','$pin','$country','$make_default')";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
				return true;
			}
			else{
				return false;
			}
	}
	
	public function set_this_address_default($customer_address_id,$customer_id){
		$sql="update customer_address set make_default=0 where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		
		$sql="update customer_address set make_default=1 where customer_address_id='$customer_address_id' and customer_id='$customer_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
		return true;
		}
		else{
			return false;
		}
			
	}
	
	public function delete_this_address($customer_address_id,$customer_id){
		$sql="delete from customer_address where customer_address_id='$customer_address_id' and customer_id='$customer_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
		return true;
		}
		else{
			return false;
		}
	}
	
	public function get_this_customer_address($customer_id,$customer_address_id){
	$sql="select * from customer_address where customer_address_id='$customer_address_id' and customer_id='$customer_id'";
	$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
}
public function save_edit_address($customer_id,$customer_address_id,$name,$mobile,$door_address,$street_address,$city,$state,$pin,$country,$make_default){
	if($make_default==1){
		$sql="update customer_address set make_default=0 where customer_id='$customer_id'";
		$result=$this->db->query($sql);	
	}
	$sql="update customer_address set customer_name='$name',mobile='$mobile',address1='$door_address',address2='$street_address',city='$city',state='$state',pincode='$pin',country='$country',make_default='$make_default' where customer_id='$customer_id' and customer_address_id='$customer_address_id'";
	$result=$this->db->query($sql);
	if($this->db->affected_rows() > 0){
		return true;
		}
		else{
			return false;
		}
}
/*addresss related function end*/
	public function get_customer_login_credentials($customer_id){
		$user_type=$this->session->userdata('user_type');
		if($user_type=='franchise_customer'){
			$sql="select email,email_verification,mobile,mobile_verification from franchise_customer where id='$customer_id'";
		}else{
			$sql="select email,email_verification,mobile,mobile_verification from customer where id='$customer_id'";
		}
		//echo $sql;
		
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function check_email_change($email_change,$customer_id){
		$user_type=$this->session->userdata('user_type');
		if($user_type=='franchise_customer'){
			$sql="select email from franchise_customer where email='$email_change' and id!='$customer_id'";
		}else{
			$sql="select email from customer where email='$email_change' and id!='$customer_id'";
		}
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function update_email($email,$customer_id){
		if($user_type=='franchise_customer'){
			$sql="update franchise_customer set email='$email',email_verification=0 where id='$customer_id'";
		}else{
			$sql="update customer set email='$email',email_verification=0 where id='$customer_id'";
		}
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	public function check_mobile_change($mobile_change,$customer_id){
		$user_type=$this->session->userdata('user_type');
		if($user_type=='franchise_customer'){
			$sql="select mobile from franchise_customer where mobile='$mobile_change' and id!='$customer_id'";
		}else{
			$sql="select mobile from customer where mobile='$mobile_change' and id!='$customer_id'";
		}
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return true;
		}
		else{
			return false;
		}
	}
	public function update_mobile($mobile,$customer_id){
		if($user_type=='franchise_customer'){
			$sql="update franchise_customer set mobile='$mobile',mobile_verification=0 where id='$customer_id'";
		}else{
			$sql="update customer set mobile='$mobile',mobile_verification=0 where id='$customer_id'";
		}
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function get_customer_profile($customer_id){
		$user_type=$this->session->userdata('user_type');
		if($user_type=='franchise_customer'){
		$sql="select * from franchise_customer where customer_id='$customer_id'";
		}else{
			$sql="select * from additional_customer_info where customer_id='$customer_id'";
		}
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	public function get_customer_profile_info($customer_id){
		$user_type=$this->session->userdata('user_type');
		if($user_type=='franchise_customer'){
			$sql="select * from franchise_customer where id='$customer_id'";
		}else{
			$sql="select * from customer where id='$customer_id'";
		}
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	public function get_default_customer_skills(){
		$sql="select * from customer_add_profile_name order by sort_order asc";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
		
	}
public function get_default_customer_preferred_material(){
		$sql="select * from customer_add_profile_values ";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
		
	}
	
	
	public function get_profile_name_info(){
		
		$sql="select profile_name from customer_add_profile_values";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
		
	}
	public function update_additional_customer_info($customer_id,$skill_material){
		if(count($skill_material)>0){
			$skill_material_in=implode(",",$skill_material);
		}
		$sql="select * from additional_customer_info where customer_id='$customer_id'";
		
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			$sql="update additional_customer_info set skill_material='$skill_material_in' where customer_id='$customer_id'";
			$result=$this->db->query($sql);
			if($this->db->affected_rows() > 0){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			$sql="insert into additional_customer_info (customer_id,skill_material) values('$customer_id','$skill_material_in')";
			$result=$this->db->query($sql);
			if($this->db->affected_rows() > 0){
				return true;
			}
			else{
				return false;
			}
		}
		
	}
	
	/* wallet related function*/
		public function get_customer_wallet_amount($customer_id){
		$sql="select * from wallet where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->wallet_amount;
		}
		else{
			return 0;
		}
	}

	public function get_wallet_exists_for_customer($customer_id){
		$sql="select * from wallet where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return true;
		}
		else{
			return false;
		}
	}
	public function get_customer_wallet_details($customer_id){
		$sql="select * from wallet where customer_id='$customer_id'";
		$result=$this->db->query($sql);
			
			return $result->row();
		
	}
	public function get_customer_wallet_summary($customer_id){
		$sql="select * from wallet where customer_id='$customer_id' ";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function update_customer_wallet($wallet_id,$customer_id,$transaction_details,$debit,$credit,$amount,$wallet_type=''){

		$sql="insert into wallet_transaction (wallet_id,customer_id,transaction_details,debit,credit,amount,wallet_type) values('$wallet_id','$customer_id','$transaction_details','$debit','$credit','$amount','$wallet_type')";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			$sql="update wallet set wallet_amount='$amount' where wallet_id='$wallet_id' and customer_id='$customer_id'";
			$result=$this->db->query($sql);
			if($this->db->affected_rows() > 0){
					return true;
				}
				else{
					return false;
				}
		}
		else{
			return false;
		}
	}
	
	
	public function wallet_transaction_processing($params,$fg){
		//define index of column
		$columns = array( 
			0 =>'wallet_transaction.id', 
			1 => 'wallet_transaction.wallet_id',
			2 => 'wallet_transaction.transaction_details',
			3 => 'wallet_transaction.debit',
			4 => 'wallet_transaction.timestamp',
			5 => 'wallet_transaction.credit',
			6 => 'wallet_transaction.amount'
		);

		$where = $sqlTot = $sqlRec = "";
		$customer_id=$this->session->userdata('customer_id');
		// getting total number records without any search
		$sql = "SELECT *, DATE_FORMAT(timestamp,'%M %d %Y %h:%i %p') as timestamp
		FROM `wallet_transaction` where customer_id='$customer_id'";
		// check search value exist
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( wallet_transaction.transaction_details LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction.debit LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction.credit LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction.amount LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction.timestamp LIKE '%".$params['search']['value']."%' )";
		}
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}

		
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
	}
	/* wallet related function ends*/
	
	public function get_customer_products_to_be_delivered($customer_id){
		$sql="select * from active_orders where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->num_rows();
		}
		else{
			return false;
		}
	}
	public function get_customer_on_going_transactions($customer_id){
		return true;
	}
	
	public function get_addon_single_or_multiple_tagged_inventories_in_frontend_for_addon_products($master_inventory_id){
		$sql="select addon_single_or_multiple_tagged_inventories_in_frontend from tagged_inventory where tagged_main_inventory_id='$master_inventory_id'";
		$result=$this->db->query($sql);
		if(!empty($result->row_array())){
			$addon_single_or_multiple_tagged_inventories_in_frontend=$result->row_array()["addon_single_or_multiple_tagged_inventories_in_frontend"];
		}
		else{
			$addon_single_or_multiple_tagged_inventories_in_frontend="";
		}
		return $addon_single_or_multiple_tagged_inventories_in_frontend;
	}
	public function carttable_dump($localstorage_data){
		if(gettype($localstorage_data)==NULL){
			$localstorage_data=array();
			}
		$status_of_updation=array();
		$customer_id=$this->session->userdata("customer_id");
		
		if(!empty($localstorage_data)){
		foreach($localstorage_data as $localstorage_dataObj){
			
			
			$addon_single_or_multiple_tagged_inventories_in_frontend=$this->get_addon_single_or_multiple_tagged_inventories_in_frontend_for_addon_products($localstorage_dataObj->inventory_id);
			
			$addon_products=$localstorage_dataObj->addon_products;
			//print_r($tt);
			$addon_products=trim(json_encode($addon_products));
			//exit;

			$product_name=$this->db->escape_str($localstorage_dataObj->product_name);
			$product_description=$this->db->escape_str($localstorage_dataObj->product_description);

			
			
			$sql_check_inventory="select * from carttable where inventory_id='$localstorage_dataObj->inventory_id' and customer_id='$customer_id'";
			$result_check_inventory=$this->db->query($sql_check_inventory);
			//echo $result_check_inventory->num_rows();
			if($result_check_inventory->num_rows()==0){
			$get_inventory_info_by_inventory_id_obj=$this->get_inventory_info_by_inventory_id($localstorage_dataObj->inventory_id);
			$purchased_price=$get_inventory_info_by_inventory_id_obj->purchased_price;
			$CGST=$get_inventory_info_by_inventory_id_obj->CGST;
			$IGST=$get_inventory_info_by_inventory_id_obj->IGST;
			$SGST=$get_inventory_info_by_inventory_id_obj->SGST;
			$localstorage_dataObj->promotion_quote=addslashes($localstorage_dataObj->promotion_quote);
			$localstorage_dataObj->promotion_default_discount_promo=addslashes($localstorage_dataObj->promotion_default_discount_promo);
			$tax_percent_own=18;
			
			$sql="insert into carttable set 
			                purchased_price='$purchased_price',
							customer_id='$customer_id',
							inventory_id='$localstorage_dataObj->inventory_id',
							product_id='$localstorage_dataObj->product_id',
							inventory_sku='$localstorage_dataObj->inventory_sku',
							product_name='$product_name',
							inventory_selling_price_with_out_discount='$localstorage_dataObj->inventory_selling_price_with_out_discount',
							product_description='$product_description',
							
							inventory_selling_price_with_discount='$localstorage_dataObj->inventory_selling_price_with_discount',
							attribute_1='$localstorage_dataObj->attribute_1',
							attribute_2='$localstorage_dataObj->attribute_2',
							attribute_3='$localstorage_dataObj->attribute_3',
							attribute_4='$localstorage_dataObj->attribute_4',
							attribute_1_value='$localstorage_dataObj->attribute_1_value',
							attribute_2_value='$localstorage_dataObj->attribute_2_value',
							attribute_3_value='$localstorage_dataObj->attribute_3_value',
							attribute_4_value='$localstorage_dataObj->attribute_4_value',
							inventory_moq='$localstorage_dataObj->inventory_moq',
							inventory_max_oq='$localstorage_dataObj->inventory_max_oq',
							inventory_moq_original='$localstorage_dataObj->inventory_moq_original',
							product_status='$localstorage_dataObj->product_status',
							inventory_image='$localstorage_dataObj->inventory_image',
							inventory_selling_price_with_discount_original='$localstorage_dataObj->inventory_selling_price_with_discount_original',

							tax_percent='$tax_percent_own',
							
							selling_price='$localstorage_dataObj->inventory_selling_price',
							
                                                        max_selling_price='$localstorage_dataObj->max_selling_price',
							selling_discount='$localstorage_dataObj->selling_discount',
							tax_percent_price='$localstorage_dataObj->tax_percent_price',
							taxable_price='$localstorage_dataObj->taxable_price',
                                                            
							return_period='$localstorage_dataObj->inventory_return_period',shipping_charge='$localstorage_dataObj->shipping_charge',logistics_price='$localstorage_dataObj->logistics_price',default_delivery_mode_id=$localstorage_dataObj->default_delivery_mode_id,logistics_parcel_category_id='$localstorage_dataObj->logistics_parcel_category_id',territory_duration='$localstorage_dataObj->territory_duration',inventory_weight_in_kg='$localstorage_dataObj->inventory_weight_in_kg',inventory_weight_in_kg_org='$localstorage_dataObj->inventory_weight_in_kg',inventory_volume_in_kg='$localstorage_dataObj->inventory_volume_in_kg',inventory_weight_in_kg_for_unit='$localstorage_dataObj->inventory_weight_in_kg_for_unit',inventory_volume_in_kg_for_unit='$localstorage_dataObj->inventory_volume_in_kg_for_unit',logistics_id='$localstorage_dataObj->logistics_id',logistics_territory_id='$localstorage_dataObj->logistics_territory_id',vendor_id='$localstorage_dataObj->vendor_id',
                            promotion_clubed_with_default_discount='$localstorage_dataObj->promotion_clubed_with_default_discount',   
                            promotion_available='$localstorage_dataObj->promotion_available',
                            promotion_residual='$localstorage_dataObj->promotion_residual', 
                            promotion_discount='$localstorage_dataObj->promotion_discount',
                            promotion_cashback='$localstorage_dataObj->promotion_cashback',
                            promotion_surprise_gift='$localstorage_dataObj->promotion_surprise_gift',
                            promotion_surprise_gift_type='$localstorage_dataObj->promotion_surprise_gift_type',
                            promotion_surprise_gift_skus='$localstorage_dataObj->promotion_surprise_gift_skus',
                            promotion_surprise_gift_skus_nums='$localstorage_dataObj->promotion_surprise_gift_skus_nums',   
                            default_discount='$localstorage_dataObj->default_discount',    
                            promotion_item_multiplier='$localstorage_dataObj->promotion_item_multiplier',    
                            promotion_item='$localstorage_dataObj->promotion_item',
                            promotion_item_num='$localstorage_dataObj->promotion_item_num',  
                            promotion_id_selected= '$localstorage_dataObj->promotion_id_selected',  
                            promotion_type_selected='$localstorage_dataObj->promotion_type_selected',
                            promotion_minimum_quantity='$localstorage_dataObj->promotion_minimum_quantity',
                            promotion_quote='$localstorage_dataObj->promotion_quote',
                            promotion_default_discount_promo='$localstorage_dataObj->promotion_default_discount_promo',
                            payment_policy_uid='$localstorage_dataObj->payment_policy_uid',
                            message='$localstorage_dataObj->message',
                            attributes='$localstorage_dataObj->attributes',
                            cod_restrictions='$localstorage_dataObj->cod_restrictions',
                            max_value='$localstorage_dataObj->max_value',
                            currency_type='$localstorage_dataObj->currency_type',
                            individual_price_of_product_with_promotion='$localstorage_dataObj->individual_price_of_product_with_promotion',
                            individual_price_of_product_without_promotion='$localstorage_dataObj->individual_price_of_product_without_promotion',
                            total_price_of_product_without_promotion='$localstorage_dataObj->total_price_of_product_without_promotion',
                            total_price_of_product_with_promotion='$localstorage_dataObj->total_price_of_product_with_promotion',
                            quantity_without_promotion='$localstorage_dataObj->quantity_without_promotion',
                            quantity_with_promotion='$localstorage_dataObj->quantity_with_promotion',
							cash_back_value='$localstorage_dataObj->cash_back_value',
							CGST='$CGST',
							IGST='$IGST',
							SGST='$SGST',
							addon_products_status='$localstorage_dataObj->addon_products_status',
							addon_products='$addon_products',
							addon_inventories='$localstorage_dataObj->addon_inventories',
							addon_total_price='$localstorage_dataObj->addon_total_price',
							addon_total_shipping_price='$localstorage_dataObj->addon_total_shipping_price',
							addon_single_or_multiple_tagged_inventories_in_frontend='$addon_single_or_multiple_tagged_inventories_in_frontend'
							";
					$result=$this->db->query($sql);
					if($result){
						$status_of_updation[]="yes";
					}
					
					
					/////////////////////////////
			$sql_tracking_temp="insert into tracking_temp set inventory_id='$localstorage_dataObj->inventory_id',inventory_sku='$localstorage_dataObj->inventory_sku',inventory_moq='$localstorage_dataObj->inventory_moq',customer_id='$customer_id',addon_inventories='$localstorage_dataObj->addon_inventories'";
			//$result_tracking_temp=$this->db->query($sql_tracking_temp);
			////////////////////////////
			
			
			
			}
			else{
				//$status_of_updation[]="yes";
				
				
				
				
			$get_inventory_info_by_inventory_id_obj=$this->get_inventory_info_by_inventory_id($localstorage_dataObj->inventory_id);
			$purchased_price=$get_inventory_info_by_inventory_id_obj->purchased_price;
			$CGST=$get_inventory_info_by_inventory_id_obj->CGST;
			$IGST=$get_inventory_info_by_inventory_id_obj->IGST;
			$SGST=$get_inventory_info_by_inventory_id_obj->SGST;
			$localstorage_dataObj->promotion_quote=addslashes($localstorage_dataObj->promotion_quote);
			$localstorage_dataObj->promotion_default_discount_promo=addslashes($localstorage_dataObj->promotion_default_discount_promo);
			$sql="update carttable set 
			                purchased_price='$purchased_price',
							customer_id='$customer_id',
							inventory_id='$localstorage_dataObj->inventory_id',
							product_id='$localstorage_dataObj->product_id',
							inventory_sku='$localstorage_dataObj->inventory_sku',
							product_name='$product_name',
							inventory_selling_price_with_out_discount='$localstorage_dataObj->inventory_selling_price_with_out_discount',
							product_description='$product_description',
							
							inventory_selling_price_with_discount='$localstorage_dataObj->inventory_selling_price_with_discount',
							attribute_1='$localstorage_dataObj->attribute_1',
							attribute_2='$localstorage_dataObj->attribute_2',
							attribute_3='$localstorage_dataObj->attribute_3',
							attribute_4='$localstorage_dataObj->attribute_4',
							attribute_1_value='$localstorage_dataObj->attribute_1_value',
							attribute_2_value='$localstorage_dataObj->attribute_2_value',
							attribute_3_value='$localstorage_dataObj->attribute_3_value',
							attribute_4_value='$localstorage_dataObj->attribute_4_value',
							inventory_moq='$localstorage_dataObj->inventory_moq',
							inventory_max_oq='$localstorage_dataObj->inventory_max_oq',
							inventory_moq_original='$localstorage_dataObj->inventory_moq_original',
							product_status='$localstorage_dataObj->product_status',
							inventory_image='$localstorage_dataObj->inventory_image',
							inventory_selling_price_with_discount_original='$localstorage_dataObj->inventory_selling_price_with_discount_original',

							tax_percent='$localstorage_dataObj->tax_percent',
							
							selling_price='$localstorage_dataObj->inventory_selling_price',
							
                                                        max_selling_price='$localstorage_dataObj->max_selling_price',
							selling_discount='$localstorage_dataObj->selling_discount',
							tax_percent_price='$localstorage_dataObj->tax_percent_price',
							taxable_price='$localstorage_dataObj->taxable_price',
                                                            
							return_period='$localstorage_dataObj->inventory_return_period',shipping_charge='$localstorage_dataObj->shipping_charge',logistics_price='$localstorage_dataObj->logistics_price',default_delivery_mode_id=$localstorage_dataObj->default_delivery_mode_id,logistics_parcel_category_id='$localstorage_dataObj->logistics_parcel_category_id',territory_duration='$localstorage_dataObj->territory_duration',inventory_weight_in_kg='$localstorage_dataObj->inventory_weight_in_kg',inventory_weight_in_kg_org='$localstorage_dataObj->inventory_weight_in_kg',inventory_volume_in_kg='$localstorage_dataObj->inventory_volume_in_kg',inventory_weight_in_kg_for_unit='$localstorage_dataObj->inventory_weight_in_kg_for_unit',inventory_volume_in_kg_for_unit='$localstorage_dataObj->inventory_volume_in_kg_for_unit',logistics_id='$localstorage_dataObj->logistics_id',logistics_territory_id='$localstorage_dataObj->logistics_territory_id',vendor_id='$localstorage_dataObj->vendor_id',
							promotion_clubed_with_default_discount='$localstorage_dataObj->promotion_clubed_with_default_discount',   
                            promotion_available='$localstorage_dataObj->promotion_available',
                            promotion_residual='$localstorage_dataObj->promotion_residual', 
                            promotion_discount='$localstorage_dataObj->promotion_discount',
                            promotion_cashback='$localstorage_dataObj->promotion_cashback',
                            promotion_surprise_gift='$localstorage_dataObj->promotion_surprise_gift',
                            promotion_surprise_gift_type='$localstorage_dataObj->promotion_surprise_gift_type',
                            promotion_surprise_gift_skus='$localstorage_dataObj->promotion_surprise_gift_skus',
                            promotion_surprise_gift_skus_nums='$localstorage_dataObj->promotion_surprise_gift_skus_nums',   
                            default_discount='$localstorage_dataObj->default_discount',    
                            promotion_item_multiplier='$localstorage_dataObj->promotion_item_multiplier',    
                            promotion_item='$localstorage_dataObj->promotion_item',
                            promotion_item_num='$localstorage_dataObj->promotion_item_num',  
                            promotion_id_selected= '$localstorage_dataObj->promotion_id_selected',  
                            promotion_type_selected='$localstorage_dataObj->promotion_type_selected',
							promotion_minimum_quantity='$localstorage_dataObj->promotion_minimum_quantity',
							promotion_quote='$localstorage_dataObj->promotion_quote',
							promotion_default_discount_promo='$localstorage_dataObj->promotion_default_discount_promo',
							payment_policy_uid='$localstorage_dataObj->payment_policy_uid',
                            message='$localstorage_dataObj->message',
                            attributes='$localstorage_dataObj->attributes',
                            cod_restrictions='$localstorage_dataObj->cod_restrictions',
                            max_value='$localstorage_dataObj->max_value',
                            currency_type='$localstorage_dataObj->currency_type',
                            individual_price_of_product_with_promotion='$localstorage_dataObj->individual_price_of_product_with_promotion',
                            individual_price_of_product_without_promotion='$localstorage_dataObj->individual_price_of_product_without_promotion',
                            total_price_of_product_without_promotion='$localstorage_dataObj->total_price_of_product_without_promotion',
                            total_price_of_product_with_promotion='$localstorage_dataObj->total_price_of_product_with_promotion',
                            quantity_without_promotion='$localstorage_dataObj->quantity_without_promotion',
                            quantity_with_promotion='$localstorage_dataObj->quantity_with_promotion',
							cash_back_value='$localstorage_dataObj->cash_back_value',
							CGST='$CGST',IGST='$IGST',SGST='$SGST',
							addon_products_status='$localstorage_dataObj->addon_products_status',
							addon_products='$addon_products',
							addon_inventories='$localstorage_dataObj->addon_inventories',
							addon_total_price='$localstorage_dataObj->addon_total_price'
							where customer_id='$customer_id' and 
							inventory_id='$localstorage_dataObj->inventory_id'";
					$result=$this->db->query($sql);
					if($result){
						$status_of_updation[]="yes";
					}
			
			/////////////////////////////
			$sql_tracking_temp="insert into tracking_temp set inventory_id='$localstorage_dataObj->inventory_id',inventory_sku='$localstorage_dataObj->inventory_sku',inventory_moq='$localstorage_dataObj->inventory_moq',customer_id='$customer_id',addon_inventories='$localstorage_dataObj->addon_inventories'";
			//$result_tracking_temp=$this->db->query($sql_tracking_temp);
			////////////////////////////
			
			
			}
		}
		}

		if(count($status_of_updation)==count($localstorage_data)){
			return true;
		}
		else{
			return false;
		}
	}
	/* combo */

	public function carttable_dump_combo_search_delete(){
		$customer_id=$this->session->userdata("customer_id");
		$sql="delete from carttable_combo where combo_customer_id='$customer_id'";
		$res=$this->db->query($sql);
	}
	public function carttable_dump_combo($localstorage_data){
		if(gettype($localstorage_data)==NULL){
			$localstorage_data=array();
		}
		$status_of_updation=array();
		$customer_id=$this->session->userdata("customer_id");
		
		$combo_details=get_admin_combo_settings();

		if(!empty($combo_details)){
				$combo_pack_min_items=$combo_details->adm_combo_pack_min_items;
				$combo_pack_discount=$combo_details->adm_combo_pack_discount;
				$combo_pack_shipping_charge=$combo_details->adm_combo_pack_shipping_charge;
				$combo_pack_status=$combo_details->adm_combo_pack_status;
			
			if(!empty($localstorage_data)){
				$sum=0;$saved=0;$max_price=0;
				foreach($localstorage_data as $data){
					$sum+=intval($data->inv_price);
					$saved+=intval($data->inv_saved_price);
					$max_price+=intval($data->inv_max_price);
				}
				$now=date('Y-m-d H:i:s');
				$grandtotal=(intval($combo_pack_shipping_charge)+intval($sum));
				$combo_max_total_price=(intval($max_price));
				$combo_products=json_encode($localstorage_data);
				$combo_number_of_products=count($localstorage_data);

				$sql_check="select * from carttable_combo where combo_customer_id='$customer_id'";
				$res_check=$this->db->query($sql_check);
				if($res_check->num_rows()>0){

						$sql="update carttable_combo set 
								combo_products='$combo_products',
								combo_number_of_products='$combo_number_of_products',
								combo_total_price='$sum',
								combo_max_total_price='$max_price',
								combo_shipping_price='$combo_pack_shipping_charge',
								combo_grand_total='$grandtotal',
								combo_discount='$combo_pack_discount',
								combo_total_amount_saved='$saved',
								timestamp='$now' where combo_customer_id='$customer_id'
								";
					$result=$this->db->query($sql);

				}else{
					$sql="insert into carttable_combo set 
					combo_customer_id='$customer_id',
					combo_products='$combo_products',
					combo_number_of_products='$combo_number_of_products',
					combo_total_price='$sum',
					combo_max_total_price='$combo_max_total_price',
					combo_shipping_price='$combo_pack_shipping_charge',
					combo_grand_total='$grandtotal',
					combo_discount='$combo_pack_discount',
					combo_total_amount_saved='$saved',
					timestamp='$now'
					";
					$result=$this->db->query($sql);
				}
				
						
				return $result;
			}
		}

	}
	public function get_combo_cart(){
		$customer_id=$this->session->userdata("customer_id");
		$sql="select * from carttable_combo where combo_customer_id='$customer_id'";
    	$res=$this->db->query($sql);
		return $res->row_array();
	}
	/* combo */



public function carttable_exists_check($input_params){
    extract($input_params);

    $product_name_hidden=$this->db->escape_str($product_name_hidden);
    $product_description_hidden=$this->db->escape_str($product_description_hidden);

    $customer_id=$this->session->userdata("customer_id");

    $sql_check_inventory_id="select inventory_sku from carttable where customer_id='$customer_id' and inventory_id='$inventory_id_hidden'";
    $res_check_inventory_id=$this->db->query($sql_check_inventory_id);
    if($res_check_inventory_id->num_rows()>0){
        return "exists";
    }else{
        return "no";
    }
}
function get_all_inventory_data($inv_id){
   
    $sql="select * from inventory ";
    $sql.=' LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`';
		$sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`';
		$sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`';
		$sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id`';
		$sql.=' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`';
                $sql.=" where id = '$inv_id'";

				//echo $sql;
    $result=$this->db->query($sql);
    
    return $result->row();
}


public function get_tagged_inventory_offerprice_mrp_by_inventory_id($master_inventory_id,$addon_inventory_id){
	//echo $master_inventory_id.",".$addon_inventory_id;exit;
	$tagged_inventory_offerprice=0;
	$tagged_inventory_mrp=0;
	
	$sql="select tagged_inventory_ids,tagged_inventory_offerprices,tagged_inventory_mrps,tagged_inventory_discounts from tagged_inventory where tagged_main_inventory_id='$master_inventory_id'";
	$result=$this->db->query($sql);
	$res_arr=$result->result_array();
	foreach($res_arr as $arr){
		$tagged_inventory_ids_arr=explode(",",$arr["tagged_inventory_ids"]);
		if(in_array($addon_inventory_id,$tagged_inventory_ids_arr)){
			$tagged_inventory_offerprices_arr=explode(",",$arr["tagged_inventory_offerprices"]);
			$tagged_inventory_mrps_arr=explode(",",$arr["tagged_inventory_mrps"]);
			$tagged_inventory_discounts_arr=explode(",",$arr["tagged_inventory_discounts"]);
			foreach($tagged_inventory_ids_arr as $k => $tagged_inv_id){
				if($addon_inventory_id==$tagged_inv_id){
					$tagged_inventory_offerprice=$tagged_inventory_offerprices_arr[$k];
					$tagged_inventory_mrp=$tagged_inventory_mrps_arr[$k];
					$tagged_inventory_discount=$tagged_inventory_discounts_arr[$k];
				}
			}
		}
	}
	return array("tagged_inventory_offerprice"=>$tagged_inventory_offerprice,"tagged_inventory_mrp"=>$tagged_inventory_mrp,"tagged_inventory_discount"=>$tagged_inventory_discount);
}
public function get_carttable_customer_id_count($customer_id){
	$sql="select count(*) as count from carttable where customer_id='$customer_id'";
	$result=$this->db->query($sql);
    return $result->row()->count;
}

public function carttable_dump_for_session_customers($input_params){
		extract($input_params);
		$addon_single_or_multiple_tagged_inventories_in_frontend="";
		$product_name_hidden=$this->db->escape_str($product_name_hidden);
		$product_description_hidden=$this->db->escape_str($product_description_hidden);
		
		
		
		$customer_id=$this->session->userdata("customer_id");
		
		$addon_inventories=$this->db->escape_str($addon_inventories_hidden);
		$addon_taggedtype=$this->db->escape_str($addon_taggedtype_hidden);
		$addon_products='';$addon_products_status='0';$addon_total_price=0;$addon_mrp_price=0;$addon_total_shipping_price=0;

		if($addon_inventories!=''){
			
			
			
			$addon_taggedtype_arr=explode(',',$addon_taggedtype);
			$addon_taggedtype_arr=array_filter($addon_taggedtype_arr);
			$addon_taggedtype=implode(',',$addon_taggedtype_arr);

			$addon_arr=explode(',',$addon_inventories);
			$addon_arr=array_filter($addon_arr);
			$addon_inventories=implode(',',$addon_arr);
			//print_r($addon_arr);
			$addon_products_arr=[];
			if(!empty($addon_arr)){
				$addon_products_status='1';
				$addon_single_or_multiple_tagged_inventories_in_frontend=$this->get_addon_single_or_multiple_tagged_inventories_in_frontend_for_addon_products($inventory_id_hidden);
				//echo $addon_taggedtype;

				if(1){
				
					foreach($addon_arr as $key=> $inv_id){

						$addon_taggedtype=(isset($addon_taggedtype_arr[$key])) ? $addon_taggedtype_arr[$key] : '';

						
						if($addon_taggedtype=="internal"){

						$res_data=$this->get_all_inventory_data($inv_id);


						//print_r($res_data);

						if(!empty($res_data)){
							$tagged_inventory_offerprice_mrp_arr=$this->get_tagged_inventory_offerprice_mrp_by_inventory_id($inventory_id_hidden,$inv_id);
							$tagged_inventory_offerprice=$tagged_inventory_offerprice_mrp_arr["tagged_inventory_offerprice"];
							$tagged_inventory_mrp=$tagged_inventory_offerprice_mrp_arr["tagged_inventory_mrp"];
							$tagged_inventory_discount=$tagged_inventory_offerprice_mrp_arr["tagged_inventory_discount"];

							//if($addon_single_or_multiple_tagged_inventories_in_frontend=="single"){
								$addon_total_price+=($tagged_inventory_offerprice);
								$addon_mrp_price+=($tagged_inventory_mrp);
								$shipping_charge_hidden=0;
							//}
							// if($addon_single_or_multiple_tagged_inventories_in_frontend=="multiple"){
							// 	$addon_total_price=($res_data->selling_price);
							// 	$addon_mrp_price=($res_data->max_selling_price);
							// }

							$name=($res_data->sku_name!='') ? $res_data->sku_name : $res_data->product_name;
							$name=trim($name);

							$addon_products_arr[]=['inv_id'=>$inv_id,'inv_price'=>$addon_total_price,'inv_mrp_price'=>$addon_mrp_price,'inv_sku_id'=>$res_data->sku_id,'inv_image'=>$res_data->image,'inv_name'=>$name,'inv_display_name'=>$name,'inv_ship_price'=>$shipping_charge_hidden,"tagged_type"=>$addon_taggedtype,"offer_percentage"=>$tagged_inventory_discount];
							$addon_total_shipping_price+=intval($shipping_charge_hidden);

						}
					}
				
				
					}//foreach
				
			
			}//internal
			}
			$addon_products=json_encode($addon_products_arr);
		}
		

		//echo $addon_products;
		//exit;

		$sql_check_inventory_id="select inventory_sku from carttable where customer_id='$customer_id' and inventory_id='$inventory_id_hidden'";
		$res_check_inventory_id=$this->db->query($sql_check_inventory_id);
		if($res_check_inventory_id->num_rows()>0){
			return "exists";
		}
		else{
		    $get_inventory_info_by_inventory_id_obj=$this->get_inventory_info_by_inventory_id($inventory_id_hidden);
			$purchased_price=$get_inventory_info_by_inventory_id_obj->purchased_price;
			
			$promotion_quote=addslashes($promotion_quote);
			$promotion_default_discount_promo=addslashes($promotion_default_discount_promo);
			$tax_percent_own=18;
		 $sql="insert into carttable set 
							purchased_price='$purchased_price',
							customer_id='$customer_id',
							inventory_id='$inventory_id_hidden',
							product_id='$product_id_hidden',
							inventory_sku='$inventory_sku_hidden',
							product_name='$product_name_hidden',
							
							inventory_selling_price_with_out_discount='$inventory_selling_price_with_out_discount_hidden',
							product_description='$product_description_hidden',
							
							inventory_selling_price_with_discount='$inventory_selling_price_with_discount_hidden',
							attribute_1='$attribute_1_hidden',
							attribute_2='$attribute_2_hidden',
							attribute_3='$attribute_3_hidden',
							attribute_4='$attribute_4_hidden',
							attribute_1_value='$attribute_1_value_hidden',
							attribute_2_value='$attribute_2_value_hidden',
							attribute_3_value='$attribute_3_value_hidden',
							attribute_4_value='$attribute_4_value_hidden',
							inventory_moq='$inventory_moq_hidden',
							inventory_moq_original='$inventory_moq_original_hidden',
							inventory_max_oq='$inventory_max_oq_hidden',
							product_status='$product_status_hidden',
							inventory_image='$inventory_image_hidden',
							inventory_selling_price_with_discount_original='$inventory_selling_price_with_discount_hidden',
							tax_percent='$tax_percent_own',
							selling_price='$inventory_selling_price_hidden',
							
                                                        max_selling_price='$max_selling_price_hidden',
							selling_discount='$selling_discount_hidden',
							tax_percent_price='$tax_percent_price_hidden',
							taxable_price='$taxable_price_hidden',
                                                            
							return_period='$inventory_return_period_hidden',shipping_charge='$shipping_charge_hidden',logistics_price='$logistics_price_hidden',default_delivery_mode_id='$default_delivery_mode_id_hidden',logistics_parcel_category_id='$logistics_parcel_category_id_hidden',territory_duration='$territory_duration_hidden',inventory_volume_in_kg='$inventory_volume_in_kg_hidden',inventory_weight_in_kg='$inventory_weight_in_kg_hidden',inventory_weight_in_kg_org='$inventory_weight_in_kg_hidden',inventory_weight_in_kg_for_unit='$inventory_weight_in_kg_for_unit_hidden',inventory_volume_in_kg_for_unit='$inventory_volume_in_kg_for_unit_hidden',logistics_id='$logistics_id_hidden',logistics_territory_id='$logistics_territory_id_hidden',vendor_id='$vendor_id_hidden',
                            promotion_clubed_with_default_discount='$promotion_clubed_with_default_discount',   
                            promotion_available='$promotion_available',
                            promotion_residual='$promotion_residual', 
                            promotion_discount='$promotion_discount',
                            promotion_cashback='$promotion_cashback',
                            promotion_surprise_gift='$promotion_surprise_gift',
                            promotion_surprise_gift_type='$promotion_surprise_gift_type',
                            promotion_surprise_gift_skus='$promotion_surprise_gift_skus',
                            promotion_surprise_gift_skus_nums='$promotion_surprise_gift_skus_nums',   
                            default_discount='$default_discount',    
                            promotion_item_multiplier='$promotion_item_multiplier',    
                            promotion_item='$promotion_item',
                            promotion_item_num='$promotion_item_num',  
                            promotion_id_selected= '$promotion_id_selected',  
                            promotion_type_selected='$promotion_type_selected',
                            promotion_minimum_quantity='$promotion_minimum_quantity',
                            promotion_quote='$promotion_quote',
                            promotion_default_discount_promo='$promotion_default_discount_promo',
                            payment_policy_uid='$payment_policy_uid_hidden',
                            message='$message_hidden',
                            attributes='$attributes_hidden',
                            cod_restrictions='$cod_restrictions_hidden',
                            max_value='$max_value_hidden',
                            currency_type='$currency_type_hidden',
                            individual_price_of_product_with_promotion='$individual_price_of_product_with_promotion',
                            individual_price_of_product_without_promotion='$individual_price_of_product_without_promotion',
                            total_price_of_product_without_promotion='$total_price_of_product_without_promotion',
                            total_price_of_product_with_promotion='$total_price_of_product_with_promotion',
                            quantity_without_promotion='$quantity_without_promotion',
                            quantity_with_promotion='$quantity_with_promotion',
                            cash_back_value='$cash_back_value',
							CGST='$CGST_hidden',
							IGST='$IGST_hidden',
							SGST='$SGST_hidden',
							addon_products_status='$addon_products_status',
							addon_products='$addon_products',
							addon_inventories='$addon_inventories',
							addon_total_price='$addon_total_price',
							addon_total_shipping_price='$addon_total_shipping_price',
							addon_single_or_multiple_tagged_inventories_in_frontend='$addon_single_or_multiple_tagged_inventories_in_frontend'
							";

			$result=$this->db->query($sql);
			
			/////////////////////////////
			$sql_tracking_temp="insert into tracking_temp set inventory_id='$inventory_id_hidden',inventory_sku='$inventory_sku_hidden',inventory_moq='$inventory_moq_hidden',customer_id='$customer_id',addon_inventries='$addon_inventories'";
			//$result_tracking_temp=$this->db->query($sql_tracking_temp);
			////////////////////////////
			
			
			return $result;
		}
		
	}
	
	public function get_cart_count(){
		$customer_id=$this->session->userdata("customer_id");
		$sql="select count(*) as count from carttable where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		return $result->row()->count;
	}	
	
	
	function delete_carttable_for_customer(){
		$customer_id=$this->session->userdata("customer_id");
		$sql="delete from carttable where customer_id='$customer_id'";
		$res=$this->db->query($sql);
	}
	public function get_cartinfo_in_json_format_checkout(){
		
		$customer_id=$this->session->userdata("customer_id");
	 	$sql="select carttable.*,logistics_inventory.logistics_parcel_category_id as parcel_category_ids,(inventory.stock-inventory.cutoff_stock) as stock_available from carttable,logistics_inventory,inventory where carttable.customer_id='$customer_id' and logistics_inventory.inventory_id=carttable.inventory_id and inventory.id=carttable.inventory_id" ;
		$res=$this->db->query($sql);
		return $res->result_array();
		
	}
	public function get_cartinfo_in_json_format_checkout_by_cart_id($cart_id){
		
		$customer_id=$this->session->userdata("customer_id");
		$sql="select carttable.*,logistics_inventory.logistics_parcel_category_id as parcel_category_ids,(inventory.stock-inventory.cutoff_stock) as stock_available from carttable,logistics_inventory,inventory where carttable.customer_id='$customer_id' and logistics_inventory.inventory_id=carttable.inventory_id and inventory.id=carttable.inventory_id and carttable.cart_id='$cart_id'";
		$res=$this->db->query($sql);
		return $res->row_array();
		
	}
	public function get_all_parcel_categories_new($logistics_id,$vendor_id){
		$sql="select * from logistics_parcel_category where	logistics_id='$logistics_id' and vendor_id='$vendor_id'";
		$res=$this->db->query($sql);
		
		return $res->result_array();
	}
	public function get_all_parcel_categories($parcel_category_ids){
		$sql="select * from logistics_parcel_category where logistics_parcel_category_id in ($parcel_category_ids)";
		$res=$this->db->query($sql);
		
		return $res->result_array();
	}
	public function get_delivery_mode_and_parcel_category_combinations($delivery_mode_arr,$parcel_category_arr,$logistics_id,$territory_name_id,$cart_id){
		$arr=array();
		foreach($delivery_mode_arr as $delivery_mode_id){
			$logistics_parcel_category_id_arr=array();
			$sql="select logistics_parcel_category_id,logistics_id from  logistics_weight where logistics_delivery_mode_id='$delivery_mode_id' and territory_name_id='$territory_name_id' and logistics_id='$logistics_id'";
			$res=$this->db->query($sql);
			foreach($res->result_array() as $varr){
				$logistics_parcel_category_id_arr[]=$varr["logistics_parcel_category_id"];
			}
			$logistics_parcel_category_id_in=implode(",",$logistics_parcel_category_id_arr);
			$sql1="select * from logistics_parcel_category where logistics_parcel_category_id in ($logistics_parcel_category_id_in)";
			$res1=$this->db->query($sql1);
			$arr[$delivery_mode_id]=$res1->result_array();
		}
		return $arr;
	}
	public function get_all_parcel_categories_by_logistics_id($parcel_category_ids,$logistics_id){
		$sql="select * from logistics_parcel_category where logistics_parcel_category_id in ($parcel_category_ids) and logistics_id='$logistics_id'";
		$res=$this->db->query($sql);
		
		return $res->result_array();
	}
	public function get_parcel_category_ids($parcel_category_ids,$logistics_id){
		$sql="select * from logistics_parcel_category where logistics_parcel_category_id in ($parcel_category_ids) and logistics_id='$logistics_id'";
		$res=$this->db->query($sql);
		$arr=array();
		foreach($res->result_array() as $varr){
			$arr[]=$varr["logistics_parcel_category_id"];
		}
		return implode(",",$arr);
	}
	public function get_cartinfo_in_json_format(){
		$customer_id=$this->session->userdata("customer_id");
		//$sql="select * from carttable where customer_id='$customer_id'";
		
		$sql="select carttable.*,(inventory.stock-inventory.cutoff_stock) as stock_available from carttable,inventory where carttable.customer_id='$customer_id' and inventory.id=carttable.inventory_id";
		
		$res=$this->db->query($sql);
		return $res->result_array();
	}
	public function get_sku_id_by_inventory_id($inventory_id){
		$sql="select sku_id from inventory where id='$inventory_id'";
		$res=$this->db->query($sql);
		return $res->row_array()["sku_id"];
	}
	public function get_promotion_id_selected_by_inventory_id($inventory_id,$cart_id,$customer_id){
		$sql="select promotion_id_selected from carttable where cart_id='$cart_id' and inventory_id='$inventory_id' and customer_id='$customer_id'";
		$res=$this->db->query($sql);
		return $res->row_array()["promotion_id_selected"];
	}
	public function delete_carttable_ids($promotion_differed_inventory_cart_ids,$customer_id){
		$promotion_differed_inventory_cart_ids_arr=explode("|",$promotion_differed_inventory_cart_ids);
		$promotion_differed_inventory_cart_ids_in=implode(",",$promotion_differed_inventory_cart_ids_arr);
		$sql="delete from carttable where cart_id in ($promotion_differed_inventory_cart_ids_in) and customer_id='$customer_id'";
		$res=$this->db->query($sql);
		return $res;
	}
	public function update_cart_info($data,$logistics_price,$shipping_charge){ 
		$customer_id=$this->session->userdata("customer_id");
		$str="";
		if(intval($data->promotion_available)==1){

			
			$individual_price_of_product_with_promotion=$data->individual_price_of_product_with_promotion;
			$individual_price_of_product_without_promotion=$data->individual_price_of_product_without_promotion;
			$total_price_of_product_without_promotion=$data->total_price_of_product_without_promotion;
			$total_price_of_product_with_promotion=$data->total_price_of_product_with_promotion;
			$quantity_without_promotion=$data->quantity_without_promotion;
			$quantity_with_promotion=$data->quantity_with_promotion;

			$str=",individual_price_of_product_with_promotion='$individual_price_of_product_with_promotion',individual_price_of_product_without_promotion='$individual_price_of_product_without_promotion',quantity_with_promotion='$total_price_of_product_without_promotion',total_price_of_product_without_promotion='$total_price_of_product_without_promotion',total_price_of_product_with_promotion='$total_price_of_product_with_promotion',quantity_without_promotion='$quantity_without_promotion',quantity_with_promotion='$quantity_with_promotion'";
			
		}else{
                        $individual_price_of_product_with_promotion=0;
			$individual_price_of_product_without_promotion=0;
			$total_price_of_product_without_promotion=0;
			$total_price_of_product_with_promotion=0;
			$quantity_without_promotion=0;
			$quantity_with_promotion=0;

			$str=",individual_price_of_product_with_promotion='$individual_price_of_product_with_promotion',individual_price_of_product_without_promotion='$individual_price_of_product_without_promotion',quantity_with_promotion='$total_price_of_product_without_promotion',total_price_of_product_without_promotion='$total_price_of_product_without_promotion',total_price_of_product_with_promotion='$total_price_of_product_with_promotion',quantity_without_promotion='$quantity_without_promotion',quantity_with_promotion='$quantity_with_promotion'";
                        
                        
                        $promotion_clubed_with_default_discount='';
                        $promotion_residual ='';
                        $promotion_discount ='';
                        $promotion_cashback='';
                        $promotion_surprise_gift='';
                        $promotion_surprise_gift_type ='';
                        $promotion_surprise_gift_skus='';
                        $promotion_surprise_gift_skus_nums='';
                        $default_discount='';
                        $promotion_item_multiplier='';
                        $promotion_item='';
                        $promotion_item_num='';
                        $promotion_id_selected='';
                        $promotion_type_selected='';
                        $promotion_minimum_quantity='';
                        $promotion_quote='';
                        $promotion_default_discount_promo='';
                        
                        $str.=",promotion_clubed_with_default_discount='$promotion_clubed_with_default_discount', 
                            promotion_residual='$promotion_residual', 
                            promotion_discount='$promotion_discount',
                            promotion_cashback='$promotion_cashback',
                            promotion_surprise_gift='$promotion_surprise_gift',
                            promotion_surprise_gift_type='$promotion_surprise_gift_type',
                            promotion_surprise_gift_skus='$promotion_surprise_gift_skus',
                            promotion_surprise_gift_skus_nums='$promotion_surprise_gift_skus_nums',   
                            default_discount='$default_discount',    
                            promotion_item_multiplier='$promotion_item_multiplier',    
                            promotion_item='$promotion_item',
                            promotion_item_num='$promotion_item_num',  
                            promotion_id_selected= '$promotion_id_selected',  
                            promotion_type_selected='$promotion_type_selected',
                            promotion_minimum_quantity='$promotion_minimum_quantity',
                            promotion_quote='$promotion_quote',
                            promotion_default_discount_promo='$promotion_default_discount_promo'";
                }
		
                $coupon_str='';
                if(isset($data->coupon_applied_status)){
                    if(intval($data->coupon_applied_status)==1){
                        /*$coupon_str.=" ,coupon_code='$data->coupon_code_appied', 
                                coupon_type='$data->coupon_type_appied', 
                                coupon_name='$data->coupon_name_appied', 
                                coupon_value='$data->coupon_value_appied' ";*/
                    }
                }
                
		 $sql="update carttable set inventory_moq='$data->inventory_moq',logistics_id='$data->logistics_id',logistics_parcel_category_id='$data->logistics_parcel_category_id',default_delivery_mode_id='$data->default_delivery_mode_id',logistics_territory_id='$data->logistics_territory_id',territory_duration='$data->territory_duration',inventory_weight_in_kg='$data->inventory_weight_in_kg',inventory_weight_in_kg_org='$data->inventory_weight_in_kg_org',inventory_weight_in_kg_for_unit='$data->inventory_weight_in_kg_for_unit',inventory_volume_in_kg='$data->inventory_volume_in_kg',logistics_price='$logistics_price',shipping_charge='$shipping_charge',cash_back_value='$data->cash_back_value' $str $coupon_str where cart_id='$data->cart_id'";
	
		$res=$this->db->query($sql);
		return $res;
	}
	
	public function update_cart_info_ss($data,$logistics_price,$shipping_charge){
		$customer_id=$this->session->userdata("customer_id");
		$str="";
		
		$sql="update carttable set inventory_moq='$data->inventory_moq',logistics_id='$data->logistics_id',logistics_parcel_category_id='$data->logistics_parcel_category_id',parcel_category_multiplier='$data->parcel_category_multiplier',default_delivery_mode_id='$data->default_delivery_mode_id',logistics_territory_id='$data->logistics_territory_id',territory_duration='$data->territory_duration',inventory_weight_in_kg='$data->inventory_weight_in_kg',inventory_weight_in_kg_org='$data->inventory_weight_in_kg_org',inventory_weight_in_kg_for_unit='$data->inventory_weight_in_kg_for_unit',inventory_volume_in_kg='$data->inventory_volume_in_kg',logistics_price='$logistics_price',shipping_charge='$shipping_charge' $str where cart_id='$data->cart_id'";
		$res=$this->db->query($sql);
		return $res;
	}
	
	public function update_cart_info_with_delivery_mode($data,$shipping_charge,$logistics_delivery_mode_id,$duration){
		$customer_id=$this->session->userdata("customer_id");
		$duration_arr=json_decode($duration,true);
		$territory_duration=$duration_arr[$data->logistics_territory_id];
		$sql="update carttable set inventory_moq='$data->inventory_moq',logistics_id='$data->logistics_id',logistics_parcel_category_id='$data->logistics_parcel_category_id',default_delivery_mode_id='$logistics_delivery_mode_id',logistics_territory_id='$data->logistics_territory_id',territory_duration='$territory_duration',inventory_weight_in_kg='$data->inventory_weight_in_kg',inventory_weight_in_kg_org='$data->inventory_weight_in_kg_org',inventory_weight_in_kg_for_unit='$data->inventory_weight_in_kg_for_unit',inventory_volume_in_kg='$data->inventory_volume_in_kg',logistics_price='$data->logistics_price',shipping_charge='$shipping_charge' where cart_id='$data->cart_id'";
		$res=$this->db->query($sql);
		return $res;
	}
	public function update_cart_info_with_parcel_category_id($data,$shipping_charge,$logistics_parcel_category_id){
		$customer_id=$this->session->userdata("customer_id");

		$sql="update carttable set inventory_moq='$data->inventory_moq',logistics_id='$data->logistics_id',logistics_parcel_category_id='$logistics_parcel_category_id',default_delivery_mode_id='$data->default_delivery_mode_id',logistics_territory_id='$data->logistics_territory_id',territory_duration='$data->territory_duration',inventory_weight_in_kg='$data->inventory_weight_in_kg',inventory_weight_in_kg_org='$data->inventory_weight_in_kg_org',inventory_weight_in_kg_for_unit='$data->inventory_weight_in_kg_for_unit',inventory_volume_in_kg='$data->inventory_volume_in_kg',logistics_price='$data->logistics_price',shipping_charge='$shipping_charge' where cart_id='$data->cart_id'";
		$res=$this->db->query($sql);
		return $res;
	}
	public function check_default_parcel_category_or_not($logistics_id,$logistics_parcel_category_id){
		$sql="select * from logistics_parcel_category where logistics_id='$logistics_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and default_parcel_category=1";
		$res=$this->db->query($sql);
		if($res->num_rows()==0){
			return "no";
		}
		else{
			return "yes";
		}
		
	}
	public function get_logistics_inventory_data($inventory_id){
		$sql="select * from logistics_inventory where inventory_id='$inventory_id'";
		$result=$this->db->query($sql);
		return $result->row();
	}
	public function get_logistics_by_logistics_id($logistics_id){
		$sql="select * from logistics where logistics_id='$logistics_id'";
		$result=$this->db->query($sql);
		return $result->row();
	}
	public function get_carttable_by_cart_id($cart_id){
		$sql="select * from carttable where cart_id='$cart_id'";
		$result=$this->db->query($sql);
		return $result->row();
	}
	public function get_logistics_parcel_box_dimension_info($logistics_id){
		$get_logistics_by_logistics_id_row=$this->get_logistics_by_logistics_id($logistics_id);
		$vendor_id=$get_logistics_by_logistics_id_row->vendor_id;
		$sql="select * from logistics_parcel_box_dimension where vendor_id='$vendor_id' order by lbh asc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id($logistics_parcel_box_dimension_id){
		$sql="select * from logistics_parcel_box_dimension where logistics_parcel_box_dimension_id='$logistics_parcel_box_dimension_id'";
		$query=$this->db->query($sql);
		return $query->row();
	} 
	public function get_logistics_price($update_cart_info_post_data){
		//print_r($update_cart_info_post_data);
		//logistics_territory_id
		//logistics_id
		//print_r($update_cart_info_post_data);
		//print_r($update_cart_info_post_data);
		
							////////// getting inventory addons weights in kg starts////////////////////////////////
							$addons_inv_list_arr=[];
							$inventory_for_addons_weight_in_kg=0;
							if($update_cart_info_post_data->addon_products_status=="1"){
								$addons_inv_list_full_arr=$update_cart_info_post_data->addon_products;
								if(gettype($addons_inv_list_full_arr)=="string"){
									$addons_inv_list_full_arr=json_decode($addons_inv_list_full_arr);
								}
								if(!empty($addons_inv_list_full_arr)){
									foreach($addons_inv_list_full_arr as $obj){
										$addons_inv_list_arr[]=$obj->inv_id;
									}
									$inventory_for_addons_weight_in_kg=$this->Model_search->get_inventory_for_addons_weight_in_kg($addons_inv_list_arr);
								}
							}
							
							
							////////// getting inventory addons weights in kg ends////////////////////////////////
							
							
							
				$logistics_price=0;
				$current_quantity=$update_cart_info_post_data->inventory_moq;
				if(isset($update_cart_info_post_data->inventory_id)){
					$inventory_id=$update_cart_info_post_data->inventory_id;
				}
				else{
					$get_carttable_by_cart_id_obj=$this->get_carttable_by_cart_id($update_cart_info_post_data->cart_id);
					$inventory_id=$get_carttable_by_cart_id_obj->inventory_id;
				}
				
				
				$logistics_id=$update_cart_info_post_data->logistics_id;
				$inventory_weight_in_kg=$update_cart_info_post_data->inventory_weight_in_kg;
				$inventory_weight_in_kg=$inventory_weight_in_kg+(float)$inventory_for_addons_weight_in_kg; // adding the addon products weight also
				//$inventory_volume_in_kg=$update_cart_info_post_data->inventory_volume_in_kg;
				
				$logistics_territory_id=$update_cart_info_post_data->logistics_territory_id;
				$default_delivery_mode_id=$update_cart_info_post_data->default_delivery_mode_id;
				$logistics_parcel_category_id=$update_cart_info_post_data->logistics_parcel_category_id;
				
				
				
				/* new code - nanthini  */
				
				$weight=$inventory_weight_in_kg;
				$sql="SELECT logistics_price,SUBSTRING_INDEX(weight_in_kg,'-',1) as 'weight_in_kg_from',SUBSTRING_INDEX(weight_in_kg,'-',-1) as 'weight_in_kg_to'
	FROM logistics_weight where $weight between SUBSTRING_INDEX(weight_in_kg,'-',1) and SUBSTRING_INDEX(weight_in_kg,'-',-1) and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id'";
				
				
				/*$sql="SELECT logistics_price,
				REPLACE(weight_in_kg, '-', ',') as dashRep
				FROM logistics_weight
				WHERE FIND_IN_SET($weight, REPLACE(weight_in_kg, '-', ',')) and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id'";*/
	
				$query=$this->db->query($sql);
	
				$res=$query->row_array();
				$logistics_price_with_tax=$res["logistics_price"];
				$required_volume_in_kg=$inventory_weight_in_kg;
	
				//print_r($res);
	
				/* new code - nanthini  */
	
				return $logistics_price_with_tax;
				
				
				
				
				
				
				/*
				
				
				
				
				
				$get_logistics_inventory_data_obj=$this->get_logistics_inventory_data($inventory_id);
				
				$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
				$logistics_volumetric_value=$get_logistics_by_logistics_id_obj->logistics_volumetric_value;
				
				$get_shipping_module_info_obj=$this->get_shipping_module_info();
				if($get_shipping_module_info_obj->each_sku==1){
				/////// for all the skus code 12-22-2019 based on packings not volumetric weight starts/////
				$inventory_weight_in_kg_per_unit=$inventory_weight_in_kg/$current_quantity;
				$inventory_length=$get_logistics_inventory_data_obj->inventory_length;
				$inventory_breadth=$get_logistics_inventory_data_obj->inventory_breadth;
				$inventory_height=$get_logistics_inventory_data_obj->inventory_height;
				
				$inventory_moq=$get_logistics_inventory_data_obj->inventory_moq;
				$suitable_box_arr=array();
				$get_logistics_parcel_box_dimension_info_result=$this->get_logistics_parcel_box_dimension_info($logistics_id);
				foreach($get_logistics_parcel_box_dimension_info_result as $get_logistics_parcel_box_dimension_info_row){
					$available_space_for_filling_length=$get_logistics_parcel_box_dimension_info_row->available_space_for_filling_length;
					$available_space_for_filling_breadth=$get_logistics_parcel_box_dimension_info_row->available_space_for_filling_breadth;
					$available_space_for_filling_height=$get_logistics_parcel_box_dimension_info_row->available_space_for_filling_height;
				
					if($inventory_length<=$available_space_for_filling_length && $inventory_breadth<=$available_space_for_filling_breadth & $inventory_height<=$available_space_for_filling_height){
							$suitable_box_arr[$get_logistics_parcel_box_dimension_info_row->logistics_parcel_box_dimension_id]=0;
					}
				}
				
				foreach($suitable_box_arr as $logistics_parcel_box_dimension_id => $val){
					$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row=$this->get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id($logistics_parcel_box_dimension_id);
					$box_available_space_for_filling_length=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->available_space_for_filling_length;
					$box_available_space_for_filling_breadth=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->available_space_for_filling_breadth;
					$box_available_space_for_filling_height=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->available_space_for_filling_height;
					
					$box_available_space_for_filling_length_temp=$box_available_space_for_filling_length;
					$box_available_space_for_filling_breadth_temp=$box_available_space_for_filling_breadth;
					$box_available_space_for_filling_height_temp=$box_available_space_for_filling_height;
					$flag=true;
					
					while($flag){
						if($inventory_length<=$box_available_space_for_filling_length_temp){
							$suitable_box_arr[$logistics_parcel_box_dimension_id]++;
							
							$box_available_space_for_filling_length_temp=$box_available_space_for_filling_length_temp-$inventory_length;
							$box_available_space_for_filling_breadth_temp=$box_available_space_for_filling_breadth_temp-$inventory_breadth;
							$box_available_space_for_filling_height_temp=$box_available_space_for_filling_height_temp-$inventory_height;
									
									
						}
						else{
							$flag=false;
						}
					}
					
				}
				arsort($suitable_box_arr);
				//print_r($suitable_box_arr);
				$no_of_boxes_arr=[];
				$extra_box_arr=[];
				$quantity_filled=0;
				$current_quantity_temp=$current_quantity;
				foreach($suitable_box_arr as $box_id => $box_fitted_quantity){
					if($current_quantity_temp>=$box_fitted_quantity){
						$no_of_boxes_arr[$box_id]["no_of_boxes"]=(int)($current_quantity_temp/$box_fitted_quantity);
						$quantity_filled+=($no_of_boxes_arr[$box_id]["no_of_boxes"]*$suitable_box_arr[$box_id]);
						$current_quantity_temp=$current_quantity_temp%$box_fitted_quantity;
					}
				}
				
				$remaining_quantity=$current_quantity-$quantity_filled;
				if($remaining_quantity>0){
					foreach($suitable_box_arr as $box_id => $box_fitted_quantity){
						if((100-(($remaining_quantity/$box_fitted_quantity))*100)<=50){ // remaining space
							$extra_box_arr[$box_id]["no_of_quantities"]=$remaining_quantity;
						}
					}
				}
				//print_r($no_of_boxes_arr);
				//print_r($extra_box_arr);
				$total_arr=array("no_of_boxes_arr"=>$no_of_boxes_arr,"extra_box_arr"=>$extra_box_arr);
				$max_actual_volumetric_arr=array();
				foreach($total_arr as $k=>$v_arr){
					if($k=="no_of_boxes_arr"){
						if(!empty($v_arr)){
							foreach($v_arr as $box_id => $boxes_arr){
								$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row=$this->get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id($box_id);
								
								$parcel_lbh=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_length*
								$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_breadth*
								$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_height;
								for($i=1;$i<=$boxes_arr["no_of_boxes"];$i++){
									$box_actual_weight_without_packing=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->box_actual_weight_without_packing;
									
									$total_actual_weight_parcel_sku=($inventory_weight_in_kg_per_unit*$suitable_box_arr[$box_id])+$box_actual_weight_without_packing;
									
									$volumetric_weight_of_parcel_in_kg=$parcel_lbh/$logistics_volumetric_value;
									
									$max_actual_volumetric_arr[]=max($total_actual_weight_parcel_sku,$volumetric_weight_of_parcel_in_kg);
								}
								//$boxes_arr["no_of_boxes"]
							}
						}
					}
					if($k=="extra_box_arr"){
						if(!empty($v_arr)){
							foreach($v_arr as $box_id => $boxes_arr){
								$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row=$this->get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id($box_id);
								
								$parcel_lbh=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_length*
								$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_breadth*
								$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_height;
								
								$box_actual_weight_without_packing=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->box_actual_weight_without_packing;
								
								$total_actual_weight_parcel_sku=($inventory_weight_in_kg_per_unit*$boxes_arr["no_of_quantities"])+$box_actual_weight_without_packing;
								
								$volumetric_weight_of_parcel_in_kg=$parcel_lbh/$logistics_volumetric_value;
								
								$max_actual_volumetric_arr[]=max($total_actual_weight_parcel_sku,$volumetric_weight_of_parcel_in_kg);
								
								//$boxes_arr["no_of_boxes"]
							}
						}
					}
				}
				//print_r($max_actual_volumetric_arr);
				$required_volume_in_kg=array_sum($max_actual_volumetric_arr);
				
				/////// for all the skus code 12-22-2019 based on packings not volumetric weight ends/////
				}
				//$required_volume_in_kg=3;
				
				////////////////////////
				
				
				//$get_logistics_inventory_data_obj=$this->get_logistics_inventory_data($inventory_id);
				//$inventory_volume_in_lbh_cms=$get_logistics_inventory_data_obj->inventory_volume_in_kg;
				//$quantity_volumetric_breakup_point=$get_logistics_inventory_data_obj->volumetric_breakup_point;
				
				//$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
				//$logistics_volumetric_value=$get_logistics_by_logistics_id_obj->logistics_volumetric_value;
				//$net_volumetric_volume_weight=$inventory_volume_in_lbh_cms/$logistics_volumetric_value;
				
				//if($current_quantity<($quantity_volumetric_breakup_point+1)){
				//	$resultant_inventory_volume_in_kg=$net_volumetric_volume_weight;
				//}
				//else{
				//	$resultant_inventory_volume_in_kg=(intval(($current_quantity-1)/$quantity_volumetric_breakup_point)+1)*$net_volumetric_volume_weight;
				//}
				
				////////////////////////
				
				
				
				//$required_volume_in_kg=max($inventory_weight_in_kg,$resultant_inventory_volume_in_kg);
				
				
				/// if parcel category id is there no problem
				$sql="SELECT logistics_price,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg!='yes'";
				
				$query=$this->db->query($sql);
				if(!empty($query->result())){
					$logistics_price=0;
					foreach($query->result() as $resObj){
						$logistics_price=$resObj->logistics_price;
						break;
					}
					//////////////////// Tax caculation starts /////
					$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
					$logistics_gst=$get_logistics_by_logistics_id_obj->logistics_gst;
					$logistics_fuelsurcharge=$get_logistics_by_logistics_id_obj->logistics_fuelsurcharge;
					$logistics_tax=$logistics_gst+$logistics_fuelsurcharge;
					$logistics_price_with_tax=$logistics_price+($logistics_price*($logistics_tax/100));
					//////////////////// Tax caculation ends /////
					return $logistics_price_with_tax;
				}
				
				/// if parcel category id is there no problem
				$sql_additional_weight_in_kg="SELECT logistics_price,volume_in_kg,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg='yes'";
				
				$query_additional_weight_in_kg=$this->db->query($sql_additional_weight_in_kg);
				
				if(!empty($query_additional_weight_in_kg->result())){
					//////////////////
					$sql_take_latest_price_before_breakpoint="SELECT logistics_price,volume_in_kg,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where  territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg!='yes' order by logistics_weight_id desc limit 0,1";
					$query_take_latest_price_before_breakpoint=$this->db->query($sql_take_latest_price_before_breakpoint);
					$logistics_price_latest_price_before_breakpoint=$query_take_latest_price_before_breakpoint->row_array()["logistics_price"];
					$logistics_weight_latest_price_before_breakpoint=$query_take_latest_price_before_breakpoint->row_array()["volume_in_kg"];
					$logistics_weight_latest_price_before_breakpoint_max=explode("-",$logistics_weight_latest_price_before_breakpoint)[1];
					/////////////////
					$logistics_price=0;
					foreach($query_additional_weight_in_kg->result() as $resObj){
						$additional_weight_value_in_kg=$resObj->additional_weight_value_in_kg;
						$do_u_want_to_roundup=$resObj->do_u_want_to_roundup;
						$do_u_want_to_include_breakupweight=$resObj->do_u_want_to_include_breakupweight;
						if($do_u_want_to_roundup=="yes" && $do_u_want_to_include_breakupweight=="yes"){
							$result_value_of_weights=(ceil($required_volume_in_kg-$logistics_weight_latest_price_before_breakpoint_max))/$additional_weight_value_in_kg;
							$logistics_price=$logistics_price_latest_price_before_breakpoint+(($result_value_of_weights)*($resObj->logistics_price));
						}
						if($do_u_want_to_roundup=="yes" && $do_u_want_to_include_breakupweight=="no"){
							$logistics_price=(ceil($required_volume_in_kg)/$additional_weight_value_in_kg)*$resObj->logistics_price;
						}
						if($do_u_want_to_roundup=="no"){
							$logistics_price_part_1=0;
							$logistics_price_part_2=0;
							
							$decimalpart_of_required_volume_in_kg=floor($required_volume_in_kg);
							$fractionalpart_of_required_volume_in_kg=$required_volume_in_kg-$decimalpart_of_required_volume_in_kg;
							$logistics_price_part_1=($decimalpart_of_required_volume_in_kg/$additional_weight_value_in_kg)*$resObj->logistics_price;
							
							$sql_fractional_part="SELECT logistics_price,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$fractionalpart_of_required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$fractionalpart_of_required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg!='yes'";
							$query_fractional_part=$this->db->query($sql_fractional_part);
							if(!empty($query_fractional_part->result())){
								foreach($query_fractional_part->result() as $resObj_fractional_part){
									$logistics_price_part_2=$resObj_fractional_part->logistics_price;
									break;
								}
							}
							$logistics_price=$logistics_price_part_1+$logistics_price_part_2;
							
						}
						break;
					}
					//////////////////// Tax caculation starts /////
					$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
					$logistics_gst=$get_logistics_by_logistics_id_obj->logistics_gst;
					$logistics_fuelsurcharge=$get_logistics_by_logistics_id_obj->logistics_fuelsurcharge;
					$logistics_tax=$logistics_gst+$logistics_fuelsurcharge;
					$logistics_price_with_tax=$logistics_price+($logistics_price*($logistics_tax/100));
					//////////////////// Tax caculation ends /////
					return $logistics_price_with_tax;
				}
				else{
					
					///////////
					/// if parcel category id is not there then take the 1 which is link with logistics weight and calculate logistics price
					$sql1="SELECT logistics_id,logistics_parcel_category_id,logistics_price,volume_in_kg FROM logistics_weight where SUBSTRING_INDEX(weight_in_kg, '-', 1) <=$required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(weight_in_kg, '-', 2), '-', -1) >=$required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' order by volume_in_kg";
					$query1=$this->db->query($sql1);
					if($query1->num_rows()==1){
						$arr=$query1->row_array();
						return $arr["logistics_price"];
					}
					//////////////
					
					
					
					
					
				$sql1="SELECT logistics_id,logistics_parcel_category_id,logistics_price,volume_in_kg,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id'  and additional_weight_in_kg!='yes'";
				
				$query1=$this->db->query($sql1);
				if($query1->num_rows()==1){
					$logistics_price=0;
					foreach($query1->result() as $resObj){
						$logistics_price=$resObj->logistics_price;
						break;
					}
					//////////////////// Tax caculation starts /////
					$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
					$logistics_gst=$get_logistics_by_logistics_id_obj->logistics_gst;
					$logistics_fuelsurcharge=$get_logistics_by_logistics_id_obj->logistics_fuelsurcharge;
					$logistics_tax=$logistics_gst+$logistics_fuelsurcharge;
					$logistics_price_with_tax=$logistics_price+($logistics_price*($logistics_tax/100));
					//////////////////// Tax caculation ends /////
					return $logistics_price_with_tax;
				}
				
				
				$sql_additional_weight_in_kg="SELECT logistics_id,logistics_parcel_category_id,logistics_price,volume_in_kg,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id'  and additional_weight_in_kg='yes'";
				
				$query_additional_weight_in_kg=$this->db->query($sql_additional_weight_in_kg);
				
				if($query_additional_weight_in_kg->num_rows()==1){
					//////////////////
					$sql_take_latest_price_before_breakpoint="SELECT logistics_id,logistics_parcel_category_id,logistics_price,volume_in_kg,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where  territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and additional_weight_in_kg!='yes' order by logistics_weight_id desc limit 0,1";
					$query_take_latest_price_before_breakpoint=$this->db->query($sql_take_latest_price_before_breakpoint);
					$logistics_price_latest_price_before_breakpoint=$query_take_latest_price_before_breakpoint->row_array()["logistics_price"];
					$logistics_weight_latest_price_before_breakpoint=$query_take_latest_price_before_breakpoint->row_array()["volume_in_kg"];
					$logistics_weight_latest_price_before_breakpoint_max=explode("-",$logistics_weight_latest_price_before_breakpoint)[1];
					/////////////////
					$logistics_price=0;
					foreach($query_additional_weight_in_kg->result() as $resObj){
						$additional_weight_value_in_kg=$resObj->additional_weight_value_in_kg;
						$do_u_want_to_roundup=$resObj->do_u_want_to_roundup;
						$do_u_want_to_include_breakupweight=$resObj->do_u_want_to_include_breakupweight;
						if($do_u_want_to_roundup=="yes" && $do_u_want_to_include_breakupweight=="yes"){
							$result_value_of_weights=(ceil($required_volume_in_kg-$logistics_weight_latest_price_before_breakpoint_max))/$additional_weight_value_in_kg;
							$logistics_price=$logistics_price_latest_price_before_breakpoint+(($result_value_of_weights)*($resObj->logistics_price));
						}
						if($do_u_want_to_roundup=="yes" && $do_u_want_to_include_breakupweight=="no"){
							$logistics_price=(ceil($required_volume_in_kg)/$additional_weight_value_in_kg)*$resObj->logistics_price;
						}
						if($do_u_want_to_roundup=="no"){
							$logistics_price_part_1=0;
							$logistics_price_part_2=0;
							
							$decimalpart_of_required_volume_in_kg=floor($required_volume_in_kg);
							$fractionalpart_of_required_volume_in_kg=$required_volume_in_kg-$decimalpart_of_required_volume_in_kg;
							$logistics_price_part_1=($decimalpart_of_required_volume_in_kg/$additional_weight_value_in_kg)*$resObj->logistics_price;
							
							$sql_fractional_part="SELECT logistics_id,logistics_parcel_category_id,logistics_price,volume_in_kg,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$fractionalpart_of_required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$fractionalpart_of_required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id'  and additional_weight_in_kg!='yes'";
							$query_fractional_part=$this->db->query($sql_fractional_part);
							if(!empty($query_fractional_part->result())){
								foreach($query_fractional_part->result() as $resObj_fractional_part){
									$logistics_price_part_2=$resObj_fractional_part->logistics_price;
									break;
								}
							}
							$logistics_price=$logistics_price_part_1+$logistics_price_part_2;
							
						}
						break;
					}
					//////////////////// Tax caculation starts /////
					$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
					$logistics_gst=$get_logistics_by_logistics_id_obj->logistics_gst;
					$logistics_fuelsurcharge=$get_logistics_by_logistics_id_obj->logistics_fuelsurcharge;
					$logistics_tax=$logistics_gst+$logistics_fuelsurcharge;
					$logistics_price_with_tax=$logistics_price+($logistics_price*($logistics_tax/100));
					//////////////////// Tax caculation ends /////
					return $logistics_price_with_tax;
				}
				
				
				
					
					else{
						/// if multiple parcel categories id are there then take the default one from logistics parcel category table and query the logistic weight for logistics price
						$query_arr=$query1->result_array();
						foreach($query_arr as $varr){
							$logistics_id=$varr["logistics_id"];
							$logistics_parcel_category_id=$varr["logistics_parcel_category_id"];
							$check_default_parcel_category_or_not_status=$this->check_default_parcel_category_or_not($logistics_id,$logistics_parcel_category_id);
							if($check_default_parcel_category_or_not_status=="yes"){
								$default_logistics_parcel_category_id=$logistics_parcel_category_id;
								break;
							}
						}
						if(!isset($default_logistics_parcel_category_id)){
							return 0;
						}
						
						//$sql2="SELECT logistics_id,logistics_parcel_category_id,logistics_price,volume_in_kg FROM logistics_weight where SUBSTRING_INDEX(weight_in_kg, '-', 1) <=$required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(weight_in_kg, '-', 2), '-', -1) >=$required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$default_logistics_parcel_category_id' order by volume_in_kg";
						//$query2=$this->db->query($sql2);
						$logistics_price=0;
						//foreach($query2->result() as $resObj){
						//	$logistics_price=$resObj->logistics_price;
						//	break;
						//}
						//return $logistics_price;
						
						
						
				$sql2="SELECT logistics_price,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$default_logistics_parcel_category_id' and additional_weight_in_kg!='yes'";
				
				$query2=$this->db->query($sql2);
				if(!empty($query2->result())){
					$logistics_price=0;
					foreach($query2->result() as $resObj){
						$logistics_price=$resObj->logistics_price;
						break;
					}
					//////////////////// Tax caculation starts /////
					$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
					$logistics_gst=$get_logistics_by_logistics_id_obj->logistics_gst;
					$logistics_fuelsurcharge=$get_logistics_by_logistics_id_obj->logistics_fuelsurcharge;
					$logistics_tax=$logistics_gst+$logistics_fuelsurcharge;
					$logistics_price_with_tax=$logistics_price+($logistics_price*($logistics_tax/100));
					//////////////////// Tax caculation ends /////
					return $logistics_price_with_tax;
				}
				
				
				$sql_additional_weight_in_kg="SELECT logistics_price,volume_in_kg,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$default_logistics_parcel_category_id' and additional_weight_in_kg='yes'";
				
				$query_additional_weight_in_kg=$this->db->query($sql_additional_weight_in_kg);
				if(!empty($query_additional_weight_in_kg->result())){
					//////////////////
					$sql_take_latest_price_before_breakpoint="SELECT logistics_price,volume_in_kg,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where  territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$default_logistics_parcel_category_id' and additional_weight_in_kg!='yes' order by logistics_weight_id desc limit 0,1";
					$query_take_latest_price_before_breakpoint=$this->db->query($sql_take_latest_price_before_breakpoint);
					$logistics_price_latest_price_before_breakpoint=$query_take_latest_price_before_breakpoint->row_array()["logistics_price"];
					$logistics_weight_latest_price_before_breakpoint=$query_take_latest_price_before_breakpoint->row_array()["volume_in_kg"];
					$logistics_weight_latest_price_before_breakpoint_max=explode("-",$logistics_weight_latest_price_before_breakpoint)[1];
					/////////////////
					$logistics_price=0;
					foreach($query_additional_weight_in_kg->result() as $resObj){
						$additional_weight_value_in_kg=$resObj->additional_weight_value_in_kg;
						$do_u_want_to_roundup=$resObj->do_u_want_to_roundup;
						$do_u_want_to_include_breakupweight=$resObj->do_u_want_to_include_breakupweight;
						if($do_u_want_to_roundup=="yes" && $do_u_want_to_include_breakupweight=="yes"){
							$result_value_of_weights=(ceil($required_volume_in_kg-$logistics_weight_latest_price_before_breakpoint_max))/$additional_weight_value_in_kg;
							$logistics_price=$logistics_price_latest_price_before_breakpoint+(($result_value_of_weights)*($resObj->logistics_price));
						}
						if($do_u_want_to_roundup=="yes" && $do_u_want_to_include_breakupweight=="no"){
							$logistics_price=(ceil($required_volume_in_kg)/$additional_weight_value_in_kg)*$resObj->logistics_price;
						}
						if($do_u_want_to_roundup=="no"){
							$logistics_price_part_1=0;
							$logistics_price_part_2=0;
							
							$decimalpart_of_required_volume_in_kg=floor($required_volume_in_kg);
							$fractionalpart_of_required_volume_in_kg=$required_volume_in_kg-$decimalpart_of_required_volume_in_kg;
							$logistics_price_part_1=($decimalpart_of_required_volume_in_kg/$additional_weight_value_in_kg)*$resObj->logistics_price;
							
							$sql_fractional_part="SELECT logistics_price,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$fractionalpart_of_required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$fractionalpart_of_required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$default_logistics_parcel_category_id' and additional_weight_in_kg!='yes'";
							$query_fractional_part=$this->db->query($sql_fractional_part);
							if(!empty($query_fractional_part->result())){
								foreach($query_fractional_part->result() as $resObj_fractional_part){
									$logistics_price_part_2=$resObj_fractional_part->logistics_price;
									break;
								}
							}
							$logistics_price=$logistics_price_part_1+$logistics_price_part_2;
							
						}
						break;
					}
					//////////////////// Tax caculation starts /////
					$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
					$logistics_gst=$get_logistics_by_logistics_id_obj->logistics_gst;
					$logistics_fuelsurcharge=$get_logistics_by_logistics_id_obj->logistics_fuelsurcharge;
					$logistics_tax=$logistics_gst+$logistics_fuelsurcharge;
					$logistics_price_with_tax=$logistics_price+($logistics_price*($logistics_tax/100));
					//////////////////// Tax caculation ends /////
					return $logistics_price_with_tax;
				}
				
				
						
					}
					
				}
				//////////////////// Tax caculation starts /////
					$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
					$logistics_gst=$get_logistics_by_logistics_id_obj->logistics_gst;
					$logistics_fuelsurcharge=$get_logistics_by_logistics_id_obj->logistics_fuelsurcharge;
					$logistics_tax=$logistics_gst+$logistics_fuelsurcharge;
					$logistics_price_with_tax=$logistics_price+($logistics_price*($logistics_tax/100));
					//////////////////// Tax caculation ends /////
				return $logistics_price_with_tax;*/
				
		}
		
	public function remove_cart_info_for_session_customer($remove_cart_info_post_data){
		$customer_id=$this->session->userdata("customer_id");
		$sql="delete from carttable where cart_id='$remove_cart_info_post_data->cart_id' and customer_id='$customer_id'";
		$res=$this->db->query($sql);
		return $res;
	}
	public function remove_cart_info_for_session_customer_addon($remove_cart_info_post_data){
		$customer_id=$this->session->userdata("customer_id");

		$cart_id=$remove_cart_info_post_data->cart_id;

		$new_addon_inventories=$remove_cart_info_post_data->new_addon_inventories;

		$addon_total_price=$remove_cart_info_post_data->addon_total_price;
		$new_addon_inventories=$remove_cart_info_post_data->addon_inventories;

		$addon_products=$remove_cart_info_post_data->addon_products;
		$addon_products_status=$remove_cart_info_post_data->addon_products_status;
		$addon_total_shipping_price=$remove_cart_info_post_data->addon_total_shipping_price;

		$sql_update="update carttable set addon_inventories='$new_addon_inventories',addon_total_price='$addon_total_price',addon_products_status='$addon_products_status',addon_products='$addon_products',addon_total_shipping_price='$addon_total_shipping_price' where cart_id='$cart_id' and customer_id='$customer_id' ";

		$res=$this->db->query($sql_update);
		return $res;

		
	}
	public function get_all_products_from_wishlist()
	{
		$customer_id=$this->session->userdata("customer_id");
		$sql="select wishlist.*,wishlist.id as w_id,inventory.* from wishlist,inventory where wishlist.customer_id='$customer_id' and inventory.id=wishlist.inventory_id";
		$res=$this->db->query($sql);
		return $res->result_array();
	}
	public function remove_wishlist($wishlist_id){
		$customer_id=$this->session->userdata("customer_id");
		$sql="delete from wishlist where customer_id='$customer_id' and id='$wishlist_id'";
		$res=$this->db->query($sql);
		return $res;
	}
	public function get_cartinfo_for_customer(){
		$customer_id=$this->session->userdata("customer_id");
		$sql="select carttable.*,logistics_delivery_mode.delivery_mode,logistics_parcel_category.parcel_type,logistics.logistics_name,logistics.logistics_weblink from carttable,logistics_delivery_mode,logistics_parcel_category,logistics where carttable.logistics_parcel_category_id=logistics_parcel_category.logistics_parcel_category_id and logistics_delivery_mode.logistics_delivery_mode_id=carttable.default_delivery_mode_id and logistics.logistics_id=carttable.logistics_id and customer_id='$customer_id'";
		
		//$sql="select * from carttable where customer_id='$customer_id'";
		$res=$this->db->query($sql);
		return $res->result();
	}
	public function get_customer_default_id(){
		$customer_id=$this->session->userdata("customer_id");
		$sql_customer_default_id="select customer_address_id from customer_address where customer_id='$customer_id' and make_default='1'";
		$res=$this->db->query($sql_customer_default_id);
		return $res->row()->customer_address_id;
	}
	public function add_shipping_address($customer_address_id){
		$customer_id=$this->session->userdata("customer_id");
		$sql="select * from customer_address where customer_address_id='$customer_address_id' and customer_id='$customer_id'";
		$res=$this->db->query($sql);
		$customer_id=$res->row()->customer_id;
		$customer_name=$res->row()->customer_name;
		$address1=addslashes($res->row()->address1);
		$address2=addslashes($res->row()->address2);
		$country=$res->row()->country;
		$state=$res->row()->state;
		$city=$res->row()->city;
		$pincode=$res->row()->pincode;
		$mobile=$res->row()->mobile;
		
		$sql_shipping_address="insert into shipping_address set customer_id='$customer_id',customer_name='$customer_name',address1='$address1',address2='$address2',country='$country',state='$state',city='$city',pincode='$pincode',mobile='$mobile'";
		$result_shipping_address=$this->db->query($sql_shipping_address);
		return $this->db->insert_id();
	}
	public function insert_temp_payment_post_data($payment_post_data_json,$order_id,$combo_checkout=''){
		$customer_id=$this->session->userdata("customer_id");
		//,checkout_type='$combo_checkout'
		$sql="insert into temp_payment_post_data set payment_post_data_json='$payment_post_data_json',customer_id='$customer_id',order_id='$order_id'";
		$this->db->query($sql);
	}
	public function get_temp_payment_post_data($order_id){
		$customer_id=$this->session->userdata("customer_id");
		$sql="select * from temp_payment_post_data where order_id='$order_id' and customer_id='$customer_id'";
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	public function delete_temp_payment_post_data($order_id){
		$customer_id=$this->session->userdata("customer_id");
		$sql="delete from temp_payment_post_data where order_id='$order_id' and customer_id='$customer_id'";
		$result=$this->db->query($sql);
	}
	public function failed_order_process($result,$order_id,$payment_post_data,$razorpayPaymentId='',$razorpayOrderId=''){
		$customer_address_id=$this->get_customer_default_id();
		$shipping_address_id=$this->add_shipping_address($customer_address_id);
		foreach($result as $resObj){
			
			$customer_id=$resObj->customer_id;
			$purchased_price=$resObj->purchased_price;
			$inventory_id=$resObj->inventory_id;

			$product_id=$resObj->product_id;
			$sku_id=$resObj->inventory_sku;
			
			$logistics_id=$resObj->logistics_id;
			$logistics_parcel_category_id=$resObj->logistics_parcel_category_id;
			$logistics_delivery_mode_id=$resObj->default_delivery_mode_id;
			$logistics_territory_id=$resObj->logistics_territory_id;
			
			$resObj->product_name;
			$resObj->inventory_selling_price_with_out_discount;
			$resObj->product_description;
			
			$resObj->attribute_1;
			$resObj->attribute_2;
			$resObj->attribute_3;
			$resObj->attribute_4;
			$resObj->attribute_1_value;
			$resObj->attribute_2_value;
			$resObj->attribute_3_value;
			$resObj->attribute_4_value;
			$quantity=$resObj->inventory_moq;
			$resObj->product_status;
			$image=$resObj->inventory_image;
			
			//$product_price=$resObj->inventory_selling_price_with_discount_original;
			$product_price=$resObj->selling_price;
                        $max_selling_price=$resObj->max_selling_price;
                        $selling_discount=$resObj->selling_discount;
                        $tax_percent_price=$resObj->tax_percent_price;
                        $taxable_price=$resObj->taxable_price;
			//$order_id=date("dmHi".$this->session->userdata("customer_id"));
			//$grandtotal=(($resObj->selling_price)*($resObj->inventory_moq))+(($resObj->inventory_moq)*($resObj->shipping_charge));

			$random_number=rand(100,10000);

			$payment_method=$payment_post_data->payment_method;

            $razorpayPaymentId=(strtolower($payment_method)=='razorpay') ? $razorpayPaymentId : '';
            $razorpayOrderId=(strtolower($payment_method)=='razorpay') ? $razorpayOrderId : '';


            $payment_type=$payment_method;
			$payment_status="no";
			$tax_percent=$resObj->tax_percent;
			
			$shipping_charge=$resObj->shipping_charge;
			$delivery_mode=$resObj->delivery_mode;
			$parcel_type=$resObj->parcel_type;
			$date = strtotime("+".$resObj->territory_duration." days", strtotime(date('Y-m-d h:i:s')) );
			$expected_delivery_date=date("D j M Y",$date);
			$logistics_name=$resObj->logistics_name;
			$logistics_weblink=$resObj->logistics_weblink;
			$return_period=$resObj->return_period;
			$rand=mt_rand(1,1000000);
			$invoice_number='#BLR_'.date("ym".date("d")).$rand;
	
			$vendor_id=$this->get_vendor_id_by_inventory_id($inventory_id);
			
			$promotion_available=$resObj->promotion_available;
			$promotion_residual=$resObj->promotion_residual;
			$promotion_discount=$resObj->promotion_discount;
			$promotion_cashback=$resObj->promotion_cashback;
			$promotion_surprise_gift=$resObj->promotion_surprise_gift;
			$promotion_surprise_gift_type=$resObj->promotion_surprise_gift_type;
			$promotion_surprise_gift_skus=$resObj->promotion_surprise_gift_skus;
			$promotion_surprise_gift_skus_nums=$resObj->promotion_surprise_gift_skus_nums;
			$default_discount=$resObj->default_discount;
			$promotion_item_multiplier=$resObj->promotion_item_multiplier;
			$promotion_item=$resObj->promotion_item;
			$promotion_item_num=$resObj->promotion_item_num;
			$promotion_clubed_with_default_discount=$resObj->promotion_clubed_with_default_discount;
			$promotion_id_selected=$resObj->promotion_id_selected;
			$promotion_type_selected=$resObj->promotion_type_selected;
			$promotion_minimum_quantity=$resObj->promotion_minimum_quantity;
			$promotion_quote=$resObj->promotion_quote;
			$promotion_default_discount_promo=$resObj->promotion_default_discount_promo;
			
			$quantity_with_promotion=$resObj->quantity_with_promotion;
			$quantity_without_promotion=$resObj->quantity_without_promotion;
            
            $individual_price_of_product_with_promotion=$resObj->individual_price_of_product_with_promotion;
            $individual_price_of_product_without_promotion=$resObj->individual_price_of_product_without_promotion;
            $total_price_of_product_without_promotion=$resObj->total_price_of_product_without_promotion;
            $total_price_of_product_with_promotion=$resObj->total_price_of_product_with_promotion;
            $cash_back_value=$resObj->cash_back_value;
            
			
			if($promotion_available==1){
				if($total_price_of_product_with_promotion!="" || $total_price_of_product_without_promotion!=""){		
					$subtotal=($total_price_of_product_with_promotion+$total_price_of_product_without_promotion);
					$grandtotal=($total_price_of_product_with_promotion+$total_price_of_product_without_promotion+$shipping_charge);  
				}else{
					$subtotal=($product_price*$quantity);
					$grandtotal=(($product_price*$quantity)+$shipping_charge);
				}
			
			}else{
				$subtotal=($product_price*$quantity);
				$grandtotal=(($product_price*$quantity)+$shipping_charge);
			}
			
			//$subtotal=(($resObj->selling_price)*($resObj->inventory_moq));
			//$grandtotal=(($resObj->selling_price)*($resObj->inventory_moq))+($resObj->shipping_charge);
			
			$ord_addon_products_status=$resObj->addon_products_status;
			$ord_addon_products=$resObj->addon_products;
			
			$ord_addon_single_or_multiple_tagged_inventories_in_frontend=$resObj->addon_single_or_multiple_tagged_inventories_in_frontend;
			$ord_addon_master_order_item_id=$resObj->addon_master_order_item_id;
			
			
			
			
			
			
			if($ord_addon_products=='"[]"'){
				$ord_addon_products='[]';
			}

			$sql="insert into failed_orders set purchased_price='$purchased_price',customer_id='$customer_id',order_id='$order_id',shipping_address_id='$shipping_address_id',inventory_id='$inventory_id',product_id='$product_id',sku_id='$sku_id',product_price='$product_price',ord_max_selling_price='$max_selling_price',ord_selling_discount='$selling_discount',ord_tax_percent_price='$tax_percent_price',ord_taxable_price='$taxable_price',subtotal='$subtotal',grandtotal='$grandtotal',quantity='$quantity',timestamp=now(),image='$image',random_number='$random_number',payment_type='$payment_type',payment_status='$payment_status',tax_percent='$tax_percent',shipping_charge='$shipping_charge',invoice_number='$invoice_number',return_period='$return_period',vendor_id='$vendor_id',logistics_name='$logistics_name',logistics_weblink='$logistics_weblink',expected_delivery_date='$expected_delivery_date',delivery_mode='$delivery_mode',parcel_category='$parcel_type',logistics_id='$logistics_id',logistics_delivery_mode_id='$logistics_delivery_mode_id',logistics_territory_id='$logistics_territory_id',logistics_parcel_category_id='$logistics_parcel_category_id',promotion_available='$promotion_available',promotion_residual='$promotion_residual',promotion_discount='$promotion_discount',promotion_cashback='$promotion_cashback',promotion_surprise_gift='$promotion_surprise_gift',promotion_surprise_gift_type='$promotion_surprise_gift_type',promotion_surprise_gift_skus='$promotion_surprise_gift_skus',promotion_surprise_gift_skus_nums='$promotion_surprise_gift_skus_nums',default_discount='$default_discount',promotion_item_multiplier='$promotion_item_multiplier',promotion_item='$promotion_item',promotion_item_num='$promotion_item_num',promotion_clubed_with_default_discount='$promotion_clubed_with_default_discount',promotion_id_selected='$promotion_id_selected',promotion_type_selected='$promotion_type_selected',promotion_minimum_quantity='$promotion_minimum_quantity',promotion_quote='$promotion_quote',promotion_default_discount_promo='$promotion_default_discount_promo',quantity_with_promotion='$quantity_with_promotion',quantity_without_promotion='$quantity_without_promotion',individual_price_of_product_with_promotion='$individual_price_of_product_with_promotion',individual_price_of_product_without_promotion='$individual_price_of_product_without_promotion',total_price_of_product_without_promotion='$total_price_of_product_without_promotion',total_price_of_product_with_promotion='$total_price_of_product_with_promotion',cash_back_value='$cash_back_value',razorpayPaymentId='$razorpayPaymentId',razorpayOrderId='$razorpayOrderId',ord_addon_products_status='$ord_addon_products_status',ord_addon_products='$ord_addon_products',ord_addon_single_or_multiple_tagged_inventories_in_frontend='$ord_addon_single_or_multiple_tagged_inventories_in_frontend',ord_addon_master_order_item_id='$ord_addon_master_order_item_id'";
			
			$flag=$this->db->query($sql);
		}
	}
	public function confirm_order_process($result,$order_id,$payment_post_data,$surprise_gift_arr,$razorpayPaymentId='',$razorpayOrderId='',$cur_checkout=''){
		
                // echo '<pre>';
                // print_r($result);
                // print_r($payment_post_data);
                // echo '</pre>';

                // exit;
                
                
		$customer_address_id=$this->get_customer_default_id();
		$shipping_address_id=$this->add_shipping_address($customer_address_id);
//print_r($result);exit;
	    $count_carttable=count($result);
		$customer_id=$this->session->userdata("customer_id");
		
		if(isset($payment_post_data->promotion_invoice_cash_back)){
			$promotion_invoice_cash_back=$payment_post_data->promotion_invoice_cash_back;
			$promotion_invoice_cashback_quote=$payment_post_data->promotion_invoice_cashback_quote;
		}else{
			$promotion_invoice_cash_back=0;
			$promotion_invoice_cashback_quote="";
		}

		$promotion_invoice_free_shipping=$payment_post_data->promotion_invoice_free_shipping;
        $promotion_invoice_discount=$payment_post_data->promotion_invoice_discount;
        $total_amount_cash_back=$payment_post_data->total_amount_cash_back;
        $total_amount_saved=$payment_post_data->total_amount_saved;
        $total_amount_to_pay=$payment_post_data->total_amount_to_pay;
		$payment_method=$payment_post_data->payment_method;
        $invoice_offer_achieved=$payment_post_data->invoice_offer_achieved;
        $wallet_amount=$payment_post_data->wallet_amount;
        $total_price_without_shipping_price=$payment_post_data->total_price_without_shipping_price;
		$total_shipping_charge=$payment_post_data->total_shipping_charge;
		$total_coupon_used_amount=isset($payment_post_data->total_coupon_used_amount) ? $payment_post_data->total_coupon_used_amount : 0;

		$inv_addon_products_amount=isset($payment_post_data->inv_addon_products_amount) ? $payment_post_data->inv_addon_products_amount : 0;

                
                /* for coupon data added */
                $cp_data=array();
                $temp_data=$payment_post_data->products_coupon;
                if(!empty($temp_data)){
                    foreach($temp_data as $v){
                        
                        if(intval($v->coupon_applied_status)==1){
                        	$cp_data["$v->inventory_id"]=array('coupon_code_appied'=>$v->coupon_code_appied,'coupon_name_appied'=>$v->coupon_name_appied,'coupon_applied_status'=>$v->coupon_applied_status,'coupon_type_appied'=>$v->coupon_type_appied,'coupon_value_appied'=>$v->coupon_value_appied,'coupon_reduce_price'=>$v->coupon_reduce_price);
                        }
                    }
                }
                
                $invoice_coupon=$payment_post_data->invoice_coupon;
                $invoice_coupon_txt='';
                if(!empty($invoice_coupon)){
                    if(isset($invoice_coupon->invoice_coupon_applied_status)){
                        $invoice_coupon_applied_status=$invoice_coupon->invoice_coupon_applied_status;
                        $invoice_coupon_code_appied=$invoice_coupon->invoice_coupon_code_appied;
                        $invoice_coupon_name_appied=$invoice_coupon->invoice_coupon_name_appied;
                        $invoice_coupon_type_appied=$invoice_coupon->invoice_coupon_type_appied;
                        $invoice_coupon_value_appied=$invoice_coupon->invoice_coupon_value_appied;
                        $invoice_coupon_reduce_price=$invoice_coupon->invoice_coupon_reduce_price;

                        $invoice_coupon_txt=" ,invoice_coupon_status='$invoice_coupon_applied_status',invoice_coupon_code='$invoice_coupon_code_appied',invoice_coupon_name='$invoice_coupon_name_appied',invoice_coupon_type='$invoice_coupon_type_appied',invoice_coupon_value='$invoice_coupon_value_appied',invoice_coupon_reduce_price='$invoice_coupon_reduce_price' ";
                    }
                    
                }else{
                    $invoice_coupon_applied_status=0;
                    $invoice_coupon_code_appied='';
                    $invoice_coupon_name_appied='';
                    $invoice_coupon_type_appied='';
                    $invoice_coupon_reduce_price=0;
                }
                /* for coupon data added */
                
                //print_r($temp_data);
                //print_r($cp_data);
                
                //exit;
                
        $razorpayPaymentId=(strtolower($payment_method)=='razorpay') ? $razorpayPaymentId : '';
        $razorpayOrderId=(strtolower($payment_method)=='razorpay') ? $razorpayOrderId : '';

		if($payment_method=="Wallet"){
		    $sql_wallet_check="select * from wallet where customer_id='$customer_id'";
             $result_d=$this->db->query($sql_wallet_check);
            if($result_d->num_rows()>0){
                $wallet_id=$result_d->row()->wallet_id;
                $wallet_amount=$result_d->row()->wallet_amount;
            }
		    $new_wallet_amount_after_transaction=$wallet_amount-$total_amount_to_pay;
            $sql_update="update wallet set wallet_amount='$new_wallet_amount_after_transaction' where wallet_id='$wallet_id' and customer_id='$customer_id'";
                $this->db->query($sql_update);
                
                
                $sql_for_wallet_transaction="insert into wallet_transaction set transaction_details='cash back debited from your wallet <br> against the order number $order_id', debit='$total_amount_to_pay' , credit='',amount='$new_wallet_amount_after_transaction',wallet_id='$wallet_id',customer_id='$customer_id'";
            
           $this->db->query($sql_for_wallet_transaction);
		}
		//print_r($surprise_gift_arr);
		
		//exit;
		
		if(!empty($surprise_gift_arr)){
			
			$invoice_surprise_gift_promo_quote=$surprise_gift_arr["promo_quote"];
			$invoice_surprise_gift=$surprise_gift_arr["to_get"];
			$invoice_surprise_gift_type=$surprise_gift_arr["get_type"];
			$invoice_surprise_gift_skus=$surprise_gift_arr["free_inv"];
			$invoice_surprise_gift_skus_nums=$surprise_gift_arr["free_inv_num"];
		}else{
			$invoice_surprise_gift_promo_quote="";
			$invoice_surprise_gift="";
			$invoice_surprise_gift_type="";
			$invoice_surprise_gift_skus="";
			$invoice_surprise_gift_skus_nums="";
		}
        
                $in_cp_text='';
                
                
                
        $sql_inv_offers="insert into invoices_offers set promotion_invoice_cash_back='$promotion_invoice_cash_back', promotion_invoice_free_shipping='$promotion_invoice_free_shipping' ,promotion_invoice_discount='$promotion_invoice_discount',total_amount_cash_back='$total_amount_cash_back',total_amount_saved='$total_amount_saved',total_amount_to_pay='$total_amount_to_pay',payment_method='$payment_method',invoice_offer_achieved='$invoice_offer_achieved',order_id='$order_id',customer_id='$customer_id',shipping_address_id='$shipping_address_id',invoice_surprise_gift_promo_quote='$invoice_surprise_gift_promo_quote',invoice_surprise_gift='$invoice_surprise_gift',invoice_surprise_gift_type='$invoice_surprise_gift_type',invoice_surprise_gift_skus='$invoice_surprise_gift_skus' ,invoice_surprise_gift_skus_nums='$invoice_surprise_gift_skus_nums',cart_quantity='$count_carttable',promotion_invoice_cashback_quote='$promotion_invoice_cashback_quote',total_price_without_shipping_price='$total_price_without_shipping_price',total_shipping_charge='$total_shipping_charge',total_coupon_used_amount='$total_coupon_used_amount',inv_addon_products_amount='$inv_addon_products_amount' $invoice_coupon_txt ";
        
        $this->db->query($sql_inv_offers);
        
		$invoices_offers_id=$this->db->insert_id();
		
		if($invoice_surprise_gift_skus!=''){
			$free_sku_arr=explode(',',$invoice_surprise_gift_skus);
			$free_sku_quan_arr=explode(',',$invoice_surprise_gift_skus_nums);
			
			$n=0;
			foreach($free_sku_arr as $free_sku_id){
				$quantity=$free_sku_quan_arr[$n];
				$sku_id=$this->get_inventory_sku_id($free_sku_id);
				$vendor_id=$this->get_vendor_id_by_inventory_id($free_sku_id);
				$logistics_id=$this->get_default_logistics($vendor_id);
				$logistics_parcel_category_id=$this->get_default_parcel_category();				
				$logistics_delivery_mode_id=$this->default_delivery_mode();		
				
				$sql_free_sku="insert into invoices_free_items_status set invoices_offers_id='$invoices_offers_id',inventory_id='$free_sku_id',sku_id='$sku_id',quantity='$quantity',vendor_id='$vendor_id',logistics_id='$logistics_id',logistics_parcel_category_id='$logistics_parcel_category_id',logistics_delivery_mode_id='$logistics_delivery_mode_id',shipping_address_id='$shipping_address_id'";
				 
				$this->db->query($sql_free_sku);	 
				$n++;
			}
			
		}

		$count_active_orders=array();
		
                
		foreach($result as $resObj){
			
			$customer_id=$resObj->customer_id;
			$purchased_price=$resObj->purchased_price;
			$inventory_id=$resObj->inventory_id;

			/* get product_vendor_id */
			$product_vendor_id=get_product_vendor_id($inventory_id);
			echo "product_vendor_id=".$product_vendor_id;
			
			/* get product_vendor_id */

			$product_id=$resObj->product_id;
			$sku_id=$resObj->inventory_sku;
			
			$logistics_id=$resObj->logistics_id;
			$logistics_parcel_category_id=$resObj->logistics_parcel_category_id;
			$logistics_delivery_mode_id=$resObj->default_delivery_mode_id;
			$logistics_territory_id=$resObj->logistics_territory_id;
			
			$resObj->product_name;
			$resObj->inventory_selling_price_with_out_discount;
			$resObj->product_description;
			
			$resObj->attribute_1;
			$resObj->attribute_2;
			$resObj->attribute_3;
			$resObj->attribute_4;
			$resObj->attribute_1_value;
			$resObj->attribute_2_value;
			$resObj->attribute_3_value;
			$resObj->attribute_4_value;
			$quantity=$resObj->inventory_moq;
			$resObj->product_status;
			$image=$resObj->inventory_image;
			
			//$product_price=$resObj->inventory_selling_price_with_discount_original;
			
			$product_price=$resObj->selling_price;
			
			$max_selling_price=$resObj->max_selling_price;
			$selling_discount=$resObj->selling_discount;
			$tax_percent_price=$resObj->tax_percent_price;
			$taxable_price=$resObj->taxable_price;
                        
			//$order_id=date("dmHi".$this->session->userdata("customer_id"));
			//$grandtotal=(($resObj->selling_price)*($resObj->inventory_moq))+(($resObj->inventory_moq)*($resObj->shipping_charge));

			$random_number=rand(100,10000);

			$payment_type=$payment_method;
			$payment_status="no";
			$tax_percent=$resObj->tax_percent;
			
			$shipping_charge=$resObj->shipping_charge;
			$delivery_mode=$resObj->delivery_mode;
			$parcel_type=$resObj->parcel_type;
			$date = strtotime("+".$resObj->territory_duration." days", strtotime(date('Y-m-d h:i:s')) );
			$expected_delivery_date=date("D j M Y",$date);
			$logistics_name=$resObj->logistics_name;
			$logistics_weblink=$resObj->logistics_weblink;
			$return_period=$resObj->return_period;
			$rand=mt_rand(1,1000000);
			//$invoice_number='#BLR_'.date("ym".date("d")).$rand;
			$invoice_number=$this->get_invoice_number();
	
			$vendor_id=$this->get_vendor_id_by_inventory_id($inventory_id);
			
			$promotion_available=$resObj->promotion_available;
			$promotion_residual=$resObj->promotion_residual;
			$promotion_discount=$resObj->promotion_discount;
			$promotion_cashback=$resObj->promotion_cashback;
			$promotion_surprise_gift=$resObj->promotion_surprise_gift;
			$promotion_surprise_gift_type=$resObj->promotion_surprise_gift_type;
			$promotion_surprise_gift_skus=$resObj->promotion_surprise_gift_skus;
			$promotion_surprise_gift_skus_nums=$resObj->promotion_surprise_gift_skus_nums;
			$default_discount=$resObj->default_discount;
			$promotion_item_multiplier=$resObj->promotion_item_multiplier;
			$promotion_item=$resObj->promotion_item;
			$promotion_item_num=$resObj->promotion_item_num;
			$promotion_clubed_with_default_discount=$resObj->promotion_clubed_with_default_discount;
			$promotion_id_selected=$resObj->promotion_id_selected;
			$promotion_type_selected=$resObj->promotion_type_selected;
			$promotion_minimum_quantity=$resObj->promotion_minimum_quantity;
			$promotion_quote=$resObj->promotion_quote;
			$promotion_default_discount_promo=$resObj->promotion_default_discount_promo;
			
			$quantity_with_promotion=$resObj->quantity_with_promotion;
			$quantity_without_promotion=$resObj->quantity_without_promotion;
            
            $individual_price_of_product_with_promotion=$resObj->individual_price_of_product_with_promotion;
            $individual_price_of_product_without_promotion=$resObj->individual_price_of_product_without_promotion;
            $total_price_of_product_without_promotion=$resObj->total_price_of_product_without_promotion;
            $total_price_of_product_with_promotion=$resObj->total_price_of_product_with_promotion;
            $cash_back_value=$resObj->cash_back_value;
            $sku_name=$resObj->product_name;
			
			$CGST=$resObj->CGST;
			$IGST=$resObj->IGST;
			$SGST=$resObj->SGST;
			
            $addon_txt='';

			/*addon products*/
			$addon_products_status=$resObj->addon_products_status;
			$addon_products=$resObj->addon_products;
			$addon_inventories=$resObj->addon_inventories;
			$addon_total_price=$resObj->addon_total_price;
			$addon_total_shipping_price=$resObj->addon_total_shipping_price;
			$addon_single_or_multiple_tagged_inventories_in_frontend=$resObj->addon_single_or_multiple_tagged_inventories_in_frontend;


			$addon_txt.=", ord_addon_products_status='$addon_products_status' ,ord_addon_products='$addon_products',ord_addon_inventories='$addon_inventories',ord_addon_total_price='$addon_total_price' ,ord_addon_single_or_multiple_tagged_inventories_in_frontend='$addon_single_or_multiple_tagged_inventories_in_frontend' ";

			/*addon products*/

			if($promotion_available==1){
				if($total_price_of_product_with_promotion!="" || $total_price_of_product_without_promotion!=""){		
					$subtotal=($total_price_of_product_with_promotion+$total_price_of_product_without_promotion);
					$grandtotal=($total_price_of_product_with_promotion+$total_price_of_product_without_promotion+$shipping_charge);  
				}else{
					$subtotal=($product_price*$quantity);
					$grandtotal=(($product_price*$quantity)+$shipping_charge);
				}
			
			}else{
				$subtotal=($product_price*$quantity);
				$grandtotal=(($product_price*$quantity)+$shipping_charge);
			}

			// if(intval($addon_total_price)>0){
			// 	$subtotal+=($addon_total_price);
			// 	$grandtotal+=($addon_total_price);
			// }
			
			$cp_txt='';
			$coupon_code_appied='';
			$coupon_name_appied='';
			$coupon_type_appied='';
			$coupon_value_appied='';
			$coupon_applied_status='0';
			$coupon_reduce_price='0';
			if(isset($cp_data[$resObj->inventory_id])){
				//extract($cp_data[$resObj->inventory_id]);
				$c_data=$cp_data[$resObj->inventory_id];
				$coupon_code_appied=$c_data['coupon_code_appied'];
				$coupon_name_appied=$c_data['coupon_name_appied'];
				$coupon_type_appied=$c_data['coupon_type_appied'];
				$coupon_value_appied=$c_data['coupon_value_appied'];
				$coupon_applied_status=$c_data['coupon_applied_status'];
				$coupon_reduce_price=$c_data['coupon_reduce_price'];
				
				$cp_txt=", ord_coupon_code='$coupon_code_appied' ,ord_coupon_name='$coupon_name_appied',ord_coupon_type='$coupon_type_appied',ord_coupon_value='$coupon_value_appied',ord_coupon_status='$coupon_applied_status',ord_coupon_reduce_price='$coupon_reduce_price'";
			}

			//$subtotal=(($resObj->selling_price)*($resObj->inventory_moq));
			//$grandtotal=(($resObj->selling_price)*($resObj->inventory_moq))+($resObj->shipping_charge);
			
			if($total_price_of_product_without_promotion==""){
				$total_price_of_product_without_promotion=0;
			}
			
			//$addon_single_or_multiple_tagged_inventories_in_frontend=="single"// for both type
			if(intval($addon_total_price)>0){
				//$subtotal=$subtotal+$addon_total_price;
				//$grandtotal=$grandtotal+$addon_total_price;
			}
			if($addon_products=='"[]"'){
				$addon_products='[]';
			}
			$table="active_orders";

			$sql="insert into $table set purchased_price='$purchased_price',customer_id='$customer_id',order_id='$order_id',shipping_address_id='$shipping_address_id',inventory_id='$inventory_id',product_id='$product_id',sku_id='$sku_id',ord_sku_name='".$sku_name."',product_price='$product_price',ord_max_selling_price='$max_selling_price',ord_selling_discount='$selling_discount',ord_tax_percent_price='$tax_percent_price',ord_taxable_price='$taxable_price',subtotal='$subtotal',grandtotal='$grandtotal',quantity='$quantity',timestamp=now(),image='$image',random_number='$random_number',payment_type='$payment_type',payment_status='$payment_status',tax_percent='$tax_percent',shipping_charge='$shipping_charge',invoice_number='$invoice_number',return_period='$return_period',vendor_id='$vendor_id',logistics_name='$logistics_name',logistics_weblink='$logistics_weblink',expected_delivery_date='$expected_delivery_date',delivery_mode='$delivery_mode',parcel_category='$parcel_type',logistics_id='$logistics_id',logistics_delivery_mode_id='$logistics_delivery_mode_id',logistics_territory_id='$logistics_territory_id',logistics_parcel_category_id='$logistics_parcel_category_id',promotion_available='$promotion_available',promotion_residual='$promotion_residual',promotion_discount='$promotion_discount',promotion_cashback='$promotion_cashback',promotion_surprise_gift='$promotion_surprise_gift',promotion_surprise_gift_type='$promotion_surprise_gift_type',promotion_surprise_gift_skus='$promotion_surprise_gift_skus',promotion_surprise_gift_skus_nums='$promotion_surprise_gift_skus_nums',default_discount='$default_discount',promotion_item_multiplier='$promotion_item_multiplier',promotion_item='$promotion_item',promotion_item_num='$promotion_item_num',promotion_clubed_with_default_discount='$promotion_clubed_with_default_discount',promotion_id_selected='$promotion_id_selected',promotion_type_selected='$promotion_type_selected',promotion_minimum_quantity='$promotion_minimum_quantity',promotion_quote='$promotion_quote',promotion_default_discount_promo='$promotion_default_discount_promo',quantity_with_promotion='$quantity_with_promotion',quantity_without_promotion='$quantity_without_promotion',individual_price_of_product_with_promotion='$individual_price_of_product_with_promotion',individual_price_of_product_without_promotion='$individual_price_of_product_without_promotion',total_price_of_product_without_promotion='$total_price_of_product_without_promotion',total_price_of_product_with_promotion='$total_price_of_product_with_promotion',cash_back_value='$cash_back_value',razorpayPaymentId='$razorpayPaymentId',razorpayOrderId='$razorpayOrderId',CGST='$CGST',IGST='$IGST',SGST='$SGST',product_vendor_id='$product_vendor_id' $cp_txt $addon_txt ";
			
			

			$flag=$this->db->query($sql);
	
			if($flag){
				if($cur_checkout=="checkout_combo"){
					$order_item_id=$this->db->insert_id();
					$count_active_orders[]="yes";
					//// update sequence generator table starts ///
					$sql_seqgen_order_id="update sequence_generator_combo set order_id='$order_id'";
					$this->db->query($sql_seqgen_order_id);
					
					$sql_seqgen_invoice_number="update sequence_generator_combo set invoice_number='$invoice_number'";
					$this->db->query($sql_seqgen_invoice_number);
					//// update sequence generator table ends ///
					
				}
				else{
					$order_item_id=$this->db->insert_id();
					$count_active_orders[]="yes";
					//// update sequence generator table starts ///
					$sql_seqgen_order_id="update sequence_generator set order_id='$order_id'";
					$this->db->query($sql_seqgen_order_id);
					
					$sql_seqgen_invoice_number="update sequence_generator set invoice_number='$invoice_number'";
					$this->db->query($sql_seqgen_invoice_number);
					//// update sequence generator table ends ///
					
				}
			}
			
			//default
			//wallet_status is 'no';
			//wallet_amount is '0';
			 
			//,ord_addon_products_status='$addon_products_status',ord_addon_products='$addon_products'
			
			if($product_vendor_id==''){
				$product_vendor_id='1';
			}

			$table="history_orders";

			$sql_history="insert into history_orders set order_item_id='$order_item_id', purchased_price='$purchased_price',customer_id='$customer_id',order_id='$order_id',shipping_address_id='$shipping_address_id',inventory_id='$inventory_id',product_id='$product_id',sku_id='$sku_id',ord_sku_name='".$sku_name."',product_price='$product_price',ord_max_selling_price='$max_selling_price',ord_selling_discount='$selling_discount',ord_tax_percent_price='$tax_percent_price',ord_taxable_price='$taxable_price',subtotal='$subtotal',grandtotal='$grandtotal',quantity='$quantity',timestamp=now(),image='$image',random_number='$random_number',payment_type='$payment_type',payment_status='$payment_status',tax_percent='$tax_percent',shipping_charge='$shipping_charge',invoice_number='$invoice_number',return_period='$return_period',vendor_id='$vendor_id',logistics_name='$logistics_name',logistics_weblink='$logistics_weblink',expected_delivery_date='$expected_delivery_date',delivery_mode='$delivery_mode',parcel_category='$parcel_type',logistics_id='$logistics_id',logistics_delivery_mode_id='$logistics_delivery_mode_id',logistics_territory_id='$logistics_territory_id',logistics_parcel_category_id='$logistics_parcel_category_id',promotion_available='$promotion_available',promotion_residual='$promotion_residual',promotion_discount='$promotion_discount',promotion_cashback='$promotion_cashback',promotion_surprise_gift='$promotion_surprise_gift',promotion_surprise_gift_type='$promotion_surprise_gift_type',promotion_surprise_gift_skus='$promotion_surprise_gift_skus',promotion_surprise_gift_skus_nums='$promotion_surprise_gift_skus_nums',default_discount='$default_discount',promotion_item_multiplier='$promotion_item_multiplier',promotion_item='$promotion_item',promotion_item_num='$promotion_item_num',promotion_clubed_with_default_discount='$promotion_clubed_with_default_discount',promotion_id_selected='$promotion_id_selected',promotion_type_selected='$promotion_type_selected',promotion_minimum_quantity='$promotion_minimum_quantity',promotion_quote='$promotion_quote',promotion_default_discount_promo='$promotion_default_discount_promo',quantity_with_promotion='$quantity_with_promotion',quantity_without_promotion='$quantity_without_promotion',individual_price_of_product_with_promotion='$individual_price_of_product_with_promotion',individual_price_of_product_without_promotion='$individual_price_of_product_without_promotion',total_price_of_product_without_promotion='$total_price_of_product_without_promotion',total_price_of_product_with_promotion='$total_price_of_product_with_promotion',cash_back_value='$cash_back_value',razorpayPaymentId='$razorpayPaymentId',razorpayOrderId='$razorpayOrderId',CGST='$CGST',IGST='$IGST',SGST='$SGST',product_vendor_id='$product_vendor_id' $cp_txt $addon_txt ";
			
			;

			//$sql_history="insert into history_orders (ord_max_selling_price,ord_selling_discount,ord_tax_percent_price,ord_taxable_price,CGST,IGST,SGST,purchased_price,order_item_id,customer_id,order_id,shipping_address_id,inventory_id,product_id,sku_id,tax_percent,product_price,shipping_charge,subtotal,wallet_status,wallet_amount,grandtotal,quantity,timestamp_active_order,image,random_number,invoice_number,logistics_name,logistics_weblink,expected_delivery_date,delivery_mode,parcel_category,vendor_id,payment_type,payment_status,return_period,logistics_id,logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id,promotion_available,promotion_residual,promotion_discount,promotion_cashback,promotion_surprise_gift,promotion_surprise_gift_type,promotion_surprise_gift_skus,promotion_surprise_gift_skus_nums,default_discount,promotion_item_multiplier,promotion_item,promotion_item_num,promotion_clubed_with_default_discount,promotion_id_selected,promotion_type_selected,promotion_minimum_quantity,promotion_quote,promotion_default_discount_promo,individual_price_of_product_with_promotion,individual_price_of_product_without_promotion,total_price_of_product_without_promotion,total_price_of_product_with_promotion,quantity_without_promotion,quantity_with_promotion,cash_back_value,razorpayPaymentId,razorpayOrderId,ord_sku_name,ord_coupon_code,ord_coupon_name,ord_coupon_type,ord_coupon_value,ord_coupon_status,ord_coupon_reduce_price,ord_addon_products_status,ord_addon_products,ord_addon_single_or_multiple_tagged_inventories_in_frontend,product_vendor_id) values('$max_selling_price','$selling_discount','$tax_percent_price','$taxable_price','$CGST','$IGST','$SGST','$purchased_price','$order_item_id','$customer_id','$order_id','$shipping_address_id','$inventory_id','$product_id','$sku_id','$tax_percent','$product_price','$shipping_charge','$subtotal','no','0','$grandtotal','$quantity',now(),'$image','$random_number','$invoice_number','$logistics_name','$logistics_weblink','$expected_delivery_date','$delivery_mode','$parcel_type','$vendor_id','$payment_type','$payment_status','$return_period','$logistics_id','$logistics_delivery_mode_id','$logistics_territory_id','$logistics_parcel_category_id','$promotion_available','$promotion_residual','$promotion_discount','$promotion_cashback','$promotion_surprise_gift','$promotion_surprise_gift_type','$promotion_surprise_gift_skus','$promotion_surprise_gift_skus_nums','$default_discount','$promotion_item_multiplier','$promotion_item','$promotion_item_num','$promotion_clubed_with_default_discount','$promotion_id_selected','$promotion_type_selected','$promotion_minimum_quantity','$promotion_quote','$promotion_default_discount_promo','$individual_price_of_product_with_promotion','$individual_price_of_product_without_promotion','$total_price_of_product_without_promotion','$total_price_of_product_with_promotion','$quantity_without_promotion','$quantity_with_promotion','$cash_back_value','$razorpayPaymentId','$razorpayOrderId','$sku_name','$coupon_code_appied','$coupon_name_appied','$coupon_type_appied','$coupon_value_appied','$coupon_applied_status','$coupon_reduce_price','$addon_products_status','$addon_products','$addon_single_or_multiple_tagged_inventories_in_frontend','$product_vendor_id')";

                                
			$result_history=$this->db->query($sql_history);   

			$sql_orders_status="insert into orders_status set order_item_id='$order_item_id',customer_id='$customer_id',notify_active='1'";
			$this->db->query($sql_orders_status);
			
			$policy_applied=get_instance()->getPolicyForInventory($inventory_id);
                        
             foreach($policy_applied as $k => $policy){
                //    echo $k;

                $policy=$policy[0];
				
                $item_level=$this->db->escape_str($policy['item_level']);
				if(isset($policy['message'])){
					$policy['message']=$this->db->escape_str($policy['message']);
				}
				
				
                if($k=="Replacement"){
                    //array_push($replacement_policy_arr,$policy);
    
                    $sql="insert into order_replacement_policy set
                    replacement_policy_uid='".$policy['replacement_policy_uid']."',order_id='$order_id',order_item_id='$order_item_id',
                     item_level='".$item_level."', message='".$policy['message']."',replacement_restrict='".$policy['replacement_restrict']."',new_days_for_replacement='".$policy['new_days_for_replacement']."'";
                     $this->db->query($sql);
                     
                }

                if($k=="Refund"){    
                    //array_push($refund_policy_arr,$policy);
                    $sql="insert into order_refund_policy set refund_policy_uid='".$policy['refund_policy_uid']."', order_id='$order_id',order_item_id='$order_item_id',item_level='".$item_level."',refund_method='".$policy['refund_method']."',message='".$policy['message']."',refund_restrict='".$policy['refund_restrict']."',new_days_for_refund='".$policy['new_days_for_refund']."'";
                     $this->db->query($sql);
                }
                if($k=="Cancellation"){  
                    //array_push($cancellation_policy_arr,$policy);
                    $sql="insert into order_cancellation_policy set cancellation_policy_uid='".$policy['cancellation_policy_uid']."',order_id='$order_id',order_item_id='$order_item_id',item_level='".$item_level."',message='".$policy['message']."',cancellation_restrict='".$policy['cancellation_restrict']."',new_days_for_cancellation='".$policy['new_days_for_cancellation']."',new_hour_limit='".$policy['new_hour_limit']."',order_status_type='".$policy['order_status_type']."'";
                    $this->db->query($sql);
                }
                if($k=="Exchange"){
                    //array_push($exchange_policy_arr,$policy);
                    //$sql="insert into order_exchange_policy set ";
                }
            }

			if(!empty($addon_products)){
				if($addon_inventories!=''){
					$this->add_addon_products_as_new_orderitems($order_id,$order_item_id,$addon_products_status,$addon_products,$addon_inventories,$addon_total_price,$addon_total_shipping_price,$shipping_charge,$resObj,$payment_method,$shipping_address_id,$cur_checkout,$product_vendor_id);
				}
			}

		}//foreach
		
		$this->delete_carttable_for_customer(); //important uncomment

		if(count($count_active_orders)==$count_carttable){
			return true;
		}
		else{
			return false;
		}
		
	}
	
	public function add_addon_products_as_new_orderitems($order_id,$addon_master_order_item_id,$addon_products_status,$addon_products,$addon_inventories,$addon_total_price,$addon_total_shipping_price,$shipping_charge,$resObj,$payment_method,$shipping_address_id,$cur_checkout,$product_vendor_id){

		if($addon_products_status=='1'){
			$addon_inventories_arr=explode(',',$addon_inventories);
			$addon_inventories_arr=array_filter($addon_inventories_arr);
			$addon_products=json_decode($addon_products);
			
			//print_r($addon_products);
			//exit;

			//print_r($addon_inventories_arr);

			
			foreach($addon_inventories_arr as $i=>$arr){

				$inv_id=$addon_inventories_arr[$i];

				//print_r($addon_products);

				$tagged_type=isset($addon_products[$i]->tagged_type) ? $addon_products[$i]->tagged_type : '';

				$purchased_price=isset($addon_products[$i]->inv_price) ? $addon_products[$i]->inv_price : '';
				$image=isset($addon_products[$i]->inv_image) ? $addon_products[$i]->inv_image : '';
				$shipping_charge=isset($addon_products[$i]->inv_ship_price) ? $addon_products[$i]->inv_ship_price : '';

				//echo "shipping_charge=".$tagged_type;

				$inv_data=array();

				if(1){
					$inv_data=get_all_inventory_data($inv_id);
				}
				
				//echo $inv_id;
				
				//print_r($inv_data);

				//exit;

				if(!empty($inv_data)){

					/** copied */

			$customer_id=$resObj->customer_id;

			// if($tagged_type=='internal'){
			// 	$purchased_price=$inv_data->purchased_price;
			// }
			
			
			
			$inventory_id=$inv_id;
			$product_id=isset($inv_data->product_id) ? $inv_data->product_id : '';//no product_id
			$sku_id=$inv_data->sku_id;
			
			$logistics_id=$resObj->logistics_id;
			$logistics_parcel_category_id=$resObj->logistics_parcel_category_id;
			$logistics_delivery_mode_id=$resObj->default_delivery_mode_id;
			$logistics_territory_id=$resObj->logistics_territory_id;
			

			

			$quantity=1;
			//$inv_data->product_status;
			//$image=($inv_data->thumbnail!='') ? $inv_data->thumbnail : $inv_data->common_image;
			
			//$product_price=$resObj->inventory_selling_price_with_discount_original;
			
			if(1){
				$product_price=$inv_data->selling_price;
				$max_selling_price=$inv_data->max_selling_price;
				$selling_discount=$inv_data->selling_discount;
				$tax_percent_price=$inv_data->tax_percent_price;
				$taxable_price=$inv_data->taxable_price;
				$tax_percent=$inv_data->tax_percent;
				//$vendor_id=$inv_data->vendor_id;
				$sku_name=$inv_data->product_name;
				
				$CGST=$inv_data->CGST;
				$IGST=$inv_data->IGST;
				$SGST=$inv_data->SGST;
			}
			
			$vendor_id=$resObj->vendor_id;
                        
			//$order_id=date("dmHi".$this->session->userdata("customer_id"));
			//$grandtotal=(($resObj->selling_price)*($resObj->inventory_moq))+(($resObj->inventory_moq)*($resObj->shipping_charge));

			$random_number=rand(100,10000);

			$payment_type=$payment_method;
			$payment_status="no";
			
			
			//$shipping_charge=$resObj->shipping_charge;
			$delivery_mode=$resObj->delivery_mode;
			$parcel_type=$resObj->parcel_type;
			$date = strtotime("+".$resObj->territory_duration." days", strtotime(date('Y-m-d h:i:s')) );
			$expected_delivery_date=date("D j M Y",$date);
			$logistics_name=$resObj->logistics_name;
			$logistics_weblink=$resObj->logistics_weblink;
			$return_period=$resObj->return_period;

			$rand=mt_rand(1,1000000);
			//$invoice_number='#BLR_'.date("ym".date("d")).$rand;
			$invoice_number=$this->get_invoice_number();
	
            $addon_txt='';

			$promotion_available=0;
			$default_discount=0;

			/** copied */

					
			/*$subtotal=(($inv_data->selling_price)*1);
			$grandtotal=(($inv_data->selling_price)*1)+($shipping_charge);*/

			$subtotal=$purchased_price;
			$grandtotal=$purchased_price;
			
			
			$total_price_of_product_without_promotion=0;
			
			$sql="insert into active_orders set purchased_price='$purchased_price',customer_id='$customer_id',order_id='$order_id',shipping_address_id='$shipping_address_id',inventory_id='$inventory_id',product_id='$product_id',sku_id='$sku_id',ord_sku_name='".$sku_name."',product_price='$product_price',ord_max_selling_price='$max_selling_price',ord_selling_discount='$selling_discount',ord_tax_percent_price='$tax_percent_price',ord_taxable_price='$taxable_price',subtotal='$subtotal',grandtotal='$grandtotal',quantity='$quantity',timestamp=now(),image='$image',random_number='$random_number',payment_type='$payment_type',payment_status='$payment_status',tax_percent='$tax_percent',shipping_charge='$shipping_charge',invoice_number='$invoice_number',return_period='$return_period',vendor_id='$vendor_id',logistics_name='$logistics_name',logistics_weblink='$logistics_weblink',expected_delivery_date='$expected_delivery_date',delivery_mode='$delivery_mode',parcel_category='$parcel_type',logistics_id='$logistics_id',logistics_delivery_mode_id='$logistics_delivery_mode_id',logistics_territory_id='$logistics_territory_id',logistics_parcel_category_id='$logistics_parcel_category_id',promotion_available='$promotion_available',CGST='$CGST',IGST='$IGST',SGST='$SGST',ord_addon_master_order_item_id='$addon_master_order_item_id',product_vendor_id='$product_vendor_id' ";

			$flag=$this->db->query($sql);
	
			if($flag){
				if($cur_checkout=="checkout_combo"){
					$order_item_id=$this->db->insert_id();
					$count_active_orders[]="yes";
					//// update sequence generator table starts ///
					$sql_seqgen_order_id="update sequence_generator_combo set order_id='$order_id'";
					$this->db->query($sql_seqgen_order_id);
					
					$sql_seqgen_invoice_number="update sequence_generator_combo set invoice_number='$invoice_number'";
					$this->db->query($sql_seqgen_invoice_number);
					//// update sequence generator table ends ///
				}
				else{
					$order_item_id=$this->db->insert_id();
					$count_active_orders[]="yes";
					//// update sequence generator table starts ///
					$sql_seqgen_order_id="update sequence_generator set order_id='$order_id'";
					$this->db->query($sql_seqgen_order_id);
					
					$sql_seqgen_invoice_number="update sequence_generator set invoice_number='$invoice_number'";
					$this->db->query($sql_seqgen_invoice_number);
					//// update sequence generator table ends ///
				}
			}
			
			//default
			//wallet_status is 'no';
			//wallet_amount is '0';
			 
			
			
			$sql_history="insert into history_orders (ord_max_selling_price,ord_selling_discount,ord_tax_percent_price,ord_taxable_price,CGST,IGST,SGST,purchased_price,order_item_id,customer_id,order_id,shipping_address_id,inventory_id,product_id,sku_id,tax_percent,product_price,shipping_charge,subtotal,wallet_status,wallet_amount,grandtotal,quantity,timestamp_active_order,image,random_number,invoice_number,logistics_name,logistics_weblink,expected_delivery_date,delivery_mode,parcel_category,vendor_id,payment_type,payment_status,return_period,logistics_id,logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id,promotion_available,ord_sku_name, ord_addon_master_order_item_id,product_vendor_id ) values('$max_selling_price','$selling_discount','$tax_percent_price','$taxable_price','$CGST','$IGST','$SGST','$purchased_price','$order_item_id','$customer_id','$order_id','$shipping_address_id','$inventory_id','$product_id','$sku_id','$tax_percent','$product_price','$shipping_charge','$subtotal','no','0','$grandtotal','$quantity',now(),'$image','$random_number','$invoice_number','$logistics_name','$logistics_weblink','$expected_delivery_date','$delivery_mode','$parcel_type','$vendor_id','$payment_type','$payment_status','$return_period','$logistics_id','$logistics_delivery_mode_id','$logistics_territory_id','$logistics_parcel_category_id','$promotion_available','$sku_name','$addon_master_order_item_id',$product_vendor_id)";

                                
			$result_history=$this->db->query($sql_history);   

			$sql_orders_status="insert into orders_status set order_item_id='$order_item_id',customer_id='$customer_id',notify_active='1'";
			$this->db->query($sql_orders_status);
			
			$policy_applied=get_instance()->getPolicyForInventory($inventory_id);
            
			if(!empty($policy_applied)){
             foreach($policy_applied as $k => $policy){
                //    echo $k;

                $policy=$policy[0];
				
                $item_level=$this->db->escape_str($policy['item_level']);
				if(isset($policy['message'])){
					$policy['message']=$this->db->escape_str($policy['message']);
				}
				
				
                if($k=="Replacement"){
                    //array_push($replacement_policy_arr,$policy);
    
                    $sql="insert into order_replacement_policy set
                    replacement_policy_uid='".$policy['replacement_policy_uid']."',order_id='$order_id',order_item_id='$order_item_id',
                     item_level='".$item_level."', message='".$policy['message']."',replacement_restrict='".$policy['replacement_restrict']."',new_days_for_replacement='".$policy['new_days_for_replacement']."'";
                     $this->db->query($sql);
                     
                }

                if($k=="Refund"){    
                    //array_push($refund_policy_arr,$policy);
                    $sql="insert into order_refund_policy set refund_policy_uid='".$policy['refund_policy_uid']."', order_id='$order_id',order_item_id='$order_item_id',item_level='".$item_level."',refund_method='".$policy['refund_method']."',message='".$policy['message']."',refund_restrict='".$policy['refund_restrict']."',new_days_for_refund='".$policy['new_days_for_refund']."'";
                     $this->db->query($sql);
                }
                if($k=="Cancellation"){  
                    //array_push($cancellation_policy_arr,$policy);
                    $sql="insert into order_cancellation_policy set cancellation_policy_uid='".$policy['cancellation_policy_uid']."',order_id='$order_id',order_item_id='$order_item_id',item_level='".$item_level."',message='".$policy['message']."',cancellation_restrict='".$policy['cancellation_restrict']."',new_days_for_cancellation='".$policy['new_days_for_cancellation']."',new_hour_limit='".$policy['new_hour_limit']."',order_status_type='".$policy['order_status_type']."'";
                    $this->db->query($sql);
                }
                if($k=="Exchange"){
                    //array_push($exchange_policy_arr,$policy);
                    //$sql="insert into order_exchange_policy set ";
                }
            }
		}

				}

			}

		}

	}
	
	public function get_invoice_number(){
		$generalstuff_arr=generalstuff();
		
		$company_shortcode=$generalstuff_arr["company_shortcode"];
		$firsthalf=strtoupper($company_shortcode).date("ym");
		
		$sql="select invoice_number from sequence_generator";
		$result=$this->db->query($sql);
		$row_arr=$result->row_array();
		if(!empty($row_arr)){
			$invoice_number=$row_arr["invoice_number"];
			if($invoice_number==""){
				$first_invoice_number=$firsthalf."00001";
				return $first_invoice_number;
			}
			else{
				$month_compare_current=strtoupper($company_shortcode).date("ym");
				$month_compare_db=substr($invoice_number,0,7);
				if($month_compare_current==$month_compare_db){
					$next_secondhalf_invoice_number=(int)substr($invoice_number,9)+1;
					if(strlen($next_secondhalf_invoice_number)==1){
						$next_invoice_number=$firsthalf."0000".$next_secondhalf_invoice_number;
					}
					if(strlen($next_secondhalf_invoice_number)==2){
						$next_invoice_number=$firsthalf."000".$next_secondhalf_invoice_number;
					}
					if(strlen($next_secondhalf_invoice_number)==3){
						$next_invoice_number=$firsthalf."00".$next_secondhalf_invoice_number;
					}
					if(strlen($next_secondhalf_invoice_number)==4){
						$next_invoice_number=$firsthalf."0".$next_secondhalf_invoice_number;
					}
					if(strlen($next_secondhalf_invoice_number)==5){
						$next_invoice_number=$firsthalf."".$next_secondhalf_invoice_number;
					}
					return $next_invoice_number;
				}
				else{
					$first_invoice_number=$firsthalf."00001";
					return $first_invoice_number;
				}
			}
		}
	}
	
	public function get_customer_all_bank($customer_id){
		$sql="select distinct account_number from refund_bank_details where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	public function get_bank_detail($account_number,$customer_id){
		$sql="select * from refund_bank_details where customer_id='$customer_id' and account_number='$account_number'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	public function get_bank_detail_delivered($account_number,$customer_id){
		$sql="select * from delivered_refund_bank_details where customer_id='$customer_id' and account_number='$account_number'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	public function get_bank_account_number_bank_id($bank_id,$type){
		if($type=="refund"){
			$sql="select distinct account_number from refund_bank_details where id='$bank_id'";
		}
		if($type=="delivered"){
			$sql="select distinct account_number from delivered_refund_bank_details where id='$bank_id'";
		}
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->account_number;
		}
		else{
			return false;
		}
	}
	
	public function update_bank_details_action($bank_id,$bank_name_rep,$bank_branch_name_rep,$bank_city_rep,$bank_ifsc_code_rep,$bank_account_number_rep,$bank_account_confirm_number_rep,$account_holder_name_rep,$bank_return_refund_phone_number_rep){
		$sql="update refund_bank_details set 
									bank_name='$bank_name_rep',
									branch_name='$bank_branch_name_rep',
									city='$bank_city_rep',
									ifsc_code='$bank_ifsc_code_rep',
									account_number='$bank_account_number_rep',
									confirm_account_number='$bank_account_confirm_number_rep',
									account_holder_name='$account_holder_name_rep',
									mobile_number='$bank_return_refund_phone_number_rep' where id='$bank_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function update_delivered_bank_details_action($bank_id,$bank_name_rep,$bank_branch_name_rep,$bank_city_rep,$bank_ifsc_code_rep,$bank_account_number_rep,$bank_account_confirm_number_rep,$account_holder_name_rep,$bank_return_refund_phone_number_rep){
		$sql="update delivered_refund_bank_details set 
									bank_name='$bank_name_rep',
									branch_name='$bank_branch_name_rep',
									city='$bank_city_rep',
									ifsc_code='$bank_ifsc_code_rep',
									account_number='$bank_account_number_rep',
									confirm_account_number='$bank_account_confirm_number_rep',
									account_holder_name='$account_holder_name_rep',
									mobile_number='$bank_return_refund_phone_number_rep' where id='$bank_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	/* function for cards */
	public function get_customer_all_cards($customer_id){
		$sql="select * from cards where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function add_customer_new_card($customer_id,$card_number,$name_on_card,$card_type,$month,$year,$card_label){
		$sql="insert into cards (customer_id,card_number,name_on_card,card_type,exp_month,exp_year,bank_name) values('$customer_id','$card_number','$name_on_card','$card_type','$month','$year','$card_label')";
			$result=$this->db->query($sql);
			if($this->db->affected_rows() > 0){
				return true;
			}
			else{
				return false;
			}
	}
	public function make_this_card_default($customer_id,$card_id){
		$sql="update cards set card_default='0' where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		$sql="update cards set card_default='1' where customer_id='$customer_id' and card_id='$card_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function delete_this_card($customer_id,$card_id){
		$sql="delete from cards where customer_id='$customer_id' and card_id='$card_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	/* function for cards ends */
	public function get_logistics_id_by_vendor_id($vendor_id){
		$sql="select logistics_id from logistics where vendor_id='$vendor_id'";
		$result=$this->db->query($sql);
		return $result->result();
	}
	
	public function check_availability_pincode($logistics_id_arr,$available_pincode,$default_logistics_id){
		$logistics_id_in="'".implode("','",$logistics_id_arr)."'";
		$pin=substr($available_pincode,0,3);
		//$sql="select count(*) as count from logistics_service where logistics_id in ($logistics_id_in) and pin='$pin' and pincode like '%$available_pincode%'";
		$sql="select count(*) as count from logistics_service where logistics_id = $default_logistics_id and pin='$pin'";
		$result=$this->db->query($sql);
		
		if($result->row()->count==0){
			return false;
		}
		else{
			return true;
		}
	}
	
	public function get_default_logistics($vendor){
		$sql="select logistics_id from logistics where vendor_id='$vendor' and default_logistics=1 order by logistics_priority asc limit 0,1";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->logistics_id;
		}
	}
	
	public function get_customer_address($customer_address_id){
		$customer_id=$this->session->userdata("customer_id");
		$sql="select * from customer_address where customer_id='$customer_id' and customer_address_id='$customer_address_id'";
		$res=$this->db->query($sql);
		return $res->row();
	}
	
	public function edit_delivery_address($customer_address_id,$customer_id,$name,$mobile,$door_address,$street_address,$city,$state,$pin,$country,$make_default){
		if($make_default==true){
			$sql="update customer_address set make_default=0 where customer_id='$customer_id'";
			$result=$this->db->query($sql);
		}
		if($make_default==true){
			$make_default=1;
		}
		if($make_default==false){
			$make_default=0;
		}
		$sql="update customer_address  set customer_id='$customer_id',customer_name='$name',mobile='$mobile',address1='$door_address',address2='$street_address',city='$city',state='$state',pincode='$pin',country='$country',make_default='$make_default' where customer_address_id='$customer_address_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
				return true;
			}
			else{
				return false;
			}
	}
	
	public function get_customer_all_address_in_json_format($customer_id){
		$sql="select * from customer_address where customer_id='$customer_id' order by make_default desc";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function get_count_customer_all_address_in_json_format($customer_id){
		$sql="select count(*) as count from customer_address where customer_id='$customer_id' order by make_default desc limit 0,4";
		$result=$this->db->query($sql);
		return $result->row()->count;
	}
	
	public function get_cancellation_details_from_admin($order_item_id){
		$sql="select * from cancels where order_item_id='$order_item_id' and cancelled_by='admin'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	public function get_cancellation_reason_details_from_admin($reason_cancel_id){
		$sql="select * from admin_cancel_reason where cancel_reason_id='$reason_cancel_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row();
		}
		else{
			return false;
		}
	}
	
	public function get_order_cancelled_by_admin_or_not($order_item_id){
		$sql="select * from cancels where cancelled_by='admin' and refund_type='' and order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()==0){
			return false;
		}
		else{
			return true;
		}
	}
	public function get_cancelled_order_customer_payment_type($order_item_id){
		$sql="select payment_type from cancelled_orders where order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->payment_type;
		}
		else{
			return false;
		}
	}
	
	public function get_shipping_address_pincode(){
		$customer_id=$this->session->userdata("customer_id");
		$sql="select pincode,city from customer_address where customer_id='$customer_id' and make_default='1'";
		$res=$this->db->query($sql);
	
		return $res->row_array();
	}
	
	public function get_all_available_delivery_mode($logistics_id,$vendor_id){
		$sql="select * from  logistics_delivery_mode where logistics_id='$logistics_id' and vendor_id='$vendor_id'";
		$res=$this->db->query($sql);
		return $res->result_array();
	}
	public function get_specific_available_delivery_mode_new($logistics_id,$logistics_territory_id,$vendor_id){
		$get_logistics_by_logistics_id_row=$this->get_logistics_by_logistics_id($logistics_id);
		$logistics_name=$get_logistics_by_logistics_id_row->logistics_name;
		
		$logistics_delivery_mode_id_arr=array();
		$sql="select logistics_delivery_mode_id from logistics_weight where logistics_id='$logistics_id' and territory_name_id='$logistics_territory_id'";
		$res=$this->db->query($sql);
		foreach($res->result_array() as $query){
			$logistics_delivery_mode_id_arr[]=$query["logistics_delivery_mode_id"];
		}
		$logistics_delivery_mode_id_in=implode(",",$logistics_delivery_mode_id_arr);
		$sql="select *,'$logistics_name' as logistics_name from  logistics_delivery_mode where logistics_delivery_mode_id in ($logistics_delivery_mode_id_in)";
		$res=$this->db->query($sql);
		return $res->result_array();
	}
	
	
	
	public function get_delivery_mode_ids($logistics_id,$logistics_territory_id,$vendor_id){
		$logistics_delivery_mode_id_arr=array();
		$sql="select logistics_delivery_mode_id from logistics_weight where logistics_id='$logistics_id' and territory_name_id='$logistics_territory_id'";
		$res=$this->db->query($sql);
		foreach($res->result_array() as $query){
			$logistics_delivery_mode_id_arr[]=$query["logistics_delivery_mode_id"];
		}
		return implode(",",$logistics_delivery_mode_id_arr);
	}

	
	public function get_availability_of_shipping($pincode){
		$pin=substr($pincode,0,3);
		$sql="select count(*) as count from logistics_service where pin='$pin' and pincode like '%$pincode%'";
		$result=$this->db->query($sql);
		if($result->row()->count==0){
			return false;
		}
		else{
			return true;
		}
	}
	public function get_vendor_id(){
			$sql="select vendor_id from logistics limit 0,1";
			$result=$this->db->query($sql);
			return $result->row_array()["vendor_id"];
	}
	public function get_default_logistics_id_for_all_vendors($vendor_id){
			$sql="select logistics_id from logistics where vendor_id='$vendor_id' and default_logistics='1' order by logistics_priority asc";
			$result=$this->db->query($sql);
			return $result->result();
		}
	public function insert_shipping_charge_to_carttable($cart_result_obj_arr,$pincode){
		foreach($cart_result_obj_arr as $cartObj){
			$inventory_id=$cartObj->inventory_id;
			$sql="select shipping_charge_id,shipping_charge from inventory where id='$inventory_id'";
			$result=$this->db->query($sql);
			$shipping_charge_id_pipe=$result->row()->shipping_charge_id;
			
			if($shipping_charge_id_pipe==""){
				// take the default one
				$default_shipping_charge=$result->row()->shipping_charge;
				$sql_cart_shipping_charge="update carttable set shipping_charge='$default_shipping_charge' where inventory_id='$inventory_id'";
				$this->db->query($sql_cart_shipping_charge);
			}
			else{
				$shipping_charge_id_iarr=explode("|",$shipping_charge_id_pipe);
				$shipping_charge_id_in="'".implode("','",$shipping_charge_id_iarr)."'";
				$sql_shipping_charge_tab="select logistics_service_id from shipping_charge where shipping_charge_id in ($shipping_charge_id_in)";
				$result_shipping_charge_tab=$this->db->query($sql_shipping_charge_tab);
				$logistic_service_id_total_arr=array();
				foreach($result_shipping_charge_tab->result() as $result_shipping_chargeObj){
					$logistic_service_id_arr=explode("|",$result_shipping_chargeObj->logistics_service_id);
					$logistic_service_id_total_arr=array_merge($logistic_service_id_total_arr,$logistic_service_id_arr);
				}
				$logistic_service_id_total_arr=array_unique($logistic_service_id_total_arr);
				$logistic_service_id_total_in="'".implode("','",$logistic_service_id_total_arr)."'";
				
				$pin=substr($pincode,0,3);
				
				$sql_logistic_service_tab="select logistics_id from logistics_service where logistics_service_id in ($logistic_service_id_total_in) and pin='$pin' and pincode like '%$pincode%' limit 0,1";
				$res_logistic_service_tab=$this->db->query($sql_logistic_service_tab);
				$logistics_id=$res_logistic_service_tab->row()->logistics_id;
				
				$sql_cal_shipping_charge="select shipping_price from shipping_charge where logistics_service_id like '%$logistics_id%' limit 0,1";
				$res_cal_shipping_charge=$this->db->query($sql_cal_shipping_charge);
				$shipping_price=$res_cal_shipping_charge->row()->shipping_price;
				
				$sql_cart_shipping_charge="update carttable set shipping_charge='$shipping_price' where inventory_id='$inventory_id'";
				$this->db->query($sql_cart_shipping_charge);
			}
		}
	}
	
	public function get_returns_conservation_chain_in_customerpanel($return_order_id){
		$sql="select * from return_replies where return_order_id='$return_order_id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function get_return_order_id_by_order_item_id($order_item_id){
		$sql="select return_order_id from returns_desired where order_item_id='$order_item_id'";
		$query=$this->db->query($sql);
		return $query->row()->return_order_id;
	}
	
	public function contact_admin($return_order_id,$user_type,$status,$description,$image_path,$customer_id,$admin_action,$customer_action){
		$description=$this->db->escape_str($description);
		$sql="insert into return_replies set return_order_id='$return_order_id',user_type='$user_type',status='$status',description='$description',image='$image_path',customer_id='$customer_id',admin_action='$admin_action',customer_action='$customer_action'";
		$query=$this->db->query($sql);
		return $query;
	}
	
	public function get_customer_name($customer_id){
		$sql="select name from customer where id='$customer_id'";
		$query=$this->db->query($sql);
		return $query->row()->name;
	}
	
	public function getspecified_returns_file($id){
	$query=$this->db->query("SELECT *  FROM return_replies where id='$id'");
		if(count($query->result())==1){
			return $query->result();
		}
	}
	
	public function get_unread_return_msg($return_order_id){
		$sql="select count(*) as count from  return_replies where user_type='admin' and return_order_id='$return_order_id' and is_read='0'";
		$query=$this->db->query($sql);
		return $query->row()->count;
	}
	
	public function update_msg_status($return_order_id){
		$sql="update return_replies set is_read=1 where user_type='admin' and return_order_id='$return_order_id'";
		$query=$this->db->query($sql);
		return $query;
	}
	
	public function get_order_details_admin_acceptance_refund_data($order_item_id){
		$sql="select order_return_decision.*,admin_return_reason.pickup_identifier from order_return_decision,admin_return_reason where order_return_decision.sub_reason_return_id=admin_return_reason.id and order_return_decision.order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	
	public function accept_reject_refund_status($order_item_id,$st){
		
		if($st=="reject"){
		$sql="update order_return_decision set customer_decision='$st',notify_return_reject='1' where order_item_id='$order_item_id'";
		$query=$this->db->query($sql);
		}if($st=="accept"){
			$sql="update order_return_decision set customer_decision='$st',notify_return_accept='1' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
		}
		
		
		$sql_update_order_return_decision="update returns_desired set order_return_decision_customer_decision='$st' where order_item_id='$order_item_id'";
		$query_update_order_return_decision=$this->db->query($sql_update_order_return_decision);
		
		
		return $query;
	}
	
	/** function for create complaint starts */
	public function get_all_complaint_regarding(){
		$sql="select * from complaint_regarding";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function get_all_service_regarding(){
		$sql="select * from complaint_service";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	public function get_all_category_service($id){
		$sql="select * from complaint_category where service_id='$id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function get_all_country_code(){
		$sql="select * from complaint_country_code where status='1'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function save_complaint_attachment($file_path,$thread_id,$complaint_thread_id,$customer_id){
		$sql="insert into complaint_attachment (thread_id,complaint_thread_id,file_path,customer_id) values('$thread_id','$complaint_thread_id','$file_path','$customer_id')";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function create_new_complaints($complaint_thread_id,$user_type,$acc_id,$severity,$regarding,$service,$category,$subject){
		$to_id="";
		$sql="insert into complaint_thread_master (complaint_thread_id,created_by,to_id,from_id,severity,regarding,service,category,subjects) values('$complaint_thread_id','$user_type','$to_id','$acc_id','$severity','$regarding','$service','$category','$subject')";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function add_thread_to_complaints($complaint_thread_id,$thread_id,$user_type,$customer_id,$discription,$contact_method,$country_code,$phone_number,$ext_number){
		$feed_back_rating="";
		//$discription=htmlspecialchars($discription);
		$discription=addslashes($discription);
		$sql="insert into complaint_thread_list (complaint_thread_id,thread_id,created_by,user_id,descriptions,contact_method,phone_country,phone_number,phone_extension,feed_back_rating) values('$complaint_thread_id','$thread_id','$user_type','$customer_id','$discription','$contact_method','$country_code','$phone_number','$ext_number','$feed_back_rating')";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function get_regarding_name($regarding){
		$sql="select regarding from complaint_regarding where regarding_id='$regarding'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->regarding;
		}
		else{
			return false;
		}
	}
	public function get_service_name($service){
		$sql="select service from complaint_service where service_id='$service'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->service;
		}
		else{
			return false;
		}
	}
	public function get_category_name($category){
		$sql="select category from complaint_category where category_id='$category'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->category;
		}
		else{
			return false;
		}
	}
	/** function for create complaint ends */
	/** function for all_master_data_of_complaints starts */
	public function get_all_master_data_of_complaints($customer_id){
		$sql="select * from complaint_thread_master where to_id='$customer_id' or from_id='$customer_id' order by timestamp desc LIMIT 5;";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function all_complaint_processing($params,$fg){
		//define index of column
		$columns = array( 
			0 =>'complaint_thread_master.id', 
			1 => 'complaint_thread_master.complaint_thread_id',
			2 => 'complaint_thread_master.created_by',
			3 => 'complaint_thread_master.to_id',
			4 => 'complaint_thread_master.from_id',
			5 => 'complaint_thread_master.severity',
			6 => 'complaint_thread_master.regarding',
			7 => 'complaint_thread_master.service',
			8 => 'complaint_thread_master.category',
			9 => 'complaint_thread_master.subjects',
			10 => 'complaint_thread_master.close_case',
			11 => 'complaint_thread_master.timestamp',
			
		);

		$where = $sqlTot = $sqlRec = "";
		$customer_id=$this->session->userdata('customer_id');
		// getting total number records without any search
		$sql = "SELECT *,case close_case when '0' then 'Pending Admin Action' when '1' then 'Pending Customer Action' when '2' then 'Resolved By Customer' when '3' then 'Resolved By Admin' when '4' then 'Disputed' end as close_case_text from `complaint_thread_master` where to_id='$customer_id' or from_id='$customer_id'";
		// check search value exist
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( complaint_thread_master.subjects LIKE '%".$params['search']['value']."%' ";
			$where .=" OR complaint_thread_master.id LIKE '%".$params['search']['value']."%' ";
			$where .=" OR complaint_thread_master.regarding LIKE '%".$params['search']['value']."%' ";
			$where .=" OR complaint_thread_master.severity LIKE '%".$params['search']['value']."%' ";
			$where .=" OR case close_case when '0' then 'Pending Admin Action' when '1' then 'Pending Customer Action' when '2' then 'Resolved By Customer' when '3' then 'Resolved By Admin' when '4' then 'Disputed' end LIKE '%".$params['search']['value']."%' )";
		}
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}

		
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
	}
	
	/** function for all_master_data_of_complaints ends */
	/*function for getting complaints starts*/
	public function get_master_data_of_complaints($complaint_thread_id){
		$sql="select * from complaint_thread_master where complaint_thread_id='$complaint_thread_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
		
	}
	
	public function get_complaint_case_user_fullname($id){
		$sql="select name from customer where id='$id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->name;
		}
		else{
			return false;
		}
	}

	public function change_complaint_case_status($case_id,$action){
		$sql="update complaint_thread_master set close_case='$action' where complaint_thread_id='$case_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function get_all_thread_complaint($complaint_id){
		$sql="select * from complaint_thread_list where complaint_thread_id='$complaint_id' order by timestamp desc";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function get_all_attachment_of_thread_id($threadid){
		$sql="select * from complaint_attachment where thread_id='$threadid'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function save_star_ratings_of_thread($thread_id,$star_val){
		$sql="update complaint_thread_list set feed_back_rating='$star_val' where thread_id='$thread_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	public function update_returns_desired_with_bank_details_update($order_item_id){
		$sql_returns_desired="select * from returns_desired where order_item_id='$order_item_id'";
		$result_returns_desired=$this->db->query($sql_returns_desired);
		if($result_returns_desired->num_rows()>0){
			$sql="update returns_desired set allow_update_bank_details='updated',notify_failure_refund=1 where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
		}
		$sql_cancels="select * from cancels where order_item_id='$order_item_id'";
		$result_cancels=$this->db->query($sql_cancels);
		if($result_cancels->num_rows()>0){
			$sql="update cancels set allow_update_bank_details='updated' where order_item_id='$order_item_id'";
		
			$result=$this->db->query($sql);
		}
	}
	public function get_vendor_id_by_inventory_id($inventory_id){
		$sql="select vendor_id from inventory where id='$inventory_id'";
		$result=$this->db->query($sql);
		return $result->row()->vendor_id;
	}
	/*function for getting complaints ends*/
	
	public function get_orders_status_data($order_item_id){
		$sql="select * from orders_status  where order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	public function get_orders_status_data_prev($order_item_id){
		$sql="select * from orders_status  where prev_order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	public function return_cancel($order_item_id,$cancel_returns_comments){
		$cancel_returns_comments=$this->db->escape_str($cancel_returns_comments);
		$sql="update returns_desired set order_return_decision_customer_status='cancel',order_return_decision_customer_status_comments='$cancel_returns_comments',notify_cancelled_refund='1' where order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		

		/* if order return decision is empty - newly added */

			/*$sql_1="select * from order_return_decision  where order_item_id='$order_item_id'";

			$res=$this->db->query($sql_1);

			if($res->num_rows()==0){
			
				$sql="select order_item_id from history_orders where order_item_id='$order_item_id'";
				$result=$this->db->query($sql);
				$obj=$result->row();

				if(!empty($obj)){

					$sql="select order_item_id from returns_desired where order_item_id='$order_item_id'";
					$result=$this->db->query($sql);
					$obj1=$result->row();

					$return_order_id=$obj1->return_order_id;
					$customer_id=$this->session->userdata("customer_id");
					$inventory_id=$obj->inventory_id;

					$product_price=$obj->product_price;
					$shipping_charge_purchased=$obj->product_price;
					$shipping_charge=$obj->product_price;
					$order_item_id=$obj->order_item_id;
					$quantity_accept=$obj1->quantity_refund;
					$type_of_refund=$obj1->refund_method;
					$sub_issue_id=$obj1->sub_issue_id;
					$comments=$cancel_returns_comments;
					$disable_comm='';
					$promotion_available=$obj->promotion_available;


					$promotion_minimum_quantity=$obj->promotion_minimum_quantity;
					$promotion_quote=$obj->promotion_quote;
					$promotion_default_discount_promo=$obj->promotion_default_discount_promo;
					$promotion_surprise_gift=$obj->promotion_surprise_gift;
					$promotion_surprise_gift_type=$obj->promotion_surprise_gift_type;
					$promotion_surprise_gift_skus=$obj->promotion_surprise_gift_skus;
					$promotion_surprise_gift_skus_nums=$obj->promotion_surprise_gift_skus_nums;
					$promotion_item=$obj->promotion_item;
					$promotion_item_num=$obj->promotion_item_num;
					$promotion_cashback=$obj->promotion_cashback;
					$promotion_item_multiplier=$obj->promotion_item_multiplier;
					$promotion_discount=$obj->promotion_discount;
					$default_discount=$obj->default_discount;
					
					$quantity_without_promotion=$obj->quantity_without_promotion;
					$quantity_with_promotion=$obj->quantity_with_promotion;
					$cash_back_value=$obj->cash_back_value;
					$individual_price_of_product_with_promotion=$obj->individual_price_of_product_with_promotion;
					$individual_price_of_product_without_promotion=$obj->individual_price_of_product_without_promotion; 
					$total_price_of_product_with_promotion=$obj->total_price_of_product_with_promotion;
					$total_price_of_product_without_promotion=$obj->total_price_of_product_without_promotion;
					
					//////////////////////promo details////////////////////////
					
					$product_price_with_offer=round(($quantity_accept*$product_price),2);
					$shipping_charge_with_offer=round(($quantity_accept*$shipping_charge),2);
					$product_price_with_offer_percentage=0;
					$shipping_charge_with_offer_percentage=0;
					$product_price_with_offer_percentage_chk='yes';
					$shipping_charge_with_offer_percentage_chk='yes';
					$comm_flag=1;

					if($obj->promotion_available==1){
			
						
							$sql="insert into order_return_decision set return_order_id='$return_order_id',customer_id='$customer_id',quantity='$quantity_accept',inventory_id='$inventory_id',product_price='$product_price',shipping_charge_purchased='$shipping_charge_purchased',shipping_charge='$shipping_charge',order_item_id='$order_item_id',sub_reason_return_id='$type_of_refund',status='reject',comments='$comments',disable_comm_flag='$comm_flag',customer_status='pending',promotion_available='$promotion_available',promotion_minimum_quantity='$promotion_minimum_quantity',promotion_quote='$promotion_quote',promotion_default_discount_promo='$promotion_default_discount_promo',promotion_surprise_gift='$promotion_surprise_gift',promotion_surprise_gift_type='$promotion_surprise_gift_type',promotion_surprise_gift_skus='$promotion_surprise_gift_skus',promotion_surprise_gift_skus_nums='$promotion_surprise_gift_skus_nums',promotion_item='$promotion_item',promotion_item_num='$promotion_item_num',promotion_cashback='$promotion_cashback',promotion_item_multiplier='$promotion_item_multiplier',promotion_discount='$promotion_discount',default_discount='$default_discount',quantity_without_promotion='$quantity_without_promotion',quantity_with_promotion='$quantity_with_promotion',cash_back_value='$cash_back_value',individual_price_of_product_with_promotion='$individual_price_of_product_with_promotion',individual_price_of_product_without_promotion='$individual_price_of_product_without_promotion',total_price_of_product_with_promotion='$total_price_of_product_with_promotion',total_price_of_product_without_promotion='$total_price_of_product_without_promotion',shipping_charge_with_offer='$shipping_charge_with_offer',product_price_with_offer='$product_price_with_offer',product_price_with_offer_percentage='$product_price_with_offer_percentage',shipping_charge_with_offer_percentage='$shipping_charge_with_offer_percentage',product_price_with_offer_percentage_chk='$product_price_with_offer_percentage_chk',shipping_charge_with_offer_percentage_chk='$shipping_charge_with_offer_percentage_chk'";
							$query=$this->db->query($sql);
						
					}else{
						
						$sql="insert into order_return_decision set return_order_id='$return_order_id',customer_id='$customer_id',quantity='$quantity_accept',inventory_id='$inventory_id',product_price='$product_price',shipping_charge_purchased='$shipping_charge_purchased',shipping_charge='$shipping_charge',order_item_id='$order_item_id',sub_reason_return_id='$type_of_refund',status='reject',comments='$comments',disable_comm_flag='$comm_flag',customer_status='pending',shipping_charge_with_offer='$shipping_charge_with_offer',product_price_with_offer='$product_price_with_offer',product_price_with_offer_percentage='$product_price_with_offer_percentage',shipping_charge_with_offer_percentage='$shipping_charge_with_offer_percentage',product_price_with_offer_percentage_chk='$product_price_with_offer_percentage_chk',shipping_charge_with_offer_percentage_chk='$shipping_charge_with_offer_percentage_chk'";
						$query=$this->db->query($sql);
					}
				}

			}
			*/
		/* if order return decision is empty - newly added */

		$sql_order_return_decision="update order_return_decision set customer_status='cancel',customer_status_comments='$cancel_returns_comments' where order_item_id='$order_item_id'";

		$result_order_return_decision=$this->db->query($sql_order_return_decision);
		
		return $result;
	}
	
	public function get_data_cancels_table($order_item_id){
		$sql="select * from cancels where order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	public function get_data_cancelled_orders($order_item_id){
		$sql="select * from cancelled_orders where order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	
	/*public function get_data_refund_table($cancel_id){
		$sql="select * from refund where cancel_id='$cancel_id'";
		$result=$this->db->query($sql);
		return $result->row_array();
	}*/
	
	public function get_data_cancel_refund_table($order_item_id){
		$sql="select * from cancel_refund where order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	
	public function get_email_db($cust_id){
		$sql="select email from customer where  id='$cust_id'";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$email = $result['email'];
		return $email;
	}
	public function get_mobile_db($cust_id){
		$sql="select mobile from customer where  id='$cust_id'";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$mobile = $result['mobile'];
		return $mobile;
	}
	
	public function get_shipping_address_id($order_id){
		$sql="select shipping_address_id from active_orders where order_id='$order_id' limit 0,1";
		$query =$this->db->query($sql);
		return $query->row()->shipping_address_id;
	}
	public function get_order_summary_mail_data($order_id){
		$sql="select active_orders.*,inventory.thumbnail,products.product_name,products.product_description,inventory.attribute_1,inventory.attribute_1_value,inventory.attribute_2,inventory.attribute_2_value,inventory.attribute_3,inventory.attribute_3_value,inventory.attribute_4,inventory.attribute_4_value from active_orders,inventory,products where active_orders.inventory_id=inventory.id and products.product_id=inventory.product_id and active_orders.order_id='$order_id'";
		$query =$this->db->query($sql);
		return $query->result_array();
	}
	
	
	public function get_customer_skills_info($skills_arr){
		$skills_in="'".implode("','",$skills_arr)."'";
		$sql="select * from customer_add_profile_name where id in ($skills_in)";
		$query =$this->db->query($sql);
		return $query->result_array();
	}
	public function customer_prefer_material_info($materials_arr){
		$materials_in="'".implode("','",$materials_arr)."'";
		$sql="select * from customer_add_profile_values where profile_value_id in ($materials_in)";
		$query =$this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_in_returned_table_or_not($order_item_id){
		$sql="select count(*) as count from returned_orders where order_item_id ='$order_item_id'";
		$query =$this->db->query($sql);
		if($query->row()->count==0){
			$returned_table="no";
		}
		else{
			$returned_table="yes";
		}
		return $returned_table;
	}
	public function get_in_completed_table_or_not($order_item_id){
		$sql="select count(*) as count from completed_orders where order_item_id ='$order_item_id'";
		$query =$this->db->query($sql);
		if($query->row()->count==0){
			$completed_table="no";
		}
		else{
			$completed_table="yes";
		}
		return $completed_table;
	}
	
	public function get_return_refund_table_by_order_item_id($order_item_id){
		$sql="select * from return_refund where order_item_id ='$order_item_id'";
		$query =$this->db->query($sql);
		return $query->row_array();
	}
	
	public function update_sub_refund($order_item_id,$return_order_id,$refund_method,$refund_bank_id,$refund_remaining_quantities_input,$refund_remaining_deductions="",$refund_remaining_quantities="",$refund_all=""){
		$sql="select * from return_refund where order_item_id ='$order_item_id'";
		$query =$this->db->query($sql);
		$return_refund_arr=$query->row_array();
		$item_price_deducted=$return_refund_arr["item_price_without_concession"]-$return_refund_arr["item_with_deductions_concession"];
		$shipping_price_deducted=$return_refund_arr["shipping_price_without_concession"]-$return_refund_arr["shipping_with_deductions_concession"];
		$total_price_deducted=$item_price_deducted+$shipping_price_deducted;
		////////////////////////////
		$sql="select sub_return_refund.*,sub_returns_desired.refund_remaining_deductions_admin,sub_returns_desired.refund_remaining_quantities_admin,sub_returns_desired.refund_all_admin from sub_return_refund,sub_returns_desired where sub_return_refund.order_item_id ='$order_item_id' and sub_return_refund.sub_returns_desired_id=sub_returns_desired.sub_returns_desired_id and sub_returns_desired.admin_action='refunded'";
		$query =$this->db->query($sql);
		$sub_return_refund_arr=$query->result_array();
		/////////////////
		foreach($sub_return_refund_arr as $sub_return_refund_val_arr){
			if($sub_return_refund_val_arr["refund_remaining_deductions_admin"]!=""){
				$item_price_deducted=0;
				$shipping_price_deducted=0;
				$total_price_deducted=0;
			}
		}
		/////////////////////
		foreach($sub_return_refund_arr as $sub_return_refund_val_arr){
			
			$sub_item_price_deducted=$sub_return_refund_arr["item_price_without_concession"]-$sub_return_refund_arr["item_with_deductions_concession"];
			$sub_shipping_price_deducted=$sub_return_refund_arr["shipping_price_without_concession"]-$sub_return_refund_arr["shipping_with_deductions_concession"];
			$sub_total_price_deducted=$sub_item_price_deducted+$sub_shipping_price_deducted;
			
			$item_price_deducted+=$sub_item_price_deducted;
			$shipping_price_deducted+=$sub_shipping_price_deducted;
			$total_price_deducted+=$sub_total_price_deducted;
		}
		//////////////////////////////
		if($refund_remaining_deductions=="" && $refund_all==""){
			$item_price_deducted=0;
			$shipping_price_deducted=0;
			$total_price_deducted=0;
		}
		$sql="insert into sub_returns_desired set 
									order_item_id='$order_item_id',
									return_order_id='$return_order_id',
									refund_method='$refund_method',
									refund_bank_id='$refund_bank_id',
									item_price_deducted='$item_price_deducted',
									shipping_price_deducted='$shipping_price_deducted',
									total_price_deducted='$total_price_deducted',
									quantity_requested='$refund_remaining_quantities_input',refund_remaining_deductions='$refund_remaining_deductions',refund_remaining_quantities='$refund_remaining_quantities',refund_all='$refund_all',notify_refund_again='1'";
		$query =$this->db->query($sql);
		
		/*$sql_update_order_return_pickup="update orders_status set order_return_pickup='0' where 
									order_item_id='$order_item_id'";
		$query_update_order_return_pickup =$this->db->query($sql_update_order_return_pickup);*/
		return $query;
	}
	public function update_sub_refund_with_promo_details($order_item_id,$return_order_id,$refund_method,$refund_bank_id,$promotion_available,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$promotion_item,$promotion_item_num,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$promotion_cashback,$promotion_item_multiplier,$promotion_discount,$default_discount,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_with_promotion,$total_price_of_product_without_promotion,$refund_remaining_quantities_input,$refund_remaining_deductions="",$refund_remaining_quantities="",$refund_all=""){
		
		$sql="select * from return_refund where order_item_id ='$order_item_id'";
		$query =$this->db->query($sql);
		$return_refund_arr=$query->row_array();
		$item_price_deducted=$return_refund_arr["item_price_without_concession"]-$return_refund_arr["item_with_deductions_concession"];
		$shipping_price_deducted=$return_refund_arr["shipping_price_without_concession"]-$return_refund_arr["shipping_with_deductions_concession"];
		$total_price_deducted=$item_price_deducted+$shipping_price_deducted;
		////////////////////////////
		$sql="select sub_return_refund.*,sub_returns_desired.refund_remaining_deductions_admin,sub_returns_desired.refund_remaining_quantities_admin,sub_returns_desired.refund_all_admin from sub_return_refund,sub_returns_desired where sub_return_refund.order_item_id ='$order_item_id' and sub_return_refund.sub_returns_desired_id=sub_returns_desired.sub_returns_desired_id and sub_returns_desired.admin_action='refunded'";
		
		$query =$this->db->query($sql);
		$sub_return_refund_arr=$query->result_array();
		/////////////////
		foreach($sub_return_refund_arr as $sub_return_refund_val_arr){
			if($sub_return_refund_val_arr["refund_remaining_deductions_admin"]!=""){
				$item_price_deducted=0;
				$shipping_price_deducted=0;
				$total_price_deducted=0;
			}
		}
		/////////////////////
		foreach($sub_return_refund_arr as $sub_return_refund_val_arr){
			
			$sub_item_price_deducted=$sub_return_refund_arr["item_price_without_concession"]-$sub_return_refund_arr["item_with_deductions_concession"];
			$sub_shipping_price_deducted=$sub_return_refund_arr["shipping_price_without_concession"]-$sub_return_refund_arr["shipping_with_deductions_concession"];
			$sub_total_price_deducted=$sub_item_price_deducted+$sub_shipping_price_deducted;
			
			$item_price_deducted+=$sub_item_price_deducted;
			$shipping_price_deducted+=$sub_shipping_price_deducted;
			$total_price_deducted+=$sub_total_price_deducted;
		}
		//////////////////////////////
		if($refund_remaining_deductions=="" && $refund_all==""){
			$item_price_deducted=0;
			$shipping_price_deducted=0;
			$total_price_deducted=0;
		}
	 	$sql="insert into sub_returns_desired set 
									
									order_item_id='$order_item_id',
									return_order_id='$return_order_id',
									refund_method='$refund_method',
									refund_bank_id='$refund_bank_id',
									item_price_deducted='$item_price_deducted',
									shipping_price_deducted='$shipping_price_deducted',
									total_price_deducted='$total_price_deducted',
									quantity_requested='$refund_remaining_quantities_input',refund_remaining_deductions='$refund_remaining_deductions',refund_remaining_quantities='$refund_remaining_quantities',refund_all='$refund_all',promotion_available='$promotion_available',promotion_minimum_quantity='$promotion_minimum_quantity',promotion_quote='$promotion_quote',promotion_default_discount_promo='$promotion_default_discount_promo',promotion_item='$promotion_item',promotion_item_num='$promotion_item_num',promotion_surprise_gift='$promotion_surprise_gift',promotion_surprise_gift_type='$promotion_surprise_gift_type',promotion_surprise_gift_skus='$promotion_surprise_gift_skus',promotion_surprise_gift_skus_nums='$promotion_surprise_gift_skus_nums',promotion_cashback='$promotion_cashback',promotion_item_multiplier='$promotion_item_multiplier',promotion_discount='$promotion_discount',default_discount='$default_discount',quantity_without_promotion='$quantity_without_promotion',quantity_with_promotion='$quantity_with_promotion',cash_back_value='$cash_back_value',individual_price_of_product_with_promotion='$individual_price_of_product_with_promotion',individual_price_of_product_without_promotion='$individual_price_of_product_without_promotion',total_price_of_product_with_promotion='$total_price_of_product_with_promotion',total_price_of_product_without_promotion='$total_price_of_product_without_promotion',notify_refund_again='1'";
		
		
		$query =$this->db->query($sql);
		
		/*$sql_update_order_return_pickup="update orders_status set order_return_pickup='0' where 
									order_item_id='$order_item_id'";
		$query_update_order_return_pickup =$this->db->query($sql_update_order_return_pickup);*/
		
		return $query;
	}
	
	public function get_grandtotal_of_return_refund_success($order_item_id){
		$sql="select sum(item_with_deductions_concession) as item_with_deductions_concession,sum(shipping_with_deductions_concession) as shipping_with_deductions_concession from return_refund where order_item_id='$order_item_id' union select sum(item_with_deductions_concession) as item_with_deductions_concession,sum(shipping_with_deductions_concession) as shipping_with_deductions_concession from sub_return_refund where order_item_id='$order_item_id' ";
		$query=$this->db->query($sql);
		return $query->row()->item_with_deductions_concession+$query->row()->shipping_with_deductions_concession;
	}
	public function get_grandtotal_of_purchased_quantity($order_item_id){
		$sql="select grandtotal from orders_status where order_item_id='$order_item_id' ";
		$query=$this->db->query($sql);
		return $query->row()->grandtotal;
	}
	
	public function return_refund_request_again_not_refunded($order_item_id){
		$sql="select * from sub_returns_desired where order_item_id='$order_item_id' and admin_action!='refunded'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	
	public function return_refund_request_again_refunded($order_item_id){
		$sql="select * from sub_returns_desired where order_item_id='$order_item_id' and admin_action='refunded'";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	public function return_refund_request_again($order_item_id){
		$sql="select * from sub_returns_desired where order_item_id='$order_item_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_sub_return_refund_table_by_order_item_id($order_item_id){
		$sql="select sub_return_refund.* from sub_return_refund,sub_returns_desired where sub_return_refund.order_item_id ='$order_item_id' and sub_return_refund.sub_returns_desired_id=sub_returns_desired.sub_returns_desired_id and sub_returns_desired.admin_action='refunded'";
		$query =$this->db->query($sql);
		return $query->row_array();
	}
	
		/////////////////////////// replacements starts////////////////////////////////////

		
	public function get_unread_return_msg_replacement($return_order_id){
		$sql="select count(*) as count from  replacement_replies where user_type='admin' and return_order_id='$return_order_id' and is_read='0'";
		$query=$this->db->query($sql);
		return $query->row()->count;
	}
	
	public function get_return_order_id_by_order_item_id_for_replacement($order_item_id){
		$sql="select return_order_id from replacement_desired where order_item_id='$order_item_id'";
		$query=$this->db->query($sql);
		return $query->row()->return_order_id;
	}
	
	public function get_returns_conservation_chain_in_customerpanel_for_replacement($return_order_id){
		$sql="select * from replacement_replies where return_order_id='$return_order_id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function get_order_details_admin_acceptance_refund_data_for_replacement($order_item_id){
		$sql="select order_replacement_decision.*,admin_replacement_reason.pickup_identifier,replacement_desired.replacement_value_each_inventory_id from order_replacement_decision,admin_replacement_reason,replacement_desired where  order_replacement_decision.sub_reason_return_id=admin_replacement_reason.id and order_replacement_decision.order_item_id='$order_item_id' and replacement_desired.return_order_id=order_replacement_decision.return_order_id and replacement_desired.order_item_id=order_replacement_decision.order_item_id";
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	
	public function contact_admin_replacement($return_order_id,$user_type,$status,$description,$image_path,$customer_id,$admin_action,$customer_action){
		$description=$this->db->escape_str($description);
		$sql="insert into replacement_replies set return_order_id='$return_order_id',user_type='$user_type',status='$status',description='$description',image='$image_path',customer_id='$customer_id',admin_action='$admin_action',customer_action='$customer_action'";
		$query=$this->db->query($sql);
		return $query;
	}
	
	public function update_msg_status_replacement($return_order_id){
		$sql="update replacement_replies set is_read=1 where user_type='admin' and return_order_id='$return_order_id'";
		$query=$this->db->query($sql);
		return $query;
	}
	
	public function getspecified_returns_file_replacement($id){
	$query=$this->db->query("SELECT *  FROM replacement_replies where id='$id'");
		if(count($query->result())==1){
			return $query->result();
		}
	}
	
	public function get_availability_of_order_item_id_in_refunds_replacements($order_item_id){
			$sql="select * from replacement_desired where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->num_rows();
		}
		
	
	public function get_inventory_info_by_inventory_id($inventory_id){
			$sql="select * from inventory where id='$inventory_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
	public function get_primary_replacement_reason_admin($reason_return_id){
			$sql="select return_options from admin_replacement_reason where id='$reason_return_id'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row()->return_options;
			}
			else{
				return false;
			}
		}
		
	public function accept_reject_replacement_status($order_item_id,$st){
		if($st=="reject"){
		$sql="update order_replacement_decision set customer_decision='$st',notify_replacement_reject='1' where order_item_id='$order_item_id'";
		$query=$this->db->query($sql);
		}if($st=="accept"){
			$sql="update order_replacement_decision set customer_decision='$st',notify_replacement_accept='1' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
		}
		$sql_update_order_replacement_decision="update replacement_desired set order_replacement_decision_customer_decision='$st' where order_item_id='$order_item_id'";
		$query_update_order_replacement_decision=$this->db->query($sql_update_order_replacement_decision);
		
		$sql_orders_status="update orders_status set order_replacement_pickup='0' where order_item_id='$order_item_id'";
		$query_orders_status=$this->db->query($sql_orders_status);
		
		return $query;
	}
	
	public function get_replaced_orders_by_prev_order_item_id($prev_order_item_id){
		$sql="select * from replaced_orders where prev_order_item_id='$prev_order_item_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	
	public function update_payment_status($order_item_id,$customer_payment_ways){
		if($customer_payment_ways=="cod"){
			$sql_update_replacement_desired="update replacement_desired set balance_amount_paid_by='$customer_payment_ways',payment_status='pending' where order_item_id='$order_item_id'";
		}else{
			$sql_update_replacement_desired="update replacement_desired set balance_amount_paid_by='$customer_payment_ways',payment_status='paid' where order_item_id='$order_item_id'";
			
			$sql_update_order_replacement="update order_replacement_decision set notify_ready_for_replace='1' where order_item_id='$order_item_id'";
			$query_update_order_replacement=$this->db->query($sql_update_order_replacement);
		}
		$query_update_replacement_desired=$this->db->query($sql_update_replacement_desired);
		echo $query_update_replacement_desired;
	}
	public function get_return_stock_notification_data_by_order_item_id($order_item_id){
		$sql="select * from return_stock_notification where order_item_id='$order_item_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	public function delete_from_return_stock_notification_by_order_item_id($order_item_id){
		$sql="delete from return_stock_notification where order_item_id='$order_item_id'";
		$query=$this->db->query($sql);
		return $query;
	}
	public function get_history_orders_data_by_order_item_id($order_item_id){
		$sql="select * from history_orders where order_item_id='$order_item_id'";
		$query=$this->db->query($sql);
		return $query->row();
	}
	public function insert_into_returns_replacement_desired_table_from_stock($return_order_id,$order_item_id,$cancel_reason,$sub_cancel_reason,$cancel_reason_comment,$desired_quantity_replacement,$selected_inventory_id_for_rep,$replacement_value_each,$balance_amount_paid_by,$refund_method,$refund_bank_id,$paid_by="",$amount_paid="",$amount_paid_item_price="",$payment_status="",$return_value_each="",$shipping_charge_purchased="",$shipping_charge_requested="",$shipping_charge_percent_paid_by_customer="",$original_inventory_id="",$order_item_invoice_discount_value_each=""){

		$amount_paid_item_price=$amount_paid;
		//$amount_paid_original=
	
		if($amount_paid_item_price==0){
			$amount_paid=$shipping_charge_requested;
		}
		if($amount_paid_item_price!=0 && $paid_by=="customer"){
			$amount_paid=$amount_paid_item_price+$shipping_charge_requested;
		}
		if($amount_paid_item_price!=0 && $paid_by=="admin"){
			if($amount_paid_item_price<$shipping_charge_requested){
				$paid_by="customer";
				$amount_paid=abs($amount_paid_item_price-$shipping_charge_requested);
			}
			if($amount_paid_item_price>$shipping_charge_requested){
				$paid_by="admin";
				$amount_paid=abs($amount_paid_item_price-$shipping_charge_requested);
			}
			if($amount_paid_item_price==$shipping_charge_requested){
				$paid_by="";
				$amount_paid=abs($amount_paid_item_price-$shipping_charge_requested);
			}
		}
		if($amount_paid_item_price!=0 && $paid_by==""){
			if($amount_paid_item_price<$shipping_charge_requested){
				$paid_by="customer";
				$amount_paid=abs($amount_paid_item_price-$shipping_charge_requested);
			}
			if($amount_paid_item_price>$shipping_charge_requested){
				$paid_by="admin";
				$amount_paid=abs($amount_paid_item_price-$shipping_charge_requested);
			}
			if($amount_paid_item_price==$shipping_charge_requested){
				$paid_by="";
				$amount_paid=abs($amount_paid_item_price-$shipping_charge_requested);
			}
		}
		if($balance_amount_paid_by!=""){
			$payment_status="pending";
		}
		
		$order_replacement_decision_customer_status="";
		
		$cancel_reason_comment=$this->db->escape_str($cancel_reason_comment);
	
		$sql="insert into replacement_desired (return_order_id,order_item_id,quantity_replacement,	reason_return_id,sub_issue_id,return_reason_comments,replacement_with_inventory_id,replacement_value_each_inventory_id,balance_amount_paid_by,refund_method,refund_bank_id,paid_by,amount_paid_item_price,amount_paid,payment_status,return_value_each_inventory_id,order_replacement_decision_status,order_replacement_decision_customer_decision,order_replacement_decision_customer_status,amount_paid_original,paid_by_original,amount_paid_item_price_original,shipping_charge_purchased,shipping_charge_requested,shipping_charge_percent_paid_by_customer,original_inventory_id,order_item_invoice_discount_value_each) values('$return_order_id','$order_item_id','$desired_quantity_replacement','$cancel_reason','sub_issue_id','$cancel_reason_comment','$selected_inventory_id_for_rep','$replacement_value_each','$balance_amount_paid_by','$refund_method','$refund_bank_id','$paid_by','$amount_paid_item_price','$amount_paid','$payment_status','$return_value_each','pending','pending','$order_replacement_decision_customer_status','$amount_paid','$paid_by','$amount_paid_item_price','$shipping_charge_purchased','$shipping_charge_requested','$shipping_charge_percent_paid_by_customer','$original_inventory_id','$order_item_invoice_discount_value_each')";
		
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
		
	}
	public function insert_into_order_replacement_decision_table_from_stock($return_order_id,$customer_id,$quantity,$balance_amount,$inventory_id,$replacement_with_inventory_id,$product_price,$shipping_charge,$order_item_id,$sub_reason_return_id,$status,$comments,$disable_comm_flag,$allow_cust_to_make_decision_flag,$customer_decision,$vendor_id,$customer_status,$customer_status_comments,$return_shipping_concession_chk,$return_shipping_concession){
		
		$comments=$this->db->escape_str($comments);
		$customer_status_comments=$this->db->escape_str($customer_status_comments);
		$sql="insert into order_replacement_decision set 
							return_order_id='$return_order_id',
							customer_id='$customer_id',
							quantity='$quantity',
							balance_amount='$balance_amount',
							inventory_id='$inventory_id',
							replacement_with_inventory_id='$replacement_with_inventory_id',
							product_price='$product_price',
							shipping_charge='$shipping_charge',
							order_item_id='$order_item_id',
							sub_reason_return_id='$sub_reason_return_id',
							status='$status',
							comments='$comments',
							disable_comm_flag='$disable_comm_flag',
							allow_cust_to_make_decision_flag='$allow_cust_to_make_decision_flag',
							customer_decision='$customer_decision',
							vendor_id='$vendor_id',
							customer_status='$customer_status',
							customer_status_comments='$customer_status_comments',
							return_shipping_concession_chk='$return_shipping_concession_chk',
							return_shipping_concession='$return_shipping_concession'";
		
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function proceed_for_cancellation_from_stock($order_item_id){
		$sql="update return_stock_notification set status='cancelled',notify_stock_yes_no_decision=1 where order_item_id='$order_item_id'";
		$query=$this->db->query($sql);
		return $query;
	}
	public function customer_decision_for_move_to_refund_from_stock($order_item_id,$customer_decision){
		if($customer_decision=="no"){
			return $this->proceed_for_cancellation_from_stock($order_item_id);
		}
		else{
			$sql="update return_stock_notification set customer_decision='$customer_decision',notify_stock_yes_no_decision=1 where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query;
		}
	}
	
	public function replacement_cancel($order_item_id,$cancel_replacement_comments,$type_of_replacement_cancel){
		$cancel_replacement_comments=$this->db->escape_str($cancel_replacement_comments);
		$sql="update replacement_desired set order_replacement_decision_customer_status='cancel',order_replacement_decision_customer_status_comments='$cancel_replacement_comments',order_replacement_decision_refund_status='need_to_refund',type_of_replacement_cancel='$type_of_replacement_cancel' where order_item_id='$order_item_id' ";
		$result=$this->db->query($sql);
	
		$sql2="update replacement_desired,orders_status set replacement_desired.notify_cancelled_replacement=1 where replacement_desired.order_item_id='$order_item_id' and  (replacement_desired.order_replacement_decision_refund_status!='refunded' and orders_status.order_replacement_pickup='0' and ((replacement_desired.payment_status!='paid' and replacement_desired.paid_by='customer') or (replacement_desired.paid_by='admin') or (replacement_desired.paid_by=''))) and orders_status.order_item_id='$order_item_id'";
		$result=$this->db->query($sql2);
		
		
		$sql_order_return_decision="update order_replacement_decision set customer_status='cancel',customer_status_comments='$cancel_replacement_comments',refund_status='need_to_refund' where order_item_id='$order_item_id'";
		$result_order_return_decision=$this->db->query($sql_order_return_decision);

		$sql3="update orders_status,order_replacement_decision,admin_replacement_reason,replacement_desired set order_replacement_decision.notify_replacement_cancel=1 where ((orders_status.order_replacement_pickup!=0 and admin_replacement_reason.pickup_identifier!='before replace') and (replacement_desired.payment_status!='pending' or replacement_desired.payment_status!='')) or (orders_status.order_replacement_pickup!=0 and admin_replacement_reason.pickup_identifier!='after replace') and order_replacement_decision.order_item_id='$order_item_id' and admin_replacement_reason.id=order_replacement_decision.sub_reason_return_id and orders_status.order_item_id=replacement_desired.order_item_id and order_replacement_decision.order_item_id=replacement_desired.order_item_id";
		
		$result3=$this->db->query($sql3);
		
		
		return $result;
	}
	public function get_order_shipped_of_item_by_return_order_id($return_order_id){
		$sql="select order_shipped from  orders_status where return_order_id='$return_order_id' and type_of_order='replaced'";
		$result=$this->db->query($sql);
		if(!empty($result->row())){
			if($result->row()->order_shipped=="1"){
				return "yes";
			}
			else{
				return "no";
			}
		}
		else{
			return "no";
		}
	}
	
	public function get_active_orders_data($prev_order_item_id){
		$sql="select * from active_orders where prev_order_item_id='$prev_order_item_id'";
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	
	public function get_replacement_desired_data_by_order_item_id($order_item_id){
			$sql="select * from replacement_desired where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row_array();
		}
	public function refund_method_to_customer_for_cancels_insert($order_item_id,$refund_method,$refund_bank_id){
		$sql="update replacement_desired set refund_method='$refund_method',refund_bank_id='$refund_bank_id' where order_item_id='$order_item_id'";
		$query=$this->db->query($sql);
		return $query;
	}
	
	public function get_replaced_quantity_in_replacements($order_item_id){
		$sql="select * from order_replacement_decision where order_item_id='$order_item_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	
	public function cancel_refund_request($return_stock_notification_id){
			$sql="update return_stock_notification set status='cancelled',notify_cancelled_return_stock=1 where return_stock_notification_id='$return_stock_notification_id'";
			$query=$this->db->query($sql);
			return $query;
	}
	public function get_minimum_and_maximum_order_quantity($inventory_id){
		
		$sql="select moq,max_oq from inventory where id='$inventory_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	
	public function update_cancels_table($order_item_id,$refund_method,$refund_bank_id){
		$sql="update cancels set refund_type='$refund_method',refund_bank_id='$refund_bank_id',refund_type_updated_by='customer' where order_item_id='$order_item_id'";
		$query=$this->db->query($sql);
			
	}
	
	public function get_refund_bank_account_details($get_bank_account_id){
		$sql="select * from refund_bank_details where id='$get_bank_account_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	
	public function add_bank_details_delivered($bank_name,$bank_branch_name,$bank_city,$bank_ifsc_code,$bank_account_number,$bank_account_confirm_number,$account_holder_name,$bank_return_refund_phone_number,$customer_id){
		
		$sql="insert into delivered_refund_bank_details (bank_name,branch_name,city,ifsc_code,account_number,confirm_account_number,account_holder_name,mobile_number,customer_id) values('$bank_name','$bank_branch_name','$bank_city','$bank_ifsc_code','$bank_account_number','$bank_account_confirm_number','$account_holder_name','$bank_return_refund_phone_number','$customer_id')";
		$result=$this->db->query($sql);
		
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	public function get_logistics_details($order_item_id){
		 $sql="select inventory_id, logistics_id,logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id from active_orders where order_item_id='$order_item_id' union select inventory_id,logistics_id, 	logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id from cancelled_orders where order_item_id='$order_item_id' union select inventory_id,logistics_id, 	logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id from completed_orders where order_item_id='$order_item_id' union select inventory_id,logistics_id, 	logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id from returned_orders where order_item_id='$order_item_id'";
	
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	public function get_logistics_inventory($inventory_id){
		$sql="select inventory_moq,inventory_weight_in_kg,inventory_volume_in_kg from logistics_inventory where inventory_id='$inventory_id'";
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	
	
	public function get_data_from_history_orders($order_item_id){
			$sql="select * from history_orders where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		public function get_logistics_price_for_desired_quantity_refund($desired_quantity_refund,$order_item_id,$calculated_weight_in_kg,$inventory_weight_in_kg,$inventory_volume_in_kg,$inventory_id,$logistics_territory_id="",$default_delivery_mode_id="",$logistics_parcel_category_id=""){
			$get_logistics_inventory_data_obj=$this->get_logistics_inventory_data($inventory_id);
			$get_data_from_history_orders_obj=$this->get_data_from_history_orders($order_item_id);
			$current_quantity=$desired_quantity_refund;
			$inventory_id=$get_data_from_history_orders_obj->inventory_id;
			$logistics_id=$get_data_from_history_orders_obj->logistics_id;
			$inventory_weight_in_kg_per_unit=$inventory_weight_in_kg/$current_quantity;
			
			$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
			$logistics_volumetric_value=$get_logistics_by_logistics_id_obj->logistics_volumetric_value;
			///////////////////
			
			
				/* new code - nanthini  */
				
				$weight=$inventory_weight_in_kg;
				$sql="SELECT logistics_price,SUBSTRING_INDEX(weight_in_kg,'-',1) as 'weight_in_kg_from',SUBSTRING_INDEX(weight_in_kg,'-',-1) as 'weight_in_kg_to'
	FROM logistics_weight where $weight between SUBSTRING_INDEX(weight_in_kg,'-',1) and SUBSTRING_INDEX(weight_in_kg,'-',-1) and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id'";
				
				
				/*$sql="SELECT logistics_price,
				REPLACE(weight_in_kg, '-', ',') as dashRep
				FROM logistics_weight
				WHERE FIND_IN_SET($weight, REPLACE(weight_in_kg, '-', ',')) and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id'";*/
	
				$query=$this->db->query($sql);
	
				$res=$query->row_array();
				$logistics_price_with_tax=$res["logistics_price"];
				$required_volume_in_kg=$inventory_weight_in_kg;
	
				//print_r($res);
	
				/* new code - nanthini  */
	
				return $logistics_price_with_tax;
				
			
			
			/*
				/////// for all the skus code 4-13-2021 based on packings not volumetric weight starts/////
				
				$inventory_length=$get_logistics_inventory_data_obj->inventory_length;
				$inventory_breadth=$get_logistics_inventory_data_obj->inventory_breadth;
				$inventory_height=$get_logistics_inventory_data_obj->inventory_height;
				
				$inventory_moq=$get_logistics_inventory_data_obj->inventory_moq;
				$suitable_box_arr=array();
				$get_logistics_parcel_box_dimension_info_result=$this->get_logistics_parcel_box_dimension_info($logistics_id);
				foreach($get_logistics_parcel_box_dimension_info_result as $get_logistics_parcel_box_dimension_info_row){
					$available_space_for_filling_length=$get_logistics_parcel_box_dimension_info_row->available_space_for_filling_length;
					$available_space_for_filling_breadth=$get_logistics_parcel_box_dimension_info_row->available_space_for_filling_breadth;
					$available_space_for_filling_height=$get_logistics_parcel_box_dimension_info_row->available_space_for_filling_height;
				
					if($inventory_length<=$available_space_for_filling_length && $inventory_breadth<=$available_space_for_filling_breadth & $inventory_height<=$available_space_for_filling_height){
							$suitable_box_arr[$get_logistics_parcel_box_dimension_info_row->logistics_parcel_box_dimension_id]=0;
					}
				}
				
				foreach($suitable_box_arr as $logistics_parcel_box_dimension_id => $val){
					$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row=$this->get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id($logistics_parcel_box_dimension_id);
					$box_available_space_for_filling_length=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->available_space_for_filling_length;
					$box_available_space_for_filling_breadth=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->available_space_for_filling_breadth;
					$box_available_space_for_filling_height=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->available_space_for_filling_height;
					
					$box_available_space_for_filling_length_temp=$box_available_space_for_filling_length;
					$box_available_space_for_filling_breadth_temp=$box_available_space_for_filling_breadth;
					$box_available_space_for_filling_height_temp=$box_available_space_for_filling_height;
					$flag=true;
					
					while($flag){
						if($inventory_length<=$box_available_space_for_filling_length_temp){
							$suitable_box_arr[$logistics_parcel_box_dimension_id]++;
							
							$box_available_space_for_filling_length_temp=$box_available_space_for_filling_length_temp-$inventory_length;
							$box_available_space_for_filling_breadth_temp=$box_available_space_for_filling_breadth_temp-$inventory_breadth;
							$box_available_space_for_filling_height_temp=$box_available_space_for_filling_height_temp-$inventory_height;
									
									
						}
						else{
							$flag=false;
						}
					}
					
				}
				arsort($suitable_box_arr);
				
				//print_r($suitable_box_arr);
				$no_of_boxes_arr=[];
				$extra_box_arr=[];
				$quantity_filled=0;
				$current_quantity_temp=$current_quantity;
				foreach($suitable_box_arr as $box_id => $box_fitted_quantity){
					if($current_quantity_temp>=$box_fitted_quantity){
						$no_of_boxes_arr[$box_id]["no_of_boxes"]=(int)($current_quantity_temp/$box_fitted_quantity);
						$quantity_filled+=($no_of_boxes_arr[$box_id]["no_of_boxes"]*$suitable_box_arr[$box_id]);
						$current_quantity_temp=$current_quantity_temp%$box_fitted_quantity;
					}
				}
				
				$remaining_quantity=$current_quantity-$quantity_filled;
				if($remaining_quantity>0){
					foreach($suitable_box_arr as $box_id => $box_fitted_quantity){
						if((100-(($remaining_quantity/$box_fitted_quantity))*100)<=50){ // remaining space
							$extra_box_arr[$box_id]["no_of_quantities"]=$remaining_quantity;
						}
					}
				}
				//print_r($no_of_boxes_arr);
				//print_r($extra_box_arr);
				$total_arr=array("no_of_boxes_arr"=>$no_of_boxes_arr,"extra_box_arr"=>$extra_box_arr);
				$max_actual_volumetric_arr=array();
				foreach($total_arr as $k=>$v_arr){
					if($k=="no_of_boxes_arr"){
						if(!empty($v_arr)){
							foreach($v_arr as $box_id => $boxes_arr){
								$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row=$this->get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id($box_id);
								
								$parcel_lbh=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_length*
								$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_breadth*
								$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_height;
								for($i=1;$i<=$boxes_arr["no_of_boxes"];$i++){
									$box_actual_weight_without_packing=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->box_actual_weight_without_packing;
									
									$total_actual_weight_parcel_sku=($inventory_weight_in_kg_per_unit*$suitable_box_arr[$box_id])+$box_actual_weight_without_packing;
									
									$volumetric_weight_of_parcel_in_kg=$parcel_lbh/$logistics_volumetric_value;
									
									$max_actual_volumetric_arr[]=max($total_actual_weight_parcel_sku,$volumetric_weight_of_parcel_in_kg);
								}
								//$boxes_arr["no_of_boxes"]
							}
						}
					}
					if($k=="extra_box_arr"){
						if(!empty($v_arr)){
							foreach($v_arr as $box_id => $boxes_arr){
								$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row=$this->get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id($box_id);
								
								$parcel_lbh=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_length*
								$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_breadth*
								$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_height;
								
								$box_actual_weight_without_packing=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->box_actual_weight_without_packing;
								
								$total_actual_weight_parcel_sku=($inventory_weight_in_kg_per_unit*$boxes_arr["no_of_quantities"])+$box_actual_weight_without_packing;
								
								$volumetric_weight_of_parcel_in_kg=$parcel_lbh/$logistics_volumetric_value;
								
								$max_actual_volumetric_arr[]=max($total_actual_weight_parcel_sku,$volumetric_weight_of_parcel_in_kg);
								
								//$boxes_arr["no_of_boxes"]
							}
						}
					}
				}
				//print_r($max_actual_volumetric_arr);
				$required_volume_in_kg=array_sum($max_actual_volumetric_arr);
				//$required_volume_in_kg=2;
				
				
				
				/////// for all the skus code  4-13-2021 based on packings not volumetric weight ends/////
				
				
			
			
				//$get_logistics_inventory_data_obj=$this->get_logistics_inventory_data($inventory_id);
				//$inventory_volume_in_lbh_cms=$get_logistics_inventory_data_obj->inventory_volume_in_kg;
				//$quantity_volumetric_breakup_point=$get_logistics_inventory_data_obj->volumetric_breakup_point;
				
				//$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
				//$logistics_volumetric_value=$get_logistics_by_logistics_id_obj->logistics_volumetric_value;
				//$net_volumetric_volume_weight=$inventory_volume_in_lbh_cms/$logistics_volumetric_value;
				
			//	if($current_quantity<($quantity_volumetric_breakup_point+1)){
				//	$resultant_inventory_volume_in_kg=$net_volumetric_volume_weight;
				//}
				//else{
			//	//	$resultant_inventory_volume_in_kg=(intval(($current_quantity-1)/$quantity_volumetric_breakup_point)+1)*$net_volumetric_volume_weight;
				//}
				
			//////////////
			
			//$required_volume_in_kg=max($calculated_weight_in_kg,$resultant_inventory_volume_in_kg);
			
			//$sql="SELECT logistics_price,volume_in_kg FROM logistics_weight where SUBSTRING_INDEX(weight_in_kg, '-', 1) <=$required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(weight_in_kg, '-', 2), '-', -1) >=$required_volume_in_kg order by volume_in_kg";
				
				//$query=$this->db->query($sql);
				//$logistics_price=0;
				//foreach($query->result() as $resObj){
					//$logistics_price=$resObj->logistics_price;
					//break;
				//}
				//return $logistics_price;
				
				
				
				$logistics_price=0;
				$sql="SELECT logistics_price,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg!='yes'";
				
				$query=$this->db->query($sql);
				if(!empty($query->result())){
					$logistics_price=0;
					foreach($query->result() as $resObj){
						$logistics_price=$resObj->logistics_price;
						break;
					}
					//////////////////// Tax caculation starts /////
					$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
					$logistics_gst=$get_logistics_by_logistics_id_obj->logistics_gst;
					$logistics_fuelsurcharge=$get_logistics_by_logistics_id_obj->logistics_fuelsurcharge;
					$logistics_tax=$logistics_gst+$logistics_fuelsurcharge;
					$logistics_price_with_tax=$logistics_price+($logistics_price*($logistics_tax/100));
					//////////////////// Tax caculation ends /////
					return $logistics_price_with_tax;
				}
				
				/// if parcel category id is there no problem
				$sql_additional_weight_in_kg="SELECT logistics_price,volume_in_kg,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg='yes'";
				
				$query_additional_weight_in_kg=$this->db->query($sql_additional_weight_in_kg);
				if(!empty($query_additional_weight_in_kg->result())){
					//////////////////
					$sql_take_latest_price_before_breakpoint="SELECT logistics_price,volume_in_kg,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where  territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg!='yes' order by logistics_weight_id desc limit 0,1";
					$query_take_latest_price_before_breakpoint=$this->db->query($sql_take_latest_price_before_breakpoint);
					$logistics_price_latest_price_before_breakpoint=$query_take_latest_price_before_breakpoint->row_array()["logistics_price"];
					$logistics_weight_latest_price_before_breakpoint=$query_take_latest_price_before_breakpoint->row_array()["volume_in_kg"];
					$logistics_weight_latest_price_before_breakpoint_max=explode("-",$logistics_weight_latest_price_before_breakpoint)[1];
					/////////////////
					$logistics_price=0;
					foreach($query_additional_weight_in_kg->result() as $resObj){
						$additional_weight_value_in_kg=$resObj->additional_weight_value_in_kg;
						$do_u_want_to_roundup=$resObj->do_u_want_to_roundup;
						$do_u_want_to_include_breakupweight=$resObj->do_u_want_to_include_breakupweight;
						if($do_u_want_to_roundup=="yes" && $do_u_want_to_include_breakupweight=="yes"){
							$result_value_of_weights=(ceil($required_volume_in_kg-$logistics_weight_latest_price_before_breakpoint_max))/$additional_weight_value_in_kg;
							$logistics_price=$logistics_price_latest_price_before_breakpoint+(($result_value_of_weights)*($resObj->logistics_price));
						}
						if($do_u_want_to_roundup=="yes" && $do_u_want_to_include_breakupweight=="no"){
							$logistics_price=(ceil($required_volume_in_kg)/$additional_weight_value_in_kg)*$resObj->logistics_price;
						}
						if($do_u_want_to_roundup=="no"){
							$logistics_price_part_1=0;
							$logistics_price_part_2=0;
							
							$decimalpart_of_required_volume_in_kg=floor($required_volume_in_kg);
							$fractionalpart_of_required_volume_in_kg=$required_volume_in_kg-$decimalpart_of_required_volume_in_kg;
							$logistics_price_part_1=($decimalpart_of_required_volume_in_kg/$additional_weight_value_in_kg)*$resObj->logistics_price;
							
							$sql_fractional_part="SELECT logistics_price,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$fractionalpart_of_required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$fractionalpart_of_required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg!='yes'";
							$query_fractional_part=$this->db->query($sql_fractional_part);
							if(!empty($query_fractional_part->result())){
								foreach($query_fractional_part->result() as $resObj_fractional_part){
									$logistics_price_part_2=$resObj_fractional_part->logistics_price;
									break;
								}
							}
							$logistics_price=$logistics_price_part_1+$logistics_price_part_2;
							
						}
						break;
					}
					//////////////////// Tax caculation starts /////
					$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
					$logistics_gst=$get_logistics_by_logistics_id_obj->logistics_gst;
					$logistics_fuelsurcharge=$get_logistics_by_logistics_id_obj->logistics_fuelsurcharge;
					$logistics_tax=$logistics_gst+$logistics_fuelsurcharge;
					$logistics_price_with_tax=$logistics_price+($logistics_price*($logistics_tax/100));
					//////////////////// Tax caculation ends /////
					return $logistics_price_with_tax;
				}
				//////////////////// Tax caculation starts /////
					$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
					$logistics_gst=$get_logistics_by_logistics_id_obj->logistics_gst;
					$logistics_fuelsurcharge=$get_logistics_by_logistics_id_obj->logistics_fuelsurcharge;
					$logistics_tax=$logistics_gst+$logistics_fuelsurcharge;
					$logistics_price_with_tax=$logistics_price+($logistics_price*($logistics_tax/100));
					//////////////////// Tax caculation ends /////
				return $logistics_price_with_tax;*/
				
		}
		
	public function add_wallet_transfer_bank($customer_id,$wallet_id,$bank_name,$branch_name,$city,$ifsc_code,$bank_account_number,$confirm_account_number,$account_holder_name,$mobile_number,$transaction_amount,$wallet_request_id,$wallet_trans_id_in){
		//$mobile_number=str_replace(" |","",$mobile_number);
		if(isset($bank_account_number)){
			$get_bank_account_id=$this->get_bank_account_details($bank_account_number,$customer_id);
			
		if(!$get_bank_account_id){
			$sql="insert into wallet_transaction_bank (bank_name,branch_name,city,ifsc_code,account_number,confirm_account_number,account_holder_name,mobile_number,customer_id,wallet_id,transaction_amount,requested_transaction_amount,wallet_request_id) values('$bank_name','$branch_name','$city','$ifsc_code','$bank_account_number','$confirm_account_number','$account_holder_name','$mobile_number','$customer_id','$wallet_id','$transaction_amount','$transaction_amount','$wallet_request_id')";
			$result=$this->db->query($sql);

			$sql="insert into refund_bank_details (bank_name,branch_name,city,ifsc_code,account_number,confirm_account_number,account_holder_name,mobile_number,customer_id) values('$bank_name','$branch_name','$city','$ifsc_code','$bank_account_number','$confirm_account_number','$account_holder_name','$mobile_number','$customer_id')";
			$result=$this->db->query($sql);
			
			
			$sql="update wallet_transaction set claim_status=1,credit_bank_transfer='2' where id in ($wallet_trans_id_in)";
			$result=$this->db->query($sql);
		}elseif($get_bank_account_id){
			$sql="insert into wallet_transaction_bank (bank_name,branch_name,city,ifsc_code,account_number,confirm_account_number,account_holder_name,mobile_number,customer_id,wallet_id,transaction_amount,requested_transaction_amount,wallet_request_id) values('$bank_name','$branch_name','$city','$ifsc_code','$bank_account_number','$confirm_account_number','$account_holder_name','$mobile_number','$customer_id','$wallet_id','$transaction_amount','$transaction_amount','$wallet_request_id')";
			$result=$this->db->query($sql);
			
			$sql="update wallet_transaction set claim_status=1,credit_bank_transfer='2' where id in ($wallet_trans_id_in)";
			$result=$this->db->query($sql);
		}
	}
		
		
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function get_wallet_transaction(){
		$customer_id=$this->session->userdata("customer_id");
		$sql="select * from wallet_transaction where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	
	public function get_customer_wallet_transaction_bank_details($customer_id){
		$sql="select * from wallet_transaction_bank where customer_id='$customer_id' and allow_update_bank_details='yes'";
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	public function get_bank_account_number_bank_id_wallet_transaction_bank_id($wallet_transaction_bank_id){
		$sql="select distinct account_number from wallet_transaction_bank where id='$wallet_transaction_bank_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->account_number;
		}
		else{
			return false;
		}
	}
	
	public function get_bank_detail_wallet_transaction_bank_id($account_number,$customer_id){
		$sql="select * from wallet_transaction_bank where customer_id='$customer_id' and account_number='$account_number'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function update_wallet_bank_transfer_details_action($bank_id,$bank_name_rep,$bank_branch_name_rep,$bank_city_rep,$bank_ifsc_code_rep,$bank_account_number_rep,$bank_account_confirm_number_rep,$account_holder_name_rep,$bank_return_refund_phone_number_rep){
		$sql="update wallet_transaction_bank set 
									bank_name='$bank_name_rep',
									branch_name='$bank_branch_name_rep',
									city='$bank_city_rep',
									ifsc_code='$bank_ifsc_code_rep',
									account_number='$bank_account_number_rep',
									confirm_account_number='$bank_account_confirm_number_rep',
									account_holder_name='$account_holder_name_rep',
									mobile_number='$bank_return_refund_phone_number_rep',allow_update_bank_details='updated' where id='$bank_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function update_cancel_table_refund_type($refund_method,$refund_bank_id,$order_item_id){
		$sql="update cancels set refund_type='$refund_method',refund_bank_id='$refund_bank_id',refund_type_updated_by='customer' where order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		return $result;
	}
	
	public function get_customer_wallet_summary_for_wallet_bank($customer_id){
			$sql="select * from wallet where customer_id='$customer_id' ";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->result_array();
			}
			else{
				return false;
			}
		}
		
		public function get_wallet_transaction_bank_table($customer_id){
		$sql="select * from wallet_transaction_bank where customer_id='$customer_id' order by id desc";
		$result=$this->db->query($sql);
		return $result->result_array();
	}	
	
	public function get_order_cancel_reason_by_reason_cancel_id($cancel_reason_id){
		$sql="select * from reason_cancel where cancel_reason_id='$cancel_reason_id'";
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	
	public function get_order_cancel_admin_reason_by_reason_cancel_id($cancel_reason_id){
		$sql="select * from admin_cancel_reason where cancel_reason_id='$cancel_reason_id'";
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	
	public function returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin($order_item_id){
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,products.product_description,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.payment_status,replacement_desired.shipping_charge_applied_cancels_chk,replacement_desired.return_shipping_concession_cancels_chk,order_replacement_decision.refund_status,order_replacement_decision.quantity from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return,order_replacement_decision where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and 
			order_replacement_decision.order_item_id = replacement_desired.order_item_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and replacement_desired.paid_by='customer' and replacement_desired.payment_status='paid' and replacement_desired.status_of_refund!='yes' and replacement_desired.order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
	
	public function get_sub_refunded_details_from_returned_orders_table($order_item_id){
			
			$sql_sub_returns_desired="select * from sub_returns_desired where order_item_id='$order_item_id'";
			$query_sub_returns_desired=$this->db->query($sql_sub_returns_desired);
			$query_sub_returns_desired_arr=$query_sub_returns_desired->row_array();
			if(!empty($query_sub_returns_desired_arr)){
				if($query_sub_returns_desired_arr["admin_action"]=="refunded"){
					$sql="select * from sub_return_refund where order_item_id='$order_item_id'";
					$query=$this->db->query($sql);
					return $query->row_array();
				}
				else{
					return array();
				}
			}
			else{
				return array();
			}
		}
		public function get_the_type_of_order_fresh_or_replaced($order_item_id){
			$sql="select type_of_order from orders_status where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result->row()->type_of_order;
		}
		public function get_undelivered_replacement_desired($order_item_id){
			$sql="select reason_for_undelivered from replacement_desired where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result->row()->reason_for_undelivered;
		}
		public function get_payment_options(){
			$sql="select * from payment_options where view=1 order by sort_order asc";
			$result=$this->db->query($sql);
			return $result->result();
		}
        public function get_wallet_amount($customer_id){
            $sql="select wallet_amount from wallet where customer_id='$customer_id'";
            $result=$this->db->query($sql);
            if($result->num_rows()>0){
                return $result->row()->wallet_amount;
            }
            else{
                return false;
            }
        }
        
    public function get_cancellation_policy_order($id,$order_id){
        $sql="select * from order_cancellation_policy where order_item_id='$id' and order_id='$order_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return false;
        }
    }
    public function get_refund_policy_order($id,$order_id){
        $sql="select * from order_refund_policy where order_item_id='$id' and order_id='$order_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return false;
        }
    }
    public function get_replacement_policy_order($id,$order_id){
        $sql="select * from order_replacement_policy where order_item_id='$id' and order_id='$order_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return false;
        }
    }
    
    public function get_delivered_time_of_order_item($order_id,$order_item_id){
        //$sql="select timestamp from completed_orders where order_item_id='$order_item_id' and order_id='$order_id'";
        $sql="select order_delivered_date,order_delivered_time from orders_status where order_item_id='$order_item_id' and order_delivered=1";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return false;
        }
    }
	public function get_per_unit_price_of_order_item($order_item_id){
		$sql="select subtotal,quantity from history_orders where order_item_id='$order_item_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return false;
        }
		
	}
	public function get_per_unit_price_of_order_item_from_active_orders($order_item_id){
		$sql="select subtotal,quantity from active_orders where order_item_id='$order_item_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return false;
        }
		
	}
	public function get_free_skus_from_inventory($promo_item_str){
		
		$sql="select products.product_name,products.product_description,inventory.sku_id,inventory.sku_name,inventory.image,inventory.id,inventory.attribute_1,inventory.attribute_1_value,inventory.attribute_2,inventory.attribute_2_value,inventory.attribute_3,inventory.attribute_3_value,inventory.attribute_4,inventory.attribute_4_value,inventory.thumbnail,inventory.image,inventory.selling_price from inventory,products where inventory.id in ($promo_item_str) and inventory.product_id=products.product_id";
		
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
            return $result->result();
        }else{
            return false;
        }
	}
	public function reduce_stocks_of_inventory($result){

		
		foreach($result as $data){

			$inventory_id=$data->inventory_id;
			$purchased_quantity=$data->inventory_moq;
			$promotion_available=$data->promotion_available;
			$promotion_minimum_quantity=$data->promotion_minimum_quantity;

			$str="";
			
			if($promotion_available==1){

				if(!empty($data->promotion_item)){
					
					$promo_item=$data->promotion_item;
					$promotion_item_num=$data->promotion_item_num;
					
					$promo_item_arr = explode(',', $promo_item);
					$promo_item_num_arr = explode(',', $promotion_item_num);
					
					$two=array_combine($promo_item_arr,$promo_item_num_arr);

					$temp_quotient=intval($purchased_quantity/$promotion_minimum_quantity);
					
					foreach($promo_item_arr as $prmo_item){
						
						$free_inv_id=intval($prmo_item);
						$reduce_count=intval($temp_quotient*$two[$prmo_item]);
						$sql_free_item="update inventory set stock = (stock-$reduce_count) where id='$free_inv_id' and stock >= $reduce_count; ";
						$result=$this->db->query($sql_free_item);
					}
					
				}
				
				if(!empty($data->promotion_surprise_gift_skus)){
					
					$promotion_surprise_gift=$data->promotion_surprise_gift;
					$promotion_surprise_gift_type=$data->promotion_surprise_gift_type;
					$promotion_surprise_gift_skus=$data->promotion_surprise_gift_skus;
					$promotion_surprise_gift_skus_nums=$data->promotion_surprise_gift_skus_nums;
					
					if($promotion_surprise_gift_type=="Qty"){
						$promotion_surprise_gift_skus=rtrim($promotion_surprise_gift_skus,',');
						$promotion_surprise_gift_skus_nums=rtrim($promotion_surprise_gift_skus_nums,',');
						
						$promo_gift_arr = explode(',', $promotion_surprise_gift_skus);
						$promo_gift_num_arr = explode(',', $promotion_surprise_gift_skus_nums);
						
						$two=array_combine($promo_gift_arr,$promo_gift_num_arr);
						foreach($two as $inv_id=>$stock_count){
							$sql_free_item_gift="update inventory set stock = (stock-$stock_count) where id='$inv_id' and stock >= $stock_count; ";
							$result=$this->db->query($sql_free_item_gift);
						}
					}

				}
	
			}
			
			
			
			/* addon stock reduce */
			$addon_inventories=$data->addon_inventories;
			if($addon_inventories!=''){
				$addon_arr=explode(',',$addon_inventories);
				$addon_arr=array_filter($addon_arr);
				$addon_inventories=implode(',',$addon_arr);
				foreach($addon_arr as $inv_id){
					if($inv_id!=''){
						$sql_1="update inventory set stock = (stock-1) where id='$inv_id' and stock >= 1; ";
						$this->db->query($sql_1);
					}
				}
			}else{
				$sql_inv="update inventory set stock = (stock-$purchased_quantity) where id='$inventory_id' and stock >= $purchased_quantity; ";
				$result=$this->db->query($sql_inv);
			}
			/* addon stock reduce */

		}
		return $result;

	}
	
	public function reduce_stocks_of_inventory_when_replacement_request($selected_inventory_id_for_rep,$desired_quantity_replacement){
		
		$sql_inv="update inventory set stock = (stock-$desired_quantity_replacement) where id='$selected_inventory_id_for_rep' and stock >= $desired_quantity_replacement; ";
		
		$result=$this->db->query($sql_inv);
		return $result;
	}
	
	public function add_inventory_stock_when_customer_cancels($inventory_id,$quantity){
		//used when cancels the replacement requests
		$sql_inv="update inventory set stock = (stock+$quantity) where id='$inventory_id'; ";
		$result=$this->db->query($sql_inv);
		return $result;	
	}
	public function add_inventory_stock_when_customer_cancels_order_item($inventory_id,$quantity,$promotion_available,$promotion_minimum_quantity,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$promotion_item,$promotion_item_num,$ord_addon_products_status,$ord_addon_inventories){
		
			$purchased_quantity=$quantity;
			
			$str="";
			
			if($promotion_available==1){
				
				if(!empty($promotion_item)){
					
					$promo_item=$promotion_item;
					$promotion_item_num=$promotion_item_num;
					
					$promo_item_arr = explode(',', $promo_item);
					$promo_item_num_arr = explode(',', $promotion_item_num);
					
					$two=array_combine($promo_item_arr,$promo_item_num_arr);
					$str_entire=array();
					
					$temp_quotient=intval($purchased_quantity/$promotion_minimum_quantity);
					
					foreach($promo_item_arr as $prmo_item){
						
						$free_inv_id=intval($prmo_item);
						$reduce_count=intval($temp_quotient*$two[$prmo_item]);
						$sql_free_item="update inventory set stock = (stock+$reduce_count) where id='$free_inv_id' ";
						$result=$this->db->query($sql_free_item);
					}
					
				}
				if(!empty($promotion_surprise_gift_skus)){

					if($promotion_surprise_gift_type=="Qty"){
						
						$promotion_surprise_gift_skus=rtrim($promotion_surprise_gift_skus,',');
						$promotion_surprise_gift_skus_nums=rtrim($promotion_surprise_gift_skus_nums,',');
						
						$promo_gift_arr = explode(',', $promotion_surprise_gift_skus);
						$promo_gift_num_arr = explode(',', $promotion_surprise_gift_skus_nums);
						
						$two=array_combine($promo_gift_arr,$promo_gift_num_arr);
						foreach($two as $inv_id=>$stock_count){
							$sql_free_item_gift="update inventory set stock = (stock+$stock_count) where id='$inv_id'";
							$result=$this->db->query($sql_free_item_gift);
						}
					}
	
				}
				
			}
			
			$sql_inv="update inventory set stock = (stock+$purchased_quantity) where id='$inventory_id'; ";
			$result=$this->db->query($sql_inv);

			/* addon section */
			if($ord_addon_products_status=='1'){
				if($ord_addon_inventories!=''){
					$addon_arr=explode(',',$ord_addon_inventories);
					$addon_arr=array_filter($addon_arr);
					
					if(!empty($addon_arr)){
						
						foreach($addon_arr as $inv_id){
							if($inv_id!=''){
								$sql_1="update inventory set stock = (stock+1) where id='$inv_id'; ";
								$result_1=$this->db->query($sql_1);
							}
						}
					}
				}
			}
			/* addon section */
			
		return $result;	
	}
	
	public function get_request_details_of_order_item($order_item_id){
		
		$sql_rep="select count(*) as count from replacement_desired where order_item_id='$order_item_id'";	
		$result_rep=$this->db->query($sql_rep);
		$count_rep=$result_rep->row()->count;
		
		$sql_ref="select count(*) as count from returns_desired where order_item_id='$order_item_id'";
		$result_ref=$this->db->query($sql_ref);
		$count_ref=$result_ref->row()->count;
		
		$count_arr=array('replace_req_count'=>$count_rep,"refund_req_count"=>$count_ref);
		
		return $count_arr;
	}
	
	public function get_closed_return_requests($order_item_id){
		//replacement request
		
		$sql_rep="select count(*) as count from replacement_desired where order_item_id='$order_item_id'";	
		$result_rep=$this->db->query($sql_rep);
		$count_rep=$result_rep->row()->count;
		
		if($count_rep>0){
			
			$sql_select_rep_succ="select order_item_id,timstamp from replacement_desired where ( order_replacement_decision_status='replaced' or order_replacement_decision_status'refunded') and order_item_id='$order_item_id'";
			
			$result_select_succ=$this->db->query($sql_select_rep_succ);
			
			$sql_select_rep_fail="select order_item_id,timstamp from replacement_desired where ( order_replacement_decision_status='reject' or order_replacement_decision_customer_decision='reject' or	order_replacement_decision_customer_decision='cancel') and order_item_id='$order_item_id'";
			
			$result_select_fail=$this->db->query($sql_select_rep_fail);
			
		}
		
		//refund request
		
		$sql_ref="select count(*) as count from returns_desired where order_item_id='$order_item_id'";
		$result_ref=$this->db->query($sql_ref);
		$count_ref=$result_ref->row()->count;
		
		if($count_ref>0){
			
			$sql_select_ref_succ="select order_item_id,timstamp from returns_desired where ( order_replacement_decision_status='refunded') and order_item_id='$order_item_id'";
			
			$result_select_ref_succ=$this->db->query($sql_select_ref_succ);
			
			$sql_select_ref_fail="select order_item_id,timstamp from returns_desired where (order_return_decision_status='reject' or order_return_decision_customer_decision='reject' or order_return_decision_customer_decision='cancel') and order_item_id='$order_item_id'";
			
			$result_select_ref_fail=$this->db->query($sql_select_ref_fail);
			
		}
		//may return count or order_item_id with timestamp
		//success and fail --- closed requests
	}
	public function get_count_of_cart($customer_id){
		$sql="select count(*) as count from carttable where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		return $result->row()->count;
	}
	public function get_invoice_surprise_gift_details($eligible_surprise_gift_on_invoice_uid){
		$sql="select promo_quote,to_buy,to_get,buy_type,get_type from promotions where promo_uid='$eligible_surprise_gift_on_invoice_uid'";
		$result=$this->db->query($sql);
		$arr=$result->row_array();
		
		if(!empty($arr)){
			
			if($arr['get_type']=='Qty'){
				
				$sql_free="select inv_id,quantity_def from promotion_items_taged_for_purchasing_free where promo_uid='$eligible_surprise_gift_on_invoice_uid'";
				$result_free=$this->db->query($sql_free);
				$arr_free=$result_free->result();
				$free_inv=array();$free_inv_num=array();
				foreach($arr_free as $free_items){
                                        if(intval($free_items->quantity_def)>0){
                                            $free_inv[]=$free_items->inv_id;
                                            $free_inv_num[]=$free_items->quantity_def;
                                        }
				}
				$free_inv_str=implode(',',$free_inv);
				$free_inv_str_num=implode(',',$free_inv_num);	
				$arr['free_inv']=$free_inv_str;
				$arr['free_inv_num']=$free_inv_str_num;
				
			}else{
				$arr['free_inv']="";
				$arr['free_inv_num']="";
			}
		}
		
		return $arr;
	}
	
	public function check_all_order_items_delivered_report($order_id){
		//SELECT  sku_id, count(*) AS amm FROM request_for_out_of_stock GROUP BY sku_id
		
		$completed=0;
		
		$sql_invoice="select cart_quantity from invoices_offers where order_id='$order_id'";
		$result_invoice=$this->db->query($sql_invoice);
		$order_item_count=$result_invoice->row()->cart_quantity;
		
		$sql_active_orders="select count(*) as count from active_orders where prev_order_item_id='' and order_id='$order_id'  GROUP BY order_id";
		
		$result_active_orders=$this->db->query($sql_active_orders);
		$active_row=$result_active_orders->row();
		if(!empty($active_row)){
			$active_count=$result_active_orders->row()->count;
		}else{
			$active_count=0;
		}
		
		//to check the completed orders
		$sql_com_orders="select count(*) as count from history_orders,orders_status where history_orders.order_id='$order_id' and history_orders.order_item_id=orders_status.order_item_id and history_orders.prev_order_item_id='' and orders_status.order_delivered=1	GROUP BY history_orders.order_id";
		$result_com_orders=$this->db->query($sql_com_orders);
		$com_row=$result_com_orders->row();
		if(!empty($com_row)){
			$com_count=$result_com_orders->row()->count;
		}else{
			$com_count=0;
		}
		
		/*$sql_com_orders="select count(*) as count from completed_orders where order_id='$order_id' GROUP BY order_id";
		$result_com_orders=$this->db->query($sql_com_orders);
		$com_row=$result_com_orders->row();
		if(!empty($com_row)){
			$com_count=$result_com_orders->row()->count;
		}else{
			$sql_rep_orders="select count(*) as count from replaced_orders where order_id='$order_id' GROUP BY order_id";
			$result_rep_orders=$this->db->query($sql_rep_orders);
			$com_row=$result_rep_orders->row();
			if(!empty($com_row)){
				$com_count=$result_rep_orders->row()->count;
			}else{
					
				$sql_ref_orders="select count(*) as count from returned_orders where order_id='$order_id' GROUP BY order_id";
				$result_ref_orders=$this->db->query($sql_ref_orders);
				$com_row=$result_ref_orders->row();
				if(!empty($com_row)){
					$com_count=$result_ref_orders->row()->count;
				}else{
					$com_count=0;
				}
			}
			
		}*/
		
		$sql_cancel_orders="select count(*) as count from cancelled_orders where order_id='$order_id' GROUP BY order_id";
		$result_cancel_orders=$this->db->query($sql_cancel_orders);
		$cancel_row=$result_cancel_orders->row();
		if(!empty($cancel_row)){
			$cancel_count=$result_cancel_orders->row()->count;
		}else{
			$cancel_count=0;
		}
		
		/*
		$sql_repl_orders="select count(*) as count from replaced_orders where order_id='$order_id' GROUP BY order_id";
		$result_repl_orders=$this->db->query($sql_repl_orders);
		$repl_row=$result_repl_orders->row();
		if(!empty($repl_row)){
			$repl_count=$result_repl_orders->row()->count;
		}else{
			$repl_count=0;
		}
		*/
		//echo $order_item_count.'|'.$active_count.'|'.$com_count.'|'.$cancel_count;
		
		if($active_count>0 && $active_count==$order_item_count){
			
			//echo 'the order is processing';
			$completed=0;
		}
		
		if($active_count>0 && $active_count<$order_item_count){
			//echo 'some of items are processing';
			$completed=0;
		}
		
		/*echo $com_count.'kkkk';
		echo $order_item_count.'uuuuuu';
		echo $com_count.'tttt';*/
		
		if($com_count>0 && $com_count==$order_item_count){
			//echo 'order is delivered //completed';
			$completed=1;
		}
		if($com_count>0 && $com_count<$order_item_count){
			//echo 'some of items are not completed//still processing';	
			$completed=0;
		}
		
		if($cancel_count>0 && $cancel_count==$order_item_count){
			//echo 'full order is cancelled';
			$completed=0;
		}
		if($cancel_count>0 && $cancel_count<$order_item_count){
			//echo 'some of items are cancelled';
			$completed=0;
		}
		
		return  $completed;
	}
	
	public function check_all_order_items_canceled($order_id){
		
		$cancelled=0;
		
		$sql_invoice="select * from invoices_offers where order_id='$order_id'";
		$result_invoice=$this->db->query($sql_invoice);
		
		$order_item_count=$result_invoice->row()->cart_quantity;
		
		$sql_cancel_orders="select count(*) as count from cancelled_orders where order_id='$order_id' GROUP BY order_id";
		
		$result_cancel_orders=$this->db->query($sql_cancel_orders);
		$cancel_row=$result_cancel_orders->row();
		if(!empty($cancel_row)){
			$cancel_count=$result_cancel_orders->row()->count;
		}else{
			$cancel_count=0;
		}
		if($cancel_count>0 && $cancel_count==$order_item_count){
			//echo 'full order is cancelled';
			$cancelled=1;
		}
		if($cancel_count>0 && $cancel_count<$order_item_count){
			//echo 'some of items are cancelled';
			$cancelled=0;
		}
		
		return $cancelled;

	}
	
	public function check_all_order_items_canceled_report($order_id){
		$cancelled=0;
		
		$sql_invoice="select * from invoices_offers where order_id='$order_id'";
		$result_invoice=$this->db->query($sql_invoice);
		
		$order_item_count=$result_invoice->row()->cart_quantity;
		$invoice_surprise_gift=$result_invoice->row()->invoice_surprise_gift;
		$invoice_surprise_gift_type=$result_invoice->row()->invoice_surprise_gift_type;
		$invoice_surprise_gift_skus=$result_invoice->row()->invoice_surprise_gift_skus;
		$invoice_surprise_gift_skus_nums=$result_invoice->row()->invoice_surprise_gift_skus_nums;
		$invoice_offer_achieved=$result_invoice->row()->invoice_offer_achieved;
		$payment_method=$result_invoice->row()->payment_method;
		$customer_id=$result_invoice->row()->customer_id;
		
		$sql_cancel_orders="select count(*) as count from cancelled_orders where order_id='$order_id' GROUP BY order_id";
		
		$result_cancel_orders=$this->db->query($sql_cancel_orders);
		$cancel_row=$result_cancel_orders->row();
		if(!empty($cancel_row)){
			$cancel_count=$result_cancel_orders->row()->count;
		}else{
			$cancel_count=0;
		}
		if($cancel_count>0 && $cancel_count==$order_item_count){
			//echo 'full order is cancelled';
			$cancelled=1;
		}
		if($cancel_count>0 && $cancel_count<$order_item_count){
			//echo 'some of items are cancelled';
			$cancelled=0;
		}
		//echo $cancelled;
		if($invoice_surprise_gift!='' && $invoice_offer_achieved==1 && $cancelled==1){
				
				//if already credited
				//if payment_method not cod and amount credited without delivery
				
			if($payment_method!='COD'){
				
				if($invoice_surprise_gift_type==curr_code){
					$surprise_gift_amount=$invoice_surprise_gift;
	
					if($surprise_gift_amount>0){
				
						$sql_wallet_check="select * from wallet where customer_id='$customer_id'";
						$result_d=$this->db->query($sql_wallet_check);
						if($result_d->num_rows()>0){
							$wallet_id=$result_d->row()->wallet_id;
							$wallet_amount=$result_d->row()->wallet_amount;
							$new_wallet_amount=($wallet_amount-$surprise_gift_amount);
							
							$sql_update_s="update wallet set wallet_amount='$new_wallet_amount' where wallet_id='$wallet_id' and customer_id='$customer_id'";
							$this->db->query($sql_update_s);
							
						}
						
					$sql_for_wallet_transaction_surprise="insert into wallet_transaction set transaction_details='surprise gift amount debited from your wallet against <br> the order number $order_id', debit='$surprise_gift_amount' , credit='',amount='$new_wallet_amount',wallet_id='$wallet_id',customer_id='$customer_id'";
						
					  $this->db->query($sql_for_wallet_transaction_surprise);
					}
					
				}
				
				if($invoice_surprise_gift_type=='Qty'){
					//reduce the stock of free items
			
					$invoice_surprise_gift_skus=rtrim($invoice_surprise_gift_skus,',');
					$invoice_surprise_gift_skus_nums=rtrim($invoice_surprise_gift_skus_nums,',');
					
					$invoice_surprise_gift_skus_arr = explode(',', $invoice_surprise_gift_skus);
					$invoice_surprise_gift_skus_nums_arr = explode(',', $invoice_surprise_gift_skus_nums);
					
					$two=array_combine($invoice_surprise_gift_skus_arr,$invoice_surprise_gift_skus_nums_arr);
					
					foreach($two as $inv_id=>$stock_count){
						$sql_free_item_gift="update inventory set stock = (stock+$stock_count) where id='$inv_id' and stock >= $stock_count; ";
						$result=$this->db->query($sql_free_item_gift);
					}
		
				}
				
			}//checking payment_method //means already credited//so debit
			
		}
		return $cancelled;

	}
	public function update_customer_status_surprise_gifts_on_invoice($order_id,$status,$customer_id){
		$pending="";
		$notify_active=0;
		if($status=="accept"){
			$pending="pending";
			$notify_active=1;
		}
		$sql="update invoices_offers set customer_response_for_surprise_gifts='$status', status_of_refund_for_surprise_gifts='$pending',notify_active='$notify_active' where order_id='$order_id' and customer_id='$customer_id'";
		$result=$this->db->query($sql);
		return $result;
	}
	public function update_customer_response_for_invoice_cashback($order_id,$status,$customer_id,$total_amount_cash_back){
		$pending='';
		if($status=="accept"){
			if($total_amount_cash_back>0){
				$pending='pending';
			}
		}
		$sql="update invoices_offers set customer_response_for_invoice_cashback='$status',status_of_refund_for_cashback='$pending',notify_invoice_promotion=1 where order_id='$order_id' and customer_id='$customer_id' ";

		
		$result=$this->db->query($sql);
		
		return $result;
	}
	public function update_invoice_surprise_gifts_on_accept($order_id,$customer_id){
		
		$sql_invoice="select * from invoices_offers where order_id='$order_id'";
		$result_invoice=$this->db->query($sql_invoice);
		
		$order_item_count=$result_invoice->row()->cart_quantity;
		$payment_method=$result_invoice->row()->payment_method;	
		$invoice_surprise_gift=$result_invoice->row()->invoice_surprise_gift;
		$invoice_surprise_gift_type=$result_invoice->row()->invoice_surprise_gift_type;
		$invoice_surprise_gift_skus=$result_invoice->row()->invoice_surprise_gift_skus;
		$invoice_surprise_gift_skus_nums=$result_invoice->row()->invoice_surprise_gift_skus_nums;

			$sql_com_orders="select count(*) as count from completed_orders where order_id='$order_id' GROUP BY order_id";
			$result_com_orders=$this->db->query($sql_com_orders);
			$com_row=$result_com_orders->row();
			if(!empty($com_row)){
				$com_count=$result_com_orders->row()->count;
			}else{
				$com_count=0;
			}
			
			if($com_count==$order_item_count){
				//all order_items are delivered// moved to completed_orders
				//credit the amount to wallet
				if($invoice_surprise_gift_type==curr_code){
		
					$surprise_gift_amount=$invoice_surprise_gift;
					
					if($surprise_gift_amount>0){
				
						$sql_wallet_check="select * from wallet where customer_id='$customer_id'";
						$result_d=$this->db->query($sql_wallet_check);
						if($result_d->num_rows()>0){
							$wallet_id=$result_d->row()->wallet_id;
							$wallet_amount=$result_d->row()->wallet_amount;
							$new_wallet_amount=($surprise_gift_amount+$wallet_amount);
							
							$sql_update_s="update wallet set wallet_amount='$new_wallet_amount' where wallet_id='$wallet_id' and customer_id='$customer_id'";
							$this->db->query($sql_update_s);
							
						}else{
								$sql_insert_s="insert into wallet set customer_id='$customer_id', wallet_amount='$surprise_gift_amount'";
								$this->db->query($sql_insert_s);
								$wallet_id=$this->db->insert_id();
								$new_wallet_amount=$surprise_gift_amount;
						}
						
						$sql_for_wallet_transaction_surprise="insert into wallet_transaction set transaction_details='surprise gift amount credited to your wallet against <br> the order number $order_id', debit='' , credit='$surprise_gift_amount',amount='$new_wallet_amount',wallet_id='$wallet_id',customer_id='$customer_id'";
						
						$this->db->query($sql_for_wallet_transaction_surprise);
					}
					
				}
				
				if($invoice_surprise_gift_type=='Qty'){
					//reduce the stock of free items

					$invoice_surprise_gift_skus=rtrim($invoice_surprise_gift_skus,',');
					$invoice_surprise_gift_skus_nums=rtrim($invoice_surprise_gift_skus_nums,',');
					
					$invoice_surprise_gift_skus_arr = explode(',', $invoice_surprise_gift_skus);
					$invoice_surprise_gift_skus_nums_arr = explode(',', $invoice_surprise_gift_skus_nums);
					
					$two=array_combine($invoice_surprise_gift_skus_arr,$invoice_surprise_gift_skus_nums_arr);
					
					foreach($two as $inv_id=>$stock_count){
						$sql_free_item_gift="update inventory set stock = (stock-$stock_count) where id='$inv_id' and stock >= $stock_count; ";
						$result=$this->db->query($sql_free_item_gift);
					}
		
				}
			}
	
	}
	public function update_invoice_surprise_gifts_on_reject($order_id,$customer_id){
		
		$sql_invoice="select * from invoices_offers where order_id='$order_id'";
		$result_invoice=$this->db->query($sql_invoice);
		
		$order_item_count=$result_invoice->row()->cart_quantity;
		$payment_method=$result_invoice->row()->payment_method;	
		$invoice_surprise_gift=$result_invoice->row()->invoice_surprise_gift;
		$invoice_surprise_gift_type=$result_invoice->row()->invoice_surprise_gift_type;
		$invoice_surprise_gift_skus=$result_invoice->row()->invoice_surprise_gift_skus;
		$invoice_surprise_gift_skus_nums=$result_invoice->row()->invoice_surprise_gift_skus_nums;


			$sql_com_orders="select count(*) as count from completed_orders where order_id='$order_id' GROUP BY order_id";
			$result_com_orders=$this->db->query($sql_com_orders);
			$com_row=$result_com_orders->row();
			
			if(!empty($com_row)){
				$com_count=$result_com_orders->row()->count;
			}else{
				$com_count=0;
			}
			
			if($com_count==$order_item_count){
				//all order_items are delivered// moved to completed_orders
				//credit the amount to wallet
				if($invoice_surprise_gift_type==curr_code){
		
					$surprise_gift_amount=$invoice_surprise_gift;
					
					if($surprise_gift_amount>0){
				
						$sql_wallet_check="select * from wallet where customer_id='$customer_id'";
						$result_d=$this->db->query($sql_wallet_check);
						if($result_d->num_rows()>0){
							$wallet_id=$result_d->row()->wallet_id;
							$wallet_amount=$result_d->row()->wallet_amount;
							$new_wallet_amount=($wallet_amount-$surprise_gift_amount);
							
							$sql_update_s="update wallet set wallet_amount='$new_wallet_amount' where wallet_id='$wallet_id' and customer_id='$customer_id'";
							$this->db->query($sql_update_s);
							
						}else{
								$sql_insert_s="insert into wallet set customer_id='$customer_id', wallet_amount='$surprise_gift_amount'";
								$this->db->query($sql_insert_s);
								$wallet_id=$this->db->insert_id();
								$new_wallet_amount=$surprise_gift_amount;
						}
						
						$sql_for_wallet_transaction_surprise="insert into wallet_transaction set transaction_details='surprise gift amount debited from your wallet against <br> the order number $order_id', debit='$surprise_gift_amount' , credit='',amount='$new_wallet_amount',wallet_id='$wallet_id',customer_id='$customer_id'";
						
						$result_free=$this->db->query($sql_for_wallet_transaction_surprise);
					}
					
				}
				
				if($invoice_surprise_gift_type=='Qty'){
					//reduce the stock of free items

					$invoice_surprise_gift_skus=rtrim($invoice_surprise_gift_skus,',');
					$invoice_surprise_gift_skus_nums=rtrim($invoice_surprise_gift_skus_nums,',');
					
					$invoice_surprise_gift_skus_arr = explode(',', $invoice_surprise_gift_skus);
					$invoice_surprise_gift_skus_nums_arr = explode(',', $invoice_surprise_gift_skus_nums);
					
					$two=array_combine($invoice_surprise_gift_skus_arr,$invoice_surprise_gift_skus_nums_arr);
					
					foreach($two as $inv_id=>$stock_count){
						$sql_free_item_gift="update inventory set stock = (stock+$stock_count) where id='$inv_id' and stock >= $stock_count; ";
						$result_free=$this->db->query($sql_free_item_gift);
					}
		
				}
			}
		
	
		//return $result_free;
	}
	
	public function get_default_parcel_category(){
		$sql="select * from logistics_parcel_category where default_parcel_category='1'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->logistics_parcel_category_id;
		}
	}
	public function default_delivery_mode(){
		$sql="select logistics_delivery_mode_id,delivery_mode from logistics_delivery_mode where default_delivery_mode=1";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->logistics_delivery_mode_id;
		}
	}
	public function  get_logistics_delivery_mode_data_by_logistics_delivery_mode_id($logistics_delivery_mode_id){
		$sql="select logistics_delivery_mode_id,delivery_mode,speed_duration from logistics_delivery_mode where logistics_delivery_mode_id='$logistics_delivery_mode_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row_array();
		}
	}
	public function get_inventory_sku_id($inventory_id){
		$sql="select sku_id from inventory where id=$inventory_id";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->sku_id;
		}
	}
	
	public function get_status_of_free_items($invoices_offers_id){
		$sql="select * from invoices_free_items_status where invoices_offers_id='$invoices_offers_id' and quantity>0";
		$result=$this->db->query($sql);
		$res_obj=$result->result();
		
		//print_r($res_obj);
		
		$array_shipped=array();
		$array_delivered=array();
		$array_comments=array();
		$array_undelivered=array();
		
		foreach($res_obj as $obj){
			$array_delivered[]=$obj->item_delivered;
			$array_shipped[]=$obj->item_shipped;
			$array_undelivered[]=$obj->item_undelivered;
			$array_comments[]=$obj->admin_comments;			
		}
		
		/*
		if(!in_array("0",$array_delivered)){
			return "delivered";
		}elseif(!in_array("0",$array_shipped)){
			return "shipped";
		}elseif(in_array("0",$array_delivered)){
			return "not delivered";
		}elseif(in_array("0",$array_shipped)){
			return "not shipped";
		}
		*/
		
		$result_arr=array('array_delivered'=>$array_delivered,'array_shipped'=>$array_shipped,'array_undelivered'=>$array_undelivered,'array_comments'=>$array_comments);
		
		return $result_arr;
	}
	public function get_all_tree_of_products($p_id){
		
		$sql="select A.pcat_name,C.cat_name,S.subcat_name,P.product_name,P.pcat_id,P.cat_id,P.subcat_id from products P inner join parent_category A on P.pcat_id = A.pcat_id inner join category C on P.cat_id = C.cat_id inner join subcategory S on P.subcat_id = S.subcat_id where P.product_id = '$p_id' and P.view=1";
		
		$result = $this->db->query($sql);
        return $result->result();
	}
	public function get_customer_pays_shipping_charge_percentage($inventory_id){
		$sql="select products.pcat_id,products.cat_id,products.subcat_id,products.brand_id,products.product_id,inventory.id as inventory_id from inventory,products where products.product_id=inventory.product_id and inventory.id='$inventory_id'";
		
		$result = $this->db->query($sql);
		
		$tree_val=$result->row();
		$entire=array();
		if(!empty($tree_val)){

			$pcat_id=$tree_val->pcat_id;
			$cat_id=$tree_val->cat_id;
			$subcat_id=$tree_val->subcat_id;
			$brand_id=$tree_val->brand_id;
			$product_id=$tree_val->product_id;
			$inventory_id=$tree_val->inventory_id;
			
			$sql_shipping_repl="select shipping_charge_on_replacement.*,policy_items.* from shipping_charge_on_replacement,policy_items where policy_items.policy_uid=shipping_charge_on_replacement.shipping_charge_on_replacement_policy_uid and (policy_items.pcat_id='$pcat_id' or (policy_items.pcat_id='' and policy_items.cat_id='' and policy_items.subcat_id='' and policy_items.brand_id='' and policy_items.product_id='' and policy_items.inv_id=''))";
			
			$result_repl = $this->db->query($sql_shipping_repl);
			
			$shipping_repl=$result_repl->result_array();
			
			//print_r($shipping_repl);
			//$result_data;
			
			foreach($shipping_repl as $shipping_repl_val){

				//echo ($shipping_repl_val->id).'||';
				$result_data="";
				if(isset($shipping_repl_val['inv_id']) && $shipping_repl_val['inv_id'] == $inventory_id) {

					$result_data=$shipping_repl_val;
					
				}elseif(isset($shipping_repl_val['product_id']) && $shipping_repl_val['product_id'] == $product_id && $shipping_repl_val['inv_id']==""){

					$result_data=$shipping_repl_val;

				}elseif(isset($shipping_repl_val['brand_id']) && $shipping_repl_val['brand_id'] == $brand_id  && $shipping_repl_val['product_id'] == "" && $shipping_repl_val['inv_id']==""){

					$result_data=$shipping_repl_val;

				}elseif(isset($shipping_repl_val['subcat_id']) && $shipping_repl_val['subcat_id'] == $subcat_id && $shipping_repl_val['brand_id'] == ""  && $shipping_repl_val['product_id'] == "" && $shipping_repl_val['inv_id']==""){
				
					$result_data=$shipping_repl_val;
		
				}elseif(isset($shipping_repl_val['cat_id']) && $shipping_repl_val['cat_id'] == $cat_id && $shipping_repl_val['subcat_id'] == "" && $shipping_repl_val['brand_id'] == ""  && $shipping_repl_val['product_id'] == "" && $shipping_repl_val['inv_id']==""){

					$result_data=$shipping_repl_val;

				}elseif(isset($shipping_repl_val['pcat_id']) && $shipping_repl_val['pcat_id'] == $pcat_id && $shipping_repl_val['cat_id'] == "" && $shipping_repl_val['subcat_id'] == "" && $shipping_repl_val['brand_id'] == ""  && $shipping_repl_val['product_id'] == "" && $shipping_repl_val['inv_id']==""){

					$result_data=$shipping_repl_val;

				}elseif($shipping_repl_val['pcat_id'] == "" && $shipping_repl_val['cat_id'] == "" && $shipping_repl_val['subcat_id'] == "" && $shipping_repl_val['brand_id'] == ""  && $shipping_repl_val['product_id'] == "" && $shipping_repl_val['inv_id']==""){
						
						$result_data=$shipping_repl_val;
						
				}else{
					//no policy//admin has to pay the shipping charge
					//$result_data=array("empty");
				}
				($result_data!='') ? $entire[]=$result_data:'';//should execute only one time
				
			}
		}
		//print_r($entire);
        return $entire;
	}
	public function get_details_of_order_item($order_item_id){
    
		$sql="select * from order_item_offers where order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		return $result->row();
	}
	public function update_reject_order_item_customer_response($order_item_id){
		$customer_id=$this->session->userdata("customer_id");
		$sql_cb_rej="update order_item_offers set customer_response='reject' where order_item_id='$order_item_id' and customer_id='$customer_id'";
		$result_cb_rej=$this->db->query($sql_cb_rej);
		return $result_cb_rej;
				
	}
	///////// Notification starts ///////////////////////////
	
	public function clear_notifications($customer_id){
		//updating as read
		$sql="UPDATE order_item_notification SET 
				approved_status_view = CASE WHEN approved_status_view = 1 THEN 0 ELSE approved_status_view	END,
				confirmed_status_view = CASE WHEN confirmed_status_view = 1 THEN 0 ELSE confirmed_status_view END,
				packed_status_view = CASE WHEN packed_status_view = 1 THEN 0 ELSE packed_status_view END,
				shipped_status_view = CASE WHEN shipped_status_view = 1 THEN 0 ELSE shipped_status_view END,
				delivered_status_view = CASE WHEN delivered_status_view = 1 THEN 0 ELSE delivered_status_view END,
				cancelled_status_view = CASE WHEN cancelled_status_view = 1 THEN 0 ELSE cancelled_status_view END
				where customer_id='$customer_id' ";
				
		$result=$this->db->query($sql);
		
		$sql_refund="UPDATE refund_notification SET 
				admin_accept_status_view = CASE WHEN admin_accept_status_view = 1 THEN 0 ELSE admin_accept_status_view	END,
				admin_reject_status_view = CASE WHEN admin_reject_status_view = 1 THEN 0 ELSE admin_reject_status_view END,
				admin_msg_view = CASE WHEN admin_msg_view = 1 THEN 0 ELSE admin_msg_view END,
				refund_label_status_view = CASE WHEN refund_label_status_view = 1 THEN 0 ELSE refund_label_status_view END,
				refund_success_view = CASE WHEN refund_success_view = 1 THEN 0 ELSE refund_success_view END
				where customer_id='$customer_id' ";
				
		$result_refund=$this->db->query($sql_refund);
		
		$sql_repl="UPDATE replacement_notification SET 
				admin_accept_status_view = CASE WHEN admin_accept_status_view = 1 THEN 0 ELSE admin_accept_status_view	END,
				admin_reject_status_view = CASE WHEN admin_reject_status_view = 1 THEN 0 ELSE admin_reject_status_view END,
				admin_msg_view = CASE WHEN admin_msg_view = 1 THEN 0 ELSE admin_msg_view END,
				repl_label_status_view = CASE WHEN repl_label_status_view = 1 THEN 0 ELSE repl_label_status_view END
				where customer_id='$customer_id' ";
				
		$result_repl=$this->db->query($sql_repl);
		
		return $result_refund;
	}
	public function get_inventory_details($inventory_id){
		$sql="select * from inventory where id='$inventory_id' ";
		$result=$this->db->query($sql);
		return $result->row();
	}
	public function get_shipping_address_of_order($order_id){
		$sql="select invoices_offers.order_id,shipping_address.* from invoices_offers,shipping_address where invoices_offers.order_id='$order_id' and invoices_offers.shipping_address_id=shipping_address.shipping_address_id ";
		$result=$this->db->query($sql);
		return $result->row();
	}
	public function total_show_all_reviews($customer_id){
		
		$sql="select * from rated_inventory where customer_id='$customer_id' and status=1";
		$res=$this->db->query($sql);
		$total_no_of_records=$res->num_rows();	
		
		return $total_no_of_records;
	}
	public function get_reviews_of_customer($customer_id){
		
 	    $sql_select="select rated_inventory.*,inventory.*,products.* from rated_inventory,products,inventory where rated_inventory.customer_id='$customer_id' and inventory.id=rated_inventory.inventory_id and inventory.product_id=products.product_id order by rated_inventory.id desc";
		$result=$this->db->query($sql_select);
		return $result->result();
	}
	public function get_details_of_products($inventory_id){
		
 	    $sql_select="select inventory.*,products.* from products,inventory where inventory.product_id=products.product_id and inventory.id='$inventory_id'";
		$result=$this->db->query($sql_select);
		return $result->row();
	}
	public function update_stock_invoice_surprise_gifts_on_accept($invoice_surprise_gift_skus,$invoice_surprise_gift_skus_nums){
			
			$invoice_surprise_gift_skus=rtrim($invoice_surprise_gift_skus,',');
			$invoice_surprise_gift_skus_nums=rtrim($invoice_surprise_gift_skus_nums,',');
			
			$invoice_surprise_gift_skus_arr = explode(',', $invoice_surprise_gift_skus);
			$invoice_surprise_gift_skus_nums_arr = explode(',', $invoice_surprise_gift_skus_nums);
			
			$two=array_combine($invoice_surprise_gift_skus_arr,$invoice_surprise_gift_skus_nums_arr);
			
			foreach($two as $inv_id=>$stock_count){
				$sql_free_item_gift="update inventory set stock = (stock-$stock_count) where id='$inv_id' and stock >= $stock_count; ";
				$result_free_gift=$this->db->query($sql_free_item_gift);
			}

	}
	public function get_replaced_orders_by_prev_order_item_id_from_history_orders($prev_order_item_id){
		
 	    $sql_select="select history_orders.*,inventory.*,products.* from history_orders,products,inventory where history_orders.prev_order_item_id='$prev_order_item_id' and history_orders.product_id=products.product_id and history_orders.inventory_id=inventory.id and inventory.product_id=products.product_id";
		$result=$this->db->query($sql_select);
		return $result->row();
	}
	public function get_sub_reasons($reason_id){
		$sql_select="select * from sub_issue_return where reason_id='$reason_id'";
		$result=$this->db->query($sql_select);
		return $result->result();
	}
	public function get_delivery_date($id){
		$sql="select expected_delivery_date,exact_delivery_date from history_orders where order_item_id='$id'";
		$result=$this->db->query($sql);
		return $result->row();
	}
	public function sended_code_to_mail($email,$rand){
		$rand="1234";
		$sql="update customer set random_no='$rand' where email='$email'";
		$result=$this->db->query($sql);
		return $result;
		
	}
	public function sended_code_to_mobile($mobile,$rand){
		$rand="1234";
		$sql="update customer set random_no='$rand' where mobile='$mobile'";
		$result=$this->db->query($sql);
		return $result;
		
	}
	public function get_verif_code_db($cust_id,$rn,$email){
		 $rn='1234';
		 $sql="select id from customer where random_no='$rn' and email='$email' and id='$cust_id'";
		$result=$this->db->query($sql);
		$result = $result->row();
		if(!empty($result)>0){
			$sql_u="update customer set email_verification='1' where id='$cust_id'";
			$result_u=$this->db->query($sql_u);
			return $result_u;
		
		}else{
			return false;
		}
	}
	public function get_verif_code_db_mobile($cust_id,$rn,$mobile){
		$rn='1234';
		 $sql="select id from customer where random_no='$rn' and mobile='$mobile' and id='$cust_id'";
		$result=$this->db->query($sql);
		$result = $result->row();
		if(!empty($result)>0){
			$sql_u="update customer set mobile_verification='1' where id='$cust_id'";
			$result_u=$this->db->query($sql_u);
			return $result_u;
		
		}else{
			return false;
		}
	}
	public function get_specific_cartinfo_for_customer(){
		$customer_id=$this->session->userdata("customer_id");
		$sql="select * from carttable where customer_id='$customer_id'";
		$res=$this->db->query($sql);
		return $res->result();
	}
	public function update_logistics_territory_id($inventory_id,$vendor_id,$logistics_territory_id,$logistics_id,$cart_id){
		$customer_id=$this->session->userdata("customer_id");
		$sql="update carttable set logistics_territory_id='$logistics_territory_id',logistics_id='$logistics_id' where customer_id='$customer_id' and cart_id='$cart_id' and vendor_id='$vendor_id'";
		$res=$this->db->query($sql);
		
	}
	//////////////////
	public function get_order_summary_mail_data_rating($order_item_id){
		$sql="select history_orders.*,inventory.thumbnail,products.product_name,products.product_description from history_orders,inventory,products where history_orders.inventory_id=inventory.id and products.product_id=inventory.product_id and history_orders.order_item_id='$order_item_id'";
		$query =$this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_feedback_questions($val){
		$sql="select * from feedback_questions where question_based_on_rating like '%$val%'";
		$query =$this->db->query($sql);
		return $query->result_array();
	}
	public function get_feedback_questions_headings($val){
		$sql="select question_heading from feedback_questions_heading where question_heading_based_on_rating like '%$val%'";
		$query =$this->db->query($sql);
		return $query->row_array()["question_heading"];
	}
	
	public function check_rateorder_for_feedback_status($order_summary_mail_data_arr,$val){
		foreach($order_summary_mail_data_arr as $order_summary_mail_data){
			$order_item_id=$order_summary_mail_data["order_item_id"];
			$inventory_id=$order_summary_mail_data["inventory_id"];
			$customer_id=$order_summary_mail_data["customer_id"];
			$rating=$val;
			$sql="select * from rateorder where order_item_id='$order_item_id' and inventory_id='$inventory_id' and customer_id='$customer_id'";
			$query =$this->db->query($sql);
			if($query->num_rows()==0){
				$sql_insert="insert into  rateorder set order_item_id='$order_item_id',inventory_id='$inventory_id',customer_id='$customer_id',rating='$rating'";
				$query =$this->db->query($sql_insert);
				return array("status"=>"new","rateorder_id"=>$this->db->insert_id());
			}
			
			if($query->num_rows()>0){
				if($query->row_array()["feedback_status"]=="0"){
					$sql_delete="delete from  rateorder where order_item_id='$order_item_id' and inventory_id='$inventory_id' and customer_id='$customer_id'";
					$query =$this->db->query($sql_delete);
					////
					$sql_insert="insert into  rateorder set order_item_id='$order_item_id',inventory_id='$inventory_id',customer_id='$customer_id',rating='$rating'";
					$query =$this->db->query($sql_insert);
					return array("status"=>"new","rateorder_id"=>$this->db->insert_id());
				}
				if($query->row_array()["feedback_status"]=="1"){
					return array("status"=>"old");
				}
			}
			
		}
	}
	
	public function rateorderbymail_action($order_item_id,$rateorder_id,$comments,$question_arr){
		$sql_rateorder="update rateorder set message='$comments',feedback_status='1' where rateorder_id='$rateorder_id'";
		$query =$this->db->query($sql_rateorder);
		if(!empty($question_arr)){
			foreach($question_arr as $question_id => $level){
				$id=explode("_",$question_id)[1];
				$sql="insert into feedback_ratings set question_id='$id',order_item_id='$order_item_id',rating_level='$level',rateorder_id='$rateorder_id'";
				$query =$this->db->query($sql);
			}
		}
		return $query;
		
	}
	/////////////////////
	public function get_mobile_number_of_admin(){
		$sql="select mobile from admin_country where user_type='Master Country'";
		$result=$this->db->query($sql);
		return $result->row()->mobile;
	}
	public function get_all_states(){
		$sql="select * from logistics_all_states order by state_id desc";
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	
	function get_shipping_module_info(){
			$sql="select * from shipping_module";
			$query =$this->db->query($sql);
			return $query->row();
		}
	function get_city_state_details_by_pincode($pincode){
		$sql="select city,state_id from logistics_all_pincodes where pincodes like '%$pincode%'";
		$query =$this->db->query($sql);
		$city=($query->num_rows()>0) ? $query->row()->city : '';
		$state_id=($query->num_rows()>0) ? $query->row()->state_id : '';
		$sql="select state_name from logistics_all_states where state_id = '$state_id'";
		$query =$this->db->query($sql);
		$state_name=($query->num_rows()>0) ? $query->row()->state_name   : '';
		return array("city"=>ucwords($city),"state"=>ucwords($state_name),"count"=>$query->num_rows());
	}
	function carttable_dump_with_withoutsession($inventory_id,$inventory_sku,$inventory_moq,$customer_id,$addon_inventories=''){
		/////////////////////////////
			$sql_tracking_temp="insert into tracking_temp set inventory_id='$inventory_id',inventory_sku='$inventory_sku',inventory_moq='$inventory_moq',customer_id='$customer_id',page_name='details',addon_inventories='$addon_inventories'";
			$result_tracking_temp=$this->db->query($sql_tracking_temp);
			////////////////////////////
	}
	function tracking_temp_dump_checkout_withinsession($customer_id){
		$sql="select * from carttable where customer_id='$customer_id'";
		$query =$this->db->query($sql);
		$result_arr=$query->result_array();
		if(count($result_arr)>0){
			foreach($result_arr as $arr){
				$inventory_id=$arr["inventory_id"];
				$inventory_sku=$arr["inventory_sku"];
				$inventory_moq=$arr["inventory_moq"];
				
				$addon_inventories=$arr["addon_inventories"];
				$curdate=date("Y-m-d");
				$sql_tracking_temp_sel="select * from tracking_temp where customer_id='$customer_id' and inventory_id='$inventory_id' and inventory_sku='$inventory_sku' and inventory_moq='$inventory_moq' and page_name='checkout' and date(timestamp)='$curdate'";
				$query_tracking_temp_sel =$this->db->query($sql_tracking_temp_sel);
				$result_tracking_temp_sel_arr=$query_tracking_temp_sel->result_array();
				if(count($result_tracking_temp_sel_arr)==0){
					//foreach($result_tracking_temp_sel_arr as $sel_arr){
						$sql_tracking_temp="insert into tracking_temp set inventory_id='$inventory_id',inventory_sku='$inventory_sku',inventory_moq='$inventory_moq',customer_id='$customer_id',page_name='checkout',addon_inventories='$addon_inventories'";
						$result_tracking_temp=$this->db->query($sql_tracking_temp);
					//}
				}
			}
		}
	}
	
	public function get_order_details_just_now_received($order_id){
		
			$sql = "select orders_status.type_of_order,orders_status.id as orders_status_id,history_orders.*,history_orders.image,
				history_orders.sku_id,
				history_orders.prev_order_item_id,
				products.product_name,
				products.product_description,
				history_orders.ord_sku_name,
				history_orders.timestamp,
				history_orders.order_id,
				history_orders.payment_type,
				orders_status.order_item_id,
				DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') AS 'order_status_timestamp',
				history_orders.quantity,
				history_orders.product_price,
				history_orders.subtotal,
				history_orders.grandtotal,
				shipping_address.customer_name,
				shipping_address.address1,
				shipping_address.address2,
				shipping_address.country,
				shipping_address.state,
				shipping_address.city,
				shipping_address.pincode,
				shipping_address.mobile,invoices_offers.* from history_orders,invoices_offers,orders_status,products,inventory,shipping_address
				where history_orders.customer_id=orders_status.customer_id and 
				history_orders.order_item_id=orders_status.order_item_id and
				history_orders.customer_id=shipping_address.customer_id and
				history_orders.shipping_address_id=shipping_address.shipping_address_id and
				history_orders.product_id=products.product_id and
				history_orders.inventory_id=inventory.id and	
				orders_status.customer_id=shipping_address.customer_id and invoices_offers.order_id=history_orders.order_id and history_orders.order_id='$order_id'";
				$query=$this->db->query($sql);
				return $query->result();
		
	}
	
	
	function get_inventory_attr_details($inventory_id){
			$sql="select attribute_1,attribute_1_value,attribute_2,attribute_2_value,attribute_3,attribute_3_value,attribute_4,attribute_4_value from inventory where id='$inventory_id'";
			$query=$this->db->query($sql);
			$get_inventory_details_row=$query->row_array();
			
			$attribute_1=$get_inventory_details_row["attribute_1"];
			$attribute_1_value=$get_inventory_details_row["attribute_1_value"];
			
			$attribute_2=$get_inventory_details_row["attribute_2"];
			$attribute_2_value=$get_inventory_details_row["attribute_2_value"];
			
			$attribute_3=$get_inventory_details_row["attribute_3"];
			$attribute_3_value=$get_inventory_details_row["attribute_3_value"];
			
			$attribute_4=$get_inventory_details_row["attribute_4"];
			$attribute_4_value=$get_inventory_details_row["attribute_4_value"];
			
			$get_inventory_attr_details_str="";
			if($attribute_1!=""){
				$get_inventory_attr_details_str.=$attribute_1." : ".$attribute_1_value."<br>";
			}
			if($attribute_2!=""){
				$get_inventory_attr_details_str.=$attribute_2." : ".$attribute_2_value."<br>";
			}
			if($attribute_3!=""){
				$get_inventory_attr_details_str.=$attribute_3." : ".$attribute_3_value."<br>";
			}
			if($attribute_4!=""){
				$get_inventory_attr_details_str.=$attribute_4." : ".$attribute_4_value."<br>";
			}
			return $get_inventory_attr_details_str;
		}
		
		public function get_shipping_address_id_after_cancellation($order_item_id){
			$sql="select shipping_address_id from history_orders where order_item_id='$order_item_id' limit 0,1";
			$query =$this->db->query($sql);
			return $query->row()->shipping_address_id;
		}
		
		public function get_orders_status_data_after_cancellation($order_item_id){
			$sql="select * from orders_status where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function get_order_placed_date_customer_id($id){
			$sql="select customer_id,timestamp from active_orders where order_id='$id' union select customer_id,timestamp from cancelled_orders where order_id='$id' union select customer_id,timestamp from completed_orders where order_id='$id' union select customer_id,timestamp from returned_orders where order_id='$id'";
			$result=$this->db->query($sql);
			return array("order_placed_date"=>$result->row()->timestamp,"customer_id"=>$result->row()->customer_id);
		}
		public function get_cancel_reason_to_check($cancel_reason_id){
			$sql="select reason_name from reason_cancel where cancel_reason_id='$cancel_reason_id'";
			$result=$this->db->query($sql);
			return $result->row()->reason_name;
		}
		public function get_order_summary_mail_data_after_cancellation($order_item_id){
			$sql="select history_orders.*,inventory.thumbnail,products.product_name,products.product_description,inventory.attribute_1,inventory.attribute_1_value,inventory.attribute_2,inventory.attribute_2_value,inventory.attribute_3,inventory.attribute_3_value,inventory.attribute_4,inventory.attribute_4_value from history_orders,inventory,products where history_orders.inventory_id=inventory.id and products.product_id=inventory.product_id and history_orders.order_item_id='$order_item_id'";
			$query =$this->db->query($sql);
			return $query->result_array();
		}
                public function check_coupon_available($inventory_id,$inventory_sku){
                    $str=$inventory_sku.'-'.$inventory_id;
                    $sql="select * from coupon where sku_ids LIKE '%".$str."%' and now() > start_date  and not end_date < now() and type_of_coupon='1' and status='1'";
                    $result=$this->db->query($sql);
                    if($result->num_rows()>0){
                        return 1;
                    }else{
                        return 0;
                    }
                    
                }
                public function check_coupon_available_invoice(){
                    
                    $sql="select * from coupon where now() > start_date  and not end_date < now() and type_of_coupon='2' and status='1'";
                    $result=$this->db->query($sql);
                    if($result->num_rows()>0){
                        return 1;
                    }else{
                        return 0;
                    }
                    
                }
                public function apply_coupon_code_sku($cp_data){
                    $str=$cp_data->inventory_sku.'-'.$cp_data->inventory_id;
                    $code=$cp_data->coupon_code;
                    $sql="select coupon_name,type,value from coupon where coupon_code='$code' and sku_ids LIKE '%".$str."%' and now() > start_date  and not end_date < now() and type_of_coupon='1' and status='1'";
                    //echo $sql;
                    $result=$this->db->query($sql);
                    if($result->num_rows()>0){
                        return $result->row_array();
                    }else{
                        return 0;
                    }
                }
                public function apply_coupon_code_invoice($cp_data){
                    $customer_id=$this->session->userdata("customer_id");
                    $code=$cp_data->coupon_code;
                    $sql="select c.coupon_name,c.type,c.value,c.tot_cart_val,c.coupon_per_user,(select count(*) from invoices_offers as inv where BINARY inv.invoice_coupon_code='$code' and inv.customer_id='$customer_id') as coupon_used_count from coupon as c where BINARY c.coupon_code='$code' and now() > c.start_date  and not c.end_date < now() and c.type_of_coupon='2' and c.status='1'";
                    //echo $sql;
                    $result=$this->db->query($sql);

                    if($result->num_rows()>0){
                        return $result->row_array();
                    }else{
                        return 0;
                    }
                }
				
	/* combo order placement */
	
	/*Combo Order For Normal Workflow*/
	public function confirm_order_process_combo_normal_order($order_id,$payment_method=''){
		$combo_object_arr=[];
		$customer_id=$this->session->userdata("customer_id");
		$combo_details=get_combo_carttable($customer_id);

		if(!empty($combo_details)){

			$combo_products=json_decode($combo_details->combo_products);
			$combo_number_of_products=$combo_details->combo_number_of_products;
			$combo_total_price=$combo_details->combo_total_price;
			$combo_shipping_price=$combo_details->combo_shipping_price;
			$combo_grand_total=$combo_details->combo_grand_total;
			$combo_discount=$combo_details->combo_discount;
			$combo_total_amount_saved=$combo_details->combo_total_amount_saved;


			$customer_address_id=$this->get_customer_default_id();
			$shipping_address_id=$this->add_shipping_address($customer_address_id);

			if(!empty($combo_products)){
				$ids = array_column($combo_products, 'inv_id');
				$inv_ids=implode(',',$ids);
				foreach($combo_products as $inv_data_combo){

					//print_r($inv_data_combo);

						$inv_id=$inv_data_combo->inv_id;
						$inv_data=get_all_inventory_data($inv_id);

						$inv_price=$inv_data_combo->inv_price;
						$inv_max_price=$inv_data_combo->inv_max_price;
						$inv_sku_id=$inv_data_combo->inv_sku_id;
						$inv_name=$inv_data_combo->inv_name;
						$inv_image=$inv_data_combo->inv_image;
						$inv_quantity=$inv_data_combo->inv_quantity;

						$combo_shipping_price=$inv_data_combo->shipping_charge;

						$promotion_id=$inv_data_combo->promotion_id;
						$promo_qty=$inv_data_combo->promo_qty;
						$promo_remain_qty=$inv_data_combo->promo_remain_qty;
						$promo_dis=$inv_data_combo->promo_dis;
						$promo_quote=$inv_data_combo->promo_quote;

						/* new customised fields */

						//echo $promotion_id;
						$promotion_available=($promotion_id!='') ? 1 : 0; 
						//echo 'cc' . $promotion_available.'ccccc';

						if($promotion_available==1){

							echo 'comming inside'.$promo_remain_qty;
							$promotion_residual=$promo_remain_qty;
							$promotion_discount=$promo_dis;
							$promotion_item_multiplier= (($inv_quantity - $promo_remain_qty) / $promo_qty);
							$promotion_id_selected=$promotion_id;
							$promotion_type_selected='dependent';
							$promotion_minimum_quantity=$promo_qty;
							$promotion_quote=$promo_quote;
							$promotion_default_discount_promo="Price for remaining";

							$individual_price_of_product_with_promotion=($inv_data->max_selling_price-round($inv_data->max_selling_price*$promo_dis/100));

							if($promo_remain_qty>0){
								$individual_price_of_product_without_promotion=$inv_data->selling_price;
							}else{
								$individual_price_of_product_without_promotion=0;
							}
							$total_price_of_product_with_promotion=($promo_qty*$individual_price_of_product_with_promotion);

							$quantity_with_promotion=$promo_qty;

							/*echo $promo_remain_qty;
							echo '||'.$individual_price_of_product_without_promotion;
							echo '<br>';*/
							if(intval($promo_remain_qty)>0){
								$total_price_of_product_without_promotion=($promo_remain_qty*$individual_price_of_product_without_promotion);

								$quantity_without_promotion=$promo_remain_qty;
							}else{
								$total_price_of_product_without_promotion=0;
								$quantity_without_promotion=0;
							}

							//echo "quantity_without_promotion=".$quantity_without_promotion;
							//echo "total_price_of_product_without_promotion=".$quantity_without_promotion;
							
						}

						/* new customised fields */

						//$product_vendor_id


						
						$object = new stdClass();
						if(!empty($inv_data)){
							$customer_id=$customer_id;
							$purchased_price=$inv_data->purchased_price;
							$inventory_id=$inv_id;
							$product_id=$inv_data->product_id;
							$sku_id=$inv_data->sku_id;
							$sku_name=$inv_data->sku_name;
							$attribute_1=$inv_data->attribute_1;
							$attribute_2=$inv_data->attribute_2;
							$attribute_3=$inv_data->attribute_3;
							$attribute_4=$inv_data->attribute_4;
							$attribute_1_value=$inv_data->attribute_1_value;
							$attribute_2_value=$inv_data->attribute_2_value;
							$attribute_3_value=$inv_data->attribute_3_value;
							$attribute_4_value=$inv_data->attribute_4_value;
				
				
							$quantity=$inv_quantity;
							$product_status=$inv_data->product_status;
							$image=$inv_data->thumbnail;
							
							$product_price=$inv_data->selling_price;
							
							
							$max_selling_price=$inv_data->max_selling_price;
							$selling_discount=$inv_data->selling_discount;
							$tax_percent_price=$inv_data->tax_percent_price;
							$taxable_price=$inv_data->taxable_price;
										
							//$grandtotal=(($resObj->selling_price)*($resObj->inventory_moq))+(($resObj->inventory_moq)*($resObj->shipping_charge));
				
							$random_number=rand(100,10000);
				
							$payment_type=$payment_method;
							$payment_status="no";
							$tax_percent=$inv_data->tax_percent;
				
							$shipping_charge=$combo_shipping_price;
							$payment_method=$payment_method ;
				
				
							/* External values */
				
							$logistics_id=$inv_data_combo->logistics_id;
							$logistics_parcel_category_id=$inv_data_combo->logistics_parcel_category_id;
							$logistics_delivery_mode_id=$inv_data_combo->default_delivery_mode_id;
							$logistics_territory_id=$inv_data_combo->logistics_territory_id;
				
							$delivery_mode=$combo_details->delivery_mode;//not there
							$parcel_type=$combo_details->parcel_type;//not there
							$logistics_name=$combo_details->logistics_name;
							$logistics_weblink=$combo_details->logistics_weblink;
							$return_period=$combo_details->return_period;

							$territory_duration=$inv_data_combo->territory_duration;

							$date = strtotime("+".$inv_data_combo->territory_duration." days", strtotime(date('Y-m-d h:i:s')) );
							$expected_delivery_date=date("D j M Y",$date);
							
							/* External values */
				
							$rand=mt_rand(1,1000000);
							//$invoice_number='#BLR_'.date("ym".date("d")).$rand;
							$invoice_number=$this->get_invoice_number();
					
							$vendor_id=$inv_data->vendor_id;
							
							$sku_name=$inv_data->sku_name;

							if($sku_name==''){
								$sku_name=$inv_data_combo->inv_name;
							}
							
							$CGST=$inv_data->CGST;
							$IGST=$inv_data->IGST;
							$SGST=$inv_data->SGST;
							
							$default_discount=0;//straight discount
				
							$addon_products_status='0';
							$addon_products='';
							$addon_inventories='';
							$addon_total_price='';
							$addon_total_shipping_price='';

							$subtotal=$inv_price;
							$grandtotal=($inv_price+$shipping_charge);
							
							$ord_addon_products_status=$addon_products_status;
							$ord_addon_products=$addon_products;
							$ord_addon_inventories=$addon_inventories;
							$ord_addon_total_price=0;

							$object->customer_id=$customer_id;
							$object->inventory_id=$inventory_id;
							$object->product_id=$product_id;
							$object->inventory_sku=$sku_id;
							$object->product_name=$sku_name;
							$object->tax_percent=$tax_percent;
							$object->selling_price=$product_price;
							$object->purchased_price=$purchased_price;
							$object->max_selling_price=$max_selling_price;
							$object->selling_discount=$selling_discount;
							$object->tax_percent_price=$tax_percent_price;
							$object->taxable_price=$taxable_price;
							$object->shipping_charge=$shipping_charge;
							$object->logistics_id=$logistics_id;
							$object->vendor_id=$vendor_id;
							$object->logistics_parcel_category_id=$logistics_parcel_category_id;
							$object->logistics_price=$shipping_charge;
							$object->default_delivery_mode_id=$logistics_delivery_mode_id;
							$object->logistics_territory_id=$logistics_territory_id;
							$object->territory_duration=$territory_duration;
							$object->inventory_weight_in_kg=0;
							$object->inventory_weight_in_kg_org=0;
							$object->inventory_weight_in_kg_for_unit=0;
							$object->inventory_volume_in_kg=0;
							$object->inventory_volume_in_kg_for_unit=0;
							$object->return_period=$return_period;
							$object->promotion_minimum_quantity='';
							$object->inventory_selling_price_with_out_discount=$max_selling_price;
							$object->product_description='';
							$object->inventory_discount='';
							$object->inventory_selling_price_with_discount=$product_price;
							$object->attribute_1=$attribute_1;
							$object->attribute_2=$attribute_2;
							$object->attribute_3=$attribute_3;
							$object->attribute_4=$attribute_4;
							$object->attribute_1_value=$attribute_1_value;
							$object->attribute_2_value=$attribute_2_value;
							$object->attribute_3_value=$attribute_3_value;
							$object->attribute_4_value=$attribute_4_value;
							$object->inventory_moq=$quantity;
							$object->inventory_moq_original=$inv_data->moq;
							$object->inventory_max_oq=$inv_data->max_oq;
							$object->product_status=$product_status;
							$object->inventory_image=$image;
							$object->inventory_selling_price_with_discount_original=$product_price;
							$object->purchased_item_type='';

							$object->promotion_available=$promotion_available;
							if($promotion_available==1){
								
								$object->promotion_residual=$promotion_residual;
								$object->promotion_discount=$promotion_discount;
								$object->promotion_cashback='';
								$object->promotion_surprise_gift='';
								$object->promotion_surprise_gift_type='';			
								$object->promotion_surprise_gift_skus='';
								$object->promotion_surprise_gift_skus_nums='';
								$object->default_discount=$default_discount;
								$object->promotion_item_multiplier=$promotion_item_multiplier;
								$object->promotion_item='';
								$object->promotion_item_num='';
								$object->promotion_clubed_with_default_discount=0;
								$object->promotion_id_selected=$promotion_id_selected;
								$object->promotion_type_selected=$promotion_type_selected;
								$object->promotion_minimum_quantity=$promotion_minimum_quantity;
								$object->promotion_quote=$promotion_quote;
								$object->promotion_default_discount_promo=$promotion_default_discount_promo;

								$object->individual_price_of_product_with_promotion=$individual_price_of_product_with_promotion;
								$object->individual_price_of_product_without_promotion=$individual_price_of_product_without_promotion;
								$object->total_price_of_product_without_promotion=$total_price_of_product_without_promotion;
								$object->total_price_of_product_with_promotion=$total_price_of_product_with_promotion;
								$object->quantity_without_promotion=$quantity_without_promotion;
								$object->quantity_with_promotion=$quantity_with_promotion;

								
							}else{
								
								$object->promotion_residual=0;
								$object->promotion_discount='';
								$object->promotion_cashback='';
								$object->promotion_surprise_gift='';
								$object->promotion_surprise_gift_type='';			
								$object->promotion_surprise_gift_skus='';
								$object->promotion_surprise_gift_skus_nums='';
								$object->default_discount=$default_discount;
								$object->promotion_item_multiplier=0;
								$object->promotion_item='';
								$object->promotion_item_num='';
								$object->promotion_clubed_with_default_discount=0;
								$object->promotion_id_selected='';
								$object->promotion_type_selected='';
								$object->promotion_minimum_quantity='';
								$object->promotion_quote='';
								$object->promotion_default_discount_promo='';
								$object->individual_price_of_product_with_promotion=0;
								$object->individual_price_of_product_without_promotion=0;
								$object->total_price_of_product_without_promotion=0;
								$object->total_price_of_product_with_promotion=0;
								$object->quantity_without_promotion=0;
								$object->quantity_with_promotion=0;

							}

							$object->payment_policy_uid='';
							$object->message='';
							$object->attributes='';
							$object->cod_restrictions='';
							$object->max_value='';
							$object->currency_type='';
							
							$object->cash_back_value='';
							$object->CGST=$CGST;
							$object->IGST=$IGST;
							$object->SGST=$SGST;
							$object->payment_method=$payment_method;
							$object->addon_products_status=$addon_products_status;
							$object->addon_products='';
							$object->addon_inventories='';
							$object->addon_total_price='';
							$object->addon_total_shipping_price='';
							$object->addon_single_or_multiple_tagged_inventories_in_frontend='';

							
				
							$object->delivery_mode=$delivery_mode;
							$object->parcel_type=$parcel_type;
							$object->logistics_name=$logistics_name;
							$object->logistics_weblink=$logistics_weblink;
							
							
							/* External values */
				
							
							$object->invoice_number=$invoice_number;
					
							
							$object->subtotal=$inv_price;

							$object->grandtotal=($inv_price+$shipping_charge);
							
							$object->ord_addon_products_status=$ord_addon_products_status;
							$object->ord_addon_products=$ord_addon_products;
							$object->ord_addon_inventories=$ord_addon_inventories;
							$object->ord_addon_total_price=$ord_addon_total_price;

							//$object->total_price_of_product_without_promotion=$total_price_of_product_without_promotion;
							
							$combo_object_arr[]=$object;
				
							//return $flag;

						}
				}
			}
		}
		
		return $combo_object_arr;

	}
	
	/*Combo Order For Normal Workflow*/
	
	public function confirm_order_process_combo($order_id){

		$customer_id=$this->session->userdata("customer_id");
		$cart_info=$this->get_combo_cart();
		$adm_combo_details=get_admin_combo_settings();
		$combo_details=get_combo_carttable($customer_id);

		if(!empty($combo_details)){

			$combo_products=json_decode($combo_details->combo_products);
			$combo_number_of_products=$combo_details->combo_number_of_products;
			$combo_total_price=$combo_details->combo_total_price;
			$combo_shipping_price=$combo_details->combo_shipping_price;
			$combo_grand_total=$combo_details->combo_grand_total;
			$combo_discount=$combo_details->combo_discount;
			$combo_total_amount_saved=$combo_details->combo_total_amount_saved;


			$customer_address_id=$this->get_customer_default_id();
			$shipping_address_id=$this->add_shipping_address($customer_address_id);

			if(!empty($combo_products)){
				$ids = array_column($combo_products, 'inv_id');
				$inv_ids=implode(',',$ids);

				/* invoice table update */

				$promotion_invoice_cash_back='';
				$promotion_invoice_free_shipping='';
				$promotion_invoice_discount='';
				$total_amount_cash_back=0;
				$total_amount_saved=$combo_total_amount_saved;
				$total_amount_to_pay=$combo_grand_total;
				$payment_method='Paytm';
				$invoice_offer_achieved='0';
				$invoice_surprise_gift_promo_quote='';
				$invoice_surprise_gift='';
				$invoice_surprise_gift_type='';
				$invoice_surprise_gift_skus='';
				$invoice_surprise_gift_skus_nums='';
				$count_carttable=$combo_number_of_products;
				$promotion_invoice_cashback_quote='';
				$total_price_without_shipping_price=$combo_total_price;
				$total_coupon_used_amount=0;
				$total_shipping_charge=$combo_shipping_price;
				$inv_addon_products_amount=$combo_grand_total;
				$invoice_coupon_txt='';

				$sql_inv_offers="insert into invoices_offers set promotion_invoice_cash_back='$promotion_invoice_cash_back', promotion_invoice_free_shipping='$promotion_invoice_free_shipping' ,promotion_invoice_discount='$promotion_invoice_discount',total_amount_cash_back='$total_amount_cash_back',total_amount_saved='$total_amount_saved',total_amount_to_pay='$total_amount_to_pay',payment_method='$payment_method',invoice_offer_achieved='$invoice_offer_achieved',order_id='$order_id',customer_id='$customer_id',shipping_address_id='$shipping_address_id',invoice_surprise_gift_promo_quote='$invoice_surprise_gift_promo_quote',invoice_surprise_gift='$invoice_surprise_gift',invoice_surprise_gift_type='$invoice_surprise_gift_type',invoice_surprise_gift_skus='$invoice_surprise_gift_skus' ,invoice_surprise_gift_skus_nums='$invoice_surprise_gift_skus_nums',cart_quantity='$count_carttable',promotion_invoice_cashback_quote='$promotion_invoice_cashback_quote',total_price_without_shipping_price='$total_price_without_shipping_price',total_shipping_charge='$total_shipping_charge',total_coupon_used_amount='$total_coupon_used_amount',inv_addon_products_amount='$inv_addon_products_amount' $invoice_coupon_txt ";
        
				$this->db->query($sql_inv_offers);
				
				$invoices_offers_id=$this->db->insert_id();
				

				/* invoice table update */

				foreach($combo_products as $key=>$inv_data_combo){

					if($key==1){
						$inv_id=$inv_data_combo->inv_id;
						$inv_price=$inv_data_combo->inv_price;
						$inv_sku_id=$inv_data_combo->inv_sku_id;
						$inv_name=$inv_data_combo->inv_name;
						$inv_image=$inv_data_combo->inv_image;

						$inv_data=get_all_inventory_data($inv_id);


						/* inv data */
						if(!empty($inv_data)){

							/** copied */
		
							$customer_id=$customer_id;
							$purchased_price=$inv_data->purchased_price;
							$inventory_id=$inv_id;
							$product_id=$inv_data->product_id;
							$sku_id=$inv_data->sku_id;

							/* get product_vendor_id */
							$product_vendor_id=get_product_vendor_id($inventory_id);
							/* get product_vendor_id */
						
							
							$inv_data->attribute_1;
							$inv_data->attribute_2;
							$inv_data->attribute_3;
							$inv_data->attribute_4;
							$inv_data->attribute_1_value;
							$inv_data->attribute_2_value;
							$inv_data->attribute_3_value;
							$inv_data->attribute_4_value;
				
							
				
							$quantity=1;
							$inv_data->product_status;
							$image=$inv_data->thumbnail;
							
							//$product_price=$resObj->inventory_selling_price_with_discount_original;
							
							$product_price=$inv_price;
							
							$max_selling_price=$inv_data->max_selling_price;
							//$selling_discount=$inv_data->selling_discount;
							$selling_discount=$combo_discount; // combo discount on mrp price
							$tax_percent_price=$inv_data->tax_percent_price;
							$taxable_price=$inv_data->taxable_price;
										
							//$order_id=date("dmHi".$this->session->userdata("customer_id"));
							//$grandtotal=(($resObj->selling_price)*($resObj->inventory_moq))+(($resObj->inventory_moq)*($resObj->shipping_charge));
				
							$random_number=rand(100,10000);
				
							$payment_type='Paytm';
							$payment_status="no";
							$tax_percent=$inv_data->tax_percent;
				
							$shipping_charge=$combo_shipping_price;
							$payment_method='Paytm' ;
				
				
							/* External values */
				
							$logistics_id=$combo_details->logistics_id;
							$logistics_parcel_category_id=$combo_details->logistics_parcel_category_id;
							$logistics_delivery_mode_id=$combo_details->default_delivery_mode_id;
							$logistics_territory_id=$combo_details->logistics_territory_id;
				
							$delivery_mode=$combo_details->delivery_mode;
							$parcel_type=$combo_details->parcel_type;
				
							$date = strtotime("+".$combo_details->territory_duration." days", strtotime(date('Y-m-d h:i:s')) );
							$expected_delivery_date=date("D j M Y",$date);
							$logistics_name=$combo_details->logistics_name;
							$logistics_weblink=$combo_details->logistics_weblink;
							$return_period=$combo_details->return_period;
							
							/* External values */
				
							$rand=mt_rand(1,1000000);
							//$invoice_number='#BLR_'.date("ym".date("d")).$rand;
							$invoice_number=$this->get_invoice_number();
					
							$vendor_id=$inv_data->vendor_id;
							
							$sku_name=$inv_data->product_name;
							
							$CGST=$inv_data->CGST;
							$IGST=$inv_data->IGST;
							$SGST=$inv_data->SGST;
							$addon_txt='';
				
							$promotion_available=0;
							$default_discount=0;
				
							/** copied */
							
							
							$addon_products_status='1';
							$addon_products=$combo_details->combo_products;
							$addon_inventories=$inv_ids;
							$addon_total_price=$combo_total_price;
							$addon_total_shipping_price=$combo_shipping_price;

							$subtotal=$combo_total_price;
							$grandtotal=$combo_grand_total;
							
							$ord_addon_products_status=$addon_products_status;
							$ord_addon_products=$addon_products;
							$ord_addon_inventories=$addon_inventories;
							$ord_addon_total_price=$combo_grand_total;

							$total_price_of_product_without_promotion=0;
							
							if($addon_products=='"[]"'){
								$addon_products='[]';
							}

							$addon_txt.=", ord_addon_products_status='$addon_products_status' ,ord_addon_products='$addon_products',ord_addon_inventories='$addon_inventories',ord_addon_total_price='$combo_grand_total'";

							$sql="insert into active_orders set purchased_price='$purchased_price',customer_id='$customer_id',order_id='$order_id',shipping_address_id='$shipping_address_id',inventory_id='$inventory_id',product_id='$product_id',sku_id='$sku_id',ord_sku_name='".$sku_name."',product_price='$inv_price',ord_max_selling_price='$max_selling_price',ord_selling_discount='$selling_discount',ord_tax_percent_price='$tax_percent_price',ord_taxable_price='$taxable_price',subtotal='$subtotal',grandtotal='$grandtotal',quantity='$quantity',timestamp=now(),image='$image',random_number='$random_number',payment_type='$payment_type',payment_status='$payment_status',tax_percent='$tax_percent',shipping_charge='$combo_shipping_price',invoice_number='$invoice_number',return_period='$return_period',vendor_id='$vendor_id',logistics_name='$logistics_name',logistics_weblink='$logistics_weblink',expected_delivery_date='$expected_delivery_date',delivery_mode='$delivery_mode',parcel_category='$parcel_type',logistics_id='$logistics_id',logistics_delivery_mode_id='$logistics_delivery_mode_id',logistics_territory_id='$logistics_territory_id',logistics_parcel_category_id='$logistics_parcel_category_id',promotion_available='$promotion_available',CGST='$CGST',IGST='$IGST',SGST='$SGST',product_vendor_id=$product_vendor_id $addon_txt ";
				
							$flag=$this->db->query($sql);
					
							if($flag){
								$order_item_id=$this->db->insert_id();
								$count_active_orders[]="yes";
								//// update sequence generator table starts ///
								$sql_seqgen_order_id="update sequence_generator_combo set order_id='$order_id'";
								$this->db->query($sql_seqgen_order_id);
								
								$sql_seqgen_invoice_number="update sequence_generator_combo set invoice_number='$invoice_number'";
								$this->db->query($sql_seqgen_invoice_number);
								//// update sequence generator table ends ///
							}
							
							//default
							//wallet_status is 'no';
							//wallet_amount is '0';
							
							
							
							$sql_history="insert into history_orders (ord_max_selling_price,ord_selling_discount,ord_tax_percent_price,ord_taxable_price,CGST,IGST,SGST,purchased_price,order_item_id,customer_id,order_id,shipping_address_id,inventory_id,product_id,sku_id,tax_percent,product_price,shipping_charge,subtotal,wallet_status,wallet_amount,grandtotal,quantity,timestamp_active_order,image,random_number,invoice_number,logistics_name,logistics_weblink,expected_delivery_date,delivery_mode,parcel_category,vendor_id,payment_type,payment_status,return_period,logistics_id,logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id,promotion_available,ord_sku_name,ord_addon_products_status,ord_addon_products,ord_addon_inventories,ord_addon_total_price,product_vendor_id) values('$max_selling_price','$selling_discount','$tax_percent_price','$taxable_price','$CGST','$IGST','$SGST','$purchased_price','$order_item_id','$customer_id','$order_id','$shipping_address_id','$inventory_id','$product_id','$sku_id','$tax_percent','$product_price','$shipping_charge','$subtotal','no','0','$grandtotal','$quantity',now(),'$image','$random_number','$invoice_number','$logistics_name','$logistics_weblink','$expected_delivery_date','$delivery_mode','$parcel_type','$vendor_id','$payment_type','$payment_status','$return_period','$logistics_id','$logistics_delivery_mode_id','$logistics_territory_id','$logistics_parcel_category_id','$promotion_available','$sku_name','$ord_addon_products_status','$ord_addon_products','$ord_addon_inventories','$ord_addon_total_price','$product_vendor_id')";
				
												
							$result_history=$this->db->query($sql_history);   
				
							$sql_orders_status="insert into orders_status set order_item_id='$order_item_id',customer_id='$customer_id',notify_active='1'";
							$this->db->query($sql_orders_status);
							
							$policy_applied=get_instance()->getPolicyForInventory($inventory_id);
							if(!empty($policy_applied))	{
								foreach($policy_applied as $k => $policy){
									//    echo $k;
					
									$policy=$policy[0];
									
									$item_level=$this->db->escape_str($policy['item_level']);
									if(isset($policy['message'])){
										$policy['message']=$this->db->escape_str($policy['message']);
									}
									
									
									if($k=="Replacement"){
										//array_push($replacement_policy_arr,$policy);
						
										$sql="insert into order_replacement_policy set
										replacement_policy_uid='".$policy['replacement_policy_uid']."',order_id='$order_id',order_item_id='$order_item_id',
										item_level='".$item_level."', message='".$policy['message']."',replacement_restrict='".$policy['replacement_restrict']."',new_days_for_replacement='".$policy['new_days_for_replacement']."'";
										$this->db->query($sql);
										
									}
					
									if($k=="Refund"){    
										//array_push($refund_policy_arr,$policy);
										$sql="insert into order_refund_policy set refund_policy_uid='".$policy['refund_policy_uid']."', order_id='$order_id',order_item_id='$order_item_id',item_level='".$item_level."',refund_method='".$policy['refund_method']."',message='".$policy['message']."',refund_restrict='".$policy['refund_restrict']."',new_days_for_refund='".$policy['new_days_for_refund']."'";
										$this->db->query($sql);
									}
									if($k=="Cancellation"){  
										//array_push($cancellation_policy_arr,$policy);
										$sql="insert into order_cancellation_policy set cancellation_policy_uid='".$policy['cancellation_policy_uid']."',order_id='$order_id',order_item_id='$order_item_id',item_level='".$item_level."',message='".$policy['message']."',cancellation_restrict='".$policy['cancellation_restrict']."',new_days_for_cancellation='".$policy['new_days_for_cancellation']."',new_hour_limit='".$policy['new_hour_limit']."',order_status_type='".$policy['order_status_type']."'";
										$this->db->query($sql);
									}
									if($k=="Exchange"){
										//array_push($exchange_policy_arr,$policy);
										//$sql="insert into order_exchange_policy set ";
									}
								}
							}
				
							return $flag;

						}//inventory data
				
				
					/* inv data */
				}//only one entry

				}
			}
		}

	}

	public function failed_order_process_combo($order_id){

		$customer_id=$this->session->userdata("customer_id");
		$cart_info=$this->get_combo_cart();
		$adm_combo_details=get_admin_combo_settings();
		$combo_details=get_combo_carttable($customer_id);

		if(!empty($combo_details)){

			$combo_products=json_decode($combo_details->combo_products);
			$combo_number_of_products=$combo_details->combo_number_of_products;
			$combo_total_price=$combo_details->combo_total_price;
			$combo_shipping_price=$combo_details->combo_shipping_price;
			$combo_grand_total=$combo_details->combo_grand_total;
			$combo_discount=$combo_details->combo_discount;
			$combo_total_amount_saved=$combo_details->combo_total_amount_saved;


			$customer_address_id=$this->get_customer_default_id();
			$shipping_address_id=$this->add_shipping_address($customer_address_id);

			if(!empty($combo_products)){
				$ids = array_column($combo_products, 'inv_id');
				$inv_ids=implode(',',$ids);

				
				foreach($combo_products as $key=>$inv_data_combo){

					if($key==1){
						$inv_id=$inv_data_combo->inv_id;
						$inv_price=$inv_data_combo->inv_price;
						$inv_sku_id=$inv_data_combo->inv_sku_id;
						$inv_name=$inv_data_combo->inv_name;
						$inv_image=$inv_data_combo->inv_image;

						$inv_data=get_all_inventory_data($inv_id);
						
						/* inv data */
						if(!empty($inv_data)){

							/** copied */
		
							$customer_id=$customer_id;
							$purchased_price=$inv_data->purchased_price;
							$inventory_id=$inv_id;
							/* get product_vendor_id */
							$product_vendor_id=get_product_vendor_id($inventory_id);
							/* get product_vendor_id */
							$product_id=$inv_data->product_id;
							$sku_id=$inv_data->sku_id;		
							$inv_data->attribute_1;
							$inv_data->attribute_2;
							$inv_data->attribute_3;
							$inv_data->attribute_4;
							$inv_data->attribute_1_value;
							$inv_data->attribute_2_value;
							$inv_data->attribute_3_value;
							$inv_data->attribute_4_value;
				
							
				
							$quantity=1;
							$inv_data->product_status;
							$image=$inv_data->thumbnail;
							
							//$product_price=$resObj->inventory_selling_price_with_discount_original;
							
							$product_price=$inv_price;
							
							$max_selling_price=$inv_data->max_selling_price;
							//$selling_discount=$inv_data->selling_discount;
							$selling_discount=$combo_discount; // combo discount on mrp price
							$tax_percent_price=$inv_data->tax_percent_price;
							$taxable_price=$inv_data->taxable_price;
										
							//$order_id=date("dmHi".$this->session->userdata("customer_id"));
							//$grandtotal=(($resObj->selling_price)*($resObj->inventory_moq))+(($resObj->inventory_moq)*($resObj->shipping_charge));
				
							$random_number=rand(100,10000);
				
							$payment_type='Paytm';
							$payment_status="no";
							$tax_percent=$inv_data->tax_percent;
				
							$shipping_charge=$combo_shipping_price;
							$payment_method='Paytm' ;
				
				
							/* External values */
				
							$logistics_id=$combo_details->logistics_id;
							$logistics_parcel_category_id=$combo_details->logistics_parcel_category_id;
							$logistics_delivery_mode_id=$combo_details->default_delivery_mode_id;
							$logistics_territory_id=$combo_details->logistics_territory_id;
				
							$delivery_mode=$combo_details->delivery_mode;
							$parcel_type=$combo_details->parcel_type;
				
							$date = strtotime("+".$combo_details->territory_duration." days", strtotime(date('Y-m-d h:i:s')) );
							$expected_delivery_date=date("D j M Y",$date);
							$logistics_name=$combo_details->logistics_name;
							$logistics_weblink=$combo_details->logistics_weblink;
							$return_period=$combo_details->return_period;
							
							/* External values */
				
							$rand=mt_rand(1,1000000);
							//$invoice_number='#BLR_'.date("ym".date("d")).$rand;
							$invoice_number=$this->get_invoice_number();
					
							$vendor_id=$inv_data->vendor_id;
							
							$sku_name=$inv_data->product_name;
							
							$CGST=$inv_data->CGST;
							$IGST=$inv_data->IGST;
							$SGST=$inv_data->SGST;
							$addon_txt='';
				
							$promotion_available=0;
							$default_discount=0;
				
							/** copied */
							
							
							$addon_products_status='1';
							$addon_products=$combo_details->combo_products;
							$addon_inventories=$inv_ids;
							$addon_total_price=$combo_total_price;
							$addon_total_shipping_price=$combo_shipping_price;

							$subtotal=$combo_total_price;
							$grandtotal=$combo_grand_total;
							
							$ord_addon_products_status=$addon_products_status;
							$ord_addon_products=$addon_products;
							$ord_addon_inventories=$addon_inventories;
							$ord_addon_total_price=$combo_grand_total;

							$total_price_of_product_without_promotion=0;
							
							$addon_txt.=", ord_addon_products_status='$addon_products_status' ,ord_addon_products='$addon_products',ord_addon_inventories='$addon_inventories',ord_addon_total_price='$combo_grand_total'";

			
							$sql="insert into failed_orders set purchased_price='$purchased_price',customer_id='$customer_id',order_id='$order_id',shipping_address_id='$shipping_address_id',inventory_id='$inventory_id',product_id='$product_id',sku_id='$sku_id',ord_sku_name='".$sku_name."',product_price='$inv_price',ord_max_selling_price='$max_selling_price',ord_selling_discount='$selling_discount',ord_tax_percent_price='$tax_percent_price',ord_taxable_price='$taxable_price',subtotal='$subtotal',grandtotal='$grandtotal',quantity='$quantity',timestamp=now(),image='$image',random_number='$random_number',payment_type='$payment_type',payment_status='$payment_status',tax_percent='$tax_percent',shipping_charge='$combo_shipping_price',invoice_number='$invoice_number',return_period='$return_period',vendor_id='$vendor_id',logistics_name='$logistics_name',logistics_weblink='$logistics_weblink',expected_delivery_date='$expected_delivery_date',delivery_mode='$delivery_mode',parcel_category='$parcel_type',logistics_id='$logistics_id',logistics_delivery_mode_id='$logistics_delivery_mode_id',logistics_territory_id='$logistics_territory_id',logistics_parcel_category_id='$logistics_parcel_category_id',promotion_available='$promotion_available',CGST='$CGST',IGST='$IGST',SGST='$SGST',product_vendor_id='$product_vendor_id' $addon_txt ";

			
			$flag=$this->db->query($sql);
						}
					}
				}
			}
		}
	}

	/* combo order placement */		

    public function get_customer_details_by_customer_id($customer_id){
		$sql="select * from customer where id='$customer_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	public function get_customer_address_details_by_customer_id($customer_id){
		$sql="select address1,pincode from customer_address where customer_id='$customer_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	
	
	
	
	
	
	
	
		
		
		
		
		
		
		
		public function carttable_update_deviations(){
			
			
			$customer_id=$this->session->userdata("customer_id");
			$sql="select selling_price,inventory_id,promotion_available,promotion_id_selected from carttable where customer_id='$customer_id'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				$result_arr=$result->result();
				foreach($result_arr as $res){
					$carttable_selling_price=$res->selling_price;
					$carttable_inventory_id=$res->inventory_id;
					$carttable_promotion_available=$res->promotion_available;
					$carttable_promotion_id_selected=$res->promotion_id_selected;
					if($carttable_promotion_available==0){
						$sql_inventory_table="select * from inventory where id='$carttable_inventory_id'";
						$result_inventory_table=$this->db->query($sql_inventory_table);
						$result_inventory_table_row_arr=$result_inventory_table->row_array();
						if($carttable_selling_price!=$result_inventory_table_row_arr["selling_price"]){
							$product_id=$result_inventory_table_row_arr["product_id"];
							$sku_id=$result_inventory_table_row_arr["sku_id"];
							$tax_percent=$result_inventory_table_row_arr["tax_percent"];
							$selling_price=$result_inventory_table_row_arr["selling_price"];
							$purchased_price=$result_inventory_table_row_arr["purchased_price"];
							$max_selling_price=$result_inventory_table_row_arr["max_selling_price"];
							$selling_discount=$result_inventory_table_row_arr["selling_discount"];
							$tax_percent_price=$result_inventory_table_row_arr["tax_percent_price"];
							$taxable_price=$result_inventory_table_row_arr["taxable_price"];
							$attribute_1=$result_inventory_table_row_arr["attribute_1"];
							$attribute_1_value=$result_inventory_table_row_arr["attribute_1_value"];
							$attribute_2=$result_inventory_table_row_arr["attribute_2"];
							$attribute_2_value=$result_inventory_table_row_arr["attribute_2_value"];
							$attribute_3=$result_inventory_table_row_arr["attribute_3"];
							$attribute_3_value=$result_inventory_table_row_arr["attribute_3_value"];
							$attribute_4=$result_inventory_table_row_arr["attribute_4"];
							$attribute_4_value=$result_inventory_table_row_arr["attribute_4_value"];
							$inventory_moq_original=$result_inventory_table_row_arr["moq"];
							$inventory_max_oq=$result_inventory_table_row_arr["max_oq"];
							$CGST=$result_inventory_table_row_arr["CGST"];
							$IGST=$result_inventory_table_row_arr["IGST"];
							$SGST=$result_inventory_table_row_arr["SGST"];
							$sql_update_carttable="update carttable set 
																	product_id='$product_id',
																	inventory_sku='$sku_id',
																	tax_percent='$tax_percent',
																	selling_price='$selling_price',
																	purchased_price='$purchased_price',
																	max_selling_price='$max_selling_price',
																	selling_discount='$selling_discount',
																	tax_percent_price='$tax_percent_price',
																	taxable_price='$taxable_price',
																	inventory_selling_price_with_out_discount='$max_selling_price',
																	inventory_selling_price_with_discount='$selling_price',
																	attribute_1='$attribute_1',
																	attribute_1_value='$attribute_1_value',
																	attribute_2='$attribute_2',
																	attribute_2_value='$attribute_2_value',
																	attribute_3='$attribute_3',
																	attribute_3_value='$attribute_3_value',
																	attribute_4='$attribute_4',
																	attribute_4_value='$attribute_4_value',
																	inventory_moq_original='$inventory_moq_original',
																	inventory_max_oq='$inventory_max_oq',
																	inventory_selling_price_with_discount_original='$selling_price',
																	CGST='$CGST',
																	IGST='$IGST',
																	SGST='$SGST'";
							$this->db->query($sql_update_carttable);
							
						}
					}
					if($carttable_promotion_available==1){
						$promo_uid=$carttable_promotion_id_selected;
						// get the quote_selector
						$sql_query_selector="select * from promotions where promo_uid='$promo_uid' and promo_active=1";
						$query_selector=$this->db->query($sql_query_selector);
						$query_row_arr=$query_selector->row_array();
						$quote_selector=$query_row_arr["quote_selector"];
						if($quote_selector==5){
							$get_type_percentage=$query_row_arr["get_type"];
							$get_type_percentage_value=(float)str_replace("%","",$get_type_percentage);
							$promo_quote=$query_row_arr["promo_quote"];
							
							$sql_inventory_table="select * from inventory where id='$carttable_inventory_id'";
							$result_inventory_table=$this->db->query($sql_inventory_table);
							$result_inventory_table_row_arr=$result_inventory_table->row_array();
							
							$product_id=$result_inventory_table_row_arr["product_id"];
							$sku_id=$result_inventory_table_row_arr["sku_id"];
							$tax_percent=$result_inventory_table_row_arr["tax_percent"];
							$selling_price=$result_inventory_table_row_arr["selling_price"];
							$selling_price_after_promo_applied=($selling_price-$selling_price*$get_type_percentage_value/100);
							$purchased_price=$result_inventory_table_row_arr["purchased_price"];
							$max_selling_price=$result_inventory_table_row_arr["max_selling_price"];
							$selling_discount=$result_inventory_table_row_arr["selling_discount"];
							
							$taxable_price=round(((float)($selling_price_after_promo_applied)/(1+(float)($tax_percent)/100)),2);
							$tax_percent_price=round(((float)($selling_price_after_promo_applied)-(float)($taxable_price)),2);
			
			
							//$tax_percent_price=$result_inventory_table_row_arr["tax_percent_price"];
							//$taxable_price=$result_inventory_table_row_arr["taxable_price"];
							$attribute_1=$result_inventory_table_row_arr["attribute_1"];
							$attribute_1_value=$result_inventory_table_row_arr["attribute_1_value"];
							$attribute_2=$result_inventory_table_row_arr["attribute_2"];
							$attribute_2_value=$result_inventory_table_row_arr["attribute_2_value"];
							$attribute_3=$result_inventory_table_row_arr["attribute_3"];
							$attribute_3_value=$result_inventory_table_row_arr["attribute_3_value"];
							$attribute_4=$result_inventory_table_row_arr["attribute_4"];
							$attribute_4_value=$result_inventory_table_row_arr["attribute_4_value"];
							$inventory_moq_original=$result_inventory_table_row_arr["moq"];
							$inventory_max_oq=$result_inventory_table_row_arr["max_oq"];
							$CGST=$result_inventory_table_row_arr["CGST"];
							$IGST=$result_inventory_table_row_arr["IGST"];
							$SGST=$result_inventory_table_row_arr["SGST"];
							$sql_update_carttable="update carttable set 
																	product_id='$product_id',
																	inventory_sku='$sku_id',
																	tax_percent='$tax_percent',
																	selling_price='$selling_price_after_promo_applied',
																	purchased_price='$purchased_price',
																	max_selling_price='$max_selling_price',
																	selling_discount='$selling_discount',
																	tax_percent_price='$tax_percent_price',
																	taxable_price='$taxable_price',
																	inventory_selling_price_with_out_discount='$max_selling_price',
																	inventory_selling_price_with_discount='$selling_price_after_promo_applied',
																	attribute_1='$attribute_1',
																	attribute_1_value='$attribute_1_value',
																	attribute_2='$attribute_2',
																	attribute_2_value='$attribute_2_value',
																	attribute_3='$attribute_3',
																	attribute_3_value='$attribute_3_value',
																	attribute_4='$attribute_4',
																	attribute_4_value='$attribute_4_value',
																	inventory_moq_original='$inventory_moq_original',
																	inventory_max_oq='$inventory_max_oq',
																	inventory_selling_price_with_discount_original='$selling_price_after_promo_applied',
																	promotion_discount='$get_type_percentage_value',
																	default_discount='$get_type_percentage_value',
																	promotion_quote='$promo_quote',
																	promotion_default_discount_promo='$promo_quote',
																	individual_price_of_product_with_promotion='$selling_price_after_promo_applied',
																	CGST='$CGST',
																	IGST='$IGST',
																	SGST='$SGST'";
							$this->db->query($sql_update_carttable);
							
						
						
						
						}
					}
				}
			}
		}
		
		
		
		function request_a_quote_different_views(
														$rq_unique_id,
														$rq_customer_id,
														$inv_id_arr,
														$inv_sku_id_arr,$v="c",$inv_qtys_arr){
			$inv_id_in=implode(",",$inv_id_arr);

			if(is_array($inv_sku_id_arr)){
				$inv_sku_id_in=implode(",",$inv_sku_id_arr);
			}else{
				$inv_sku_id_in=$inv_sku_id_arr;
			}
			//print_r($inv_qtys_arr);

			if(is_array($inv_qtys_arr)){
				$inv_qtys_arr_in=implode(",",$inv_qtys_arr);
			}else{
				$inv_qtys_arr_in=$inv_qtys_arr;
			}
			$vendor_id_arr=[];
			
			$sql_sku_id="select vendor_id from inventory where 
							id in ($inv_id_in) and active=1";
			$query_sku_id_arr=$this->db->query($sql_sku_id);
			$res_arr=$query_sku_id_arr->result_array();
			foreach($res_arr as $arr){
				$vendor_id_arr[]=$arr["vendor_id"];
			}
			$vendor_id_in=implode(",",$vendor_id_arr);
			$sql="insert into request_a_quote set
                                    rq_unique_id='$rq_unique_id',	
									rq_customer_id='$rq_customer_id',
									rq_inv_ids='$inv_id_in',
									rq_sku_ids='$inv_sku_id_in',
									rq_inv_qtys='$inv_qtys_arr_in',
									vendor_id_list='$vendor_id_in'
									";
			$query=$this->db->query($sql);
			return $query;
	}
		
		
	
	
	public function get_details_from_rfq($rq_unique_id){
            $customer_id=$this->session->userdata("customer_id");
            if($this->session->userdata("customer_id")){
                $sql="select * from  request_a_quote where rq_customer_id='$customer_id' and rq_unique_id='$rq_unique_id'";
            }else{
                $sql="select * from  request_a_quote where rq_unique_id='$rq_unique_id'";
            }
            
            $result=$this->db->query($sql);
            return  $result->row(); 
        }
		
		
		public function get_category_name_by_category_id($cat_id){
		$sql="select cat_name from category where cat_id='$cat_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->cat_name;
		}
		else{
			return "";
		}
	}
	
	
	public function get_subcategory_name_by_subcategory_id($subcat_id){
		$sql="select subcat_name from subcategory where subcat_id='$subcat_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->subcat_name;
		}
		else{
			return "";
		}
	}
	public function get_brand_name_by_brand_id($brand_id){
		$sql="select brand_name from brands where brand_id='$brand_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->brand_name;
		}
		else{
			return "";
		}
	}
	
	public function get_product_name_by_product_id($product_id){
		$sql="select product_name from products where product_id='$product_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->product_name;
		}
		else{
			return "";
		}
	}
	
	
	public function request_a_quote_common_action(
	            $rq_unique_id,
				$rq_customer_id,
				$rq_sku_ids,
				$rq_inv_ids,
				$rq_inv_qtys,
				$rq_pincode,
				$rq_name,
				$rq_email,
				$rq_mobile,
				$rq_location
			){
			
			
			


			
				
			if($rq_unique_id!=""){
				
				$sql="update 
						request_a_quote set 
							rq_customer_id=".$this->db->escape($rq_customer_id).",
							rq_sku_ids=".$this->db->escape($rq_sku_ids).",
							rq_inv_ids=".$this->db->escape($rq_inv_ids).",
							rq_inv_qtys=".$this->db->escape($rq_inv_qtys).",
							rq_pincode=".$this->db->escape($rq_pincode).",
							rq_name=".$this->db->escape($rq_name).",
							rq_email=".$this->db->escape($rq_email).",
							rq_mobile=".$this->db->escape($rq_mobile).",
							rq_location=".$this->db->escape($rq_location)." where rq_unique_id='$rq_unique_id'";
						
						$query=$this->db->query($sql);
			}
			else{
				
				$sql_rq_unique_id="insert into request_a_quote set rq_unique_id='$rq_unique_id'";
				$query_rq_unique_id=$this->db->query($sql_rq_unique_id);
			
			
				$sql="update 
						request_a_quote set 
							rq_customer_id=".$this->db->escape($rq_customer_id).",
							rq_sku_ids=".$this->db->escape($rq_sku_ids).",
							rq_inv_ids=".$this->db->escape($rq_inv_ids).",
							rq_inv_qtys=".$this->db->escape($rq_inv_qtys).",
							rq_pincode=".$this->db->escape($rq_pincode).",
							rq_name=".$this->db->escape($rq_name).",
							rq_email=".$this->db->escape($rq_email).",
							rq_mobile=".$this->db->escape($rq_mobile).",
							rq_location=".$this->db->escape($rq_location)." 
							where rq_unique_id='$rq_unique_id'";
						
						$query=$this->db->query($sql);
			}
			return $query;
			}
			
	public function get_vendor_email_list_by_vendor_id_list($vendor_id_list){
		$sql="select email from product_vendors where vendor_id  in ($vendor_id_list)";
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	public function update_vendor_email_list_log_need_to_be_sent($vendor_email_arr,$vendor_id_list,$rq_unique_id){
		$vendor_email_in=implode(",",$vendor_email_arr);
		$sql="update request_a_quote set vendor_id_list='$vendor_id_list',vendor_total_email_list='$vendor_email_in',vendor_email_list_sent='' where rq_unique_id='$rq_unique_id'";
		$result=$this->db->query($sql);
	}
	public function get_email_of_admin(){
		$sql="select email from admin_country where user_type='Master Country'";
		$result=$this->db->query($sql);
		return $result->row()->email;
	}
	public function update_vendor_email_list_log_sent($rq_unique_id){
		$sql="update request_a_quote set vendor_email_list_sent=1 where rq_unique_id='$rq_unique_id'";
		$result=$this->db->query($sql);
		return $result;
	}
	public function request_a_quote_common_action_mail_status($rq_unique_id){
		$sql="select vendor_email_list_sent from request_a_quote where rq_unique_id='$rq_unique_id'";
		$result=$this->db->query($sql);
		return $result->row_array()["vendor_email_list_sent"];
	}
	public function update_bank_details_franchise($customer_id,$bank_name,$bank_branch_name,$bank_branch_city,$bank_ifsc_code,$bank_account_number,$account_holder_name){
		$sql="update franchise_customer set 
									bank_name='$bank_name',
									branch_name='$bank_branch_name',
									branch_city='$bank_branch_city',
									ifsc_code='$bank_ifsc_code',
									account_number='$bank_account_number',
									account_holder_name='$account_holder_name' where id='$customer_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	/* new section for franchise */
	
	public function add_customer_result($name,$email,$mobile,$password,$rand,$pincode,$city,$state,$country)
	 {
		 $rand="1234";
		 $temp_arr=array();
		 $pass=md5($password);
		 //$pass='';		 
		 $image="assets/pictures/franchise_uploads/user.jpg";
		 $data=array(
					"name"=>$name,
					"email"=>$email,
					"mobile"=>$mobile,
					"password"=>$pass,
					"password_org"=>$password,
					"random_no"=>$rand,
					"image"=>$image,
					"pincode"=>$pincode,
					"city"=>$city,
					"state"=>$state,
					"country"=>$country
					);
					
					
		$result=$this->db->insert( "franchise_customer",$data);
		return $result;
	 }
	/* new section for franchise */
	public function add_design_result($name,$email,$mobile,$pincode,$city,$state,$country,$size_of_home,$interior_budget,$design_type,$size_of_home_des)
	 {
		 
		 $data=array(
					"name"=>$name,
					"email"=>$email,
					"mobile"=>$mobile,
					"pincode"=>$pincode,
					"city"=>$city,
					"state"=>$state,
					"country"=>$country,
					"size_of_home"=>$size_of_home,
					"interior_budget"=>$interior_budget,
					"design_type"=>$design_type,
					"size_of_home_des"=>$size_of_home_des
					);
					
					
		$result=$this->db->insert( "customer_design_request",$data);
		return $result;
	 }

	 public function add_design_kitchen_result($name,$email,$mobile,$pincode,$city,$state,$country,$interior_budget,$design_type,$kitchen_layout,$area_of_kitchen,$kitchen_top)
	 {
		 
		 $data=array(
					"name"=>$name,
					"email"=>$email,
					"mobile"=>$mobile,
					"pincode"=>$pincode,
					"city"=>$city,
					"state"=>$state,
					"country"=>$country,
					"interior_budget"=>$interior_budget,
					"kitchen_layout"=>$kitchen_layout,
					"area_of_kitchen"=>$area_of_kitchen,
					"design_type"=>$design_type,
					"kitchen_top"=>$kitchen_top
					);
					
					
		$result=$this->db->insert( "customer_design_request",$data);
		return $result;
	 }
	 public function add_design_bedroom_result($name,$email,$mobile,$pincode,$city,$state,$country,$interior_budget,$design_type,$bedroom_layout,$bedroom_style,$area_of_bedroom,$bedroom_utility){
		 
		 $data=array(
					"name"=>$name,
					"email"=>$email,
					"mobile"=>$mobile,
					"pincode"=>$pincode,
					"city"=>$city,
					"state"=>$state,
					"country"=>$country,
					"design_type"=>$design_type,
					"interior_budget"=>$interior_budget,
					"bedroom_layout"=>$bedroom_layout,
					"bedroom_style"=>$bedroom_style,
					"area_of_bedroom"=>$area_of_bedroom,
					"bedroom_utility"=>$bedroom_utility
					);
					
					
		$result=$this->db->insert( "customer_design_request",$data);
		return $result;
	 }
	 public function add_design_our_office_result($name,$email,$mobile,$pincode,$city,$state,$country,$interior_budget,$design_type,$office_layout,$requirement,$office_type,$size_of_office,$office_style){
		 
		$data=array(
				   "name"=>$name,
				   "email"=>$email,
				   "mobile"=>$mobile,
				   "pincode"=>$pincode,
				   "city"=>$city,
				   "state"=>$state,
				   "country"=>$country,
				   "design_type"=>$design_type,
				   "interior_budget"=>$interior_budget,
				   "office_layout"=>$office_layout,
				   "office_requirement"=>$requirement,
				   "office_type"=>$office_type,
				   "size_of_office"=>$size_of_office,
				   "office_style"=>$office_style,
				   );
				   
				   
	   $result=$this->db->insert( "customer_design_request",$data);
	   return $result;
	}

	public function add_design_conf_room_result($name,$email,$mobile,$pincode,$city,$state,$country,$interior_budget,$design_type,$conf_room_layout,$conf_room_style,$size_of_conf_room,$conf_room_type){
		 
		$data=array(
				   "name"=>$name,
				   "email"=>$email,
				   "mobile"=>$mobile,
				   "pincode"=>$pincode,
				   "city"=>$city,
				   "state"=>$state,
				   "country"=>$country,
				   "design_type"=>$design_type,
				   "interior_budget"=>$interior_budget,
				   "conf_room_layout"=>$conf_room_layout,
				   "conf_room_type"=>$conf_room_type,
				   "size_of_conf_room"=>$size_of_conf_room,
				   "conf_room_style"=>$conf_room_style,
				   );
				   
				   
	   $result=$this->db->insert( "customer_design_request",$data);
	   return $result;
	}
	
	public function add_design_workstations_result($name,$email,$mobile,$pincode,$city,$state,$country,$interior_budget,$design_type,$workstations_style,$workstations_material,$size_of_workstations,$seating_capacity){
		 
		$data=array(
				   "name"=>$name,
				   "email"=>$email,
				   "mobile"=>$mobile,
				   "pincode"=>$pincode,
				   "city"=>$city,
				   "state"=>$state,
				   "country"=>$country,
				   "design_type"=>$design_type,
				   "interior_budget"=>$interior_budget,
				   "workstations_style"=>$workstations_style,
				   "workstations_material"=>$workstations_material,
				   "size_of_workstations"=>$size_of_workstations,
				   "seating_capacity_workstations"=>$seating_capacity,
				   );
				   
				   
	   $result=$this->db->insert( "customer_design_request",$data);
	   return $result;
	}
	public function update_addon_changes_in_cart($cart_id,$addon_products,$addon_products_status,$addon_inventories,$addon_total_price,$addon_total_shipping_price){
		$customer_id=$this->session->userdata("customer_id");

		$sql_update="update carttable set addon_inventories='$addon_inventories',addon_total_price='$addon_total_price',addon_products_status='$addon_products_status',addon_products='$addon_products',addon_total_shipping_price='$addon_total_shipping_price' where cart_id='$cart_id' and customer_id='$customer_id' ";

		$res=$this->db->query($sql_update);
		return $res;
	}
	public function update_and_get_cartable_combo_info($combo_shipping_price,$combo_grand_total,$combo_products){
		$customer_id=$this->session->userdata("customer_id");

		$sql_update="update carttable_combo set combo_shipping_price='$combo_shipping_price',combo_grand_total='$combo_grand_total',combo_products='$combo_products' where combo_customer_id='$customer_id' ";

		$res=$this->db->query($sql_update);
		return $res;
	}
	
}


?>