<?php
class Front_recomendations extends CI_model{
    public function getAllRecomendation_data($rec_uid){
        $sql="select * from recomendation_tagged where recomendation_uid='$rec_uid'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }else{
            return false;
        }
    }
    
	public function get_sub_category_level_recomendations($pcat_id,$cat_id,$subcat_id){
	    
        $sql="select * from recomendation_recepient where pcat_id='$pcat_id' and cat_id='$cat_id' and subcat_id='$subcat_id' and brand_id='' and product_id='' and inv_id=''";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }else{
            return false;
        }	    
	}
    public function get_brand_level_recomendations($pcat_id,$cat_id,$subcat_id,$brand_id){
        $sql="select * from recomendation_recepient where pcat_id='$pcat_id' and cat_id='$cat_id' and subcat_id='$subcat_id' and brand_id='$brand_id' and product_id='' and inv_id=''";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }else{
            return false;
        }
    }
    public function get_product_level_recomendations($pcat_id,$cat_id,$subcat_id,$brand_id,$product_id){
        $sql="select * from recomendation_recepient where pcat_id='$pcat_id' and cat_id='$cat_id' and subcat_id='$subcat_id' and brand_id='$brand_id' and product_id='$product_id' and inv_id=''";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }else{
            return false;
        }
    }
    public function get_inventory_level_recomendations($pcat_id,$cat_id,$subcat_id,$brand_id,$product_id,$inventory_id){
        $sql="select * from recomendation_recepient where pcat_id='$pcat_id' and cat_id='$cat_id' and subcat_id='$subcat_id' and brand_id='$brand_id' and product_id='$product_id' and inv_id='$inventory_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }else{
            return false;
        }
    }
    
    public function getInventoryData($inv_id){
        $sql="select * from inventory where id='$inv_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return false;
        }
        
    }
    public function getProductName($productId){
         $sql="select product_name from products where product_id='$productId'";
         $result=$this->db->query($sql);
         if($result->num_rows()>0){
             return $result->row()->product_name;
         }else{
             return "Not Available";
         }
     }

}
?>