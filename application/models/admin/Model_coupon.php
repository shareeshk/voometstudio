<?php

class Model_coupon extends CI_Model{

    public function manage_coupon_processing($params,$fg){
			
        $var_to =$params['to_date'];
        $var_from =$params['from_date'];		

        //define index of column
        $columns = array(
            0 => 'coupon.coupon_id', 
            1 => 'coupon.coupon_code',
            2 => 'coupon.coupon_name',
            3 => 'coupon.coupon_status',
            4 => 'coupon.created_at',
            5 => 'coupon.created_at',
            6 => 'coupon.created_at'
        );

        $where = $sqlTot = $sqlRec = "";


        $sql = "select * from coupon ";

        if(!empty($params['search']['value']) ){   
            //$where .=" and ";
            $where .=" ( coupon.coupon_name LIKE '%".$params['search']['value']."%' ";    
            $where .=" OR coupon.coupon_code LIKE '%".$params['search']['value']."%' ";			
            $where .=" OR coupon.value LIKE '%".$params['search']['value']."%')";	 
            if(!empty($var_from) || !empty($var_to)){
               $where .=" AND "; 
            }
        }
        if(!empty($var_from) && !empty($var_to)){

            $date = str_replace('/', '-', $var_from);
            $from_date=date('Y-m-d', strtotime($date));
            $date = str_replace('/', '-', $var_to);
            $to_date=date('Y-m-d', strtotime($date));

            //$where .=" AND ";
            $where .=" DATE_FORMAT(coupon.start_date,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(coupon.end_date,'%Y-%m-%d') <= '$to_date'";


        }elseif(!empty($var_from) && empty($var_to)){
            $date = str_replace('/', '-', $var_from);
            $from_date=date('Y-m-d', strtotime($date));
            //$where .=" AND ";
            $where .=" DATE_FORMAT(coupon.start_date,'%Y-%m-%d') >= '$from_date'";
        }else{
            $where .="";
        }

        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= " where ".$where;
            $sqlRec .=  " where ".$where;
        }

        //echo $sqlTot;
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";

        //echo $sqlTot;

        if($fg=="get_total_num_recs"){
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }
    public function get_all_coupon_data($coupon_id){
        $sql="select * from coupon where coupon_id='$coupon_id'";
        $queryRecords=$this->db->query($sql);
        return $queryRecords->row();
    }
    public function parent_category() {
         $query = $this->db->query("SELECT * FROM `parent_category` where active=1");
         return $query -> result();
     }
     public function show_available_categories($pcat_id,$active){
         $active_str=($active!=2)? "and active='$active'" :"";
         $sql="select cat_id,cat_name,active from category where trim(pcat_id) = '$pcat_id'  $active_str";
         $query=$this->db->query($sql);
         return $query->result();
     }
     public function show_available_subcategories($cat_id,$active){
         $active_str=($active!=2)? "and active='$active'" :"";
         $sql="select subcat_id,subcat_name,active from subcategory where trim(cat_id) = trim('$cat_id') $active_str";
         $query=$this->db->query($sql);
         return $query->result();
     }
     public function show_available_brands($subcat_id,$active){
         $active_str=($active!=2)? "and active='$active'" :"";
         $sql="select brand_id,brand_name,active from brands where trim(subcat_id) = '$subcat_id' $active_str";
         $query=$this->db->query($sql);
         return $query->result();
     }
     public function show_available_products($brand_id,$active){
            $active_str=($active!=2)? "and active='$active'" :"";
            $sql="select product_id,product_name,active from  products where trim(brand_id) = '$brand_id' $active_str";
            $query=$this->db->query($sql);
            return $query->result();
    }

     public function show_available_skus($product_id,$active){
         $active_str=($active!=2)? "and inventory.active='$active' " :"";
         $active_str.=($active!=2)? " and products.active='$active' " :"";
         $sql="select inventory.id,sku_id,sku_name from  inventory 
               LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`
               where trim(products.product_id) = '$product_id' $active_str ";

         $query=$this->db->query($sql);
         return $query->result();
     }
    public function check_coupon_exists($coupon_code,$coupon_name,$coupon_id){
        if($coupon_id!=''){
            $sql="select * from coupon where (trim(coupon_code) = '$coupon_code' or coupon_name='$coupon_name') and coupon_id!='$coupon_id'";
        }else{
            $sql="select * from coupon where trim(coupon_code) = '$coupon_code' or coupon_name='$coupon_name'";
        }
        $query=$this->db->query($sql);
        if($query->num_rows()>0){
            $flag=1;
        }else{
            $flag=0;
        }
        return $flag;
    }
    public function add_or_update_coupon($coupon_id,$arr){
        
        $arr["sku_ids"]="";
        if(!empty($arr["to_select_list"])){
                $arr["sku_ids"]=implode("|",$arr["to_select_list"]);
        }
        unset($arr["to_select_list"]);
        unset($arr["from_select_list"]);
        unset($arr["q"]);
        $arr["quantity"]=1;
        
        $date = str_replace('/', '-', $arr['start_date']);
        $from_date=date('Y-m-d H:i', strtotime($date));
        $date = str_replace('/', '-', $arr['end_date']);
        $to_date=date('Y-m-d H:i', strtotime($date));
        
        $arr['start_date']=$from_date;
        $arr['end_date']=$to_date;
        
        $ty=$arr['type'];
        $v1=$arr['value1'];
        $v2=$arr['value2'];
        
        unset($arr['value1']);
        unset($arr['value2']);
        
        if($ty=='1'){
           $arr["value"]=$v2;//percentage 
        }
        if($ty=='2'){
            $arr["value"]=$v1;//flat
        }
        
        if($coupon_id==''){
            $this->db->insert('coupon', $arr);
        }else{
            $this->db->where('coupon_id', $coupon_id);
            $this->db->update('coupon', $arr);
        }

        if($this->db->affected_rows() > 0){
                return true;
        }else{
                return false;
        }
    }
    public function change_status($coupon_id,$status){
        $arr=[];
        if($status=='0'){
            $arr['status']=1;
        }else{
            $arr['status']=0;
        }
        if($coupon_id!='') {
            $this->db->where('coupon_id', $coupon_id);
            $this->db->update('coupon', $arr);
        }

        if($this->db->affected_rows() > 0){
                return true;
        }else{
                return false;
        }
    }
}
?>