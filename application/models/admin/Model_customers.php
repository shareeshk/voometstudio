<?php
class Model_customers extends CI_Model{
	public function get_all_customer(){
		$sql="select * from customer";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function customers_processing($params,$fg){
		$columns = array( 
			0 =>'customer.name', 
			1 =>'customer.id', 
			3 => 'customer.name',
			4 => 'customer.gender',
			5 => 'customer.email',
			6 => 'customer.mobile',
		);
		$var_to =$params['to_date'];
		$var_from =$params['from_date'];
		
		$where = $sqlTot = $sqlRec = "";
		$customer_status=$params["customer_status"];
		
			if($customer_status=="active"){
				$str="(customer.active='1')";
			}
			if($customer_status=="inactive"){
				$str="(customer.active='0')";
			}
			if($customer_status=="all"){
				$str="(customer.active='1' or customer.active='0')";
			}
			
		$sql="SELECT * FROM `customer` where $str";
		
		if(!empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( customer.name LIKE '%".$params['search']['value']."%' )";
		}
		if(!empty($var_from) && !empty($var_to)){
				
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$date = str_replace('/', '-', $var_to);
				$to_date=date('Y-m-d', strtotime($date));
				
				$where .=" AND ";
				$where .="DATE_FORMAT(customer.timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(customer.timestamp,'%Y-%m-%d') <= '$to_date'";
				
			}elseif(!empty($var_from) && empty($var_to)){
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				
				$where .=" AND ";
				$where .="DATE_FORMAT(customer.timestamp,'%Y-%m-%d') <=> '$from_date'";
				
			}
			else{
				$where .="";
			}
			
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by customer.timestamp desc";
		//echo $sqlRec;
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}		
	}

	
	public function set_view_customer($customer_id)
	{
		$sql = "UPDATE `customer` SET view='1' where id='$customer_id'";
		$result=$this->db->query($sql);
		return $result;
		
	}
	public function customers_count(){
		$sql="select count(*) as count from customer";
		$query=$this->db->query($sql);
		return $query->row()->count;
	}
	public function get_data_from_customer($customer_id){
		$sql="select * from additional_customer_info where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			$sql="select customer.*,additional_customer_info.* from customer,additional_customer_info where additional_customer_info.customer_id=customer.id and customer.id='$customer_id'";	
		}
		else{
			$sql="select * from customer where id='$customer_id'";	
		}
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row();
		}
		else{
			return false;
		}
		
	}
	public function get_additional_customer_info($customer_id){
		$sql="select * from additional_customer_info where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		
		if($result->num_rows()>0){
			return $result->row_array()["skill_material"];
			
		}
		else{
			return false;
		}
	}
	public function get_skill_material_info($sk_mat){
		$sql="select customer_add_profile_name.profile_name,customer_add_profile_values.profile_value from customer_add_profile_name,customer_add_profile_values where customer_add_profile_values.addl_profile_name_id=customer_add_profile_name.id and customer_add_profile_values.profile_value_id='$sk_mat'";
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	public function get_all_customer_addresses($customer_id){
		$sql="select * from customer_address where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		return $result->result();
	}
	public function get_all_products_from_wishlist($customer_id)
	{
		$sql="select wishlist.*,inventory.sku_name from wishlist,inventory where wishlist.inventory_id=inventory.id and wishlist.customer_id='$customer_id'";
		$res=$this->db->query($sql);
		return $res->result();
	}
	public function get_customer_reviews_and_ratings($customer_id){
		$sql="select rated_inventory.*,inventory.id,inventory.sku_name,products.product_name,inventory.thumbnail,inventory.selling_price from  rated_inventory,inventory,products where inventory.product_id=products.product_id and inventory.id=rated_inventory.inventory_id and rated_inventory.customer_id='$customer_id'";
		$res=$this->db->query($sql);
		return $res->result();
	}
	public function get_customer_all_bank($customer_id){
		$sql="select distinct account_number from refund_bank_details where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		return $result->result();
	}
	public function get_bank_detail($customer_id,$account_number){
		$sql="select * from refund_bank_details where customer_id='$customer_id' and account_number='$account_number'";
		$result=$this->db->query($sql);
		return $result->result();
	}
	public function get_customer_all_cards($customer_id){
		$sql="select * from cards where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		return $result->result();
	}
	public function get_customer_wallet_summary($customer_id){
		$sql="select * from wallet where customer_id='$customer_id' ";
		$result=$this->db->query($sql);
		return $result->result();
	}
	public function get_wallet_transaction_details($customer_id){
		
		$sql="select wallet.*,wallet_transaction.* from wallet,wallet_transaction where wallet_transaction.wallet_id=wallet.wallet_id and wallet.customer_id='$customer_id' ";
		$result=$this->db->query($sql);
		return $result->result();
		
	}
	
	public function get_customer_skills_info($skills_arr){
		$skills_in="'".implode("','",$skills_arr)."'";
		$sql="select * from customer_skills where skill_id in ($skills_in)";
		$query =$this->db->query($sql);
		return $query->result_array();
	}
	public function customer_prefer_material_info($materials_arr){
		$materials_in="'".implode("','",$materials_arr)."'";
		$sql="select * from customer_preferred_material where material_id in ($materials_in)";
		$query =$this->db->query($sql);
		return $query->result_array();
	}
	public function get_all_orders($customer_id){
		$sql="select * from orders_status where customer_id='$customer_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	public function get_all_distinct_orders_id($customer_id){
		$order_id=array();
		$sql="select distinct order_id from active_orders where customer_id='$customer_id' order by timestamp desc";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			foreach($result->result() as $res){
				$order_id[]=$res->order_id;
			}
		}
		$sql="select distinct order_id from cancelled_orders where customer_id='$customer_id' order by timestamp desc";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			foreach($result->result() as $res){
				$order_id[]=$res->order_id;
			}
		}
		$sql="select distinct order_id from completed_orders where customer_id='$customer_id' order by timestamp desc";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			foreach($result->result() as $res){
				$order_id[]=$res->order_id;
			}
		}
		
		$sql="select distinct order_id from returned_orders where customer_id='$customer_id' order by timestamp desc";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			foreach($result->result() as $res){
				$order_id[]=$res->order_id;
			}
		}
		
		return $order_id;
	}
	public function all_orders($order_id){
		//$order_item_id_in="'".implode("','",$order_item_id_arr)."'";
		$sql="select order_id,sku_id,order_item_id,product_id,vendor_id,grandtotal,timestamp,product_price,shipping_charge,'active' as status from active_orders where  order_id='$order_id' union select order_id,sku_id,order_item_id,product_id,vendor_id,grandtotal,timestamp,product_price,shipping_charge,'cancelled' as status from cancelled_orders where  order_id='$order_id' union select order_id,sku_id,order_item_id,product_id,vendor_id,grandtotal,timestamp,product_price,shipping_charge,'completed' as status from completed_orders where order_id='$order_id'  union select order_id,sku_id,order_item_id,product_id,vendor_id,grandtotal,timestamp,product_price,shipping_charge,'returned' as status from  returned_orders where  order_id='$order_id' ";
		
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
		
	}
	public function all_shipped_address($shipping_address_id){
		
		$sql="select * from shipping_address where  shipping_address_id='$shipping_address_id' ";
		
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
		
	}
	public function get_all_order_items($id,$order_id){
		$sql="select *,'active' as status from active_orders where order_id='$order_id' and order_item_id='$id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			$sql="select *,'cancelled' as status from cancelled_orders where order_id='$order_id' and  order_item_id='$id'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->result_array();
			}
			else{
				$sql="select *,'completed' as status from completed_orders where order_id='$order_id' and  order_item_id='$id'";
				$result=$this->db->query($sql);
				if($result->num_rows()>0){
					return $result->result_array();
				}
				else{
					$sql="select *,'returned' as status from returned_orders where order_id='$order_id' and  order_item_id='$id'";
					$result=$this->db->query($sql);
					if($result->num_rows()>0){
						return $result->result_array();
					}
					else{
						return false;
					}
				}
			}
		}
	}
	public function get_order_placed_date($id){
		$sql="select timestamp from active_orders where order_id='$id' union select timestamp from cancelled_orders where order_id='$id' union select timestamp from completed_orders where order_id='$id' union select timestamp from returned_orders where order_id='$id'";
		$result=$this->db->query($sql);
		return $result->row()->timestamp;
	}
	public function get_order_grand_total($id){
		$sql="select grandtotal from active_orders where order_id='$id' union select grandtotal from cancelled_orders where order_id='$id' union select grandtotal from completed_orders where order_id='$id' union select grandtotal from returned_orders where order_id='$id'";
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	public function get_product_name($product_id){
		$sql="select product_name from products where product_id='$product_id'";
		$result=$this->db->query($sql);
		return $result->row()->product_name;
	}
	public function get_in_completed_table_or_not($order_item_id){
		$sql="select count(*) as count from completed_orders where order_item_id ='$order_item_id'";
		$query =$this->db->query($sql);
		if($query->row()->count==0){
			$completed_table="no";
		}
		else{
			$completed_table="yes";
		}
		return $completed_table;
	}
	public function get_in_returned_table_or_not($order_item_id){
		$sql="select count(*) as count from returned_orders where order_item_id ='$order_item_id'";
		$query =$this->db->query($sql);
		if($query->row()->count==0){
			$returned_table="no";
		}
		else{
			$returned_table="yes";
		}
		return $returned_table;
	}
	
	public function get_in_replaced_table_or_not($order_item_id){
		$sql="select count(*) as count from replacement_desired where order_item_id ='$order_item_id'";
		$query =$this->db->query($sql);
		if($query->row()->count==0){
			$replaced_table="no";
		}
		else{
			$replaced_table="yes";
		}
		return $replaced_table;
	}
	public function get_return_refund_table_by_order_item_id($order_item_id){
		$sql="select * from return_refund where order_item_id ='$order_item_id'";
		$query =$this->db->query($sql);
		return $query->row_array();
	}
	public function get_return_replacement_table_by_order_item_id($order_item_id){
		$sql="select * from order_replacement_decision where order_item_id ='$order_item_id'";
		$query =$this->db->query($sql);
		return $query->row_array();
	}
	public function get_return_replacement_desired_order_item_id($order_item_id){
		$sql="select * from replacement_desired where order_item_id ='$order_item_id'";
		$query =$this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_order_item_status_summary($id){
		$sql="select 
		order_placed,
		order_placed_timestamp,
		order_confirmed,
		order_confirmed_timestamp,	
		order_packed,
		order_packed_timestamp,
		order_shipped,
		order_shipped_timestamp,
		order_delivered,
        order_delivered_timestamp,
		order_cancelled,
		order_cancelled_timestamp,
		order_delayed,
		order_delayed_timestamp,
		return_order_id,
		order_return_pickup,
		order_replacement_pickup,
		timestamp
				from orders_status where order_item_id='$id'";
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	public function de_activate_customer($id,$deactivate_reason,$comments,$block_user){
		
		if($block_user){
			$blocked='yes';
		}
		else{
			$blocked='no';
		}
		$comments=$this->db->escape_str($comments);
		$sql="insert into deactivated_history_admin (customer_id,deactivate_reason,deactivate_comments,block_user,blocked) values('$id','$deactivate_reason','$comments','$block_user','$blocked')";
		$result=$this->db->query($sql);
		
		$sql="update customer set active='0' where id='$id'";
		$result=$this->db->query($sql);
		return $result;
		
	}
	public function de_activate_customer_acc($id){
		
		$sql="update customer set active='0' where id='$id'";
		$result=$this->db->query($sql);
		return $result;
	}
	public function activate_customer($id,$reactivate_comments){
		$reactivate_comments=$this->db->escape_str($reactivate_comments);
		$sql="update deactivated_history_admin set reactivate_comments='$reactivate_comments',block_user='0' where customer_id='$id' order by id desc limit 1 ";
		$result=$this->db->query($sql);
		
		$sql="update customer set active='1' where id='$id'";
		$result=$this->db->query($sql);
		return $result;
		
		 
	}

	public function deactivate_history_check($id){
		$sql="Select * from deactivated_history_admin where customer_id='$id' and block_user='1' order by id desc limit 1";
		$query =$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
		
				
	}
	public function get_service_ratings($customer_id){
		
		$sql="select rateorder.*,completed_orders.*,completed_orders.timestamp as delivered_timestamp,rateorder.timestamp as rateorder_timestamp, customer.id,customer.name as original_customer,shipping_address.*,shipping_address.address1 as shipped_address1,shipping_address.address2 as shipped_address2,shipping_address.country as shipped_country,shipping_address.state as shipped_state,shipping_address.city as shipped_city,shipping_address.pincode as shipped_pincode,shipping_address.pincode as shipped_pincode,shipping_address.mobile as shipped_mobile,logistics.*,vendors.*,inventory.id,inventory.sku_name,inventory.sku_id,products.product_id,products.product_name,products.brand_id,products.subcat_id,products.cat_id,products.pcat_id,orders_status.order_item_id,orders_status.order_delivered,orders_status.order_delivered_timestamp from rateorder,completed_orders,customer,shipping_address,logistics,vendors,inventory,products,orders_status  where rateorder.order_item_id=completed_orders.order_item_id and rateorder.order_item_id=orders_status.order_item_id and completed_orders.inventory_id=inventory.id and products.product_id=inventory.product_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and completed_orders.customer_id=customer.id and vendors.vendor_id=completed_orders.vendor_id and lower(replace(completed_orders.logistics_name, ' ', ''))=(replace(logistics.logistics_name, ' ', '')) and customer.id='$customer_id'";
		
		$queryRecords=$this->db->query($sql);
		return $queryRecords->result();
	}
	
	public function get_wallet_transaction_table_by_wallet_transaction_bank($wallet_id,$customer_id){
		$sql="select wallet_transaction.* from wallet_transaction where wallet_transaction.claim_status='1' and wallet_transaction.credit_bank_transfer='2' and wallet_transaction.customer_id='$customer_id' and wallet_transaction.wallet_id='$wallet_id'";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	public function customer_bank_transfer_processing($params,$fg){
		
		$var_to =$params['to_date'];
		$var_from =$params['from_date'];
		
		//define index of column
		$columns = array( 
			0 =>'wallet_transaction_bank.id', 
			1 => 'wallet_transaction_bank.wallet_id',
			2 => 'wallet_transaction_bank.transaction_amount'
		);

		$where = $sqlTot = $sqlRec = "";
		$customer_id=$this->session->userdata('customer_id');
		
		$status=$params["status"];
		
			if($status=="reject"){
				$str="(wallet_transaction_bank.status='reject')";
			}
			if($status=="accept"){
				$str="(wallet_transaction_bank.status='accept')";
			}
			if($status=="success"){
				$str="(wallet_transaction_bank.status='success')";
			}
			if($status=="failure"){
				$str="(wallet_transaction_bank.status='failure')";
			}
			if($status=="pending"){
				$str="(wallet_transaction_bank.status='pending')";
			}
			if($status=="all_status"){
				$str="(wallet_transaction_bank.status='reject' or wallet_transaction_bank.status='accept' or wallet_transaction_bank.status='success' or wallet_transaction_bank.status='failure' or wallet_transaction_bank.status='pending')";
			}
		// getting total number records without any search
	
		$sql = "SELECT customer.name,wallet_transaction_bank.*
		FROM `wallet_transaction_bank`,customer where customer.id=wallet_transaction_bank.customer_id  and $str";
		// check search value exist
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( wallet_transaction_bank.id LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.wallet_id LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.transaction_amount LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.bank_name LIKE '%".$params['search']['value']."%' ";
			$where .=" OR customer.name LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.ifsc_code LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.city LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.branch_name LIKE '%".$params['search']['value']."%' )";
		}
		if(!empty($var_from) && !empty($var_to)){
				
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$date = str_replace('/', '-', $var_to);
				$to_date=date('Y-m-d', strtotime($date));
				
				$where .=" AND ";
				if($status=="success"){
				$where .="DATE_FORMAT(wallet_transaction_bank.success_timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(wallet_transaction_bank.success_timestamp,'%Y-%m-%d') <= '$to_date'";
				}
				if($status=="reject"){
					
				$where .="DATE_FORMAT(wallet_transaction_bank.reject_timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(wallet_transaction_bank.reject_timestamp,'%Y-%m-%d') <= '$to_date'";
				}
				if($status=="all_status"){
				$where .="DATE_FORMAT(wallet_transaction_bank.timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(wallet_transaction_bank.timestamp,'%Y-%m-%d') <= '$to_date'";
				}
			}elseif(!empty($var_from) && empty($var_to)){
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$where .=" AND ";
				if($status=="success"){
				$where .="DATE_FORMAT(wallet_transaction_bank.success_timestamp,'%Y-%m-%d') <=> '$from_date'";
				}
				if($status=="reject"){
				$where .="DATE_FORMAT(wallet_transaction_bank.reject_timestamp,'%Y-%m-%d') <=> '$from_date'";
				}
				if($status=="all_status"){
				$where .="DATE_FORMAT(wallet_transaction_bank.timestamp,'%Y-%m-%d') <=> '$from_date'";
				}
			}
			else{
				$where .="";
			}
			
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}

		
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
	}
	public function wallet_transaction_bank_accept($wallet_transaction_bank_id,$accepted_transaction_amount,$accepted_transaction_comments){
		/* transaction amount is overridden */
		$sql="update wallet_transaction_bank set status='accept',notify_bank_transfer='1',accepted_transaction_amount='$accepted_transaction_amount',transaction_amount='$accepted_transaction_amount',accepted_transaction_comments='$accepted_transaction_comments' where id='$wallet_transaction_bank_id' ";
		$query=$this->db->query($sql);
		return $query;
	}
	
	public function wallet_transaction_bank_reject($wallet_transaction_bank_id,$rejected_comments,$wallet_transaction_id){
		
		$rejected_comments=$this->db->escape_str($rejected_comments);
		$sql="update wallet_transaction_bank set rejected_comments='$rejected_comments',status='reject',reject_timestamp=now(),wallet_transaction_id='$wallet_transaction_id' where id='$wallet_transaction_bank_id'";
		$query=$this->db->query($sql);
		return $query;
	}
	public function update_customer_wallet($wallet_id,$customer_id,$transaction_details,$debit,$credit,$amount,$wallet_type=''){
		
		$credit_bank_transfer=1;
		$sql="insert into wallet_transaction (wallet_id,customer_id,transaction_details,debit,credit,amount,credit_bank_transfer,wallet_type) values('$wallet_id','$customer_id','$transaction_details','$debit','$credit','$amount','$credit_bank_transfer','$wallet_type')";
		$result=$this->db->query($sql);
		$wallet_transaction_id=$this->db->insert_id();
		
		$sql="update wallet set wallet_amount='$amount' where wallet_id='$wallet_id' and customer_id='$customer_id'";
		$result=$this->db->query($sql);
		return $wallet_transaction_id;
			
	}
	public function customer_bank_tranfer($wallet_transaction_bank_id){
		$sql = "SELECT wallet_transaction.transaction_details,wallet_transaction.credit,customer.name,wallet_transaction_bank.*
		FROM `wallet_transaction_bank`,customer,wallet_transaction where customer.id=wallet_transaction_bank.customer_id and wallet_transaction.wallet_id=wallet_transaction_bank.wallet_id and wallet_transaction.customer_id=wallet_transaction_bank.customer_id and wallet_transaction_bank.status='pending' and wallet_transaction_bank.id='$wallet_transaction_bank_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	
	public function customer_bank_tranfer_accounts_incoming($wallet_transaction_bank_id){
		$sql = "SELECT wallet_transaction.transaction_details,wallet_transaction.credit,customer.name,wallet_transaction_bank.*
		FROM `wallet_transaction_bank`,customer,wallet_transaction where customer.id=wallet_transaction_bank.customer_id and wallet_transaction.wallet_id=wallet_transaction_bank.wallet_id and wallet_transaction.customer_id=wallet_transaction_bank.customer_id and wallet_transaction_bank.status='accept' and wallet_transaction.claim_status='1' and wallet_transaction.credit_bank_transfer='2' and wallet_transaction_bank.id='$wallet_transaction_bank_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	
	public function customer_bank_tranfer_accounts_incoming_processing($params,$fg){
		//define index of column
		$columns = array( 
			0 =>'wallet_transaction_bank.id', 
			1 => 'wallet_transaction_bank.wallet_id',
			2 => 'wallet_transaction_bank.transaction_amount'
		);

		$where = $sqlTot = $sqlRec = "";
		$customer_id=$this->session->userdata('customer_id');
		// getting total number records without any search
		/*$sql = "SELECT wallet_transaction.transaction_details,wallet_transaction.credit,customer.name,wallet_transaction_bank.*
		FROM `wallet_transaction_bank`,customer,wallet_transaction where customer.id=wallet_transaction_bank.customer_id and wallet_transaction.wallet_id=wallet_transaction_bank.wallet_id and wallet_transaction.customer_id=wallet_transaction_bank.customer_id and wallet_transaction_bank.status='accept' and wallet_transaction.claim_status='1' and wallet_transaction.credit_bank_transfer='2'";*/
		$sql = "SELECT customer.name,wallet_transaction_bank.*
		FROM `wallet_transaction_bank`,customer where customer.id=wallet_transaction_bank.customer_id and wallet_transaction_bank.status='accept'";
		// check search value exist
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( wallet_transaction_bank.id LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.wallet_id LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.transaction_amount LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.bank_name LIKE '%".$params['search']['value']."%' ";
			$where .=" OR customer.name LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.ifsc_code LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.city LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.branch_name LIKE '%".$params['search']['value']."%' )";
		}
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}

		
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
	}
	
	public function get_customer_wallet_summary_for_wallet_bank($customer_id){
		$sql="select * from wallet where customer_id='$customer_id' ";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	
	public function wallet_transaction_bank_success($wallet_transaction_bank_id,$transaction_id,$transaction_date,$transaction_time,$customer_id,$transaction_amount){
		
		/* now only customer wallet needs to be debited */
		
		// $wallet_summary=$this->get_customer_wallet_summary_for_wallet_bank($customer_id);
		// foreach($wallet_summary as $summary){
		// 	$wallet_id=$summary['wallet_id'];
		// 	$amount=$summary['wallet_amount'];
		// }
		
		// $transaction_details="Transferring Wallet Money to Bank";
		// $wallet_type='Debit from Wallet - Bank Transfer is Successful';
		// $debit=$transaction_amount;
		// $credit=0;
		// $amount=$amount-$debit;
		// $wallet_transaction_id=$this->update_customer_wallet($wallet_id,$customer_id,$transaction_details,$debit,$credit,$amount,$wallet_type);
		//$wallet_transaction_id='';,wallet_transaction_id='$wallet_transaction_id'
		/* now only customer wallet needs to be debited */

		$sql="update wallet_transaction_bank set transaction_id='$transaction_id',transaction_date='$transaction_date',transaction_time='$transaction_time',status='success',success_timestamp=now() where id='$wallet_transaction_bank_id'";

		$query=$this->db->query($sql);
		return $query;
	}
	
	public function customer_bank_tranfer_accounts_success_processing($params,$fg){
		//define index of column
		$columns = array( 
			0 =>'wallet_transaction_bank.id', 
			1 => 'wallet_transaction_bank.wallet_id',
			2 => 'wallet_transaction_bank.transaction_amount'
		);

		$where = $sqlTot = $sqlRec = "";
		$customer_id=$this->session->userdata('customer_id');
		// getting total number records without any search
		/*$sql = "SELECT wallet_transaction.transaction_details,wallet_transaction.credit,customer.name,wallet_transaction_bank.*
		FROM `wallet_transaction_bank`,customer,wallet_transaction where customer.id=wallet_transaction_bank.customer_id and wallet_transaction.wallet_id=wallet_transaction_bank.wallet_id and wallet_transaction.customer_id=wallet_transaction_bank.customer_id and wallet_transaction_bank.status='success'  and wallet_transaction.claim_status='1' and wallet_transaction.credit_bank_transfer='2'";*/
		
		$sql = "SELECT customer.name,wallet_transaction_bank.*
		FROM `wallet_transaction_bank`,customer where customer.id=wallet_transaction_bank.customer_id  and wallet_transaction_bank.status='success'";
		
		// check search value exist
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( wallet_transaction_bank.id LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.wallet_id LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.transaction_amount LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.bank_name LIKE '%".$params['search']['value']."%' ";
			$where .=" OR customer.name LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.ifsc_code LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.city LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.branch_name LIKE '%".$params['search']['value']."%' )";
		}
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}

		
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
	}
	
	public function wallet_transaction_bank_failure($wallet_transaction_bank_id,$refund_fail_comments,$allow_update_bank_details){
		if($allow_update_bank_details){
				$allow_update_bank_details="yes";
			}
			else{
				$allow_update_bank_details="no";
			}
		$refund_fail_comments=$this->db->escape_str($refund_fail_comments);	
			
		$sql="update wallet_transaction_bank set status='failure',refund_fail_comments='$refund_fail_comments',allow_update_bank_details='$allow_update_bank_details' where id='$wallet_transaction_bank_id'";
		$query=$this->db->query($sql);
		return $query;
	}
	
	public function customer_bank_tranfer_accounts_failure_processing($params,$fg){
		//define index of column
		$columns = array( 
			0 =>'wallet_transaction_bank.id', 
			1 => 'wallet_transaction_bank.wallet_id',
			2 => 'wallet_transaction_bank.transaction_amount'
		);

		$where = $sqlTot = $sqlRec = "";
		$customer_id=$this->session->userdata('customer_id');
		// getting total number records without any search
		/*$sql = "SELECT wallet_transaction.transaction_details,wallet_transaction.credit,customer.name,wallet_transaction_bank.*
		FROM `wallet_transaction_bank`,customer,wallet_transaction where customer.id=wallet_transaction_bank.customer_id and wallet_transaction.wallet_id=wallet_transaction_bank.wallet_id and wallet_transaction.customer_id=wallet_transaction_bank.customer_id and wallet_transaction_bank.status='failure'  and wallet_transaction.claim_status='1' and wallet_transaction.credit_bank_transfer='2'";*/
		
			$sql = "SELECT customer.name,wallet_transaction_bank.*
		FROM `wallet_transaction_bank`,customer where customer.id=wallet_transaction_bank.customer_id  and wallet_transaction_bank.status='failure'";
		
		// check search value exist
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( wallet_transaction_bank.id LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.wallet_id LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.transaction_amount LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.bank_name LIKE '%".$params['search']['value']."%' ";
			$where .=" OR customer.name LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.ifsc_code LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.city LIKE '%".$params['search']['value']."%' ";
			$where .=" OR wallet_transaction_bank.branch_name LIKE '%".$params['search']['value']."%' )";
		}
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}

		
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
	}
	public function returns_replacement_request_orders_accounts_success($order_item_id){
			$sql = "select history_orders.sku_id,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.payment_status,replacement_desired.shipping_charge_applied_cancels_chk,replacement_desired.return_shipping_concession_cancels_chk,order_replacement_decision.refund_status from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return,order_replacement_decision where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and 
			order_replacement_decision.order_item_id = replacement_desired.order_item_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and ((replacement_desired.order_replacement_decision_status='refunded' or replacement_desired.order_replacement_decision_refund_status='refunded')  and (replacement_desired.amount_paid!=0 or order_replacement_decision.return_shipping_concession!=0) and (replacement_desired.status_of_refund='yes')) and replacement_desired.order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
	
	public function customer_profile_processing($params,$fg){
		$columns = array( 
			0 =>'customer_add_profile_name.id', 
			1 =>'customer_add_profile_name.profile_name', 
			2 => 'customer_add_profile_name.sort_order',
			4 => 'customer_add_profile_name.timestamp',

		);
		
		$where = $sqlTot = $sqlRec = "";
		$sql = "SELECT * FROM customer_add_profile_name ";
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( customer_add_profile_name.profile_name LIKE '%".$params['search']['value']."%' ";
			
		}

		//$where .=" AND ";
		//$where .="vendors.vendor_id='$vendor_id'";
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by customer_add_profile_name.timestamp desc";

		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
	}
	
	public function customer_add_profile_name(){
		$query = $this->db->query("SELECT * FROM `customer_add_profile_name` ");
		return $query -> result();
	}
	
	public function customer_add_profile_value() {
		$query = $this->db->query("SELECT customer_add_profile_value.*,customer_add_profile_name.id,customer_add_profile_name.profile_name FROM customer_add_profile_value,customer_add_profile_name where vendors.vendor_id=customer_add_profile_value.id and vendors.active=1");
		return $query -> result();
	}
	
	public function add_customer_profile($profile_name,$sort_order,$view){
		$insertinfo = array("profile_name" => $profile_name,"sort_order" => $sort_order, "view" => $view);
		$flag=$this -> db -> insert("customer_add_profile_name", $insertinfo);
		return $flag;
	}
	
	public function get_customer_profile_data($id){
		
		$query =$this->db->query("SELECT * FROM customer_add_profile_name Where id='$id'");
		return $query->row_array();		
	}
	
	public function delete_customer_profile_selected($selected_list){
		$sql_deletename="delete from customer_add_profile_name Where id in ($selected_list)";
		$flag1=$this->db->query($sql_deletename);
		$sql_deletevalue="delete from customer_add_profile_values Where addl_profile_name_id in ($selected_list)";
		$flag2=$this->db->query($sql_deletevalue);
		return $flag1 && $flag2;
		
	}
	
	public function edit_customer_profile($profile_name,$sort_order,$view,$id)
	{
		$sql = "UPDATE `customer_add_profile_name` SET profile_name='$profile_name',sort_order='$sort_order',view='$view' WHERE `id` = '$id'";
		
		$result=$this->db->query($sql);
		return $result;
		
		
	}
	
	public function customer_profile_values_processing($params,$fg){
		$columns = array( 
			0 =>'customer_add_profile_values.profile_value_id', 
			1 =>'customer_add_profile_values.profile_value', 
			2 => 'customer_add_profile_values.sort_order',
			4 => 'customer_add_profile_values.timestamp',

		);
		$profile_id =$params['profile_id'];
		$where = $sqlTot = $sqlRec = "";
		$sql = "SELECT customer_add_profile_values.*,customer_add_profile_name.id,customer_add_profile_name.profile_name FROM customer_add_profile_name,customer_add_profile_values where customer_add_profile_name.id=customer_add_profile_values.addl_profile_name_id";
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( customer_add_profile_values.profile_value LIKE '%".$params['search']['value']."%' ";
			$where .="  OR customer_add_profile_name.profile_name LIKE '%".$params['search']['value']."%' )";
		}

		$where .=" AND ";
		$where .="customer_add_profile_name.id='$profile_id'";
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by customer_add_profile_values.timestamp desc";

		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
	}
	
	public function add_customer_profile_value($profile_value,$create_sort_order,$view,$profile_name_id){
		
		$insertinfo = array("profile_value" => $profile_value,"sort_order" => $create_sort_order,"view" => $view,"addl_profile_name_id"=>$profile_name_id);
		$flag=$this -> db -> insert("customer_add_profile_values", $insertinfo);
		return $flag;
		
	}
	
	public function get_customer_profile_value_data($profile_value_id){
		
		$query =$this->db->query("SELECT customer_add_profile_values.*,customer_add_profile_name.id as add_profile_name_id,customer_add_profile_name.profile_name FROM customer_add_profile_values,customer_add_profile_name Where customer_add_profile_values.profile_value_id='$profile_value_id' and customer_add_profile_values.addl_profile_name_id=customer_add_profile_name.id");
		return $query->row_array();		
	}
	
	public function edit_customer_profile_value($edit_profile_value,$edit_sort_order,$id,$view,$edit_profile_name)
	{
		$sql = "UPDATE `customer_add_profile_values` SET profile_value='$edit_profile_value',sort_order='$edit_sort_order',view='$view' WHERE `profile_value_id` = '$id'";
		
		$result=$this->db->query($sql);
		
		return $result;
		
	}
	
	public function delete_customer_profile_values_selected($selected_list){
		$sql_deletepcat="delete from customer_add_profile_values Where profile_value_id in ($selected_list)";
		$flag1=$this->db->query($sql_deletepcat);
		return $flag1;
		
	}
	public function deactivate_reason(){
		$sql="select * from customer_deactivate_reason";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function deactivate_history_details($customer_id){
		$sql="select * from deactivated_history_admin where customer_id='$customer_id' order by timestamp desc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function requests_count(){
		$sql="select count(*) as count from wallet_transaction_bank where view=0 ";
		$query=$this->db->query($sql);
		
		if($query->row()->count>0){
			return 'yes';
		}else{
			return 'no';
		}
	}
	
	public function update_wallet_bank_transfer_view(){
		$view_0_arr=array();
		$view_0_arr_in="";
		$sql="select id from wallet_transaction_bank where view='0'";
		$query=$this->db->query($sql);
		foreach($query->result_array() as $query_arr){
			$view_0_arr[]=$query_arr["id"];
		}
		if(count($view_0_arr)>0){
			$view_0_arr_in="'".implode("','",$view_0_arr)."'";
		}
		if($view_0_arr_in!=""){
		$sql="update wallet_transaction_bank set view='1' where id in ($view_0_arr_in)";
		$result=$this->db->query($sql);
		return $result;
		}
		
	}
	public function notify_account_bank_transfer_unset(){
		$sql="update wallet_transaction_bank set notify_bank_transfer='0' order by id desc limit 1000";
		$result=$this->db->query($sql);
	}
	public function design_request_processing($params,$fg){
		$columns = array( 
			0 =>'customer_design_request.name', 
			1 =>'customer_design_request.name', 
			2 =>'customer_design_request.email', 
			3 => 'customer_design_request.mobile',
			4 => 'customer_design_request.timestamp',
			5 => 'customer_design_request.status_updated_on',
			
		);
		$var_to =$params['to_date'];
		$var_from =$params['from_date'];
		$design_type =$params['design_type'];
		
		$where = $sqlTot = $sqlRec = "";
		
		$str=' 1=1 ';
		$sql="SELECT * FROM `customer_design_request` where $str";
		if(!empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( customer_design_request.name LIKE '%".$params['search']['value']."%' )";
		}
		if(!empty($var_from) && !empty($var_to)){
				
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$date = str_replace('/', '-', $var_to);
				$to_date=date('Y-m-d', strtotime($date));
				
				$where .=" AND ";
				$where .="DATE_FORMAT(customer_design_request.timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(customer_design_request.timestamp,'%Y-%m-%d') <= '$to_date'";
				
			}elseif(!empty($var_from) && empty($var_to)){
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				
				$where .=" AND ";
				$where .="DATE_FORMAT(customer_design_request.timestamp,'%Y-%m-%d') <=> '$from_date'";
				
			}
			else{
				$where .="";
			}
			
			if($design_type != ""){
				$where .=" AND ";
				$where .=" customer_design_request.design_type = '$design_type'";
				
			}
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}

		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by customer.timestamp desc";
		//echo $sqlRec;
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}		
	}
	public function update_status_request_design($request_id,$status,$contacted_status,$contacted_comments){
		
		echo $sql="update customer_design_request set status='$status',contacted_status='$contacted_status',contacted_comments='$contacted_comments' where request_id='$request_id'";
		$query=$this->db->query($sql);
		return $query;
	}
	public function parent_category() {
		$query = $this->db->query("SELECT * FROM `parent_category` where active=1");
		return $query -> result();
	}
}
?>