<?php
class Site_newsletter extends CI_model{

    public function all_news_letter_processing($params,$fg){
        //define index of column
        $columns = array( 
            0 =>'newsletter.email_address', 
            1 => 'newsletter.subscribed',
            2 => 'newsletter.timestamp',
        );

        $where = $sqlTot = $sqlRec = "";
        $customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT newsletter.email_address,date_format(newsletter.timestamp,'%a,%e %b %Y') as timestamp, 
                  case 
                    when newsletter.subscribed = 1 then 'Subscribed' 
                    when newsletter.subscribed = 0 then 'Un Subscribed' 
                  end as subscribed  
              from  newsletter where 1=1 ";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" (  date_format(newsletter.timestamp,'%a,%e %b %Y')  LIKE '%".$params['search']['value']."%' ";
            $where .="  or case 
                    when newsletter.subscribed = 1 then 'Subscribed' 
                    when newsletter.subscribed = 0 then 'Un Subscribed' 
                  end  LIKE '%".addslashes($params['search']['value'])."%' ";
            $where .="  or newsletter.email_address LIKE '%".addslashes($params['search']['value'])."%' )";
            //$where .="  blog.villa_type LIKE '%".addslashes($params['search']['value'])."%' ";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            //echo $sqlTot; exit;
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
           // echo $sqlRec; exit;
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }
    
}
?>