<?php
	class Policys_manage_data extends CI_Model{
	    public function get_policy_type($policy_type_id){
	        $sql="select name from policys_list where id='$policy_type_id'";
            $result=$this->db->query($sql);
            if($result->num_rows()>0){
                return $result->row()->name;
            }
	    }
	    public function get_all_policy_list(){
	        $sql="select * from policys_list where active=1";
            $result=$this->db->query($sql);
            if($result->num_rows()>0){
                return $result->result_array();
            }
            else{
                return false;
            }
	    }
        public function get_all_payment_policy_attributes($id){
            $sql="select * from policy_attributes where policys_list_id='$id'";
            $result=$this->db->query($sql);
            if($result->num_rows()>0){
                return $result->result_array();
            }
            else{
                return false;
            }
        }
        public function details_for_policy_payment_restriction($policy_uid,$message,$max_val,$min_val,$currency_type,$payment_method,$name_of_products_tagged_for_purchasing,$cod_restrictions){
			$message=$this->db->escape_str($message);
            $sql="insert into payment_policy (payment_policy_uid,message,max_value,min_value,currency_type,attributes,item_level,cod_restrictions) values('$policy_uid','$message','$max_val','$min_val','$currency_type','$payment_method','".$this->db->escape_str($name_of_products_tagged_for_purchasing)."','$cod_restrictions')";
            $result=$this->db->query($sql);
            if($this->db->affected_rows()){
                return true;
            }
            else{
                return false;
            }
        }
        public function add_items_tagged_for_policy_restriction($policy_type_id,$policy_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector){
            
            $inv_id_arr=explode(',',$products_tagged_for_purchasing);
                    $success=0;
                    foreach($inv_id_arr as $inv_id){
                        $sqlen="select * from policy_items where inv_id='$inv_id' and policy_uid='$policy_uid' and policy_type='$policy_type_id'";
                        $resultenq=$this->db->query($sqlen);
                        if($resultenq->num_rows()==0){
                               $sql="insert into policy_items (policy_type,policy_uid,pcat_id,cat_id,subcat_id,brand_id,product_id,inv_id) values('$policy_type_id','$policy_uid','$parent_category','$category','$sub_category','$attributes','$attributes_selector','$inv_id')";
                    $result=$this->db->query($sql);
                       }
                    }
    
            if($this->db->affected_rows()){
                return true;
            }
            else{
                return false;
            }
        }
        
        public function update_for_policy_payment_restriction($policy_uid,$message,$max_val,$min_val,$currency_type,$payment_method,$name_of_products_tagged_for_purchasing,$cod_restrictions){
			$message=$this->db->escape_str($message);
            $sql="update payment_policy set message='$message',max_value='$max_val',min_value='$min_val',currency_type='$currency_type',attributes='$payment_method',item_level='".$this->db->escape_str($name_of_products_tagged_for_purchasing)."',cod_restrictions='$cod_restrictions' where payment_policy_uid='$policy_uid'";

            $result=$this->db->query($sql);
            if($this->db->affected_rows()){
                return true;
            }
            else{
                return true;
            }
        }
        public function update_items_tagged_for_policy_restriction($policy_type_id,$policy_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector){
            $sql="delete from policy_items where policy_uid='$policy_uid' and policy_type='$policy_type_id'";
            $result=$this->db->query($sql);
            $inv_id_arr=explode(',',$products_tagged_for_purchasing);
                    $success=0;
                    foreach($inv_id_arr as $inv_id){
                        $sqlen="select * from policy_items where inv_id='$inv_id' and policy_uid='$policy_uid' and policy_type='$policy_type_id'";
                        $resultenq=$this->db->query($sqlen);
                        if($resultenq->num_rows()==0){
                               $sql="insert into policy_items (policy_type,policy_uid,pcat_id,cat_id,subcat_id,brand_id,product_id,inv_id) values('$policy_type_id','$policy_uid','$parent_category','$category','$sub_category','$attributes','$attributes_selector','$inv_id')";
                    $result=$this->db->query($sql);
                       }
                    }
    
            if($this->db->affected_rows()){
                return true;
            }
            else{
                return false;
            }
        }

        public function delete_policy_payment($policy_list_id){
            $sql="delete from policy_items where policy_uid='$policy_list_id'";
            $result=$this->db->query($sql);
            $sql="delete from payment_policy where payment_policy_uid='$policy_list_id'";
            $result=$this->db->query($sql);
            if($this->db->affected_rows()){
                return true;
            }
            else{
                return false;
            }
        }
        public function get_all_payment_related_policy(){
            $sql="select * from payment_policy";
            $result=$this->db->query($sql);
            if($result->num_rows()>0){
                return $result->result_array();
            }
            else{
                return false;
            }
        }
        
        public function Payment_processing($params,$fg){
        //define index of column
        $columns = array( 
            0 =>'payment_policy.id', 
            1 => 'payment_policy.payment_policy_uid',
            2 => 'payment_policy.item_level',
            3 => 'payment_policy.message',
            4 => 'payment_policy.max_value',
            5 => 'payment_policy.min_value',
            6 => 'payment_policy.currency_type',
            7 => 'payment_policy.attributes',
            8 => 'payment_policy.timestamp',
            9 => 'payment_policy.active'
        );

        $where = $sqlTot = $sqlRec = "";
        $customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT payment_policy.* FROM `payment_policy`";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" ( payment_policy.id LIKE '%".$params['search']['value']."%' ";
            $where .=" OR payment_policy.payment_policy_uid LIKE '%".$params['search']['value']."%' ";
            $where .=" OR payment_policy.item_level LIKE '%".$params['search']['value']."%' ";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }
    
        public function getDataOfParticularPaymentPolicy($id){
        $sql="select * from payment_policy where payment_policy_uid='$id'";
        $result=$this->db->query($sql);
        return $result->result_array();
    }
        public function getItemsOfParticularPolicy($id){
        $sql="select * from policy_items where policy_uid='$id'";
        $result=$this->db->query($sql);
        return $result->result_array();
    }   
        
        public function details_for_policy_replacement_restriction($policy_uid,$message,$replacement_restrict,$new_days_for_replacement,$name_of_products_tagged_for_purchasing){
			$message=$this->db->escape_str($message);
            $sql="insert into replacement_policy set replacement_policy_uid='$policy_uid',item_level='".$this->db->escape_str($name_of_products_tagged_for_purchasing)."',message='$message',replacement_restrict='$replacement_restrict',new_days_for_replacement='$new_days_for_replacement'";
            $result=$this->db->query($sql);
            if($this->db->affected_rows()){
                return true;
            }else{
                return false;
            }
        }
        
        public function Replacement_processing($params,$fg){
        //define index of column
        $columns = array( 
            0 =>'replacement_policy.id', 
            1 => 'replacement_policy.replacement_policy_uid',
            2 => 'replacement_policy.item_level',
            3 => 'replacement_policy.message',
            4 => 'replacement_policy.replacement_restrict',
            5 => 'replacement_policy.new_days_for_replacement',
            8 => 'replacement_policy.timestamp',
            9 => 'replacement_policy.active'
        );

        $where = $sqlTot = $sqlRec = "";
        //$customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT replacement_policy.* FROM `replacement_policy`";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" ( replacement_policy.id LIKE '%".$params['search']['value']."%' ";
            $where .=" OR replacement_policy.payment_policy_uid LIKE '%".$params['search']['value']."%' ";
            $where .=" OR replacement_policy.item_level LIKE '%".$params['search']['value']."%' ";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }

    public function getDataOfParticularReplacementPolicy($id){
        $sql="select * from replacement_policy where replacement_policy_uid='$id'";
        $result=$this->db->query($sql);
        return $result->result_array(); 
    }
    
    public function update_for_policy_replacement_restriction($policy_uid,$message,$replacement_restrict,$new_days_for_replacement,$name_of_products_tagged_for_purchasing){
		$message=$this->db->escape_str($message);
        $sql="update replacement_policy set item_level='".$this->db->escape_str($name_of_products_tagged_for_purchasing)."',message='$message',replacement_restrict='$replacement_restrict',new_days_for_replacement='$new_days_for_replacement' where replacement_policy_uid='$policy_uid'";

            $result=$this->db->query($sql);
            if($this->db->affected_rows()){
                return true;
            }
            else{
                return true;
            }
        
    }
    public function delete_replacement_payment($policy_list_id){
        $sql="delete from policy_items where policy_uid='$policy_list_id'";
            $result=$this->db->query($sql);
            $sql="delete from replacement_policy where replacement_policy_uid='$policy_list_id'";
            $result=$this->db->query($sql);
            if($this->db->affected_rows()){
                return true;
            }
            else{
                return false;
            }
    }
    
    public function details_for_policy_refund_restriction($policy_uid,$message,$refund_restrict,$new_days_for_refund,$name_of_products_tagged_for_purchasing,$refund_method){
		$message=$this->db->escape_str($message);
        $sql="insert into refund_policy set refund_policy_uid='$policy_uid',item_level='".$this->db->escape_str($name_of_products_tagged_for_purchasing)."',message='$message',refund_restrict='$refund_restrict',new_days_for_refund='$new_days_for_refund',refund_method='$refund_method'";
        $result=$this->db->query($sql);
        if($this->db->affected_rows()){
            return true;
        }else{
            return false;
        }
        
    }
    
    public function Refund_processing($params,$fg){
        //define index of column
        $columns = array( 
            0 =>'refund_policy.id', 
            1 => 'refund_policy.refund_policy_uid',
            2 => 'refund_policy.item_level',
            3 => 'refund_policy.refund_method',
            4 => 'refund_policy.message',
            5 => 'refund_policy.refund_restrict',
            8 => 'refund_policy.new_days_for_refund',
            9 => 'refund_policy.timestamp',
            10 => 'refund_policy.active'
        );

        $where = $sqlTot = $sqlRec = "";
        //$customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT refund_policy.* FROM `refund_policy`";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" ( refund_policy.id LIKE '%".$params['search']['value']."%' ";
            $where .=" OR refund_policy.payment_policy_uid LIKE '%".$params['search']['value']."%' ";
            $where .=" OR refund_policy.item_level LIKE '%".$params['search']['value']."%' ";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }

    public function getDataOfParticularRefundPolicy($id){
        $sql="select * from refund_policy where refund_policy_uid='$id'";
        $result=$this->db->query($sql);
        return $result->result_array(); 
    }
    
    public function update_for_policy_refund_restriction($policy_uid,$message,$refund_restrict,$new_days_for_refund,$name_of_products_tagged_for_purchasing,$refund_method){
		$message=$this->db->escape_str($message);
        $sql="update refund_policy set item_level='".$this->db->escape_str($name_of_products_tagged_for_purchasing)."',message='$message',refund_restrict='$refund_restrict',new_days_for_refund='$new_days_for_refund',refund_method='$refund_method' where refund_policy_uid='$policy_uid'";

            $result=$this->db->query($sql);
            if($this->db->affected_rows()){
                return true;
            }
            else{
                return true;
            }
        
    }
    public function delete_refund_payment($policy_list_id){
        $sql="delete from policy_items where policy_uid='$policy_list_id'";
            $result=$this->db->query($sql);
            $sql="delete from refund_policy where refund_policy_uid='$policy_list_id'";
            $result=$this->db->query($sql);
            if($this->db->affected_rows()){
                return true;
            }
            else{
                return false;
            }
    }
    
    public function details_for_policy_cancellation_restriction($policy_uid,$message,$cancellation_restrict,$new_days_for_cancellation,$name_of_products_tagged_for_purchasing,$new_hour_limit,$select_order_status_type){
		$message=$this->db->escape_str($message);
        $sql="insert into cancellation_policy set cancellation_policy_uid='$policy_uid',item_level='".$this->db->escape_str($name_of_products_tagged_for_purchasing)."',message='$message',cancellation_restrict='$cancellation_restrict',new_days_for_cancellation='$new_days_for_cancellation',new_hour_limit='$new_hour_limit',order_status_type='$select_order_status_type'";
            $result=$this->db->query($sql);
            if($this->db->affected_rows()){
                return true;
            }else{
                return false;
            }
    }
    
    public function Cancellation_processing($params,$fg){
        //define index of column
        $columns = array( 
            0 =>'cancellation_policy.id', 
            1 => 'cancellation_policy.cancellation_policy_uid',
            2 => 'cancellation_policy.item_level',
            3 => 'cancellation_policy.message',
            4 => 'cancellation_policy.cancellation_restrict',
            5 => 'cancellation_policy.new_days_for_cancellation',
            8 => 'cancellation_policy.timestamp',
            9 => 'cancellation_policy.active'
        );

        $where = $sqlTot = $sqlRec = "";
        //$customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT cancellation_policy.* FROM `cancellation_policy`";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" ( cancellation_policy.id LIKE '%".$params['search']['value']."%' ";
            $where .=" OR cancellation_policy.cancellation_policy_uid LIKE '%".$params['search']['value']."%' ";
            $where .=" OR cancellation_policy.item_level LIKE '%".$params['search']['value']."%' ";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        if($fg=="get_total_num_recs"){
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }

    public function getDataOfParticularCancellationPolicy($id){
        $sql="select * from cancellation_policy where cancellation_policy_uid='$id'";
        $result=$this->db->query($sql);
        return $result->result_array(); 
    }
    
    public function update_for_policy_cancellation_restriction($policy_uid,$message,$cancellation_restrict,$new_days_for_cancellation,$new_hour_limit,$name_of_products_tagged_for_purchasing,$select_order_status_type){
		$message=$this->db->escape_str($message);
       $sql="update cancellation_policy set item_level='".$this->db->escape_str($name_of_products_tagged_for_purchasing)."',message='$message',cancellation_restrict='$cancellation_restrict',new_days_for_cancellation='$new_days_for_cancellation',new_hour_limit='$new_hour_limit',order_status_type='$select_order_status_type' where cancellation_policy_uid='$policy_uid'";

            $result=$this->db->query($sql);
            if($this->db->affected_rows()){
                return true;
            }
            else{
                return true;
            }
        
    }
    public function delete_cancellation_payment($policy_list_id){
        $sql="delete from policy_items where policy_uid='$policy_list_id'";
            $result=$this->db->query($sql);
            $sql="delete from cancellation_policy where cancellation_policy_uid='$policy_list_id'";
            $result=$this->db->query($sql);
            if($this->db->affected_rows()){
                return true;
            }
            else{
                return false;
            }
    }

    public function details_for_policy_exchange($policy_uid,$message_ui,$exchange_value_min,$exchange_value_max,$name_of_products_tagged_for_purchasing,$exchange_value_currency,$message_t_c,$content,$location,$shipping,$message_disclaimer,$exchange_start_date,$exchange_end_date){
		$message_ui=$this->db->escape_str($message_ui);
		$message_t_c=$this->db->escape_str($message_t_c);
		$message_disclaimer=$this->db->escape_str($message_disclaimer);
        $sql="insert into exchange_policy set exchange_policy_uid='$policy_uid',item_level='".$this->db->escape_str($name_of_products_tagged_for_purchasing)."',message_ui='$message_ui',exchange_value_min='$exchange_value_min',exchange_value_max='$exchange_value_max',exchange_value_currency='$exchange_value_currency',message_t_c='$message_t_c',content='$content',location='$location',shipping='$shipping',message_disclaimer='$message_disclaimer',exchange_start_date='$exchange_start_date',exchange_end_date='$exchange_end_date' ";
        $result=$this->db->query($sql);
        if($this->db->affected_rows()){
            return true;
        }
        else{
            return false;
        }
    }
    public function Exchange_processing($params,$fg){
        //define index of column
        $columns = array( 
            0 =>'exchange_policy.id', 
            1 => 'exchange_policy.exchange_policy_uid',
            2 => 'exchange_policy.item_level',
            4 => 'exchange_policy.exchange_value_min',
            5 => 'exchange_policy.exchange_value_max',
            9 => 'exchange_policy.location',
            12 => 'exchange_policy.exchange_start_date',
            13 => 'exchange_policy.exchange_end_date',
            14 => 'exchange_policy.timestamp',
        );

        $where = $sqlTot = $sqlRec = "";
        //$customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT exchange_policy.* FROM `exchange_policy`";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" ( exchange_policy.id LIKE '%".$params['search']['value']."%' ";
            $where .=" OR exchange_policy.exchange_policy_uid LIKE '%".$params['search']['value']."%' ";
            $where .=" OR exchange_policy.item_level LIKE '%".$params['search']['value']."%' ";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }
    public function getDataOfParticularExchangePolicy($id){
        $sql="select * from exchange_policy where exchange_policy_uid='$id'";
        $result=$this->db->query($sql);
        return $result->result_array(); 
    }

    public function update_for_policy_exchange_restriction($policy_uid,$message_ui,$exchange_value_min,$exchange_value_max,$name_of_products_tagged_for_purchasing,$exchange_value_currency,$message_t_c,$content,$location,$shipping,$message_disclaimer,$exchange_start_date,$exchange_end_date,$image,$pickup_charge,$pincodes_applicable){
		$message_ui=$this->db->escape_str($message_ui);
		$message_disclaimer=$this->db->escape_str($message_disclaimer);
		$message_t_c=$this->db->escape_str($message_t_c);
		if($image==""){
			$str="";
		}else{
			$str=",image='$image'";
		}
        $sql="update  exchange_policy set item_level='".$this->db->escape_str($name_of_products_tagged_for_purchasing)."',message_ui='$message_ui',exchange_value_min='$exchange_value_min',exchange_value_max='$exchange_value_max',exchange_value_currency='$exchange_value_currency',message_t_c='$message_t_c',content='$content',location='$location',shipping='$shipping',message_disclaimer='$message_disclaimer',exchange_start_date='$exchange_start_date',exchange_end_date='$exchange_end_date' $str,pickup_charge='$pickup_charge',pincodes_applicable='$pincodes_applicable' where exchange_policy_uid='$policy_uid' ";
        $result=$this->db->query($sql);
        if($this->db->affected_rows()){
            return true;
        }
        else{
            return false;
        }
    }
    public function delete_exchange_payment($policy_list_id){
        $sql="delete from policy_items where policy_uid='$policy_list_id'";
            $result=$this->db->query($sql);
            $sql="delete from exchange_policy where exchange_policy_uid='$policy_list_id'";
            $result=$this->db->query($sql);
            if($this->db->affected_rows()){
                return true;
            }
            else{
                return false;
            }
    }
            
    public function getExisting_policy($selected_attr,$selected_level,$policy_type_id){
            if($selected_level=="store_level"){
                $sql="select * from policy_items where policy_type='$policy_type_id' and pcat_id='' and cat_id='' and subcat_id='' and brand_id='' and product_id=''";
            }
            if($selected_level=="parent_category"){
                $sql="select * from policy_items where policy_type='$policy_type_id' and pcat_id='$selected_attr' and cat_id='' and subcat_id='' and brand_id='' and product_id=''";
            }
            if($selected_level=="category"){
                $sql="select * from policy_items where policy_type='$policy_type_id' and pcat_id!='' and cat_id='$selected_attr' and subcat_id='' and brand_id='' and product_id=''";
            }
            if($selected_level=="sub_category"){
                $sql="select * from policy_items where policy_type='$policy_type_id' and pcat_id!='' and cat_id!='' and subcat_id='$selected_attr' and brand_id='' and product_id=''";
            }
            if($selected_level=="attributes"){
                $sql="select * from policy_items where policy_type='$policy_type_id' and pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id='$selected_attr' and product_id=''";
            }
            if($selected_level=="attributes_selector"){
                $sql="select * from policy_items where policy_type='$policy_type_id' and pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id='$selected_attr'";
            }
            $result=$this->db->query($sql);
            if($result->num_rows>0){
                return $result->row()->policy_uid;
            }
            else{
                return false;
            }
        
    }       
    public function draft_promotions_processing($params,$fg){
        //define index of column
        $columns = array( 
            1 =>'promotions.id', 
            4 => 'promotions.promo_type',
            5 => 'promotions.promo_name',
            6 => 'promotions.promo_quote',
            7 => 'promotions.promo_end_date',
            8 => 'promotions.promo_start_date'
        );

        $where = $sqlTot = $sqlRec = "";
        $customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT promotions.*,promotion_type.promotion_name FROM `promotions`,promotion_type where  promotions.   promo_approval=0 and promotions.approval_sent='0' and promotions.promo_active='0' and promotions.activated_once='0'  and promotion_type.id=promotions.promo_type and not promotions.promo_end_date < now()";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" ( promotions.id LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_type LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_name LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_quote LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_start_date LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotion_type.promotion_name like '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_end_date LIKE '%".$params['search']['value']."%' )";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }
    
    public function details_for_policy_shipping_charge_for_replacement_restriction($policy_uid,$replacement_perentage_shipping_charge_by_customer,$shipping_charge_on,$replacement_perentage_shipping_charge_by_admin,$name_of_products_tagged_for_purchasing){
        $sql="insert into shipping_charge_on_replacement set shipping_charge_on_replacement_policy_uid='$policy_uid',percent_paid_by_customer='$replacement_perentage_shipping_charge_by_customer',percent_paid_by_admin='$replacement_perentage_shipping_charge_by_admin',shipping_charge_on='$shipping_charge_on',item_level='".$this->db->escape_str($name_of_products_tagged_for_purchasing)."'";
        $result=$this->db->query($sql);
        if($this->db->affected_rows()>0){
            return true;
        }else{
            return false;
        }
    }
    public function shipping_charge_for_replacement_processing($params,$fg){
         //define index of column
        $columns = array( 
            0 =>'shipping_charge_on_replacement.id', 
            1 => 'shipping_charge_on_replacement.shipping_charge_on_replacement_policy_uid',
            2 => 'shipping_charge_on_replacement.item_level',
            3 => 'shipping_charge_on_replacement.percent_paid_by_customer',
            4 => 'shipping_charge_on_replacement.percent_paid_by_admin',
        );

        $where = $sqlTot = $sqlRec = "";
        //$customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT shipping_charge_on_replacement.* FROM `shipping_charge_on_replacement`";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" ( shipping_charge_on_replacement.id LIKE '%".$params['search']['value']."%' ";
            $where .=" OR shipping_charge_on_replacement.shipping_charge_on_replacement_policy_uid LIKE '%".$params['search']['value']."%' ";
            $where .=" OR shipping_charge_on_replacement.item_level LIKE '%".$params['search']['value']."%' ";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }
    public function getDataOfParticularshipping_charge_for_replacementPolicy($id){
        $sql="select * from shipping_charge_on_replacement where shipping_charge_on_replacement_policy_uid='$id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return false;
        }
    }
    public function update_for_policy_shipping_charge_for_replacement_restriction($policy_uid,$replacement_perentage_shipping_charge_by_customer,$shipping_charge_on,$replacement_perentage_shipping_charge_by_admin,$name_of_products_tagged_for_purchasing){
        $sql="update  shipping_charge_on_replacement set percent_paid_by_customer='$replacement_perentage_shipping_charge_by_customer',percent_paid_by_admin='$replacement_perentage_shipping_charge_by_admin',shipping_charge_on='$shipping_charge_on',item_level='".$this->db->escape_str($name_of_products_tagged_for_purchasing)."' where shipping_charge_on_replacement_policy_uid='$policy_uid'";
        $result=$this->db->query($sql);
        return true;
    }
    
    public function delete_shipping_charge_for_replacement($policy_list_id){
            $sql="delete from policy_items where policy_uid='$policy_list_id'"; 
            $result=$this->db->query($sql);
            $sql="delete from shipping_charge_on_replacement where shipping_charge_on_replacement_policy_uid='$policy_list_id'";
            $result=$this->db->query($sql);
            if($this->db->affected_rows()){
                return true;
            }
            else{
                return false;
            }
    }
    
    }
?>