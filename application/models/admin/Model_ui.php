<?php
	class Model_ui extends CI_Model{
		
	public function get_ui_banner(){
		$sql="select * from ui_front where type='banner' and page='common'";
		$res=$this->db->query($sql);
		return $res->row();
	}
	public function get_ui_footer(){
		$sql="select * from ui_front where type='footer' and page='common'";
		$res=$this->db->query($sql);
		return $res->row();
	}
	public function get_ui(){
		$sql="select * from ui_front where page='common'";
		$res=$this->db->query($sql);
		return $res->result();
	}
	public function get_ui_content($id,$type,$page){
		$sql="select * from ui_front where id='$id' and type='$type' and page='$page'";
		$res=$this->db->query($sql);
		return $res->row();
	}
	public function get_banner_category(){
		$sql="select * from ui_front where page='category'";
		$res=$this->db->query($sql);
		return $res->row();
	}
	public function banner_category_processing($params,$fg){
		
		$columns = array( 
			0 =>'category.cat_name', 
			1 => 'ui_front.background_image',
			2 => 'ui_front.text',
			3 => 'ui_front.type',
			4 => 'ui_front.timestamp',

		);
		$pcat_id =$params['pcat_id'];
		$cat_id =$params['cat_id'];
		$where = $sqlTot = $sqlRec = "";
		if(intval($pcat_id)!=0 && $pcat_id!=""){
			$sql = "select ui_front.*,category.*, parent_category.pcat_id,parent_category.pcat_name from ui_front,category, parent_category where category.pcat_id=parent_category.pcat_id and ui_front.page='category' and ui_front.cat_id=category.cat_id ";

		}else{
				$sql="select ui_front.*,category.* from ui_front,category where ui_front.page='category' and ui_front.cat_id=category.cat_id ";
		}

		if(!empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( category.cat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}
		if($pcat_id!=0 && $pcat_id!=""){
			$where .=" AND ";
			$where .="parent_category.pcat_id='$pcat_id'";	
		}
		if($cat_id!=0 && $cat_id!=""){
			$where .=" AND ";
			$where .="category.cat_id='$cat_id'";	
		}
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		//$sqlRec .=  "order by category.timestamp desc";
		//echo $sqlRec;
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}	
		
	}
	public function add_banner($text,$background_image){
		$sql="insert into ui_front set text='$text',background_image='$background_image',type='banner',page='common'";
		$res=$this->db->query($sql);
		return $res;
	}
	public function add_footer($background_image){
		$sql="insert into ui_front set background_image='$background_image',type='footer',page='common'";
		$res=$this->db->query($sql);
		return $res;
	}
	public function update_banner($text,$background_image,$id){
		$sql="update ui_front set text='$text',background_image='$background_image' where id='$id'";
		$res=$this->db->query($sql);
		return $res;
	}
	public function delete_content($id,$type,$page){
		$sql_select="select * from ui_front where id='$id' and type='$type' and page='$page'";
		$query_select =$this->db->query($sql_select);		
		$result=$query_select->row();
		if(!empty($result)){
			if(file_exists($result->background_image)){
				unlink($result->background_image);
			}
			$sql="delete from ui_front where id='$id'";
			$res=$this->db->query($sql);
			return $res;
		}else{
			return true;
		}
	}
	public function delete_testimonial($id){
		$sql_select="select * from testimonial where id='$id'";
		$query_select =$this->db->query($sql_select);		
		$result=$query_select->row();
		if(!empty($result)){
			if(file_exists($result->image)){
				unlink($result->image);
			}
			$sql="delete from testimonial where id='$id'";
			$res=$this->db->query($sql);
			return $res;
		}else{
			return true;
		}
	}
	public function add_banner_category($cat_id,$cat_text,$background_image){
		$sql="insert into ui_front set background_image='$background_image',cat_id='$cat_id',cat_text='$cat_text', type='banner',page='category'";
		$res=$this->db->query($sql);
		return $res;
	}
	public function get_banner_category_content($id,$cat_id){
		$sql="select * from ui_front where page='category' and cat_id='$cat_id' and id='$id'";
		$res=$this->db->query($sql);
		return $res->row();
	}
	public function get_category($pcat_id){
		$sql="select * from category where pcat_id='$pcat_id'";
		$res=$this->db->query($sql);
		return $res->result();
	}
	public function update_banner_category($id,$cat_id,$cat_text,$background_image){
		$sql="update ui_front set cat_id='$cat_id',cat_text='$cat_text',background_image='$background_image' where id='$id'";
		$res=$this->db->query($sql);
		return $res;
	}
	public function check_cat_exists($cat_id){
		$sql="select * from ui_front where cat_id='$cat_id' and page='category' and type='banner'";
		$res=$this->db->query($sql);
		return count($res->result());
	}
	public function testimonial(){
		$sql="select * from testimonial";
		$res=$this->db->query($sql);
		return $res->result();
	}
	public function testimonial_processing($params,$fg){
		
		$columns = array( 
			0 =>'testimonial.timestamp', 
			1 => 'testimonial.image',
			2 => 'testimonial.name',
			3 => 'testimonial.content',
			4 => 'testimonial.timestamp',
		);
		$pcat_id =$params['pcat_id'];
		$cat_id =$params['cat_id'];
		$where = $sqlTot = $sqlRec = "";
		
		if(intval($pcat_id)!=0 && $pcat_id!="" && $cat_id!=''){
			$sql = "select testimonial.*,category.*, parent_category.pcat_id,parent_category.pcat_name from testimonial,category, parent_category where category.pcat_id=parent_category.pcat_id and testimonial.page='category' and testimonial.cat_id=category.cat_id and testimonial.page='category'";
		}else{
			if($pcat_id=="" && $cat_id==""){
				$sql="select testimonial.* from testimonial where testimonial.page='common'";
			}else{
				if($cat_id!=""){
					$sql="select testimonial.*,category.* from testimonial,category where testimonial.page='category' and testimonial.cat_id=category.cat_id and testimonial.page='category'";
				}else{
					$sql="select testimonial.* from testimonial where testimonial.page!='' ";
				}
			}
		}

		if(!empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( testimonial.designation LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" AND testimonial.name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}
		if($pcat_id!=0 && $pcat_id!=""){
			$where .=" AND ";
			$where .="parent_category.pcat_id='$pcat_id'";	
		}
		if($cat_id!=0 && $cat_id!=""){
			$where .=" AND ";
			$where .="category.cat_id='$cat_id'";	
		}
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		//$sqlRec .=  "order by category.timestamp desc";
		//echo $sqlRec;
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}	
		
	}
	public function add_testimonial($cat_id,$image,$name,$designation,$heading,$content,$heading2,$content2,$heading3,$content3,$page){
		$sql="insert into testimonial set image='$image',cat_id='$cat_id',heading='$heading',content='$content',heading2='$heading2',content2='$content2',heading3='$heading3',content3='$content3',page='$page',name='$name',designation='$designation'";
		$res=$this->db->query($sql);
		return $res;
	}
	public function update_testimonial($id,$cat_id,$image,$name,$designation,$heading,$content,$heading2,$content2,$heading3,$content3,$page){
		$sql="update testimonial set cat_id='$cat_id',heading='$heading',content='$content',heading2='$heading2',content2='$content2',heading3='$heading3',content3='$content3',image='$image',name='$name',designation='$designation',page='$page' where id='$id'";
		$res=$this->db->query($sql);
		return $res;
	}
	public function get_testimonial($id){
		$sql="select * from testimonial where id='$id'";
		$res=$this->db->query($sql);
		return $res->row();
	}
	public function site_description(){
		$sql="select * from site_description";
		$res=$this->db->query($sql);
		return $res->result();
	}
	public function add_site_description($title,$description,$background_image,$view_order,$active){
		$sql="insert into site_description set title='$title',description='$description',background_image='$background_image',view_order='$view_order',active='$active'";
		$res=$this->db->query($sql);
		return $res;
	}
	public function get_site_description($id){
		$sql="select * from site_description where id='$id'";
		$res=$this->db->query($sql);
		return $res->row();
	}
	public function update_site_description($id,$title,$description,$background_image,$view_order,$active){
		echo $sql="update site_description set title='$title',description='$description',background_image='$background_image',view_order='$view_order',active='$active' where id='$id'";
		$res=$this->db->query($sql);
		return $res;
	}
	public function delete_site_des($id){
		$sql_select="select * from site_description where id='$id'";
		$query_select =$this->db->query($sql_select);		
		$result=$query_select->row();
		if(!empty($result)){
			if(file_exists($result->image)){
				unlink($result->image);
			}
			$sql="delete from site_description where id='$id'";
			$res=$this->db->query($sql);
			return $res;
		}else{
			return true;
		}
	}
	public function assign_inventory(){
		$sql="select * from inv_to_index";
		$res =$this->db->query($sql);		
		return $res->result();
	}
	public function get_brand_name($brand_id){
            
            $sql="select brand_name from brands where  brand_id='$brand_id'";
            $query=$this->db->query($sql);
            return $query->row()->brand_name;
        }
	public function get_category_name($cat_id){
            
            $sql="select cat_name from category where  cat_id='$cat_id'";
            $query=$this->db->query($sql);
            return $query->row()->cat_name;
        }
	public function assign_inv_processing($params,$fg){
		
		$columns = array( 
			0 => 'category.cat_name', 
			1 => 'products.product_name',
			2 => 'parent_category.pcat_name',
			3 => 'inv_to_index.page',
			4 => 'inv_to_index.timestamp',

		);
		
		$pcat_id =$params['pcat_id'];
		$cat_id =$params['cat_id'];
		$page =$params['page'];
		$section_name_in_page =$params['section_name_in_page'];
		
		
		$where = $sqlTot = $sqlRec = "";
		
		if($section_name_in_page=="Shop by Category"){
			$sql="select brand_to_index_shopbycategory.*,brand_to_index_shopbycategory.id as i_id,category.* from brand_to_index_shopbycategory,category where brand_to_index_shopbycategory.cat_id=category.cat_id and brand_to_index_shopbycategory.page='$page' and brand_to_index_shopbycategory.section_name_in_page='$section_name_in_page'";

			if(!empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( category.cat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" AND brands.brand_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
			}
			if($pcat_id!=0 && $pcat_id!=""){
				$where .=" AND ";
				$where .="category.pcat_id='$pcat_id'";	
			}
			if($cat_id!=0 && $cat_id!=""){
				$where .=" AND ";
				$where .="category.cat_id='$cat_id'";	
			}
		}
		else if($section_name_in_page=="Shop by Parent Category"){
			$sql="select brand_to_index_shopbyparentcategory.*,brand_to_index_shopbyparentcategory.id as i_id,parent_category.* from brand_to_index_shopbyparentcategory,parent_category where brand_to_index_shopbyparentcategory.pcat_id=parent_category.pcat_id and brand_to_index_shopbyparentcategory.page='$page' and brand_to_index_shopbyparentcategory.section_name_in_page='$section_name_in_page'";

			if(!empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( parent_category.pcat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
			}
			if($pcat_id!=0 && $pcat_id!=""){
				$where .=" AND ";
				$where .="parent_category.pcat_id='$pcat_id'";	
			}
		}
		else if($section_name_in_page=="Shop by Brand"){
			$sql="select brand_to_index_shopbybrand.*,brand_to_index_shopbybrand.id as i_id from brand_to_index_shopbybrand where brand_to_index_shopbybrand.page='$page' and brand_to_index_shopbybrand.section_name_in_page='$section_name_in_page'";

			if(!empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( brand_to_index_shopbybrand.brand_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
			}
			
		}
		else{
			$sql="select inv_to_index.*,inv_to_index.id as i_id,category.*,inventory.*,inventory.id as inventoy_id,products.* from inv_to_index,inventory,category,products where inv_to_index.inventory_id=inventory.id and inv_to_index.product_id=inventory.product_id and inventory.product_id=products.product_id and products.cat_id=category.cat_id and products.active=1 and inv_to_index.page='$page' and inv_to_index.section_name_in_page='$section_name_in_page'";

			if(!empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( category.cat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" AND products.product_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
			}
			if($pcat_id!=0 && $pcat_id!=""){
				$where .=" AND ";
				$where .="products.pcat_id='$pcat_id'";	
			}
			if($cat_id!=0 && $cat_id!=""){
				$where .=" AND ";
				$where .="products.cat_id='$cat_id'";	
			}
		}
		
		
		
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		if($section_name_in_page=="Shop by Parent Category"){
			$sqlRec .=  " ORDER BY parent_category.pcat_name  ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		}
		else if($section_name_in_page=="Shop by Brand"){
			$sqlRec .=  " ORDER BY brand_to_index_shopbybrand.brand_name  ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		}
		else{
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		}
		
		//$sqlRec .=  "order by category.timestamp desc";
		//echo $sqlRec;
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}	
		
	}
	public function get_assign_inventory_data(){
		$sql="select count(*) as count from inv_to_index";
		$query=$this->db->query($sql);
		return $query->row_array()["count"];
	}
	public function show_available_inventory($product_id){
		
		$sql="select inventory.id,inventory.sku_id,inventory.product_id,inventory.thumbnail from  inventory  where inventory_type!=2";// filtering the addon(inventory_type 2)
		if($this->get_assign_inventory_data()>0){
			//$sql.=",inv_to_index ";
		}
		$sql.=" and trim(inventory.product_id) = '$product_id' ";
		if($this->get_assign_inventory_data()>0){
			//$sql.="and inv_to_index.inventory_id!=inventory.id ";
		}
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function add_assign_inventory($product_id,$cat_id,$inventory,$page,$section_name_in_page){
		$err=array();
		if(!empty($inventory)){
			//$sql='';
			foreach($inventory as $inventory_id){

			$sql="INSERT INTO inv_to_index (inventory_id, product_id,cat_id,page,section_name_in_page) SELECT * FROM (SELECT '$inventory_id' as x, '$product_id' as y,'$cat_id' as z,'$page' as n,'$section_name_in_page' as n1) AS tmp WHERE NOT EXISTS ( SELECT inventory_id FROM inv_to_index WHERE inventory_id = '$inventory_id' and  page='$page' and section_name_in_page='$section_name_in_page') LIMIT 1 ;";	
			$res=$this->db->query($sql);
				if($res==false){
					$err[]=0;
				}
			}
			
			if(empty($err)){
				return true;
			}else{
				return false;
			}
			
		}
	}
	public function update_view_order($id,$view_order,$section_name_in_page){
		if($section_name_in_page=="Shop by Category"){
			$sql="update brand_to_index_shopbycategory set view_order='$view_order' where id='$id'";
		}
		else if($section_name_in_page=="Shop by Parent Category"){
			$sql="update brand_to_index_shopbyparentcategory set view_order='$view_order' where id='$id'";
		}
		else if($section_name_in_page=="Shop by Brand"){
			$sql="update brand_to_index_shopbybrand set view_order='$view_order' where id='$id'";
		}
		else{
			$sql="update inv_to_index set view_order='$view_order' where id='$id'";
		}
		$res=$this->db->query($sql);
		return $res;
	}
	public function delete_assigned_inventory($selected_list,$section_name_in_page){
		if($section_name_in_page=="Shop by Category"){
			$sql="select * from brand_to_index_shopbycategory where id in ($selected_list) and lower(section_name_in_page)=lower('$section_name_in_page')";
			$query=$this->db->query($sql);
			$result_shopbycategory=$query->result();
			foreach($result_shopbycategory as $res){
				if(file_exists($res->image_shopbycategory)){
					unlink($res->image_shopbycategory);
				}
			}
			
			$sql_delete="delete from brand_to_index_shopbycategory where id in ($selected_list) and lower(section_name_in_page)=lower('$section_name_in_page')";
		}
		else if($section_name_in_page=="Shop by Parent Category"){
			$sql="select * from brand_to_index_shopbyparentcategory where id in ($selected_list) and lower(section_name_in_page)=lower('$section_name_in_page')";
			$query=$this->db->query($sql);
			$result_shopbycategory=$query->result();
			foreach($result_shopbycategory as $res){
				if(file_exists($res->image_shopbyparentcategory)){
					unlink($res->image_shopbyparentcategory);
				}
			}
			
			$sql_delete="delete from brand_to_index_shopbyparentcategory where id in ($selected_list) and lower(section_name_in_page)=lower('$section_name_in_page')";
		}
		else if($section_name_in_page=="Shop by Brand"){
			$sql="select * from brand_to_index_shopbybrand where id in ($selected_list) and lower(section_name_in_page)=lower('$section_name_in_page')";
			$query=$this->db->query($sql);
			$result_shopbycategory=$query->result();
			foreach($result_shopbycategory as $res){
				if(file_exists($res->image_shopbybrand)){
					unlink($res->image_shopbybrand);
				}
			}
			
			$sql_delete="delete from brand_to_index_shopbybrand where id in ($selected_list) and lower(section_name_in_page)=lower('$section_name_in_page')";
		}
		else{
			$sql_delete="delete from inv_to_index where id in ($selected_list) and lower(section_name_in_page)=lower('$section_name_in_page')";
		}
		$flag =$this->db->query($sql_delete);
		return $flag;
	
	}
	
	
	/* shopbycategory starts */
	public function show_available_brands_under_category($category_id){
		
		$sql="select * from brands where trim(brands.cat_id) = '$category_id' ";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function add_assign_brand_shopbycategory($pcat_id,$cat_id,$brand,$page,$section_name_in_page,$legend1,$legend2,$legend3,$image_shopbycategory){
		$err=array();
		$brand_id_list="";
		if(!empty($brand)){
			$brand_id_list=implode(",",$brand);
		}
			//$sql='';
			

			//$sql="INSERT INTO brand_to_index_shopbycategory (brand_id_list,cat_id,page,section_name_in_page,legend1,legend2,legend3,image_shopbycategory,pcat_id) SELECT * FROM (SELECT '$brand_id_list' as x,'$cat_id' as z,'$page' as n,'$section_name_in_page' as n1,'$legend1' as l1,'$legend2' as l2,'$legend3' as l3,'$image_shopbycategory' as l4,'$pcat_id' as l5) AS tmp WHERE NOT EXISTS ( SELECT cat_id FROM brand_to_index_shopbycategory WHERE cat_id = '$cat_id' and  page='$page' and section_name_in_page='$section_name_in_page') LIMIT 1 ;";	
			
			$sql="INSERT INTO brand_to_index_shopbycategory (brand_id_list,cat_id,page,section_name_in_page,legend1,legend2,legend3,image_shopbycategory,pcat_id) SELECT * FROM (SELECT '$brand_id_list' as x,'$cat_id' as z,'$page' as n,'$section_name_in_page' as n1,'$legend1' as l1,'$legend2' as l2,'$legend3' as l3,'$image_shopbycategory' as l4,'$pcat_id' as l5) AS tmp;";	
			$res=$this->db->query($sql);
				if($res==false){
					$err[]=0;
				}
			
			
			if(empty($err)){
				return true;
			}else{
				return false;
			}
			
		
	}
	/* shopbycategory ends */	
	
	
	
	/* shopbyparentcategory starts */
	
	public function add_assign_brand_shopbyparentcategory($pcat_id,$cat_id,$page,$section_name_in_page,$legend1,$legend2,$legend3,$image_shopbyparentcategory){
		$err=array();
		$cat_id_list="";
		if(!empty($cat_id)){
			$cat_id_list=implode(",",$cat_id);
		}
		
			//$sql='';
			

			//$sql="INSERT INTO brand_to_index_shopbyparentcategory (cat_id_list,pcat_id,page,section_name_in_page,legend1,legend2,legend3,image_shopbyparentcategory) SELECT * FROM (SELECT '$cat_id_list' as x,'$pcat_id' as z,'$page' as n,'$section_name_in_page' as n1,'$legend1' as l1,'$legend2' as l2,'$legend3' as l3,'$image_shopbyparentcategory' as l4) AS tmp WHERE NOT EXISTS ( SELECT pcat_id FROM brand_to_index_shopbyparentcategory WHERE pcat_id = '$pcat_id' and  page='$page' and section_name_in_page='$section_name_in_page') LIMIT 1 ;";	
			
			$sql="INSERT INTO brand_to_index_shopbyparentcategory (cat_id_list,pcat_id,page,section_name_in_page,legend1,legend2,legend3,image_shopbyparentcategory) SELECT * FROM (SELECT '$cat_id_list' as x,'$pcat_id' as z,'$page' as n,'$section_name_in_page' as n1,'$legend1' as l1,'$legend2' as l2,'$legend3' as l3,'$image_shopbyparentcategory' as l4) AS tmp ;";	
			
			
			$res=$this->db->query($sql);
				if($res==false){
					$err[]=0;
				}
			
			
			if(empty($err)){
				return true;
			}else{
				return false;
			}
			
		
	}
	/* shopbyparentcategory ends */	
	
	/* shopbybrand starts */
	
	public function add_assign_brand_shopbybrand($brand_name,$page,$section_name_in_page,$legend1,$legend2,$legend3,$image_shopbybrand){
		$err=array();
		
		
			//$sql='';
			

			$sql="INSERT INTO brand_to_index_shopbybrand (brand_name,page,section_name_in_page,legend1,legend2,legend3,image_shopbybrand) SELECT * FROM (SELECT '$brand_name' as z,'$page' as n,'$section_name_in_page' as n1,'$legend1' as l1,'$legend2' as l2,'$legend3' as l3,'$image_shopbybrand' as l4) AS tmp WHERE NOT EXISTS ( SELECT brand_name FROM brand_to_index_shopbybrand WHERE brand_name = '$brand_name' and  page='$page' and section_name_in_page='$section_name_in_page') LIMIT 1 ;";	
			
			$res=$this->db->query($sql);
				if($res==false){
					$err[]=0;
				}
			
			
			if(empty($err)){
				return true;
			}else{
				return false;
			}
			
		
	}
	/* shopbybrand ends */	
}
?>