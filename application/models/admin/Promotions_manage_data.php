<?php
	class Promotions_manage_data extends CI_Model{
		
	public function get_all_promotion_type(){
		$sql="select * from promotion_type ";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
				return $result->result_array();
			}
			else{
				return false;
			}
	}
	public function get_all_promotion_quote($id){
		$sql="select * from promotion_quote where promotion_type_id='$id' ";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
				return $result->result_array();
		}
		else{
			return false;
		}
	}
	public function get_quote_selector_value($quote_selector){
	    $sql="select promotion_quote from promotion_quote where id='$quote_selector' ";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->row()->promotion_quote;
        }
        else{
            return false;
        }
	}
	public function get_all_promotion_quote_addons($quote_selector_val){
		$sql="select * from promotion_quote where id='$quote_selector_val' ";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
				return $result->result_array();
		}
		else{
			return false;
		}
	}
	public function get_all_promotion_type_variables($quote_selector_val){
		$sql="select * from promotion_type_variables where 	promotion_quote_id='$quote_selector_val' ";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
				return $result->result_array();
		}
		else{
			return false;
		}
	}
		
	public function save_promotions($promo_uid,$deal_type,$quote_selector,$deal_name,$buy_deal_value,$get_deal_value,$buy_deal_type,$get_deal_type,$final_offer_value,$start_date,$end_date,$promotion_at_check_out,$promotion_dependency){
		$sql="insert into promotions (promo_uid,promo_type,quote_selector,promo_name,to_buy,to_get,buy_type,get_type,applied_at_invoice,promo_quote,promo_start_date,promo_end_date,promotion_dependency) values('$promo_uid','$deal_type','$quote_selector','$deal_name','$buy_deal_value','$get_deal_value','$buy_deal_type','$get_deal_type','$promotion_at_check_out','$final_offer_value','$start_date','$end_date','$promotion_dependency')";
		$result=$this->db->query($sql);
		if($this->db->affected_rows()){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function get_promo_master_data($promo_uid){
		$sql="select * from promotions where promo_uid='$promo_uid'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function get_promo_type_name($id){
		$sql="select promotion_name from promotion_type where id='$id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->promotion_name;
		}
		else{
			return false;
		}
	}
	
	public function add_items_tagged_for_purchasing($promo_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector){
	       if($products_tagged_for_purchasing!=""){
                    $sqlen="select * from promotion_items_taged_for_purchasing where pcat_id='' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and  promo_uid='$promo_uid'";
                    $resultenq=$this->db->query($sqlen);
                    if($resultenq->num_rows()>0){
                        $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and  promo_uid='$promo_uid'";
                        $this->db->query($sqlen);
                    }
                    $sqlen="select * from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='$category' and subcat_id='$sub_category' and brand_id='$attributes' and product_id='$attributes_selector' and  promo_uid='$promo_uid'";
                    $resultenq=$this->db->query($sqlen);
                    if($resultenq->num_rows()>0){
                        $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='$category' and subcat_id='$sub_category' and brand_id='$attributes' and product_id='$attributes_selector' and  promo_uid='$promo_uid'";
                        $this->db->query($sqlen);
                    }
                    
                    $sqlen="select * from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='$category' and subcat_id='$sub_category' and brand_id='$attributes' and product_id='' and  promo_uid='$promo_uid'";
                    $resultenq=$this->db->query($sqlen);
                    if($resultenq->num_rows()>0){
                        $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='$category' and subcat_id='$sub_category' and brand_id='$attributes'and product_id='' and  promo_uid='$promo_uid'";
                        $this->db->query($sqlen);
                    }
                    $sqlen="select * from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='$category' and subcat_id='$sub_category' and brand_id='' and product_id='' and  promo_uid='$promo_uid'";
                    $resultenq=$this->db->query($sqlen);
                    if($resultenq->num_rows()>0){
                        $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='$category' and subcat_id='$sub_category' and brand_id='' and product_id='' and  promo_uid='$promo_uid'";
                        $this->db->query($sqlen);
                    }
                    $sqlen="select * from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='$category'  and subcat_id='' and brand_id='' and product_id='' and  promo_uid='$promo_uid'";
                    $resultenq=$this->db->query($sqlen);
                    if($resultenq->num_rows()>0){
                        $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='$category' and subcat_id='' and brand_id='' and product_id='' and  promo_uid='$promo_uid'";
                        $this->db->query($sqlen);
                    }
                    $sqlen="select * from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and  promo_uid='$promo_uid'";
                    $resultenq=$this->db->query($sqlen);
                    if($resultenq->num_rows()>0){
                        $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and  promo_uid='$promo_uid'";
                        $this->db->query($sqlen);
                    }
  
                   
	           
	           $inv_id_arr=explode(',',$products_tagged_for_purchasing);
                    $success=0;
                    foreach($inv_id_arr as $inv_id){
                        $sqlen="select * from promotion_items_taged_for_purchasing where inv_id='$inv_id' and promo_uid='$promo_uid'";
                        $resultenq=$this->db->query($sqlen);
                        if($resultenq->num_rows()==0){
                                $sql="insert into promotion_items_taged_for_purchasing (promo_uid,pcat_id,cat_id,subcat_id,brand_id,product_id,inv_id) values('$promo_uid','$parent_category','$category','$sub_category','$attributes','$attributes_selector','$inv_id')";
                    $result=$this->db->query($sql);
            	       }
                    }
                }
                    
            else{
                /*echo $promo_uid ." promo_uid<br>";
                echo $products_tagged_for_purchasing." products_tagged_for_purchasing<br>";
                echo $parent_category." parent_category<br>";
                echo $category." category<br>";
                echo $sub_category." sub_category<br>";
                echo $attributes." attributes<br>";
                echo $attributes_selector." attributes_selector<br>";*/

                if($parent_category!=""&& $category!="" && $sub_category!="" && $attributes!="" && $attributes_selector!=""&&$products_tagged_for_purchasing==""){
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='$category' and subcat_id='$sub_category' and brand_id='$attributes' and product_id='$attributes_selector' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='$category' and subcat_id='$sub_category' and brand_id='$attributes' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='$category' and subcat_id='$sub_category' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='$category' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                
                
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id!='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id!='' and inv_id!='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                 
               }
               if($parent_category!=""&& $category!="" && $sub_category!="" && $attributes!="" && $attributes_selector==""&&$products_tagged_for_purchasing==""){
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='$category' and subcat_id='$sub_category' and brand_id='$attributes' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='$category' and subcat_id='$sub_category' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='$category' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                
                
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id!='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id!='' and inv_id!='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
               }
               if($parent_category!=""&& $category!="" && $sub_category!="" && $attributes=="" && $attributes_selector==""&&$products_tagged_for_purchasing==""){
                  
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='$category' and subcat_id='$sub_category' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='$category' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                
                
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id!='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id!='' and inv_id!='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                    
               }
               if($parent_category!=""&& $category=="" && $sub_category=="" && $attributes=="" && $attributes_selector==""&&$products_tagged_for_purchasing==""){
               $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='$parent_category' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id!='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id!='' and inv_id!='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
               }

               if($parent_category==""&& $category=="" && $sub_category=="" && $attributes=="" && $attributes_selector==""&&$products_tagged_for_purchasing==""){
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id='' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                
                
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id!='' and inv_id='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
                $sqlen="delete from promotion_items_taged_for_purchasing where pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id!='' and inv_id!='' and  promo_uid='$promo_uid'";
                $this->db->query($sqlen);
               }

                $sql="insert into promotion_items_taged_for_purchasing (promo_uid,pcat_id,cat_id,subcat_id,brand_id,product_id,inv_id) values('$promo_uid','$parent_category','$category','$sub_category','$attributes','$attributes_selector','$products_tagged_for_purchasing')";
                $result=$this->db->query($sql);
                }
		return true;
	}

    public function getAllStoreRelatedProductDiscount($promo_uid){
        $sql="select promotion_items_taged_for_purchasing.*,promotions.promo_active,promotions.promo_approval,promotions.approval_sent,promotions.activated_once,promotions.promo_quote,promotions.promo_start_date,promotions.promo_end_date from promotion_items_taged_for_purchasing,promotions where promotion_items_taged_for_purchasing.pcat_id='' and promotion_items_taged_for_purchasing.cat_id='' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.inv_id='' and promotions.promo_uid=promotion_items_taged_for_purchasing.promo_uid and now() between promotions.promo_start_date and promotions.promo_end_date and promotion_items_taged_for_purchasing.promo_uid!='$promo_uid'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->result_array();
         }
         else{
              return false;
         }
    }
    public function getAllparent_categoryRelatedProductDiscount($promo_uid,$parent_category){
        $sql="select promotion_items_taged_for_purchasing.*,promotions.promo_active,promotions.promo_approval,promotions.approval_sent,promotions.activated_once,promotions.promo_quote,promotions.promo_start_date,promotions.promo_end_date from promotion_items_taged_for_purchasing,promotions where promotion_items_taged_for_purchasing.pcat_id='$parent_category' and promotion_items_taged_for_purchasing.cat_id='' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.inv_id='' and promotions.promo_uid=promotion_items_taged_for_purchasing.promo_uid and now() between promotions.promo_start_date and promotions.promo_end_date and promotion_items_taged_for_purchasing.promo_uid!='$promo_uid'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->result_array();
         }
         else{
              return false;
         }
    }
    public function getAllcategoryRelatedProductDiscount($promo_uid,$parent_category,$category){
        $sql="select promotion_items_taged_for_purchasing.*,promotions.promo_active,promotions.promo_approval,promotions.approval_sent,promotions.activated_once,promotions.promo_quote,promotions.promo_start_date,promotions.promo_end_date from promotion_items_taged_for_purchasing,promotions where promotion_items_taged_for_purchasing.pcat_id='$parent_category' and promotion_items_taged_for_purchasing.cat_id='$category' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.inv_id='' and promotions.promo_uid=promotion_items_taged_for_purchasing.promo_uid and now() between promotions.promo_start_date and promotions.promo_end_date and promotion_items_taged_for_purchasing.promo_uid!='$promo_uid'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->result_array();
         }
         else{
              return false;
         }
    }
    public function getAllsub_categoryRelatedProductDiscount($promo_uid,$parent_category,$category,$sub_category){
        $sql="select promotion_items_taged_for_purchasing.*,promotions.promo_active,promotions.promo_approval,promotions.approval_sent,promotions.activated_once,promotions.promo_quote,promotions.promo_start_date,promotions.promo_end_date from promotion_items_taged_for_purchasing,promotions where promotion_items_taged_for_purchasing.pcat_id='$parent_category' and promotion_items_taged_for_purchasing.cat_id='$category' and promotion_items_taged_for_purchasing.subcat_id='$sub_category' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.inv_id='' and promotions.promo_uid=promotion_items_taged_for_purchasing.promo_uid and now() between promotions.promo_start_date and promotions.promo_end_date and promotion_items_taged_for_purchasing.promo_uid!='$promo_uid'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->result_array();
         }
         else{
              return false;
         }
    }
    public function getAllattributesRelatedProductDiscount($promo_uid,$parent_category,$category,$sub_category,$attributes){
        $sql="select promotion_items_taged_for_purchasing.*,promotions.promo_active,promotions.promo_approval,promotions.approval_sent,promotions.activated_once,promotions.promo_quote,promotions.promo_start_date,promotions.promo_end_date from promotion_items_taged_for_purchasing,promotions where promotion_items_taged_for_purchasing.pcat_id='$parent_category' and promotion_items_taged_for_purchasing.cat_id='$category' and promotion_items_taged_for_purchasing.subcat_id='$sub_category' and promotion_items_taged_for_purchasing.brand_id='$attributes' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.inv_id='' and promotions.promo_uid=promotion_items_taged_for_purchasing.promo_uid and now() between promotions.promo_start_date and promotions.promo_end_date and promotion_items_taged_for_purchasing.promo_uid!='$promo_uid'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->result_array();
         }
         else{
              return false;
         }
    }
    public function getAllattributes_selectorRelatedProductDiscount($promo_uid,$parent_category,$category,$sub_category,$attributes,$attributes_selector){
        $sql="select promotion_items_taged_for_purchasing.*,promotions.promo_active,promotions.promo_approval,promotions.approval_sent,promotions.activated_once,promotions.promo_quote,promotions.promo_start_date,promotions.promo_end_date from promotion_items_taged_for_purchasing,promotions where promotion_items_taged_for_purchasing.pcat_id='$parent_category' and promotion_items_taged_for_purchasing.cat_id='$category' and promotion_items_taged_for_purchasing.subcat_id='$sub_category' and promotion_items_taged_for_purchasing.brand_id='$attributes' and promotion_items_taged_for_purchasing.product_id='$attributes_selector' and promotion_items_taged_for_purchasing.inv_id='' and promotions.promo_uid=promotion_items_taged_for_purchasing.promo_uid and now() between promotions.promo_start_date and promotions.promo_end_date and promotion_items_taged_for_purchasing.promo_uid!='$promo_uid'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->result_array();
         }
         else{
              return false;
         }
        
    }
    public function getAllproducts_tagged_for_purchasingRelatedProductDiscount($promo_uid,$parent_category,$category,$sub_category,$attributes,$attributes_selector,$products_tagged_for_purchasing){
            
        $inv_id_arr=explode(',',$products_tagged_for_purchasing);
        $inv_level_promos=array();
        foreach($inv_id_arr as $inv_id){
            $sql="select promotion_items_taged_for_purchasing.*,promotions.promo_active,promotions.promo_approval,promotions.approval_sent,promotions.activated_once,promotions.promo_quote,promotions.promo_start_date,promotions.promo_end_date from promotion_items_taged_for_purchasing,promotions where promotion_items_taged_for_purchasing.pcat_id='$parent_category' and promotion_items_taged_for_purchasing.cat_id='$category' and promotion_items_taged_for_purchasing.subcat_id='$sub_category' and promotion_items_taged_for_purchasing.brand_id='$attributes' and promotion_items_taged_for_purchasing.product_id='$attributes_selector' and promotion_items_taged_for_purchasing.inv_id='$inv_id' and promotions.promo_uid=promotion_items_taged_for_purchasing.promo_uid and now() between promotions.promo_start_date and promotions.promo_end_date and promotion_items_taged_for_purchasing.promo_uid!='$promo_uid'";
            $result=$this->db->query($sql);
            if($result->num_rows()>0){
                    $inv_level_promos[$inv_id] =$result->result_array();
             }
            
        }
        if(count($inv_level_promos)>0){
            return $inv_level_promos;
        }
        else{
            return false;
        }
                        
        /*$sql="select promotion_items_taged_for_purchasing.*,promotions.promo_start_date,promotions.promo_end_date from promotion_items_taged_for_purchasing,promotions where promotion_items_taged_for_purchasing.pcat_id='$parent_category' and promotion_items_taged_for_purchasing.cat_id='$category' and promotion_items_taged_for_purchasing.subcat_id='$sub_category' and promotion_items_taged_for_purchasing.brand_id='$attributes' and promotion_items_taged_for_purchasing.product_id='$attributes_selector' and promotion_items_taged_for_purchasing.inv_id='' and promotions.promo_uid=promotion_items_taged_for_purchasing.promo_uid and now() between promotions.promo_start_date and promotions.promo_end_date";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->result_array();
         }
         else{
              return false;
         }*/
    }
    
    
    public function delete_bundle_item($bundle_id){
        $sql="delete from promotion_items_taged_for_purchasing where id='$bundle_id'";
        $result=$this->db->query($sql);
        if($this->db->affected_rows()){
            return true;
        }
        else{
            return false;
        }
    }
	
	public function add_items_tagged_for_purchasing_free($promo_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector){
		$inv_id_arr=explode(',',$products_tagged_for_purchasing);
		$success=0;
		foreach($inv_id_arr as $inv_id){
			$sqlen="select * from promotion_items_taged_for_purchasing_free where inv_id='$inv_id' and promo_uid='$promo_uid'";
			$resultenq=$this->db->query($sqlen);
			if($resultenq->num_rows()==0){
					$sql="insert into promotion_items_taged_for_purchasing_free (promo_uid,pcat_id,cat_id,subcat_id,brand_id,product_id,inv_id) values('$promo_uid','$parent_category','$category','$sub_category','$attributes','$attributes_selector','$inv_id')";
		$result=$this->db->query($sql);
				
			}

		}
		return true;
	}
	
	public function promoitems_on_purchase($promo_uid){
		$sql="select distinct product_id from promotion_items_taged_for_purchasing where inv_id!='' and  promo_uid='$promo_uid'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
				return $result->result_array();
			}
			else{
				return false;
			}
	}
    public function bundle_promoitems_on_purchase($promo_uid){
        $sql="select distinct * from promotion_items_taged_for_purchasing where  inv_id='' and  promo_uid='$promo_uid'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->result_array();
         }
         else{
              return false;
         }
    }
    public function get_parent_Category_name($pcat_id){
        $sql="select pcat_name from parent_category where pcat_id='$pcat_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->row()->pcat_name;
         }
         else{
              return false;
         }
    }
    public function get_Category_name($cat_id){
        $sql="select cat_name from category where cat_id='$cat_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->row()->cat_name;
         }
         else{
              return false;
         }
    }
    public function get_Sub_Category_name($sub_cat_id){
        $sql="select subcat_name from subcategory where subcat_id='$sub_cat_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->row()-> subcat_name;
         }
         else{
              return false;
         }
    }
    public function get_Brand_name($brand_id){
        $sql="select brand_name from brands where brand_id='$brand_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->row()-> brand_name;
         }
         else{
              return false;
         }
    }
    

	public function promoitems_on_purchase_free($promo_uid){
		$sql="select inv_id,product_id,quantity_def from promotion_items_taged_for_purchasing_free where promo_uid='$promo_uid'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
				return $result->result_array();
			}
			else{
				return false;
			}
	}
	
	public function update_promo_free_units_number($promo_id,$inv_id,$quant_val){
		$sql="update promotion_items_taged_for_purchasing_free set quantity_def='$quant_val' where promo_uid='$promo_id' and inv_id='$inv_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows()){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function get_product_name($product_id){
		$sql="select product_name from products where product_id='$product_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
				return $result->row()->product_name;
			}
			else{
				return false;
			}
	}
	
	public function get_product_list_in_product_promo($product_id,$promo_uid){
		$sql="select inv_id from promotion_items_taged_for_purchasing where product_id='$product_id' and promo_uid='$promo_uid'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
				return $result->result_array();
			}
			else{
				return false;
			}
	
	}
	
	public function get_inv_attribute($list_in_product){
		$sql="select * from inventory ";
        $user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");
        if($user_type=='vendor'){
			$sql.=' LEFT JOIN `vendor_catalog_inventory` ON `vendor_catalog_inventory`.`vendor_catalog_id` = `inventory`.`inv_vendor_catalog_id`';
		}
        $sql.=" where id='$list_in_product' and inventory_type!='2' ";
        if($user_type=='vendor'){
			$sql .=" and ";
			$sql .=" vendor_catalog_inventory.vendor_id='$a_id' ";
		}
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
				return $result->result_array();
			}
			else{
				return false;
			}
	}
	
	public function delete_this_promo_item_purchasing($promo_id,$inv_id){
		$sql="delete from promotion_items_taged_for_purchasing where promo_uid='$promo_id' and inv_id='$inv_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows()){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function delete_this_promo_item_purchasing_free($promo_id,$inv_id){
		$sql="delete from promotion_items_taged_for_purchasing_free where promo_uid='$promo_id' and inv_id='$inv_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows()){
			return true;
		}
		else{
			return false;
		}
	}	
	public function get_all_parent_category(){
	    $sql="select * from parent_category where view=1";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return false;
        }
	}	
	public function get_all_category($pcat_id){
		$sql="select * from category where pcat_id='$pcat_id' and view=1";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function get_all_sub_category($cat_id){
		$sql="select * from subcategory where cat_id='$cat_id' and view=1";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function get_all_brand($sub_cat_val){
		$sql="select * from brands where subcat_id='$sub_cat_val' and view=1";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function get_all_attributes($brands_val){
		$sql="select * from products where brand_id='$brands_val' and view=1";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	public function get_all_items_in_type($product_val){
        

		$sql="select * from inventory ";

        $user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");
        if($user_type=='vendor'){
			$sql.=' LEFT JOIN `vendor_catalog_inventory` ON `vendor_catalog_inventory`.`vendor_catalog_id` = `inventory`.`inv_vendor_catalog_id`';
		}
        $sql .=" where product_id='$product_val'  and inventory_type!='2' ";
        if($user_type=='vendor'){
			$sql .=" and ";
			$sql .=" vendor_catalog_inventory.vendor_id='$a_id' ";
		}
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	public function get_all_items_in_type_filter($product_val){
		$type_filter=array();
		$sql="select distinct attribute_1_value as attr1,attribute_1 from inventory ";
        $user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");
        if($user_type=='vendor'){
			$sql.=' LEFT JOIN `vendor_catalog_inventory` ON `vendor_catalog_inventory`.`vendor_catalog_id` = `inventory`.`inv_vendor_catalog_id`';
		}
        $sql.=" where product_id='$product_val' and inventory_type!='2' ";
        if($user_type=='vendor'){
			$sql .=" and ";
			$sql .=" vendor_catalog_inventory.vendor_id='$a_id' ";
		}
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			foreach($result->result() as $resObj){
				if(preg_match("/:/",$resObj->attr1)){
					$resObj->attr1= substr($resObj->attr1, 0, strrpos($resObj->attr1, ":"));
				}
				$type_filter["attr1"][]=$resObj->attr1;
				if(!isset($type_filter["attribute_1"])){
					$type_filter["attribute_1"]=$resObj->attribute_1;
				}
			}
		}
		
		$sql="select distinct attribute_2_value as attr2,attribute_2 from inventory " ;

        $user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");
        if($user_type=='vendor'){
			$sql.=' LEFT JOIN `vendor_catalog_inventory` ON `vendor_catalog_inventory`.`vendor_catalog_id` = `inventory`.`inv_vendor_catalog_id`';
		}
        $sql.=" where product_id='$product_val' and attribute_2_value!='' and inventory_type!='2' ";
        if($user_type=='vendor'){
			$sql .=" and ";
			$sql .=" vendor_catalog_inventory.vendor_id='$a_id' ";
		}
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			foreach($result->result() as $resObj){
				$type_filter["attr2"][]=$resObj->attr2;
				if(!isset($type_filter["attribute_2"])){
					$type_filter["attribute_2"]=$resObj->attribute_2;
				}
			}
		}
		return $type_filter;
	}
	public function get_all_items_in_type_filter_ref($product_val){
		$type_filter=array();
		$sql="select distinct attribute_1_value as color from inventory ";
        $user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");
        if($user_type=='vendor'){
			$sql.=' LEFT JOIN `vendor_catalog_inventory` ON `vendor_catalog_inventory`.`vendor_catalog_id` = `inventory`.`inv_vendor_catalog_id`';
		}
        $sql.="  where product_id='$product_val' and inventory_type!='2' ";
        if($user_type=='vendor'){
			$sql .=" and ";
			$sql .=" vendor_catalog_inventory.vendor_id='$a_id' ";
		}
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			foreach($result->result() as $resObj){
				$resObj->color= substr($resObj->color, 0, strpos($resObj->color, ":"));
				$type_filter["color"][]=$resObj->color;
			}
		}
		
		$sql="select distinct attribute_2_value as size from inventory ";
        $user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");
        if($user_type=='vendor'){
			$sql.=' LEFT JOIN `vendor_catalog_inventory` ON `vendor_catalog_inventory`.`vendor_catalog_id` = `inventory`.`inv_vendor_catalog_id`';
		}
        $sql.=" where product_id='$product_val' and inventory_type!='2' ";
        if($user_type=='vendor'){
			$sql .=" and ";
			$sql .=" vendor_catalog_inventory.vendor_id='$a_id' ";
		}
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			foreach($result->result() as $resObj){
				$type_filter["size"][]=$resObj->size;
			}
		}
		return $type_filter;
	}
	
	public function get_all_items_after_filter($color_val,$size_val,$product_val){
		$color_val_arr=array();
		$size_val_arr=array();
		if(!empty($color_val)){
			$color_val_arr=explode("-",$color_val);
		}
		if(!empty($size_val)){
			$size_val_arr=explode("-",$size_val);
		}

		$where="where product_id='$product_val' ";
		if(count($color_val_arr)!=0 && count($size_val_arr)==0){
			$color_val_in="'".implode("','",$color_val_arr)."'";
			$color_arr=array();
			foreach($color_val_arr as $v){
				$color_arr[]="attribute_1_value like '%$v%'";
			}
			$color_or=implode(" or ",$color_arr);
			//$where.="and (attribute_1_value in ($color_val_in))";
			$where.="and ($color_or)";
		}
		if(count($color_val_arr)==0 && count($size_val_arr)!=0){
			$size_val_in="'".implode("','",$size_val_arr)."'";
			$where.="and (attribute_2_value in ($size_val_in))";
		}
		if(count($color_val_arr)!=0 && count($size_val_arr)!=0){
			$where.=" and ( 1=2 ";
			foreach($color_val_arr as $color){
				foreach($size_val_arr as $size){
					$where.=" or (attribute_1_value like '%$color%' and attribute_2_value='$size') ";
				}	
			}
			$where.=")";
		}
        $user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");
        
        
		 $sql="select * from inventory ";
         if($user_type=='vendor'){
			$sql.=' LEFT JOIN `vendor_catalog_inventory` ON `vendor_catalog_inventory`.`vendor_catalog_id` = `inventory`.`inv_vendor_catalog_id`';
		}
        $sql.=" $where and inventory_type!='2' ";

         if($user_type=='vendor'){
			$sql .=" and ";
			$sql .=" vendor_catalog_inventory.vendor_id='$a_id' ";
		}
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	public function update_promotion_deal_name($promo_id,$deal_name){
		$sql="update promotions set promo_name='$deal_name',promo_active=0 ,approval_sent=0 ,promo_approval=0, time_active=now(), time_approval=now(), time_approval_sent=now() where promo_uid='$promo_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	public function update_promotion_deal_quote($promo_id,$new_deal_quote,$buy_deal_value,$get_deal_value,$buy_deal_type,$get_deal_type,$endDate,$start_date,$promotion_at_check_out){
		$buy_deal_value=$this->input->post('buy_deal_value');
		$get_deal_value=$this->input->post('get_deal_value');

		$sql="update promotions set promo_quote='$new_deal_quote',to_buy='$buy_deal_value',to_get='$get_deal_value',buy_type='$buy_deal_type',get_type='$get_deal_type', 	promo_end_date='$endDate',promo_start_date='$start_date',applied_at_invoice='$promotion_at_check_out',promo_active=0 ,approval_sent=0 ,promo_approval=0, time_active=now(), time_approval=now(), time_approval_sent=now() where promo_uid='$promo_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function clear_promotion_items_taged_for_purchasing($promo_id){
		$sql="delete from promotion_items_taged_for_purchasing where promo_uid='$promo_id'";
		$result=$this->db->query($sql);
	}
	
	public function clear_promotion_items_taged_for_purchasing_free($promo_id){
		$sql="delete from promotion_items_taged_for_purchasing_free where promo_uid='$promo_id'";
		$result=$this->db->query($sql);
	}
	
	public function update_promotion_end_date_date($promo_id,$end_date){
		$sql="update promotions set promo_end_date='$end_date', promo_active=0 ,approval_sent=0 ,promo_approval=0, time_active=now(), time_approval=now(), time_approval_sent=now() where promo_uid='$promo_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function update_promotion_start_date($promo_id,$start_date,$end_date){
		$sql="update promotions set promo_start_date='$start_date',promo_end_date='$end_date',promo_active=0 ,approval_sent=0 ,promo_approval=0, time_active=now(), time_approval=now(), time_approval_sent=now() where promo_uid='$promo_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function activate_promotion($promo_id){
		$sql="update promotions set promo_active=1,activated_once=1,is_promo_editable='no' where promo_uid='$promo_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function save_as_draft_promotion($promo_id){
		$sql="update promotions set promo_active=0 ,approval_sent=0 ,promo_approval=0, time_active=now(), time_approval=now(), time_approval_sent=now() where promo_uid='$promo_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function save_as_draft_promotion_temp($promo_id){
		$sql="update promotions set promo_active=0 , time_active=now() where promo_uid='$promo_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function getEndDateDepend($id){
		$sql="select input_type_for_buy from promotion_quote where id='$id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->input_type_for_buy;
		}
		else{
			return false;
		}
	}
	
	public function count_total_promotion_for_buying($promo_uid){
		$sql="select * from promotion_items_taged_for_purchasing where promo_uid='$promo_uid'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->num_rows();
		}
		else{
			return 0;
		}
	}
	

	public function pending_promotions_processing($params,$fg){
		//define index of column
		$columns = array( 
			1 =>'promotions.id', 
			4 => 'promotions.promo_type',
			5 => 'promotions.promo_name',
			6 => 'promotions.promo_quote',
			7 => 'promotions.promo_end_date',
			8 => 'promotions.promo_start_date'
		);

		$where = $sqlTot = $sqlRec = "";
		$customer_id=$this->session->userdata('customer_id');
		// getting total number records without any search
		$sql = "SELECT promotions.*,promotion_type.promotion_name FROM `promotions`,promotion_type where promotions.approval_sent=1 and promotions.promo_approval='0' and promotions.promo_active='0' and promotion_type.id=promotions.promo_type and not promotions.promo_end_date < now()";
		// check search value exist
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( promotions.id LIKE '%".$params['search']['value']."%' ";
			$where .=" OR promotions.promo_type LIKE '%".$params['search']['value']."%' ";
			$where .=" OR promotions.promo_name LIKE '%".$params['search']['value']."%' ";
			$where .=" OR promotions.promo_quote LIKE '%".$params['search']['value']."%' ";
			$where .=" OR promotions.promo_start_date LIKE '%".$params['search']['value']."%' ";
			$where .=" OR promotion_type.promotion_name like '%".$params['search']['value']."%' ";
			$where .=" OR promotions.promo_end_date LIKE '%".$params['search']['value']."%' )";
		}
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}

		
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
	}
	
    public function approved_promotions_processing($params,$fg){
        //define index of column
        $columns = array( 
            1 =>'promotions.id', 
            4 => 'promotions.promo_type',
            5 => 'promotions.promo_name',
            6 => 'promotions.promo_quote',
            7 => 'promotions.promo_end_date',
            8 => 'promotions.promo_start_date'
        );

        $where = $sqlTot = $sqlRec = "";
        $customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT promotions.*,promotion_type.promotion_name FROM `promotions`,promotion_type where  promotions.   promo_approval=1 and promotions.approval_sent='1' and promotions.promo_active='0' and promotions.activated_once='0'  and promotion_type.id=promotions.promo_type and not promotions.promo_end_date < now()";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" ( promotions.id LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_type LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_name LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_quote LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_start_date LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotion_type.promotion_name like '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_end_date LIKE '%".$params['search']['value']."%' )";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }
    public function revertedDraft_promotions_processing($params,$fg){
        //define index of column
        $columns = array( 
            1 =>'promotions.id', 
            4 => 'promotions.promo_type',
            5 => 'promotions.promo_name',
            6 => 'promotions.promo_quote',
            7 => 'promotions.promo_end_date',
            8 => 'promotions.promo_start_date'
        );

        $where = $sqlTot = $sqlRec = "";
        $customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT promotions.*,promotion_type.promotion_name FROM `promotions`,promotion_type where  promotions.   promo_approval=0 and promotions.approval_sent='0' and promotions.promo_active='0' and promotions.activated_once='1'  and promotion_type.id=promotions.promo_type and not promotions.promo_end_date < now()";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" ( promotions.id LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_type LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_name LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_quote LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_start_date LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotion_type.promotion_name like '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_end_date LIKE '%".$params['search']['value']."%' )";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }
    
    public function rejected_promotions_processing($params,$fg){
        //define index of column
        $columns = array( 
            1 =>'promotions.id', 
            4 => 'promotions.promo_type',
            5 => 'promotions.promo_name',
            6 => 'promotions.promo_quote',
            7 => 'promotions.promo_end_date',
            8 => 'promotions.promo_start_date'
        );

        $where = $sqlTot = $sqlRec = "";
        $customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT promotions.*,promotion_type.promotion_name FROM `promotions`,promotion_type where  promotions.   promo_approval=0 and promotions.promo_active='0'  and  promotions.rejected_flag=1 and promotion_type.id=promotions.promo_type and not promotions.promo_end_date < now()";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" ( promotions.id LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_type LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_name LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_quote LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_start_date LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotion_type.promotion_name like '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_end_date LIKE '%".$params['search']['value']."%' )";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }
    
	public function draft_promotions_processing($params,$fg){
        //define index of column
        $columns = array( 
            1 =>'promotions.id', 
            4 => 'promotions.promo_type',
            5 => 'promotions.promo_name',
            6 => 'promotions.promo_quote',
            7 => 'promotions.promo_end_date',
            8 => 'promotions.promo_start_date'
        );

        $where = $sqlTot = $sqlRec = "";
        $customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT promotions.*,promotion_type.promotion_name FROM `promotions`,promotion_type where  promotions.   promo_approval=0 and promotions.approval_sent='0' and promotions.promo_active='0' and promotions.activated_once='0'  and promotion_type.id=promotions.promo_type and not promotions.promo_end_date < now()";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" ( promotions.id LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_type LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_name LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_quote LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_start_date LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotion_type.promotion_name like '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_end_date LIKE '%".$params['search']['value']."%' )";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }
	
	public function expired_promotions_processing($params,$fg){
        //define index of column
        $columns = array( 
            1 =>'promotions.id', 
            4 => 'promotions.promo_type',
            5 => 'promotions.promo_name',
            6 => 'promotions.promo_quote',
            7 => 'promotions.promo_end_date',
            8 => 'promotions.promo_start_date'
        );

        $where = $sqlTot = $sqlRec = "";
        $customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT promotions.*,promotion_type.promotion_name FROM `promotions`,promotion_type where  promotions.promo_end_date < now() and promotion_type.id=promotions.promo_type";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" ( promotions.id LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_type LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_name LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_quote LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_start_date LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotion_type.promotion_name like '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_end_date LIKE '%".$params['search']['value']."%' )";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }
    
	
	public function active_promotions_processing($params,$fg){
        //define index of column
        $columns = array( 
            0 =>'promotions.id', 
            1 => 'promotions.promo_type',
            2 => 'promotions.promo_name',
            3 => 'promotions.promo_quote',
            4 => 'promotions.promo_start_date',
            5 => 'promotions.promo_end_date'
        );

        $where = $sqlTot = $sqlRec = "";
        $customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT promotions.*,promotion_type.promotion_name FROM `promotions`,promotion_type where promotions.promo_active='1' and promotions.promo_approval='1' and promotions.approval_sent='1' and promotions.   activated_once='1' and promotion_type.id=promotions.promo_type and not promotions.promo_end_date < now()";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" ( promotions.id LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_type LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_name LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_quote LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_start_date LIKE '%".$params['search']['value']."%' ";
            $where .=" OR promotion_type.promotion_name like '%".$params['search']['value']."%' ";
            $where .=" OR promotions.promo_end_date LIKE '%".$params['search']['value']."%' )";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }
	
	public function deactive_promotions_processing($params,$fg){
		//define index of column
		$columns = array( 
			0 =>'promotions.id', 
			1 => 'promotions.promo_type',
			2 => 'promotions.promo_name',
			3 => 'promotions.promo_quote',
			4 => 'promotions.promo_start_date',
			5 => 'promotions.promo_end_date'
		);

		$where = $sqlTot = $sqlRec = "";
		$customer_id=$this->session->userdata('customer_id');
		// getting total number records without any search
		$sql = "SELECT promotions.*,promotion_type.promotion_name FROM `promotions`,promotion_type where promotions.promo_active='0' and promotions.promo_approval='1' and promotions.approval_sent='1' and promotions.activated_once='1' and promotion_type.id=promotions.promo_type and not promotions.promo_end_date < now()";
		// check search value exist
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( promotions.id LIKE '%".$params['search']['value']."%' ";
			$where .=" OR promotions.promo_type LIKE '%".$params['search']['value']."%' ";
			$where .=" OR promotions.promo_name LIKE '%".$params['search']['value']."%' ";
			$where .=" OR promotions.promo_quote LIKE '%".$params['search']['value']."%' ";
			$where .=" OR promotions.promo_start_date LIKE '%".$params['search']['value']."%' ";
			$where .=" OR promotion_type.promotion_name like '%".$params['search']['value']."%' ";
			$where .=" OR promotions.promo_end_date LIKE '%".$params['search']['value']."%' )";
		}
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}

		
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
	}
	
	
	public function update_promotion_log($promo_uid,$changes,$final_offer_value,$username,$promo_status){
		$sql="INSERT INTO promotion_chang_log (`id`, `promo_uid`, `changes`, `commit`, `user_id`, `timestamp`,`promo_status`) VALUES (NULL, '$promo_uid', '$changes', '$final_offer_value', '$username', CURRENT_TIMESTAMP,'$promo_status')";
		$result=$this->db->query($sql);
		if($this->db->affected_rows()){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function get_all_log_data_of_promotion($promo_id){
		$sql="select * from promotion_chang_log where promo_uid='$promo_id' order by timestamp desc";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function getcurrentpromodata($promo_id){
		$sql="select * from promotions where promo_uid='$promo_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function send_approval_for_promotion($promo_id,$temp_email_num,$su_email){
		$sql="update promotions set approval_sent=1,rejected_flag=0, time_approval_sent=now(),temp_email_num='$temp_email_num',su_email='$su_email',activated_once=0 where promo_uid='$promo_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function cancel_promotion_approval_request($promo_id){
	    $sql="update promotions set approval_sent=0, time_approval_sent='' where promo_uid='$promo_id'";
        $result=$this->db->query($sql);
        if($this->db->affected_rows() > 0){
            return true;
        }
        else{
            return false;
        }
	}
    
    public function revertdraft_promotion_num(){
        $sql="select * from promotions where  promotions.promo_approval=0 and promotions.approval_sent='0' and promotions.promo_active='0' and promotions.activated_once='1' and not promotions.promo_end_date < now()";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->num_rows();
            }
            else{
                return 0;
            }
    }
    public function expired_promotion_num(){
        $sql="select * from promotions where promotions.promo_end_date < now()";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->num_rows();
            }
            else{
                return 0;
            }
    }
    public function active_promotion_num(){
        $sql="select * from promotions where  promotions.promo_approval=1 and promotions.approval_sent='1' and promotions.promo_active='1' and promotions.activated_once='1' and not promotions.promo_end_date < now()";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->num_rows();
            }
            else{
                return 0;
            }
    }
    public function draft_promotion_num(){
        $sql="select * from promotions where  promotions.promo_approval=0 and promotions.approval_sent='0' and promotions.promo_active='0' and promotions.activated_once='0' and not promotions.promo_end_date < now()";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->num_rows();
            }
            else{
                return 0;
            }
    }
    public function deactivated_promotion_num(){
        $sql="select * from promotions where  promotions.promo_approval=1 and promotions.approval_sent='1' and promotions.promo_active='0' and promotions.activated_once='1' and not promotions.promo_end_date < now()";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->num_rows();
            }
            else{
                return 0;
            }
    }
    public function pending_promotion_num(){
        $sql="select * from promotions where  promotions.promo_approval=0 and promotions.approval_sent='1' and promotions.promo_active='0' and not promotions.promo_end_date < now()";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->num_rows();
            }
            else{
                return 0;
            }
    }
    public function approved_promotion_num(){
        $sql="select * from promotions where  promotions.promo_approval=1 and promotions.approval_sent='1' and promotions.promo_active='0' and promotions.activated_once='0' and not promotions.promo_end_date < now()";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->num_rows();
            }
            else{
                return 0;
            }
    }
    public function rejected_promotions_num(){
        $sql="select * from promotions where  promotions.promo_approval=0  and promotions.promo_active='0' and promotions.rejected_flag='1' and promotions.activated_once='0' and not promotions.promo_end_date < now()";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
                return $result->num_rows();
            }
            else{
                return 0;
            }
    }
    public function get_su_admin_email(){
        $sql="select email from admin_country where user_type='Su Master Country'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->row()->email;
        }
        else{
            return false;
        }
    }
	
	public function get_is_promo_editable_in_promo_master_data($promo_uid){
		$sql="select is_promo_editable from promotions where promo_uid='$promo_uid'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row_array()["is_promo_editable"];
		}
		else{
			return "yes";
		}
	}
    public function get_moq_of_invs($level,$promo_uid,$parent_category='',$category='',$sub_category='',$attributes='',$attributes_selector='',$products_tagged_for_purchasing=''){

	//echo $promo_uid;

        //$level== 1;
        if(1){
            /* store */

            //echo $promo_uid;

            $sql="select * from promotions ";
            
            $sql.="where 
            promotions.buy_type='Qty' and get_type LIKE '%".'%'."%' and 
             now() between promotions.promo_start_date and promotions.promo_end_date and promotions.promo_uid='$promo_uid'";

             //echo $sql;

            $result=$this->db->query($sql);

            if($result->num_rows()>0){

                $res=$result->row();

                //print_r($res);

                $buy_qty=0;
                if(!empty($res)){
                        $buy_qty=$res->to_buy;
                        $sql_1 = "select inventory.sku_id,inventory.moq,inventory.approval_status as publish_status,inventory.status_updated_on as status_on from inventory ";
        
                        $sql_1.= ' LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`'
                                .' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`'
                                .' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`'
                                .' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`'
                                .' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id` ';

                        $user_type=$this->session->userdata("user_type");
                        $a_id=$this->session->userdata("user_id");
                        if($user_type=='vendor'){
                            $sql_1.=' LEFT JOIN `vendor_catalog_inventory` ON `vendor_catalog_inventory`.`vendor_catalog_id` = `inventory`.`inv_vendor_catalog_id`';
                        }
                        $sql_1.=" where  inventory.active='1' and inventory.moq>'$buy_qty' and inventory_type!='2' ";

                        if($user_type=='vendor'){
                            $sql_1 .=" and ";
                            $sql_1 .=" vendor_catalog_inventory.vendor_id='$a_id' ";
                        }

                        if($parent_category!= ''){
                            $sql_1.= " and `parent_category`.`pcat_id`='$parent_category' ";
                        }
                         if($category!= ''){
                            $sql_1.= " and `category`.`cat_id`='$category' ";
                        }
                        if($sub_category!= ''){
                            $sql_1.= " and `subcategory`.`subcat_id`='$sub_category' ";
                        }
                        
                        if($attributes!= ''){
                            $sql_1.= " and `brands`.`brand_id`='$attributes' ";
                        }
                        if($attributes_selector!= ''){
                            $sql_1.= " and `products`.`product_id`='$attributes_selector' ";
                        }


                        if($products_tagged_for_purchasing!=''){
                            $inv_id_arr=explode(',',$products_tagged_for_purchasing);

                            $sql_1.= " and inventory.id in ($products_tagged_for_purchasing)";
                            //$sql_1.= " and find_in_set(`inventory`.`id`,$products_tagged_for_purchasing)>0 ";
                        }
                        //brand_id='$attributes' and product_id='$attributes_selector'
                                
                        //echo $sql_1;

                        $result_1=$this->db->query($sql_1);
                        if($result_1->num_rows()>0){
                            return $result_1->result_array();
                        }
                }

             }
             else{
                  return false;
             }

        }

    }

}
?>