<?php
class Model_catalogue extends CI_Model {
	function __construct() {
		$this -> load -> library('pagination');
		//$this -> load -> library('encryption');
		$this->load->library('My_PHPMailer');
		$this -> load -> library('session');
		$this -> load -> database();
	}
	public function parent_category() {
		$query = $this->db->query("SELECT * FROM `parent_category` where active=1");
		return $query -> result();
	}
	public function get_all_brand_names() {
		$query = $this->db->query("SELECT distinct brand_name FROM `brands` where active=1");
		return $query -> result();
	}
	public function get_all_external_inventories(){
		$query = $this->db->query("SELECT * FROM `external_inventory` order by external_inventory_id desc");
		return $query -> result();
	}
	public function reason_return() {
		$query = $this->db->query("SELECT * FROM `reason_return`");
		return $query -> result();
	}
	public function archived_parent_category() {
		$query = $this->db->query("SELECT * FROM `parent_category` where active=0");
		return $query -> result();
	}
	public function add_parent_category($pcat_name,$menu_level,$show_sub_menu,$menu_sort_order,$view,$combo_pack_status){
		$insertinfo = array("pcat_name" => $pcat_name,"menu_level" => $menu_level, "show_sub_menu" => $show_sub_menu, "menu_sort_order" => $menu_sort_order,"view" => $view,"combo_pack_status"=>$combo_pack_status);
		$flag=$this -> db -> insert("parent_category", $insertinfo);
		return $flag;
	}
	public function get_parent_category_data($pcat_id){
		$query =$this->db->query("SELECT * FROM `parent_category` where pcat_id='$pcat_id' and active=1");
		return $query->row_array();		
	}
	public function parent_category_edit_cat($pcat_id){
		$query =$this->db->query("SELECT * FROM `parent_category` where pcat_id!='$pcat_id' and active=1");
		return $query-> result();		
	}
	public function edit_parent_category($pcat_id,$pcat_name,$menu_level,$show_sub_menu,$menu_sort_order,$view,$type,$show_sort_order,$common_cat_id,$combo_pack_status){
		if($type=="pcat"){
			if($common_cat_id!=""){
				$sql="SELECT * FROM `parent_category` Where pcat_id='$common_cat_id'";
				$query =$this->db->query($sql);
				$menuto_sort_order=$query->row_array()["menu_sort_order"];
				$sql="SELECT * FROM `parent_category` Where pcat_id='$pcat_id'";
				$query =$this->db->query($sql);
				$menufrom_sort_order=$query->row_array()["menu_sort_order"];
				
				$sql = "UPDATE `parent_category` SET menu_sort_order='$menuto_sort_order' WHERE `pcat_id` = '$pcat_id'";
				$result=$this->db->query($sql);
				
				$sql = "UPDATE `parent_category` SET menu_sort_order='$menufrom_sort_order' WHERE `pcat_id` = '$common_cat_id'";
				$result=$this->db->query($sql);
			}
			
			$sql = "UPDATE `parent_category` SET pcat_name='$pcat_name',menu_level='$menu_level',show_sub_menu='$show_sub_menu',view='$view',combo_pack_status='$combo_pack_status' WHERE `pcat_id` = '$pcat_id'";
			$result=$this->db->query($sql);
			
		}
		
		if($type=="cat"){
			$sql="SELECT * FROM `category` Where cat_id='$common_cat_id'";
			$query =$this->db->query($sql);
			$menuto_sort_order=$query->row_array()["menu_sort_order"];
			
			$sql = "UPDATE `parent_category` SET menu_sort_order='$menuto_sort_order' WHERE `pcat_id` = '$pcat_id'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `category` SET menu_sort_order='$menu_sort_order' WHERE `cat_id` = '$common_cat_id'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `parent_category` SET pcat_name='$pcat_name',menu_level='$menu_level',show_sub_menu='$show_sub_menu',view='$view' WHERE `pcat_id` = '$pcat_id'";
			$result=$this->db->query($sql);
		}
		if($type=="" && $show_sort_order!=""){
						
			$sql = "UPDATE `parent_category` SET menu_sort_order='$show_sort_order' WHERE `pcat_id` = '$pcat_id'";
			$result=$this->db->query($sql);
			$sql = "UPDATE `parent_category` SET pcat_name='$pcat_name',menu_level='$menu_level',show_sub_menu='$show_sub_menu',view='$view' WHERE `pcat_id` = '$pcat_id'";
			$result=$this->db->query($sql);
			
		}
		if($type=="" && $show_sort_order==""){
			$sql = "UPDATE `parent_category` SET pcat_name='$pcat_name',menu_level='$menu_level',show_sub_menu='$show_sub_menu',menu_sort_order='$menu_sort_order',view='$view' WHERE `pcat_id` = '$pcat_id'";
			$result=$this->db->query($sql);
		}
		
		return $result;
	}
	public function delete_parent_category_selected($selected_list){
		$sql_update="update parent_category set menu_sort_order=0,view=0,active=0 where pcat_id in ($selected_list)";
		$query1 =$this->db->query($sql_update);
		$sql_update_category = "UPDATE `category` SET pcat_id=0,menu_level=1,menu_sort_order=0,view=0 WHERE pcat_id in ($selected_list)";
		$query2 = $this->db->query($sql_update_category);
		$sql_update_subcategory = "UPDATE `subcategory` SET pcat_id=0 WHERE pcat_id in ($selected_list)";
		$query3 = $this->db->query($sql_update_subcategory);
		$sql_update_brands = "UPDATE `brands` SET pcat_id=0 WHERE pcat_id in ($selected_list)";
		$query4 = $this->db->query($sql_update_brands);
		$sql_update_products = "UPDATE `products` SET pcat_id=0 WHERE pcat_id in ($selected_list)";
		$query5 = $this->db->query($sql_update_products);
		$sql_update_attr = "UPDATE `attribute` SET pcat_id=0 WHERE pcat_id in ($selected_list)";
		$query6 = $this->db->query($sql_update_attr);
		$sql_update_filterbox = "UPDATE `filterbox_to_category` SET pcat_id=0 WHERE pcat_id in ($selected_list)";
		$query7 = $this->db->query($sql_update_filterbox);
		$sql_update_speci_group = "UPDATE `specification_group_to_categories` SET pcat_id=0 WHERE pcat_id in ($selected_list)";
		$query8 = $this->db->query($sql_update_speci_group);
		
		if($query1==true && $query2==true && $query3==true && $query4==true && $query5==true && $query6==true && $query7==true && $query8==true){
			return true;
		}else{
			return false;
		}	
	}
	public function get_product_details($product_id){
		$sql="select * from products where product_id='$product_id'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	public function get_reasons_details($reason_id){
		$sql="select * from reason_return where reason_id='$reason_id'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	public function get_inventory_details($inventory_id){
		$sql="select * from inventory where id='$inventory_id'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	public function get_inventory_positioning($inventory_id){
		$sql="select * from positioning where position_inventory_id='$inventory_id'";
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function get_inventory_details_result($product_id='',$pcat_id='',$cat_id='',$subcat_id='',$brand_id=''){
		$sql="select sku_id, attribute_1, attribute_1_value, attribute_2, attribute_2_value, attribute_3, attribute_3_value, attribute_4, attribute_4_value, tax_percent, base_price, selling_price, purchased_price, mrp_quantity, moq, moq_base_price, moq_price, moq_purchased_price, max_oq, num_sales, stock, low_stock, cutoff_stock from inventory ";

		$sql.=' LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`';
		/*$sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`';
		$sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`';
		$sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id`';
		$sql.=' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`';*/

		$where='';
		if($product_id!='' || $pcat_id!='' || $cat_id!=''|| $subcat_id!=''  || $brand_id!=''){

			$where= " where " ;

			if($product_id!='') {
				$where.="products.product_id='$product_id' ";
				if($pcat_id!='' || $cat_id!=''|| $subcat_id!=''  || $brand_id!='') {
					$where .= " AND ";
				}
			}

			if($pcat_id!='') {
				$where .= "products.pcat_id='$pcat_id'";
				if($cat_id!=''|| $subcat_id!=''  || $brand_id!='') {
					$where .= " AND ";
				}
			}
			if($cat_id!='') {
				$where .= " products.cat_id='$cat_id'";
				if($subcat_id!=''  || $brand_id!='') {
					$where .= " AND ";
				}
			}
			if($subcat_id!='') {
				$where .= " products.subcat_id='$subcat_id'";
				if( $brand_id!='') {
					$where .= " AND ";
				}
			}
			if($brand_id!='') {
				$where .= " products.brand_id='$brand_id'";

			}
		}
		$sql=$sql.$where;

		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function get_inventory_details_result_new($product_id='',$pcat_id='',$cat_id='',$subcat_id='',$brand_id=''){
		$sql="select inventory.product_id,sku_id,inventory.id, attribute_1, attribute_1_value, attribute_2, attribute_2_value, attribute_3, attribute_3_value, attribute_4, attribute_4_value, tax_percent, base_price, selling_price,selling_discount,CGST,IGST,SGST, purchased_price, mrp_quantity, moq, moq_base_price, moq_price, moq_purchased_price, max_oq, num_sales, stock, low_stock, cutoff_stock from inventory ";

		$sql.=' LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`';
		/*$sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`';
		$sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`';
		$sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id`';
		$sql.=' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`';*/

		$where='';
		if($product_id!='' || $pcat_id!='' || $cat_id!=''|| $subcat_id!=''  || $brand_id!=''){

			$where= " where " ;

			if($product_id!='') {
				$where.="products.product_id='$product_id' ";
				if($pcat_id!='' || $cat_id!=''|| $subcat_id!=''  || $brand_id!='') {
					$where .= " AND ";
				}
			}

			if($pcat_id!='') {
				$where .= "products.pcat_id='$pcat_id'";
				if($cat_id!=''|| $subcat_id!=''  || $brand_id!='') {
					$where .= " AND ";
				}
			}
			if($cat_id!='') {
				$where .= " products.cat_id='$cat_id'";
				if($subcat_id!=''  || $brand_id!='') {
					$where .= " AND ";
				}
			}
			if($subcat_id!='') {
				$where .= " products.subcat_id='$subcat_id'";
				if( $brand_id!='') {
					$where .= " AND ";
				}
			}
			if($brand_id!='') {
				$where .= " products.brand_id='$brand_id'";

			}
		}
		$sql=$sql.$where;

		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function get_inventory_details_result_selected($selected_list){

		$sql="select inventory.id,inventory.product_id,sku_id, attribute_1, attribute_1_value, attribute_2, attribute_2_value, attribute_3, attribute_3_value, attribute_4, attribute_4_value, tax_percent, base_price, selling_price,selling_discount,CGST,IGST,SGST, purchased_price, mrp_quantity, moq, moq_base_price, moq_price, moq_purchased_price, max_oq, num_sales, stock, low_stock, cutoff_stock from inventory ";

		$sql.=' LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`';
		/*$sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`';
		$sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`';
		$sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id`';
		$sql.=' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`';*/
		$where='';
		if(!empty($selected_list)){
			$where=" where inventory.id in ($selected_list)";
		}
		
		$sql=$sql.$where;

		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function get_product_details_result($product_id){

		$sql = "select products.product_name,products.product_description,subcategory.subcat_name,category.cat_name,parent_category.pcat_name,brands.brand_name from products ";
		$sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`';
		$sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`';
		$sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id`';
		$sql.=' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`';
		$sql.=" where product_id='$product_id'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	public function update_parent_category_selected($selected_list,$active){
		
		$sql_update="update parent_category set menu_sort_order=0,view=0,active='$active' where pcat_id in ($selected_list)";
		$flag1=$this->db->query($sql_update);		
		$sql_update_category = "UPDATE `category` SET active='$active' WHERE pcat_id in ($selected_list)";
		$flag2=$this->db->query($sql_update_category);
		$sql_update_subcategory = "UPDATE `subcategory` SET active='$active' WHERE pcat_id in ($selected_list)";
		$flag3=$this->db->query($sql_update_subcategory);
		$sql_update_brands = "UPDATE `brands` SET active='$active' WHERE pcat_id in ($selected_list)";
		$flag4=$this->db->query($sql_update_brands);
		$sql_select_product="select * from products where pcat_id in ($selected_list)";
		$query_product=$this->db->query($sql_select_product);
		$product_info = $query_product -> result();
		if(count($product_info)>0){
			$product_id_arr=array();
			foreach($product_info as $product){
				$product_id_arr[]=$product->product_id;
			}
			$product_ids_in=implode(',',$product_id_arr);
			$sql_update_product="UPDATE `products` SET active='$active' WHERE product_id in ($product_ids_in)";
			$flag=$this->db->query($sql_update_product);			
			$sql_inventory="select * from inventory where product_id in ($product_ids_in)";
			$query_inventory = $this->db->query($sql_inventory);			
			$inventory_info = $query_inventory -> result();
			if(count($inventory_info)>0){
				$inventory_id_arr=array();
				foreach($inventory_info as $inventory){
					$inventory_id_arr[]=$inventory->id;
				}
				$inventory_id_in=implode(',',$inventory_id_arr);
				$res_inv=$this->update_inventory_selected($inventory_id_in,$active);	
			}else{
				$res_inv=true;		
			}
			if($flag==true && $res_inv==true){
				$flag_product_update=true;
			}else{
				$flag_product_update=false;
			}	
		}else{
			$flag_product_update=true;
		}
		
		$sql_select_attr="select * from attribute where pcat_id in ($selected_list)";
		$query_attr=$this->db->query($sql_select_attr);
		$attr_info = $query_attr -> result();
		if(count($attr_info)>0){
			$attribute_id_arr=array();
			foreach($attr_info as $attr) {
				$attribute_id_arr[]=$attr->attribute_id;
			}
			$attribute_ids_in=implode(',',$attribute_id_arr);
			$flag_attribute=$this->update_attribute_selected($attribute_ids_in,$active);
		}else{
			$flag_attribute=true;
		}
		
		$sql_specification_group="select * from specification_group_to_categories where pcat_id in ($selected_list)";
		$query_spec_group=$this->db->query($sql_specification_group);
		$spec_group_info = $query_spec_group->result();
		if(count($spec_group_info)>0){
			$spec_group_id_arr=array();
			foreach($spec_group_info as $spec_group) {
				$spec_group_id_arr[]=$spec_group->specification_group_id;
			}
			$spec_group_ids_in=implode(',',$spec_group_id_arr);
			$flag_spec_group=$this->update_specification_group_selected($spec_group_ids_in,$active);
		}else{
			$flag_spec_group=true;
		}
		
		$sql_filterbox="select * from filterbox_to_category where subcat_id in ($selected_list)";
		$query_filterbox=$this->db->query($sql_filterbox);
		$filterbox_info = $query_filterbox -> result();
		if(count($filterbox_info)>0){
			$filterbox_id_arr=array();
			foreach($filterbox_info as $filterbox) {
				$filterbox_id_arr[]=$filterbox->filterbox_id;
			}
			$filterbox_info_ids_in=implode(',',$filterbox_id_arr);
			$flag_filterbox=$this->update_filterbox_selected($filterbox_info_ids_in,$active);
		}else{
			$flag_filterbox=true;
		}
		
		if($flag1==true && $flag2==true && $flag3==true && $flag4==true && $flag_product_update==true && $flag_attribute==true && $flag_spec_group==true && $flag_filterbox==true){
			return true;
		}else{
			return false;
		}	
	}
	public function delete_parent_category_no_data_selected($selected_list){
		$sql="select count(*) from category where pcat_id in ($selected_list)";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$count = $result['count(*)'];
		if($count==0){
			$sql_delete="delete from parent_category where pcat_id in ($selected_list)";
			$flag =$this->db->query($sql_delete);
			return $flag;
		}else{
			return 2;
		}
	}
	public function category(){
		$sql="select category.*,parent_category.pcat_id,parent_category.pcat_name from category,parent_category where category.pcat_id=parent_category.pcat_id and parent_category.active=1 and category.active=1 union select category.*,parent_category.pcat_id,parent_category.pcat_name from category,parent_category where category.pcat_id=0 and parent_category.active=1 and category.active=1";		
		$query = $this->db->query($sql);

		$c=count($query -> result());
		if($c==0){
			$sql="select category.* from category where category.active=1";		
			$query = $this->db->query($sql);
		}
		return $query -> result();
	}
	public function get_category_data($pcat_id,$cat_id){
		$sql="SELECT * FROM `category` Where pcat_id='$pcat_id' and cat_id='$cat_id' and active=1";
		$query =$this->db->query($sql);
		return $query->row_array();	
	}
	public function add_category($cat_name,$pcat_id,$menu_level,$menu_sort_order,$view){
		$insertinfo = array("cat_name" => $cat_name,"pcat_id" => $pcat_id,"menu_level" => $menu_level, "menu_sort_order" => $menu_sort_order,"view" => $view);
		$flag=$this -> db -> insert("category", $insertinfo);
		return $flag;
	}
	public function edit_category($cat_id,$cat_name,$pcat_id,$menu_level,$menu_sort_order,$view,$type,$common_cat_id){
		if($type=="pcat"){
			
			$sql="SELECT * FROM `parent_category` Where pcat_id='$common_cat_id'";
			$query =$this->db->query($sql);
			$menuto_sort_order=$query->row_array()["menu_sort_order"];
			
			$sql="SELECT * FROM `category` Where cat_id='$cat_id'";
			$query =$this->db->query($sql);
			$menufrom_sort_order=$query->row_array()["menu_sort_order"];
			
			$sql = "UPDATE `parent_category` SET menu_sort_order='$menufrom_sort_order' WHERE `pcat_id` = '$common_cat_id'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `category` SET menu_sort_order='$menuto_sort_order' WHERE `cat_id` = '$cat_id'";
			$result=$this->db->query($sql);
			
			$sql_select="select pcat_id from category where cat_id='$cat_id'";
		$query_res=$this->db->query($sql_select);
		$old_pcat_id=$query_res->row()->pcat_id;
		
		$sql = "UPDATE `category` SET cat_name='$cat_name',pcat_id='$pcat_id',menu_level='$menu_level',view='$view' WHERE `cat_id` = '$cat_id'";
		$query = $this->db->query($sql);
		
		if($pcat_id!=0){	
		if($pcat_id!=$old_pcat_id){
			
			$query_flag=$this->update_all_pcat_id_by_cat_id($cat_id,$pcat_id);
			}
		else{
			$query_flag=true;			
		}
		}else{
			$query_flag=$this->update_all_pcat_id_by_cat_id($cat_id,$pcat_id);			
		}
		if($query_flag==true && $query==true){
			return true;
		}else{
			return false;
		}
		}
		
		if($type=="cat"){
			$sql="SELECT * FROM `category` Where cat_id='$common_cat_id'";
			$query =$this->db->query($sql);
			$menuto_sort_order=$query->row_array()["menu_sort_order"];
			
			$sql="SELECT * FROM `category` Where cat_id='$cat_id'";
			$query =$this->db->query($sql);
			$menufrom_sort_order=$query->row_array()["menu_sort_order"];
			
			$sql = "UPDATE `category` SET menu_sort_order='$menuto_sort_order' WHERE `cat_id` = '$cat_id'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `category` SET menu_sort_order='$menufrom_sort_order' WHERE `cat_id` = '$common_cat_id'";
			$result=$this->db->query($sql);
			
			$sql_select="select pcat_id from category where cat_id='$cat_id'";
		$query_res=$this->db->query($sql_select);
		$old_pcat_id=$query_res->row()->pcat_id;
		
		$sql = "UPDATE `category` SET cat_name='$cat_name',pcat_id='$pcat_id',menu_level='$menu_level',view='$view' WHERE `cat_id` = '$cat_id'";
		$query = $this->db->query($sql);
		
		if($pcat_id!=0){	
		if($pcat_id!=$old_pcat_id){
			
			$query_flag=$this->update_all_pcat_id_by_cat_id($cat_id,$pcat_id);
			}
		else{
			$query_flag=true;			
		}
		}else{
			$query_flag=$this->update_all_pcat_id_by_cat_id($cat_id,$pcat_id);			
		}
		if($query_flag==true && $query==true){
			return true;
		}else{
			return false;
		}
		}
		if($type==""){
			$sql_select="select pcat_id from category where cat_id='$cat_id'";
		$query_res=$this->db->query($sql_select);
		$old_pcat_id=$query_res->row()->pcat_id;
		
		$sql = "UPDATE `category` SET cat_name='$cat_name',pcat_id='$pcat_id',menu_level='$menu_level',menu_sort_order='$menu_sort_order',view='$view' WHERE `cat_id` = '$cat_id'";
		$query = $this->db->query($sql);
			
		if($pcat_id!=0){	
		if($pcat_id!=$old_pcat_id){
			
			$query_flag=$this->update_all_pcat_id_by_cat_id($cat_id,$pcat_id);
			}
		else{
			$query_flag=true;			
		}
		}else{
			$query_flag=$this->update_all_pcat_id_by_cat_id($cat_id,$pcat_id);			
		}
		if($query_flag==true && $query==true){
			return true;
		}else{
			return false;
		}
		}
		
	}
	public function update_all_pcat_id_by_cat_id($cat_id,$pcat_id){
		$sql_update1="UPDATE `subcategory` SET pcat_id='$pcat_id' WHERE cat_id='$cat_id'";
		$flag1=$this->db->query($sql_update1);
		$sql_update2="UPDATE `brands` SET pcat_id='$pcat_id' WHERE cat_id='$cat_id'";
		$flag2=$this->db->query($sql_update2);
		$sql_update3="UPDATE `products` SET pcat_id='$pcat_id' WHERE cat_id='$cat_id'";
		$flag3=$this->db->query($sql_update3);
		$sql_update4="UPDATE `attribute` SET pcat_id='$pcat_id' WHERE cat_id='$cat_id'";
		$flag4=$this->db->query($sql_update4);
		$sql_update5="UPDATE `specification_group_to_categories` SET pcat_id='$pcat_id' WHERE cat_id='$cat_id'";
		$flag5=$this->db->query($sql_update5);
		$sql_update6="UPDATE `filterbox_to_category` SET pcat_id='$pcat_id' WHERE cat_id='$cat_id'";
		$flag6=$this->db->query($sql_update6);
		
		if($flag1==true && $flag2==true && $flag3==true && $flag4==true && $flag5==true && $flag6==true){
			return true;
		}else{
			return false;
		}
		
	}	
	public function update_category_selected($selected_list,$active){
		
		$sql_update_cat="UPDATE `category` SET menu_sort_order=0,view=0,active='$active' WHERE cat_id in ($selected_list)";
		$flag1=$this->db->query($sql_update_cat);
		$sql_update2="UPDATE `subcategory` SET active='$active' WHERE cat_id in ($selected_list)";
		$flag2=$this->db->query($sql_update2);
		$sql_update3="UPDATE `brands` SET active='$active' WHERE cat_id in ($selected_list)";
		$flag3=$this->db->query($sql_update3);
		$sql_select_product="select * from products where cat_id in ($selected_list)";
		$query_product=$this->db->query($sql_select_product);
		$product_info = $query_product -> result();
		if(count($product_info)>0){
			$product_id_arr=array();
			foreach($product_info as $product) {
				$product_id_arr[]=$product->product_id;
			}
			$product_ids_in=implode(',',$product_id_arr);
			$sql_update_product="UPDATE `products` SET active='$active' WHERE product_id in ($product_ids_in)";
			$flag=$this->db->query($sql_update_product);			
			$sql_inventory="select * from inventory where product_id in ($product_ids_in)";
			$query_inventory = $this->db->query($sql_inventory);			
			$inventory_info = $query_inventory -> result();
			if(count($inventory_info)>0){
				$inventory_id_arr=array();
				foreach($inventory_info as $inventory){
					$inventory_id_arr[]=$inventory->id;
				}
				$inventory_id_in=implode(',',$inventory_id_arr);
				$res_inv=$this->update_inventory_selected($inventory_id_in,$active);	
			}else{
				$res_inv=true;		
			}
			if($flag==true && $res_inv==true){
				$flag_product_update=true;
			}else{
				$flag_product_update=false;
			}	
		}else{
			$flag_product_update=true;
		}
		
		$sql_select_attr="select * from attribute where cat_id in ($selected_list)";
		$query_attr=$this->db->query($sql_select_attr);
		$attr_info = $query_attr -> result();
		if(count($attr_info)>0){
			$attribute_id_arr=array();
			foreach($attr_info as $attr) {
				$attribute_id_arr[]=$attr->attribute_id;
			}
			$attribute_ids_in=implode(',',$attribute_id_arr);
			$flag_attribute=$this->update_attribute_selected($attribute_ids_in,$active);
		}else{
			$flag_attribute=true;
		}
		
		$sql_specification_group="select * from specification_group_to_categories where cat_id in ($selected_list)";
		$query_spec_group=$this->db->query($sql_specification_group);
		$spec_group_info = $query_spec_group->result();
		if(count($spec_group_info)>0){
			$spec_group_id_arr=array();
			foreach($spec_group_info as $spec_group) {
				$spec_group_id_arr[]=$spec_group->specification_group_id;
			}
			$spec_group_ids_in=implode(',',$spec_group_id_arr);
			$flag_spec_group=$this->update_specification_group_selected($spec_group_ids_in,$active);
		}else{
			$flag_spec_group=true;
		}
		
		$sql_filterbox="select * from filterbox_to_category where cat_id in ($selected_list)";
		$query_filterbox=$this->db->query($sql_filterbox);
		$filterbox_info = $query_filterbox -> result();
		if(count($filterbox_info)>0){
			$filterbox_id_arr=array();
			foreach($filterbox_info as $filterbox) {
				$filterbox_id_arr[]=$filterbox->filterbox_id;
			}
			$filterbox_info_ids_in=implode(',',$filterbox_id_arr);
			$flag_filterbox=$this->update_filterbox_selected($filterbox_info_ids_in,$active);
		}else{
			$flag_filterbox=true;
		}
		
		if($flag1==true && $flag2==true && $flag3==true && $flag_product_update==true && $flag_attribute==true && $flag_spec_group==true && $flag_filterbox==true){
			return true;
		}else{
			return false;
		}
	}
	public function delete_category_when_no_data($selected_list){
		
		$sql="select count(*) from subcategory where cat_id in ($selected_list)";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$count = $result['count(*)'];
		if($count==0){
			$sql_delete="delete from category where cat_id in ($selected_list)";
			$flag =$this->db->query($sql_delete);
			return $flag;		
		}else{
			return 2;
		}
	}
	public function subcategories(){
		$sql="select subcategory.*,category.cat_name,category.cat_id from category,subcategory where category.cat_id=subcategory.cat_id and subcategory.active=1 and category.active=1";		
		$query = $this->db->query($sql);		
		return $query -> result();
	}
	public function show_available_categories($pcat_id,$active){
		$active_str=($active!=2)? "and active='$active'" :"";
		$sql="select cat_id,cat_name,active from category where trim(pcat_id) = '$pcat_id'  $active_str";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_subcategories($cat_id,$active){
		$active_str=($active!=2)? "and active='$active'" :"";
		$sql="select subcat_id,subcat_name,active from subcategory where trim(cat_id) = trim('$cat_id') $active_str";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_subcategories_for_filterbox($cat_id,$active){
		$active_str=($active!=2)? "and active='$active'" :"";
		$sql="select subcat_id,subcat_name,active from subcategory where trim(cat_id) = trim('$cat_id') $active_str";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function show_available_brands($subcat_id,$active){
		$user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");

		$active_str=($active!=2)? "and active='$active'" :"";
		$sql="select brand_id,brand_name,active from brands where trim(subcat_id) = '$subcat_id' $active_str";

		if($user_type=='vendor'){
			$sql.=" and vendor_id='$a_id'";
		}

		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_products($brand_id,$active){
		$active_str=($active!=2)? "and active='$active'" :"";
		$sql="select product_id,product_name,active from  products where trim(brand_id) = '$brand_id' $active_str";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_filterbox($arr,$active){
		extract($arr);
		$active_str=($active!=2)? "and filterbox_to_category.active='$active'" :"";
		/*
		if($type=="subcat"){
			
			$sql="select filterbox.filterbox_id,filterbox.filterbox_name,filterbox_to_category.active from  filterbox,filterbox_to_category where filterbox_to_category.subcat_id = '$id' and filterbox_to_category.cat_id!=0 and filterbox_to_category.pcat_id!=0 and filterbox_to_category.filterbox_id=filterbox.filterbox_id $active_str";
		}
		if($type=="cat"){
			
			$sql="select filterbox.filterbox_id,filterbox.filterbox_name,filterbox_to_category.active from  filterbox,filterbox_to_category where filterbox_to_category.cat_id = '$id' and filterbox_to_category.subcat_id=0 and filterbox_to_category.pcat_id!=0 and filterbox_to_category.filterbox_id=filterbox.filterbox_id $active_str";
		}
		if($type=="pcat"){
			
			$sql="select filterbox.filterbox_id,filterbox.filterbox_name,filterbox_to_category.active from  filterbox,filterbox_to_category where filterbox_to_category.pcat_id = '$id' and filterbox_to_category.cat_id=0 and filterbox_to_category.subcat_id=0 and filterbox_to_category.filterbox_id=filterbox.filterbox_id $active_str";
		}
*/

		if($type=="subcat"){
			
			 $sql="select filterbox_to_category.*,filterbox.* from filterbox_to_category,filterbox where filterbox_to_category.filterbox_id=filterbox.filterbox_id and ((filterbox_to_category.subcat_id='$subcat_id' and cat_id='$cat_id' and pcat_id='$pcat_id') or (filterbox_to_category.cat_id='$cat_id'   and subcat_id='0' and pcat_id='$pcat_id') or (filterbox_to_category.pcat_id='$pcat_id' and cat_id='0' and subcat_id='0'))";
		}
		if($type=="cat"){
			
			$sql="select filterbox_to_category.*,filterbox.* from filterbox_to_category,filterbox where filterbox_to_category.filterbox_id=filterbox.filterbox_id and ((filterbox_to_category.cat_id='$cat_id'  and filterbox_to_category.pcat_id='$pcat_id' and filterbox_to_category.subcat_id=0) or (filterbox_to_category.pcat_id='$pcat_id' and cat_id='0' and filterbox_to_category.subcat_id=0))";

		}
		if($type=="pcat"){
			
		$sql="select filterbox.filterbox_id,filterbox.filterbox_name,filterbox_to_category.active from  filterbox,filterbox_to_category where (filterbox_to_category.pcat_id = '$pcat_id')  and (filterbox_to_category.cat_id=0 and filterbox_to_category.subcat_id=0) and filterbox_to_category.filterbox_id=filterbox.filterbox_id $active_str";
		
		}
		
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_specification_group($arr,$active){
		extract($arr);
		$active_str=($active!=2)? "and specification_group_to_categories.active='$active'" :"";
		/*if($type=="subcat"){
			$active_str=($active!=2)? "and specification_group_to_categories.active='$active'" :"";
			$sql="select specification_group.specification_group_id,specification_group.specification_group_name,specification_group_to_categories.active from  specification_group,specification_group_to_categories where specification_group_to_categories.subcat_id = '$id' and cat_view!=0 and pcat_view!=0 and specification_group_to_categories.specification_group_id=specification_group.specification_group_id $active_str";
		}
		if($type=="cat"){
			$active_str=($active!=2)? "and specification_group_to_categories.active='$active'" :"";
			$sql="select specification_group.specification_group_id,specification_group.specification_group_name,specification_group_to_categories.active from  specification_group,specification_group_to_categories where specification_group_to_categories.cat_id = '$id' and subcat_view=0 and pcat_view!=0 and specification_group_to_categories.specification_group_id=specification_group.specification_group_id $active_str";
		}
		if($type=="pcat"){
			$active_str=($active!=2)? "and specification_group_to_categories.active='$active'" :"";
			$sql="select specification_group.specification_group_id,specification_group.specification_group_name,specification_group_to_categories.active from  specification_group,specification_group_to_categories where specification_group_to_categories.pcat_id = '$id' and subcat_view=0 and cat_view=0 and specification_group_to_categories.specification_group_id=specification_group.specification_group_id $active_str";
		}*/
		
		if($arr["type"]=="subcat"){
			
			 $sql="select specification_group_to_categories.*,specification_group.* from specification_group_to_categories,specification_group where specification_group_to_categories.specification_group_id=specification_group.specification_group_id and ((specification_group_to_categories.subcat_id='$subcat_id' and cat_id='$cat_id' and pcat_id='$pcat_id') or (specification_group_to_categories.cat_id='$cat_id' and subcat_id='0' and pcat_id='$pcat_id') or (specification_group_to_categories.pcat_id='$pcat_id' and cat_id='0' and subcat_id='0'))";
		}
		if($arr["type"]=="cat"){
			
			$sql="select specification_group_to_categories.*,specification_group.* from specification_group_to_categories,specification_group where specification_group_to_categories.specification_group_id=specification_group.specification_group_id and ((specification_group_to_categories.cat_id='$cat_id'  and specification_group_to_categories.pcat_id='$pcat_id' and specification_group_to_categories.subcat_id=0) or (specification_group_to_categories.pcat_id='$pcat_id' and cat_id='0' and specification_group_to_categories.subcat_id=0))";

		}
		if($arr["type"]=="pcat"){
			
		$sql="select specification_group.specification_group_id,specification_group.specification_group_name,specification_group_to_categories.active from  specification_group,specification_group_to_categories where (specification_group_to_categories.pcat_id = '$pcat_id')  and (specification_group_to_categories.cat_id=0 and specification_group_to_categories.subcat_id=0) and specification_group_to_categories.specification_group_id=specification_group.specification_group_id $active_str";
		
		}
		
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_cities($logistics_id){
		$sql="select logistics_service.logistics_service_id,logistics_service.pin,logistics_service.city,logistics.vendor_id from logistics_service,vendors,logistics where trim(logistics_service.logistics_id) = '$logistics_id' and logistics_service.logistics_id=logistics.logistics_id";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function add_subcategory($subcat_name,$cat_id,$pcat_id,$menu_sort_order,$view,$subcat_comparision){
		$insertinfo = array("subcat_name" => $subcat_name,"cat_id" => $cat_id,"pcat_id" => $pcat_id, "menu_sort_order" => $menu_sort_order,"view" => $view,"subcat_comparision"=>$subcat_comparision);
		$flag=$this -> db -> insert("subcategory", $insertinfo);
		return $flag;
	}
	public function get_subcategory_data($pcat_id,$cat_id,$subcat_id){
		$sql="SELECT * FROM `subcategory` Where pcat_id='$pcat_id' and cat_id='$cat_id' and subcat_id='$subcat_id'";
		$query =$this->db->query($sql);
		return $query->row_array();	
	}
	public function edit_subcategory($subcat_id,$cat_id,$subcat_name,$pcat_id,$menu_sort_order,$view,$common_subcat,$subcat_comparision){
		if($common_subcat!=""){
			$sql="SELECT * FROM `subcategory` Where subcat_id='$common_subcat'";
			$query =$this->db->query($sql);
			$menuto_sort_order=$query->row_array()["menu_sort_order"];
			$sql="SELECT * FROM `subcategory` Where subcat_id='$subcat_id'";
			$query =$this->db->query($sql);
			$menufrom_sort_order=$query->row_array()["menu_sort_order"];
			
			$sql = "UPDATE `subcategory` SET menu_sort_order='$menuto_sort_order',subcat_comparision='$subcat_comparision' WHERE `subcat_id` = '$subcat_id'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `subcategory` SET menu_sort_order='$menufrom_sort_order',subcat_comparision='$subcat_comparision' WHERE `subcat_id` = '$common_subcat'";
			$result=$this->db->query($sql);
			$sql = "UPDATE `subcategory` SET subcat_name='$subcat_name',pcat_id='$pcat_id',cat_id='$cat_id',view='$view',subcat_comparision='$subcat_comparision' WHERE `subcat_id` = '$subcat_id'";
			$query = $this->db->query($sql);
			}else{
		$sql = "UPDATE `subcategory` SET subcat_name='$subcat_name',pcat_id='$pcat_id',cat_id='$cat_id',menu_sort_order='$menu_sort_order',view='$view',subcat_comparision='$subcat_comparision' WHERE `subcat_id` = '$subcat_id'";
		$query = $this->db->query($sql);
		}
				
		return $query;
	}
	public function update_subcategory_selected($selected_list,$active){
		$sql_update_subcat="update subcategory set menu_sort_order=0,view=0,active='$active' where subcat_id in ($selected_list)";
		$flag1=$this->db->query($sql_update_subcat);
		$sql_update_2="update brands set active='$active' where subcat_id in ($selected_list)";
		$flag2=$this->db->query($sql_update_2);	
		$sql_select_product="select * from products where subcat_id in ($selected_list)";
		$query_product=$this->db->query($sql_select_product);
		$product_info = $query_product -> result();
		if(count($product_info)>0){
			$product_id_arr=array();
			foreach($product_info as $product) {
				$product_id_arr[]=$product->product_id;
			}
			$product_ids_in=implode(',',$product_id_arr);
			$sql_update_product="UPDATE `products` SET active='$active' WHERE product_id in ($product_ids_in)";
			$flag=$this->db->query($sql_update_product);			
			$sql_inventory="select * from inventory where product_id in ($product_ids_in)";
			$query_inventory = $this->db->query($sql_inventory);			
			$inventory_info = $query_inventory -> result();
			if(count($inventory_info)>0){
				$inventory_id_arr=array();
				foreach($inventory_info as $inventory){
					$inventory_id_arr[]=$inventory->id;
				}
				$inventory_id_in=implode(',',$inventory_id_arr);
				$res_inv=$this->update_inventory_selected($inventory_id_in,$active);	
			}else{
				$res_inv=true;		
			}
			if($flag==true && $res_inv==true){
				$flag_product_update=true;
			}else{
				$flag_product_update=false;
			}	
		}else{
			$flag_product_update=true;
		}
		
		$sql_select_attr="select * from attribute where subcat_id in ($selected_list)";
		$query_attr=$this->db->query($sql_select_attr);
		$attr_info = $query_attr -> result();
		if(count($attr_info)>0){
			$attribute_id_arr=array();
			foreach($attr_info as $attr) {
				$attribute_id_arr[]=$attr->attribute_id;
			}
			$attribute_ids_in=implode(',',$attribute_id_arr);
			$flag_attribute=$this->update_attribute_selected($attribute_ids_in,$active);
		}else{
			$flag_attribute=true;
		}
		
		$sql_specification_group="select * from specification_group_to_categories where subcat_id in ($selected_list)";
		$query_spec_group=$this->db->query($sql_specification_group);
		$spec_group_info = $query_spec_group->result();
		if(count($spec_group_info)>0){
			$spec_group_id_arr=array();
			foreach($spec_group_info as $spec_group) {
				$spec_group_id_arr[]=$spec_group->specification_group_id;
			}
			$spec_group_ids_in=implode(',',$spec_group_id_arr);
			$flag_spec_group=$this->update_specification_group_selected($spec_group_ids_in,$active);
		}else{
			$flag_spec_group=true;
		}	
		$sql_filterbox="select * from filterbox_to_category where subcat_id in ($selected_list)";
		$query_filterbox=$this->db->query($sql_filterbox);
		$filterbox_info = $query_filterbox -> result();
		if(count($filterbox_info)>0){
			$filterbox_id_arr=array();
			foreach($filterbox_info as $filterbox) {
				$filterbox_id_arr[]=$filterbox->filterbox_id;
			}
			$filterbox_info_ids_in=implode(',',$filterbox_id_arr);
			$flag_filterbox=$this->update_filterbox_selected($filterbox_info_ids_in,$active);
		}else{
			$flag_filterbox=true;
		}
		
		if($flag1==true && $flag2==true && $flag_product_update==true && $flag_attribute==true && $flag_spec_group==true && $flag_filterbox==true){
			return true;
		}else{
			return false;
		}
	}
	public function delete_subcategory_when_no_data($selected_list){
		$sql="select count(*) from brands where subcat_id in ($selected_list)";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$count = $result['count(*)'];
		if($count==0){
			$sql_delete="delete from subcategory where subcat_id in ($selected_list)";
			$flag =$this->db->query($sql_delete);
			return $flag;		
		}else{
			return 2;
		}
	}
	public function brands(){
		//$sql="select B.*,S.subcat_name,S.subcat_id,C.cat_name,C.cat_id, P.pcat_id,P.pcat_name from brands B inner join parent_category P on B.pcat_id=P.pcat_id inner join category C on C.cat_id=B.cat_id inner join subcategory S on S.subcat_id=B.subcat_id";

		$sql="select brands.*,subcategory.subcat_name,subcategory.subcat_id,category.cat_name,category.cat_id,vendors.name as vendor_name  from brands";
		$sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `brands`.`pcat_id`';
		$sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `brands`.`cat_id`';
		$sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `brands`.`subcat_id`';
		$sql.=' LEFT JOIN `vendors` ON `vendors`.`vendor_id` = `brands`.`vendor_id`';
		$sql.="where brands.active=1";		
		$query = $this->db->query($sql);		
		return $query -> result();
	}
	public function add_brand($brand_name,$subcat_id,$cat_id,$pcat_id,$view){
		$vendors_data_result=$this->get_vendors_data_obj();
		$user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");
		foreach($vendors_data_result as $rowObj){
			$vendor_id=$rowObj->vendor_id;
		}
		$insertinfo = array("vendor_id"=>$vendor_id,"brand_name"=> $brand_name,"subcat_id" => $subcat_id,"cat_id" => $cat_id,"pcat_id" => $pcat_id,"view" => $view);
		
		$insertinfo['added_by']='admin';
		$insertinfo['approval_status']='1';
		$insertinfo['status_updated_on']=date('Y-m-d H:i:s');
	
		$flag=$this -> db -> insert("brands", $insertinfo);
		return $flag;
	}
	public function get_brand_data($pcat_id,$cat_id,$subcat_id,$brand_id){
		$sql="SELECT brands.*,vendors.name as vendor_name  FROM `brands` 
		LEFT JOIN `vendors` ON `vendors`.`vendor_id` = `brands`.`vendor_id`
		Where pcat_id='$pcat_id' and cat_id='$cat_id' and subcat_id='$subcat_id' and brand_id='$brand_id'";
		$query =$this->db->query($sql);
		return $query->row_array();	
	}
	public function edit_brand($brand_id,$brand_name,$subcat_id,$cat_id,$pcat_id,$view){
		$sql = "UPDATE `brands` SET brand_name='$brand_name',pcat_id='$pcat_id',cat_id='$cat_id', subcat_id='$subcat_id',view='$view' WHERE `brand_id` = '$brand_id'";	
		$query = $this->db->query($sql);		
		return $query;
	}
	public function update_brand_selected($selected_list,$active){
		$sql_update_br="update brands set view=0,active='$active' where brand_id in ($selected_list)";
		$flag1=$this->db->query($sql_update_br);
		$sql_select_product="select * from products where brand_id in ($selected_list)";
		$query_product=$this->db->query($sql_select_product);
		
		$product_info = $query_product->result();
		if(count($product_info)>0){
			$product_id_arr=array();
			foreach($product_info as $product) {
				$product_id_arr[]=$product->product_id;
			}
			$product_ids_in=implode(',',$product_id_arr);

			$sql_update_product="UPDATE `products` SET active='$active' WHERE product_id in ($product_ids_in)";
			$flag=$this->db->query($sql_update_product);
			
			$sql_inventory="select * from inventory where product_id in ($product_ids_in)";
			$query_inventory = $this->db->query($sql_inventory);
			
			$inventory_info = $query_inventory -> result();
			if(count($inventory_info)>0){
				$inventory_id_arr=array();
				foreach ($inventory_info as $inventory){
					$inventory_id_arr[]=$inventory->id;
				}
				$inventory_id_in=implode(',',$inventory_id_arr);
				$res_inv=$this->update_inventory_selected($inventory_id_in,$active);
				
			}else{
				$res_inv=true;		
			}
			if($flag==true && $res_inv==true){
				$flag_product_update=true;
			}else{
				$flag_product_update=false;
			}
			
		}else{
			$flag_product_update=true;
		}
		if($flag1==true && $flag_product_update==true){
			return true;
		}else{
			return false;
		}
	}
	public function delete_brand_when_no_data($selected_list){
		
		$sql="select count(*) from products where brand_id in ($selected_list)";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$count = $result['count(*)'];
		if($count==0){
			$sql_delete="delete from brands where brand_id in ($selected_list)";
			$flag =$this->db->query($sql_delete);
			return $flag;		
		}else{
			return 2;
		}
	}
	public function attribute(){
		$sql="select attribute.*,category.cat_id,category.cat_name,subcategory.subcat_id,subcategory.subcat_name from attribute,category,subcategory where attribute.cat_id=category.cat_id and attribute.subcat_id=subcategory.subcat_id and attribute.active=1";
		$query =$this->db->query($sql);
		return $query->result();	
	}
	public function add_attribute($attribute1_name,$attribute1_option,$attribute11_name,$attribute11_option,$attribute2_name,$attribute2_option,$attribute3_name,$attribute3_option,$attribute4_name,$attribute4_option,$subcat_id,$cat_id,$pcat_id,$view,$value){
		if($value==1){
		$insertinfo = array("attribute1_name"=> $attribute1_name,"attribute1_option"=> $attribute1_option,"attribute2_name"=> $attribute2_name,"attribute2_option"=> $attribute2_option,"attribute3_name"=> $attribute3_name,"attribute3_option"=> $attribute3_option,"attribute4_name"=> $attribute4_name,"attribute4_option"=> $attribute4_option,"pcat_id"=>$pcat_id,"cat_id"=>$cat_id,"subcat_id"=>$subcat_id,"view" => $view);
		}
		if($value==2){
			$insertinfo = array("attribute1_name"=> $attribute11_name,"attribute1_option"=> $attribute11_option,"attribute2_name"=> $attribute2_name,"attribute2_option"=> $attribute2_option,"attribute3_name"=> $attribute3_name,"attribute3_option"=> $attribute3_option,"attribute4_name"=> $attribute4_name,"attribute4_option"=> $attribute4_option,"pcat_id"=>$pcat_id,"cat_id"=>$cat_id,"subcat_id"=>$subcat_id,"view" => $view);
			
		}

		$flag1=$this -> db -> insert("attribute", $insertinfo);
		$attribute_id=$this->db->insert_id();
		if(!empty($subcat_id)){
			$sql = "UPDATE `attribute` SET catalog='subcat_id',catalog_id='$subcat_id' WHERE `attribute_id` = '$attribute_id'";
			$query1 = $this->db->query($sql);	
		}
		else if(!empty($cat_id)){
			$sql2 = "UPDATE `attribute` SET catalog='cat_id',catalog_id='$cat_id' WHERE `attribute_id` = '$attribute_id'";		
			$query2 = $this->db->query($sql2);	
		}
		else{
			if(!empty($pcat_id)){
				$sql1 = "UPDATE `attribute` SET catalog='pcat_id',catalog_id='$pcat_id' WHERE `attribute_id` = '$attribute_id'";
				$query1 = $this->db->query($sql1);	
			}
		}
		return $flag1;	
	}
	
	
	public function get_attribute_data($catalog,$catalog_id,$attribute_id){
		$sql="SELECT * FROM `attribute` Where attribute_id='$attribute_id' and attribute.active=1";
		$query =$this->db->query($sql);

		if($catalog=="subcat_id"){
			$sql1="select subcategory.subcat_id,subcategory.subcat_name,category.cat_id,category.cat_name from subcategory,category where subcategory.subcat_id='$catalog_id' and subcategory.cat_id=category.cat_id and subcategory.active=1";	
		}
		if($catalog=="cat_id"){
			$sql1="select category.cat_id,category.cat_name from category where category.cat_id='$catalog_id' and category.active=1";
		}
		if($catalog=="pcat_id"){
			$sql1="select pcat_id,pcat_name from parent_category where pcat_id='$catalog_id' and parent_category.active=1";
		}
		$query1 =$this->db->query($sql1);
		$query1->row_array();
			
		$temp=array();
		foreach($query->row_array() as $k => $v){
			$temp[$k]=$v;
		}
		foreach($query1->row_array() as $k => $v){
			$temp[$k]=$v;
		}
		return $temp;	
	}
	public function edit_attribute($attribute_id,$attribute1_name,$attribute1_option,$attribute1_name1,$attribute1_option1,$attribute2_name,$attribute2_option,$attribute3_name,$attribute3_option,$attribute4_name,$attribute4_option,$subcat_id,$cat_id,$pcat_id,$view,$value){
		if($value==1){
		$sql = "UPDATE `attribute` SET attribute1_name='$attribute1_name',attribute2_name='$attribute2_name',attribute3_name='$attribute3_name',attribute4_name='$attribute4_name',attribute1_option='$attribute1_option',attribute2_option='$attribute2_option',attribute3_option='$attribute3_option',attribute4_option='$attribute4_option',pcat_id='$pcat_id',cat_id='$cat_id',subcat_id='$subcat_id',view='$view' where `attribute_id` = '$attribute_id'";
		}	
		if($value==2){
			$sql = "UPDATE `attribute` SET attribute1_name='$attribute1_name1',attribute2_name='$attribute2_name',attribute3_name='$attribute3_name',attribute4_name='$attribute4_name',attribute1_option='$attribute1_option1',attribute2_option='$attribute2_option',attribute3_option='$attribute3_option',attribute4_option='$attribute4_option',pcat_id='$pcat_id',cat_id='$cat_id',subcat_id='$subcat_id',view='$view' where `attribute_id` = '$attribute_id'";
		}			
		$query = $this->db->query($sql);	
		return $query;
	}
	public function update_attribute_selected($selected_list,$active){
		$sql_update="update attribute set view=0,active='$active' where attribute_id in ($selected_list)";
		$flag=$this->db->query($sql_update);
		return $flag;
	}
	public function delete_attribute_when_no_data($selected_list){
		
		$sql_select="select attribute.*,inventory.attribute_1,inventory.attribute_2,inventory.attribute_3,inventory.attribute_4 from attribute,inventory where attribute.attribute_id in ($selected_list) and attribute.attribute1_name=inventory.attribute_1 and attribute.attribute2_name=inventory.attribute_2 and attribute.attribute3_name=inventory.attribute_3 and attribute.attribute4_name=inventory.attribute_4 and inventory.attribute_2!='' and inventory.attribute_3!='' and inventory.attribute_4!='' and attribute.attribute2_name!='' and attribute.attribute3_name!='' and attribute.attribute4_name!=''";
		$query=$this->db->query($sql_select);
		$res=$query->result();
		$count=$query->num_rows();
		if($count==0){
			///////// delete expansions starts /////
			$attribute_id_arr=explode(",",$selected_list);
				foreach($attribute_id_arr as $attribute_id){
					$sql="select * from attribute_expansions where attribute_id='$attribute_id'";
					$query = $this->db->query($sql);
					$attribute_expansions_id=$query->row_array()["attribute_expansions_id"];
					
					$sql="delete from attribute_expansions where attribute_id='$attribute_id'";
					$query = $this->db->query($sql);
					$sql_attribute_expansions_id="delete from attribute_expansions_sub_options where attribute_expansions_id='$attribute_expansions_id'";
					$query_attribute_expansions_id = $this->db->query($sql_attribute_expansions_id);
					
				}
			///////// delete expansions ends /////
			$sql_delete="delete from attribute where attribute_id in ($selected_list)";
			$flag=$this->db->query($sql_delete);
			return $flag;
		}else{
			return 2;
		}
	}
	public function filterbox()
	{	
		//$sql="SELECT  filterbox_to_category.*, if( filterbox_to_category.subcat_id!=0,subcategory.subcat_name,'' ) as first_subcatname,if( filterbox_to_category.subcat_id=0,0,subcategory.subcat_id ) as second_subcat_id,category.cat_id,category.cat_name,subcategory.subcat_id,subcategory.subcat_name,parent_category.pcat_name, parent_category.pcat_id from filterbox_to_category,category,subcategory,parent_category where   filterbox_to_category.cat_id=category.cat_id AND parent_category.pcat_id=filterbox_to_category.pcat_id and if( filterbox_to_category.subcat_id!=0,subcategory.subcat_id,0)=filterbox_to_category.subcat_id";
		
		$sql="select * from filterbox,filterbox_to_category where filterbox.filterbox_id=filterbox_to_category.filterbox_id and filterbox_to_category.active=1";	
		$query =$this->db->query($sql);
		$res=$query->result();
		/*$catalog_arr=array();
		foreach($res as $v ){
			if($v->subcat_id !=0){
				$subcat_id=$v->subcat_id;
				$sql1="select subcategory.subcat_id,subcategory.subcat_name,category.cat_id,category.cat_name from subcategory,category where subcategory.subcat_id='$subcat_id' and subcategory.cat_id=category.cat_id and subcategory.active=1";		
			}else if($v->cat_id !=0){
				$cat_id=$v->cat_id;
				$sql1="select category.cat_id,category.cat_name from category where category.cat_id='$cat_id' and category.active=1";	
			}else{
				$pcat_id=$v->pcat_id;
				$sql1="select pcat_id,pcat_name from parent_category where pcat_id='$pcat_id' and parent_category.active=1";
			}
			
			$query1 =$this->db->query($sql1);
			$catalog_arr[]=$query1->row_array();
		}
		$catalog_temp_arr=array();
		$temp_total=array();
		foreach($query->result_array() as $k => $filterbox_arr){
			$catalog_temp_arr=$catalog_arr[$k];
			$temp=array();
			foreach($filterbox_arr as $k1 => $v1){
				$temp[$k1]=$v1;
			}
			foreach($catalog_temp_arr as $k2 => $v2){
				$temp[$k2]=$v2;
			}
			$temp_total[]=(object)$temp;
		}*/
			
		return $res;
	}
	public function add_filterbox($filterbox_name,$filterbox_units,$type,$sort_order,$subcat_id,$cat_id,$pcat_id,$view_filterbox,$pcat_view,$cat_view,$subcat_view){
		
		$insertinfo = array("filterbox_name"=> $filterbox_name,"filterbox_units"=> $filterbox_units,"type"=> $type,"sort_order"=> $sort_order,"view_filterbox" => $view_filterbox);
		$flag1=$this -> db -> insert("filterbox", $insertinfo);	
		$filterbox_id=$this->db->insert_id();	
		$insertinfo = array("filterbox_id"=>$filterbox_id,"pcat_id"=>$pcat_id,"cat_id"=>$cat_id,"subcat_id"=>$subcat_id,"pcat_view"=>$pcat_view,"cat_view"=>$cat_view,"subcat_view"=>$subcat_view);
		$flag2=$this -> db -> insert("filterbox_to_category", $insertinfo);
		if($flag1==true && $flag2==true){
			return $flag1;	
		}

	}
	public function get_filterbox_data($filterbox_id,$cat_id,$subcat_id,$pcat_id){
		$sql="SELECT * FROM `filterbox` Where filterbox_id='$filterbox_id'";
		$query =$this->db->query($sql);
		$sql1="select * from filterbox_to_category where filterbox_id='$filterbox_id' and subcat_id='$subcat_id' and cat_id='$cat_id' and pcat_id='$pcat_id' ";	
		$query1 =$this->db->query($sql1);
		$temp=array();
		foreach($query->row_array() as $k => $v){
			$temp[$k]=$v;
		}
		foreach($query1->row_array() as $k => $v){
			$temp[$k]=$v;
		}
		return $temp;	
	}
	public function edit_filterbox($filterbox_id,$filterbox_name,$filterbox_units,$type,$sort_order,$subcat_id,$cat_id,$pcat_id,$view_filterbox,$pcat_view,$cat_view,$subcat_view,$common_filterbox){
	if($common_filterbox!=""){
			$sql="SELECT * FROM `filterbox` Where filterbox_id='$common_filterbox'";
			$query =$this->db->query($sql);
			$to_sort_order=$query->row_array()["sort_order"];
			$sql="SELECT * FROM `filterbox` Where filterbox_id='$filterbox_id'";
			$query =$this->db->query($sql);
			$from_sort_order=$query->row_array()["sort_order"];
			
			$sql = "UPDATE `filterbox` SET sort_order='$to_sort_order' WHERE `filterbox_id` = '$filterbox_id'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `filterbox` SET sort_order='$from_sort_order' WHERE `filterbox_id` = '$common_filterbox'";
			$result=$this->db->query($sql);
			
			
			$sql = "UPDATE `filterbox` SET filterbox_name='$filterbox_name',filterbox_units='$filterbox_units',type='$type',view_filterbox='$view_filterbox' where `filterbox_id` = '$filterbox_id'";
			$query = $this->db->query($sql);
	
			$sql_c = "UPDATE `filterbox_to_category` SET pcat_id='$pcat_id',cat_id='$cat_id',subcat_id='$subcat_id',pcat_view='$pcat_view',cat_view='$cat_view',subcat_view='$subcat_view' WHERE `filterbox_id` = '$filterbox_id'";
			$query1 = $this->db->query($sql_c);	
		}else{
		
			$sql = "UPDATE `filterbox` SET filterbox_name='$filterbox_name',filterbox_units='$filterbox_units',type='$type',sort_order='$sort_order',view_filterbox='$view_filterbox' where `filterbox_id` = '$filterbox_id'";
			$query = $this->db->query($sql);
	
			$sql_c = "UPDATE `filterbox_to_category` SET pcat_id='$pcat_id',cat_id='$cat_id',subcat_id='$subcat_id',pcat_view='$pcat_view',cat_view='$cat_view',subcat_view='$subcat_view' WHERE `filterbox_id` = '$filterbox_id'";
			$query1 = $this->db->query($sql_c);	
		}
		if($query==true && $query1==true){
			return $query;	
		}else{
			return false;
		}
	}
	public function update_filterbox_selected($selected_list,$active){
		$sql_update1="update filterbox_to_category set active='$active' where filterbox_id in ($selected_list)";
		$flag1=$this->db->query($sql_update1);
		$sql_update2="update filterbox set sort_order=0,view_filterbox=0,active='$active' where filterbox_id in ($selected_list)";
		$flag2=$this->db->query($sql_update2);	
		$sql_filter="select * from filter where filterbox_id in ($selected_list)";	
		$query_filter = $this->db->query($sql_filter);	
		$filter_info = $query_filter -> result();
		if(count($filter_info)>0){
			$filter_id_arr=array();
			foreach ($filter_info as $filter) {
				$filter_id_arr[]=$filter->filter_id;
			}
			$filter_id_arr_in=implode(',',$filter_id_arr);		

			$sql_filter="Update filter set active='$active' where filter_id in ($filter_id_arr_in)";
			$flag1_f=$this->db->query($sql_filter);	
			$sql_products_filter="Update products_filter set active='$active' where filter_id in ($filter_id_arr_in)";
			$flag2_f=$this->db->query($sql_products_filter);
			if($flag1_f==true && $flag2_f==true){
				$flag3=true;
			}else{
				$flag3=false;
			}
		}else{
			$flag3=true;
		}
		
		if($flag1=true && $flag2==true && $flag3==true){
			return true;
		}else{
			return false;
		}
	}
	
	public function delete_filterbox_when_no_data($selected_list){
		
		$sql="select count(*) from filter where filterbox_id in ($selected_list)";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$count = $result['count(*)'];
		if($count==0){
			$sql_delete="delete from filterbox where filterbox_id in ($selected_list)";
			$flag =$this->db->query($sql_delete);
			
			$sql_delete1="delete from filterbox_to_category where filterbox_id in ($selected_list)";
			$flag1 =$this->db->query($sql_delete1);
			if($flag==true && $flag1==true){
				return true;	
			}else{
				return false;
			}			
		}else{
			return 2;
		}
	}
	/*public function filter(){	
		$sql="select * from filter,filterbox,filterbox_to_category where filterbox.filterbox_id=filterbox_to_category.filterbox_id and filter.filterbox_id=filterbox.filterbox_id";	
		$query =$this->db->query($sql);
		$res=$query->result();
		$catalog_arr=array();
		foreach($res as $v ){
			if($v->subcat_id !=0){
				$subcat_id=$v->subcat_id;
				$sql1="select subcategory.subcat_id,subcategory.subcat_name,category.cat_id,category.cat_name from subcategory,category where subcategory.subcat_id='$subcat_id' and subcategory.cat_id=category.cat_id and subcategory.active=1";	
				
			}else if($v->cat_id !=0){
				$cat_id=$v->cat_id;
				$sql1="select category.cat_id,category.cat_name from category where category.cat_id='$cat_id' and  category.active=1";
				
			}else{
				$pcat_id=$v->pcat_id;
				$sql1="select pcat_id,pcat_name from parent_category where pcat_id='$pcat_id' and parent_category.active=1";
			}
			
			$query1 =$this->db->query($sql1);
			$catalog_arr[]=$query1->row_array();
		}
		$catalog_temp_arr=array();
		
			$temp_total=array();
			foreach($query->result_array() as $k => $filterbox_arr){
				$catalog_temp_arr=$catalog_arr[$k];
				$temp=array();
				foreach($filterbox_arr as $k1 => $v1){
					$temp[$k1]=$v1;
				}
				foreach($catalog_temp_arr as $k2 => $v2){
					$temp[$k2]=$v2;
				}
				$temp_total[]=(object)$temp;
			}
			
		return $temp_total;
	}*/
	public function add_filter($filter_options,$filterbox_id,$sort_order,$subcat_id,$cat_id,$pcat_id,$view_filter){
		
		$insertinfo = array("filter_options"=> $filter_options,"filterbox_id"=> $filterbox_id,"sort_order"=> $sort_order,"view_filter" => $view_filter);
		$flag=$this -> db -> insert("filter", $insertinfo);	
		if($flag==true){
			return $flag;	
		}
	}
	public function get_filter_data($filter_id,$cat_id,$subcat_id,$pcat_id){
		$sql="SELECT * FROM `filter` Where filter_id='$filter_id'";
		$query =$this->db->query($sql);
		$sql="select filter.*,filterbox.*, filterbox_to_category.*,filter.sort_order as filter_sort_order,filterbox.sort_order as filterbox_sort_order from filter,filterbox,filterbox_to_category where filterbox.filterbox_id=filterbox_to_category.filterbox_id and filter.filterbox_id=filterbox.filterbox_id and filter.filter_id='$filter_id' and filterbox_to_category.active=1";	
		$query =$this->db->query($sql);
		return $query->row_array();;	
	}
	public function edit_filter($filter_id,$filter_options,$filterbox_id,$sort_order,$subcat_id,$cat_id,$pcat_id,$view_filter,$common_filter){
		if($common_filter!=""){
			$sql="SELECT * FROM `filter` Where filter_id='$common_filter'";
			$query =$this->db->query($sql);
			$to_sort_order=$query->row_array()["sort_order"];
			$sql="SELECT * FROM `filter` Where filter_id='$filter_id'";
			$query =$this->db->query($sql);
			$from_sort_order=$query->row_array()["sort_order"];
			
			$sql = "UPDATE `filter` SET sort_order='$to_sort_order' WHERE `filter_id` = '$filter_id'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `filter` SET sort_order='$from_sort_order' WHERE `filter_id` = '$common_filter'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `filter` SET filter_options='$filter_options',view_filter='$view_filter',filterbox_id='$filterbox_id' where `filter_id` = '$filter_id'";	
			$query = $this->db->query($sql);
			}else{
		
			$sql = "UPDATE `filter` SET filter_options='$filter_options',sort_order='$sort_order',view_filter='$view_filter',filterbox_id='$filterbox_id' where `filter_id` = '$filter_id'";	
			$query = $this->db->query($sql);
			}
			if($query==true){
			return $query;	
			}
		
	}
	public function update_filter_selected($selected_list,$active){
		$sql_filter="Update filter set sort_order=0,view_filter=0,active=$active where filter_id in ($selected_list)";
		$flag1=$this->db->query($sql_filter);	
		$sql_products_filter="Update products_filter set active=$active where filter_id in ($selected_list)";
		$flag2=$this->db->query($sql_products_filter);
		if($flag1==true && $flag2==true){
			return true;
		}else{
			return false;
		}	
	}
	public function delete_products_filter_selected($selected_list){
		
		if($flag2==true){
			return true;
		}else{
			return false;
		}
	}
	public function delete_filter_when_no_data($selected_list){
		$sql="select count(*) from products_filter where filter_id in ($selected_list)";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$count = $result['count(*)'];
		if($count==0){	
			$sql_delete="delete from filter where filter_id in ($selected_list)";
			$flag=$this->db->query($sql_delete);
			return $flag;
		}else{
			return 2;
		}
	}
	public function specification_group(){
		$sql="select * from specification_group,specification_group_to_categories where specification_group.specification_group_id=specification_group_to_categories.specification_group_id and specification_group_to_categories.active=1";	
		$query =$this->db->query($sql);
		$res=$query->result();
		/*$catalog_arr=array();
		foreach($res as $v ){
			if($v->subcat_id !=0){
				$subcat_id=$v->subcat_id;
				$sql1="select subcategory.subcat_id,subcategory.subcat_name,category.cat_id,category.cat_name from subcategory,category where subcategory.subcat_id='$subcat_id' and subcategory.cat_id=category.cat_id and subcategory.active=1";	
				
			}else if($v->cat_id !=0){
				$cat_id=$v->cat_id;
				$sql1="select category.cat_id,category.cat_name from category where category.cat_id='$cat_id' and category.active=1";	
			}else{
				$pcat_id=$v->pcat_id;
				$sql1="select pcat_id,pcat_name from parent_category where pcat_id='$pcat_id'";
			}
			
			$query1 =$this->db->query($sql1);
			$catalog_arr[]=$query1->row_array();
		}
		$catalog_temp_arr=array();
		
			$temp_total=array();
			foreach($query->result_array() as $k => $filterbox_arr){
				$catalog_temp_arr=$catalog_arr[$k];
				$temp=array();
				foreach($filterbox_arr as $k1 => $v1){
					$temp[$k1]=$v1;
				}
				foreach($catalog_temp_arr as $k2 => $v2){
					$temp[$k2]=$v2;
				}
				$temp_total[]=(object)$temp;
			}*/
			
		return $res;
	}
	public function add_specification_group($specification_group_name,$sort_order,$subcat_id,$cat_id,$pcat_id,$view_specification_group,$pcat_view,$cat_view,$subcat_view){
		
		$insertinfo = array("specification_group_name"=> $specification_group_name,"sort_order"=> $sort_order,"view_specification_group" => $view_specification_group);
		$flag1=$this -> db -> insert("specification_group", $insertinfo);	
		$specification_group_id=$this->db->insert_id();	
		$insertinfo = array("specification_group_id"=>$specification_group_id,"pcat_id"=>$pcat_id,"cat_id"=>$cat_id,"subcat_id"=>$subcat_id,"pcat_view"=>$pcat_view,"cat_view"=>$cat_view,"subcat_view"=>$subcat_view);
		$flag2=$this -> db -> insert("specification_group_to_categories", $insertinfo);
		if($flag1==true && $flag2==true){
			return $flag1;	
		}

	}
	public function get_specification_group_data($pcat_id,$cat_id,$subcat_id,$specification_group_id){
		$sql="SELECT * FROM `specification_group` Where specification_group_id='$specification_group_id' and specification_group.active=1";
		$query =$this->db->query($sql);
		$sql1="select * from specification_group_to_categories where specification_group_id='$specification_group_id' and subcat_id='$subcat_id' and cat_id='$cat_id' and pcat_id='$pcat_id' and specification_group_to_categories.active=1";	
		$query1 =$this->db->query($sql1);
		$temp=array();
		foreach($query->row_array() as $k => $v){
			$temp[$k]=$v;
		}
		foreach($query1->row_array() as $k => $v){
			$temp[$k]=$v;
		}
		return $temp;	
	}
	public function edit_specification_group($specification_group_id,$specification_group_name,$sort_order,$subcat_id,$cat_id,$pcat_id,$view_specification_group,$pcat_view,$cat_view,$subcat_view,$common_specification_group){
		if($common_specification_group!=""){
			$sql="SELECT * FROM `specification_group` Where specification_group_id='$common_specification_group'";
			$query =$this->db->query($sql);
			$to_sort_order=$query->row_array()["sort_order"];
			$sql="SELECT * FROM `specification_group` Where specification_group_id='$specification_group_id'";
			$query =$this->db->query($sql);
			$from_sort_order=$query->row_array()["sort_order"];
			
			$sql = "UPDATE `specification_group` SET sort_order='$to_sort_order' WHERE `specification_group_id` = '$specification_group_id'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `specification_group` SET sort_order='$from_sort_order' WHERE `specification_group_id` = '$common_specification_group'";
			$result=$this->db->query($sql);
			
			
			$sql = "UPDATE `specification_group` SET specification_group_name='$specification_group_name',view_specification_group='$view_specification_group' where `specification_group_id` = '$specification_group_id'";
		
		$query = $this->db->query($sql);
	
		$sql_c = "UPDATE `specification_group_to_categories` SET pcat_id='$pcat_id',cat_id='$cat_id',subcat_id='$subcat_id',pcat_view='$pcat_view',cat_view='$cat_view',subcat_view='$subcat_view' WHERE `specification_group_id` = '$specification_group_id'";
		$query1 = $this->db->query($sql_c);	
		}else{
		
			$sql = "UPDATE `specification_group` SET specification_group_name='$specification_group_name',sort_order='$sort_order',view_specification_group='$view_specification_group' where `specification_group_id` = '$specification_group_id'";
		
		$query = $this->db->query($sql);
	
		$sql_c = "UPDATE `specification_group_to_categories` SET pcat_id='$pcat_id',cat_id='$cat_id',subcat_id='$subcat_id',pcat_view='$pcat_view',cat_view='$cat_view',subcat_view='$subcat_view' WHERE `specification_group_id` = '$specification_group_id'";
		$query1 = $this->db->query($sql_c);	
		}
		
		if($query==true && $query1==true){
			return $query;	
		}else{
			return false;
		}
	}
	public function update_specification_group_selected($selected_list,$active){
		$sql_update1="update specification_group_to_categories set active='$active' where specification_group_id in ($selected_list)";
		$flag1=$this->db->query($sql_update1);
		$sql_update2="update specification_group set sort_order=0,view_specification_group=0,active='$active' where specification_group_id in ($selected_list)";
		$flag2=$this->db->query($sql_update2);
		
			$sql_specification="select * from specification where specification_group_id in ($selected_list)";	
			$query_specification = $this->db->query($sql_specification);	
			$specification_info = $query_specification -> result();
			if(count($specification_info)>0){
				$specification_id_arr=array();
				foreach ($specification_info as $specification) {
					$specification_id_arr[]=$specification->specification_id;
				}
				$specification_id_arr_in=implode(',',$specification_id_arr);		

				$sql_update1="update specification set active='$active' where specification_id in ($specification_id_arr_in)";
				$flag1_s=$this->db->query($sql_update1);
				$sql_update2="update inventory_specification set active='$active' where specification_id in ($specification_id_arr_in)";
				$flag2_s=$this->db->query($sql_update2);
				if($flag1_s==true && $flag2_s==true){
					$flag3=true;
				}else{
					$flag3=false;
				}
				
			}else{
				$flag3=true;
			}
		
		if($flag1=true && $flag2==true && $flag3==true){
			return true;
		}else{
			return false;
		}

	}
	public function delete_specification_group_when_no_data($selected_list){
		
		$sql="select count(*) from specification where specification_group_id in ($selected_list)";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$count = $result['count(*)'];
		if($count==0){
			$sql_delete="delete from specification_group where specification_group_id in ($selected_list)";
			$flag =$this->db->query($sql_delete);
			
			$sql_delete1="delete from specification_group_to_categories where specification_group_id in ($selected_list)";
			$flag1 =$this->db->query($sql_delete1);
			if($flag==true && $flag1==true){
				return true;	
			}else{
				return false;
			}
		}else{
			return 2;
		}
	}
	/*public function specification(){	
		$sql="select * from specification,specification_group,specification_group_to_categories where specification_group.specification_group_id=specification_group_to_categories.specification_group_id and specification.specification_group_id=specification_group.specification_group_id";	
		$query =$this->db->query($sql);
		$res=$query->result();
		$catalog_arr=array();
		foreach($res as $v ){
			if($v->subcat_id !=0){
				$subcat_id=$v->subcat_id;
				$sql1="select subcategory.subcat_id,subcategory.subcat_name,category.cat_id,category.cat_name from subcategory,category where subcategory.subcat_id='$subcat_id' and subcategory.cat_id=category.cat_id and subcategory.active=1";	
			}else if($v->cat_id !=0){
				$cat_id=$v->cat_id;
				$sql1="select category.cat_id,category.cat_name from category where category.cat_id='$cat_id' and  category.active=1";
			}else{
				$pcat_id=$v->pcat_id;
				$sql1="select pcat_id,pcat_name from parent_category where pcat_id='$pcat_id'";
			}
			
			$query1 =$this->db->query($sql1);
			$catalog_arr[]=$query1->row_array();
		}
		$catalog_temp_arr=array();
		
			$temp_total=array();
			foreach($query->result_array() as $k => $filterbox_arr){
				$catalog_temp_arr=$catalog_arr[$k];
				$temp=array();
				foreach($filterbox_arr as $k1 => $v1){
					$temp[$k1]=$v1;
				}
				foreach($catalog_temp_arr as $k2 => $v2){
					$temp[$k2]=$v2;
				}
				$temp_total[]=(object)$temp;
			}
			
		return $temp_total;
	}*/
	public function add_specification($specification_name,$specification_value,$specification_group_id,$sort_order,$subcat_id,$cat_id,$pcat_id,$view_specification,$view_comparision){
		
		$insertinfo = array("specification_name"=> $specification_name,"specification_value"=> $specification_value,"specification_group_id"=> $specification_group_id,"sort_order"=> $sort_order,"view_specification" => $view_specification,"view_comparision" => $view_comparision);
		$flag=$this -> db -> insert("specification", $insertinfo);	
		if($flag==true){
			return $flag;	
		}
	}
	public function get_specification_data($specification_id,$cat_id,$subcat_id,$pcat_id){
		$sql="SELECT * FROM `specification` Where specification_id='$specification_id'";
		$query =$this->db->query($sql);
		$sql1="select specification.*,specification_group.*, specification_group_to_categories.*,specification.sort_order as specification_sort_order,specification_group.sort_order as specification_group_sort_order from specification,specification_group,specification_group_to_categories where specification_group.specification_group_id=specification_group_to_categories.specification_group_id and specification.specification_group_id=specification_group.specification_group_id and specification.specification_id='$specification_id'";	
		$query1 =$this->db->query($sql1);
		return $query1->row_array();;	
	}
	public function edit_specification($specification_id,$specification_name,$specification_value,$specification_group_id,$sort_order,$subcat_id,$cat_id,$pcat_id,$view_specification,$common_specification){
		if($common_specification!=""){
			$sql="SELECT * FROM `specification` Where specification_id='$common_specification'";
			$query =$this->db->query($sql);
			$to_sort_order=$query->row_array()["sort_order"];
			$sql="SELECT * FROM `specification` Where specification_id='$specification_id'";
			$query =$this->db->query($sql);
			$from_sort_order=$query->row_array()["sort_order"];
			
			$sql = "UPDATE `specification` SET sort_order='$to_sort_order' WHERE `specification_id` = '$specification_id'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `specification` SET sort_order='$from_sort_order' WHERE `specification_id` = '$common_specification'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `specification` SET specification_name='$specification_name',specification_value='$specification_value',view_specification='$view_specification',specification_group_id='$specification_group_id' where `specification_id` = '$specification_id'";
			$query = $this->db->query($sql);
			}else{
		
			$sql = "UPDATE `specification` SET specification_name='$specification_name',specification_value='$specification_value',sort_order='$sort_order',view_specification='$view_specification',specification_group_id='$specification_group_id' where `specification_id` = '$specification_id'";
			$query = $this->db->query($sql);
			}
		if($query==true){
			return $query;	
		}
	}
	public function update_specification_selected($selected_list,$active){
		$sql_update1="update specification set sort_order=0,view_specification=0,active='$active' where specification_id in ($selected_list)";
		$flag1=$this->db->query($sql_update1);
		$sql_update2="update inventory_specification set active='$active' where specification_id in ($selected_list)";
		$flag2=$this->db->query($sql_update2);
		if($flag1==true && $flag2==true){
			return true;
		}else{
			return false;
		}
	}
	public function delete_specification_selected_when_no_data($selected_list){
		$sql="select count(*) from inventory_specification where specification_id in ($selected_list)";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$count = $result['count(*)'];
		if($count==0){	
			$sql_delete="delete from specification where specification_id in ($selected_list)";
			$flag=$this->db->query($sql_delete);
			return $flag;
		}else{
			return 2;
		}
	}
	public function products(){
		$sql="select products.*,category.cat_id,category.cat_name,subcategory.subcat_id,subcategory.subcat_name,brands.brand_id,brands.brand_name from products,category,subcategory,brands where products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and products.active=1";
		$query =$this->db->query($sql);
		return $query->result();	
	}
	public function add_products($product_name,$product_description,$sugg1,$sugg2,$generalsugg1,$generalsugg2,$generalsugg3,$generalsugg4,$brand_id,$subcat_id,$cat_id,$pcat_id,$view){
		$product_description=$this->db->escape_str($product_description);
		$insertinfo = array("product_name"=> $product_name,"product_description"=> $product_description,"sugg1"=> $sugg1,"sugg2"=> $sugg2,"generalsugg1"=> $generalsugg1,"generalsugg2"=> $generalsugg2,"generalsugg3"=> $generalsugg3,"generalsugg4"=> $generalsugg4,"view" => $view,"brand_id"=> $brand_id,"subcat_id"=> $subcat_id,"cat_id"=> $cat_id,"pcat_id"=> $pcat_id);
		$flag=$this -> db -> insert("products", $insertinfo);	
		if($flag==true){
			return $flag;	
		}
	}
	public function get_products_data($pcat_id,$product_id,$cat_id,$subcat_id,$brand_id){
		$sql="select products.*,category.cat_id,category.cat_name,subcategory.subcat_id,subcategory.subcat_name,brands.brand_id,brands.brand_name from products,category,subcategory,brands where products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and products.product_id='$product_id' and products.active=1";
		$query =$this->db->query($sql);
		return $query->row_array();;	
	}
	public function edit_products($product_id,$product_name,$product_description,$sugg1,$sugg2,$generalsugg1,$generalsugg2,$generalsugg3,$generalsugg4,$brand_id,$subcat_id,$cat_id,$pcat_id,$view){
		$product_description=$this->db->escape_str($product_description);
		$sql = "UPDATE `products` SET product_name='$product_name',product_description='$product_description',view='$view',pcat_id='$pcat_id',cat_id='$cat_id',subcat_id='$subcat_id',brand_id='$brand_id',sugg1='$sugg1',sugg2='$sugg2',generalsugg1='$generalsugg1',generalsugg2='$generalsugg2',generalsugg3='$generalsugg3',generalsugg4='$generalsugg4' where `product_id` = '$product_id'";
		$query = $this->db->query($sql);
		if($query==true){
			return true;	
		}else{
			return false;
		}
	}
	public function update_products_selected($selected_list,$active){
		
		$sql_update_product="UPDATE `products` SET view=0,active='$active' WHERE product_id in ($selected_list)";
		$flag=$this->db->query($sql_update_product);
		
		$sql_inventory="select * from inventory where product_id in ($selected_list)";
		$query_inventory = $this->db->query($sql_inventory);
		
		$inventory_info = $query_inventory -> result();
		if(count($inventory_info)>0){
			$inventory_id_arr=array();
			foreach ($inventory_info as $inventory){
				$inventory_id_arr[]=$inventory->id;
			}
			$inventory_id_in=implode(',',$inventory_id_arr);
			$res_inv=$this->update_inventory_selected($inventory_id_in,$active);
			
		}else{
			$res_inv=true;		
		}
		if($flag==true && $res_inv==true){
			return true;
		}else{
			return false;
		}
	}
	public function delete_products_when_no_data($selected_list){	
		$sql_inv="select count(*) from inventory where product_id in ($selected_list) and purchased!=1 and active=1";
		$query_inv=$this->db->query($sql_inv);
		$result_inv=$query_inv->row_array();
		$count_inv=$result_inv['count(*)'];
		
		if($count_inv==0){
			$sql_delete_product="delete from `products` WHERE product_id in ($selected_list)";
			$flag_delete_product=$this->db->query($sql_delete_product);
			return $flag_delete_product;
		}else{
			return 2;
		}
	}
	public function vendors(){
		$query = $this->db->query("SELECT * FROM `vendors` where active=1");
		return $query -> result();
	}
	public function product_vendors(){
		$query = $this->db->query("SELECT * FROM `product_vendors` where active=1");
		return $query -> result();
	}
	
	public function get_logistics_services_name(){
		$query = $this->db->query("SELECT * FROM `logistics_service`");
		return $query -> result();
	}
	
	public function archived_vendors() {
		$query = $this->db->query("SELECT * FROM `vendors` where active=0");
		return $query -> result();
	}
	public function add_vendors($name,$email,$mobile,$landline,$address1,$address2,$pincode,$password,$image){
                $pass=md5($password);
		$insertinfo = array("name" => $name,"email" => $email,"password"=>$pass,"password_org"=>$password, "mobile" => $mobile, "landline" => $landline,"address1" => $address1,"address2" => $address2,"pincode" => $pincode,'image'=>$image);
		$flag=$this -> db -> insert("vendors", $insertinfo);
		return $flag;
	}
	public function get_vendors_data($vendor_id){
		$query =$this->db->query("SELECT * FROM `vendors` Where vendor_id='$vendor_id' and active=1");
		return $query->row_array();		
	}
	public function get_vendors_data_obj(){
		$query =$this->db->query("SELECT * FROM `vendors`");
		return $query->result();		
	}
	
	public function get_product_vendors_data($vendor_id){
		$query =$this->db->query("SELECT * FROM `product_vendors` Where vendor_id='$vendor_id' and active=1");
		return $query->row_array();		
	}
	public function get_product_vendors_data_obj(){
		$query =$this->db->query("SELECT * FROM `product_vendors`");
		return $query->result();		
	}
	public function get_vendors_data_obj_city_based(){
		$query =$this->db->query("SELECT * FROM `product_vendors` group by city");
		return $query->result();		
	}
	public function edit_vendors($vendor_id,$name,$email,$mobile,$landline,$address1,$address2,$pincode,$post_data_arr){

		
		
		/* new fields */
		$city=$this->db->escape_str((isset( $post_data_arr['city'])) ? $post_data_arr['city']: '');
		$state=(isset( $post_data_arr['state'])) ? $this->db->escape_str($post_data_arr['state']) : '';
		$country=(isset( $post_data_arr['country'])) ? $this->db->escape_str($post_data_arr['country']): '';
		
		$about_furniture_operations=(isset( $post_data_arr['about_furniture_operations'])) ? $post_data_arr['about_furniture_operations']: '';//take keys and put comma separated values
		if(!empty($about_furniture_operations)){
			$about_furniture_operations=$this->db->escape_str(implode(',',array_keys($about_furniture_operations)));
		}else{
			$about_furniture_operations='';
		}

		$applicable_media=(isset( $post_data_arr['country'])) ? $post_data_arr['applicable_media'] : '';


		$website_url=isset($applicable_media["website_url"]) ? $applicable_media["website_url"] : '';
		$instagram_url=isset($applicable_media["instagram_url"]) ? $applicable_media["instagram_url"] : '';
		$facebook_url=isset($applicable_media["facebook_url"]) ? $applicable_media["facebook_url"] : '';

		$size_of_team=(isset( $post_data_arr['size_of_team'])) ? $this->db->escape_str($post_data_arr['size_of_team']) : '';
		$kind_of_furniture=(isset( $post_data_arr['size_of_team'])) ? $post_data_arr['kind_of_furniture'] : '';
		if(!empty($kind_of_furniture)){
			$kind_of_furniture=$this->db->escape_str(implode(',',array_keys($kind_of_furniture)));
		}else{
			$kind_of_furniture='';
		}
		$years_of_business=(isset( $post_data_arr['years_of_business'])) ? $this->db->escape_str($post_data_arr['years_of_business']) : '';
		$shipping_capabilities=(isset( $post_data_arr['shipping_capabilities'])) ? $this->db->escape_str($post_data_arr['shipping_capabilities']) : ''; 
		$shipping_capabilities_description=(isset( $post_data_arr['shipping_capabilities_description'])) ? $this->db->escape_str($post_data_arr['shipping_capabilities_description']) : '';
		
		$whatsapp_number=(isset( $post_data_arr['whatsapp_number'])) ? $this->db->escape_str($post_data_arr['whatsapp_number']) : '';

		/* new fields */

		$data=array(
			"name"=>$name,
			"email"=>$email,
			"mobile"=>$mobile,
			"landline"=>$landline,
			"address1"=>$this->db->escape_str($address1),
			"address2"=>$this->db->escape_str($address2),
			"pincode"=>$this->db->escape_str($pincode),
			"city"=>$city,
			"state"=>$state,
			"country"=>$country,
			"about_furniture_operations"=>$about_furniture_operations,
			"website_url"=>$this->db->escape_str($website_url),
			"instagram_url"=>$this->db->escape_str($instagram_url),
			"facebook_url"=>$this->db->escape_str($facebook_url),
			"size_of_team"=>$size_of_team,
			"kind_of_furniture"=>$kind_of_furniture,
			"years_of_business"=>$years_of_business,
			"shipping_capabilities"=>$shipping_capabilities,
			"shipping_capabilities_description"=>$shipping_capabilities_description,
			"whatsapp_number"=>$whatsapp_number
			);

			$this->db->where(array("vendor_id"=>$vendor_id));
			$query=$this->db->update("product_vendors",$data);	

		//$sql = "UPDATE `vendors` SET name='$name',email='$email',mobile='$mobile',landline='$landline',address1='$address1',address2='$address2',pincode='$pincode' WHERE `vendor_id` = '$vendor_id'";
		
		//$result=$this->db->query($sql);
		
		return $query;
	}
	public function update_vendors_selected($selected_list,$active){

		$sql_update="update vendors set active='$active' where vendor_id in ($selected_list)";
		$flag1=$this->db->query($sql_update);
		$sql_select="select logistics_id from logistics where vendor_id in ($selected_list)";
		$query=$this->db->query($sql_select);
		$result=$query->result();
		if(count($result)>0){
			$temp_arr=array();
			foreach($result as $logistics_id_val){
				$temp_arr[]=$logistics_id_val->logistics_id;
			}
			$logistics_id_in=implode(',',$temp_arr);
			$flag2=$this->delete_logistics_selected($logistics_id_in);
		}else{
			$flag2=true;
		}
		$sql_select_inv="select id from inventory where vendor_id in ($selected_list)";
		$query_inv=$this->db->query($sql_select_inv);
		$result_inv=$query_inv->result();
		if(count($result_inv)>0){
			$temp_arr_inv=array();
			foreach($result_inv as $inventory_id_val){
				$temp_arr_inv[]=$inventory_id_val->id;
			}
			$inventory_id_in=implode(',',$temp_arr_inv);
			$flag3=$this->update_vendor_removed_in_inventory($inventory_id_in,$active);
			if($active==0){
				$flag4=$this->update_inventory_selected($inventory_id_in,$active);
			}else{
				$flag4=true;
			}		
		}else{
			$flag3=true;
			$flag4=true;
		}
		if($flag1==true && $flag2==true && $flag3==true && $flag4=true){
			return true;
		}else{
			return false;
		}
	}
	public function get_vendor_removed_inventories($selected_list){
		$sql_select_inv="select id from inventory where vendor_id in ($selected_list)";
		$query_inv=$this->db->query($sql_select_inv);
		$result_inv=$query_inv->result();
		if(count($result_inv)>0){
			$temp_arr_inv=array();
			foreach($result_inv as $inventory_id_val){
				$temp_arr_inv[]=$inventory_id_val->id;
			}
			return $inventory_id_in=implode(',',$temp_arr_inv);
		}else{
			return '';
		}
		
	}
	public function update_vendor_removed_in_inventory($inventory_id_in,$active){
		$sql = "UPDATE `inventory` SET vendor_removed=1 and active='$active' WHERE `id` in ($inventory_id_in)";
		$query=$this->db->query($sql);
		return $query;
	}
	public function inventory($product_id){
		$query = $this->db->query("select * from inventory where product_id='$product_id'");
		return $query -> result();
	}
	
	public function get_all_specification_group($pcat_id,$cat_id,$subcat_id){
		
		if($pcat_id==0){
			$specification_group_id_arr=array();
			$sql="select cat_view,specification_group_id from specification_group_to_categories where cat_id='$cat_id'";
			$query =$this->db->query($sql);
			$res=$query->result_array();
			
			foreach($res as $row){
				if($row["cat_view"]=="1"){
					$specification_group_id_arr[]=$row["specification_group_id"];
				}
			}
			
			$sql="select subcat_view,specification_group_id from specification_group_to_categories where subcat_id='$subcat_id'";
			$query =$this->db->query($sql);
			$res=$query->result_array();
			
			foreach($res as $row){
				if($row["subcat_view"]=="1"){
					$specification_group_id_arr[]=$row["specification_group_id"];
				}
			}
			$specification_group_id_in=implode(",",$specification_group_id_arr);
			
			$sql="select * from specification_group,specification_group_to_categories where specification_group.specification_group_id=specification_group_to_categories.specification_group_id";
			if($specification_group_id_in!=""){
				$sql.=" and specification_group_to_categories.specification_group_id in ($specification_group_id_in)";
			}
		}else{
			$specification_group_id_arr=array();
			$sql="select pcat_view,specification_group_id from specification_group_to_categories where pcat_id='$pcat_id'";
			$query =$this->db->query($sql);
			$res=$query->result_array();
			foreach($res as $row){
				if($row["pcat_view"]=="1"){
					$specification_group_id_arr[]=$row["specification_group_id"];
				}
			}
			
			$sql="select cat_view,specification_group_id from specification_group_to_categories where cat_id='$cat_id'";
			$query =$this->db->query($sql);
			$res=$query->result_array();
			
			foreach($res as $row){
				if($row["cat_view"]=="1"){
					$specification_group_id_arr[]=$row["specification_group_id"];
				}
			}
			
			$sql="select subcat_view,specification_group_id from specification_group_to_categories where subcat_id='$subcat_id'";
			$query =$this->db->query($sql);
			$res=$query->result_array();
			
			foreach($res as $row){
				if($row["subcat_view"]=="1"){
					$specification_group_id_arr[]=$row["specification_group_id"];
				}
			}
			$specification_group_id_in=implode(",",$specification_group_id_arr);
			
			$sql="select * from specification_group,specification_group_to_categories where specification_group.specification_group_id=specification_group_to_categories.specification_group_id";
			if($specification_group_id_in!=""){
				$sql.=" and specification_group_to_categories.specification_group_id in ($specification_group_id_in)";
			}
			
		}
		$query =$this->db->query($sql);
		$res=$query->result();
		return $res;
	}
	public function get_all_specification($specification_group_id){
		$sql="select * from specification,specification_group where  specification.specification_group_id='$specification_group_id' and specification.specification_group_id=specification_group.specification_group_id";	
		$query =$this->db->query($sql);
		$res=$query->result();
		return $res;
	}
	public function get_all_filterbox($pcat_id,$cat_id,$subcat_id){
		if($pcat_id==0){
			$sql="select * from filterbox,filterbox_to_category where filterbox.filterbox_id=filterbox_to_category.filterbox_id and filterbox_to_category.cat_id='$cat_id'";
		}else{
			
			$filterbox_id_arr=array();
			$sql="select pcat_view,filterbox_id from filterbox_to_category where pcat_id='$pcat_id'";
			$query =$this->db->query($sql);
			$res=$query->result_array();
			foreach($res as $row){
				if($row["pcat_view"]=="1"){
					$filterbox_id_arr[]=$row["filterbox_id"];
				}
			}
			
			$sql="select cat_view,filterbox_id from filterbox_to_category where cat_id='$cat_id'";
			$query =$this->db->query($sql);
			$res=$query->result_array();
			
			foreach($res as $row){
				if($row["cat_view"]=="1"){
					$filterbox_id_arr[]=$row["filterbox_id"];
				}
			}
			
			$sql="select subcat_view,filterbox_id from filterbox_to_category where subcat_id='$subcat_id'";
			$query =$this->db->query($sql);
			$res=$query->result_array();
			
			foreach($res as $row){
				if($row["subcat_view"]=="1"){
					$filterbox_id_arr[]=$row["filterbox_id"];
				}
			}
			$filterbox_id_in=implode(",",$filterbox_id_arr);
			if(!empty($filterbox_id_in)){
				$sql="select * from filterbox,filterbox_to_category where filterbox.filterbox_id=filterbox_to_category.filterbox_id and filterbox_to_category.pcat_id='$pcat_id' and filterbox_to_category.filterbox_id in ($filterbox_id_in)";
			}else{
				$sql="select * from filterbox,filterbox_to_category where filterbox.filterbox_id=filterbox_to_category.filterbox_id and filterbox_to_category.pcat_id='$pcat_id'";
			}
			
			
		}
		$query =$this->db->query($sql);
		return $res=$query->result();	
	}
	public function get_all_filter($filterbox_id){
		$sql="select * from filter,filterbox where filter.filterbox_id='$filterbox_id' and filter.filterbox_id=filterbox.filterbox_id";	
		$query =$this->db->query($sql);
		return $res=$query->result();	
	}
	
	public function get_all_attributes($subcat_id){
		$sql="select * from attribute where subcat_id='$subcat_id'";	
		$query =$this->db->query($sql);
		return $res=$query->result();	
	}
	public function get_all_shipping_charge($vendor_id){
		$query = $this->db->query("SELECT shipping_charge.*,logistics_service.logistics_service_id,logistics_service.city,logistics.logistics_name,logistics.logistics_id FROM logistics_service,logistics,shipping_charge where logistics.logistics_id=logistics_service.logistics_id and shipping_charge.vendor_id='$vendor_id' and shipping_charge.logistics_service_id = logistics_service.logistics_service_id");
		return $query -> result();
	}
			
	public function add_inventory($product_id,$sku_id,$sku_display_name_on_button_1,$sku_display_name_on_button_2,$attribute_1,$attribute_1_value,$attribute_2,$attribute_2_value,$attribute_3,$attribute_3_value,$attribute_4,$attribute_4_value,$common_image,$image_arr,$thumbnail_arr,$largeimage_arr,$vendor_id,$base_price,$selling_price,$cost_price,$purchased_price,$moq,$max_oq,$logistics_parcel_category_id,$inventory_weight_in_kg,$inventory_volume_in_kg,$shipping_charge_not_applicable,$flat_shipping_charge,$inventory_length,$inventory_breadth,$inventory_height,$logistics_territory_id,$tax,$stock,$add_to_cart,$request_for_quotation,$product_status,$product_status_view,$low_stock,$cutoff_stock,$specification_textarea_value_arr,$specification_select_value_arr,$filter_arr,$highlight,$selling_discount,$max_selling_price,$tax_percent_price,$taxable_price,$CGST,$IGST,$SGST,$sku_name,$highlight_faq,$highlight_whatsinpack,$highlight_aboutthebrand,$file_1_name,$file_2_name,$file_3_name,$file_4_name,$file_5_name,$file_6_name,$downloads_success_arr,$prod_tech_spec_name_1,$prod_tech_spec_desc_1,$prod_tech_spec_name_2,$prod_tech_spec_desc_2,$prod_tech_spec_name_3,$prod_tech_spec_desc_3,$prod_tech_spec_name_4,$prod_tech_spec_desc_4,$prod_tech_spec_name_5,$prod_tech_spec_desc_5,$prod_tech_spec_name_6,$prod_tech_spec_desc_6,$prod_tech_spec_name_7,$prod_tech_spec_desc_7,$prod_tech_spec_name_8,$prod_tech_spec_desc_8,$prod_tech_spec_name_9,$prod_tech_spec_desc_9,$prod_tech_spec_name_10,$prod_tech_spec_desc_10,$testimonial_name_1,$testimonial_desc_1,$testimonial_name_2,$testimonial_desc_2,$testimonial_name_3,$testimonial_desc_3,$warranty,$shipswithin,$under_pcat_frontend,$brouchure,$emi_options,$price_validity,$material,$inventory_unit,$position_text,$position_uhave,$position_others_have,$positioning_uhave_title,$positioning_others_have_title,$inventory_addon_weight_in_kg,$inventory_type=''){
		
		if($inventory_type=='2'){
			$moq=1;
			$moq_base_price=$selling_price;
			$purchased_price=$cost_price;
		}else{
			$moq_base_price=$moq*$base_price;
		}

		if($inventory_type==''){
			/* consider as a main */
			$inventory_type=1;
			/* consider as a main */
		}

		//$moq_base_price=$moq*$base_price;
		$moq_price=$moq*$selling_price;
		//$moq_purchased_price=$moq*$purchased_price;
		// I added the below if else 6/11/2021 packs
		if(trim($purchased_price)==""){
			$moq_purchased_price="";
		}
		else{
			$moq_purchased_price=$moq*$purchased_price;
		}
		$file_1_upload="";
		$file_2_upload="";
		$file_3_upload="";
		$file_4_upload="";
		$file_5_upload="";
		$file_6_upload="";
		
		if(!empty($downloads_success_arr)){
			foreach($downloads_success_arr as $kk => $vv){
				if($kk=="file_1_upload"){
					$file_1_upload=$vv;
				}
				if($kk=="file_2_upload"){
					$file_2_upload=$vv;
				}
				if($kk=="file_3_upload"){
					$file_3_upload=$vv;
				}
				if($kk=="file_4_upload"){
					$file_4_upload=$vv;
				}
				if($kk=="file_5_upload"){
					$file_5_upload=$vv;
				}
				if($kk=="file_6_upload"){
					$file_6_upload=$vv;
				}
			}
		}
		
		$insertinfo = array("product_id" => $product_id,"sku_display_name_on_button_1"=>$sku_display_name_on_button_1,"sku_display_name_on_button_2"=>$sku_display_name_on_button_2,"sku_id" => $sku_id, "sku_name"=>$sku_name,"attribute_1" => $attribute_1,"attribute_1_value" => $attribute_1_value,"attribute_2" => $attribute_2, "attribute_2_value" => $attribute_2_value,"attribute_3" => $attribute_3,"attribute_3_value" => $attribute_3_value, "attribute_4" => $attribute_4,"attribute_4_value" => $attribute_4_value,"image" => $image_arr[0], "thumbnail" => $thumbnail_arr[0], "largeimage" => $largeimage_arr[0],"image2" => $image_arr[1], "thumbnail2" => $thumbnail_arr[1], "largeimage2" => $largeimage_arr[1],"image3" => $image_arr[2], "thumbnail3" => $thumbnail_arr[2], "largeimage3" => $largeimage_arr[2],"image4" => $image_arr[3], "thumbnail4" => $thumbnail_arr[3], "largeimage4" => $largeimage_arr[3],"image5" => $image_arr[4], "thumbnail5" => $thumbnail_arr[4], "largeimage5" => $largeimage_arr[4],"image6" => $image_arr[5], "thumbnail6" => $thumbnail_arr[5], "largeimage6" => $largeimage_arr[5],"vendor_id" => $vendor_id, "base_price"=>$base_price,"selling_price" => $selling_price,"cost_price"=>$cost_price,"purchased_price"=>$purchased_price,"moq" => $moq,"max_oq"=>$max_oq,"moq_base_price"=>$moq_base_price,"moq_price" => $moq_price,"moq_purchased_price"=>$moq_purchased_price,"tax_percent" => $tax,"stock" => $stock,"add_to_cart" => $add_to_cart,"request_for_quotation" => $request_for_quotation,"product_status" => $product_status,"product_status_view" => $product_status_view, "low_stock" => $low_stock,"cutoff_stock" => $cutoff_stock,"common_image"=>$common_image,"highlight"=>$highlight,"selling_discount"=>$selling_discount,"max_selling_price"=>$max_selling_price,"tax_percent_price"=>$tax_percent_price,"taxable_price"=>$taxable_price,"CGST"=>$CGST,"IGST"=>$IGST,"SGST"=>$SGST,"highlight_faq"=>$highlight_faq,"highlight_whatsinpack"=>$highlight_whatsinpack,"highlight_aboutthebrand"=>$highlight_aboutthebrand,"file_1_name"=>$file_1_name,"file_2_name"=>$file_2_name,"file_3_name"=>$file_3_name,"file_4_name"=>$file_4_name,"file_5_name"=>$file_5_name,"file_6_name"=>$file_6_name,"file_1_upload"=>$file_1_upload,"file_2_upload"=>$file_2_upload,"file_3_upload"=>$file_3_upload,"file_4_upload"=>$file_4_upload,"file_5_upload"=>$file_5_upload,"file_6_upload"=>$file_6_upload,
		
		"prod_tech_spec_name_1"=>$prod_tech_spec_name_1,"prod_tech_spec_desc_1"=>$prod_tech_spec_desc_1,
		"prod_tech_spec_name_2"=>$prod_tech_spec_name_2,"prod_tech_spec_desc_2"=>$prod_tech_spec_desc_2,
		"prod_tech_spec_name_3"=>$prod_tech_spec_name_3,"prod_tech_spec_desc_3"=>$prod_tech_spec_desc_3,
		"prod_tech_spec_name_4"=>$prod_tech_spec_name_4,"prod_tech_spec_desc_4"=>$prod_tech_spec_desc_4,
		"prod_tech_spec_name_5"=>$prod_tech_spec_name_5,"prod_tech_spec_desc_5"=>$prod_tech_spec_desc_5,
		"prod_tech_spec_name_6"=>$prod_tech_spec_name_6,"prod_tech_spec_desc_6"=>$prod_tech_spec_desc_6,
		"prod_tech_spec_name_7"=>$prod_tech_spec_name_7,"prod_tech_spec_desc_7"=>$prod_tech_spec_desc_7,
		"prod_tech_spec_name_8"=>$prod_tech_spec_name_8,"prod_tech_spec_desc_8"=>$prod_tech_spec_desc_8,
		"prod_tech_spec_name_9"=>$prod_tech_spec_name_9,"prod_tech_spec_desc_9"=>$prod_tech_spec_desc_9,
		"prod_tech_spec_name_10"=>$prod_tech_spec_name_10,"prod_tech_spec_desc_10"=>$prod_tech_spec_desc_10,
		"testimonial_name_1"=>$testimonial_name_1,"testimonial_desc_1"=>$testimonial_desc_1,
		"testimonial_name_2"=>$testimonial_name_2,"testimonial_desc_2"=>$testimonial_desc_2,
		"testimonial_name_3"=>$testimonial_name_3,"testimonial_desc_3"=>$testimonial_desc_3,
		"warranty"=>$warranty,"shipswithin"=>$shipswithin,
		"under_pcat_frontend"=>$under_pcat_frontend,"brouchure"=>$brouchure,
		"emi_options"=>$emi_options,"price_validity"=>$price_validity,
		"material"=>$material,
		"positioning_uhave_title"=>$positioning_uhave_title,
		"positioning_others_have_title"=>$positioning_others_have_title,"inventory_type"=>$inventory_type,"inventory_addon_weight_in_kg"=>$inventory_addon_weight_in_kg);

		if($inventory_type=='2'){
			$insertinfo["addon_product_allowed"]=0;
		}
		$user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");

		if($user_type=="vendor"){
			$insertinfo["added_by"]="vendor";
			$insertinfo["vendor_id"]=$a_id;
		}else{
			$insertinfo["added_by"]="admin";
			$insertinfo["vendor_id"]=$vendor_id;
		}

		$flag=$this -> db -> insert("inventory", $insertinfo);
		$inventory_id=$this->db->insert_id();

		//print_r($position_text);

		if(!empty($position_text)){
			foreach($position_text as $k=>$v){
				$text=$v;
				$uhave=isset($position_uhave[$k]) ? $position_uhave[$k] : 'no';
				$others_have=isset($position_others_have[$k]) ? $position_others_have[$k] : 'no';

				$insert_array=array('position_text'=>$text,'position_uhave'=>$uhave,'position_others_have'=>$others_have,'position_inventory_id'=>$inventory_id);

				$flag11=$this -> db -> insert("positioning", $insert_array);
			}
		}
		
		//exit;

		$flag1=$this->add_inventory_specification($inventory_id,$specification_textarea_value_arr,$specification_select_value_arr);
		$flag2=$this->add_products_filter($inventory_id,$filter_arr);

		//$flag3=$this->add_logistics_inventory($inventory_id,$logistics_parcel_category_id,$moq,$inventory_weight_in_kg,$inventory_volume_in_kg,$shipping_charge_not_applicable,$flat_shipping_charge,$inventory_length,$inventory_breadth,$inventory_height,$logistics_territory_id,$inventory_unit);

		if($inventory_type!='2'){
			$flag3=$this->add_logistics_inventory($inventory_id,$logistics_parcel_category_id,$moq,$inventory_weight_in_kg,$inventory_volume_in_kg,$shipping_charge_not_applicable,$flat_shipping_charge,$inventory_length,$inventory_breadth,$inventory_height,$logistics_territory_id,$inventory_unit);
		}else{
			$flag3=true;
		}

		$return_values=array();
		if($flag==true && $flag1==true && $flag2==true && $flag3==true){
			$return_values['res']=true;
			$return_values['inventory_id']=$inventory_id;
			return $return_values;
		}else{
			$return_values['res']=false;
			$return_values['inventory_id']='';
			return $return_values;
		}
	}
	public function add_logistics_inventory($inventory_id,$logistics_parcel_category_id,$moq,$inventory_weight_in_kg,$inventory_volume_in_kg,$shipping_charge_not_applicable,$flat_shipping_charge,$inventory_length,$inventory_breadth,$inventory_height,$logistics_territory_id,$inventory_unit=''){
		
		$insertinfo = array("inventory_id"=>$inventory_id,"logistics_parcel_category_id" => $logistics_parcel_category_id,"inventory_moq" => $moq,"inventory_weight_in_kg" => $inventory_weight_in_kg,"inventory_volume_in_kg" => $inventory_volume_in_kg,'shipping_charge_not_applicable'=>$shipping_charge_not_applicable,'flat_shipping_charge'=>$flat_shipping_charge,'inventory_length'=>$inventory_length,'inventory_breadth'=>$inventory_breadth,'inventory_height'=>$inventory_height,'free_shipping_territories'=>$logistics_territory_id,'inventory_unit'=>$inventory_unit);
		$flag=$this -> db -> insert("logistics_inventory", $insertinfo);
		return $flag;
	}
	public function add_inventory_specification($inventory_id,$specification_textarea_value_arr,$specification_select_value_arr){
		if(!empty($specification_select_value_arr)){
			foreach($specification_select_value_arr as $specification_id => $val){
				if($val=="Others"){
					$spec_val=$specification_textarea_value_arr[$specification_id];
					$value_texted=1;
				}else{
					$spec_val=$val;
					$value_texted=0;
				}

				$insertinfo = array("inventory_id"=>$inventory_id,"specification_id" => $specification_id,"specification_value" => $spec_val,"value_texted" => $value_texted);
				if($spec_val!='0' && $spec_val!=""){
				$flag=$this -> db -> insert("inventory_specification", $insertinfo);
				}
			}
			return $flag;
		}
		else{
			return true;
		}
	}
	public function add_products_filter($inventory_id,$filter_arr){
		$flag=true;
		if(!empty($filter_arr)){
			foreach($filter_arr as $key => $filter_id){
				if($filter_id!=0){
					$insertinfo = array("inventory_id"=>$inventory_id,"filter_id" => $filter_id);
					$flag=$this -> db -> insert("products_filter", $insertinfo);
				}
				
			}
		}

		return $flag;
		
	}
	public function get_all_inventory_data($product_id,$inventory_id){

		$sql="select *, inventory.vendor_id as inv_vendor_id from inventory";
		
		$sql.= ' LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`'
				.' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`'
				.' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`'
				.' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`'
				.' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id` ';
				
		$sql.=' LEFT JOIN `logistics_inventory` ON `logistics_inventory`.`inventory_id` = `inventory`.`id`';
		
		$sql.=" where inventory.product_id='$product_id' and inventory.id='$inventory_id'";

		//echo $sql;
		$query = $this->db->query($sql);

		return $query -> row_array();
	}
	public function get_all_filter_by_inventory_id($inventory_id){
		$query = $this->db->query("select products_filter.filter_id,products_filter.product_filter_id,filter.filterbox_id,filter.filter_options,filterbox_name from products_filter,filter,filterbox where products_filter.inventory_id='$inventory_id' and products_filter.filter_id=filter.filter_id and filter.filterbox_id=filterbox.filterbox_id");
		return $query -> result_array();
	}
	public function get_all_specification_by_inventory_id($inventory_id){
		$query = $this->db->query("select * from inventory_specification where  inventory_id='$inventory_id'");
		return $query -> result_array();
	}
	public function get_all_specification_ids($subcat_id){
		$query = $this->db->query("select specification.specification_id from specification,specification_group,specification_group_to_categories where  specification_group_to_categories.subcat_id='$subcat_id' and specification.specification_group_id=specification_group.specification_group_id and specification_group.specification_group_id=specification_group_to_categories.specification_group_id");
		return $query -> result();
	}
	public function get_all_filterbox_ids($subcat_id){
		$query = $this->db->query("select filterbox.filterbox_id from filterbox,filterbox_to_category where  filterbox_to_category.subcat_id='$subcat_id' and filterbox.filterbox_id=filterbox_to_category.filterbox_id");
		return $query -> result();
	}
	public function edit_inventory($inventory_id,$product_id,$sku_display_name_on_button_1,$sku_display_name_on_button_2,$sku_id,$attribute_1,$attribute_1_value,$attribute_2,$attribute_2_value,$attribute_3,$attribute_3_value,$attribute_4,$attribute_4_value,$common_image,$image_arr,$thumbnail_arr,$largeimage_arr,$vendor_id,$base_price,$selling_price,$cost_price,$purchased_price,$moq,$max_oq,$logistics_parcel_category_id,$inventory_weight_in_kg,$inventory_volume_in_kg,$shipping_charge_not_applicable,$flat_shipping_charge,$inventory_length,$inventory_breadth,$inventory_height,$logistics_territory_id,$tax,$stock,$edit_add_to_cart,$edit_request_for_quotation,$product_status,$product_status_view,$low_stock,$cutoff_stock,$specification_textarea_value_arr,$specification_select_value_arr,$filter_arr,$inventory_specification_id_arr,$product_filter_id_arr,$highlight,$selling_discount,$max_selling_price,$tax_percent_price,$taxable_price,$CGST,$IGST,$SGST,$sku_name,$highlight_faq,$highlight_whatsinpack,$highlight_aboutthebrand,$file_1_name,$file_2_name,$file_3_name,$file_4_name,$file_5_name,$file_6_name,$downloads_success_arr,$prod_tech_spec_name_1,$prod_tech_spec_desc_1,$prod_tech_spec_name_2,$prod_tech_spec_desc_2,$prod_tech_spec_name_3,$prod_tech_spec_desc_3,$prod_tech_spec_name_4,$prod_tech_spec_desc_4,$prod_tech_spec_name_5,$prod_tech_spec_desc_5,$prod_tech_spec_name_6,$prod_tech_spec_desc_6,$prod_tech_spec_name_7,$prod_tech_spec_desc_7,$prod_tech_spec_name_8,$prod_tech_spec_desc_8,$prod_tech_spec_name_9,$prod_tech_spec_desc_9,$prod_tech_spec_name_10,$prod_tech_spec_desc_10,$testimonial_name_1,$testimonial_desc_1,$testimonial_name_2,$testimonial_desc_2,$testimonial_name_3,$testimonial_desc_3,$warranty,$shipswithin,$under_pcat_frontend,$brouchure,$emi_options,$price_validity,$material,$inventory_unit,$position_text,$position_uhave,$position_others_have,$position_id,$positioning_uhave_title,$positioning_others_have_title,$inventory_addon_weight_in_kg,$inventory_type=''){

		//echo $moq;
		//echo $base_price;
		if($inventory_type!='2'){
			$moq=1;
			$moq_base_price=$selling_price;
			$purchased_price=$cost_price;
		}else{
			$moq_base_price=$moq*$base_price;
		}
		//$moq_base_price=($moq*$selling_price);//$base_price
		$moq_price=$moq*$selling_price;
		//$moq_purchased_price=$moq*$purchased_price;
		// I added the below if else 6/11/2021 packs
		if(trim($purchased_price)==""){
			$moq_purchased_price="";
		}
		else{
			$moq_purchased_price=$moq*$purchased_price;
		}
		
		
		$img_str="";
		
		
		for($j=1;$j<=6;$j++){
			
			if($j==1){
				if(!empty($thumbnail_arr[0])){
				
					$img_str.="thumbnail='".$thumbnail_arr[0]."',";
				}
				if(empty($thumbnail_arr[0])){
				
					$img_str.="thumbnail='',";
				}
				if(!empty($image_arr[0])){
				
					$img_str.="image='".$image_arr[0]."',";
				}
				if(empty($image_arr[0])){
				
					$img_str.="image='',";
				}
				if(!empty($largeimage_arr[0])){
				
					$img_str.="largeimage='".$largeimage_arr[0]."',";
				}
				if(empty($largeimage_arr[0])){
				
					$img_str.="largeimage='',";
				}
				
			}
			else{
				$i=$j-1;
				if(!empty($thumbnail_arr[$i])){
					
					$img_str.="thumbnail".$j."='".$thumbnail_arr[$i]."',";
				}
				if(empty($thumbnail_arr[$i])){
					
					$img_str.="thumbnail".$j."='',";
				}
				if(!empty($image_arr[$i])){
					
					$img_str.="image".$j."='".$image_arr[$i]."',";
				}
				if(empty($image_arr[$i])){
					
					$img_str.="image".$j."='',";
				}
				if(!empty($largeimage_arr[$i])){
					
					$img_str.="largeimage".$j."='".$largeimage_arr[$i]."',";
				}
				if(empty($largeimage_arr[$i])){
					
					$img_str.="largeimage".$j."='',";
				}
			}
			
		}
		
		
		$sql="select file_1_upload,file_2_upload,file_3_upload,file_4_upload,file_5_upload,file_6_upload from inventory where id='$inventory_id' and sku_id='$sku_id'";
		$query=$this->db->query($sql);
		$row_arr=$query->row_array();
		
		
		$file_1_upload=$row_arr["file_1_upload"];
		$file_2_upload=$row_arr["file_2_upload"];
		$file_3_upload=$row_arr["file_3_upload"];
		$file_4_upload=$row_arr["file_4_upload"];
		$file_5_upload=$row_arr["file_5_upload"];
		$file_6_upload=$row_arr["file_6_upload"];
		
		if(!empty($downloads_success_arr)){
			foreach($downloads_success_arr as $kk => $vv){
				if($kk=="file_1_upload"){
					$file_1_upload=$vv;
				}
				if($kk=="file_2_upload"){
					$file_2_upload=$vv;
				}
				if($kk=="file_3_upload"){
					$file_3_upload=$vv;
				}
				if($kk=="file_4_upload"){
					$file_4_upload=$vv;
				}
				if($kk=="file_5_upload"){
					$file_5_upload=$vv;
				}
				if($kk=="file_6_upload"){
					$file_6_upload=$vv;
				}
			}
		}
		
		
		
		
		
		
		$img_str=",".$img_str;
		
		 $sql = "UPDATE `inventory` SET sku_display_name_on_button_1='$sku_display_name_on_button_1',sku_display_name_on_button_2='$sku_display_name_on_button_2',sku_id='$sku_id',sku_name='".$sku_name."',attribute_1='$attribute_1',attribute_1_value='$attribute_1_value',attribute_2='$attribute_2',attribute_2_value='$attribute_2_value',attribute_3='$attribute_3',attribute_3_value='$attribute_3_value',attribute_4='$attribute_4',attribute_4_value='$attribute_4_value',common_image='$common_image'".$img_str."vendor_id='$vendor_id',base_price='$base_price',selling_price='$selling_price',cost_price='$cost_price',purchased_price='$purchased_price',moq='$moq',max_oq='$max_oq',moq_base_price='$moq_base_price',moq_price='$moq_price',moq_purchased_price='$moq_purchased_price',tax_percent='$tax',stock='$stock',add_to_cart='$edit_add_to_cart',request_for_quotation='$edit_request_for_quotation',product_status='$product_status',product_status_view='$product_status_view',low_stock='$low_stock',cutoff_stock='$cutoff_stock',highlight='$highlight',highlight_faq='$highlight_faq',highlight_whatsinpack='$highlight_whatsinpack',highlight_aboutthebrand='$highlight_aboutthebrand',file_1_name='$file_1_name',file_2_name='$file_2_name',file_3_name='$file_3_name',file_4_name='$file_4_name',file_5_name='$file_5_name',file_6_name='$file_6_name',file_1_upload='$file_1_upload',file_2_upload='$file_2_upload',file_3_upload='$file_3_upload',file_4_upload='$file_4_upload',file_5_upload='$file_5_upload',file_6_upload='$file_6_upload',
		    prod_tech_spec_name_1='$prod_tech_spec_name_1',
			prod_tech_spec_desc_1='$prod_tech_spec_desc_1',
			prod_tech_spec_name_2='$prod_tech_spec_name_2',
			prod_tech_spec_desc_2='$prod_tech_spec_desc_2',
			prod_tech_spec_name_3='$prod_tech_spec_name_3',
			prod_tech_spec_desc_3='$prod_tech_spec_desc_3',
			prod_tech_spec_name_4='$prod_tech_spec_name_4',
			prod_tech_spec_desc_4='$prod_tech_spec_desc_4',
			prod_tech_spec_name_5='$prod_tech_spec_name_5',
			prod_tech_spec_desc_5='$prod_tech_spec_desc_5',
			prod_tech_spec_name_6='$prod_tech_spec_name_6',
			prod_tech_spec_desc_6='$prod_tech_spec_desc_6',
			prod_tech_spec_name_7='$prod_tech_spec_name_7',
			prod_tech_spec_desc_7='$prod_tech_spec_desc_7',
			prod_tech_spec_name_8='$prod_tech_spec_name_8',
			prod_tech_spec_desc_8='$prod_tech_spec_desc_8',
			prod_tech_spec_name_9='$prod_tech_spec_name_9',
			prod_tech_spec_desc_9='$prod_tech_spec_desc_9',
			prod_tech_spec_name_10='$prod_tech_spec_name_10',
			prod_tech_spec_desc_10='$prod_tech_spec_desc_10',
			testimonial_name_1='$testimonial_name_1',
			testimonial_desc_1='$testimonial_desc_1',
			testimonial_name_2='$testimonial_name_2',
			testimonial_desc_2='$testimonial_desc_2',
			testimonial_name_3='$testimonial_name_3',
			testimonial_desc_3='$testimonial_desc_3',
			warranty='$warranty',
			shipswithin='$shipswithin',
			under_pcat_frontend='$under_pcat_frontend',
			brouchure='$brouchure',
			emi_options='$emi_options',
			price_validity='$price_validity',
			material='$material',
			positioning_uhave_title='$positioning_uhave_title',
			positioning_others_have_title='$positioning_others_have_title',

			selling_discount='$selling_discount',max_selling_price='$max_selling_price',tax_percent_price ='$tax_percent_price',taxable_price='$taxable_price',CGST='$CGST',IGST='$IGST',SGST='$SGST',inventory_addon_weight_in_kg='$inventory_addon_weight_in_kg' WHERE `id` = '$inventory_id'";


		/* updating positioning  */

		//print_r($position_text);

		if(!empty($position_text)){
			foreach($position_text as $k=>$v){
				$text=$v;
				$uhave=isset($position_uhave[$k]) ? $position_uhave[$k] : 'no';
				$others_have=isset($position_others_have[$k]) ? $position_others_have[$k] : 'no';
				$p_inventory_id=$inventory_id;
				$p_id=isset($position_id[$k]) ? $position_id[$k] : '';

				if($p_id!=''){
					$sql_p = "UPDATE `positioning` SET position_text='$text',position_uhave='$uhave',position_others_have='$others_have' WHERE `position_id` = '$p_id'";
					$result_p=$this->db->query($sql_p);

				}else{
				
					$insert_array=array('position_text'=>$text,'position_uhave'=>$uhave,'position_others_have'=>$others_have,'position_inventory_id'=>$p_inventory_id);

					$flag11=$this -> db -> insert("positioning", $insert_array);
				}
			}
		}
		
		/* updating positioning  */

		
		
		
		if($inventory_type!='2'){

			$flag1=$this->edit_inventory_specification($inventory_id,$specification_textarea_value_arr,$specification_select_value_arr,$inventory_specification_id_arr);
			
			$flag2=$this->edit_products_filter($inventory_id,$filter_arr,$product_filter_id_arr);
			
			$flag3=$this->edit_logistics_inventory($inventory_id,$logistics_parcel_category_id,$moq,$inventory_weight_in_kg,$inventory_volume_in_kg,$shipping_charge_not_applicable,$flat_shipping_charge,$inventory_length,$inventory_breadth,$inventory_height,$logistics_territory_id,$inventory_unit);
		}else{
			$flag1=true;
			$flag2=true;
			$flag3=true;
		}

		$result=$this->db->query($sql);
		
		// && $flag3 // in if condition && changed to ||

		if($flag1==true || $flag2==true || $result==true ){
			return 1;
		}else{
			return 0;
		}
	}
	
	public function update_delete_file_n_upload($update_delete_file_n_upload_arr,$update_delete_file_n_name_arr,$inventory_id,$sku_id){
		$update_delete_file_n_upload_in=implode(",",$update_delete_file_n_upload_arr);
		$sql="select $update_delete_file_n_upload_in from inventory where id='$inventory_id' and sku_id='$sku_id'";
		$query=$this->db->query($sql);
		$row_arr=$query->row_array();
		foreach($row_arr as $key_name => $value_name){
			if(file_exists($value_name) && $value_name!=''){
				unlink($value_name);
				rmdir(str_replace("/".basename($value_name),"",$value_name));
			}
			if($key_name=="file_1_upload"){
				$sql_update="update inventory set file_1_name='',file_1_upload='' where id='$inventory_id' and sku_id='$sku_id'";
				$query_update=$this->db->query($sql_update);
			}
			if($key_name=="file_2_upload"){
				$sql_update="update inventory set file_2_name='',file_2_upload='' where id='$inventory_id' and sku_id='$sku_id'";
				$query_update=$this->db->query($sql_update);
			}
			if($key_name=="file_3_upload"){
				$sql_update="update inventory set file_3_name='',file_3_upload='' where id='$inventory_id' and sku_id='$sku_id'";
				$query_update=$this->db->query($sql_update);
			}
			if($key_name=="file_4_upload"){
				$sql_update="update inventory set file_4_name='',file_4_upload='' where id='$inventory_id' and sku_id='$sku_id'";
				$query_update=$this->db->query($sql_update);
			}
			if($key_name=="file_5_upload"){
				$sql_update="update inventory set file_5_name='',file_5_upload='' where id='$inventory_id' and sku_id='$sku_id'";
				$query_update=$this->db->query($sql_update);
			}
			if($key_name=="file_6_upload"){
				$sql_update="update inventory set file_6_name='',file_6_upload='' where id='$inventory_id' and sku_id='$sku_id'";
				$query_update=$this->db->query($sql_update);
			}
		}
	}
	
	public function edit_inventory_specification($inventory_id,$specification_textarea_value_arr,$specification_select_value_arr,$inventory_specification_id_arr){
		if(!empty($specification_select_value_arr)){
			$res=array();
			foreach($specification_select_value_arr as $specification_id => $val){
				$inventory_specification_id=$inventory_specification_id_arr[$specification_id];
				
				if($val=="Others"){
					$spec_val=addslashes($specification_textarea_value_arr[$specification_id]);
					$value_texted=1;
					if($spec_val!="" && $inventory_specification_id!='' && $val!='0'){
						$sql = "UPDATE `inventory_specification` SET specification_value='$spec_val',value_texted='$value_texted' WHERE `inventory_specification_id` = '$inventory_specification_id'";

					}else{
						
						if($inventory_specification_id=='' && $val!='0' && $val!="" && $val=="Others"){
							
							$sql="insert into `inventory_specification` set specification_value='$spec_val',value_texted='$value_texted',inventory_id='$inventory_id',specification_id='$specification_id'";
						}elseif(($inventory_specification_id!='') && $spec_val=="0"){
							
							$sql="delete from `inventory_specification` where `inventory_specification_id` = '$inventory_specification_id'";
						
						}elseif($inventory_specification_id=='' && $val=='0'){
							$sql='';
						}
					}
				}else{
					$spec_val=addslashes($val);
					$value_texted=0;
					
					if($spec_val!="" && $inventory_specification_id!=''){
						$sql = "UPDATE `inventory_specification` SET specification_value='$spec_val',value_texted='$value_texted' WHERE `inventory_specification_id` = '$inventory_specification_id'";

					}else{
						
						if($inventory_specification_id=="" && $spec_val!='0' && $spec_val!=""){
							$sql="insert into `inventory_specification` set specification_value='$spec_val',value_texted='$value_texted',inventory_id='$inventory_id',specification_id='$specification_id'";
						}elseif(($inventory_specification_id!="") && $spec_val==""){
							
							$sql="delete from `inventory_specification` where `inventory_specification_id` = '$inventory_specification_id'";
						
						}elseif($inventory_specification_id=="" && $spec_val=='0'){
							$sql='';
						}
						elseif($inventory_specification_id=="" && $spec_val==''){
							$sql='';
						}
					}
				}
				
				//echo $sql;
				if($sql!=''){
					$result=$this->db->query($sql);
					$res[]=$result;
				}
				else{
					$res[]=true;
				}
			}
		
			if(in_array("1", $res)){
				return true;
			}else{
				return false;
			}
		}
		else{
			return true;
		}
		
	}
	public function edit_products_filter($inventory_id,$filter_arr,$product_filter_id_arr){
		$res=array();
		if(!empty($filter_arr)){
		foreach($filter_arr as $key => $filter_id){
			$product_filter_id=$product_filter_id_arr[$key];
			if($filter_id!=0){
				if($product_filter_id!=""){
					$sql = "UPDATE `products_filter` SET filter_id='$filter_id' WHERE `product_filter_id` = '$product_filter_id'";
					$result=$this->db->query($sql);
			$res[]=$result;
				}
				else{
					$sql = "insert into `products_filter` SET filter_id='$filter_id',inventory_id='$inventory_id'";
					$result=$this->db->query($sql);
			$res[]=$result;
				}
			}
			if($filter_id==0){
				if($product_filter_id!=""){
				$sql="delete from products_filter where product_filter_id='$product_filter_id'";
				$result=$this->db->query($sql);
			$res[]=$result;
				}

			}


		
		}
	
			if(in_array("1", $res)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}

	}
	public function edit_logistics_inventory($inventory_id,$logistics_parcel_category_id,$moq,$inventory_weight_in_kg,$inventory_volume_in_kg,$shipping_charge_not_applicable,$flat_shipping_charge,$inventory_length,$inventory_breadth,$inventory_height,$logistics_territory_id,$inventory_unit=''){
		
		$sql = "UPDATE `logistics_inventory` SET logistics_parcel_category_id='$logistics_parcel_category_id',inventory_moq='$moq',inventory_weight_in_kg='$inventory_weight_in_kg',inventory_volume_in_kg='$inventory_volume_in_kg',shipping_charge_not_applicable='$shipping_charge_not_applicable',flat_shipping_charge='$flat_shipping_charge',inventory_length='$inventory_length',inventory_breadth='$inventory_breadth',inventory_height='$inventory_height',inventory_unit='$inventory_unit',free_shipping_territories='$logistics_territory_id' WHERE `inventory_id` = '$inventory_id'";

		$flag=$this->db->query($sql);
		return $flag;
	
	}
	public function delete_inventory_when_no_info($selected_list){
		
		$sql_purchase="select count(*) from inventory where id in ($selected_list) and purchased=1 and active=1";
		$query_purchase =$this->db->query($sql_purchase);
		$result_purchase = $query_purchase->row_array();
		$count_purchase = $result_purchase['count(*)'];
		
		$sql_rating="select count(*) from rated_inventory where id in ($selected_list) and active=1";
		$query_rating=$this->db->query($sql_rating);
		$result_rating= $query_rating->row_array();
		$count_rating = $result_rating['count(*)'];

		if($count_purchase==0 && $count_rating==0){
			$sql_select="select * from inventory where id in ($selected_list) and active=1";
			$query_select =$this->db->query($sql_select);		
			$result=$query_select->result();
			foreach ($result as $inventory) {
				
				if(file_exists($inventory->image)){
					unlink($inventory->image);
				}
				if(file_exists($inventory->thumbnail)){
					unlink($inventory->thumbnail);
				}
			}
			$sql_delete1="delete from inventory_specification where inventory_id in ($selected_list)";
			$sql_delete2="delete from products_filter where inventory_id in ($selected_list)";
			$sql_delete3="delete from logistics_inventory where inventory_id in ($selected_list)";
			$sql_delete4="delete from inventory where id in ($selected_list) and purchased=0";
			$sql_delete5="delete from rated_inventory where inventory_id in ($selected_list)";
			
			$res1=$this->db->query($sql_delete1);
			$res2=$this->db->query($sql_delete2);
			$res3=$this->db->query($sql_delete3);
			$res4=$this->db->query($sql_delete4);
			$res5=$this->db->query($sql_delete4);
			if($res1==true && $res2==true && $res3==true && $res4==true && $res5==true){
				return true;
			}else{
				return false;
			}
		}else{
			return 2;
		}

	}
	
	public function update_inventory_selected($selected_list,$active){
		
		$sql_update1="update inventory_specification set active='$active' where inventory_id in ($selected_list)";
		$sql_update2="update products_filter set active='$active' where inventory_id in ($selected_list)";
		$sql_update3="update logistics_inventory set active='$active' where inventory_id in ($selected_list)";	
		$sql_update4="update rated_inventory set active='$active' where inventory_id in ($selected_list)";
		$sql_update5="update inventory set active='$active' where id in ($selected_list)";		
		$res1=$this->db->query($sql_update1);
		$res2=$this->db->query($sql_update2);
		$res3=$this->db->query($sql_update3);
		$res4=$this->db->query($sql_update4);
		$res5=$this->db->query($sql_update5);
		//return array($res1,$res2,$res3,$res4,$res5);		
		if($res1==true && $res2==true && $res3==true && $res4==true && $res5=true){
			return true;
		}else{
			return false;
		}
		
	}
	public function logistics() {
		$query = $this->db->query("SELECT logistics.*,vendors.vendor_id,vendors.name FROM logistics,vendors where vendors.vendor_id=logistics.vendor_id and vendors.active=1");
		return $query -> result();
	}
	public function add_logistics($logistics_name,$logistics_weblink,$vendor_id,$tracking_id_generation,$logistics_priority,$default_logistics,$logistics_volumetric_value,$logistics_gst,$logistics_fuelsurcharge){
		$insertinfo = array("logistics_name" => $logistics_name,"logistics_weblink" => $logistics_weblink, "vendor_id" => $vendor_id,"tracking_id_generation"=>$tracking_id_generation,"logistics_priority"=>$logistics_priority,"default_logistics"=>$default_logistics,"logistics_volumetric_value"=>$logistics_volumetric_value,"logistics_gst"=>$logistics_gst,"logistics_fuelsurcharge"=>$logistics_fuelsurcharge);

		$flag1=$this -> db -> insert("logistics", $insertinfo);
		echo $flag1;
	}
	
	public function get_logistics_data($logistics_id){
		
		$query =$this->db->query("SELECT logistics.*,vendors.vendor_id,vendors.name FROM logistics,vendors Where logistics.logistics_id='$logistics_id' and logistics.vendor_id=vendors.vendor_id and vendors.active=1");
		return $query->row_array();		
	}
	public function edit_logistics($logistics_id,$logistics_name,$logistics_weblink,$vendor_id,$tracking_id_generation,$logistics_priority,$common_logistics,$logistics_priority_default,$default_logistics,$logistics_volumetric_value,$logistics_gst,$logistics_fuelsurcharge)
	{
		/////////////////////
		
		if($common_logistics!=""){ 
			$sql="SELECT * FROM `logistics` Where logistics_id='$common_logistics'";
			$query =$this->db->query($sql);
			$menuto_sort_order=$query->row_array()["logistics_priority"];
			$sql="SELECT * FROM `logistics` Where logistics_id='$logistics_id'";
			$query =$this->db->query($sql);
			$menufrom_sort_order=$query->row_array()["logistics_priority"];
			/*
			echo $common_logistics."-";//1
			echo $menuto_sort_order."-";//11
			echo $menufrom_sort_order."-";//13
			echo $logistics_id."-";//3
			*/
			$sql = "UPDATE `logistics` SET logistics_name='$logistics_name',logistics_weblink='$logistics_weblink',vendor_id='$vendor_id',tracking_id_generation='$tracking_id_generation',logistics_priority='$menuto_sort_order',logistics_volumetric_value='$logistics_volumetric_value',logistics_gst='$logistics_gst',logistics_fuelsurcharge='$logistics_fuelsurcharge' WHERE `logistics_id` = '$logistics_id'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `logistics` SET logistics_priority='$menufrom_sort_order' WHERE `logistics_id` = '$common_logistics'";
			$result=$this->db->query($sql);
			$sql = "UPDATE `logistics` SET default_logistics='$default_logistics',logistics_name='$logistics_name',vendor_id='$vendor_id' WHERE `logistics_id` = '$logistics_id'";
			$query = $this->db->query($sql);
			}else{
		$sql = "UPDATE `logistics` SET default_logistics='$default_logistics',logistics_name='$logistics_name',logistics_weblink='$logistics_weblink',vendor_id='$vendor_id',tracking_id_generation='$tracking_id_generation',logistics_priority='$logistics_priority',logistics_volumetric_value='$logistics_volumetric_value',logistics_gst='$logistics_gst',logistics_fuelsurcharge='$logistics_fuelsurcharge' WHERE `logistics_id` = '$logistics_id'";
		$query = $this->db->query($sql);
		}
				
		
	
		//////////////////////
		
		echo $query;
		
	}
	public function delete_logistics_selected($selected_list){
		
		$sql="select logistics_parcel_category_id  from logistics_parcel_category where logistics_id in ($selected_list)";
		$query = $this->db->query($sql);
		$logistics_parcel_category_id_result=$query->result_array();
		$logistics_parcel_category_id_arr=array();
		if(!empty($logistics_parcel_category_id_result)){
			foreach($logistics_parcel_category_id_result as $v){
				$logistics_parcel_category_id_arr[]=$v["logistics_parcel_category_id"];
			}
		}
		
		$sql_logistics_parcel_category_id="select logistics_parcel_category_id  from logistics_inventory";
		$query_logistics_parcel_category_id = $this->db->query($sql_logistics_parcel_category_id);
		$result_logistics_parcel_category_id_arr=$query_logistics_parcel_category_id->result_array();
		if(!empty($result_logistics_parcel_category_id_arr)){
			foreach($result_logistics_parcel_category_id_arr as $v){
				$logistics_parcel_category_id_list=$v["logistics_parcel_category_id"];
				$logistics_parcel_category_id_list_arr=explode(",",$logistics_parcel_category_id_list);
				foreach($logistics_parcel_category_id_list_arr as $logistics_inventory_parcel_category_id){
					if(in_array($logistics_inventory_parcel_category_id,$logistics_parcel_category_id_arr)){
						return "logistics_parcel_category_id_exists";
						break;
					}
				}
			}
		}
		exit;
		
		$sql_delete_logistics="delete from logistics where logistics_id in ($selected_list)";
		$flag_delete_logistics=$this->db->query($sql_delete_logistics);
		
		$sql_delete_logistics_all_territory_name="delete from logistics_all_territory_name where logistics_id in ($selected_list)";
		$flag_delete_logistics_all_territory_name=$this->db->query($sql_delete_logistics_all_territory_name);
		
		$sql_delete_logistics_territory_states="delete from logistics_territory_states where logistics_id in ($selected_list)";
		$flag_delete_logistics_territory_states=$this->db->query($sql_delete_logistics_territory_states);
		
		$sql_delete_logistics_territory_states_cities="delete from logistics_territory_states_cities where logistics_id in ($selected_list)";
		$flag_delete_logistics_territory_states_cities=$this->db->query($sql_delete_logistics_territory_states_cities);
		
		$sql_delete_logistics_service="delete from logistics_service where logistics_id in ($selected_list)";
		$flag_delete_logistics_service=$this->db->query($sql_delete_logistics_service);
		
		$sql_delete_logistics_weight="delete from logistics_weight where logistics_id in ($selected_list)";
		$flag_delete_logistics_weight=$this->db->query($sql_delete_logistics_weight);
		
		$sql_delete_logistics_delivery_mode="delete from logistics_delivery_mode where logistics_id in ($selected_list)";
		$flag_delete_logistics_delivery_mode=$this->db->query($sql_delete_logistics_delivery_mode);
		
		
		
		$sql_delete_logistics_parcel_category="delete from logistics_parcel_category where logistics_id in ($selected_list)";
		$flag_delete_logistics_parcel_category=$this->db->query($sql_delete_logistics_parcel_category);
		return $flag_delete_logistics_parcel_category;
		
		
		
	}
	public function logistics_service() {
		$query = $this->db->query("SELECT logistics_service.*,logistics.logistics_id,logistics.logistics_name FROM logistics_service,logistics where logistics.logistics_id=logistics_service.logistics_id");
		return $query -> result();
	}
	public function show_available_logistics($vendor_id){
		$sql="select logistics_id,logistics_name from logistics where trim(vendor_id) = '$vendor_id'";
		$query=$this->db->query($sql);
		return $query->result();
		
	}
	public function show_available_logistics_territory($logistics_service_id){
		$sql="select * from logistics_territory where trim(logistics_service_id) = '$logistics_service_id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_reasons($subcat_id){
		$sql="select * from reason_return where trim(subcat_id) = '$subcat_id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function add_logistics_service($city,$pin,$pincode,$logistics_id){
		$insertinfo = array("city" => $city,"pin" => $pin, "pincode" => $pincode,"logistics_id" => $logistics_id);
		$flag=$this -> db -> insert("logistics_service", $insertinfo);
		echo $flag;
	}
	public function get_available_logistics_pincodes($pin){

		//$query =$this->db->query("SELECT logistics_all_pincodes.pincodes ,logistics_service.pincode from logistics_all_pincodes,logistics_service where logistics_all_pincodes.pin='$pin' and logistics_all_pincodes.pin=logistics_service.pin");
		$query =$this->db->query("SELECT pincodes from logistics_all_pincodes where pin='$pin'");
		return $query->row_array();		
	}
	public function get_logistics_service_data($logistics_service_id){
		$query =$this->db->query("SELECT logistics_service.*,logistics.logistics_id,logistics.logistics_name,logistics.vendor_id,logistics_all_pincodes.pincodes FROM logistics_service,logistics,logistics_all_pincodes Where logistics_service.logistics_service_id='$logistics_service_id' and logistics.logistics_id=logistics_service.logistics_id and logistics_service.pin=logistics_all_pincodes.pin");
		return $query->row_array();		
	}
	public function edit_logistics_service($logistics_service_id,$city,$pin,$pincode,$logistics_id){
		$sql = "UPDATE `logistics_service` SET city='$city',pin='$pin',pincode='$pincode',logistics_id='$logistics_id' WHERE `logistics_service_id` = '$logistics_service_id'";
		$result=$this->db->query($sql);
		return $result;
	}
	public function delete_logistics_service_selected($selected_list){
		$sql_delete1="delete from logistics_service where logistics_service_id in ($selected_list)";
		$flag1=$this->db->query($sql_delete1);
		$sql_select_territory="select logistics_territory_id from logistics_territory where logistics_service_id in ($selected_list)";
		$query_select =$this->db->query($sql_select_territory);
		$temp_res=$query_select->result();

		if(count($temp_res)>0){
			$logistics_territory_arr=array();
			foreach($temp_res as $logistics_territory) {
				$logistics_territory_arr[]=$logistics_territory->logistics_territory_id;
			}
			$territory_in=implode(',',$logistics_territory_arr);
			$flag2=$this->delete_logistics_territory_selected($territory_in);
		}else{
			$flag2=true;
		}

		if($flag1==true && $flag2==true){
			return true;
		}else{
			return false;
		}
	}
	public function logistics_territory() {
		$query = $this->db->query("SELECT logistics_territory.*,logistics_service.logistics_service_id,logistics_service.city,vendors.vendor_id,vendors.name,logistics.logistics_name,logistics.logistics_id,logistics.vendor_id FROM logistics_service,logistics,vendors,logistics_territory where logistics.logistics_id=logistics_service.logistics_id and vendors.vendor_id=logistics.vendor_id and logistics_territory.logistics_service_id = logistics_service.logistics_service_id");
		return $query -> result();
	}
	public function logistics_territory_by_logistics_id($logistics_id) {
		$query = $this->db->query("select * from logistics_all_territory_name where logistics_id='$logistics_id'");
		return $query -> result();
	}
	public function add_logistics_territory($vendor_id,$logistics_service_id,$territory_name){
		$insertinfo = array("logistics_service_id" => $logistics_service_id, "territory_name" => $territory_name);
		$flag=$this -> db -> insert("logistics_territory", $insertinfo);
		echo $flag;
	}
	public function get_logistics_territory_data($logistics_territory_id){
		
		$query =$this->db->query("SELECT logistics_territory.*,logistics_all_territory_name.*,logistics_service.logistics_service_id,logistics_service.city,vendors.vendor_id,vendors.name,logistics.logistics_name,logistics.logistics_id,logistics.vendor_id FROM logistics_service,logistics,vendors,logistics_territory,logistics_all_territory_name where logistics.logistics_id=logistics_service.logistics_id and vendors.vendor_id=logistics.vendor_id and logistics_territory.logistics_service_id = logistics_service.logistics_service_id and logistics_territory.logistics_territory_id='$logistics_territory_id'");
		return $query->row_array();		
	}
	public function edit_logistics_territory($logistics_territory_id,$vendor_id,$logistics_service_id,$territory_name){
		$sql = "UPDATE `logistics_territory` SET logistics_service_id='$logistics_service_id',territory_name='$territory_name' WHERE `logistics_territory_id` = '$logistics_territory_id'";
		$result=$this->db->query($sql);
		return $result;
	}
	public function delete_logistics_territory_selected($selected_list){
		$sql_delete1="delete from logistics_territory where logistics_territory_id in ($selected_list)";
		$flag1=$this->db->query($sql_delete1);
		
		if($flag1==true){
			return true;
		}else{
			return false;
		}
	}
	public function logistics_delivery_mode_by_logistics_id($vendor_id,$logistics_id) {
		$sql="SELECT logistics_delivery_mode.*,logistics.logistics_name from logistics_delivery_mode,logistics where logistics_delivery_mode.logistics_id=logistics.logistics_id and logistics.vendor_id='$vendor_id' and logistics.logistics_id='$logistics_id' order by logistics_delivery_mode.logistics_delivery_mode_id ";
		$query = $this->db->query($sql);
		return $query -> result();
	}
	
	public function add_logistics_delivery_mode($vendor_id,$logistics_id,$delivery_mode,$speed_territory_duration,$default_logistics){
		$insertinfo = array("vendor_id"=>$vendor_id,"logistics_id"=>$logistics_id,"delivery_mode" => $delivery_mode,"speed_duration" => $speed_territory_duration, "default_delivery_mode" => $default_logistics);

		$flag=$this -> db -> insert("logistics_delivery_mode", $insertinfo);
		
		$logistics_delivery_mode_id=$this->db->insert_id();
		if($default_logistics==1){
		$flag_default_logistic=$this->update_default_logistic_delivery($logistics_delivery_mode_id,$default_logistics,$logistics_id);
		}else{
			$flag_default_logistic=true;
		}
		
		if($flag_default_logistic==true && $flag==true){
			return $flag;
		}else{
			return false;
		}
	}
	public function update_default_logistic_delivery($logistics_delivery_mode_id,$default_logistics,$logistics_id){
		
		if($default_logistics==1){
			$sql = "UPDATE `logistics_delivery_mode` SET default_delivery_mode=0 WHERE `logistics_delivery_mode_id` != '$logistics_delivery_mode_id' and logistics_id='$logistics_id' ";
			$result=$this->db->query($sql);
			return $result;
		}
		else{
			return true;
		}
	}
	public function get_logistics_delivery_mode_data($logistics_delivery_mode_id){
		$query =$this->db->query("SELECT logistics_delivery_mode.*,logistics.logistics_name from logistics_delivery_mode,logistics where logistics_delivery_mode.logistics_delivery_mode_id='$logistics_delivery_mode_id' and logistics.logistics_id=logistics_delivery_mode.logistics_id");
		return $query->row_array();		
	}
	public function edit_logistics_delivery_mode($logistics_delivery_mode_id,$delivery_mode,$speed_territory_duration,$default_logistics){
		$sql = "UPDATE `logistics_delivery_mode` SET delivery_mode='$delivery_mode', speed_duration='$speed_territory_duration',default_delivery_mode='$default_logistics' WHERE `logistics_delivery_mode_id` = '$logistics_delivery_mode_id'";
		$result=$this->db->query($sql);
		$get_logistics_delivery_mode_by_logistics_delivery_mode_id_arr=$this->get_logistics_delivery_mode_by_logistics_delivery_mode_id($logistics_delivery_mode_id);
		$logistics_id=$get_logistics_delivery_mode_by_logistics_delivery_mode_id_arr["logistics_id"];
		$flag_default_logistic=$this->update_default_logistic_delivery($logistics_delivery_mode_id,$default_logistics,$logistics_id);
		
		if($flag_default_logistic==true && $result==true){
			return $result;
		}else{
			return false;
		}
	}
	public function delete_logistics_delivery_mode_selected($selected_list){
		$sql_delete1="delete from logistics_delivery_mode where logistics_delivery_mode_id in ($selected_list)";
		$flag1=$this->db->query($sql_delete1);
		if($flag1==true){
			return true;
		}else{
			return false;
		}
	}

	

	public function get_logistics_delivery_mode_by_logistics_delivery_mode_id($logistics_delivery_mode_id){
		$query = $this->db->query("SELECT * from logistics_delivery_mode where logistics_delivery_mode_id='$logistics_delivery_mode_id'");
		return $query -> row_array();
	}
	public function get_logistics_parcel_category_by_logistics_parcel_category_id($logistics_parcel_category_id){
		$query = $this->db->query("SELECT * from logistics_parcel_category where logistics_parcel_category_id='$logistics_parcel_category_id'");
		return $query -> row_array();
	}
	public function logistics_weight() {
		$query = $this->db->query("SELECT logistics_weight.*,logistics.logistics_name,logistics.logistics_id from logistics_weight,logistics where logistics.logistics_id=logistics_weight.logistics_id");
		return $query -> result();
	}
	public function add_logistics_weight_ref($logistics_id,$weight_in_kg,$volume_in_kg,$logistics_price){
		$insertinfo = array("logistics_id"=>$logistics_id,"weight_in_kg" => $weight_in_kg, "volume_in_kg" => $volume_in_kg,"logistics_price" => $logistics_price);
		$flag=$this -> db -> insert("logistics_weight", $insertinfo);
		echo $flag;
	}
	public function check_break_point_is_there_or_not_logistics_weight($vendor_id,$logistics_id,$logistics_territory_id,$logistics_delivery_mode_id,$logistics_parcel_category_id){
		$sql="select * from logistics_weight where logistics_id='$logistics_id' and territory_name_id='$logistics_territory_id' and logistics_delivery_mode_id='$logistics_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg='yes'";
		$query =$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function check_break_point_is_there_or_not_logistics_weight_edit($vendor_id,$logistics_id,$logistics_territory_id,$logistics_delivery_mode_id,$logistics_parcel_category_id,$logistics_weight_id){
		$sql="select * from logistics_weight where logistics_id='$logistics_id' and territory_name_id='$logistics_territory_id' and logistics_delivery_mode_id='$logistics_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg='yes' and logistics_weight_id!='$logistics_weight_id'";
		$query =$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function add_logistics_weight($logistics_id,$territory_name_id,$logistics_delivery_mode_id,$logistics_parcel_category_id,$weight_in_kg,$lesser_v,$lesser_m,$v_range,$v_m_arr,$greater_v,$greater_m,$additional_weight_in_kg,$additional_weight_value_in_kg,$do_u_want_to_roundup,$do_u_want_to_include_breakupweight){
		
		$volume_in_kg=$lesser_v;
		
		$insertinfo1 = array("logistics_delivery_mode_id"=>$logistics_delivery_mode_id,"logistics_parcel_category_id"=>$logistics_parcel_category_id,"logistics_id"=>$logistics_id,"territory_name_id"=>$territory_name_id,"weight_in_kg" => $weight_in_kg, "volume_in_kg" => $volume_in_kg,"logistics_price" => $lesser_m,"additional_weight_in_kg"=>$additional_weight_in_kg,"additional_weight_value_in_kg"=>$additional_weight_value_in_kg,"do_u_want_to_roundup"=>$do_u_want_to_roundup,"do_u_want_to_include_breakupweight"=>$do_u_want_to_include_breakupweight);
		$flag1=$this -> db -> insert("logistics_weight", $insertinfo1);
		/*
		$e=0;
		if(!empty($v_range) && (count($v_range)==count($v_m_arr))){
			for($i=0;$i<count($v_range);$i++){
				$insert = array("logistics_delivery_mode_id"=>$logistics_delivery_mode_id,"logistics_parcel_category_id"=>$logistics_parcel_category_id,"logistics_id"=>$logistics_id,"territory_name_id"=>$territory_name_id,"weight_in_kg" => $weight_in_kg, "volume_in_kg" => $v_range[$i],"logistics_price" => $v_m_arr[$i]);
				$flag=$this -> db -> insert("logistics_weight", $insert);
				if($flag!=true){
					$e++;
				}
			}
		}
		
		$insertinfo2 = array("logistics_delivery_mode_id"=>$logistics_delivery_mode_id,"logistics_parcel_category_id"=>$logistics_parcel_category_id,"logistics_id"=>$logistics_id,"territory_name_id"=>$territory_name_id,"weight_in_kg" => $weight_in_kg, "volume_in_kg" => ">".$greater_v,"logistics_price" => $greater_m);
		$flag2=$this -> db -> insert("logistics_weight", $insertinfo2);
		
		if($flag1==true && $flag2==true && $e==0){
			return true;
		}
		*/
		if($flag1==true){
			return true;
		}
		else{
			return false;
		}
	}
	public function get_logistics_weight_data($logistics_weight_id){
		$query =$this->db->query("SELECT logistics_weight.*,logistics.logistics_name,logistics.logistics_id,logistics.vendor_id from logistics_weight,logistics where logistics.logistics_id=logistics_weight.logistics_id and logistics_weight.logistics_weight_id='$logistics_weight_id' GROUP BY logistics_weight.weight_in_kg");
		return $query->row_array();		
	}
	public function get_logistics_weight_single($logistics_territory_id,$logistics_id,$logistics_delivery_mode_id,$logistics_parcel_category_id,$weight_in_kg){
		$query =$this->db->query("SELECT logistics_weight.*,logistics.logistics_name,logistics.logistics_id,logistics.vendor_id from logistics_weight,logistics where logistics.logistics_id=logistics_weight.logistics_id and logistics_weight.logistics_id='$logistics_id' and logistics_weight.weight_in_kg='$weight_in_kg' and logistics_weight.logistics_delivery_mode_id='$logistics_delivery_mode_id' and logistics_weight.logistics_parcel_category_id='$logistics_parcel_category_id' and logistics_weight.territory_name_id='$logistics_territory_id' GROUP BY logistics_weight.weight_in_kg");
		return $query->row_array();		
	}
	public function edit_logistics_weight_ref($logistics_weight_id,$logistics_id,$weight_in_kg,$volume_in_kg,$logistics_price){
		$sql = "UPDATE `logistics_weight` SET logistics_id='$logistics_id',weight_in_kg='$weight_in_kg',volume_in_kg='$volume_in_kg',logistics_price='$logistics_price' WHERE `logistics_weight_id` = '$logistics_weight_id'";
		$result=$this->db->query($sql);
		return $result;
	}
	public function get_logistics_weight_by_logistics_weight_id($logistics_weight_id){
		$sql = "select * from logistics_weight where logistics_weight_id='$logistics_weight_id'";
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	public function edit_logistics_weight($logistics_id,$territory_name_id,$weight_in_kg,$lesser_v,$lesser_m,$lesser_id,$v_range,$v_m_arr,$v_ids_arr,$greater_v,$greater_m,$greater_id,$diff,$additional_weight_in_kg,$additional_weight_value_in_kg,$do_u_want_to_roundup,$do_u_want_to_include_breakupweight){
		$get_logistics_weight_by_logistics_weight_id_arr=$this->get_logistics_weight_by_logistics_weight_id($lesser_id);
		$logistics_delivery_mode_id=$get_logistics_weight_by_logistics_weight_id_arr["logistics_delivery_mode_id"];
		$logistics_parcel_category_id=$get_logistics_weight_by_logistics_weight_id_arr["logistics_parcel_category_id"];
		
		
		$sql_l = "UPDATE `logistics_weight` SET logistics_id='$logistics_id',territory_name_id='$territory_name_id',weight_in_kg='$weight_in_kg',volume_in_kg='$lesser_v',logistics_price='$lesser_m',additional_weight_in_kg='$additional_weight_in_kg',additional_weight_value_in_kg='$additional_weight_value_in_kg',do_u_want_to_roundup='$do_u_want_to_roundup',do_u_want_to_include_breakupweight='$do_u_want_to_include_breakupweight' WHERE `logistics_weight_id` = '$lesser_id'";
		$result_l=$this->db->query($sql_l);
/*
		$greater_v=">".$greater_v;
		
		$sql_g = "UPDATE `logistics_weight` SET logistics_id='$logistics_id',territory_name_id='$territory_name_id',weight_in_kg='$weight_in_kg',volume_in_kg='$greater_v',logistics_price='$greater_m' WHERE `logistics_weight_id` = '$greater_id'";
		$result_g=$this->db->query($sql_g);
		
		$er=0;
		
		if(!empty($v_range)){
			
			for($i=0;$i<count($v_range);$i++){	
				
				if($v_ids_arr[$i]!=''){
					
					$sql = "UPDATE `logistics_weight` SET logistics_id='$logistics_id',territory_name_id='$territory_name_id',weight_in_kg='$weight_in_kg',volume_in_kg='$v_range[$i]',logistics_price='$v_m_arr[$i]' WHERE `logistics_weight_id` = '$v_ids_arr[$i]'";
				}else{
					$sql="insert into logistics_weight set logistics_delivery_mode_id='$logistics_delivery_mode_id',logistics_parcel_category_id='$logistics_parcel_category_id',logistics_id='$logistics_id'territory_name_id='$territory_name_id',,weight_in_kg='$weight_in_kg',volume_in_kg='$v_range[$i]',logistics_price='$v_m_arr[$i]'";
				}
				$result=$this->db->query($sql);
				if($result!=true){
					$er++;
				}
			}
			
		}
		if(!empty($diff)){
			foreach($diff as $id){
				$sql_d="delete from logistics_weight where logistics_id='$logistics_id' and logistics_weight_id='$id'";
				$result_d=$this->db->query($sql_d);
				if($result_d!=true){
					$er++;
				}
			}
		}
		
		if($result_l==true && $result_g==true && $er==0){
			return true;
		}else{
			return false;
		}
		*/
		if($result_l==true){
			return true;
		}else{
			return false;
		}
	}
	public function delete_logistics_weight_selected($selected_list){
		if($selected_list!=''){
			$arr=explode(',',$selected_list);
			$sql="select logistics_id,weight_in_kg from logistics_weight where logistics_weight_id in ($selected_list)";
			$res=$this->db->query($sql);
			$result=$res->result();
			if(!empty($result)){
				foreach($result as $data){
					$weight_in_kg=$data->weight_in_kg;
					$logistics_id=$data->logistics_id;
					$sql_delete="delete from logistics_weight where weight_in_kg='$weight_in_kg' and logistics_id='$logistics_id'";
					$flag=$this->db->query($sql_delete);
				}
			}else{
				$flag=true;
			}
			return $flag;
		}
		
	}
	public function logistics_parcel_category_by_logistics_id($vendor_id,$logistics_id){
		$query = $this->db->query("SELECT logistics_parcel_category.*,logistics.logistics_name from logistics_parcel_category,logistics where logistics_parcel_category.logistics_id=logistics.logistics_id and logistics_parcel_category.vendor_id='$vendor_id'  and logistics_parcel_category.logistics_id='$logistics_id' order by logistics_parcel_category.logistics_parcel_category_id desc");
		return $query -> result();
	}
	public function logistics_parcel_category(){
		$query = $this->db->query("SELECT logistics_parcel_category.*,logistics.logistics_name from logistics_parcel_category,logistics where logistics_parcel_category.logistics_id=logistics.logistics_id order by logistics_parcel_category.logistics_parcel_category_id desc");
		return $query -> result();
	}
	public function add_logistics_parcel_category($vendor_id,$logistics_id,$parcel_type,$parcel_type_description,$default_logistics){
		$parcel_type_description=$this->db->escape_str($parcel_type_description);
		$insertinfo = array("vendor_id"=>$vendor_id,"logistics_id"=>$logistics_id,"parcel_type" => $parcel_type, "parcel_type_description" => $parcel_type_description, "default_parcel_category" => $default_logistics);
		$flag=$this -> db -> insert("logistics_parcel_category", $insertinfo);
		$logistics_parcel_category_id=$this->db->insert_id();
		if($default_logistics==1){
		$flag_default_logistic=$this->update_default_logistic_parcel_category($logistics_parcel_category_id,$default_logistics,$logistics_id);
		}else{
			$flag_default_logistic=true;
		}
		
		if($flag_default_logistic==true && $flag==true){
			return $flag;
		}else{
			return false;
		}
		
	}
	public function update_default_logistic_parcel_category($logistics_parcel_category_id,$default_logistics,$logistics_id){
		
		if($default_logistics==1){
			$sql = "UPDATE `logistics_parcel_category` SET default_parcel_category=0 WHERE `logistics_parcel_category_id` != '$logistics_parcel_category_id' and logistics_id='$logistics_id' ";
			$result=$this->db->query($sql);
			return $result;
		}
		else{
			return true;
		}
	}
	public function get_logistics_parcel_category_data($logistics_parcel_category_id){
		$query =$this->db->query("Select logistics_parcel_category.*,logistics.logistics_name from logistics_parcel_category,logistics where logistics_parcel_category.logistics_parcel_category_id='$logistics_parcel_category_id' and logistics_parcel_category.logistics_id=logistics.logistics_id order by logistics_parcel_category.logistics_parcel_category_id desc");
		return $query->row_array();		
	}
	public function edit_logistics_parcel_category($vendor_id,$logistics_id,$logistics_parcel_category_id,$parcel_type,$parcel_type_description,$default_logistics){
		$parcel_type_description=$this->db->escape_str($parcel_type_description);
		$sql = "UPDATE `logistics_parcel_category` SET parcel_type='$parcel_type',parcel_type_description='$parcel_type_description',default_parcel_category='$default_logistics' WHERE `logistics_parcel_category_id` = '$logistics_parcel_category_id'";
		$result=$this->db->query($sql);
		
		$get_logistics_parcel_category_by_logistics_parcel_category_id_arr=$this->get_logistics_parcel_category_by_logistics_parcel_category_id($logistics_parcel_category_id);
		$logistics_id=$get_logistics_parcel_category_by_logistics_parcel_category_id_arr["logistics_id"];
		
		
		
		$flag_default_logistic=$this->update_default_logistic_parcel_category($logistics_parcel_category_id,$default_logistics,$logistics_id);
		if($flag_default_logistic==true && $result==true){
			return $result;
		}else{
			return false;
		}
	}
	public function delete_logistics_parcel_category_selected($selected_list){
		$sql_delete="delete from logistics_parcel_category where logistics_parcel_category_id in ($selected_list)";
		$flag=$this->db->query($sql_delete);
		return $flag;
	}
	public function logistics_all_pincodes() {
		$query = $this->db->query("SELECT * from logistics_all_pincodes");
		return $query -> result();
	}
	public function add_logistics_all_pincodes($state_id,$city,$pin,$pincodes,$pincodes_range,$distinct_pincodes){
if(isset($distinct_pincodes)){}else{$distinct_pincodes='';}
		$insertinfo = array("state_id"=>$state_id,"city" => $city, "pin" => $pin, "pincodes" => $pincodes, "pincodes_range" => $pincodes_range, "distinct_pincodes" => $distinct_pincodes);
		$flag=$this -> db -> insert("logistics_all_pincodes", $insertinfo);
		return $flag;
	}
	
	public function get_logistics_all_pincodes_data($pincode_id){
		$query =$this->db->query("Select * from logistics_all_pincodes where pincode_id='$pincode_id'");
		return $query->row_array();		
	}
	
	public function edit_logistics_all_pincodes($state_id,$pincode_id,$city,$pin,$pincodes,$pincodes_range,$distinct_pincodes){
if(isset($distinct_pincodes)){}else{$distinct_pincodes='';}
		$sql = "UPDATE `logistics_all_pincodes` SET state_id='$state_id',city='$city',pin='$pin',pincodes='$pincodes',pincodes_range='$pincodes_range',distinct_pincodes='$distinct_pincodes' WHERE `pincode_id` = '$pincode_id'";
		$result=$this->db->query($sql);
		return $result;
	}
	public function delete_logistics_all_pincodes_selected($selected_list){
		$sql_delete="delete from logistics_all_pincodes where pincode_id in ($selected_list)";
		$flag=$this->db->query($sql_delete);
		return $flag;
	}
	public function logistics_all_territory_name_by_logistics_id($vendor_id,$logistics_id) {
		$sql="SELECT logistics_all_territory_name.*,logistics.logistics_name from logistics_all_territory_name,logistics where logistics_all_territory_name.logistics_id=logistics.logistics_id and logistics.vendor_id='$vendor_id' and logistics.logistics_id='$logistics_id' order by logistics_all_territory_name.territory_name_id ";
		$query = $this->db->query($sql);
		return $query -> result();
		
	}
	public function logistics_all_territory_name() {
		$query = $this->db->query("SELECT * from logistics_all_territory_name");
		return $query -> result();
	}
	public function check_make_as_local_territory($logistics_id,$territory_name_id){
		$query = $this->db->query("SELECT * from logistics_all_territory_name where logistics_id='$logistics_id' and territory_name_id='$territory_name_id' and make_as_local='1'");
		return $query -> row();
	}
	public function check_make_as_local_logistics_all_territory_name($logistics_id){
		$query = $this->db->query("SELECT * from logistics_all_territory_name where logistics_id='$logistics_id' and make_as_local='1'");
		return $query -> row();
	}
	public function add_logistics_all_territory_name($logistics_id,$territory_name,$make_as_local){
		if($make_as_local=='1'){
			$sql="update logistics_all_territory_name set make_as_local='0' where logistics_id='$logistics_id'";
			$this->db->query($sql);
		}
		$insertinfo = array("logistics_id"=>$logistics_id,"territory_name" => $territory_name,"make_as_local"=>$make_as_local);
		$flag=$this -> db -> insert("logistics_all_territory_name", $insertinfo);
		echo $flag;
	}
	public function get_logistics_all_territory_name_data($territory_name_id){
		$query =$this->db->query("SELECT * from logistics_all_territory_name where territory_name_id='$territory_name_id'");
		return $query->row_array();		
	}
	public function edit_logistics_all_territory_name($territory_name_id,$territory_name,$make_as_local,$make_as_local_skip){
		if($make_as_local=='1'){
			$get_logistics_all_territory_name_data_arr=$this->get_logistics_all_territory_name_data($territory_name_id);
			if(!empty($get_logistics_all_territory_name_data_arr)){
				$logistics_id=$get_logistics_all_territory_name_data_arr["logistics_id"];
				$sql="update logistics_all_territory_name set make_as_local='0' where logistics_id='$logistics_id'";
				$this->db->query($sql);
			}
		}
		if($make_as_local_skip=="no"){
			$sql = "UPDATE logistics_all_territory_name SET territory_name='$territory_name',make_as_local='$make_as_local' WHERE territory_name_id ='$territory_name_id' ";
		}
		if($make_as_local_skip=="yes"){
			$sql = "UPDATE logistics_all_territory_name SET territory_name='$territory_name' WHERE territory_name_id ='$territory_name_id' ";
		}
		$result=$this->db->query($sql);
		return $result;
	}
	public function delete_logistics_all_territory_name_selected($selected_list){
		$sql_delete="delete from logistics_all_territory_name where territory_name_id in ($selected_list)";
		$flag=$this->db->query($sql_delete);
		return $flag;
	}
	public function create_search_index($inventory_id){
		$sql="select inventory.id as inventory_id,inventory.sku_id,inventory.sku_name,products.*,inventory.image,inventory.thumbnail,inventory.selling_price,inventory.base_price,inventory.tax_percent,inventory.max_selling_price,inventory.selling_discount,category.cat_name,subcategory.subcat_name,brands.brand_name,inventory.common_image,inventory.product_status,inventory.stock,inventory.inventory_type from products,inventory,category,subcategory,brands where products.product_id=inventory.product_id and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and inventory.id='$inventory_id' and inventory.vendor_removed=0";
		$query=$this->db->query($sql);
		return $query->row();
	}
	public function get_filter_id_new($inventory_id){
		$sql="select * from  products_filter where inventory_id='$inventory_id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function get_filter_id_new_except_filter_id($inventory_id,$filter_id){
		$sql="select * from  products_filter where inventory_id='$inventory_id' and filter_id!= '$filter_id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function get_filterbox_name($filter_id){
		$sql="select filterbox_name from  filterbox where filterbox_id = (select filterbox_id from filter where filter_id='$filter_id')";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
		return $query->row()->filterbox_name;
		}else{
			return "";
		}
	}
	
	public function get_filterbox_type($filter_id){
		$sql="select type from  filterbox where view_filterbox=1 and active=1 and filterbox_id = (select filterbox_id from filter where filter_id='$filter_id')";
		$query=$this->db->query($sql);
		
		if(!empty($query->row())){
			return $query->row()->type;
		}
		else{
			return "";
		}
		
	}
	public function get_filter_options($filter_id){
		$sql="select filter_options from filter where filter_id='$filter_id'";
		$query=$this->db->query($sql);
		
		if(!empty($query->row())){
			return $query->row()->filter_options;
		}
		else{
			return "";
		}
		
	}
	
	
	public function get_all_inventoy_by_pcat_id_selected_list($selected_list){
		$sql="select inventory.id as inventory_id,inventory.sku_id,products.*,inventory.image,inventory.thumbnail,inventory.selling_price,parent_category.pcat_name,category.cat_name,subcategory.subcat_name,brands.brand_name,inventory.common_image from products,inventory,parent_category,category,subcategory,brands where products.product_id=inventory.product_id and products.pcat_id=parent_category.pcat_id and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and products.pcat_id in ($selected_list) and inventory.vendor_removed=0";
		$query=$this->db->query($sql);
		return $query->result();
	}	
	public function get_all_inventoy_by_cat_id_selected($selected_list){
		$sql="select inventory.id as inventory_id,inventory.sku_id,products.*,inventory.image,inventory.thumbnail,inventory.selling_price,category.cat_name,subcategory.subcat_name,brands.brand_name,inventory.common_image from products,inventory,category,subcategory,brands where products.product_id=inventory.product_id and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and products.cat_id in ($selected_list) and inventory.vendor_removed=0";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function get_all_inventoy_by_subcat_id_selected($selected_list){
		$sql="select inventory.id as inventory_id,inventory.sku_id,products.*,inventory.image,inventory.thumbnail,inventory.selling_price,category.cat_name,subcategory.subcat_name,brands.brand_name,inventory.common_image from products,inventory,category,subcategory,brands where products.product_id=inventory.product_id and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and products.subcat_id in ($selected_list) and inventory.vendor_removed=0";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function get_all_inventoy_by_brand_id_selected($selected_list){
		$sql="select inventory.id as inventory_id,inventory.sku_id,products.*,inventory.image,inventory.thumbnail,inventory.selling_price,category.cat_name,subcategory.subcat_name,brands.brand_name,inventory.common_image from products,inventory,category,subcategory,brands where products.product_id=inventory.product_id and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and products.brand_id in ($selected_list) and inventory.vendor_removed=0";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function get_all_inventoy_by_product_id_selected($selected_list){
		$sql="select inventory.id as inventory_id,inventory.sku_id,products.*,inventory.image,inventory.thumbnail,inventory.selling_price,category.cat_name,subcategory.subcat_name,brands.brand_name,inventory.common_image from products,inventory,category,subcategory,brands where products.product_id=inventory.product_id and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and products.product_id in ($selected_list) and inventory.vendor_removed=0";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function get_all_inventoy_by_filterbox_id($filterbox_id){
		$sql="select filterbox.filterbox_name,filterbox.filterbox_id,filterbox_to_category.filterbox_id,filterbox_to_category.subcat_id,filterbox_to_category.cat_id,filterbox_to_category.pcat_id,inventory.id as inventory_id,inventory.sku_id,products.*,inventory.image,inventory.thumbnail,inventory.selling_price,category.cat_name,subcategory.subcat_name,brands.brand_name,inventory.common_image from products,inventory,filterbox_to_category,filterbox,category,subcategory,brands where filterbox_to_category.pcat_id=products.pcat_id and filterbox_to_category.cat_id=products.cat_id and filterbox_to_category.subcat_id=products.subcat_id and inventory.product_id=products.product_id and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and filterbox.filterbox_id=filterbox_to_category.filterbox_id and filterbox.filterbox_id='$filterbox_id' and inventory.vendor_removed=0";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function get_inventory_by_filter_id_selected($selected_list){
		$sql="select inventory.vendor_removed,products_filter.inventory_id from products_filter,inventory where products_filter.filter_id in ($selected_list) and inventory.vendor_removed=0";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function get_inventory_by_filterbox_id_selected($selected_list){
		$sql="select filter_id from filter where filterbox_id in ($selected_list)";
		$query=$this->db->query($sql);
		$val=$query->result();
		if(count($val)>0){
			$arr=array();
			foreach($val as $filter_id_val){
				$arr[]=$filter_id_val->filter_id;
			}		
			$arr_in=implode(',',$arr);
			$sql2="select inventory.vendor_removed,products_filter.inventory_id from products_filter,inventory where products_filter.filter_id in ($arr_in) and inventory.vendor_removed=0";
			$query2=$this->db->query($sql2);
			return $query2->result();		
		}else{
			return $query->result();
		}
	}
	public function category_processing($params,$fg){
		
		$columns = array( 
			0 =>'category.cat_id', 
			1 => 'category.cat_name',
			2 => 'category.pcat_name',
			3 => 'category.menu_sort_order',
			4 => 'category.timestamp',

		);
		$pcat_id =$params['pcat_id'];
		$active =$params['active'];
		$where = $sqlTot = $sqlRec = "";
		if($pcat_id==0){
			$sql = "select category.* from category where category.pcat_id=0 and category.active='$active' ";
		}else{
			$sql = "select category.*, parent_category.pcat_id,parent_category.pcat_name from category, parent_category where category.pcat_id=parent_category.pcat_id and category.active='$active' ";
		}
		if($pcat_id==""){
			$sql.=" and 1=0 ";
		}
		else{
			$sql.=" and 1=1 ";
		}
		
		if(!empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( category.cat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}
		if($pcat_id!=0){
			$where .=" AND ";
			$where .="parent_category.pcat_id='$pcat_id'";	
		}
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		//$sqlRec .=  "order by category.timestamp desc";
		//echo $sqlRec;
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}		
	}
	public function subcategory_processing($params,$fg){
		$columns = array( 
			0 =>'subcategory.subcat_id', 
			1 => 'subcategory.subcat_name',
			2 => 'subcategory.cat_name',
			3 => 'subcategory.pcat_name',
			4 => 'subcategory.menu_sort_order',
			5 => 'subcategory.timestamp',

		);
		$pcat_id =$params['pcat_id'];
		$cat_id =$params['cat_id'];
		$active =$params['active'];
		$where = $sqlTot = $sqlRec = "";
		if($pcat_id==0){
			$sql = "select subcategory.*,category.cat_name,category.cat_id from subcategory,category  where subcategory.pcat_id=0 and category.cat_id=subcategory.cat_id and category.pcat_id=subcategory.pcat_id and subcategory.active='$active' ";
		}else{
			$sql = "select subcategory.*,category.cat_name,category.cat_id, parent_category.pcat_id,parent_category.pcat_name from subcategory,parent_category,category  where subcategory.pcat_id=parent_category.pcat_id and category.cat_id=subcategory.cat_id and subcategory.active='$active' ";
		}
		
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( subcategory.subcat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR category.cat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}
		
		if($pcat_id!=0){
			$where .=" AND ";
			$where .="parent_category.pcat_id='$pcat_id'";
		}		
		$where .=" AND ";
		$where .="category.cat_id='$cat_id'";
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by subcategory.timestamp desc";
		//echo $sqlRec;
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}		
	}
	public function brands_processing($params,$fg){
		$columns = array( 
			0 =>'brands.brand_id', 
			1 => 'brands.brand_name',
			2 => 'brands.subcat_name',
			3 => 'brands.cat_name',
			4 => 'brands.pcat_name',
			6 => 'brands.timestamp',

		);
		$pcat_id =$params['pcat_id'];
		$cat_id =$params['cat_id'];
		$subcat_id =$params['subcat_id'];
		$active =$params['active'];
		$where = $sqlTot = $sqlRec = "";
		$user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");

		if($pcat_id==0){
			$sql="select brands.*,subcategory.subcat_name,subcategory.subcat_id,category.cat_name,category.cat_id  from brands";
			$sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `brands`.`cat_id`';
			$sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `brands`.`subcat_id`';
			
			$sql.=" where brands.pcat_id=0 and brands.active='$active' ";

		}else{
			$sql="select brands.*,subcategory.subcat_name,subcategory.subcat_id,category.cat_name,category.cat_id,parent_category.pcat_id,parent_category.pcat_name from brands";
			$sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `brands`.`pcat_id`';
			$sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `brands`.`cat_id`';
			$sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `brands`.`subcat_id`';
			$sql.=" where  brands.active='$active' ";


		}
		
		if(!empty($params['search']['value'])){   
			$where .=" and ";
			$where .=" ( brands.brand_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR subcategory.subcat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR category.cat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR parent_category.pcat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}	
		if($pcat_id!=0){
			$where .=" AND ";
			$where .="parent_category.pcat_id='$pcat_id'";
		}
		
		$where .=" AND ";
		$where .="category.cat_id='$cat_id'";
		$where .=" AND ";
		$where .="subcategory.subcat_id='$subcat_id'";
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by brands.timestamp desc";
		//echo $sqlRec;		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}			
	}
	public function attribute_processing($params,$fg){
		
		$pcat_id =$params['pcat_id'];
		$cat_id =$params['cat_id'];
		$subcat_id =$params['subcat_id'];
		$active =$params['active'];	
		$where = $sqlTot = $sqlRec = "";	
		if($pcat_id==0){
			if($subcat_id==0){
			$sql="select attribute.*,category.cat_id,category.cat_name from attribute,category where attribute.cat_id=category.cat_id and attribute.subcat_id=0 and attribute.pcat_id=0 and attribute.active='$active' ";	
			}else{
			$sql="select attribute.*,category.cat_id,category.cat_name,subcategory.subcat_id,subcategory.subcat_name from attribute,category,subcategory where attribute.cat_id=category.cat_id and attribute.subcat_id=subcategory.subcat_id and attribute.pcat_id=0 and attribute.active='$active' ";
			}
		}else{
			if($subcat_id==0){
			$sql="select attribute.*,category.cat_id,category.cat_name from attribute,parent_category,category where attribute.cat_id=category.cat_id and attribute.subcat_id=0 and attribute.pcat_id=parent_category.pcat_id and attribute.active='$active' ";	
			}else{
			$sql="select attribute.*,category.cat_id,category.cat_name,subcategory.subcat_id,subcategory.subcat_name from attribute,parent_category,category,subcategory where attribute.cat_id=category.cat_id and attribute.subcat_id=subcategory.subcat_id and attribute.pcat_id=parent_category.pcat_id and attribute.active='$active' ";
			}
		}
		
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( attribute.attribute1_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR attribute.attribute1_option LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR attribute.attribute2_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR attribute.attribute2_option LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR subcategory.subcat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR category.cat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
			
		}
		if($pcat_id!=0){
			$where .=" AND ";
			$where .="attribute.pcat_id='$pcat_id'";
		}
		
		$where .=" AND ";
		$where .="attribute.cat_id='$cat_id'";
		if($subcat_id==0){
		$where .=" AND ";
		$where .="attribute.subcat_id=0";
		}else{
		$where .=" AND ";
		$where .="attribute.subcat_id='$subcat_id'";	
		}
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " order by attribute.timestamp desc";

		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}	
	}
	public function filterbox_processing($params,$fg){
		$columns = array( 
			0 =>'filterbox.filterbox_id', 
			1 => 'filterbox.filterbox_name',
			2 => 'filterbox.type',
			3 => 'filterbox.sort_order',
			6 => 'filterbox.timestamp',

		);
		$pcat_id =$params['pcat_id'];
		$cat_id =$params['cat_id'];
		$subcat_id =$params['subcat_id'];
		$subcat_id=($subcat_id=='')?0:$subcat_id;
		$active =$params['active'];
		$where = $sqlTot = $sqlRec = "";
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( filterbox.filterbox_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}	
		$sql = "select filterbox.*, filterbox_to_category.* from filterbox,filterbox_to_category where filterbox.filterbox_id=filterbox_to_category.filterbox_id and filterbox.active='$active' ";
		if($pcat_id=="" && $cat_id=="" && $subcat_id==""){
			$sql.=" and 1=0 ";
		}
		else{
			$sql.=" and 1=1 ";
		}
		
		if($pcat_id!="" && $cat_id=="" && $subcat_id==""){
			$where .="and (filterbox_to_category.pcat_id='$pcat_id' AND filterbox_to_category.pcat_view='1')";
		}
		if($pcat_id!="" && $cat_id!="" && $subcat_id==""){
			$where .=" and (filterbox_to_category.cat_id='$cat_id' AND filterbox_to_category.cat_view='1')";
			
		}
		if($pcat_id!="" && $cat_id!="" && $subcat_id!=""){
			$where .=" and (filterbox_to_category.subcat_id='$subcat_id' AND  filterbox_to_category.subcat_view='1')";
		}
		
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by filterbox.timestamp desc";

		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}		
	}
	public function get_catalog_structure($pcat_id,$cat_id,$subcat_id){
		if($subcat_id !=0){
			if($pcat_id==0){
				$sql1="select subcategory.subcat_name,category.cat_name from subcategory,category where subcategory.subcat_id='$subcat_id' and subcategory.pcat_id=0 and subcategory.cat_id=category.cat_id";
			}else{
				$sql1="select subcategory.subcat_name,category.cat_name,parent_category.pcat_name from subcategory,category,parent_category where subcategory.subcat_id='$subcat_id' and subcategory.pcat_id=parent_category.pcat_id and subcategory.cat_id=category.cat_id";
			}	
				
		}else if($cat_id !=0){		
			if($pcat_id==0){
				$sql1="select category.cat_name from category where category.cat_id='$cat_id' and  category.pcat_id=0";
			}else{
				$sql1="select category.cat_name,parent_category.pcat_name from category,parent_category where category.cat_id='$cat_id' and  category.pcat_id=parent_category.pcat_id";
			}	
		}else{
			if($pcat_id!=0){
				$sql1="select pcat_id,pcat_name from parent_category where pcat_id='$pcat_id'";		
			}else{
				$sql1='';
			}
		}
			
		if($sql1!=''){
			$query1 =$this->db->query($sql1);
			$catalog_arr=$query1->row_array();
			return implode(',',$catalog_arr);
		}else{
			$catalog_arr=array();
			$str='';
			return $str;
		}
	}
	public function filter_processing($params,$fg){
		$columns = array( 
			0 =>'filter.filter_id', 
			1 => 'filter.filter_options',
			2 => 'filter.filterbox_name',
			4 => 'filter.sort_order',
			6 => 'filter.timestamp',

		);
		$pcat_id =$params['pcat_id'];
		$cat_id =$params['cat_id'];
		$subcat_id =$params['subcat_id'];

		$filterbox_id =$params['filterbox_id'];
		$active =$params['active'];
		$where = $sqlTot = $sqlRec = "";
		$sql = "select filter.*,filterbox.*, filterbox_to_category.*,filter.sort_order as filter_sort_order,filterbox.sort_order as filterbox_sort_order from filter,filterbox,filterbox_to_category where filterbox.filterbox_id=filterbox_to_category.filterbox_id and filter.filterbox_id=filterbox.filterbox_id and filter.active='$active' and ((filterbox_to_category.cat_id='$cat_id' and filterbox_to_category.subcat_id='$subcat_id') or (filterbox_to_category.cat_id='0' and filterbox_to_category.subcat_id='0') or (filterbox_to_category.cat_id='0' and filterbox_to_category.subcat_id='$subcat_id') or (filterbox_to_category.cat_id='$cat_id' and filterbox_to_category.subcat_id='0'))";
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( filter.filter_options LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .="  OR filterbox.filterbox_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}
		if($pcat_id!=0){
			$where .=" AND ";
			$where .="filterbox_to_category.pcat_id='$pcat_id'";
		}
		/*
		$where .=" AND ";
		$where .="filterbox_to_category.cat_id='$cat_id'";
		$where .=" AND ";
		$where .="filterbox_to_category.subcat_id='$subcat_id'";*/
		$where .=" AND ";
		$where .="filter.filterbox_id='$filterbox_id'";

		$sqlTot .= $sql;
		$sqlRec .= $sql;

		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by filterbox.timestamp desc";

		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}		
	}
	public function specification_group_processing($params,$fg){
		$columns = array( 
			0 =>'specification_group.specification_group_id', 
			1 => 'specification_group.specification_group_name',
			2 => 'specification_group.sort_order',
			5 => 'specification_group.timestamp',

		);
		$pcat_id =$params['pcat_id'];
		$cat_id =$params['cat_id'];
		$subcat_id =$params['subcat_id'];
		$subcat_id=($subcat_id=='')?0:$subcat_id;
		$active =$params['active'];
		$where = $sqlTot = $sqlRec = "";
		
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( specification_group.specification_group_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}
		$sql = "select specification_group.*, specification_group_to_categories.* from specification_group,specification_group_to_categories where specification_group. 	specification_group_id=specification_group_to_categories.specification_group_id and specification_group.active='$active'";
		
			
		if($pcat_id=="" && $cat_id=="" && $subcat_id==""){
			$sql.=" and 1=0 ";
		}
		else{
			$sql.=" and 1=1 ";
		}
		if($pcat_id!="" && $cat_id=="" && $subcat_id==""){
			$where .="and (specification_group_to_categories.pcat_id='$pcat_id' AND specification_group_to_categories.pcat_view='1')";
		}
		if($pcat_id!="" && $cat_id!="" && $subcat_id==""){
			$where .=" and (specification_group_to_categories.cat_id='$cat_id' AND specification_group_to_categories.cat_view='1')";
			
		}
		if($pcat_id!="" && $cat_id!="" && $subcat_id!=""){
			$where .=" and (specification_group_to_categories.subcat_id='$subcat_id' AND specification_group_to_categories.subcat_view='1')";
		}
		
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by specification_group.timestamp desc";
		//echo $sqlRec;
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}		
	}
	public function specification_processing($params,$fg){
		$columns = array( 
			0 =>'specification.specification_id', 
			1 => 'specification.specification_name',
			5 => 'specification.sort_order',
			7 => 'specification.timestamp',

		);
		$pcat_id =$params['pcat_id'];
		$cat_id =$params['cat_id'];
		$subcat_id =$params['subcat_id'];
		$specification_group_id =$params['specification_group_id'];
		$active =$params['active'];
		$where = $sqlTot = $sqlRec = "";
		$sql = "select specification.*,specification_group.*, specification_group_to_categories.*,specification.sort_order as specification_sort_order,specification_group.sort_order as specification_group_sort_order from specification,specification_group,specification_group_to_categories where specification_group.specification_group_id=specification_group_to_categories.specification_group_id and specification.specification_group_id=specification_group.specification_group_id and specification.active='$active' and ((specification_group_to_categories.cat_id='$cat_id' and specification_group_to_categories.subcat_id='$subcat_id') or (specification_group_to_categories.cat_id='0' and specification_group_to_categories.subcat_id='0') or (specification_group_to_categories.cat_id='0' and specification_group_to_categories.subcat_id='$subcat_id') or (specification_group_to_categories.cat_id='$cat_id' and specification_group_to_categories.subcat_id='0'))";
		
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( specification.specification_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .="  OR specification_group.specification_group_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}
		
		if($pcat_id!=0){
			$where .=" AND ";
			$where .="specification_group_to_categories.pcat_id='$pcat_id'";
		}
		/*$where .=" AND ";
		$where .="specification_group_to_categories.cat_id='$cat_id'";
		$where .=" AND ";
		$where .="specification_group_to_categories.subcat_id='$subcat_id'";*/
		$where .=" AND ";
		$where .="specification_group.specification_group_id='$specification_group_id'";
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by specification.timestamp desc";

		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}		
	}
	public function products_processing($params,$fg){
		$columns = array( 
			0 =>'products.product_id', 
			1 => 'products.product_name',
			3 => 'products.timestamp',

		);
		$pcat_id =$params['pcat_id'];
		$cat_id =$params['cat_id'];
		$subcat_id =$params['subcat_id'];
		$brand_id =$params['brand_id'];
		$active =$params['active'];

		$user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");

		$where = $sqlTot = $sqlRec = "";		
		if($pcat_id==0){
			$sql = "select products.*, products.active as product_active,brands.active as brand_active,brands.brand_name,brands.brand_id,subcategory.subcat_id,subcategory.subcat_name,category.cat_name,category.cat_id from products,brands,subcategory,category  where products.pcat_id=0 and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=products.brand_id and products.active='$active' ";
		}else{
			$sql = "select products.*, products.active as product_active,brands.active as brand_active,brands.brand_name,brands.brand_id,subcategory.subcat_id,subcategory.subcat_name,category.cat_name,category.cat_id, parent_category.pcat_id,parent_category.pcat_name from products,brands,subcategory,parent_category,category  where products.pcat_id=parent_category.pcat_id and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and products.active='$active' ";
		}	


		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( products.product_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR brands.brand_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR subcategory.subcat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR category.cat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}
		if($pcat_id!=0){
			$where .=" AND ";
			$where .="parent_category.pcat_id='$pcat_id'";
		}	
		$where .=" AND ";
		$where .="category.cat_id='$cat_id'";
		$where .=" AND ";
		$where .="subcategory.subcat_id='$subcat_id'";
		$where .=" AND ";
		$where .="brands.brand_id='$brand_id'";
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		if(isset($where) && $where != '') {
			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by products.timestamp desc";
		//echo $sqlRec;		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}		
	}
	public function inventory_processing($params,$fg){
		
		$attribute_1_value =$params['search_attribute_1_value'];
		$attribute_2_value =$params['search_attribute_2_value'];
		$attribute_3_value =$params['search_attribute_3_value'];
		$attribute_4_value =$params['search_attribute_4_value'];
		$product_status =$params['search_product_status'];
		$product_status_view =$params['search_view_product_status'];
		$product_id =$params['product_id'];
		$active =$params['active'];
		$inventory_type =$params['inventory_type'];

		$user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");

		$where = $sqlTot = $sqlRec = "";
		$sql = "select *,inventory.approval_status as publish_status,inventory.status_updated_on as status_on from inventory ";
 
		$sql.= ' LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`'
				.' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`'
				.' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`'
				.' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`'
				.' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id` ';

		if($user_type=='vendor'){
			$sql.=' LEFT JOIN `vendor_catalog_inventory` ON `vendor_catalog_inventory`.`vendor_catalog_id` = `inventory`.`inv_vendor_catalog_id`';
		}
		
		$sql.= " where products.product_id='$product_id' and  inventory.active='$active' ";
	
		if($user_type=='vendor'){
			$where .=" and ";
			$where .=" vendor_catalog_inventory.vendor_id='$a_id' ";
		}
		if($inventory_type!=''){
			$where .=" and ";
			$where .=" inventory.inventory_type='$inventory_type' ";
		}
		if(!empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" (sku_id LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR selling_price LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}
		if(!empty($attribute_1_value)){
			$where .=" AND ";
			$where .="attribute_1_value='$attribute_1_value' ";
		}
		if(!empty($attribute_2_value)){
				
			$where .=" AND ";
			$where .="attribute_2_value='$attribute_2_value'";
		}
		if(!empty($attribute_3_value)){
			$where .=" AND ";
			$where .="attribute_3_value='$attribute_3_value'";
		}
		if(!empty($attribute_4_value)){
			
			$where .=" AND ";
			$where .="attribute_4_value='$attribute_4_value'";
		}
		if(!empty($product_status)){
			$where .=" AND ";
			$where .="product_status='$product_status'";
		}
		if($product_status_view!=""){
			$where .=" AND ";
			$where .="product_status_view='$product_status_view'";
		}
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		//echo $sql;

		if(isset($where) && $where != '') {
			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " order by inventory.timestamp desc";

		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
			
	}
	public function logistics_processing($params,$fg){
		$columns = array( 
			0 =>'logistics.logistics_id', 
			1 => 'logistics.logistics_name',
			5 => 'logistics.timestamp',

		);
		$vendor_id =$params['vendor_id'];
		$where = $sqlTot = $sqlRec = "";
		$sql = "SELECT logistics.*,vendors.vendor_id,vendors.name FROM logistics,vendors where vendors.vendor_id=logistics.vendor_id and vendors.active=1";
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( logistics.logistics_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .="  OR vendors.name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}

		$where .=" AND ";
		$where .="vendors.vendor_id='$vendor_id'";
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by logistics.timestamp desc";

		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
	}
	public function logistics_service_processing($params,$fg){
		$columns = array( 
			0 =>'logistics_service.logistics_service_id', 
			1 => 'logistics_service.logistics_name',
			2 => 'logistics_service.city',
			4 => 'logistics_service.timestamp',

		);
		$vendor_id =$params['vendor_id'];
		$logistics_id =$params['logistics_id'];
		$where = $sqlTot = $sqlRec = "";
		$sql = "SELECT logistics_service.*,logistics.logistics_id,logistics.logistics_name,logistics.vendor_id,logistics_all_pincodes.pincodes FROM logistics_service,logistics,logistics_all_pincodes Where  logistics.logistics_id=logistics_service.logistics_id and logistics_service.pin=logistics_all_pincodes.pin";

		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( logistics.logistics_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .="  OR logistics_service.pin LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .="  OR logistics_service.city LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}

		$where .=" AND ";
		$where .="logistics.vendor_id='$vendor_id'";
		$where .=" AND ";
		$where .="logistics_service.logistics_id='$logistics_id'";
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		if(isset($where) && $where != '') {
			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by logistics.timestamp desc";

		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
			
	}
	public function logistics_territory_processing($params,$fg){
		$columns = array( 
			0 =>'logistics_territory.logistics_territory_id', 
			1 => 'logistics_service.logistics_name',
			2 => 'logistics_service.city',
			4 => 'logistics_territory.territory_name',
			5 => 'logistics_territory.timestamp',

		);
		$vendor_id =$params['vendor_id'];
		$logistics_id =$params['logistics_id'];

		$where = $sqlTot = $sqlRec = "";

		$sql = "SELECT logistics_territory.*,logistics_service.logistics_service_id,logistics_service.city,vendors.vendor_id,vendors.name,logistics.logistics_name,logistics.logistics_id,logistics.vendor_id FROM logistics_service,logistics,vendors,logistics_territory where logistics.logistics_id=logistics_service.logistics_id and vendors.vendor_id=logistics.vendor_id and logistics_territory.logistics_service_id = logistics_service.logistics_service_id and vendors.active=1";

		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( logistics_territory.territory_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .="  OR logistics.logistics_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .="  OR logistics_service.pin LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .="  OR logistics_service.city LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}
		$where .=" AND ";
		$where .="logistics.vendor_id='$vendor_id'";
		$where .=" AND ";
		$where .="logistics_service.logistics_id='$logistics_id'";
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by logistics_territory.timestamp desc";

		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}	
	}
	
	public function logistics_weight_processing($params,$fg){
		$columns = array( 
			0 =>'logistics_weight.logistics_weight_id', 
			2 => 'logistics_weight.weight_in_kg',
			3 => 'logistics_weight.volume_in_kg',
			4 => 'logistics_weight.logistics_price',
			5 => 'logistics_weight.timestamp',

		);
		$vendor_id =$params['vendor_id'];
		$logistics_id =$params['logistics_id'];
		$territory_name_id =$params['logistics_territory_id'];
		$logistics_delivery_mode_id =$params['logistics_delivery_mode_id'];
		$logistics_parcel_category_id =$params['logistics_parcel_category_id'];
		$where = $sqlTot = $sqlRec = "";
		$sql = "SELECT logistics_weight.*,logistics.logistics_name,logistics.logistics_id,logistics.vendor_id from logistics_weight,logistics where logistics.logistics_id=logistics_weight.logistics_id ";
		if(!empty($params['search']['value'])){   
			$where .=" and ";
			$where .=" ( logistics_weight.weight_in_kg LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .="  OR logistics_weight.volume_in_kg LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .="  OR logistics_weight.logistics_price LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}
		$where .=" AND ";
		$where .="logistics.vendor_id='$vendor_id'";
		$where .=" AND ";
		$where .="logistics.logistics_id='$logistics_id'";
		$where .=" AND ";
		$where .="logistics_weight.territory_name_id='$territory_name_id'";
		$where .=" AND ";
		$where .="logistics_weight.logistics_delivery_mode_id='$logistics_delivery_mode_id'";
		$where .=" AND ";
		$where .="logistics_weight.logistics_parcel_category_id='$logistics_parcel_category_id'";
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " GROUP BY logistics_weight.weight_in_kg ";
		$sqlTot .=  " GROUP BY logistics_weight.weight_in_kg ";
		
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		
		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}	
	}
	public function get_parent_category_name_by_pcat_id($pcat_id){
		$sql="select pcat_name from parent_category where pcat_id='$pcat_id'";
		$query=$this->db->query($sql);
		return $query->row()->pcat_name;
	}
/////////ashw integrated///////////////
	public function reasons(){
		$sql="select R.*,S.subcat_name,S.subcat_id,C.cat_name,C.cat_id, P.pcat_id,P.pcat_name from reason_return R ,parent_category P,category C ,subcategory S where R.pcat_id=P.pcat_id and C.cat_id=R.cat_id and S.subcat_id=R.subcat_id";			
		$query = $this->db->query($sql);		
		return $query -> result();
	}
	public function reasons_processing($params,$fg){
		$columns = array( 
			0 =>'reason_return.reason_name', 
			1 =>'reason_return.reason_name', 
			6 => 'reason_return.timestamp',

		);
		$pcat_id =$params['pcat_id'];
		$cat_id =$params['cat_id'];
		$subcat_id =$params['subcat_id'];		
		$where = $sqlTot = $sqlRec = "";
		$sql = "select reason_return.*,subcategory.subcat_id,subcategory.subcat_name,category.cat_name,category.cat_id, parent_category.pcat_id,parent_category.pcat_name from reason_return ";
		$sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `reason_return`.`pcat_id`';
		$sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `reason_return`.`cat_id`';
		$sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `reason_return`.`subcat_id`';

		if(!empty($params['search']['value'])){   
			$where .=" and ";
			$where .=" ( reason_return.reason_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR subcategory.subcat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR category.cat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			if(intval($pcat_id)!=0) {
				$where .=" OR parent_category.pcat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
			}else{
				$where .="  )";
			}
		}
		$where .=" AND ";
		//if(intval($pcat_id)!=0) {
			$where .= "parent_category.pcat_id='$pcat_id'";
			$where .= " AND ";
		//}
		$where .="category.cat_id='$cat_id'";
		$where .=" AND ";
		$where .="subcategory.subcat_id='$subcat_id'";
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		if(isset($where) && $where != '') {
			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by reason_return.timestamp desc";
		//echo $sqlRec;
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}		
	}
	public function add_reason($reason_name,$subcat_id,$cat_id,$pcat_id,$menu_sort_order,$view){	
		$insertinfo = array("reason_name"=> $reason_name,"subcat_id" => $subcat_id,"cat_id" => $cat_id,"pcat_id" => $pcat_id,"sort_order" => $menu_sort_order,"view" => $view);
		$flag=$this -> db -> insert("reason_return", $insertinfo);		
		return $flag;		
	}
	
	public function edit_reason($reason_id,$reason_name,$subcat_id,$cat_id,$pcat_id,$menu_sort_order,$view,$common_reason_return){
		if($common_reason_return!=""){
			$sql="SELECT * FROM `reason_return` Where reason_id='$common_reason_return'";
			$query =$this->db->query($sql);
			$menuto_sort_order=$query->row_array()["sort_order"];
			$sql="SELECT * FROM `reason_return` Where reason_id='$reason_id'";
			$query =$this->db->query($sql);
			$menufrom_sort_order=$query->row_array()["sort_order"];
			
			$sql = "UPDATE `reason_return` SET sort_order='$menuto_sort_order' WHERE `reason_id` = '$reason_id'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `reason_return` SET sort_order='$menufrom_sort_order' WHERE `reason_id` = '$common_reason_return'";
			$result=$this->db->query($sql);
			$sql = "UPDATE `reason_return` SET reason_name='$reason_name',pcat_id='$pcat_id',cat_id='$cat_id', subcat_id='$subcat_id',view='$view' WHERE `reason_id` = '$reason_id'";	
			$query = $this->db->query($sql);
			}else{
		$sql = "UPDATE `reason_return` SET reason_name='$reason_name',pcat_id='$pcat_id',cat_id='$cat_id', subcat_id='$subcat_id',sort_order='$menu_sort_order',view='$view' WHERE `reason_id` = '$reason_id'";	
		$query = $this->db->query($sql);
		}
		return $query;
	}
	public function get_reason_data($pcat_id,$cat_id,$subcat_id,$reason_id){
		$sql="SELECT * FROM `reason_return` Where pcat_id='$pcat_id' and cat_id='$cat_id' and subcat_id='$subcat_id' and reason_id='$reason_id'";
		$query =$this->db->query($sql);
		return $query->row_array();	
	}
	public function delete_reason($reason_id){
		$sql_delete="delete from reason_return where reason_id='$reason_id'";
		$sql_delete1="delete from sub_issue_return where reason_id='$reason_id'";		
		$res1=$this->db->query($sql_delete);
		$res2=$this->db->query($sql_delete1);
		if($res1==true && $res2==true){
			return true;
		}else{
			return false;
		}
	}
	public function subreason($reason_id) {
		$query = $this->db->query("select * from sub_issue_return where reason_id='$reason_id'");
		return $query -> result();
	}
	public function add_subreason($sub_issue_name,$reason_id,$menu_sort_order,$view){
		$insertinfo = array("sub_issue_name"=> $sub_issue_name,"reason_id" => $reason_id,"sort_order" => $menu_sort_order,"view" => $view);
		$flag=$this -> db -> insert("sub_issue_return", $insertinfo);	
		return $flag;	
	}	
	public function subreasons_processing($params,$fg){
		$columns = array( 
			0 =>'sub_issue_return.sub_issue_name', 
			1 =>'sub_issue_return.sub_issue_name', 
			2 => 'sub_issue_return.sort_order',
			4 => 'sub_issue_return.timestamp',

		);
		$reason_id =$params['reason_id'];
		$where = $sqlTot = $sqlRec = "";
		$sql = "select sub_issue_return.*,reason_return.reason_id from sub_issue_return,reason_return where sub_issue_return.reason_id=reason_return.reason_id ";
		if(!empty($params['search']['value'])){   
			$where .=" and ";
			$where .=" ( sub_issue_return.sub_issue_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";		
		}
		$where .=" AND ";
		$where .="reason_return.reason_id='$reason_id'";	
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by sub_issue_return.timestamp desc";
		//echo $sqlRec;	
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}		
	}
	public function get_subreason_data($sub_issue_id){
		$sql="SELECT * FROM `sub_issue_return` Where sub_issue_id='$sub_issue_id'";
		$query =$this->db->query($sql);
		return $query->row_array();	
	}
	public function edit_subreason($edit_subreason_name,$edit_view,$reason_id,$edit_sub_issue_id,$menu_sort_order,$common_subreason){
		if($common_subreason!=""){
			$sql="SELECT * FROM `sub_issue_return` Where sub_issue_id='$common_subreason'";
			$query =$this->db->query($sql);
			$menuto_sort_order=$query->row_array()["sort_order"];
			$sql="SELECT * FROM `sub_issue_return` Where sub_issue_id='$edit_sub_issue_id'";
			$query =$this->db->query($sql);
			$menufrom_sort_order=$query->row_array()["sort_order"];
			
			$sql = "UPDATE `sub_issue_return` SET sort_order='$menuto_sort_order' WHERE `sub_issue_id` = '$edit_sub_issue_id'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `sub_issue_return` SET sort_order='$menufrom_sort_order' WHERE `sub_issue_id` = '$common_subreason'";
			$result=$this->db->query($sql);
			$sql = "UPDATE `sub_issue_return` SET sub_issue_name='$edit_subreason_name',view='$edit_view' WHERE `sub_issue_id` = '$edit_sub_issue_id'";
			$query = $this->db->query($sql);
			}else{
		$sql = "UPDATE `sub_issue_return` SET sub_issue_name='$edit_subreason_name',sort_order='$menu_sort_order',view='$edit_view' WHERE `sub_issue_id` = '$edit_sub_issue_id'";
		$query = $this->db->query($sql);
		}
		
		return $query;
	}
	public function delete_subreason($sub_issue_id){
		$sql_delete="delete from sub_issue_return where sub_issue_id='$sub_issue_id'";		
		$res1=$this->db->query($sql_delete);	
		if($res1==true){
			return true;
		}else{
			return false;
		}
	}		
/////////ashw integrated///////////////	

public function reason_cancel() {
		$query = $this->db->query("SELECT * FROM `reason_cancel`");
		return $query -> result();
	} 
	
	public function add_reason_cancel($reason_name,$menu_sort_order){
		$insertinfo = array("reason_name" => $reason_name,"sort_order" => $menu_sort_order);
		$flag=$this -> db -> insert("reason_cancel", $insertinfo);
		return $flag;
	}
	
	public function get_reason_cancel_data($cancel_reason_id){
		$query =$this->db->query("SELECT * FROM `reason_cancel` Where cancel_reason_id='$cancel_reason_id'");
		return $query->row_array();		
	}
	
	public function edit_reason_cancel($cancel_reason_id,$reason_name,$menu_sort_order,$common_reason_cancel){
		if($common_reason_cancel!=""){
			$sql="SELECT * FROM `reason_cancel` Where cancel_reason_id='$common_reason_cancel'";
			$query =$this->db->query($sql);
			$menuto_sort_order=$query->row_array()["sort_order"];
			$sql="SELECT * FROM `reason_cancel` Where cancel_reason_id='$cancel_reason_id'";
			$query =$this->db->query($sql);
			$menufrom_sort_order=$query->row_array()["sort_order"];
			
			$sql = "UPDATE `reason_cancel` SET sort_order='$menuto_sort_order' WHERE `cancel_reason_id` = '$cancel_reason_id'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `reason_cancel` SET sort_order='$menufrom_sort_order' WHERE `cancel_reason_id` = '$common_reason_cancel'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `reason_cancel` SET reason_name='$reason_name' WHERE `cancel_reason_id` = '$cancel_reason_id'";
			$result=$this->db->query($sql);
			}else{
		$sql = "UPDATE `reason_cancel` SET reason_name='$reason_name',sort_order='$menu_sort_order' WHERE `cancel_reason_id` = '$cancel_reason_id'";
		$result=$this->db->query($sql);
		}
		
		return $result;
	}
	
	public function delete_reason_cancel_selected($selected_list){
		$sql_deletepcat="delete from reason_cancel where cancel_reason_id in ($selected_list)";
		$flag1=$this->db->query($sql_deletepcat);
		return $flag1;
	}
	
	public function admin_cancel_reason() {
		$query = $this->db->query("SELECT * FROM `admin_cancel_reason`");
		return $query -> result();
	} 
	
	public function add_admin_cancel_reason($cancel_reason,$menu_sort_order){
		$insertinfo = array("cancel_reason" => $cancel_reason,"sort_order" => $menu_sort_order);
		$flag=$this -> db -> insert("admin_cancel_reason", $insertinfo);
		return $flag;
	}
	
	public function get_admin_cancel_reason_data($cancel_reason_id){
		$query =$this->db->query("SELECT * FROM `admin_cancel_reason` Where cancel_reason_id='$cancel_reason_id'");
		return $query->row_array();		
	}
	
	public function edit_admin_cancel_reason($cancel_reason_id,$reason_name,$menu_sort_order,$common_admin_reason_cancel){
		if($common_admin_reason_cancel!=""){
			$sql="SELECT * FROM `admin_cancel_reason` Where cancel_reason_id='$common_admin_reason_cancel'";
			$query =$this->db->query($sql);
			$menuto_sort_order=$query->row_array()["sort_order"];
			$sql="SELECT * FROM `admin_cancel_reason` Where cancel_reason_id='$cancel_reason_id'";
			$query =$this->db->query($sql);
			$menufrom_sort_order=$query->row_array()["sort_order"];
			
			$sql = "UPDATE `admin_cancel_reason` SET sort_order='$menuto_sort_order' WHERE `cancel_reason_id` = '$cancel_reason_id'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `admin_cancel_reason` SET sort_order='$menufrom_sort_order' WHERE `cancel_reason_id` = '$common_admin_reason_cancel'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `admin_cancel_reason` SET cancel_reason='$reason_name' WHERE `cancel_reason_id` = '$cancel_reason_id'";
			$result=$this->db->query($sql);
			}else{
		$sql = "UPDATE `admin_cancel_reason` SET cancel_reason='$reason_name',sort_order='$menu_sort_order' WHERE `cancel_reason_id` = '$cancel_reason_id'";
		$result=$this->db->query($sql);
		}

		return $result;
	}
	
	public function delete_admin_cancel_reason_selected($selected_list){
		$sql_deletepcat="delete from admin_cancel_reason where cancel_reason_id in ($selected_list)";
		$flag1=$this->db->query($sql_deletepcat);
		return $flag1;
	}
	
	public function admin_replacement_reason() {
		$query = $this->db->query("SELECT * FROM `admin_replacement_reason`");
		return $query -> result();
	}
	
	public function add_admin_replacement_reason($return_options,$decision_return,$pickup_identifier){
		$insertinfo = array("return_options" => $return_options,"decision_return" => $decision_return, "pickup_identifier" => $pickup_identifier);
		$flag=$this -> db -> insert("admin_replacement_reason", $insertinfo);
		return $flag;
	}
	
	public function get_admin_replacement_reason_data($id){
		$query =$this->db->query("SELECT * FROM `admin_replacement_reason` Where id='$id'");
		return $query->row_array();		
	}
	
	public function edit_admin_replacement_reason($id,$edit_return_options,$edit_decision_return,$edit_pickup_identifier){
		$sql = "UPDATE `admin_replacement_reason` SET return_options='$edit_return_options',decision_return='$edit_decision_return',pickup_identifier='$edit_pickup_identifier' WHERE `id` = '$id'";
		$result=$this->db->query($sql);
		return $result;
	}
	
	public function delete_admin_replacement_reason_selected($selected_list){
		$sql_deletepcat="delete from admin_replacement_reason where id in ($selected_list)";
		$flag1=$this->db->query($sql_deletepcat);
		return $flag1;
	}
	
	public function admin_return_reason() {
		$query = $this->db->query("SELECT * FROM `admin_return_reason`");
		return $query -> result();
	}
	
	public function add_admin_return_reason($return_options,$decision_return,$pickup_identifier){
		$insertinfo = array("return_options" => $return_options,"decision_return" => $decision_return, "pickup_identifier" => $pickup_identifier);
		$flag=$this -> db -> insert("admin_return_reason", $insertinfo);
		return $flag;
	}
	
	public function get_admin_return_reason_data($id){
		$query =$this->db->query("SELECT * FROM `admin_return_reason` Where id='$id'");
		return $query->row_array();		
	}
	
	public function edit_admin_return_reason($id,$edit_return_options,$edit_decision_return,$edit_pickup_identifier){
		$sql = "UPDATE `admin_return_reason` SET return_options='$edit_return_options',decision_return='$edit_decision_return',pickup_identifier='$edit_pickup_identifier' WHERE `id` = '$id'";
		$result=$this->db->query($sql);
		return $result;
	}
	public function delete_admin_return_reason_selected($selected_list){
		$sql_deletepcat="delete from admin_return_reason where id in ($selected_list)";
		$flag1=$this->db->query($sql_deletepcat);
		return $flag1;
	}
	
	public function customer_deactivate_reason() {
		$query = $this->db->query("SELECT * FROM `customer_deactivate_reason`");
		return $query -> result();
	}
	
	public function add_deactivate_reason($deactivate_reason,$menu_sort_order,$view){
		$insertinfo = array("deactivate_reason" => $deactivate_reason,"sort_order" => $menu_sort_order,"view" => $view);
		$flag=$this -> db -> insert("customer_deactivate_reason", $insertinfo);
		return $flag;
	}
	
	public function get_customer_deactivate_reason_data($id){
		$query =$this->db->query("SELECT * FROM `customer_deactivate_reason` Where id='$id'");
		return $query->row_array();		
	}
	
	public function edit_deactivate_reason($id,$edit_deactivate_reason,$menu_sort_order,$view,$common_customer_deactivate){
		if($common_customer_deactivate!=""){
			$sql="SELECT * FROM `customer_deactivate_reason` Where id='$common_customer_deactivate'";
			$query =$this->db->query($sql);
			$menuto_sort_order=$query->row_array()["sort_order"];
			$sql="SELECT * FROM `customer_deactivate_reason` Where id='$id'";
			$query =$this->db->query($sql);
			$menufrom_sort_order=$query->row_array()["sort_order"];
			
			$sql = "UPDATE `customer_deactivate_reason` SET sort_order='$menuto_sort_order' WHERE `id` = '$id'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `customer_deactivate_reason` SET sort_order='$menufrom_sort_order' WHERE `id` = '$common_customer_deactivate'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `customer_deactivate_reason` SET deactivate_reason='$edit_deactivate_reason',view='$view' WHERE `id` = '$id'";
			$result=$this->db->query($sql);
			}else{
		$sql = "UPDATE `customer_deactivate_reason` SET deactivate_reason='$edit_deactivate_reason',sort_order='$menu_sort_order',view='$view' WHERE `id` = '$id'";
		$result=$this->db->query($sql);
		}
		
		return $result;
	}
	
	public function delete_deactivate_reason_selected($selected_list){
		$sql_deletepcat="delete from customer_deactivate_reason where id in ($selected_list)";
		$flag1=$this->db->query($sql_deletepcat);
		return $flag1;
	}
	
	public function payment_options() {
		$query = $this->db->query("SELECT * FROM `payment_options`");
		return $query -> result();
	}
	
	public function add_payment_options($payment_option,$menu_sort_order,$view){
		$insertinfo = array("payment_option_name" => $payment_option,"sort_order" => $menu_sort_order,"view" => $view);
		$flag=$this -> db -> insert("payment_options", $insertinfo);
		return $flag;
	}
	
	public function get_payment_options_data($id){
		$query =$this->db->query("SELECT * FROM `payment_options` Where id='$id'");
		return $query->row_array();		
	}
	
	public function edit_payment_options($id,$edit_payment_option_name,$menu_sort_order,$view,$common_payment_options){
		if($common_payment_options!=""){
			
			$sql="SELECT * FROM `payment_options` Where id='$common_payment_options'";
			$query =$this->db->query($sql);
			$menuto_sort_order=$query->row_array()["sort_order"];
			$sql="SELECT * FROM `payment_options` Where id='$id'";
			$query =$this->db->query($sql);
			$menufrom_sort_order=$query->row_array()["sort_order"];
			
			$sql = "UPDATE `payment_options` SET sort_order='$menuto_sort_order' WHERE `id` = '$id'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `payment_options` SET sort_order='$menufrom_sort_order' WHERE `id` = '$common_payment_options'";
			$result=$this->db->query($sql);
			
			$sql = "UPDATE `payment_options` SET payment_option_name='$edit_payment_option_name',view='$view' WHERE `id` = '$id'";
			$result=$this->db->query($sql);
			}else{
				
		$sql = "UPDATE `payment_options` SET payment_option_name='$edit_payment_option_name',sort_order='$menu_sort_order',view='$view' WHERE `id` = '$id'";
		$result=$this->db->query($sql);
		}
		return $result;
	}
	
	public function delete_payment_options_selected($selected_list){
		$sql_deletepcat="delete from payment_options where id in ($selected_list)";
		$flag1=$this->db->query($sql_deletepcat);
		return $flag1;
	}
	
	public function parent_category_count(){
		$sql="select count(*) as count from  parent_category where active=1";
		$query=$this->db->query($sql);
		return $query->row()->count;
	}
	public function archived_parent_category_count(){
		$sql="select count(*) as count from  parent_category where active=0 ";
		$query=$this->db->query($sql);
		return $query->row()->count;
	}
	public function vendors_count(){
		$sql="select count(*) as count from  vendors where active=1";
		$query=$this->db->query($sql);
		return $query->row()->count;
	}
	public function archived_vendor_count(){
		$sql="select count(*) as count from  vendors where active=0";
		$query=$this->db->query($sql);
		return $query->row()->count;
	}
	public function logistics_pincode_count(){
		$sql="select count(*) as count from  logistics_all_pincodes";
		$query=$this->db->query($sql);
		return $query->row()->count;
	}
	
	public function show_sort_order(){
		$sql="select pcat_id as common_cat_id,pcat_name as common_cat_name,menu_sort_order,'pcat' as  'type' from parent_category  union select cat_id as common_cat_id,cat_name as common_cat_name,menu_sort_order,'cat' as 'type' from category where menu_level=1";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	public function get_all_parent_category(){
		$sql="select pcat_id as common_cat_id,pcat_name as common_cat_name,menu_sort_order,'pcat' as  'type' from parent_category union select cat_id as common_cat_id,cat_name as common_cat_name,menu_sort_order,'cat' as 'type' from category where  menu_level=1 and active=1";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	public function show_edit_sort_order($edit_menu_sort_order){
		$sql="select pcat_id as common_cat_id,pcat_name as common_cat_name,menu_sort_order,'pcat' as  'type' from parent_category where menu_sort_order!='$edit_menu_sort_order' and menu_level=1 and active=1 and menu_sort_order!=0 union all select cat_id as common_cat_id,cat_name as common_cat_name,menu_sort_order,'cat' as 'type' from category where menu_sort_order!='$edit_menu_sort_order' and menu_level=1 and active=1 and menu_sort_order!=0";
		$query=$this->db->query($sql);
		return $query->result_array();
	}

	public function get_cat_all_parent_category($menu_level,$pcat_id){
		if($menu_level==1){
		$sql="select pcat_id as common_cat_id,pcat_name as common_cat_name,menu_sort_order,'pcat' as  'type' from parent_category where menu_level=1 and active=1 union select cat_id as common_cat_id,cat_name as common_cat_name,menu_sort_order,'cat' as 'type' from category where menu_level=1 and active=1";
		$query=$this->db->query($sql);
		return $query->result_array();
		}

		if($menu_level==2){
			$sql="select cat_id as common_cat_id,cat_name as common_cat_name,menu_sort_order,'cat' as 'type' from category where pcat_id='$pcat_id' and menu_level=2 and active=1";
		$query=$this->db->query($sql);
		return $query->result_array();
		}
		
	}
	
	public function show_cat_edit_sort_order($edit_menu_sort_order,$edit_menu_level,$pcat_id){
		if($edit_menu_level==1){
		$sql="select pcat_id as common_cat_id,pcat_name as common_cat_name,menu_sort_order,'pcat' as  'type' from parent_category where menu_sort_order!='$edit_menu_sort_order' and menu_level=1 and active=1 and menu_sort_order!=0 union select cat_id as common_cat_id,cat_name as common_cat_name,menu_sort_order,'cat' as 'type' from category where menu_sort_order!='$edit_menu_sort_order' and menu_level=1 and active=1 and menu_sort_order!=0";
		$query=$this->db->query($sql);
		return $query->result_array();
		}
		if($edit_menu_level==2){
		$sql="select cat_id as common_cat_id,cat_name as common_cat_name,menu_sort_order,'cat' as 'type' from category where menu_sort_order!='$edit_menu_sort_order' and menu_level=2 and pcat_id='$pcat_id' and active=1 and menu_sort_order!=0";
		$query=$this->db->query($sql);
		return $query->result_array();	
		}
	}
	
	public function get_edit_all_parent_category($edit_menu_level,$pcat_id){
		if($edit_menu_level==1){
		$sql="select pcat_id as common_cat_id,pcat_name as common_cat_name,menu_sort_order,'pcat' as  'type' from parent_category where menu_level=1 and active=1 union select cat_id as common_cat_id,cat_name as common_cat_name,menu_sort_order,'cat' as 'type' from category where menu_level=1 and active=1";
		$query=$this->db->query($sql);
		return $query->result_array();
		}
		if($edit_menu_level==2){
			$sql="select cat_id as common_cat_id,cat_name as common_cat_name,menu_sort_order,'cat' as 'type' from category where pcat_id='$pcat_id' and menu_level=2 and active=1";
		$query=$this->db->query($sql);
		return $query->result_array();
		}
		
	}
	
	public function get_all_sub_category($pcat_id,$cat_id){
		$sql="select * from subcategory where pcat_id='$pcat_id' and cat_id='$cat_id' and active=1";
		$query=$this->db->query($sql);
		return $query->result_array();
		
	}
	
	public function get_all_payment_options(){
		$sql="select * from payment_options";
		$query=$this->db->query($sql);
		return $query->result_array();
		
	}
	public function show_subcategory_edit_sort_order($edit_menu_sort_order,$pcat_id,$cat_id){
		$sql="select subcat_id,subcat_name,menu_sort_order from subcategory where menu_sort_order!='$edit_menu_sort_order' and pcat_id='$pcat_id' and cat_id='$cat_id' and active=1 and menu_sort_order!=0";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	/*public function show_sort_order_validation_edit($menu_sort_order,$menu_level,$pcat_id,$cat_id) {
		
		if($menu_level==1){	
			$sql="select * from parent_category where menu_sort_order='$menu_sort_order' and menu_level=1 and active=1 union select * from category where  menu_sort_order='$menu_sort_order' and menu_level=1 and active=1";
			$query = $this->db->query($sql);
			if($query->num_rows()>0){
				return true;
			}else{
				return false;
			}
		}
		if($menu_level==2){
			$sql="select * from category where pcat_id='$pcat_id' and menu_sort_order='$menu_sort_order' and menu_level=2 and active=1  and cat_id!='$cat_id'";
			$query = $this->db->query($sql);
			if($query->num_rows()>0){
				return true;
			}else{
				return false;
			}
		}
	}*/
	/*public function show_sort_order_validation($menu_sort_order,$menu_level,$pcat_id) {
		
		if($menu_level==1){	
			$sql="select * from parent_category where menu_sort_order='$menu_sort_order' and menu_level=1 and active=1 union select * from category where  menu_sort_order='$menu_sort_order' and menu_level=1 and active=1";
			$query = $this->db->query($sql);
			if($query->num_rows()>0){
				return true;
			}else{
				return false;
			}
		}
		if($menu_level==2){
			$sql="select * from category where pcat_id='$pcat_id' and menu_sort_order='$menu_sort_order' and menu_level=2 and active=1";
			$query = $this->db->query($sql);
			if($query->num_rows()>0){
				return true;
			}else{
				return false;
			}
		}
	}*/
	/*public function show_sort_order_validation_for_subcategory($menu_sort_order,$cat_id,$pcat_id) {
		$sql="select * from subcategory where pcat_id='$pcat_id' and cat_id='$cat_id' and menu_sort_order='$menu_sort_order' and active=1";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}*/
	
	public function parent_category_attribute_check() {
		$sql="select * from parent_category where active=1";
		$query = $this->db->query($sql);
		$parent_category_info = $query -> result();
		if(count($parent_category_info)>0){
			$parent_category_id_arr=array();
			foreach($parent_category_info as $parent_category){
				$parent_category_id_arr[]=$parent_category->pcat_id;
			}
		$parent_category_ids=implode(',',$parent_category_id_arr);
		}
		$sql1="select * from attribute where catalog='pcat_id' and active=1";
		$query1 = $this->db->query($sql1);
		$attribute_info = $query1 -> result();
		if(count($attribute_info)>0){
		$attribute_id_arr=array();
			foreach($attribute_info as $attribute){
				$attribute_id_arr[]=$attribute->catalog_id;
			}
			$attribute_ids=implode(',',$attribute_id_arr);
			
		}
		if(count($attribute_info)>0 && count($parent_category_info)>0){
		$common_attribute_ids=array_intersect($parent_category_id_arr,$attribute_id_arr);
		return $common_attribute_ids;
		}else{
			return '';
		}
	
	}
		
	public function add_attribute_check($pcat_id){
		$sql="select * from category where pcat_id='$pcat_id' and active=1";
		$query =$this->db->query($sql);
		$attribute_check_cat = $query -> result();
		if(count($attribute_check_cat)>0){
			$attribute_check_cat_id_arr=array();
			foreach($attribute_check_cat as $attribute_cat){
				$attribute_check_cat_id_arr[]=$attribute_cat->cat_id;
			}
		$attribute_check_cat_ids=implode(',',$attribute_check_cat_id_arr);
		}
		$sql1="select * from attribute where catalog='cat_id' and active=1";
		$query1 = $this->db->query($sql1);
		$attribute_info = $query1 -> result();
		if(count($attribute_info)>0){
		$attribute_id_arr=array();
			foreach($attribute_info as $attribute){
				$attribute_id_arr[]=$attribute->catalog_id;
			}
			$attribute_ids=implode(',',$attribute_id_arr);
			
		}
		if(count($attribute_info)>0 && count($attribute_check_cat)>0){
		$common_attribute_ids=array_intersect($attribute_check_cat_id_arr,$attribute_id_arr);
		return $common_attribute_ids;
		}else{
			return array();
		}
	}
	
	public function add_attribute_subcheck($cat_id){
		$sql="select * from subcategory where cat_id='$cat_id' and active=1";
		$query =$this->db->query($sql);
		$attribute_check_subcat = $query -> result();
		if(count($attribute_check_subcat)>0){
			$attribute_check_subcat_id_arr=array();
			foreach($attribute_check_subcat as $attribute_cat){
				$attribute_check_subcat_id_arr[]=$attribute_cat->subcat_id;
			}
		$attribute_check_cat_ids=implode(',',$attribute_check_subcat_id_arr);
		}
		$sql1="select * from attribute where catalog='subcat_id' and active=1";
		$query1 = $this->db->query($sql1);
		$attribute_info = $query1 -> result();
		if(count($attribute_info)>0){
		$attribute_id_arr=array();
			foreach($attribute_info as $attribute){
				$attribute_id_arr[]=$attribute->catalog_id;
			}
			$attribute_ids=implode(',',$attribute_id_arr);
			
		}
		if(count($attribute_info)>0 && count($attribute_check_subcat)>0){
		$common_attribute_ids=array_intersect($attribute_check_subcat_id_arr,$attribute_id_arr);
		return $common_attribute_ids;
		}else{
			return array();
		}
	}
	
	public function check_if_subcategories_availablity($cat_id) {
		$sql="select * from attribute where cat_id='$cat_id' and catalog='cat_id' and subcat_id=0";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	
	public function sort_order_filterbox_pcat($pcat_id,$cat_id){
		if($pcat_id==0){
			$sql="select filterbox_id from filterbox_to_category where cat_id='$cat_id' and active=1";
			$query=$this->db->query($sql);
		}else{
		$sql="select filterbox_id from filterbox_to_category where pcat_id='$pcat_id' and active=1";
		$query=$this->db->query($sql);
		}
		if($query->num_rows()>0){
			$filterbox_id_arr=$query->result_array();
			$arr=array();
			foreach($filterbox_id_arr as $v){
				$arr[]=$v["filterbox_id"];
			}
			$filterbox_id_in="'".implode("','",$arr)."'";
			$sql="select * from filterbox where filterbox_id in ($filterbox_id_in) and active=1";
			$query=$this->db->query($sql);
			return $query->result_array();
		}else{
			return false;
		}

	}
	
	
	/*public function show_sort_order_validation_for_filterbox($pcat_id,$cat_id,$sort_order) {
		if($pcat_id==0){
			$sql="select filterbox_id from filterbox_to_category where cat_id='$cat_id' and active=1";
			$query=$this->db->query($sql);
		}else{
		$sql="select filterbox_id from filterbox_to_category where pcat_id='$pcat_id' and active=1";
		$query=$this->db->query($sql);
		}
		
		$filterbox_id_arr=$query->result_array();
		$arr=array();
		foreach($filterbox_id_arr as $v){
			$arr[]=$v["filterbox_id"];
		}
		$filterbox_id_in="'".implode("','",$arr)."'";
		$sql1="select * from filterbox where filterbox_id in ($filterbox_id_in) and sort_order='$sort_order' and active=1";
		$query1=$this->db->query($sql1);
		if($query1->num_rows()>0){
			return true;
	}
		else{
			return false;
		}
	}*/
	
	public function show_filterbox_edit_sort_order($edit_sort_order,$pcat_id,$cat_id,$subcat_id,$result_type){
		if($result_type=="pcat_view"){
			$sql="select filterbox_id from filterbox_to_category where pcat_id='$pcat_id' and pcat_view=1 and active=1";
			$query=$this->db->query($sql);
		}
		if($result_type=="cat_view"){
		$sql="select filterbox_id from filterbox_to_category where pcat_id='$pcat_id' and cat_id='$cat_id' and  cat_view=1 and pcat_view=0 and active=1";
		$query=$this->db->query($sql);
		}
		if($result_type=="subcat_view"){
		$sql="select filterbox_id from filterbox_to_category where pcat_id='$pcat_id' and cat_id='$cat_id' and subcat_id='$subcat_id' and subcat_view=1 and cat_view=0 and pcat_view=0 and active=1";
		$query=$this->db->query($sql);
		}
		if($query->num_rows()>0){
			$filterbox_id_arr=$query->result_array();
			$arr=array();
			foreach($filterbox_id_arr as $v){
				$arr[]=$v["filterbox_id"];
			}
			$filterbox_id_in="'".implode("','",$arr)."'";
			$sql="select filterbox_id,filterbox_name,sort_order from filterbox where filterbox_id in ($filterbox_id_in) and sort_order!='$edit_sort_order' and active=1";
			$query=$this->db->query($sql);
			return $query->result_array();
		}
		else{
			return false;
		}
		
	}
	
	public function check_filterbox_visibility($pcat_id) {
		$sql="select * from filterbox_to_category where pcat_id='$pcat_id' and pcat_view=1 and active=1";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	
	public function check_filterbox_visibility_cat($cat_id) {
		$sql="select * from filterbox_to_category where cat_id='$cat_id' and cat_view=1 and active=1";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	
	public function get_all_filter_sort_order($filterbox_id){
		$sql="select * from filter where filterbox_id='$filterbox_id' and active=1";
		$query=$this->db->query($sql);
		return $query->result_array();
		
	}
	
	/*public function show_sort_order_validation_for_filter($sort_order,$filterbox_id) {
		$sql="select * from filter where filterbox_id='$filterbox_id' and sort_order='$sort_order' and active=1";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}*/
	
	public function show_filter_edit_sort_order($edit_sort_order,$filterbox_id){
		$sql="select filter_id,filter_options,sort_order from filter where sort_order!='$edit_sort_order' and filterbox_id='$filterbox_id' and active=1";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	public function sort_order_specification_pcat($pcat_id,$cat_id,$subcat_id){
		$specification_group_id_arr=array();
		$sql="select specification_group_id from specification_group_to_categories where pcat_id='$pcat_id' and active=1";
		$query=$this->db->query($sql);
		foreach($query->result_array() as $arr){
			$specification_group_id_arr[]=$arr["specification_group_id"];
		}
		$sql="select specification_group_id from specification_group_to_categories where pcat_id='$pcat_id' and cat_id='$cat_id' and active=1";
		$query=$this->db->query($sql);
		foreach($query->result_array() as $arr){
			$specification_group_id_arr[]=$arr["specification_group_id"];
		}
		
		$sql="select specification_group_id from specification_group_to_categories where pcat_id='$pcat_id' and cat_id='$cat_id' and subcat_id='$subcat_id' and active=1";
		$query=$this->db->query($sql);
		foreach($query->result_array() as $arr){
			$specification_group_id_arr[]=$arr["specification_group_id"];
		}
		//return $specification_group_id_arr;exit;
		if(count($specification_group_id_arr)>0){
			$specification_group_id_in="'".implode("','",$specification_group_id_arr)."'";
			$sql="select * from specification_group where specification_group_id in ($specification_group_id_in) and active=1";
			$query=$this->db->query($sql);
			
			return $query->result_array();
		}else{
			return false;
		}

	}
	
	public function show_sort_order_validation_for_specification_group($pcat_id,$cat_id,$subcat_id,$sort_order){
		$specification_group_id_arr=array();
		$sql="select specification_group_id from specification_group_to_categories where pcat_id='$pcat_id' and active=1";
		$query=$this->db->query($sql);
		foreach($query->result_array() as $arr){
			$specification_group_id_arr[]=$arr["specification_group_id"];
		}
		$sql="select specification_group_id from specification_group_to_categories where pcat_id='$pcat_id' and cat_id='$cat_id' and active=1";
		$query=$this->db->query($sql);
		foreach($query->result_array() as $arr){
			$specification_group_id_arr[]=$arr["specification_group_id"];
		}
		
		$sql="select specification_group_id from specification_group_to_categories where pcat_id='$pcat_id' and cat_id='$cat_id' and subcat_id='$subcat_id' and active=1";
		$query=$this->db->query($sql);
		foreach($query->result_array() as $arr){
			$specification_group_id_arr[]=$arr["specification_group_id"];
		}
		
		if(count($specification_group_id_arr)>0){
			$specification_group_id_in="'".implode("','",$specification_group_id_arr)."'";
			$sql="select * from specification_group where specification_group_id in ($specification_group_id_in) and sort_order='$sort_order' and active=1";
			$query=$this->db->query($sql);
			return $query->result_array();
		}else{
			return false;
		}

	}
	
	public function show_specification_group_edit_sort_order($edit_sort_order,$pcat_id,$cat_id,$subcat_id,$result_type){
		$specification_group_id_arr=array();
		if($result_type=="pcat_view"){
		$sql="select specification_group_id from specification_group_to_categories where pcat_id='$pcat_id' and pcat_view=1 and active=1";
		$query=$this->db->query($sql);
		foreach($query->result_array() as $arr){
			$specification_group_id_arr[]=$arr["specification_group_id"];
		}
		}
		if($result_type=="cat_view"){
		$sql="select specification_group_id from specification_group_to_categories where pcat_id='$pcat_id' and cat_id='$cat_id' and cat_view=1 and active=1";
		$query=$this->db->query($sql);
		foreach($query->result_array() as $arr){
			$specification_group_id_arr[]=$arr["specification_group_id"];
		}
		}
		if($result_type=="subcat_view"){
		$sql="select specification_group_id from specification_group_to_categories where pcat_id='$pcat_id' and cat_id='$cat_id' and subcat_id='$subcat_id' and subcat_view=1 and active=1";
		$query=$this->db->query($sql);
		foreach($query->result_array() as $arr){
			$specification_group_id_arr[]=$arr["specification_group_id"];
		}
		}
		if(count($specification_group_id_arr)>0){
			$specification_group_id_in="'".implode("','",$specification_group_id_arr)."'";
			$sql="select specification_group_id,specification_group_name,sort_order from specification_group where specification_group_id in ($specification_group_id_in) and sort_order!='$edit_sort_order' and active=1";
			$query=$this->db->query($sql);
			return $query->result_array();
		}else{
			return false;
		}

	}
	
	public function get_all_specification_sort_order($specification_group_id){
		$sql="select * from specification where specification_group_id='$specification_group_id' and active=1";
		$query=$this->db->query($sql);
		return $query->result_array();
		
	}
	
	public function show_sort_order_validation_for_specification($sort_order,$specification_group_id) {
		$sql="select * from specification where specification_group_id='$specification_group_id' and sort_order='$sort_order' and active=1";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	
	public function show_specification_edit_sort_order($edit_sort_order,$specification_group_id){
		$sql="select specification_id,specification_name,sort_order from specification where sort_order!='$edit_sort_order' and specification_group_id='$specification_group_id' and active=1";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	public function check_specification_group_visibility($pcat_id) {
		$sql="select * from specification_group_to_categories where pcat_id='$pcat_id' and pcat_view=1 and active=1";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	
	public function check_specification_group_visibility_cat($cat_id) {
		$sql="select * from specification_group_to_categories where cat_id='$cat_id' and cat_view=1 and active=1";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	
	public function get_all_reason_cancel_sort(){
		$sql="select * from reason_cancel";
		$query=$this->db->query($sql);
		return $query->result_array();
		
	}
	
	/*public function show_sort_order_validation_for_reason_cancel($menu_sort_order) {
		$sql="select * from reason_cancel where sort_order='$menu_sort_order'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}*/
	
	public function show_reason_cancel_edit_sort_order($edit_menu_sort_order){
		$sql="select cancel_reason_id,reason_name,sort_order from reason_cancel where sort_order!='$edit_menu_sort_order'";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	public function get_all_admin_cancel_reason_sort(){
		$sql="select * from admin_cancel_reason";
		$query=$this->db->query($sql);
		return $query->result_array();
		
	}
	/*public function show_sort_order_validation_for_admin_cancel_reason($menu_sort_order) {
		$sql="select * from admin_cancel_reason where sort_order='$menu_sort_order'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}*/
	public function show_admin_cancel_reason_edit_sort_order($edit_menu_sort_order){
		$sql="select cancel_reason_id,cancel_reason,sort_order from admin_cancel_reason where sort_order!='$edit_menu_sort_order'";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	public function get_all_customer_deactivate_sort(){
		$sql="select * from customer_deactivate_reason";
		$query=$this->db->query($sql);
		return $query->result_array();
		
	}	
	public function show_sort_order_validation_for_customer_deactivate($menu_sort_order) {
		$sql="select * from customer_deactivate_reason where sort_order='$menu_sort_order'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function show_customer_deactivate_edit_sort_order($edit_menu_sort_order){
		$sql="select id,deactivate_reason,sort_order from customer_deactivate_reason where sort_order!='$edit_menu_sort_order'";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	/*public function show_sort_order_validation_for_payment_options($menu_sort_order) {
		$sql="select * from payment_options where sort_order='$menu_sort_order'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}*/
	public function show_payment_options_edit_sort_order($edit_menu_sort_order){
		$sql="select id,payment_option_name,sort_order from payment_options where sort_order!='$edit_menu_sort_order'";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_all_reason_return_sort($pcat_id,$cat_id,$subcat_id){
		$sql="select * from reason_return where pcat_id='$pcat_id' and cat_id='$cat_id' and subcat_id='$subcat_id'";
		$query=$this->db->query($sql);
		return $query->result_array();
		
	}
	/*public function show_sort_order_validation_for_reason_return($menu_sort_order,$subcat_id,$cat_id,$pcat_id) {
		$sql="select * from reason_return where pcat_id='$pcat_id' and cat_id='$cat_id' and subcat_id='$subcat_id' and sort_order='$menu_sort_order'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}*/
	public function show_reason_return_edit_sort_order($edit_menu_sort_order,$pcat_id,$cat_id,$subcat_id){
		$sql="select reason_id,reason_name,sort_order from reason_return where sort_order!='$edit_menu_sort_order' and pcat_id='$pcat_id' and cat_id='$cat_id' and subcat_id='$subcat_id'";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_all_subreason_sort($reason_id){
		$sql="select * from sub_issue_return where reason_id='$reason_id'";
		$query=$this->db->query($sql);
		return $query->result_array();
		
	}
	
	public function show_sort_order_validation_for_subreason($menu_sort_order,$reason_id) {
		$sql="select * from sub_issue_return where reason_id='$reason_id' and sort_order='$menu_sort_order'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	
	public function show_subreason_edit_sort_order($edit_menu_sort_order,$reason_id){
		$sql="select sub_issue_id,sub_issue_name,sort_order from sub_issue_return where sort_order!='$edit_menu_sort_order' and reason_id='$reason_id' ";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	public function check_duplication_name($pcat_name) {
		$sql="select pcat_name from parent_category where pcat_name='$pcat_name'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_category($cat_name,$pcat_id,$menu_level){
		if($menu_level==1){
		$sql="select pcat_id as common_cat_id,pcat_name as common_cat_name,'pcat' as  'type' from parent_category where pcat_name='$cat_name' and menu_level=1 and active=1 union select cat_id as common_cat_id,cat_name as common_cat_name,'cat' as 'type' from category where cat_name='$cat_name' and menu_level=1 and active=1";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
		}
		if($menu_level==2){
		$sql="select cat_id as common_cat_id,cat_name as common_cat_name,'cat' as 'type' from category where cat_name='$cat_name' and menu_level=2 and pcat_id='$pcat_id' and active=1";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
		}
	}
	
	public function check_duplication_name_sub_category($subcat_name,$pcat_id,$cat_id) {
		$sql="select * from subcategory where subcat_name='$subcat_name' and pcat_id='$pcat_id' and cat_id='$cat_id'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	
	public function check_duplication_name_brand($brand_name,$subcat_id,$pcat_id,$cat_id) {
		$sql="select * from brands where brand_name='$brand_name' and pcat_id='$pcat_id' and cat_id='$cat_id' and subcat_id='$subcat_id'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	
	public function check_duplication_name_product($product_name,$pcat_id,$cat_id,$subcat_id,$brand_id) {
		$sql="select * from products where lower(product_name)=lower('$product_name') and pcat_id='$pcat_id' and cat_id='$cat_id' and subcat_id='$subcat_id' and brand_id='$brand_id'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_filterbox($filterbox_name,$pcat_id) {
		$sql="select * from filterbox,filterbox_to_category where filterbox_name='$filterbox_name' and pcat_id='$pcat_id'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_filter($filter_options,$filterbox_id) {
		$sql="select * from filter where filter_options='$filter_options' and filterbox_id='$filterbox_id'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_specification_group($specification_group_name,$pcat_id) {
		$sql="select * from specification_group,specification_group_to_categories where specification_group_name='$specification_group_name' and pcat_id='$pcat_id'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_specification($specification_name,$specification_group_id) {
		$sql="select * from specification where specification_name='$specification_name' and specification_group_id='$specification_group_id'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_reasons($reason_name,$pcat_id) {
		$sql="select * from reason_return where reason_name='$reason_name' and pcat_id='$pcat_id'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_subreasons($sub_issue_name,$reason_id) {
		$sql="select * from sub_issue_return where sub_issue_name='$sub_issue_name' and reason_id='$reason_id'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_payment_options($payment_option) {
		$sql="select * from payment_options where payment_option_name='$payment_option'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_customer_cancel_reason($reason_name) {
		$sql="select * from reason_cancel where reason_name='$reason_name'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_admin_cancel_reason($cancel_reason) {
		$sql="select * from admin_cancel_reason where cancel_reason='$cancel_reason'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_admin_replacement_reason($return_options) {
		$sql="select * from admin_replacement_reason where return_options='$return_options'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_admin_return_reason($return_options) {
		$sql="select * from admin_return_reason where return_options='$return_options'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_customer_deactivate($deactivate_reason) {
		$sql="select * from customer_deactivate_reason where deactivate_reason='$deactivate_reason'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_vendor($email,$mobile,$landline,$vendor_id) {
		if($vendor_id!=""){
			$sql="select * from product_vendors where (email='$email' or mobile='$mobile') and vendor_id!='$vendor_id'";
		}
		else{
			$sql="select * from product_vendors where (email='$email' or mobile='$mobile')";
		}
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_all_pincodes($city) {
		$sql="select * from logistics_all_pincodes where city='$city'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	
	public function check_duplication_name_delivery_mode_edit($delivery_mode,$logistics_delivery_mode_id) {
		$get_logistics_delivery_mode_by_logistics_delivery_mode_id_arr=$this->get_logistics_delivery_mode_by_logistics_delivery_mode_id($logistics_delivery_mode_id);
		$vendor_id=$get_logistics_delivery_mode_by_logistics_delivery_mode_id_arr["vendor_id"];
		$logistics_id=$get_logistics_delivery_mode_by_logistics_delivery_mode_id_arr["logistics_id"];
		$sql="select * from  logistics_delivery_mode where lower(delivery_mode)=lower('$delivery_mode') and vendor_id='$vendor_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id!='$logistics_delivery_mode_id'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_delivery_mode($delivery_mode,$logistics_id,$vendor_id) {
		$sql="select * from  logistics_delivery_mode where lower(delivery_mode)=lower('$delivery_mode') and vendor_id='$vendor_id' and logistics_id='$logistics_id'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_parcel_category($parcel_type,$vendor_id,$logistics_id) {
		$sql="select * from logistics_parcel_category where parcel_type='$parcel_type' and vendor_id='$vendor_id' and logistics_id='$logistics_id'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function check_duplication_name_parcel_category_edit($parcel_type,$logistics_parcel_category_id){
		
		$get_logistics_parcel_category_by_logistics_parcel_category_id_arr=$this->get_logistics_parcel_category_by_logistics_parcel_category_id($logistics_parcel_category_id);
		$vendor_id=$get_logistics_parcel_category_by_logistics_parcel_category_id_arr["vendor_id"];
		$logistics_id=$get_logistics_parcel_category_by_logistics_parcel_category_id_arr["logistics_id"];
		
		
		$sql="select * from logistics_parcel_category where parcel_type='$parcel_type' and logistics_parcel_category_id!='$logistics_parcel_category_id' and vendor_id='$vendor_id' and logistics_id='$logistics_id'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function get_attribute_info($attribute_id){
		$sql="select * from attribute where attribute_id='$attribute_id'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_attribute_value($attribute_id,$attribute_name){
		$sql="select $attribute_name from attribute where attribute_id='$attribute_id'";
		$query = $this->db->query($sql);
		return $query->row_array()[$attribute_name];
	}
	
	public function attrib_expansion_form1_action($attribute_id,$attribute_name,$attribute_value,$attribute_expansion_name,$attribute_expansion_value,$attribute_sub_options){
		$sql="insert into attribute_expansions set attribute_id='$attribute_id',attribute_name='$attribute_name',attribute_value='$attribute_value',attribute_expansion_name='$attribute_expansion_name',attribute_expansion_value='$attribute_expansion_value',attribute_sub_options='$attribute_sub_options'";
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function get_attribute_sub_options($attribute_id){
		$sql="select attribute_expansions_id,attribute_sub_options from attribute_expansions where attribute_id='$attribute_id'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function attrib_expansion_form2_action($attribute_id,$attribute_expansions_id,$attribute_sub_options){
		$get_attribute_sub_options_arr=explode(",",$attribute_sub_options);
		$total_sub_options_arr=[];
		foreach($get_attribute_sub_options_arr as $k => $v){
			$v=trim($v);
			$v=str_replace(" ","_",$v);
			$get_attribute_sub_options_arr[$k]=trim($v);
			$total_sub_options_arr[]=$this->input->post($get_attribute_sub_options_arr[$k]);
		}
		for($i=0;$i<count($total_sub_options_arr[0]);$i++){
			$sql="insert into attribute_expansions_sub_options set attribute_expansions_id='$attribute_expansions_id',";
			foreach($total_sub_options_arr as $k => $val_arr){
				if(array_key_exists($i,$val_arr)){
					$get_attribute_sub_options_arr[$k];
					$val_arr[$i];
					$j=$k+1;
					if($get_attribute_sub_options_arr[$k]!=""){
					$sql.="attribute_sub_option".$j."='$get_attribute_sub_options_arr[$k]',attribute_sub_value".$j."='$val_arr[$i]',";
					}
				}
			}
			$sql=rtrim($sql,",");
			$query = $this->db->query($sql);
		}
		return $query;
	}
	public function attribute_expansion_processing($params,$fg){
		$attribute_id =$params['attribute_id'];
		
		$sql="select * from attribute_expansions where  attribute_id='$attribute_id'";	
		
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( attribute_expansions.attribute_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR attribute_expansions.attribute_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR attribute_expansions.attribute_value LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR attribute_expansions.attribute_expansion_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR attribute_expansions.attribute_expansion_value LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
			
		}
		
		$sqlTot= $sql;
		$sqlRec= $sql;

		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " order by attribute_expansions.timestamp desc";

		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}	
	}
	
	public function get_attrib_expansions_count($attribute_id){
		$sql="select count(*) as count from attribute_expansions where attribute_id='$attribute_id'";
		$query=$this->db->query($sql);
		return $query->row_array()["count"];
	}
	
	public function get_attribute_expansion_info($attribute_id){
		$sql="select * from attribute_expansions where attribute_id='$attribute_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	public function edit_attrib_expansion_form1_action($attribute_id,$attribute_name,$attribute_value,$attribute_expansion_name,$attribute_expansion_value,$attribute_sub_options){
		$sql="update attribute_expansions set attribute_id='$attribute_id',attribute_value='$attribute_value',attribute_expansion_name='$attribute_expansion_name',attribute_expansion_value='$attribute_expansion_value',attribute_sub_options='$attribute_sub_options' where attribute_id='$attribute_id'";
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function edit_attrib_expansion_form2_action($attribute_id,$attribute_expansions_id,$attribute_sub_options){
		$sql_sub_options="delete from attribute_expansions_sub_options where attribute_expansions_id='$attribute_expansions_id'";
		$query_sub_options = $this->db->query($sql_sub_options);
		
		
		$get_attribute_sub_options_arr=explode(",",$attribute_sub_options);
		$total_sub_options_arr=[];
		foreach($get_attribute_sub_options_arr as $k => $v){
			$v=trim($v);
			$v=str_replace(" ","_",$v);
			$get_attribute_sub_options_arr[$k]=trim($v);
			$total_sub_options_arr[]=$this->input->post($get_attribute_sub_options_arr[$k]);
		}
		for($i=0;$i<count($total_sub_options_arr[0]);$i++){
			$sql="insert into attribute_expansions_sub_options set attribute_expansions_id='$attribute_expansions_id',";
			foreach($total_sub_options_arr as $k => $val_arr){
				if(array_key_exists($i,$val_arr)){
					$get_attribute_sub_options_arr[$k];
					$val_arr[$i];
					$j=$k+1;
					if($get_attribute_sub_options_arr[$k]!=""){
					$sql.="attribute_sub_option".$j."='$get_attribute_sub_options_arr[$k]',attribute_sub_value".$j."='$val_arr[$i]',";
					}
				}
			}
			$sql=rtrim($sql,",");
			$query = $this->db->query($sql);
		}
		return $query;
	}
	
	public function attribute_expansions_sub_options($attribute_expansions_id){
		$sql="select * from attribute_expansions_sub_options where attribute_expansions_id='$attribute_expansions_id'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function delete_attrib_expansions($attribute_id){
		$sql="select * from attribute_expansions where attribute_id='$attribute_id'";
		$query = $this->db->query($sql);
		$attribute_expansions_id=$query->row_array()["attribute_expansions_id"];
		
		$sql="delete from attribute_expansions where attribute_id='$attribute_id'";
		$query = $this->db->query($sql);
		$sql_attribute_expansions_id="delete from attribute_expansions_sub_options where attribute_expansions_id='$attribute_expansions_id'";
		$query_attribute_expansions_id = $this->db->query($sql_attribute_expansions_id);
		return $query;
	}
	public function get_brand_name($brand_id){
		$sql="select brand_name from brands where brand_id='$brand_id'";
		$query = $this->db->query($sql);
		
		return $query->row()->brand_name;
	}
	public function get_all_volumetric_weights($logistics_territory_id,$logistics_delivery_mode_id,$logistics_parcel_category_id,$weight_in_kg,$logistics_id,$arr=''){
		$sql="select volume_in_kg,logistics_price,logistics_weight_id from logistics_weight where weight_in_kg='$weight_in_kg' and logistics_id='$logistics_id' and logistics_weight.logistics_delivery_mode_id='$logistics_delivery_mode_id' and logistics_weight.logistics_parcel_category_id='$logistics_parcel_category_id' and logistics_weight.territory_name_id='$logistics_territory_id' order by timestamp asc";
		$query = $this->db->query($sql);
		if($arr!=''){
			return $query->result_array();
		}else{
			return $query->result();
		}
		
	}
	public function logistics_all_states() {
		$query = $this->db->query("SELECT * from logistics_all_states order by state_id desc");
		return $query -> result();
	}
	public function logistics_all_states_except_some_territory_states($logistics_territory_state_id){
			$get_logistics_territory_states_arr=$this->get_logistics_territory_states($logistics_territory_state_id);
			$logistics_id=$get_logistics_territory_states_arr["logistics_id"];
			
			$get_logistics_territory_states_by_logistics_id_arr=$this->get_logistics_territory_states_by_logistics_id($logistics_id);
			$state_id_list_arr=array();
			$state_id_list_in="";
			$state_id_list_in_pipe="";
			if(!empty($get_logistics_territory_states_by_logistics_id_arr)){
				foreach($get_logistics_territory_states_by_logistics_id_arr as $arr){
					$get_logistics_all_territory_name_data_arr=$this->get_logistics_all_territory_name_data($arr["territory_name_id"]);
					if(strtolower($get_logistics_all_territory_name_data_arr["make_as_local"])!="1"){
						$state_id_list_arr[]=$arr["state_id_list"];
					}
				}
				$state_id_list_in_pipe=implode("|",$state_id_list_arr);
				$state_id_list_in=str_replace("|",",",$state_id_list_in_pipe);
			}
			$sql="select state_id,state_name,circle_id from logistics_all_states where 1=1 ";
			if($state_id_list_in!=""){
				//$sql.=" and state_id not in ($state_id_list_in)";
			}
			$sql.=" order by state_id desc";
			$query=$this->db->query($sql);
			return $query->result();
			
	}
	public function get_logistics_territory_states_by_logistics_id($logistics_id){
		$sql="select * from logistics_territory_states where trim(logistics_id) = '$logistics_id'";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	public function get_logistics_territory_states_cities_by_logistics_id($logistics_id){
		$sql="select * from logistics_territory_states_cities where trim(logistics_id) = '$logistics_id'";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	public function show_available_logistics_territories_for_delivery_modes($logistics_id){
		$sql="select territory_name_id,territory_name from logistics_all_territory_name where trim(logistics_id) = '$logistics_id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_logistics_territories($logistics_id){
		$get_logistics_territory_states_by_logistics_id_arr=$this->get_logistics_territory_states_by_logistics_id($logistics_id);
		$territory_name_id_arr=array();
		$territory_name_id_in="";
		if(!empty($get_logistics_territory_states_by_logistics_id_arr)){
			foreach($get_logistics_territory_states_by_logistics_id_arr as $arr){
				$territory_name_id_arr[]=$arr["territory_name_id"];
			}
			$territory_name_id_in=implode(",",$territory_name_id_arr);
		}
		$sql="select territory_name_id,territory_name from logistics_all_territory_name where trim(logistics_id) = '$logistics_id'";
		if($territory_name_id_in!=""){
			$sql.=" and territory_name_id not in ($territory_name_id_in)";
		}
		$check_make_as_local_logistics_all_territory_name_row=$this->check_make_as_local_logistics_all_territory_name($logistics_id);
		if(empty($check_make_as_local_logistics_all_territory_name_row)){
			$sql.=" and 1=2";
		}
		$query=$this->db->query($sql);
		return $query->result();
		
	}
	public function show_available_logistics_territories_states_cities($logistics_id){
		
		$get_logistics_territory_states_cities_by_logistics_id_arr=$this->get_logistics_territory_states_cities_by_logistics_id($logistics_id);
		$territory_name_id_arr=array();
		$territory_name_id_in="";
		if(!empty($get_logistics_territory_states_cities_by_logistics_id_arr)){
			foreach($get_logistics_territory_states_cities_by_logistics_id_arr as $arr){
				$territory_name_id_arr[]=$arr["territory_name_id"];
			}
			$territory_name_id_in=implode(",",$territory_name_id_arr);
		}
		$sql="select territory_name_id,territory_name from logistics_all_territory_name where trim(logistics_id) = '$logistics_id'";
		if($territory_name_id_in!=""){
			$sql.=" and territory_name_id not in ($territory_name_id_in)";
		}
		
		$query=$this->db->query($sql);
		return $query->result();
		
	}
	public function show_available_logistics_territories_all($logistics_id){
		$sql="select territory_name_id,territory_name from logistics_all_territory_name where trim(logistics_id) = '$logistics_id'";
		$query=$this->db->query($sql);
		return $query->result();
		
	}
	public function show_available_logistics_delivery_modes($logistics_id){
		$sql="select logistics_delivery_mode_id,delivery_mode from logistics_delivery_mode where trim(logistics_id) = '$logistics_id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_logistics_parcel_categories($logistics_id){
		$sql="select logistics_parcel_category_id,parcel_type from logistics_parcel_category where trim(logistics_id) = '$logistics_id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_logistics_territories_states($logistics_id,$territory_name_id){
		
		$get_logistics_territory_states_by_logistics_id_arr=$this->get_logistics_territory_states_by_logistics_id($logistics_id);
		$state_id_list_arr=array();
		$state_id_list_in="";
		$state_id_list_in_pipe="";
		if(!empty($get_logistics_territory_states_by_logistics_id_arr)){
			foreach($get_logistics_territory_states_by_logistics_id_arr as $arr){
				$get_logistics_all_territory_name_data_arr=$this->get_logistics_all_territory_name_data($arr["territory_name_id"]);
				if(strtolower($get_logistics_all_territory_name_data_arr["make_as_local"])!="1"){
					$state_id_list_arr[]=$arr["state_id_list"];
				}
			}
			$state_id_list_in_pipe=implode("|",$state_id_list_arr);
			$state_id_list_in=str_replace("|",",",$state_id_list_in_pipe);
		}
		
		$sql="select state_id,state_name,circle_id from logistics_all_states where 1=1 ";
		if($state_id_list_in!=""){
			//$sql.=" and state_id not in ($state_id_list_in)";
		}
		$sql.=" order by state_id desc";
		$query=$this->db->query($sql);
		return $query->result();
		
	}
	
	public function logistics_pincodes_by_state_ids($to_select_list_states_in){
		$to_select_list_states_arr=explode(",",$to_select_list_states_in);
		$to_select_list_states_in1=implode("','",$to_select_list_states_arr);
		if($to_select_list_states_in1!=''){
			$sql="SELECT * from logistics_all_pincodes where state_id in ('$to_select_list_states_in1')";
		}
		else{
			$sql="SELECT * from logistics_all_pincodes where 1=2";
		}
		$query = $this->db->query($sql);
		return $query -> result();
	}
	public function check_logistics_territory($logistics_id,$territory_name_id){
		$sql="select * from logistics_territory_states where logistics_id='$logistics_id' and territory_name_id='$territory_name_id'";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	public function add_logistics_territory_states($logistics_id,$territory_name_id,$to_select_list_states){
		$to_select_list_states_in="";
		
		if(!empty($to_select_list_states)){
			$to_select_list_states_in=implode("|",$to_select_list_states);
		}
		
		$insertinfo = array("logistics_id" => $logistics_id,"territory_name_id" => $territory_name_id, "state_id_list" => $to_select_list_states_in);
		$flag=$this -> db -> insert("logistics_territory_states", $insertinfo);
		echo $flag;
	}
	
	public function logistics_territory_states_processing($params,$fg){
		$columns = array( 
			0 =>' logistics_territory_states.logistics_territory_state_id'

		);
		$vendor_id =$params['vendor_id'];
		$logistics_id =$params['logistics_id'];
		$territory_name_id =$params['territory_name_id'];
		$where = $sqlTot = $sqlRec = "";
		$sql = "SELECT  logistics_territory_states.*,logistics.logistics_name,logistics.vendor_id FROM logistics_territory_states,logistics Where  logistics.logistics_id=logistics_territory_states.logistics_id ";

		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .="  logistics.logistics_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			
		}

		$where .=" AND ";
		$where .="logistics.vendor_id='$vendor_id'";
		$where .=" AND ";
		$where .="logistics_territory_states.logistics_id='$logistics_id'";
		$where .=" AND ";
		$where .="logistics_territory_states.territory_name_id='$territory_name_id'";
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		if(isset($where) && $where != '') {
			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by logistics.timestamp desc";

		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
			
	}
	
	public function get_logistics_territory_states($logistics_territory_state_id){
		$sql="SELECT  logistics_territory_states.*,logistics.logistics_name,logistics.vendor_id FROM logistics_territory_states,logistics Where  logistics.logistics_id=logistics_territory_states.logistics_id and logistics_territory_states.logistics_territory_state_id='$logistics_territory_state_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_statelist($statelist){
		$statelist_arr=explode("|",$statelist);
		$statelist_in=implode(",",$statelist_arr);
		$sql="select state_id,state_name,circle_id from logistics_all_states where state_id in ($statelist_in) order by state_id desc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function edit_logistics_territory_states($logistics_territory_state_id,$to_select_list_states){
		$to_select_list_states_in="";
		
		if(!empty($to_select_list_states)){
			$to_select_list_states_in=implode("|",$to_select_list_states);
		}
	
		$sql="update logistics_territory_states set state_id_list='$to_select_list_states_in' where logistics_territory_state_id='$logistics_territory_state_id'";
		$query=$this->db->query($sql);
		echo $query;
	}
	
	public function delete_logistics_territory_states_selected($selected_list){
		$sql_delete1="delete from logistics_territory_states where logistics_territory_state_id in ($selected_list)";
		$flag1=$this->db->query($sql_delete1);
		

		if($flag1==true){
			return true;
		}else{
			return false;
		}
	}
	public function get_logistics_territory_states_cities_by_logistics_id_territory_name_id($territory_name_id,$logistics_id){
		$sql="select * from logistics_territory_states_cities where trim(logistics_id) = '$logistics_id' and territory_name_id='$territory_name_id'";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	public function show_available_logistics_territories_states_by_territory($territory_name_id,$logistics_id){
		$sql="select state_id_list from logistics_territory_states where territory_name_id='$territory_name_id' and logistics_id='$logistics_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
		
	}
	public function show_available_logistics_territories_states_by_territory_all($territory_name_id,$logistics_id){
		$sql="select state_id_list from logistics_territory_states where territory_name_id='$territory_name_id' and logistics_id='$logistics_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
		
	}
	public function get_state_name($state_id){
		$sql="select state_id,state_name,circle_id from logistics_all_states where state_id='$state_id' order by state_id desc";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	
	public function show_available_logistics_territories_states_cities_by_state($state_id,$logistics_id,$territory_name_id){
		
		$get_logistics_territory_states_cities_by_logistics_id_arr=$this->get_logistics_territory_states_cities_by_logistics_id($logistics_id);
			$city_id_list_arr=array();
			$city_id_list_in="";
			$city_id_list_in_pipe="";
			
			if(!empty($get_logistics_territory_states_cities_by_logistics_id_arr)){
				foreach($get_logistics_territory_states_cities_by_logistics_id_arr as $arr){
					$get_logistics_all_territory_name_data_arr=$this->get_logistics_all_territory_name_data($arr["territory_name_id"]);
					if(strtolower($get_logistics_all_territory_name_data_arr["make_as_local"])=="1"){
						$city_id_list_arr[]=$arr["city_id_list"];
					}
				}
				
				$city_id_list_in_pipe=implode("|",$city_id_list_arr);
				$city_id_list_in=str_replace("|",",",$city_id_list_in_pipe);
			}
			
			
			
		$sql="select pin,pincode_id,city from logistics_all_pincodes where state_id='$state_id'";
		if($city_id_list_in!=""){
				$sql.=" and pincode_id not in ($city_id_list_in)";
			}
			$sql.=" order by pincode_id desc";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	public function add_logistics_territory_states_cities($logistics_id,$territory_name_id,$state_id,$to_select_list_cities){
		$to_select_list_cities_in="";
		
		if(!empty($to_select_list_cities)){
			$to_select_list_cities_in=implode("|",$to_select_list_cities);
			
			foreach($to_select_list_cities as $pincode_id){
				$sql="select * from logistics_all_pincodes where pincode_id='$pincode_id'";
				$query=$this->db->query($sql);
				$logistics_all_pincodes_arr=$query->row_array();
				$pin=$logistics_all_pincodes_arr["pin"];
				$city=$logistics_all_pincodes_arr["city"];
				$pincode=$logistics_all_pincodes_arr["pincodes"];
				$sql_service_select="select * from logistics_service where pincode_id='$pincode_id' and logistics_id='$logistics_id' and territory_name_id='$territory_name_id' and state_id='$state_id'";
				$query_service_select=$this->db->query($sql_service_select);
				$logistics_service_arr=$query_service_select->row_array();
				if(!empty($logistics_service_arr)){
					$sql_service_update="update logistics_service set logistics_id='$logistics_id',state_id='$state_id',territory_name_id='$territory_name_id',city='$city',pincode='$pincode',pin='$pin' where pincode_id='$pincode_id'";
					$query_service_update=$this->db->query($sql_service_update);
				}
				else{
					$sql_service_update="insert into logistics_service set logistics_id='$logistics_id',state_id='$state_id',territory_name_id='$territory_name_id',city='$city',pincode_id='$pincode_id',pincode='$pincode',pin='$pin'";
					$query_service_update=$this->db->query($sql_service_update);
				}
			}
			
		}
		
		$insertinfo = array("logistics_id" => $logistics_id,"territory_name_id" => $territory_name_id, "city_id_list" => $to_select_list_cities_in,"state_id"=>$state_id);
		$flag=$this -> db -> insert("logistics_territory_states_cities", $insertinfo);
		
		
		
		
		echo $flag;
	}
	
	public function check_logistics_territory_state($logistics_id,$territory_name_id,$state_id){
		$sql="select * from logistics_territory_states_cities where logistics_id='$logistics_id' and territory_name_id='$territory_name_id' and state_id='$state_id'";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	public function logistics_territory_states_cities_processing($params,$fg){
		$columns = array( 
			0 =>' logistics_territory_states_cities.logistics_territory_state_city_id'

		);
		$vendor_id =$params['vendor_id'];
		$logistics_id =$params['logistics_id'];
		$territory_name_id =$params['territory_name_id'];
		$state_id =$params['state_id'];
		$where = $sqlTot = $sqlRec = "";
		$sql = "SELECT  logistics_territory_states_cities.*,logistics.logistics_name,logistics.vendor_id FROM logistics_territory_states_cities,logistics Where  logistics.logistics_id=logistics_territory_states_cities.logistics_id ";

		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" logistics.logistics_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
		}

		$where .=" AND ";
		$where .="logistics.vendor_id='$vendor_id'";
		$where .=" AND ";
		$where .="logistics_territory_states_cities.logistics_id='$logistics_id'";
		$where .=" AND ";
		$where .="logistics_territory_states_cities.territory_name_id='$territory_name_id'";
		$where .=" AND ";
		$where .="logistics_territory_states_cities.state_id='$state_id'";
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		if(isset($where) && $where != '') {
			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by logistics.timestamp desc";

		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
			
	}
	public function get_logistics_territory_states_cities($logistics_territory_state_city_id){
		$sql="SELECT  logistics_territory_states_cities.*,logistics.logistics_name,logistics.vendor_id FROM logistics_territory_states_cities,logistics Where  logistics.logistics_id=logistics_territory_states_cities.logistics_id and logistics_territory_states_cities.logistics_territory_state_city_id='$logistics_territory_state_city_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	public function get_logistics_territory_states_cities_not($logistics_territory_state_city_id){
		$sql="SELECT  logistics_territory_states_cities.*,logistics.logistics_name,logistics.vendor_id FROM logistics_territory_states_cities,logistics Where  logistics.logistics_id=logistics_territory_states_cities.logistics_id and logistics_territory_states_cities.logistics_territory_state_city_id!='$logistics_territory_state_city_id'";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	public function get_citylist($citylist){
		$citylist_arr=explode("|",$citylist);
		$citylist_in=implode(",",$citylist_arr);
		if(!empty($citylist_in)){
			$sql="select pincode_id,pin,city from logistics_all_pincodes where pincode_id in ($citylist_in) order by pincode_id desc";
		
			$query=$this->db->query($sql);
			return $query->result();
		}else{
			return '';
		}
	}
	
	public function logistics_cities_by_state_id($state_id){
		$query = $this->db->query("SELECT * from logistics_all_pincodes where state_id='$state_id' order by pincode_id desc");
		return $query -> result();
	}
	
	public function logistics_cities_by_state_id_except_some_territory_cities($state_id,$logistics_territory_state_city_id){
		$city_id_list_in_not_in="";
		$city_id_list_not_arr=array();
		$city_id_list_in_pipe_not="";
		$city_id_list_not_in="";
		$get_logistics_territory_states_cities_not_arr=$this->get_logistics_territory_states_cities_not($logistics_territory_state_city_id);
		if(!empty($get_logistics_territory_states_cities_not_arr)){
			foreach($get_logistics_territory_states_cities_not_arr as $varr){
				if($varr["city_id_list"]!=''){
					$city_id_list_not_arr[]=$varr["city_id_list"];
				}
			}
			$city_id_list_not_arr=array_filter($city_id_list_not_arr);
			$city_id_list_in_pipe_not=implode("|",$city_id_list_not_arr);
		}
		
				
		$get_logistics_territory_states_cities_arr=$this->get_logistics_territory_states_cities($logistics_territory_state_city_id);
		$logistics_id=$get_logistics_territory_states_cities_arr["logistics_id"];
		
		$get_logistics_territory_states_cities_by_logistics_id_arr=$this->get_logistics_territory_states_cities_by_logistics_id($logistics_id);
			$city_id_list_arr=array();
			$city_id_list_in="";
			$city_id_list_in_pipe="";
			
			if(!empty($get_logistics_territory_states_cities_by_logistics_id_arr)){
				foreach($get_logistics_territory_states_cities_by_logistics_id_arr as $arr){
					$get_logistics_all_territory_name_data_arr=$this->get_logistics_all_territory_name_data($arr["territory_name_id"]);
					if(strtolower($get_logistics_all_territory_name_data_arr["make_as_local"])=="1"){
						if($arr["city_id_list"]!=''){
							$city_id_list_arr[]=$arr["city_id_list"];
						}
					}
				}
				$city_id_list_arr=array_filter($city_id_list_arr);
				$city_id_list_in_pipe=implode("|",$city_id_list_arr);
				
			}
			if($city_id_list_in_pipe_not!=""){
				$city_id_list_in_not_in=str_replace("|",",",$city_id_list_in_pipe.",".$city_id_list_in_pipe_not);
			}
			else{
				$city_id_list_in_not_in=str_replace("|",",",$city_id_list_in_pipe);
			}
			$sql="select pincode_id,pin,city from logistics_all_pincodes where state_id='$state_id'";
			if($city_id_list_in_not_in!=""){
				$sql.=" and pincode_id not in ($city_id_list_in_not_in)";
			}
			$sql.=" order by pincode_id desc";
			
		$query=$this->db->query($sql);
		return $query->result();
			
	}
	public function edit_logistics_territory_states_cities($logistics_territory_state_city_id,$to_select_list_cities,$select_list_division=[]){
		
		$get_logistics_territory_states_cities_arr=$this->get_logistics_territory_states_cities($logistics_territory_state_city_id);
		$logistics_id=$get_logistics_territory_states_cities_arr["logistics_id"];
		$territory_name_id=$get_logistics_territory_states_cities_arr["territory_name_id"];
		$state_id=$get_logistics_territory_states_cities_arr["state_id"];
		$to_select_list_cities_in="";

		if(!empty($select_list_division)){
			
			$sql_service_delete="delete from logistics_service where logistics_id='$logistics_id' and state_id='$state_id' and territory_name_id='$territory_name_id'";
			$query_service_delete=$this->db->query($sql_service_delete);
			
		}
		$division_list='';
		$division_pincodes='';

		if(!empty($select_list_division)){

			//$select_list_division_arr=implode("|",$select_list_division);

			/* Select all pincodes under each division */
			$pincode_arr=[];
			if(!empty($select_list_division)){
				foreach($select_list_division as $divs_id){

					//echo $divs_id.'xxx';
					$sql="select * from logistics_all_pincodes where imp_info_json like '%$divs_id%'";
					$result=$this->db->query($sql);
					$obj=$result->result();

					//print_r($obj);

					if(!empty($obj)){
						foreach($obj as $ob){
							//print_r($ob);
							$temp_arr=array();
							$nn=json_decode($ob->imp_info_json);

							//print_r($nn);//under one division

							$division_name='';$first_3='';$first_3_arr=[];$city='';
							foreach($nn as $n){
								if($n->Division_ID==$divs_id){
									$pincode_arr[]=$n->Pincode;
									$temp_arr[]=$n->Pincode;
									$division_name=$n->Division_Name;
									$division_id=$n->Division_ID;
									$city=$n->city;
									$first_3=substr($n->Pincode, 0, 3);
									$first_3_arr[]=$first_3;
								}
							}
							$pin='';
							if(!empty($first_3_arr)){
								$pin_arr=array_unique($first_3_arr);
								$pin=implode('|',$pin_arr);
							}
							$div_pincodes=implode('|',$temp_arr);

							//print_r($temp_arr);

							/* get city auto increment id */
							$sql="select * from logistics_all_pincodes where city='$city'";
							$query=$this->db->query($sql);
							$c_arr=$query->row_array();
							//$city=$c_arr["city"];
							$pincode_id=$c_arr["pincode_id"];
							/* get city auto increment id */
				
							$sql_service_update="insert into logistics_service set logistics_id='$logistics_id',state_id='$state_id',territory_name_id='$territory_name_id',division_name='$division_name',division_id='$division_id',pincode_id='$pincode_id',city='$city',pincode='$div_pincodes',pin='$pin'";

							//pincode - instead of city pincodes, it is taken based on divisions

							$query=$this->db->query($sql_service_update);

							//echo "result= ". $query."||";
								
						}
					}
					
				}
				
			}

			//print_r($pincode_arr);

			$division_pincodes=implode('|',$pincode_arr);
			$division_list=implode('|',$select_list_division);


			/* Select all pincodes under each division */

		}
		
		//exit;

		
		
		if(!empty($to_select_list_cities)){
			
			$to_select_list_cities_in=implode("|",$to_select_list_cities);
			
			foreach($to_select_list_cities as $pincode_id){
				$sql="select * from logistics_all_pincodes where pincode_id='$pincode_id'";
				$query=$this->db->query($sql);
				$logistics_all_pincodes_arr=$query->row_array();
				$city=$logistics_all_pincodes_arr["city"];
				$pincode=$logistics_all_pincodes_arr["pincodes"];
				$pin=$logistics_all_pincodes_arr["pin"];
				
				/* commented because of division concept - hurbz */

				//$sql_service_update="insert into logistics_service set logistics_id='$logistics_id',state_id='$state_id',territory_name_id='$territory_name_id',city='$city',pincode_id='$pincode_id',pincode='$pincode',pin='$pin'";

				//$query_service_update=$this->db->query($sql_service_update);
			}
			
		}
		
		$to_select_list_cities_in="";
		
		if(!empty($to_select_list_cities)){
			$to_select_list_cities_in=implode("|",$to_select_list_cities);
		}
	
		$sql="update logistics_territory_states_cities set city_id_list='$to_select_list_cities_in',division_id_list='$division_list',division_pincodes='$division_pincodes' where logistics_territory_state_city_id='$logistics_territory_state_city_id' ";
		$query=$this->db->query($sql);
		echo $query;
		
	}
	public function delete_logistics_territory_states_cities_selected($selected_list){
		$sql_select1="select * from logistics_territory_states_cities where logistics_territory_state_city_id in ($selected_list)";
		$query_select1=$this->db->query($sql_select1);
		$result_select1_arr=$query_select1->result_array();
		if(!empty($result_select1_arr)){
			foreach($result_select1_arr as $varr){
				$logistics_id=$varr["logistics_id"];
				$territory_name_id=$varr["territory_name_id"];
				$state_id=$varr["state_id"];
				$sql_delete_services="delete from logistics_service where logistics_id='$logistics_id' and territory_name_id='$territory_name_id' and state_id='$state_id'";
				$flag_delete_services=$this->db->query($sql_delete_services);
			}
		}
		
		$sql_delete1="delete from logistics_territory_states_cities where logistics_territory_state_city_id in ($selected_list)";
		$flag1=$this->db->query($sql_delete1);
		

		if($flag1==true){
			return true;
		}else{
			return false;
		}
	}
	
	public function show_available_logistics_territories_states_cities_by_state_id($territory_name_id,$logistics_id,$state_id){
		$sql="select city_id_list from logistics_territory_states_cities where territory_name_id='$territory_name_id' and logistics_id='$logistics_id' and state_id='$state_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
		
	}
	public function show_available_logistics_territories_states_divisions_by_state_id($territory_name_id,$logistics_id,$state_id){
		$sql="select division_id_list from logistics_territory_states_cities where territory_name_id='$territory_name_id' and logistics_id='$logistics_id' and state_id='$state_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
		
	}
	public function logistics_territory_states_cities_pincodes_processing($params,$fg){
		$columns = array( 
			0 =>' logistics_service.logistics_service_id'

		);
		$vendor_id =$params['vendor_id'];
		$logistics_id =$params['logistics_id'];
		$territory_name_id =$params['territory_name_id'];
		$state_id =$params['state_id'];
		$city_pin =$params['city_pin'];
		$where = $sqlTot = $sqlRec = "";
		$sql = "SELECT  logistics_service.*,logistics.logistics_name,logistics.vendor_id FROM logistics_service,logistics Where  logistics.logistics_id=logistics_service.logistics_id ";

		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" logistics.logistics_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
		}

		$where .=" AND ";
		$where .="logistics.vendor_id='$vendor_id'";
		$where .=" AND ";
		$where .="logistics_service.pincode_id='$city_pin'";
		$where .=" AND ";
		$where .="logistics_service.logistics_id='$logistics_id'";
		$where .=" AND ";
		$where .="logistics_service.territory_name_id='$territory_name_id'";
		$where .=" AND ";
		$where .="logistics_service.state_id='$state_id'";
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		if(isset($where) && $where != '') {
			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		//$sqlRec .=  "order by logistics.timestamp desc";

		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
			
	}
	
	public function get_logistics_territory_states_cities_pincodes($logistics_service_id){
		$sql="SELECT  logistics_service.*,logistics.logistics_name,logistics.vendor_id FROM logistics_service,logistics Where  logistics.logistics_id=logistics_service.logistics_id and logistics_service_id='$logistics_service_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	
	public function logistics_cities_pincodes_by_city_pin($city_pin){
		$query = $this->db->query("SELECT pincodes from logistics_all_pincodes where pin='$city_pin' order by pincode_id desc");
		return $query -> row();
	}
	
	public function get_pincodelist($logistics_service_id){
		$sql="select pincode from logistics_service where logistics_service_id='$logistics_service_id'";
		$query=$this->db->query($sql);
		return $query->row();
	}
	public function get_city_name($city_pin){
		$sql="select city from logistics_all_pincodes where pin='$city_pin'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	
	public function edit_logistics_territory_states_cities_pincodes($logistics_service_id,$to_select_list_pincodes){
		$to_select_list_pincodes_in="";
		if(!empty($to_select_list_pincodes)){
			$to_select_list_pincodes_in=implode("|",$to_select_list_pincodes);
		}
		$sql="update logistics_service set pincode='$to_select_list_pincodes_in' where logistics_service_id='$logistics_service_id'";
		$query=$this->db->query($sql);
		echo $query;
	}
	
	public function logistics_territory_weights_by_logistics_id($logistics_id){
		$sql="select distinct territory_name_id from logistics_weight where trim(logistics_id) = '$logistics_id'";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	public function show_available_logistics_territories_for_weight($logistics_id){
		$logistics_territory_weights_by_logistics_id_arr=$this->logistics_territory_weights_by_logistics_id($logistics_id);
		$territory_name_id_arr=array();
		$territory_name_id_in="";
		if(!empty($logistics_territory_weights_by_logistics_id_arr)){
			foreach($logistics_territory_weights_by_logistics_id_arr as $arr){
				$territory_name_id_arr[]=$arr["territory_name_id"];
			}
			$territory_name_id_in=implode(",",$territory_name_id_arr);
		}
		$sql="select territory_name_id,territory_name from logistics_all_territory_name where trim(logistics_id) = '$logistics_id'";
		if($territory_name_id_in!=""){
			$sql.=" and territory_name_id not in ($territory_name_id_in)";
		}
		
		
		$query=$this->db->query($sql);
		return $query->result();
		
	}
	
	public function get_all_logistics($vendor_id){
		$sql="select * from logistics where vendor_id='$vendor_id'";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	
	public function show_logistics_edit_sort_order($edit_logistics_priority,$vendor_id){
		$sql="select logistics_id,logistics_name,logistics_priority from logistics where logistics_priority!='$edit_logistics_priority' and vendor_id='$vendor_id' and logistics_priority!=0";
		$query=$this->db->query($sql);
		return $query->result_array();
	}
	public function inventory_processing_all($params,$fg){
	
		$columns = array( 
			0 =>'inventory.id', 
			1 => 'inventory.id',
			2 => 'inventory.timestamp',
			3 => 'inventory.timestamp'
		);
			
		$user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");
					
		
		$active =$params['active'];
		$sku_status =isset($params['sku_status']) ? $params['sku_status'] : '';
		
		$where = $sqlTot = $sqlRec = "";
		$sql = "select *,inventory.approval_status as publish_status,inventory.status_updated_on as status_on from inventory ";
		
				
		$sql.= ' LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`'
		. ' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`'
		.' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`'
		.' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`'
		.' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id` ';

		if($user_type=='vendor'){
			$sql.=' LEFT JOIN `vendor_catalog_inventory` ON `vendor_catalog_inventory`.`vendor_catalog_id` = `inventory`.`inv_vendor_catalog_id`';
		}
		$sql.= " where  inventory.active='$active' ";
				
		if($user_type=='vendor'){
			$where .=" and ";
			$where .=" vendor_catalog_inventory.vendor_id='$a_id' ";
		}
				
		if(!empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" (inventory.sku_id LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
						$where .=" OR inventory.attribute_1_value LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR inventory.attribute_2_value LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
						$where .="  OR inventory.sku_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
						$where .="  OR products.product_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR inventory.selling_price LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}

		if($user_type=="vendor"){
			
			$where .=" AND ";
			$where .=" ( inventory.vendor_id ='$a_id') ";
		
			$where .=" AND ";
			$where .=' ( inventory.vendor_id IS NULL OR inventory.vendor_id IS NOT NULL) ';

			if($sku_status=='selected'){
				$where .=" AND ";
				$where .=" ( inventory.approval_status ='1') ";
			}
			if($sku_status=='unselected'){
				$where .=" AND ";
				$where .=" ( inventory.approval_status ='2') ";
			}
		
	}
				
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		if(isset($where) && $where != '') {
			$sqlTot .= $where;
			$sqlRec .= $where;
		}
				$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";

		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
			
	}
	public function show_available_skus($brand_id,$active){
		$active_str=($active!=2)? "and inventory.active='$active' " :"";
		$active_str.=($active!=2)? " and products.active='$active' " :"";
		$sql="select inventory.id,sku_id,sku_name,attribute_1,attribute_1_value,attribute_2,attribute_2_value from  inventory 
                      LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`
                      where trim(products.brand_id) = '$brand_id' $active_str ";
                
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_skus_by_product_id($product_id,$active){
		$active_str=($active!=2)? "and inventory.active='$active' " :"";
		$active_str.=($active!=2)? " and products.active='$active' " :"";
		$sql="select inventory.id,sku_id,sku_name,attribute_1,attribute_1_value,attribute_2,attribute_2_value from  inventory 
                      LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`
                      where trim(products.product_id) = '$product_id' $active_str ";
                
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_skus_by_subcat($subcat_id,$active){
		$active_str=($active!=2)? "and inventory.active='$active' " :"";
		$active_str.=($active!=2)? " and products.active='$active' " :"";
		$sql="select inventory.id,sku_id,sku_name,attribute_1,attribute_1_value,attribute_2,attribute_2_value from  inventory 
                      LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`
                      where trim(products.subcat_id) = '$subcat_id' $active_str ";
                
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function get_inventory_list_details_all($inventory_list_id_str){
		$sql="select id,sku_id,attribute_1_value,attribute_2_value,selling_price,max_selling_price from inventory where id in ($inventory_list_id_str)";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function add_tagged_invs($data_posted){
		$addon_single_or_multiple_tagged_inventories_in_frontend=$data_posted['choose_addon_single_or_multiple_tagged_inventories_in_frontend'];
		$inv_id=$data_posted['tagged_main_inventory_id'];
		
		//////////////////////////////////
		$tagged_inventory_mrps_arr=array();
		$tagged_inventory_offerprices_arr=array();
		$tagged_inventory_discounts_arr=array();
		$tagged_inventory_costprice_arr=array();
		$tagged_inventory_profitability_arr=array();

		$to_select_list=$data_posted['to_select_list'];
		
		if(isset($data_posted["taggedinvoffers"])){

			for($i=0;$i<count($data_posted['to_select_list']);$i++){
				$tagged_inventory_mrps_arr[]=$data_posted["taggedinvoffers"]["mrp_".$data_posted['to_select_list'][$i]];
				if($data_posted["taggedinvoffers"]["discount_".$data_posted['to_select_list'][$i]]==0){
					$tagged_inventory_offerprices_arr[]=$data_posted["taggedinvoffers"]["mrp_".$data_posted['to_select_list'][$i]];
				}
				else{
					$tagged_inventory_offerprices_arr[]=$data_posted["taggedinvoffers"]["offerprice_".$data_posted['to_select_list'][$i]];
				}
				$tagged_inventory_discounts_arr[]=$data_posted["taggedinvoffers"]["discount_".$data_posted['to_select_list'][$i]];
				$tagged_inventory_costprice_arr[]=$data_posted["taggedinvoffers"]["costprice_".$data_posted['to_select_list'][$i]];
				$tagged_inventory_profitability_arr[]=$data_posted["taggedinvoffers"]["profitability_".$data_posted['to_select_list'][$i]];
			}
		}
		else{
			for($i=0;$i<count($to_select_list);$i++){

				$inventory_list_details_arr=$this->get_inventory_list_details_all($data_posted['to_select_list'][$i]);

				if(!empty($inventory_list_details_arr)){
					foreach($inventory_list_details_arr as $arr){
						$tagged_inventory_mrps_arr[]=$arr["max_selling_price"];
						$tagged_inventory_offerprices_arr[]=$arr["selling_price"];
						$tagged_inventory_discounts_arr[]=0;
						$tagged_inventory_costprice_arr[]=0;
						$tagged_inventory_profitability_arr[]=0;
					}
				}else{
					unset($data_posted['to_select_list'][$i]);
				}
			}
		}


		$tagged_inventory_ids=implode(',',$data_posted['to_select_list']);
		$tagged_inventory_mrps=implode(',',$tagged_inventory_mrps_arr);
		$tagged_inventory_offerprices=implode(',',$tagged_inventory_offerprices_arr);
		$tagged_inventory_discounts=implode(',',$tagged_inventory_discounts_arr);
		$tagged_inventory_costprices=implode(',',$tagged_inventory_costprice_arr);
		$tagged_inventory_profitabilities=implode(',',$tagged_inventory_profitability_arr);
		
		
		///////////////////////////////

		$select_array=array("tagged_main_inventory_id" => $inv_id,"tagged_pcat_id" => $data_posted['sku_pcat_id'], "tagged_cat_id" => $data_posted['sku_cat_id'],"tagged_subcat_id" => $data_posted['sku_subcat_id'],"tagged_brand_id" => $data_posted['sku_brand_id'],"tagged_product_id" => $data_posted['sku_product_id'],"tagged_type"=>"internal");

		$insertinfo = array("tagged_main_inventory_id" => $inv_id,"tagged_inventory_ids" => $tagged_inventory_ids,"tagged_inventory_mrps"=>$tagged_inventory_mrps,"tagged_inventory_offerprices"=>$tagged_inventory_offerprices,"tagged_inventory_discounts"=>$tagged_inventory_discounts,"tagged_inventory_costprices"=>$tagged_inventory_costprices,"tagged_inventory_profitabilities"=>$tagged_inventory_profitabilities, "tagged_pcat_id" => $data_posted['sku_pcat_id'], "tagged_cat_id" => $data_posted['sku_cat_id'],"tagged_subcat_id" => $data_posted['sku_subcat_id'],"tagged_brand_id" => $data_posted['sku_brand_id'],"tagged_product_id" => $data_posted['sku_product_id'],"tagged_timestamp"=>date('Y-m-d H:i:s'),"addon_single_or_multiple_tagged_inventories_in_frontend"=>$addon_single_or_multiple_tagged_inventories_in_frontend,"tagged_type"=>"internal");
		
		
		//////////////////////////////////////////////////////
		$sql_addon_single_or_multiple_tagged_inventories_in_frontend="update tagged_inventory set addon_single_or_multiple_tagged_inventories_in_frontend='$addon_single_or_multiple_tagged_inventories_in_frontend' where tagged_main_inventory_id='$inv_id'";

		$this->db->query($sql_addon_single_or_multiple_tagged_inventories_in_frontend);
		////////////////////////////////////////////////

		$query=$this->db->select("tagged_id")->from("tagged_inventory")->where($select_array)->get();
		$res=$query->row();
		//$query->num_rows()>0
		if(0){
			$tagged_id=$res->tagged_id;
			$this->db->where(array("tagged_id"=>$tagged_id));

			$query=$this->db->update("tagged_inventory",$insertinfo);	
			
			if($query==true){
				return true;
			}else{
				return false;
			}
			
		}else{
		
			$flag=$this -> db -> insert("tagged_inventory", $insertinfo);
			return $flag;
		}

	}
	
	public function delete_tagged_invs($tagged_id,$tagged_type){
        
        if($tagged_id!='') {
            $this->db->where(array('tagged_id'=> $tagged_id,"tagged_type"=>$tagged_type));
            $this->db->delete('tagged_inventory');
        }

        if($this->db->affected_rows() > 0){
                return true;
        }else{
                return false;
        }
    }
	public function get_already_selected_skus($product_id,$inv_id){
		$query = $this->db->query("SELECT GROUP_CONCAT(tagged_inventory_ids SEPARATOR ',') as all_invs  FROM `tagged_inventory` where tagged_main_inventory_id='$inv_id' and tagged_product_id='$product_id'");
		$res=$query -> row();
		$arr=array();
		if($query->num_rows()>0){
			//print_r($res);
			$inv_ids=$res->all_invs;
			$arr=explode(',',$inv_ids);
			$arr=array_filter($arr);
			$arr=array_unique($arr);
			
		}

		return $arr;
	}
	public function get_admin_settings(){
		$query = $this->db->query("SELECT * FROM `admin_settings`");
		$res=$query -> row();
		return $res;
	}
	public function get_marquee(){
		$query = $this->db->query("SELECT * FROM `marquee`");
		$res=$query -> result_array();
		return $res;
	}
	public function admin_settings_submit($post_arr){
		$this->db->where('adm_settings_id', 1);
		//print_r($post_arr);

		$arr=array(
			'adm_addon_product_allowed'=>$post_arr['adm_addon_product_allowed'],
			'adm_combo_pack_min_items'=>$post_arr['adm_combo_pack_min_items'],
			'adm_combo_pack_discount'=>$post_arr['adm_combo_pack_discount'],
			'adm_combo_pack_shipping_charge'=>$post_arr['adm_combo_pack_shipping_charge'],
			'adm_combo_pack_status'=>$post_arr['adm_combo_pack_status'],
			'adm_request_for_quotations_min_items'=>$post_arr['adm_request_for_quotations_min_items'],
			'adm_request_for_quotations_max_items'=>$post_arr['adm_request_for_quotations_max_items']
		);
		$flag=$this->db->update('admin_settings', $arr);
		//exit;
		//echo '<pre>';

		//print_r($post_arr);
		$id_arr=$post_arr['id'];
		$text_arr=$post_arr['anouncement'];
		$status_arr=$post_arr['status'];

		if(!empty($id_arr)){
			foreach($id_arr as $key=>$val){
					$id=$val;
					$update_arr=array('text'=>$text_arr[$val],'status'=>$status_arr[$val]);
					//print_r($update_arr);
					$this->db->where('id',$id);
					$flag=$this->db->update('marquee', $update_arr);
		
			}
		}

		//exit;
		return $flag;
		
	}
	
	
	
	public function get_inventory_list_details($inventory_list_id_str){
		$sql="select id,sku_id,attribute_1_value,attribute_2_value,max_selling_price from inventory where id in ($inventory_list_id_str) and active=1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function get_inventory_list_details_single_all($inventory_list_id_str){
		$sql="select id,sku_id,attribute_1_value,attribute_2_value,max_selling_price,cost_price from inventory where id in ($inventory_list_id_str)";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	/*** show all category chain etc starts */
	public function show_all_categories($pcat_id){
		echo $sql="select cat_id,cat_name,active from category where trim(pcat_id) = '$pcat_id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_all_subcategories($cat_id){
		$sql="select subcat_id,subcat_name,active from subcategory where trim(cat_id) = trim('$cat_id')";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_all_brands($subcat_id){
		$sql="select brand_id,brand_name,active from brands where trim(subcat_id) = '$subcat_id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_all_products($brand_id){
		$sql="select product_id,product_name,active from  products where trim(brand_id) = '$brand_id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_all_skus_by_product_id($product_id,$inv_type=''){
		
		$str='';
		if($inv_type!=''){
			/* not to show main products */
			$str.=" and inventory_type!='1' ";
		}
		$sql="select inventory.id,sku_id,sku_name,attribute_1,attribute_1_value,attribute_2,attribute_2_value,inventory.active from  inventory 
                      LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`
                      where trim(products.product_id) = '$product_id' $str";
                
		$query=$this->db->query($sql);
		return $query->result();
	}
	/*** show all category chain etc ends */
	/*** external inventories starts ***/
	
	
	
	public function check_existing_skus_in_db_info($sku_id){
		$sql="select sku_id from inventory where trim(sku_id) = '$sku_id'";
		$query=$this->db->query($sql);
		$inventory_sku_id_arr=$query->row_array();
		
		if(empty($inventory_sku_id_arr) ){
			return "not_exists";
		}
		else{
			return "exists";
		}
	}
	
	
	
	public function product_vendors_processing($params,$fg){
		
		$columns = array( 
			0 =>'product_vendors.vendor_id', 
			1 => 'product_vendors.name',
			2 => 'product_vendors.email',
			3 => 'product_vendors.mobile',
			4 => 'product_vendors.timestamp'
		);
		$user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");
		$active =$params['active'];
		
		$where = $sqlTot = $sqlRec = "";
                
                $sql = "select product_vendors.* from product_vendors where product_vendors.active='$active' ";
		
		if(!empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( product_vendors.name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .="  or product_vendors.email LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .="  or product_vendors.mobile LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
                        $where .= " )";
		}
		
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		//$sqlRec .=  "order by category.timestamp desc";
		//echo $sqlRec;
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}		
	}

	public function vendors_processing($params,$fg){
		
		$columns = array( 
			0 =>'vendors.vendor_id', 
			1 => 'vendors.name',
			2 => 'vendors.email',
			3 => 'vendors.mobile',
			4 => 'vendors.timestamp'
		);
		$user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");
		$active =$params['active'];
		
		$where = $sqlTot = $sqlRec = "";
                
                $sql = "select vendors.* from vendors where vendors.active='$active' ";
		
		if(!empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( vendors.name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .="  or vendors.email LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .="  or vendors.mobile LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
                        $where .= " )";
		}
		
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		//$sqlRec .=  "order by category.timestamp desc";
		//echo $sqlRec;
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}		
	}
	public function vendor_change_status($vendor_id,$status){
		$arr=[];
		$arr['approval_status']=$status;
		$arr['status_updated_on']=date('Y-m-d H:i:s');
		if($vendor_id!='') {
			$this->db->where('vendor_id', $vendor_id);
			$this->db->update('product_vendors', $arr);
		}
		if($this->db->affected_rows() > 0){
				return true;
		}else{ 
				return false;
		}
	}
	public function brands_change_status($brand_id,$vendor_id,$status){
		$arr=[];
		$arr['approval_status']=$status;
		$arr['status_updated_on']=date('Y-m-d H:i:s');
		if($vendor_id!='') {
			$this->db->where('vendor_id', $vendor_id);
			$this->db->where('brand_id', $brand_id);
			$this->db->update('brands', $arr);
		}
		if($this->db->affected_rows() > 0){
				return true;
		}else{ 
				return false;
		}
	}
	public function inventory_change_status($inventory_id,$vendor_id,$status,$comments){
		$arr=[];
		$arr['approval_status']=$status;
		$arr['rejection_comments']=$comments;
		$arr['status_updated_on']=date('Y-m-d H:i:s');
		
		if($vendor_id!='') {
			//$this->db->where('vendor_id', $vendor_id);
			$this->db->where('id', $inventory_id);
			$this->db->update('inventory', $arr);
		}
		if($this->db->affected_rows() > 0){
				return true;
		}else{ 
				return false;
		}
	}
	public function delete_catalog_notification($id){
		$arr=["notification_read_status"=>'1',"notification_read_on"=>date('Y-m-d H:i:s')];
		$this->db->where('vendor_catalog_id', $id);
		$flag=$this->db->update('vendor_catalog_inventory', $arr);
		return $flag;
	}
	public function vendor_inventory_change_status($inventory_id,$vendor_id,$status,$comments){
		$arr=[];
		$arr['approval_status']=$status;
		$arr['rejection_comments']=$comments;
		$arr['status_updated_on']=date('Y-m-d H:i:s');
		
		//echo $vendor_id;
		//echo $inventory_id;
		if($vendor_id!='') {
			$this->db->where('vendor_catalog_id', $inventory_id);
			//$this->db->where('vendor_id', $vendor_id);
			$this->db->update('vendor_catalog_inventory', $arr);
		}
		if($this->db->affected_rows() > 0){
				return true;
		}else{ 
				return false;
		}
	}
	public function vendor_update_password($password,$vendor_id,$new_password){
		
		$sql="update product_vendors set password='$password',password_org='$new_password' where vendor_id='$vendor_id'";
		
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	public function vendor_check_current_password($current_password,$vendor_id){
		$user_type=$this->session->userdata('user_type');
		
		$sql="select * from product_vendors where password='$current_password' and vendor_id='$vendor_id'";
		
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return true;
		}
		else{
			return false;
		}
	}
	public function add_vendor_catalog($sku_id,$common_image,$image_arr,$thumbnail_arr,$largeimage_arr,$vendor_id,$offer_price,$moq,$max_oq,$weight_per_unit,$stock,$product_status,$highlight,$sku_name,$shipswithin,$under_pcat_frontend,$brouchure,$emi_options,$price_validity,$material,$vendor_catalog_id,$category_id,$admin_margin_percentage,$admin_margin_value,$manufacture_days_per_piece,$sku_available_date,$discontinuation_comments,$discontinued_date,$dimension_of_furniture,$inventory_unit,$inventory_length,$inventory_height,$inventory_breadth,$old_product_status){

		
		$insertinfo = array("sku_id" => $sku_id, "sku_name"=>$sku_name,
		
		"vendor_id" => $vendor_id,"offer_price"=>$offer_price,"moq" => $moq,"max_oq"=>$max_oq,"stock" => $stock,"product_status" => $product_status,"common_image"=>$common_image,"description"=>$highlight,
		"shipswithin"=>$shipswithin,
		"under_pcat_frontend"=>$under_pcat_frontend,
		"brouchure"=>$brouchure,
		"emi_options"=>$emi_options,
		"price_validity"=>$price_validity,
		"material"=>$material,
		"category_id"=>$category_id,
		"admin_margin_percentage"=>$admin_margin_percentage,	
		"admin_margin_value"=>$admin_margin_value,
		"manufacture_days_per_piece"=>$manufacture_days_per_piece,
		"sku_available_date"=>$sku_available_date,
		"discontinuation_comments"=>$discontinuation_comments,
		"discontinued_date"=>$discontinued_date,
		"dimension_of_furniture"=>$dimension_of_furniture,
		"weight_per_unit"=>$weight_per_unit,
		"inventory_unit"=>$inventory_unit,
		"inventory_length"=>$inventory_length,
		"inventory_height"=>$inventory_height,
		"inventory_breadth"=>$inventory_breadth
);

		$arr1=[];
		$arr2=[];
		$arr3=[];
		$arr4=[];
		$arr5=[];
		$arr6=[];
		

		if(isset($image_arr[0]))
		$arr1=["image" => $image_arr[0], "thumbnail" => $thumbnail_arr[0], "largeimage" => $largeimage_arr[0]];
		if(isset($image_arr[1]))
		$arr2=["image2" => $image_arr[1], "thumbnail2" => $thumbnail_arr[1], "largeimage2" => $largeimage_arr[1]];
		if(isset($image_arr[2]))
		$arr3=["image3" => $image_arr[2], "thumbnail3" => $thumbnail_arr[2], "largeimage3" => $largeimage_arr[2]];
		if(isset($image_arr[3]))
		$arr4=["image4" => $image_arr[3], "thumbnail4" => $thumbnail_arr[3], "largeimage4" => $largeimage_arr[3]];
		if(isset($image_arr[4]))
		$arr5=["image5" => $image_arr[4], "thumbnail5" => $thumbnail_arr[4], "largeimage5" => $largeimage_arr[4]];
		if(isset($image_arr[5]))
		$arr6=["image6" => $image_arr[5], "thumbnail6" => $thumbnail_arr[5], "largeimage6" => $largeimage_arr[5]];

		$insertinfo=array_merge($insertinfo,$arr1,$arr2,$arr3,$arr4,$arr5,$arr6);

		if($vendor_catalog_id==''){
			$flag=$this -> db -> insert("vendor_catalog_inventory", $insertinfo);
			$inventory_id=$this->db->insert_id();
			return $flag;
		}else{

			$this->db->where(array("vendor_catalog_id"=>$vendor_catalog_id));
			$flag=$this -> db -> update("vendor_catalog_inventory", $insertinfo);
			
			if($product_status!='' && $product_status!=$old_product_status && $old_product_status!='' && $old_product_status!=null){
				$arr=[];
				if($product_status=='Discontinued'){		
					$arr=[
						"product_status"=>$product_status,
						"discontinuation_comments"=>$discontinuation_comments,
						"discontinued_date"=>$discontinued_date,
						"product_status_updatated_on"=>date('Y-m-d H:i:s')
					];
				}
				if($product_status=='Out Of Stock'){
					$arr=[
						"product_status"=>$product_status,
						"stock"=>'0',
						"sku_available_date"=>$sku_available_date,
						"product_status_updatated_on"=>date('Y-m-d H:i:s')					
					];
				}

				if($product_status=='In Stock'){
					$arr=[
						"product_status"=>$product_status,
						"stock"=>$stock,
						"product_status_updatated_on"=>date('Y-m-d H:i:s')				
					];
				}
				if($product_status=='Made To Order'){
					$arr=[
						"product_status"=>$product_status,
						"manufacture_days_per_piece"=>$manufacture_days_per_piece,
						"product_status_updatated_on"=>date('Y-m-d H:i:s')				
					];
				}

				if(!empty($arr)){
					$this->db->where(array("inv_vendor_catalog_id"=>$vendor_catalog_id));
					$flag=$this -> db -> update("inventory", $arr);
				}
				
			}
			return $flag;
		}
		
	}
	public function vendor_inventory_processing($params,$fg){
		
		
		$vendor_id =$params['vendor_id'];
		$vendor_city =$params['vendor_city'];
		$var_from =$params['from_date'];
		$var_to =$params['to_date'];
		
		$user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");

		$where = $sqlTot = $sqlRec = "";
		$sql = "select *,vendor_catalog_inventory.image as inv_image,vendor_catalog_inventory.timestamp as inv_timestamp,vendor_catalog_inventory.approval_status as inv_approval_status,vendor_catalog_inventory.status_updated_on as status_on,parent_category.pcat_name as catagory_name from vendor_catalog_inventory ";		

		$sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `vendor_catalog_inventory`.`category_id`';
		$sql.=' LEFT JOIN `product_vendors` ON `product_vendors`.`vendor_id` = `vendor_catalog_inventory`.`vendor_id`';

		$sql.=' where 1=1 ';

		if(!empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" (sku_id LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR offer_price LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";

		}

		if($vendor_id!=''){
			$where .=" and ";
			$where .=" vendor_catalog_inventory.vendor_id =".$vendor_id." ";
		}

		if($vendor_city!=''){
			$where .=" and ";
			$where .=" product_vendors.city = '".$this->db->escape_str($vendor_city)."'";
		}
		
		if(!empty($var_from) && !empty($var_to)){
				
			$date = str_replace('/', '-', $var_from);
			$from_date=date('Y-m-d', strtotime($date));
			$date = str_replace('/', '-', $var_to);
			$to_date=date('Y-m-d', strtotime($date));
			
			$where .=" AND ";
			$where .="DATE_FORMAT(vendor_catalog_inventory.timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(vendor_catalog_inventory.timestamp,'%Y-%m-%d') <= '$to_date'";
			
		}elseif(!empty($var_from) && empty($var_to)){
			$date = str_replace('/', '-', $var_from);
			$from_date=date('Y-m-d', strtotime($date));
			
			$where .=" AND ";
			$where .="DATE_FORMAT(vendor_catalog_inventory.timestamp,'%Y-%m-%d') <=> '$from_date'";
			
		}
		else{
			$where .="";
		}
		
		$sqlTot .= $sql;
		$sqlRec .= $sql;

		//echo $sql;

		if(isset($where) && $where != '') {
			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " order by vendor_catalog_inventory.timestamp desc";

		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
			
	}
	public function get_data_of_catalog_vendor($vendor_catalog_id){
		$query = $this->db->query("SELECT * FROM `vendor_catalog_inventory` where vendor_catalog_id='$vendor_catalog_id'");
		return $query -> row();

	}
	public function tag_vendor_inventory($post_arr){
		//print_r($post_arr);
		
		$vendor_catalog_id=$post_arr["vendor_catalog_id"];
		$vendor_id=$post_arr["vendor_id"];
		$parent_category=$post_arr["parent_category"];
		$categories=$post_arr["categories"];
		$subcategories=$post_arr["subcategories"];
		$brands=$post_arr["brands"];
		$products=$post_arr["products"];
		
		$query = $this->db->query("SELECT * FROM `vendor_catalog_inventory` where vendor_catalog_id='$vendor_catalog_id'");
		$catalog_data=$query -> row();
		
		if(!empty($catalog_data)){

			$product_id=$products;
			$sku_id=$catalog_data->sku_id;
			$sku_name=$catalog_data->sku_name;

			$common_image=$catalog_data->common_image;
			$image=$catalog_data->image;
			$thumbnail=$catalog_data->thumbnail;
			$largeimage=$catalog_data->largeimage;
			
			$selling_price=$catalog_data->offer_price;
			$cost_price=$catalog_data->admin_margin_value;
			$selling_discount=$catalog_data->admin_margin_percentage;

			$moq=$catalog_data->moq;
			$max_oq=$catalog_data->max_oq;
			$stock=$catalog_data->stock;

			$product_status=$catalog_data->product_status;
			
			$product_status_view='0'; // default hide

			$vendor_id=$catalog_data->vendor_id;
			$active='0';

			$image2=$catalog_data->image2;
			$thumbnail2=$catalog_data->thumbnail2;
			$largeimage2=$catalog_data->largeimage2;

			$image3=$catalog_data->image3;
			$thumbnail3=$catalog_data->thumbnail3;
			$largeimage3=$catalog_data->largeimage3;

			$image4=$catalog_data->image4;
			$thumbnail4=$catalog_data->thumbnail4;
			$largeimage4=$catalog_data->largeimage4;

			$image5=$catalog_data->image5;
			$thumbnail5=$catalog_data->thumbnail5;
			$largeimage5=$catalog_data->largeimage5;

			$highlight=$catalog_data->description;

			$shipswithin=$catalog_data->shipswithin;
			$brouchure=$catalog_data->brouchure;
			$emi_options=$catalog_data->emi_options;
			$price_validity=$catalog_data->price_validity;
			$material=$catalog_data->material;

			$inv_vendor_catalog_id=$catalog_data->vendor_catalog_id;
			$under_pcat_frontend=$catalog_data->under_pcat_frontend;
			$weight_per_unit=$catalog_data->weight_per_unit;
			$dimension_of_furniture=$catalog_data->dimension_of_furniture;
			

			$inventory_unit=$catalog_data->inventory_unit;
			$inventory_length=$catalog_data->inventory_length;
			$inventory_height=$catalog_data->inventory_height;
			$inventory_breadth=$catalog_data->inventory_breadth;


			$insert_arr=array(

			'inv_vendor_catalog_id'=>$inv_vendor_catalog_id,
			'product_id'=>$product_id,
			'sku_id'=>$sku_id,
			'sku_name'=>$sku_name,
			'common_image'=>$common_image,
			'image'=>$image,
			'thumbnail'=>$thumbnail,
			'largeimage'=>$largeimage,
			'image2'=>$image2,
			'thumbnail2'=>$thumbnail2,
			'largeimage2'=>$largeimage2,
			'image3'=>$image3,
			'thumbnail3'=>$thumbnail3,
			'largeimage3'=>$largeimage3,
			'image4'=>$image4,
			'thumbnail4'=>$thumbnail4,
			'largeimage4'=>$largeimage4,
			'image5'=>$image5,
			'thumbnail5'=>$thumbnail5,
			'largeimage5'=>$largeimage5,
			'selling_price'=>$selling_price,
			'cost_price'=>$cost_price,
			'selling_discount'=>$selling_discount,
			'moq'=>$moq,
			'max_oq'=>$max_oq,
			'stock'=>$stock,
			'product_status'=>$product_status,
			'product_status_view'=>$product_status_view, // default hide
			'vendor_id'=>$vendor_id,
			'active'=>$active,
			'highlight'=>$highlight,
			'shipswithin'=>$shipswithin,
			'brouchure'=>$brouchure,
			'emi_options'=>$emi_options,
			'price_validity'=>$price_validity,
			'material'=>$material,
			'under_pcat_frontend'=>$under_pcat_frontend
		
		);

		$user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");

		if($user_type=="vendor"){
			$insert_arr["added_by"]="vendor";
			$insert_arr["vendor_id"]=$a_id;
		}else{
			$insert_arr["added_by"]="admin";
			$insert_arr["vendor_id"]=$vendor_id;
		}

		$flag=$this -> db -> insert("inventory", $insert_arr);
		$inventory_id=$this->db->insert_id();


		/* Need to add data in logistics inventory */

		$query = $this->db->query("SELECT GROUP_CONCAT(logistics_parcel_category_id SEPARATOR ',') as parcel_ids  FROM `logistics_parcel_category` where vendor_id='$vendor_id'");
		//

		$parcel_ids=(isset($query -> row()->parcel_ids)) ? $query -> row()->parcel_ids : '';

		$logistics_parcel_category_id=$parcel_ids;
		$moq;
		
		$inventory_weight_in_kg=$weight_per_unit;
		$inventory_volume_in_kg=$dimension_of_furniture;
		$shipping_charge_not_applicable="no";
		$flat_shipping_charge="";
		$inventory_length=$inventory_length;
		$inventory_breadth=$inventory_breadth;
		$inventory_height=$inventory_height;
		$inventory_unit=$inventory_unit;
		$logistics_territory_id='';

		$flag3=$this->add_logistics_inventory($inventory_id,$logistics_parcel_category_id,$moq,$inventory_weight_in_kg,$inventory_volume_in_kg,$shipping_charge_not_applicable,$flat_shipping_charge,$inventory_length,$inventory_breadth,$inventory_height,$logistics_territory_id,$inventory_unit);

		/* Need to add data in logistics inventory */

			return array("inv_id"=>$inventory_id,"product_id"=>$product_id);
		}else{
			return array();
		}
		
	}
	public function get_all_tree_data_by_product($product_id){

		$sql="select * from products";
		
	$sql.= ' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`'
				.' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`'
				.' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`'
				.' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id` ';
		
		$sql.=" where products.product_id='$product_id'";

		//echo $sql;
		$query = $this->db->query($sql);

		return $query -> row_array();
	}
	public function add_tagged_invs_combo($data_posted){
		$inv_id=$data_posted['tagged_main_inventory_id'];
		$tagged_inventory_ids=implode(',',$data_posted['to_select_list']);
		$select_array=array("tagged_main_inventory_id" => $inv_id,"tagged_pcat_id" => $data_posted['sku_pcat_id'], "tagged_cat_id" => $data_posted['sku_cat_id'],"tagged_subcat_id" => $data_posted['sku_subcat_id'],"tagged_brand_id" => $data_posted['sku_brand_id'],"tagged_product_id" => $data_posted['sku_product_id']);
		$insertinfo = array("tagged_main_inventory_id" => $inv_id,"tagged_inventory_ids" => $tagged_inventory_ids, "tagged_pcat_id" => $data_posted['sku_pcat_id'], "tagged_cat_id" => $data_posted['sku_cat_id'],"tagged_subcat_id" => $data_posted['sku_subcat_id'],"tagged_brand_id" => $data_posted['sku_brand_id'],"tagged_product_id" => $data_posted['sku_product_id'],"tagged_timestamp"=>date('Y-m-d H:i:s'));
		$query=$this->db->select("tagged_id")->from("tagged_inventory_combo")->where($select_array)->get();
		$res=$query->row();
		if($query->num_rows()>0){
			$tagged_id=$res->tagged_id;
			$this->db->where(array("tagged_id"=>$tagged_id));
			$query=$this->db->update("tagged_inventory_combo",$insertinfo);	
			if($query==true){
				return true;
			}else{
				return false;
			}
		}else{
			$flag=$this -> db -> insert("tagged_inventory_combo", $insertinfo);
			return $flag;
		}
	}
	public function delete_tagged_invs_combo($tagged_id){
        
        if($tagged_id!='') {
            $this->db->where(array('tagged_id'=> $tagged_id));
            $this->db->delete('tagged_inventory_combo');
        }

        if($this->db->affected_rows() > 0){
                return true;
        }else{
                return false;
        }
    }
	public function get_state_list($state_id_list){

		$sql="select state_id,state_name,circle_id from logistics_all_states where state_id in ($state_id_list)";
		$sql.=" order by state_id desc";

		$query=$this->db->query($sql);
		return $query->result();

	}
	public function get_all_divisions($state_id,$territory_name_id){

		$sql="select imp_info_json from logistics_all_pincodes where state_id='$state_id'";
		//$sql.=" order by state_id desc";

		$query=$this->db->query($sql);
		return $query->result();

	}
	public function update_addon_product_allowed($id,$addon_product_allowed,$addon_selection_type){

		$sql = "UPDATE `inventory` SET addon_product_allowed='$addon_product_allowed' WHERE `id` = '$id'";
		$result=$this->db->query($sql);
		
		if($addon_selection_type!=''){
			$sql_1 = "UPDATE `tagged_inventory` SET addon_single_or_multiple_tagged_inventories_in_frontend='$addon_selection_type' WHERE `tagged_main_inventory_id` = '$id'";
			$result_11=$this->db->query($sql_1);
		}
		
		return $result;
	}
}
?>
