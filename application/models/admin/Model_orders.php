<?php
	class Model_orders extends CI_Model{
		public function get_inventory_info_by_inventory_id($inventory_id){
			$sql="select * from inventory where id='$inventory_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function get_product_info_by_product_id($product_id){
			$sql="select * from products where product_id='$product_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function get_subcat_info_by_product_id($product_id){
			$sql="select * from subcategory where subcat_id = (select subcat_id from products where product_id='$product_id')";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function onhold_orders_processing($params,$fg){
			
			$var_to =$params['to_date'];
			$var_from =$params['from_date'];		

			//define index of column
			$columns = array(
				0 => 'active_orders.timestamp', 
				1 => 'active_orders.subtotal',
				2 => 'shipping_address.customer_name',
				3 => 'active_orders.timestamp',
				4 => 'active_orders.quantity',
				5 => 'shipping_address.customer_name',
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select orders_status.type_of_order,orders_status.id as orders_status_id,active_orders.*,active_orders.image,
			active_orders.sku_id,
			active_orders.prev_order_item_id,
			products.product_name,
			products.product_description,
			active_orders.timestamp,
			active_orders.order_id,
			active_orders.payment_type,
			orders_status.order_item_id,
			DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') AS 'order_status_timestamp',
			active_orders.quantity,
			active_orders.product_price,
			active_orders.subtotal,
			active_orders.grandtotal,
			active_orders.ord_addon_master_order_item_id,
			shipping_address.customer_name,
			shipping_address.address1,
			shipping_address.address2,
			shipping_address.country,
			shipping_address.state,
			shipping_address.city,
			shipping_address.pincode,
			shipping_address.mobile,invoices_offers.* from active_orders,invoices_offers,orders_status,products,inventory,shipping_address
			where active_orders.customer_id=orders_status.customer_id and 
			active_orders.order_item_id=orders_status.order_item_id and
			active_orders.customer_id=shipping_address.customer_id and
			active_orders.shipping_address_id=shipping_address.shipping_address_id and
			active_orders.product_id=products.product_id and
			active_orders.inventory_id=inventory.id and	
			orders_status.customer_id=shipping_address.customer_id and invoices_offers.order_id=active_orders.order_id and
			orders_status.order_placed='0' and 
			orders_status.order_confirmed='0' and 
			orders_status.order_packed='0' and 
			orders_status.order_shipped='0' and 
			orders_status.order_delivered='0' and 
			orders_status.order_cancelled='0'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( active_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.subtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.customer_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address1 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address2 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.country LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.state LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.city LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.pincode LIKE '%".$params['search']['value']."%' ";			
				$where .=" OR shipping_address.mobile LIKE '%".$params['search']['value']."%')";	 
			}

			$user_type=$this->session->userdata("user_type");
			$a_id=$this->session->userdata("user_id");

			if($user_type=='vendor'){
				$where .=" AND ";
				$where .=" active_orders.product_vendor_id= '$a_id' ";
			}

			if(!empty($var_from) && !empty($var_to)){
				
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$date = str_replace('/', '-', $var_to);
				$to_date=date('Y-m-d', strtotime($date));
				
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') <= '$to_date'";
				
			}elseif(!empty($var_from) && empty($var_to)){
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') >= '$from_date'";
			}
			else{
				$where .="";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			//echo $sqlTot;
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			//echo $sqlTot;
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		public function set_as_approved($list_to_be_approved){
			$list_to_be_approved_arr=explode("-",$list_to_be_approved);
			$success_arr=array();
			foreach($list_to_be_approved_arr as $orders_status_id){
				$sql="update orders_status set order_placed='1',order_placed_timestamp=now() where id='$orders_status_id'";
				$query=$this->db->query($sql);
				if($query){
					$success_arr[]="yes";
				}
				
			}
			if(count($list_to_be_approved_arr)==count($success_arr)){
				
				return true;
			}
			else{
				return false;
			}
		}
		
		//////////////////////////
		public function approved_orders_processing($params,$fg){
			
			$var_to =$params['to_date'];
			$var_from =$params['from_date'];
			
			//define index of column

			$columns = array(
				0 => 'active_orders.timestamp', 
				1 => 'active_orders.subtotal',
				2 => 'shipping_address.customer_name',
				3 => 'active_orders.timestamp',
				4 => 'active_orders.quantity',
				5 => 'shipping_address.customer_name',
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select orders_status.type_of_order,orders_status.id as orders_status_id,active_orders.*,active_orders.image,
			active_orders.sku_id,
			active_orders.prev_order_item_id,
			products.product_name,
			products.product_description,
			active_orders.timestamp,
			active_orders.order_id,
			active_orders.payment_type,
			orders_status.order_item_id,
			active_orders.quantity,
			active_orders.product_price,
			active_orders.shipping_charge,
			active_orders.subtotal,
			active_orders.grandtotal,
			active_orders.ord_addon_master_order_item_id,
			shipping_address.customer_name,
			shipping_address.address1,
			shipping_address.address2,
			shipping_address.country,
			shipping_address.state,
			shipping_address.city,
			shipping_address.pincode,
			shipping_address.mobile,invoices_offers.* from active_orders,invoices_offers,orders_status,products,inventory,shipping_address
			where active_orders.customer_id=orders_status.customer_id and 
			active_orders.order_item_id=orders_status.order_item_id and
			active_orders.customer_id=shipping_address.customer_id and
			active_orders.shipping_address_id=shipping_address.shipping_address_id and
			active_orders.product_id=products.product_id and
			active_orders.inventory_id=inventory.id and	
			orders_status.customer_id=shipping_address.customer_id and invoices_offers.order_id=active_orders.order_id and
			orders_status.order_placed='1' and 
			orders_status.order_confirmed='0' and 
			orders_status.order_packed='0' and 
			orders_status.order_shipped='0' and 
			orders_status.order_delivered='0' and 
			orders_status.order_cancelled='0'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( active_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.subtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.customer_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address1 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address2 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.country LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.state LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.city LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.pincode LIKE '%".$params['search']['value']."%' ";
			
				$where .=" OR shipping_address.mobile LIKE '%".$params['search']['value']."%' )";
			}

			$user_type=$this->session->userdata("user_type");
			$a_id=$this->session->userdata("user_id");

			if($user_type=='vendor'){
				$where .=" AND ";
				$where .=" active_orders.product_vendor_id= '$a_id' ";
			}
			
			if(!empty($var_from) && !empty($var_to)){
				
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$date = str_replace('/', '-', $var_to);
				$to_date=date('Y-m-d', strtotime($date));
				
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') <= '$to_date'";
				
			}elseif(!empty($var_from) && empty($var_to)){
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') >= '$from_date'";
			}
			else{
				$where .="";
			}
			
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		public function set_as_confirmed($list_to_be_confirmed){
			$list_to_be_confirmed_arr=explode("-",$list_to_be_confirmed);
			$success_arr=array();
			foreach($list_to_be_confirmed_arr as $orders_status_id){
				$sql="update orders_status set order_confirmed='1',order_confirmed_timestamp=now() where id='$orders_status_id'";
				$query=$this->db->query($sql);
				if($query){
					$success_arr[]="yes";
				}
			}
			if(count($list_to_be_confirmed_arr)==count($success_arr)){
				return true;
			}
			else{
				return false;
			}
		}
		
		//////////////////////////
		public function confirmed_orders_processing($params,$fg){
			$var_to =$params['to_date'];
			$var_from =$params['from_date'];
			
			//define index of column
			$columns = array(
				0 => 'active_orders.timestamp', 
				1 => 'active_orders.subtotal',
				2 => 'shipping_address.customer_name',
				3 => 'active_orders.timestamp',
				4 => 'active_orders.quantity',
				5 => 'shipping_address.customer_name',
				6 => 'active_orders.timestamp', 
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select orders_status.type_of_order,orders_status.id as orders_status_id,active_orders.*,active_orders.image,
       active_orders.sku_id,
	   active_orders.prev_order_item_id,
	   products.product_name,
	   products.product_description,
       active_orders.timestamp,
       active_orders.order_id,
	   active_orders.payment_type,
       orders_status.order_item_id,
       active_orders.quantity,
       active_orders.product_price,
       active_orders.shipping_charge,
       active_orders.subtotal,
	    active_orders.grandtotal,
       shipping_address.customer_name,
       shipping_address.address1,
       shipping_address.address2,
       shipping_address.country,
       shipping_address.state,
       shipping_address.city,
       shipping_address.pincode,
       shipping_address.mobile,invoices_offers.* from active_orders,invoices_offers,orders_status,products,inventory,shipping_address
       where active_orders.customer_id=orders_status.customer_id and 
	     active_orders.order_item_id=orders_status.order_item_id and
	     active_orders.customer_id=shipping_address.customer_id and
	     active_orders.shipping_address_id=shipping_address.shipping_address_id and
             active_orders.product_id=products.product_id and
             active_orders.inventory_id=inventory.id and	
             orders_status.customer_id=shipping_address.customer_id and invoices_offers.order_id=active_orders.order_id and
             orders_status.order_placed='1' and 
             orders_status.order_confirmed='1' and 
             orders_status.order_packed='0' and 
             orders_status.order_shipped='0' and 
             orders_status.order_delivered='0' and 
             orders_status.order_cancelled='0'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( active_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.subtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.customer_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address1 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address2 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.country LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.state LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.city LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.pincode LIKE '%".$params['search']['value']."%' ";
			
				$where .=" OR shipping_address.mobile LIKE '%".$params['search']['value']."%' )";
			}
			if(!empty($var_from) && !empty($var_to)){
				
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$date = str_replace('/', '-', $var_to);
				$to_date=date('Y-m-d', strtotime($date));
				
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') <= '$to_date'";
				
			}elseif(!empty($var_from) && empty($var_to)){
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') >= '$from_date'";
			}
			else{
				$where .="";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		public function set_as_packed($list_to_be_packed){
			$list_to_be_packed_arr=explode("-",$list_to_be_packed);
			$success_arr=array();
			foreach($list_to_be_packed_arr as $orders_status_id){
				$sql="update orders_status set order_packed='1',order_packed_timestamp=now() where id='$orders_status_id'";
				$query=$this->db->query($sql);
				if($query){
					$success_arr[]="yes";
				}
			}
			if(count($list_to_be_packed_arr)==count($success_arr)){
				return true;
			}
			else{
				return false;
			}
		}
		
		//////////////////////////
		public function packed_orders_processing($params,$fg){
			$var_to =$params['to_date'];
			$var_from =$params['from_date'];	

			//define index of column
			$columns = array(
				0 => 'active_orders.timestamp', 
				1 => 'active_orders.subtotal',
				2 => 'shipping_address.customer_name',
				3 => 'active_orders.timestamp',
				4 => 'active_orders.quantity',
				5 => 'shipping_address.customer_name'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select orders_status.type_of_order,orders_status.id as orders_status_id,active_orders.*,active_orders.image,
			active_orders.sku_id,
			active_orders.prev_order_item_id,
			products.product_name,
			products.product_description,
			active_orders.timestamp,
			active_orders.order_id,
			active_orders.payment_type,
			orders_status.order_item_id,
			active_orders.quantity,
			active_orders.product_price,
			active_orders.shipping_charge,
			active_orders.subtotal,
			active_orders.grandtotal,
			active_orders.ord_addon_master_order_item_id,
			shipping_address.customer_name,
			shipping_address.address1,
			shipping_address.address2,
			shipping_address.country,
			shipping_address.state,
			shipping_address.city,
			shipping_address.pincode,
			shipping_address.mobile,invoices_offers.* from active_orders,invoices_offers,orders_status,products,inventory,shipping_address
			where active_orders.customer_id=orders_status.customer_id and 
			active_orders.order_item_id=orders_status.order_item_id and
			active_orders.customer_id=shipping_address.customer_id and
			active_orders.shipping_address_id=shipping_address.shipping_address_id and
			active_orders.product_id=products.product_id and
			active_orders.inventory_id=inventory.id and	
			orders_status.customer_id=shipping_address.customer_id and invoices_offers.order_id=active_orders.order_id and
			orders_status.order_placed='1' and 
			orders_status.order_confirmed='1' and 
			orders_status.order_packed='1' and 
			orders_status.order_shipped='0' and 
			orders_status.order_delivered='0' and 
			orders_status.order_cancelled='0'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( active_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.subtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.customer_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address1 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address2 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.country LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.state LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.city LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.pincode LIKE '%".$params['search']['value']."%' ";
			
				$where .=" OR shipping_address.mobile LIKE '%".$params['search']['value']."%' )";
			}
			if(!empty($var_from) && !empty($var_to)){
				
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$date = str_replace('/', '-', $var_to);
				$to_date=date('Y-m-d', strtotime($date));
				
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') <= '$to_date'";
				
			}elseif(!empty($var_from) && empty($var_to)){
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') >= '$from_date'";
			}
			else{
				$where .="";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		public function set_as_shipped($list_to_be_shipped){
			$list_to_be_shipped_arr=explode("-",$list_to_be_shipped);
			$success_arr=array();
			foreach($list_to_be_shipped_arr as $orders_status_id){
				$sql="update orders_status set order_shipped='1',order_shipped_timestamp=now() where id='$orders_status_id'";
				$query=$this->db->query($sql);
				if($query){
					$success_arr[]="yes";
				}
			}
			if(count($list_to_be_shipped_arr)==count($success_arr)){
				return true;
			}
			else{
				return false;
			}
		}
			//////////////////////////
		public function shipped_orders_processing($params,$fg){
			$var_to =$params['to_date'];
			$var_from =$params['from_date'];	
			//define index of column
			$columns = array(
				0 => 'active_orders.timestamp', 
				1 => 'active_orders.subtotal',
				2 => 'shipping_address.customer_name',
				3 => 'active_orders.timestamp',
				4 => 'active_orders.quantity',
				5 => 'shipping_address.customer_name'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select orders_status.type_of_order,orders_status.id as orders_status_id,active_orders.*,active_orders.image,
			active_orders.sku_id,
			active_orders.prev_order_item_id,
			products.product_name,
			products.product_description,
			active_orders.timestamp,
			active_orders.order_id,
			active_orders.payment_type,
			orders_status.order_item_id,
			active_orders.quantity,
			active_orders.product_price,
			active_orders.shipping_charge,
			active_orders.subtotal,
			active_orders.grandtotal,
			active_orders.ord_addon_master_order_item_id,
			shipping_address.customer_name,
			shipping_address.address1,
			shipping_address.address2,
			shipping_address.country,
			shipping_address.state,
			shipping_address.city,
			shipping_address.pincode,
			shipping_address.mobile,invoices_offers.* from active_orders,invoices_offers,orders_status,products,inventory,shipping_address
			where active_orders.customer_id=orders_status.customer_id and 
			active_orders.order_item_id=orders_status.order_item_id and
			active_orders.customer_id=shipping_address.customer_id and
			active_orders.shipping_address_id=shipping_address.shipping_address_id and
			active_orders.product_id=products.product_id and
			active_orders.inventory_id=inventory.id and	
			orders_status.customer_id=shipping_address.customer_id and invoices_offers.order_id=active_orders.order_id and
			orders_status.order_placed='1' and 
			orders_status.order_confirmed='1' and 
			orders_status.order_packed='1' and 
			orders_status.order_shipped='1' and 
			orders_status.order_delivered='0' and 
			orders_status.order_cancelled='0'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( active_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR active_orders.subtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.customer_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address1 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address2 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.country LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.state LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.city LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.pincode LIKE '%".$params['search']['value']."%' ";
			
				$where .=" OR shipping_address.mobile LIKE '%".$params['search']['value']."%' )";
			}

			$user_type=$this->session->userdata("user_type");
			$a_id=$this->session->userdata("user_id");

			if($user_type=='vendor'){
				$where .=" AND ";
				$where .=" active_orders.product_vendor_id= '$a_id' ";
			}

			if(!empty($var_from) && !empty($var_to)){
				
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$date = str_replace('/', '-', $var_to);
				$to_date=date('Y-m-d', strtotime($date));
				
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') <= '$to_date'";
				
			}elseif(!empty($var_from) && empty($var_to)){
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') >= '$from_date'";
			}
			else{
				$where .="";
			}
			
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		public function set_as_delivered($list_to_be_delivered,$order_delivered_date,$order_delivered_time,$order_id,$customer_id){
			
			$list_to_be_delivered_arr=explode("-",$list_to_be_delivered);
			$success_arr=array();
			
			$order_replacement_pickup_flag='';
			
			foreach($list_to_be_delivered_arr as $orders_status_id){

				$sql="update orders_status set order_delivered='1',order_delivered_timestamp=now(),order_delivered_date='$order_delivered_date',order_delivered_time='$order_delivered_time' where id='$orders_status_id'";

				$query=$this->db->query($sql);
				
				if($query){
					$success_arr[]="yes";
				}
			}
			
			//invoice offers
		
/*		
			$sql_invoice="select * from invoices_offers where order_id='$order_id'";
			$result_invoice=$this->db->query($sql_invoice);
			
			$order_item_count=$result_invoice->row()->cart_quantity;
			$payment_method=$result_invoice->row()->payment_method;	
			$invoice_surprise_gift=$result_invoice->row()->invoice_surprise_gift;
			$invoice_surprise_gift_type=$result_invoice->row()->invoice_surprise_gift_type;
			$invoice_surprise_gift_skus=$result_invoice->row()->invoice_surprise_gift_skus;
			$invoice_surprise_gift_skus_nums=$result_invoice->row()->invoice_surprise_gift_skus_nums;
			
			if($payment_method=='COD'){

					$sql_com_orders="select count(*) as count from completed_orders where order_id='$order_id' GROUP BY order_id";
					$result_com_orders=$this->db->query($sql_com_orders);
					$com_row=$result_com_orders->row();
					if(!empty($com_row)){
						$com_count=$result_com_orders->row()->count;
					}else{
						$com_count=0;
					}
					
					if($com_count==$order_item_count){
						//all order_items are delivered// moved to completed_orders
						//credit the amount to wallet
						if($invoice_surprise_gift_type==curr_code){
				
							$surprise_gift_amount=$invoice_surprise_gift;
							
							if($surprise_gift_amount>0){
						
								$sql_wallet_check="select * from wallet where customer_id='$customer_id'";
								$result_d=$this->db->query($sql_wallet_check);
								if($result_d->num_rows()>0){
									$wallet_id=$result_d->row()->wallet_id;
									$wallet_amount=$result_d->row()->wallet_amount;
									$new_wallet_amount=($surprise_gift_amount+$wallet_amount);
									
									$sql_update_s="update wallet set wallet_amount='$new_wallet_amount' where wallet_id='$wallet_id' and customer_id='$customer_id'";
									$this->db->query($sql_update_s);
									
								}else{
										$sql_insert_s="insert into wallet set customer_id='$customer_id', wallet_amount='$surprise_gift_amount'";
										$this->db->query($sql_insert_s);
										$wallet_id=$this->db->insert_id();
										$new_wallet_amount=$surprise_gift_amount;
								}
								
								$sql_for_wallet_transaction_surprise="insert into wallet_transaction set transaction_details='surprise gift amount credited to your wallet against <br> the order number $order_id', debit='' , credit='$surprise_gift_amount',amount='$new_wallet_amount',wallet_id='$wallet_id',customer_id='$customer_id'";
								
								$this->db->query($sql_for_wallet_transaction_surprise);
							}
							
						}
						
						if($invoice_surprise_gift_type=='Qty'){
							//reduce the stock of free items

							$invoice_surprise_gift_skus=rtrim($invoice_surprise_gift_skus,',');
							$invoice_surprise_gift_skus_nums=rtrim($invoice_surprise_gift_skus_nums,',');
							
							$invoice_surprise_gift_skus_arr = explode(',', $invoice_surprise_gift_skus);
							$invoice_surprise_gift_skus_nums_arr = explode(',', $invoice_surprise_gift_skus_nums);
							
							$two=array_combine($invoice_surprise_gift_skus_arr,$invoice_surprise_gift_skus_nums_arr);
							
							foreach($two as $inv_id=>$stock_count){
								$sql_free_item_gift="update inventory set stock = (stock-$stock_count) where id='$inv_id' and stock >= $stock_count; ";
								$result=$this->db->query($sql_free_item_gift);
							}
				
						}
					}
				
			}
			*/
			
			if(count($list_to_be_delivered_arr)==count($success_arr)){
				return true;
			}else{
				return false;
			}
		}
		
			//////////////////////////
		public function delivered_orders_processing($params,$fg){
			$var_to =$params['to_date'];
			$var_from =$params['from_date'];
			//define index of column
			
			$columns = array(
				0 => 'completed_orders.timestamp', 
				1 => 'completed_orders.subtotal',
				2 => 'shipping_address.customer_name',
				3 => 'completed_orders.timestamp',
				4 => 'completed_orders.quantity',
				5 => 'shipping_address.customer_name'
			);
			
			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select orders_status.type_of_order,orders_status.id as orders_status_id,completed_orders.*,completed_orders.image,
			completed_orders.sku_id,
			products.product_name,
			products.product_description,
			completed_orders.timestamp,
			completed_orders.order_id,
			orders_status.order_item_id,
			orders_status.order_delivered_date,
			orders_status.order_delivered_time,
			orders_status.order_replacement_pickup,
			completed_orders.quantity,
			completed_orders.product_price,
			completed_orders.shipping_charge,
			completed_orders.subtotal,
			completed_orders.grandtotal,
			completed_orders.ord_addon_master_order_item_id,
			shipping_address.customer_name,
			shipping_address.address1,
			shipping_address.address2,
			shipping_address.country,
			shipping_address.state,
			shipping_address.city,
			shipping_address.pincode,
			shipping_address.mobile,invoices_offers.* ,
                        history_orders.ord_sku_name
                        from completed_orders,history_orders,invoices_offers,orders_status,products,inventory,shipping_address
			where completed_orders.customer_id=orders_status.customer_id and 
			completed_orders.order_item_id=orders_status.order_item_id and
			completed_orders.customer_id=shipping_address.customer_id and
			completed_orders.shipping_address_id=shipping_address.shipping_address_id and
			completed_orders.product_id=products.product_id and
			completed_orders.inventory_id=inventory.id and	
			completed_orders.order_item_id=history_orders.order_item_id and	
			orders_status.customer_id=shipping_address.customer_id and invoices_offers.order_id=completed_orders.order_id and
			orders_status.order_placed='1' and 
			orders_status.order_confirmed='1' and 
			orders_status.order_packed='1' and 
			orders_status.order_shipped='1' and 
			orders_status.order_delivered='1' and 
			orders_status.order_cancelled='0'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( completed_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.subtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.customer_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address1 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address2 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.country LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.state LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.city LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.pincode LIKE '%".$params['search']['value']."%' ";
			
				$where .=" OR shipping_address.mobile LIKE '%".$params['search']['value']."%' )";
			}

			$user_type=$this->session->userdata("user_type");
			$a_id=$this->session->userdata("user_id");

			if($user_type=='vendor'){
				$where .=" AND ";
				$where .=" completed_orders.product_vendor_id= '$a_id' ";
			}

			if(!empty($var_from) && !empty($var_to)){
				
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$date = str_replace('/', '-', $var_to);
				$to_date=date('Y-m-d', strtotime($date));
				
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') <= '$to_date'";
				
			}elseif(!empty($var_from) && empty($var_to)){
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') >= '$from_date'";
			}
			else{
				$where .="";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		////////////////////
		public function set_as_cancelled($list_to_be_cancelled,$reason_for_cancel_order){
			$list_to_be_cancelled_arr=explode("-",$list_to_be_cancelled);
			$success_arr=array();
			foreach($list_to_be_cancelled_arr as $orders_status_id){
				$sql="update orders_status set order_cancelled='1',order_cancelled_timestamp=now() where id='$orders_status_id'";
				$query=$this->db->query($sql);
				if($query){
					$success_arr[]="yes";
				}
			}
			if(count($list_to_be_cancelled_arr)==count($success_arr)){
				return true;
			}
			else{
				return false;
			}
		}
		public function get_order_item_id_by_orders_status_id($orders_status_id){
			$sql="select order_item_id from orders_status where id='$orders_status_id'";
			$query=$this->db->query($sql);
			$order_item_id=$query->row()->order_item_id;
			return $order_item_id;
		}
		
		public function add_cancel_orders_to_cancels_table($order_item_id,$cancel_reason_id,$cancel_reason_comment,$update_inventory_stock,$user_type,$refund_method,$refund_bank_id){
			$cancel_reason_comment=$this->db->escape_str($cancel_reason_comment);
			$sql="insert into cancels (order_item_id,reason_cancel_id,cancel_reason_comments,update_inventory_stock,cancelled_by,refund_type,refund_bank_id) values('$order_item_id','$cancel_reason_id','$cancel_reason_comment','$update_inventory_stock','$user_type','$refund_method','$refund_bank_id')";
			$result=$this->db->query($sql);
			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}
		}
		
		public function get_all_order_data_from_order_item($order_item_id){
			$sql="select active_orders.*,invoices_offers.* from active_orders,invoices_offers where active_orders.order_item_id='$order_item_id' and active_orders.order_id=invoices_offers.order_id";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->result_array();
			}
		}
		
		
		public function active_order_add_to_cancel_orders($purchased_price,$prev_order_item_id,$order_item_id,$customer_id,$order_id,$shipping_address_id,$inventory_id,$product_id,$sku_id,$tax_percent,$product_price,$shipping_charge,$subtotal,$wallet_status,$wallet_amount,$grandtotal,$quantity,$timestamp,$image,$random_number,$invoice_email,$invoice_number,$logistics_name,$logistics_weblink,$tracking_number,$expected_delivery_date,$delivery_mode,$parcel_category,$vendor_id,$payment_type,$payment_status,$return_period,$logistics_id,$logistics_delivery_mode_id,$logistics_territory_id,$logistics_parcel_category_id,$promotion_available,$promotion_residual,$promotion_discount,$promotion_cashback,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$default_discount,$promotion_item_multiplier,$promotion_item,$promotion_item_num,$promotion_clubed_with_default_discount,$promotion_id_selected,$promotion_type_selected,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_without_promotion,$total_price_of_product_with_promotion,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$order_item_invoice_discount_value,$razorpayOrderId,$razorpayPaymentId,$ord_max_selling_price,$ord_selling_discount,$ord_tax_percent_price,$ord_taxable_price,$ord_sku_name,$ord_coupon_code,$ord_coupon_name,$ord_coupon_type,$ord_coupon_value,$ord_coupon_reduce_price,$ord_coupon_status,$ord_addon_products_status,$ord_addon_products,$ord_addon_inventories,$ord_addon_total_price,$ord_addon_single_or_multiple_tagged_inventories_in_frontend,$ord_addon_master_order_item_id,$product_vendor_id){
			
			$sql="insert into cancelled_orders (purchased_price,prev_order_item_id,order_item_id,customer_id,order_id,shipping_address_id,inventory_id,product_id,sku_id,tax_percent,product_price,shipping_charge,subtotal,wallet_status,wallet_amount,grandtotal,quantity,timestamp,image,random_number,invoice_email,invoice_number,logistics_name,logistics_weblink,tracking_number,expected_delivery_date,delivery_mode,parcel_category,vendor_id,payment_type,payment_status,return_period,logistics_id,logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id,promotion_available,promotion_residual,promotion_discount,promotion_cashback,promotion_surprise_gift,promotion_surprise_gift_type,promotion_surprise_gift_skus,promotion_surprise_gift_skus_nums,default_discount,promotion_item_multiplier,promotion_item,promotion_item_num,promotion_clubed_with_default_discount,promotion_id_selected,promotion_type_selected,promotion_minimum_quantity,promotion_quote,promotion_default_discount_promo,individual_price_of_product_with_promotion,individual_price_of_product_without_promotion,total_price_of_product_without_promotion,total_price_of_product_with_promotion,quantity_without_promotion,quantity_with_promotion,cash_back_value,order_item_invoice_discount_value,notify_cancel,razorpayOrderId,razorpayPaymentId,ord_max_selling_price,ord_selling_discount,ord_tax_percent_price,ord_taxable_price,ord_sku_name,ord_coupon_code,ord_coupon_name,ord_coupon_type,ord_coupon_value,ord_coupon_reduce_price,ord_coupon_status,ord_addon_products_status,ord_addon_products,ord_addon_inventories,ord_addon_total_price,ord_addon_single_or_multiple_tagged_inventories_in_frontend,ord_addon_master_order_item_id,product_vendor_id) values('$purchased_price','$prev_order_item_id','$order_item_id','$customer_id','$order_id','$shipping_address_id','$inventory_id','$product_id','$sku_id','$tax_percent','$product_price','$shipping_charge','$subtotal','$wallet_status','$wallet_amount','$grandtotal','$quantity','$timestamp','$image','$random_number','$invoice_email','$invoice_number','$logistics_name','$logistics_weblink','$tracking_number','$expected_delivery_date','$delivery_mode','$parcel_category','$vendor_id','$payment_type','$payment_status','$return_period','$logistics_id','$logistics_delivery_mode_id','$logistics_territory_id','$logistics_parcel_category_id','$promotion_available','$promotion_residual','$promotion_discount','$promotion_cashback','$promotion_surprise_gift','$promotion_surprise_gift_type','$promotion_surprise_gift_skus','$promotion_surprise_gift_skus_nums','$default_discount','$promotion_item_multiplier','$promotion_item','$promotion_item_num','$promotion_clubed_with_default_discount','$promotion_id_selected','$promotion_type_selected','$promotion_minimum_quantity','$promotion_quote','$promotion_default_discount_promo','$individual_price_of_product_with_promotion','$individual_price_of_product_without_promotion','$total_price_of_product_without_promotion','$total_price_of_product_with_promotion','$quantity_without_promotion','$quantity_with_promotion','$cash_back_value','$order_item_invoice_discount_value','0','$razorpayOrderId','$razorpayPaymentId','$ord_max_selling_price','$ord_selling_discount','$ord_tax_percent_price','$ord_taxable_price','$ord_sku_name','$ord_coupon_code','$ord_coupon_name','$ord_coupon_type','$ord_coupon_value','$ord_coupon_reduce_price','$ord_coupon_status','$ord_addon_products_status','$ord_addon_products','$ord_addon_inventories','$ord_addon_total_price','$ord_addon_single_or_multiple_tagged_inventories_in_frontend','$ord_addon_master_order_item_id','$product_vendor_id')";
			$result=$this->db->query($sql);
			if($this->db->affected_rows() > 0){
				
				return true;
			}
			else{
				return false;
			}
		}
		
		public function get_cancel_id_of_this_transaction($order_item_id){
			$sql="select cancel_id from cancels where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result->row()->cancel_id;
		}
		
		public function get_wallet_exists_for_customer($customer_id){
			$sql="select * from wallet where customer_id='$customer_id'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return true;
			}
			else{
				return false;
			}
		}
		
		/*public function add_this_transaction_to_refund_table($return_id,$cancel_id,$refund_method,$product_id,$sku_id,$product_price,$grandtotal,$quantity,$refund_bank_id,$order_item_id){
			$sql="insert into refund (return_id,order_item_id,cancel_id,refund_type,product_id,sku_id,product_price,grand_total,quantity,refund_bank_id) values('$return_id','$order_item_id','$cancel_id','$refund_method','$product_id','$sku_id','$product_price','$grandtotal','$quantity','$refund_bank_id') ";
			$result=$this->db->query($sql);
			if($this->db->affected_rows() > 0){
				return true;
			}
			else{
				return false;
			}
		}*/
		
		public function get_customer_wallet_summary($customer_id){
		$sql="select * from wallet where customer_id='$customer_id' ";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	
	
	public function update_order_item_id_to_cancel($order_item_id){
		$sql_get_status="select * from  orders_status where order_placed='0' and order_confirmed='0' and order_packed='0' and order_shipped='0' and order_delivered='0' and order_item_id='$order_item_id'";
		$result_get_status=$this->db->query($sql_get_status);
		if($result_get_status->num_rows()==0){
			$sql="update orders_status set order_cancelled='1',order_cancelled_timestamp=now() where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			if($this->db->affected_rows() > 0){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			$sql="update orders_status set order_placed='1',order_placed_timestamp=now(),order_cancelled='1',order_cancelled_timestamp=now() where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			if($this->db->affected_rows() > 0){
				return true;
			}
			else{
				return false;
			}
		}
		
	}
	
	public function remove_data_from_active_order_table($order_item_id){
		$sql="delete from active_orders where order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	///////////////////////
	public function cancelled_orders_processing($params,$fg){
		

		//define index of column
			$columns = array(
				0 =>'cancelled_orders.timestamp', 
				1 => 'cancelled_orders.subtotal',
				2 => 'shipping_address.customer_name'
			);

			$where = $sqlTot = $sqlRec = "";
		
		$status_of_order=$params['status_of_order'];
		
		if($status_of_order=="in_process"){
			$str=" and cancels.status='refund' ";
		}elseif($status_of_order=="is_on_wait"){
			$str=" and cancels.status='' ";
		}elseif($status_of_order=="failure"){
			$str=" and cancels.status='not refunded' ";
		}else{
			$str=" and (cancels.status='refund' or cancels.status='')";
		}
		
		
			// getting total number records without any search
			$sql = "select orders_status.type_of_order,orders_status.id as orders_status_id,cancelled_orders.*,cancelled_orders.image,
       cancelled_orders.sku_id,
	   cancelled_orders.prev_order_item_id,
       products.product_name,
	   products.product_description,
       cancelled_orders.timestamp,
       cancelled_orders.order_id,
       orders_status.order_item_id,
       cancelled_orders.quantity,
       cancelled_orders.product_price,
       cancelled_orders.shipping_charge,
       cancelled_orders.subtotal,
	   cancelled_orders.grandtotal,
	   cancelled_orders.ord_addon_master_order_item_id,
       shipping_address.customer_name,
       shipping_address.address1,
       shipping_address.address2,
       shipping_address.country,
       shipping_address.state,
       shipping_address.city,
       shipping_address.pincode,
       shipping_address.mobile,invoices_offers.*,cancels.* from cancelled_orders,invoices_offers,orders_status,products,inventory,shipping_address,cancels
       where cancelled_orders.customer_id=orders_status.customer_id and 
	     cancelled_orders.order_item_id=orders_status.order_item_id and
	     cancelled_orders.customer_id=shipping_address.customer_id and
	     cancelled_orders.shipping_address_id=shipping_address.shipping_address_id and
             cancelled_orders.product_id=products.product_id and
             cancelled_orders.inventory_id=inventory.id and	
			 products.product_id=inventory.product_id and 
             orders_status.customer_id=shipping_address.customer_id and invoices_offers.order_id=cancelled_orders.order_id and cancels.order_item_id=cancelled_orders.order_item_id and 
             orders_status.order_cancelled='1' and cancelled_orders.prev_order_item_id='' and cancelled_orders.payment_type!='cod' $str";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( cancelled_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.subtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.customer_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address1 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address2 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.country LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.state LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.city LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.pincode LIKE '%".$params['search']['value']."%' ";
			
				$where .=" OR shipping_address.mobile LIKE '%".$params['search']['value']."%' )";
			}

			$user_type=$this->session->userdata("user_type");
			$a_id=$this->session->userdata("user_id");

			if($user_type=='vendor'){
				$where .=" AND ";
				$where .=" cancelled_orders.product_vendor_id= '$a_id' ";
			}

			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		public function cancelled_orders_inactive_processing($params,$fg){
		

		//define index of column
			$columns = array(
				0 =>'cancelled_orders.timestamp', 
				1 => 'cancelled_orders.subtotal',
				2 => 'shipping_address.customer_name'
			);

			$where = $sqlTot = $sqlRec = "";
		
		$status_of_order=$params['status_of_order'];
		
		if($status_of_order=="success"){
			$str=" and cancels.status='refunded' ";
		}elseif($status_of_order=="failure"){
			//$str=" and cancels.status='not refunded' ";
		}elseif($status_of_order=="inactive"){
			$str=" and cancelled_orders.payment_type='cod' ";
		}else{
			$str=" and (cancels.status='refunded' or cancels.status='not refunded' or cancelled_orders.payment_type='cod') ";
		}
		
		
			// getting total number records without any search
			 $sql = "select orders_status.type_of_order,orders_status.id as orders_status_id,cancelled_orders.*,cancelled_orders.image,
       cancelled_orders.sku_id,
	   cancelled_orders.prev_order_item_id,
       products.product_name,
	   products.product_description,
       cancelled_orders.timestamp,
       cancelled_orders.order_id,
       orders_status.order_item_id,
       cancelled_orders.quantity,
       cancelled_orders.product_price,
       cancelled_orders.shipping_charge,
       cancelled_orders.subtotal,
	   cancelled_orders.grandtotal,
	   cancelled_orders.ord_addon_master_order_item_id,
       shipping_address.customer_name,
       shipping_address.address1,
       shipping_address.address2,
       shipping_address.country,
       shipping_address.state,
       shipping_address.city,
       shipping_address.pincode,
       shipping_address.mobile,invoices_offers.*,cancels.* from cancelled_orders,invoices_offers,orders_status,products,inventory,shipping_address,cancels
       where cancelled_orders.customer_id=orders_status.customer_id and 
	     cancelled_orders.order_item_id=orders_status.order_item_id and
	     cancelled_orders.customer_id=shipping_address.customer_id and
	     cancelled_orders.shipping_address_id=shipping_address.shipping_address_id and
             cancelled_orders.product_id=products.product_id and
             cancelled_orders.inventory_id=inventory.id and	
			 products.product_id=inventory.product_id and 
             orders_status.customer_id=shipping_address.customer_id and invoices_offers.order_id=cancelled_orders.order_id and cancels.order_item_id=cancelled_orders.order_item_id and 
             orders_status.order_cancelled='1' and cancelled_orders.prev_order_item_id='' $str";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( cancelled_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.subtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.customer_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address1 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address2 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.country LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.state LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.city LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.pincode LIKE '%".$params['search']['value']."%' ";
			
				$where .=" OR shipping_address.mobile LIKE '%".$params['search']['value']."%' )";
			}


			$user_type=$this->session->userdata("user_type");
			$a_id=$this->session->userdata("user_id");

			if($user_type=='vendor'){
				$where .=" AND ";
				$where .=" cancelled_orders.product_vendor_id= '$a_id' ";
			}

			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		///////////////////////
		public function get_status_of_transaction($order_item_id){
			$sql="select * from cancels where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row();
			}
		}
		
		public function get_data_from_active_orders($order_item_id){
			$sql="select * from active_orders where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		
		public function get_data_from_completed_orders($order_item_id){
			$sql="select * from completed_orders where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		
		public function get_vendor_info_by_vendor_id($vendor_id){
			$sql="select * from vendors where vendor_id='$vendor_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		
		public function get_shipping_add_info_by_shipping_address_id($shipping_address_id){
			$sql="select * from shipping_address where shipping_address_id='$shipping_address_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		
		public function get_customer_info_by_customer_id($customer_id){
			$sql="select * from customer where id='$customer_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		
		public function active_order_add_to_completed_orders($purchased_price,$order_item_id,$customer_id,$order_id,$shipping_address_id,$inventory_id,$product_id,$sku_id,$tax_percent,$product_price,$shipping_charge,$subtotal,$wallet_status,$wallet_amount,$grandtotal,$quantity,$timestamp,$image,$random_number,$invoice_email,$invoice_number,$logistics_name,$logistics_weblink,$tracking_number,$expected_delivery_date,$delivery_mode,$parcel_category,$vendor_id,$payment_type,$payment_status,$return_period,$logistics_id,$logistics_delivery_mode_id,$logistics_territory_id,$logistics_parcel_category_id,$promotion_available,$promotion_residual,$promotion_discount,$promotion_cashback,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$default_discount,$promotion_item_multiplier,$promotion_item,$promotion_item_num,$promotion_clubed_with_default_discount,$promotion_id_selected,$promotion_type_selected,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_without_promotion,$total_price_of_product_with_promotion,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$razorpayOrderId,$razorpayPaymentId,$ord_max_selling_price,$ord_selling_discount,$ord_tax_percent_price,$ord_taxable_price,$ord_sku_name,$ord_coupon_code,$ord_coupon_name,$ord_coupon_type,$ord_coupon_value,$ord_coupon_reduce_price,$ord_coupon_status,$ord_addon_products_status,$ord_addon_products,$ord_addon_inventories,$ord_addon_total_price,$ord_addon_single_or_multiple_tagged_inventories_in_frontend,$ord_addon_master_order_item_id,$product_vendor_id){

			$sql="insert into completed_orders (purchased_price,order_item_id,customer_id,order_id,shipping_address_id,inventory_id,product_id,sku_id,tax_percent,product_price,shipping_charge,subtotal,wallet_status,wallet_amount,grandtotal,quantity,timestamp,image,random_number,invoice_email,invoice_number,logistics_name,logistics_weblink,tracking_number,expected_delivery_date,delivery_mode,parcel_category,vendor_id,payment_type,payment_status,return_period,logistics_id,logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id,promotion_available,promotion_residual,promotion_discount,promotion_cashback,promotion_surprise_gift,promotion_surprise_gift_type,promotion_surprise_gift_skus,promotion_surprise_gift_skus_nums,default_discount,promotion_item_multiplier,promotion_item,promotion_item_num,promotion_clubed_with_default_discount,promotion_id_selected,promotion_type_selected,promotion_minimum_quantity,promotion_quote,promotion_default_discount_promo,individual_price_of_product_with_promotion,individual_price_of_product_without_promotion,total_price_of_product_without_promotion,total_price_of_product_with_promotion,quantity_without_promotion,quantity_with_promotion,cash_back_value,razorpayOrderId,razorpayPaymentId,ord_max_selling_price,ord_selling_discount,ord_tax_percent_price,ord_taxable_price,ord_sku_name,ord_coupon_code,ord_coupon_name,ord_coupon_type,ord_coupon_value,ord_coupon_reduce_price,ord_coupon_status,ord_addon_products_status,ord_addon_products,ord_addon_inventories,ord_addon_total_price,ord_addon_single_or_multiple_tagged_inventories_in_frontend,ord_addon_master_order_item_id,product_vendor_id) values('$purchased_price','$order_item_id','$customer_id','$order_id','$shipping_address_id','$inventory_id','$product_id','$sku_id','$tax_percent','$product_price','$shipping_charge','$subtotal','$wallet_status','$wallet_amount','$grandtotal','$quantity','$timestamp','$image','$random_number','$invoice_email','$invoice_number','$logistics_name','$logistics_weblink','$tracking_number','$expected_delivery_date','$delivery_mode','$parcel_category','$vendor_id','$payment_type','$payment_status','$return_period','$logistics_id','$logistics_delivery_mode_id','$logistics_territory_id','$logistics_parcel_category_id','$promotion_available','$promotion_residual','$promotion_discount','$promotion_cashback','$promotion_surprise_gift','$promotion_surprise_gift_type','$promotion_surprise_gift_skus','$promotion_surprise_gift_skus_nums','$default_discount','$promotion_item_multiplier','$promotion_item','$promotion_item_num','$promotion_clubed_with_default_discount','$promotion_id_selected','$promotion_type_selected','$promotion_minimum_quantity','$promotion_quote','$promotion_default_discount_promo','$individual_price_of_product_with_promotion','$individual_price_of_product_without_promotion','$total_price_of_product_without_promotion','$total_price_of_product_with_promotion','$quantity_without_promotion','$quantity_with_promotion','$cash_back_value','$razorpayOrderId','$razorpayPaymentId','$ord_max_selling_price','$ord_selling_discount','$ord_tax_percent_price','$ord_taxable_price','$ord_sku_name','$ord_coupon_code','$ord_coupon_name','$ord_coupon_type','$ord_coupon_value','$ord_coupon_reduce_price','$ord_coupon_status','$ord_addon_products_status','$ord_addon_products','$ord_addon_inventories','$ord_addon_total_price','$ord_addon_single_or_multiple_tagged_inventories_in_frontend','$ord_addon_master_order_item_id','$product_vendor_id')";
			
			
			$result=$this->db->query($sql);
			if($this->db->affected_rows() > 0){
				return true;
			}
			else{
				return false;
			}
		}
		
		public function get_data_cancels_table($order_item_id){
			$sql="select * from cancels where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		
		public function get_data_cancelled_orders_table($order_item_id){
			$sql="select cancelled_orders.*,invoices_offers.* from cancelled_orders,invoices_offers where cancelled_orders.order_item_id='$order_item_id' and invoices_offers.order_id=cancelled_orders.order_id";
			$result=$this->db->query($sql);
			return $result->row();
		}
		
		/*public function get_data_refund_table($cancel_id){
			$sql="select * from refund where cancel_id='$cancel_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}*/
		
		public function get_bank_details($id){
			$sql="select * from delivered_refund_bank_details where id='$id'";
			$result=$this->db->query($sql);
			
			if($result->num_rows()>0){
				//print_r($result->result_array());exit;
				return $result->result_array();
			}
			else{
				return false;
			}
		}
		
		public function cancels_issue_full_refund($order_item_id,$cancel_id,$total_refund_without_concession,$item_price_without_concession,$item_deduction_percentage,$shipping_price_without_concession,$shipping_deduction_percentage,$item_with_deductions_concession,$shipping_with_deductions_concession,$memo_to_buyer,$memo_to_seller,$refund_method,$refund_bank_id,$customer_id,$quantity_refunded,$razorpay_refund_id,$razorpay_refund_status,$razorpay_paymentid){
			
			$memo_to_buyer=$this->db->escape_str($memo_to_buyer);
			$memo_to_seller=$this->db->escape_str($memo_to_seller);
			
			$total_refund_with_concession=$item_with_deductions_concession+$shipping_with_deductions_concession;
			$sql="insert into cancel_refund set 
							order_item_id='$order_item_id',
							cancel_id='$cancel_id',
							total_refund_without_concession='$total_refund_without_concession',
							item_price_without_concession='$item_price_without_concession',
							item_deduction_percentage='$item_deduction_percentage',
							shipping_price_without_concession='$shipping_price_without_concession',
							shipping_deduction_percentage='$shipping_deduction_percentage',
							item_with_deductions_concession='$item_with_deductions_concession',
							shipping_with_deductions_concession='$shipping_with_deductions_concession',
							total_refund_with_concession='$total_refund_with_concession',
							memo_to_buyer='$memo_to_buyer',
							memo_to_seller='$memo_to_seller',
							refund_method='$refund_method',
							refund_bank_id='$refund_bank_id',
							customer_id='$customer_id',
							razorpay_refund_id='$razorpay_refund_id',
							razorpay_refund_status='$razorpay_refund_status',
							razorpay_payment_id='$razorpay_paymentid',
							type_of_refund='full_refund',quantity='$quantity_refunded'";
			$query=$this->db->query($sql);
			
			$sql_cancels="update cancels set status='refund' where order_item_id='$order_item_id'";
			$query_cancels=$this->db->query($sql_cancels);
			
			return $query;
		}
		
		public function cancels_issue_partial_refund($order_item_id,$cancel_id,$item_price_without_concession,$item_deduction_percentage,$shipping_price_without_concession,$shipping_deduction_percentage,$total_refund_without_concession,$item_with_deductions_concession,$shipping_with_deductions_concession,$total_refund_with_concession,$refund_method,$refund_bank_id,$customer_id,$memo_to_buyer_partial_refund,$memo_to_seller_partial_refund,$quantity_refunded,$razorpay_refund_id,$razorpay_refund_status,$razorpay_paymentid){
			
			$memo_to_buyer_partial_refund=$this->db->escape_str($memo_to_buyer_partial_refund);
			$memo_to_seller_partial_refund=$this->db->escape_str($memo_to_seller_partial_refund);
			
			$sql="insert into cancel_refund set 
							order_item_id='$order_item_id',
							cancel_id='$cancel_id',
							item_price_without_concession='$item_price_without_concession',
							item_deduction_percentage='$item_deduction_percentage',
							shipping_price_without_concession='$shipping_price_without_concession',
							shipping_deduction_percentage='$shipping_deduction_percentage',
							total_refund_without_concession='$total_refund_without_concession',
							item_with_deductions_concession='$item_with_deductions_concession',
							shipping_with_deductions_concession='$shipping_with_deductions_concession',
							total_refund_with_concession='$total_refund_with_concession',
							refund_method='$refund_method',
							refund_bank_id='$refund_bank_id',
							customer_id='$customer_id',
							memo_to_buyer='$memo_to_buyer_partial_refund',
							memo_to_seller='$memo_to_seller_partial_refund',
							razorpay_refund_id='$razorpay_refund_id',
							razorpay_refund_status='$razorpay_refund_status',
							razorpay_payment_id='$razorpay_paymentid',
							type_of_refund='partial_refund',quantity='$quantity_refunded'";
			$query=$this->db->query($sql);
			
			$sql_cancels="update cancels set status='refund' where order_item_id='$order_item_id'";
			$query_cancels=$this->db->query($sql_cancels);
			
			return $query;
		}
		
		public function cancels_refund_request_orders_accounts_incoming($order_item_id){ 
			$sql = "select orders_status.id as orders_status_id,cancelled_orders.image,
       cancelled_orders.sku_id,
       products.product_name,
	   products.product_description,
       cancelled_orders.timestamp,
       cancelled_orders.order_id,
       orders_status.order_item_id,
       cancelled_orders.quantity,
       cancelled_orders.product_price,
       cancelled_orders.shipping_charge,
       cancelled_orders.subtotal,
	    cancelled_orders.grandtotal,
       shipping_address.customer_name,
       shipping_address.address1,
       shipping_address.address2,
       shipping_address.country,
       shipping_address.state,
       shipping_address.city,
       shipping_address.pincode,
       shipping_address.mobile,
	   cancelled_orders.customer_id,
	   cancels.cancel_id,if(cancels.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') as type_of_transaction from cancelled_orders,orders_status,products,inventory,shipping_address,cancels
       where cancelled_orders.customer_id=orders_status.customer_id and 
	     cancelled_orders.order_item_id=orders_status.order_item_id and
	     cancelled_orders.customer_id=shipping_address.customer_id and
	     cancelled_orders.shipping_address_id=shipping_address.shipping_address_id and
             cancelled_orders.product_id=products.product_id and
             cancelled_orders.inventory_id=inventory.id and	
             orders_status.customer_id=shipping_address.customer_id and cancels.order_item_id=cancelled_orders.order_item_id and cancels.status='refund' and 
             orders_status.order_cancelled='1' and orders_status.order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function cancels_refund_request_orders_accounts_incoming_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'cancelled_orders.timestamp', 
				1 => 'cancelled_orders.subtotal',
				2 => 'shipping_address.customer_name'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select orders_status.id as orders_status_id,cancelled_orders.image,
       cancelled_orders.sku_id,
       products.product_name,
	   products.product_description,
       cancelled_orders.timestamp,
       cancelled_orders.order_id,
       orders_status.order_item_id,
       cancelled_orders.quantity,
       cancelled_orders.product_price,
       cancelled_orders.shipping_charge,
       cancelled_orders.subtotal,
	    cancelled_orders.grandtotal,
		cancelled_orders.ord_addon_master_order_item_id,
       shipping_address.customer_name,
       shipping_address.address1,
       shipping_address.address2,
       shipping_address.country,
       shipping_address.state,
       shipping_address.city,
       shipping_address.pincode,
       shipping_address.mobile,cancels.cancel_id,if(cancels.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') as type_of_transaction,razorpayPaymentId,return_order_id from cancelled_orders,orders_status,products,inventory,shipping_address,cancels
       where cancelled_orders.customer_id=orders_status.customer_id and 
	     cancelled_orders.order_item_id=orders_status.order_item_id and
	     cancelled_orders.customer_id=shipping_address.customer_id and
	     cancelled_orders.shipping_address_id=shipping_address.shipping_address_id and
             cancelled_orders.product_id=products.product_id and
             cancelled_orders.inventory_id=inventory.id and	
             orders_status.customer_id=shipping_address.customer_id and cancels.order_item_id=cancelled_orders.order_item_id and cancels.status='refund' and 
             orders_status.order_cancelled='1'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( cancelled_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.subtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.customer_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address1 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address2 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.country LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.state LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.city LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.pincode LIKE '%".$params['search']['value']."%' ";
				$where .=" OR if(cancels.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.mobile LIKE '%".$params['search']['value']."%' )";
			}


			$user_type=$this->session->userdata("user_type");
			$a_id=$this->session->userdata("user_id");

			if($user_type=='vendor'){
				$where .=" AND ";
				$where .=" cancelled_orders.product_vendor_id= '$a_id' ";
			}

			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		public function get_cancel_refund_details($order_item_id){
			$sql="select * from cancel_refund where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function cancel_refund_success($order_item_id,$transaction_id,$transaction_date,$transaction_time,$razorpay_refund_id='',$razorpay_refund_status=''){
			
			$sql="update cancels set status='refunded',status_of_refund='yes',transaction_id='$transaction_id',transaction_date='$transaction_date',transaction_time='$transaction_time',razorpay_refund_id='$razorpay_refund_id',razorpay_refund_status='$razorpay_refund_status' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			return $query;
		}
		
		public function cancel_refund_failure($order_item_id,$refund_fail_comments,$allow_update_bank_details){
			if($allow_update_bank_details){
				$allow_update_bank_details="yes";
			}
			else{
				$allow_update_bank_details="no";
			}
			$refund_fail_comments=$this->db->escape_str($refund_fail_comments);
			$sql="update cancels set status='not refunded',status_of_refund='no',refund_fail_comments='$refund_fail_comments',allow_update_bank_details='$allow_update_bank_details' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			return $query;
		}
		
		public function activate_wallet_customer($customer_id){
			$sql="select * from wallet where customer_id='$customer_id' ";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return false;
			}
			else{
				$sql="insert into wallet set customer_id='$customer_id'";
				$result=$this->db->query($sql);
				if($this->db->affected_rows() > 0){
						return true;
					}
					else{
						return false;
					}
			}
		}
		
		public function update_customer_wallet($wallet_id,$customer_id,$transaction_details,$debit,$credit,$amount,$credit_bank_transfer){
		if($credit_bank_transfer){
				$credit_bank_transfer=1;
		}else{
				$credit_bank_transfer=0;
		}
		$sql="insert into wallet_transaction (wallet_id,customer_id,transaction_details,debit,credit,amount,credit_bank_transfer) values('$wallet_id','$customer_id','$transaction_details','$debit','$credit','$amount','$credit_bank_transfer')";
		$result=$this->db->query($sql);
		$wallet_transaction_id=$this->db->insert_id();
		
		$sql="update wallet set wallet_amount='$amount' where wallet_id='$wallet_id' and customer_id='$customer_id'";
		$result=$this->db->query($sql);
		return $wallet_transaction_id;
			
	}
	
	
		
		public function cancels_refund_request_orders_refund_success_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'cancelled_orders.timestamp', 
				1 => 'cancelled_orders.subtotal',
				2 => 'shipping_address.customer_name'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select orders_status.id as orders_status_id,cancelled_orders.image,
       cancelled_orders.sku_id,
       products.product_name,
	   products.product_description,
       cancelled_orders.timestamp,
       cancelled_orders.order_id,
       orders_status.order_item_id,
       cancelled_orders.quantity,
       cancelled_orders.product_price,
       cancelled_orders.shipping_charge,
       cancelled_orders.subtotal,
	    cancelled_orders.grandtotal,
       shipping_address.customer_name,
       shipping_address.address1,
       shipping_address.address2,
       shipping_address.country,
       shipping_address.state,
       shipping_address.city,
       shipping_address.pincode,
       shipping_address.mobile,
	   cancels.cancel_id,
	   cancels.transaction_id,
	   cancels.transaction_date,
	   cancels.transaction_time,
	   if(cancels.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') as type_of_transaction from cancelled_orders,orders_status,products,inventory,shipping_address,cancels
       where cancelled_orders.customer_id=orders_status.customer_id and 
	     cancelled_orders.order_item_id=orders_status.order_item_id and
	     cancelled_orders.customer_id=shipping_address.customer_id and
	     cancelled_orders.shipping_address_id=shipping_address.shipping_address_id and
             cancelled_orders.product_id=products.product_id and
             cancelled_orders.inventory_id=inventory.id and	
             orders_status.customer_id=shipping_address.customer_id and cancels.order_item_id=cancelled_orders.order_item_id and cancels.status='refunded' and 
             orders_status.order_cancelled='1'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( cancelled_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.subtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.customer_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address1 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address2 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.country LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.state LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.city LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.pincode LIKE '%".$params['search']['value']."%' ";
				$where .=" OR if(cancels.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.mobile LIKE '%".$params['search']['value']."%' )";
			}

			$user_type=$this->session->userdata("user_type");
			$a_id=$this->session->userdata("user_id");

			if($user_type=='vendor'){
				$where .=" AND ";
				$where .=" cancelled_orders.product_vendor_id= '$a_id' ";
			}

			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		public function cancels_refund_request_orders_accounts_success($order_item_id){ 
			$sql = "select orders_status.id as orders_status_id,cancelled_orders.image,
       cancelled_orders.sku_id,
       products.product_name,
	   products.product_description,
       cancelled_orders.timestamp,
       cancelled_orders.order_id,
       orders_status.order_item_id,
       cancelled_orders.quantity,
       cancelled_orders.product_price,
       cancelled_orders.shipping_charge,
       cancelled_orders.subtotal,
	    cancelled_orders.grandtotal,
       shipping_address.customer_name,
       shipping_address.address1,
       shipping_address.address2,
       shipping_address.country,
       shipping_address.state,
       shipping_address.city,
       shipping_address.pincode,
       shipping_address.mobile,
	   cancels.cancel_id,
	   cancels.transaction_id,
	   cancels.transaction_date,
	   cancels.transaction_time,
	   if(cancels.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') as type_of_transaction from cancelled_orders,orders_status,products,inventory,shipping_address,cancels
       where cancelled_orders.customer_id=orders_status.customer_id and 
	     cancelled_orders.order_item_id=orders_status.order_item_id and
	     cancelled_orders.customer_id=shipping_address.customer_id and
	     cancelled_orders.shipping_address_id=shipping_address.shipping_address_id and
             cancelled_orders.product_id=products.product_id and
             cancelled_orders.inventory_id=inventory.id and	
             orders_status.customer_id=shipping_address.customer_id and cancels.order_item_id=cancelled_orders.order_item_id and cancels.status='refunded' and 
             orders_status.order_cancelled='1' and  orders_status.order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function cancels_refund_request_orders_accounts_success_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'cancelled_orders.timestamp', 
				1 => 'cancelled_orders.subtotal',
				2 => 'shipping_address.customer_name'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select orders_status.id as orders_status_id,cancelled_orders.image,
       cancelled_orders.sku_id,
       products.product_name,
	   products.product_description,
       cancelled_orders.timestamp,
       cancelled_orders.order_id,
       orders_status.order_item_id,
       cancelled_orders.quantity,
       cancelled_orders.product_price,
       cancelled_orders.shipping_charge,
       cancelled_orders.subtotal,
	    cancelled_orders.grandtotal,
       shipping_address.customer_name,
       shipping_address.address1,
       shipping_address.address2,
       shipping_address.country,
       shipping_address.state,
       shipping_address.city,
       shipping_address.pincode,
       shipping_address.mobile,
	   cancels.cancel_id,
	   cancels.transaction_id,
	   cancels.razorpay_refund_id,
	   cancels.razorpay_refund_status,
	   cancels.transaction_date,
	   cancels.transaction_time,
	   if(cancels.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') as type_of_transaction from cancelled_orders,orders_status,products,inventory,shipping_address,cancels
       where cancelled_orders.customer_id=orders_status.customer_id and 
	     cancelled_orders.order_item_id=orders_status.order_item_id and
	     cancelled_orders.customer_id=shipping_address.customer_id and
	     cancelled_orders.shipping_address_id=shipping_address.shipping_address_id and
             cancelled_orders.product_id=products.product_id and
             cancelled_orders.inventory_id=inventory.id and	
             orders_status.customer_id=shipping_address.customer_id and cancels.order_item_id=cancelled_orders.order_item_id and cancels.status='refunded' and 
             orders_status.order_cancelled='1'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( cancelled_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.subtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.customer_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address1 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address2 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.country LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.state LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.city LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.pincode LIKE '%".$params['search']['value']."%' ";
				$where .=" OR if(cancels.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.mobile LIKE '%".$params['search']['value']."%' )";
			}

			$user_type=$this->session->userdata("user_type");
			$a_id=$this->session->userdata("user_id");

			if($user_type=='vendor'){
				$where .=" AND ";
				$where .=" cancelled_orders.product_vendor_id= '$a_id' ";
			}

			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}

		public function resend_cancel_refund($order_item_id){
			$sql="update cancels set status='refund' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			$sql_acc="update cancel_refund set notify_cancel_account='1' where order_item_id='$order_item_id'";
			$query_acc=$this->db->query($sql_acc);
			
			
			return $query;
		}
		
		public function cancel_transfer_to_wallet($order_item_id){
			
			//$sql="update refund set refund_type='Wallet' where order_item_id='$order_item_id'";
			//$query=$this->db->query($sql);
			
			$sql="update cancels set status='refund' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			$sql="update cancel_refund set refund_method='Wallet',notify_cancel_account='1' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			return $query;
		}
		
		public function cancels_refund_request_orders_accounts_failure_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'cancelled_orders.timestamp', 
				1 => 'cancelled_orders.subtotal',
				2 => 'shipping_address.customer_name'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select orders_status.id as orders_status_id,cancelled_orders.image,
       cancelled_orders.sku_id,
       products.product_name,
	   products.product_description,
       cancelled_orders.timestamp,
       cancelled_orders.order_id,
       orders_status.order_item_id,
       cancelled_orders.quantity,
       cancelled_orders.product_price,
       cancelled_orders.shipping_charge,
       cancelled_orders.subtotal,
	    cancelled_orders.grandtotal,
       shipping_address.customer_name,
       shipping_address.address1,
       shipping_address.address2,
       shipping_address.country,
       shipping_address.state,
       shipping_address.city,
       shipping_address.pincode,
       shipping_address.mobile,
	   cancels.cancel_id,
	   cancels.transaction_id,
	   cancels.transaction_date,
	   cancels.transaction_time,
	   cancels.refund_fail_comments,
	   cancels.allow_update_bank_details,
	   if(cancels.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') as type_of_transaction from cancelled_orders,orders_status,products,inventory,shipping_address,cancels
       where cancelled_orders.customer_id=orders_status.customer_id and 
	     cancelled_orders.order_item_id=orders_status.order_item_id and
	     cancelled_orders.customer_id=shipping_address.customer_id and
	     cancelled_orders.shipping_address_id=shipping_address.shipping_address_id and
             cancelled_orders.product_id=products.product_id and
             cancelled_orders.inventory_id=inventory.id and	
             orders_status.customer_id=shipping_address.customer_id and cancels.order_item_id=cancelled_orders.order_item_id and cancels.status='not refunded' and 
             orders_status.order_cancelled='1'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( cancelled_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR cancelled_orders.subtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.customer_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address1 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address2 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.country LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.state LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.city LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.pincode LIKE '%".$params['search']['value']."%' ";
				$where .=" OR if(cancels.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.mobile LIKE '%".$params['search']['value']."%' )";
			}

			$user_type=$this->session->userdata("user_type");
			$a_id=$this->session->userdata("user_id");

			if($user_type=='vendor'){
				$where .=" AND ";
				$where .=" cancelled_orders.product_vendor_id= '$a_id' ";
			}

			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
	public function get_shipping_address_id($order_item_id){
		$sql="select shipping_address_id from history_orders where order_item_id='$order_item_id' limit 0,1";
		$query =$this->db->query($sql);
		return $query->row()->shipping_address_id;
	}
	public function get_shipping_address($shipping_address_id)
	{
		$sql="select * from shipping_address where shipping_address_id='$shipping_address_id'";
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	
	public function get_order_summary_mail_data($order_item_id){
		$sql="select history_orders.*,inventory.thumbnail,products.product_name,products.product_description,inventory.attribute_1,inventory.attribute_1_value,inventory.attribute_2,inventory.attribute_2_value,inventory.attribute_3,inventory.attribute_3_value,inventory.attribute_4,inventory.attribute_4_value from history_orders,inventory,products where history_orders.inventory_id=inventory.id and products.product_id=inventory.product_id and history_orders.order_item_id='$order_item_id'";
		$query =$this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_email_db($cust_id){
		$sql="select email from customer where  id='$cust_id'";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$email = $result['email'];
		return $email;
	}
	public function get_mobile_db($cust_id){
		$sql="select mobile from customer where  id='$cust_id'";
		$query =$this->db->query($sql);
		$result = $query->row_array();
		$mobile = $result['mobile'];
		return $mobile;
	}
	
	public function get_order_item_id_list_by_orders_status_id($orders_status_id_list){
		$orders_status_id_list_arr=explode("-",$orders_status_id_list);
		$orders_status_id_list_in="'".implode("','",$orders_status_id_list_arr)."'";
		$sql="select orders_status.order_item_id,active_orders.order_id,active_orders.customer_id,active_orders.prev_order_item_id from orders_status,active_orders where orders_status.id in ($orders_status_id_list_in) and active_orders.order_item_id=orders_status.order_item_id";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function set_order_cancellation_policy($order_item_id,$order_id,$flag){
		$sql="update order_cancellation_policy set order_status_type='$flag' where order_item_id='$order_item_id' and order_id='$order_id'";
		$query=$this->db->query($sql);
	}
	public function get_refunded_details_from_returned_orders_table($order_item_id){
		$order_return_decision_obj=$this->get_order_return_decision_data($order_item_id);
		
		if(!empty($order_return_decision_obj)){
			if($order_return_decision_obj->status=="refunded"){
				//$sql="select * from return_refund where order_item_id='$order_item_id'";
				
				$sql="select return_refund.*,returns_desired.individual_price_of_product_with_promotion,returns_desired.individual_price_of_product_without_promotion,returns_desired.total_price_of_product_with_promotion,returns_desired.total_price_of_product_without_promotion,returns_desired.cash_back_value,returns_desired.cash_back_value,returns_desired.order_item_invoice_discount_value_each from return_refund,returns_desired where return_refund.return_order_id=returns_desired.return_order_id and  return_refund.order_item_id=returns_desired.order_item_id and return_refund.order_item_id='$order_item_id'";
				$query=$this->db->query($sql);
				return $query->row_array();
			}else{
				return array();
			}
		}else{
			return array();
		}
	}
	
	public function get_data_from_history_orders($order_item_id){
		$sql="select * from history_orders where order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		return $result->row();
	}
	
	public function get_the_type_of_order_fresh_or_replaced($order_item_id){
		$sql="select type_of_order from orders_status where order_item_id='$order_item_id'";
		$result=$this->db->query($sql);
		return $result->row()->type_of_order;
	}
	public function active_order_add_to_replaced_orders($purchased_price,$prev_order_item_id,$order_item_id,$customer_id,$order_id,$shipping_address_id,$inventory_id,$product_id,$sku_id,$tax_percent,$product_price,$shipping_charge,$subtotal,$wallet_status,$wallet_amount,$grandtotal,$quantity,$timestamp,$image,$random_number,$invoice_email,$invoice_number,$logistics_name,$logistics_weblink,$tracking_number,$expected_delivery_date,$delivery_mode,$parcel_category,$vendor_id,$payment_type,$payment_status,$return_period,$logistics_id,$logistics_delivery_mode_id,$logistics_territory_id,$logistics_parcel_category_id,$promotion_available,$promotion_residual,$promotion_discount,$promotion_cashback,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$default_discount,$promotion_item_multiplier,$promotion_item,$promotion_item_num,$promotion_clubed_with_default_discount,$promotion_id_selected,$promotion_type_selected,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_without_promotion,$total_price_of_product_with_promotion,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$razorpayOrderId,$razorpayPaymentId,$ord_max_selling_price,$ord_selling_discount,$ord_tax_percent_price,$ord_taxable_price,$ord_sku_name,$ord_coupon_code,$ord_coupon_name,$ord_coupon_type,$ord_coupon_value,$ord_coupon_reduce_price,$ord_coupon_status,$ord_addon_products_status,$ord_addon_products,$ord_addon_inventories,$ord_addon_total_price,$ord_addon_single_or_multiple_tagged_inventories_in_frontend,$ord_addon_master_order_item_id,$product_vendor_id){
			
			/*$sql_orders_status="update orders_status set order_return_pickup='1' where order_item_id='$order_item_id'";
			$this->db->query($sql_orders_status);*/	
			
			$sql_update_replacement_desired="update replacement_desired set order_replacement_decision_status='replaced' where order_item_id='$prev_order_item_id'";
			$this->db->query($sql_update_replacement_desired);
			
			$sql_update_order_replacement_decision="update order_replacement_decision set status='replaced' where order_item_id='$prev_order_item_id'";
			$this->db->query($sql_update_order_replacement_decision);
			
			$replacement_desired_data_obj=$this->get_replacement_desired_data($prev_order_item_id);
			if($replacement_desired_data_obj->paid_by=="customer" && $replacement_desired_data_obj->balance_amount_paid_by=="cod"){
				$sql_update_replacement_desired="update replacement_desired set payment_status='paid' where order_item_id='$prev_order_item_id'";
				$this->db->query($sql_update_replacement_desired);
			}
			
			$sql="insert into replaced_orders (purchased_price,prev_order_item_id,order_item_id,customer_id,order_id,shipping_address_id,inventory_id,product_id,sku_id,tax_percent,product_price,shipping_charge,subtotal,wallet_status,wallet_amount,grandtotal,quantity,timestamp,image,random_number,invoice_email,invoice_number,logistics_name,logistics_weblink,tracking_number,expected_delivery_date,delivery_mode,parcel_category,vendor_id,payment_type,payment_status,return_period,logistics_id,logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id,promotion_available,promotion_residual,promotion_discount,promotion_cashback,promotion_surprise_gift,promotion_surprise_gift_type,promotion_surprise_gift_skus,promotion_surprise_gift_skus_nums,default_discount,promotion_item_multiplier,promotion_item,promotion_item_num,promotion_clubed_with_default_discount,promotion_id_selected,promotion_type_selected,promotion_minimum_quantity,promotion_quote,promotion_default_discount_promo,individual_price_of_product_with_promotion,individual_price_of_product_without_promotion,total_price_of_product_without_promotion,total_price_of_product_with_promotion,quantity_without_promotion,quantity_with_promotion,cash_back_value,razorpayOrderId,razorpayPaymentId,ord_max_selling_price,ord_selling_discont,ord_tax_percent_price,ord_taxable_price,ord_sku_name,ord_coupon_code,ord_coupon_name,ord_coupon_type,ord_coupon_value,ord_coupon_reduce_price,ord_coupon_status,ord_addon_products_status,ord_addon_products,ord_addon_inventories,ord_addon_total_price,ord_addon_single_or_multiple_tagged_inventories_in_frontend,ord_addon_master_order_item_id,product_vendor_id) values('$purchased_price','$prev_order_item_id','$order_item_id','$customer_id','$order_id','$shipping_address_id','$inventory_id','$product_id','$sku_id','$tax_percent','$product_price','$shipping_charge','$subtotal','$wallet_status','$wallet_amount','$grandtotal','$quantity','$timestamp','$image','$random_number','$invoice_email','$invoice_number','$logistics_name','$logistics_weblink','$tracking_number','$expected_delivery_date','$delivery_mode','$parcel_category','$vendor_id','$payment_type','$payment_status','$return_period','$logistics_id','$logistics_delivery_mode_id','$logistics_territory_id','$logistics_parcel_category_id','$promotion_available','$promotion_residual','$promotion_discount','$promotion_cashback','$promotion_surprise_gift','$promotion_surprise_gift_type','$promotion_surprise_gift_skus','$promotion_surprise_gift_skus_nums','$default_discount','$promotion_item_multiplier','$promotion_item','$promotion_item_num','$promotion_clubed_with_default_discount','$promotion_id_selected','$promotion_type_selected','$promotion_minimum_quantity','$promotion_quote','$promotion_default_discount_promo','$individual_price_of_product_with_promotion','$individual_price_of_product_without_promotion','$total_price_of_product_without_promotion','$total_price_of_product_with_promotion','$quantity_without_promotion','$quantity_with_promotion','$cash_back_value','$razorpayOrderId','$razorpayPaymentId','$ord_max_selling_price','$ord_selling_discount','$ord_tax_percent_price','$ord_taxable_price','$ord_sku_name','$ord_coupon_code','$ord_coupon_name','$ord_coupon_type','$ord_coupon_value','$ord_coupon_reduce_price','$ord_coupon_status','$ord_addon_products_status','$ord_addon_products','$ord_addon_inventories','$ord_addon_total_price','$ord_addon_single_or_multiple_tagged_inventories_in_frontend','$ord_addon_master_order_item_id','$product_vendor_id')";			
			
			$result=$this->db->query($sql);
			if($this->db->affected_rows() > 0){
				return true;
			}
			else{
				return false;
			}
		}
		
		public function get_order_replacement_decision_data($order_item_id){
			$sql="select admin_replacement_reason.pickup_identifier,order_replacement_decision.*,replacement_desired.replacement_value_each_inventory_id,replacement_desired.return_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.paid_by,replacement_desired.payment_status,replacement_desired.order_item_invoice_discount_value_each from order_replacement_decision,admin_replacement_reason,replacement_desired where admin_replacement_reason.id=order_replacement_decision.sub_reason_return_id and order_replacement_decision.order_item_id='$order_item_id' and replacement_desired.return_order_id=order_replacement_decision.return_order_id and replacement_desired.order_item_id=order_replacement_decision.order_item_id";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function get_replacement_desired_data($order_item_id){
			$sql="select * from replacement_desired where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function get_orders_status_data($order_item_id){
			$sql="select * from orders_status where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function get_orders_status_data_of_replaced_item($prev_order_item_id){
			$sql="select * from orders_status where prev_order_item_id='$prev_order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function returns_replacement_request_orders_accounts_success($order_item_id){
			$sql = "select history_orders.sku_id,history_orders.inventory_id,products.product_name,products.product_description,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.payment_status,replacement_desired.shipping_charge_applied_cancels_chk,replacement_desired.return_shipping_concession_cancels_chk,order_replacement_decision.refund_status from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return,order_replacement_decision where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and 
			order_replacement_decision.order_item_id = replacement_desired.order_item_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and ((replacement_desired.order_replacement_decision_status='refunded' or replacement_desired.order_replacement_decision_refund_status='refunded' or replacement_desired.order_replacement_decision_status='replaced' or replacement_desired.order_replacement_decision_status='refund') and (replacement_desired.status_of_refund='yes' or (replacement_desired.status_of_refund='' and (replacement_desired.order_replacement_decision_status='replaced' or replacement_desired.order_replacement_decision_status='refund')))) and replacement_desired.order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin($order_item_id){
			$sql = "select history_orders.sku_id,history_orders.inventory_id,products.product_name,products.product_description,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.payment_status,replacement_desired.shipping_charge_applied_cancels_chk,replacement_desired.return_shipping_concession_cancels_chk,order_replacement_decision.refund_status from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return,order_replacement_decision where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and 
			order_replacement_decision.order_item_id = replacement_desired.order_item_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and replacement_desired.paid_by='customer' and replacement_desired.payment_status='paid' and (replacement_desired.order_replacement_decision_status='replaced' or replacement_desired.order_replacement_decision_status='refund' or replacement_desired.order_replacement_decision_status='refunded') and replacement_desired.status_of_refund='' and order_replacement_decision.return_shipping_concession_chk='no' and replacement_desired.order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function get_availability_of_order_item_id_in_refund_and_replacement($order_item_id){
			$sql_returns_desired="select * from returns_desired where order_item_id='$order_item_id'";
			$query_returns_desired=$this->db->query($sql_returns_desired);
			
			$sql_replacement_desired="select * from replacement_desired where order_item_id='$order_item_id'";
			$query_replacement_desired=$this->db->query($sql_replacement_desired);
			
			if($query_returns_desired->num_rows()>0 && $query_replacement_desired->num_rows()>0){
				return "yes";
			}
			else{
				return "no";
			}
		}
		
		public function get_sub_refunded_details_from_returned_orders_table($order_item_id){
			
			$sql_sub_returns_desired="select * from sub_returns_desired where order_item_id='$order_item_id'";
			$query_sub_returns_desired=$this->db->query($sql_sub_returns_desired);
			$query_sub_returns_desired_arr=$query_sub_returns_desired->row_array();
			if(!empty($query_sub_returns_desired_arr)){
				if($query_sub_returns_desired_arr["admin_action"]=="refunded"){
					$sql="select * from sub_return_refund where order_item_id='$order_item_id'";
					$query=$this->db->query($sql);
					return $query->row_array();
				}
				else{
					return array();
				}
			}
			else{
				return array();
			}
		}
		
		public function get_order_return_decision_data($order_item_id){
			$sql="select * from order_return_decision where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}

		
		
		public function get_admin_order_cancel_reasons(){
			$sql="select * from admin_cancel_reason";
			$result=$this->db->query($sql);
			return $result->result_array();
		}
	
		public function get_replaced_orders($order_item_id){
			$sql="select * from replaced_orders where prev_order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result->row_array();
		}
		
		public function returned_orders_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'history_orders.timestamp', 
				1 => 'history_orders.subtotal',
				2 => 'shipping_address.customer_name'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select orders_status.type_of_order,orders_status.id as orders_status_id,history_orders.*,history_orders.image,
       history_orders.sku_id,
       products.product_name,
	   products.product_description,
       history_orders.timestamp,
       history_orders.order_id,
       orders_status.order_item_id,
       orders_status.order_replacement_pickup,
       orders_status.order_delivered_date,
       orders_status.order_delivered_time,
       history_orders.quantity,
       history_orders.product_price,
       history_orders.shipping_charge,
       history_orders.subtotal,
	   history_orders.grandtotal,
       shipping_address.customer_name,
       shipping_address.address1,
       shipping_address.address2,
       shipping_address.country,
       shipping_address.state,
       shipping_address.city,
       shipping_address.pincode,
       shipping_address.mobile,invoices_offers.* from history_orders,invoices_offers,orders_status,products,inventory,shipping_address
       where history_orders.customer_id=orders_status.customer_id and 
	     history_orders.order_item_id=orders_status.order_item_id and
	     history_orders.customer_id=shipping_address.customer_id and
	     history_orders.shipping_address_id=shipping_address.shipping_address_id and
             history_orders.product_id=products.product_id and
             history_orders.inventory_id=inventory.id and	
             orders_status.customer_id=shipping_address.customer_id and invoices_offers.order_id=history_orders.order_id and
             orders_status.order_placed='1' and 
             orders_status.order_confirmed='1' and 
             orders_status.order_packed='1' and 
             orders_status.order_shipped='1' and 
             orders_status.order_delivered='1' and 
             orders_status.order_cancelled='0'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( history_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.subtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.customer_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address1 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address2 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.country LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.state LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.city LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.pincode LIKE '%".$params['search']['value']."%' ";
			
				$where .=" OR shipping_address.mobile LIKE '%".$params['search']['value']."%' )";
			}

			$user_type=$this->session->userdata("user_type");
			$a_id=$this->session->userdata("user_id");

			if($user_type=='vendor'){
				$where .=" AND ";
				$where .=" history_orders.product_vendor_id= '$a_id' ";
			}

			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		function set_as_cancelled_for_shipped($list_to_be_cancelled_order_item_arr,$reason_for_cancel_order,$cancel_reason){
			foreach($list_to_be_cancelled_order_item_arr as $order_item_id){
				
				$reason_for_cancel_order=$this->db->escape_str($reason_for_cancel_order);
				$type_of_replacement_cancel="refund_back";
				$sql="update replacement_desired set order_replacement_decision_customer_status='cancel',order_replacement_decision_customer_status_comments='$reason_for_cancel_order',order_replacement_decision_refund_status='need_to_refund',type_of_replacement_cancel='$type_of_replacement_cancel',reason_for_undelivered='$cancel_reason' where order_item_id='$order_item_id'";
				$result=$this->db->query($sql);
				
				$sql_order_return_decision="update order_replacement_decision set customer_status='cancel',customer_status_comments='$reason_for_cancel_order',refund_status='need_to_refund',reason_for_undelivered='$cancel_reason' where order_item_id='$order_item_id'";
				$result_order_return_decision=$this->db->query($sql_order_return_decision);
			}
			return $result;
		}
		public function get_prev_order_item_id_by_orders_status_id($orders_status_id){
			$sql="select prev_order_item_id from orders_status where id='$orders_status_id'";
			$query=$this->db->query($sql);
			$order_item_id=$query->row()->prev_order_item_id;
			return $order_item_id;
		}
		public function get_cancel_reason($cancel_reason_id){
			$sql="select cancel_reason from admin_cancel_reason where cancel_reason_id='$cancel_reason_id'";
			$query=$this->db->query($sql);
			$order_item_id=$query->row()->cancel_reason;
			return $order_item_id;
		}
		public function get_cancelled_order_customer_payment_type($order_item_id){
			$sql="select payment_type from cancelled_orders where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row()->payment_type;
			}
			else{
				return false;
			}
		}
		public function check_request_is_there_for_this_order($order_item_id){
			 $sql="select * from replacement_desired where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				$flag1="yes";
			}else{
				$flag1="no";
			}
			 $sql2="select * from returns_desired where order_item_id='$order_item_id'";
			$result2=$this->db->query($sql2);
			if($result2->num_rows()>0){
				$flag2="yes";
			}else{
				$flag2="no";
			}
			if($flag2=="yes" || $flag1=="yes"){
				return "yes";
			}else{
				return "no";
			}
		}
		public function get_data_from_invoice_offers($order_id){
			
			$sql="select * from invoices_offers where order_id='$order_id'";
			$result=$this->db->query($sql);
			return $result->row();
			
		}

		public function get_shipping_charge_and_grand_total_from_history_orders($order_item_id){
			
			$sql="select shipping_charge,grandtotal,quantity from history_orders where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		public function get_replaced_item_price_individual($order_item_id){
			
			$sql="select replacement_value_each_inventory_id from replacement_desired where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result->row()->replacement_value_each_inventory_id;
		}
		public function get_cancel_reason_to_check($cancel_reason_id){
			$sql="select cancel_reason from admin_cancel_reason where cancel_reason_id='$cancel_reason_id'";
			$result=$this->db->query($sql);
			return $result->row()->cancel_reason;
		}
		
		public function add_stocks_of_inventory($inventory_id,$purchased_quantity,$promotion_available,$promotion_item,$promotion_item_num,$promotion_minimum_quantity,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$ord_addon_products_status,$ord_addon_inventories){

			$str="";
			
			if($promotion_available==1){
				
				if(!empty($promotion_item)){
					
					$promo_item=$promotion_item;
					$promotion_item_num=$promotion_item_num;
					
					$promo_item_arr = explode(',', $promo_item);
					$promo_item_num_arr = explode(',', $promotion_item_num);
					
					$two=array_combine($promo_item_arr,$promo_item_num_arr);
		
					$temp_quotient=intval($purchased_quantity/$promotion_minimum_quantity);
	
					foreach($promo_item_arr as $prmo_item){
						
						$free_inv_id=intval($prmo_item);
						$reduce_count=intval($temp_quotient*$two[$prmo_item]);
						
						$sql_free_item="update inventory set stock = (stock+'$reduce_count') where id='$free_inv_id'; ";
						$result=$this->db->query($sql_free_item);
					}
					
				}
				if(!empty($promotion_surprise_gift_skus)){

					if($promotion_surprise_gift_type=="Qty"){
						
						$promotion_surprise_gift_skus=rtrim($promotion_surprise_gift_skus,',');
						$promotion_surprise_gift_skus_nums=rtrim($promotion_surprise_gift_skus_nums,',');
						
						$promo_gift_arr = explode(',', $promotion_surprise_gift_skus);
						$promo_gift_num_arr = explode(',', $promotion_surprise_gift_skus_nums);
						
						$two=array_combine($promo_gift_arr,$promo_gift_num_arr);
						foreach($two as $inv_id=>$stock_count){
							$sql_free_item_gift="update inventory set stock = (stock+$stock_count) where id='$inv_id'";
							$result=$this->db->query($sql_free_item_gift);
						}
					}
	
				}
				
			}
			
			$sql_inv="update inventory set stock = (stock+'$purchased_quantity') where id='$inventory_id'; ";
			$result=$this->db->query($sql_inv);

			/* addon section */
			if($ord_addon_products_status=='1'){
				if($ord_addon_inventories!=''){
					$addon_arr=explode(',',$ord_addon_inventories);
					$addon_arr=array_filter($addon_arr);
					
					if(!empty($addon_arr)){
						
						foreach($addon_arr as $inv_id){
							if($inv_id!=''){
								$sql_1="update inventory set stock = (stock+1) where id='$inv_id'; ";
								$result_1=$this->db->query($sql_1);
							}
						}
					}
				}
			}
			/* addon section */
			
			return $result;

		}
		
		public function get_sku_ids($promo_gift_arr){
			$sql="select id,sku_id from inventory where id in ($promo_gift_arr)";
			$result=$this->db->query($sql);
			
			$row_res=$result->result_array();
			$one=array();
			$two=array();
			
			foreach($row_res as $v){
				foreach($v as $r=>$n){
					if($r=="id"){
						$one[]=$n;
					}
					if($r=="sku_id"){
						$two[]=$n;
					}
				}
			}
			
			$three=array_combine($one,$two);
			return $three;
		}
		/*low stock notification*/
		public function get_low_stock_inventory(){
			$sql="select inventory.*,products.*,parent_category.pcat_name,category.cat_name,subcategory.subcat_name,brands.brand_name from parent_category,category,subcategory,brands,products,inventory where inventory.stock<=inventory.low_stock and inventory.product_id=products.product_id and products.pcat_id=parent_category.pcat_id and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id";
			$query_res=$this->db->query($sql);
			return $query_res->result();
			
		}
		public function low_stock_notification_processing($params,$fg){

			$where = $sqlTot = $sqlRec = "";

			$sql="select inventory.*,products.*,parent_category.pcat_name,category.cat_name,subcategory.subcat_name,brands.brand_name from parent_category,category,subcategory,brands,products,inventory where inventory.stock<=inventory.low_stock and inventory.product_id=products.product_id and  products.pcat_id=parent_category.pcat_id and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and products.pcat_id!='0' 
union 
select inventory.*,products.*,'' as pcat_name,category.cat_name,subcategory.subcat_name,brands.brand_name from category,subcategory,brands,products,inventory where inventory.stock<=inventory.low_stock and inventory.product_id=products.product_id and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and products.pcat_id='0'";	

			if(!empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( sku_id LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" OR product_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" OR cat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" OR pcat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" OR subcat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" OR brand_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
			}
			
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			$sqlRec .=  " order by stock asc";
			//echo $sqlRec;
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}	
		}
		public function get_stock_available_or_not_by_inventory($sku_id,$inventory_id){
			$sql="select stock,low_stock from inventory where id='$inventory_id' and sku_id='$sku_id'";
			$result=$this->db->query($sql);
			$resObj=$result->row();
			if(!empty($resObj)){
				$stock=$resObj->stock;
				$low_stock=$resObj->low_stock;
				if($stock>=$low_stock){
					return "yes";
				}
				else{
					return "no";
				}
			}
			else{
				return "no";
			}
		}
		
		public function update_inventory_stock($inventory_id,$stock){
			$sql="update inventory set stock='$stock' where id='$inventory_id'";
			$result=$this->db->query($sql);
			
			$sql_re_stock="update return_stock_notification set notify_stock_notification='1' where replacement_with_inventory_id='$inventory_id'";
			$result_re=$this->db->query($sql_re_stock);
			
			return $result;
		}
		public function mail_notification_btn_show_or_hide($inventory_id){
			$sql="select count(*) as count from request_for_out_of_stock where inventory_id='$inventory_id' and email!='' and mail_sent=0";
			$result=$this->db->query($sql);
			return $result->row_array()["count"];
		}
		public function mobile_notification_btn_show_or_hide($inventory_id){
			$sql="select count(*) as count from request_for_out_of_stock where inventory_id='$inventory_id' and mobile!='' and mobile_sent=0";
			$result=$this->db->query($sql);
			return $result->row_array()["count"];
		}
		public function request_for_out_of_stock_processing($params,$fg){
			$where = $sqlTot = $sqlRec = "";
			$str='';
			
			/*if($status=="mail_sent"){
				$str.=' and ((request_for_out_of_stock.mail_sent=1 and request_for_out_of_stock.mobile_sent=0) or (request_for_out_of_stock.mail_sent=0 and request_for_out_of_stock.mobile_sent=1))';
			}
			if($status=="mail_not_sent"){
				$str.=' and (request_for_out_of_stock.mail_sent=0 and request_for_out_of_stock.mobile_sent=0) and inventory.stock > inventory.low_stock ';

			}
			if($status=="out_of_stock"){
				$str.=' and inventory.stock<=inventory.low_stock ';
			}*/
			 

			$sql="select request_for_out_of_stock.*,count(request_for_out_of_stock.sku_id) AS request_count,inventory.*,products.*,parent_category.pcat_name,category.cat_name,subcategory.subcat_name,brands.brand_name from parent_category,category,subcategory,brands,products,inventory,request_for_out_of_stock where inventory.product_id=products.product_id and products.pcat_id=parent_category.pcat_id and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and request_for_out_of_stock.inventory_id=inventory.id and products.pcat_id!='0' and request_for_out_of_stock.sku_id=inventory.sku_id $str GROUP BY request_for_out_of_stock.sku_id union select request_for_out_of_stock.*,count(request_for_out_of_stock.sku_id) AS request_count,inventory.*,products.*,'' as pcat_name,category.cat_name,subcategory.subcat_name,brands.brand_name from category,subcategory,brands,products,inventory,request_for_out_of_stock where inventory.product_id=products.product_id  and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and request_for_out_of_stock.inventory_id=inventory.id and products.pcat_id='0' and request_for_out_of_stock.sku_id=inventory.sku_id $str GROUP BY request_for_out_of_stock.sku_id";
			
			
			if(!empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( sku_id LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" OR product_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" OR pcat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" OR cat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" OR subcat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" OR email LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" OR brand_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
			}
			
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			$sqlRec .=  " order by stock asc";
			//echo $sqlRec;
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		public function get_list_of_emails($sku_id,$inventory_id){
			$sql="select email from request_for_out_of_stock where mobile='' and sku_id='$sku_id' and inventory_id='$inventory_id'";
			$res=$this->db->query($sql);
			return $res->result();
			
		}
		public function get_list_of_mobiles($sku_id,$inventory_id){
			$sql="select mobile from request_for_out_of_stock where email='' and sku_id='$sku_id' and inventory_id='$inventory_id'";
			$res=$this->db->query($sql);
			return $res->result();
			
		}
		public function update_notification_group_id($inventory_id,$rn){
			$sql="update request_for_out_of_stock set group_id='$rn' where inventory_id='$inventory_id' and group_id=''";
			$result=$this->db->query($sql);
			return $result;
		}
		public function completed_notification_group_id($inventory_id,$rn){
			$sql="insert into completed_request_for_out_of_stock(customer_id,inventory_id,sku_id,email,mail_sent,mail_sent_timestamp,mobile,mobile_sent,mobile_sent_timestamp,timestamp,group_id) select customer_id,inventory_id,sku_id,email,mail_sent,mail_sent_timestamp,mobile,mobile_sent,mobile_sent_timestamp,timestamp,group_id from request_for_out_of_stock where group_id='$rn' and inventory_id='$inventory_id'";
			$result=$this->db->query($sql);
			
			$sql_delete="delete from request_for_out_of_stock where group_id='$rn' and inventory_id='$inventory_id'";
			$result_delete=$this->db->query($sql_delete);
			return $result_delete;
		}
		public function update_mailsent_in_request_for_out_of_stock($inventory_id,$sku_id){
			$sql="update request_for_out_of_stock set mail_sent=1,mail_sent_timestamp=now() where sku_id='$sku_id' and inventory_id='$inventory_id' and email!='' and mobile='' and mail_sent!='1'";
			$result=$this->db->query($sql);
			return $result;
		}
		
		public function update_mobilesent_in_request_for_out_of_stock($inventory_id,$sku_id){
			$sql="update request_for_out_of_stock set mobile_sent=1,mobile_sent_timestamp=now() where sku_id='$sku_id' and inventory_id='$inventory_id' and email='' and mobile!='' and mobile_sent!='1'";
			$result=$this->db->query($sql);
			return $result;
		}

		public function update_wallet($wallet_id,$customer_id,$transaction_details,$debit,$credit,$amount,$credit_bank_transfer){

			$sql="insert into wallet_transaction (wallet_id,customer_id,transaction_details,debit,credit,amount,credit_bank_transfer) values('$wallet_id','$customer_id','$transaction_details','$debit','$credit','$amount','$credit_bank_transfer')";
			$result=$this->db->query($sql);
			
			$sql_w="update wallet set wallet_amount='$amount' where wallet_id='$wallet_id' and customer_id='$customer_id'";
			$result=$this->db->query($sql_w);
			
			return $result;
			
		}
		public function get_free_skus_from_inventory($promo_item_str){
		
			 $sql="select products.product_name,products.product_description,inventory.sku_id,inventory.id,inventory.attribute_1,inventory.attribute_1_value,inventory.attribute_2,inventory.attribute_2_value,inventory.attribute_3,inventory.attribute_3_value,inventory.attribute_4,inventory.attribute_4_value,inventory.thumbnail,inventory.image from inventory,products where inventory.id in ($promo_item_str) and inventory.product_id=products.product_id";
			
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->result();
			}else{
				return false;
			}
		}
		public function check_amount_paid_or_not($order_id,$invoice_surprise_gift){
			
			//$sql="select * from rated_inventory where inventory_id='$inventory_id' and status=1 ORDER BY `rating` DESC limit 0,10";
			
			$sql="select count(*) as count from wallet_transaction where transaction_details LIKE '%$order_id%' and credit='$invoice_surprise_gift' ORDER BY `timestamp` DESC limit 0,1";
			
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row()->count;
			}else{
				return false;
			}
		}
		public function check_amount_debit_or_not($order_id,$invoice_surprise_gift){
			
			//$sql="select * from rated_inventory where inventory_id='$inventory_id' and status=1 ORDER BY `rating` DESC limit 0,10";
			
			$sql="select count(*) as count from wallet_transaction where transaction_details LIKE '%$order_id%' and debit='$invoice_surprise_gift' ORDER BY `timestamp` DESC limit 0,1";
			
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row()->count;
			}else{
				return false;
			}
		}
		public function check_all_order_items_canceled_report($order_id){
			$cancelled=0;
			
			$sql_invoice="select * from invoices_offers where order_id='$order_id'";
			$result_invoice=$this->db->query($sql_invoice);
			
			$order_item_count=$result_invoice->row()->cart_quantity;
			$invoice_surprise_gift=$result_invoice->row()->invoice_surprise_gift;
			$invoice_surprise_gift_type=$result_invoice->row()->invoice_surprise_gift_type;
			$invoice_surprise_gift_skus=$result_invoice->row()->invoice_surprise_gift_skus;
			$invoice_surprise_gift_skus_nums=$result_invoice->row()->invoice_surprise_gift_skus_nums;
			$invoice_offer_achieved=$result_invoice->row()->invoice_offer_achieved;
			$payment_method=$result_invoice->row()->payment_method;
			$customer_id=$result_invoice->row()->customer_id;
			
			$sql_cancel_orders="select count(*) as count from cancelled_orders where order_id='$order_id' GROUP BY order_id";
			
			$result_cancel_orders=$this->db->query($sql_cancel_orders);
			$cancel_row=$result_cancel_orders->row();
			if(!empty($cancel_row)){
				$cancel_count=$result_cancel_orders->row()->count;
			}else{
				$cancel_count=0;
			}
			if($cancel_count>0 && $cancel_count==$order_item_count){
				//echo 'full order is cancelled';
				$cancelled=1;
			}
			if($cancel_count>0 && $cancel_count<$order_item_count){
				//echo 'some of items are cancelled';
				$cancelled=0;
			}
			//echo $cancelled;
			if($invoice_surprise_gift!='' && $invoice_offer_achieved==1 && $cancelled==1){
					
					//if already credited
					//if payment_method not cod and amount credited without delivery
					
				if($payment_method!='COD'){
					
					if($invoice_surprise_gift_type==curr_code){
						$surprise_gift_amount=$invoice_surprise_gift;
		
						if($surprise_gift_amount>0){
					
							$sql_wallet_check="select * from wallet where customer_id='$customer_id'";
							$result_d=$this->db->query($sql_wallet_check);
							if($result_d->num_rows()>0){
								$wallet_id=$result_d->row()->wallet_id;
								$wallet_amount=$result_d->row()->wallet_amount;
								$new_wallet_amount=($wallet_amount-$surprise_gift_amount);
								
								$sql_update_s="update wallet set wallet_amount='$new_wallet_amount' where wallet_id='$wallet_id' and customer_id='$customer_id'";
								$this->db->query($sql_update_s);
								
							}
							
						$sql_for_wallet_transaction_surprise="insert into wallet_transaction set transaction_details='surprise gift amount debited from your wallet against <br> the order number $order_id', debit='$surprise_gift_amount' , credit='',amount='$new_wallet_amount',wallet_id='$wallet_id',customer_id='$customer_id'";
							
						  $this->db->query($sql_for_wallet_transaction_surprise);
						}
						
					}
					
					if($invoice_surprise_gift_type=='Qty'){
						//reduce the stock of free items
				
						$invoice_surprise_gift_skus=rtrim($invoice_surprise_gift_skus,',');
						$invoice_surprise_gift_skus_nums=rtrim($invoice_surprise_gift_skus_nums,',');
						
						$invoice_surprise_gift_skus_arr = explode(',', $invoice_surprise_gift_skus);
						$invoice_surprise_gift_skus_nums_arr = explode(',', $invoice_surprise_gift_skus_nums);
						
						$two=array_combine($invoice_surprise_gift_skus_arr,$invoice_surprise_gift_skus_nums_arr);
						
						foreach($two as $inv_id=>$stock_count){
							$sql_free_item_gift="update inventory set stock = (stock+$stock_count) where id='$inv_id' and stock >= $stock_count; ";
							$result=$this->db->query($sql_free_item_gift);
						}
			
					}
					
				}//checking payment_method //means already credited//so debit
				
			}
			return $cancelled;

		}
		public function invoices_free_items_status_processing($params,$fg){
			
			$var_to =$params['to_date'];
			$var_from =$params['from_date'];		
			$order_id =$params['order_id'];		

			//define index of column
			$columns = array(
				0 => 'invoices_free_items_status.invoices_free_items_status_id', 
				1 => 'invoices_free_items_status.invoices_offers_id',
				2 => 'invoices_free_items_status.inventory_id',
				3 => 'invoices_free_items_status.sku_id',
				4 => 'invoices_free_items_status.vendor_id',
				5 => 'invoices_free_items_status.logistics_id',
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			
			$sql="select invoices_free_items_status.*,product_vendors.*,logistics.*, logistics_parcel_category.*,shipping_address.*,invoices_offers.*,inventory.product_id,inventory.sku_name,products.product_id,products.product_name,products.product_description from invoices_free_items_status 
			";

			$sql.=" LEFT JOIN `logistics` ON invoices_free_items_status.logistics_id=logistics.logistics_id ";
			$sql.=" LEFT JOIN `logistics_parcel_category` ON invoices_free_items_status.logistics_parcel_category_id=logistics_parcel_category.logistics_parcel_category_id ";
			$sql.=" LEFT JOIN `logistics_delivery_mode` ON invoices_free_items_status.logistics_delivery_mode_id=logistics_delivery_mode.logistics_delivery_mode_id ";
			$sql.=" LEFT JOIN `shipping_address` ON invoices_free_items_status.shipping_address_id=shipping_address.shipping_address_id ";
			$sql.=" LEFT JOIN `invoices_offers` ON invoices_free_items_status.invoices_offers_id=invoices_offers.id ";
			$sql.=" LEFT JOIN `inventory` ON inventory.id=invoices_free_items_status.inventory_id ";
			$sql.=" LEFT JOIN `vendor_catalog_inventory` ON `vendor_catalog_inventory`.`vendor_catalog_id` = `inventory`.`inv_vendor_catalog_id`";
			$sql.=" LEFT JOIN `product_vendors` ON `product_vendors`.`vendor_id` = `vendor_catalog_inventory`.`vendor_id`";
			$sql.=" LEFT JOIN `products` ON products.product_id=inventory.product_id ";

			//$sql = "select invoices_free_items_status.*,vendors.*,logistics.*, logistics_parcel_category.*,shipping_address.*,invoices_offers.*,inventory.product_id,inventory.sku_name,products.product_id,products.product_name,products.product_description from invoices_free_items_status,vendors,logistics,logistics_parcel_category,logistics_delivery_mode,shipping_address,invoices_offers,products,inventory where invoices_free_items_status.invoices_offers_id=invoices_offers.id and invoices_free_items_status.vendor_id=vendors.vendor_id and invoices_free_items_status.logistics_id=logistics.logistics_id and invoices_free_items_status.logistics_delivery_mode_id=logistics_delivery_mode.logistics_delivery_mode_id and invoices_free_items_status.logistics_parcel_category_id=logistics_parcel_category.logistics_parcel_category_id and invoices_free_items_status.shipping_address_id=shipping_address.shipping_address_id and products.product_id=inventory.product_id and inventory.id=invoices_free_items_status.inventory_id and invoices_offers.order_id='$order_id' and invoices_free_items_status.quantity>0";
			
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( invoices_free_items_status.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR invoices_free_items_status.timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR invoices_free_items_status.invoices_free_items_status_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR invoices_free_items_status.invoices_offers_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR invoices_free_items_status.sku_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR invoices_free_items_status.inventory_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR invoices_offers.order_id LIKE '%".$params['search']['value']."%' ";
				
				$where .=" OR logistics_parcel_category.parcel_type LIKE '%".$params['search']['value']."%' ";
				$where .=" OR logistics_delivery_mode.delivery_mode LIKE '%".$params['search']['value']."%' ";
				$where .=" OR logistics.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.country LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.state LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.city LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.pincode LIKE '%".$params['search']['value']."%' ";			
				$where .=" OR shipping_address.mobile LIKE '%".$params['search']['value']."%')";	 
			}
			if(!empty($var_from) && !empty($var_to)){
				
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$date = str_replace('/', '-', $var_to);
				$to_date=date('Y-m-d', strtotime($date));
				
				$where .=" AND ";
				$where .="DATE_FORMAT(invoices_free_items_status.timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(invoices_free_items_status.timestamp,'%Y-%m-%d') <= '$to_date'";
				
			}elseif(!empty($var_from) && empty($var_to)){
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$where .=" AND ";
				$where .="DATE_FORMAT(invoices_free_items_status.timestamp,'%Y-%m-%d') >= '$from_date'";
			}
			else{
				$where .="";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			//echo $sqlTot;
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			//echo $sqlTot;
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		public function get_data_from_free_items_status($order_id,$invoices_free_items_status_id){
			$sqlRec = "select invoices_free_items_status.*,vendors.*,logistics.*, logistics_parcel_category.*,shipping_address.*,invoices_offers.*,inventory.product_id,inventory.sku_name,inventory.image,inventory.attribute_1,inventory.attribute_1_value,inventory.attribute_2,inventory.attribute_2_value,inventory.attribute_3,inventory.attribute_3_value,inventory.attribute_4,inventory.attribute_4_value,products.product_id,products.product_name,products.product_description,logistics_delivery_mode.delivery_mode,logistics_delivery_mode.speed_duration from invoices_free_items_status,vendors,logistics,logistics_parcel_category,logistics_delivery_mode,shipping_address,invoices_offers,products,inventory where invoices_free_items_status.invoices_offers_id=invoices_offers.id and invoices_free_items_status.vendor_id=vendors.vendor_id and invoices_free_items_status.logistics_id=logistics.logistics_id and invoices_free_items_status.logistics_delivery_mode_id=logistics_delivery_mode.logistics_delivery_mode_id and invoices_free_items_status.logistics_parcel_category_id=logistics_parcel_category.logistics_parcel_category_id and invoices_free_items_status.shipping_address_id=shipping_address.shipping_address_id and products.product_id=inventory.product_id and inventory.id=invoices_free_items_status.inventory_id and invoices_free_items_status.invoices_free_items_status_id='$invoices_free_items_status_id' and invoices_offers.order_id='$order_id'";
			
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->row();
		}
		public function update_free_item_status_on_invoice($order_id,$invoices_free_items_status_id,$status,$item_delivered_date,$item_delivered_time,$admin_comments){
			
			$admin_comments=$this->db->escape_str($admin_comments);
			
			if($status=="shipped"){
				$sql="update invoices_free_items_status set item_shipped=1,item_shipped_timestamp=now(),admin_comments='$admin_comments' where invoices_free_items_status_id='$invoices_free_items_status_id'";
			}
			if($status=="delivered"){
				$sql="update invoices_free_items_status set item_delivered=1,item_delivered_timestamp=now(),item_delivered_date='$item_delivered_date',item_delivered_time='$item_delivered_time',admin_comments='$admin_comments' where invoices_free_items_status_id='$invoices_free_items_status_id'";
			}
			if($status=="undelivered"){
				$sql="update invoices_free_items_status set item_undelivered=1,item_undelivered_timestamp=now(),admin_comments='$admin_comments' where invoices_free_items_status_id='$invoices_free_items_status_id'";
			}
			
			$query_res=$this->db->query($sql);
			return $query_res;
			
		}
		public function get_status_of_free_items($invoices_offers_id){
			$sql="select * from invoices_free_items_status where invoices_offers_id='$invoices_offers_id'";
			$result=$this->db->query($sql);
			$res_obj=$result->result();
			
			//print_r($res_obj);
			
			$array_shipped=array();
			$array_delivered=array();
			$array_comments=array();
			$array_undelivered=array();
			
			foreach($res_obj as $obj){
				$array_delivered[]=$obj->item_delivered;
				$array_shipped[]=$obj->item_shipped;
				$array_undelivered[]=$obj->item_undelivered;
				$array_comments[]=$obj->admin_comments;			
			}
			
			/*
			if(!in_array("0",$array_delivered)){
				return "delivered";
			}elseif(!in_array("0",$array_shipped)){
				return "shipped";
			}elseif(in_array("0",$array_delivered)){
				return "not delivered";
			}elseif(in_array("0",$array_shipped)){
				return "not shipped";
			}
			*/
			
			$result_arr=array('array_delivered'=>$array_delivered,'array_shipped'=>$array_shipped,'array_undelivered'=>$array_undelivered,'array_comments'=>$array_comments);
			
			return $result_arr;
		}
		public function update_order_item_offers($order_item_id,$order_id,$customer_id,$refund_wallet_comments,$amount,$promotion_quote){
			$refund_wallet_comments=$this->db->escape_str($refund_wallet_comments);
			$sql="insert into order_item_offers set order_item_id='$order_item_id',order_id='$order_id', customer_id='$customer_id', comments_on_delivery='$refund_wallet_comments', promotion_quote='$promotion_quote',amount_to_refund='$amount',status_of_refund='pending',timestamp=now()";
			$result=$this->db->query($sql);
			return $result;
		}
		public function get_type_of_order_item($order_item_id){
			$sql="select type_of_order from orders_status where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row()->type_of_order;
			}else{
				return '';
			}
		}
		public function get_details_of_order_item($order_item_id){
    
			$sql="select * from order_item_offers where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		public function get_all_order_invoice_offers_obj($id){
    
			$sql="select * from invoices_offers where order_id='$id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		public function get_details_orders_status($order_item_id){
    
			$sql="select * from orders_status where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		public function update_invoice_numbers_in_invoices_free_items_status_id($invoices_free_items_status_id,$invoice_number){
			$sql="update invoices_free_items_status set invoice_number='$invoice_number' where invoices_free_items_status_id='$invoices_free_items_status_id'";
			$result=$this->db->query($sql);
			return $result;
		}
		
		public function notify_activeorders_unset(){
			$sql="update orders_status set notify_active='0' order by id desc limit 1000";
			$result=$this->db->query($sql);
		}
		
		public function notify_free_items_unset_invoice(){
			$sql="update invoices_offers set notify_active='0' order by id desc limit 1000";
			$result=$this->db->query($sql);
		}
		
		public function notify_cancel_orders_unset(){
			$sql="update cancelled_orders set notify_cancel='0' order by order_item_id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_cancel_account_unset(){
			$sql="update cancel_refund set notify_cancel_account='0' order by cancel_refund_id desc limit 1000";
			$result=$this->db->query($sql);
		}
		
		///////////////to send nptification /////////////////
		
		public function insert_notification_to_send($arr,$send_notification){
			if($send_notification==0){
				$send_notification=2;//no need to send
			}
			foreach($arr as $result_order_item_id_list_obj){
				$customer_id=$result_order_item_id_list_obj->customer_id;
				$order_item_id=$result_order_item_id_list_obj->order_item_id;
				$order_id=$result_order_item_id_list_obj->order_id;
				$sql_noti="insert into order_item_notification set approved_status_view='$send_notification',customer_id='$customer_id',order_item_id='$order_item_id',order_id='$order_id'";
				
				$result=$this->db->query($sql_noti);
			}
			return $result;
		}
		public function update_customer_response_for_invoice_cashback($order_id,$status,$customer_id,$total_amount_cash_back){

			$sql="update invoices_offers set customer_response_for_invoice_cashback='$status',status_of_refund_for_cashback='',notify_invoice_promotion=1 where order_id='$order_id' and customer_id='$customer_id' ";

			$result=$this->db->query($sql);
			
			return $result;
		}
		public function update_customer_status_surprise_gifts_on_invoice($order_id,$status,$customer_id){
			$pending="";
			$notify_active=0;
			//REFER customer_account
			$sql="update invoices_offers set customer_response_for_surprise_gifts='$status', status_of_refund_for_surprise_gifts='$pending',notify_active='0' where order_id='$order_id' and customer_id='$customer_id'";
			$result=$this->db->query($sql);
			return $result;
		}
		
		/*////////////notification/////////*/
		
		public function update_notification_to_send($arr,$send_notification,$status_view){
			if($send_notification==0){
				$send_notification=2;//no need to send
			}
			foreach($arr as $result_order_item_id_list_obj){
				$customer_id=$result_order_item_id_list_obj->customer_id;
				$prev_order_item_id=$result_order_item_id_list_obj->prev_order_item_id;
				$order_item_id=$result_order_item_id_list_obj->order_item_id;
				$order_id=$result_order_item_id_list_obj->order_id;
				
				if($prev_order_item_id!=''){
					
					$sql_noti="update order_item_notification set approved_status_view='2',confirmed_status_view='2',packed_status_view='2',shipped_status_view='2',delivered_status_view='2',cancelled_status_view='2' where customer_id='$customer_id' and order_item_id='$prev_order_item_id' and order_id='$order_id'";
					$result=$this->db->query($sql_noti);
					
					$sql_in="INSERT INTO order_item_notification (customer_id,order_item_id,order_id,prev_order_item_id,$status_view) VALUES('$customer_id', '$order_item_id','$order_id','$prev_order_item_id','$send_notification') ON DUPLICATE KEY UPDATE  $status_view='$send_notification'";
					
					$result_in=$this->db->query($sql_in);
				
				}else{
					$sql_noti="INSERT INTO order_item_notification (customer_id,order_item_id,order_id,$status_view) VALUES('$customer_id', '$order_item_id','$order_id','$send_notification') ON DUPLICATE KEY UPDATE  $status_view='$send_notification'";	
					$result=$this->db->query($sql_noti);
				}
				
			}
			return $result;
		}
		public function update_notification_to_send_cancel($arr,$send_notification,$status_view){
			if($send_notification==0){
				$send_notification=2;//no need to send
			}
			foreach($arr as $data){
				$customer_id=$data["customer_id"];
				$order_item_id=$data["order_item_id"];
				$prev_order_item_id=$data["prev_order_item_id"];
				$order_id=$data["order_id"];
				
				if($prev_order_item_id!=''){
					
					$sql_noti="update order_item_notification set approved_status_view='2',confirmed_status_view='2',packed_status_view='2',shipped_status_view='2',delivered_status_view='2',cancelled_status_view='2' where customer_id='$customer_id' and order_item_id='$prev_order_item_id' and order_id='$order_id'";
					$result=$this->db->query($sql_noti);
					
					$sql_in="INSERT INTO order_item_notification (customer_id,order_item_id,order_id,prev_order_item_id,$status_view) VALUES('$customer_id', '$order_item_id','$order_id','$prev_order_item_id','$send_notification') ON DUPLICATE KEY UPDATE  $status_view='$send_notification'";
					$result_in=$this->db->query($sql_in);
				
				}else{
					
					$sql_noti="INSERT INTO order_item_notification (customer_id,order_item_id,order_id,$status_view) VALUES('$customer_id', '$order_item_id','$order_id','$send_notification') ON DUPLICATE KEY UPDATE  $status_view='$send_notification'";
					
					$result=$this->db->query($sql_noti);
				}
			}
			return $result;
		}
		public function update_notification_for_replaced_order_item($arr,$send_notification,$status_view){
			if($send_notification==0){
				$send_notification=2;//no need to send
			}
			foreach($arr as $data){
				$customer_id=$data["customer_id"];
				$order_item_id=$data["order_item_id"];
				$prev_order_item_id=$data["prev_order_item_id"];
				$order_id=$data["order_id"];
				
				$sql_noti="update order_item_notification set $status_view='2' where customer_id='$customer_id' and order_item_id='$order_item_id' and order_id='$order_id'";
				$result=$this->db->query($sql_noti);
				
				$sql_in="INSERT INTO order_item_notification (customer_id,order_item_id,order_id,$status_view) VALUES('$customer_id', '$prev_order_item_id','$order_id','$send_notification') ON DUPLICATE KEY UPDATE  $status_view='$send_notification'";
				
				$result_in=$this->db->query($sql_in);

			}
			return $result;
			
		}
		public function update_stock_invoice_surprise_gifts_on_accept($invoice_surprise_gift_skus,$invoice_surprise_gift_skus_nums){
	
			$invoice_surprise_gift_skus=rtrim($invoice_surprise_gift_skus,',');
			$invoice_surprise_gift_skus_nums=rtrim($invoice_surprise_gift_skus_nums,',');
			
			$invoice_surprise_gift_skus_arr = explode(',', $invoice_surprise_gift_skus);
			$invoice_surprise_gift_skus_nums_arr = explode(',', $invoice_surprise_gift_skus_nums);
			
			$two=array_combine($invoice_surprise_gift_skus_arr,$invoice_surprise_gift_skus_nums_arr);
			
			foreach($two as $inv_id=>$stock_count){
				$sql_free_item_gift="update inventory set stock = (stock+$stock_count) where id='$inv_id' and stock >= $stock_count; ";
				$result_free_gift=$this->db->query($sql_free_item_gift);
			}

		}
		public function return_details_of_order_item($order_item_id){
			$sql = "select orders_status.type_of_order,orders_status.id as orders_status_id,history_orders.*,history_orders.image,
			history_orders.sku_id,
			products.product_name,
			products.product_description,
			history_orders.timestamp,
			history_orders.order_id,
			orders_status.order_item_id,
			orders_status.order_delivered_date,
			orders_status.order_delivered_time,
			orders_status.order_replacement_pickup,
			history_orders.quantity,
			history_orders.product_price,
			history_orders.shipping_charge,
			history_orders.subtotal,
			history_orders.grandtotal,
			history_orders.ord_addon_master_order_item_id,
			shipping_address.customer_name,
			shipping_address.address1,
			shipping_address.address2,
			shipping_address.country,
			shipping_address.state,
			shipping_address.city,
			shipping_address.pincode,
			shipping_address.mobile,invoices_offers.* from history_orders,invoices_offers,orders_status,products,inventory,shipping_address
			where history_orders.customer_id=orders_status.customer_id and 
			history_orders.order_item_id=orders_status.order_item_id and
			history_orders.customer_id=shipping_address.customer_id and
			history_orders.shipping_address_id=shipping_address.shipping_address_id and
			history_orders.product_id=products.product_id and
			history_orders.inventory_id=inventory.id and	
			orders_status.customer_id=shipping_address.customer_id and invoices_offers.order_id=history_orders.order_id and
			orders_status.order_placed='1' and 
			orders_status.order_confirmed='1' and 
			orders_status.order_packed='1' and 
			orders_status.order_shipped='1' and 
			orders_status.order_delivered='1' and 
			orders_status.order_cancelled='0' and history_orders.order_item_id='$order_item_id'";
			
			$queryRecords=$this->db->query($sql);
			return $queryRecords->result();
		}
		
		public function get_info_of_products_packed(){
			$sql="select active_orders.tracking_number,active_orders.order_id,orders_status.order_item_id from active_orders,orders_status where orders_status.order_packed='1' and orders_status.order_shipped='0' and orders_status.order_item_id=active_orders.order_item_id";
			$queryRecords=$this->db->query($sql);
			return $queryRecords->result_array();
		}
		
		public function get_tracking_id_generation(){
			$sql="select tracking_id_generation from logistics";
			$queryRecords=$this->db->query($sql);
			return $queryRecords->row_array()["tracking_id_generation"];
		}
		
		public function update_tracking_id_active_orders($order_item_id,$tracking_id){
			$sql="update active_orders set tracking_number='$tracking_id' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			$sql_update_his="update history_orders set tracking_number='$tracking_id' where order_item_id='$order_item_id'";
			$query_update_his=$this->db->query($sql_update_his);
		}
		
		public function get_tracking_number_availability($orders_status_id){
			$sql="select active_orders.tracking_number from active_orders,orders_status where orders_status.order_item_id=active_orders.order_item_id and orders_status.id='$orders_status_id'";
			$query=$this->db->query($sql);
			return $query->row_array()["tracking_number"];
		}
		public function update_tracking_number($orders_status_id,$tracking_numer){
			$sql="select active_orders.order_item_id from active_orders,orders_status where orders_status.order_item_id=active_orders.order_item_id and orders_status.id='$orders_status_id'";
			$query=$this->db->query($sql);
			$order_item_id=$query->row_array()["order_item_id"];
			
			
			$sql_update_his="update history_orders set tracking_number='$tracking_numer' where order_item_id='$order_item_id'";
			$query_update_his=$this->db->query($sql_update_his);
			
			$sql_update="update active_orders set tracking_number='$tracking_numer' where order_item_id='$order_item_id'";
			$query_update=$this->db->query($sql_update);
			return $query_update;
			
		}
		public function update_cancel_table_refund_type($refund_method,$refund_bank_id,$order_item_id,$refund_type_comments){
			$refund_type_comments=$this->db->escape_str($refund_type_comments);
			$sql="update cancels set refund_type='$refund_method',refund_bank_id='$refund_bank_id',refund_type_updated_by='admin',refund_type_comments_admin='$refund_type_comments' where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result;
		}
		public function update_change_logistics($order_item_id,$logistics_name,$tracking_number,$logistics_weblink,$delivery_mode,$use_same_tracking_number){
			$order_item_id=$this->db->escape_str($order_item_id);
			$logistics_name=$this->db->escape_str($logistics_name);
			$tracking_number=$this->db->escape_str($tracking_number);
			$logistics_weblink=$this->db->escape_str($logistics_weblink);
			$delivery_mode=$this->db->escape_str($delivery_mode);
			$use_same_tracking_number=$this->db->escape_str($use_same_tracking_number);
			$sql="update active_orders set logistics_name='$logistics_name'";
			if($use_same_tracking_number==0){
				$sql.=",tracking_number='$tracking_number'";
			}
			$sql.=",logistics_weblink='$logistics_weblink',delivery_mode='$delivery_mode' where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			
			
			$sql_h="update history_orders set logistics_name='$logistics_name'";
			if($use_same_tracking_number==0){
				$sql_h.=",tracking_number='$tracking_number'";
			}
			$sql_h.=",logistics_weblink='$logistics_weblink',delivery_mode='$delivery_mode' where order_item_id='$order_item_id'";
			
			$result_h=$this->db->query($sql_h);
			
			return $result_h;
		}
		public function update_exact_date_to_empty($order_item_id){
			$sql="update active_orders set exact_delivery_date='' where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			
			$sql_h="update history_orders set exact_delivery_date='' where order_item_id='$order_item_id'";
			$result_h=$this->db->query($sql_h);
			
			return $result_h;
		}
		public function update_exact_date($order_item_id,$exact_delivery_date){
			$sql="update active_orders set exact_delivery_date='$exact_delivery_date' where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			
			$sql_h="update history_orders set exact_delivery_date='$exact_delivery_date' where order_item_id='$order_item_id'";
			$result_h=$this->db->query($sql_h);
			
			return $result_h;
		}
		public function completed_request_for_out_of_stock_processing($params,$fg){
			$where = $sqlTot = $sqlRec = "";
			$str='';
			
			

			$sql="select completed_request_for_out_of_stock.*,count(completed_request_for_out_of_stock.sku_id) AS request_count,inventory.*,products.*,parent_category.pcat_name,category.cat_name,subcategory.subcat_name,brands.brand_name from parent_category,category,subcategory,brands,products,inventory,completed_request_for_out_of_stock where inventory.product_id=products.product_id and products.pcat_id=parent_category.pcat_id and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and completed_request_for_out_of_stock.inventory_id=inventory.id and products.pcat_id!='0' and completed_request_for_out_of_stock.sku_id=inventory.sku_id $str GROUP BY completed_request_for_out_of_stock.group_id union select completed_request_for_out_of_stock.*,count(completed_request_for_out_of_stock.sku_id) AS request_count,inventory.*,products.*,'' as pcat_name,category.cat_name,subcategory.subcat_name,brands.brand_name from category,subcategory,brands,products,inventory,completed_request_for_out_of_stock where inventory.product_id=products.product_id  and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and completed_request_for_out_of_stock.inventory_id=inventory.id and products.pcat_id='0' and completed_request_for_out_of_stock.sku_id=inventory.sku_id $str GROUP BY completed_request_for_out_of_stock.group_id";
			
			
			if(!empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( sku_id LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" OR product_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" OR pcat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" OR cat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" OR subcat_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" OR email LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" OR brand_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
			}
			
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			$sqlRec .=  " order by stock asc";
			//echo $sqlRec;
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		public function get_logistics_all_territory_name_data($territory_name_id){
			$query =$this->db->query("SELECT * from logistics_all_territory_name where territory_name_id='$territory_name_id'");
			return $query->row_array();		
		}
		public function get_make_as_local_state_by_logistics_id($logistics_id){
			$sql1="select territory_name_id from logistics_all_territory_name where logistics_id='$logistics_id' and make_as_local='1'";
			$query1=$this->db->query($sql1);
			$territory_name_id=$query1->row_array()["territory_name_id"];
			
			$sql2="select state_id_list from logistics_territory_states where territory_name_id='$territory_name_id' and logistics_id='$logistics_id'";
			$query2=$this->db->query($sql2);
			$state_id=$query2->row_array()["state_id_list"];
			
			$sql3="select state_name from logistics_all_states where state_id='$state_id'";
			$query3=$this->db->query($sql3);
			$state_name=$query3->row_array()["state_name"];
			
			return $state_name;
			
		}
		
		public function delivered_orders_inventory_wise_processing($params,$fg){
			$var_to =$params['to_date'];
			$var_from =$params['from_date'];
			$filter_inventory_id =$params['filter_inventory_id'];
			//define index of column
			
			$columns = array(
				0 => 'completed_orders.timestamp', 
				1 => 'completed_orders.subtotal',
				2 => 'shipping_address.customer_name',
				3 => 'completed_orders.timestamp',
				4 => 'completed_orders.quantity',
				5 => 'shipping_address.customer_name'
			);
			
			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select orders_status.type_of_order,orders_status.id as orders_status_id,completed_orders.*,completed_orders.image,
			completed_orders.sku_id,
			products.product_name,
			products.product_description,
			completed_orders.timestamp,
			completed_orders.order_id,
			orders_status.order_item_id,
			orders_status.order_delivered_date,
			orders_status.order_delivered_time,
			orders_status.order_replacement_pickup,
			completed_orders.quantity,
			completed_orders.product_price,
			completed_orders.shipping_charge,
			completed_orders.subtotal,
			completed_orders.grandtotal,
			completed_orders.ord_addon_master_order_item_id,
			shipping_address.customer_name,
			shipping_address.address1,
			shipping_address.address2,
			shipping_address.country,
			shipping_address.state,
			shipping_address.city,
			shipping_address.pincode,
			shipping_address.mobile,invoices_offers.*,
                        history_orders.ord_sku_name
                        from completed_orders,history_orders,invoices_offers,orders_status,products,inventory,shipping_address
			where completed_orders.customer_id=orders_status.customer_id and 
			completed_orders.order_item_id=orders_status.order_item_id and
			completed_orders.order_item_id=history_orders.order_item_id and
			completed_orders.customer_id=shipping_address.customer_id and
			completed_orders.shipping_address_id=shipping_address.shipping_address_id and
			completed_orders.product_id=products.product_id and
			completed_orders.inventory_id=inventory.id and	
			orders_status.customer_id=shipping_address.customer_id and invoices_offers.order_id=completed_orders.order_id and
			orders_status.order_placed='1' and 
			orders_status.order_confirmed='1' and 
			orders_status.order_packed='1' and 
			orders_status.order_shipped='1' and 
			orders_status.order_delivered='1' and 
			orders_status.order_cancelled='0' and 
			completed_orders.inventory_id='$filter_inventory_id' ";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( completed_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.subtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.customer_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address1 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address2 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.country LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.state LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.city LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.pincode LIKE '%".$params['search']['value']."%' ";
			
				$where .=" OR shipping_address.mobile LIKE '%".$params['search']['value']."%' )";
			}

			$user_type=$this->session->userdata("user_type");
			$a_id=$this->session->userdata("user_id");

			if($user_type=='vendor'){
				$where .=" AND ";
				$where .=" completed_orders.product_vendor_id= '$a_id' ";
			}
			if(!empty($var_from) && !empty($var_to)){
				
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$date = str_replace('/', '-', $var_to);
				$to_date=date('Y-m-d', strtotime($date));
				
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') <= '$to_date'";
				
			}elseif(!empty($var_from) && empty($var_to)){
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') >= '$from_date'";
			}
			else{
				$where .="";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		public function get_inventory_chain_by_inventory_id($inventory_id){
			if($this->get_pcat_id_by_inventory_id($inventory_id)==0){
				$sql="select inventory.sku_id,inventory.id,products.product_id,products.pcat_id,category.cat_id,subcategory.subcat_id,brands.brand_id from category,subcategory,brands,products,inventory where  inventory.product_id=products.product_id and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and inventory.id='$inventory_id'";
				$query_res=$this->db->query($sql);
				return $query_res->row();
			}
			else{
				$sql="select inventory.sku_id,inventory.id,products.product_id,parent_category.pcat_id,category.cat_id,subcategory.subcat_id,brands.brand_id from parent_category,category,subcategory,brands,products,inventory where  inventory.product_id=products.product_id and products.pcat_id=parent_category.pcat_id and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and inventory.id='$inventory_id'";
				$query_res=$this->db->query($sql);
				return $query_res->row();
			}
		
		}
		public function get_pcat_id_by_inventory_id($inventory_id){
			$sql="select products.pcat_id from products,inventory where inventory.product_id=products.product_id and inventory.id='$inventory_id'";
			$query_res=$this->db->query($sql);
			return $query_res->row()->pcat_id;
			
		
		}
		
		/// shipping charges starts 
			public function shipping_charges_processing($params,$fg){
			$var_to =$params['to_date'];
			$var_from =$params['from_date'];
			//define index of column
			
			$columns = array(
				0 => 'completed_orders.timestamp', 
				1 => 'completed_orders.subtotal',
				2 => 'shipping_address.customer_name',
				3 => 'completed_orders.timestamp',
				4 => 'completed_orders.quantity',
				5 => 'shipping_address.customer_name'
			);
			
			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select orders_status.type_of_order,orders_status.id as orders_status_id,completed_orders.*,completed_orders.image,
			completed_orders.sku_id,
			inventory.sku_name,
			products.product_name,
			products.product_description,
			completed_orders.timestamp,
			completed_orders.order_id,
			orders_status.order_item_id,
			orders_status.order_delivered_date,
			orders_status.order_delivered_time,
			orders_status.order_replacement_pickup,
			completed_orders.quantity,
			completed_orders.product_price,
			completed_orders.shipping_charge,
			completed_orders.subtotal,
			completed_orders.grandtotal,
			completed_orders.ord_addon_master_order_item_id,
			shipping_address.customer_name,
			shipping_address.address1,
			shipping_address.address2,
			shipping_address.country,
			shipping_address.state,
			shipping_address.city,
			shipping_address.pincode,
			shipping_address.mobile,invoices_offers.*,
                        history_orders.ord-sku_name
                        from completed_orders,history_orders,invoices_offers,orders_status,products,inventory,shipping_address
			where completed_orders.customer_id=orders_status.customer_id and 
			completed_orders.order_item_id=orders_status.order_item_id and
			completed_orders.order_item_id=history_orders.order_item_id and
			completed_orders.customer_id=shipping_address.customer_id and
			completed_orders.shipping_address_id=shipping_address.shipping_address_id and
			completed_orders.product_id=products.product_id and
			completed_orders.inventory_id=inventory.id and	
			orders_status.customer_id=shipping_address.customer_id and invoices_offers.order_id=completed_orders.order_id and
			orders_status.order_placed='1' and 
			orders_status.order_confirmed='1' and 
			orders_status.order_packed='1' and 
			orders_status.order_shipped='1' and 
			orders_status.order_delivered='1' and 
			orders_status.order_cancelled='0'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( completed_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.subtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.customer_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address1 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address2 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.country LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.state LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.city LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.pincode LIKE '%".$params['search']['value']."%' ";
			
				$where .=" OR shipping_address.mobile LIKE '%".$params['search']['value']."%' )";
			}

			$user_type=$this->session->userdata("user_type");
			$a_id=$this->session->userdata("user_id");

			if($user_type=='vendor'){
				$where .=" AND ";
				$where .=" completed_orders.product_vendor_id= '$a_id' ";
			}

			if(empty($var_from) && empty($var_to)){
				
				
				$where .=" AND ";
				$where .="1=2";
				
			}
			else if(!empty($var_from) && !empty($var_to)){
				
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$date = str_replace('/', '-', $var_to);
				$to_date=date('Y-m-d', strtotime($date));
				
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.order_shipped_timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(orders_status.order_shipped_timestamp,'%Y-%m-%d') <= '$to_date'";
				
			}elseif(!empty($var_from) && empty($var_to)){
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.order_shipped_timestamp,'%Y-%m-%d') >= '$from_date'";
			}
			else{
				$where .="";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		//// shipping charges ends 
		
		public function get_order_placed_date_customer_id($id){
			$sql="select customer_id,timestamp from active_orders where order_id='$id' union select customer_id,timestamp from cancelled_orders where order_id='$id' union select customer_id,timestamp from completed_orders where order_id='$id' union select customer_id,timestamp from returned_orders where order_id='$id'";
			$result=$this->db->query($sql);
			return array("order_placed_date"=>$result->row()->timestamp,"customer_id"=>$result->row()->customer_id);
		}
		
		public function get_order_summary_mail_data_from_history_table($order_id){
			$sql="select history_orders.*,inventory.thumbnail,products.product_name,products.product_description,inventory.attribute_1,inventory.attribute_1_value,inventory.attribute_2,inventory.attribute_2_value,inventory.attribute_3,inventory.attribute_3_value,inventory.attribute_4,inventory.attribute_4_value from history_orders,inventory,products where history_orders.inventory_id=inventory.id and products.product_id=inventory.product_id and history_orders.order_id='$order_id'";
			$query =$this->db->query($sql);
			return $query->result_array();
		}
		
		public function get_inventory_details($inventory_id){
			$sql="select * from inventory where id='$inventory_id' ";
			$result=$this->db->query($sql);
			return $result->row();
		}
		public function get_all_order_invoice_offers($id){
			
			$sql="select * from invoices_offers where order_id='$id'";
			$result=$this->db->query($sql);
			return $result->result_array();
		}
		public function out_of_stock_switch_fun($out_of_stock_switch_status){
			$sql="truncate table out_of_stock_switch";
			$this->db->query($sql);
			$sql="insert into out_of_stock_switch set switch_status='$out_of_stock_switch_status'";
			$query=$this->db->query($sql);
			return $query;
		}
		public function get_out_of_stock_switch(){
			$sql="select * from out_of_stock_switch";
			$result=$this->db->query($sql);
			$row_arr=$result->row_array();
			if(empty($row_arr)){
				return "no";
			}
			else{
				$out_of_stock_switch=$row_arr["switch_status"];
				if($out_of_stock_switch=="yes"){
					return "yes";
				}
				else{
					return "no";
				}
			}
		}
		public function get_customer_name_by_emailid($email){
			$sql="select name from customer where email='$email'";
			$result=$this->db->query($sql);
			$row_arr=$result->row_array();
			if(!empty($row_arr)){
				return $row_arr["name"];
			}
			else{
				return "";
			}
		}
		public function update_refund_data($cancel_id,$refund_id,$refund_status,$order_item_id){
            $sql="update cancels set razorpay_refund_id='$refund_id',razorpay_refund_status='$refund_status' where order_item_id='$order_item_id' and cancel_id='$cancel_id'";
            $result=$this->db->query($sql);
            if($this->db->affected_rows() > 0){
                return true;
            }else{
                return false;
            }
        }
        public function get_razorpay_paymentID($order_item_id){
            $sql="select razorpayPaymentId from history_orders where order_item_id='$order_item_id'";
            $result=$this->db->query($sql);
            return ($result->num_rows()>0) ? $result->row()->razorpayPaymentId : '';
        }
		
		public function get_cancel_reason_stuff($order_item_id){
			$sql="select reason_cancel_id,cancel_reason_comments from cancels where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			$row_arr=$result->row_array();
			return $row_arr;
		}

		// failed orders processing starts
		public function failed_orders_processing($params,$fg){
			
			$var_to =$params['to_date'];
			$var_from =$params['from_date'];		

			//define index of column
			$columns = array(
				0 => 'failed_orders.timestamp', 
				1 => 'failed_orders.subtotal',
				2 => 'shipping_address.customer_name',
				3 => 'failed_orders.timestamp',
				4 => 'failed_orders.quantity',
				5 => 'shipping_address.customer_name',
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select orders_status.type_of_order,orders_status.id as orders_status_id,failed_orders.*,failed_orders.image,
			failed_orders.sku_id,
			failed_orders.prev_order_item_id,
			products.product_name,
			products.product_description,
			failed_orders.timestamp,
			failed_orders.order_id,
			failed_orders.payment_type,
			orders_status.order_item_id,
			DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') AS 'order_status_timestamp',
			failed_orders.quantity,
			failed_orders.product_price,
			failed_orders.subtotal,
			failed_orders.grandtotal,
			failed_orders.ord_addon_master_order_item_id,
			shipping_address.customer_name,
			shipping_address.address1,
			shipping_address.address2,
			shipping_address.country,
			shipping_address.state,
			shipping_address.city,
			shipping_address.pincode,
			shipping_address.mobile,invoices_offers.* from failed_orders,invoices_offers,orders_status,products,inventory,shipping_address
			where failed_orders.customer_id=orders_status.customer_id and 
			failed_orders.order_item_id=orders_status.order_item_id and
			failed_orders.customer_id=shipping_address.customer_id and
			failed_orders.shipping_address_id=shipping_address.shipping_address_id and
			failed_orders.product_id=products.product_id and
			failed_orders.inventory_id=inventory.id and	
			orders_status.customer_id=shipping_address.customer_id and invoices_offers.order_id=failed_orders.order_id and
			orders_status.order_placed='0' and 
			orders_status.order_confirmed='0' and 
			orders_status.order_packed='0' and 
			orders_status.order_shipped='0' and 
			orders_status.order_delivered='0' and 
			orders_status.order_cancelled='0' ";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( failed_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR failed_orders.timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR failed_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR failed_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR failed_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR failed_orders.subtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.customer_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address1 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.address2 LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.country LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.state LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.city LIKE '%".$params['search']['value']."%' ";
				$where .=" OR shipping_address.pincode LIKE '%".$params['search']['value']."%' ";			
				$where .=" OR shipping_address.mobile LIKE '%".$params['search']['value']."%')";	 
			}

			$user_type=$this->session->userdata("user_type");
			$a_id=$this->session->userdata("user_id");

			if($user_type=='vendor'){
				$where .=" AND ";
				$where .=" failed_orders.product_vendor_id= '$a_id' ";
			}

			if(!empty($var_from) && !empty($var_to)){
				
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$date = str_replace('/', '-', $var_to);
				$to_date=date('Y-m-d', strtotime($date));
				
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') <= '$to_date'";
				
			}elseif(!empty($var_from) && empty($var_to)){
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.timestamp,'%Y-%m-%d') >= '$from_date'";
			}
			else{
				$where .="";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			//echo $sqlTot;
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			//echo $sqlTot;
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		// failed orders processing ends
		
				
		
	}
?>