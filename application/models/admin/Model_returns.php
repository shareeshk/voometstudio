<?php
	class Model_returns extends CI_Model{
		public function get_inventory_info_by_inventory_id($inventory_id){
			$sql="select * from inventory where id='$inventory_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function get_product_info_by_product_id($product_id){
			$sql="select * from products where product_id='$product_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function returns_refund_request_orders($order_item_id){ 
		
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.*,returns_desired.order_item_id,returns_desired.promotion_available,returns_desired.promotion_quote,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id,inventory.moq,invoices_offers.*,returns_desired.order_item_invoice_discount_value_each,completed_orders.ord_addon_products_status,completed_orders.ord_addon_products,completed_orders.ord_addon_inventories,completed_orders.ord_addon_total_price,completed_orders.subtotal  from completed_orders,products,returns_desired,orders_status,shipping_address,reason_return,inventory,invoices_offers where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='pending' and returns_desired.order_return_decision_customer_status!='cancel' and returns_desired.order_item_id='$order_item_id' and completed_orders.inventory_id=inventory.id and completed_orders.order_id=invoices_offers.order_id";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function returns_refund_order_decision_data($order_item_id){
			
			$sql=" select * from order_return_decision where order_item_id='$order_item_id' ";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function returns_refund_order_decision_data_arr($order_item_id){
			
			$sql=" select * from order_return_decision where order_item_id='$order_item_id' ";
			$query=$this->db->query($sql);
			return $query->row_array();
		}
		public function returns_refund_returns_desired_data($order_item_id){
			
			$sql=" select * from returns_desired where order_item_id='$order_item_id' ";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function returns_refund_request_orders_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'completed_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'returns_desired.order_item_id',
				3 => 'completed_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.order_item_id,returns_desired.promotion_available,returns_desired.promotion_quote,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id,returns_desired.order_item_invoice_discount_value_each from completed_orders,products,returns_desired,orders_status,shipping_address,reason_return where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='pending' and returns_desired.order_return_decision_customer_status!='cancel'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( completed_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";

			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}

		public function get_sub_issue_name($sub_issue_id){
			$sql="select sub_issue_name from sub_issue_return where sub_issue_id='$sub_issue_id'";
			$query=$this->db->query($sql);
			return $query->row()->sub_issue_name;
		}
		
		public function contact_buyer($return_order_id,$user_type,$status,$description,$image_path,$customer_id,$admin_action,$customer_action){
			$description=$this->db->escape_str($description);
			$sql="insert into return_replies set return_order_id='$return_order_id',user_type='$user_type',status='$status',description='$description',image='$image_path',customer_id='$customer_id',admin_action='$admin_action',customer_action='$customer_action'";
			$query=$this->db->query($sql);
			return $query;
		}
		
		public function get_returns_conservation_chain_in_adminpanel($return_order_id){
			$sql="select * from return_replies where return_order_id='$return_order_id'";
			$query=$this->db->query($sql);
			return $query->result();
		}
		
		public function get_customer_name($customer_id){
			$sql="select name from customer where id='$customer_id'";
			$query=$this->db->query($sql);
			return $query->row()->name;
		}
		
		public function getspecified_returns_file($id){
		$query=$this->db->query("SELECT *  FROM return_replies where id='$id'");
			if(count($query->result())==1){
				return $query->result();
			}
		}
		
		public function get_unread_return_msg($return_order_id){
			$sql="select count(*) as count from  return_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='pending' and customer_action='pending'";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		
		public function update_msg_status($return_order_id){
			$sql="update return_replies set is_read=1 where user_type='customer' and return_order_id='$return_order_id'";
			$query=$this->db->query($sql);
			return $query;
		}
		
		public function get_order_return_primary_reasons(){
			$sql="select * from reason_return";
			$result=$this->db->query($sql);
			return $result->result_array();
		}
		
		public function get_order_return_primary_reasons_admin(){
			$sql="select * from admin_return_reason";
			$result=$this->db->query($sql);
			return $result->result_array();
		}
		
		public function return_refund_accept($return_order_id,$customer_id,$inventory_id,$product_price,$shipping_charge_purchased,$shipping_charge,$order_item_id,$quantity_accept,$type_of_refund,$comments,$disable_comm,$allow_cust_to_make_decision,$product_price_with_offer,$shipping_charge_with_offer,$product_price_with_offer_percentage,$shipping_charge_with_offer_percentage,$product_price_with_offer_percentage_chk,$shipping_charge_with_offer_percentage_chk){
			if($disable_comm){
				$comm_flag=1;
			}
			else{
				$comm_flag=0;
			}
			if($allow_cust_to_make_decision){
				$allow_cust_to_make_decision_flag=1;
				$customer_decision="pending";
			}
			else{
				$allow_cust_to_make_decision_flag=0;
				$customer_decision="accept";
			}
			$comments=$this->db->escape_str($comments);
			$sql="insert into order_return_decision set return_order_id='$return_order_id',customer_id='$customer_id',quantity='$quantity_accept',inventory_id='$inventory_id',product_price='$product_price',shipping_charge_purchased='$shipping_charge_purchased',shipping_charge='$shipping_charge',order_item_id='$order_item_id',sub_reason_return_id='$type_of_refund',status='accept',comments='$comments',disable_comm_flag='$comm_flag',allow_cust_to_make_decision_flag='$allow_cust_to_make_decision_flag',product_price_with_offer='$product_price_with_offer',shipping_charge_with_offer='$shipping_charge_with_offer',product_price_with_offer_percentage='$product_price_with_offer_percentage',shipping_charge_with_offer_percentage='$shipping_charge_with_offer_percentage',product_price_with_offer_percentage_chk='$product_price_with_offer_percentage_chk',shipping_charge_with_offer_percentage_chk='$shipping_charge_with_offer_percentage_chk',customer_decision='$customer_decision',customer_status='pending'";
			$query=$this->db->query($sql);
			
			$sql_update_returns_desired="update returns_desired set order_return_decision_status='accept',order_return_decision_customer_decision='$customer_decision',order_return_decision_customer_status='pending' where order_item_id='$order_item_id'";
			$query_update_returns_desired=$this->db->query($sql_update_returns_desired);
			
			
			$return_options=$this->get_primary_return_reason_admin($type_of_refund);
			
			/*if($return_options=="refund without collecting an item"){
				$sql_order_return_pickup="update orders_status set order_return_pickup='1' where order_item_id='$order_item_id'";
				$query_order_return_pickup=$this->db->query($sql_order_return_pickup);
			}*/
			return $query;
		}
		
		public function return_refund_accept_with_promo_details($return_order_id,$customer_id,$inventory_id,$product_price,$shipping_charge_purchased,$shipping_charge,$order_item_id,$quantity_accept,$type_of_refund,$comments,$disable_comm,$allow_cust_to_make_decision,$product_price_with_offer,$shipping_charge_with_offer,$product_price_with_offer_percentage,$shipping_charge_with_offer_percentage,$product_price_with_offer_percentage_chk,$shipping_charge_with_offer_percentage_chk,$promotion_available,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$promotion_item,$promotion_item_num,$promotion_cashback,$promotion_item_multiplier,$promotion_discount,$default_discount,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_with_promotion,$total_price_of_product_without_promotion){
			if($disable_comm){
				$comm_flag=1;
			}
			else{
				$comm_flag=0;
			}
			if($allow_cust_to_make_decision){
				$allow_cust_to_make_decision_flag=1;
				$customer_decision="pending";
			}
			else{
				$allow_cust_to_make_decision_flag=0;
				$customer_decision="accept";
			}
			$comments=$this->db->escape_str($comments);
			$sql="insert into order_return_decision set return_order_id='$return_order_id',customer_id='$customer_id',quantity='$quantity_accept',inventory_id='$inventory_id',product_price='$product_price',shipping_charge_purchased='$shipping_charge_purchased',shipping_charge='$shipping_charge',order_item_id='$order_item_id',sub_reason_return_id='$type_of_refund',status='accept',comments='$comments',disable_comm_flag='$comm_flag',allow_cust_to_make_decision_flag='$allow_cust_to_make_decision_flag',product_price_with_offer='$product_price_with_offer',shipping_charge_with_offer='$shipping_charge_with_offer',product_price_with_offer_percentage='$product_price_with_offer_percentage',shipping_charge_with_offer_percentage='$shipping_charge_with_offer_percentage',product_price_with_offer_percentage_chk='$product_price_with_offer_percentage_chk',shipping_charge_with_offer_percentage_chk='$shipping_charge_with_offer_percentage_chk',customer_decision='$customer_decision',customer_status='pending',promotion_available='$promotion_available',promotion_minimum_quantity='$promotion_minimum_quantity',promotion_quote='$promotion_quote',promotion_default_discount_promo='$promotion_default_discount_promo',promotion_surprise_gift='$promotion_surprise_gift',promotion_surprise_gift_type='$promotion_surprise_gift_type',promotion_surprise_gift_skus='$promotion_surprise_gift_skus',promotion_surprise_gift_skus_nums='$promotion_surprise_gift_skus_nums',promotion_item='$promotion_item',promotion_item_num='$promotion_item_num',promotion_cashback='$promotion_cashback',promotion_item_multiplier='$promotion_item_multiplier',promotion_discount='$promotion_discount',default_discount='$default_discount',quantity_without_promotion='$quantity_without_promotion',quantity_with_promotion='$quantity_with_promotion',cash_back_value='$cash_back_value',individual_price_of_product_with_promotion='$individual_price_of_product_with_promotion',individual_price_of_product_without_promotion='$individual_price_of_product_without_promotion',total_price_of_product_with_promotion='$total_price_of_product_with_promotion',total_price_of_product_without_promotion='$total_price_of_product_without_promotion'";
			
			$query=$this->db->query($sql);
			
			$sql_update_returns_desired="update returns_desired set order_return_decision_status='accept',order_return_decision_customer_decision='$customer_decision',order_return_decision_customer_status='pending' where order_item_id='$order_item_id'";
			$query_update_returns_desired=$this->db->query($sql_update_returns_desired);
			
			
			$return_options=$this->get_primary_return_reason_admin($type_of_refund);
			
			/*if($return_options=="refund without collecting an item"){
				$sql_order_return_pickup="update orders_status set order_return_pickup='1' where order_item_id='$order_item_id'";
				$query_order_return_pickup=$this->db->query($sql_order_return_pickup);
			}*/
			return $query;
		}
		
		public function returns_refund_request_orders_awaiting($order_item_id){ 
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.*,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id,inventory.moq,invoices_offers.*,completed_orders.ord_addon_products_status,completed_orders.ord_addon_products,completed_orders.ord_addon_inventories,completed_orders.ord_addon_total_price,completed_orders.subtotal   from completed_orders,products,returns_desired,orders_status,shipping_address,reason_return,inventory,invoices_offers where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='accept' and  returns_desired.order_return_decision_customer_decision='pending' and returns_desired.order_return_decision_customer_status!='cancel' and returns_desired.order_item_id='$order_item_id' and completed_orders.inventory_id=inventory.id and completed_orders.order_id=invoices_offers.order_id";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function returns_refund_request_orders_awaiting_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'completed_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'returns_desired.order_item_id',
				3 => 'completed_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id from completed_orders,products,returns_desired,orders_status,shipping_address,reason_return where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='accept' and returns_desired.order_return_decision_customer_decision='pending' and returns_desired.order_return_decision_customer_status!='cancel'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( completed_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		public function get_refund_request_awaiting_info($orders_status_id){
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id from completed_orders,products,returns_desired,orders_status,shipping_address,reason_return where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and  returns_desired.order_return_decision_status='accept' and returns_desired.order_return_decision_customer_decision='pending' and orders_status.id='$orders_status_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function get_order_return_decision_data($order_item_id){
			//$sql="select * from order_return_decision where order_item_id='$order_item_id'";
			$sql="select order_return_decision.*,invoices_offers.*,history_orders.grandtotal,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.ord_addon_products_status,history_orders.ord_addon_products,history_orders.ord_addon_inventories,history_orders.ord_addon_total_price,history_orders.subtotal  from order_return_decision,invoices_offers,history_orders where order_return_decision.order_item_id='$order_item_id' and history_orders.order_item_id=order_return_decision.order_item_id and history_orders.order_id=invoices_offers.order_id";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function update_return_refund_accept($return_order_id,$customer_id,$inventory_id,$product_price,$shipping_charge_purchased,$shipping_charge,$order_item_id,$quantity_accept,$type_of_refund,$comments,$disable_comm,$allow_cust_to_make_decision,$product_price_with_offer,$shipping_charge_with_offer,$product_price_with_offer_percentage,$shipping_charge_with_offer_percentage,$product_price_with_offer_percentage_chk,$shipping_charge_with_offer_percentage_chk){
			if($disable_comm){
				$comm_flag=1;
			}
			else{
				$comm_flag=0;
			}
			if($allow_cust_to_make_decision){
				$allow_cust_to_make_decision_flag=1;
				$customer_decision="pending";
			}
			else{
				$allow_cust_to_make_decision_flag=0;
				$customer_decision="accept";
			}
			$comments=$this->db->escape_str($comments);
			$sql="update order_return_decision set return_order_id='$return_order_id',customer_id='$customer_id',quantity='$quantity_accept',inventory_id='$inventory_id',product_price='$product_price',shipping_charge_purchased='$shipping_charge_purchased',shipping_charge='$shipping_charge',order_item_id='$order_item_id',sub_reason_return_id='$type_of_refund',status='accept',customer_decision='$customer_decision',comments='$comments',disable_comm_flag='$comm_flag',allow_cust_to_make_decision_flag='$allow_cust_to_make_decision_flag',product_price_with_offer='$product_price_with_offer',shipping_charge_with_offer='$shipping_charge_with_offer',product_price_with_offer_percentage='$product_price_with_offer_percentage',shipping_charge_with_offer_percentage='$shipping_charge_with_offer_percentage',product_price_with_offer_percentage_chk='$product_price_with_offer_percentage_chk',shipping_charge_with_offer_percentage_chk='$shipping_charge_with_offer_percentage_chk' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			$sql_returns_desired="update returns_desired set order_return_decision_status='accept',order_return_decision_customer_decision='$customer_decision' where order_item_id='$order_item_id'";
			$query_returns_desired=$this->db->query($sql_returns_desired);
			
			/*if($type_of_refund=="2"){
				$sql_order_return_pickup="update orders_status set order_return_pickup='1' where order_item_id='$order_item_id'";
				$query_order_return_pickup=$this->db->query($sql_order_return_pickup);
			}
			*/
			return $query;
		}
		public function update_return_refund_accept_with_promo_details($return_order_id,$customer_id,$inventory_id,$product_price,$shipping_charge_purchased,$shipping_charge,$order_item_id,$quantity_accept,$type_of_refund,$comments,$disable_comm,$allow_cust_to_make_decision,$product_price_with_offer,$shipping_charge_with_offer,$product_price_with_offer_percentage,$shipping_charge_with_offer_percentage,$product_price_with_offer_percentage_chk,$shipping_charge_with_offer_percentage_chk,$promotion_available,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$promotion_item,$promotion_item_num,$promotion_cashback,$promotion_item_multiplier,$promotion_discount,$default_discount,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_with_promotion,$total_price_of_product_without_promotion){
			if($disable_comm){
				$comm_flag=1;
			}
			else{
				$comm_flag=0;
			}
			if($allow_cust_to_make_decision){
				$allow_cust_to_make_decision_flag=1;
				$customer_decision="pending";
			}
			else{
				$allow_cust_to_make_decision_flag=0;
				$customer_decision="accept";
			}
			$comments=$this->db->escape_str($comments);
			$sql="update order_return_decision set return_order_id='$return_order_id',customer_id='$customer_id',quantity='$quantity_accept',inventory_id='$inventory_id',product_price='$product_price',shipping_charge_purchased='$shipping_charge_purchased',shipping_charge='$shipping_charge',order_item_id='$order_item_id',sub_reason_return_id='$type_of_refund',status='accept',customer_decision='$customer_decision',comments='$comments',disable_comm_flag='$comm_flag',allow_cust_to_make_decision_flag='$allow_cust_to_make_decision_flag',product_price_with_offer='$product_price_with_offer',shipping_charge_with_offer='$shipping_charge_with_offer',product_price_with_offer_percentage='$product_price_with_offer_percentage',shipping_charge_with_offer_percentage='$shipping_charge_with_offer_percentage',product_price_with_offer_percentage_chk='$product_price_with_offer_percentage_chk',shipping_charge_with_offer_percentage_chk='$shipping_charge_with_offer_percentage_chk',promotion_available='$promotion_available',promotion_minimum_quantity='$promotion_minimum_quantity',promotion_quote='$promotion_quote',promotion_default_discount_promo='$promotion_default_discount_promo',promotion_surprise_gift='$promotion_surprise_gift',promotion_surprise_gift_type='$promotion_surprise_gift_type',promotion_surprise_gift_skus='$promotion_surprise_gift_skus',promotion_surprise_gift_skus_nums='$promotion_surprise_gift_skus_nums',promotion_item='$promotion_item',promotion_item_num='$promotion_item_num',promotion_cashback='$promotion_cashback',promotion_item_multiplier='$promotion_item_multiplier',promotion_discount='$promotion_discount',default_discount='$default_discount',quantity_without_promotion='$quantity_without_promotion',quantity_with_promotion='$quantity_with_promotion',cash_back_value='$cash_back_value',individual_price_of_product_with_promotion='$individual_price_of_product_with_promotion',individual_price_of_product_without_promotion='$individual_price_of_product_without_promotion',total_price_of_product_with_promotion='$total_price_of_product_with_promotion',total_price_of_product_without_promotion='$total_price_of_product_without_promotion' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			$sql_returns_desired="update returns_desired set order_return_decision_status='accept',order_return_decision_customer_decision='$customer_decision' where order_item_id='$order_item_id'";
			$query_returns_desired=$this->db->query($sql_returns_desired);
			
			/*if($type_of_refund=="2"){
				$sql_order_return_pickup="update orders_status set order_return_pickup='1' where order_item_id='$order_item_id'";
				$query_order_return_pickup=$this->db->query($sql_order_return_pickup);
			}
			*/
			return $query;
		}
		
		public function get_unread_return_msg_awaiting($return_order_id){
			$sql="select count(*) as count from  return_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='accept' and customer_action='pending'";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		
		public function returns_refund_request_orders_customer_rejected($order_item_id){ 
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.*,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id,inventory.moq,invoices_offers.*,completed_orders.ord_addon_products_status,completed_orders.ord_addon_products,completed_orders.ord_addon_inventories,completed_orders.ord_addon_total_price,completed_orders.subtotal  from completed_orders,products,returns_desired,orders_status,shipping_address,reason_return,inventory,invoices_offers where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='accept' and returns_desired.
			order_return_decision_customer_decision='reject' and returns_desired.order_return_decision_customer_status!='cancel' and returns_desired.order_item_id='$order_item_id' and completed_orders.inventory_id=inventory.id and completed_orders.order_id=invoices_offers.order_id";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function returns_refund_request_orders_customer_rejected_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'completed_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'returns_desired.order_item_id',
				3 => 'completed_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id from completed_orders,products,returns_desired,orders_status,shipping_address,reason_return where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='accept' and returns_desired.order_return_decision_customer_decision='reject' and returns_desired.order_return_decision_customer_status!='cancel'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( completed_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}

		public function get_unread_return_msg_customer_rejected($return_order_id){
			$sql="select count(*) as count from  return_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='accept' and customer_action='reject'";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		
		
		public function returns_refund_request_orders_customer_accepted($order_item_id){ 
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.*,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id,inventory.moq,invoices_offers.*,completed_orders.ord_addon_products_status,completed_orders.ord_addon_products,completed_orders.ord_addon_inventories,completed_orders.ord_addon_total_price,completed_orders.subtotal from completed_orders,products,returns_desired,orders_status,shipping_address,reason_return,inventory,invoices_offers where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='accept' and returns_desired.order_return_decision_customer_decision='accept' and orders_status.order_return_pickup='0' and returns_desired.order_return_decision_customer_status!='cancel' and returns_desired.order_item_id='$order_item_id' and completed_orders.inventory_id=inventory.id and completed_orders.order_id=invoices_offers.order_id";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function get_unread_return_msg_customer_accepted($return_order_id){
			$sql="select count(*) as count from  return_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='accept' and customer_action='accept'";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		
		public function returns_refund_request_orders_customer_accepted_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'completed_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'returns_desired.order_item_id',
				3 => 'completed_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id,orders_status.order_return_pickup from completed_orders,products,returns_desired,orders_status,shipping_address,reason_return where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='accept' and returns_desired.order_return_decision_customer_decision='accept' and orders_status.order_return_pickup='0' and (orders_status.initiate_return_pickup!='0' and  orders_status.initiate_return_pickup!='1') and returns_desired.order_return_decision_customer_status!='cancel'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( completed_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		public function get_primary_return_reason($reason_return_id){
			$sql="select reason_name from reason_return where reason_id='$reason_return_id'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row()->reason_name;
			}
			else{
				return false;
			}
		}
		
		public function get_primary_return_reason_admin($reason_return_id){
			$sql="select return_options from admin_return_reason where id='$reason_return_id'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row()->return_options;
			}
			else{
				return false;
			}
		}
		public function get_pickup_identifier_return_reason_admin($reason_return_id){
			$sql="select pickup_identifier from admin_replacement_reason where id='$reason_return_id'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row()->pickup_identifier;
			}
			else{
				return false;
			}
		}
		
		public function return_refund_reject($return_order_id,$customer_id,$inventory_id,$product_price,$shipping_charge_purchased,$shipping_charge,$order_item_id,$quantity_accept,$type_of_refund,$comments,$disable_comm){
			/* changed for samrick 5/8/2021 starts */
			$product_price_with_offer=round(($quantity_accept*$product_price),2);
			$shipping_charge_with_offer=round(($quantity_accept*$shipping_charge),2);
			$product_price_with_offer_percentage=0;
			$shipping_charge_with_offer_percentage=0;
			$product_price_with_offer_percentage_chk='yes';
			$shipping_charge_with_offer_percentage_chk='yes';
			/* changed for samrick 5/8/2021 ends */

			if($disable_comm){
				$comm_flag=1;
			}
			else{
				$comm_flag=0;
			}
			$comments=$this->db->escape_str($comments);
			$sql="select * from order_return_decision where order_item_id='$order_item_id'";
			$res=$this->db->query($sql);
			if($res->num_rows()==0){
				
				$sql="insert into order_return_decision set return_order_id='$return_order_id',customer_id='$customer_id',quantity='$quantity_accept',inventory_id='$inventory_id',product_price='$product_price',shipping_charge_purchased='$shipping_charge_purchased',shipping_charge='$shipping_charge',order_item_id='$order_item_id',sub_reason_return_id='$type_of_refund',status='reject',comments='$comments',disable_comm_flag='$comm_flag',customer_status='pending',shipping_charge_with_offer='$shipping_charge_with_offer',product_price_with_offer='$product_price_with_offer',product_price_with_offer_percentage='$product_price_with_offer_percentage',shipping_charge_with_offer_percentage='$shipping_charge_with_offer_percentage',product_price_with_offer_percentage_chk='$product_price_with_offer_percentage_chk',shipping_charge_with_offer_percentage_chk='$shipping_charge_with_offer_percentage_chk'";
				$query=$this->db->query($sql);
			}
			else{
				$sql="update order_return_decision set return_order_id='$return_order_id',customer_id='$customer_id',quantity='$quantity_accept',inventory_id='$inventory_id',product_price='$product_price',shipping_charge_purchased='$shipping_charge_purchased',shipping_charge='$shipping_charge',order_item_id='$order_item_id',sub_reason_return_id='$type_of_refund',status='reject',comments='$comments',disable_comm_flag='$comm_flag',customer_status='pending',shipping_charge_with_offer='$shipping_charge_with_offer',product_price_with_offer='$product_price_with_offer',product_price_with_offer_percentage='$product_price_with_offer_percentage',shipping_charge_with_offer_percentage='$shipping_charge_with_offer_percentage',product_price_with_offer_percentage_chk='$product_price_with_offer_percentage_chk',shipping_charge_with_offer_percentage_chk='$shipping_charge_with_offer_percentage_chk' where order_item_id='$order_item_id'";
				$query=$this->db->query($sql);
			}
			
			$sql_update_returns_desired="update returns_desired set order_return_decision_status='reject',order_return_decision_customer_status='pending' where order_item_id='$order_item_id'";
				$query_update_returns_desired=$this->db->query($sql_update_returns_desired);
			return $query;
		}
		public function return_refund_reject_with_promo_details($return_order_id,$customer_id,$inventory_id,$product_price,$shipping_charge_purchased,$shipping_charge,$order_item_id,$quantity_accept,$type_of_refund,$comments,$disable_comm,$promotion_available,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$promotion_item,$promotion_item_num,$promotion_cashback,$promotion_item_multiplier,$promotion_discount,$default_discount,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_with_promotion,$total_price_of_product_without_promotion){
			
			/* changed for samrick 5/8/2021 starts */
			$product_price_with_offer=round(($quantity_accept*$product_price),2);
			$shipping_charge_with_offer=round(($quantity_accept*$shipping_charge),2);
			$product_price_with_offer_percentage=0;
			$shipping_charge_with_offer_percentage=0;
			$product_price_with_offer_percentage_chk='yes';
			$shipping_charge_with_offer_percentage_chk='yes';
			/* changed for samrick 5/8/2021 ends */
			
			if($disable_comm){
				$comm_flag=1;
			}
			else{
				$comm_flag=0;
			}
			$comments=$this->db->escape_str($comments);
			$sql="select * from order_return_decision where order_item_id='$order_item_id'";
			$res=$this->db->query($sql);
			if($res->num_rows()==0){
				
				 $sql="insert into order_return_decision set return_order_id='$return_order_id',customer_id='$customer_id',quantity='$quantity_accept',inventory_id='$inventory_id',product_price='$product_price',shipping_charge_purchased='$shipping_charge_purchased',shipping_charge='$shipping_charge',order_item_id='$order_item_id',sub_reason_return_id='$type_of_refund',status='reject',comments='$comments',disable_comm_flag='$comm_flag',customer_status='pending',promotion_available='$promotion_available',promotion_minimum_quantity='$promotion_minimum_quantity',promotion_quote='$promotion_quote',promotion_default_discount_promo='$promotion_default_discount_promo',promotion_surprise_gift='$promotion_surprise_gift',promotion_surprise_gift_type='$promotion_surprise_gift_type',promotion_surprise_gift_skus='$promotion_surprise_gift_skus',promotion_surprise_gift_skus_nums='$promotion_surprise_gift_skus_nums',promotion_item='$promotion_item',promotion_item_num='$promotion_item_num',promotion_cashback='$promotion_cashback',promotion_item_multiplier='$promotion_item_multiplier',promotion_discount='$promotion_discount',default_discount='$default_discount',quantity_without_promotion='$quantity_without_promotion',quantity_with_promotion='$quantity_with_promotion',cash_back_value='$cash_back_value',individual_price_of_product_with_promotion='$individual_price_of_product_with_promotion',individual_price_of_product_without_promotion='$individual_price_of_product_without_promotion',total_price_of_product_with_promotion='$total_price_of_product_with_promotion',total_price_of_product_without_promotion='$total_price_of_product_without_promotion',shipping_charge_with_offer='$shipping_charge_with_offer',product_price_with_offer='$product_price_with_offer',product_price_with_offer_percentage='$product_price_with_offer_percentage',shipping_charge_with_offer_percentage='$shipping_charge_with_offer_percentage',product_price_with_offer_percentage_chk='$product_price_with_offer_percentage_chk',shipping_charge_with_offer_percentage_chk='$shipping_charge_with_offer_percentage_chk'";
				$query=$this->db->query($sql);
			}
			else{
				 $sql="update order_return_decision set return_order_id='$return_order_id',customer_id='$customer_id',quantity='$quantity_accept',inventory_id='$inventory_id',product_price='$product_price',shipping_charge_purchased='$shipping_charge_purchased',shipping_charge='$shipping_charge',order_item_id='$order_item_id',sub_reason_return_id='$type_of_refund',status='reject',comments='$comments',disable_comm_flag='$comm_flag',customer_status='pending',promotion_available='$promotion_available',promotion_minimum_quantity='$promotion_minimum_quantity',promotion_quote='$promotion_quote',promotion_default_discount_promo='$promotion_default_discount_promo',promotion_surprise_gift='$promotion_surprise_gift',promotion_surprise_gift_type='$promotion_surprise_gift_type',promotion_surprise_gift_skus='$promotion_surprise_gift_skus',promotion_surprise_gift_skus_nums='$promotion_surprise_gift_skus_nums',promotion_item='$promotion_item',promotion_item_num='$promotion_item_num',promotion_cashback='$promotion_cashback',promotion_item_multiplier='$promotion_item_multiplier',promotion_discount='$promotion_discount',default_discount='$default_discount',quantity_without_promotion='$quantity_without_promotion',quantity_with_promotion='$quantity_with_promotion',cash_back_value='$cash_back_value',individual_price_of_product_with_promotion='$individual_price_of_product_with_promotion',individual_price_of_product_without_promotion='$individual_price_of_product_without_promotion',total_price_of_product_with_promotion='$total_price_of_product_with_promotion',total_price_of_product_without_promotion='$total_price_of_product_without_promotion',shipping_charge_with_offer='$shipping_charge_with_offer',product_price_with_offer='$product_price_with_offer',product_price_with_offer_percentage='$product_price_with_offer_percentage',shipping_charge_with_offer_percentage='$shipping_charge_with_offer_percentage',product_price_with_offer_percentage_chk='$product_price_with_offer_percentage_chk',shipping_charge_with_offer_percentage_chk='$shipping_charge_with_offer_percentage_chk' where order_item_id='$order_item_id'";
				$query=$this->db->query($sql);
			}
			
			$sql_update_returns_desired="update returns_desired set order_return_decision_status='reject',order_return_decision_customer_status='pending' where order_item_id='$order_item_id'";
				$query_update_returns_desired=$this->db->query($sql_update_returns_desired);
			return $query;
		}
		
		
		public function returns_refund_request_orders_admin_rejected($order_item_id){ 
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.*,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id,invoices_offers.*,completed_orders.ord_addon_products_status,completed_orders.ord_addon_products,completed_orders.ord_addon_inventories,completed_orders.ord_addon_total_price,completed_orders.subtotal   from completed_orders,products,returns_desired,orders_status,shipping_address,reason_return,invoices_offers where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='reject' and returns_desired.order_item_id='$order_item_id' and completed_orders.order_id=invoices_offers.order_id";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function returns_refund_request_orders_admin_rejected_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'completed_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'returns_desired.order_item_id',
				3 => 'completed_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id,returns_desired.order_return_decision_status,returns_desired.order_return_decision_customer_decision,returns_desired.order_return_decision_customer_status,returns_desired.order_return_decision_customer_status_comments from completed_orders,products,returns_desired,orders_status,shipping_address,reason_return where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='reject'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( completed_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}	
		
		public function get_unread_return_msg_admin_rejected($return_order_id){
			$sql="select count(*) as count from  return_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='reject'";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		
		public function get_returns_desired_data($order_item_id){
			$sql="select returns_desired.*,history_orders.quantity as purchased_qty,invoices_offers.*,history_orders.ord_addon_products_status,history_orders.ord_addon_products,history_orders.ord_addon_inventories,history_orders.ord_addon_total_price,history_orders.subtotal   from returns_desired,history_orders,invoices_offers where returns_desired.order_item_id='$order_item_id' and returns_desired.order_item_id=history_orders.order_item_id and history_orders.order_id=invoices_offers.order_id";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function get_bank_details($id){
			//$sql="select * from refund_bank_details where id='$id'";
			$sql="select * from delivered_refund_bank_details where id='$id'";
			$result=$this->db->query($sql);
			
			if($result->num_rows()>0){
				//print_r($result->result_array());exit;
				return $result->result_array();
			}
			else{
				return false;
			}
		}
		
		public function returns_issue_full_refund($order_item_id,$return_order_id,$total_refund_without_concession,$return_shipping_concession,$return_other_concession,$total_refund_with_concession,$item_price_without_concession,$item_deduction_percentage,$shipping_price_without_concession,$shipping_deduction_percentage,$item_with_deductions_concession,$shipping_with_deductions_concession,$memo_to_buyer,$memo_to_seller,$refund_method,$refund_bank_id,$customer_id,$quantity_refund){
			$memo_to_buyer=$this->db->escape_str($memo_to_buyer);
			$memo_to_seller=$this->db->escape_str($memo_to_seller);
			$sql="insert into return_refund set 
							order_item_id='$order_item_id',
							return_order_id='$return_order_id',
							total_refund_without_concession='$total_refund_without_concession',
							return_shipping_concession='$return_shipping_concession',
							return_other_concession='$return_other_concession',
							total_refund_with_concession='$total_refund_with_concession',
							item_price_without_concession='$item_price_without_concession',
							item_deduction_percentage='$item_deduction_percentage',
							shipping_price_without_concession='$shipping_price_without_concession',
							shipping_deduction_percentage='$shipping_deduction_percentage',
							item_with_deductions_concession='$item_with_deductions_concession',
							shipping_with_deductions_concession='$shipping_with_deductions_concession',
							memo_to_buyer='$memo_to_buyer',
							memo_to_seller='$memo_to_seller',
							refund_method='$refund_method',
							refund_bank_id='$refund_bank_id',
							customer_id='$customer_id',
							type_of_refund='full_refund',quantity='$quantity_refund'";
			$query=$this->db->query($sql);
			
			$sql_order_return_decision="update order_return_decision set status='refund' where order_item_id='$order_item_id'";
			$query_order_return_decision=$this->db->query($sql_order_return_decision);
			
			$sql_returns_desired="update  returns_desired set order_return_decision_status='refund' where order_item_id='$order_item_id'";
			$query_returns_desired=$this->db->query($sql_returns_desired);
			
			return $query;
		}
		
		public function returns_issue_partial_refund($order_item_id,$return_order_id,$item_price_without_concession,$item_deduction_percentage,$shipping_price_without_concession,$shipping_deduction_percentage,$total_refund_without_concession,$item_with_deductions_concession,$shipping_with_deductions_concession,$return_shipping_concession,$return_other_concession,$total_refund_with_concession,$refund_method,$refund_bank_id,$customer_id,$memo_to_buyer_partial_refund,$memo_to_seller_partial_refund,$quantity_refund){
			
			$memo_to_buyer_partial_refund=$this->db->escape_str($memo_to_buyer_partial_refund);
			$memo_to_seller_partial_refund=$this->db->escape_str($memo_to_seller_partial_refund);
			$sql="insert into return_refund set 
							order_item_id='$order_item_id',
							return_order_id='$return_order_id',
							item_price_without_concession='$item_price_without_concession',
							item_deduction_percentage='$item_deduction_percentage',
							shipping_price_without_concession='$shipping_price_without_concession',
							shipping_deduction_percentage='$shipping_deduction_percentage',
							total_refund_without_concession='$total_refund_without_concession',
							item_with_deductions_concession='$item_with_deductions_concession',
							shipping_with_deductions_concession='$shipping_with_deductions_concession',
							return_shipping_concession='$return_shipping_concession',
							return_other_concession='$return_other_concession',
							total_refund_with_concession='$total_refund_with_concession',
							refund_method='$refund_method',
							refund_bank_id='$refund_bank_id',
							customer_id='$customer_id',
							memo_to_buyer='$memo_to_buyer_partial_refund',
							memo_to_seller='$memo_to_seller_partial_refund',
							type_of_refund='partial_refund',quantity='$quantity_refund'";
			$query=$this->db->query($sql);
			
			$sql_order_return_decision="update order_return_decision set status='refund' where order_item_id='$order_item_id'";
			$query_order_return_decision=$this->db->query($sql_order_return_decision);
			
			$sql_returns_desired="update  returns_desired set order_return_decision_status='refund' where order_item_id='$order_item_id'";
			$query_returns_desired=$this->db->query($sql_returns_desired);
	
			return $query;
		}
		
		public function get_refund_status($order_item_id){
			$sql="select count(*) as count from return_refund where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			if($query->row()->count>0){
				return true;
			}
			else{
				return false;
			}
		}
		
		public function returns_refund_request_orders_refund_inprocess_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'completed_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'returns_desired.order_item_id',
				3 => 'completed_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id,returns_desired.allow_update_bank_details,if(returns_desired.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') as type_of_transaction,returns_desired.refund_method,invoices_offers.* from completed_orders,invoices_offers,products,returns_desired,orders_status,shipping_address,reason_return where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and invoices_offers.order_id=completed_orders.order_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='refund'";
			// check search value exist
			if( !empty($params['search']['value']) ){
				$where .=" and ";
				$where .=" ( completed_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				
				$where .=" OR if(returns_desired.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') LIKE '%".$params['search']['value']."%' ";
				
				$where .=" OR completed_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}

		public function get_refund_details($order_item_id){
			$sql="select * from return_refund where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function returns_refund_request_orders_accounts_incoming($order_item_id){ 
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id,returns_desired.refund_method,returns_desired.refund_bank_id,returns_desired.refund_method from completed_orders,products,returns_desired,orders_status,shipping_address,reason_return where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='refund' and returns_desired.order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function returns_refund_request_orders_accounts_incoming_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'completed_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'returns_desired.order_item_id',
				3 => 'completed_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id,returns_desired.allow_update_bank_details, if(returns_desired.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') as type_of_transaction,returns_desired.refund_method,completed_orders.razorpayPaymentId,completed_orders.razorpayOrderId from completed_orders,products,returns_desired,orders_status,shipping_address,reason_return where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='refund'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( completed_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				
				$where .=" OR if(returns_desired.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') LIKE '%".$params['search']['value']."%' ";
				
				$where .=" OR completed_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		public function returns_desired_table_data($order_item_id){
			$sql="select * from returns_desired where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row_array();
		}
		public function return_refund_success($order_item_id,$transaction_id,$transaction_date,$transaction_time,$razorpay_refund_id='',$razorpay_refund_status=''){
			
			$get_returns_desired_table_data=$this->returns_desired_table_data($order_item_id);
			if($get_returns_desired_table_data["refund_method"]=="Bank Transfer"){
				$refund_bank_id=$get_returns_desired_table_data["refund_bank_id"];
				$sql="select * from refund_bank_details where id='$refund_bank_id'";
				$query=$this->db->query($sql);
				$refund_details_arr=$query->row_array();
				$sql="insert into delivered_refund_bank_details set 	
					bank_name='$refund_details_arr[bank_name]',
					branch_name='$refund_details_arr[branch_name]',
					city='$refund_details_arr[city]',
					ifsc_code='$refund_details_arr[ifsc_code]',
					account_number='$refund_details_arr[account_number]',
					confirm_account_number='$refund_details_arr[confirm_account_number]',
					account_holder_name='$refund_details_arr[account_holder_name]',
					mobile_number='$refund_details_arr[mobile_number]',
					customer_id='$refund_details_arr[customer_id]'";
					$query=$this->db->query($sql);
			}
			//$sql="update orders_status set order_return_pickup='0' where order_item_id='$order_item_id'";
			
			//$query=$this->db->query($sql);
			//$sql="update orders_status set initiate_return_pickup='' where order_item_id='$order_item_id'";	
			//$query=$this->db->query($sql);
			
			$sql="update returns_desired set order_return_decision_status='refunded',status_of_refund='yes',transaction_id='$transaction_id',transaction_date='$transaction_date',transaction_time='$transaction_time',razorpay_refund_id='$razorpay_refund_id',razorpay_refund_status='$razorpay_refund_status' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			$sql_order_return_decision="update order_return_decision set status='refunded' where order_item_id='$order_item_id'";
			
			$query_order_return_decision=$this->db->query($sql_order_return_decision);
			
			/* notify customer */
			
			$sql_noti="update refund_notification set refund_success_view=1,refund_success_timestamp=now() where order_item_id='$order_item_id'";
			$query_notification=$this->db->query($sql_noti);
			
			$sql_delete_returned_orders="delete from returned_orders where order_item_id='$order_item_id'";
			$query_delete_returned_orders=$this->db->query($sql_delete_returned_orders);
			
			
			$sql_returned_orders="insert into returned_orders select * from completed_orders where order_item_id='$order_item_id'";
			$query_returned_orders=$this->db->query($sql_returned_orders);
			/////////////////////
			$sql_get_refund_data="select * from order_return_decision where order_item_id='$order_item_id'";
			$query_get_refund_data=$this->db->query($sql_get_refund_data);
			$quantity_to_be_refuned=$query_get_refund_data->row()->quantity;
			$subtotal_to_be_refuned=($query_get_refund_data->row()->product_price)*($query_get_refund_data->row()->quantity);
			$grandtotal_to_be_refuned=($query_get_refund_data->row()->product_price+$query_get_refund_data->row()->shipping_charge)*($query_get_refund_data->row()->quantity);
			
			$sql_update_returned_orders="update returned_orders set subtotal='$subtotal_to_be_refuned',grandtotal='$grandtotal_to_be_refuned',quantity='$quantity_to_be_refuned' where order_item_id='$order_item_id'";
			$query_update_returned_orders=$this->db->query($sql_update_returned_orders);
			///////////////////////
			$sql_get_quantity_completed_orders="select quantity from completed_orders where order_item_id='$order_item_id'";
			$query_get_quantity_completed_orders=$this->db->query($sql_get_quantity_completed_orders);
			$quantity_purchased=$query_get_quantity_completed_orders->row()->quantity;
			////////////////////////
			$sql_return_refund_total_refund_with_concession="select item_with_deductions_concession,shipping_with_deductions_concession from return_refund where order_item_id='$order_item_id'";
			$query_return_refund_total_refund_with_concession=$this->db->query($sql_return_refund_total_refund_with_concession);
			$item_with_deductions_concession=$query_return_refund_total_refund_with_concession->row()->item_with_deductions_concession;
			$shipping_with_deductions_concession=$query_return_refund_total_refund_with_concession->row()->shipping_with_deductions_concession;
			$total_refund_with_concession_withoutotheranyconcessions=$item_with_deductions_concession+$shipping_with_deductions_concession;
			///////////////////////////////////////////////
			if(($quantity_to_be_refuned==$quantity_purchased) && ($grandtotal_to_be_refuned==$total_refund_with_concession_withoutotheranyconcessions)){
				$sql_completed_orders="delete from completed_orders where order_item_id='$order_item_id'";
				$query_completed_orders=$this->db->query($sql_completed_orders);
			}
			
			//////
			$sql_order_replacement_decision="select * from order_replacement_decision where order_item_id='$order_item_id'";
			$query_order_replacement_decision=$this->db->query($sql_order_replacement_decision);
			$query_order_replacement_decision_arr=$query_order_replacement_decision->row_array();
			if(count($query_order_replacement_decision_arr)>0){
				if($query_order_replacement_decision_arr["status"]=="refunded"){
					if(($quantity_to_be_refuned+$query_order_replacement_decision_arr["quantity"])==$quantity_purchased){
						$sql_completed_orders="delete from completed_orders where order_item_id='$order_item_id'";
						$query_completed_orders=$this->db->query($sql_completed_orders);
					}
				}
			}
			//////
			/*$returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin_obj=$this->returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin($order_item_id);
			if(count($returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin_obj)>0){
				if(($quantity_to_be_refuned+$returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin_obj->quantity)==$quantity_purchased){
						$sql_completed_orders="delete from completed_orders where order_item_id='$order_item_id'";
						$query_completed_orders=$this->db->query($sql_completed_orders);
					}
			}*/
			/////////////////////
			
			
			return $query;
		}
		public function get_grandtotal_of_return_refund_success($order_item_id){
			$sql="select sum(item_with_deductions_concession) as item_with_deductions_concession,sum(shipping_with_deductions_concession) as shipping_with_deductions_concession from return_refund where order_item_id='$order_item_id' union select sum(item_with_deductions_concession) as item_with_deductions_concession,sum(shipping_with_deductions_concession) as shipping_with_deductions_concession from sub_return_refund where order_item_id='$order_item_id' ";
			$query=$this->db->query($sql);
			return $query->row()->item_with_deductions_concession+$query->row()->shipping_with_deductions_concession;
		}
		public function get_grandtotal_of_purchased_quantity($order_item_id){
			$sql="select grandtotal from orders_status where order_item_id='$order_item_id' ";
			$query=$this->db->query($sql);
			return $query->row()->grandtotal;
		}
		public function returns_refund_request_orders_refund_success($order_item_id){
			
			$sql = "select history_orders.grandtotal as grandtotal_of_quantity_purchased,history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,returns_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,history_orders.customer_id,returns_desired.transaction_id,returns_desired.transaction_date,returns_desired.transaction_time,inventory.moq,invoices_offers.*,returns_desired.order_item_invoice_discount_value_each from history_orders,products,returns_desired,orders_status,shipping_address,reason_return,inventory,invoices_offers where history_orders.order_item_id=returns_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='refunded' and returns_desired.order_item_id='$order_item_id' and history_orders.inventory_id=inventory.id and history_orders.order_id=invoices_offers.order_id";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function returns_refund_request_orders_refund_success_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'history_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'returns_desired.order_item_id',
				3 => 'history_orders.order_id'
			);

			$status_of_refund=$params["status_of_refund"];
			if($status_of_refund=="all"){
				$str="((orders_status.order_return_pickup='0' and orders_status.initiate_return_pickup!='0' and orders_status.initiate_return_pickup!='1') or (orders_status.order_return_pickup='1' and trim(orders_status.initiate_return_pickup)=trim('')) or (orders_status.order_return_pickup='0' and orders_status.initiate_return_pickup!='0' and orders_status.initiate_return_pickup!='1') or (orders_status.order_return_pickup='1' and orders_status.initiate_return_pickup='1' ) or (orders_status.order_return_pickup='0' and (orders_status.initiate_return_pickup='0' or orders_status.initiate_return_pickup='1' ) ) )";
			}
			if($status_of_refund=="success"){
				$str="((orders_status.order_return_pickup='1' and trim(orders_status.initiate_return_pickup)=trim('')) or (orders_status.order_return_pickup='1' and orders_status.initiate_return_pickup='1' ) or (orders_status.order_return_pickup='0' and orders_status.initiate_return_pickup='0' ) )";
			}
			if($status_of_refund=="pending"){
				$str="((orders_status.order_return_pickup='0' and orders_status.initiate_return_pickup='1' ) )";
			}
		
		
			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select orders_status.order_return_pickup,orders_status.order_return_pickup_again,orders_status.initiate_return_pickup,orders_status.initiate_return_pickup_again,history_orders.grandtotal as grandtotal_of_quantity_purchased,history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,returns_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,history_orders.cash_back_value as purchased_cash_back_value,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,history_orders.customer_id,returns_desired.transaction_id,returns_desired.transaction_date,returns_desired.transaction_time,invoices_offers.* from history_orders,invoices_offers,products,returns_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=returns_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and invoices_offers.order_id=history_orders.order_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='refunded' and $str";
			
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( history_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		public function returns_refund_request_orders_accounts_success_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'history_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'returns_desired.order_item_id',
				3 => 'history_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,returns_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,history_orders.customer_id,returns_desired.transaction_id,returns_desired.transaction_date,returns_desired.transaction_time,returns_desired.refund_method,returns_desired.razorpay_refund_id,returns_desired.razorpay_refund_status from history_orders,products,returns_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=returns_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='refunded'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( history_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		public function get_refund_request_accounts_success_info($orders_status_id,$order_item_id){
			
				$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,returns_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,history_orders.customer_id,returns_desired.refund_method,returns_desired.refund_bank_id,returns_desired.transaction_id,returns_desired.transaction_date,returns_desired.transaction_time from history_orders,products,returns_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=returns_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and  returns_desired.order_return_decision_status='refunded'  and orders_status.id='$orders_status_id'";
			
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function return_refund_failure($order_item_id,$refund_fail_comments,$allow_update_bank_details){
			if($allow_update_bank_details){
				$allow_update_bank_details="yes";
			}
			else{
				$allow_update_bank_details="no";
			}
			$refund_fail_comments=$this->db->escape_str($refund_fail_comments);
			$sql="update returns_desired set order_return_decision_status='not refunded',status_of_refund='no',refund_fail_comments='$refund_fail_comments',allow_update_bank_details='$allow_update_bank_details' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			$sql_order_return_decision="update order_return_decision set status='not refunded' where order_item_id='$order_item_id'";
			$query_order_return_decision=$this->db->query($sql_order_return_decision);
			
			return $query;
		}
		
		public function returns_refund_request_orders_refund_failure($order_item_id){
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id,returns_desired.refund_fail_comments from completed_orders,products,returns_desired,orders_status,shipping_address,reason_return where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='not refunded' and returns_desired.order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function returns_refund_request_orders_refund_failure_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'completed_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'returns_desired.order_item_id',
				3 => 'completed_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id,returns_desired.refund_fail_comments,returns_desired.allow_update_bank_details,returns_desired.refund_method from completed_orders,products,returns_desired,orders_status,shipping_address,reason_return where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='not refunded'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( completed_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}

		public function returns_refund_request_orders_accounts_failure_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'completed_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'returns_desired.order_item_id',
				3 => 'completed_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id,returns_desired.refund_fail_comments from completed_orders,products,returns_desired,orders_status,shipping_address,reason_return where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='not refunded'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( completed_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}

		public function get_unread_return_msg_refund_failure($return_order_id){
			$sql="select count(*) as count from return_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='not refunded' and customer_action='not refunded'";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		
		public function resend_return_refund($order_item_id){
			$sql="update returns_desired set order_return_decision_status='refund' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			$sql="update order_return_decision set status='refund' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			$sql_acc="update return_refund set notify_refund_account='1' where order_item_id='$order_item_id'";
			$query_acc=$this->db->query($sql_acc);
			
			return $query;
		}
		
		public function returns_refund_request_orders_refund_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'completed_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'returns_desired.order_item_id',
				3 => 'completed_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id,invoices_offers.* from completed_orders,invoices_offers,products,returns_desired,orders_status,shipping_address,reason_return where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and invoices_offers.order_id=completed_orders.order_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='accept' and returns_desired.order_return_decision_customer_decision='accept' and ((orders_status.order_return_pickup='1' and orders_status.initiate_return_pickup!='0' and orders_status.initiate_return_pickup!='1') or (orders_status.order_return_pickup='0' and orders_status.initiate_return_pickup='1') or (orders_status.order_return_pickup='0' and orders_status.initiate_return_pickup='0'))";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( completed_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}

		
		public function update_order_return_pickup($orders_status_id,$return_order_id,$order_item_id,$update_inventory_stock_when_item_picked_up){
			$sql="update orders_status set order_return_pickup='1',return_order_id='$return_order_id' ,update_stock_order_return_pickup='$update_inventory_stock_when_item_picked_up' where id='$orders_status_id'";
			$query=$this->db->query($sql);
			return $query;
		}
		
		public function update_order_replacement_pickup($orders_status_id,$return_order_id,$order_item_id,$update_inventory_stock_when_item_picked_up,$return_shipping_concession){
			
			if($return_shipping_concession==0 || $return_shipping_concession==''){
				$return_shipping_concession=0;
			}else{
				$sql_r="update order_replacement_decision set return_shipping_concession='$return_shipping_concession' where order_item_id='$order_item_id'";
				$query_r=$this->db->query($sql_r);
			}
	
			$sql_replacement_desired="select * from replacement_desired where order_item_id='$order_item_id'";
			$query_replacement_desired=$this->db->query($sql_replacement_desired);
			
			$sql_order_replacement_decision="select * from order_replacement_decision where order_item_id='$order_item_id'";
			
			$query_order_replacement_decision=$this->db->query($sql_order_replacement_decision);
			if(count($query_order_replacement_decision->row_array())>0 && $query_replacement_desired->row()->paid_by=="customer"){
				$sql="update order_replacement_decision set customer_decision='paybalance' where order_item_id='$order_item_id'";
				$query=$this->db->query($sql);
			}

			
			$sql="update orders_status set order_replacement_pickup='1',return_order_id='$return_order_id',update_stock_order_replacement_pickup='$update_inventory_stock_when_item_picked_up' where id='$orders_status_id'";
			$query=$this->db->query($sql);
			return $query;
		}
		
		public function get_vendor_id_by_order_item_id($order_item_id){
			$sql="select vendor_id from completed_orders where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			$vendor_id=$query->row()->vendor_id;
			return $vendor_id;
		}
		
		public function send_return_lable($vendor_id,$order_item_id){
			$sql="update order_return_decision set vendor_id='$vendor_id' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query;
		}
		
		public function view_vendor_info_pdf($vendor_id){
			$sql="select * from vendors where vendor_id='$vendor_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function activate_wallet_customer($customer_id){
			$sql="select * from wallet where customer_id='$customer_id' ";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return false;
			}
			else{
				$sql="insert into wallet set customer_id='$customer_id'";
				$result=$this->db->query($sql);
				if($this->db->affected_rows() > 0){
						return true;
					}
					else{
						return false;
					}
			}
		}
		public function get_wallet_exists_for_customer($customer_id){
			$sql="select * from wallet where customer_id='$customer_id'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return true;
			}
			else{
				return false;
			}
		}
		
		public function get_customer_wallet_summary($customer_id){
			$sql="select * from wallet where customer_id='$customer_id' ";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->result_array();
			}
			else{
				return false;
			}
		}
		
	public function update_customer_wallet($wallet_id,$customer_id,$transaction_details,$debit,$credit,$amount,$credit_bank_transfer="",$wallet_type=''){
		if(!empty($credit_bank_transfer)){
				$credit_bank_transfer=1;
			}
			else{
				$credit_bank_transfer=0;
			}
		$transaction_details=$this->db->escape_str($transaction_details);
		
		$sql="insert into wallet_transaction (wallet_id,customer_id,transaction_details,debit,credit,amount,credit_bank_transfer,wallet_type) values('$wallet_id','$customer_id','$transaction_details','$debit','$credit','$amount','$credit_bank_transfer','$wallet_type')";
		$result=$this->db->query($sql);
		$wallet_transaction_id=$this->db->insert_id();
		
		$sql="update wallet set wallet_amount='$amount' where wallet_id='$wallet_id' and customer_id='$customer_id'";
		$result=$this->db->query($sql);
		return $wallet_transaction_id;
			
	}
	
	public function transfer_to_wallet($order_item_id){
		
			//$sql="update refund set refund_type='Wallet' where order_item_id='$order_item_id'";
			//$query=$this->db->query($sql);
			
			$sql="update returns_desired set order_return_decision_status='refund',refund_method='Wallet' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			$sql="update return_refund set refund_method='Wallet',notify_refund_account='1' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			$sql="update order_return_decision set status='refund' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			
			return $query;
		}
		
		
		public function returns_refund_request_orders_refund_success_full_refund_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'history_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'returns_desired.order_item_id',
				3 => 'history_orders.order_id'
			);

			$status_of_refund=$params["status_of_refund"];
			if($status_of_refund=="all"){
				$str="and ( (orders_status.order_return_pickup='1' and trim(orders_status.initiate_return_pickup)=trim('')) or (orders_status.order_return_pickup='1' and orders_status.initiate_return_pickup!='0' and orders_status.initiate_return_pickup!='1') or (orders_status.order_return_pickup='1' and orders_status.initiate_return_pickup='1' ) or (orders_status.order_return_pickup='0' and (orders_status.initiate_return_pickup='0' or orders_status.initiate_return_pickup='1' ) ) )";
			}
			if($status_of_refund=="success"){
				$str="and ((orders_status.order_return_pickup='1' and trim(orders_status.initiate_return_pickup)=trim('')) or  (orders_status.order_return_pickup='1' and orders_status.initiate_return_pickup='1' ) or (orders_status.order_return_pickup='0' and orders_status.initiate_return_pickup='0' ))";
			}
			if($status_of_refund=="pending"){
				$str="and ((orders_status.order_return_pickup='0' and orders_status.initiate_return_pickup='1' ) )";
			}
			$where = $sqlTot = $sqlRec = "";
			
			$str="";
			
			// getting total number records without any search
			
			$sql = "select history_orders.grandtotal as grandtotal_of_quantity_purchased,history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,returns_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,history_orders.cash_back_value as purchased_cash_back_value,orders_status.order_delivered_timestamp,orders_status.initiate_return_pickup,orders_status.initiate_return_pickup_again,orders_status.order_return_pickup,orders_status.order_return_pickup_again,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,history_orders.customer_id,returns_desired.transaction_id,returns_desired.transaction_date,returns_desired.transaction_time,invoices_offers.*,history_orders.ord_addon_products_status,history_orders.ord_addon_products,history_orders.ord_addon_inventories,history_orders.ord_addon_total_price,history_orders.subtotal from history_orders,invoices_offers,products,returns_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=returns_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and invoices_offers.order_id=history_orders.order_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='refunded' $str";

			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( history_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		public function get_refund_request_refund_success_full_refund_info($orders_status_id,$order_item_id){
			
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,returns_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,history_orders.customer_id from history_orders,products,returns_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=returns_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and  returns_desired.order_return_decision_status='refunded'  and orders_status.id='$orders_status_id'";
			
			
			$query=$this->db->query($sql);
			return $query->row();
		}
			
		public function return_refund_request_again_not_refunded($order_item_id){
			$sql="select * from sub_returns_desired where order_item_id='$order_item_id' and admin_action!='refunded'";
			$query=$this->db->query($sql);
			return $query->row_array();
		}
		public function request_again_admin_action($admin_action,$order_item_id){
			if($admin_action=="rejected"){
				$sql="update sub_returns_desired set admin_action='rejected' where order_item_id='$order_item_id' and admin_action='pending'";
				$query=$this->db->query($sql);
			}
			if($admin_action=="accepted"){
				
				$refund_remaining_item_deductions=$this->input->post("refund_remaining_item_deductions");
				
				$refund_remaining_quantities=$this->input->post("refund_remaining_quantities");
				
				$refund_remaining_shipping_deductions=$this->input->post("refund_remaining_shipping_deductions");
	
				$refund_all=$this->input->post("refund_all");
				$promotion_available=$this->input->post("promotion_available");
				
				$sql_get_refund_stuff="select * from sub_returns_desired where  order_item_id='$order_item_id' and admin_action='pending'";
				
				$query_get_refund_stuff=$this->db->query($sql_get_refund_stuff);
				$query_get_refund_stuff_arr=$query_get_refund_stuff->row_array();
				
				$item_price_deducted_admin=0;
				$shipping_price_deducted_admin=0;
				$total_price_deducted_admin=0;
				$quantity_requested_admin=0;
					
				if($refund_remaining_item_deductions || $refund_remaining_shipping_deductions){
					$refund_remaining_deductions_admin='refund_remaining_deductions';
					if($refund_remaining_item_deductions){
						$item_price_deducted_admin=$query_get_refund_stuff_arr["item_price_deducted"];
					}
					else{
						$item_price_deducted_admin=0;
					}
					if($refund_remaining_shipping_deductions){
						$shipping_price_deducted_admin=$query_get_refund_stuff_arr["shipping_price_deducted"];
					}
					else{
						$shipping_price_deducted_admin=0;
					}
					$total_price_deducted_admin=$item_price_deducted_admin+$shipping_price_deducted_admin;
				}
				else{ 
					$refund_remaining_deductions_admin='';
					
				}
				
				$quantity_requested=$this->input->post("quantity_requested");
				
				if($refund_remaining_quantities){ 
						$quantity_requested_admin=$quantity_requested;
						if($promotion_available==1){
							
							$quantity_without_promotion_admin=$this->input->post("decided_quantity_without_promotion");
							$quantity_with_promotion_admin=$this->input->post("decided_quantity_with_promotion");
							$cash_back_value_admin=$this->input->post("decided_cash_back_value");
							$individual_price_of_product_with_promotion_admin=$this->input->post("decided_individual_price_of_product_with_promotion");
							$individual_price_of_product_without_promotion_admin=$this->input->post("decided_individual_price_of_product_without_promotion"); 
							$total_price_of_product_with_promotion_admin=$this->input->post("decided_total_price_of_product_with_promotion");
							$total_price_of_product_without_promotion_admin=$this->input->post("decided_total_price_of_product_without_promotion");
	
						}
						
				}else{
					$quantity_requested_admin=0;
				}
				
				if($refund_remaining_quantities){ 
					$refund_remaining_quantities_admin='refund_remaining_quantities';
					$total_product_price_requested_with_promo_admin=$this->input->post("total_product_price_requested_with_promo_admin");
				}else{ 
					
					$refund_remaining_quantities_admin='';
					$total_product_price_requested_with_promo_admin=0;
				}
				
				if($refund_all){
					
					$refund_all_admin='refund_all';
					
					if($refund_remaining_item_deductions){
						$item_price_deducted_admin=$query_get_refund_stuff_arr["item_price_deducted"];
					}
					else{
						$item_price_deducted_admin=0;
					}
					if($refund_remaining_shipping_deductions){
						$shipping_price_deducted_admin=$query_get_refund_stuff_arr["shipping_price_deducted"];
					}
					else{
						$shipping_price_deducted_admin=0;
					}
					
					$total_price_deducted_admin=$item_price_deducted_admin+$shipping_price_deducted_admin;	
					
				}
				else{ 
					$refund_all_admin='';
				}
				
				if($promotion_available==1){
					$sql="update sub_returns_desired set refund_remaining_deductions_admin='$refund_remaining_deductions_admin',refund_remaining_quantities_admin='$refund_remaining_quantities_admin',refund_all_admin='$refund_all_admin',item_price_deducted_admin='$item_price_deducted_admin',shipping_price_deducted_admin='$shipping_price_deducted_admin',total_price_deducted_admin='$total_price_deducted_admin',quantity_requested_admin='$quantity_requested_admin',total_product_price_requested_with_promo_admin='$total_product_price_requested_with_promo_admin',quantity_without_promotion_admin='$quantity_without_promotion_admin',quantity_with_promotion_admin='$quantity_with_promotion_admin',cash_back_value_admin='$cash_back_value_admin',individual_price_of_product_with_promotion_admin='$individual_price_of_product_with_promotion_admin',individual_price_of_product_without_promotion_admin='$individual_price_of_product_without_promotion_admin',total_price_of_product_with_promotion_admin='$total_price_of_product_with_promotion_admin',total_price_of_product_without_promotion_admin='$total_price_of_product_without_promotion_admin',admin_action='accepted' where order_item_id='$order_item_id' and admin_action='pending'";
				
				}else{
					$sql="update sub_returns_desired set refund_remaining_deductions_admin='$refund_remaining_deductions_admin',refund_remaining_quantities_admin='$refund_remaining_quantities_admin',refund_all_admin='$refund_all_admin',item_price_deducted_admin='$item_price_deducted_admin',shipping_price_deducted_admin='$shipping_price_deducted_admin',total_price_deducted_admin='$total_price_deducted_admin',quantity_requested_admin='$quantity_requested_admin',total_product_price_requested_with_promo_admin='$total_product_price_requested_with_promo_admin',admin_action='accepted' where order_item_id='$order_item_id' and admin_action='pending'";
				}
				
				
				$query=$this->db->query($sql);
			}
			return $query;
		}
		
		public function returns_refund_request_orders_refund_again_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'history_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'returns_desired.order_item_id',
				3 => 'history_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,returns_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,history_orders.customer_id,sub_returns_desired.*,invoices_offers.*,returns_desired.order_item_invoice_discount_value_each from history_orders,invoices_offers,products,returns_desired,orders_status,shipping_address,reason_return,sub_returns_desired where history_orders.order_item_id=returns_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and invoices_offers.order_id=history_orders.order_id and reason_return.reason_id=returns_desired.reason_return_id and sub_returns_desired.order_item_id=orders_status.order_item_id and (sub_returns_desired.admin_action='accepted' or sub_returns_desired.admin_action='refund') and ((orders_status.order_return_pickup_again='1' and trim(orders_status.initiate_return_pickup_again)=trim('')) or (orders_status.order_return_pickup_again='0' and orders_status.initiate_return_pickup_again='1') or (orders_status.order_return_pickup_again='0' and orders_status.initiate_return_pickup_again='0') or (orders_status.order_return_pickup_again='0' and trim(orders_status.initiate_return_pickup_again)=trim('') and sub_returns_desired.quantity_requested_admin=0))";
			
			//
			
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( history_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}

		public function return_refund_request_again_need_to_refund($order_item_id){
			$sql="select * from sub_returns_desired where order_item_id='$order_item_id' and admin_action='accepted'";
			$query=$this->db->query($sql);
			return $query->row_array();
		}
		
		public function return_refund_table_data_by_order_item_id($order_item_id){
			$sql="select * from return_refund where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row_array();
		}
		
		public function sub_return_refund_table_data_refunded_by_order_item_id($order_item_id){
			$sql="select sub_return_refund.* from sub_return_refund,sub_returns_desired where sub_return_refund.order_item_id='$order_item_id' and sub_return_refund.sub_returns_desired_id=sub_returns_desired.sub_returns_desired_id and sub_returns_desired.admin_action='refunded'";
			$query=$this->db->query($sql);
			return $query->row_array();
		}
		
		public function returns_issue_partial_refund_again($order_item_id,$return_order_id,$item_price_without_concession,$item_deduction_percentage,$shipping_price_without_concession,$shipping_deduction_percentage,$total_refund_without_concession,$item_with_deductions_concession,$shipping_with_deductions_concession,$return_shipping_concession,$return_other_concession,$total_refund_with_concession,$refund_method,$refund_bank_id,$customer_id,$memo_to_buyer_partial_refund,$memo_to_seller_partial_refund,$quantity_refund,$sub_returns_desired_id,$deducted_item_price,$deducted_shipping_price){
			
			$memo_to_buyer_partial_refund=$this->db->escape_str($memo_to_buyer_partial_refund);
			$memo_to_seller_partial_refund=$this->db->escape_str($memo_to_seller_partial_refund);
			
			$sql="insert into sub_return_refund set 
							order_item_id='$order_item_id',
							return_order_id='$return_order_id',
							item_price_without_concession='$item_price_without_concession',
							item_deduction_percentage='$item_deduction_percentage',
							shipping_price_without_concession='$shipping_price_without_concession',
							shipping_deduction_percentage='$shipping_deduction_percentage',
							total_refund_without_concession='$total_refund_without_concession',
							item_with_deductions_concession='$item_with_deductions_concession',
							shipping_with_deductions_concession='$shipping_with_deductions_concession',
							return_shipping_concession='$return_shipping_concession',
							return_other_concession='$return_other_concession',
							total_refund_with_concession='$total_refund_with_concession',
							refund_method='$refund_method',
							refund_bank_id='$refund_bank_id',
							customer_id='$customer_id',
							memo_to_buyer='$memo_to_buyer_partial_refund',
							memo_to_seller='$memo_to_seller_partial_refund',
							type_of_refund='partial_refund',quantity='$quantity_refund',sub_returns_desired_id='$sub_returns_desired_id',
							deducted_item_price='$deducted_item_price',
							deducted_shipping_price='$deducted_shipping_price'";
			$query=$this->db->query($sql);
			
			$sql_sub_returns_desired="update sub_returns_desired set admin_action='refund' where sub_returns_desired_id='$sub_returns_desired_id'";
			$query_sub_returns_desired=$this->db->query($sql_sub_returns_desired);
			
			return $query;
		}
		
		public function returns_issue_full_refund_again($order_item_id,$return_order_id,$total_refund_without_concession,$return_shipping_concession,$return_other_concession,$total_refund_with_concession,$item_price_without_concession,$item_deduction_percentage,$shipping_price_without_concession,$shipping_deduction_percentage,$item_with_deductions_concession,$shipping_with_deductions_concession,$memo_to_buyer,$memo_to_seller,$refund_method,$refund_bank_id,$customer_id,$quantity_refund,$sub_returns_desired_id,$deducted_item_price,$deducted_shipping_price){
			
			$memo_to_buyer=$this->db->escape_str($memo_to_buyer);
			$memo_to_seller=$this->db->escape_str($memo_to_seller);
			$sql="insert into sub_return_refund set 
							order_item_id='$order_item_id',
							return_order_id='$return_order_id',
							total_refund_without_concession='$total_refund_without_concession',
							return_shipping_concession='$return_shipping_concession',
							return_other_concession='$return_other_concession',
							total_refund_with_concession='$total_refund_with_concession',
							item_price_without_concession='$item_price_without_concession',
							item_deduction_percentage='$item_deduction_percentage',
							shipping_price_without_concession='$shipping_price_without_concession',
							shipping_deduction_percentage='$shipping_deduction_percentage',
							item_with_deductions_concession='$item_with_deductions_concession',
							shipping_with_deductions_concession='$shipping_with_deductions_concession',
							memo_to_buyer='$memo_to_buyer',
							memo_to_seller='$memo_to_seller',
							refund_method='$refund_method',
							refund_bank_id='$refund_bank_id',
							customer_id='$customer_id',
							type_of_refund='full_refund',quantity='$quantity_refund',
							sub_returns_desired_id='$sub_returns_desired_id',
							deducted_item_price='$deducted_item_price',
							deducted_shipping_price='$deducted_shipping_price'";
			$query=$this->db->query($sql);
			
			$sql_sub_returns_desired="update sub_returns_desired set admin_action='refund',notify_refund_again_full=1 where sub_returns_desired_id='$sub_returns_desired_id'";
			$query_sub_returns_desired=$this->db->query($sql_sub_returns_desired);
			
			return $query;
		}
		
		public function returns_refund_request_orders_accounts_incoming_again($order_item_id){ 
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,returns_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,history_orders.customer_id,returns_desired.allow_update_bank_details, if(returns_desired.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') as type_of_transaction,returns_desired.refund_method as returns_desired_refund_method,sub_returns_desired.refund_method as sub_returns_desired_refund_method,sub_returns_desired.timestamp as sub_returns_desired_timestamp,sub_returns_desired.quantity_requested as sub_returns_desired_quantity_requested,sub_returns_desired.quantity_requested_admin as sub_returns_desired_quantity_requested_admin from history_orders,products,returns_desired,orders_status,shipping_address,reason_return,sub_returns_desired where history_orders.order_item_id=returns_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and sub_returns_desired.order_item_id=orders_status.order_item_id and sub_returns_desired.admin_action='refund' and returns_desired.order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function returns_refund_request_orders_accounts_incoming_again_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'history_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'returns_desired.order_item_id',
				3 => 'history_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,returns_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,history_orders.customer_id,returns_desired.allow_update_bank_details, if(returns_desired.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') as type_of_transaction,returns_desired.refund_method as returns_desired_refund_method,sub_returns_desired.refund_method as sub_returns_desired_refund_method,sub_returns_desired.timestamp as sub_returns_desired_timestamp,sub_returns_desired.quantity_requested as sub_returns_desired_quantity_requested,sub_returns_desired.quantity_requested_admin as sub_returns_desired_quantity_requested_admin,history_orders.razorpayPaymentId,history_orders.razorpayOrderId from history_orders,products,returns_desired,orders_status,shipping_address,reason_return,sub_returns_desired where history_orders.order_item_id=returns_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and sub_returns_desired.order_item_id=orders_status.order_item_id and sub_returns_desired.admin_action='refund'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( history_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR returns_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				
				$where .=" OR if(returns_desired.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') LIKE '%".$params['search']['value']."%' ";
				
				$where .=" OR history_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}

		public function get_again_refund_details($order_item_id){
			$sql="select * from sub_return_refund where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function return_refund_request_again_need_to_refund_details($order_item_id){
			$sql="select * from sub_returns_desired where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row_array();
		}
		
		public function sub_return_refund_success($order_item_id,$transaction_id,$transaction_date,$transaction_time,$razorpay_refund_id='',$razorpay_refund_status=''){
			
			//$sql="update orders_status set order_return_pickup='0' where order_item_id='$order_item_id'";
			//$query=$this->db->query($sql);
			
			$sql="update sub_returns_desired set admin_action='refunded',status_of_refund='yes',transaction_id='$transaction_id',transaction_date='$transaction_date',transaction_time='$transaction_time',razorpay_refund_id='$razorpay_refund_id',razorpay_refund_status='$razorpay_refund_status' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			/*notify customer*/
			$sql_noti="update refund_notification set refund_success_view=1,refund_success_timestamp=now() where order_item_id='$order_item_id'";
			$query_notification=$this->db->query($sql_noti);
			
			$sql_delete_returned_orders="delete from returned_orders where order_item_id='$order_item_id'";
			$query_delete_returned_orders=$this->db->query($sql_delete_returned_orders);	
			
			$sql_returned_orders="insert into returned_orders select * from completed_orders where order_item_id='$order_item_id'";
			$query_returned_orders=$this->db->query($sql_returned_orders);
			/////////////////////
			$sql_get_refund_data="select * from order_return_decision where order_item_id='$order_item_id'";
			$query_get_refund_data=$this->db->query($sql_get_refund_data);
			$org_product_price=$query_get_refund_data->row()->product_price;
			$org_shipping_price=$query_get_refund_data->row()->shipping_charge;
			$quantity_to_be_refuned_in_first_transaction=$query_get_refund_data->row()->quantity;
			
			////
			$sql_get_refund_data="select * from sub_returns_desired where order_item_id='$order_item_id'";
			$query_get_refund_data=$this->db->query($sql_get_refund_data);
			$quantity_to_be_refuned=$query_get_refund_data->row()->quantity_requested_admin;
			$subtotal_to_be_refuned=($org_product_price)*(($query_get_refund_data->row()->quantity_requested_admin+$quantity_to_be_refuned_in_first_transaction));
			$grandtotal_to_be_refuned=($org_product_price+$org_shipping_price)*(($query_get_refund_data->row()->quantity_requested_admin+$quantity_to_be_refuned_in_first_transaction));
			/////////////////////
			
			$quantity_to_be_refuned_total=$quantity_to_be_refuned_in_first_transaction+$quantity_to_be_refuned;
			$sql_update_returned_orders="update returned_orders set subtotal='$subtotal_to_be_refuned',grandtotal='$grandtotal_to_be_refuned',quantity='$quantity_to_be_refuned_total' where order_item_id='$order_item_id'";
			$query_update_returned_orders=$this->db->query($sql_update_returned_orders);
			
			//////////////////////////////////////////
			
			///////////////////////
			$sql_get_quantity_completed_orders="select quantity from completed_orders where order_item_id='$order_item_id'";
			$query_get_quantity_completed_orders=$this->db->query($sql_get_quantity_completed_orders);
			$quantity_purchased=$query_get_quantity_completed_orders->row()->quantity;
			
			////////////////////////
			// first transaction
			$sql_return_refund_total_refund_with_concession="select item_with_deductions_concession,shipping_with_deductions_concession from return_refund where order_item_id='$order_item_id'";
			$query_return_refund_total_refund_with_concession=$this->db->query($sql_return_refund_total_refund_with_concession);
			$item_with_deductions_concession=$query_return_refund_total_refund_with_concession->row()->item_with_deductions_concession;
			$shipping_with_deductions_concession=$query_return_refund_total_refund_with_concession->row()->shipping_with_deductions_concession;
			
			// second transaction
			$sql_sub_return_refund_total_refund_with_concession="select item_with_deductions_concession,shipping_with_deductions_concession from sub_return_refund where order_item_id='$order_item_id'";
			$query_sub_return_refund_total_refund_with_concession=$this->db->query($sql_sub_return_refund_total_refund_with_concession);
			$item_with_deductions_concession_sub=$query_sub_return_refund_total_refund_with_concession->row()->item_with_deductions_concession;
			$shipping_with_deductions_concession_sub=$query_sub_return_refund_total_refund_with_concession->row()->shipping_with_deductions_concession;
			
			
			$total_refund_with_concession_withoutotheranyconcessions=($item_with_deductions_concession+$item_with_deductions_concession_sub)+($shipping_with_deductions_concession+$shipping_with_deductions_concession_sub);
			///////////////////////////////////////////////
			
			if((($quantity_to_be_refuned_in_first_transaction+$quantity_to_be_refuned)==$quantity_purchased) && ($grandtotal_to_be_refuned==$total_refund_with_concession_withoutotheranyconcessions)){
				$sql_completed_orders="delete from completed_orders where order_item_id='$order_item_id'";
				$query_completed_orders=$this->db->query($sql_completed_orders);
			}
			//////
			$sql_order_replacement_decision="select * from order_replacement_decision where order_item_id='$order_item_id'";
			$query_order_replacement_decision=$this->db->query($sql_order_replacement_decision);
			$query_order_replacement_decision_arr=$query_order_replacement_decision->row_array();
			if(count($query_order_replacement_decision_arr)>0){
				if($query_order_replacement_decision_arr["status"]=="refunded"){
					if(($quantity_to_be_refuned_in_first_transaction+$quantity_to_be_refuned+$query_order_replacement_decision_arr["quantity"])==$quantity_purchased){
						$sql_completed_orders="delete from completed_orders where order_item_id='$order_item_id'";
						$query_completed_orders=$this->db->query($sql_completed_orders);
					}
				}
			}
			//////////////////
			//////
			/*$returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin_obj=$this->returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin($order_item_id);
			if(count($returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin_obj)>0){
				if(($quantity_to_be_refuned_in_first_transaction+$quantity_to_be_refuned+$returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin_obj->quantity)==$quantity_purchased){
						$sql_completed_orders="delete from completed_orders where order_item_id='$order_item_id'";
						$query_completed_orders=$this->db->query($sql_completed_orders);
					}
			}*/
			/////////////////////
			return $query;
		}
		
		public function transfer_to_wallet_for_sub_returns($order_item_id){
		
			//$sql="update refund set refund_type='Wallet' where order_item_id='$order_item_id'";
			//$query=$this->db->query($sql);
			
			$sql="update sub_returns_desired set refund_method='Wallet' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			$sql="update sub_return_refund set refund_method='Wallet' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			return $query;
		}
		
		public function sub_returns_desired_table_data_refunded_by_order_item_id($order_item_id){
			$sql="select * from sub_returns_desired where order_item_id='$order_item_id' and admin_action='refunded'";
			$query=$this->db->query($sql);
			return $query->row_array();
		}
		
		public function get_subcat_info_by_product_id($product_id){
			$sql="select * from subcategory where subcat_id = (select subcat_id from products where product_id='$product_id')";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function get_shipping_add_info_by_shipping_address_id($shipping_address_id){
			$sql="select * from shipping_address where shipping_address_id='$shipping_address_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		
		public function get_data_from_history_orders($order_item_id){
			$sql="select * from history_orders where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		
		public function get_customer_info_by_customer_id($customer_id){
			$sql="select * from customer where id='$customer_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		
		
		public function returns_replacement_request_orders($order_item_id){ 
		
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.paid_by_original,replacement_desired.amount_paid,replacement_desired.amount_paid_original,replacement_desired.return_value_each_inventory_id,replacement_desired.amount_paid_item_price,replacement_desired.amount_paid_item_price_original,inventory.moq,replacement_desired.shipping_charge_requested,replacement_desired.shipping_charge_percent_paid_by_customer from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return,inventory where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and replacement_desired.order_replacement_decision_status='pending' and replacement_desired.order_replacement_decision_customer_decision='pending' and replacement_desired.order_replacement_decision_customer_status!='cancel' and replacement_desired.order_item_id='$order_item_id' and history_orders.inventory_id=inventory.id";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function returns_replacement_request_orders_processing($params,$fg){
			//define index of column
			
			$columns = array(
				0 =>'history_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'replacement_desired.order_item_id',
				3 => 'history_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select history_orders.*, history_orders.sku_id,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and replacement_desired.order_replacement_decision_status='pending' and replacement_desired.order_replacement_decision_customer_decision='pending' and replacement_desired.order_replacement_decision_customer_status!='cancel'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( history_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		public function get_replacement_request_info($orders_status_id){
			
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and orders_status.id='$orders_status_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function get_unread_return_msg_replacement($return_order_id){
			$sql="select count(*) as count from  replacement_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='pending' and customer_action='pending'";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		
		public function update_msg_status_replacement($return_order_id){
			$sql="update replacement_replies set is_read=1 where user_type='customer' and return_order_id='$return_order_id'";
			$query=$this->db->query($sql);
			return $query;
		}
		
		public function get_returns_conservation_chain_in_adminpanel_replacement($return_order_id){
			$sql="select * from replacement_replies where return_order_id='$return_order_id'";
			$query=$this->db->query($sql);
			return $query->result();
		}
		
		public function getspecified_returns_file_replacement($id){
		$query=$this->db->query("SELECT *  FROM replacement_replies where id='$id'");
			if(count($query->result())==1){
				return $query->result();
			}
		}
		
		public function contact_buyer_replacement($return_order_id,$user_type,$status,$description,$image_path,$customer_id,$admin_action,$customer_action){
			$description=$this->db->escape_str($description);
			$sql="insert into replacement_replies set return_order_id='$return_order_id',user_type='$user_type',status='$status',description='$description',image='$image_path',is_read='1',customer_id='$customer_id',admin_action='$admin_action',customer_action='$customer_action'";
			$query=$this->db->query($sql);
			return $query;
		}
		
		public function get_availability_of_order_item_id_in_refunds_replacements($order_item_id){
			$sql="select * from replacement_desired where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->num_rows();
		}
		
		public function get_order_replacement_primary_reasons_admin(){
			$sql="select * from admin_replacement_reason";
			$result=$this->db->query($sql);
			return $result->result_array();
		}
		
		public function return_replacement_accept($return_order_id,$customer_id,$inventory_id,$replacement_with_inventory_id,$order_item_id,$quantity_accept,$balance_amount,$type_of_refund,$comments,$disable_comm,$allow_cust_to_make_decision,$return_shipping_concession_chk,$shipping_charge_purchased,$shipping_charge_replacement,$balance_amount_item_price,$paid_by,$product_price,$update_inventory_stock){
			if($disable_comm){
				$comm_flag=1;
			}
			else{
				$comm_flag=0;
			}
			if($allow_cust_to_make_decision){
				$allow_cust_to_make_decision_flag=1;
				$customer_decision="pending";
			}
			else{
				$allow_cust_to_make_decision_flag=0;
				$customer_decision="accept";
			}
			if($return_shipping_concession_chk){
				$return_shipping_concession_chk="yes";
			}
			else{
				$return_shipping_concession_chk="no";
			}
			$comments=$this->db->escape_str($comments);
			$sql="insert into order_replacement_decision set return_order_id='$return_order_id',customer_id='$customer_id',quantity='$quantity_accept',balance_amount='$balance_amount',inventory_id='$inventory_id',replacement_with_inventory_id='$replacement_with_inventory_id',product_price='$product_price',order_item_id='$order_item_id',sub_reason_return_id='$type_of_refund',status='accept',comments='$comments',disable_comm_flag='$comm_flag',allow_cust_to_make_decision_flag='$allow_cust_to_make_decision_flag',customer_decision='$customer_decision',customer_status='pending',return_shipping_concession_chk='$return_shipping_concession_chk',shipping_charge_purchased='$shipping_charge_purchased',shipping_charge_replacement='$shipping_charge_replacement',balance_amount_item_price='$balance_amount_item_price',paid_by='$paid_by',update_inventory_stock='$update_inventory_stock'";
			$query=$this->db->query($sql);
			
			$sql_select="select * from replacement_desired where order_item_id='$order_item_id'";
			$query_select=$this->db->query($sql_select);
			$replacement_desired=$query_select->row();
			
			if(!empty($replacement_desired)){
				$refund_method_ref=$replacement_desired->refund_method;
				$paid_by_original_ref=$replacement_desired->paid_by_original;
				$paid_by_exist=$replacement_desired->paid_by;
				$payment_status_ref=$replacement_desired->payment_status;
				$amount_paid_original_ref=$replacement_desired->amount_paid_original;
			}

			//////////////////////////////
			
			if($amount_paid_original_ref==0){
				
				if($refund_method_ref=="" && $paid_by=="admin" && $paid_by_original_ref==""){
					
					$sql_update_returns_desired="update replacement_desired set order_replacement_decision_status='accept',order_replacement_decision_customer_decision='$customer_decision',amount_paid='$balance_amount',amount_paid_item_price='$balance_amount_item_price',paid_by='$paid_by',refund_method='Wallet' where order_item_id='$order_item_id'";
				
				}elseif($paid_by=="customer" && $paid_by_original_ref==""){
					$sql_update_returns_desired="update replacement_desired set order_replacement_decision_status='accept',order_replacement_decision_customer_decision='$customer_decision',amount_paid='$balance_amount',amount_paid_item_price='$balance_amount_item_price',paid_by='$paid_by',refund_method='',payment_status='pending' where order_item_id='$order_item_id'";
				}elseif($paid_by=="" && $paid_by_original_ref==""){
					$sql_update_returns_desired="update replacement_desired set order_replacement_decision_status='accept',order_replacement_decision_customer_decision='$customer_decision',amount_paid='$balance_amount',amount_paid_item_price='$balance_amount_item_price',paid_by='$paid_by',refund_method='',payment_status='pending' where order_item_id='$order_item_id'";
				}else{
					$sql_update_returns_desired="update replacement_desired set order_replacement_decision_status='accept',order_replacement_decision_customer_decision='$customer_decision',amount_paid='$balance_amount',amount_paid_item_price='$balance_amount_item_price',paid_by='$paid_by' where order_item_id='$order_item_id'";
				}
			}else{
				$sql_update_returns_desired="update replacement_desired set order_replacement_decision_status='accept',order_replacement_decision_customer_decision='$customer_decision',amount_paid='$balance_amount',amount_paid_item_price='$balance_amount_item_price',paid_by='$paid_by' where order_item_id='$order_item_id'";
			}
			$query_update_returns_desired=$this->db->query($sql_update_returns_desired);
			
			
			return $query;
		}
		
		public function return_replacement_reject($return_order_id,$customer_id,$inventory_id,$replacement_with_inventory_id,$order_item_id,$quantity_accept,$balance_amount,$type_of_refund,$comments,$disable_comm,$shipping_charge_purchased,$shipping_charge_replacement,$balance_amount_item_price,$paid_by,$product_price,$update_inventory_stock){
			
			if($disable_comm){
				$comm_flag=1;
			}else{
				$comm_flag=0;
			}
			$comments=$this->db->escape_str($comments);
			$sql="select * from order_replacement_decision where order_item_id='$order_item_id'";
			$res=$this->db->query($sql);
			if($res->num_rows()==0){
				
				$sql="insert into order_replacement_decision set return_order_id='$return_order_id',customer_id='$customer_id',quantity='$quantity_accept',balance_amount='$balance_amount',inventory_id='$inventory_id',replacement_with_inventory_id='$replacement_with_inventory_id',product_price='$product_price',order_item_id='$order_item_id',sub_reason_return_id='$type_of_refund',status='reject',comments='$comments',disable_comm_flag='$comm_flag',customer_status='pending',shipping_charge_purchased='$shipping_charge_purchased',shipping_charge_replacement='$shipping_charge_replacement',balance_amount_item_price='$balance_amount_item_price',paid_by='$paid_by',update_inventory_stock='$update_inventory_stock'";
				$query=$this->db->query($sql);
			}
			else{
				$sql="update order_replacement_decision set return_order_id='$return_order_id',customer_id='$customer_id',quantity='$quantity_accept',balance_amount='$balance_amount',inventory_id='$inventory_id',replacement_with_inventory_id='$replacement_with_inventory_id',order_item_id='$order_item_id',sub_reason_return_id='$type_of_refund',status='reject',comments='$comments',disable_comm_flag='$comm_flag',customer_status='pending',shipping_charge_purchased='$shipping_charge_purchased',shipping_charge_replacement='$shipping_charge_replacement',balance_amount_item_price='$balance_amount_item_price',paid_by='$paid_by',update_inventory_stock='$update_inventory_stock' where order_item_id='$order_item_id'";
				$query=$this->db->query($sql);
			}
			
			$sql_update_replacement_desired="update replacement_desired set order_replacement_decision_status='reject',order_replacement_decision_customer_status='pending' where order_item_id='$order_item_id'";
			$query_update_replacement_desired=$this->db->query($sql_update_replacement_desired);
			return $query;
		}
		
		public function returns_replacement_request_orders_awaiting($order_item_id){ 
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.amount_paid_item_price,replacement_desired.amount_paid_item_price_original,replacement_desired.paid_by_original,replacement_desired.amount_paid_original,inventory.moq,replacement_desired.shipping_charge_percent_paid_by_customer from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return,inventory where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and replacement_desired.order_replacement_decision_status='accept' and  replacement_desired.order_replacement_decision_customer_decision='pending' and replacement_desired.order_replacement_decision_customer_status!='cancel' and replacement_desired.order_item_id='$order_item_id' and history_orders.inventory_id=inventory.id";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function returns_replacement_request_orders_awaiting_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'history_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'replacement_desired.order_item_id',
				3 => 'history_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and replacement_desired.order_replacement_decision_status='accept' and replacement_desired.order_replacement_decision_customer_decision='pending' and replacement_desired.order_replacement_decision_customer_status!='cancel'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( history_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		public function get_unread_return_msg_awaiting_replacement($return_order_id){
			$sql="select count(*) as count from  replacement_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='accept' and customer_action='pending'";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		
		public function get_order_replacement_decision_data($order_item_id){
			$sql="select order_replacement_decision.*,admin_replacement_reason.pickup_identifier from order_replacement_decision,admin_replacement_reason where  order_replacement_decision.sub_reason_return_id=admin_replacement_reason.id and order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function update_return_replacement_accept($return_order_id,$customer_id,$inventory_id,$replacement_with_inventory_id,$order_item_id,$quantity_accept,$balance_amount,$type_of_refund,$comments,$disable_comm,$allow_cust_to_make_decision,$return_shipping_concession,$shipping_charge_purchased,$shipping_charge_replacement,$balance_amount_item_price,$paid_by,$return_shipping_concession_chk,$customer_accepted,$update_inventory_stock){
			
			if($disable_comm){
				$comm_flag=1;
			}else{
				$comm_flag=0;
			}
			if($allow_cust_to_make_decision){
				$allow_cust_to_make_decision_flag=1;
				$customer_decision="pending";
			}else{
				$allow_cust_to_make_decision_flag=0;
				$customer_decision="accept";
			}
			if($customer_accepted=="yes"){
				if($return_shipping_concession_chk=="no"){
					$return_shipping_concession=0;
					$str=",return_shipping_concession_chk='$return_shipping_concession_chk'";
				}
				if($return_shipping_concession_chk=="yes"){
					$str=",return_shipping_concession_chk='$return_shipping_concession_chk'";
				}
			}else{
				$str='';
			}
			$comments=$this->db->escape_str($comments);
			$sql="update order_replacement_decision set return_order_id='$return_order_id',customer_id='$customer_id',quantity='$quantity_accept',balance_amount='$balance_amount',inventory_id='$inventory_id',replacement_with_inventory_id='$replacement_with_inventory_id',order_item_id='$order_item_id',sub_reason_return_id='$type_of_refund',status='accept',comments='$comments',disable_comm_flag='$comm_flag',allow_cust_to_make_decision_flag='$allow_cust_to_make_decision_flag',customer_decision='$customer_decision',customer_status='pending',return_shipping_concession='$return_shipping_concession',shipping_charge_purchased='$shipping_charge_purchased',shipping_charge_replacement='$shipping_charge_replacement',balance_amount_item_price='$balance_amount_item_price',paid_by='$paid_by',update_inventory_stock='$update_inventory_stock' $str where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			$sql_select="select * from replacement_desired where order_item_id='$order_item_id'";
			$query_select=$this->db->query($sql_select);
			$replacement_desired=$query_select->row();
			
			if(!empty($replacement_desired)){
				$refund_method_ref=$replacement_desired->refund_method;
				$paid_by_original_ref=$replacement_desired->paid_by_original;
				$paid_by_exist=$replacement_desired->paid_by;
				$payment_status_ref=$replacement_desired->payment_status;
				$amount_paid_original_ref=$replacement_desired->amount_paid_original;
			}

			//////////////////////////////
			
			if($amount_paid_original_ref==0){
				
				if($refund_method_ref=="" && $paid_by=="admin" && $paid_by_original_ref==""){
					
					$sql_update_returns_desired="update replacement_desired set order_replacement_decision_status='accept',order_replacement_decision_customer_decision='$customer_decision',amount_paid='$balance_amount',amount_paid_item_price='$balance_amount_item_price',paid_by='$paid_by',refund_method='Wallet' where order_item_id='$order_item_id'";
				
				}elseif($paid_by=="customer" && $paid_by_original_ref==""){
					$sql_update_returns_desired="update replacement_desired set order_replacement_decision_status='accept',order_replacement_decision_customer_decision='$customer_decision',amount_paid='$balance_amount',amount_paid_item_price='$balance_amount_item_price',paid_by='$paid_by',refund_method='',payment_status='pending' where order_item_id='$order_item_id'";
				}elseif($paid_by=="" && $paid_by_original_ref==""){
					$sql_update_returns_desired="update replacement_desired set order_replacement_decision_status='accept',order_replacement_decision_customer_decision='$customer_decision',amount_paid='$balance_amount',amount_paid_item_price='$balance_amount_item_price',paid_by='$paid_by',refund_method='',payment_status='pending' where order_item_id='$order_item_id'";
				}else{
					$sql_update_returns_desired="update replacement_desired set order_replacement_decision_status='accept',order_replacement_decision_customer_decision='$customer_decision',amount_paid='$balance_amount',amount_paid_item_price='$balance_amount_item_price',paid_by='$paid_by' where order_item_id='$order_item_id'";
				}
			}else{
				$sql_update_returns_desired="update replacement_desired set order_replacement_decision_status='accept',order_replacement_decision_customer_decision='$customer_decision',amount_paid='$balance_amount',amount_paid_item_price='$balance_amount_item_price',paid_by='$paid_by' where order_item_id='$order_item_id'";
			}
			$query_update_returns_desired=$this->db->query($sql_update_returns_desired);

			return $query;
			
		}
	
		public function returns_replacement_request_orders_customer_accepted($order_item_id){ 
		
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.amount_paid_item_price,replacement_desired.payment_status,replacement_desired.amount_paid_item_price_original,replacement_desired.paid_by_original,replacement_desired.amount_paid_original,inventory.moq,replacement_desired.shipping_charge_percent_paid_by_customer from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return,inventory where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and replacement_desired.order_replacement_decision_status='accept' and replacement_desired.order_replacement_decision_customer_decision='accept' and orders_status.order_replacement_pickup='0' and replacement_desired.order_replacement_decision_customer_status!='cancel' and replacement_desired.order_item_id='$order_item_id' and history_orders.inventory_id=inventory.id";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function returns_replacement_request_orders_customer_accepted_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'history_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'replacement_desired.order_item_id',
				3 => 'history_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql ="select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,history_orders.*,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.prev_order_item_id,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.payment_status,replacement_desired.payment_status,replacement_desired.original_inventory_id from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and replacement_desired.order_replacement_decision_status='accept' and replacement_desired.order_replacement_decision_customer_decision='accept' and orders_status.order_replacement_pickup='0' and (orders_status.initiate_replacement_pickup!='0' and  orders_status.initiate_replacement_pickup!='1') and replacement_desired.order_replacement_decision_customer_status!='cancel'";
			
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( history_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		public function send_replacement_lable($vendor_id,$order_item_id){
			$sql="update order_replacement_decision set vendor_id='$vendor_id' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query;
		}
		
		public function get_unread_replacement_msg_customer_accepted($return_order_id){
			$sql="select count(*) as count from replacement_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='accept' and customer_action='accept'";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		
		public function returns_replacement_request_orders_customer_rejected($order_item_id){ 
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.amount_paid_item_price,replacement_desired.amount_paid_item_price_original,replacement_desired.paid_by_original,replacement_desired.amount_paid_original,inventory.moq,replacement_desired.shipping_charge_percent_paid_by_customer from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return,inventory where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and replacement_desired.order_replacement_decision_status='accept' and replacement_desired.
			order_replacement_decision_customer_decision='reject' and replacement_desired.order_item_id='$order_item_id' and history_orders.inventory_id =inventory.id";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function returns_replacement_request_orders_customer_rejected_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'history_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'replacement_desired.order_item_id',
				3 => 'history_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and replacement_desired.order_replacement_decision_status='accept' and replacement_desired.order_replacement_decision_customer_decision='reject'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( history_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		public function get_unread_return_msg_customer_rejected_replacement($return_order_id){
			$sql="select count(*) as count from  replacement_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='accept' and customer_action='reject'";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		
		public function returns_replacement_request_orders_admin_rejected($order_item_id){ 
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.amount_paid_item_price,replacement_desired.amount_paid_item_price_original,replacement_desired.paid_by_original,replacement_desired.amount_paid_original,inventory.moq,replacement_desired.shipping_charge_percent_paid_by_customer from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return,inventory where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and replacement_desired.order_replacement_decision_status='reject' and replacement_desired.order_item_id='$order_item_id' and history_orders.inventory_id=inventory.id";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function returns_replacement_request_orders_admin_rejected_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'completed_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'replacement_desired.order_item_id',
				3 => 'completed_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,replacement_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,completed_orders.customer_id,replacement_desired.order_replacement_decision_status,replacement_desired.order_replacement_decision_customer_decision,replacement_desired.order_replacement_decision_customer_status,replacement_desired.order_replacement_decision_customer_status_comments,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id from completed_orders,products,replacement_desired,orders_status,shipping_address,reason_return where completed_orders.order_item_id=replacement_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and replacement_desired.order_replacement_decision_status='reject'";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( completed_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR completed_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		public function get_unread_return_msg_admin_rejected_replacement($return_order_id){
			$sql="select count(*) as count from  replacement_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='reject'";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		
		
		
		public function returns_replacement_request_orders_ready_to_replace_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'history_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'replacement_desired.order_item_id',
				3 => 'history_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";
			
			if($params["status_of_replaced_order"]=="ready_to_replace"){
			// getting total number records without any search
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.payment_status,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.order_replacement_decision_status,replacement_desired.type_of_replacement_cancel from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and (replacement_desired.order_replacement_decision_status='accept' and ((orders_status.order_replacement_pickup='1' and orders_status.initiate_replacement_pickup!='0' and orders_status.initiate_replacement_pickup!='1') or (orders_status.order_replacement_pickup='0' and orders_status.initiate_replacement_pickup='1') or (orders_status.order_replacement_pickup='0' and orders_status.initiate_replacement_pickup='0')) and replacement_desired.status_of_refund!='yes' and replacement_desired.order_replacement_decision_customer_status!='cancel') ";
			
			}else{
				$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.payment_status,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.order_replacement_decision_status,replacement_desired.type_of_replacement_cancel from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and (replacement_desired.order_replacement_decision_status='replace' and replacement_desired.order_replacement_decision_customer_status!='cancel') ";
			}
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( history_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		public function send_order_for_replacement($type_of_order,$order_item_id){
			// dumping as it is from history to active orders table
			
			$sql_update_replacement_desired="update replacement_desired set order_replacement_decision_status='replace' where order_item_id='$order_item_id'";
			$this->db->query($sql_update_replacement_desired);

			$sql_update_order_replacement_decision="update order_replacement_decision set status='replace' where order_item_id='$order_item_id'";
			$flag=$this->db->query($sql_update_order_replacement_decision);
			

			$sql_order_deci="select * from order_replacement_decision where order_item_id='$order_item_id'";
			$query_order_decision=$this->db->query($sql_order_deci);
			
			$shipping_charge=$query_order_decision->row()->shipping_charge_replacement;
			$accepted_qty=$query_order_decision->row()->quantity;
			//$product_price=$query_order_decision->row()->product_price;
			
			$sql_order_repl_desired="select * from replacement_desired where order_item_id='$order_item_id'";
			$query_repl_desir=$this->db->query($sql_order_repl_desired);

			$replacement_with_inventory_id=$query_repl_desir->row()->replacement_with_inventory_id;
			$replacement_value_each_inventory_id=$query_repl_desir->row()->replacement_value_each_inventory_id;
			
			$inventory_info_by_inventory_id_obj=$this->get_inventory_info_by_inventory_id($replacement_with_inventory_id);
			
			$sku_id=$inventory_info_by_inventory_id_obj->sku_id;

			////
			/* getting the shipping charge from previous order item id starts*/
			/*$sql_get_shipping_charge="select shipping_charge from history_orders where order_item_id='$order_item_id'";
			$query_get_shipping_charge=$this->db->query($sql_get_shipping_charge);
			$shipping_charge=$query_get_shipping_charge->row()->shipping_charge;*/
			/* getting the shipping charge from previous order item id ends*/
			////
			
			$quantity_need_to_be_replace=$accepted_qty;
			$product_price=$replacement_value_each_inventory_id;
			$subtotal=$quantity_need_to_be_replace*$replacement_value_each_inventory_id;
			$grandtotal=($quantity_need_to_be_replace*$replacement_value_each_inventory_id)+$shipping_charge;
			$image=$inventory_info_by_inventory_id_obj->thumbnail;

			//////////////////////
			$sql="select * from history_orders where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			$purchased_price=$query->row()->purchased_price;
			$customer_id=$query->row()->customer_id;
			$order_id=$query->row()->order_id;
			$shipping_address_id=$query->row()->shipping_address_id;
			$inventory_id=$replacement_with_inventory_id;
			$product_id=$query->row()->product_id;
			$sku_id=$sku_id;
			$tax_percent=$query->row()->tax_percent;
			$product_price=$product_price;
			$shipping_charge=$shipping_charge;
			$return_period=$query->row()->return_period;
			$subtotal=$subtotal;
			$wallet_status=$query->row()->wallet_status;
			$wallet_amount=$query->row()->wallet_amount;
			$grandtotal=$grandtotal;
			$quantity=$quantity_need_to_be_replace;
			$timestamp=$query->row()->timestamp;
			$image=$image;
			$random_number=$query->row()->random_number;
			$invoice_email=0;
			$invoice_number=$query->row()->invoice_number;
			$logistics_name=$query->row()->logistics_name;
			$logistics_weblink=$query->row()->logistics_weblink;
			$tracking_number=$query->row()->tracking_number;
			$expected_delivery_date=$query->row()->expected_delivery_date;
			$delivery_mode=$query->row()->delivery_mode;
			$parcel_category=$query->row()->parcel_category;

			$vendor_id=$query->row()->vendor_id;
			$payment_type=$query->row()->payment_type;
			$payment_status=$query->row()->payment_status;
			$return_period=$query->row()->return_period;
			
			$logistics_id=$query->row()->logistics_id;
			$logistics_delivery_mode_id=$query->row()->logistics_delivery_mode_id;
			$logistics_territory_id=$query->row()->logistics_territory_id;
			$logistics_parcel_category_id=$query->row()->logistics_parcel_category_id;

			$promotion_available=$query->row()->promotion_available;
			$promotion_residual=$query->row()->promotion_residual;
			$promotion_discount=$query->row()->promotion_discount;
			$promotion_cashback=$query->row()->promotion_cashback;
			$promotion_surprise_gift=$query->row()->promotion_surprise_gift;
			$promotion_surprise_gift_type=$query->row()->promotion_surprise_gift_type;
			$promotion_surprise_gift_skus=$query->row()->promotion_surprise_gift_skus;
			$promotion_surprise_gift_skus_nums=$query->row()->promotion_surprise_gift_skus_nums;
			$default_discount=$query->row()->default_discount;
			$promotion_item_multiplier=$query->row()->promotion_item_multiplier;
			$promotion_item=$query->row()->promotion_item;
			$promotion_item_num=$query->row()->promotion_item_num;
			$promotion_clubed_with_default_discount=$query->row()->promotion_clubed_with_default_discount;
			$promotion_id_selected=$query->row()->promotion_id_selected;
			$promotion_type_selected=$query->row()->promotion_type_selected;
			$promotion_minimum_quantity=$query->row()->promotion_minimum_quantity;
			$promotion_quote=$query->row()->promotion_quote;
			$promotion_default_discount_promo=$query->row()->promotion_default_discount_promo;
			$individual_price_of_product_with_promotion=$query->row()->individual_price_of_product_with_promotion;
			$individual_price_of_product_without_promotion=$query->row()->individual_price_of_product_without_promotion;
			$total_price_of_product_without_promotion=$query->row()->total_price_of_product_without_promotion;
			$total_price_of_product_with_promotion=$query->row()->total_price_of_product_with_promotion;
			$quantity_without_promotion=$query->row()->quantity_without_promotion;
			$quantity_with_promotion=$query->row()->quantity_with_promotion;
			$cash_back_value=$query->row()->cash_back_value;
			
			
			$product_vendor_id=$query->row()->product_vendor_id;
			

			///////////////////
			
			
		$sql="insert into active_orders set 
		                    purchased_price='$purchased_price',
							customer_id='$customer_id',
							order_id='$order_id',
							shipping_address_id='$shipping_address_id',
							inventory_id='$inventory_id',
							product_id='$product_id',
							sku_id='$sku_id',
							tax_percent='$tax_percent',
							product_price='$product_price',
							shipping_charge='$shipping_charge',
							return_period='$return_period',
							subtotal='$subtotal',
							wallet_status='$wallet_status',
							wallet_amount='$wallet_amount',
							grandtotal='$grandtotal',
							quantity='$quantity',
							image='$image',
							invoice_email='$invoice_email',
							invoice_number='$invoice_number',
							logistics_name='$logistics_name',
							logistics_weblink='$logistics_weblink',
							tracking_number='$tracking_number',
							expected_delivery_date='$expected_delivery_date',
							delivery_mode='$delivery_mode',
							parcel_category='$parcel_category',
							vendor_id='$vendor_id',
							payment_type='$payment_type',
							payment_status='$payment_status',
							prev_order_item_id='$order_item_id',	
							logistics_id='$logistics_id',
							logistics_delivery_mode_id='$logistics_delivery_mode_id',
							logistics_territory_id='$logistics_territory_id',
							logistics_parcel_category_id='$logistics_parcel_category_id',
							promotion_available='$promotion_available',
							promotion_residual='$promotion_residual',
							promotion_discount='$promotion_discount',
							promotion_cashback='$promotion_cashback',
							promotion_surprise_gift='$promotion_surprise_gift',
							promotion_surprise_gift_type='$promotion_surprise_gift_type',
							promotion_surprise_gift_skus='$promotion_surprise_gift_skus',
							promotion_surprise_gift_skus_nums='$promotion_surprise_gift_skus_nums',
							default_discount='$default_discount',
							promotion_item_multiplier='$promotion_item_multiplier',
							promotion_item='$promotion_item',
							promotion_item_num='$promotion_item_num',
							promotion_clubed_with_default_discount='$promotion_clubed_with_default_discount',	
							promotion_id_selected='$promotion_id_selected',
							promotion_type_selected='$promotion_type_selected',
							promotion_minimum_quantity='$promotion_minimum_quantity',
							promotion_quote='$promotion_quote',
							promotion_default_discount_promo='$promotion_default_discount_promo',
							individual_price_of_product_with_promotion='$individual_price_of_product_with_promotion',
							individual_price_of_product_without_promotion='$individual_price_of_product_without_promotion',
							total_price_of_product_without_promotion='$total_price_of_product_without_promotion',
							total_price_of_product_with_promotion='$total_price_of_product_with_promotion',
							quantity_without_promotion='$quantity_without_promotion',
							quantity_with_promotion='$quantity_with_promotion',
							cash_back_value='$cash_back_value',
							
							product_vendor_id='$product_vendor_id'
							";
			$query=$this->db->query($sql);
			$new_order_item_id=$this->db->insert_id();
			
			 $sql_history="insert into history_orders (purchased_price,order_item_id,prev_order_item_id,customer_id,order_id,shipping_address_id,inventory_id,product_id,sku_id,tax_percent,product_price,shipping_charge,subtotal,wallet_status,wallet_amount,grandtotal,quantity,timestamp,image,random_number,invoice_email,invoice_number,logistics_name,logistics_weblink,tracking_number,expected_delivery_date,delivery_mode,parcel_category,vendor_id,payment_type,payment_status,return_period,logistics_id,logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id,promotion_available,promotion_residual,promotion_discount,promotion_cashback,promotion_surprise_gift,promotion_surprise_gift_type,promotion_surprise_gift_skus,promotion_surprise_gift_skus_nums,default_discount,promotion_item_multiplier,promotion_item,promotion_item_num,promotion_clubed_with_default_discount,promotion_id_selected,promotion_type_selected,promotion_minimum_quantity,promotion_quote,promotion_default_discount_promo,individual_price_of_product_with_promotion,individual_price_of_product_without_promotion,total_price_of_product_without_promotion,total_price_of_product_with_promotion,quantity_without_promotion,quantity_with_promotion,cash_back_value,product_vendor_id) values('$purchased_price','$new_order_item_id','$order_item_id','$customer_id','$order_id','$shipping_address_id','$inventory_id','$product_id','$sku_id','$tax_percent','$product_price','$shipping_charge','$subtotal','$wallet_status','$wallet_amount','$grandtotal','$quantity','$timestamp','$image','$random_number','$invoice_email','$invoice_number','$logistics_name','$logistics_weblink','$tracking_number','$expected_delivery_date','$delivery_mode','$parcel_category','$vendor_id','$payment_type','$payment_status','$return_period','$logistics_id','$logistics_delivery_mode_id','$logistics_territory_id','$logistics_parcel_category_id','$promotion_available','$promotion_residual','$promotion_discount','$promotion_cashback','$promotion_surprise_gift','$promotion_surprise_gift_type','$promotion_surprise_gift_skus','$promotion_surprise_gift_skus_nums','$default_discount','$promotion_item_multiplier','$promotion_item','$promotion_item_num','$promotion_clubed_with_default_discount','$promotion_id_selected','$promotion_type_selected','$promotion_minimum_quantity','$promotion_quote','$promotion_default_discount_promo','$individual_price_of_product_with_promotion','$individual_price_of_product_without_promotion','$total_price_of_product_without_promotion','$total_price_of_product_with_promotion','$quantity_without_promotion','$quantity_with_promotion','$cash_back_value','$product_vendor_id')";
			
			$result_history=$this->db->query($sql_history);
			
			//////////////////////////////////////////////////
			/// refunded starts ////
			//$sql_update_orders_status="update orders_status set order_return_pickup='0' where order_item_id='$order_item_id'";
			//$this->db->query($sql_update_orders_status);
			
			
			/// refunded starts ////
			//////////////////////////////////////////////////
			$sql_return_order_id="select return_order_id from orders_status where order_item_id='$order_item_id'";
			$query_return_order_id=$this->db->query($sql_return_order_id);
			$return_order_id=$query_return_order_id->row()->return_order_id;
			if($type_of_order=="onhold"){
				$sql_orders_status="insert into orders_status set order_item_id='$new_order_item_id',customer_id='$customer_id',return_order_id='$return_order_id',type_of_order='replaced',prev_order_item_id='$order_item_id'";
				$this->db->query($sql_orders_status);
			}
			if($type_of_order=="approved"){
				$sql_orders_status="insert into orders_status set order_placed=1,order_placed_timestamp=now(),order_item_id='$new_order_item_id',customer_id='$customer_id',return_order_id='$return_order_id',type_of_order='replaced',prev_order_item_id='$order_item_id'";
				$this->db->query($sql_orders_status);
			}
			if($type_of_order=="confirmed"){
				$sql_orders_status="insert into orders_status set order_placed=1,order_placed_timestamp=now(),order_confirmed=1,order_confirmed_timestamp=now(),order_item_id='$new_order_item_id',customer_id='$customer_id',return_order_id='$return_order_id',type_of_order='replaced',prev_order_item_id='$order_item_id'";
				$this->db->query($sql_orders_status);
			}
			return $query;
		}
		
		
		public function returns_replacement_request_orders_refund_success_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'replaced_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'replacement_desired.order_item_id',
				3 => 'replaced_orders.order_id'
			);
			
			$where = $sqlTot = $sqlRec = "";

			$status_of_refund=$params["status_of_refund"];
			if($status_of_refund=="all"){
				
				$str='';
			}
			if($status_of_refund=="success"){
				$str="and ((orders_status.order_replacement_pickup='1' and trim(orders_status.initiate_replacement_pickup)=trim('')) or (orders_status.order_replacement_pickup='1' and orders_status.initiate_replacement_pickup='1' ) or (orders_status.order_replacement_pickup='0' and orders_status.initiate_replacement_pickup='0' ) )";
			}
			if($status_of_refund=="pending"){
				$str="and ((orders_status.order_replacement_pickup='0' and orders_status.initiate_replacement_pickup='1' ) )";
			}
			
			// getting total number records without any search
			$sql = "select orders_status.order_replacement_pickup,replaced_orders.*,replaced_orders.timestamp,replaced_orders.grandtotal as grandtotal_of_quantity_purchased,replaced_orders.sku_id,replaced_orders.inventory_id,products.product_name,replaced_orders.order_item_id,replaced_orders.prev_order_item_id,replaced_orders.order_id,replaced_orders.quantity,replaced_orders.product_price,replaced_orders.shipping_charge,replaced_orders.grandtotal,orders_status.order_delivered_timestamp,replaced_orders.logistics_name,replaced_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,replaced_orders.customer_id,replacement_desired.transaction_id,replacement_desired.transaction_date,replacement_desired.transaction_time,orders_status.order_replacement_pickup,orders_status.initiate_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.order_replacement_decision_status,replacement_desired.payment_status,invoices_offers.* from replaced_orders,invoices_offers,products,replacement_desired,orders_status,shipping_address,reason_return where products.product_id=replaced_orders.product_id and orders_status.order_item_id=replaced_orders.prev_order_item_id and
			replacement_desired.order_item_id=orders_status.order_item_id and
			replaced_orders.shipping_address_id=shipping_address.shipping_address_id and invoices_offers.order_id=replaced_orders.order_id and reason_return.reason_id=replacement_desired.reason_return_id and (replacement_desired.order_replacement_decision_status='replaced' or replacement_desired.order_replacement_decision_status='refund' or replacement_desired.order_replacement_decision_status='refunded') $str";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( replaced_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replaced_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replaced_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replaced_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replaced_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replaced_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replaced_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replaced_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		public function get_replaced_orders_by_order_item_id($order_item_id){
			$sql="select * from replaced_orders where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		
		public function returns_replacement_request_orders_refund_success_full_refund_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'replaced_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'replacement_desired.order_item_id',
				3 => 'replaced_orders.order_id'
			);
			
			$status_of_refund=$params["status_of_refund"];
			if($status_of_refund=="all"){
				
				$str='';
			}
			if($status_of_refund=="success"){
				$str="and ((orders_status.order_replacement_pickup='1' and trim(orders_status.initiate_replacement_pickup)=trim('')) or (orders_status.order_replacement_pickup='1' and orders_status.initiate_replacement_pickup='1' ) or (orders_status.order_replacement_pickup='0' and orders_status.initiate_replacement_pickup='0' ) )";
			}
			if($status_of_refund=="pending"){
				$str="and ((orders_status.order_replacement_pickup='0' and orders_status.initiate_replacement_pickup='1' ) )";
			}
			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			
			 $sql = "select orders_status.order_replacement_pickup,replaced_orders.timestamp,replaced_orders.*,replaced_orders.grandtotal as grandtotal_of_quantity_purchased,replaced_orders.sku_id,replaced_orders.inventory_id,products.product_name,replaced_orders.order_item_id,replaced_orders.prev_order_item_id,replaced_orders.order_id,replaced_orders.quantity,replaced_orders.product_price,replaced_orders.shipping_charge,replaced_orders.grandtotal,orders_status.order_delivered_timestamp,replaced_orders.logistics_name,replaced_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,replaced_orders.customer_id,replacement_desired.transaction_id,replacement_desired.transaction_date,replacement_desired.transaction_time,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.order_replacement_decision_status,replacement_desired.payment_status,invoices_offers.* from replaced_orders,invoices_offers,products,replacement_desired,orders_status,shipping_address,reason_return where products.product_id=replaced_orders.product_id and orders_status.order_item_id=replaced_orders.prev_order_item_id and replaced_orders.shipping_address_id=shipping_address.shipping_address_id and invoices_offers.order_id=replaced_orders.order_id and reason_return.reason_id=replacement_desired.reason_return_id and orders_status.order_item_id=replacement_desired.order_item_id and (replacement_desired.order_replacement_decision_status='replaced' or replacement_desired.order_replacement_decision_status='refund' or replacement_desired.order_replacement_decision_status='refunded') $str";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( replaced_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replaced_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replaced_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replaced_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replaced_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replaced_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replaced_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replaced_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		public function return_stock_notification($order_item_id){

			// getting total number records without any search
			$sql = "select return_stock_notification.*, DATE_FORMAT(return_stock_notification.timestamp,'%Y-%m-%d') AS 'requested_timestamp',history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,orders_status.id as orders_status_id,history_orders.customer_id,orders_status.order_replacement_pickup,reason_return.reason_name,inventory.stock,inventory.sku_id as requested_sku_id from history_orders,products,orders_status,shipping_address,reason_return,return_stock_notification,inventory where return_stock_notification.order_item_id=history_orders.order_item_id and return_stock_notification.order_item_id=orders_status.order_item_id and products.product_id=history_orders.product_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=return_stock_notification.reason_return_id and inventory.id=return_stock_notification.replacement_with_inventory_id and return_stock_notification.status='active' and return_stock_notification.order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function return_stock_notification_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'history_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'replacement_desired.order_item_id',
				3 => 'history_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";
			$status_of_stock=$params['status_of_stock'];
			$str='';
			if($status_of_stock=='not_available'){
				$str=' and return_stock_notification.quantity_replacement>=inventory.stock ';
			}elseif($status_of_stock=='available'){
				$str=' and return_stock_notification.quantity_replacement<inventory.stock ';
			}else{
				$str='';
			}
			
			// getting total number records without any search
			$sql = "select return_stock_notification.*, DATE_FORMAT(return_stock_notification.timestamp,'%Y-%m-%d') AS 'requested_timestamp',history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,orders_status.id as orders_status_id,history_orders.customer_id,orders_status.order_replacement_pickup,reason_return.reason_name,inventory.stock,inventory.sku_id as requested_sku_id from history_orders,products,orders_status,shipping_address,reason_return,return_stock_notification,inventory where return_stock_notification.order_item_id=history_orders.order_item_id and return_stock_notification.order_item_id=orders_status.order_item_id and products.product_id=history_orders.product_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=return_stock_notification.reason_return_id and inventory.id=return_stock_notification.replacement_with_inventory_id and return_stock_notification.status='active' $str";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( history_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		public function update_return_stock_notification($orders_status_id,$return_stock_notification_id,$quantity_replacement){
			
			$sql="update return_stock_notification set reserved_stock='$quantity_replacement' where return_stock_notification_id='$return_stock_notification_id'";
			$query=$this->db->query($sql);
			return $query;
		}		
		public function update_timelimitdefined_stock_notification($return_stock_notification_id,$timelimitdefined,$edit_timelimit_comments){
			$edit_timelimit_comments=$this->db->escape_str($edit_timelimit_comments);
			$sql="update return_stock_notification set timelimitdefined='$timelimitdefined',edit_timelimit_comments='$edit_timelimit_comments' where return_stock_notification_id='$return_stock_notification_id'";
			$query=$this->db->query($sql);
			return $query;
		}
		public function get_all_data_of_return_stock_notification($return_stock_notification_id){
			
			$sql="select * from return_stock_notification where return_stock_notification_id='$return_stock_notification_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function move_to_returns_desired($order_item_id,$return_order_id,$reason_return_id,$sub_issue_id,$return_reason_comments,$quantity_refund,$refund_method,$refund_bank_id,$promotion_available,$promotion_discount,$promotion_cashback,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$default_discount,$promotion_item_multiplier,$promotion_item,$promotion_item_num,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_without_promotion,$total_price_of_product_with_promotion,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$original_inventory_id,$order_item_invoice_discount_value_each){
		
			$return_reason_comments=$this->db->escape_str($return_reason_comments);
		
			$sql="insert into returns_desired (quantity_refund,return_order_id,order_item_id,reason_return_id,sub_issue_id,return_reason_comments,refund_method,refund_bank_id,order_return_decision_status,order_return_decision_customer_decision,promotion_available,promotion_discount,promotion_cashback,promotion_surprise_gift,promotion_surprise_gift_type,promotion_surprise_gift_skus,promotion_surprise_gift_skus_nums,default_discount,promotion_item_multiplier,promotion_item,promotion_item_num,promotion_minimum_quantity,promotion_quote,promotion_default_discount_promo,individual_price_of_product_with_promotion,individual_price_of_product_without_promotion,total_price_of_product_without_promotion,total_price_of_product_with_promotion,quantity_without_promotion,quantity_with_promotion,cash_back_value,original_inventory_id,order_item_invoice_discount_value_each) values('$quantity_refund','$return_order_id','$order_item_id','$reason_return_id','$sub_issue_id','$return_reason_comments','$refund_method','$refund_bank_id','pending','pending','$promotion_available','$promotion_discount','$promotion_cashback','$promotion_surprise_gift','$promotion_surprise_gift_type','$promotion_surprise_gift_skus','$promotion_surprise_gift_skus_nums','$default_discount','$promotion_item_multiplier','$promotion_item','$promotion_item_num','$promotion_minimum_quantity','$promotion_quote','$promotion_default_discount_promo','$individual_price_of_product_with_promotion','$individual_price_of_product_without_promotion','$total_price_of_product_without_promotion','$total_price_of_product_with_promotion','$quantity_without_promotion','$quantity_with_promotion','$cash_back_value','$original_inventory_id','$order_item_invoice_discount_value_each')";

			$result=$this->db->query($sql);
			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}
		}
		public function delete_return_stock_notification($return_stock_notification_id){
			$sql="delete from return_stock_notification where return_stock_notification_id=$return_stock_notification_id";
			$result=$this->db->query($sql);
			echo $result;
		}
		public function cancel_refund_request($return_stock_notification_id){
			$sql="update return_stock_notification set status='cancelled' where return_stock_notification_id='$return_stock_notification_id'";
			$query=$this->db->query($sql);
			return $query;
		}
		
		public function returns_replacement_request_orders_balance_clear($order_item_id){
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.amount_paid_item_price,replacement_desired.amount_paid_item_price_original,replacement_desired.paid_by_original,replacement_desired.amount_paid_original,inventory.moq,replacement_desired.shipping_charge_percent_paid_by_customer from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return,inventory where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and (replacement_desired.order_replacement_decision_status='accept' and replacement_desired.order_replacement_decision_customer_decision='accept' and ((orders_status.order_replacement_pickup='1' and orders_status.initiate_replacement_pickup!='0' and orders_status.initiate_replacement_pickup!='1') or (orders_status.order_replacement_pickup='0' and orders_status.initiate_replacement_pickup='1') or (orders_status.order_replacement_pickup='0' and orders_status.initiate_replacement_pickup='0')) and replacement_desired.amount_paid!='0' and replacement_desired.paid_by='customer' and replacement_desired.payment_status='pending' and replacement_desired.order_replacement_decision_customer_status!='cancel' and replacement_desired.balance_amount_paid_by!='cod') and replacement_desired.order_item_id='$order_item_id' and history_orders.inventory_id=inventory.id";
			
			
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function returns_replacement_request_orders_balance_clear_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'history_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'replacement_desired.order_item_id',
				3 => 'history_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select history_orders.sku_id,history_orders.*,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and (replacement_desired.order_replacement_decision_status='accept' and replacement_desired.order_replacement_decision_customer_decision='accept' and ((orders_status.order_replacement_pickup='1' and orders_status.initiate_replacement_pickup!='0' and orders_status.initiate_replacement_pickup!='1') or (orders_status.order_replacement_pickup='0' and orders_status.initiate_replacement_pickup='1') or (orders_status.order_replacement_pickup='0' and orders_status.initiate_replacement_pickup='0')) and replacement_desired.amount_paid!='0' and replacement_desired.paid_by='customer' and replacement_desired.payment_status='pending' and replacement_desired.order_replacement_decision_customer_status!='cancel' and replacement_desired.balance_amount_paid_by!='cod')";
			
			
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( history_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		public function get_unread_replacement_msg_balance_clear($return_order_id){
			$sql="select count(*) as count from replacement_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='accept' and (customer_action='paybalance' or customer_action='accept' or customer_action='replace_cancel' or customer_action='balanceclear')";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		
		public function return_replacement_balance_clear($order_item_id){
			$sql_update_replacement_desired="update replacement_desired set order_replacement_decision_status='refund' where order_item_id='$order_item_id'";
			$this->db->query($sql_update_replacement_desired);
			
			$sql_update_order_replacement_decision="update order_replacement_decision set status='refund',notify_replacement_account='1' where order_item_id='$order_item_id'";
			$flag=$this->db->query($sql_update_order_replacement_decision);
			return $flag;
		}
		
		public function returns_replacement_request_orders_accounts_incoming($order_item_id){
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.payment_status,replacement_desired.shipping_charge_applied_cancels_chk,replacement_desired.return_shipping_concession_cancels_chk,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,order_replacement_decision.refund_status,order_replacement_decision .sub_reason_return_id,admin_replacement_reason.pickup_identifier from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return,order_replacement_decision,admin_replacement_reason where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and 
			order_replacement_decision.order_item_id = replacement_desired.order_item_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and admin_replacement_reason.id=order_replacement_decision.sub_reason_return_id and ((replacement_desired.order_replacement_decision_status='refund'  or order_replacement_decision.refund_status='refund') and (replacement_desired.amount_paid!=0 or order_replacement_decision.return_shipping_concession!=0 or order_replacement_decision.refund_status='refund') and (replacement_desired.status_of_refund!='yes')) and replacement_desired.order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function returns_replacement_request_orders_accounts_incoming_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'history_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'replacement_desired.order_item_id',
				3 => 'history_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.payment_status,replacement_desired.shipping_charge_applied_cancels_chk,replacement_desired.return_shipping_concession_cancels_chk,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,order_replacement_decision.refund_status,order_replacement_decision .sub_reason_return_id,admin_replacement_reason.pickup_identifier from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return,order_replacement_decision,admin_replacement_reason where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and 
			order_replacement_decision.order_item_id = replacement_desired.order_item_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and admin_replacement_reason.id=order_replacement_decision.sub_reason_return_id and ((replacement_desired.order_replacement_decision_status='refund'  or order_replacement_decision.refund_status='refund') and (replacement_desired.amount_paid!=0 or order_replacement_decision.return_shipping_concession!=0 or order_replacement_decision.refund_status='refund') and (replacement_desired.status_of_refund!='yes'))";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( history_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		public function replacement_desired_table_data($order_item_id){
			$sql="select * from replacement_desired where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row_array();
		}
		
		public function return_replacement_success($order_item_id,$transaction_id,$transaction_date,$transaction_time,$refund_status=""){
			
			$get_replacement_desired_table_data=$this->replacement_desired_table_data($order_item_id);
			if($get_replacement_desired_table_data["refund_method"]=="Bank Transfer"){
				$refund_bank_id=$get_replacement_desired_table_data["refund_bank_id"];
				/*$sql="select * from refund_bank_details where id='$refund_bank_id'";
				$query=$this->db->query($sql);
				$refund_details_arr=$query->row_array();
				$sql="insert into delivered_refund_bank_details set 
					bank_name='$refund_details_arr[bank_name]',
					branch_name='$refund_details_arr[branch_name]',
					city='$refund_details_arr[city]',
					ifsc_code='$refund_details_arr[ifsc_code]',
					account_number='$refund_details_arr[account_number]',
					confirm_account_number='$refund_details_arr[confirm_account_number]',
					account_holder_name='$refund_details_arr[account_holder_name]',
					mobile_number='$refund_details_arr[mobile_number]',
					customer_id='$refund_details_arr[customer_id]'";
					$query=$this->db->query($sql);
					$refund_bank_id=$this->db->insert_id();*/
			}
			
			if($get_replacement_desired_table_data["refund_method"]=="Bank Transfer"){
				$sql="update replacement_desired set status_of_refund='yes',transaction_id='$transaction_id',transaction_date='$transaction_date',transaction_time='$transaction_time' where order_item_id='$order_item_id'";
				$query=$this->db->query($sql);
			}
			if($get_replacement_desired_table_data["refund_method"]=="Wallet"){
				$sql="update replacement_desired set status_of_refund='yes',transaction_id='$transaction_id',transaction_date='$transaction_date',transaction_time='$transaction_time' where order_item_id='$order_item_id'";
				$query=$this->db->query($sql);
			}
			
			if($refund_status!="refund"){
				$sql_update_replacement_desired="update replacement_desired set order_replacement_decision_status='refunded' where order_item_id='$order_item_id'";
				$this->db->query($sql_update_replacement_desired);
			
				$sql_update_order_replacement_decision="update order_replacement_decision set status='refunded' where order_item_id='$order_item_id'";
				$flag=$this->db->query($sql_update_order_replacement_decision);
			}
			
			
			if($refund_status=="refund"){
				$sql="update replacement_desired set order_replacement_decision_refund_status='refunded' where order_item_id='$order_item_id'";
				$query=$this->db->query($sql);
				
				$sql="update order_replacement_decision set refund_status='refunded' where order_item_id='$order_item_id'";
				$query=$this->db->query($sql);
				
	
			}
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				
			$get_orders_status_data=$this->get_orders_status_data($order_item_id);
			if($get_orders_status_data->order_replacement_pickup==1 && $get_replacement_desired_table_data["order_replacement_decision_customer_status"]=="cancel"){
				
				$sql_delete_returned_orders="delete from returned_orders where order_item_id='$order_item_id'";
				$query_delete_returned_orders=$this->db->query($sql_delete_returned_orders);
				
				
				$sql_returned_orders="insert into returned_orders select * from completed_orders where order_item_id='$order_item_id'";
				$query_returned_orders=$this->db->query($sql_returned_orders);
				/////////////////////
				
				$sql_get_completed_orders_data="select * from completed_orders where order_item_id='$order_item_id'";
				$query_get_completed_orders_data=$this->db->query($sql_get_completed_orders_data);
				$completed_orders_product_price=$query_get_completed_orders_data->row()->product_price;
				$completed_orders_shipping_charge=$query_get_completed_orders_data->row()->shipping_charge;
				
				$sql_get_refund_data="select * from order_replacement_decision where order_item_id='$order_item_id'";
				$query_get_refund_data=$this->db->query($sql_get_refund_data);
				$quantity_to_be_refuned=$query_get_refund_data->row()->quantity;
				
				$subtotal_to_be_refuned=($completed_orders_product_price)*($quantity_to_be_refuned);
				$grandtotal_to_be_refuned=($completed_orders_product_price+$completed_orders_shipping_charge)*($quantity_to_be_refuned);
				
				$sql_update_returned_orders="update returned_orders set subtotal='$subtotal_to_be_refuned',grandtotal='$grandtotal_to_be_refuned',quantity='$quantity_to_be_refuned' where order_item_id='$order_item_id'";
				$query_update_returned_orders=$this->db->query($sql_update_returned_orders);
				///////////////////////
				$sql_get_quantity_completed_orders="select quantity from completed_orders where order_item_id='$order_item_id'";
				$query_get_quantity_completed_orders=$this->db->query($sql_get_quantity_completed_orders);
				$quantity_purchased=$query_get_quantity_completed_orders->row()->quantity;
				////////////////////////
				if($quantity_to_be_refuned==$quantity_purchased){
					$sql_completed_orders="delete from completed_orders where order_item_id='$order_item_id'";
					$query_completed_orders=$this->db->query($sql_completed_orders);
				}
				
				/////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				$sql_order_return_decision="select * from order_return_decision where order_item_id='$order_item_id'";
				$query_order_return_decision=$this->db->query($sql_order_return_decision);
				$query_order_return_decision_arr=$query_order_return_decision->row_array();
				if(count($query_order_return_decision_arr)>0 && $query_get_refund_data->row()->customer_status=="cancel"){
					if($query_order_return_decision_arr["status"]=="refunded"){
						if(($quantity_to_be_refuned+$query_order_return_decision_arr["quantity"])==$quantity_purchased){
							$sql_completed_orders="delete from completed_orders where order_item_id='$order_item_id'";
							$query_completed_orders=$this->db->query($sql_completed_orders);
						}
					}
				}
			}
			//////
			/*$returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin_obj=$this->returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin($order_item_id);
			if(count($returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin_obj)>0){
				if(($quantity_to_be_refuned+$returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin_obj->quantity)==$quantity_purchased){
						$sql_completed_orders="delete from completed_orders where order_item_id='$order_item_id'";
						$query_completed_orders=$this->db->query($sql_completed_orders);
					}
			}*/
			/////////////////////
			/////////////////////
			
			return $query;
			
		}
		
		public function returns_replacement_request_orders_accounts_success($order_item_id){
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.payment_status,replacement_desired.shipping_charge_applied_cancels_chk,replacement_desired.return_shipping_concession_cancels_chk,order_replacement_decision.refund_status,order_replacement_decision .sub_reason_return_id,admin_replacement_reason.pickup_identifier from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return,order_replacement_decision,admin_replacement_reason where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and 
			order_replacement_decision.order_item_id = replacement_desired.order_item_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and admin_replacement_reason.id=order_replacement_decision.sub_reason_return_id and ((replacement_desired.order_replacement_decision_status='refunded' or replacement_desired.order_replacement_decision_refund_status='refunded')  and (replacement_desired.amount_paid!=0 or order_replacement_decision.return_shipping_concession!=0) and (replacement_desired.status_of_refund='yes')) and replacement_desired.order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function returns_replacement_request_orders_accounts_success_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'history_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'replacement_desired.order_item_id',
				3 => 'history_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.payment_status,replacement_desired.shipping_charge_applied_cancels_chk,replacement_desired.return_shipping_concession_cancels_chk,order_replacement_decision.refund_status,order_replacement_decision .sub_reason_return_id,admin_replacement_reason.pickup_identifier from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return,order_replacement_decision,admin_replacement_reason where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and 
			 order_replacement_decision.order_item_id = replacement_desired.order_item_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and admin_replacement_reason.id=order_replacement_decision.sub_reason_return_id and ((replacement_desired.order_replacement_decision_status='refunded' or replacement_desired.order_replacement_decision_refund_status='refunded')  and (replacement_desired.amount_paid!=0 or order_replacement_decision.return_shipping_concession!=0) and (replacement_desired.status_of_refund='yes'))";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( history_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		
		public function get_unread_return_msg_stock($return_order_id){
			$sql="select count(*) as count from  replacement_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='stock' and customer_action='stock'";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		public function update_move_to_refund_flag_in_return_stock_notification_table($order_item_id){
			$sql_move_to_refund_flag="update return_stock_notification set move_to_refund_flag='1' where order_item_id='$order_item_id'";
			$this->db->query($sql_move_to_refund_flag);
		}
		
		public function get_primary_replacement_reason_admin($reason_return_id){
			$sql="select return_options from admin_replacement_reason where id='$reason_return_id'";
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row()->return_options;
			}
			else{
				return false;
			}
		}
		
		public function returns_replacement_request_orders_replacement_cancels_processing($params,$fg){
			//define index of column
			$columns = array(
				0 =>'history_orders.sku_id', 
				1 => 'products.product_name',
				2 => 'replacement_desired.order_item_id',
				3 => 'history_orders.order_id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.prev_order_item_id,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_shipped,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.payment_status,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.order_replacement_decision_status,replacement_desired.move_to_refund_flag,replacement_desired.order_replacement_decision_refund_status,replacement_desired.shipping_charge_applied_cancels_chk,replacement_desired.return_shipping_concession_cancels_chk,replacement_desired.type_of_replacement_cancel,order_replacement_decision.sub_reason_return_id,admin_replacement_reason.pickup_identifier from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return,order_replacement_decision,admin_replacement_reason where order_replacement_decision.order_item_id=replacement_desired.order_item_id and history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and admin_replacement_reason.id=order_replacement_decision.sub_reason_return_id and (replacement_desired.order_replacement_decision_customer_status='cancel' and (replacement_desired.order_replacement_decision_refund_status='need_to_refund' or replacement_desired.order_replacement_decision_refund_status='refund' or replacement_desired.order_replacement_decision_refund_status='refunded'))";
			// check search value exist
			if( !empty($params['search']['value']) ){ 
				$where .=" and ";
				$where .=" ( history_orders.sku_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.order_item_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.quantity LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.product_price LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.shipping_charge LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.grandtotal LIKE '%".$params['search']['value']."%' ";
				$where .=" OR orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' ";
				$where .=" OR replacement_desired.return_order_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.logistics_name LIKE '%".$params['search']['value']."%' ";
				$where .=" OR history_orders.tracking_number LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		public function update_move_to_refund_flag_in_replacement_desired_table($order_item_id){
			$sql_move_to_refund_flag="update replacement_desired set move_to_refund_flag='1' where order_item_id='$order_item_id'";
			$this->db->query($sql_move_to_refund_flag);
		}
		
		public function return_replacement_balance_clear_for_refund($order_item_id,$shipping_charge_applied_cancels_chk,$return_shipping_concession_cancels_chk){
			$sql_update_replacement_desired="update replacement_desired set order_replacement_decision_refund_status='refund',shipping_charge_applied_cancels_chk='$shipping_charge_applied_cancels_chk',return_shipping_concession_cancels_chk='$return_shipping_concession_cancels_chk' where order_item_id='$order_item_id'";
			$this->db->query($sql_update_replacement_desired);
			
			$sql_update_order_replacement_decision="update order_replacement_decision set refund_status='refund' where order_item_id='$order_item_id'";
			$flag=$this->db->query($sql_update_order_replacement_decision);
			return $flag;
		}
		public function update_return_shipping_concession_for_replacement($return_shipping_concession,$order_item_id){
			$sql="update order_replacement_decision set return_shipping_concession_chk='yes',return_shipping_concession='$return_shipping_concession' where order_item_id='$order_item_id'";
			$this->db->query($sql);
		}
		public function update_refund_method_as_wallet_in_replacement_desired_table($order_item_id){
			$sql="update replacement_desired set refund_method='Wallet',refund_bank_id='' where order_item_id='$order_item_id'";
			$this->db->query($sql);
		}
		
		public function returns_cancelled_orders_processing($params,$fg){
			//define index of column
			
			$columns = array(
				0 =>'orders_status.id'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			$sql = "select * from (select return_desired_id as 'table_id', orders_status.id as orders_status_id,returns_desired.order_item_id,'returns_desired' as 'table' from returns_desired,orders_status where returns_desired.order_return_decision_customer_status='cancel' and orders_status.order_item_id=returns_desired.order_item_id   
			 union 
			select replacement_desired_id as 'table_id', orders_status.id as orders_status_id,replacement_desired.order_item_id,'replacement_desired' as 'table' from replacement_desired,orders_status where replacement_desired.order_replacement_decision_customer_status='cancel' and (replacement_desired.order_replacement_decision_refund_status!='refunded' and orders_status.order_replacement_pickup='0' and ((replacement_desired.payment_status!='paid' and replacement_desired.paid_by='customer') or (replacement_desired.paid_by='admin') or (replacement_desired.paid_by=''))) and orders_status.order_item_id=replacement_desired.order_item_id  
			 union 
			select return_stock_notification_id as 'table_id',orders_status.id as orders_status_id,return_stock_notification.order_item_id,'return_stock_notification' as 'table' from return_stock_notification,orders_status where return_stock_notification.status='cancelled' and orders_status.order_item_id=return_stock_notification.order_item_id) as a  
			";
			// check search value exist
			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .="( orders_status.order_delivered_timestamp LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY table_id    ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}

		public function return_stock_notification_table_data($order_item_id){
			
			$sql="select * from return_stock_notification where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row_array();
		}
		
		public function get_order_return_primary_reasons_by_reason_id($reason_id){
			$sql="select * from reason_return where reason_id='$reason_id'";
			$result=$this->db->query($sql);
			return $result->row_array();
		}
		
		public function get_order_return_primary_reasons_by_reason_id_refund($sub_reason_return_id){
			$sql="select * from admin_return_reason where id='$sub_reason_return_id'";
			$result=$this->db->query($sql);
			return $result->row_array();
		}
		public function restore_cancelled_refund_order($order_item_id){
			$sql="update returns_desired set order_return_decision_customer_status='pending',order_return_decision_customer_status_comments='' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			
			$sql="update order_return_decision set customer_status='pending',customer_status_comments='' where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query;
		}
		
		public function get_skuid_by_inventory_id_replace($replacement_with_inventory_id){
			$sql="select * from inventory where id='$replacement_with_inventory_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		
		public function get_order_return_primary_reasons_by_reason_id_replace($sub_reason_return_id){
			$sql="select * from admin_replacement_reason where id='$sub_reason_return_id'";
			$result=$this->db->query($sql);
			return $result->row_array();
		}
		
		public function get_logistics_details($order_item_id){
		 $sql="select inventory_id, logistics_id,logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id from active_orders where order_item_id='$order_item_id' union select inventory_id,logistics_id, 	logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id from cancelled_orders where order_item_id='$order_item_id' union select inventory_id,logistics_id, 	logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id from completed_orders where order_item_id='$order_item_id' union select inventory_id,logistics_id, 	logistics_delivery_mode_id,logistics_territory_id,logistics_parcel_category_id from returned_orders where order_item_id='$order_item_id'";
	
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	
	public function get_logistics_inventory($inventory_id){
		$sql="select inventory_moq,inventory_weight_in_kg,inventory_volume_in_kg from logistics_inventory where inventory_id='$inventory_id'";
		$result=$this->db->query($sql);
		return $result->row_array();
	}
	public function get_logistics_inventory_data($inventory_id){
			$sql="select * from logistics_inventory where inventory_id='$inventory_id'";
			$result=$this->db->query($sql);
			return $result->result()[0];
		}
	public function get_logistics_by_logistics_id($logistics_id){
			$sql="select * from logistics where logistics_id='$logistics_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		public function get_logistics_parcel_box_dimension_info($logistics_id){
			$get_logistics_by_logistics_id_row=$this->get_logistics_by_logistics_id($logistics_id);
			$vendor_id=$get_logistics_by_logistics_id_row->vendor_id;
			$sql="select * from logistics_parcel_box_dimension where vendor_id='$vendor_id' order by lbh asc";
			$query=$this->db->query($sql);
			return $query->result();
		}
		
		public function get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id($logistics_parcel_box_dimension_id){
		$sql="select * from logistics_parcel_box_dimension where logistics_parcel_box_dimension_id='$logistics_parcel_box_dimension_id'";
		$query=$this->db->query($sql);
		return $query->row();
	} 
	public function get_logistics_price_for_desired_quantity_refund($desired_quantity_replacement,$order_item_id,$calculated_weight_in_kg,$inventory_weight_in_kg,$inventory_volume_in_kg,$inventory_id,$logistics_territory_id="",$default_delivery_mode_id="",$logistics_parcel_category_id=""){
		$get_logistics_inventory_data_obj=$this->get_logistics_inventory_data($inventory_id);
		$get_data_from_history_orders_obj=$this->get_data_from_history_orders($order_item_id);
		$current_quantity=$desired_quantity_replacement;
		$inventory_id=$get_data_from_history_orders_obj->inventory_id;
		$logistics_id=$get_data_from_history_orders_obj->logistics_id;
		$inventory_weight_in_kg_per_unit=$inventory_weight_in_kg/$current_quantity;
		
		$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
		$logistics_volumetric_value=$get_logistics_by_logistics_id_obj->logistics_volumetric_value;
			
		///////////////////
		
		
		
			/* new code - nanthini  */
			
			$weight=$inventory_weight_in_kg;
			$sql="SELECT logistics_price,SUBSTRING_INDEX(weight_in_kg,'-',1) as 'weight_in_kg_from',SUBSTRING_INDEX(weight_in_kg,'-',-1) as 'weight_in_kg_to'
FROM logistics_weight where $weight between SUBSTRING_INDEX(weight_in_kg,'-',1) and SUBSTRING_INDEX(weight_in_kg,'-',-1) and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id'";
			
			
			/*$sql="SELECT logistics_price,
			REPLACE(weight_in_kg, '-', ',') as dashRep
			FROM logistics_weight
			WHERE FIND_IN_SET($weight, REPLACE(weight_in_kg, '-', ',')) and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id'";*/

			$query=$this->db->query($sql);

			$res=$query->row_array();
			$logistics_price_with_tax=$res["logistics_price"];
			$required_volume_in_kg=$inventory_weight_in_kg;

			//print_r($res);

			/* new code - nanthini  */

			return $logistics_price_with_tax;
			
			
			
		
		
		/*
			/////// for all the skus code 4-13-2021 based on packings not volumetric weight starts/////
			
			$inventory_length=$get_logistics_inventory_data_obj->inventory_length;
			$inventory_breadth=$get_logistics_inventory_data_obj->inventory_breadth;
			$inventory_height=$get_logistics_inventory_data_obj->inventory_height;
			
			$inventory_moq=$get_logistics_inventory_data_obj->inventory_moq;
			$suitable_box_arr=array();
			$get_logistics_parcel_box_dimension_info_result=$this->get_logistics_parcel_box_dimension_info($logistics_id);
			foreach($get_logistics_parcel_box_dimension_info_result as $get_logistics_parcel_box_dimension_info_row){
				$available_space_for_filling_length=$get_logistics_parcel_box_dimension_info_row->available_space_for_filling_length;
				$available_space_for_filling_breadth=$get_logistics_parcel_box_dimension_info_row->available_space_for_filling_breadth;
				$available_space_for_filling_height=$get_logistics_parcel_box_dimension_info_row->available_space_for_filling_height;
			
				if($inventory_length<=$available_space_for_filling_length && $inventory_breadth<=$available_space_for_filling_breadth & $inventory_height<=$available_space_for_filling_height){
						$suitable_box_arr[$get_logistics_parcel_box_dimension_info_row->logistics_parcel_box_dimension_id]=0;
				}
			}
			
			foreach($suitable_box_arr as $logistics_parcel_box_dimension_id => $val){
				$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row=$this->get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id($logistics_parcel_box_dimension_id);
				$box_available_space_for_filling_length=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->available_space_for_filling_length;
				$box_available_space_for_filling_breadth=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->available_space_for_filling_breadth;
				$box_available_space_for_filling_height=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->available_space_for_filling_height;
				
				$box_available_space_for_filling_length_temp=$box_available_space_for_filling_length;
				$box_available_space_for_filling_breadth_temp=$box_available_space_for_filling_breadth;
				$box_available_space_for_filling_height_temp=$box_available_space_for_filling_height;
				$flag=true;
				
				while($flag){
					if($inventory_length<=$box_available_space_for_filling_length_temp){
						$suitable_box_arr[$logistics_parcel_box_dimension_id]++;
						
						$box_available_space_for_filling_length_temp=$box_available_space_for_filling_length_temp-$inventory_length;
						$box_available_space_for_filling_breadth_temp=$box_available_space_for_filling_breadth_temp-$inventory_breadth;
						$box_available_space_for_filling_height_temp=$box_available_space_for_filling_height_temp-$inventory_height;
								
								
					}
					else{
						$flag=false;
					}
				}
				
			}
			arsort($suitable_box_arr);
			
			//print_r($suitable_box_arr);
			$no_of_boxes_arr=[];
			$extra_box_arr=[];
			$quantity_filled=0;
			$current_quantity_temp=$current_quantity;
			foreach($suitable_box_arr as $box_id => $box_fitted_quantity){
				if($current_quantity_temp>=$box_fitted_quantity){
					$no_of_boxes_arr[$box_id]["no_of_boxes"]=(int)($current_quantity_temp/$box_fitted_quantity);
					$quantity_filled+=($no_of_boxes_arr[$box_id]["no_of_boxes"]*$suitable_box_arr[$box_id]);
					$current_quantity_temp=$current_quantity_temp%$box_fitted_quantity;
				}
			}
			
			$remaining_quantity=$current_quantity-$quantity_filled;
			if($remaining_quantity>0){
				foreach($suitable_box_arr as $box_id => $box_fitted_quantity){
					if((100-(($remaining_quantity/$box_fitted_quantity))*100)<=50){ // remaining space
						$extra_box_arr[$box_id]["no_of_quantities"]=$remaining_quantity;
					}
				}
			}
			//print_r($no_of_boxes_arr);
			//print_r($extra_box_arr);
			$total_arr=array("no_of_boxes_arr"=>$no_of_boxes_arr,"extra_box_arr"=>$extra_box_arr);
			$max_actual_volumetric_arr=array();
			foreach($total_arr as $k=>$v_arr){
				if($k=="no_of_boxes_arr"){
					if(!empty($v_arr)){
						foreach($v_arr as $box_id => $boxes_arr){
							$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row=$this->get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id($box_id);
							
							$parcel_lbh=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_length*
							$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_breadth*
							$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_height;
							for($i=1;$i<=$boxes_arr["no_of_boxes"];$i++){
								$box_actual_weight_without_packing=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->box_actual_weight_without_packing;
								
								$total_actual_weight_parcel_sku=($inventory_weight_in_kg_per_unit*$suitable_box_arr[$box_id])+$box_actual_weight_without_packing;
								
								$volumetric_weight_of_parcel_in_kg=$parcel_lbh/$logistics_volumetric_value;
								
								$max_actual_volumetric_arr[]=max($total_actual_weight_parcel_sku,$volumetric_weight_of_parcel_in_kg);
							}
							//$boxes_arr["no_of_boxes"]
						}
					}
				}
				if($k=="extra_box_arr"){
					if(!empty($v_arr)){
						foreach($v_arr as $box_id => $boxes_arr){
							$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row=$this->get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id($box_id);
							
							$parcel_lbh=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_length*
							$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_breadth*
							$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->parcel_height;
							
							$box_actual_weight_without_packing=$get_logistics_parcel_box_dimension_info_by_logistics_parcel_box_dimension_id_row->box_actual_weight_without_packing;
							
							$total_actual_weight_parcel_sku=($inventory_weight_in_kg_per_unit*$boxes_arr["no_of_quantities"])+$box_actual_weight_without_packing;
							
							$volumetric_weight_of_parcel_in_kg=$parcel_lbh/$logistics_volumetric_value;
							
							$max_actual_volumetric_arr[]=max($total_actual_weight_parcel_sku,$volumetric_weight_of_parcel_in_kg);
							
							//$boxes_arr["no_of_boxes"]
						}
					}
				}
			}
			//print_r($max_actual_volumetric_arr);
			$required_volume_in_kg=array_sum($max_actual_volumetric_arr);
			//$required_volume_in_kg=2;
			
			
			
			/////// for all the skus code  4-13-2021 based on packings not volumetric weight ends/////
			
			
			
			
		
		//$get_logistics_inventory_data_obj=$this->get_logistics_inventory_data($inventory_id);
			//$inventory_volume_in_lbh_cms=$get_logistics_inventory_data_obj->inventory_volume_in_kg;
			//$quantity_volumetric_breakup_point=$get_logistics_inventory_data_obj->volumetric_breakup_point;
			
			//$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
			//$logistics_volumetric_value=$get_logistics_by_logistics_id_obj->logistics_volumetric_value;
			//$net_volumetric_volume_weight=$inventory_volume_in_lbh_cms/$logistics_volumetric_value;
			
			//if($current_quantity<($quantity_volumetric_breakup_point+1)){
				//$resultant_inventory_volume_in_kg=$net_volumetric_volume_weight;
			//}
			//else{
				//$resultant_inventory_volume_in_kg=(intval(($current_quantity-1)/$quantity_volumetric_breakup_point)+1)*$net_volumetric_volume_weight;
			//}
			
		//////////////
		//$required_volume_in_kg=max($calculated_weight_in_kg,$resultant_inventory_volume_in_kg);
		
		//$sql="SELECT logistics_price,volume_in_kg FROM logistics_weight where SUBSTRING_INDEX(weight_in_kg, '-', 1) <=$required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(weight_in_kg, '-', 2), '-', -1) >=$required_volume_in_kg order by volume_in_kg";
			
			//$query=$this->db->query($sql);
			//$logistics_price=0;
			//foreach($query->result() as $resObj){
			//	$logistics_price=$resObj->logistics_price;
			//	break;
			//}
			//return $logistics_price;
			
			
			
			$logistics_price=0;
			$sql="SELECT logistics_price,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg!='yes'";
			
			$query=$this->db->query($sql);
			if(!empty($query->result())){
				$logistics_price=0;
				foreach($query->result() as $resObj){
					$logistics_price=$resObj->logistics_price;
					break;
				}
				//////////////////// Tax caculation starts /////
				$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
				$logistics_gst=$get_logistics_by_logistics_id_obj->logistics_gst;
				$logistics_fuelsurcharge=$get_logistics_by_logistics_id_obj->logistics_fuelsurcharge;
				$logistics_tax=$logistics_gst+$logistics_fuelsurcharge;
				$logistics_price_with_tax=$logistics_price+($logistics_price*($logistics_tax/100));
				//////////////////// Tax caculation ends /////
				return $logistics_price_with_tax;
			}
			
			/// if parcel category id is there no problem
			$sql_additional_weight_in_kg="SELECT logistics_price,volume_in_kg,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg='yes'";
			
			$query_additional_weight_in_kg=$this->db->query($sql_additional_weight_in_kg);
			if(!empty($query_additional_weight_in_kg->result())){
				//////////////////
				$sql_take_latest_price_before_breakpoint="SELECT logistics_price,volume_in_kg,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where  territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg!='yes' order by logistics_weight_id desc limit 0,1";
				$query_take_latest_price_before_breakpoint=$this->db->query($sql_take_latest_price_before_breakpoint);
				$logistics_price_latest_price_before_breakpoint=$query_take_latest_price_before_breakpoint->row_array()["logistics_price"];
				$logistics_weight_latest_price_before_breakpoint=$query_take_latest_price_before_breakpoint->row_array()["volume_in_kg"];
				$logistics_weight_latest_price_before_breakpoint_max=explode("-",$logistics_weight_latest_price_before_breakpoint)[1];
				/////////////////
				$logistics_price=0;
				foreach($query_additional_weight_in_kg->result() as $resObj){
					$additional_weight_value_in_kg=$resObj->additional_weight_value_in_kg;
					$do_u_want_to_roundup=$resObj->do_u_want_to_roundup;
					$do_u_want_to_include_breakupweight=$resObj->do_u_want_to_include_breakupweight;
					if($do_u_want_to_roundup=="yes" && $do_u_want_to_include_breakupweight=="yes"){
						$result_value_of_weights=(ceil($required_volume_in_kg-$logistics_weight_latest_price_before_breakpoint_max))/$additional_weight_value_in_kg;
						$logistics_price=$logistics_price_latest_price_before_breakpoint+(($result_value_of_weights)*($resObj->logistics_price));
					}
					if($do_u_want_to_roundup=="yes" && $do_u_want_to_include_breakupweight=="no"){
						$logistics_price=(ceil($required_volume_in_kg)/$additional_weight_value_in_kg)*$resObj->logistics_price;
					}
					if($do_u_want_to_roundup=="no"){
						$logistics_price_part_1=0;
						$logistics_price_part_2=0;
						
						$decimalpart_of_required_volume_in_kg=floor($required_volume_in_kg);
						$fractionalpart_of_required_volume_in_kg=$required_volume_in_kg-$decimalpart_of_required_volume_in_kg;
						$logistics_price_part_1=($decimalpart_of_required_volume_in_kg/$additional_weight_value_in_kg)*$resObj->logistics_price;
						
						$sql_fractional_part="SELECT logistics_price,do_u_want_to_roundup,do_u_want_to_include_breakupweight,additional_weight_value_in_kg FROM logistics_weight where SUBSTRING_INDEX(volume_in_kg, '-', 1) <=$fractionalpart_of_required_volume_in_kg and SUBSTRING_INDEX(SUBSTRING_INDEX(volume_in_kg, '-', 2), '-', -1) >=$fractionalpart_of_required_volume_in_kg  and territory_name_id='$logistics_territory_id' and logistics_id='$logistics_id' and logistics_delivery_mode_id='$default_delivery_mode_id' and logistics_parcel_category_id='$logistics_parcel_category_id' and additional_weight_in_kg!='yes'";
						$query_fractional_part=$this->db->query($sql_fractional_part);
						if(!empty($query_fractional_part->result())){
							foreach($query_fractional_part->result() as $resObj_fractional_part){
								$logistics_price_part_2=$resObj_fractional_part->logistics_price;
								break;
							}
						}
						$logistics_price=$logistics_price_part_1+$logistics_price_part_2;
						
					}
					break;
				}
				//////////////////// Tax caculation starts /////
				$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
				$logistics_gst=$get_logistics_by_logistics_id_obj->logistics_gst;
				$logistics_fuelsurcharge=$get_logistics_by_logistics_id_obj->logistics_fuelsurcharge;
				$logistics_tax=$logistics_gst+$logistics_fuelsurcharge;
				$logistics_price_with_tax=$logistics_price+($logistics_price*($logistics_tax/100));
				//////////////////// Tax caculation ends /////
				return $logistics_price_with_tax;
			}
			//////////////////// Tax caculation starts /////
				$get_logistics_by_logistics_id_obj=$this->get_logistics_by_logistics_id($logistics_id);
				$logistics_gst=$get_logistics_by_logistics_id_obj->logistics_gst;
				$logistics_fuelsurcharge=$get_logistics_by_logistics_id_obj->logistics_fuelsurcharge;
				$logistics_tax=$logistics_gst+$logistics_fuelsurcharge;
				$logistics_price_with_tax=$logistics_price+($logistics_price*($logistics_tax/100));
				//////////////////// Tax caculation ends /////
			return $logistics_price_with_tax;
			*/
			
	}
	
	public function get_parcel_category_multiplier($logistics_parcel_category_id){
			$sql="select multiplier from logistics_parcel_category where logistics_parcel_category_id='$logistics_parcel_category_id'";
			$result=$this->db->query($sql);
			
			return $result->row()->multiplier;
	
		
	}
	
	
	public function returns_replacement_request_orders_accounts_paid_by_customer_not_by_admin($order_item_id){
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.payment_status,replacement_desired.shipping_charge_applied_cancels_chk,replacement_desired.return_shipping_concession_cancels_chk,order_replacement_decision.refund_status,order_replacement_decision.quantity from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return,order_replacement_decision where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and 
			order_replacement_decision.order_item_id = replacement_desired.order_item_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and replacement_desired.paid_by='customer' and replacement_desired.payment_status='paid' and replacement_desired.status_of_refund!='yes' and replacement_desired.order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		///added//////
		public function initiate_order_return_pickupFun($orders_status_id,$return_order_id,$order_item_id,$initiate_return_pickup){
			$sql="update orders_status set initiate_return_pickup='$initiate_return_pickup',return_order_id='$return_order_id' where id='$orders_status_id'";
			$query=$this->db->query($sql);
			return $query;
		}
		public function initiate_order_return_again_pickupFun($orders_status_id,$return_order_id,$order_item_id,$initiate_return_pickup_again){
			$sql="update orders_status set initiate_return_pickup_again='$initiate_return_pickup_again',return_order_id='$return_order_id' where id='$orders_status_id'";
			$query=$this->db->query($sql);
			return $query;
		}
		public function update_order_return_pickup_again($orders_status_id,$return_order_id,$order_item_id,$update_inventory_stock_when_item_picked_up){
			$sql="update orders_status set order_return_pickup_again='1',return_order_id='$return_order_id',update_stock_order_return_pickup_again='$update_inventory_stock_when_item_picked_up' where id='$orders_status_id'";
			$query=$this->db->query($sql);
			return $query;
		}
		public function initiate_order_replacement_pickupFun($orders_status_id,$return_order_id,$order_item_id,$initiate_replacement_pickup){
			
			$sql="update orders_status set initiate_replacement_pickup='$initiate_replacement_pickup',return_order_id='$return_order_id' where id='$orders_status_id'";
			$query=$this->db->query($sql);
			return $query;
		}
		public function get_orders_status_data($order_item_id){
			$sql="select * from orders_status where order_item_id='$order_item_id'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function update_order_replacement_pickup_prev_order($orders_status_id,$return_order_id,$order_item_id,$update_inventory_stock_when_item_picked_up){
			
			$sql="update orders_status set order_replacement_pickup='1',return_order_id='$return_order_id',update_stock_order_replacement_pickup='$update_inventory_stock_when_item_picked_up' where id='$orders_status_id'";
			$query=$this->db->query($sql);
			
			return $query;
		}
		public function get_all_order_invoice_offers_obj($id){
    
			$sql="select * from invoices_offers where order_id='$id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		public function get_shipping_charge_and_grand_total_from_history_orders($order_item_id){
			
			$sql="select shipping_charge,grandtotal,quantity,subtotal from history_orders where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		public function get_data_from_invoice_offers($order_id){
			
			$sql="select * from invoices_offers where order_id='$order_id'";
			$result=$this->db->query($sql);
			return $result->row();
			
		}
		public function get_free_skus_from_inventory($promo_item_str){
			
			$sql="select products.product_name,inventory.sku_id,inventory.id,inventory.attribute_1,inventory.attribute_1_value,inventory.attribute_2,inventory.attribute_2_value,inventory.attribute_3,inventory.attribute_3_value,inventory.attribute_4,inventory.attribute_4_value,inventory.thumbnail,inventory.image,inventory.selling_price from inventory,products where inventory.id in ($promo_item_str) and inventory.product_id=products.product_id";
			
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->result();
			}else{
				return false;
			}
		}
		
		
		public function get_order_replacement_decision_accepted_quantity($order_item_id){
			$sql="select quantity from order_replacement_decision  where order_item_id='$order_item_id'; ";
			$result=$this->db->query($sql);
			return $result->row()->quantity;	
			
		}
		public function get_items_to_be_collected($order_item_id){
			$sql="select prev_order_item_id,quantity,promotion_available,promotion_item,promotion_item_num,promotion_minimum_quantity,promotion_surprise_gift,promotion_surprise_gift_type,promotion_surprise_gift_skus,promotion_surprise_gift_skus_nums from history_orders where order_item_id='$order_item_id'; ";
			$result=$this->db->query($sql);
			return $result->row();		
		}
		public function add_inventory_stock_when_admin_accepts($replacement_with_inventory_id,$stock_to_be_updated){
			//replacement
			$sql_inv="update inventory set stock = (stock+$stock_to_be_updated) where id='$replacement_with_inventory_id'; ";
		
			$result=$this->db->query($sql_inv);
			return $result;	
		}
		public function add_inventory_stock_when_admin_accepts_less_than_previous($replacement_with_inventory_id,$stock_to_be_updated){
			//replacement
			$sql_inv="update inventory set stock = (stock-$stock_to_be_updated) where id='$replacement_with_inventory_id'; ";
		
			$result=$this->db->query($sql_inv);
			return $result;	
		}
		public function update_inventory_stock_when_item_picked_up($original_inventory_id,$quantity){
			//replacement pickup (without free item)
			$sql_update="update inventory set stock=(stock+$quantity) where id=$original_inventory_id;";
			$result=$this->db->query($sql_update);			
			return $result;
		}
		public function update_inventory_stock_and_free_items_when_item_picked_up($original_inventory_id,$quantity,$free_skus_arr,$free_skus_for_gift_arr,$order_item_id){
			
			$sql="select * from history_orders where order_item_id='$order_item_id'; ";
			$result=$this->db->query($sql);
			$res=$result->row();
			if(!empty($res)){
				if($res->ord_addon_products_status=='1'){
					$invs=$res->ord_addon_inventories;
					$invs_ids=explode(',',$invs);
					$invs_ids=array_filter($invs_ids);
					if(!empty($invs_ids)){
						foreach($invs_ids as $inV_id){
							if($inV_id!=''){
								//$quantity=1;
								$sql_update="update inventory set stock=(stock) where id=$inV_id;";
								$result=$this->db->query($sql_update);
							}
						}
					}
				}else{
					//refund pickup with free item
					$sql_update="update inventory set stock=(stock+$quantity) where id=$original_inventory_id;";

					$result=$this->db->query($sql_update);
				}
			}


			$result_free=true;
			if(!empty($free_skus_arr)){
				foreach($free_skus_arr as $id=>$stock){
					$sql_update_free_inv="update inventory set stock=(stock+$stock) where id=$id;";
					$result_free=$this->db->query($sql_update_free_inv);
				}
			}
			if(!empty($free_skus_for_gift_arr)){
				foreach($free_skus_for_gift_arr as $id=>$stock){
					$sql_update_free__gift_inv="update inventory set stock=(stock+$stock) where id=$id;";
					$result_free=$this->db->query($sql_update_free__gift_inv);
				}
			}
				
			if($result_free==true && $result==true){
				return true;	
			}else{
				return false;
			}
			
		}
		public function check_amount_paid_or_not($order_id,$invoice_surprise_gift){
			
			//$sql="select * from rated_inventory where inventory_id='$inventory_id' and status=1 ORDER BY `rating` DESC limit 0,10";
			
			$sql="select count(*) as count from wallet_transaction where transaction_details LIKE '%$order_id%' and credit='$invoice_surprise_gift' ORDER BY `timestamp` DESC limit 0,1";
			
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row()->count;
			}else{
				return false;
			}
		}
		public function check_amount_debit_or_not($order_id,$invoice_surprise_gift){
			
			//$sql="select * from rated_inventory where inventory_id='$inventory_id' and status=1 ORDER BY `rating` DESC limit 0,10";
			
			$sql="select count(*) as count from wallet_transaction where transaction_details LIKE '%$order_id%' and debit='$invoice_surprise_gift' ORDER BY `timestamp` DESC limit 0,1";
			
			$result=$this->db->query($sql);
			if($result->num_rows()>0){
				return $result->row()->count;
			}else{
				return false;
			}
		}
		public function get_sku_ids($promo_gift_arr){
			$sql="select id,sku_id from inventory where id in ($promo_gift_arr)";
			$result=$this->db->query($sql);
			
			$row_res=$result->result_array();
			$one=array();
			$two=array();
			
			foreach($row_res as $v){
				foreach($v as $r=>$n){
					if($r=="id"){
						$one[]=$n;
					}
					if($r=="sku_id"){
						$two[]=$n;
					}
				}
			}
			
			$three=array_combine($one,$two);
			return $three;
		}
		public function get_status_of_free_items($invoices_offers_id){
			$sql="select * from invoices_free_items_status where invoices_offers_id='$invoices_offers_id'";
			$result=$this->db->query($sql);
			$res_obj=$result->result();
			
			//print_r($res_obj);
			
			$array_shipped=array();
			$array_delivered=array();
			$array_comments=array();
			$array_undelivered=array();
			
			foreach($res_obj as $obj){
				$array_delivered[]=$obj->item_delivered;
				$array_shipped[]=$obj->item_shipped;
				$array_undelivered[]=$obj->item_undelivered;
				$array_comments[]=$obj->admin_comments;			
			}

			/*
			if(!in_array("0",$array_delivered)){
				return "delivered";
			}elseif(!in_array("0",$array_shipped)){
				return "shipped";
			}elseif(in_array("0",$array_delivered)){
				return "not delivered";
			}elseif(in_array("0",$array_shipped)){
				return "not shipped";
			}
			*/
			
			$result_arr=array('array_delivered'=>$array_delivered,'array_shipped'=>$array_shipped,'array_undelivered'=>$array_undelivered,'array_comments'=>$array_comments);
			
			return $result_arr;
		}
		public function get_customer_pays_shipping_charge_percentage($inventory_id){
			$sql="select products.pcat_id,products.cat_id,products.subcat_id,products.brand_id,products.product_id,inventory.id as inventory_id from inventory,products where products.product_id=inventory.product_id and inventory.id='$inventory_id'";
			
			$result = $this->db->query($sql);
			$tree_val=$result->row();
			$entire=array();
			if(!empty($tree_val)){

				$pcat_id=$tree_val->pcat_id;
				$cat_id=$tree_val->cat_id;
				$subcat_id=$tree_val->subcat_id;
				$brand_id=$tree_val->brand_id;
				$product_id=$tree_val->product_id;
				$inventory_id=$tree_val->inventory_id;
				
				$sql_shipping_repl="select shipping_charge_on_replacement.*,policy_items.* from shipping_charge_on_replacement,policy_items where policy_items.policy_uid=shipping_charge_on_replacement.shipping_charge_on_replacement_policy_uid and policy_items.pcat_id='$pcat_id'";
				
				$result_repl = $this->db->query($sql_shipping_repl);
				
				$shipping_repl=$result_repl->result_array();
				
				//print_r($shipping_repl);
				$result_data;
				
				foreach($shipping_repl as $shipping_repl_val){
					//echo ($shipping_repl_val->id).'||';
					$result_data="";
					if(isset($shipping_repl_val['inv_id']) && $shipping_repl_val['inv_id'] == $inventory_id) {

						$result_data=$shipping_repl_val;
						
					}elseif(isset($shipping_repl_val['product_id']) && $shipping_repl_val['product_id'] == $product_id && $shipping_repl_val['inv_id']==""){

						$result_data=$shipping_repl_val;

					}elseif(isset($shipping_repl_val['brand_id']) && $shipping_repl_val['brand_id'] == $brand_id  && $shipping_repl_val['product_id'] == "" && $shipping_repl_val['inv_id']==""){

						$result_data=$shipping_repl_val;

					}elseif(isset($shipping_repl_val['subcat_id']) && $shipping_repl_val['subcat_id'] == $subcat_id && $shipping_repl_val['brand_id'] == ""  && $shipping_repl_val['product_id'] == "" && $shipping_repl_val['inv_id']==""){
					
						$result_data=$shipping_repl_val;
			
					}elseif(isset($shipping_repl_val['cat_id']) && $shipping_repl_val['cat_id'] == $cat_id && $shipping_repl_val['subcat_id'] == "" && $shipping_repl_val['brand_id'] == ""  && $shipping_repl_val['product_id'] == "" && $shipping_repl_val['inv_id']==""){

						$result_data=$shipping_repl_val;

					}elseif(isset($shipping_repl_val['pcat_id']) && $shipping_repl_val['pcat_id'] == $pcat_id && $shipping_repl_val['cat_id'] == "" && $shipping_repl_val['subcat_id'] == "" && $shipping_repl_val['brand_id'] == ""  && $shipping_repl_val['product_id'] == "" && $shipping_repl_val['inv_id']==""){

						$result_data=$shipping_repl_val;

					}elseif($shipping_repl_val['pcat_id'] == "" && $shipping_repl_val['cat_id'] == "" && $shipping_repl_val['subcat_id'] == "" && $shipping_repl_val['brand_id'] == ""  && $shipping_repl_val['product_id'] == "" && $shipping_repl_val['inv_id']==""){
						
						$result_data=$shipping_repl_val;
						
					}else{
						//no policy//admin has to pay the shipping charge
						//$result_data=array("empty");
					}
					($result_data!='') ? $entire[]=$result_data:'';//should execute only one time
					
				}
				//print_r($entire);
			}
			return $entire;
		}
		public function update_transaction_details_of_invoice_offers_for_surprise_gifts($wallet_transaction_id,$transaction_timestamp,$order_id){
			
			$sql="update invoices_offers set status_of_refund_for_surprise_gifts='refunded', 	transaction_timestamp_for_surprise_gifts=now(),wallet_transaction_id_for_surprise_gifts='$wallet_transaction_id' where order_id='$order_id' and customer_response_for_surprise_gifts='accept'";
			$result=$this->db->query($sql);
			return $result;
		}
		public function update_transaction_details_of_invoice_offers_for_cashback($wallet_transaction_id,$transaction_timestamp,$order_id){
			
			$sql="update invoices_offers set status_of_refund_for_cashback='refunded',transaction_timestamp_for_cashback=now(),wallet_transaction_id_for_cashback='$wallet_transaction_id' where order_id='$order_id' and customer_response_for_invoice_cashback='accept'";
			$result=$this->db->query($sql);
			return $result;
		}
		public function invoice_offers_request_for_refund_processing($params,$fg){
			
			$status_of_refund=$params["status_of_refund"];
			
			//define index of column
			$columns = array(
				0 =>'invoices_offers.id', 
				1 => 'invoices_offers.order_id',
				2 => 'invoices_offers.timestamp',
				3 => 'invoices_offers.timestamp',
				4 => 'invoices_offers.timestamp'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			if($status_of_refund=="reject"){
				
				$sql = "select * from invoices_offers where customer_response_for_invoice_cashback='reject' or customer_response_for_surprise_gifts='reject' ";
				 
			}else{
				$sql = "select * from invoices_offers where (status_of_refund_for_surprise_gifts='$status_of_refund' and customer_response_for_surprise_gifts='accept') or (status_of_refund_for_cashback='$status_of_refund' and customer_response_for_invoice_cashback='accept')";
			}
			
			// check search value exist
			if( !empty($params['search']['value']) ){
				$where .=" and ";
				$where .=" ( invoices_offers.order_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR invoices_offers.customer_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR invoices_offers.id LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		public function get_detals_of_wallet_transation($wallet_transaction_id){
			$sql="select * from wallet_transaction where id='$wallet_transaction_id'";
			$result=$this->db->query($sql);
			
			return $result->row();
			
		}
		public function order_item_offers_request_for_refund_processing($params,$fg){
			
			$status_of_refund=$params["status_of_refund"];
			
			//define index of column
			$columns = array(
				0 =>'order_item_offers.id', 
				1 => 'order_item_offers.order_id',
				2 => 'order_item_offers.promotion_quote',
				3 => 'order_item_offers.order_item_id',
				4 => 'order_item_offers.timestamp'
			);

			$where = $sqlTot = $sqlRec = "";

			// getting total number records without any search
			if($status_of_refund=="reject"){
					
				$sql = "select * from order_item_offers where customer_response='reject'";
			}else{
				$sql = "select * from order_item_offers where status_of_refund='$status_of_refund' and customer_response=''";
			}
			
			// check search value exist
			if( !empty($params['search']['value']) ){
				$where .=" and ";
				$where .=" ( order_item_offers.order_id LIKE '%".$params['search']['value']."%' ";    
				$where .=" OR order_item_offers.customer_id LIKE '%".$params['search']['value']."%' ";
				$where .=" OR order_item_offers.order_item_id LIKE '%".$params['search']['value']."%' )";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;
			//concatenate search sql if value exist
			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			
			$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
			
			
			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
		}
		public function get_details_of_order_item($order_item_id){
    
			$sql="select * from order_item_offers where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		public function update_transaction_details_of_order_item_offers($wallet_transaction_id,$transaction_timestamp,$order_id,$order_item_id){
			
			$sql="update order_item_offers set status_of_refund='refunded',transaction_timestamp='$transaction_timestamp',wallet_transaction_id='$wallet_transaction_id' where order_id='$order_id' and order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result;
		}
		public function update_invoice_numbers_in_invoices_offers($order_id,$type_of_order,$invoice_number){
			if($type_of_order=="cashback"){
				$sql="update invoices_offers set invoice_number_for_cashback='$invoice_number' where order_id='$order_id'";
			}else{
				$sql="update invoices_offers set invoice_number_for_surprise_gifts='$invoice_number' where order_id='$order_id'";
			}	
			$result=$this->db->query($sql);
			return $result;
		}
		public function notify_return_replacement_unset(){
			$sql="update replacement_desired set notify_replacement='0' order by replacement_desired_id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_return_refund_unset(){
			$sql="update returns_desired set notify_refund='0' order by return_desired_id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_return_replacement_reject_unset(){
			$sql="update order_replacement_decision set notify_replacement_reject='0' order by id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_return_replacement_accept_unset(){
			$sql="update order_replacement_decision set notify_replacement_accept='0' order by id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function returns_replacement_request_orders_ready_to_replace($order_item_id){
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.payment_status,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.order_replacement_decision_status,replacement_desired.type_of_replacement_cancel from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and ((replacement_desired.order_replacement_decision_status='accept' and ((orders_status.order_replacement_pickup='1' and orders_status.initiate_replacement_pickup!='0' and orders_status.initiate_replacement_pickup!='1') or (orders_status.order_replacement_pickup='0' and orders_status.initiate_replacement_pickup='1') or (orders_status.order_replacement_pickup='0' and orders_status.initiate_replacement_pickup='0')) and replacement_desired.status_of_refund!='yes' and replacement_desired.order_replacement_decision_customer_status!='cancel') or  (replacement_desired.order_replacement_decision_status='replace' and replacement_desired.order_replacement_decision_customer_status!='cancel'))";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function returns_replacement_request_orders_partial_success($order_item_id){
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.payment_status,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.order_replacement_decision_status,replacement_desired.type_of_replacement_cancel from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and (replacement_desired.order_replacement_decision_status='replaced' or replacement_desired.order_replacement_decision_status='refund' or replacement_desired.order_replacement_decision_status='refunded')";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function returns_replacement_request_orders_full_success($order_item_id){
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.payment_status,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.order_replacement_decision_status,replacement_desired.type_of_replacement_cancel from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and (replacement_desired.order_replacement_decision_status='replaced' or replacement_desired.order_replacement_decision_status='refund' or replacement_desired.order_replacement_decision_status='refunded')";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function returns_replacement_request_orders_cancel($order_item_id){
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.prev_order_item_id,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_shipped,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,orders_status.order_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.payment_status,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.order_replacement_decision_status,replacement_desired.move_to_refund_flag,replacement_desired.order_replacement_decision_refund_status,replacement_desired.shipping_charge_applied_cancels_chk,replacement_desired.return_shipping_concession_cancels_chk,replacement_desired.type_of_replacement_cancel,order_replacement_decision.sub_reason_return_id,admin_replacement_reason.pickup_identifier from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return,order_replacement_decision,admin_replacement_reason where order_replacement_decision.order_item_id=replacement_desired.order_item_id and history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and admin_replacement_reason.id=order_replacement_decision.sub_reason_return_id and (replacement_desired.order_replacement_decision_customer_status='cancel' and (replacement_desired.order_replacement_decision_refund_status='need_to_refund' or replacement_desired.order_replacement_decision_refund_status='refund' or replacement_desired.order_replacement_decision_refund_status='refunded'))";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function get_unread_return_msg_replacement_ready_to_replace($return_order_id){
			$sql="select count(*) as count from replacement_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and (admin_action='accept' or admin_action='replace') and (customer_action='replace' or customer_action='accept' or customer_action='replace_cancel' or customer_action='paybalance' or customer_action='initiate_replace')";
			//ready_to _replace
			
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		public function get_unread_return_msg_replacement_partial_success($return_order_id){
			$sql="select count(*) as count from replacement_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and (admin_action='replaced' or admin_action='refund' or admin_action='refunded')";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		public function get_unread_return_msg_replacement_full_success($return_order_id){
			$sql="select count(*) as count from replacement_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and (admin_action='replaced' or admin_action='refund' or admin_action='refunded')";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		public function notify_replacement_ready_to_replace_unset(){
			$sql="update order_replacement_decision set notify_ready_for_replace='0' order by id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_return_replacement_cancel_unset(){
			$sql="update order_replacement_decision set notify_replacement_cancel='0' order by id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_return_replacement_stock_unset(){
			$sql="update return_stock_notification set notify_stock_notification='0' order by return_stock_notification_id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_return_replacement_stock_unset_yes_no(){
			$sql="update return_stock_notification set notify_stock_yes_no_decision='0' order by return_stock_notification_id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function get_unread_replacement_msg_return_cancel($return_order_id){
			$sql="select count(*) as count from replacement_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='accept' and customer_action='replace_cancel'";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		public function notify_return_refund_reject_unset(){
			$sql="update order_return_decision set notify_return_reject='0' order by id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_return_refund_accept_unset(){
			$sql="update order_return_decision set notify_return_accept='0' order by id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function get_unread_return_msg_need_to_refund($return_order_id){
			$sql="select count(*) as count from return_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='accept' and customer_action='initiate_refund'";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		public function returns_refund_need_to_refund($order_item_id){
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id,invoices_offers.* from completed_orders,invoices_offers,products,returns_desired,orders_status,shipping_address,reason_return where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and invoices_offers.order_id=completed_orders.order_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='accept' and returns_desired.order_return_decision_customer_decision='accept' and ((orders_status.order_return_pickup='1' and orders_status.initiate_return_pickup!='0' and orders_status.initiate_return_pickup!='1') or (orders_status.order_return_pickup='0' and orders_status.initiate_return_pickup='1') or (orders_status.order_return_pickup='0' and orders_status.initiate_return_pickup='0'))";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function get_unread_return_refund_issue_inprocess($return_order_id){
			$sql="select count(*) as count from return_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='refund' and customer_action='initiate_refund'";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		
		public function returns_refund_issue_inprocess($order_item_id){
			$sql = "select completed_orders.sku_id,completed_orders.inventory_id,products.product_name,returns_desired.order_item_id,completed_orders.order_id,completed_orders.quantity,completed_orders.product_price,completed_orders.shipping_charge,completed_orders.grandtotal,orders_status.order_delivered_timestamp,completed_orders.logistics_name,completed_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,completed_orders.customer_id,returns_desired.allow_update_bank_details,if(returns_desired.allow_update_bank_details!='','Failed Transaction','Fresh Transaction') as type_of_transaction,returns_desired.refund_method,invoices_offers.* from completed_orders,invoices_offers,products,returns_desired,orders_status,shipping_address,reason_return where completed_orders.order_item_id=returns_desired.order_item_id and products.product_id=completed_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and invoices_offers.order_id=completed_orders.order_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='refund'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function get_unread_return_refund_full_success($return_order_id){
			$sql="select count(*) as count from return_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and (admin_action='refunded' or admin_action='refund_again')  and (customer_action='initiate_refund' or customer_action='initiate_refund_again')";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		public function get_unread_return_msg_refund_success_partial_refund($return_order_id){
			$sql="select count(*) as count from  return_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and (admin_action='refunded' or admin_action='refund_again') and (customer_action='initiate_refund' or customer_action='accept' or customer_action='initiate_refund_again')";
			
			//$sql="select count(*) as count from return_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='refund_again' and customer_action='initiate_refund_again'";
			
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		public function returns_refund_partial_and_full_success($order_item_id){
			$sql = "select history_orders.grandtotal as grandtotal_of_quantity_purchased,history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,returns_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,history_orders.cash_back_value as purchased_cash_back_value,orders_status.order_delivered_timestamp,orders_status.initiate_return_pickup,orders_status.initiate_return_pickup_again,orders_status.order_return_pickup,orders_status.order_return_pickup_again,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,history_orders.customer_id,returns_desired.transaction_id,returns_desired.transaction_date,returns_desired.transaction_time,invoices_offers.* from history_orders,invoices_offers,products,returns_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=returns_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and invoices_offers.order_id=history_orders.order_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='refunded'";
			$query=$this->db->query($sql);
			return $query->row();
		}
		
		public function get_unread_return_msg_refund_again($return_order_id){
			$sql="select count(*) as count from return_replies where user_type='customer' and return_order_id='$return_order_id' and is_read='0' and admin_action='refund_again' and customer_action='initiate_refund_again'";
			$query=$this->db->query($sql);
			return $query->row()->count;
		}
		
		public function returns_refund_orders_again($order_item_id){
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,returns_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,history_orders.customer_id,sub_returns_desired.*,invoices_offers.*,returns_desired.order_item_invoice_discount_value_each from history_orders,invoices_offers,products,returns_desired,orders_status,shipping_address,reason_return,sub_returns_desired where history_orders.order_item_id=returns_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and invoices_offers.order_id=history_orders.order_id and reason_return.reason_id=returns_desired.reason_return_id and sub_returns_desired.order_item_id=orders_status.order_item_id and (sub_returns_desired.admin_action='accepted' or sub_returns_desired.admin_action='refund') and ((orders_status.order_return_pickup_again='1' and trim(orders_status.initiate_return_pickup_again)=trim('')) or (orders_status.order_return_pickup_again='0' and orders_status.initiate_return_pickup_again='1') or (orders_status.order_return_pickup_again='0' and orders_status.initiate_return_pickup_again='0') or (orders_status.order_return_pickup_again='0' and trim(orders_status.initiate_return_pickup_again)=trim('') and sub_returns_desired.quantity_requested_admin=0))";
			$query=$this->db->query($sql);
			return $query->row();
		}
		public function notify_return_refund_again_unset(){
			$sql="update sub_returns_desired set notify_refund_again='0' order by sub_returns_desired_id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_refund_account_unset(){
			$sql="update return_refund set notify_refund_account='0' order by return_refund_id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_replacement_account_unset(){
			$sql="update order_replacement_decision set notify_replacement_account='0' order by id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_return_cancel_unset(){
			$sql="update returns_desired set notify_cancelled_refund='0' order by return_desired_id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_return_cancel_unset_rep(){
			$sql="update replacement_desired set notify_cancelled_replacement='0' order by replacement_desired_id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_return_cancel_unset_stock(){
			$sql="update return_stock_notification set notify_cancelled_return_stock='0' order by return_stock_notification_id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_return_promotion_unset(){
			$sql="update order_item_offers set notify_surprise_qty='0' order by id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_return_promotion_invoice_unset(){
			$sql="update invoices_offers set notify_invoice_promotion='0' order by id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_return_refund_failure_unset(){
			$sql="update returns_desired set notify_failure_refund='0' order by return_desired_id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_refund_account_incoming_again_unset(){
			$sql="update  sub_returns_desired set notify_refund_again_full='0' order by sub_returns_desired_id desc limit 1000";
			$result=$this->db->query($sql);
		}
		public function notify_to_customer($return_order_id,$customer_id,$order_item_id,$order_id,$view_status,$view_timestamp){
			
			// set unique keys to the table
			//INSERT INTO table (id, name, age) VALUES(1, "A", 19) ON DUPLICATE KEY UPDATE   name="A", age=19

			$sql="INSERT INTO refund_notification (return_order_id,customer_id,order_item_id,$view_status,$view_timestamp) VALUES('$return_order_id','$customer_id', '$order_item_id',1,now()) ON DUPLICATE KEY UPDATE  $view_status='1', $view_timestamp=now()";
			$result=$this->db->query($sql);
			return $result;
		}
		public function notify_to_customer_update_label($order_item_id,$view_status,$view_timestamp){
			$sql="update refund_notification set $view_status=1,$view_timestamp=now() where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
		}
		public function notify_to_customer_repl($return_order_id,$customer_id,$order_item_id,$view_status,$view_timestamp){
			
			// set unique keys to the table
			//INSERT INTO table (id, name, age) VALUES(1, "A", 19) ON DUPLICATE KEY UPDATE   name="A", age=19

			$sql="INSERT INTO replacement_notification (return_order_id,customer_id,order_item_id,$view_status,$view_timestamp) VALUES('$return_order_id','$customer_id', '$order_item_id',1,now()) ON DUPLICATE KEY UPDATE  $view_status='1', $view_timestamp=now()";
			$result=$this->db->query($sql);
			return $result;
		}
		public function notify_to_customer_update_label_repl($order_item_id,$view_status,$view_timestamp){
			$sql="update replacement_notification set $view_status=1,$view_timestamp=now() where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
		}
		public function get_prev_accepted_quantity($order_item_id){
			$sql="select quantity from order_return_decision where order_item_id=$order_item_id";
			$result=$this->db->query($sql);
			return $result->row();
			
		}
		public function update_inv_when_stock_is_available($replacement_with_inventory_id,$stock_to_be_updated){
			//replacement
			$sql_inv="update inventory set stock = (stock-$stock_to_be_updated) where id='$replacement_with_inventory_id'; ";
		
			$result=$this->db->query($sql_inv);
			return $result;	
		}
		public function add_inv_when_stock_request_is_cancelled($replacement_with_inventory_id,$stock_to_be_updated){
			//replacement
			$sql_inv="update inventory set stock = (stock+$stock_to_be_updated) where id='$replacement_with_inventory_id'; ";
		
			$result=$this->db->query($sql_inv);
			return $result;	
		}
		
		public function get_rep_details($order_item_id,$order_id){
			
			$sql = "select orders_status.order_replacement_pickup,replaced_orders.*,replaced_orders.timestamp,replaced_orders.grandtotal as grandtotal_of_quantity_purchased,replaced_orders.sku_id,replaced_orders.inventory_id,products.product_name,replaced_orders.order_item_id,replaced_orders.prev_order_item_id,replaced_orders.order_id,replaced_orders.quantity,replaced_orders.product_price,replaced_orders.shipping_charge,replaced_orders.grandtotal,orders_status.order_delivered_timestamp,replaced_orders.logistics_name,replaced_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,replaced_orders.customer_id,replacement_desired.transaction_id,replacement_desired.transaction_date,replacement_desired.transaction_time,orders_status.order_replacement_pickup,orders_status.initiate_replacement_pickup,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,replacement_desired.order_replacement_decision_status,replacement_desired.payment_status,invoices_offers.* from replaced_orders,invoices_offers,products,replacement_desired,orders_status,shipping_address,reason_return where products.product_id=replaced_orders.product_id and orders_status.order_item_id=replaced_orders.prev_order_item_id and
			replacement_desired.order_item_id=orders_status.order_item_id and
			replaced_orders.shipping_address_id=shipping_address.shipping_address_id and invoices_offers.order_id=replaced_orders.order_id and reason_return.reason_id=replacement_desired.reason_return_id and (replacement_desired.order_replacement_decision_status='replaced' or replacement_desired.order_replacement_decision_status='refund' or replacement_desired.order_replacement_decision_status='refunded') and replaced_orders.order_id='$order_id' and replaced_orders.order_item_id='$order_item_id'";
			
			$queryRecords=$this->db->query($sql);
			return $queryRecords->result();
		}
		
		public function get_ref_details($order_item_id,$order_id){
			
			$sql = "select orders_status.order_return_pickup,orders_status.order_return_pickup_again,orders_status.initiate_return_pickup,orders_status.initiate_return_pickup_again,history_orders.grandtotal as grandtotal_of_quantity_purchased,history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,returns_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,history_orders.cash_back_value as purchased_cash_back_value,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,history_orders.customer_id,returns_desired.transaction_id,returns_desired.transaction_date,returns_desired.transaction_time,invoices_offers.* from history_orders,invoices_offers,products,returns_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=returns_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and invoices_offers.order_id=history_orders.order_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='refunded' and history_orders.order_id='$order_id' and history_orders.order_item_id='$order_item_id'";
			
			
			$queryRecords=$this->db->query($sql);
			return $queryRecords->result();
		}
		
		public function returns_replacement_stock($order_item_id){
			$sql="select return_stock_notification.*,history_orders.customer_id from return_stock_notification,history_orders where return_stock_notification.order_item_id='$order_item_id' and history_orders.order_item_id=return_stock_notification.order_item_id";
			$queryRecords=$this->db->query($sql);
			return $queryRecords->row();
		}
		public function returns_replacement_desired($order_item_id){
			$sql="select replacement_desired.*,history_orders.customer_id from replacement_desired,history_orders where replacement_desired.order_item_id='$order_item_id' and history_orders.order_item_id=replacement_desired.order_item_id";
			$queryRecords=$this->db->query($sql);
			return $queryRecords->row();
		}
		public function returns_refund_desired($order_item_id){
			$sql="select returns_desired.*,history_orders.customer_id,history_orders.order_id from returns_desired,history_orders where returns_desired.order_item_id='$order_item_id' and history_orders.order_item_id=returns_desired.order_item_id";
			$queryRecords=$this->db->query($sql);
			return $queryRecords->row();
		}
		public function update_replacement_refund_type($refund_method,$refund_bank_id,$order_item_id){
			$sql="update replacement_desired set refund_method='$refund_method',refund_bank_id='$refund_bank_id' where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result;
		}
		public function get_order_id_by_order_item_id($order_item_id){
			$sql="select order_id from history_orders where order_item_id='$order_item_id'";
			$res=$this->db->query($sql);
			return $res->row();
		}
		public function get_order_item_info($orders_status_id,$request_type){
			
			$sql='';
			
			if($request_type=="replacement"){
				
			$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,replacement_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,replacement_desired.return_order_id,replacement_desired.timestamp,replacement_desired.quantity_replacement,orders_status.id as orders_status_id,reason_return.reason_name,replacement_desired.sub_issue_id,replacement_desired.return_reason_comments,history_orders.customer_id,replacement_desired.replacement_with_inventory_id,replacement_desired.replacement_value_each_inventory_id,replacement_desired.balance_amount_paid_by,replacement_desired.refund_method,replacement_desired.refund_bank_id,replacement_desired.paid_by,replacement_desired.amount_paid,replacement_desired.return_value_each_inventory_id,history_orders.ord_addon_products_status,history_orders.ord_addon_products,history_orders.ord_addon_inventories,history_orders.ord_addon_total_price,history_orders.subtotal   from history_orders,products,replacement_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=replacement_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=replacement_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=replacement_desired.reason_return_id and orders_status.id='$orders_status_id'";
			
			}
			
			if($request_type=="refund"){
				
				$sql = "select history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,returns_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,history_orders.customer_id,history_orders.ord_addon_products_status,history_orders.ord_addon_products,history_orders.ord_addon_inventories,history_orders.ord_addon_total_price,history_orders.subtotal   from history_orders,products,returns_desired,orders_status,shipping_address,reason_return where history_orders.order_item_id=returns_desired.order_item_id and products.product_id=history_orders.product_id and orders_status.order_item_id=returns_desired.order_item_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=returns_desired.reason_return_id and orders_status.id='$orders_status_id'";
			}
			
			if($request_type=="stock"){
				
				$sql = "select return_stock_notification.*, DATE_FORMAT(return_stock_notification.timestamp,'%Y-%m-%d') AS 'requested_timestamp',history_orders.sku_id,history_orders.ord_sku_name,history_orders.inventory_id,products.product_name,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.ord_max_selling_price,history_orders.ord_selling_discount,history_orders.shipping_charge,history_orders.grandtotal,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,shipping_address.customer_name,shipping_address.address1,shipping_address.address2,shipping_address.country,shipping_address.state,shipping_address.city,shipping_address.pincode,shipping_address.mobile,orders_status.id as orders_status_id,history_orders.customer_id,orders_status.order_return_pickup,reason_return.reason_name,inventory.stock,inventory.sku_id as requested_sku_id,history_orders.ord_addon_products_status,history_orders.ord_addon_products,history_orders.ord_addon_inventories,history_orders.ord_addon_total_price,history_orders.subtotal from history_orders,products,orders_status,shipping_address,reason_return,return_stock_notification,inventory where return_stock_notification.order_item_id=history_orders.order_item_id and return_stock_notification.order_item_id=orders_status.order_item_id and products.product_id=history_orders.product_id and history_orders.shipping_address_id=shipping_address.shipping_address_id and reason_return.reason_id=return_stock_notification.reason_return_id and inventory.id=return_stock_notification.replacement_with_inventory_id  and orders_status.id='$orders_status_id'";
			}
			
			if(!empty($sql)){
				$query=$this->db->query($sql);
				return $query->row();	
			}
			
		}
		public function get_check_availability_of_free_shipping_promotion(){
			$sql="select to_buy from promotions where to_get='' and get_type='' and promo_active=1 and now() between promo_start_date and promo_end_date";
			$result=$this->db->query($sql);
			if($result->num_rows($result)>0){
				return array("status"=>"yes","free_shipping_amount_limit"=>$result->row_array()["to_buy"]);
			}
			else{
				return array("status"=>"no");
			}
		}
		
		
	}
?>