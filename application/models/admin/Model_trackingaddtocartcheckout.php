<?php
	class Model_trackingaddtocartcheckout extends CI_Model{
		
	public function trackingaddtocartcheckout_processing($params,$fg){
		
		$columns = array( 
			0 =>'tracking_temp.timestamp'
		);
		
		$where = $sqlTot = $sqlRec = "";
		
		$sql = "select tracking_temp.inventory_sku,tracking_temp.inventory_moq,IF(customer.name is null, 'GUEST', customer.name) as customer_name,tracking_temp.page_name,DATE_FORMAT(tracking_temp.timestamp,'%b %d %Y %h:%i %p') as tracking_temp_timestamp_format from  tracking_temp left join customer on tracking_temp.customer_id=customer.id where 1=1 ";
		
		if(!empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( tracking_temp.inventory_sku LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR tracking_temp.inventory_moq LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR IF(customer.name is null, 'GUEST', customer.name) LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR DATE_FORMAT(tracking_temp.timestamp,'%b %d %Y %h:%i %p') LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR tracking_temp.page_name LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}
		
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		//$sqlRec .=  "order by category.timestamp desc";
		//echo $sqlRec;
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		/*if($fg=="get_total_num_recs_initial"){
			$queryTot=$this->db->query($sql);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}*/
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}	
		
	}
	
	public function total_records_in_tracking_temp(){
		$sql = "select tracking_temp.inventory_sku,tracking_temp.inventory_moq,IF(customer.name is null, 'GUEST', customer.name) as customer_name,tracking_temp.page_name,DATE_FORMAT(tracking_temp.timestamp,'%b %d %Y %h:%i %p') as tracking_temp_timestamp_format from  tracking_temp left join customer on tracking_temp.customer_id=customer.id where 1=1 ";
		$queryTot=$this->db->query($sql);
		$totalRecords=$queryTot->num_rows();
		return $totalRecords;
	}
		
}
?>