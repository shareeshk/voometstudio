<?php
class Admin_manage_model extends CI_Model{
	public function add_subadmins_action($name,$email,$password,$mobile,$landline,$gender,$dob,$user_type,$panel_access,$filenamepath){
		
		$date = new DateTime();
		$mt=microtime();
		$salt="guestspriam";
		$rand=mt_rand(1, 1000000);
		$ts=$date->getTimestamp();
		$str=$salt.$rand.$ts.$mt;
		$user_id=md5($str);
		
		$sql="insert into admin_country set name='$name',user_id='$user_id',email='$email',password='$password',mobile='$mobile',landline='$landline',gender='$gender',dob='$dob',user_type='$user_type',panel_access='$panel_access',profile_pic='$filenamepath',active='1'";
		
		$query=$this->db->query($sql);
		if($query){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function check_email_subadmins($val){
		$sql="select count(*) as count from admin_country where email='$val'";
		$valid = true;
		$query=$this->db->query($sql);
		if($query->row()->count>0){
			$valid=false;
		}
		echo json_encode(array('valid' => $valid));
	}
	
	public function check_mobile_subadmins($val){
		$sql="select count(*) as count from admin_country where mobile='$val'";
		$valid = true;
		$query=$this->db->query($sql);
		if($query->row()->count>0){
			$valid=false;
		}
		echo json_encode(array('valid' => $valid));
	}
	
	public function view_subadmins(){
		$sql="select * from admin_country where user_type!='Master Country' order by id desc";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function update_subadmins($subadmin_user_id){
		$sql="select * from admin_country where user_id='$subadmin_user_id'";
		$query=$this->db->query($sql);
		return $query->row();
	}
	public function check_email_subadmins_edit($email,$edit_subadmin_user_id){
		$sql="select count(*) as count from admin_country where email='$email' and user_id !='$edit_subadmin_user_id'";
		$valid = true;
		$query=$this->db->query($sql);
		if($query->row()->count>0){
			$valid=false;
		}
		echo json_encode(array('valid' => $valid));
	}
	public function check_mobile_subadmins_edit($mobile,$edit_subadmin_user_id){
		$sql="select count(*) as count from admin_country where mobile='$mobile' and user_id !='$edit_subadmin_user_id'";
		$valid = true;
		$query=$this->db->query($sql);
		if($query->row()->count>0){
			$valid=false;
		}
		echo json_encode(array('valid' => $valid));
	}
	
	public function edit_subadmins_action($edit_subadmin_user_id,$name,$email,$password,$mobile,$landline,$gender,$dob,$user_type,$panel_access,$filenamepath){
		
		$date = new DateTime();
		$mt=microtime();
		$salt="guestspriam";
		$rand=mt_rand(1, 1000000);
		$ts=$date->getTimestamp();
		$str=$salt.$rand.$ts.$mt;
		$user_id=md5($str);
		if($filenamepath!=""){
			$sql="select profile_pic from admin_country where user_id='$edit_subadmin_user_id'";
			$query=$this->db->query($sql);
			$profile_pic_image=$query->result();
			unlink($profile_pic_image[0]->profile_pic);
		}
		if($filenamepath!=""){
			$profile_pic_sql=",profile_pic='".$filenamepath."'";
		}
		else{
			$profile_pic_sql="";
		}
		$sql="update admin_country set name='$name',user_id='$user_id',email='$email',password='$password',mobile='$mobile',landline='$landline',gender='$gender',dob='$dob',user_type='$user_type',panel_access='$panel_access',active='1'$profile_pic_sql where user_id='$edit_subadmin_user_id'";
		
		$query=$this->db->query($sql);
		if($query){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function delete_subadmins($subadmin_user_id){
		$sql="select profile_pic from admin_country where user_id='$subadmin_user_id'";
		$query=$this->db->query($sql);
		$profile_pic_image=$query->result();
		unlink($profile_pic_image[0]->profile_pic);
		$sql="delete from admin_country where user_id='$subadmin_user_id'";
		$query=$this->db->query($sql);
		if($query){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function get_total_unread_return_msg(){
		$sql="select count(*) as count from  return_replies where user_type='customer' and is_read='0' and admin_action='pending'";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	
	public function get_total_unread_return_msg_awaiting(){
		$sql="select count(*) as count from  return_replies where user_type='customer' and is_read='0' and admin_action='accept' and customer_action='pending'";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	
	public function get_total_unread_return_msg_customer_rejected(){
		$sql="select count(*) as count from  return_replies where user_type='customer' and is_read='0' and admin_action='accept' and customer_action='reject'";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	
	public function get_total_unread_return_msg_customer_accepted(){
		$sql="select count(*) as count from  return_replies where user_type='customer' and is_read='0' and admin_action='accept' and customer_action='accept'";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	
	public function get_total_unread_return_msg_admin_rejected(){
		$sql="select count(*) as count from  return_replies where user_type='customer' and is_read='0' and admin_action='reject'";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	
	public function get_total_unread_return_msg_refund_failure(){
		$sql="select count(*) as count from  return_replies where user_type='customer' and is_read='0' and admin_action='not refunded' and customer_action='not refunded'";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	
	
	/////////////////////////////////////////////
	public function get_total_unread_return_msg_replacement(){
		$sql="select count(*) as count from  replacement_replies where user_type='customer' and is_read='0' and admin_action='pending'";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	public function get_total_unread_return_msg_awaiting_replacement(){
		$sql="select count(*) as count from  replacement_replies where user_type='customer' and is_read='0' and admin_action='accept' and customer_action='pending'";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	
	public function get_total_unread_return_msg_customer_accepted_replacement(){
		$sql="select count(*) as count from  replacement_replies where user_type='customer' and is_read='0' and admin_action='accept' and customer_action='accept'";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	
	public function get_total_unread_return_msg_customer_rejected_replacement(){
		$sql="select count(*) as count from  replacement_replies where user_type='customer' and is_read='0' and admin_action='accept' and customer_action='reject'";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	
	public function get_total_unread_return_msg_balance_clear_replacement(){
		$sql="select count(*) as count from  replacement_replies where user_type='customer' and is_read='0' and admin_action='accept' and customer_action='balanceclear'";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	
	public function get_total_unread_return_msg_admin_rejected_replacement(){
		$sql="select count(*) as count from  replacement_replies where user_type='customer' and is_read='0' and admin_action='reject'";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	
	public function get_total_unread_return_msg_refund_success_partial_refund_replacement(){
		$sql="select count(*) as count from  replacement_replies where user_type='customer' and is_read='0' and (admin_action='replaced' or admin_action='refund' or admin_action='refunded')";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
				return '';
			}else{
				return $query->row()->count;
			}
		}
	
		
	//////////////added////////////////////////
	public function get_total_return_stock_ready_to_replace(){
		$sql="select count(*) as count from  replacement_replies where user_type='customer' and is_read='0' and ((admin_action='accept' and customer_action='replace') or (admin_action='replace' and customer_action='initiate_replace'))";
	    $query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
		
	}
	public function get_total_replacement_return_cancel(){
		$sql="select count(*) as count from  replacement_replies where user_type='customer' and is_read='0' and admin_action='accept' and customer_action='replace_cancel'";
	    $query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
		
	}
	public function get_total_return_stock_notification(){
		$sql="select count(*) as count from  replacement_replies where user_type='customer' and is_read='0' and admin_action='stock' and customer_action='stock'";
	    $query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
		
	}
	public function get_total_unread_return_msg_refund_success_refund_replacement(){
		/*$sql="select count(*) as count from replacement_desired,orders_status,replaced_orders where orders_status.order_item_id=replaced_orders.prev_order_item_id and orders_status.order_item_id=replacement_desired.order_item_id and replacement_desired.order_replacement_decision_status='refunded' and orders_status.order_replacement_pickup='0'";*/
		$sql="select count(*) as count from  replacement_replies where user_type='customer' and is_read='0' and (admin_action='replaced' or admin_action='refund' or admin_action='refunded')";
	    $query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
		
	}
	public function get_total_return_stock_requests_replacement(){
		$sql="select count(*) as count from return_stock_notification where status='active'";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
		
	}
	public function get_total_need_to_be_refund(){
		//$sql="select count(*) as count from returns_desired,orders_status where orders_status.order_item_id=returns_desired.order_item_id and returns_desired.order_return_decision_status='accept' and returns_desired.order_return_decision_customer_decision='accept' and orders_status.order_return_pickup='1'";
		$sql="select count(*) as count from  return_replies where user_type='customer' and is_read='0' and admin_action='accept' and customer_action='initiate_refund'";
		
	    $query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
		
	}
	public function get_total_refund_in_process(){
		//$sql="select count(*) as count from returns_desired,orders_status where orders_status.order_item_id=returns_desired.order_item_id  and returns_desired.order_return_decision_status='refund'";
		$sql="select count(*) as count from  return_replies where user_type='customer' and is_read='0' and admin_action='refund' and customer_action='initiate_refund'";
	    $query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}	
	}
	

	public function get_total_refund_failure(){
		$sql="select count(*) as count from returns_desired,orders_status where orders_status.order_item_id=returns_desired.order_item_id  and returns_desired.order_return_decision_status='not refunded'";
	    $query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}	
	}
	public function get_total_count_of_onhold_orders(){
		$sql="select count(*) as count from orders_status where order_placed=0 and order_confirmed=0 and order_packed=0 and order_shipped=0 and order_delivered=0 and order_cancelled=0";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	
	public function get_total_count_of_order_approved(){
		$sql="select count(*) as count from  orders_status where order_placed=1 and order_confirmed=0 and order_packed=0 and order_shipped=0 and order_delivered=0 and order_cancelled=0";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}

	public function get_total_count_of_order_confirmed(){
		$sql="select count(*) as count from  orders_status where order_placed=1 and order_confirmed=1 and order_packed=0 and order_shipped=0 and order_delivered=0 and order_cancelled=0";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	public function get_total_count_of_order_packed(){
		$sql="select count(*) as count from  orders_status where order_placed=1 and order_confirmed=1 and order_packed=1 and order_shipped=0 and order_delivered=0 and order_cancelled=0";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
		
	}
	public function get_total_count_of_order_shipped(){
		$sql="select count(*) as count from  orders_status where order_placed=1 and order_confirmed=1 and order_packed=1 and order_shipped=1 and order_delivered=0 and order_cancelled=0";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}	
	public function get_total_count_of_order_delivered(){
		$sql="select count(*) as count from  orders_status where order_placed=1 and order_confirmed=1 and order_packed=1 and order_shipped=1 and order_delivered=1 and order_cancelled=0";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	public function get_total_count_of_order_cancelled(){
		$sql="select count(*) as count from  orders_status where order_placed=1 and order_confirmed=1 and order_packed=1 and order_shipped=1 and order_delivered=1 and order_cancelled=1";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	public function get_total_count_cancel_refund_in_process(){
		
		$sql="select count(*) as count from cancelled_orders,cancels,orders_status where cancelled_orders.customer_id=orders_status.customer_id and cancelled_orders.order_item_id=orders_status.order_item_id and cancels.order_item_id=cancelled_orders.order_item_id and cancels.status='refund' and orders_status.order_cancelled='1'";
	   $query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	public function get_total_count_cancel_refund_success(){
		
		$sql="select count(*) as count from cancelled_orders,orders_status,cancels where cancelled_orders.customer_id=orders_status.customer_id and cancelled_orders.order_item_id=orders_status.order_item_id and cancels.order_item_id=cancelled_orders.order_item_id and cancels.status='refunded' and orders_status.order_cancelled='1'";
	    $query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	public function get_total_count_cancel_refund_failure(){
		
		$sql="select count(*) as count from cancelled_orders,orders_status,cancels where cancelled_orders.customer_id=orders_status.customer_id and cancelled_orders.order_item_id=orders_status.order_item_id and cancels.order_item_id=cancelled_orders.order_item_id and cancels.status='not refunded' and orders_status.order_cancelled='1'";
	    $query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	
	public function get_total_unread_return_msg_return_stock_notification(){
		$sql="select count(*) as count from  replacement_replies where user_type='customer' and is_read='0' and admin_action='stock'";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	public function get_total_count_of_notification(){
		
		$sql="select count(*) as count from inventory where stock<=low_stock";
	
		$query=$this->db->query($sql);
		
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	public function get_total_count_of_request_of_out_of_stock(){
		
		$sql="select count(*) as count from inventory,request_for_out_of_stock where inventory.stock>inventory.low_stock and inventory.id=request_for_out_of_stock.inventory_id and inventory.sku_id=request_for_out_of_stock.sku_id and request_for_out_of_stock.mail_sent=0";
	
		$query=$this->db->query($sql);
		
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	public function get_total_of_unread_data_of_vendor_sku(){
		
		$sql="select count(*) as count from vendor_catalog_inventory where notification_read_status='0'";
	
		$query=$this->db->query($sql);
		
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	
	public function get_notify_active(){
		$sql="select notify_active from orders_status where notify_active='1' and order_cancelled!='1' order by id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	
	public function get_notify_invoice(){
		$sql="select notify_active from invoices_offers ";
		$sql.=" LEFT JOIN `invoices_free_items_status` ON invoices_free_items_status.invoices_offers_id=invoices_offers.id ";
		$sql.=" where notify_active='1' and invoices_offers.id!=NULL ";
		$sql.=" order by invoices_offers.id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_cancel_order(){	
		$sql="select notify_cancel from cancelled_orders where notify_cancel='1' order by order_item_id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_return_replacement(){
		$sql="select notify_replacement from replacement_desired where notify_replacement='1' order by replacement_desired_id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_return_refund(){
		$sql="select notify_refund from returns_desired where notify_refund='1' order by return_desired_id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_replacement_reject(){
		$sql="select notify_replacement_reject from order_replacement_decision where notify_replacement_reject='1' order by id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_replacement_accept(){
		$sql="select notify_replacement_accept from order_replacement_decision where notify_replacement_accept='1' order by id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_replacement_ready_to_replace(){
		$sql="select notify_ready_for_replace from order_replacement_decision where notify_ready_for_replace='1' order by id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_replacement_cancel(){
		$sql="select notify_replacement_cancel from order_replacement_decision where notify_replacement_cancel='1' order by id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_replacement_stock(){
		$sql="select notify_stock_notification from return_stock_notification where notify_stock_notification='1' order by return_stock_notification_id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_replacement_stock_yes_no_decision(){
		$sql="select notify_stock_yes_no_decision from return_stock_notification where notify_stock_yes_no_decision=1 order by return_stock_notification_id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function returns_replacement_request_orders_refund(){
		$sql = "select replaced_orders.quantity,replaced_orders.prev_order_item_id,replacement_desired.return_order_id,replacement_replies.* from replacement_replies,replaced_orders,replacement_desired,orders_status,reason_return where orders_status.order_item_id=replaced_orders.prev_order_item_id and
			replacement_desired.order_item_id=orders_status.order_item_id and reason_return.reason_id=replacement_desired.reason_return_id and (replacement_desired.order_replacement_decision_status='replaced' or replacement_desired.order_replacement_decision_status='refund' or replacement_desired.order_replacement_decision_status='refunded') and replacement_replies.return_order_id=replacement_desired.return_order_id and replacement_replies.is_read=0";
			
		$query=$this->db->query($sql);
		
		return $query->result();
		/*if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}*/
	}
		public function get_data_from_history_orders($order_item_id){
			$sql="select * from history_orders where order_item_id='$order_item_id'";
			$result=$this->db->query($sql);
			return $result->row();
		}
		
	public function get_notify_return_reject(){
		$sql="select notify_return_reject from order_return_decision where notify_return_reject='1' order by id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_refund_accept(){
		$sql="select notify_return_accept from order_return_decision where notify_return_accept='1' order by id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_refund_again(){
		$sql="select notify_refund_again from sub_returns_desired where notify_refund_again='1' order by sub_returns_desired_id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function returns_refund_request_orders_refund_success(){
			
		$sql = "select orders_status.order_return_pickup,orders_status.order_return_pickup_again,orders_status.initiate_return_pickup,orders_status.initiate_return_pickup_again,history_orders.grandtotal as grandtotal_of_quantity_purchased,history_orders.sku_id,history_orders.inventory_id,returns_desired.order_item_id,history_orders.order_id,history_orders.quantity,history_orders.product_price,history_orders.shipping_charge,history_orders.grandtotal,history_orders.cash_back_value as purchased_cash_back_value,orders_status.order_delivered_timestamp,history_orders.logistics_name,history_orders.tracking_number,returns_desired.return_order_id,returns_desired.timestamp,returns_desired.quantity_refund,orders_status.id as orders_status_id,reason_return.reason_name,returns_desired.sub_issue_id,returns_desired.return_reason_comments,history_orders.customer_id,returns_desired.transaction_id,returns_desired.transaction_date,returns_desired.transaction_time,invoices_offers.* from history_orders,invoices_offers,returns_desired,orders_status,reason_return,return_replies where history_orders.order_item_id=returns_desired.order_item_id and orders_status.order_item_id=returns_desired.order_item_id and invoices_offers.order_id=history_orders.order_id and reason_return.reason_id=returns_desired.reason_return_id and returns_desired.order_return_decision_status='refunded' and ((orders_status.order_return_pickup='0' and orders_status.initiate_return_pickup!='0' and orders_status.initiate_return_pickup!='1') or (orders_status.order_return_pickup='1' and trim(orders_status.initiate_return_pickup)=trim('')) or (orders_status.order_return_pickup='0' and orders_status.initiate_return_pickup!='0' and orders_status.initiate_return_pickup!='1') or (orders_status.order_return_pickup='1' and orders_status.initiate_return_pickup='1' ) or (orders_status.order_return_pickup='0' and (orders_status.initiate_return_pickup='0' or orders_status.initiate_return_pickup='1' ) ) ) and return_replies.return_order_id=returns_desired.return_order_id and return_replies.is_read=0";
		
		$queryRecords=$this->db->query($sql);
		return $queryRecords->result();
			
	}
	public function return_refund_request_again_need_to_refund_details($order_item_id){
		$sql="select * from sub_returns_desired where order_item_id='$order_item_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	public function sub_return_refund_table_data_refunded_by_order_item_id($order_item_id){
		$sql="select sub_return_refund.* from sub_return_refund,sub_returns_desired where sub_return_refund.order_item_id='$order_item_id' and sub_return_refund.sub_returns_desired_id=sub_returns_desired.sub_returns_desired_id and sub_returns_desired.admin_action='refunded'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	public function get_grandtotal_of_return_refund_success($order_item_id){
		$sql="select sum(item_with_deductions_concession) as item_with_deductions_concession,sum(shipping_with_deductions_concession) as shipping_with_deductions_concession from return_refund where order_item_id='$order_item_id' union select sum(item_with_deductions_concession) as item_with_deductions_concession,sum(shipping_with_deductions_concession) as shipping_with_deductions_concession from sub_return_refund where order_item_id='$order_item_id' ";
		$query=$this->db->query($sql);
		return $query->row()->item_with_deductions_concession+$query->row()->shipping_with_deductions_concession;
	}
	public function get_order_return_decision_data($order_item_id){
		
		$sql="select order_return_decision.*,invoices_offers.*,history_orders.grandtotal from order_return_decision,invoices_offers,history_orders where order_return_decision.order_item_id='$order_item_id' and history_orders.order_item_id=order_return_decision.order_item_id and history_orders.order_id=invoices_offers.order_id";
		$query=$this->db->query($sql);
		return $query->row();
	}
	public function get_total_unread_return_msg_refund_success_partial_refund(){
		$sql="select count(*) as count from  return_replies where user_type='customer' and is_read='0' and ((admin_action='refunded' and customer_action='initiate_refund') or (admin_action='refund_again' and customer_action='initiate_refund_again'))";
		$query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}
	}
	public function get_total_refund_success_full_refund(){
		//$sql="select count(*) as count from returns_desired,orders_status where orders_status.order_item_id=returns_desired.order_item_id  and returns_desired.order_return_decision_status='refunded'";
		$sql="select count(*) as count from  return_replies where user_type='customer' and is_read='0' and admin_action='refunded' and customer_action='initiate_refund'";
	    $query=$this->db->query($sql);
		if($query->row()->count==0){
			return '';
		}else{
			return $query->row()->count;
		}	
	}
	public function get_notify_refund_account(){
		$sql="select notify_refund_account from return_refund where notify_refund_account='1' order by return_refund_id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_cancel_account(){
		$sql="select notify_cancel_account from cancel_refund where notify_cancel_account='1' order by cancel_refund_id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_replacement_account(){
		$sql="select notify_replacement_account from order_replacement_decision where notify_replacement_account='1' order by id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_bank_transfer_account(){
		$sql="select notify_bank_transfer from  wallet_transaction_bank where notify_bank_transfer='1' order by id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_return_cancel(){
		$sql="select notify_cancelled_refund from returns_desired where notify_cancelled_refund='1' order by return_desired_id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_return_cancel_rep(){
		//$sql="select * from replacement_desired,orders_status where replacement_desired.	notify_cancelled_replacement='1' and replacement_desired.order_replacement_decision_customer_status='cancel' and (replacement_desired.order_replacement_decision_refund_status!='refunded' and orders_status.order_replacement_pickup='0' and ((replacement_desired.payment_status!='paid' and replacement_desired.paid_by='customer') or (replacement_desired.paid_by='admin') or (replacement_desired.paid_by='')))";
		$sql="select * from replacement_desired where notify_cancelled_replacement=1";
		
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_return_cancel_stock_notification(){
		$sql="select notify_cancelled_return_stock from return_stock_notification where notify_cancelled_return_stock='1' order by return_stock_notification_id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_promotion_account(){
		$sql="select notify_surprise_qty from  order_item_offers where notify_surprise_qty='1' order by id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_promotion_account_invoice(){
		$sql="select notify_invoice_promotion from  invoices_offers where notify_invoice_promotion='1' order by id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_refund_failure(){
		$sql="select notify_failure_refund from  returns_desired where notify_failure_refund='1' order by return_desired_id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function get_notify_refund_account_incoming_again(){
		$sql="select notify_refund_again_full from sub_returns_desired where notify_refund_again_full='1' order by sub_returns_desired_id desc limit 0,1000";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function request_quote_processing($params,$fg){
			
		$var_to =$params['to_date'];
		$var_from =$params['from_date'];		
		$vendor_id =$params['vendor_id'];		

		$user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");
		
		//define index of column
		$columns = array( 
				0 =>'request_a_quote.rq_id', 
				1 => 'request_a_quote.rq_id',
				2 => 'request_a_quote.rq_id',
				3 => 'request_a_quote.rq_id',
				4 => 'request_a_quote.rq_id'
		);

		$where = $sqlTot = $sqlRec = "";

		// getting total number records without any search
		$sql = "select request_a_quote.* from request_a_quote
				 ";
		
	// check search value exist
			if($user_type=="vendor"){
					//$where .=" AND ";
					$where .=" find_in_set('$vendor_id',request_a_quote.vendor_id_list) <> 0 ";
			}else{
				if($vendor_id!=''){
					$where .=" find_in_set('$vendor_id',request_a_quote.vendor_id_list) <> 0";
				}
			}
	if( !empty($params['search']['value']) ){  
					if($where!=''){
						$where .=" AND ";
					}
		$where .=" ( request_a_quote.rq_pincode LIKE '%".$params['search']['value']."%' ";
		//$where .=" OR request_a_quote.rq_gsm LIKE '%".$params['search']['value']."%' ";
		//$where .=" OR request_a_quote.rq_size LIKE '%".$params['search']['value']."%' ";
		//$where .=" OR request_a_quote.rq_units LIKE '%".$params['search']['value']."%' ";
		//$where .=" OR request_a_quote.rq_details LIKE '%".$params['search']['value']."%' ";
		//$where .=" OR request_a_quote.rq_payment_terms LIKE '%".$params['search']['value']."%' ";
		$where .=" OR request_a_quote.rq_name LIKE '%".$params['search']['value']."%' ";
		$where .=" OR request_a_quote.rq_unique_id LIKE '%".$params['search']['value']."%' ";
		// $where .=" OR request_a_quote.rq_cat_name LIKE '%".$params['search']['value']."%' ";
		//$where .=" OR request_a_quote.rq_quote_text LIKE '%".$params['search']['value']."%' ";
		//$where .=" OR request_a_quote.rq_subcat_name LIKE '%".$params['search']['value']."%' ";
		$where .=" OR request_a_quote.rq_sku_ids LIKE '%".$params['search']['value']."%' )";
	}
		if(!empty($var_from) && !empty($var_to)){

				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$date = str_replace('/', '-', $var_to);
				$to_date=date('Y-m-d', strtotime($date));

				if($where!=''){
					$where .=" AND ";
				}
				$where .="DATE_FORMAT(request_a_quote.rq_timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(request_a_quote.rq_timestamp,'%Y-%m-%d') <= '$to_date'";

		}elseif(!empty($var_from) && empty($var_to)){
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				if($where!=''){
					$where .=" AND ";
				}
				$where .="DATE_FORMAT(request_a_quote.rq_timestamp,'%Y-%m-%d') >= '$from_date'";
		}
		else{
				$where .="";
		}
		/* for vendor panel */
		
	   
		$where1='';
		if($where !=''){
			$where1 .=" where ".$where;
		}
		/* for vendor panel */

		$sqlTot .= $sql;
		$sqlRec .= $sql;
		//concatenate search sql if value exist
		if(isset($where1) && $where1 != '') {

				$sqlTot .= $where1;
				$sqlRec .= $where1;
		}

		//echo $sqlTot;
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";

		//echo $sqlTot;

		if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
		}
		if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
		}
}
public function become_a_reseller_processing($params,$fg){
			
	$var_to =$params['to_date'];
	$var_from =$params['from_date'];		
	
	//define index of column
	$columns = array( 
			0 =>'become_a_reseller.becomeareseller_id', 
			1 => 'become_a_reseller.becomeareseller_id',
			2 => 'become_a_reseller.becomeareseller_id',
			3 => 'become_a_reseller.becomeareseller_id',
			4 => 'become_a_reseller.becomeareseller_id'
	);

	$where = $sqlTot = $sqlRec = "";

	// getting total number records without any search
	$sql = "select become_a_reseller.* from become_a_reseller
			 ";
	

	if( !empty($params['search']['value']) ){  
					if($where!=''){
						$where .=" AND ";
					}
		$where .=" ( become_a_reseller.becomeareseller_name LIKE '%".$params['search']['value']."%' ";
		$where .=" OR become_a_reseller.becomeareseller_email LIKE '%".$params['search']['value']."%' ";
		$where .=" OR become_a_reseller.becomeareseller_mobile LIKE '%".$params['search']['value']."%' ";
		$where .=" OR become_a_reseller.becomeareseller_type LIKE '%".$params['search']['value']."%' ";
		$where .=" OR become_a_reseller.becomeareseller_city LIKE '%".$params['search']['value']."%' )";
	}
	if(!empty($var_from) && !empty($var_to)){

			$date = str_replace('/', '-', $var_from);
			$from_date=date('Y-m-d', strtotime($date));
			$date = str_replace('/', '-', $var_to);
			$to_date=date('Y-m-d', strtotime($date));

			if($where!=''){
				$where .=" AND ";
			}
			$where .="DATE_FORMAT(become_a_reseller.becomeareseller_timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(become_a_reseller.becomeareseller_timestamp,'%Y-%m-%d') <= '$to_date'";

	}elseif(!empty($var_from) && empty($var_to)){
			$date = str_replace('/', '-', $var_from);
			$from_date=date('Y-m-d', strtotime($date));
			if($where!=''){
				$where .=" AND ";
			}
			$where .="DATE_FORMAT(become_a_reseller.becomeareseller_timestamp,'%Y-%m-%d') >= '$from_date'";
	}
	else{
			$where .="";
	}
	/* for vendor panel */
	
   
	$where1='';
	if($where !=''){
		$where1 .=" where ".$where;
	}
	/* for vendor panel */

	$sqlTot .= $sql;
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where1) && $where1 != '') {

			$sqlTot .= $where1;
			$sqlRec .= $where1;
	}

	//echo $sqlTot;
	$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";

	//echo $sqlTot;

	if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
	}
	if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
	}
}
}
?>