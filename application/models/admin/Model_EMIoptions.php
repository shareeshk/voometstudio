<?php
	class Model_EMIoptions extends CI_Model{
		public function add_EMIoptions($name_emioptions,$planname_emioptions,$emiplan_emioptions_details_json,$emistartvalue_emioptions,$image_emioptions){
			$sql="insert into emioptions set 
					name='$name_emioptions',
					planname='$planname_emioptions',
					emiplan_details_json='$emiplan_emioptions_details_json',
					emistartvalue='$emistartvalue_emioptions',
					image_emioptions_path='$image_emioptions'";
			$query=$this->db->query($sql);
			return $query;
		}
		
		public function EMIoptions_processing($params,$fg){
		
		$columns = array( 
			0 =>'emioptions.timestamp'

		);
		

		
		
		$where = $sqlTot = $sqlRec = "";
		
		
			$sql="select * from emioptions where 1=1 ";

			if(!empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" or planname LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" or emiplan_details_json LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" or emistartvalue LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
				$where .=" or planname LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
			}
			
		
		
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		//$sqlRec .=  "order by category.timestamp desc";
		//echo $sqlRec;
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}	
		
	}
	
	public function delete_emioptions($selected_list){
		
			$sql="select * from emioptions where emioptions_id in ($selected_list) ";
			$query=$this->db->query($sql);
			$result_emioptions=$query->result();
			foreach($result_emioptions as $res){
				unlink($res->image_emioptions_path);
			}
			
			$sql_delete="delete from emioptions where emioptions_id in ($selected_list)";
		
		$flag =$this->db->query($sql_delete);
		return $flag;
	
	}
	
	}
?>