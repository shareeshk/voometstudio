<?php
class Analytics_model extends CI_Model{
	
public function getCustomerRegistrationsAnalytics($from_date,$to_date,$limit_result_val,$particular_year){
   //$sql="select CAST(count(name) as UNSIGNED) as count,DATE_FORMAT(timestamp, '%d/%m/%Y') as month from customer WHERE MONTH(timestamp) >= 1 AND MONTH(timestamp) <= 12 group by MONTH(timestamp)";
   
   if($particular_year==""){
       $sql="select COUNT(DISTINCT id) as count,year(timestamp) as y,(month(timestamp)-1) as m,'1' as d  from customer WHERE MONTH(timestamp) >= 1 AND MONTH(timestamp) <= 12 ";
       
       if($from_date!="" && $to_date!=""){
           $sql.=" and date(customer.timestamp) >= '$from_date' and date(customer.timestamp) <= '$to_date'"; 
        }
        if($from_date!="" && $to_date==""){
           $sql.=" and date(customer.timestamp) >= '$from_date' "; 
        }
        if($from_date=="" && $to_date!=""){
           $sql.=" and  date(customer.timestamp) <= '$to_date'"; 
        }
    
        $sql.="group by y,m";
   }else{
       $sql="SELECT COUNT(u.id) AS count,$particular_year as y,(month(u.timestamp)) as m,'1' as d ,m.month
     FROM (
           SELECT 'Jan' AS MONTH
           UNION SELECT 'Feb' AS MONTH
           UNION SELECT 'Mar' AS MONTH
           UNION SELECT 'Apr' AS MONTH
           UNION SELECT 'May' AS MONTH
           UNION SELECT 'Jun' AS MONTH
           UNION SELECT 'Jul' AS MONTH
           UNION SELECT 'Aug' AS MONTH
           UNION SELECT 'Sep' AS MONTH
           UNION SELECT 'Oct' AS MONTH
           UNION SELECT 'Nov' AS MONTH
           UNION SELECT 'Dec' AS MONTH
                      ) AS m
            LEFT JOIN customer u 
            ON MONTH(STR_TO_DATE(CONCAT(m.month),'%M ')) = MONTH(u.timestamp)
            AND YEAR(u.timestamp)='$particular_year'
            GROUP BY m.month,YEAR(u.timestamp)
            ORDER BY MONTH(STR_TO_DATE(CONCAT(m.month),'%M '))
   
            ";
   }

   
   $result=$this->db->query($sql);
   if($result->num_rows()>0){
       return $result->result_array();
   }
   else{
       return false;
   }
}

public function getCustomerAgeAnalytics($from_date,$to_date,$limit_result_val){
    
    $sql="
   
    SELECT CASE WHEN
    age BETWEEN 18 AND 25 THEN '18 - 25' WHEN age BETWEEN 26 AND 35 THEN '26 - 35' WHEN age BETWEEN 36 AND 45 THEN '36 - 45' WHEN age BETWEEN 46 AND 55 THEN '46 - 55' WHEN age >= 56 THEN 'Over 56' WHEN age IS NULL THEN 'Not Filled In (NULL)'
    END AS age_range,
    COUNT(*) AS COUNT
    FROM
  (
  SELECT TIMESTAMPDIFF
    (YEAR,
    dob,
    CURDATE()) AS age
  FROM
    customer ";
    
    if($from_date!="" && $to_date!=""){
       $sql.=" where date(timestamp) >= '$from_date' and date(timestamp) <= '$to_date'"; 
    }
    if($from_date!="" && $to_date==""){
       $sql.=" where date(timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
       $sql.=" where  date(timestamp) <= '$to_date'"; 
    }
    $sql.=") AS derived
    
  GROUP BY
    age_range
   
   ";
   $result=$this->db->query($sql);
   if($result->num_rows()>0){
       return $result->result_array();
   }
   else{
       return false;
   } 
}

public function getCustomerGenderAnalytics($from_date,$to_date,$limit_result_val){
    $sql="select count(case when gender='male' then 1 end) as Male, count(case when gender='female' then 1 end) as Female from customer";
    
    
   if($from_date!="" && $to_date!=""){
       $sql.=" where date(customer.timestamp) >= '$from_date' and date(customer.timestamp) <= '$to_date'"; 
    }
    if($from_date!="" && $to_date==""){
       $sql.=" where date(customer.timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
       $sql.=" where  date(customer.timestamp) <= '$to_date'"; 
    }
   
   $sql.="";
    
    $result=$this->db->query($sql);
       if($result->num_rows()>0){
           return $result->result_array();
       }
       else{
           return false;
       }
}
public function getCustomerPriceSegmentationAnalytics($from_date,$to_date,$limit_result_val){
    $sql="
   
    SELECT CASE WHEN
    grandtotal < 501 THEN 'less than 500' WHEN
    grandtotal BETWEEN 501 AND 5000 THEN '501 - 5000' WHEN
    grandtotal BETWEEN 5001 AND 15000 THEN '5001 - 15000' WHEN grandtotal BETWEEN 15001 AND 30000 THEN '15001 - 30000' WHEN grandtotal BETWEEN 30001 AND 50000 THEN '30001 - 50000' WHEN grandtotal > 50001 THEN 'Over 50000' WHEN grandtotal IS NULL THEN 'Not Filled In (NULL)'
    END AS grandtotal_range,
    COUNT(*) AS COUNT
    FROM
  (
  SELECT grandtotal
  FROM
    history_orders";  
    
    if($from_date!="" && $to_date!=""){
       $sql.=" where date(history_orders.timestamp) >= '$from_date' and date(history_orders.timestamp) <= '$to_date'"; 
    }
    if($from_date!="" && $to_date==""){
       $sql.=" where date(history_orders.timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
       $sql.=" where  date(history_orders.timestamp) <= '$to_date'"; 
    }
    
    $sql.=") AS derived
    
  GROUP BY
    grandtotal_range
   
   ";
   $result=$this->db->query($sql);
   if($result->num_rows()>0){
       return $result->result_array();
   }
   else{
       return false;
   } 
}
public function get_searched_keywords_with_counts($from_date,$to_date,$limit_result_val,$limit_result_order_val){
    
    $sql="select searched_keyword,number_of_hits from searched_keyword ";
    
    if($from_date!="" && $to_date!=""){
       $sql.=" where date(searched_keyword.timestamp) >= '$from_date' and date(searched_keyword.timestamp) <= '$to_date'"; 
    }
    if($from_date!="" && $to_date==""){
       $sql.=" where date(searched_keyword.timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
       $sql.=" where  date(searched_keyword.timestamp) <= '$to_date' "; 
    }
    if($limit_result_order_val!=""){
        $sql.=" order by number_of_hits $limit_result_order_val ";
    }
    else{
        $sql.=" order by number_of_hits desc ";
    }
    if($limit_result_val!=""){
        $sql.=" limit $limit_result_val";
    }
	
    $result=$this->db->query($sql);
   
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    } 
}


public function get_all_inventory_added($from_date,$to_date,$limit_result_val,$select_level,$particular_year){
    $sql="select parent_category.pcat_id,parent_category.pcat_name,count(*) as counts from parent_category join category on parent_category.pcat_id=category.pcat_id ";
    
    if($from_date!=""||$to_date!=""||$limit_result_val!=""||$select_level!=""|| $particular_year!=""){
         $sql.=" where 1=1  ";
    }
    
    if($from_date!="" && $to_date!=""){
       $sql.=" and  date(parent_category.timestamp) >= '$from_date' and date(parent_category.timestamp) <= '$to_date' "; 
    }
    if($from_date!="" && $to_date==""){
       $sql.=" and date(parent_category.timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
       $sql.=" and  date(parent_category.timestamp) <= '$to_date' "; 
    }

    
    if($select_level!=""){
        $sql.=" and ( ";
        $select_level_arr=explode(",",$select_level);
        $i=0;
        foreach($select_level_arr as $select_levels){
            $sql.=" parent_category.pcat_id='$select_levels' "; 
                if($i<(count($select_level_arr)-1)){
                    $sql.=" or ";
                }
            $i++;
        }
        $sql.=" ) ";
    }
    if($particular_year!=""){
        $sql.=" and year(parent_category.timestamp)='$particular_year' "; 
    }
    

    $sql.=" GROUP BY parent_category.pcat_name ORDER BY parent_category.pcat_id";
    

    
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}

public function get_all_category_arr_by_pcat($from_date,$to_date,$limit_result_val,$pcat){
    $sql="select category.cat_id, category.cat_name,parent_category.pcat_id,parent_category.pcat_name
	from parent_category join category on parent_category.pcat_id=category.pcat_id
	WHERE parent_category.pcat_name='$pcat' GROUP BY category.cat_name ORDER BY parent_category.pcat_id";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}
	

public function get_all_category_arr($from_date,$to_date,$limit_result_val,$select_level,$particular_year){
    $sql="select category.cat_id, category.cat_name ,count(*) as counts from category join subcategory on
	category.cat_id=subcategory.cat_id";
	
    if($from_date!=""||$to_date!=""||$limit_result_val!=""||$select_level!=""|| $particular_year!=""){
         $sql.=" where 1=1  ";
    }
    
    if($from_date!="" && $to_date!=""){
       $sql.=" and  date(category.timestamp) >= '$from_date' and date(category.timestamp) <= '$to_date' "; 
    }
    if($from_date!="" && $to_date==""){
       $sql.=" and date(category.timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
       $sql.=" and  date(category.timestamp) <= '$to_date' "; 
    }
    
    if($select_level!=""){
        $sql.=" and ( ";
        $select_level_arr=explode(",",$select_level);
        $i=0;
        foreach($select_level_arr as $select_levels){
            $sql.=" category.cat_id='$select_levels' "; 
                if($i<(count($select_level_arr)-1)){
                    $sql.=" or ";
                }
            $i++;
        }
        $sql.=" ) ";
    }
	
    if($particular_year!=""){
        $sql.=" and year(category.timestamp)='$particular_year' "; 
    }

	$sql.=" group by category.cat_name order by category.cat_id";

    //echo $sql;

    $result=$this->db->query($sql);
    
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}
public function get_all_brands_arr($from_date,$to_date,$limit_result_val,$select_level,$particular_year){
    $sql="select brands.brand_id, brands.brand_name ,count(*) as counts from brands join products on
    products.brand_id=brands.brand_id ";
    
    
    if($from_date!=""||$to_date!=""||$limit_result_val!=""||$select_level!=""|| $particular_year!=""){
         $sql.=" where 1=1  ";
    }
    
    if($from_date!="" && $to_date!=""){
       $sql.=" and  date(brands.timestamp) >= '$from_date' and date(brands.timestamp) <= '$to_date' "; 
    }
    if($from_date!="" && $to_date==""){
       $sql.=" and date(brands.timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
       $sql.=" and  date(brands.timestamp) <= '$to_date' "; 
    }
    
    if($select_level!=""){
        $sql.=" and ( ";
        $select_level_arr=explode(",",$select_level);
        $i=0;
        foreach($select_level_arr as $select_levels){
            $sql.=" brands.brand_id='$select_level' "; 
                if($i<(count($select_level_arr)-1)){
                    $sql.=" or ";
                }
            $i++;
        }
        $sql.=" ) ";
    }
    
    if($particular_year!=""){
        $sql.=" and year(brands.timestamp)='$particular_year' "; 
    }
    
    
    
    /*if($select_level!=""){
        $sql.=" where brands.subcat_id='$select_level'";
    }*/
    
    $sql.=" group by brands.brand_name order by brands.brand_id";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}


public function get_all_products_arr_by_brand($brand_id){
      $sql="select brands.brand_id, brands.brand_name,products.product_id,products.product_name from brands join products on brands.brand_id=products.brand_id WHERE products.brand_id='$brand_id' GROUP BY products.product_name ORDER BY brands.brand_id";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}
public function getNumOfProductsInInventory($product_id){
    $sql="select * from inventory where product_id='$product_id'";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->num_rows();
    }
    else{
       return 0;
    }
}



public function get_all_products_arr($from_date,$to_date,$limit_result_val,$select_level,$particular_year){
    $sql="select products.product_id, products.product_name ,count(*) as counts from products join inventory on
    products.product_id=inventory.product_id";
    
    if($from_date!=""||$to_date!=""||$limit_result_val!=""||$select_level!=""|| $particular_year!=""){
         $sql.=" where 1=1  ";
    }
    
    if($from_date!="" && $to_date!=""){
       $sql.=" and  date(products.timestamp) >= '$from_date' and date(products.timestamp) <= '$to_date' "; 
    }
    if($from_date!="" && $to_date==""){
       $sql.=" and date(products.timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
       $sql.=" and  date(products.timestamp) <= '$to_date' "; 
    }
    
    if($select_level!=""){
        $sql.=" and ( ";
        $select_level_arr=explode(",",$select_level);
        $i=0;
        foreach($select_level_arr as $select_levels){
            $sql.=" products.product_id='$select_levels' "; 
                if($i<(count($select_level_arr)-1)){
                    $sql.=" or ";
                }
            $i++;
        }
        $sql.=" ) ";
    }
    
    if($particular_year!=""){
        $sql.=" and year(products.timestamp)='$particular_year' "; 
    }
    
    
    /*if($select_level!=""){
        $sql.=" where products.brand_id='$select_level'";
    }*/
    
    $sql.=" group by products.product_name order by products.product_id";
    $result=$this->db->query($sql);
    
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}

public function get_all_inventory_arr_by_product($product_id){
      $sql="select inventory.id, inventory.sku_id,products.product_id,products.product_name from inventory join products on products.product_id=inventory.product_id WHERE inventory.product_id='$product_id' GROUP BY inventory.sku_id ORDER BY products.product_id";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}

public function getNumOfSkusInInventory($sku_id){
    $sql="select stock from inventory where sku_id='$sku_id'";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->row()->stock;
    }
    else{
       return 0;
    }
}

public function getNumOfCategoriesInSubCat($cat_id){
    $sql="select * from subcategory where cat_id='$cat_id'";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->num_rows();
    }
    else{
       return 0;
    }
}

public function get_all_sub_category_arr_by_cat($cat_id){
    $sql="select subcategory.subcat_id, subcategory.subcat_name,category.cat_id,category.cat_name from category join subcategory on category.cat_id=subcategory.cat_id WHERE category.cat_id='$cat_id' GROUP BY subcategory.subcat_name ORDER BY category.cat_id";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}

public function get_all_sub_category_arr($from_date,$to_date,$limit_result_val,$select_level,$particular_year){
    $sql="select subcategory.subcat_id, subcategory.subcat_name ,count(*) as counts from subcategory join brands on
    subcategory.subcat_id=brands.subcat_id ";

    if($from_date!=""||$to_date!=""||$limit_result_val!=""||$select_level!=""|| $particular_year!=""){
         $sql.=" where 1=1  ";
    }
    
    if($from_date!="" && $to_date!=""){
       $sql.=" and  date(subcategory.timestamp) >= '$from_date' and date(subcategory.timestamp) <= '$to_date' "; 
    }
    if($from_date!="" && $to_date==""){
       $sql.=" and date(subcategory.timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
       $sql.=" and  date(subcategory.timestamp) <= '$to_date' "; 
    }
    
    if($select_level!=""){
        $sql.=" and ( ";
        $select_level_arr=explode(",",$select_level);
        $i=0;
        foreach($select_level_arr as $select_levels){
            $sql.=" subcategory.subcat_id='$select_level' "; 
                if($i<(count($select_level_arr)-1)){
                    $sql.=" or ";
                }
            $i++;
        }
        $sql.=" ) ";
    }
    
    if($particular_year!=""){
        $sql.=" and year(subcategory.timestamp)='$particular_year' "; 
    }
    
    
    $sql.=" group by subcategory.subcat_name order by subcategory.subcat_id";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}

public function get_all_barnds_arr_by_subcat($subcat_id){
    $sql="select brands.brand_id, brands.brand_name,subcategory.subcat_id,subcategory.subcat_name from subcategory join brands on brands.subcat_id=subcategory.subcat_id WHERE subcategory.subcat_id='$subcat_id' GROUP BY brands.brand_name ORDER BY subcategory.subcat_id";
    
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
    
}

public function getNumOfBrandsInProducts($brand_id){
    
    $sql="select * from products where brand_id='$brand_id'";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->num_rows();
    }
    else{
       return 0;
    }
}

public function getNumOfSubCatInBrands($subcat_id){
    $sql="select * from brands where subcat_id='$subcat_id'";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->num_rows();
    }
    else{
       return 0;
    }
}

public function get_all_brands_arr_by_subcat($subcat_id){
    $sql="select subcategory.subcat_id, subcategory.subcat_name,brands.brand_id,brands.brand_name from subcategory join brands on subcategory.subcat_id=brands.subcat_id WHERE brands.subcat_id='$subcat_id' GROUP BY brands.brand_name ORDER BY subcategory.subcat_id";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}

public function getAllOptionsForParentCategory(){
    $sql="select pcat_id as id,pcat_name as name from parent_category";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}
public function getAllOptionsForCategory(){
    $sql="select cat_id as id,cat_name as name from category";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}
public function getAllOptionsForSubCategory(){
    $sql="select subcat_id as id,subcat_name as name from subcategory";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}
public function getAllOptionsForBrands(){
    $sql="select brand_id as id ,brand_name as name from brands";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}
public function getAllOptionsForProducts(){
    $sql="select product_id as id,product_name as name from products";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}

public function getallPurchasedProductsForAnalytics($from_date,$to_date,$limit_result_val,$limit_result_order_val,$select_level,$particular_year){
    $sql="select count(*) as count ,products.product_name from products JOIN history_orders on history_orders.product_id=products.product_id ";
    
    if($from_date!=""||$to_date!=""||$limit_result_val!=""||$limit_result_order_val!=""||$select_level!=""|| $particular_year!=""){
         $sql.=" where 1=1  ";
    }
    
    if($from_date!="" && $to_date!=""){
       $sql.=" and  date(history_orders.timestamp) >= '$from_date' and date(history_orders.timestamp) <= '$to_date' "; 
    }
    if($from_date!="" && $to_date==""){
       $sql.=" and date(history_orders.timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
       $sql.=" and  date(history_orders.timestamp) <= '$to_date' "; 
    }
    
    if($particular_year!=""){
       $sql.=" and year(history_orders.timestamp)='$particular_year' ";
    }
    
    if($select_level!=""){
        $sql.=" and ( ";
        $select_level_arr=explode(",",$select_level);
        $i=0;
        foreach($select_level_arr as $select_levels){
            $sql.=" products.pcat_id='$select_level' "; 
                if($i<(count($select_level_arr)-1)){
                    $sql.=" or ";
                }
            $i++;
        }
        $sql.=" ) ";
    }
    
    
    $sql.=" GROUP by products.product_id "; 
    if($limit_result_order_val!=""){
        $sql.=" order by count $limit_result_order_val ";
    }
    else{
        $sql.=" order by count desc ";
    }
    
    if($limit_result_val!=""){
        $sql.=" limit $limit_result_val ";
    }
    
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}


public function getallRatingReportsForAnalytics($from_date,$to_date,$limit_result_val,$limit_result_order_val,$select_level,$particular_year){
    $sql="SELECT rating, COUNT(rating) as Count FROM rated_inventory ";
    
    if($select_level!=""){
        $sql.=" left join inventory on rated_inventory.inventory_id=inventory.id left join products on inventory.product_id=products.product_id ";
    }
    
    if($from_date!=""||$to_date!=""||$limit_result_val!=""||$limit_result_order_val!=""||$select_level!=""|| $particular_year!=""){
         $sql.=" where 1=1  ";
    }
    
    if($from_date!="" && $to_date!=""){
       $sql.=" and  date(rated_inventory.timestamp) >= '$from_date' and date(rated_inventory.timestamp) <= '$to_date' "; 
    }
    if($from_date!="" && $to_date==""){
       $sql.=" and date(rated_inventory.timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
       $sql.=" and  date(rated_inventory.timestamp) <= '$to_date' "; 
    }
    
    if($particular_year!=""){
       $sql.=" and year(rated_inventory.timestamp)='$particular_year' ";
    }
    
    if($select_level!=""){
        $sql.=" and ( ";
        $select_level_arr=explode(",",$select_level);
        $i=0;
        foreach($select_level_arr as $select_levels){
            $sql.=" products.pcat_id='$select_level' "; 
                if($i<(count($select_level_arr)-1)){
                    $sql.=" or ";
                }
            $i++;
        }
        $sql.=" ) ";
    }
    
    
    $sql.=" GROUP BY rating  "; 
    if($limit_result_order_val!=""){
        $sql.=" order by rating $limit_result_order_val ";
    }
    else{
        $sql.=" order by rating desc ";
    }
    
    if($limit_result_val!=""){
        $sql.=" limit $limit_result_val ";
    }
    
    $result=$this->db->query($sql);
//echo $this->db->last_query();
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}

public function getNumberOfPromotions($from_date,$to_date,$limit_result_val,$limit_result_order_val,$particular_year){

       if($particular_year==""){
       $sql="select COUNT(DISTINCT id) as count,year(timestamp) as y,(month(timestamp)-1) as m,'1' as d ,
       
       (case when (promotions.promo_start_date < now() and promotions.promo_end_date > now() and promotions.promo_active=1 and NOT (promotions.promo_active = 0)) 
         THEN
              1 
         ELSE
              0 
         END) as active
         
         ,(case when (promotions.promo_start_date < now() and promotions.promo_end_date > now() and promotions.promo_active=0 and NOT (promotions.promo_active = 1)) 
         THEN
              1 
         ELSE
              0 
         END) as stopped
       
       
       
         from promotions WHERE MONTH(timestamp) >= 1 AND MONTH(timestamp) <= 12 ";
       
       if($from_date!="" && $to_date!=""){
           $sql.=" and date(promotions.timestamp) >= '$from_date' and date(promotions.timestamp) <= '$to_date'"; 
        }
        if($from_date!="" && $to_date==""){
           $sql.=" and date(promotions.timestamp) >= '$from_date' "; 
        }
        if($from_date=="" && $to_date!=""){
           $sql.=" and  date(promotions.timestamp) <= '$to_date'"; 
        }
    
        $sql.=" group by y,m,stopped,active ";
   }else{
       $sql="SELECT COUNT(u.id) AS count,$particular_year as y,(month(u.timestamp)-1) as m,'1' as d ,m.month
       
       ,
       
       (case when (u.promo_start_date < now() and u.promo_end_date > now() and u.promo_active=1 and NOT (u.promo_active = 0)) 
         THEN
              1 
         ELSE
              0 
         END) as active
         
         ,
         
       (case when (u.promo_start_date < now() and u.promo_end_date > now() and u.promo_active=0 and NOT (u.promo_active = 1)) 
         THEN
              1 
         ELSE
              0 
         END) as stopped
       
       
     FROM (
           SELECT 'Jan' AS MONTH
           UNION SELECT 'Feb' AS MONTH
           UNION SELECT 'Mar' AS MONTH
           UNION SELECT 'Apr' AS MONTH
           UNION SELECT 'May' AS MONTH
           UNION SELECT 'Jun' AS MONTH
           UNION SELECT 'Jul' AS MONTH
           UNION SELECT 'Aug' AS MONTH
           UNION SELECT 'Sep' AS MONTH
           UNION SELECT 'Oct' AS MONTH
           UNION SELECT 'Nov' AS MONTH
           UNION SELECT 'Dec' AS MONTH
                      ) AS m
            LEFT JOIN promotions u 
            ON MONTH(STR_TO_DATE(CONCAT(m.month),'%M ')) = MONTH(u.timestamp)
            AND YEAR(u.timestamp)='$particular_year'
            GROUP BY m.month,YEAR(u.timestamp),stopped,active
            ORDER BY MONTH(STR_TO_DATE(CONCAT(m.month),'%M '))
   
            ";
   }

   
   $result=$this->db->query($sql);
   if($result->num_rows()>0){
       return $result->result_array();
   }
   else{
       return false;
   }
}


public function getPromotionsPerMonth($from_date,$to_date,$limit_result_val,$limit_result_order_val,$particular_year){

       $sql="select COUNT(DISTINCT id) as count,year(timestamp) as y,(MONTHNAME(timestamp)) as m,(month(timestamp)) as months,'1' as d
       
       ,
       
       (case when (promotions.promo_start_date < now() and promotions.promo_end_date > now() and promotions.promo_active=1 and NOT (promotions.promo_active = 0)) 
         THEN
              1 
         ELSE
              0 
         END) as active
         
         ,
         
       (case when (promotions.promo_start_date < now() and promotions.promo_end_date > now() and promotions.promo_active=0 and NOT (promotions.promo_active = 1)) 
         THEN
              1 
         ELSE
              0 
         END) as stopped
       
       
         from promotions WHERE MONTH(timestamp) >= 1 AND MONTH(timestamp) <= 12 ";
       
       if($from_date!="" && $to_date!=""){
           $sql.=" and date(promotions.timestamp) >= '$from_date' and date(promotions.timestamp) <= '$to_date'"; 
        }
        if($from_date!="" && $to_date==""){
           $sql.=" and date(promotions.timestamp) >= '$from_date' "; 
        }
        if($from_date=="" && $to_date!=""){
           $sql.=" and  date(promotions.timestamp) <= '$to_date'"; 
        }
    
        $sql.="group by y,m order by months";


   
   $result=$this->db->query($sql);
   if($result->num_rows()>0){
       return $result->result_array();
   }
   else{
       return false;
   }
}

public function get_all_promotion_name_by_year_month($date,$to_date,$limit_result_val,$m,$y){
    $sql="select promo_name,promo_uid  from promotions where year(timestamp)=$y and MONTHNAME(timestamp)='$m' and not( applied_at_invoice=1 )";
    $result=$this->db->query($sql);
   if($result->num_rows()>0){
       return $result->result_array();
   }
   else{
       return false;
   }
}

public function get_number_of_invoices_made_with_promo_uid($promo_ids){
    $sql="SELECT count(*) as counts from `history_orders` where promotion_id_selected='$promo_ids'";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
        return $result->row()->counts;
    }
    else{
        return 0;
    }
}


public function getNumberOfPopularPromotions($from_date,$to_date,$limit_result_val,$limit_result_order_val,$particular_year){
    $sql="select promotions.promo_uid,promotions.promo_name from promotions where promotions.promo_start_date < now() and promotions.promo_end_date > now()";
    
    if($particular_year!=""){
        $sql.=" and year(promotions.timestamp)='$particular_year'";
    }
    if($from_date!="" && $to_date!=""){
       $sql.=" and date(promotions.timestamp) >= '$from_date' and date(promotions.timestamp) <= '$to_date'"; 
    }
    if($from_date!="" && $to_date==""){
       $sql.=" and date(promotions.timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
       $sql.=" and  date(promotions.timestamp) <= '$to_date'"; 
    }
    
    $result=$this->db->query($sql);
    $promotion_popular_data_arr=array();
    if($result->num_rows()>0){
 
       foreach($result->result_array() as $promo_id){
           $promotion_popular_data=array();
           $promotion_popular_data['promo_quote']=$promo_id['promo_name'];
           $promo_ids=$promo_id['promo_uid'];
           $sql="SELECT count(*) as counts from `history_orders` where promotion_id_selected='$promo_ids'";
           $result=$this->db->query($sql);
           $promotion_popular_data['promo_num']=$result->row()->counts;
           
           array_push($promotion_popular_data_arr,$promotion_popular_data);
       }
       
       return $promotion_popular_data_arr;
    }
    else{
       return false;
    }
    /*SELECT count(*) as counts, promotion_quote from `history_orders` where quantity_with_promotion IS NOT NULL group by promotion_id_selected*/
}


public function getAllNewReplacementRequest($from_date,$to_date,$particular_year){

        $sql="
            SELECT COUNT(u.return_order_id) AS counts,$particular_year as y,(month(u.timestamp)) as m,'1' as d ,m.month
     FROM (
           SELECT 'Jan' AS MONTH
           UNION SELECT 'Feb' AS MONTH
           UNION SELECT 'Mar' AS MONTH
           UNION SELECT 'Apr' AS MONTH
           UNION SELECT 'May' AS MONTH
           UNION SELECT 'Jun' AS MONTH
           UNION SELECT 'Jul' AS MONTH
           UNION SELECT 'Aug' AS MONTH
           UNION SELECT 'Sep' AS MONTH
           UNION SELECT 'Oct' AS MONTH
           UNION SELECT 'Nov' AS MONTH
           UNION SELECT 'Dec' AS MONTH
                      ) AS m
            LEFT JOIN replacement_desired u 
            ON MONTH(STR_TO_DATE(CONCAT(m.month),'%M ')) = MONTH(u.timestamp)
            AND YEAR(u.timestamp)='$particular_year'
            GROUP BY m.month,YEAR(u.timestamp)
            ORDER BY MONTH(STR_TO_DATE(CONCAT(m.month),'%M '))
        ";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return 0;
        }
        
    
        /*$sql="SELECT COUNT(replacement_desired.return_order_id),replacement_desired.return_order_id from replacement_desired where replacement_desired.order_replacement_decision_customer_status<>'cancel' GROUP by replacement_desired.return_order_id";*/
    }

public function getAllReplacementsInitialClosedByCustomer($from_date,$to_date,$particular_year){
        
        $sql="
            SELECT COUNT(u.return_order_id) AS counts,$particular_year as y,(month(u.timestamp)) as m,'1' as d ,m.month
     FROM (
           SELECT 'Jan' AS MONTH
           UNION SELECT 'Feb' AS MONTH
           UNION SELECT 'Mar' AS MONTH
           UNION SELECT 'Apr' AS MONTH
           UNION SELECT 'May' AS MONTH
           UNION SELECT 'Jun' AS MONTH
           UNION SELECT 'Jul' AS MONTH
           UNION SELECT 'Aug' AS MONTH
           UNION SELECT 'Sep' AS MONTH
           UNION SELECT 'Oct' AS MONTH
           UNION SELECT 'Nov' AS MONTH
           UNION SELECT 'Dec' AS MONTH
                      ) AS m
            LEFT JOIN replacement_desired u 
            ON MONTH(STR_TO_DATE(CONCAT(m.month),'%M ')) = MONTH(u.timestamp)
            AND YEAR(u.timestamp)='$particular_year' and u.order_replacement_decision_customer_status='cancel'
            GROUP BY m.month,YEAR(u.timestamp)
            ORDER BY MONTH(STR_TO_DATE(CONCAT(m.month),'%M '))
        ";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return 0;
        }
        
    
    /*$sql="SELECT COUNT(replacement_desired.return_order_id),replacement_desired.return_order_id from replacement_desired where replacement_desired.order_replacement_decision_customer_status='cancel' GROUP by replacement_desired.return_order_id";*/
}

public function getAllReplacementsLaterClosedByCustomerOrAdmin($from_date,$to_date,$particular_year){
       
      $sql="
            SELECT COUNT(u.return_order_id) AS counts,$particular_year as y,(month(u.timestamp)) as m,'1' as d ,m.month
     FROM (
           SELECT 'Jan' AS MONTH
           UNION SELECT 'Feb' AS MONTH
           UNION SELECT 'Mar' AS MONTH
           UNION SELECT 'Apr' AS MONTH
           UNION SELECT 'May' AS MONTH
           UNION SELECT 'Jun' AS MONTH
           UNION SELECT 'Jul' AS MONTH
           UNION SELECT 'Aug' AS MONTH
           UNION SELECT 'Sep' AS MONTH
           UNION SELECT 'Oct' AS MONTH
           UNION SELECT 'Nov' AS MONTH
           UNION SELECT 'Dec' AS MONTH
                      ) AS m
            LEFT JOIN replacement_desired u 
            ON MONTH(STR_TO_DATE(CONCAT(m.month),'%M ')) = MONTH(u.timestamp)
            AND YEAR(u.timestamp)='$particular_year' 
            and (u.order_replacement_decision_status='reject' or u.order_replacement_decision_status='replaced' or u.order_replacement_decision_status='refunded')
            GROUP BY m.month,YEAR(u.timestamp)
            ORDER BY MONTH(STR_TO_DATE(CONCAT(m.month),'%M '))
        ";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return 0;
        } 
    

}

public function getAllReplacementsRemainPending($from_date,$to_date,$particular_year){
        $sql="
            SELECT COUNT(u.return_order_id) AS counts,$particular_year as y,(month(u.timestamp)) as m,'1' as d ,m.month
     FROM (
           SELECT 'Jan' AS MONTH
           UNION SELECT 'Feb' AS MONTH
           UNION SELECT 'Mar' AS MONTH
           UNION SELECT 'Apr' AS MONTH
           UNION SELECT 'May' AS MONTH
           UNION SELECT 'Jun' AS MONTH
           UNION SELECT 'Jul' AS MONTH
           UNION SELECT 'Aug' AS MONTH
           UNION SELECT 'Sep' AS MONTH
           UNION SELECT 'Oct' AS MONTH
           UNION SELECT 'Nov' AS MONTH
           UNION SELECT 'Dec' AS MONTH
                      ) AS m
            LEFT JOIN replacement_desired u 
            ON MONTH(STR_TO_DATE(CONCAT(m.month),'%M ')) = MONTH(u.timestamp)
            AND YEAR(u.timestamp)='$particular_year' 
            and (u.order_replacement_decision_status<>'replaced' and u.order_replacement_decision_customer_status<>'cancel')
            GROUP BY m.month,YEAR(u.timestamp)
            ORDER BY MONTH(STR_TO_DATE(CONCAT(m.month),'%M '))
        ";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return 0;
        }            
                
            

}


public function getAllNewRefundRequest($from_date,$to_date,$particular_year){

        $sql="
            SELECT COUNT(u.return_order_id) AS counts,$particular_year as y,(month(u.timestamp)) as m,'1' as d ,m.month
     FROM (
           SELECT 'Jan' AS MONTH
           UNION SELECT 'Feb' AS MONTH
           UNION SELECT 'Mar' AS MONTH
           UNION SELECT 'Apr' AS MONTH
           UNION SELECT 'May' AS MONTH
           UNION SELECT 'Jun' AS MONTH
           UNION SELECT 'Jul' AS MONTH
           UNION SELECT 'Aug' AS MONTH
           UNION SELECT 'Sep' AS MONTH
           UNION SELECT 'Oct' AS MONTH
           UNION SELECT 'Nov' AS MONTH
           UNION SELECT 'Dec' AS MONTH
                      ) AS m
            LEFT JOIN returns_desired u 
            ON MONTH(STR_TO_DATE(CONCAT(m.month),'%M ')) = MONTH(u.timestamp)
            AND YEAR(u.timestamp)='$particular_year'
            GROUP BY m.month,YEAR(u.timestamp)
            ORDER BY MONTH(STR_TO_DATE(CONCAT(m.month),'%M '))
        ";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return 0;
        }
        
    
        /*$sql="SELECT COUNT(replacement_desired.return_order_id),replacement_desired.return_order_id from replacement_desired where replacement_desired.order_replacement_decision_customer_status<>'cancel' GROUP by replacement_desired.return_order_id";*/
    }

public function getAllRefundInitialClosedByCustomer($from_date,$to_date,$particular_year){
        
        $sql="
            SELECT COUNT(u.return_order_id) AS counts,$particular_year as y,(month(u.timestamp)) as m,'1' as d ,m.month
     FROM (
           SELECT 'Jan' AS MONTH
           UNION SELECT 'Feb' AS MONTH
           UNION SELECT 'Mar' AS MONTH
           UNION SELECT 'Apr' AS MONTH
           UNION SELECT 'May' AS MONTH
           UNION SELECT 'Jun' AS MONTH
           UNION SELECT 'Jul' AS MONTH
           UNION SELECT 'Aug' AS MONTH
           UNION SELECT 'Sep' AS MONTH
           UNION SELECT 'Oct' AS MONTH
           UNION SELECT 'Nov' AS MONTH
           UNION SELECT 'Dec' AS MONTH
                      ) AS m
            LEFT JOIN returns_desired u 
            ON MONTH(STR_TO_DATE(CONCAT(m.month),'%M ')) = MONTH(u.timestamp)
            AND YEAR(u.timestamp)='$particular_year' and u.order_return_decision_customer_status='cancel'
            GROUP BY m.month,YEAR(u.timestamp)
            ORDER BY MONTH(STR_TO_DATE(CONCAT(m.month),'%M '))
        ";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return 0;
        }
        
    
    /*$sql="SELECT COUNT(replacement_desired.return_order_id),replacement_desired.return_order_id from replacement_desired where replacement_desired.order_replacement_decision_customer_status='cancel' GROUP by replacement_desired.return_order_id";*/
}

public function getAllRefundLaterClosedByCustomerOrAdmin($from_date,$to_date,$particular_year){
       
      $sql="
            SELECT COUNT(u.return_order_id) AS counts,$particular_year as y,(month(u.timestamp)) as m,'1' as d ,m.month
     FROM (
           SELECT 'Jan' AS MONTH
           UNION SELECT 'Feb' AS MONTH
           UNION SELECT 'Mar' AS MONTH
           UNION SELECT 'Apr' AS MONTH
           UNION SELECT 'May' AS MONTH
           UNION SELECT 'Jun' AS MONTH
           UNION SELECT 'Jul' AS MONTH
           UNION SELECT 'Aug' AS MONTH
           UNION SELECT 'Sep' AS MONTH
           UNION SELECT 'Oct' AS MONTH
           UNION SELECT 'Nov' AS MONTH
           UNION SELECT 'Dec' AS MONTH
                      ) AS m
            LEFT JOIN returns_desired u 
            ON MONTH(STR_TO_DATE(CONCAT(m.month),'%M ')) = MONTH(u.timestamp)
            AND YEAR(u.timestamp)='$particular_year' 
            and (u.order_return_decision_status='reject' or u.order_return_decision_status='refunded')
            GROUP BY m.month,YEAR(u.timestamp)
            ORDER BY MONTH(STR_TO_DATE(CONCAT(m.month),'%M '))
        ";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return 0;
        } 
    

}

public function getAllRefundRemainPending($from_date,$to_date,$particular_year){
        $sql="
            SELECT COUNT(u.return_order_id) AS counts,$particular_year as y,(month(u.timestamp)) as m,'1' as d ,m.month
     FROM (
           SELECT 'Jan' AS MONTH
           UNION SELECT 'Feb' AS MONTH
           UNION SELECT 'Mar' AS MONTH
           UNION SELECT 'Apr' AS MONTH
           UNION SELECT 'May' AS MONTH
           UNION SELECT 'Jun' AS MONTH
           UNION SELECT 'Jul' AS MONTH
           UNION SELECT 'Aug' AS MONTH
           UNION SELECT 'Sep' AS MONTH
           UNION SELECT 'Oct' AS MONTH
           UNION SELECT 'Nov' AS MONTH
           UNION SELECT 'Dec' AS MONTH
                      ) AS m
            LEFT JOIN returns_desired u 
            ON MONTH(STR_TO_DATE(CONCAT(m.month),'%M ')) = MONTH(u.timestamp)
            AND YEAR(u.timestamp)='$particular_year' 
            and (u.order_return_decision_status<>'refunded' and u.order_return_decision_customer_status<>'cancel')
            GROUP BY m.month,YEAR(u.timestamp)
            ORDER BY MONTH(STR_TO_DATE(CONCAT(m.month),'%M '))
        ";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return 0;
        }            
                
            

}


public function get_daily_purchases($from_date,$to_date){
        $daily_purchases=array();
    
        $sql="select COUNT(DISTINCT order_id) as count,year(timestamp) as y,(month(timestamp)-1) as m,DAYOFMONTH(timestamp) as d  from active_orders ";
        if($from_date!="" || $to_date!=""){
            $sql.=" where 1=1 ";
        }
       
       if($from_date!="" && $to_date!=""){
           $sql.=" and date(active_orders.timestamp) >= '$from_date' and date(active_orders.timestamp) <= '$to_date'"; 
        }
        if($from_date!="" && $to_date==""){
           $sql.=" and date(active_orders.timestamp) >= '$from_date' "; 
        }
        if($from_date=="" && $to_date!=""){
           $sql.=" and  date(active_orders.timestamp) <= '$to_date'"; 
        }
    
        $sql.="group by d";
        
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            //return $result->result_array();
            $daily_purchases['carts']=$result->result_array();
        }
        else{
            $daily_purchases['carts']= 0;
        }
        
        $sql="select COUNT(DISTINCT sku_id) as count,year(timestamp) as y,(month(timestamp)-1) as m,DAYOFMONTH(timestamp) as d  from active_orders ";
        if($from_date!="" || $to_date!=""){
            $sql.=" where 1=1 ";
        }
       
       if($from_date!="" && $to_date!=""){
           $sql.=" and date(active_orders.timestamp) >= '$from_date' and date(active_orders.timestamp) <= '$to_date'"; 
        }
        if($from_date!="" && $to_date==""){
           $sql.=" and date(active_orders.timestamp) >= '$from_date' "; 
        }
        if($from_date=="" && $to_date!=""){
           $sql.=" and  date(active_orders.timestamp) <= '$to_date'"; 
        }
    
        $sql.="group by d";
        
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            //return $result->result_array();
            $daily_purchases['distinct_skus']=$result->result_array();
        }
        else{
            $daily_purchases['distinct_skus']= 0;
        }
        
        $sql="select sum(quantity) as count,year(timestamp) as y,(month(timestamp)-1) as m,DAYOFMONTH(timestamp) as d  from active_orders ";
        
        if($from_date!="" || $to_date!=""){
            $sql.=" where 1=1 ";
        }
       
       if($from_date!="" && $to_date!=""){
           $sql.=" and date(active_orders.timestamp) >= '$from_date' and date(active_orders.timestamp) <= '$to_date'"; 
        }
        if($from_date!="" && $to_date==""){
           $sql.=" and date(active_orders.timestamp) >= '$from_date' "; 
        }
        if($from_date=="" && $to_date!=""){
           $sql.=" and  date(active_orders.timestamp) <= '$to_date'"; 
        }
    
        $sql.="group by d";
        
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            //return $result->result_array();
            $daily_purchases['daily_quantity']=$result->result_array();
        }
        else{
            $daily_purchases['daily_quantity']= 0;
        }
        
        
        $sql="DROP FUNCTION IF EXISTS SUM_OF_LIST";
        $result=$this->db->query($sql);
        $sql= 'CREATE FUNCTION SUM_OF_LIST(s TEXT)
                  RETURNS DOUBLE
                  DETERMINISTIC
                  NO SQL
                BEGIN
                  DECLARE res DOUBLE DEFAULT 0;
                  WHILE INSTR(s, ",") > 0 DO
                    SET res = res + SUBSTRING_INDEX(s, ",", 1);
                    SET s = MID(s, INSTR(s, ",") + 1);
                  END WHILE;
                  RETURN res + s;
                END
        ';
        $result=$this->db->query($sql);
        
        $sql="select SUM_OF_LIST(promotion_item_num) as count,year(timestamp) as y,(month(timestamp)-1) as m,DAYOFMONTH(timestamp) as d  from active_orders ";
        
        if($from_date!="" || $to_date!=""){
            $sql.=" where 1=1 ";
        }
       
       if($from_date!="" && $to_date!=""){
           $sql.=" and date(active_orders.timestamp) >= '$from_date' and date(active_orders.timestamp) <= '$to_date'"; 
        }
        if($from_date!="" && $to_date==""){
           $sql.=" and date(active_orders.timestamp) >= '$from_date' "; 
        }
        if($from_date=="" && $to_date!=""){
           $sql.=" and  date(active_orders.timestamp) <= '$to_date'"; 
        }
    
        $sql.="group by d";
        
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            //return $result->result_array();
            $daily_purchases['promotion_quantity']=$result->result_array();
        }
        else{
            $daily_purchases['promotion_quantity']= 0;
        }
        
        return $daily_purchases;
}

public function get_daily_purchases_complete($from_date,$to_date){
        $daily_purchases=array();
    
        $sql="select COUNT(DISTINCT order_id) as count,year(orders_status.order_delivered_timestamp) as y,(month(orders_status.order_delivered_timestamp)-1) as m,DAYOFMONTH(orders_status.order_delivered_timestamp) as d,orders_status.order_item_id  from history_orders,orders_status where history_orders.order_item_id=orders_status.order_item_id ";
        
        if($from_date!="" || $to_date!=""){
          //  $sql.=" where 1=1 ";
        }
       
       if($from_date!="" && $to_date!=""){
           $sql.=" and date(orders_status.order_delivered_timestamp) >= '$from_date' and date(orders_status.order_delivered_timestamp) <= '$to_date'"; 
        }
        if($from_date!="" && $to_date==""){
           $sql.=" and date(orders_status.order_delivered_timestamp) >= '$from_date' "; 
        }
        if($from_date=="" && $to_date!=""){
           $sql.=" and  date(orders_status.order_delivered_timestamp) <= '$to_date'"; 
        }
    
        $sql.="group by d";
        
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            //return $result->result_array();
            $daily_purchases['carts']=$result->result_array();
        }
        else{
            $daily_purchases['carts']= 0;
        }
        
        $sql="select COUNT(DISTINCT sku_id) as count,year(orders_status.order_delivered_timestamp) as y,(month(orders_status.order_delivered_timestamp)-1) as m,DAYOFMONTH(orders_status.order_delivered_timestamp) as d,orders_status.order_item_id  from history_orders,orders_status where history_orders.order_item_id=orders_status.order_item_id ";
        
        if($from_date!="" || $to_date!=""){
          //  $sql.=" where 1=1 ";
        }
       
       if($from_date!="" && $to_date!=""){
           $sql.=" and date(orders_status.order_delivered_timestamp) >= '$from_date' and date(orders_status.order_delivered_timestamp) <= '$to_date'"; 
        }
        if($from_date!="" && $to_date==""){
           $sql.=" and date(orders_status.order_delivered_timestamp) >= '$from_date' "; 
        }
        if($from_date=="" && $to_date!=""){
           $sql.=" and  date(orders_status.order_delivered_timestamp) <= '$to_date'"; 
        }
    
        $sql.="group by d";
        
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            //return $result->result_array();
            $daily_purchases['distinct_skus']=$result->result_array();
        }
        else{
            $daily_purchases['distinct_skus']= 0;
        }
        
        $sql="select sum(quantity) as count,year(orders_status.order_delivered_timestamp) as y,(month(orders_status.order_delivered_timestamp)-1) as m,DAYOFMONTH(orders_status.order_delivered_timestamp) as d,orders_status.order_item_id  from history_orders,orders_status where history_orders.order_item_id=orders_status.order_item_id ";
        
        if($from_date!="" || $to_date!=""){
           // $sql.=" where 1=1 ";
        }
       
       if($from_date!="" && $to_date!=""){
           $sql.=" and date(orders_status.order_delivered_timestamp) >= '$from_date' and date(orders_status.order_delivered_timestamp) <= '$to_date'"; 
        }
        if($from_date!="" && $to_date==""){
           $sql.=" and date(orders_status.order_delivered_timestamp) >= '$from_date' "; 
        }
        if($from_date=="" && $to_date!=""){
           $sql.=" and  date(orders_status.order_delivered_timestamp) <= '$to_date'"; 
        }
    
        $sql.="group by d";
        
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            //return $result->result_array();
            $daily_purchases['daily_quantity']=$result->result_array();
        }
        else{
            $daily_purchases['daily_quantity']= 0;
        }
        
        
        $sql="DROP FUNCTION IF EXISTS SUM_OF_LIST";
        $result=$this->db->query($sql);
        $sql= 'CREATE FUNCTION SUM_OF_LIST(s TEXT)
                  RETURNS DOUBLE
                  DETERMINISTIC
                  NO SQL
                BEGIN
                  DECLARE res DOUBLE DEFAULT 0;
                  WHILE INSTR(s, ",") > 0 DO
                    SET res = res + SUBSTRING_INDEX(s, ",", 1);
                    SET s = MID(s, INSTR(s, ",") + 1);
                  END WHILE;
                  RETURN res + s;
                END
        ';
        $result=$this->db->query($sql);
        
        $sql="select SUM_OF_LIST(promotion_item_num) as count,year(orders_status.order_delivered_timestamp) as y,(month(orders_status.order_delivered_timestamp)-1) as m,DAYOFMONTH(orders_status.order_delivered_timestamp) as d ,orders_status.order_item_id  from history_orders,orders_status where history_orders.order_item_id=orders_status.order_item_id  ";
        
        if($from_date!="" || $to_date!=""){
           // $sql.=" where 1=1 ";
        }
       
       if($from_date!="" && $to_date!=""){
           $sql.=" and date(orders_status.order_delivered_timestamp) >= '$from_date' and date(orders_status.order_delivered_timestamp) <= '$to_date'"; 
        }
        if($from_date!="" && $to_date==""){
           $sql.=" and date(orders_status.order_delivered_timestamp) >= '$from_date' "; 
        }
        if($from_date=="" && $to_date!=""){
           $sql.=" and  date(orders_status.order_delivered_timestamp) <= '$to_date'"; 
        }
    
        $sql.="group by d";
        
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            //return $result->result_array();
            $daily_purchases['promotion_quantity']=$result->result_array();
        }
        else{
            $daily_purchases['promotion_quantity']= 0;
        }
        
        return $daily_purchases;
}
/* 
Cancelled Orders old coding commented 6/10/2021
public function getCancelledOrders($from_date,$to_date){
    $sql="SELECT count(DISTINCT cancelled_orders.order_item_id) as cancelcount,cancels.cancelled_by, count(DISTINCT history_orders.order_item_id) as histCount,count(DISTINCT active_orders.order_item_id) as actcount,year(cancelled_orders.timestamp) as y,(month(cancelled_orders.timestamp)-1) as m,DAYOFMONTH(cancelled_orders.timestamp) as d, HOUR(cancelled_orders.timestamp) as h from cancelled_orders left join active_orders on active_orders.order_id=cancelled_orders.order_id left join history_orders on history_orders.order_id=cancelled_orders.order_id left join cancels on cancels.order_item_id=cancelled_orders.order_item_id group by cancels.cancelled_by,cancelled_orders.order_id";
    $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return 0;
        } 
}*/

public function cancelledOrdersNewFun($from_date, $to_date){
    

 if($from_date=="" && $to_date==""){
            $dates_OrderReceived=" date(timestamp) = CURDATE() "; 
        }
        if($from_date==$to_date && ($from_date!="" && $to_date!="")){
            $dates_OrderReceived=" date(timestamp) = '$from_date' "; 
        }
        else{
            if($from_date!="" && $to_date!=""){
               $dates_OrderReceived=" date(timestamp) >= '$from_date' and date(timestamp) <= '$to_date'"; 
            }
            if($from_date!="" && $to_date==""){
               $dates_OrderReceived=" date(timestamp) >= '$from_date' "; 
            }
            if($from_date=="" && $to_date!=""){
               $dates_OrderReceived=" date(timestamp) <= '$to_date'"; 
            }
        } 
      
    

 $sql="SELECT 

(SELECT count(*) FROM `cancels` WHERE cancelled_by='customer'  and  ".$dates_OrderReceived.") as 'Customer Cancelled',

(SELECT count(*) FROM `cancels` WHERE cancelled_by='admin'  and  ".$dates_OrderReceived.") as 'Admin Cancelled'
from cancels  limit 1";
$result=$this->db->query($sql);
    if($result->num_rows()>0){
        return $result->result_array();
    }
    else{
        return 0;
    }

}

public function get_purchaseTrend_by_hour($from_date,$to_date){
    $sql="select count(distinct active_orders.order_id) as activecount,hour(active_orders.timestamp) as h,DAYOFMONTH(active_orders.timestamp) as d from active_orders " ;
        if($from_date!="" || $to_date!=""){
            $sql.=" where 1=1 ";
        }
        
        if($from_date==$to_date && ($from_date!="" && $to_date!="")){
            $sql.=" and date(active_orders.timestamp) = '$from_date' "; 
        }
        else{
            if($from_date!="" && $to_date!=""){
               $sql.=" and date(active_orders.timestamp) >= '$from_date' and date(active_orders.timestamp) <= '$to_date'"; 
            }
            if($from_date!="" && $to_date==""){
               $sql.=" and date(active_orders.timestamp) >= '$from_date' "; 
            }
            if($from_date=="" && $to_date!=""){
               $sql.=" and  date(active_orders.timestamp) <= '$to_date'"; 
            }
        }
        
    
    $sql.=" group by hour(active_orders.timestamp) ";
    
     $sql.="union select count(distinct history_orders.order_id) as historycount,hour(history_orders.timestamp_active_order) as h,DAYOFMONTH(history_orders.timestamp) as d from history_orders ";
     
     if($from_date!="" || $to_date!=""){
            $sql.=" where 1=1  ";
        }
     if($from_date==$to_date && ($from_date!="" && $to_date!="")){
            $sql.=" and date(history_orders.timestamp_active_order) = '$from_date' "; 
        }
        else{
     
            if($from_date!="" && $to_date!=""){
               $sql.=" and date(history_orders.timestamp_active_order) >= '$from_date' and date(history_orders.timestamp_active_order) <= '$to_date'"; 
            }
            if($from_date!="" && $to_date==""){
               $sql.=" and date(history_orders.timestamp_active_order) >= '$from_date' "; 
            }
            if($from_date=="" && $to_date!=""){
               $sql.=" and  date(history_orders.timestamp_active_order) <= '$to_date'"; 
            }
        }
     
     $sql.=" group by hour(history_orders.timestamp_active_order)";    
    
        $result=$this->db->query($sql);
    //echo $this->db->last_query();
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return 0;
        }
}
public function getPaymentMethodAnalytics($from_date,$to_date){
        $sql="select count(distinct history_orders.payment_type) as y,sum(history_orders.grandtotal) as total,history_orders.payment_type as name from history_orders ";
        
        if($from_date!="" || $to_date!=""){
            $sql.=" where 1=1  ";
        }
     if($from_date==$to_date && ($from_date!="" && $to_date!="")){
            $sql.=" and date(history_orders.timestamp_active_order) = '$from_date' "; 
        }
        else{
     
            if($from_date!="" && $to_date!=""){
               $sql.=" and date(history_orders.timestamp_active_order) >= '$from_date' and date(history_orders.timestamp_active_order) <= '$to_date'"; 
            }
            if($from_date!="" && $to_date==""){
               $sql.=" and date(history_orders.timestamp_active_order) >= '$from_date' "; 
            }
            if($from_date=="" && $to_date!=""){
               $sql.=" and  date(history_orders.timestamp_active_order) <= '$to_date'"; 
            }
        }
        
        
         $sql.=" group by history_orders.payment_type ";
        
        
       $sql.=" union select count(distinct active_orders.payment_type) as y,sum(active_orders.grandtotal) as total,active_orders.payment_type as name from active_orders ";
       
       if($from_date!="" || $to_date!=""){
            $sql.=" where 1=1 ";
        }
        
        if($from_date==$to_date && ($from_date!="" && $to_date!="")){
            $sql.=" and date(active_orders.timestamp) = '$from_date' "; 
        }
        else{
            if($from_date!="" && $to_date!=""){
               $sql.=" and date(active_orders.timestamp) >= '$from_date' and date(active_orders.timestamp) <= '$to_date'"; 
            }
            if($from_date!="" && $to_date==""){
               $sql.=" and date(active_orders.timestamp) >= '$from_date' "; 
            }
            if($from_date=="" && $to_date!=""){
               $sql.=" and  date(active_orders.timestamp) <= '$to_date'"; 
            }
        }
       
       
        $sql.=" group by active_orders.payment_type ";

      $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return 0;
        }
     
}
        
public function get_all_products_arr_by_rating($from_date,$to_date,$limit_result_val,$rating){
    $sql="select count(rated_inventory.rating) as counts, products.product_name as name from products join inventory as pinv on pinv.product_id=products.product_id join rated_inventory  on pinv.id=rated_inventory.inventory_id where rated_inventory.rating='$rating' group by products.product_id order by counts";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
        return $result->result_array();
    }
    else{
        return 0;
    }
}    

public function ordersStages($from_date, $to_date){
    

 if($from_date=="" && $to_date==""){
            $dates_OrderReceived=" date(order_placed_timestamp) = CURDATE() "; 
        }
        if($from_date==$to_date && ($from_date!="" && $to_date!="")){
            $dates_OrderReceived=" date(order_placed_timestamp) = '$from_date' "; 
        }
        else{
            if($from_date!="" && $to_date!=""){
               $dates_OrderReceived=" date(order_placed_timestamp) >= '$from_date' and date(order_placed_timestamp) <= '$to_date'"; 
            }
            if($from_date!="" && $to_date==""){
               $dates_OrderReceived=" date(order_placed_timestamp) >= '$from_date' "; 
            }
            if($from_date=="" && $to_date!=""){
               $dates_OrderReceived=" date(order_placed_timestamp) <= '$to_date'"; 
            }
        } 
        
        if($from_date=="" && $to_date==""){
            $dates_OrderApprove=" date(order_placed_timestamp) = CURDATE() "; 
        }
        if($from_date==$to_date && ($from_date!="" && $to_date!="")){
            $dates_OrderApprove=" date(order_placed_timestamp) = '$from_date' "; 
        }
        else{
            if($from_date!="" && $to_date!=""){
               $dates_OrderApprove=" date(order_placed_timestamp) >= '$from_date' and date(order_placed_timestamp) <= '$to_date'"; 
            }
            if($from_date!="" && $to_date==""){
               $dates_OrderApprove=" date(order_placed_timestamp) >= '$from_date' "; 
            }
            if($from_date=="" && $to_date!=""){
               $dates_OrderApprove=" date(order_placed_timestamp) <= '$to_date'"; 
            }
        }
        
        if($from_date=="" && $to_date==""){
            $dates_ShippingConfirm=" date(order_confirmed_timestamp) = CURDATE() "; 
        }
        if($from_date==$to_date && ($from_date!="" && $to_date!="")){
            $dates_ShippingConfirm=" date(order_confirmed_timestamp) = '$from_date' "; 
        }
        else{
            if($from_date!="" && $to_date!=""){
               $dates_ShippingConfirm=" date(order_confirmed_timestamp) >= '$from_date' and date(order_confirmed_timestamp) <= '$to_date'"; 
            }
            if($from_date!="" && $to_date==""){
               $dates_ShippingConfirm=" date(order_confirmed_timestamp) >= '$from_date' "; 
            }
            if($from_date=="" && $to_date!=""){
               $dates_ShippingConfirm=" date(order_confirmed_timestamp) <= '$to_date'"; 
            }
        }
        
        if($from_date=="" && $to_date==""){
            $dates_OrderPacked="date(order_confirmed_timestamp) = CURDATE() "; 
        }
        if($from_date==$to_date && ($from_date!="" && $to_date!="")){
            $dates_OrderPacked=" date(order_confirmed_timestamp) = '$from_date' "; 
        }
        else{
            if($from_date!="" && $to_date!=""){
               $dates_OrderPacked=" date(order_confirmed_timestamp) >= '$from_date' and date(order_confirmed_timestamp) <= '$to_date'"; 
            }
            if($from_date!="" && $to_date==""){
               $dates_OrderPacked=" date(order_confirmed_timestamp) >= '$from_date' "; 
            }
            if($from_date=="" && $to_date!=""){
               $dates_OrderPacked=" date(order_confirmed_timestamp) <= '$to_date'"; 
            }
        }  
        
        if($from_date=="" && $to_date==""){
            $dates_OrderShipped=" date(order_confirmed_timestamp) = CURDATE() "; 
        }
        if($from_date==$to_date && ($from_date!="" && $to_date!="")){
            $dates_OrderShipped=" date(order_confirmed_timestamp) = '$from_date' "; 
        }
        else{
            if($from_date!="" && $to_date!=""){
               $dates_OrderShipped=" date(order_confirmed_timestamp) >= '$from_date' and date(order_confirmed_timestamp) <= '$to_date'"; 
            }
            if($from_date!="" && $to_date==""){
               $dates_OrderShipped=" date(order_confirmed_timestamp) >= '$from_date' "; 
            }
            if($from_date=="" && $to_date!=""){
               $dates_OrderShipped=" date(order_confirmed_timestamp) <= '$to_date'"; 
            }
        }
        
        if($from_date=="" && $to_date==""){
            $dates_OrderDelivered=" date(order_confirmed_timestamp) = CURDATE() "; 
        }
        if($from_date==$to_date && ($from_date!="" && $to_date!="")){
            $dates_OrderDelivered=" date(order_confirmed_timestamp) = '$from_date' "; 
        }
        else{
            if($from_date!="" && $to_date!=""){
               $dates_OrderDelivered=" date(order_confirmed_timestamp) >= '$from_date' and date(order_confirmed_timestamp) <= '$to_date'"; 
            }
            if($from_date!="" && $to_date==""){
               $dates_OrderDelivered=" date(order_confirmed_timestamp) >= '$from_date' "; 
            }
            if($from_date=="" && $to_date!=""){
               $dates_OrderDelivered=" date(order_confirmed_timestamp) <= '$to_date'"; 
            }
        }   
    

$sql="SELECT 

(select count(*) from orders_status where (order_placed=0 or order_placed=1)  and  ".$dates_OrderReceived.") as 'Orders Received',

(select count(*) from orders_status where (order_placed=1)  and  ".$dates_OrderReceived.") as 'Orders Approved',

(select count(*) from orders_status where (order_confirmed=1)  and  ".$dates_ShippingConfirm.") as 'Shipping Confirmed',

ABS((select count(*) from orders_status where (order_placed=1)  and  ".$dates_OrderReceived.") - (select count(*) from orders_status where (order_confirmed=1)  and  ".$dates_ShippingConfirm.")) as 'Yet To Confirm',

(select count(*) from orders_status where (order_packed=1)  and  ".$dates_OrderPacked.") as 'Orders Packed',

(select count(*) from orders_status where (order_shipped=1)  and  ".$dates_OrderShipped.") as 'Orders Shipped',

(select count(*) from orders_status where (order_delivered=1)  and  ".$dates_OrderDelivered.") as 'Orders Delivered'
from orders_status  limit 1";
$result=$this->db->query($sql);
    if($result->num_rows()>0){
        return $result->result_array();
    }
    else{
        return 0;
    }

}
public function get_visitor_data(){
	$sql="select * from visitor_data order by timestamp desc";
	$res=$this->db->query($sql);
	return $res->result();
}
public function get_cat_name($cat_id){	
	$sql="select cat_name from category where cat_id='$cat_id'";
	$query=$this->db->query($sql);
	return $query->row()->cat_name;
}
public function get_sku($inv_id){	
	$sql="select inventory.*,products.* from inventory,products where inventory.id='$inv_id' and inventory.product_id=products.product_id";
	$query=$this->db->query($sql);
	return $query->row();
}
public function get_sku_by_sku_id($sku_id){	
	$sql="select inventory.*,products.* from inventory,products where inventory.sku_id=lower('$sku_id') and inventory.product_id=products.product_id";
	$query=$this->db->query($sql);
	return $query->row();
}

public function get_product_name($product_id){	
	$sql="select product_name from products where product_id='$product_id'";
	$query=$this->db->query($sql);
	return $query->row()->product_name;
}
    /*SELECT 

(select count(*) from orders_status where (order_placed=0 or order_placed=1)  and  date(order_placed_timestamp)=CURDATE()) as 'Orders Received',

(select count(*) from orders_status where (order_placed=1)  and  date(order_placed_timestamp)=CURDATE()) as 'Orders Approved',

(select count(*) from orders_status where (order_confirmed=1)  and  date(   order_confirmed_timestamp)=CURDATE()) as 'Shipping Confirmed',

ABS((select count(*) from orders_status where (order_placed=1)  and  date(order_placed_timestamp)=CURDATE()) - (select count(*) from orders_status where (order_confirmed=1)  and  date(   order_confirmed_timestamp)=CURDATE())) as 'Yet To Confirm',

(select count(*) from orders_status where (order_packed=1)  and  date(   order_packed_timestamp)=CURDATE()) as 'Orders Packed',

(select count(*) from orders_status where (order_shipped=1)  and  date(   order_shipped_timestamp)=CURDATE()) as 'Orders Shipped',

(select count(*) from orders_status where (order_delivered=1)  and  date(   order_delivered_timestamp)=CURDATE()) as 'Orders Delivered'
from orders_status  limit 1
     * 
     *  $sql="select 
CASE WHEN orders_status.order_placed='1' then count(orders_status.id) 
ELSE 0 END as Placed ,

CASE WHEN orders_status.order_confirmed='1' then count(orders_status.id) 
ELSE 0 END as Confirmed ,
CASE WHEN orders_status.order_confirmed='0' then count(orders_status.id) 
ELSE 0 END as UnConfirmed ,

CASE WHEN orders_status.order_packed='1' then count(orders_status.id) 
ELSE 0 END as Packed ,
CASE WHEN orders_status.order_packed='0' then count(orders_status.id) 
ELSE 0 END as UnPacked ,

CASE WHEN orders_status.order_shipped='1' then count(orders_status.id) 
ELSE 0 END as Shipped ,
CASE WHEN orders_status.order_shipped='0' then count(orders_status.id) 
ELSE 0 END as UnShipped ,

CASE WHEN orders_status.order_delivered='1' then count(orders_status.id) 
ELSE 0 END as Delivered,
CASE WHEN orders_status.order_delivered='0' then count(orders_status.id) 
ELSE 0 END as UnDelivered 
    * 

    * 
from orders_status";
    * 
    * 
    * 
    * SELECT 

(select count(*) from orders_status where order_placed=0 and  date(order_placed_timestamp)=CURDATE()) as Placed,
(select count(*) from orders_status where order_placed=1 and  date(order_placed_timestamp)=CURDATE()) as Approved

from orders_status  limit 1
$sql="SELECT 
(select count(*) from orders_status ) as Total,
(select count(*) from orders_status where order_placed=0) as Placed,
(select count(*) from orders_status where order_placed=1) as Approved,
(select count(*) from orders_status where order_confirmed=0) as UnConfirmed,
(select count(*) from orders_status where order_confirmed=1) as Confirmed,
(select count(*) from orders_status where order_packed=1) as Packed,
(select count(*) from orders_status where order_packed=0) as UnPacked,
(select count(*) from orders_status where order_shipped=1) as Shipped,
(select count(*) from orders_status where order_shipped=0) as UnShipped,
(select count(*) from orders_status where order_delivered=1) as Delivered,
(select count(*) from orders_status where order_delivered=0) as Undelivered
from orders_status limit 1";*/
        
/*$pendingCases=array();
        $sql="select return_order_id from replacement_desired"; 
        $result=$this->db->query($sql);  
        if($result->num_rows()>0){
            $array1=$result->result_array();
        }
        $sql="select return_order_id from order_replacement_decision"; 
        $result=$this->db->query($sql);  
        if($result->num_rows()>0){
            $array2=$result->result_array();
        } 
        $replacement_desired=array();
        foreach($array1 as $arr){
            array_push($replacement_desired,$arr['return_order_id']);
        }
        $order_replacement_decision=array();
        foreach($array2 as $arr){
            array_push($order_replacement_decision,$arr['return_order_id']);
        }
        
        $result_only_in_replacementDesired=array_diff($replacement_desired,$order_replacement_decision);
        $result_from_return_desired=array();
        if(count($result_only_in_replacementDesired)>0){
        
            foreach($result_only_in_replacementDesired as $replId){
                $sql="
                SELECT COUNT(u.return_order_id) AS counts,$particular_year as y,(month(u.timestamp)) as m,'1' as d ,m.month
         FROM (
               SELECT 'Jan' AS MONTH
               UNION SELECT 'Feb' AS MONTH
               UNION SELECT 'Mar' AS MONTH
               UNION SELECT 'Apr' AS MONTH
               UNION SELECT 'May' AS MONTH
               UNION SELECT 'Jun' AS MONTH
               UNION SELECT 'Jul' AS MONTH
               UNION SELECT 'Aug' AS MONTH
               UNION SELECT 'Sep' AS MONTH
               UNION SELECT 'Oct' AS MONTH
               UNION SELECT 'Nov' AS MONTH
               UNION SELECT 'Dec' AS MONTH
                          ) AS m
                LEFT JOIN replacement_desired u 
                ON MONTH(STR_TO_DATE(CONCAT(m.month),'%M ')) = MONTH(u.timestamp)
                AND YEAR(u.timestamp)='$particular_year' and u.order_replacement_decision_customer_status<>'cancel' where u.return_order_id='$replId'
                GROUP BY m.month,YEAR(u.timestamp)
                ORDER BY 1+1
            ";
            $result=$this->db->query($sql);
            if($result->num_rows()>0){
                $result_from_return_desired+=$result->result_array();
            }
            
            }
        }
        
        
        $sql="
            SELECT COUNT(u.return_order_id) AS counts,$particular_year as y,(month(u.timestamp)) as m,'1' as d ,m.month
     FROM (
           SELECT 'Jan' AS MONTH
           UNION SELECT 'Feb' AS MONTH
           UNION SELECT 'Mar' AS MONTH
           UNION SELECT 'Apr' AS MONTH
           UNION SELECT 'May' AS MONTH
           UNION SELECT 'Jun' AS MONTH
           UNION SELECT 'Jul' AS MONTH
           UNION SELECT 'Aug' AS MONTH
           UNION SELECT 'Sep' AS MONTH
           UNION SELECT 'Oct' AS MONTH
           UNION SELECT 'Nov' AS MONTH
           UNION SELECT 'Dec' AS MONTH
                      ) AS m
            LEFT JOIN order_replacement_decision u 
            ON MONTH(STR_TO_DATE(CONCAT(m.month),'%M ')) = MONTH(u.timestamp)
            AND YEAR(u.timestamp)='$particular_year' and (u.customer_decision<>'accept' and u.customer_decision<>'reject' and u.status<>'accept' and u.status<>'reject') 
            GROUP BY m.month,YEAR(u.timestamp)
            ORDER BY 1+1
        ";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            $arrayFromDecision=$result->result_array();
        }
        $pendingCases=array_merge($pendingCases,$arrayFromDecision);

        $pendingCases=array_merge($pendingCases,$result_from_return_desired);
        return $pendingCases;
    
   */
        
    
    /*$sql="SELECT count(order_replacement_decision.return_order_id) as counts from order_replacement_decision where order_replacement_decision.customer_decision='accept' or order_replacement_decision.customer_decision='reject' or order_replacement_decision.status='accept' or order_replacement_decision.status='reject' group by order_replacement_decision.return_order_id";*/
      /*$sql="
            SELECT COUNT(u.return_order_id) AS counts,$particular_year as y,(month(u.timestamp)) as m,'1' as d ,m.month
     FROM (
           SELECT 'Jan' AS MONTH
           UNION SELECT 'Feb' AS MONTH
           UNION SELECT 'Mar' AS MONTH
           UNION SELECT 'Apr' AS MONTH
           UNION SELECT 'May' AS MONTH
           UNION SELECT 'Jun' AS MONTH
           UNION SELECT 'Jul' AS MONTH
           UNION SELECT 'Aug' AS MONTH
           UNION SELECT 'Sep' AS MONTH
           UNION SELECT 'Oct' AS MONTH
           UNION SELECT 'Nov' AS MONTH
           UNION SELECT 'Dec' AS MONTH
                      ) AS m
            LEFT JOIN order_replacement_decision u 
            ON MONTH(STR_TO_DATE(CONCAT(m.month),'%M ')) = MONTH(u.timestamp)
            AND YEAR(u.timestamp)='$particular_year' 
            and (u.customer_decision='accept' or u.customer_decision='reject' or u.status='accept' or u.status='reject')
            GROUP BY m.month,YEAR(u.timestamp)
            ORDER BY 1+1
        ";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
        else{
            return 0;
        }  */    
        
    /*select 
CASE 
WHEN orders_status.order_placed='1' then count(orders_status.id) as counts_order_placed
WHEN orders_status.order_confirmed='1' then count(orders_status.id) as counts_order_confirmed
WHEN orders_status.order_packed='1' then count(orders_status.id) as counts_order_packed
WHEN orders_status.order_shipped='1' then count(orders_status.id) as counts_order_shipped
WHEN orders_status.order_delivered='1' then count(orders_status.id) as counts_order_delivered
ELSE count(orders_status.id) as counts_order_others END AS order_statuses

from orders_status*/
    /*$sql="SELECT count(order_replacement_decision.return_order_id) as counts from order_replacement_decision where order_replacement_decision.customer_decision='accept' or order_replacement_decision.customer_decision='reject' or order_replacement_decision.status='accept' or order_replacement_decision.status='reject' group by order_replacement_decision.return_order_id";*/

/*public function get_all_products_arr_by_brand($brand_id){
      $sql="select brands.brand_id, brands.brand_name,products.product_id,products.product_name from brands join products on brands.brand_id=products.brand_id WHERE products.brand_id='$brand_id' GROUP BY products.product_name ORDER BY brands.brand_id";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    } 
}*/
/*public function getNumOfProductsInInventory($product_id){
   $sql="select * from inventory where product_id='$product_id'";
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
       return $result->num_rows();
    }
    else{
       return 0;
    } 
}*

public function get_parent_category_added($from_date,$to_date,$limit_result_val){
    $sql="select * from parent_category";
    $result=$this->db->query($sql);

    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}
public function get_category_added($from_date,$to_date,$limit_result_val){
    $sql="select * from parent_category";
    $result=$this->db->query($sql);

    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}
public function get_sub_categoryy_added($from_date,$to_date,$limit_result_val){
    $sql="select * from parent_category";
    $result=$this->db->query($sql);

    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}
public function get_brand_added($from_date,$to_date,$limit_result_val){
    $sql="select * from parent_category";
    $result=$this->db->query($sql);

    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}
public function get_products_added($from_date,$to_date,$limit_result_val){
    $sql="select * from parent_category";
    $result=$this->db->query($sql);

    if($result->num_rows()>0){
       return $result->result_array();
    }
    else{
       return false;
    }
}
//$sql="select count(case when gender='M' then 1 end) as Male, count(case when gender='F' then 1 end) as Female from customer";
//SELECT TIMESTAMPDIFF(YEAR, dob, CURDATE()) AS age FROM dancefit_users WHERE TIMESTAMPDIFF(YEAR, dob, CURDATE()) >= $start AND TIMESTAMPDIFF(YEAR, dob, CURDATE()) <= $end
/*
 * SELECT COUNT(dob) AS 18_25 FROM customer WHERE TIMESTAMPDIFF(YEAR, dob, CURDATE()) >= 18 AND TIMESTAMPDIFF(YEAR, dob, CURDATE()) <= 25
UNION   SELECT COUNT(dob) AS 26_35 FROM customer WHERE TIMESTAMPDIFF(YEAR, dob, CURDATE()) >= 26 AND TIMESTAMPDIFF(YEAR, dob, CURDATE()) <= 35
UNION   SELECT COUNT(dob) AS 36_45 FROM customer WHERE TIMESTAMPDIFF(YEAR, dob, CURDATE()) >= 36 AND TIMESTAMPDIFF(YEAR, dob, CURDATE()) <= 45
UNION   SELECT COUNT(dob) AS 46_55 FROM customer WHERE TIMESTAMPDIFF(YEAR, dob, CURDATE()) >= 46 AND TIMESTAMPDIFF(YEAR, dob, CURDATE()) <= 55
UNION   SELECT COUNT(dob) AS 56 FROM customer WHERE TIMESTAMPDIFF(YEAR, dob, CURDATE()) >= 56 */
/*SELECT
    SUM(IF(age < 25,1,18) as 'Under 20',
    SUM(IF(age BETWEEN 26 and 35,1,0) as '26 - 35',
 SUM(IF(age BETWEEN 36 and 45,1,0) as '30 - 39',
 SUM(IF(age BETWEEN 46 and 55,1,0) as '30 - 39',
 SUM(IF(age BETWEEN 45 and 100,1,0) as '30 - 39' from customer

 * 
 * 
 * 	*/
 
 
 
 
 
 
 
 /////////////////////////////////////////////////////////
 
 
 
 public function vendor_getCustomerGenderAnalytics($from_date,$to_date,$limit_result_val){

    $sql="select count(case when gender='male' then 1 end) as Male, count(case when gender='female' then 1 end) as Female, count(case when gender='' then 1 end) as Others from customer ";
    
   if($from_date!="" && $to_date!=""){
       $sql.=" where date(customer.timestamp) >= '$from_date' and date(customer.timestamp) <= '$to_date'"; 
    }
    if($from_date!="" && $to_date==""){
       $sql.=" where date(customer.timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
       $sql.=" where  date(customer.timestamp) <= '$to_date'"; 
    }
   
   $sql.="";
    
    $result=$this->db->query($sql);
       if($result->num_rows()>0){
           return $result->result_array();
       }
       else{
           return false;
       }
}
public function vendor_getCustomerAgeAnalytics($from_date,$to_date,$limit_result_val){
    
    $sql="
   
    SELECT CASE WHEN
    age BETWEEN 18 AND 25 THEN '18 - 25' WHEN age BETWEEN 26 AND 35 THEN '26 - 35' WHEN age BETWEEN 36 AND 45 THEN '36 - 45' WHEN age BETWEEN 46 AND 55 THEN '46 - 55' WHEN age >= 56 THEN 'Over 56' WHEN age IS NULL THEN 'Not Filled In (NULL)'
    END AS age_range,
    COUNT(*) AS COUNT
    FROM
  (
  SELECT TIMESTAMPDIFF
    (YEAR,
    dob,
    CURDATE()) AS age
  FROM
    customer ";
    
    if($from_date!="" && $to_date!=""){
       $sql.=" where date(timestamp) >= '$from_date' and date(timestamp) <= '$to_date'"; 
    }
    if($from_date!="" && $to_date==""){
       $sql.=" where date(timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
       $sql.=" where  date(timestamp) <= '$to_date'"; 
    }
    $sql.=") AS derived
    
  GROUP BY
    age_range
   
   ";
   $result=$this->db->query($sql);
   if($result->num_rows()>0){
       return $result->result_array();
   }
   else{
       return false;
   } 
}
public function vendor_getCustomerRegistrationsAnalytics($from_date,$to_date,$limit_result_val,$particular_year){
    //$sql="select CAST(count(name) as UNSIGNED) as count,DATE_FORMAT(timestamp, '%d/%m/%Y') as month from customer WHERE MONTH(timestamp) >= 1 AND MONTH(timestamp) <= 12 group by MONTH(timestamp)";
    
    if($particular_year==""){
        $sql="select COUNT(DISTINCT id) as count,year(timestamp) as y,(month(timestamp)-1) as m,'1' as d  from customer WHERE MONTH(timestamp) >= 1 AND MONTH(timestamp) <= 12 ";
        
        if($from_date!="" && $to_date!=""){
            $sql.=" and date(customer.timestamp) >= '$from_date' and date(customer.timestamp) <= '$to_date'"; 
         }
         if($from_date!="" && $to_date==""){
            $sql.=" and date(customer.timestamp) >= '$from_date' "; 
         }
         if($from_date=="" && $to_date!=""){
            $sql.=" and  date(customer.timestamp) <= '$to_date'"; 
         }
     
         $sql.="group by y,m";
    }else{
        $sql="SELECT COUNT(u.id) AS count,$particular_year as y,(month(u.timestamp)) as m,'1' as d ,m.month
      FROM (
            SELECT 'Jan' AS MONTH
            UNION SELECT 'Feb' AS MONTH
            UNION SELECT 'Mar' AS MONTH
            UNION SELECT 'Apr' AS MONTH
            UNION SELECT 'May' AS MONTH
            UNION SELECT 'Jun' AS MONTH
            UNION SELECT 'Jul' AS MONTH
            UNION SELECT 'Aug' AS MONTH
            UNION SELECT 'Sep' AS MONTH
            UNION SELECT 'Oct' AS MONTH
            UNION SELECT 'Nov' AS MONTH
            UNION SELECT 'Dec' AS MONTH
                       ) AS m
             LEFT JOIN customer u 
             ON MONTH(STR_TO_DATE(CONCAT(m.month),'%M ')) = MONTH(u.timestamp)
             AND YEAR(u.timestamp)='$particular_year'
             GROUP BY m.month,YEAR(u.timestamp)
             ORDER BY MONTH(STR_TO_DATE(CONCAT(m.month),'%M '))
    
             ";
    }
 
    
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
        return $result->result_array();
    }
    else{
        return false;
    }
 }
 public function vendor_revenue($from_date,$to_date, $gender='',$not_buy=''){

    if($not_buy!=''){
        
        $sql="select COUNT(history_orders.order_id) as order_count,IFNULL(sum(history_orders.grandtotal),0) as total_revenue,count(case when customer.gender='male' then 1 end) as Male, count(case when customer.gender='female' then 1 end) as Female,count(case when customer.gender='' then 1 end) as Others,count(customer.id) as total_custmers from customer 
            LEFT JOIN history_orders on customer.id =history_orders.customer_id ";


    }else{

        $sql="select COUNT(history_orders.order_id) as order_count,IFNULL(sum(history_orders.grandtotal),0) as total_revenue,count(case when customer.gender='male' then 1 end) as Male, count(case when customer.gender='female' then 1 end) as Female,count(case when customer.gender='' then 1 end) as Others,count(customer.id) as total_custmers,count(customer.id) as total_custmers from history_orders 
        LEFT JOIN customer on customer.id =history_orders.customer_id";
    }
    $s=1;
    if($from_date!="" && $to_date!=""){
        $s++;
        $sql.=" where date(customer.timestamp) >= '$from_date' and date(customer.timestamp) <= '$to_date'"; 
    }
    if($from_date!="" && $to_date==""){
        $s++;
        $sql.=" where date(customer.timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
        $s++;
        $sql.=" where  date(customer.timestamp) <= '$to_date'"; 
    }

    if($gender!=''){
        if($s==1){
            $s++;
            $sql.=" where ";
        }else{
            $sql.=" and ";
        }
        if($gender!=''){
            if($gender=='others'){
                $sql.=" customer.gender='' "; 
            }else{
                $sql.=" customer.gender='$gender' "; 
            }
            
        }
    }

    $user_type=$this->session->userdata("user_type");
    $a_id=$this->session->userdata("user_id");

    if($user_type=='vendor' && $not_buy==''){
        if($s==1){
            $s++;
            $sql.=" where ";
        }else{
            $sql.=" and ";
        }
        $sql .=" history_orders.vendor_id='$a_id' ";
    }

    if( $not_buy!='' ){
       
        $sql.=' GROUP by history_orders.order_id having total_revenue=0';

    }else{
        $sql.=' GROUP by history_orders.order_id';
    }

    //echo $sql;

    //$sql.=' group by customer.id';
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
        return $result->row_array();
    }else{
        return array();
    }


 }
public function vendor_getCustomerSubscribedAnalytics($from_date,$to_date){

    $sql="select count(newsletter.id) as total_subscriber, count(case when newsletter.customer_id='0' then 1 end) as anonymus_customer from newsletter "
    ;
    $s=1;
    if($from_date!="" && $to_date!=""){
        $s++;
        $sql.=" where date(newsletter.timestamp) >= '$from_date' and date(newsletter.timestamp) <= '$to_date'"; 
    }
    if($from_date!="" && $to_date==""){
        $s++;
        $sql.=" where date(newsletter.timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
        $s++;
        $sql.=" where  date(newsletter.timestamp) <= '$to_date'"; 
    }

    $user_type=$this->session->userdata("user_type");
    $a_id=$this->session->userdata("user_id");

    $result=$this->db->query($sql);
    if($result->num_rows()>0){
        return $result->row_array();
    }else{
        return false;
    }


 }
 public function vendor_getCustomerSustainabilityAnalytics($from_date,$to_date,$from_count,$to_count){

    $sql="select count(order_id) as purchase_count from history_orders ";

    if(($from_date!="" && $to_date!="") && ($from_date==$to_date)){
        $sql.=" where date(history_orders.timestamp) <='$from_date'";
    }else{
        if($from_date!="" && $to_date!=""){
            $sql.=" where date(history_orders.timestamp) >= '$from_date' and date(history_orders.timestamp) <= '$to_date'"; 
        }
    }
    $sql.=" group by order_id";

    //vendor_getCustomerSustainabilityAnalytics // not purchasecount - it should be days

    //having purchase_count>$from_count and purchase_count<=$to_count
    
    $user_type=$this->session->userdata("user_type");
    $a_id=$this->session->userdata("user_id");

    $result=$this->db->query($sql);
    
    if($result->num_rows()>0){
        return $result->row_array();
    }else{
        return false;
    }

 }
 
 public function customer_number_of_visit($from_date,$to_date){

    $sql="select customer.id as customer_id, count(visitor_data.id) as visit_count  from visitor_data 
        LEFT JOIN customer on customer.id =visitor_data.user_type
        where visitor_data.user_type!='guest'
    ";
    /*
    if($from_date!="" && $to_date!=""){
       
        $sql.=" and date(visitor_data.timestamp) >= '$from_date' and date(visitor_data.timestamp) <= '$to_date' "; 
    }
    if($from_date!="" && $to_date==""){
        
        $sql.=" and date(visitor_data.timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
        
        $sql.=" and  date(visitor_data.timestamp) <= '$to_date' "; 
    }
*/
    $sql.=" group by visitor_data.user_type having visit_count>0";

    $result=$this->db->query($sql);
    
    if($result->num_rows()>0){
        return $result->result_array();
    }else{
        return false;
    }
 }

 public function customer_values($customer_id){

    $sql="select customer.id as customer_id, IFNULL(sum(carttable.inventory_moq*carttable.selling_price),0) as cart_price, IFNULL(sum(carttable.total_price_of_product_without_promotion+carttable.total_price_of_product_with_promotion),0) as cart_price_promotion, IFNULL(sum(history_orders.grandtotal),0) as purchased_value  from customer 
        
        LEFT JOIN carttable on carttable.customer_id = customer.id 
        LEFT JOIN history_orders on history_orders.customer_id = customer.id 
        where customer.id='$customer_id' 
    
    ";

    $user_type=$this->session->userdata("user_type");
    $a_id=$this->session->userdata("user_id");

    if($user_type=='vendor'){
        $sql.=" and carttable.vendor_id = '$a_id'";
    }

    $result=$this->db->query($sql);
    
    if($result->num_rows()>0){
        return $result->result_array();
    }else{
        return false;
    }

 }
 public function abandoned_cart_value($from_date,$to_date){

    $sql="select IFNULL(sum(carttable.inventory_moq*carttable.selling_price),0) as cart_price, IFNULL(sum(carttable.total_price_of_product_without_promotion+carttable.total_price_of_product_with_promotion),0) as cart_price_promotion from carttable ";

    if($from_date!="" && $to_date!=""){
       
        $sql.=" where  date(carttable.timestamp) >= '$from_date' and date(carttable.timestamp) <= '$to_date' "; 
    }
    if($from_date!="" && $to_date==""){
        
        $sql.=" where date(carttable.timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
        
        $sql.=" where  date(carttable.timestamp) <= '$to_date' "; 
    }

    $user_type=$this->session->userdata("user_type");
    $a_id=$this->session->userdata("user_id");

    if($user_type=='vendor'){
        $sql.=" and carttable.vendor_id = '$a_id'";
    }
    
    //echo $sql;
    //exit;

    $result=$this->db->query($sql);

    if($result->num_rows()>0){
        return $result->result_array();
    }else{
        return false;
    }

 }

 public function get_visitors_traffic_data($from_date,$to_date){

    $sql="select count(id) as number_of_visit from visitor_data  ";

    $sql.=" where visitor_data.user_type!='guest' ";

    if($from_date!="" && $to_date!=""){
       
        $sql.=" and  date(visitor_data .timestamp) >= '$from_date' and date(visitor_data .timestamp) <= '$to_date' "; 
    }
    if($from_date!="" && $to_date==""){
        
        $sql.=" and date(visitor_data .timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
        
        $sql.=" and  date(visitor_data .timestamp) <= '$to_date' "; 
    }

    $sql.=' group by visitor_data.user_type';

    $result=$this->db->query($sql);

    if($result->num_rows()>0){
        return $result->result_array();
    }else{
        return false;
    }

 }
 public function get_visitor_request_page($from_date,$to_date){

    $sql="select  request_page from visitor_data  ";

    $sql.=" where visitor_data.user_type!='guest' ";

    if($from_date!="" && $to_date!=""){
       
        $sql.=" and  date(visitor_data .timestamp) >= '$from_date' and date(visitor_data .timestamp) <= '$to_date' "; 
    }
    if($from_date!="" && $to_date==""){
        
        $sql.=" and date(visitor_data .timestamp) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
        
        $sql.=" and  date(visitor_data .timestamp) <= '$to_date' "; 
    }

    //$sql.=' group by visitor_data.user_type';

    $result=$this->db->query($sql);

    if($result->num_rows()>0){
        return $result->result_array();
    }else{
        return false;
    }

 }
 public function get_top_viewed_pages($from_date,$to_date,$limit_result_val){

    $sql="select view_count,sku_id,sku_name,image,attribute_1,attribute_1_value,attribute_2,attribute_2_value,category.cat_name,subcategory.subcat_name,brands.brand_name,products.product_name from  inventory ";

    $sql.=' LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`';
    //$sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`';
    $sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`';
    $sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id`';
    $sql.=' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`';

    $sql.=" where inventory.active='1' ";

    if($from_date!="" && $to_date!=""){
       
        $sql.=" and  date(inventory.view_count_updated_on) >= '$from_date' and date(inventory.view_count_updated_on) <= '$to_date' "; 
    }
    if($from_date!="" && $to_date==""){
        
        $sql.=" and date(inventory .view_count_updated_on) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
        
        $sql.=" and  date(inventory .view_count_updated_on) <= '$to_date' "; 
    }

    $sql.=" order by view_count desc ";
    
    if($limit_result_val!=""){
        $sql.=" limit $limit_result_val ";
    }

    $result=$this->db->query($sql);

    if($result->num_rows()>0){
        return $result->result_array();
    }else{
        return false;
    }

 }
 public function get_bottom_viewed_pages($from_date,$to_date,$limit_result_val){

    $sql="select view_count,sku_id,sku_name,image,attribute_1,attribute_1_value,attribute_2,attribute_2_value,category.cat_name,subcategory.subcat_name,brands.brand_name,products.product_name from  inventory ";

    $sql.=' LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`';
    //$sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`';
    $sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`';
    $sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id`';
    $sql.=' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`';

    $sql.=" where inventory.active='1' ";

    if($from_date!="" && $to_date!=""){
       
        $sql.=" and  date(inventory.view_count_updated_on) >= '$from_date' and date(inventory.view_count_updated_on) <= '$to_date' "; 
    }
    if($from_date!="" && $to_date==""){
        
        $sql.=" and date(inventory .view_count_updated_on) >= '$from_date' "; 
    }
    if($from_date=="" && $to_date!=""){
        
        $sql.=" and  date(inventory .view_count_updated_on) <= '$to_date' "; 
    }

    $sql.=" order by view_count asc ";
    
    if($limit_result_val!=""){
        $sql.=" limit $limit_result_val ";
    }

    $result=$this->db->query($sql);

    if($result->num_rows()>0){
        return $result->result_array();
    }else{
        return false;
    }

}


 
 

 public function profit_for_today(){

    $total_revenue_today=0;
    $total_revenue_yesterday=0;
    $total_revenue=0;
    $total_sales_today=0;
    $total_sales_yesterday=0;
    $total_sales=0;

    $sql="select IFNULL(sum(product_price-inventory.cost_price),0) as total_profit_today, IFNULL(sum(grandtotal),0) as total_revenue_today,IFNULL(sum(product_price),0) as total_sales_today from completed_orders";

    $user_type=$this->session->userdata("user_type");
    $a_id=$this->session->userdata("user_id");

    
    
    $sql.=" left join inventory on completed_orders.inventory_id=inventory.id 
    left join products on inventory.product_id=products.product_id ";
    
    $sql.=" where DATE(completed_orders.timestamp) = DATE(now())";
    if($user_type=='vendor'){
        $sql .=" AND ";
        $sql .=" completed_orders.product_vendor_id= '$a_id' ";
    }

    $sql1="SELECT IFNULL(sum(product_price-inventory.cost_price),0) as total_profit_yesterday,IFNULL(sum(grandtotal),0) as total_revenue_yesterday,IFNULL(sum(product_price),0) as total_sales_yesterday FROM completed_orders ";
    $sql1.=" left join inventory on completed_orders.inventory_id=inventory.id 
    left join products on inventory.product_id=products.product_id ";

    $sql1.=" WHERE DATE(completed_orders.timestamp) = DATE(NOW() - INTERVAL 1 DAY)";

    if($user_type=='vendor'){
        $sql1 .=" AND ";
        $sql1 .=" completed_orders.product_vendor_id= '$a_id' ";
    }

    $sql2="SELECT IFNULL(sum(product_price-inventory.cost_price),0) as total_revenue, IFNULL(sum(product_price),0) as total_sales  FROM completed_orders";
    $sql2.=" left join inventory on completed_orders.inventory_id=inventory.id 
    left join products on inventory.product_id=products.product_id ";

    if($user_type=='vendor'){
        $sql2 .=" AND ";
        $sql2 .=" completed_orders.product_vendor_id= '$a_id' ";
    }
    $result=$this->db->query($sql);
    if($result->num_rows()>0){
        $total_revenue_today=$result->row()->total_profit_today;
        $total_sales_today=$result->row()->total_sales_today;
    }

    $result1=$this->db->query($sql1);
    if($result1->num_rows()>0){
        $total_revenue_yesterday=$result1->row()->total_profit_yesterday;
        $total_sales_yesterday=$result1->row()->total_sales_yesterday;
    }

    $result2=$this->db->query($sql2);
    if($result2->num_rows()>0){
        $total_revenue=$result2->row()->total_revenue;
        $total_sales=$result2->row()->total_sales;
    }

    return array('total_revenue_today'=>$total_revenue_today,'total_revenue_yesterday'=>$total_revenue_yesterday,'total_revenue'=>$total_revenue,'total_sales_today'=>$total_sales_today,'total_sales_yesterday'=>$total_sales_yesterday,'total_sales'=>$total_sales);

 }
 
 public function get_order_details($type){
    $user_type=$this->session->userdata("user_type");
    $a_id=$this->session->userdata("user_id");

    $total_orders=0;
    if($type=='total'){
        $sql="select count(*) as total_orders from history_orders";

        if($user_type=='vendor'){
            $sql .=" where ";
            $sql .=" history_orders.product_vendor_id= '$a_id' ";
        }

        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            $total_orders=$result->row()->total_orders;
            return $total_orders;
        }
    }
    if($type=='cat'){
        $sql="select count(*) as total_orders , category.cat_name, parent_category.pcat_name from history_orders ";
        $sql.= ' LEFT JOIN `products` ON `products`.`product_id` = `history_orders`.`product_id`'
				.' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`'
				.' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`'
				.' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`'
				.' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id`';

                if($user_type=='vendor'){
                    $sql .=" where ";
                    $sql .=" history_orders.product_vendor_id= '$a_id' ";
                }
        $sql.="GROUP BY  `category`.`cat_id` ";
		
        $result=$this->db->query($sql);
        
        return $result->result();
        
    }
 }
 public function getRevenueOnYearBasis($year){
    $user_type=$this->session->userdata("user_type");
    $a_id=$this->session->userdata("user_id");

  $sql_m=  "select sum(grandtotal) as total_revenue,month(timestamp) as month_number,monthname(timestamp) as m_name,LEFT(DATE_FORMAT(timestamp, '%M'), 3) AS Short from history_orders where year(timestamp) = '$year' ";
    if($user_type=='vendor'){
        $sql_m .=" and ";
        $sql_m .=" history_orders.product_vendor_id= '$a_id' ";
    }

    $sql_m.=" group by month(timestamp) ";
  $result=$this->db->query($sql_m);
  
  return $result->result();

 }
 public function profileRegisterCount($year){

  $sql_m=  "select count(*) as total_register,month(timestamp) as month_number,monthname(timestamp) as m_name,LEFT(DATE_FORMAT(timestamp, '%M'), 3) AS Short from customer where year(timestamp) = '$year' ";
 
  $sql_m.=" group by month(timestamp) ";

  $result=$this->db->query($sql_m);
        
  return $result->result();

 }
 public function getRevenueOnYearBasis_revenue($year){
  
    $user_type=$this->session->userdata("user_type");
    $a_id=$this->session->userdata("user_id");

  $sql="select IFNULL(sum(grandtotal),0) as total_revenue from history_orders where year(timestamp) = '$year' ";

  if($user_type=='vendor'){
    $sql .=" and ";
    $sql .=" history_orders.product_vendor_id= '$a_id' ";
}
  $result=$this->db->query($sql);
        
  return $result->row()->total_revenue;

 }
public function get_overall_payments(){
    $user_type=$this->session->userdata("user_type");
    $a_id=$this->session->userdata("user_id");

  $sql="select IFNULL(sum(grandtotal),0) as total_revenue from history_orders where payment_type!='cod'";

  if($user_type=='vendor'){
    $sql .=" and ";
    $sql .=" history_orders.product_vendor_id= '$a_id' ";
}
  $result=$this->db->query($sql);
        
  return $result->row()->total_revenue;

 }
 public function refund_requests_for(){
  
  $sql="select IFNULL(sum(total_refund_without_concession),0) as total_refund from return_refund ";
  $sql.=" LEFT JOIN history_orders ON history_orders.order_item_id = return_refund.order_item_id ";
  $user_type=$this->session->userdata("user_type");
  $a_id=$this->session->userdata("user_id");
  if($user_type=='vendor'){
    $sql .=" where ";
    $sql .=" history_orders.product_vendor_id= '$a_id' ";
    }
  $result=$this->db->query($sql);
        
  return $result->row()->total_refund;

 }
 public function get_wallet_amount_total(){
  
  $sql="select IFNULL(sum(wallet_amount),0) as total_amount from wallet";
  $result=$this->db->query($sql);
        
  return $result->row()->total_amount;

 }

 public function get_wallet_amounts_customer(){
  
    $sql="select * from wallet 
    LEFT JOIN customer ON customer.id = wallet.customer_id order by wallet_amount desc";
    $result=$this->db->query($sql);
            
    return $result->result();

}
public function get_orders_data(){
    $user_type=$this->session->userdata("user_type");
    $a_id=$this->session->userdata("user_id");
    $ord=array();

    $sql="select count(*) as processing_orders,IFNULL(sum(grandtotal),0) as total_amount from orders_status 
    LEFT JOIN history_orders ON history_orders.order_item_id = orders_status.order_item_id ";
    $sql.=" where order_delivered=0 and order_cancelled=0";

    if($user_type=='vendor'){
        $sql .=" and ";
        $sql .=" history_orders.product_vendor_id= '$a_id' ";
    }

    $result=$this->db->query($sql);         
    $ord["processing_orders"]=$result->row()->processing_orders;
    $ord["processing_orders_total_amount"]=$result->row()->total_amount;

    $sql="select count(*) as deliverd_orders,IFNULL(sum(grandtotal),0) as total_amount from orders_status 
    LEFT JOIN history_orders ON history_orders.order_item_id = orders_status.order_item_id ";

   
    $sql.=" where order_delivered=1 and order_cancelled=0";
    if($user_type=='vendor'){
        $sql .=" and ";
        $sql .=" history_orders.product_vendor_id= '$a_id' ";
    }

    $result=$this->db->query($sql);         
    $ord["delivered_orders"]=$result->row()->deliverd_orders;
    $ord["delivered_orders_total_amount"]=$result->row()->total_amount;

    $sql="select count(*) as cancelled_orders,IFNULL(sum(grandtotal),0) as total_amount from orders_status 
    LEFT JOIN history_orders ON history_orders.order_item_id = orders_status.order_item_id ";
    $sql.=" where order_delivered=0 and order_cancelled=1";

    if($user_type=='vendor'){
        $sql .=" and ";
        $sql .=" history_orders.product_vendor_id= '$a_id' ";
    }

    $result=$this->db->query($sql);         
    $ord["cancelled_orders"]=$result->row()->cancelled_orders;
    $ord["cancelled_orders_total_amount"]=$result->row()->total_amount;

    $sql="select count(*) as return_orders,IFNULL(sum(grandtotal),0) as total_amount from orders_status 
    LEFT JOIN history_orders ON history_orders.order_item_id = orders_status.order_item_id ";
    $sql.=" where order_delivered=1 and return_order_id!=''";

    if($user_type=='vendor'){
        $sql .=" and ";
        $sql .=" history_orders.product_vendor_id= '$a_id' ";
    }
    $result=$this->db->query($sql);         
    $ord["return_orders"]=$result->row()->return_orders;
    $ord["return_orders_total_amount"]=$result->row()->total_amount;


    $sql="select count(*) as failed_orders,IFNULL(sum(grandtotal),0) as total_amount from failed_orders 
    ";
    $sql.=" ";
    if($user_type=='vendor'){
        $sql .=" where ";
        $sql .=" failed_orders.product_vendor_id= '$a_id' ";
    }
    $result=$this->db->query($sql);         
    $ord["failed_orders"]=$result->row()->failed_orders;
    $ord["failed_orders_total_amount"]=$result->row()->total_amount;

    return $ord;

}


public function get_products_purchase_data($from_date,$to_date){
    $user_type=$this->session->userdata("user_type");
    $a_id=$this->session->userdata("user_id");
    $sql="select IFNULL(sum(grandtotal),0) as total_revenue,IFNULL(sum(quantity),0) as total_sales, sku_id from completed_orders ";
    if($from_date!="" && $to_date!=""){
        $sql.=" where date(completed_orders.timestamp) >= '$from_date' and date(completed_orders.timestamp) <= '$to_date'"; 
    }
    if($user_type=='vendor'){
        $sql .=" and ";
        $sql .=" completed_orders.product_vendor_id= '$a_id' ";
    }
    $sql.=" group by completed_orders.sku_id";

    $result=$this->db->query($sql);   
    
    return $result->result();
    
}
public function getProfitAndSales($from_date,$to_date){
    $user_type=$this->session->userdata("user_type");
    $a_id=$this->session->userdata("user_id");
    /* profit */
    $sql="select IFNULL(sum(inventory.cost_price),0) as total_cost_price, IFNULL(sum(product_price-inventory.cost_price),0) as total_profit, IFNULL(sum(product_price),0) as total_sales from completed_orders";

    $sql.=" left join inventory on completed_orders.inventory_id=inventory.id 
    left join products on inventory.product_id=products.product_id ";
    $st=1;
    if($from_date!="" && $to_date!=""){
        $st=2;
        $sql.=" where date(completed_orders.timestamp) >= '$from_date' and date(completed_orders.timestamp) <= '$to_date'"; 
    }


    if($user_type=='vendor'){
        if($st==2){
        $sql .=" and ";
        }
        if($st==1){
            $sql .=" where ";
            }
        $sql .=" completed_orders.product_vendor_id= '$a_id' ";
    }
    //$sql.=" group by completed_orders.sku_id";

    $result=$this->db->query($sql);   
    
    return $result->row();
}


}
?>