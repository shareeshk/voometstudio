<?php
class Site_blog extends CI_model{
	public function unPublishComment($id){
	$sql="update blogs_comment set active=0 where id='$id'";
	$result=$this->db->query($sql);	
	if($this->db->affected_rows()>0){
		return true;
	}
	else{
		return false;
	}
	}
	public function deleteComment($id){
		$sql="delete from blogs_comment  where id='$id'";
		$result=$this->db->query($sql);	
		if($this->db->affected_rows()>0){
			return true;
		}
		else{
			return false;
		}
	}
	public function publishComment($id){
	$sql="update blogs_comment set active=1 where id='$id'";
	$result=$this->db->query($sql);	
	if($this->db->affected_rows()>0){
		return true;
	}
	else{
		return false;
	}
	}
	public function all_blogs_published_comments_processing($params,$fg){
        //define index of column
        $columns = array( 
            0 =>'blogs_comment.id', 
            1 => 'blog.blog_title',
            2 => 'blog.slug',
			3 => 'blogs_comment.customer_name',
			4 => 'blogs_comment.message',
			5 => 'blogs_comment.blog_id',
        );

        $where = $sqlTot = $sqlRec = "";
        $customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT blogs_comment.*,blog.slug,blog.blog_title,date_format(blogs_comment.timestamp,'%a,%e %b %Y') as timestamp from `blogs_comment` inner join blog on blog.blog_id=blogs_comment.blog_id where blogs_comment.active=1  ";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" (  date_format(blogs_comment.timestamp,'%a,%e %b %Y')  LIKE '%".$params['search']['value']."%' ";
            $where .="  or blog.blog_title LIKE '%".addslashes($params['search']['value'])."%' ";
            $where .="  or blogs_comment.customer_name LIKE '%".addslashes($params['search']['value'])."%' ";
			$where .="  or blogs_comment.message LIKE '%".addslashes($params['search']['value'])."%' )";
            //$where .="  blog.villa_type LIKE '%".addslashes($params['search']['value'])."%' ";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            //echo $sqlTot; exit;
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
           // echo $sqlRec; exit;
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }	
	
	public function all_blogs_un_published_comments_processing($params,$fg){
        //define index of column
        $columns = array( 
            0 =>'blogs_comment.id', 
            1 => 'blog.blog_title',
            2 => 'blog.slug',
			3 => 'blogs_comment.customer_name',
			4 => 'blogs_comment.message',
			5 => 'blogs_comment.blog_id',
        );

        $where = $sqlTot = $sqlRec = "";
        $customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT blogs_comment.*,blog.slug,blog.blog_title,date_format(blogs_comment.timestamp,'%a,%e %b %Y') as timestamp from `blogs_comment` inner join blog on blog.blog_id=blogs_comment.blog_id where blogs_comment.active=0  ";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" (  date_format(blogs_comment.timestamp,'%a,%e %b %Y')  LIKE '%".$params['search']['value']."%' ";
            $where .="  or blog.blog_title LIKE '%".addslashes($params['search']['value'])."%' ";
            $where .="  or blogs_comment.customer_name LIKE '%".addslashes($params['search']['value'])."%' ";
			$where .="  or blogs_comment.message LIKE '%".addslashes($params['search']['value'])."%' )";
            //$where .="  blog.villa_type LIKE '%".addslashes($params['search']['value'])."%' ";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            //echo $sqlTot; exit;
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
           // echo $sqlRec; exit;
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }
	
	
	public function publish($slug){
		$sql="update blog set blogger_request_publish=1,admin_publish=1 where slug='$slug'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function request_unpublish_cancel($slug){
		$sql="update blog set blogger_request_publish=1,admin_publish=1,blogger_request_unpublish=0 where slug='$slug'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function request_unpublish_accept($slug){
		$sql="update blog set blogger_request_publish=0,admin_publish=0,blogger_request_unpublish=0 where slug='$slug'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function unpublish($slug){
		$sql="update blog set blogger_request_publish=1,admin_publish=0 where slug='$slug'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows()>0){
			return true;
		}else{
			return false;
		}
	}
    
    public function all_blogs_unpublished_processing($params,$fg){
        //define index of column
        $columns = array( 
            0 =>'blog.id', 
            1 => 'blog.blog_id',
            2 => 'blog.blog_title',
            3 => 'blog.blogger_request_publish',
            4 => 'blog.admin_publish',
            5 => 'blog.blogger_request_unpublish',
            6 => 'blog.topic_sel',
            7 => 'blog.slug',
        );

        $where = $sqlTot = $sqlRec = "";
        $customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT blog.*,date_format(blog.timestamp,'%a,%e %b %Y') as timestamp,blog_topic.topic_sel from `blog` inner join blog_topic on blog_topic.topic_id=blog.topic_sel where blog.blogger_request_publish=1 and blog.admin_publish=0 ";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" (  date_format(blog.timestamp,'%a,%e %b %Y')  LIKE '%".$params['search']['value']."%' ";
            $where .="  or blog.blog_title LIKE '%".addslashes($params['search']['value'])."%' ";
            $where .="  or blog.topic_sel LIKE '%".addslashes($params['search']['value'])."%' ";
			$where .="  or blog_topic.topic_sel LIKE '%".addslashes($params['search']['value'])."%' )";
            //$where .="  blog.villa_type LIKE '%".addslashes($params['search']['value'])."%' ";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            //echo $sqlTot; exit;
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
           // echo $sqlRec; exit;
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }
    public function all_blogs_published_processing($params,$fg){
        //define index of column
        $columns = array( 
            0 =>'blog.id', 
            1 => 'blog.blog_id',
            2 => 'blog.blog_title',
            3 => 'blog.blogger_request_publish',
            4 => 'blog.admin_publish',
            5 => 'blog.blogger_request_unpublish',
            6 => 'blog.topic_sel',
            7 => 'blog.slug',
        );

        $where = $sqlTot = $sqlRec = "";
        $customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT blog.*,date_format(blog.timestamp,'%a,%e %b %Y') as timestamp,blog_topic.topic_sel from `blog` inner join blog_topic on blog_topic.topic_id=blog.topic_sel where blog.blogger_request_publish=1 and blog.admin_publish=1 ";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" (  date_format(blog.timestamp,'%a,%e %b %Y')  LIKE '%".$params['search']['value']."%' ";
            $where .="  or blog.blog_title LIKE '%".addslashes($params['search']['value'])."%' ";
            $where .="  or blog.topic_sel LIKE '%".addslashes($params['search']['value'])."%' ";
			$where .="  or blog_topic.topic_sel LIKE '%".addslashes($params['search']['value'])."%' )";
            //$where .="  blog.villa_type LIKE '%".addslashes($params['search']['value'])."%' ";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            //echo $sqlTot; exit;
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
           // echo $sqlRec; exit;
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }
  
	
	public function add_blog_image($blog_id_for_image,$file_location){
		$sql="update blog set blog_image='$file_location' where id='$blog_id_for_image'";
		$res=$this->db->query($sql);
		return $res;
	}
	
	public function post_to_frontend($blog_id,$post){
		if($post=="post"){
			$sql="select id from blog where post_to_frontend='1' and admin_publish='1'";
			$res=$this->db->query($sql);
			if($res->num_rows()<=1){
				$sql_update="update blog set post_to_frontend='1' where id='$blog_id'";
				$res_update=$this->db->query($sql_update);
			}
			if($res->num_rows()>1){
				foreach($res->result_array() as $k => $resArr){
					if($k!=0){
						$sql_update="update blog set post_to_frontend='0' where id='$resArr[id]'";
						$res_update=$this->db->query($sql_update);
					}
				}
				$sql_update="update blog set post_to_frontend='1' where id='$blog_id'";
				$res_update=$this->db->query($sql_update);
			}
			return $res_update;
		}
		if($post=="unpost"){			
			$sql_update="update blog set post_to_frontend='0' where id='$blog_id'";
			$res_update=$this->db->query($sql_update);
			return $res_update;
		}
	}
	
	  public function update_blog_content($blog_content,$blog_id,$customer_id){
        $sql="update blog set blog_content=".$this->db->escape($blog_content)." where blog_id='$blog_id' and customer_id='$customer_id'";
        $result=$this->db->query($sql);
        if($this->db->affected_rows()>0){
            return 1;
        }else{
            if($this->db->_error_message()){
                return false;
            }else{
                return 2;
            }
            
        }
    }
	
	function get_blog_topic_data(){
		$sql="select * from blog_topic order by id asc";
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	function get_blog_tags_data(){
		$sql="select * from blog_tag order by id asc";
		$result=$this->db->query($sql);
		return $result->result_array();
	}
	function check_slugg($blog_title){
        $slug = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($blog_title)));
        $sql="select COUNT(*) AS NumHits  from blog where blog_title  LIKE '%$slug%'";
        $query=$this->db->query($sql);
        if($query->num_rows()>0){
            $row=$query->row_array();
			$numHits=$row["NumHits"];
            return ($numHits > 0) ? ($slug . '-' . $numHits) : $slug;
        }else{
            return $slug;
        }
    }
	function blog_action_add_initials($blog_date,$topic_sel,$tag_sel,$blog_title,$blog_id,$customer_id){
		$slug_field=$this->check_slugg($blog_title);
		$sql="insert into blog set blog_date='$blog_date',slug='$slug_field', topic_sel='$topic_sel', tag_sel='$tag_sel',blog_title='$blog_title',blog_id='$blog_id',customer_id='$customer_id'";
		$result=$this->db->query($sql);
		return $result;
	}
	
	function get_all_blog_data($blog_id,$customer_id){
        $sql="select * from blog where blog_id='$blog_id' and customer_id='$customer_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row_array();
		}
		else{
            return false;
        }
    }
	function update_initial_blog($topic_sel,$tag_sel,$blog_title,$blog_content,$blog_id,$customer_id,$file_location,$blog_image_for_content_exists){
		$slug_field=$this->check_slugg($blog_title);
		$blogger_request_publish=1;
		$sql="update blog set slug='$slug_field',topic_sel='$topic_sel', tag_sel='$tag_sel',blog_title='$blog_title',blog_content='$blog_content', blogger_request_publish='$blogger_request_publish'";
		if($blog_image_for_content_exists=="no"){
			$sql.=",blog_image_for_content='$file_location'";
		}
		if($blog_image_for_content_exists=="yes" && $file_location!=""){
			$sql.=",blog_image_for_content='$file_location'";
		}
		$sql.=" where blog_id='$blog_id' and customer_id='$customer_id'";
		$query=$this->db->query($sql);
		return $query;
	}
}
?>