<?php
class Recomendations_model extends CI_Model{
    
        public function recomendations_processing($params,$fg){
        //define index of column
        $columns = array( 
            0 =>'recomendation_recepient.id', 
            2 => 'recomendation_recepient.recomendation_uid',
            3 => 'recomendation_recepient.timestamp',
            4 => 'recomendation_recepient.name_of_items'
        );

        $where = $sqlTot = $sqlRec = "";
        //$customer_id=$this->session->userdata('customer_id');
        // getting total number records without any search
        $sql = "SELECT * from recomendation_recepient where 1=1 ";
        // check search value exist
        if( !empty($params['search']['value']) ){   
            $where .=" and ";
            $where .=" ( recomendation_recepient.id LIKE '%".$params['search']['value']."%' ";
            $where .=" OR recomendation_recepient.name_of_items LIKE '%".$params['search']['value']."%' )";
        }
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        
        $sqlRec .=  " ORDER BY recomendation_recepient.timestamp   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
        
        
        if($fg=="get_total_num_recs"){
            $queryTot=$this->db->query($sqlTot);
            $totalRecords=$queryTot->num_rows();
            return $totalRecords;
        }
        if($fg=="get_recs"){
            $queryRecords=$this->db->query($sqlRec);
            return $queryRecords->result();
        }
    }
    
    public function add_receipient($recomendation_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector,$name_of_products_tagged_for_purchasing){
        if($products_tagged_for_purchasing!=""){
           $inv_id_arr=explode(',',$products_tagged_for_purchasing);
           
            foreach($inv_id_arr as $inv_id){
                $sqlen="select * from recomendation_recepient where inv_id='$inv_id' and recomendation_uid='$recomendation_uid'";
                $resultenq=$this->db->query($sqlen);
                if($resultenq->num_rows()==0){
                $sql="insert into recomendation_recepient set recomendation_uid='$recomendation_uid',pcat_id='$parent_category',cat_id='$category',subcat_id='$sub_category',
        brand_id='$attributes',product_id='$attributes_selector', inv_id='$inv_id',name_of_items='".$this->db->escape_str($name_of_products_tagged_for_purchasing)."'
        ";
            $result=$this->db->query($sql);
               }
            } 
        }else{
            $sql="insert into recomendation_recepient set recomendation_uid='$recomendation_uid',pcat_id='$parent_category',cat_id='$category',subcat_id='$sub_category'
        ,brand_id='$attributes',product_id='$attributes_selector', inv_id='',name_of_items='".$this->db->escape_str($name_of_products_tagged_for_purchasing)."'
        ";
            $result=$this->db->query($sql);
        }
    
        if($this->db->affected_rows()){
            return true;
        }
        else{
            return false;
        }  
    }
public function update_receipient($recomendation_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector,$name_of_products_tagged_for_purchasing){
                
            
       if($products_tagged_for_purchasing!=""){
           $inv_id_arr=explode(',',$products_tagged_for_purchasing);
           $i=0;
            foreach($inv_id_arr as $inv_id){
                $sqlen="select * from recomendation_recepient where  recomendation_uid='$recomendation_uid'";
                $resultenq=$this->db->query($sqlen);
                if($resultenq->num_rows()>0){
                    if($i<=$resultenq->num_rows()){
                        $sql="update recomendation_recepient set pcat_id='$parent_category',cat_id='$category',subcat_id='$sub_category',
        brand_id='$attributes',product_id='$attributes_selector', inv_id='$inv_id',name_of_items='".$this->db->escape_str($name_of_products_tagged_for_purchasing)."' where recomendation_uid='$recomendation_uid'
        ";
                         $result=$this->db->query($sql);
                    }else{
                        $sql="insert into recomendation_recepient set recomendation_uid='$recomendation_uid',pcat_id='$parent_category',cat_id='$category',subcat_id='$sub_category',brand_id='$attributes',product_id='$attributes_selector', inv_id='$inv_id',name_of_items='".$this->db->escape_str($name_of_products_tagged_for_purchasing)."'";
                        $result=$this->db->query($sql);
                    }

               }
               else{
                   $sql="insert into recomendation_recepient set recomendation_uid='$recomendation_uid',pcat_id='$parent_category',cat_id='$category',subcat_id='$sub_category',
            brand_id='$attributes',product_id='$attributes_selector', inv_id='$inv_id',name_of_items='".$this->db->escape_str($name_of_products_tagged_for_purchasing)."'";
                   $result=$this->db->query($sql);
               }
$i++;
            } 
        }else{ 
            $sql="update recomendation_recepient set pcat_id='$parent_category',cat_id='$category',subcat_id='$sub_category',brand_id='$attributes',product_id='$attributes_selector', inv_id='',name_of_items='".$this->db->escape_str($name_of_products_tagged_for_purchasing)."',timestamp=now() where recomendation_uid='$recomendation_uid'";
            $result=$this->db->query($sql);
           $row_num=$this->db->affected_rows();
           
           $sql="select id from recomendation_recepient where recomendation_uid='$recomendation_uid'";
           $result=$this->db->query($sql);
           if($result->num_rows()>0){
               $num_rows=$result->num_rows();
               $k=0;
               foreach($result->result_array() as $data){
                   $ids=$data['id'];
                   if($k<$num_rows-1){
                        $sql="delete from recomendation_recepient where  recomendation_uid='$recomendation_uid' and id='$ids'";
                        $result=$this->db->query($sql);
                   }
                       
                   $k++;
               }
           }
            
        }
        if($this->db->affected_rows()>0){
                return true;
            }else{
                return false;
            }
}
     public function getRecepientdata($recomendation_uid){
         $sql=" select * from recomendation_recepient where recomendation_uid='$recomendation_uid' order by timestamp desc";
         $result=$this->db->query($sql);
         if($result->num_rows()>0){
             return $result->result_array();
         }
        else{
            return false;
        }
     }
     public function getRecepientTaggeddataFirstLevel($recomendation_uid){
         $sql=" select * from recomendation_tagged where recomendation_uid='$recomendation_uid' and recomendation_level='first_level'";
         $result=$this->db->query($sql);
         if($result->num_rows()>0){
             return $result->result_array();
         }
        else{
            return false;
        }
     }
     public function getRecepientTaggedSecondLevel($recomendation_uid){
         $sql=" select * from recomendation_tagged where recomendation_uid='$recomendation_uid' and recomendation_level='second_level'";
         $result=$this->db->query($sql);
         if($result->num_rows()>0){
             return $result->result_array();
         }
        else{
            return false;
        }
     }
     public function getRecepientTaggedThirdLevel($recomendation_uid){
         $sql=" select * from recomendation_tagged where recomendation_uid='$recomendation_uid' and recomendation_level='third_level'";
         $result=$this->db->query($sql);
         if($result->num_rows()>0){
             return $result->result_array();
         }
        else{
            return false;
        }
     }
     public function getSKUId($inventoryId){
         $sql="select sku_id from inventory where id='$inventoryId'";
         $result=$this->db->query($sql);
         if($result->num_rows()>0){
             return $result->row()->sku_id;
         }else{
             return "Not Available";
         }
     }
     public function getProductName($productId){
         $sql="select product_name from products where product_id='$productId'";
         $result=$this->db->query($sql);
         if($result->num_rows()>0){
             return $result->row()->product_name;
         }else{
             return "Not Available";
         }
     }
     public function getRecepientLastdata($recomendation_uid){
         $sql=" select name_of_items from recomendation_recepient where recomendation_uid='$recomendation_uid' order by timestamp desc limit 1";
         $result=$this->db->query($sql);
         if($result->num_rows()>0){
             return $result->row()->name_of_items;
         }
        else{
            return false;
        }
     }
     public function add_items_tagged_under_recomendations($recomendation_uid,$products_tagged_for_recomendation,$parent_category,$category,$sub_category,$attributes,$attributes_selector,$level_recomendation){
             
         $sql="select product_id,inv_id from recomendation_tagged where recomendation_uid='$recomendation_uid' and recomendation_level='$level_recomendation' and product_id='$attributes_selector'";
         $result=$this->db->query($sql);
         if($result->num_rows()==1){
             $products_tagged_for_recomendation=$result->row()->inv_id.','.$products_tagged_for_recomendation;
             $sql="update recomendation_tagged set inv_id='$products_tagged_for_recomendation' where recomendation_uid='$recomendation_uid' and recomendation_level='$level_recomendation' and product_id='$attributes_selector'";
             $result=$this->db->query($sql);
             if($this->db->affected_rows()>0){
                     return true;
                 }else{
                     return false;
                 }
          }else{
              $sql="insert into recomendation_tagged set recomendation_uid='$recomendation_uid',recomendation_level='$level_recomendation',pcat_id='$parent_category',cat_id='$category',subcat_id='$sub_category',brand_id='$attributes',product_id='$attributes_selector',inv_id='$products_tagged_for_recomendation'";
             $result=$this->db->query($sql);
             if($this->db->affected_rows()>0){
                 return true;
             }else{
                 return false;
             }
          }
         
     }
     public function getExisting_recomendation($selected_attr_one,$selected_level_one,$selected_attr_two,$selected_level_two,$selected_attr_three,$selected_level_three,$selected_attr_four,$selected_level_four,$selected_attr_five,$selected_level_five,$products_tagged_for_purchasing){
         
         if($products_tagged_for_purchasing!=""){
                 $inv_ids_arr=explode(',',$products_tagged_for_purchasing);
                 $finding=1;
                 $items_present=array();
                 $ss=1;
                 $sku_array=array();
                 foreach($inv_ids_arr as $inv_id){
                     
                     $sql="select * from recomendation_recepient where  pcat_id='$selected_attr_one' and cat_id='$selected_attr_two' and subcat_id='$selected_attr_three' and brand_id='$selected_attr_four' and product_id='$selected_attr_five' and inv_id='$inv_id'";
                     
                     $result=$this->db->query($sql);
                     
                     if($result->num_rows>0){
                         if($ss==1){
                             array_push($items_present,$result->row()->name_of_items);
                             array_push($items_present,'SKU ID as '.$this->getSKUId($result->row()->inv_id));
                         }else{
                             array_push($items_present,'SKU ID as '.$this->getSKUId($result->row()->inv_id));
                         }
                         array_push($sku_array,$result->row()->inv_id);
                         $finding=0;
                         $ss++;
                    }
                 }
                 if($finding==1){
                     return 0;
                 }else{
                     $final_data=array();
                     $final_data['items']=$items_present;
                     $final_data['skus']=$sku_array;
                     $final_data['recomendation_uid']=0;
                     return $final_data;
                 }
             
             
             
            /*$sql="select * from recomendation_recepient where  pcat_id='$selected_attr_one' and cat_id='$selected_attr_two' and subcat_id='$selected_attr_three' and brand_id='$selected_attr_four' and product_id='$selected_attr_five' and inv_id=''";
            $result=$this->db->query($sql);
            if($result->num_rows>0){
                $final_data=array();
                $items_present=array();
                array_push($items_present,$result->row()->name_of_items);
                $final_data['items']=$items_present;
                $final_data['skus']='group_delete';
                $final_data['recomendation_uid']=$result->row()->recomendation_uid;
                return $final_data;
            }
            else{
                 $inv_ids_arr=explode(',',$products_tagged_for_purchasing);
                 $finding=1;
                 $items_present=array();
                 $ss=1;
                 $sku_array=array();
                 foreach($inv_ids_arr as $inv_id){
                     
                     $sql="select * from recomendation_recepient where  pcat_id='$selected_attr_one' and cat_id='$selected_attr_two' and subcat_id='$selected_attr_three' and brand_id='$selected_attr_four' and product_id='$selected_attr_five' and inv_id='$inv_id'";
                     
                     $result=$this->db->query($sql);
                     
                     if($result->num_rows>0){
                         if($ss==1){
                             array_push($items_present,$result->row()->name_of_items);
                             array_push($items_present,'SKU ID as '.$this->getSKUId($result->row()->inv_id));
                         }else{
                             array_push($items_present,'SKU ID as '.$this->getSKUId($result->row()->inv_id));
                         }
                         array_push($sku_array,$result->row()->inv_id);
                         $finding=0;
                         $ss++;
                    }
                 }
                 if($finding==1){
                     return 0;
                 }else{
                     $final_data=array();
                     $final_data['items']=$items_present;
                     $final_data['skus']=$sku_array;
                     $final_data['recomendation_uid']=0;
                     return $final_data;
                 }

            }*/
             
         }else{
           $sql="select * from recomendation_recepient where  pcat_id='$selected_attr_one' and cat_id='$selected_attr_two' and subcat_id='$selected_attr_three' and brand_id='$selected_attr_four' and product_id='$selected_attr_five' and inv_id=''";
            $result=$this->db->query($sql);
            if($result->num_rows>0){
                $final_data=array();
                $items_present=array();
                array_push($items_present,$result->row()->name_of_items);
                $final_data['items']=$items_present;
                $final_data['skus']="group_delete";
                $final_data['recomendation_uid']=$result->row()->recomendation_uid;
                return $final_data;
            }
            else{
                return false;
            }
         } 
     }
    
    public function getExisting_recomendation_tagged($selected_attr_one,$selected_level_one,$selected_attr_two,$selected_level_two,$selected_attr_three,$selected_level_three,$selected_attr_four,$selected_level_four,$selected_attr_five,$selected_level_five,$recomenation_uid,$products_tagged_for_recomendation){
        $new_added_product_arr=explode(',',$products_tagged_for_recomendation);
        $arr_first_level_culprit=array();
        $arr_second_level_culprit=array();
        $arr_third_level_culprit=array();
            foreach($new_added_product_arr as $data){
                /*first level*/
                $sql="select inv_id,id from recomendation_tagged where recomendation_level='first_level' and recomendation_uid='$recomenation_uid' and pcat_id='$selected_attr_one' and cat_id='$selected_attr_two' and subcat_id='$selected_attr_three' and brand_id='$selected_attr_four' and product_id='$selected_attr_five'";
                $result=$this->db->query($sql);
                if($result->num_rows()>0){
                    $result_for_first_level=$result->result_array();
                    foreach($result_for_first_level as $first_level_data){
                            $current_invs_arr=explode(',',$first_level_data['inv_id']);
                            foreach($current_invs_arr as $arrsss){
                                if($data==$arrsss){
                                    $array_data=array();
                                    $array_data['sku_item']=$arrsss;
                                    $array_data['id']=$first_level_data['id'];
                                    array_push($arr_first_level_culprit,$array_data);
                                }
                            }
                    }
                }
                /*second level*/
                $sql="select inv_id,id from recomendation_tagged where recomendation_level='second_level' and recomendation_uid='$recomenation_uid' and  pcat_id='$selected_attr_one' and cat_id='$selected_attr_two' and subcat_id='$selected_attr_three' and brand_id='$selected_attr_four' and product_id='$selected_attr_five'";
                $result=$this->db->query($sql);
                if($result->num_rows()>0){
                    $result_for_first_level=$result->result_array();
                    foreach($result_for_first_level as $second_level_data){
                            $current_invs_arr=explode(',',$second_level_data['inv_id']);
                            foreach($current_invs_arr as $arrsss){
                                if($data==$arrsss){
                                    $array_data=array();
                                    $array_data['sku_item']=$arrsss;
                                    $array_data['id']=$second_level_data['id'];
                                    array_push($arr_second_level_culprit,$array_data);
                                }
                            }
                    }
                }
                /*third level*/
                $sql="select inv_id,id from recomendation_tagged where recomendation_level='third_level' and recomendation_uid='$recomenation_uid' and  pcat_id='$selected_attr_one' and cat_id='$selected_attr_two' and subcat_id='$selected_attr_three' and brand_id='$selected_attr_four' and product_id='$selected_attr_five'";
                $result=$this->db->query($sql);
                if($result->num_rows()>0){
                    $result_for_first_level=$result->result_array();
                    foreach($result_for_first_level as $third_level_data){
                            $current_invs_arr=explode(',',$third_level_data['inv_id']);
                            foreach($current_invs_arr as $arrsss){
                                if($data==$arrsss){
                                    $array_data=array();
                                    $array_data['sku_item']=$arrsss;
                                    $array_data['id']=$third_level_data['id'];
                                    array_push($arr_third_level_culprit,$array_data);
                                }
                            }
                    }
                }
            }
        $array_results_com=array();
        if(count($arr_first_level_culprit)>0){
            $array_results_com['first']=$arr_first_level_culprit;
        }
        if(count($arr_second_level_culprit)>0){
            $array_results_com['second']=$arr_second_level_culprit;
        }
        if(count($arr_third_level_culprit)>0){
            $array_results_com['third']=$arr_third_level_culprit;
        }
        if(count($array_results_com)>0){
            return $array_results_com;
        }else{
            return false;
        }

    }
    
    public function delete_previous_recomendations($parent_category,$category,$sub_category,$attributes,$attributes_selector,$override_previous_recomendation){
        $skus_arr=explode(',',$override_previous_recomendation);
        $deleted=0;
        foreach($skus_arr as $data){
                       
            if(strlen($data)==32){
                //print_r($data);
                $sql="delete from recomendation_recepient where pcat_id='$parent_category' and cat_id='$category' and subcat_id='$sub_category' and brand_id='$attributes' and product_id='$attributes_selector' and inv_id!='' and recomendation_uid='$data'";
                 $result=$this->db->query($sql); 
                 if($this->db->affected_rows()){
                     $deleted=0;
                 }else{
                     $deleted=0;
                     /*$sql="delete from recomendation_recepient where pcat_id='$parent_category' and cat_id='$category' and subcat_id='$sub_category' and brand_id='$attributes' and product_id='$attributes_selector' and inv_id='' and recomendation_uid='$data'";
                       $result=$this->db->query($sql);
                        if($this->db->affected_rows()){
                             $deleted=0;
                         }*/
                 } 
                
            }else{
                 $sql="delete from recomendation_recepient where pcat_id='$parent_category' and cat_id='$category' and subcat_id='$sub_category' and brand_id='$attributes' and product_id='$attributes_selector' and inv_id='$data'";
                 $result=$this->db->query($sql);
                 if($this->db->affected_rows()){
                     $deleted=0;
                 }else{
                     $deleted=1;
                 } 
            }
        }
    if($deleted==1){
        return false;
    }else{
        return true;
    }
    }
    public function group_delete_previous_recomendations($parent_category,$category,$sub_category,$attributes,$attributes_selector){
        $sql="delete from recomendation_recepient where pcat_id='$parent_category' and cat_id='$category' and subcat_id='$sub_category' and brand_id='$attributes' and product_id='$attributes_selector' ";
        $result=$this->db->query($sql);
        if($this->db->affected_rows()){
             return true;
         }else{
             return false;
         }
    }
    
    public function delete_group_of_recomendation_level_items($recomendation_level,$recomendation_id,$ids){
        $sql="delete from recomendation_tagged where recomendation_uid='$recomendation_id' and recomendation_level='$recomendation_level' and id='$ids'";
        $result=$this->db->query($sql);
        if($this->db->affected_rows()>0){
            return true;
        }
        else{
            return false;
        }
    }
    public function delete_group_of_recomendation_level_items_sub($recomendation_level,$recomendation_id,$ids,$sub_item){
        $sql="select inv_id from recomendation_tagged where recomendation_uid='$recomendation_id' and recomendation_level='$recomendation_level' and id='$ids'";
        $result=$this->db->query($sql);
        $array_inv;
        if($result->num_rows()>0){
             $array_inv= $result->row()->inv_id;
         }
        $inv_tot_arr=explode(',',$array_inv);
        $new_array=array();
        $i=1;
        foreach($inv_tot_arr as $arr){
            if($arr!=$sub_item){
                //$new_array.=$arr;
                array_push($new_array,$arr);
            }
            $i++;
        }
        $new_array = implode(',', $new_array);
        if($new_array!=""){
            $sql="update recomendation_tagged set inv_id='$new_array' where recomendation_uid='$recomendation_id' and recomendation_level='$recomendation_level' and id='$ids'";
            $result=$this->db->query($sql);
            if($this->db->affected_rows()>0){
                return true;
            }
            else{
                return false;
            }
        }else{
            $sql="delete from recomendation_tagged where recomendation_uid='$recomendation_id' and recomendation_level='$recomendation_level' and id='$ids'";
            $result=$this->db->query($sql);
            if($this->db->affected_rows()>0){
                return true;
            }
            else{
                return false;
            }
        }
    }
public function delete_group_of_recomendation_level_items_sub_rr($ids,$sub_item){
        $sql="select inv_id from recomendation_tagged where  id='$ids'";
        $result=$this->db->query($sql);
        $array_inv;
        if($result->num_rows()>0){
             $array_inv= $result->row()->inv_id;
         }
        $inv_tot_arr=explode(',',$array_inv);
        $new_array=array();
        $i=1;
        foreach($inv_tot_arr as $arr){
            if($arr!=$sub_item){
                //$new_array.=$arr;
                array_push($new_array,$arr);
            }
            $i++;
        }
        $new_array = implode(',', $new_array);
        if($new_array!=""){
            $sql="update recomendation_tagged set inv_id='$new_array' where  id='$ids'";
            $result=$this->db->query($sql);
            if($this->db->affected_rows()>0){
                return true;
            }
            else{
                return false;
            }
        }else{
            $sql="delete from recomendation_tagged where  id='$ids'";
            $result=$this->db->query($sql);
            if($this->db->affected_rows()>0){
                return true;
            }
            else{
                return false;
            }
        }
    }

    public function getRecipientData($id){
        $sql="select * from recomendation_recepient where recomendation_uid='$id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }else{
            return false;
        }
    }
	public function delete_receipient($recomendation_uid){
		$sql="delete from recomendation_recepient where  recomendation_uid='$recomendation_uid'";
        $result=$this->db->query($sql);
		return $result;
	}
}
?>