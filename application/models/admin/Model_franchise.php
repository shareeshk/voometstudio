
<?php
class Model_franchise extends CI_Model {
	function __construct() {
		$this -> load -> library('pagination');
		//$this -> load -> library('encryption');
		$this->load->library('My_PHPMailer');
		$this -> load -> library('session');
		$this -> load -> database();
	}
	public function franchises(){
		$query = $this->db->query("SELECT * FROM `franchise_customer` where active=1");
		return $query -> result();
	}
	public function check_duplication_name_franchise($email,$mobile,$landline,$franchise_id) {
		if($franchise_id!=""){
			$sql="select * from franchise_customer where (email='$email' or mobile='$mobile') and id!='$id'";
		}
		else{
			$sql="select * from franchise_customer where (email='$email' or mobile='$mobile')";
		}
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function add_franchises($name,$email,$mobile,$landline,$address1,$address2,$pincode,$password,$image){
		$pass=md5($password);
$insertinfo = array("name" => $name,"email" => $email,"password"=>$pass,"password_org"=>$password, "mobile" => $mobile, "landline" => $landline,"address1" => $address1,"address2" => $address2,"pincode" => $pincode,'image'=>$image);
$flag=$this -> db -> insert("franchise_customer", $insertinfo);
return $flag;
}
public function get_franchises_data($id){
	$query =$this->db->query("SELECT * FROM `franchise_customer` Where id='$id' and active=1");
	return $query->row_array();		
}
public function edit_franchises($id,$name,$email,$mobile,$landline,$address1,$address2,$pincode){
	$sql = "UPDATE `franchise_customer` SET name='$name',email='$email',mobile='$mobile',landline='$landline',address1='$address1',address2='$address2',pincode='$pincode' WHERE `id` = '$id'";
	$result=$this->db->query($sql);
	return $result;
}
public function update_franchises_selected($selected_list,$active){

	$sql_update="update franchise_customer set active='$active' where id in ($selected_list)";
	$flag1=$this->db->query($sql_update);
	
	if($flag1==true){
		return true;
	}else{
		return false;
	}
}

public function franchise_change_status($id,$status,$password){
	$arr=[];
	$arr['approval_status']=$status;
	$arr['status_updated_on']=date('Y-m-d H:i:s');

	/* if status approved - generate password */
	if($password!=''){
		$pass=md5($password);
		$arr['password']=$pass;
	}
	/* if status approved - generate password */

	if($id!='') {
		$this->db->where('id', $id);
		$this->db->update('franchise_customer', $arr);
	}
	if($this->db->affected_rows() > 0){
			return true;
	}else{ 
			return false;
	}
}

public function franchises_processing($params,$fg){
		
	$columns = array( 
		0 =>'franchise_customer.id', 
		1 => 'franchise_customer.name',
		2 => 'franchise_customer.email',
		3 => 'franchise_customer.mobile',
		4 => 'franchise_customer.id',
		5 => 'franchise_customer.timestamp'
	);
	$user_type=$this->session->userdata("user_type");
	$a_id=$this->session->userdata("user_id");
	$active =$params['active'];
	
	$where = $sqlTot = $sqlRec = "";
			
			$sql = "select franchise_customer.* from franchise_customer where franchise_customer.active='$active' ";
	
	if(!empty($params['search']['value']) ){   
		$where .=" and ";
		$where .=" ( franchise_customer.name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
		$where .="  or franchise_customer.email LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
		$where .="  or franchise_customer.mobile LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
					$where .= " )";
	}
	
	$sqlTot .= $sql;
	$sqlRec .= $sql;
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}
	$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
	
	//$sqlRec .=  "order by category.timestamp desc";
	//echo $sqlRec;
	if($fg=="get_total_num_recs"){
		$queryTot=$this->db->query($sqlTot);
		$totalRecords=$queryTot->num_rows();
		return $totalRecords;
	}
	if($fg=="get_recs"){
		$queryRecords=$this->db->query($sqlRec);
		return $queryRecords->result();
	}		
}

public function franchises_projects_processing($params,$fg){
		
	$columns = array( 
		0 =>'franchise_projects.project_id', 
		1 => 'franchise_projects.project_name',
		2 => 'franchise_projects.client_name',
		3 => 'franchise_projects.project_location',
		4 => 'franchise_projects.project_id',
		5 => 'franchise_projects.project_id',
		6 => 'franchise_projects.project_id',
		7 => 'franchise_projects.project_id',
		8 => 'franchise_projects.project_id',
		9 => 'franchise_projects.project_id',
		10 => 'franchise_projects.project_last_updated_on'
	);
	$user_type=$this->session->userdata("user_type");
	$a_id=$this->session->userdata("user_id");
	$active =$params['active'];
	$id =$params['franchise_customer_id'];
	
	$where = $sqlTot = $sqlRec = "";
			
			$sql = "select franchise_projects.* from franchise_projects where franchise_projects.franchise_customer_id='$id' ";
	
	if(!empty($params['search']['value']) ){   
		$where .=" and ";
		$where .=" ( franchise_projects.name LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
		$where .="  or franchise_projects.email LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
		$where .="  or franchise_projects.mobile LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
					$where .= " )";
	}
	
	$sqlTot .= $sql;
	$sqlRec .= $sql;
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}
	$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
	
	//$sqlRec .=  "order by category.timestamp desc";
	//echo $sqlRec;
	if($fg=="get_total_num_recs"){
		$queryTot=$this->db->query($sqlTot);
		$totalRecords=$queryTot->num_rows();
		return $totalRecords;
	}
	if($fg=="get_recs"){
		$queryRecords=$this->db->query($sqlRec);
		return $queryRecords->result();
	}		
}

public function franchises_count(){
	$sql="select count(*) as count from  franchise_customer";
	$query=$this->db->query($sql);
	return $query->row()->count;
}
public function franchise_details($id){
	$sql="select * from  franchise_customer where id='$id'";
	$query=$this->db->query($sql);
	return $query->row();
}
public function franchises_projects_count($id){
	$sql="select count(*) as count from  franchise_projects where franchise_customer_id='$id'";
	$query=$this->db->query($sql);
	return $query->row()->count;
}
	
}
?>
