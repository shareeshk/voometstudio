<?php
	class Complaint_manage_data extends CI_Model{
	
	public function get_all_master_data_of_complaints(){
		$sql="select * from complaint_thread_master";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function all_complaint_processing($params,$fg){
		//define index of column
		$columns = array( 
			0 =>'complaint_thread_master.id', 
			1 => 'complaint_thread_master.complaint_thread_id',
			2 => 'complaint_thread_master.created_by',
			3 => 'complaint_thread_master.to_id',
			4 => 'complaint_thread_master.from_id',
			5 => 'complaint_thread_master.severity',
			6 => 'complaint_thread_master.regarding',
			7 => 'complaint_thread_master.service',
			8 => 'complaint_thread_master.category',
			9 => 'complaint_thread_master.subjects',
			10 => 'complaint_thread_master.close_case',
			11 => 'complaint_thread_master.timestamp',
			
		);

		$where = $sqlTot = $sqlRec = "";
		//$customer_id=$this->session->userdata('customer_id');
		// getting total number records without any search
		$sql = "SELECT *,case close_case when '0' then 'Pending Admin Action' when '1' then 'Pending Customer Action' when '2' then 'Resolved By Customer' when '3' then 'Resolved By Admin' when '4' then 'Disputed' end as close_case_text from `complaint_thread_master` where 1=1 ";
		// check search value exist
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( complaint_thread_master.subjects LIKE '%".$params['search']['value']."%' ";
			$where .=" OR complaint_thread_master.id LIKE '%".$params['search']['value']."%' ";
			$where .=" OR complaint_thread_master.regarding LIKE '%".$params['search']['value']."%' ";
			$where .=" OR complaint_thread_master.severity LIKE '%".$params['search']['value']."%' ";
			$where .=" OR case close_case when '0' then 'Pending Admin Action' when '1' then 'Pending Customer Action' when '2' then 'Resolved By Customer' when '3' then 'Resolved By Admin' when '4' then 'Disputed' end LIKE '%".$params['search']['value']."%' )";
		}
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}

		
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
	}
	
	/*function for getting complaints starts*/
	public function get_master_data_of_complaints($complaint_thread_id){
		$sql="select * from complaint_thread_master where complaint_thread_id='$complaint_thread_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
		
	}
	
	public function get_complaint_case_user_fullname($id){
		$sql="select name from customer where id='$id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->row()->name;
		}
		else{
			return false;
		}
	}

	public function change_complaint_case_status($case_id,$action){
		$sql="update complaint_thread_master set close_case='$action' where complaint_thread_id='$case_id'";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function get_all_thread_complaint($complaint_id){
		$sql="select * from complaint_thread_list where complaint_thread_id='$complaint_id' order by timestamp desc";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	public function get_all_attachment_of_thread_id($threadid){
		$sql="select * from complaint_attachment where thread_id='$threadid'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		else{
			return false;
		}
	}
	
	
	
	public function save_complaint_attachment($file_path,$thread_id,$complaint_thread_id,$customer_id){
		$sql="insert into complaint_attachment (thread_id,complaint_thread_id,file_path,customer_id) values('$thread_id','$complaint_thread_id','$file_path','$customer_id')";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	public function add_thread_to_complaints($complaint_thread_id,$thread_id,$user_type,$customer_id,$discription,$contact_method,$country_code,$phone_number,$ext_number){
		$feed_back_rating="";
		//$discription=htmlspecialchars($discription);

		$discription=$this->db->escape_str($discription);
		$sql="insert into complaint_thread_list (complaint_thread_id,thread_id,created_by,user_id,descriptions,contact_method,phone_country,phone_number,phone_extension,feed_back_rating) values('$complaint_thread_id','$thread_id','$user_type','$customer_id','$discription','$contact_method','$country_code','$phone_number','$ext_number','$feed_back_rating')";
		$result=$this->db->query($sql);
		if($this->db->affected_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	

}
?>