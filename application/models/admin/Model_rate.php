<?php
	class Model_rate extends CI_Model{

	public function get_all_rated_inventory($status) {
		
		if($status=="pending"){$flag=2;}if($status=="allowed"){$flag=1;}if($status=="blocked"){$flag=0;}
		 
		$query = $this->db->query("SELECT rated_inventory.*, DATE_FORMAT(rated_inventory.posteddate,'%b %d %Y %h:%i %p') as pdate,inventory.product_id,products.product_name FROM rated_inventory,inventory,products where  rated_inventory.status=$flag and inventory.id=rated_inventory.inventory_id and inventory.product_id=products.product_id order by rated_inventory.posteddate desc");			
		
		return $query->result();
		
	}
	public function rated_inventory_processing($params,$fg){
		$status =$params['status'];
		$rating =$params['rating'];
		$inventory_id =$params['inventory_id'];
		if($status=="pending"){$flag=2;}if($status=="allowed"){$flag=1;}if($status=="blocked"){$flag=0;}
			$columns = array(
				0 =>'rated_inventory.id', 
				1 => 'products.product_name',
				2 => 'rated_inventory.rating',
				3 => 'rated_inventory.review'
			);

			$where = $sqlTot = $sqlRec = "";

			$sql = "SELECT rated_inventory.*, DATE_FORMAT(rated_inventory.posteddate,'%b %d %Y %h:%i %p') as pdate,inventory.product_id,products.product_name FROM rated_inventory,inventory,products where  rated_inventory.status='$flag' and inventory.id=rated_inventory.inventory_id and inventory.product_id=products.product_id ";

			if( !empty($params['search']['value']) ){   
				$where .=" and ";
				$where .=" ( products.product_name LIKE '%".$params['search']['value']."%' ";
				$where .="  Or rated_inventory.review LIKE '%".$params['search']['value']."%' ";
				$where .="  Or rated_inventory.rating LIKE '%".$params['search']['value']."%' )";
			}
			
			if(!empty($inventory_id)){
				$where .=" AND ";
				$where .="rated_inventory.inventory_id='$inventory_id'";
			}
			
			if(!empty($rating)){
				
				$where .=" AND ";
				$where .="rated_inventory.rating='$rating'";
				
			}else{
				$where .="";
			}
			$sqlTot .= $sql;
			$sqlRec .= $sql;

			if(isset($where) && $where != '') {

				$sqlTot .= $where;
				$sqlRec .= $where;
			}
			$sqlRec .=  "order by rated_inventory.posteddate desc";

			if($fg=="get_total_num_recs"){
				$queryTot=$this->db->query($sqlTot);
				$totalRecords=$queryTot->num_rows();
				return $totalRecords;
			}
			if($fg=="get_recs"){
				$queryRecords=$this->db->query($sqlRec);
				return $queryRecords->result();
			}
	}
	public function allow_rated_inventory($selected_list){
		$sql = "UPDATE `rated_inventory` SET `status` = 1 WHERE `id` in ($selected_list)";
		$result=$this->db->query($sql);
		return $result;
	}
	public function notallow_rated_inventory($selected_list){
		$sql = "UPDATE `rated_inventory` SET `status` = 0 WHERE `id` in ($selected_list)";
		$result=$this->db->query($sql);
		return $result;
	}
	public function parent_category() {
		$query = $this->db->query("SELECT * FROM `parent_category`");
		return $query -> result();
	}
	public function show_available_categories($pcat_id){
		$sql="select cat_id,cat_name from  category where trim(pcat_id) = '$pcat_id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_subcategories($cat_id){
		$sql="select subcat_id,subcat_name from  subcategory where trim(cat_id) = '$cat_id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_brands($subcat_id)
	{
		$sql="select brand_id,brand_name from  brands where trim(subcat_id) = '$subcat_id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_products($brand_id){
		
		$sql="select product_id,product_name from  products where trim(brand_id) = '$brand_id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_inventory($product_id){
		
		$sql="select id,sku_id,product_id from  inventory where trim(product_id) = '$product_id' where inventory_type!=2";// filtering the addon(inventory_type 2)
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function ratings_for_service_processing($params,$fg){
		
		$rating =$params['rating'];
		$var_from =$params['from_date'];
		$var_to =$params['to_date'];
		$var_delivered_from =$params['delivered_date_from'];
		$var_delivered_to =$params['delivered_date_to'];
		$pcat_id =$params['pcat_id'];
		$cat_id =$params['cat_id'];
		$subcat_id =$params['subcat_id'];
		$brand_id =$params['brand_id'];
		$product_id =$params['product_id'];
		$inventory_id =$params['inventory_id'];
		
		$columns = array(
			0 =>'rateorder.rateorder_id', 
			1 => 'rateorder.order_item_id',
			2 => 'rateorder.rating',
			3 => 'rateorder.feedback_status',
			3 => 'customer.id'
		);

		$where = $sqlTot = $sqlRec = "";

		$sql="select rateorder.*,completed_orders.*,completed_orders.timestamp as delivered_timestamp,rateorder.timestamp as rateorder_timestamp, customer.id,customer.name as original_customer,shipping_address.*,logistics.*,vendors.*,inventory.id,inventory.sku_id,products.product_id,products.product_name,products.brand_id,products.subcat_id,products.cat_id,products.pcat_id,orders_status.order_item_id,orders_status.order_delivered,orders_status.order_delivered_timestamp from rateorder,completed_orders,customer,shipping_address,logistics,vendors,inventory,products,orders_status  where rateorder.order_item_id=completed_orders.order_item_id and rateorder.order_item_id=orders_status.order_item_id and completed_orders.inventory_id=inventory.id and products.product_id=inventory.product_id and completed_orders.shipping_address_id=shipping_address.shipping_address_id and completed_orders.customer_id=customer.id and vendors.vendor_id=completed_orders.vendor_id and lower(replace(completed_orders.logistics_name, ' ', ''))=(replace(logistics.logistics_name, ' ', ''))";
		
		
		if( !empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" ( products.product_name LIKE '%".$params['search']['value']."%' ";    
			$where .="  OR customer.name LIKE '%".$params['search']['value']."%' "; 
			$where .="  OR completed_orders.order_item_id LIKE '%".$params['search']['value']."%' "; 
			$where .=" OR logistics.logistics_name LIKE '%".$params['search']['value']."%' ";
			$where .=" OR rateorder.rating LIKE '%".$params['search']['value']."%' )";
		}
		
		if(!empty($var_from) && !empty($var_to)){
				
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$date = str_replace('/', '-', $var_to);
				$to_date=date('Y-m-d', strtotime($date));			
				$where .=" AND ";
				$where .="DATE_FORMAT(completed_orders.timestamp,'%Y-%m-%d') >= '$from_date' AND DATE_FORMAT(completed_orders.timestamp,'%Y-%m-%d') <= '$to_date'";
				
			}elseif(!empty($var_from) && empty($var_to)){
				$date = str_replace('/', '-', $var_from);
				$from_date=date('Y-m-d', strtotime($date));
				$where .=" AND ";
				$where .="DATE_FORMAT(completed_orders.timestamp,'%Y-%m-%d') >= '$from_date'";
			}elseif(empty($var_from) && !empty($var_to)){
				$date = str_replace('/', '-', $var_to);
				$to_date=date('Y-m-d', strtotime($date));
				$where .=" AND ";
				$where .="DATE_FORMAT(completed_orders.timestamp,'%Y-%m-%d') <= '$to_date'";
			}
			else{
				$where .="";
			}
			
			if(!empty($var_delivered_from) && !empty($var_delivered_to)){
				
				$date = str_replace('/', '-', $var_delivered_from);
				$delivered_from_date=date('Y-m-d', strtotime($date));
				$date = str_replace('/', '-', $var_delivered_to);
				$delivered_to_date=date('Y-m-d', strtotime($date));			
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.order_delivered_timestamp,'%Y-%m-%d') >= '$delivered_from_date' AND DATE_FORMAT(orders_status.order_delivered_timestamp,'%Y-%m-%d') <= '$delivered_to_date'";
				
			}elseif(!empty($var_delivered_from) && empty($var_delivered_to)){
				$date = str_replace('/', '-', $var_delivered_from);
				$delivered_from_date=date('Y-m-d', strtotime($date));
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.order_delivered_timestamp,'%Y-%m-%d') >= '$delivered_from_date'";
			}elseif(empty($var_delivered_from) && !empty($var_delivered_to)){
				$date = str_replace('/', '-', $var_delivered_to);
				$delivered_to_date=date('Y-m-d', strtotime($date));
				$where .=" AND ";
				$where .="DATE_FORMAT(orders_status.order_delivered_timestamp,'%Y-%m-%d') <= '$delivered_to_date'";
			}
			else{
				$where .="";
			}
		
			if(!empty($rating)){
				
				$where .=" AND ";
				$where .="rateorder.rating = '$rating'";
			}
			if(!empty($inventory_id)){
				
				$where .=" AND ";
				$where .="inventory.id = '$inventory_id'";
			}
			if(!empty($product_id)){
				
				$where .=" AND ";
				$where .="inventory.product_id = '$product_id'";
			}
			if(!empty($subcat_id)){
				
				$where .=" AND ";
				$where .="products.subcat_id = '$subcat_id'";
			}
			if(!empty($brand_id)){
				
				$where .=" AND ";
				$where .="products.brand_id = '$brand_id'";
			}
			if(!empty($cat_id)){
				
				$where .=" AND ";
				$where .="products.cat_id = '$cat_id'";
			}
			if(!empty($pcat_id)){
				
				$where .=" AND ";
				$where .="products.pcat_id = '$pcat_id'";
			}
		
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}

		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
	}
	public function show_survey_result($rateorder_id){
		$sql="select feedback_questions.question,feedback_ratings.rating_level,rateorder.message from  feedback_questions,feedback_ratings,rateorder where feedback_questions.id=feedback_ratings.question_id and feedback_ratings.rateorder_id='$rateorder_id' and rateorder.rateorder_id='$rateorder_id'";
		$result=$this->db->query($sql);
		return $result->result();
	}
	public function show_rateorder_result($rateorder_id){
		$sql="select * from rateorder where rateorder_id='$rateorder_id'";
		$result=$this->db->query($sql);
		return $result->result();
	}
	public function get_msg_from_rateorder($rateorder_id){
		$sql="select message from rateorder where rateorder_id='$rateorder_id'";
		$result=$this->db->query($sql);
		return $result->row()->message;
	}
		
}
?>