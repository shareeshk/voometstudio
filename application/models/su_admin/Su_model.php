<?php
class Su_model extends CI_Model{
	public function update_promotion_request($action_num,$action_reject,$approval_sent_state,$promo_id,$hold_flag){
	    if($hold_flag==1){
	        $sql="update promotions set promo_approval='$action_num',approval_sent='$approval_sent_state',  rejected_flag='$action_reject',hold_flag='$hold_flag' where promo_uid='$promo_id' ";
	    }
        else{
            $sql="update promotions set promo_approval='$action_num',approval_sent='$approval_sent_state',  rejected_flag='$action_reject',hold_flag='$hold_flag',temp_email_num='',su_email='' where promo_uid='$promo_id' ";
        }
	    
        $result=$this->db->query($sql);
        if($this->db->affected_rows()>0){
            return 1;
        }
        else{
            return 0;
        }
	}
    public function check_temp_email_num($temp_email_num){
        $sql="select * from promotions where temp_email_num='$temp_email_num' and approval_sent=1";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return true;
        }
        else{
            return false;
        }
    }
	
	
	public function all_inventory_processing($params,$fg){
		
		$columns = array( 
			0 =>'inventory.timestamp'

		);
		
		$active =1;
		
		$where = $sqlTot = $sqlRec = "";
		$sql = "select * from inventory where 1=1 ";
		if(!empty($params['search']['value']) ){   
			$where .=" and ";
			$where .=" (sku_id LIKE '%".$this->db->escape_str($params['search']['value'])."%' ";
			$where .=" OR selling_price LIKE '%".$this->db->escape_str($params['search']['value'])."%' )";
		}
		
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		if(isset($where) && $where != '') {
			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";

		//echo $sqlRec;
		
		if($fg=="get_total_num_recs"){
			$queryTot=$this->db->query($sqlTot);
			$totalRecords=$queryTot->num_rows();
			return $totalRecords;
		}
		if($fg=="get_recs"){
			$queryRecords=$this->db->query($sqlRec);
			return $queryRecords->result();
		}
			
	}
	
	
	public function get_product_details($product_id){
		$sql="select * from products where product_id='$product_id'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function get_inventory_details($inventory_id){
		$sql="select * from inventory where id='$inventory_id'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function inventory($product_id){
		$query = $this->db->query("select * from inventory where product_id='$product_id'");
		return $query -> result();
	}
	
	public function parent_category() {
		$query = $this->db->query("SELECT * FROM `parent_category` where active=1");
		return $query -> result();
	}
	
	public function get_all_attributes($subcat_id){
		$sql="select * from attribute where subcat_id='$subcat_id'";	
		$query =$this->db->query($sql);
		return $res=$query->result();	
	}
	
	public function vendors(){
		$query = $this->db->query("SELECT * FROM `vendors` where active=1");
		return $query -> result();
	}
	
	public function get_all_specification_group($pcat_id,$cat_id,$subcat_id){
		
		if($pcat_id==0){
			$specification_group_id_arr=array();
			$sql="select cat_view,specification_group_id from specification_group_to_categories where cat_id='$cat_id'";
			$query =$this->db->query($sql);
			$res=$query->result_array();
			
			foreach($res as $row){
				if($row["cat_view"]=="1"){
					$specification_group_id_arr[]=$row["specification_group_id"];
				}
			}
			
			$sql="select subcat_view,specification_group_id from specification_group_to_categories where subcat_id='$subcat_id'";
			$query =$this->db->query($sql);
			$res=$query->result_array();
			
			foreach($res as $row){
				if($row["subcat_view"]=="1"){
					$specification_group_id_arr[]=$row["specification_group_id"];
				}
			}
			$specification_group_id_in=implode(",",$specification_group_id_arr);
			
			$sql="select * from specification_group,specification_group_to_categories where specification_group.specification_group_id=specification_group_to_categories.specification_group_id";
			if($specification_group_id_in!=""){
				$sql.=" and specification_group_to_categories.specification_group_id in ($specification_group_id_in)";
			}
		}else{
			$specification_group_id_arr=array();
			$sql="select pcat_view,specification_group_id from specification_group_to_categories where pcat_id='$pcat_id'";
			$query =$this->db->query($sql);
			$res=$query->result_array();
			foreach($res as $row){
				if($row["pcat_view"]=="1"){
					$specification_group_id_arr[]=$row["specification_group_id"];
				}
			}
			
			$sql="select cat_view,specification_group_id from specification_group_to_categories where cat_id='$cat_id'";
			$query =$this->db->query($sql);
			$res=$query->result_array();
			
			foreach($res as $row){
				if($row["cat_view"]=="1"){
					$specification_group_id_arr[]=$row["specification_group_id"];
				}
			}
			
			$sql="select subcat_view,specification_group_id from specification_group_to_categories where subcat_id='$subcat_id'";
			$query =$this->db->query($sql);
			$res=$query->result_array();
			
			foreach($res as $row){
				if($row["subcat_view"]=="1"){
					$specification_group_id_arr[]=$row["specification_group_id"];
				}
			}
			$specification_group_id_in=implode(",",$specification_group_id_arr);
			
			$sql="select * from specification_group,specification_group_to_categories where specification_group.specification_group_id=specification_group_to_categories.specification_group_id";
			if($specification_group_id_in!=""){
				$sql.=" and specification_group_to_categories.specification_group_id in ($specification_group_id_in)";
			}
			
		}
		$query =$this->db->query($sql);
		$res=$query->result();
		return $res;
	}
	
	
	public function get_all_filterbox($pcat_id,$cat_id,$subcat_id){
		if($pcat_id==0){
			$sql="select * from filterbox,filterbox_to_category where filterbox.filterbox_id=filterbox_to_category.filterbox_id and filterbox_to_category.cat_id='$cat_id'";
		}else{
			
			$filterbox_id_arr=array();
			$sql="select pcat_view,filterbox_id from filterbox_to_category where pcat_id='$pcat_id'";
			$query =$this->db->query($sql);
			$res=$query->result_array();
			foreach($res as $row){
				if($row["pcat_view"]=="1"){
					$filterbox_id_arr[]=$row["filterbox_id"];
				}
			}
			
			$sql="select cat_view,filterbox_id from filterbox_to_category where cat_id='$cat_id'";
			$query =$this->db->query($sql);
			$res=$query->result_array();
			
			foreach($res as $row){
				if($row["cat_view"]=="1"){
					$filterbox_id_arr[]=$row["filterbox_id"];
				}
			}
			
			$sql="select subcat_view,filterbox_id from filterbox_to_category where subcat_id='$subcat_id'";
			$query =$this->db->query($sql);
			$res=$query->result_array();
			
			foreach($res as $row){
				if($row["subcat_view"]=="1"){
					$filterbox_id_arr[]=$row["filterbox_id"];
				}
			}
			$filterbox_id_in=implode(",",$filterbox_id_arr);
						
			$sql="select * from filterbox,filterbox_to_category where filterbox.filterbox_id=filterbox_to_category.filterbox_id and filterbox_to_category.pcat_id='$pcat_id' and filterbox_to_category.filterbox_id in ($filterbox_id_in)";
			
			
		}
		$query =$this->db->query($sql);
		return $res=$query->result();	
	}
	
	public function logistics_parcel_category(){
		$query = $this->db->query("SELECT logistics_parcel_category.*,logistics.logistics_name from logistics_parcel_category,logistics where logistics_parcel_category.logistics_id=logistics.logistics_id order by logistics_parcel_category.logistics_parcel_category_id desc");
		return $query -> result();
	}
	
	public function logistics_all_territory_name() {
		$query = $this->db->query("SELECT * from logistics_all_territory_name");
		return $query -> result();
	}
	
	public function get_all_inventory_data($product_id,$inventory_id){
		$query = $this->db->query("select inventory.*,logistics_inventory.* from inventory,logistics_inventory where inventory.product_id='$product_id' and inventory.id='$inventory_id' and logistics_inventory.inventory_id=inventory.id");
		return $query -> row_array();
	}
	
	public function get_all_specification_by_inventory_id($inventory_id){
		$query = $this->db->query("select * from inventory_specification where  inventory_id='$inventory_id'");
		return $query -> result_array();
	}
	
	public function get_all_filter_by_inventory_id($inventory_id){
		$query = $this->db->query("select products_filter.filter_id,products_filter.product_filter_id,filter.filterbox_id,filter.filter_options,filterbox_name from products_filter,filter,filterbox where products_filter.inventory_id='$inventory_id' and products_filter.filter_id=filter.filter_id and filter.filterbox_id=filterbox.filterbox_id");
		return $query -> result_array();
	}
	
	public function get_all_specification($specification_group_id){
		$sql="select * from specification,specification_group where  specification.specification_group_id='$specification_group_id' and specification.specification_group_id=specification_group.specification_group_id";	
		$query =$this->db->query($sql);
		$res=$query->result();
		return $res;
	}
	
	public function get_all_filter($filterbox_id){
		$sql="select * from filter,filterbox where filter.filterbox_id='$filterbox_id' and filter.filterbox_id=filterbox.filterbox_id";	
		$query =$this->db->query($sql);
		return $res=$query->result();	
	}
	
	public function create_search_index($inventory_id){
		$sql="select inventory.id as inventory_id,inventory.sku_id,products.*,inventory.image,inventory.thumbnail,inventory.selling_price,inventory.base_price,inventory.tax_percent,category.cat_name,subcategory.subcat_name,brands.brand_name,inventory.common_image,inventory.inventory_type from products,inventory,category,subcategory,brands where products.product_id=inventory.product_id and products.cat_id=category.cat_id and products.subcat_id=subcategory.subcat_id and products.brand_id=brands.brand_id and inventory.id='$inventory_id' and inventory.vendor_removed=0";
		$query=$this->db->query($sql);
		return $query->row();
	}
	
	public function get_filter_id_new($inventory_id){
		$sql="select * from  products_filter where inventory_id='$inventory_id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function get_filterbox_name($filter_id){
		$sql="select filterbox_name from  filterbox where filterbox_id = (select filterbox_id from filter where filter_id='$filter_id')";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
		return $query->row()->filterbox_name;
		}else{
			return "";
		}
	}
	
	
	public function edit_inventory($inventory_id,$product_id,$common_image,$image_arr,$thumbnail_arr,$largeimage_arr,$base_price,$selling_price,$purchased_price,$moq,$max_oq,$tax,$stock,$product_status,$product_status_view,$low_stock,$cutoff_stock,$filter_arr,$highlight){
		$moq_base_price=$moq*$base_price;
		$moq_price=$moq*$selling_price;
		//$moq_purchased_price=$moq*$purchased_price;
		// I added the below if else 6/11/2021 packs
		if(trim($purchased_price)==""){
			$moq_purchased_price="";
		}
		else{
			$moq_purchased_price=$moq*$purchased_price;
		}
		
		
		$img_str="";
		
		
		for($j=1;$j<=5;$j++){
			
			if($j==1){
				if(!empty($thumbnail_arr[0])){
				
					$img_str.="thumbnail='".$thumbnail_arr[0]."',";
				}
				if(empty($thumbnail_arr[0])){
				
					$img_str.="thumbnail='',";
				}
				if(!empty($image_arr[0])){
				
					$img_str.="image='".$image_arr[0]."',";
				}
				if(empty($image_arr[0])){
				
					$img_str.="image='',";
				}
				if(!empty($largeimage_arr[0])){
				
					$img_str.="largeimage='".$largeimage_arr[0]."',";
				}
				if(empty($largeimage_arr[0])){
				
					$img_str.="largeimage='',";
				}
				
			}
			else{
				$i=$j-1;
				if(!empty($thumbnail_arr[$i])){
					
					$img_str.="thumbnail".$j."='".$thumbnail_arr[$i]."',";
				}
				if(empty($thumbnail_arr[$i])){
					
					$img_str.="thumbnail".$j."='',";
				}
				if(!empty($image_arr[$i])){
					
					$img_str.="image".$j."='".$image_arr[$i]."',";
				}
				if(empty($image_arr[$i])){
					
					$img_str.="image".$j."='',";
				}
				if(!empty($largeimage_arr[$i])){
					
					$img_str.="largeimage".$j."='".$largeimage_arr[$i]."',";
				}
				if(empty($largeimage_arr[$i])){
					
					$img_str.="largeimage".$j."='',";
				}
			}
			
		}
		
		$img_str=",".$img_str;
		
		$sql = "UPDATE `inventory` SET common_image='$common_image'".$img_str."base_price='$base_price',selling_price='$selling_price',purchased_price='$purchased_price',moq='$moq',max_oq='$max_oq',moq_base_price='$moq_base_price',moq_price='$moq_price',moq_purchased_price='$moq_purchased_price',tax_percent='$tax',stock='$stock',product_status='$product_status',product_status_view='$product_status_view',low_stock='$low_stock',cutoff_stock='$cutoff_stock',highlight='$highlight' WHERE `id` = '$inventory_id'";

		
		
		$result=$this->db->query($sql);
		
		if($result==true){
			return 1;
		}else{
			return 0;
		}
	}
	
	
	public function get_inventory_product_info_by_inventory_id($inventory_id){
			$sql="select products.*,inventory.*,logistics_inventory.*,(inventory.stock-inventory.cutoff_stock) as stock_available from products,inventory,logistics_inventory where products.product_id=inventory.product_id and id='$inventory_id' and inventory.id=logistics_inventory.inventory_id";
			
			$query=$this->db->query($sql);
			return $query->row();
		}
		
	public function get_a_TO_z_id($product_id){
        $sql="select * from products where product_id='$product_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()==1){
          return  $result->result_array();
        }
        else{
            return false;
        }
    }
	
	public function product_level_promotion_uid($product_id){
        $sql="select * from promotion_items_taged_for_purchasing where  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='$product_id' and promotion_items_taged_for_purchasing.brand_id  is not NULL and promotion_items_taged_for_purchasing.subcat_id is not NULL and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
	public function brand_level_promotion_uid($brand_id){
        $sql="select * from  promotion_items_taged_for_purchasing where  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='$brand_id' and promotion_items_taged_for_purchasing.subcat_id is not NULL and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
	public function subcat_level_promotion_uid($subcat_id){
        $sql="select *  from  promotion_items_taged_for_purchasing where  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='$subcat_id' and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
	public function cat_level_promotion_uid($cat_id){
        $sql="select *  from  promotion_items_taged_for_purchasing where  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.cat_id='$cat_id' and promotion_items_taged_for_purchasing.pcat_id is not NULL";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
	public function pcat_level_promotion_uid($pcat_id){
        $sql="select *  from  promotion_items_taged_for_purchasing where  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id is not NULL and promotion_items_taged_for_purchasing.subcat_id  is not NULL and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id='$pcat_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
	public function store_level_promotion_uid(){
        $sql="select * from promotion_items_taged_for_purchasing where promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.cat_id='' and promotion_items_taged_for_purchasing.pcat_id=''";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
	
	public function product_level_promotion($promo_uid,$product_id){
        $sqls="SELECT *,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id
         FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid' and  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='$product_id' and promotion_items_taged_for_purchasing.brand_id is not NULL and promotion_items_taged_for_purchasing.subcat_id is not NULL and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL and now() > promotions.promo_start_date  and not promotions.promo_end_date < now()";
        $result=$this->db->query($sqls);
        return $result->result();
    }
	
	public function brand_level_promotion($promo_uid,$brand_id){
      $sqls="SELECT * ,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id
         FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid'  and promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='$brand_id' and promotion_items_taged_for_purchasing.subcat_id is not NULL and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL and now() > promotions.promo_start_date and  not promotions.promo_end_date < now()";
        $result=$this->db->query($sqls);
        return $result->result();
    }
	 public function subcat_level_promotion($promo_uid,$subcat_id){
        $sqls="SELECT *,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id
         FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid'  and promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='$subcat_id' and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL and now() > promotions.promo_start_date  and not promotions.promo_end_date < now()";
        $result=$this->db->query($sqls);
        return $result->result();
    }
	public function cat_level_promotion($promo_uid,$cat_id){
        $sqls="SELECT * ,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid'  and promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.cat_id='$cat_id' and promotion_items_taged_for_purchasing.pcat_id  is not NULL and now() > promotions.promo_start_date  and not promotions.promo_end_date < now()";
        $result=$this->db->query($sqls);
        return $result->result();
    }
	
	 public function pcat_level_promotion($promo_uid,$pcat_id){
        $sqls="SELECT * ,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid'  and promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.cat_id='' and promotion_items_taged_for_purchasing.pcat_id='$pcat_id' and now() > promotions.promo_start_date  and not promotions.promo_end_date < now()";
        $result=$this->db->query($sqls);
        return $result->result();
    }
	
	public function store_level_promotion($promo_uid){
        $sqls="SELECT * ,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid' and promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.cat_id='' and promotion_items_taged_for_purchasing.pcat_id='' and promotion_items_taged_for_purchasing.inv_id='' and now() > promotions.promo_start_date   and not promotions.promo_end_date < now()";
        $result=$this->db->query($sqls);
        return $result->result();
    }
	
	public function get_promotions($inventory_id){
		$sql="select * from promotion_items_taged_for_purchasing where inv_id='$inventory_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		
	}
	
	public function get_all_promotions_data($promo_uid,$inventory_id){
		$sqls="SELECT * ,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id FROM (`promotions`) 
				LEFT JOIN `promotion_items_taged_for_purchasing_free` 
				ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
				LEFT JOIN `promotion_items_taged_for_purchasing` 
				ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid' and promotion_items_taged_for_purchasing.inv_id='$inventory_id' and now() > promotions.promo_start_date and  not promotions.promo_end_date < now()";
		$result=$this->db->query($sqls);
        return $result->result();
	}
	
	
	public function get_inventory_details_result($product_id='',$pcat_id='',$cat_id='',$subcat_id='',$brand_id=''){
		$sql="select sku_id, attribute_1, attribute_1_value, attribute_2, attribute_2_value, attribute_3, attribute_3_value, attribute_4, attribute_4_value, tax_percent, base_price, selling_price, purchased_price, mrp_quantity, moq, moq_base_price, moq_price, moq_purchased_price, max_oq, num_sales, stock, low_stock, cutoff_stock from inventory ";

		$sql.=' LEFT JOIN `products` ON `products`.`product_id` = `inventory`.`product_id`';
		/*$sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`';
		$sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`';
		$sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id`';
		$sql.=' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`';*/

		$where='';
		if($product_id!='' || $pcat_id!='' || $cat_id!=''|| $subcat_id!=''  || $brand_id!=''){

			$where= " where " ;

			if($product_id!='') {
				$where.="products.product_id='$product_id' ";
				if($pcat_id!='' || $cat_id!=''|| $subcat_id!=''  || $brand_id!='') {
					$where .= " AND ";
				}
			}

			if($pcat_id!='') {
				$where .= "products.pcat_id='$pcat_id'";
				if($cat_id!=''|| $subcat_id!=''  || $brand_id!='') {
					$where .= " AND ";
				}
			}
			if($cat_id!='') {
				$where .= " products.cat_id='$cat_id'";
				if($subcat_id!=''  || $brand_id!='') {
					$where .= " AND ";
				}
			}
			if($subcat_id!='') {
				$where .= " products.subcat_id='$subcat_id'";
				if( $brand_id!='') {
					$where .= " AND ";
				}
			}
			if($brand_id!='') {
				$where .= " products.brand_id='$brand_id'";

			}
		}
		$sql=$sql.$where;

		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function get_product_details_result($product_id){

		$sql = "select products.product_name,products.product_description,subcategory.subcat_name,category.cat_name,parent_category.pcat_name,brands.brand_name from products ";
		$sql.=' LEFT JOIN `parent_category` ON `parent_category`.`pcat_id` = `products`.`pcat_id`';
		$sql.=' LEFT JOIN `category` ON `category`.`cat_id` = `products`.`cat_id`';
		$sql.=' LEFT JOIN `subcategory` ON `subcategory`.`subcat_id` = `products`.`subcat_id`';
		$sql.=' LEFT JOIN `brands` ON `brands`.`brand_id` = `products`.`brand_id`';
		$sql.=" where product_id='$product_id'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function show_available_categories($pcat_id,$active){
		$active_str=($active!=2)? "and active='$active'" :"";
		$sql="select cat_id,cat_name,active from category where trim(pcat_id) = '$pcat_id'  $active_str";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_subcategories($cat_id,$active){
		$active_str=($active!=2)? "and active='$active'" :"";
		$sql="select subcat_id,subcat_name,active from subcategory where trim(cat_id) = trim('$cat_id') $active_str";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_brands($subcat_id,$active){
		$active_str=($active!=2)? "and active='$active'" :"";
		$sql="select brand_id,brand_name,active from brands where trim(subcat_id) = '$subcat_id' $active_str";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function show_available_products($brand_id,$active){
		$active_str=($active!=2)? "and active='$active'" :"";
		$sql="select product_id,product_name,active from  products where trim(brand_id) = '$brand_id' $active_str";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
}
?>