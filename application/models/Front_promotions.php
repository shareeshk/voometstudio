<?php
class Front_promotions extends CI_model{
	public function get_promotions($inventory_id){
		$sql="select * from promotion_items_taged_for_purchasing where inv_id='$inventory_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
			return $result->result_array();
		}
		
	}
	public function get_all_promotions_data($promo_uid,$inventory_id){
		$sqls="SELECT * ,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id FROM (`promotions`) 
				LEFT JOIN `promotion_items_taged_for_purchasing_free` 
				ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
				LEFT JOIN `promotion_items_taged_for_purchasing` 
				ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid' and promotion_items_taged_for_purchasing.inv_id='$inventory_id' and now() > promotions.promo_start_date and  not promotions.promo_end_date < now()";
		$result=$this->db->query($sqls);
        return $result->result();
	}
    
    public function product_level_promotion_uid($product_id){
        $sql="select * from promotion_items_taged_for_purchasing where  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='$product_id' and promotion_items_taged_for_purchasing.brand_id  is not NULL and promotion_items_taged_for_purchasing.subcat_id is not NULL and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    public function brand_level_promotion_uid($brand_id){
        $sql="select * from  promotion_items_taged_for_purchasing where  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='$brand_id' and promotion_items_taged_for_purchasing.subcat_id is not NULL and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    public function subcat_level_promotion_uid($subcat_id){
        $sql="select *  from  promotion_items_taged_for_purchasing where  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='$subcat_id' and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    public function cat_level_promotion_uid($cat_id){
        $sql="select *  from  promotion_items_taged_for_purchasing where  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.cat_id='$cat_id' and promotion_items_taged_for_purchasing.pcat_id is not NULL";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    public function pcat_level_promotion_uid($pcat_id){
        $sql="select *  from  promotion_items_taged_for_purchasing where  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id is not NULL and promotion_items_taged_for_purchasing.subcat_id  is not NULL and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id='$pcat_id'";
        
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    public function store_level_promotion_uid(){
        $sql="select * from promotion_items_taged_for_purchasing where promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.cat_id='' and promotion_items_taged_for_purchasing.pcat_id=''";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result_array();
        }
    }
    
    public function product_level_promotion($promo_uid,$product_id){
        $sqls="SELECT *,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id
         FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid' and  promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='$product_id' and promotion_items_taged_for_purchasing.brand_id is not NULL and promotion_items_taged_for_purchasing.subcat_id is not NULL and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL and now() > promotions.promo_start_date  and not promotions.promo_end_date < now()";
        $result=$this->db->query($sqls);
        return $result->result();
    }
    
    public function brand_level_promotion($promo_uid,$brand_id){
      $sqls="SELECT * ,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id
         FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid'  and promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='$brand_id' and promotion_items_taged_for_purchasing.subcat_id is not NULL and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL and now() > promotions.promo_start_date and  not promotions.promo_end_date < now()";
        $result=$this->db->query($sqls);
        return $result->result();
    }
    
    public function subcat_level_promotion($promo_uid,$subcat_id){
        $sqls="SELECT *,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id
         FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid'  and promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='$subcat_id' and promotion_items_taged_for_purchasing.cat_id is not NULL and promotion_items_taged_for_purchasing.pcat_id is not NULL and now() > promotions.promo_start_date  and not promotions.promo_end_date < now()";
        $result=$this->db->query($sqls);
        return $result->result();
    }
    
    public function cat_level_promotion($promo_uid,$cat_id){
        $sqls="SELECT * ,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid'  and promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.cat_id='$cat_id' and promotion_items_taged_for_purchasing.pcat_id  is not NULL and now() > promotions.promo_start_date  and not promotions.promo_end_date < now()";
        $result=$this->db->query($sqls);
        return $result->result();
    }
    
    public function pcat_level_promotion($promo_uid,$pcat_id){
         $sqls="SELECT * ,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid'  and promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.cat_id='' and promotion_items_taged_for_purchasing.pcat_id='$pcat_id' and now() > promotions.promo_start_date  and not promotions.promo_end_date < now()";
               // exit;
        $result=$this->db->query($sqls);
        return $result->result();
    }
    
    public function store_level_promotion($promo_uid){
        $sqls="SELECT * ,
        `promotion_items_taged_for_purchasing`.`pcat_id` as purchasing_pcat_id,
        `promotion_items_taged_for_purchasing`.`cat_id` as purchasing_cat_id,
        `promotion_items_taged_for_purchasing`.`subcat_id` as purchasing_subcat_id,
        `promotion_items_taged_for_purchasing`.`brand_id` as purchasing_brand_id,
        `promotion_items_taged_for_purchasing`.`product_id` as purchasing_product_id,
        `promotion_items_taged_for_purchasing`.`inv_id` as purchasing_inv_id,
        `promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
                LEFT JOIN `promotion_items_taged_for_purchasing` 
                ON `promotion_items_taged_for_purchasing`.`promo_uid` = `promotions`.`promo_uid` where `promotions`.`promo_active`=1 and `promotions`.`promo_uid`='$promo_uid' and promotion_items_taged_for_purchasing.inv_id='' and promotion_items_taged_for_purchasing.product_id='' and promotion_items_taged_for_purchasing.brand_id='' and promotion_items_taged_for_purchasing.subcat_id='' and promotion_items_taged_for_purchasing.cat_id='' and promotion_items_taged_for_purchasing.pcat_id='' and promotion_items_taged_for_purchasing.inv_id='' and now() > promotions.promo_start_date   and not promotions.promo_end_date < now()";
        $result=$this->db->query($sqls);
        return $result->result();
    }
    
    public function get_product_id($inventory_id){
        $sql="select product_id from inventory where id='$inventory_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
          return  $result->row()-> product_id;
        }
        else{
            return false;
        }
    }
    public function get_a_TO_z_id($product_id){
        $sql="select * from products where product_id='$product_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()==1){
          return  $result->result_array();
        }
        else{
            return false;
        }
    }
    
    public function get_free_item_data($ids){
        $sql="select * from inventory where id='$ids'";
        $result=$this->db->query($sql);
        if($result->num_rows()==1){
          return  $result->result_array();
        }
        else{
            return false;
        }
    }
	public function get_free_item_data_image_path_attributes($ids){
		$sql="select inventory.thumbnail,inventory.attribute_1,inventory.attribute_1_value,inventory.attribute_2,inventory.attribute_2_value,inventory.attribute_3,inventory.attribute_3_value,inventory.attribute_4,inventory.attribute_4_value,products.product_name from inventory,products where id='$ids' and inventory.product_id=products.product_id";
        $result=$this->db->query($sql);
        if($result->num_rows()==1){
          return  $result->row_array();
        }
        else{
            return array();
        }
	}
    public function get_product_name($product_id){
       $sql="select product_name from products where product_id='$product_id'";
        $result=$this->db->query($sql);
         return  $result->row()->product_name;
 
    }
	
	public function get_all_promotion_free_item($inv_id){
		$sql="select * from inventory where id='$inv_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()==1){
          return  $result->result_array();
        }
        else{
            return false;
        }
	}
	
	public function get_promotions_at_cart(){
		$sql="select * from promotions where `promotions`.`promo_active`=1 and promotions.applied_at_invoice=1 and now() > promotions.promo_start_date  and not promotions.promo_end_date < now()";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
          return  $result->result_array();
        }
        else{
            return false;
        }
		/*echo $sql="select *,
		`promotions`.`promo_uid` as promo_cart_id,
		`promotion_items_taged_for_purchasing_free`.`inv_id` as purchasing_free_inv_id FROM (`promotions`) 
                LEFT JOIN `promotion_items_taged_for_purchasing_free` 
                ON `promotion_items_taged_for_purchasing_free`.`promo_uid` = `promotions`.`promo_uid`
		 where `promotions`.`promo_active`=1 and promotions.applied_at_invoice=1 and now() > promotions.promo_start_date  and not promotions.promo_end_date < now()";
        exit;*/
	}
	
	public function get_all_straight_discount(){
		$sql="select promo_uid,get_type from promotions where promotions.to_buy=1 and promotions.get_type LIKE '%' and now() > promotions.promo_start_date   and not promotions.promo_end_date < now() and promo_active=1";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
          return  $result->result_array();
        }
        else{
            return false;
        }
	}
	public function getStoreLevelStraightDiscount($promo_uid){
		$sql="select * from promotion_items_taged_for_purchasing where promo_uid='$promo_uid' and pcat_id='' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id=''";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
          return  $result->result_array();
        }
        else{
            return false;
        }
	}
	public function getParentCategoryLevelStraightDiscount($promo_uid,$pcat_id){
		$sql="select * from promotion_items_taged_for_purchasing where promo_uid='$promo_uid' and pcat_id='$pcat_id' and cat_id='' and subcat_id='' and brand_id='' and product_id='' and inv_id=''";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
          return  $result->result_array();
        }
        else{
            return false;
        }
	}
	public function getCategoryLevelStraightDiscount($promo_uid,$cat_id){
		$sql="select * from promotion_items_taged_for_purchasing where promo_uid='$promo_uid' and pcat_id!='' and cat_id='$cat_id' and subcat_id='' and brand_id='' and product_id='' and inv_id=''";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
          return  $result->result_array();
        }
        else{
            return false;
        }
	}
	public function getSubCategoryLevelStraightDiscount($promo_uid,$subcat_id){
		$sql="select * from promotion_items_taged_for_purchasing where promo_uid='$promo_uid' and pcat_id!='' and cat_id!='' and subcat_id='$subcat_id' and brand_id='' and product_id='' and inv_id=''";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
          return  $result->result_array();
        }
        else{
            return false;
        }
	}
	public function getBrandLevelStraightDiscount($promo_uid,$brand_id){
		$sql="select * from promotion_items_taged_for_purchasing where promo_uid='$promo_uid' and pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id='$brand_id' and product_id='' and inv_id=''";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
          return  $result->result_array();
        }
        else{
            return false;
        }
	}
	public function getProdctLevelStraightDiscount($promo_uid,$product_id){
		$sql="select * from promotion_items_taged_for_purchasing where promo_uid='$promo_uid' and pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id='$product_id' and inv_id=''";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
          return  $result->result_array();
        }
        else{
            return false;
        }
	}
	public function getInventoryLevelStraightDiscount($promo_uid,$inv_id){
		$sql="select * from promotion_items_taged_for_purchasing where promo_uid='$promo_uid' and pcat_id!='' and cat_id!='' and subcat_id!='' and brand_id!='' and product_id!='' and inv_id='$inv_id'";
		$result=$this->db->query($sql);
		if($result->num_rows()>0){
          return  $result->result_array();
        }
        else{
            return false;
        }
	}
    public function getSimilarProducts($product_id){
        $sql="select subcat_id from products where product_id='$product_id'";
        $result=$this->db->query($sql);
        $subcat_id=$result->row()->subcat_id;
        $products_arr=array();
        
        $sql="select brand_id from brands where subcat_id='$subcat_id'";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            $products_arr=array();
            
            foreach($result->result_array() as $data){
                $brand_id=$data['brand_id'];
                //echo $brand_id."<br>";
                $sql="select product_id from products where brand_id='$brand_id'";
                $result=$this->db->query($sql);
                if($result->num_rows()>0){
                    array_push($products_arr,$result->result_array());
                }
            }
        }
        //echo count($products_arr);
		$suggestion_arr=array();
        if(count($products_arr)>0){
            $suggestion_arr=array();
            foreach($products_arr[0] as $data){
                $product_ids=$data['product_id'];
                //echo $product_id."<br>";
                if($product_id!=$product_ids){
                    $sql="select *,products.product_name as productsssName from inventory join products on products.product_id=inventory.product_id where inventory.product_id='$product_ids' limit 2";
                }
                else{
                    $sql="select *,products.product_name as productsssName from inventory join products on products.product_id=inventory.product_id where inventory.product_id='$product_ids'";
                }
                
                $result=$this->db->query($sql);
                if($result->num_rows()>0){
                    array_push($suggestion_arr,$result->result_array());
                }
            }
        }


        
        if(count($suggestion_arr)>0){
            return $suggestion_arr;
        }else{
            return false;
        }
    }
	
	public function get_check_availability_of_free_shipping_promotion(){
		$sql="select to_buy from promotions where to_get='' and get_type='' and promo_active=1 and now() between promo_start_date and promo_end_date";
		$result=$this->db->query($sql);
		if($result->num_rows($result)>0){
			return array("status"=>"yes","free_shipping_amount_limit"=>$result->row_array()["to_buy"]);
		}
		else{
			return array("status"=>"no");
		}
	}
}
?>