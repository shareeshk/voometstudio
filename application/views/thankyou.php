<style>
	@media (max-width: 480px) {
		.border_right {
			border: none;
		}
	}

	.border_right {
		border-right: 1px solid #ccc;
	}

	.thank_you {
		padding: 10px;
		line-height: 20px;
	}

	.separator_line {
		border-right: 1px solid #ccc;
		margin-left: 15px;
		margin-right: 20px;
	}

	.error_msg {
		margin-left: 15px;
		height: 271px;
		text-align: center;
		padding-top: 115px;
		color: red;
	}

	/*.price_details_thankyou + .popover {
left: -111px !important;
}
.price_details_thankyou + .popover .arrow {
left: 93% !important;
}
.price_details_thankyou2 + .popover {
left: -53px !important;
width:285px;
}
.price_details_thankyou2 + .popover .arrow {
left: 78% !important;
}*/
	#center_column {
		margin-top: 1em;
	}

	.ordrk-color a {
		color: #e46c0a;
	}

	/* Image checkbox */
	.nopad {
		padding-left: 0 !important;
		padding-right: 0 !important;
	}

	/*image gallery*/
	.image-checkbox {

		cursor: pointer;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		border: 1px solid #eee;
		margin-bottom: 0;
		outline: 0;
		margin: 5px 5px 5px 5px;
		border-radius: 5px;
	}

	.image-checkbox img {
		border-radius: 5px;
	}

	.image-checkbox input[type="checkbox"] {
		display: none;
	}

	.image-checkbox-checked {
		border-color: #50d42c;
	}

	.image-checkbox-checked:hover {
		border-color: #50d42c;
	}

	.image-checkbox .fa {
		position: absolute;
		color: red;
		background-color: #fff;
		padding: 5px;
		top: 5px;
		right: 5px;
		border-radius: 12px;
		font-size: 14px;
		height: 25px;
	}

	.image-checkbox-checked .fa {
		display: block !important;
	}

	.image-checkbox:hover {
		border-color: #50d42c;
	}

	.margin-top {
		margin-top: 10px;
	}

	.margin-bottom {
		margin-bottom: 10px;
	}

	@media only screen and (min-width: 768px) {
		/*.table-responsive{
	overflow-x: inherit !important;
}*/
	}
</style>
<!-- Home slideder-->
<?php
$you_saved_fin = 0;
$inc_txt = '';
$promo_dis = 0;
$subtotal = 0;
$shipping_charge_waived_off = 0;
$sku_coupon_used = 0;
if (!empty($invoice_offers)) {
	foreach ($invoice_offers as $data) {
		$promotion_invoice_cash_back = ($data["promotion_invoice_cash_back"]);
		$promotion_invoice_free_shipping = $data["promotion_invoice_free_shipping"];
		$promotion_invoice_discount = $data["promotion_invoice_discount"];
		$total_amount_cash_back = ($data["total_amount_cash_back"]);
		$total_amount_saved = $data["total_amount_saved"];
		$total_amount_to_pay = $data["total_amount_to_pay"];
		$payment_method = $data["payment_method"];
		$invoice_offer_achieved = $data["invoice_offer_achieved"];
		$total_coupon_used_amount = $data["total_coupon_used_amount"];
		$invoice_coupon_status = $data["invoice_coupon_status"];
		$invoice_coupon_reduce_price = $data["invoice_coupon_reduce_price"];
		if ($invoice_coupon_status == '1') {
			$inc_txt .= '(';
			$inc_txt .= ($data["invoice_coupon_type"] == '2') ? curr_sym : '' . '';
			$inc_txt .= round($data["invoice_coupon_value"]);
			$inc_txt .= ($data["invoice_coupon_type"] == '1') ? '%' : 'Flat';
			$inc_txt .= ')';
		}
		if ($promotion_invoice_free_shipping > 0 && $invoice_offer_achieved == 1) {
			$shipping_charge_waived = "yes";
		} else {
			$shipping_charge_waived = "no";
		}
	}
}
?>
<div class="container">
	<div class="my_orders_links">
		<?php
		if (empty($order_summary_mail_data_arr)) {
			?>
			<div id="columns" class="error_msg">
				<h2 class="alert alert-danger"> Sorry... This page is not available.</h2>
			</div>
			<?php
		} else {
			?>
			<?php
			$grandtotal = 0;
			foreach ($order_summary_mail_data_arr as $order_summary_mail_data) {
				//	echo "|".$order_summary_mail_data["grandtotal"]."|";
				$grandtotal += $order_summary_mail_data["grandtotal"];
			}
			?>
			<div class="row mt-50">
				<div class="col-md-12">
					<div class="well" style="background-color:#fff;">
						<div class="row">
							<div class="col-md-7">
								<div class="row">
									<div class="col-md-12 thank_you">
										<h1 class="page-heading pl-0">
											Thank you for your order
										</h1>
										<p style="text-align: justify;margin-top: 10px;">
											Your order has been placed. When the item(s) are shipped. you will receive an
											email with the details. You can track this order through <u><a
													href="<?php echo base_url() ?>Account/my_order">My orders</a></u> page.
										</p>
										<br>
										<p><i class="fa fa-check fa-1x" aria-hidden="true" style="color:green;"> </i> <span
												style="font-size:1.2em;">

												<?php

												//echo "grandtotal=".$grandtotal."||".$total_amount_to_pay;

												if ($grandtotal <= $total_amount_to_pay) {

													?>

													<b>
														<?php echo curr_sym; ?>
														<?php echo $grandtotal; ?>
													</b>
													<?php /*?>(paid via <?php echo $payment_method;?>)<?php */ ?>
													</td>
													</tr>
												<?php
												} else { ?>

													<b>
														<?php echo curr_sym; ?>
														<?php echo $total_amount_to_pay; ?>
													</b> <del>
														<?php echo curr_sym; ?>
														<?php echo $grandtotal; ?>
													</del>
													<?php /*?>(paid via <?php echo $payment_method;?>)<?php */ ?>

												<?php }
												?>
											</span>
										</p>
									</div>
								</div>
							</div>
							<div class="col-md-5">
								<div class="row">
									<div class="col-md-12 thank_you">
										<?php
										foreach ($shipping_address_table_data_arr as $shipping_address_table_data) {
											?>
											<p>
											<div class="add_align" style="margin-top:10px;">
												<h6 style="margin-top: 10px;font-size: 24px;">
													<?php
													echo $shipping_address_table_data['customer_name'];
													?>
												</h6>
												<h6 style="font-size:15px;margin-top:10px;">
													<b>Mob :</b>
													<?php
													echo $shipping_address_table_data['mobile'];
													?>
												</h6>
											</div>
											<?php
											echo $shipping_address_table_data['address1'] . ", ";
											echo $shipping_address_table_data['address2'] . ", ";
											echo $shipping_address_table_data['city'] . "-";
											echo $shipping_address_table_data['pincode'] . ", ";
											echo "<br>";
											echo $shipping_address_table_data['state'] . ", ";
											echo $shipping_address_table_data['country'];
											?>
											<?php
										}
										?>
										</p>
									</div>
								</div>
							</div>
						</div>

						<?php

						if (!empty($invoice_offers)) {

							if ($invoice_offer_achieved == 1) { ?>




								<div class="row">
									<div class="col-md-12">
										<?php
										if ($total_amount_cash_back > 0 || $promotion_invoice_free_shipping > 0 || $promotion_invoice_discount > 0 || $invoice_coupon_status == 1 || $total_amount_saved > 0) {
											?>
											<div style="display:inline-block">
												<i class="fa fa-heart" style="color:#ff0000"></i>
											</div>
											<?php
										}
										?>
										<div style="display:inline-block">
											<ul>
												<?php
												if ($total_amount_cash_back > 0) {
													?>
													<li>Total Invoice cash back:
														<?php echo curr_sym; ?><?php echo $promotion_invoice_cash_back; ?>
													</li>
												<?php } ?>

												<?php
												if ($promotion_invoice_free_shipping > 0) {
													?>
													<li>Shipping charges waived on invoice:
														<?php echo curr_sym; ?><?php echo $promotion_invoice_free_shipping; ?>
													</li>
												<?php } ?>

												<?php
												if ($promotion_invoice_discount > 0) {
													?>
													<li>Total Invoice Discount:
														<?php echo curr_sym; ?><?php echo $promotion_invoice_discount; ?>
													</li>
												<?php } ?>
												<?php
												if ($invoice_coupon_status == '1') {
													?>
													<li>Total Invoice Coupon
														<?php echo $inc_txt; ?>:
														<?php echo curr_sym; ?><?php echo round($invoice_coupon_reduce_price); ?>
													</li>
												<?php } ?>


												<?php /*
								  if($total_coupon_used_amount>0){
								  ?>
								  <li>Total Coupon Discount: <?php echo curr_sym; ?> <?php echo round($total_coupon_used_amount); ?></li>
								  <?php } */ ?>

												<?php
												if ($total_amount_saved > 0) {
													?>
													<li>Total amount you saved on invoice:
														<?php echo curr_sym; ?><?php echo $total_amount_saved; ?>
													</li>
												<?php } ?>

												<?php
												if ($total_amount_saved > 0) {
													?>
													<li>Total amount paid on invoice:
														<?php echo curr_sym; ?><?php echo $total_amount_to_pay; ?>
													</li>
												<?php } ?>
											</ul>
										</div>
									</div>
								</div>
								<?php
							}
						}
						?>
						<div class="row margin-top margin-bottom">
							<div class="col-md-12 col-sm-12 text-center">
								<?php $date = $controller->get_order_placed_date($order_id); ?>
								<div class="col-md-4 text-center border_right">
									Ordered on
									<?php echo date("j F Y", strtotime($date)); ?>
									<!--<i class="separator_line"></i>-->
								</div>
								<div class="col-md-4 text-center border_right">
									Order#
									<?php echo $order_id; ?>
									<!--<i class="separator_line"></i>-->
								</div>
								<div class="col-md-4 text-center">
									<span style="font-size:15px;">
										<?php echo count($order_summary_mail_data_arr); ?> Item(s)
									</span>
								</div>
							</div>
						</div>
						<div class="row margin-top margin-bottom">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered cart_summary mb-0">
										<thead>
											<tr>
												<th style="vertical-align: middle;">Product Image</th>
												<th style="vertical-align: middle;">Product Details</th>
												<th style="vertical-align: middle;">Delivery By</th>
												<th style="vertical-align: middle;">Subtotal</th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach ($order_summary_mail_data_arr as $order_summary_mail_data) {
												$total_promo_discount_price = 0;
												$grand_total_default_discount_price = 0;

												$st = '';
												if ($order_summary_mail_data['ord_addon_products_status'] == '1') {
													$st = 'style="display:none;"';
												}

												?>
												<tr>
													<td>
														<a
															href="<?php echo base_url() ?>detail/<?php echo $controller->sample_code() . $order_summary_mail_data['inventory_id']; ?>">

															<?php
															$inventory_id = $order_summary_mail_data['inventory_id'];
															$image = $order_summary_mail_data['image'];
															$img_path = get_inventory_image_path($inventory_id, $image);
															?>
															<img src="<?php echo base_url() . $img_path; ?>"
																alt="SPECIAL PRODUCTS">
														</a>
													</td>
													<td>
														<div class="f-row product-name ordrk-color">
															<a
																href="<?php echo base_url() ?>detail/<?php echo $controller->sample_code() . $order_summary_mail_data['inventory_id']; ?>">
																<?php echo $order_summary_mail_data['ord_sku_name']; ?>
															</a>
															<?php if ($order_summary_mail_data['product_description'] != '') { ?>
																<div class="mb-10">
																	<?php echo html_entity_decode($order_summary_mail_data['product_description']); ?>
																</div>
															<?php } ?>

														</div>
														<div class="f-row small-text">
															<?php
															$inv_obj = $controller->get_inventory_details($order_summary_mail_data['inventory_id']);

															//$inv_obj->attribute_1_value= substr($inv_obj->attribute_1_value, 0, strpos($inv_obj->attribute_1_value, ":"));
															if (!empty($inv_obj->attribute_1))
																echo $inv_obj->attribute_1 . " : " . $inv_obj->attribute_1_value . '<br>';
															if (!empty($inv_obj->attribute_2))
																echo $inv_obj->attribute_2 . " : " . $inv_obj->attribute_2_value . '<br>';
															if (!empty($inv_obj->attribute_3))
																echo $inv_obj->attribute_3 . " : " . $inv_obj->attribute_3_value . '<br>';
															if (!empty($inv_obj->attribute_4))
																echo $inv_obj->attribute_4 . " : " . $inv_obj->attribute_4_value . '<br>';
															?>


														</div>

														<div class="f-row small-text">SKU :<b>
																<?php echo $order_summary_mail_data['sku_id']; ?>
															</b></div>
														<div class="f-row small-text">Qty:<b>
																<?php echo $order_summary_mail_data['quantity']; ?>
															</b></div>
													</td>
													<td>
														<?php
														echo "Expected on " . date("l j F, Y", strtotime($order_summary_mail_data['expected_delivery_date']));
														?><br>
														<?php
														echo "Delivery by <b>" . $order_summary_mail_data['logistics_name'] . "</b>";
														/*
														echo "<b>Delivery Mode:</b> ".$order_summary_mail_data['delivery_mode'];
														?><br>
														<?php
														echo "<b>Package:</b> ".$order_summary_mail_data['parcel_category'];
														*/
														?>
													</td>
													<td align="right">
														<?php
														//print_r($order_summary_mail_data);
														$discount_saved = 0;
														$order_item = $order_summary_mail_data;
														$order_item_id = $order_item["order_item_id"];
														$promotion_item = $order_item["promotion_item"];
														$promotion_item_num = $order_item["promotion_item_num"];

														//no surprise gift--(surprise gift will come when order item is delivered)
												
														if ($promotion_item != '') {
															$quantity = $order_item['quantity'];
															$promotion_minimum_quantity = $order_item['promotion_minimum_quantity'];

															$promo_item = rtrim($promotion_item, ',');
															$promotion_item_num = rtrim($promotion_item_num, ',');

															$promo_item_arr = explode(',', $promo_item);
															$promo_item_num_arr = explode(',', $promotion_item_num);

															$two = array_combine($promo_item_arr, $promo_item_num_arr);

															$temp_quotient = intval($quantity / $promotion_minimum_quantity);
															if ($promotion_item != '') {
																$free_items_arr = $controller->get_free_skus_from_inventory_with_details($promotion_item, $promotion_item_num);
															}
														}
														?>
														<input type="hidden" id="price_paid"
															price_paid="<?php echo $order_item['product_price']; ?>">



														<p class="product-price">
															<?php echo curr_sym; ?>
															<?php if ($promotion_invoice_free_shipping > 0) {
																echo round($order_item['subtotal']);
																$subtotal += round($order_item['subtotal']);
															} else {
																echo round($order_item['grandtotal']);
																$subtotal += round($order_item['grandtotal']);
															}
															?>


															<a data-trigger="click" data-placement="left" data-toggle="popover"
																data-html="true"
																id="price_details_1_<?php echo $order_item_id; ?>"
																content_id="#content_price_details_1_<?php echo $order_item_id; ?>"
																class="price_details_thankyou cursor-pointer"
																onclick="toggle_content(this)">[<i class="fa fa-question"
																	aria-hidden="true"></i>]</a>

														<div id="content_price_details_1_<?php echo $order_item_id; ?>"
															style="display:none;">
															<table class="table table-bordered"
																style="font-size:0.8em;text-align:justify;margin-bottom: 0px;">
																<thead>
																	<tr>
																		<th style="padding:0px"></th>
																		<th width="25%" style="padding:0px"></th>
																		<th style="padding:0px" width="25%"
																			style="text-align: right"></th>
																	</tr>
																</thead>


																<?php
																// /$order_item["ord_addon_products_status"]=='1'
																if (0) {


																	//$total_amount_normal+=round($order_item["ord_addon_total_price"]);
																	$addon = json_decode($order_item["ord_addon_products"]);
																	//print_r($addon);
																	$addon_count = count($addon);

																	?>
																	<tr>
																		<td>Total Price</td>
																		<td>
																			<?php echo $addon_count . ' items'; ?>
																		</td>
																		<td class="text-right">
																			<?php echo curr_sym; ?><?php echo round($order_item["subtotal"]); ?>
																		</td>
																	</tr>

																	<tr>
																		<td colspan="3">Applied Discount
																			<?php echo round($order_item["ord_selling_discount"]); ?>%
																		</td>
																	</tr>

																	<tr>
																		<td colspan="2">Shipping Charge</td>
																		<td class="text-right">
																			<?php echo curr_sym; ?>
																			<?php echo round($order_item["shipping_charge"]); ?>
																		</td>
																	</tr>

																	<?php
																	/*
																	?>
																	<tr>
																	<td colspan="2">Grand Total</td>
																	<td class="text-right" ><?php echo curr_sym; ?><?php echo round($order_item["ord_addon_total_price"]); ?></td>
																	</tr>
																	<?php
																	*/
																	?>

																<?php

																} else {

																	?>


																	<!---normal items popup------>
																	<?php

																	$total_product_price_normal = ($order_item["quantity"] * round($order_item["ord_max_selling_price"]));
																	$shipping_price_normal = $order_item["shipping_charge"];


																	$total_amount_normal = ($shipping_price_normal + $total_product_price_normal);


																	?>

																	<tr>
																		<td>Base product price</td>
																		<td>
																			<?php echo $order_item['quantity']; ?> x
																			<?php echo round($order_item['ord_max_selling_price']); ?>
																		</td>
																		<td class="text-right">
																			<?php echo curr_sym; ?>
																			<?php echo $total_product_price_normal; ?>
																		</td>
																	</tr>

																	<tr>
																		<td colspan="2">Shipping charge for
																			<?php echo $order_item['quantity']; ?> unit(s)
																		</td>
																		<td class="text-right">
																			<?php echo curr_sym; ?>
																			<?php echo $shipping_price_normal; ?>
																		</td>
																	</tr>

																	<tr>
																		<td colspan="2">Total Value </td>
																		<td class="text-right">
																			<?php echo curr_sym; ?>
																			<?php echo $total_amount_normal; ?>
																		</td>
																	</tr>

																	<?php
																	if ($order_item['promotion_available'] == 1) {

																		?>

																		<?php if (intval($order_item['quantity_without_promotion']) > 0 && $order_item['default_discount'] > 0) { ?>

																			<?php

																			$default_discount_price = floatval($order_item['ord_max_selling_price'] - $order_item['individual_price_of_product_without_promotion']); // changed from product price
// $default_discount_price=(($order_item['product_price']*$order_item['default_discount'])/100);
																			$total_default_discount_price = ($order_item['quantity_without_promotion'] * $default_discount_price);
																			$grand_total_default_discount_price += intval($total_default_discount_price);
																			$total_item_price_with_base_price = ($order_item['product_price'] * $order_item['quantity_without_promotion']);

																			?>
																			<tr>
																				<td> Discount @ (
																					<?php echo $order_item['default_discount'] ?>%)<br>Eligible
																					Quantity :
																					<?php echo $order_item['quantity_without_promotion'] ?><br>Applied
																					Offer <br><small style="font-size: .9em;color: blue">
																						<?php echo $order_item['promotion_default_discount_promo']; ?>
																					</small>
																				</td>
																				<td>
																					<?php echo $order_item['quantity_without_promotion'] ?>
																					<i class="fa fa-times" aria-hidden="true"></i>
																					<?php echo $default_discount_price ?>
																				</td>
																				<td class="text-right" style="color:red">
																					<?php echo curr_sym; ?>
																					<?php echo $total_default_discount_price; ?>
																				</td>
																			</tr>


																		<?php } ?>

																		<?php if (intval($order_item['quantity_with_promotion']) > 0) { ?>

																			<?php if (intval($order_item['promotion_discount']) > 0 && (intval($order_item['promotion_discount']) == intval($order_item['default_discount']))) { ?>

																				<?php

																				$default_discount_price = floatval($order_item['ord_max_selling_price'] - $order_item['individual_price_of_product_with_promotion']);
																				// $default_discount_price=(($order_item['product_price']*$order_item['promotion_discount'])/100);
																				$total_default_discount_price = ($order_item['quantity_with_promotion'] * $default_discount_price);
																				$grand_total_default_discount_price += intval($total_default_discount_price);
																				$total_item_price_with_base_price = ($order_item['product_price'] * $order_item['quantity_with_promotion']);

																				?>
																				<tr>
																					<td> Discount @ (
																						<?php echo $order_item['promotion_discount'] ?>%)
																						<br>Eligible Quantity :
																						<?php echo $order_item['quantity_with_promotion'] ?><br>Applied
																						Offer<br><small style="font-size: .9em;color: blue">
																							<?php echo $order_item['promotion_quote']; ?>
																						</small>
																					</td>
																					<td>
																						<?php echo $order_item['quantity_with_promotion'] ?> <i
																							class="fa fa-times" aria-hidden="true"></i>
																						<?php echo $default_discount_price ?>
																					</td>
																					<td class="text-right" style="color:red">
																						<?php echo curr_sym; ?>
																						<?php echo $total_default_discount_price; ?>
																					</td>
																				</tr>

																			<?php } ?>

																			<?php if (intval($order_item['promotion_discount']) > 0 && (intval($order_item['promotion_discount']) != intval($order_item['default_discount']))) { ?>

																				<?php

																				$promo_discount_price = floatval($order_item['ord_max_selling_price'] - $order_item['individual_price_of_product_with_promotion']);

																				//$promo_discount_price=(($order_item['product_price']*$order_item['promotion_discount'])/100);
																				$total_promo_discount_price = ($order_item['quantity_with_promotion'] * $promo_discount_price);
																				$total_item_price_with_base_price = ($order_item['product_price'] * $order_item['quantity_with_promotion']);

																				?>
																				<tr>
																					<td>Promotion Discount @ (
																						<?php echo $order_item['promotion_discount'] ?>%)<br>Eligible
																						Quantity :
																						<?php echo $order_item['quantity_with_promotion'] ?><br>Applied
																						Offer<br><small style="font-size: .9em;color: blue">
																							<?php echo $order_item['promotion_quote']; ?>
																						</small>
																					</td>
																					<td>
																						<?php echo $order_item['quantity_with_promotion'] ?> <i
																							class="fa fa-times" aria-hidden="true"></i>
																						<?php echo $promo_discount_price; ?>
																					</td>
																					<td class="text-right" style="color:red;">
																						<?php echo curr_sym; ?>
																						<?php echo $total_promo_discount_price; ?>
																					</td>
																				</tr>
																			<?php } ?>

																			<?php if (intval($order_item['promotion_discount']) == 0 && $order_item['promotion_default_discount_promo'] == "Price for remaining") { ?>
																				<?php if (intval($order_item['default_discount'] > 0)) { ?>

																					<tr>
																						<td>Standard MRP @ (
																							<?php echo $order_item['product_price'] ?>) <br>Eligible
																							Quantity :
																							<?php echo $order_item['quantity_with_promotion'] ?><br>Applied
																							Offer<br><small style="font-size: .9em;color: blue">
																								<?php echo $order_item['promotion_quote']; ?>
																							</small>
																						</td>
																						<td>
																							<?php echo $order_item['quantity_with_promotion'] ?> <i
																								class="fa fa-times" aria-hidden="true"></i>
																							<?php echo $order_item['product_price'] ?>
																						</td>
																						<td class="text-right">
																							<?php echo curr_sym; ?>
																							<?php echo $order_item["total_price_of_product_with_promotion"]; ?>
																						</td>
																					</tr>

																				<?php } else {
																					?>
																					<tr>
																						<td colspan="3">Applied Offer<br><small
																								style="font-size: .9em;color: blue">
																								<?php echo $order_item['promotion_quote']; ?>
																							</small></td>
																					</tr>
																					<?php
																				} ?>
																			<?php } ?>


																		<?php } ?>
																		<?php if (intval($order_item['quantity_without_promotion']) > 0 && $order_item['default_discount'] == 0) { ?>
																			<!-- no quote and no default promotion---->
																			<?php /*?>											   <tr><td>Standard MRP @ (<?php echo $order_item['product_price'] ?>) <br>Eligible Quantity : <?php echo $order_item['quantity_without_promotion'] ?><br></td><td><?php echo $order_item['quantity_without_promotion'] ?>*<?php echo $order_item['product_price'] ?></td><td class="text-right"><?php echo curr_sym; ?> <?php echo $order_item["total_price_of_product_without_promotion"];?> </td></tr>
														 <?php */ ?>
																		<?php } ?>


																		<!---added --->
														<?php

														/*echo $order_item['promotion_quote'].'||';
														echo $order_item['promotion_default_discount_promo'].'||';
														echo $order_item['default_discount'].'||';*/

														//&& $order_item['promotion_quote']==$order_item['promotion_default_discount_promo']
										
														if ($order_item['promotion_default_discount_promo'] != "" && $order_item['default_discount'] > 0) {
															/* straight discount */

														} else {

															if($order_item['quantity_without_promotion']>0){
																$qt=$order_item['quantity_without_promotion'];
															}else{
																$qt=$order_item['quantity'];
															}

															if ($order_item['ord_selling_discount'] > 0) {

																?>
														<tr>
															<td colspan="2"> Discount @ (
																<?php echo round($order_item['ord_selling_discount']) ?>%)<br>Eligible
																Quantity :
																<?php echo $qt; ?><br>
															</td>
															<td class="text-right">
																<?php echo curr_sym; ?>
																<?php echo ($qt * ($order_item['ord_max_selling_price'] - $order_item['product_price']));

																$discount_saved = round($qt * ($order_item['ord_max_selling_price'] - $order_item['product_price']));

																?>
															</td>
														</tr>
														<?php

															}
														

														}


														?>
														<!---added --->

														<?php } else {

																		if ($order_item['ord_selling_discount'] > 0) {

																			?>
														<tr>
															<td colspan="2">Discount @ (
																<?php echo round($order_item['ord_selling_discount']) ?>%)<br>Eligible
																Quantity :
																<?php echo $order_item['quantity'] ?><br>
															</td>
															<td class="text-right">
																<?php echo curr_sym; ?>
																<?php echo ($order_item['quantity'] * ($order_item['ord_max_selling_price'] - $order_item['product_price']));
																$discount_saved = round($order_item['quantity'] * ($order_item['ord_max_selling_price'] - $order_item['product_price']));
																?>
															</td>
														</tr>
														<?php

																		}

																	} ?>
														<?php

														if ($promotion_item != '') {

															foreach ($free_items_arr as $free_items) {
																?>
														<tr>
															<td>
																<?php echo "SKU : " . $free_items->sku_id . "(free)"; ?>
															</td>
															<td>
																<?php $id = $free_items->id;
																echo "Qty " . ($temp_quotient * $two[$id]); ?>
															</td>
															<td class="text-right">
																<del>
																	<?php echo curr_sym . (($temp_quotient * $two[$id]) * $free_items->selling_price); ?>
																</del>
															</td>
														</tr>
														<?php
															}
														}

														?>

														<!----sku coupon--->
														<?php


														if ($order_item['ord_coupon_status'] == '1') {

															$sku_coupon_used = round($order_item['ord_coupon_reduce_price']);
															?>

														<tr>
															<td colspan="2">Coupon applied on SKU <b>(
																	<?php echo ($order_item['ord_coupon_type'] == '2') ? curr_sym : ''; ?>
																	<?php echo round($order_item['ord_coupon_value']) ?>
																	<?php echo ($order_item['ord_coupon_type'] == '1') ? '%' : 'Flat'; ?>)
																</b>

															</td>
															<td class="text-right">
																<?php echo curr_sym; ?>
																<?php echo round($order_item['ord_coupon_reduce_price']); ?>
															</td>
														</tr>

														<?php } ?>
														<!----sku coupon--->


														<tr>
															<?php

															if ($promotion_invoice_free_shipping > 0) { ?>

															<td>Shipping charge
																( <span style="color:red">waived off</span> )
															</td>
															<td>
																<?php echo $order_item['quantity'] ?> units(s)
															</td>
															<td class="text-right" style="color:red">

																<?php echo curr_sym; ?>
																<?php echo $order_item["shipping_charge"]; ?>

															</td>
															<?php
															} ?>


														</tr>

														<?php
														if ($order_item["cash_back_value"] != "" && $order_item["cash_back_value"] != 0) { ?>
														<tr>

															<td>Cashback value of
																<?php echo $order_item['promotion_cashback'] ?> %
																<br><em>(credited to wallet) </em>
															</td>
															<td>
																<?php echo $order_item['quantity'] ?> <i
																	class="fa fa-times" aria-hidden="true"></i>
																<?php echo round($order_item["cash_back_value"] / $order_item['quantity']) ?>
															</td>
															<td class="text-right">
																<?php echo curr_sym; ?>
																<?php echo $order_item["cash_back_value"]; ?>
															</td>
														</tr>

														<?php

														}
														?>


														<!---minus--->
														<?php
														$minustext = '';

														if ($discount_saved > 0 || $sku_coupon_used > 0 || $promotion_invoice_free_shipping > 0) {
															$minustext = '(';
															$minustext .= curr_sym . $total_amount_normal;
															if (intval($discount_saved) > 0) {
																$minustext .= '-' . curr_sym . $discount_saved;
															}
															if ($total_promo_discount_price > 0) {
																$minustext .= '-' . curr_sym . $total_promo_discount_price;
																$promo_dis += round($total_promo_discount_price);
															}
															if (intval($grand_total_default_discount_price) > 0) {
																$minustext .= '-' . curr_sym . $grand_total_default_discount_price;
																$promo_dis += round($grand_total_default_discount_price);
															}
															if ($sku_coupon_used > 0) {
																$minustext .= '-' . curr_sym . $sku_coupon_used;
															}
															if ($promotion_invoice_free_shipping > 0) {
																$minustext .= '-' . curr_sym . $order_item["shipping_charge"];
															}

															$minustext .= ')';
														}

														?>
														<!---minus--->


														<?php if ($promotion_invoice_free_shipping > 0) { ?>

														<tr>
															<td colspan="2"><em><b>You Pay </b></em>
																<?php echo $minustext; ?>
															</td>
															<td class="text-right"><b>
																	<?php echo curr_sym; ?>
																	<?php echo ($order_item["subtotal"] - $sku_coupon_used); ?>
																</b></td>
														</tr>

														<?php } else { ?>
														<tr>
															<td colspan="2"><em><b>You Pay </b></em>
																<?php echo $minustext; ?>
															</td>
															<td class="text-right"><b>
																	<?php echo curr_sym; ?>
																	<?php echo ($order_item["grandtotal"] - $sku_coupon_used); ?>
																</b></td>
														</tr>
														<?php } ?>



														<?php
														if ($total_amount_normal > $order_item["grandtotal"]) {

															$you_saved = ($total_amount_normal - $order_item["grandtotal"]);
															$you_saved_fin += $you_saved;

															if ($promotion_invoice_free_shipping > 0) {
																$you_saved += $shipping_price_normal;
																$shipping_charge_waived_off += $shipping_price_normal;
															}
															if (intval($sku_coupon_used) > 0) {
																$you_saved += $sku_coupon_used;
															}
															?>

														<tr>
															<td colspan="2"><em><b>You Saved </b></em></td>
															<td class="text-right" style="color:green"><b>
																	<?php echo curr_sym; ?>
																	<?php echo $you_saved; ?>
																</b></td>
														</tr>
														<?php

															//$you_saved_fin+=$you_saved;//shipping charge waived off adding two times.
											
														}
														?>

														<!---normal items popup------>


																	<?php
																}// else condition for combo products 
																?>
															</table>

														</div>



														<?php
														if ($order_item['promotion_available'] == 1) {
															if ($order_item['total_price_of_product_with_promotion'] != "" || $order_item['total_price_of_product_without_promotion'] != "") {
																$r = 0;
																if ($order_item['promotion_quote'] != "") {
																	$r = 1;
																}
																if ($order_item['promotion_default_discount_promo'] != "" && $order_item['default_discount'] > 0 && $order_item['promotion_quote'] != $order_item['promotion_default_discount_promo']) {

																	$r += 1;
																}


																?>

																<a data-trigger="click" data-placement="left" data-toggle="popover"
																	data-html="true" id="price_details_2_<?php echo $order_item_id; ?>"
																	class="price_details_thankyou2 cursor-pointer"
																	content_id="#content_price_details_2_<?php echo $order_item_id; ?>"
																	onclick="toggle_content(this)">Offers :
																	<?php echo $r; ?>
																</a>

																<div id="content_price_details_2_<?php echo $order_item_id; ?>"
																	style="display:none;width:58px;">
																	<table class="table table-bordered"
																		style="font-size: .8em;text-align:justify;margin-bottom: 0px;">
																		<?php
																		$r = 0;
																		if ($order_item['promotion_quote'] != "") {
																			$r = 1;
																			?>
																			<tr>
																				<td class="text-center">
																					<h5 style="color:#0000ff">Offer
																						<?php echo $r; ?>
																					</h5>
																					<?php
																					$promoquote_display_checktotalcostincludingshippingcharge = $order_item['promotion_quote']; ?>
																					<?php
																					if ($order_item['promotion_item'] != "") {
																						if (isset($free_items_arr)) {

																							foreach ($free_items_arr as $freeitemObj) {

																								//$promoquote_display_checktotalcostincludingshippingcharge.=" of ";
																
																								// $promoquote_display_checktotalcostincludingshippingcharge.=$freeitemObj->product_name;
																								$color_attribute_is_there = "no";
																								if (strtolower($freeitemObj->attribute_1) == "color" || strtolower($freeitemObj->attribute_2) == "color" || strtolower($freeitemObj->attribute_3) == "color" || strtolower($freeitemObj->attribute_4) == "color") {
																									$color_attribute_is_there = "yes";
																									if (strtolower($freeitemObj->attribute_1) != "color") {
																										$promoquote_display_checktotalcostincludingshippingcharge .= "<font class='text-warning'> (" . $freeitemObj->attribute_1_value . ")</font>";
																									} else if (strtolower($freeitemObj->attribute_2) != "color") {
																										$promoquote_display_checktotalcostincludingshippingcharge .= "<font class='text-warning'> (" . $freeitemObj->attribute_2_value . ")</font>";
																									} else if (strtolower($freeitemObj->attribute_3) != "color") {
																										$promoquote_display_checktotalcostincludingshippingcharge .= "<font class='text-warning'> (" . $freeitemObj->attribute_3_value . ")</font>";
																									} else if (strtolower($freeitemObj->attribute_4) != "color") {
																										$promoquote_display_checktotalcostincludingshippingcharge .= "<font class='text-warning'> (" . $freeitemObj->attribute_4_value . ")</font>";
																									}
																								} else {
																									/////////////////////////////////	
																
																									$noncolor_attribute_first_is_there = "no";
																									$noncolor_attribute_second_is_there = "no";
																									if (strtolower($freeitemObj->attribute_1) != "color") {
																										$noncolor_attribute_first_is_there = "yes";
																										$promoquote_display_checktotalcostincludingshippingcharge .= " - " . $freeitemObj->attribute_1_value;
																									}
																									if (strtolower($freeitemObj->attribute_2) != "color") {
																										if ($noncolor_attribute_first_is_there == "yes") {
																											$noncolor_attribute_second_is_there = "yes";
																											if ($freeitemObj->attribute_2_value != "") {
																												$promoquote_display_checktotalcostincludingshippingcharge .= "<font class='text-warning'> (" . $freeitemObj->attribute_2_value . ")</font>";
																											}
																										} else {
																											$noncolor_attribute_first_is_there = "yes";
																											$promoquote_display_checktotalcostincludingshippingcharge .= " - " . $get_free_item_data_image_path_attributes_arr["attribute_2_value"];
																										}
																									}
																									if (strtolower($freeitemObj->attribute_3) != "color") {
																										if ($noncolor_attribute_first_is_there == "no" || $noncolor_attribute_second_is_there == "no") {
																											if ($noncolor_attribute_first_is_there == "yes") {
																												$noncolor_attribute_second_is_there = "yes";
																												if ($freeitemObj->attribute_3_value != "") {
																													$promoquote_display_checktotalcostincludingshippingcharge .= "<font class='text-warning'> (" . $freeitemObj->attribute_3_value . ")</font>";
																												}
																											} else {
																												$noncolor_attribute_first_is_there = "yes";
																												$promoquote_display_checktotalcostincludingshippingcharge .= " - " . $freeitemObj->attribute_3_value;
																											}
																										}
																									}
																									if (strtolower($freeitemObj->attribute_4) != "color") {
																										if ($noncolor_attribute_first_is_there == "no" || $noncolor_attribute_second_is_there == "no") {
																											if ($noncolor_attribute_first_is_there == "yes") {
																												$noncolor_attribute_second_is_there = "yes";
																												if ($freeitemObj->attribute_4_value != "") {
																													$promoquote_display_checktotalcostincludingshippingcharge .= "<font class='text-warning'> (" . $freeitemObj->attribute_4_value . ")</font>";
																												}
																											} else {
																												$noncolor_attribute_first_is_there = "yes";
																												$promoquote_display_checktotalcostincludingshippingcharge .= " - " . $freeitemObj->attribute_4_value;
																											}
																										}
																									}

																									//////////////////////////////
																
																								}
																								$color_attribute_is_there = "no";
																								$noncolor_attribute_first_is_there = "no";
																								$noncolor_attribute_second_is_there = "no";
																								$promoquote_display_checktotalcostincludingshippingcharge .= " <img src='" . base_url() . $freeitemObj->image . "' width='40%'>";




																							}
																						}
																					}
																					echo $promoquote_display_checktotalcostincludingshippingcharge . "<br>";
																					?>
																				</td>
																			</tr>
																			<?php
																		}
																		if ($order_item['promotion_default_discount_promo'] != "" && $order_item['default_discount'] > 0 && $order_item['promotion_quote'] != $order_item['promotion_default_discount_promo']) {
																			$r += 1;
																			?>
																			<tr>
																				<td class="text-center">
																					<h5 style="color:#0000ff">Offer
																						<?php echo $r; ?>
																					</h5>
																					<?php echo $order_item['promotion_default_discount_promo'] ?>
																				</td>
																			</tr>
																		<?php
																		}
																		?>
																	</table>
																</div>
															<?php
															}
														} else {
															?>
															<!-----added offer newly ---->
															<?php
															if ($order_item['ord_selling_discount'] > 0) {
																?>
																<!--<small>(<i>Inclusive of shipping charges</i>)</small>-->
																<a data-trigger="click" data-placement="bottom"
																	data-toggle="price_details2" data-html="true"
																	id="price_details_3_<?php echo $order_item_id; ?>"
																	class="price_details_2 cursor-pointer"
																	content_id="#content_price_details_3_<?php echo $order_item_id; ?>"
																	onclick="toggle_content_ord_selling_discount(this)">Offers : 1 </a>
																<div id="content_price_details_3_<?php echo $order_item_id; ?>"
																	style="display:none;width:72px;">
																	<table class="table table-bordered"
																		style="font-size: .8em;text-align:justify;margin-bottom: 0px;">
																		<tr class="success">
																			<td colspan="2">Discount
																				<?php echo round($order_item['ord_selling_discount']); ?>
																				% Off
																			</td>
																		</tr>
																	</table>
																</div>
															<?php
															}
															?>
															<!-----added offer newly ---->

															<?php

														}
														?>

														</p>
													</td>
												</tr>
												<?php
												if ($order_summary_mail_data['ord_addon_single_or_multiple_tagged_inventories_in_frontend'] == 'single') {
													if ($order_summary_mail_data['ord_addon_products'] == '"[]"') {
														$arr = array();
													} else {
														$arr = json_decode($order_summary_mail_data['ord_addon_products']);
														$count = count($arr);
													}
													if (!empty($arr)) {
														?>
														<!-----addon products-->
														<?php /* ?><h5 class=""><i><b>Combo products (<?php echo $count; ?>)</b></i></h5><?php */ ?>


														<?php

														//if($order_summary_mail_data['ord_addon_inventories']!=''){
														if (0) {

															$i = 1;
															foreach ($arr as $val) {


																$inv_id = $val->inv_id;
																$name = $val->inv_name;
																$image = $val->inv_image;
																$sku_id = $val->inv_sku_id;
																$price = $val->inv_price;
																$inv_mrp_price = $val->inv_mrp_price;

																?>


																<tr>
																	<td>
																		<div class="products-block-left">
																			<?php
																			$img_path = get_inventory_image_path_combo($inv_id, $image);
																			?>
																			<img src="<?php echo base_url() . $img_path; ?>"
																				alt="ADDITIONAL PRODUCTS">

																		</div>
																	</td>

																	<td colspan="3">
																		<div class="f-row product-name ordrk-color">
																			<div class="f-row small-text"><b>Addon for
																					<?php echo $order_summary_mail_data['ord_sku_name']; ?>
																				</b></div>
																			<a>
																				<?php echo $name; ?>
																			</a>

																			<div class="f-row small-text">SKU :<b>
																					<?php echo $sku_id; ?>
																				</b></div>
																			<?php
																			if (round($inv_mrp_price) != round($price)) {
																				?>
																				<div class="f-row small-text">Price :<b> <del>
																							<?php echo curr_sym . round($inv_mrp_price); ?>
																						</del>
																						<?php echo curr_sym . round($price); ?>
																					</b></div>
																				<?php
																			} else {
																				?>
																				<div class="f-row small-text">Price :<b>
																						<?php echo curr_sym . round($price); ?>
																					</b></div>
																				<?php
																			}
																			?>

																		</div>
																	</td>

																</tr>


																<?php

																$i++;
															}
														}

														?>
														<!-----addon products-->
													<?php
													}
												}
												?>
												<script>

													$(document).ready(function () {
														var order_item_id = '<?php echo $order_item_id; ?>';

														$("#price_details_1_" + order_item_id).popover({
															html: true,
															content: function () {
																if ($(window).width() > 480) {
																	$('#content_price_details_1_' + order_item_id).hide();
																	return $('#content_price_details_1_' + order_item_id).html();
																}
															}
														});

														$("#price_details_2_" + order_item_id).popover({
															html: true,
															content: function () {
																if ($(window).width() > 480) {
																	$('#content_price_details_2_' + order_item_id).hide();
																	return $('#content_price_details_2_' + order_item_id).html();
																}
															}
														});

													});
												</script>
											<?php } ?>
										</tbody>
										<tfoot>
											<tr>
												<td colspan="4" style="vertical-align:middle;">
													<small>
														<b>
															<?php
															if ($subtotal > 0) {
																?>
																<p>Total Amount:
																	<?php echo curr_sym; ?>
																	<?php echo $subtotal; ?>
																</p>
															<?php } ?>


															<?php
															if (intval($sku_coupon_used) > 0) {
																?>
																<p>Total Coupon used Amount on SKUs:
																	<?php echo curr_sym; ?>
																	<?php echo $sku_coupon_used; ?>
																</p>
															<?php } ?>

															<?php
															if ($invoice_coupon_status == '1') {
																?>
																<p>Total Invoice Coupon
																	<?php echo $inc_txt; ?>:
																	<?php echo curr_sym; ?>
																	<?php echo round($invoice_coupon_reduce_price); ?>
																</p>
															<?php } ?>

															<!--- with sku coupon used amount ---->
															<?php
															if (intval($sku_coupon_used) > 0 && $invoice_coupon_status == '1') {
																?>
																<p>Total Coupon Discount:
																	<?php echo curr_sym; ?>
																	<?php echo round($total_coupon_used_amount); ?>
																</p>
															<?php } ?>
														</b>
													</small>
												</td>
											</tr>
											<tr>
												<td align="right" colspan="2">
													<small>
														<b>
															<?php
															//echo '||'.$total_amount_saved.'||'.$you_saved_fin.'||'.$promo_dis;
															if ($total_amount_saved > 0) {
																// echo "Your Total Saving is " . curr_sym . "" . ($total_amount_saved + $you_saved_fin + $promo_dis);
																echo "Your Total Saving is " . curr_sym . "" . round($total_amount_saved);
															} elseif ($you_saved_fin > 0) {
																echo "Your Total Saving is " . curr_sym . "" . ($you_saved_fin);//+$discount_saved
															} else {
																echo "";
															}
															?>
														</b>
													</small>
												</td>
												<td colspan="2" align="right" style="color:#004b9b;font-size:1.2em;">Grand
													Total<span class="order_grand_total">
														<?php echo curr_sym; ?>
														<?php //echo $total_amount_to_pay;  ?>
														<?php echo (intval($subtotal) - intval($total_coupon_used_amount)); ?>
													</span>
												</td>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
		?>
	</div>
</div>
<!-- END Home slideder-->
<script>
	function toggle_content(ele) {
		var cont_id = $(ele).attr("content_id");
		//alert(cont_id)
		if ($(window).width() <= 480) {
			$(ele).popover('hide');
			$(cont_id).toggle();
		}
	}
	function toggle_content_ord_selling_discount(ele) {
		var cont_id = $(ele).attr("content_id");
		//alert(cont_id)
		//if($(window).width() <= 480){
		$(ele).popover('hide');
		$(cont_id).toggle();
		//}
	}
</script>