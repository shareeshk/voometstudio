<?php
	$email_stuff_arr=email_stuff();
?>

<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo base_url()?>search" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">About Us</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">Links</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
									<li class="active"><span></span><a href="<?php echo base_url();?>aboutus">About Us</a></li>
                                    
                                    <li><span></span><a href="<?php echo base_url();?>sellthroughus">Sell Through Us</a></li>
									
									
									<!--<li><span></span><a href="<?php //echo base_url();?>returns">Returns</a></li>
									<li><span></span><a href="<?php //echo base_url();?>replacements">Replacements</a></li>
									<li><span></span><a href="<?php //echo base_url();?>refunds">Refunds</a></li>
									<li><span></span><a href="<?php //echo base_url();?>cancellations">Cancellations</a></li>-->
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
                
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9 section8" id="center_column">
                <!-- page heading-->
              
<h3 class="section-title text-center align-bottom align-top">About Us</h3>

                <!-- Content page -->
                <div class="content-text clearfix">
               
					
					<span style="font-size:15px;">

					<p>Voomet Studio is a dynamic design house committed to innovation, creativity, and quality in every project. Specializing in furniture ecommerce, we excel in enhancing customer experiences through advanced tools and technologies that allow for interactive visualization of furniture in personal spaces. 
					<br><br>
					Our mission extends beyond selling products; we aim to help individuals and businesses create personalized spaces that reflect their unique personalities and preferences. With a focus on customer satisfaction, honesty, transparency, action orientation, and efficiency, Voomet Studio is dedicated to building long-term, sustainable partnerships and driving industry-leading solutions in the ever-evolving digital marketplace.
					
					</p>

										</span>

                  

                </div>
                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<!-- ./page wapper-->
