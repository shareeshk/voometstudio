<style type="text/css">

.margin-b-20{
    margin-top:20px ;
    margin-bottom:20px ;
    text-transform: capitalize;
}
.highlight{
    color:red;
}
.disc-style{
    list-style: disc;
    margin-left: 20px;
    margin-bottom: 10px;
}
.number-style{
    list-style: number;
    margin-left: 20px;
    margin-bottom: 10px;
}
</style>

<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo base_url()?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Business Policies</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">Infomations</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                 <ul class="tree-menu">
                                    <li><span></span><a href="<?php echo base_url();?>termsandconditions">Terms &amp; Conditions</a></li>
                                 	
									<li><span></span><a href="<?php echo base_url();?>policy">Privacy Policy</a></li>
                                    <li class="active"><span></span><a href="<?php echo base_url();?>business_policy" >Business Policies</a></li>
									<!-- <li><span></span><a href="<?php echo base_url();?>payments">Payments</a></li>
									
									<li><span></span><a href="<?php echo base_url();?>delivery">Delivery</a></li>
									<li><span></span><a href="<?php echo base_url();?>warranty">Warranty</a></li> -->
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
                
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading text-center">
                    <span class="page-heading-title2">Business Policies</span>
                </h2>
                <!-- Content page -->
                <div class="content-text clearfix">    


<h4 class="margin-b-20">
   <b>Returns</b>
</h4>

<p>Voomet Studio is dedicated to providing high-quality furniture products and exceptional customer service. Our business policy outlines our commitments to our customers, partners, and stakeholders.</p>



<h4 class="margin-b-20">
   <b>1. Quality Assurance</b>
</h4>
<p>We are committed to crafting furniture of the highest quality using premium materials and skilled craftsmanship. Each piece is meticulously inspected to ensure durability, functionality, and aesthetic appeal.
</p>

<h4 class="margin-b-20">
   <b>2. Customer Satisfaction</b>
</h4>
<p>Our customers are our top priority. We strive to exceed expectations by offering personalized assistance, timely responses to inquiries, and hassle-free shopping experiences. Your satisfaction is our measure of success.
</p>

<h4 class="margin-b-20">
   <b>3. Sustainability</b>
</h4>
<p>We are committed to sustainable business practices and environmental stewardship. We source materials responsibly, minimize waste in our production processes, and support initiatives that promote sustainability within our industry.
</p>

<h4 class="margin-b-20">
   <b>4. Ethical Standards</b>
</h4>
<p>We adhere to ethical standards in all aspects of our business operations. We treat our employees, partners, and customers with fairness, honesty, and respect. Integrity is the foundation of our business practices.
</p>

<h4 class="margin-b-20">
   <b>5. Innovation</b>
</h4>
<p>We embrace innovation in design, technology, and business processes to stay ahead in a competitive market. We continuously explore new ideas and trends to offer fresh, innovative furniture solutions to our customers.
</p>

<h4 class="margin-b-20">
   <b>6. Innovation</b>
</h4>
<p>We embrace innovation in design, technology, and business processes to stay ahead in a competitive market. We continuously explore new ideas and trends to offer fresh, innovative furniture solutions to our customers.
</p>

<h4 class="margin-b-20">
   <b>7. Partnership</b>
</h4>
<p>We value collaborative partnerships with suppliers, distributors, and retailers who share our commitment to excellence and integrity. Together, we strive to create mutually beneficial relationships that drive growth and success.
</p>


<h4 class="margin-b-20">
   <b>8. Community Engagement</b>
</h4>
<p>We believe in giving back to the communities where we operate. Through philanthropic initiatives and community outreach programs, we aim to make a positive impact and contribute to the well-being of society.
</p>

<h4 class="margin-b-20">
   <b>9. Continuous Improvement</b>
</h4>
<p>We are dedicated to continuous improvement in all aspects of our business. We actively seek feedback from customers and stakeholders to identify areas for enhancement and implement solutions that drive progress.
</p>

<h4 class="margin-b-20">
   <b>10. Compliance</b>
</h4>
<p>We adhere to all applicable laws, regulations, and industry standards governing our business operations. Compliance with legal and regulatory requirements is non-negotiable.
</p>

<h4 class="margin-b-20">
   <b>11. Transparency</b>
</h4>
<p>We believe in transparency and open communication with our customers, partners, and stakeholders. We provide clear and accurate information about our products, services, and policies to foster trust and accountability. At Voomet Studio, we are committed to upholding these principles and delivering exceptional value to our customers and partners.
</p>

<h4 class="margin-b-20">
   <b>12. Returns</b>
</h4>
<ul class="number-style">

<li>You may return your purchase within 5 days of receiving your order for a refund or store credit.</li>
<li>To initiate a return, please contact our customer service team at customer service email or phone number in contact us page to obtain a Return Authorization (RA) number.</li>
<li>Items must be returned in their original packaging, unused, and in the same condition as received.</li> 
<li>Return shipping costs are the responsibility of the customer, unless the return is due to a manufacturing defect or shipping error on our part.</li>
<li>Once we receive and inspect the returned item, we will issue a refund or store credit, as per your preference.</li>
</ul>

<h4 class="margin-b-20">
   <b>13. Replacements</b>
</h4>
<ul class="number-style">

<li>If you receive a damaged, defective, or incorrect item, please contact us within 5 days of delivery to arrange for a replacement.</li>
<li>We may require photographic evidence of the damage or defect for our records.</li>
<li>Replacement items will be shipped to you at no additional cost.</li> 
<li>If the item is no longer available, we will offer a suitable replacement or provide a refund.</li>
</ul>


<h4 class="margin-b-20">
   <b>14. Exceptions</b>
</h4>
<ul class="number-style">

<li>Custom-made or personalized items may not be eligible for return or replacement unless they are defective or damaged</li>
<li>Clearance or final sale items are non-returnable and non-refundable.</li>
<li>Returns of large furniture items may be subject to restocking fees.</li>
</ul>

<h4 class="margin-b-20">
   <b>15. Refunds</b>
</h4>
<ul class="number-style">

<li>Refunds will be issued to the original payment method used for the purchase.</li>
<li>Please allow 10 business days for the refund to be processed and reflected in your account.</li>

</ul>

<h4 class="margin-b-20">
   <b>16. Cancellation Policy</b>
</h4>
<ul class="number-style">

<li><strong>Order Cancellation</strong>: You may cancel your order for eligible items within hours of placing it, provided that the order has not yet been processed or shipped. To cancel your orde12hrs, please contact our customer service team as soon as possible.
</li>
<li><strong>Cancellation Fees</strong>: Orders cancelled after 12 hours may be subject to a cancellation fee to cover any incurred processing or restocking costs. The cancellation fee will be deducted from your refund.
</li>
<li><strong>Custom Orders</strong>: Custom or made-to-order items may not be cancelled once production has begun. Please review your custom order carefully before placing it to avoid any cancellation fees.
</li>
<li><strong>Refunds for Cancelled Orders</strong>: Refunds for cancelled orders will be processed promptly, and funds will be returned to the original payment method used for the purchase.
</li>
</ul>

<h4 class="margin-b-20">
   <b>17. Disclaimer</b>
</h4>

<p>
The products sold on the Website are intended to be replaceable in the case of any defect. 
Orders made wrongly or other non-defect related orders will not be entertained for replacements. We urge the buyers to ascertain the right requirements before placing their order. We will not be responsible if proper care is not taken by the Buyer while ordering, resulting in orders made wrongly. In such cases we will not entertain replacement or refund of the products. </p>

<p>The Buyer is responsible for determining the suitability of various products listed on this website for their various needs and that the products are usable in condition that they arrive in.</p> 

<p>For the avoidance of doubt, the Returns, Replacements, and Refunds Policies only relate to the Buyer and Seller and that the Seller cannot make any claims whatsoever towards the company or its brand. 
</p>

                 
                </div>
                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<!-- ./page wapper-->
