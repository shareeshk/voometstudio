<?php 
	if($this->session->userdata("franchise_name")){

		if($this->session->userdata("cur_page")){
			$cur_page=$this->session->userdata("cur_page");
			header("Location: ".$cur_page."");
		}else{	
			header("Location: ".base_url()."");
		}
	}

?>
<?php
	$email_stuff_arr=email_stuff();
?>
<style type="text/css">
.form-group .help-block {
  display: none;
}
.form-group.has-error .help-block {
  display: block;
}
#mydiv {  
    position:absolute;
    top:0;
    left:0;
    width:100%;
    height:100%;
    z-index:1000;
   /* background-color:#fff;*/
    opacity: .8;
 }

.ajax-loader {
    position: absolute;
    left: 50%;
    top: 50%;
    margin-left: -32px; /* -1 * image width / 2 */
    margin-top: -32px;  /* -1 * image height / 2 */
    display: block;     
}
#mydiv_re {  
    position:absolute;
    top:0;
    left:0;
    width:100%;
    height:100%;
    z-index:1000;
   /* background-color:#fff;*/
    opacity: .8;
 }

.ajax-loader_re {
    position: absolute;
    left: 50%;
    top: 32%;
    margin-left: -32px; /* -1 * image width / 2 */
    margin-top: -32px;  /* -1 * image height / 2 */
    display: block;     
}
</style>

<script type="text/javascript">

module.directive('emailExistC', function($http) {
	return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attr, ctrl) {
			function customValidator(ngModelValue)
			{
				ctrl.$setValidity('emailexistValidator', true);
				var FormData = {'email' : ngModelValue};

				 $http({
					method : "POST",
					url : "<?php echo base_url();?>Franchiseloginsession/check_email",
					data:FormData,
					dataType: 'json',
                                        async:false
					}).success(function mySucces(res) {
						if(res.valid!=null)
						{
							if(res.valid==true)
							{
								ctrl.$setValidity('emailexistValidator',true );
							}
							if(res.valid==false){
								ctrl.$setValidity('emailexistValidator', false);
							}
						}
						else{
							ctrl.$setValidity('emailexistValidator', true);
						}

					}, function myError(response) {
							/*bootbox.alert({
							  size: "small",
							  message: 'Error',
							});*/
							alert('Error');
					});

				return ngModelValue;
			}
			ctrl.$parsers.push(customValidator);
      }
    };
});

module.directive('mobileExistC', function($http) {
	return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attr, ctrl) {

			function custommValidator(ngModelValue)
			{
				ctrl.$setValidity('mobileexistValidator',true );
				var FormData = {'mobile' : ngModelValue};
				 $http({
					method : "POST",
					url : "<?php echo base_url();?>Franchiseloginsession/check_mobile",
					data:FormData,
					dataType: 'json',
                                        async:false
					}).success(function mySucces(res) {
						if(res.valid!=null)
						{
							if(res.valid==true)
							{
								ctrl.$setValidity('mobileexistValidator',true );
							}
							if(res.valid==false){
								ctrl.$setValidity('mobileexistValidator',false);
							}
						}else{
							ctrl.$setValidity('mobileexistValidator',true );
						}

					}, function myError(response) {
						/*/bootbox.alert({
						  size: "small",
						  message: 'Error',
						});*/
						alert('Error');
					});

				return ngModelValue;
			}
			ctrl.$parsers.push(custommValidator);
      }
    };
});


module.directive('validPasswordC', function() {

  return {
    require: 'ngModel',
    scope: {
      reference: '=validPasswordC'
    },
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$setValidity('noMatch', true);
      ctrl.$parsers.unshift(function(viewValue, $scope) {

        var noMatch = viewValue != scope.reference
        ctrl.$setValidity('noMatch', !noMatch);
        return (noMatch)?noMatch:!noMatch;
      });

      scope.$watch("reference", function(value) {;
        ctrl.$setValidity('noMatch', value === ctrl.$viewValue);
      });
    }
  }
});

module.directive('allowOnlyNumbers', function () {
            return {  
                restrict: 'A',  
                link: function (scope, elm, attrs, ctrl) {  
                    elm.on('keydown', function (event) {  
                        if (event.which == 64 || event.which == 16 || event.which == 46 || event.which == 86) {  
                            // to allow numbers  
                            return false;  
                        } else if (event.which >= 48 && event.which <= 57) {  
                            // to allow numbers  
                            return true;  
                        } else if (event.which >= 96 && event.which <= 105) {  
                            // to allow numpad number  
                            return true;  
                        } else if ([8, 9, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {  
                            // to allow backspace, enter, escape, arrows  
                            return true;  
                        } else {  
                            event.preventDefault();  
                            // to stop others  
                            return false;  
                        }  
                    });  
                }  
            }  
        });
module.directive('showErrors', function($timeout) {
    return {
      restrict: 'A',
      require: '^form',
      link: function (scope, el, attrs, formCtrl) {
        // find the text box element, which has the 'name' attribute
        var inputEl   = el[0].querySelector("[name]");
        // convert the native text box element to an angular element
        var inputNgEl = angular.element(inputEl);
        // get the name on the text box
        var inputName = inputNgEl.attr('name');

        // only apply the has-error class after the user leaves the text box
        var blurred = false;
        inputNgEl.bind('blur', function() {
          blurred = true;
          el.toggleClass('has-error', formCtrl[inputName].$invalid);
        });

        scope.$watch(function() {
          return formCtrl[inputName].$invalid
        }, function(invalid) {
          // we only want to toggle the has-error class after the blur
          // event or if the control becomes valid
          if (!blurred && invalid) { return }
          el.toggleClass('has-error', invalid);
        });

        scope.$on('show-errors-check-validity', function() {
          el.toggleClass('has-error', formCtrl[inputName].$invalid);
        });

        scope.$on('show-errors-reset', function() {
          $timeout(function() {
            el.removeClass('has-error');
          }, 0, false);
        });
      }
    }
  });



function generate_random_number()
{
	ra=Math.floor(100000 + Math.random() * 900000);
	return ra;
}

module.directive('showOtperrors', function($timeout) {
    return {
      restrict: 'A',
      require: '^form',
      link: function (scope, el, attrs, formCtrl) {
        var inputEl   = el[0].querySelector("[name]");
        var inputNgEl = angular.element(inputEl);
        var inputName = inputNgEl.attr('name');
        var blurred = false;
        inputNgEl.bind('blur', function() {
          blurred = true;
          el.toggleClass('has-error', formCtrl[inputName].$invalid);
        });
        
        scope.$watch(function() {
          return formCtrl[inputName].$invalid
        }, function(invalid) {
          if (!blurred && invalid) { return }
          el.toggleClass('has-error', invalid);
        });
        
        scope.$on('show-errors-check-validity', function() {
          el.toggleClass('has-error', formCtrl[inputName].$invalid);
        });
        
        scope.$on('show-errors-reset', function() {
          $timeout(function() {
            el.removeClass('has-error');
          }, 0, false);
        });
      }
    }
  });
module.directive('userExistC', function($http) {
	return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attr, ctrl) {
			function custmValidator(ngModelValue)
			{
				ctrl.$setValidity('userexistValidator', true);

				return ngModelValue;
			}
			ctrl.$parsers.push(custmValidator);
      }
    };
});

module.directive('passwordCheckC', function($http) {
	return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attr, ctrl) {
			function custmmValidator(ngModelValue)
			{
				ctrl.$setValidity('passwordwrongValidator', true);	
				return ngModelValue;
			}
			ctrl.$parsers.push(custmmValidator);     
      }
    };
});
module.service('myservice', function() {
      this.add = function(mobile){
			this.mobile_t = mobile;
		  };
		this.get=function(){
			//return mobile_t;
			this.setmobileno=this.mobile_t;
		}
    });
function NewUserController($scope,Scopes,$http, myservice) {
	Scopes.store('NewUserController', $scope);
	//$scope.formData = MyService.formData;
	$scope.myservice = myservice;
	$scope.regex=/(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z]).{6,10}/;

	$scope.save = function($event) {

    $scope.$broadcast('show-errors-check-validity');
	if ($scope.userForm.$valid) {
		myservice.add($scope.formData.mobile);
				/*password=$scope.formData.password;
				original_password=$scope.formData.password;

				var temppwd="";
				for(i=0;i<password.length;i++){
				temppwd+=String.fromCharCode(password.charCodeAt(i)-6);
				}*/

				var temppwd="";

			$data=$scope.formData;
			$data.temppassword=temppwd;

			if(Scopes.get('NewUserController').c_id != null)
			{
				c_id=Scopes.get('NewUserController').c_id;
				$data.c_id=c_id;

				url="<?php echo base_url();?>Franchiseloginsession/update_customer";
			}else{

				url="<?php echo base_url();?>Franchiseloginsession/add_customer";
			}

			original_password='';
		$scope.formData.password=original_password;

		$("#reg_btn").html('<i class="fa fa-spin fa-refresh"></i> Processing');
        document.getElementById("reg_btn").disabled = true;
		$http({
        method : "POST",
        url : url,
		data: $data,
		dataType: 'json',
                async:false
		}).success(function mySucces(res) {

			if(res.data==true)
			{
				myservice.get();
				$scope.c_id=res.c_id;
				$("#createuser").hide();
				$("#show_change_mobile").show();
				$("#mobile_verification").show();
				$scope.setvariables();
				 //$scope.reset_for_change();
			}else{
				/*bootbox.alert({
				  size: "small",
				  message: 'New User not created',
				});*/
				alert('New User not created');
			}
            $("#reg_btn").html('<i class="fa fa-user"></i> Create');
            document.getElementById("reg_btn").disabled = false;

		},function myError(response) {

		});


    }
        $event.stopPropagation();
  };

  $scope.get_city_state_details_by_pincodeFun=function(){
		pin=$scope.formData.pin;
		
		if(pin===undefined){
		}
		else{
		$http({
				method : "POST",
				url : "<?php echo base_url();?>Account/get_city_state_details_by_pincode",
				data:{'pin' : pin},
				dataType:"JSON",
				}).success(function mySucces(result) {
					$scope.formData.city=result.city;
					$scope.formData.state=result.state;
					$scope.formData.country="India";
					
					if(parseInt(result.count)==0){
                        $scope.delivery_address_pin_error=1;
                        $('.delivery_address_pin_error').show();
                        $('#pin_code_check').addClass('has-error');
                    }else{
                        //alert(result.count+"no error");
                        $scope.delivery_address_pin_error=0;
                        $('.delivery_address_pin_error').hide();
                        $('#pin_code_check').removeClass('has-error');
                    }

				}, function myError(response) {
					
				});
		}	
	 }

  $scope.setvariables = function() {
			  if(Scopes.get('NewUserController').c_id != null)
			  {
				c_id=Scopes.get('NewUserController').c_id;
				mobile=Scopes.get('NewUserController').formData.mobile;
				email=Scopes.get('NewUserController').formData.email;
				name=Scopes.get('NewUserController').formData.name;
				//alert('c_id='+c_id+',mobile='+mobile+',email='+email);
				$scope.$broadcast('show-errors-reset');
				$scope.formData = {name: name, email: email,mobile: mobile,password: '',password_c: ''};
			  }
        }

    $scope.reset = function() {
    $scope.$broadcast('show-errors-reset');
    $scope.formData = {name: '', email: '',mobile: '',password: '',password_c: ''};
  }

	$scope.go_back=function(){
		$('#login_form').show();
		$('#createuser').hide();
		$('#forgot_form').hide();
		$('#mobile_verification').hide();
	}
}

function MobileOtpController($scope, Scopes, $http, myservice) {

    Scopes.store('MobileOtpController', $scope);
    //$scope.formData = MyService.formData;
    $scope.myservice = myservice;
	$scope.show_change_number=1;
    $scope.change_mobile = function () {
        c_id = Scopes.get('NewUserController').c_id;
        $("#createuser").show();
        $("#mobile_verification").hide();

    };

    $scope.resend_code = function () {

        c_id = Scopes.get('NewUserController').c_id;
        mobile = Scopes.get('NewUserController').formData.mobile;
		if(c_id===undefined){
			c_id=Scopes.get('LoginController').c_id;
		}
        
        $data = {'c_id': c_id, 'val': mobile}
        $("#createuser").hide();
		
        $("#mobile_verification").show();
        url = "<?php echo base_url();?>Franchiseloginsession/resend_code_to_newuser";
        $http({
            method: "POST",
            url: url,
            data: $data,
            dataType: 'json',
            async:false
        }).success(function mySucces(res) {

            if (res.data == true) {
                /*bootbox.alert({
                    size: "small",
                    message: 'Code has been resent to your mobile number',
                });*/
                alert('Code has been resent to your mobile number');

            } else {

                /*bootbox.alert({
                    size: "small",
                    message: 'New User not created',
                });*/
                alert('New User not created');

            }

        }, function myError(response) {
            /*bootbox.alert({
                size: "small",
                message: 'Error',
            });*/
            alert('Error');
        });


    }

    $scope.otpverify = function () {

        c_id = Scopes.get('NewUserController').c_id;
		$scope.show_change_number=1;
		if(c_id===undefined){
			c_id=Scopes.get('LoginController').c_id;
			$scope.show_change_number=0;

		}

        if ($scope.otp_code != null && c_id !==undefined) {
            var otpData = {'otp_code': $scope.otp_code, 'c_id': c_id};
            $("#verify_btn").html('<i class="fa fa-spin fa-refresh"></i> Processing');
            document.getElementById("verify_btn").disabled = true;
            $http({
                method: "POST",
                url: "<?php echo base_url();?>Franchiseloginsession/check_otpcode",
                data: otpData,
                dataType: 'json',
            }).success(function mySucces(res) {

                if (res.data != null) {
                    if (res.data == true) {

                        c_id = Scopes.get('NewUserController').c_id;
						if(c_id===undefined){
                            c_id=Scopes.get('LoginController').c_id;
                        }
                        var active_data = {'c_id': c_id};

                        url = "<?php echo base_url();?>Franchiseloginsession/activate_new_user";
                        $http({
                            method: "POST",
                            url: url,
                            data: active_data,
                            dataType: 'json',
                        }).success(function mySucces(res) {
                            //alert(res.data);
                            if (res.data == true) {

								cur_page = '<?php echo $this->session->userdata("cur_page");?>';
								base_url_site = "<?php echo base_url();?>";

								alert("Mobile no " + res.mobileno_in_session + " is verified, You are now a registered user.");
								if (cur_page) {
									window.location = cur_page;
								} else {
									window.location = base_url_site;
								}

                            } else {
                                /*bootbox.alert({
                                    size: "small",
                                    message: 'New User not activated',
                                });*/
                                alert('New User not activated');
                            }

                        }, function myError(response) {
                            /*bootbox.alert({
                                size: "small",
                                message: 'Error',
                            });*/
                            alert('Error');
                        });

                    }
                    if (res.data == false) {
                        $scope.mobileOtp.otp_code.$setValidity("otpsameValidator", false);
                    }

                } else {
                    $scope.mobileOtp.otp_code.$setValidity("otpsameValidator", true);
                }

                $("#verify_btn").html('<i class="fa fa-check"></i> Verify');
                document.getElementById("verify_btn").disabled = false;

            }, function myError(response) {
                /*bootbox.alert({
                    size: "small",
                    message: 'Error',
                });*/
                alert('Error');
                $("#verify_btn").html('<i class="fa fa-check"></i> Verify');
                document.getElementById("verify_btn").disabled = false;

            });

            $scope.$broadcast('show-errors-check-validity');

        }//if condition
    };
}


function MobileOtpController_verifyfalse($scope, Scopes, $http, myservice) {

    Scopes.store('MobileOtpController_verifyfalse', $scope);
    //$scope.formData = MyService.formData;
    $scope.myservice = myservice;


    $scope.resend_code_verifyfalse = function () {
        c_id = Scopes.get('LoginController').c_id;
        mobile = Scopes.get('LoginController').mobile;
        $data = {'c_id': c_id, 'val': mobile}
        //myservice.add($scope.mobile);
        //myservice.get();

        $("#loginForm").hide();
        $("#mobile_verification_verifyfalse").show();
        url = "<?php echo base_url();?>Franchiseloginsession/resend_code_to_newuser";
        $http({
            method: "POST",
            url: url,
            data: $data,
            dataType: 'json',
        }).success(function mySucces(res) {

            if (res.data == true) {
                /*bootbox.alert({
                    size: "small",
                    message: 'Code has been resent to your mobile number',
                });*/
                alert('Code has been resent to your mobile number');

            } else {

                /*
                bootbox.alert({
                    size: "small",
                    message: 'User not created',
                });*/
                alert('User not created');

            }

        }, function myError(response) {
            /*bootbox.alert({
                size: "small",
                message: 'Error',
            });*/
            alert('Error');
        });


    }

    $scope.otpverify_verifyfalse = function () {

        c_id = Scopes.get('LoginController').c_id;

        if ($scope.otp_code_verifyfalse != null) {

            var otpData = {'otp_code': $scope.otp_code_verifyfalse, 'c_id': c_id};

            $("#verify_btn_verifyfalse").html('<i class="fa fa-spin fa-refresh"></i> Processing');
            document.getElementById("verify_btn_verifyfalse").disabled = true;
            $http({
                method: "POST",
                url: "<?php echo base_url();?>Franchiseloginsession/check_otpcode",
                data: otpData,
                dataType: 'json',
            }).success(function mySucces(res) {


                if (res.data != null) {
                    if (res.data == true) {

                        c_id = Scopes.get('LoginController').c_id;
                        var active_data = {'c_id': c_id};

                        url = "<?php echo base_url();?>Franchiseloginsession/activate_new_user";
                        $http({
                            method: "POST",
                            url: url,
                            data: active_data,
                            dataType: 'json',
                        }).success(function mySucces(res) {

							$scope.mobileOtp_verifyfalse.otp_code_verifyfalse.$setValidity("otpsameValidator", true);
							jQuery('#mobileOtp_verifyfalse')[0].reset();
							$(".mobile_verification_div").css({"display": "none"});
							//////////////////////////
							cur_page = '<?php echo $this->session->userdata("cur_page");?>';
							base_url_site = "<?php echo base_url();?>";

							alert("Mobile no " + res.mobileno_in_session + " is verified, You are now a registered user.");
							if (cur_page) {
								window.location = cur_page;
							} else {
								window.location = base_url_site;
							}

                        }, function myError(response) {
                            /*bootbox.alert({
                                size: "small",
                                message: 'Error',
                            });*/
                            alert('Error');
                        });

                    }
                    if (res.data == false) {
                        $scope.mobileOtp_verifyfalse.otp_code_verifyfalse.$setValidity("otpsameValidator", false);
                    }
                    $("#verify_btn_verifyfalse").html('<i class="fa fa-check"></i> Verify');
                    document.getElementById("verify_btn_verifyfalse").disabled = false;
                } else {
                    $scope.mobileOtp_verifyfalse.otp_code_verifyfalse.$setValidity("otpsameValidator", true);
                    $("#verify_btn_verifyfalse").html('<i class="fa fa-check"></i> Verify');
                    document.getElementById("verify_btn_verifyfalse").disabled = false;
                }

            }, function myError(response) {
                /*bootbox.alert({
                    size: "small",
                    message: 'Error',
                });*/
                alert('Error');
                $("#verify_btn_verifyfalse").html('<i class="fa fa-check"></i> Verify');
                document.getElementById("verify_btn_verifyfalse").disabled = false;
            });

            $scope.$broadcast('show-errors-check-validity');

        }//if condition
    };
}

function LoginController($scope,Scopes,$http)
{
		$('#mydiv').hide();
		Scopes.store('LoginController', $scope);
		$scope.new_user_reg = function(){
			$('#login_form').hide();
			$('#createuser').show();
			$('#forgot_form').hide();
			$('#mobile_verification').hide();
			
		}
		$scope.flag=0;
		$scope.login_auth = function(type='') {
		$scope.$broadcast('show-errors-check-validity');	
		username=$scope.username;
		password=$scope.password;
		$scope.loginForm.username.$setValidity('userexistValidator', true);
		$scope.loginForm.password.$setValidity('passwordwrongValidator', true);		
		$scope.loginForm.username.$setValidity('userUnBlockedValidator', true);		
                
                if(type=='re_activate'){
                    $("#reactivate_btn").html('<i class="fa fa-spin fa-refresh"></i> Processing');
                    document.getElementById("reactivate_btn").disabled = true;
                    document.getElementById('reactivate_btn').style.pointerEvents = 'none';
                }else {
                    $("#login_btn").html('<i class="fa fa-spin fa-refresh"></i> Processing');
                    document.getElementById("login_btn").disabled = true;
                    document.getElementById('login_btn').style.pointerEvents = 'none';
                    //document.getElementById("create_new_btn").disabled = true;
                    //document.getElementById('create_new_btn').style.pointerEvents = 'none';
                    
                }
                
		if(username != null){
			$scope.check_user();
			if(type=='re_activate'){
				$scope.loginForm.username.$setValidity('userUnBlockedValidator',true);
			}else{
				
				
				if($scope.loginForm.$valid){
                                    
				}else{
                                    /* If validation fails */
                                    if(type=='re_activate'){
                                        $("#reactivate_btn").html('<i class="fa fa-lock"></i>  Re-Activate');
                                        document.getElementById("reactivate_btn").disabled = false;
                                        document.getElementById('reactivate_btn').style.pointerEvents = 'auto';
                                    }else {
                                        $("#login_btn").html('<i class="fa fa-lock"></i> Login');
                                        document.getElementById("login_btn").disabled = false;
                                        document.getElementById('login_btn').style.pointerEvents = 'auto';
                                       // document.getElementById("create_new_btn").disabled = false;
                                       // document.getElementById('create_new_btn').style.pointerEvents = 'auto';
                                    }
                                    return false;
				}	
			}
		}
		
               
                
		if(password != null && username != null){
				var temppwd="";
				for(i=0;i<password.length;i++){
				temppwd+=String.fromCharCode(password.charCodeAt(i)-7);
				}
				var FormData = {'password' : temppwd,'username' :username,'type':type}; 	

				 $http({
					method : "POST",
					url : "<?php echo base_url();?>Franchiseloginsession/customer_login",
					data:FormData,
					dataType: 'json',
                                        async:false,
					}).success(function mySucces(resul) {

						if(resul.valid!=null){
							if(resul.valid==true){
								 
								$scope.loginForm.password.$setValidity('passwordwrongValidator', true);
								$scope.loginForm.username.$setValidity('userUnBlockedValidator',true);
								
								if($scope.loginForm.$valid){
									
									
									cur_page_js="<?php echo $this->session->userdata('cur_page');?>";
									//alert(cur_page_js)
									if(cur_page_js){
										window.location=cur_page_js;
									}
									else{
										window.location="<?php echo base_url();?>Account/franchise_account";
									}
									
									
								}
								if(type=="re_activate"){
								//	alert('Your account has been activated Successfully');
								}
							}
							if(resul.valid=="verifyfalse"){
								 $scope.loginForm.password.$setValidity('passwordwrongValidator', true);
								 $scope.loginForm.username.$setValidity('userUnBlockedValidator',true);
								 $scope.c_id=resul.customer_id;
								 $scope.mobile=resul.mobile;
								 $scope.logintype="verifyfalse";
								 $("#login_form").hide();
								 $("#verifyfalse_mobile").html(resul.mobile);
								 $("#mobile_verification").show();//_verifyfalse

								 $("#show_change_mobile").hide();
									
							 }
							 if(resul.valid=='approve_false'){
									$scope.c_id=resul.customer_id;
									$scope.logintype="approve_false";
									alert('Your account is not approved. Please contact admin');
									location.reload();
								
								}

							 if(resul.valid==false){
								 $scope.loginForm.password.$setValidity('passwordwrongValidator', false);
								 $scope.loginForm.username.$setValidity('userUnBlockedValidator',true);
							 }
						}else{
							
							$scope.loginForm.password.$setValidity('passwordwrongValidator', true);
							$scope.loginForm.username.$setValidity('userUnBlockedValidator',true);
						}

                     if(type=='re_activate'){
                         $("#reactivate_btn").html('<i class="fa fa-spin fa-refresh"></i> Processing');
                         document.getElementById("reactivate_btn").disabled = true;
                         document.getElementById('reactivate_btn').style.pointerEvents = 'none';
                         
                     }else {
                         $("#login_btn").html('<i class="fa fa-lock"></i> Login');
                         document.getElementById("login_btn").disabled = false;
                         if($scope.loginForm.$valid){
                            document.getElementById('login_btn').style.pointerEvents = 'none';
                           // document.getElementById("create_new_btn").disabled = true;
                           // document.getElementById('create_new_btn').style.pointerEvents = 'none';
                        }else{
                            /* error is there */
                            document.getElementById('login_btn').style.pointerEvents = 'auto';
                           // document.getElementById("create_new_btn").disabled = false;
                           // document.getElementById('create_new_btn').style.pointerEvents = 'auto';
                        }
                     }

					}, function myError(response) {
						$scope.loginForm.password.$setValidity('passwordwrongValidator', false);
					});
		}

		
	};
	$scope.forget_password = function(){
		
		username=$scope.username;

		if(username != null)
		{
			$scope.$broadcast('show-errors-check-validity');
	
			var FormData = {'val' : username}; 
				
				$http({
					method : "POST",
					url : "<?php echo base_url();?>Franchiseloginsession/customer_check_user",
					data:FormData,
					dataType: 'json',
					async:false
					}).success(function mySucces(res) {

						if(res.valid!=null)
						{
							if(res.valid==false){
								$scope.loginForm.username.$setValidity('userexistValidator',true );
								
								$('#mydiv').show();
                                                                //return false;
								$http({
										method : "POST",
										url : "<?php echo base_url();?>Franchiseloginsession/resend_code_to_setpassword",
										data:FormData,
										dataType: 'json',
                                                                                async:false

										}).success(function mySucces(res) {
											if(res.valid!=null)
											{
												if(res.valid==true)
												{
													$('#mydiv').hide();										
													$scope.customer_id=res.c_id;
													$scope.field=res.field;
													$scope.value_f=res.value;
													
					$('#login_form').hide();
					$('#forgot_form').show();
					$('#text_title').html("<span> Verification code has been sent to your "+$scope.field+"</span><span><b> "+$scope.value_f+"</b></span>");
					
				//$scope.$broadcast('show-errors-reset');
				//$scope = {username: username, password: ''};
				
												}else{
										

													alert("It seems your account is not activated to set the password");
													$('#mydiv').hide();
													$scope.$broadcast('show-errors-reset');
													$scope.password = '';
							
												}
											}
											
										}, function myError(response) {
											/*
											bootbox.alert({ 
											  size: "small",
											  message: 'forget password error',
											});	*/
											alert('Forget password error');
										});
							}
							else{
								$scope.loginForm.username.$setValidity('userexistValidator',false );
							}
						}
						else{
							$scope.loginForm.username.$setValidity('userexistValidator', false);		
						}
						
					}, function myError(response) {
						
						$scope.loginForm.username.$setValidity('userexistValidator', false);
						
					});
				
		}else
		{
			$scope.$broadcast('show-errors-check-validity');
		}


	};
	$scope.check_user = function(){
			$scope.loginForm.username.$setValidity('userUnBlockedValidator',true);
			var FormData = {'val' : username}; 
				
				$http({
					method : "POST",
					url : "<?php echo base_url();?>Franchiseloginsession/customer_check_user",
					data:FormData,
					dataType: 'json',
                                        async:false
					}).success(function mySucces(res) {	
						if(res.valid!=null){
							if(res.valid==true){
							
								$scope.loginForm.username.$setValidity('userexistValidator',false );//show the error
							}
							if(res.valid==false){
								$scope.loginForm.username.$setValidity('userexistValidator', true);	//no error	
							}
							if(res.is_user_blocked==1 && res.active==0 && res.deactivated_by=="admin"){
								
								$scope.loginForm.username.$setValidity('userBlockedValidator',false);
							}
							if(res.is_user_blocked==0 && res.active==0 && res.deactivated_by=="" && res.mobile_verification==1){
								$("#login_btn").hide();
								$("#reactivate_btn").show();
								
								$scope.loginForm.username.$setValidity('userUnBlockedValidator',false);
								$scope.flag=1;
							}else{
								$scope.loginForm.username.$setValidity('userUnBlockedValidator',true);
							}
						}else{
							$scope.loginForm.username.$setValidity('userexistValidator', false);
							$scope.loginForm.username.$setValidity('userUnBlockedValidator', true);									
						}
						
						
					}, function myError(response) {
						
						$scope.loginForm.username.$setValidity('userexistValidator', false);
						$scope.loginForm.username.$setValidity('userUnBlockedValidator', true);		
						
					});
		
	};
	
	
	
}
function fogotPasswController($scope,Scopes,$http){
	$("#mydiv_re").hide();
	Scopes.store('fogotPasswController', $scope);

	$scope.set_password = function(){
	
			$scope.$broadcast('show-errors-check-validity');	
			customer_id=Scopes.get('LoginController').customer_id;
			
				if($scope.forgotForm.$valid){
					password=$scope.f_password;
					var temppwd="";
					for(i=0;i<password.length;i++){
						temppwd+=String.fromCharCode(password.charCodeAt(i)-7);
					}
			
                    var sec_data = {'otp_code' : $scope.f_otp,'c_id' : customer_id,password : temppwd};
                    url="<?php echo base_url();?>Franchiseloginsession/reset_password";

                    $("#reset_password").html('<i class="fa fa-spin fa-refresh"></i> Processing');
                    document.getElementById("reset_password").disabled = true;

				$http({
					method : "POST",
					url : url,
					data: sec_data,
					dataType: 'json',
                                        async:false
					}).success(function mySucces(res) {
				
						if(res.data==true){
							$scope.c_id=res.c_id;
							$("#createuser").hide();
							$("#forgot_form").hide();
							
				f_value=Scopes.get('LoginController').value_f;
				var FormData = {'password' : temppwd,'username' :f_value,'type':''};
				 $http({
					method : "POST",
					url : "<?php echo base_url();?>Franchiseloginsession/customer_login",
					data:FormData,
					dataType: 'json',
                                        async:false
					}).success(function mySucces(resul) {

						

						if(resul.valid!=null)
						{

							 if(resul.valid==true)
							 {
								
																		
										<?php
									if($this->session->userdata("cur_page")){
										$cur_page=$this->session->userdata("cur_page");
										?>
										window.location="<?php echo $cur_page;?>";
										<?php
									}
									else{
										?>		
									
										window.location="<?php echo base_url();?>Account/franchise_account";
									<?php } ?>
									
							
							 }else{

								if(resul.valid=='approve_false'){
									alert('Your account is not approved. Please contact admin');
									location.reload();
								}else{
									/*bootbox.alert({
									  size: "small",
									  message: 'Error',
									});	*/
									alert('Error');
								}
								 	
							 }
							 
						}
						
					}, function myError(response) {
						
					});
							
						
						}else{
								/*bootbox.alert({
								  size: "small",
								  message: 'password is not reset Try again',
								});	*/
								alert('Password is not reset Try again');
						}


					},function myError(response) {

                        /*bootbox.alert({
                            size: "small",
                            message: 'error',
                        });*/
                        alert('Error');
					});	
					
				}
				
				
			  if(Scopes.get('LoginController').customer_id != null)
			  {		
				customer_id=Scopes.get('LoginController').customer_id;		  
				field=Scopes.get('LoginController').field;		
				f_value=Scopes.get('LoginController').value_f;

				//alert(Scopes.get('LoginController').customer_id;);
				//alert(Scopes.get('LoginController').c_id);

		 if($scope.f_otp!= null)
		 {	 	
			var otpData = {'otp_code' : $scope.f_otp,'c_id' : customer_id}; 	
				$http({
					method : "POST",
					url : "<?php echo base_url();?>Franchiseloginsession/check_otpcode",
					data:otpData,
					dataType: 'json',
					}).success(function mySucces(res) {
						
						if(res.data!=null)
						{
							if(res.data==true)
							{
								$scope.forgotForm.f_otp.$setValidity("otpsameValidator", true);
							}
							if(res.data==false){
								$scope.forgotForm.f_otp.$setValidity("otpsameValidator", false);
							}
						}else{
							$scope.forgotForm.f_otp.$setValidity("otpsameValidator", true);
						}
						
					}, function myError(response) {

						/*bootbox.alert({
						  size: "small",
						  message: 'error',
						});	*/
						alert('Error');
					});
					
				$scope.$broadcast('show-errors-check-validity');

				}//if condition f_otp

			  }

        $("#reset_password").html('Submit');
        document.getElementById("reset_password").disabled = false;

        //alert('reset password');

    };
	

	$scope.resend_code_fp = function() {
		
		customer_id=Scopes.get('LoginController').customer_id;
		
		field=Scopes.get('LoginController').field;		
		f_value=Scopes.get('LoginController').value_f;
		$("#mydiv_re").show();
		if(customer_id != null)
		{
			var data = {'c_id' : customer_id, val : f_value}; 
			
			$("#createuser").hide();
			$("#mobile_verification").hide();
			url="<?php echo base_url();?>Franchiseloginsession/resend_code_to_newpassword";
			$http({
			method : "POST",
			url : url,
			data: data,
			dataType: 'json',
                        async:false
			}).success(function mySucces(res) {
		
				if(res.valid==true)
				{
					/*bootbox.alert({
					  size: "small",
					  message: 'code has been resent',
					});	*/
					alert('Code has been resent');
					$("#mydiv_re").hide();
				}else{
					/*bootbox.alert({
					  size: "small",
					  message: 'code has not been resent',
					});	*/
					alert('Code has not been resent');
				}
				
			},function myError(response) {
				/*bootbox.alert({
					  size: "small",
					  message: 'error',
					});
				 */
				 alert('Error');
			});	
			
			
			
		}	
		
	};
	
	$scope.change_user_f= function() {
		//location.reload();
		$("#forgot_form").hide();
		$("#login_form").show();
	};
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
        return false;
    }
    return true;
}
$('#mobile_number').on('keydown', function(e){
    evt = (e) ? e : window.e;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
        return false;
    }
    return true;
});
</script>
<!-- page wapper-->

<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb 
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php //echo base_url()?>search" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Login</span>
        </div>-->
        <!-- ./breadcrumb -->
        <!-- page heading-->
        
        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
			<div class="col-md-4 col-md-offset-4">
			<div class="well">
			<div class="row">
                <div class="col-md-12" id="createuser" style="display:none;">
                    <div class="box-authentication" ng-controller="NewUserController" style="border:none;">
                        <h3 class="text-muted">Franchise - Create Account</h3>
					<form name="userForm" novalidate>
						<div class="form-group" show-errors>
							
							<input name="name" ng-model="formData.name" required placeholder="Name" type="text" class="form-control">
							<p class="help-block" ng-if="userForm.name.$error.required">The user's name is required</p>
						</div>
						
						<div class="form-group" show-errors>
						
                          <input type="email" class="form-control" name="email" ng-model="formData.email" required placeholder="Email" email-exist-c="formData.email" id="email" />
				  
							<p class="help-block" ng-if="userForm.email.$error.emailexistValidator">This Email is  registered already.</p>
				  
							<p class="help-block" ng-if="userForm.email.$error.required">The user's email is required</p>
							<p class="help-block" ng-if="userForm.email.$error.email">The email address is invalid</p>				
	 
						</div>
						
						<div class="form-group" show-errors>
							
							<input id="mobile_number" type="number"  ng-minlength="10" 	
                   ng-maxlength="10" min="0" onpaste="return false;" name="mobile" class="form-control" ng-model="formData.mobile" required allow-only-numbers mobile-exist-c="formData.mobile" placeholder="Mobile No" maxlength="10" onkeypress="return isNumber(event)">	
				   	
				   <p class="help-block" ng-show="(userForm.mobile.$error.required || userForm.mobile.$error.number) || (userForm.mobile.$invalid && !userForm.mobile.$pristine)">Valid mobile number is required
					</p>
				<p class="help-block" ng-show="userForm.mobile.$error.mobileexistValidator">This mobile is registered already.</p>
				
					<p class="help-block" ng-show="((userForm.mobile.$error.minlength ||
								   userForm.mobile.$error.maxlength) && 
								   userForm.mobile.$dirty) ">
								   mobile number should be 10 digits
					</p>
							
						</div>

						<!---password generation --->
<!-- 						
		<div class="form-group" show-errors>
		
		<input type="password" name="password" ng-model="formData.password" required class="form-control" ng-minlength="6" ng-maxlength="10"  placeholder="Password" ng-pattern="regex" >
		<p class="help-block" ng-if="userForm.password.$error.required">The password is required</p>
								
		<p ng-show="userForm.password.$error.minlength" class="help-block">
		Passwords must be between 6 and 10 characters.</p>


		</div>
		
		<div class="form-group" show-errors ng-class="{'has-error':formData.password_c.$invalid && !formData.password_c.$pristine}">
		
		<input type="password" id="password_c" name="password_c" ng-model="formData.password_c" valid-password-c="formData.password" required class="form-control" placeholder="Confirm Password"/>

		<p ng-if="userForm.password_c.$error.noMatch" class="help-block">Passwords do not match.</p>
		<p ng-if="userForm.password_c.$error.required" class="help-block">Confirm Password</p>
		</div>
-->

		<p class="lead" style="font-size:1em;">We will send you a text to verify.</p>

	  <!--- address of parter--->
	  
	  
	  <div class="form-group" show-errors id="pin_code_check">
								
								<input type="text" ng-minlength="6"
								   ng-maxlength="6" name="pin" class="form-control" ng-model="formData.pin" required allow-only-numbers  placeholder='PIN/ZIP Code'  onkeypress="return isNumber(event)"  ng-paste="$event.preventDefault()" ng-pattern="/^[0-9]*$/" oninput="if(value.length>6)value=value.slice(0,6)" ng-keyup="get_city_state_details_by_pincodeFun()">
								   
								   <p class="help-block" ng-show="userForm.pin.$error.required || userForm.pin.$error.number">Valid pin code is required
									</p>
								
									<p class="help-block" ng-show="((userForm.pin.$error.minlength ||
												   userForm.pin.$error.maxlength) && 
												   userForm.pin.$dirty) ">
												   Pincode should be 6 digits
									</p>
                                    <span class="delivery_address_pin_error" style="display:none;color:#a94442">
                                        Please enter valid pincode
                                    </span>
											
							</div>
							
							
							<div class="form-group"  show-errors>
								
								<input  name="city" ng-model="formData.city" required placeholder="City" type="text" class="form-control" readonly>
								<p class="help-block" ng-if="userForm.city.$error.required">The City is required</p>
							</div>
						
							<div class="form-group"  show-errors>
							<input  name="state" ng-model="formData.state" required placeholder="State/Province" type="text" class="form-control" readonly>
								
								
								<p class="help-block" ng-if="userForm.state.$error.required">The State is required</p>
							</div>
							
							
							
							<div class="form-group"  show-errors>
								<input  name="country" ng-model="formData.country" required placeholder="Country" type="text" class="form-control" readonly>
								<p class="help-block" ng-if="userForm.country.$error.required">The Country is required</p>
							</div>
						
<!--- address of parter--->

	  

		<div class="form-group">
		<button class="btn  btn-sm btn-block" ng-click="save();$event.stopPropagation();" ng-disabled="buttonClicked"  style="background-color:#f5540e;color:#fff;" id="reg_btn"><i class="fa fa-user"></i> Create</button>
							
		<button class="btn btn-sm btn-block" ng-click="reset()"   style="background-color:#1e1c10;color:#fff;">Reset</button>
		</div>
		<div class="form-group">
			<a href='#' ng-click="go_back()"><span> Already have an account? Sign in</span></a>
		</div>
	
					</form>
					
                    </div>
                </div>
           
		<div class="col-md-12" id="mobile_verification" style="display:none;">
                    <div class="box-authentication" ng-controller="MobileOtpController" style="border:none;">
					<h4 class="text-muted">Mobile Number Verification</h4>
						<form name="mobileOtp" novalidate id="mobileOtp">

							<label id="result"></label>
						
							
					<div class="form-group" show-Otperrors>
							
							<input id="otp_code" type="text" class="form-control" placeholder="Enter Verification Code" name="otp_code" required ng-model="otp_code" otp-check-c allow-only-numbers ng-minlength="4" ng-maxlength="6" min="0">
							
							<p ng-if="mobileOtp.otp_code.$error.otpsameValidator" class="help-block">Verification code is Wrong</p>
							
							<p ng-if="mobileOtp.otp_code.$error.required" class="help-block">Verification code is required</p>

							<p class="help-block" ng-if="((mobileOtp.otp_code.$error.minlength ||
								   mobileOtp.otp_code.$error.maxlength) && 
								   mobileOtp.otp_code.$dirty) ">
								   Verification code is not to be more than 6
					</p>
					</div>	
							<button class="btn btn-success btn-sm btn-block" ng-click="otpverify()" id="verify_btn"><i class="fa fa-check"></i> Verify</button>
							
							<button ng-if="show_change_number==1" class="btn btn-info btn-sm btn-block" ng-click="change_mobile()" id="show_change_mobile">Change Mobile No</button>
							
							<button class="btn btn-warning btn-sm btn-block" ng-click="resend_code()">Resend Code</button>
						</form>
					
                    </div>
            </div>
			
		   <div class="col-md-12" id="login_form">
                    
				<div class="box-authentication" ng-controller="LoginController" style="border:none;">
					<h3 class="text-muted">Franchise Login</h3>
					
				<form name="loginForm" novalidate id="loginForm" class="form-horizontal">
					
					<div class="form-group" show-errors>
					<input name="username" type="text" class="form-control" placeholder="Email / Mobile" required ng-model="username" user-exist-c="username">
					<p ng-if="loginForm.username.$error.required" class="help-block">Please enter Email ID or Mobile No</p>
					
					<p class="help-block" ng-if="loginForm.username.$error.userexistValidator">Customer is Not Yet Registered</p>
					<p class="help-block" ng-if="loginForm.username.$error.userBlockedValidator">Your ID is blocked.Please Contact admin.</p>
					<p ng-if="loginForm.username.$error.userUnBlockedValidator">Your ID is blocked. Click Re-activate button with old password to activate your account.</p>
					</div>
					
					
					<div class="form-group" show-errors>
					<input name="password" id="password" type="password" class="form-control" placeholder="Enter Password" required ng-model="password" password-check-c="password">
					
					
					<p ng-if="loginForm.password.$error.required" class="help-block">Please enter password</p>
					
					<p ng-if="loginForm.password.$error.passwordwrongValidator" class="help-block">Please Check the password</p>
					
					</div>
					
					
					
					<div class="form-group">
					
					<button class="btn btn-sm btn-block" ng-click="login_auth()" id="login_btn" style="background-color:#f5540e;color:#fff;"><i class="fa fa-lock"></i> LOGIN</button>
					<button class="btn btn-info btn-xs btn-block" ng-click="login_auth('re_activate')" id="reactivate_btn" style="display:none;"><i class="fa fa-lock"></i>  Re-Activate</button>
					</div>
					<div class="form-group">
					<button class="btn btn-sm btn-block"  ng-click="new_user_reg()"  style="background-color:#2d0000;color:#fff;"> Create New Account</button>
					<p class="forgot-pass text-right"><a href="" ng-click="forget_password()" style="color:#1c79e7;font-size:0.9em;">Forgot Password?</a></p>
					</div>
					
					<div id="mydiv">
						<img src="<?php echo base_url(); ?>assets/pictures/images/loading.gif" style="margin-top: -16px;" class="ajax-loader"/> 
					</div>
					
					
				</form>
				</div>
					
            </div>
			
			<div class="col-md-12" id="forgot_form"  style="display:none;">
                    <div class="box-authentication" ng-controller="fogotPasswController" style="border:none;">
                        <h4 class="text-muted">Verify and Set Password</h4>
					<form name="forgotForm" novalidate>
                        
					<div class="form-group">
					<label for="verification_code">
					<span id="text_title"></span>
					
					<a href=''><span ng-click='change_user_f()' style='color:#382e03;'><u> Change</u></span></a>
					</label>
					</div>
					
					<div class="form-group">
					<label for="verification_code"><a href=""><span ng-click="resend_code_fp()" style="color:#382e03;"><u>Resend Code</u></span></a></label>
					</div> 
					<div id="mydiv_re">
						<img src="<?php echo base_url(); ?>assets/pictures/images/loading.gif" style="margin-top: -16px;" class="ajax-loader_re"/>
					</div>
					
					<div class="form-group" show-errors>	
					
					<input id="f_otp" type="text" class="form-control" placeholder="Enter Verification Code" name="f_otp" required ng-model="f_otp" otp-check-c allow-only-numbers ng-minlength="4" ng-maxlength="6" min="0">
							
							<p ng-if="forgotForm.f_otp.$error.otpsameValidator" class="help-block">Verification code is Wrong</p>
							
							<p ng-if="forgotForm.f_otp.$error.required" class="help-block">Verification code is required</p>

							<p class="help-block" ng-if="((forgotForm.f_otp.$error.minlength ||
								   forgotForm.f_otp.$error.maxlength) && 
								   forgotForm.f_otp.$dirty) ">
								   Verification code is not to be more than 6
							</p>

					</div>
					
						
						<div class="form-group" show-errors>
                        <input type="password" name="f_password" ng-model="f_password" required class="form-control" ng-minlength="6" ng-maxlength="10" placeholder="Set Password">
						<p class="help-block" ng-if="forgotForm.f_password.$error.required">The password is required</p>
												
		<p ng-show="forgotForm.f_password.$error.minlength" class="help-block">
          Passwords must be between 6 and 10 characters.</p>
		  
		  
						</div>
						
		<div class="form-group" show-errors ng-class="{'has-error':f_password_c.$invalid && !f_password_c.$pristine}">
        <input type="password" id="f_password_c" name="f_password_c" ng-model="f_password_c" valid-password-c="f_password" required class="form-control" placeholder="Confirm Password"/>

        <p ng-if="forgotForm.f_password_c.$error.noMatch" class="help-block">Passwords do not match.</p>
        <p ng-if="forgotForm.f_password_c.$error.required" class="help-block">Confirm Password</p>
		</div>
		
		<button class="btn btn-warning btn-sm btn-block" id="reset_password" style="background-color: #f5540e;"  ng-click="set_password()"><i class="fa fa-user"></i> Submit</button>
						
	
					</form>
					
                    </div>
               </div>
			
			
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->
