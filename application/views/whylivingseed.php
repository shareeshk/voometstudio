  <style>
  .font{
	  font-size:1.2em;
  }
  
  .gap50{
	  margin-top:50px;
  }
  .gap25{
	  margin-top:25px;
  }
  
  .bglabelgreen{	  
	  background-color:#548235;
	  box-shadow: 2px 2px 2px #888888;
	  color:#fff;	 
  }
  
  .bglabelbrown{	  
	  background-color:#663300;
	  box-shadow: 2px 2px 2px #888888;
	  color:#fff;	 
  }
  
   .bglabellgbrown{	  
	  background-color:#c55a11;
	  box-shadow: 2px 2px 2px #888888;
	  color:#fff;	 
  }
  
  .squarelist {list-style-type: square;}
  .flex-row .thumbnail,
.flex-row .caption {
  flex:1 0 auto;
  flex-direction:column;
}
.flex-row .caption {
height:25vh;	
}
.flex-text {
  flex-grow:1
}
.flex-row img {
  height:auto;
  width:100%
}
@media only screen and (min-width : 481px) {
    .flex-row {
        display: flex;
        flex-wrap: wrap;
    }
    .flex-row > [class*='col-'] {
        display: flex;
        flex-direction: column;
    }
    .flex-row.row:after, 
    .flex-row.row:before {
        display: flex;
    }
}
  </style>
  

<div class="block-banner8 container" id="contributingfactors">
<div class="section8">
			<h3 class="section-title">Our Values</h3>
		</div>
    <div class="row gap25">
		<div class="col-md-3 col-md-offset-1">
      <div class = "thumbnail">
         <img src = "<?php echo base_url()?>assets/images/values.jpg" alt = "Generic placeholder thumbnail">
      </div>
   </div>
   <div class = "col-md-7">
         <p class="lead font text-justify">We believe that healthy body makes the best fashion statement and a true beauty. We believe that when we are in true harmony with nature, our skin brightens up, our eyes illuminate, and our vitality is restored. We see beauty in the flowers and the plants and everything that is born from nature. We see it in you! We are committed to promoting your holistic wellbeing and beauty, and we strive for each of us to be empowered to cultivate beauty which is healthy and safe, for ourselves, for our children and the future of our planet.<br><br>

We’re dedicated to making high quality, 100% natural products that overflow with botanical ingredients, making you beautiful from inside out.
</p>
   </div>
    </div>
	<div class="row gap50"></div>
	 
	<div class="section8">
			<h3 class="section-title">Pure Ingredients</h3>
		</div>
  
		 <div class="row gap25">
   <div class = "col-md-7">
         <p class="lead font text-justify">Your skin has the ability to absorb certain compounds; however, a protective barrier is able to keep dangerous compounds, to some degree, out of the body. Even then, research still concludes that personal care products, when applied topically, can damage bodily systems and lead to untold health consequences. One of the key rules for makeup, and any beauty product is basic and straightforward: if you can’t eat it, don’t wear it.<br><br>

We carefully craft the highest quality, cleanest lip balms, body butters, hair oils and all our personal care products using only the purest natural ingredients nature has to offer. All our oils are cold-pressed without the use of any chemical solvents. All our butters are pure, unrefined and unprocessed. All our herbs are traditionally prepared to preserve their full effects.<br><br>

Our all products are lovingly produced in small batches making sure they are imbued in that lovingly-handcrafted energy. 

</p>
   </div>
   <div class="col-md-3 col-md-offset-1">
      <div class = "thumbnail">
         <img src = "<?php echo base_url()?>assets/images/ingredients.jpg" alt = "Generic placeholder thumbnail">
      </div>
   </div>
    </div>
	
	
	
	
	<div class="row gap50"></div>
	
	<div class="section8">
			<h3 class="section-title">Absolutely No Chemicals</h3>
		</div>
    <div class="row gap25">
		<div class="col-md-3 col-md-offset-1">
      <div class = "thumbnail">
         <img src = "<?php echo base_url()?>assets/images/nochemicals.jpg" alt = "Generic placeholder thumbnail">
      </div>
   </div>
   <div class = "col-md-7">
         <p class="lead font text-justify">We never use any man-made chemicals, colours, fragrances, petroleum by-products, or man-made preservatives. Your skin eats (absorbs) what’s on it. If you wouldn’t eat it, you wouldn’t want to put it on your skin. Our products are made only and only of "living’ substances such as plants or their extracts, are balanced by Nature and contain the vibratory energy that constitutes life.<br><br>

Although they may be effective for improving appearance, albeit temporarily, the chemicals in conventional body care products are often very harsh on skin and may promote irritation or allergic reactions in sensitive individuals. More importantly, many of the chemicals are downright poisonous to the endocrine system. Parabens and phthalates are two common examples of substances that are used extensively in cosmetics.<br><br>

</p>
   </div>
    </div>
	<div class="row gap50"></div>
	 
	<div class="section8">
			<h3 class="section-title">Vedic Processes</h3>
		</div>
  
		 <div class="row gap25">
   <div class = "col-md-7">
         <p class="lead font text-justify">We do not use any harsh processes ensuring that we retain the goodness of the ingredients and pass it onto you in it’s most potent form possible. <br><br>

There are two levels of healing in Ayurveda: the Physical and the Mental. The basic means of healing on the physical level is through herbs. The basic means of healing on the mental or psychological level is through “mantras”.<br><br>

Doing specific mantras while preparing herbal preparations gives them a greatly increased potency. Just as food should be prepared with love to be really nourishing, so herbs should be prepared with a mantra. All healing preparations should flow from the love and energy of a mantra. 
<br><br>
</p>


   </div>
   <div class="col-md-3 col-md-offset-1">
      <div class = "thumbnail">
         <img src = "<?php echo base_url()?>assets/images/vedicprocesses.jpg" alt = "Generic placeholder thumbnail">
      </div>
   </div>
    </div>
	
	
	
	<div class="row gap50"></div>
	<div class="section8">
			<h3 class="section-title">Healing From Within
</h3>
		</div>
    <div class="row gap25">
		<div class="col-md-3 col-md-offset-1">
      <div class = "thumbnail">
         <img src = "<?php echo base_url()?>assets/images/healing.jpg" alt = "Generic placeholder thumbnail">
      </div>
   </div>
   <div class = "col-md-7">
         <p class="lead font text-justify">Our formulations are prepared to Heal.<br><br>Rates of illness and disease continue to rise. There are increasing studies showing the connection between illness and chemicals. We need to eliminate or reduce exposure to synthetics. Synthetics are simply not necessary in personal care and in many cases synthetics are downright scary. These ingredients are often fast acting and invasive. They are causing harm human eye cannot see. Often this exposure rears its ugly head as skin irritation, chronic headaches, cancers, breathing disabilities, hormone disruption, and so on. The range of symptoms is too vast to list everything, but you can see how it covers minor symptoms right up to life threatening consequences.
<br><br>

We use natural, minimally processed ingredients which work with the sophisticated natural systems of our body allowing the body to care for and mend itself. 

</p>
   </div>
    </div>
	<div class="row gap50"></div>
	 
	<div class="section8">
			<h3 class="section-title">Satisfaction Guaranteed</h3>
		</div>
  
		 <div class="row gap25">
   <div class = "col-md-7">
         <p class="lead font text-justify">We source the finest natural ingredients nature has to offer. Our formulas are then hand crafted in small batches to be the freshest, most effective, all natural personal care products possible. As a result we are proud to offer you a 100% satisfaction guarantee. <br><br>

We are confident you are going to love LivingSeed personal care products. But if you are unhappy with your purchase for any reason, we will promptly refund your money so that you maintain your level of pleasantness with us. 

</p>
   </div>
   <div class="col-md-3 col-md-offset-1">
      <div class = "thumbnail">
         <img src = "<?php echo base_url()?>assets/images/satgua.jpg" alt = "Generic placeholder thumbnail">
      </div>
   </div>
    </div>
	
	
	
	
	
	
</div>
