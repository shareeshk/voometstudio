<style type="text/css">

.margin-b-20{
    margin-top:20px ;
    margin-bottom:20px ;
    text-transform: capitalize;
}
.highlight{
    /* color:red; */
}
.disc-style{
    list-style: disc;
    margin-left: 20px;
    margin-bottom: 10px;
}
</style>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo base_url();?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Terms of Use</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">Infomations</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <li class="active"><span></span><a href="<?php echo base_url();?>termsandconditions">Terms of Use</a></li>                                 	
									<li><span></span><a href="<?php echo base_url();?>policy">Privacy Policy</a></li>
                                    <li><span></span><a href="<?php echo base_url();?>business_policy" >Business Policies</a></li>

									<!-- <li><span></span><a href="<?php echo base_url();?>payments">Payments</a></li>
									
									<li><span></span><a href="<?php echo base_url();?>delivery">Delivery</a></li>
									<li><span></span><a href="<?php echo base_url();?>warranty">Warranty</a></li> -->
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
                
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading text-center">
                    <span class="page-heading-title2">Terms of Use</span>
                </h2>
                <!-- Content page -->
                <div class="content-text clearfix">    



  <p><span class="highlight">Welcome to Voomet Studio! These terms govern your use of our website</span>. By accessing or using our site, you agree to abide by these terms. If you disagree with any part of these terms, please refrain from using our website.</p>
<ul class="disc-style">
<li><strong>Acceptance of Terms</strong> By accessing this website, you agree to be bound by these Terms of Use, all applicable laws, and regulations. If you do not agree with any of these terms, you are prohibited from using or accessing this site.</li>
<li><strong>Use License</strong> Permission is granted to temporarily download one copy of the materials (information or software) on Voomet Studio's website for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title.</li>
<li><strong>Disclaimer</strong> The materials on Voomet Studio's website are provided on an 'as is' basis. Voomet Studio makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights.</li>
<li><strong>Limitations</strong> In no event shall Voomet Studio or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption) arising out of the use or inability to use the materials on Voomet Studio's website, even if Voomet Studio or a Voomet Studio authorized representative has been notified orally or in writing of the possibility of such damage.</li>
<li><strong>Revisions and Errata</strong> The materials appearing on Voomet Studio's website could include technical, typographical, or photographic errors. Voomet Studio does not warrant that any of the materials on its website are accurate, complete, or current.</li>
<li><strong>Links</strong> Voomet Studio has not reviewed all of the sites linked to its website and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Voomet Studio of the site. Use of any such linked website is at the user's own risk.</li>
<li><strong>Modifications</strong> Voomet Studio may revise these terms of service for its website at any time without notice. By using this website you are agreeing to be bound by the then current version of these Terms of Use.</li>
<li><strong>Governing Law</strong> These terms and conditions are governed by and construed in accordance with the laws of [Your Jurisdiction] and you irrevocably submit to the exclusive jurisdiction of the courts in that State or location.</li></ul>
<p>By using our website, you signify your acceptance of these terms. If you have any questions about these Terms of Use, please contact us.</p>



                  

                </div>
                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<!-- ./page wapper-->
