<div class="top-header">
<div class="container-fluid">
<div class="main-header">
<div class="row">
<div class="col-md-12 col-sm-12 col-lg-12 text-center">
<div class="nav-top-links">
	<a href="mailto:<?php echo SITE_EMAIL_ENQ; ?>" style="font-size:1.1em;color:#4dad24;font-weight:bold;"><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo SITE_EMAIL_ENQ; ?></a>
	<a href="https://api.whatsapp.com/send?phone=918050504950" target="_blank" style="font-size:1.1em;color:#4dad24;font-weight:bold;">
	
	<i class="fa fa-whatsapp" aria-hidden="true"></i> 
	
	8050504950</a>
</div>
</div>


<div class="col-md-4 pull-right col-xs-12 col-sm-4 col-lg-3" id="shopping-cart-box-ontop1_topheader" style="display:none;">
            <div class="header-search-box">
                <form  class="form-inline" name="searchForm_org" id="searchForm_org" method="post" autocomplete="off" onsubmit="return searchFun('<?php echo $current_controller;?>')">    
				<?php
				    if(!isset($_REQUEST['search_cat'])){
				        $_REQUEST['search_cat']="";
				    }
				?>
                    <div class="form-group form-category"  style="display:none;">
                        <select class="select-category" name='search_cat' id='search_cat'>
						<option value="" <?php if($_REQUEST['search_cat']==""){echo "selected";}?>>All Categories</option>
							<?php
							if(!empty($get_all_parent_categories)){
								foreach($get_all_parent_categories as $parent_categories_arr){
									?>
									<option value="<?php echo $parent_categories_arr["pcat_id"]."_pcat"; ?>" <?php if($_REQUEST['search_cat']==$parent_categories_arr["pcat_id"]."_pcat"){ echo "selected";}?> ><?php echo $parent_categories_arr["pcat_name"];?></option>
									<?php
								}
							}
							?>
                            <?php
							if(!empty($get_all_categories)){
								foreach($get_all_categories as $categories_arr){
									?>
									<option value="<?php echo $categories_arr["pcat_id"]."_".$categories_arr["cat_id"];?>" <?php if($_REQUEST['search_cat']==$categories_arr["pcat_id"]."_".$categories_arr["cat_id"]){ echo "selected";} ?>><?php echo $categories_arr["cat_name"];?></option>
									<?php
								}
							}
							?>
							
                        </select>
                    </div>
                      <div class="form-group input-serach ui-widget">
                        <input type="text" onkeyup="doSearch()" id="search_keywords" name="search_keywords" placeholder="Search..." value="<?php if(!empty($_REQUEST["search_keyword"])){echo $_REQUEST["search_keyword"];}?>">
                      </div>
                      <div class="dropdown" id="dropssss" style="display:block;">
                          <ul class="dropdown-menu" id="search_listss" style="margin-top:8px;padding:2px 5px;width:100%;"></ul>
                      </div>
                      <input type="hidden" name="level_type" id="level_type" value="">
                      <input type="hidden" name="level_value" id="level_value"  value="">
                      <input type="hidden" name="level_name" id="level_name"  value="">
					  <input type="hidden" name="search_keyword_index_page" id="search_keyword_index_page" value="">
					  <input type="submit" id="search_keyword_btn" class="pull-right btn-search" value="">

                </form>
            </div>
            <!---adding to the cart---->
	
<script>
    
function UseSuggestKeyWord(obj){
    document.getElementById('search_keywords').value=""
    document.getElementById('search_keywords').value=obj.innerText;
    document.getElementById('level_type').value=obj.getAttribute('level_type');
    document.getElementById('level_value').value=obj.getAttribute('level_value');
    document.getElementById('level_name').value=obj.getAttribute('level_name');
    removeSugessation();
    //document.getElementById("searchForm_org").submit();
}

function removeSugessation(){
    document.getElementById('dropssss').classList.remove('open');
    document.getElementById('search_listss').innerHTML=""
    //document.getElementById('search_keywords').value=""
}

function doSearch(){
    var search_cat=document.getElementById('search_cat').value;
    var search_keyword=document.getElementById('search_keywords').value;
    document.getElementById('search_listss').innerHTML=""

        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if(xhr.responseText.length>2){
								
                               var dats=JSON.parse(xhr.responseText);
                               var str="";
                               /*str+='<li class="text-right"><a  href="javascript:removeSugessation(this);"> <i class="fa fa-times"></i></a></li>'*/
                               if(dats['exact']){
                                   for(var i=0;i<dats['exact'].length;i++){
                                       var keys = Object.keys(dats['exact'][i]); 
                                       var attrs=keys[1];
                                       var attrsk=keys[2];
                                      var att0='level_type="'+attrsk+'"'

                                      var att2=""
                                        if(attrs=="product_name"){
                                            att2+='level_name="'+dats['exact'][i].product_name+'"'
                                        }
                                        if(attrs=="brand_name"){
                                            att2+='level_name="'+dats['exact'][i].brand_name+'"'
                                        }
                                        if(attrs=="subcat_name"){
                                            att2+='level_name="'+dats['exact'][i].subcat_name+'"'
                                        } 
                                      var att1=""
                                        if(attrsk=="product_id"){
                                            att1+='level_value="'+dats['exact'][i].product_id+'"'
                                        }
                                        if(attrsk=="brand_id"){
                                            att1+='level_value="'+dats['exact'][i].brand_id+'"'
                                        }
                                        if(attrsk=="subcat_id"){
                                            att1+='level_value="'+dats['exact'][i].subcat_id+'"'
                                        } 
                                     
                                       str+='<li onclick="UseSuggestKeyWord(this)" '+att0+' '+att1+' '+att2+' ><a  href="javascript:void(0)"> '+dats['exact'][i].label+'</a></li>'
                                   }
                               }

                               document.getElementById('search_listss').innerHTML=str
                               document.getElementById('dropssss').classList.add('open');
                            }else{
                                document.getElementById('dropssss').classList.remove('open');
                            }
                           
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>getSearchSugessations", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('search_cat='+search_cat+"&search_keyword="+search_keyword);
                }
          return false; 
}
</script>	
			
        </div>
		
		
		</div>
		</div>
<div id="user-info-top" class="hidden-lg hidden-md hidden-sm visible-xs pull-right">
<div class="dropdown">
<?php if($this->session->userdata("customer_name")) { ?>
<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><?php echo mb_strimwidth($this->session->userdata("customer_name"), 0, 15, "..."); ?> <span class="caret"></span></a>
<ul class="dropdown-menu mega_dropdown" role="menu">
<li><a href="<?php echo base_url();?>account">Account</a></li>
<li><a href="<?php echo base_url();?>account/my_order">Order</a></li>
<li><a href="<?php echo base_url();?>account/wallet">Wallet</a></li>
<li><a href="<?php echo base_url();?>account/wishlist">Wishlists</a></li>
<li><a href="<?php echo base_url();?>account/reviews">Reviews &amp; Ratings</a></li>
<li><a href="javascript:logoutFun();">Logout</a></li>
</ul> 					
<?php } else { ?>
<!--<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><span>My Account <i class="fa fa-angle-down" aria-hidden="true"></i> </span></a>
<ul class="dropdown-menu mega_dropdown" role="menu">-->
<a href="<?php echo base_url();?>login">Log In</a>
<!--</ul>--> 
<?php } ?>
<!--<a class="top_header_cart" href="<?php //echo base_url() ?>cart"><i class="fa fa-shopping-cart cart-right-corner"> | <span class="cart_count"> {{cart_count}}</span></i></a>--->
</div>
</div>
</div>
</div>

<script type="text/javascript">
	function searchFun(current_controller){
	
	    var search_keyword=document.getElementById("search_keywords").value;
	    if(search_keyword.trim()==""){
	        location.reload();
	        return false;
	    }
		var level_name=document.getElementById("level_name").value
		if(level_name.trim()==""){
			alert('Searched Item does not exist');
	        document.getElementById("search_keywords").value='';
	        return false;
	    }
		if(current_controller=="search"){
			
			var search_cat=document.getElementById("search_cat").value;
			var search_keyword=document.getElementById("search_keywords").value;
			document.getElementById("search_keyword_index_page").value=search_keyword;
			
			if(search_cat==""){
				action_url="<?php echo base_url()?>search_category/a8i5t9l5g0";
			}
			else{
			    
				search_cat_arr=search_cat.split("_");
				
				if(search_cat_arr[1]=="pcat"){
					action_url="<?php echo base_url()?>search_category/a8i5t9l5g"+search_cat_arr[0];
				}
				else{
					action_url="<?php echo base_url()?>search_category/a8i5t9l5g"+search_cat_arr[0]+"/adomyhrxh"+search_cat_arr[1];
				}
			}
			
			document.getElementById("searchForm_org").action=action_url;
			document.getElementById("searchForm_org").submit();
			
		}
		else{
		   
			var search_cat=document.getElementById("search_cat").value;
			var search_keyword=document.getElementById("search_keywords").value;
			//var search_cat=document.getElementById("search_cat").value;
			if(search_cat==""){
				action_url="<?php echo base_url()?>search_category/a8i5t9l5g0";
			}
			else{
				search_cat_arr=search_cat.split("_");
				if(search_cat_arr[1]=="pcat"){
					action_url="<?php echo base_url()?>search_category/a8i5t9l5g"+search_cat_arr[0];
				}
				else{
					action_url="<?php echo base_url()?>search_category/a8i5t9l5g"+search_cat_arr[0]+"/adomyhrxh"+search_cat_arr[1];
				}
			}
			document.getElementById("searchForm_org").action=action_url;
			//document.searchForm_org.submit();
			
			
			//if(search_keyword!=""){
				$("input[name='search_keyword']").val(search_keyword);
				//$("input[name='search_cat']").val(search_cat);
				//
				$("input[name='price_filter']").val("");
				$("input[name='cat_filter']").val("");
				$("input[name='brand_filter']").val("");
				//
				document.getElementById("searchForm_org").submit();
			//}
		}
	}
	
	$(document).ready(function(){
		$(".ui-autocomplete").click(function(){
			//alert($( "#search_keyword" ).val());
			$("#search_keyword_btn").trigger("click");
		})
	})

	$('body').on('click', 'a.disabled', function(event){
		event.preventDefault();
	});
	

</script>
<!--/.top-header -->