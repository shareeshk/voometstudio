
<script type="text/javascript" src="<?php echo base_url();?>assets/js/multiselect.js"></script>

<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js" ></script>
<link href="<?php echo base_url();?>assets/css/datepicker.css" rel="stylesheet" /> 

<style type="text/css">
    .margin_top{
        margin-top: 30px;
    }
    .verify_code_form{
        margin-top: 20px;
        background-color: #eee;
        padding: 10px;
    }
    .margin{
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .container_img {
	/* width: 30em; */
	overflow-x: auto;
	white-space: nowrap;
    width: 93%;
}
.td_padd{
    padding:10px;
    /* box-shadow: 1px 1px 10px #999;  */
}
.td_inside{
    box-shadow: 1px 1px 10px #999;
    padding: 10px;

    width: 130px;
    height: 100px;
}
</style>

<div class="columns-container">
    <div class="container" id="columns">
        <ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item active">Request a Quote</li>
		</ol>
        <div class="row">
            <?php 
            
            //print_r($this->session->userdata());
            
            ?>
			<?php //require_once 'leftcolum.php';?>
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
				<div class="panel panel-default">
               
                <!----------sku details------------------>
                
          
                    
    
                   
                     
        <div class="panel-body">
        
        <div class="col-sm-offset-2 col-sm-10">
        
           
            
        <form id="rfq_common_form" class="form-horizontal mt-20" enctype="multipart/form-data">
					
			
			<input type="hidden" name="rq_unique_id" id="rq_unique_id" value="<?php echo $rq_unique_id;?>">
            <input type="hidden" name="rq_customer_id" id="rq_customer_id" value="<?php echo $rq_customer_id;?>">
			<input type="hidden" name="rq_sku_ids" id="rq_sku_ids" value="<?php echo $rq_sku_ids;?>">
			<input type="hidden" name="rq_inv_ids" id="rq_inv_ids" value="<?php echo $rq_inv_ids;?>">
			<input type="hidden" name="rq_inv_qtys" id="rq_inv_qtys" value="<?php echo $rq_inv_qtys;?>">
			<input type="hidden" name="vendor_id_list" id="vendor_id_list" value="<?php echo $vendor_id_list;?>">
			
                           
                                    
                            
                                    <div class="card row">
									  <div class="col-sm-11 col-sm-offset-2">

                                      <h3 class="">Request for Quotation</h3>

										<h4 class="mt-20 text-primary">
											List of Products Chosen  <?php //echo $rq_sku_ids; ?></h4>
											<br>
                                            <div class="row mb-20">

                                            <div class="col-md-7 text-center" class="">

                                            <div class="container_img">
<table>
	<tbody>
		<tr>
                                            <?php 
                                            $inv_arr=explode(',',$rq_inv_ids);
                                            $inv_qtys_arr=explode(',',$rq_inv_qtys);

                                            //print_r($inv_qtys_arr);

                                            $inv_data=get_inv_info($inv_arr);
                                            if(!empty($inv_data)){
                                                $i=0;
                                                foreach($inv_data as $data){
                                            
                                                    ?>
                                                <td class="td_padd">
                                                    <div class="td_inside">
                                                        <img src="<?php echo base_url().$data->thumbnail; ?>" style="width:40%;">
                                                        <div><small><small><?php echo "SKU ID : ".$data->sku_id; ?></small>
                                                    <br><small>Qty:<?php echo $inv_qtys_arr[$i] ?></small>
                                                    </small></div>
                                                    </div>
                                                </td>
                                                    <?php 

                                                    $i++;

                                                }
                                            }
                                            
                                            ?>
        </tr>
	</tbody>
</table>
</div>
                                            </div>
                                            </div>
										
									  </div>
									</div>
                                   
								   
								   
									
			
			
                                    
                                    <div class="form-group">
                                        <label for="name" class="control-label col-sm-2"> Name</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="rq_name" value="<?php echo $rq_name;?>" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="control-label col-sm-2"> Email</label>
                                        <div class="col-sm-6">
                                            <input type="email" class="form-control" name="rq_email" value="<?php echo $rq_email;?>" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="control-label col-sm-2"> Mobile</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="rq_mobile" value="<?php echo $rq_mobile;?>" onkeypress="return isNumber(event)" maxlength="10">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="control-label col-sm-2"> City</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="rq_location" value="" >
                                        </div>
                                    </div>
                                
                                

									
									
									 <div class="form-group mt-20">
										<label for="name" class="control-label col-sm-2">Postal code</label>
										<div class="col-sm-6">
											<input type="text" onkeypress="return isNumber(event)" class="form-control" name="rq_pincode"  id="rq_pincode" placeholder="Enter your postal code" maxlength="6" value="<?php echo $rq_pincode;?>">
										</div>
									</div><!-- postalcode -->
									
									
                                    
                                    <div class="form-group">
                                        <div class="col-sm-3 col-sm-offset-2">
                                          <button type="submit" class="button btn-block preventDflt" id="rfq_common_form_btn" style="background-color:#1429a0;color:#fff;font-size:1 em;opacity:1"> Submit </button>
                                        </div>
                                        <div class="col-sm-3">
                                          <button type="reset" class="button btn-block preventDflt" style="background-color:#1429a0;color:#fff;font-size:1 em;opacity:1"> Cancel </button>
                                        </div>
                                    
                                    </div>
									
									
									
									<div class="row" id="rfq_common_form_btn_progress_bar_div" style="display:none;">
			  <div class="col-md-12">
		<div class="form-group">
                        <div class="progress" style="height:10px !important;">
                            <div class="progress-bar progress-bar-success progress-bar-success progress-bar-striped myprogress_webinar" role="progressbar" style="width:0%;"></div>
                        </div>

                        
                    </div>
				</div>
			</div>
			
			
                                    
                                    
                                </form>
					</div>	
				</div>
    
    
                
                <!----------sku details------------------>
                
                
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

<script>
   
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
            return false;
        }
        return true;
    }
    
       
	   
$(document).ready(function(){	   
	$("#rfq_common_form").on('submit',(function(e) {
		e.preventDefault();
		
		rq_pincode=document.getElementById("rq_pincode").value;
		if(rq_pincode.length!=6){
			swal({
				title:"Info", 
				html: "Please enter 6 digit postal code", 
				type: "info",
				allowOutsideClick: false
			}).then(function () {
				document.getElementById("rq_subcat_id").value="";
				return false;
			});
			return false;
		}
		
		rq_unique_id=$("#rq_unique_id").val();
		
		$.ajax({
			url:"<?php echo base_url();?>Account/request_a_quote_common_action",
			type:'POST',
			data:new FormData(this),
			contentType: false,
			cache: false,	
			processData:false,
			beforeSend:function(){
					$("#rfq_common_form_btn").html("Processing <i class='fa fa-refresh fa-spin'></i>");
					$("#rfq_common_form_btn_progress_bar_div").css({"display":"block"});
				},
				xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    setInterval_webinar=setInterval(function(){ 
                                            $.ajax({
                                                    url:"<?php echo base_url();?>Account/request_a_quote_common_action_mail_status/"+rq_unique_id,
                                                    type: "POST",      				// Type of request to be send, called as method
                                                    data:  "1=2",
                                                    dataType:"JSON",
                                                    success:function(res_total){
                                                            vendor_email_list_sent=res_total.vendor_email_list_sent;
                                                            percentComplete=(vendor_email_list_sent/1);
                                                            percentComplete = parseInt(percentComplete * 100);
                                                            //$('.myprogress_webinar').text(percentComplete + '%');
                                                            $('.myprogress_webinar').css('width', percentComplete + '%');
                                                    }
                                            });
                                    }, 1000);
                                }
                            }, false);
                            return xhr;
                        },
			success:function(data){
                                //console.log(data);
                                //return false;
				$("#rfq_common_form_btn").html("Submit");
				if(data=="mail_sent" || data=="mail_not_sent" || data=="no_vendors"){
					swal({
						title:"Success", 
						text:"Your request is captured", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
                        clearRFQPanel();
						location.href="<?php echo base_url();?>catalog_requestforquotation";
					});
				}
				else{
					swal("Error", "Error", "error");
				}
			}
		});
	}));
});

function clearRFQPanel() {
       
        localStorage.removeItem("LocalCustomerRequestForQuotationCart");
        localStorage.removeItem("LocalCustomerRequestForQuotationCartLength");
        localStorage.removeItem("LocalCustomerRequestForQuotationCartTotal");
        localStorage.removeItem("LocalCustomerRequestForQuotationCartGrandTotal");
        localStorage.removeItem("LocalCustomerRequestForQuotationCartSavedTotal");
}


</script>