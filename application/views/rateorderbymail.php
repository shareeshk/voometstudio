<style>
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
	border-top:0px;
}
.table>thead>tr>th{
	border-bottom:0px;
}
</style>
<div class="columns-container">
    <div class="container" id="columns">
        <ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item active">Rate Order By Mail</li>
		</ol>
        <div class="row mt-10">
            <div class="center_column col-xs-12 col-sm-12">
					<?php
						foreach($order_summary_mail_data_arr as $order_summary_mail_data){
						$order_details='';
							$order_details.='<table>';
							$order_details.='<tr>';
							$order_details.='<td>';
							$order_details.="<img src='".base_url().$order_summary_mail_data['thumbnail']."'>";
							
							$order_details.='</td>';
							$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:middle;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.=$order_summary_mail_data['product_name'];
							$order_details.='</p></td>';
							$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:middle;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.=$order_summary_mail_data['order_id'];
							$order_details.='</p></td>';
							
							
							$get_delivered_time_of_order_item=$controller->get_delivered_time_of_order_item($order_summary_mail_data['order_id'],$order_summary_mail_data["order_item_id"]);
							
							
							$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:middle;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
							foreach($get_delivered_time_of_order_item as $delivered_time_of_order_item){
								$order_details.=date("D j M,Y",strtotime($delivered_time_of_order_item["order_delivered_date"]))." ".$delivered_time_of_order_item["order_delivered_time"];
							}
							$order_details.='</p></td>';
							
							
							$order_details.='</tr>';
							
							
							$order_details.='</table>';
						}
						echo $order_details;
					?>
            </div>
            <!-- ./ Center colunm -->
        </div>
		
        <!-- ./row-->
		
		
		
		<div class="row">
            <div class="center_column col-xs-12 col-sm-12">
				<form action="<?php echo base_url()?>Account/rateorderbymail_action" method="post" class="form-horizontal">
				
				<input type="hidden" name="order_item_id" value="<?php echo $order_summary_mail_data['order_item_id'];?>">
				<input type="hidden" name="rateorder_id" value="<?php echo $rateorder_id;?>">
				
				<table class="table">
				<thead>
					<tr>
						<th colspan="6" class="text-center">
							<h4><?php echo $get_feedback_questions_heading;?></h4>
						</th>
					</tr>
					<?php
						if(!empty($get_feedback_questions_arr)){
					?>
					<tr>
						<th>Parameters</th>
						<th>1</th>
						<th>2</th>
						<th>3</th>
						<th>4</th>
						<th>5</th>
					</tr>
					<?php
						}
					?>
				</thead>
				<tbody>
				<?php
					if(!empty($get_feedback_questions_arr)){
						foreach($get_feedback_questions_arr as $feedback_questions_arr){
							?>
								<tr>
									<td><?php echo $feedback_questions_arr["question"];?></td>
									<td><label><input type="radio" name="question[question_<?php echo $feedback_questions_arr["id"];?>]" value="1" required> Very Poor</label></td>
									<td><label><input type="radio" name="question[question_<?php echo $feedback_questions_arr["id"];?>]" value="2" required> Poor</label></td>
									<td><label><input type="radio" name="question[question_<?php echo $feedback_questions_arr["id"];?>]" value="3" required> Average</label></td>
									<td><label><input type="radio" name="question[question_<?php echo $feedback_questions_arr["id"];?>]" value="4" required> Good</label></td>
									<td><label><input type="radio" name="question[question_<?php echo $feedback_questions_arr["id"];?>]" value="5" required> Excellent</label></td>
								</tr>
							<?php
						}
					}
				?>
				<?php
					if($given_rating==3 || $given_rating==4 || $given_rating==5){
				?>
				<tr>
					<td colspan="5">
						<textarea class="form-control" name="comments" placeholder="Comments" required cols="5" rows="5"></textarea>
					</td>
				</tr>
				<?php
					}
				?>
				<tr>
					<td colspan="6"><button class="btn btn-default" type="submit">Submit</button></td>
				</tr>
				</tbody>
				</table>
				</form>
			</div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
		
		
    </div>
</div>
