  <style>
  .font{
	  font-size:1.2em;
  }
  
  .gap50{
	  margin-top:50px;
  }
  .gap25{
	  margin-top:25px;
  }
  
  .bglabelgreen{	  
	  background-color:#548235;
	  box-shadow: 2px 2px 2px #888888;
	  color:#fff;	 
  }
  
  .bglabelbrown{	  
	  background-color:#663300;
	  box-shadow: 2px 2px 2px #888888;
	  color:#fff;	 
  }
  
   .bglabellgbrown{	  
	  background-color:#c55a11;
	  box-shadow: 2px 2px 2px #888888;
	  color:#fff;	 
  }
  
  .squarelist {list-style-type: square;}
  .flex-row .thumbnail,
.flex-row .caption {
  flex:1 0 auto;
  flex-direction:column;
}
.flex-row .caption {
height:25vh;	
}
.flex-text {
  flex-grow:1
}
.flex-row img {
  height:auto;
  width:100%
}
@media only screen and (min-width : 481px) {
    .flex-row {
        display: flex;
        flex-wrap: wrap;
    }
    .flex-row > [class*='col-'] {
        display: flex;
        flex-direction: column;
    }
    .flex-row.row:after, 
    .flex-row.row:before {
        display: flex;
    }
}
.lead {
        font-size: 14px;
    }
  </style>
  

<div class="block-banner8 container" id="contributingfactors">
<div class="section8">
			<h3 class="section-title align-bottom align-top">Why Voomet Studio</h3>
		</div>
    <div class="row gap25">
		<div class="col-md-3 col-md-offset-1">
      <div class = "thumbnail">
         <img src = "<?php echo base_url()?>assets/images/founder1.jpg" class="img-responsive" alt = "About Us Our Story">
      </div>
   </div>
   <div class = "col-md-7">
         <p class="lead  text-justify">
		Voomet Studio distinguishes as a design house through our unwavering commitment to innovation, creativity, and quality in every project. By partnering with Voomet Studio, partner / collaborator can unlock a wealth of opportunities. In the world of furniture sales, customer experience is the key determinant of success. Voomet Studio excels in enhancing this experience for furniture ecommerce by providing advanced tools and technologies that allow customers to interact with and visualize furniture in their own spaces, ensuring the perfect fit for their homes. This partnership enables furniture retailers to leverage innovative solutions, gain a competitive edge, and boost their online presence. Furthermore, Voomet Studio's industry expertise helps retailers navigate the crowded ecommerce landscape, simplifying the selection of the best platforms and strategies for success.

		
</p>
<p class="lead  text-justify">
Voomet Studio : Your End To End Destination For <strong>Endless Options. Unmatched Prices. Customizable Furniture. Free Shipping </strong>

</p>
   </div>
    </div>
	<div class="row gap50"></div>
	 
	<div class="section8">
			<h3 class="section-title"></h3>
		</div>
  
		 <div class="row gap25">
   <div class = "col-md-7">
         <p class="lead text-justify">
		 
        At Voomet Studio, we understand that our mission goes beyond selling well-designed products. We're here to help you create spaces that reflect your personality. We want you to discover the joy of creation, starting with your home. We encourage you to see setting up a space as an invigorating, creative endeavor. To breathe life into empty rooms with your ideas and transform a few square feet of nothing into something beautiful.</p>
 <p class="lead text-justify"><strong>Brand Recognition:</strong> Voomet Studio has established itself as a trusted name in the industry, renowned for its commitment to excellence and customer satisfaction. By aligning with our brand, partners can leverage our reputation to enhance their own visibility and credibility in the market.</p>
 <p class="lead text-justify"><strong>Expanded Reach:</strong> As a rapidly growing brand, Voomet Studio operates in multiple cities across India, offering partners the opportunity to tap into new markets and reach a broader audience. Our expansive network and market presence provide partners with a platform for growth and expansion.</p>
 <p class="lead text-justify"><strong>Collaborative Opportunities:</strong> Voomet  Studio values collaboration and partnerships, fostering relationships with suppliers, manufacturers, and other industry stakeholders. By joining forces with us, partners can explore collaborative opportunities and benefit from synergies that drive mutual success.</p>
 <p class="lead text-justify"><strong>Innovation Hub:</strong> Voomet Studio prides itself on being at the forefront of design innovation, constantly seeking new ideas and trends to inspire creativity. Partnerships with Voomet Studio provide access to an innovation hub where ideas are nurtured, and creativity flourishes, leading to the development of cutting-edge products and services.</p>
 <p class="lead text-justify">It is no longer the matter of the price that defines whether or not the furniture is going to sell well, it is the customer’s experience. A successful and profitable furniture ecommerce website allows the client to experiment with the offered item so that the best fit-for-their-homes design is projected. However, to develop and thrive on the digital market, furniture retailers usually join top-rated ecommerce platforms, and that is a wise decision. The only downside is that the platforms are many, and it is quite challenging to track them all on one’s own.</p>

		</p>
		
   </div>
   <div class="col-md-3 col-md-offset-1">
      <div class = "thumbnail">
         <img src = "<?php echo base_url()?>assets/images/founder.jpg" alt = "About Us Our Story">
      </div>
   </div>
    </div>
	
	
	
	
	<div class="row gap50"></div>
	
	
    
	
	 

	
	
	
	
	
	
</div>
