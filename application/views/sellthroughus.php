<style>
#content {
    width: 100%;
    padding: 20px;
    min-height: 100vh;
    transition: all 0.3s;
    position: unset!important;
    top: 0;
    right: 0;
}
.box h4{
	font-weight:bold;
	position:relative;
	padding-bottom:10px;
} 
.page-heading {
    height: 32px;
    /* border-bottom: 1px solid #eaeaea; */
    line-height: 24px;
    position: relative;
    font-size: 24px;
    padding: 0px;
}
.mt-30 {
    margin-top: 3em;
}

</style>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo base_url()?>search" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Sell Through Us</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">Informations</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
									<li><span></span><a href="<?php echo base_url();?>aboutus">About Us</a></li>
                                    
                                    <li class="active"><span></span><a href="<?php echo base_url();?>sellthroughus">Sell Through Us</a></li>
									
									
									<!--<li><span></span><a href="<?php //echo base_url();?>returns">Returns</a></li>
									<li><span></span><a href="<?php //echo base_url();?>replacements">Replacements</a></li>
									<li><span></span><a href="<?php //echo base_url();?>refunds">Refunds</a></li>
									<li><span></span><a href="<?php //echo base_url();?>cancellations">Cancellations</a></li>-->
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
                
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9 section8" id="center_column">
                <!-- page heading-->             
                <!-- Content page -->
                <div class="content-text1 clearfix">

                    <!------------content added-------->
					
					<div class="col-md-12">
		 
        <div id="content" class="para">
            
            
        
            <section id="div1">
<h3 class="section-title text-center align-bottom align-top">Who can partner with us</h3>

              
                <p style="padding-top:1.5em">
			Voomet Studio is a design house with a vision – Elevating Home. Empowering Partners. We are on the mission to transform a million homes into stunning, personalized spaces.</p>
<p class="mt-10">we welcome a diverse range of partners who share our passion for exceptional quality and design in the world of furniture and home interiors:</p>
            
<ol type="1" class="mt-10"><li style="padding-bottom:5px">1. <strong>Furniture Manufacturers:</strong> Display your expertly crafted pieces to our extensive customer base.</li>
<li style="padding-bottom:5px">2. <strong>Artisans and Designers:</strong> Present your unique, handmade, or custom designs to a discerning audience.</li>
<li style="padding-bottom:5px">3. <strong>Home Decor Suppliers:</strong> Enhance our furniture collections with your stylish and complementary accessories.</li>
<li style="padding-bottom:5px">4. <strong>Vintage Collectors:</strong> Offer your curated selection of antique and vintage finds to our enthusiasts.</li>
<li style="padding-bottom:5px">5. <strong>Eco-friendly Brands:</strong> Join us in promoting sustainable living with your environmentally conscious furniture and décor.</li></ol>

<p class="mt-10">Partner with us and elevate your business by reaching new customers and expanding your market presence. Together, let's create stunning, personalized spaces that inspire and delight. Let's make beautiful homes, together!</p>
     
            </section>
        
        
            <section id="div2 mt-50" style="padding-top:35px">
<h3 class="section-title text-center align-bottom align-top">Creating a WIN within Win</h3>

               
               <p style="padding-top:1.5em">
                    At Voomet Studio, we believe in creating a thriving ecosystem where our sellers can flourish. By partnering with us, you unlock a world of opportunities to elevate your business and reach new heights. Here's how we ensure a win-win collaboration:</p>
	<div class="row mt-50">
	<div class="col-md-4 text-center">
	 <img src = "<?php echo base_url()?>assets/images/seamless-integration.png" class="img-responsive w-75 pb-10" style="margin:auto" alt ="Extensive Reach and Seamless Integration">
	<div class="box pt-10"><div class="box-title"><h4 class="tittle">Extensive Reach and Seamless Integration</h4>
</div>
<p class="mt-10">Tap into our vast customer base and showcase your products beautifully with our user-friendly platform. Enjoy a smooth onboarding process and watch your market presence soar.</p> 
</div>
     </div>
	 <div class="col-md-4 text-center">
	 	 <img src = "<?php echo base_url()?>assets/images/competitive-fees.png" class="img-responsive w-75 pb-10 text-center" style="margin:auto" alt ="Marketing Support and Competitive Fees">
	 
	<div class="box pt-10"><div class="box-title"><h4 class="tittle">Marketing Support and Competitive Fees</h4>
</div>
<p class="mt-10">Benefit from our robust marketing campaigns and promotional activities, driving traffic to your listings. Maximize your profits with our transparent and competitive fee structure.</p>
</div>
     </div>
	 <div class="col-md-4 text-center">
	  <img src = "<?php echo base_url()?>assets/images/Support-growth.png" class="img-responsive w-75  pb-10" style="margin:auto" alt ="Dedicated Support and Growth Opportunities">
	 
	<div class="box pt-10"><div class="box-title"><h4 class="tittle">Dedicated Support and Growth Opportunities</h4>
</div>
<p class="mt-10">Rely on our expert team for assistance and guidance every step of the way. Take advantage of exclusive opportunities for brand exposure and growth within our thriving marketplace. Join Voomet Studio  today and let's embark on a journey of mutual success. Together, we'll transform homes and create lasting impressions with every piece.</p>
</div>
     </div>
</div>	 
                
            </section>
        
        
                   
        
          <section id="div4">  
 <div class="col-md-12">          
				<h2 class="page-heading mt-30">
                    <span class="page-heading-title2">Reach out to us</span>
                </h2>
				
                <form method="post" role="form" id="contactForm2" name="contactForm2" class="form-horizontal" onsubmit="submitForm();">
			<div class="form-group">
			<div class="col-md-5 col-sm-5">
			<input name="name" id="name" placeholder="Enter your Name" class="form-control" type="text" onkeyup="remove_error();">
			</div>
					<div class="col-md-5 col-sm-5">
			<input name="city" placeholder="Enter your City" id="city" class="form-control" type="text" onkeyup="remove_error();">
			</div>
					</div>
					<div class="form-group">
					<div class="col-md-5 col-sm-5">
			<input name="email" id="email" placeholder="Enter your E-mail ID" class="form-control" type="text" onkeyup="remove_error();">
			</div>
					<div class="col-md-5 col-sm-5">
		<input name="mobile" id="mobile" placeholder="Enter your Mobile Number" maxlength="10" class="form-control" onkeypress="return isNumber(event)" type="text" onkeyup="remove_error();">
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-10 col-sm-10">
	<input name="company_name" id="company_name" placeholder="Enter your Company Name" class="form-control" type="text" onkeyup="remove_error();">
	</div>
	</div>
	<div class="form-group">
		<div class="col-md-10 col-sm-10">
		<select name="interested_in" class="form-control place_holder" id="interested_in" onchange="remove_error();">
		<option value="" selected="">--You are interested in--</option>
										 
		  <option value="Selling to <?php echo SITE_NAME; ?>">Selling through <?php echo SITE_NAME; ?></option>
		  <option value="Becoming <?php echo SITE_NAME; ?> Franchise">Becoming <?php echo SITE_NAME; ?> Franchise</option>
		     <option value="Co-Manufacturing">Co-Manufacturing</option>
		</select>
		</div>
	</div>
			<div class="form-group">
<div class="col-md-10 col-sm-10">			
		<textarea name="comment" id="comment" placeholder="Say it here (250 chars)" class="form-control" maxlength="250" onkeyup="remove_error();"></textarea>
		</div>
		</div>
		<div class="form-group">
    <div class="col-sm-10">
     <!--<input type="submit"  value="Send" class="custom_btn btn btn-primary" />-->
	  <button type="submit" class="button" id="formsubmit1">Send</button>
	  <div id="overlay" style="display:none;"><p></p><h4 id="success_para" style="font-size:16px; color:#9b3e01;">Message Sent</h4><p></p></div>
    </div>
  </div>
					
				</form>
          </div>  
            </section>
        
        </div><!-- /#content -->	
	</div>
					
					
                    <!------------content added-------->

                </div>
                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<!-- ./page wapper-->

<script>
function remove_error(){
	$('#comment').css({"border": "1px solid #ccc"});
	$('#mobile').css({"border": "1px solid #ccc"});
	$('#city').css({"border": "1px solid #ccc"});
	$('#company_name').css({"border": "1px solid #ccc"});
	$('#interested_in').css({"border": "1px solid #ccc"});
	$('#name').css({"border": "1px solid #ccc"});
	$('#email').css({"border": "1px solid #ccc"});
}
function submitForm(){
	
	err=0;
	
	name=$("#name").val().trim();
	city=$("#city").val().trim();
	emailid=$("#email").val().trim();
	mobileno=$("#mobile").val().trim();
	companyname=$("#company_name").val().trim();
	interested_in=$("#interested_in").val().trim();
	comment=$("#comment").val().trim();

	if(!emailid.match('[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}')){
	   $('#email').css("border", "1px solid red");	   
	   err = 1;
	}else{			
		$('#email').css({"border": "1px solid #ccc"});
	}
	if(name=='' && !(name.length>=3)){
	   $('#name').css("border", "1px solid red");	   
	   err = 1;
	}else{			
		$('#name').css({"border": "1px solid #ccc"});
	}
	if(city==''){
	   $('#city').css("border", "1px solid red");	   
	   err = 1;
	}else{			
		$('#city').css({"border": "1px solid #ccc"});
	}
	if(companyname==''){
	   $('#company_name').css("border", "1px solid red");	   
	   err = 1;
	}else{			
		$('#company_name').css({"border": "1px solid #ccc"});
	}
	if(interested_in==''){
	   $('#interested_in').css("border", "1px solid red");	   
	   err = 1;
	}else{			
		$('#interested_in').css({"border": "1px solid #ccc"});
	}
	if(mobileno==''){
	   $('#mobile').css("border", "1px solid red");	   
	   err = 1;
	}else{			
		$('#mobile').css({"border": "1px solid #ccc"});
	}
	if(comment==''){
	   $('#comment').css("border", "1px solid red");	   
	   err = 1;
	}else{			
		$('#comment').css({"border": "1px solid #ccc"});
	}
	
	if(err==1){
		bootbox.alert({ 
		  size: "small",
		  message: 'Please Fill All Fields',
		});
		return false;
	}else{
		$.ajax({
			type: 'post',
			url: '<?php echo base_url(); ?>add_partner',
			data: $('#contactForm2').serialize(),
			dataType:"JSON",
			beforeSend: function() {	
					$('#formsubmit1').html('<i class="fa fa-refresh fa-spin"></i> Processing...');
				},
			success: function (res) {
				//alert(response);
				//return false;
				$('#formsubmit1').html('Send');
				//alert(res.status)
				if(res.status==true){
					partner_unique_id=res.partner_unique_id;
					location.href="<?php echo base_url();?>thankyou_partner/"+partner_unique_id;
				}
				else{
					bootbox.alert({ 
					  size: "small",
					  message: 'Oops Error!',
					});	
				}
			}
		});
		
	}
	

	return false;
///////////////////////	
				
 }
function isNumber(evt) {

    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
	
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>

