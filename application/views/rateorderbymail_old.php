<div class="columns-container">
    <div class="container" id="columns">
        <ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item active">Rate Order By Mail</li>
		</ol>
        <div class="row mt-10">
            <div class="center_column col-xs-12 col-sm-12">
					<?php
						foreach($order_summary_mail_data_arr as $order_summary_mail_data){
						$order_details='';
							$order_details.='<table>';
							$order_details.='<tr>';
							$order_details.='<td>';
							$order_details.="<img src='".base_url().$order_summary_mail_data['thumbnail']."'>";
							
							$order_details.='</td>';
							$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.=$order_summary_mail_data['product_name'];
							$order_details.='</p></td>';
							$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.=$order_summary_mail_data['order_id'];
							$order_details.='</p></td>';
							
							
							$get_delivered_time_of_order_item=$controller->get_delivered_time_of_order_item($order_summary_mail_data['order_id'],$order_summary_mail_data["order_item_id"]);
							
							
							$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
							foreach($get_delivered_time_of_order_item as $delivered_time_of_order_item){
								$order_details.=date("D j M,Y",strtotime($delivered_time_of_order_item["order_delivered_date"]))." ".$delivered_time_of_order_item["order_delivered_time"];
							}
							$order_details.='</p></td>';
							
							
							$order_details.='</tr>';
							
							
							$order_details.='</table>';
						}
						echo $order_details;
					?>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
		
		
		
		<div class="row">
            <div class="center_column col-xs-12 col-sm-12 text-center">
				<h2>Your feedback already recorded</h2>
			</div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
		
		
    </div>
</div>
