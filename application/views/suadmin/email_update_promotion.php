<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Flamingo Admin Approval</title>
<link rel="shortcut icon" href="<?php echo base_url();?>assets/pictures/images/favicon/VS_favicon.ico">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/font-awesome.min.css" />
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
</head>
<body>
    <div align="center" id="alerting"></div>
<script> 
window.load=apply_to_promotions();
    function apply_to_promotions(){
        var obj=document.getElementById('alerting');
        var actions ='<?php echo $action ?>';
        var promo_id ='<?php echo $promo_id  ?>';
        var temp_email_num='<?php echo $temp_email_num?>';
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {
                                           
                    obj.innerHTML='<i class="fa fa-refresh fa-spin fa-5x" aria-hidden="true"></i>'
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if(xhr.responseText==1){
                           obj.innerHTML='<i class="fa fa-thumbs-up fa-5x" aria-hidden="true"></i><br><br>Success<br><br>You can close the browser'
                           
                           }
                           if(xhr.responseText==0){
                            obj.innerHTML='<i class="fa fa-times fa-3x" aria-hidden="true"></i><br><br>A Previous action has been taken on this request'
                            
                           }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>suadmin/Su_admin/update_promotion_request", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('actions='+actions+'&promo_id='+promo_id+'&temp_email_num='+temp_email_num);
                }   
}
</script>   
</body>
</html>