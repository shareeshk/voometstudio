<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />

<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>

<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<style>
button.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 5px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
}

button.accordion.active, button.accordion:hover {
    background-color: #ddd;
}

button.accordion:after {
    content: 'Expand \02795';
    font-size: 13px;
    color: #777;
    margin-left: 5px;
}

button.accordion.active:after {
    content: "Collapse \2796";
}

div.panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: 0.6s ease-in-out;
    opacity: 0;
}

div.panel.show {
    opacity: 1;
    max-height: 500px;
}

</style>
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   
   var table = $('#table_inventory').DataTable({
	   "pageLength": 10,
       'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': ['nosort']
        }],
        "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>suadmin/Su_admin/all_inventory_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#table_inventory_processing").css("display","none");
            }
          },
          
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'1%',
         'className': 'dt-body-center'
      }],
      'order': [0, 'desc']
   });
}); 

function editInventoryFun(inventory_id,product_id){
	location.href="<?php echo base_url()?>suadmin/Su_admin/edit_inventory_form/"+inventory_id+"/"+product_id;

}

</script> 

<div class="container-fluid">
    <div class="col-sm-12 ">
        <b>All Inventories</b><hr>
        <table id="table_inventory" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead bgcolor="#d9edf7">
            <tr>
				<!--
					<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
				-->
				<th>Image</th>
				<th>SKU Details</th>
				<th>Details</th>
				<th>Action</th>		
			</tr>
        </thead>
        </table>
    </div>
    <div class="col-sm-12 " >
    </div>         
</div>



