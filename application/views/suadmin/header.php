<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Flamingo Admin Approval</title>
<link rel="shortcut icon" href="<?php echo base_url();?>assets/pictures/images/favicon/VS_favicon.ico">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/font-awesome.min.css" />
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

<link href="<?php echo base_url();?>assets/css/sweetalert2.min.css" rel="stylesheet" /> 
<script src="<?php echo base_url();?>assets/js/sweetalert2.min.js"></script>

<style>
.badge
{
	cursor: pointer;
	width: 18px;
	text-align: center;
	background-color: red;
	border-radius: 0;
	font-size: 10px;
	margin-top: 2px;
	position: absolute;
	color: #fff;
	margin-left: 4px;
}

.change_to_log{
    
    font-size:0.8em;
    font-style:italic;
}
.change_head{
    color:ff0000;
}
.change_data{
    color:#8080ff;
    font-style: oblique;
}

#log_data  td:nth-child(3) { font-size:0.8em;
    font-style:italic; }
#log_data  td:nth-child(4) { font-size:0.8em;
    font-style:italic; }
    
#log_data  td:nth-child(5) { font-size:0.8em;
    font-style:italic; }

    
 .log_dates{
     background-color:#e6ffe6;    
     
 }   
    
.badge-yellow {
    background-color: #f2994b !important;
} 
    
    
</style>
<script>
<?php if($module_type=="promotions"){ ?>
$(document).ready(function(){
	get_all_num_data_of_all_promotion();
});
<?php } ?>
function get_all_num_data_of_all_promotion(){
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {
                                           
                    var listElems=document.getElementsByClassName('badge-yellow');
                    for(var i=0;i<listElems.length;i++){
                        listElems[i].innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
                    }
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            var all_nums=JSON.parse(xhr.responseText)
                            document.getElementById('pending_promotion_num').innerHTML=all_nums['pending_promotion_num'];
                            
                        }else{
                            var listElems=document.getElementsByClassName('badge-yellow');
                            for(var i=0;i<listElems.length;i++){
                                listElems[i].innerHTML='<i class="fa fa-exclamation-circle" aria-hidden="true"></i>'
                            }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>suadmin/Su_admin/get_all_num_data_of_all_promotion", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send();
                }   
}

</script>
</head>
<body style="padding-top: 0px;">

<?php

if($this->session->flashdata("notification"))
{
	echo "<script type='text/javascript'> alert(\"".$this->session->flashdata("notification")."\"); </script>";	
}
?>


<!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
			<?php if($module_type=="promotions"){ ?>
			<li class="dropdown" onclick="get_all_num_data_of_all_promotion()"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Promotions <b class="caret"></b></a>
	        <ul class="dropdown-menu">
            <li><a href="<?php echo base_url();?>suadmin/Su_admin/all_pending_approval"> Pending Approval <span  id="pending_promotion_num" class="badge badge-yellow pull-right" ></span></a></li>
            </ul>
	        </li>
			<?php }?>
			<?php if($module_type=="catalog"){ ?>
				<li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Catalog <b class="caret"></b></a>
	        <ul class="dropdown-menu">
            <li><a href="<?php echo base_url();?>suadmin/Su_admin/all_inventories"> Inventories <span  class="badge badge-yellow pull-right" ></span></a></li>
			
			 
			 
			 
            </ul>
	        </li>
			<?php }?>
			<li><a href="<?php echo base_url();?>suadmin/Su_admin/inventory_export"> Export <span  class="badge badge-yellow pull-right" ></span></a></li>
			
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">
              	
              	<?php 
              	$name=$this->session->userdata("email");
				$parts = explode("@", $name);
				$username = $parts[0];
              	echo $username;
              	?><b class="caret"></b></a>
              	<ul class="dropdown-menu">
					<li><a href='<?php echo base_url();?>admin/Adminout'>Logout</a></li>
		        </ul>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <!-- Main component for a primary marketing message or call to action -->

</div>
</nav>
</body>
</html>

 