<?php if(!empty($promo_data)){
    foreach ($promo_data as $data){
        $promo_id=$data['id'];
        $get_deal_value=$data['to_get'];
        $buy_deal_value=$data['to_buy'];
        $get_deal_type=$data['get_type'];
        $buy_deal_type=$data['buy_type'];
        $promo_type=$data['promo_type'];
        $quote_selector=$data['quote_selector'];
        $promo_uid=$data['promo_uid'];
        $promo_name=$data['promo_name'];
        $promo_quote=htmlspecialchars_decode($data['promo_quote']);
        $promo_start_datev=$data['promo_start_date'];
        $promo_end_datev=$data['promo_end_date'];
        $promo_end_date="";
        $approval_sent=$data['approval_sent'];
        $promo_approval=$data['promo_approval'];
        $promo_start_date="";
        $applied_at_invoice=$data['applied_at_invoice'];
        $temp_email_num=$data['temp_email_num'];
        
        if($data['promo_start_date']!=""){
            $promo_start_date=date("D,j M Y H:i", strtotime($data['promo_start_date']));
        }
        if($data['promo_end_date']!=""){
            $promo_end_date=date("D,j M Y H:i", strtotime($data['promo_end_date']));
        }
        
        $approval_sent=$data['approval_sent'];
        if($approval_sent==0){
            $promo_active_status=" Not Sent ";
        }
        else{
            $promo_active_status=" Sent For Approval ";
        }
        $promo_to_get=$data['to_get'];
        $get_type=$data['get_type'];
    }
        $quote_selector_value=$controller->get_quote_selector_value($quote_selector);
        
        if (stripos($quote_selector_value,'same') !== false) {
            $qualify_for_same=1;
        }
        else{
            $qualify_for_same=0;
        }
        if (stripos($quote_selector_value,'different') !== false) {
            $qualify_for_different=1;
        }
        else{
            $qualify_for_different=0;
        }
        
        if (stripos($quote_selector_value,'tiered') !== false) {
            $qualify_for_tier=1;
        }
        else{
            $qualify_for_tier=0;
        }
        if($qualify_for_tier==1){
          $promo_quote_arr=explode('<br>',$promo_quote);
            $promo_quotes=""; 
            $i=0;
            array_pop($promo_quote_arr); 
            foreach($promo_quote_arr as $arr){
                $promo_quotes.='<li id="'.$i.'_listNUM">'.$arr.'</li>';
                $i++;
            }
            
        }

}
else{
    redirect(base_url().'admin/Promotions/promotion_not_found');
}
?>
<?php 
if($this->session->flashdata("notification")){
    
} 
?>
<div class="container">
<div class="row">
     <div class="col-sm-10 pull-left"><h2>Category : <?php echo $controller->get_promo_type_name($promo_type) ?> | Promo-id #<?php echo $promo_id ?></h2>
    </div>
<div class="col-sm-12">
    
   
    <div class="panel panel-default">
          <div class="panel-heading">Deal Name</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td>Promotion Status:</td>
                            <td><i class="lead" id="promo_active_status"><?php echo $promo_active_status?></i></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Deal Name:</td>
                            <td><i class="lead" id="disp_deal_name_edit_container"><?php echo $promo_name?></i>  &nbsp; </td>
                        </tr>
                        <tr>
                            <td>Deal Quote:</td>
                            <td><i id="disp_deal_quote_edit_container" class="lead"><?php if($qualify_for_tier!=1) {
                                echo $promo_quote;
                                }else{
                                    echo $promo_quotes;
                                }?></i>  &nbsp; </td>
                        </tr>
                        <tr>
                            <td>Start Date:</td>
                            <td><i id="disp_deal_start_date_edit_container" class="lead"><?php echo $promo_start_date?></i></td>
                        </tr>
                        <tr>
                            <td>End Date:</td>
                            <td><i id="disp_deal_end_date_edit_container" class="lead"> <?php echo $promo_end_date?></i></td>
                        </tr>
                    </table>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-3 text-right">
                    <button promo_id="<?php echo $promo_uid ?>" actions="accept" class="btn btn-lg btn-success" onclick="apply_to_promotions(this)">Approve</button>
                    </div> 
                    <div class="col-sm-3 text-center">
                    <button promo_id="<?php echo $promo_uid ?>"  actions="hold"  class="btn btn-lg btn-warning" onclick="apply_to_promotions(this)">Hold</button>
                    </div> 
                    <div class="col-sm-3 text-left">
                    <button promo_id="<?php echo $promo_uid ?>"  actions="reject"  class="btn btn-lg btn-danger" onclick="apply_to_promotions(this)">Reject</button>
                    </div> 
                </div>
                
              </div>
   </div>
 </div>
 </div>
</div>
<script> 
    function apply_to_promotions(obj){
        var actions =obj.getAttribute('actions');
        var promo_id =obj.getAttribute('promo_id');
        var temp_email_num='<?php echo $temp_email_num?>';
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {
                                           
                    obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if(xhr.responseText==1){
                           obj.innerHTML='Updated <i class="fa fa-thumbs-up" aria-hidden="true"></i>'
                           setTimeout(function(){ window.location.href="<?php echo base_url()?>suadmin/Su_admin/all_pending_approval" }, 1000);
                           }
                           if(xhr.responseText==0){
                            alert('A Previous action has been taken on this request on this approval')
                           setTimeout(function(){ window.location.href="<?php echo base_url()?>suadmin/Su_admin/all_pending_approval" }, 1000); 
                           }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>suadmin/Su_admin/update_promotion_request", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('actions='+actions+'&promo_id='+promo_id+'&temp_email_num='+temp_email_num);
                }   
}
</script>
