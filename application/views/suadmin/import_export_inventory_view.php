<html>
<head>
    <meta charset="utf-8">
    <title>Import and Export</title>
    <link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
    
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
    <style>
        .wizard-card{
            box-shadow:none;
        }
        .form-group{
            margin-top:15px;
        }

    </style>
</head>
<body>
<div
        class="animsition"
        data-animsition-in-class="fade-in"
        data-animsition-in-duration="500"
        data-animsition-out-class="fade-out"
        data-animsition-out-duration="400"
>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            <div class="col-md-6">

                <div class="wizard-card" data-color="green" id="wizardProfile">

                <div class="float-right">
            <?php
            if(isset($import_success)){
                if($import_success==1){
                    ?>
                    <h6><?php echo $import_success_msg; ?></h6>
                    <?php
                }
            }
            ?>

        </div>

        <h4>Export Inventory <small></small></h4>

        <form  method="post" action="#" id="export_inventory" >
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group label-floating">
                        <label class="control-label">-Select Parent Category-</label>
                        <select name="parent_category" id="parent_category" class="form-control" onchange="showAvailableCategories(this)">
                            <option value=""></option>
                            <?php foreach ($parent_category as $parent_category_value) {  ?>
                                <option value="<?php echo $parent_category_value->pcat_id; ?>" <?php echo ($parent_category_value->pcat_id==$pcat_id)? "selected":''; ?> ><?php echo $parent_category_value->pcat_name; ?></option>
                            <?php } ?>
                            <option value="0">--None--</option>

                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group label-floating">
                        <label class="control-label">-Select Category-</label>
                        <select name="categories" id="categories" class="form-control" onchange="showAvailableSubCategories(this)">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group label-floating">
                        <label class="control-label">-Select Sub Category-</label>
                        <select name="subcategories" id="subcategories" class="form-control" onchange="showAvailableBrands(this)">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group label-floating">
                        <label class="control-label">-Select Brands-</label>
                        <select name="brands" id="brands" class="form-control" onchange="showAvailableProducts(this)">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group label-floating">
                        <label class="control-label">-Select Products-</label>

                        <select name="products" id="products" class="form-control" >
                            <option value=""></option>
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" value="1" name="export_inventory">
              <button id="export_inventory" class="btn btn-info " type="submit">Export Inventory</button>
            <!--<a href="javascript:void(0);" class="btn btn-info btn-xs" onclick="formToggle('importFrm');"><i class="plus"></i> Import Inventory</a>-->
        </form>
        <!---import uantity ---->

        <!-- Import link -->
        <div class="col-md-12 head">
<hr/>
        </div>
<?php
/*
?>
        <h4>Import Inventory <small>Update values with respect to SKUs</small></h4>
        <!-- File upload form -->
       

	   <div class="col-md-12 well" id="importFrm" style="">
            <form action="<?php echo base_url('/admin/Catalogue/inventory_import'); ?>" method="post" enctype="multipart/form-data">
                <input type="file" name="file" required/>
                <input type="hidden" value="1" name="import_inventory">

                <input type="submit" class="btn btn-primary" name="importSubmit" value="IMPORT">
            </form>
        </div>
		<?php
*/
?>
        <!---import uantity ---->

                </div>
            </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
            $(document).ready(function (){
                pcat_id=$('#pcat_id').val();
                cat_id=$('#cat_id').val();
                subcat_id=$('#subcat_id').val();
                brand_id=$('#brand_id').val();
                product_id=$('#product_id').val();
                //inventory_id=$('#inventory_id').val();


            });

    function showAvailableProducts(obj){

        if(obj.value!="" && obj.value!="None"){
            brand_id=obj.value;
            $.ajax({
                url:"<?php echo base_url(); ?>suadmin/Su_admin/show_available_products",
                type:"post",
                data:"brand_id="+brand_id+"&active=1",
                success:function(data){
                    if(data!=0){
                        $("#products").html(data);
                    }
                    else{
                        $("#products").html('<option value=""></option>');
                    }
                }
            });
        }

    }

    function showAvailableBrands(obj){

        if(obj.value!="" && obj.value!="None"){
            subcat_id=obj.value;
            $.ajax({
                url:"<?php echo base_url(); ?>suadmin/Su_admin/show_available_brands",
                type:"post",
                data:"subcat_id="+subcat_id+"&active=1",
                success:function(data){
                    if(data!=0){
                        $("#brands").html(data);
                    }
                    else{
                        $("#brands").html('<option value=""></option>');
                    }
                }
            });
        }

    }
    function showAvailableSubCategories(obj){

        if(obj.value!="" && obj.value!="None"){
            cat_id=obj.value;
            $.ajax({
                url:"<?php echo base_url(); ?>suadmin/Su_admin/show_available_subcategories",
                type:"post",
                data:"cat_id="+cat_id+"&active=1",
                success:function(data){
                    if(data!=0){
                        $("#subcategories").html(data);
                    }
                    else{
                        $("#subcategories").html('<option value=""></option>')
                        $("#brands").html('<option value=""></option>')

                    }
                }
            });
        }

    }

    function showAvailableCategories(obj){
        if(obj.value!="" && obj.value!="None"){
            pcat_id=obj.value;
            $.ajax({
                url:"<?php echo base_url()?>suadmin/Su_admin/show_available_categories",
                type:"post",
                data:"pcat_id="+pcat_id+"&active=1",
                success:function(data){
                    if(data!=0){
                        $("#categories").html(data);
                    }
                    else{
                        $("#categories").html('<option value=""></option>')
                        $("#brands").html('<option value=""></option>')

                    }
                }
            });
        }

    }


            /* Export inventory */

            function export_inventory(table){

                $.ajax({
                    url:"<?php echo base_url()?>suadmin/Su_admin/export_inventory_all",
                    type:"post",
                    data:"table="+table+"&active=0",
                    success:function(data){
                        if(data==true){

                        } else{
                            swal("Error", "Error in exporting inventories", "error");
                        }
                    }
                });
            }
            /* Export inventory */
            /* Import inventory */
            function formToggle(ID){
                var element = document.getElementById(ID);
                if(element.style.display === "none"){
                    element.style.display = "block";
                }else{
                    element.style.display = "none";
                }
            }
    </script>
</body>
</html>