<!doctype html>
<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />	
<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 
<style type="text/css">
 .wizard-navigation {
        -webkit-transform: translateZ(0);
		-webkit-backface-visibility: hidden;
    }
.small_color_box{
	width: 30px;
	height: 30px;
	border: 1px solid #ccc;
	margin-top: 1px;
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}

#edit_wizardPictureSmall_picname{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname2{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname2{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname3{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname3{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname4{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname4{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname5{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname5{
	word-wrap: break-word;
}
textarea.form-control {
    height:38px;
}
.CamListDiv {
    height: 450px;
    float: left;
    overflow: scroll;
    overflow-x:hidden;

}
.well{
	width:100%;

}
.moving-tab{
	width:347.33px !important;
}
#edit_stock_err,#edit_low_stock_err{
	color:#ff0000;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<?php 
	if(isset($id)){
		
		$inventory_id=$id;
		$cat_id=$cat_id;
		$subcat_id=$subcat_id;
		$brand_id=$brand_id;
		$product_id=$product_id;
	}else{
		header("Location:".base_url()."suadmin/Su_admin/all_inventories/");	
	}

?>
<script type="text/javascript">
$(document).ready(function (){
$('#back').on('click',function(){	
		var form = document.getElementById('back_to_inventory');
		form.action='<?php echo base_url(); ?>suadmin/Su_admin/all_inventories';
		form.submit();
	});
});		
</script>
<script type="text/javascript">
function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

function edit_filter_details_form_valid()
{
	var err = 0;	
	var filter=document.getElementsByName("edit_filter[]");

	if(err==1)
	{
		//$('#edit_validation_error').show();
		return false;
		
	}else{
				//$('#edit_validation_error').hide();
				var data = $("#highlight").Editor("getText").trim();
				$("#highlight").val(data);
		
				var form_status = $('<div class="form_status"></div>');	
				var form_edit = document.forms.namedItem("edit_inventory");
				var ajaxData = new FormData(form_edit);	
		
				var form = $('#edit_inventory');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {				
				$.ajax({
				url: '<?php echo base_url()."suadmin/Su_admin/edit_inventory/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false,  
				
			}).done(function(data)
			  {
				  
				if(data==true)
				{
					swal({
						title:"Success", 
						text:"Inventory is successfully Updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						//location.reload();
						location.href="<?php echo base_url();?>suadmin/Su_admin/all_inventories";
					});
				}
				else
				{
					swal({
						title:"Error", 
						text:"Inventory not yet Updated!", 
						type: "error",
						allowOutsideClick: false
					}).then(function () {
						location.reload();
					});	
				}
		return true;
	});
	}
}]);
		return true;
	}
}
var edit_val;


</script>
<script type="text/javascript">
		

	function edit_display_color(color_value){
		colors_arr=color_value.split(":");
		color_code=colors_arr[1];
		color_name=colors_arr[0];
		if(color_value!=""){
		$('#edit_small_color_box').css({"background-color": color_code});
		}else{
			$('#edit_small_color_box').css({"background-color": ""});
		}
		$('#edit_small_color_box').attr('title',color_name);
	}

	function edit_filter_display_color(color_value){
		colors_arr=color_value.split(":");
		color_code=colors_arr[1];
		color_name=colors_arr[0];
		$('#edit_filter_small_color_box').css({"background-color": color_code});
		$('#edit_filter_small_color_box').attr('title',color_name);
	}
</script>	

<div class="container">
<div class="row">			
<div class="col-md-12">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
<form action="" name="edit_inventory" id="edit_inventory" method="post" enctype="multipart/form-data">
	<?php
		
		foreach($get_all_specification_group as $get_all_specification_group_value)
		{
			$sp=$controller->get_all_specification($get_all_specification_group_value->specification_group_id);
			?>
			<script type="text/javascript">
				var $validator = $('.wizard-card form').validate({
					rules: {
						
			
						edit_base_price: {
							required: true,
		     
						},
						edit_selling_price: {
							required: true,
		     
						},
						edit_purchased_price: {
							required: true,
						},
						edit_tax: {
							required: true,
		      
						},
						edit_moq: {
							required: true,
		     
						},
						edit_max_oq: {
							required: true,
		      
						},
						edit_low_stock: {
							required: true,
		     
						},
						edit_cutoff_stock: {
							required: true,
		      
						},
						/*'edit_logistics_parcel_category_id[]': {
							required: true,
		      
						},*/
						
						
					
						edit_stock: {
							required: true,
		      
						},
						edit_product_status: {
							required: true,
		      
						},
						edit_product_status_view: {
							required: true,
		      
						},

			<?php
			
			foreach($filterbox as $filterbox_value)
			{
				//$filter=$controller->get_all_filter($filterbox_value->filterbox_id);
				//if(strtolower($filterbox_value->filterbox_name)!="color"){
					
				//foreach($filter as $filter_value)
				//{
					echo "'".'edit_filter['.$filterbox_value->filterbox_id.']'."'".':{';
					echo 'required: true },';
				
				//}
				//}
			}
			?>
			
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
			</script>
		<?php } ?>
		<div class="wizard-header">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<h5 class="wizard-title text-left"> SKU ID : <?php echo $sku_id; ?></h5>
						<h5 class="wizard-title text-left"> HSN Code : <?php echo $hsncode; ?></h5>
					</div>
					<div class="col-md-5">
						<h5 class="wizard-title text-center"> 
							Name : <?php echo $product_name; ?>
							<?php
								$attributes_arr=[];
								if($attribute_1_value_display!=""){
									if(preg_match("/:/",$attribute_1_value_display)){
										$attributes_arr[]=explode(":",$attribute_1_value_display)[0];
									}
									else{
										$attributes_arr[]=$attribute_1_value_display;
									}
								}
								if($attribute_2_value_display!=""){
									$attributes_arr[]=$attribute_2_value_display;
								}
								if($attribute_3_value_display!=""){
									$attributes_arr[]=$attribute_3_value_display;
								}
								if($attribute_4_value_display!=""){
									$attributes_arr[]=$attribute_4_value_display;
								}
								echo implode(" - ",$attributes_arr);
							?>
						</h5>
					</div>
					<div class="col-md-3 text-right">
						<img src="<?php echo base_url().$inventory_data_arr["thumbnail"]?>" alt="Image Not Found" width="100"/>
					</div>
				</div>
			</div>
		</div>  

		<div class="wizard-navigation">
			<ul>
				<li><a href="#edit_step-11" data-toggle="tab">General</a></li>
				<li><a href="#edit_step-71" data-toggle="tab">Highlights</a></li>
				<li><a href="#edit_step-61" data-toggle="tab">Images</a></li>
			</ul>
		</div>
		
			<input name="edit_product_id" type="hidden" id="edit_product_id" value="<?php echo $product_id;?>">
			<input name="edit_inventory_id" id="edit_inventory_id" type="hidden"  value="<?php echo $inventory_id;?>"/>
			<input name="previous_largeimage1" id="previous_largeimage1" type="hidden" value="<?php echo $inventory_data_arr["largeimage"];?>"/>
			<input name="previous_image" id="previous_image" type="hidden" value="<?php echo $inventory_data_arr["common_image"];?>"/>
			<input name="previous_image1" id="previous_image1" type="hidden" value="<?php echo $inventory_data_arr["image"];?>"/>
			<input name="previous_thumbnail1" id="previous_thumbnail1" type="hidden" value="<?php echo $inventory_data_arr["thumbnail"];?>"/>
			
			<input name="previous_largeimage2" id="previous_largeimage2" type="hidden" value="<?php echo $inventory_data_arr["largeimage2"];?>"/>
			<input name="previous_image2" id="previous_image2" type="hidden" value="<?php echo $inventory_data_arr["image2"];?>"/>
			<input name="previous_thumbnail2" id="previous_thumbnail2" type="hidden" value="<?php echo $inventory_data_arr["thumbnail2"];?>"/>
			
			<input name="previous_largeimage3" id="previous_largeimage3" type="hidden" value="<?php echo $inventory_data_arr["largeimage3"];?>"/>
			<input name="previous_image3" id="previous_image3" type="hidden" value="<?php echo $inventory_data_arr["image3"];?>"/>
			<input name="previous_thumbnail3" id="previous_thumbnail3" type="hidden" value="<?php echo $inventory_data_arr["thumbnail3"];?>"/>
			
			<input name="previous_largeimage4" id="previous_largeimage4" type="hidden" value="<?php echo $inventory_data_arr["largeimage4"];?>"/>
			<input name="previous_image4" id="previous_image4" type="hidden" value="<?php echo $inventory_data_arr["image4"];?>"/>
			<input name="previous_thumbnail4" id="previous_thumbnail4" type="hidden" value="<?php echo $inventory_data_arr["thumbnail4"];?>"/>
			
			<input name="previous_largeimage5" id="previous_largeimage5" type="hidden" value="<?php echo $inventory_data_arr["largeimage5"];?>"/>
			<input name="previous_image5" id="previous_image5" type="hidden" value="<?php echo $inventory_data_arr["image5"];?>"/>
			<input name="previous_thumbnail5" id="previous_thumbnail5" type="hidden" value="<?php echo $inventory_data_arr["thumbnail5"];?>"/>
		
			<input name="previous_largeimage1_unlink" id="previous_largeimage1_unlink" type="hidden" value="<?php echo $inventory_data_arr["largeimage"];?>"/>
			<input name="previous_image1_unlink" id="previous_image1_unlink" type="hidden" value="<?php echo $inventory_data_arr["image"];?>"/>
			<input name="previous_thumbnail1_unlink" id="previous_thumbnail1_unlink" type="hidden" value="<?php echo $inventory_data_arr["thumbnail"];?>"/>
			
			<input name="previous_largeimage2_unlink" id="previous_largeimage2_unlink" type="hidden" value="<?php echo $inventory_data_arr["largeimage2"];?>"/>
			<input name="previous_image2_unlink" id="previous_image2_unlink" type="hidden" value="<?php echo $inventory_data_arr["image2"];?>"/>
			<input name="previous_thumbnail2_unlink" id="previous_thumbnail2_unlink" type="hidden" value="<?php echo $inventory_data_arr["thumbnail2"];?>"/>
			
			<input name="previous_largeimage3_unlink" id="previous_largeimage3_unlink" type="hidden" value="<?php echo $inventory_data_arr["largeimage3"];?>"/>
			<input name="previous_image3_unlink" id="previous_image3_unlink" type="hidden" value="<?php echo $inventory_data_arr["image3"];?>"/>
			<input name="previous_thumbnail3_unlink" id="previous_thumbnail3_unlink" type="hidden" value="<?php echo $inventory_data_arr["thumbnail3"];?>"/>
			
			<input name="previous_largeimage4_unlink" id="previous_largeimage4_unlink" type="hidden" value="<?php echo $inventory_data_arr["largeimage4"];?>"/>
			<input name="previous_image4_unlink" id="previous_image4_unlink" type="hidden" value="<?php echo $inventory_data_arr["image4"];?>"/>
			<input name="previous_thumbnail4_unlink" id="previous_thumbnail4_unlink" type="hidden" value="<?php echo $inventory_data_arr["thumbnail4"];?>"/>
			
			<input name="previous_largeimage5_unlink" id="previous_largeimage5_unlink" type="hidden" value="<?php echo $inventory_data_arr["largeimage5"];?>"/>
			<input name="previous_image5_unlink" id="previous_image5_unlink" type="hidden" value="<?php echo $inventory_data_arr["image5"];?>"/>
			<input name="previous_thumbnail5_unlink" id="previous_thumbnail5_unlink" type="hidden" value="<?php echo $inventory_data_arr["thumbnail5"];?>"/>
			
			
	<div class="tab-content">
	<div class="tab-pane" id="edit_step-11">
		<div class="row">
			<div class="col-md-12">
			    <h4 class="info-text"> Product and Stock Information</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 col-md-offset-1">
				<div class="form-group">
					<label class="control-label">-Product Status-
					</label>
					<select name="edit_product_status_view" id="edit_product_status_view" class="form-control search_select">
						<option value="" selected></option>
						<option value="1" <?php if($inventory_data_arr["product_status_view"]=="1"){echo "selected";}?>>Active</option>
						<option value="0" <?php if($inventory_data_arr["product_status_view"]=="0"){echo "selected";}?>>Inactive</option>
			
					</select>
				</div>
			</div>
			
			<?php
				$inv_discount_data = $controller->get_default_discount_for_inv($inventory_data_arr["inventory_id"], $inventory_data_arr["selling_price"], $inventory_data_arr["product_id"]);
				if(empty($inv_discount_data)){
					$edit_selling_price_after_promotion=0;
					$edit_promo_discount=0;
				}
				else{
					$edit_selling_price_after_promotion=round($inventory_data_arr["selling_price"]*(1-($inv_discount_data["discount"]/100)),2);
					$edit_promo_discount=$inv_discount_data["discount"];
				}
			?>
			
			<div class="col-md-5">
				<div class="form-group">
					<label class="control-label">Web Selling Price (<?php echo curr_sym;?>) {MRP}
					</label>
					<input name="edit_selling_price_after_promotion" id="edit_selling_price_after_promotion" type="text" class="form-control"   value="<?php echo $edit_selling_price_after_promotion; ?>" onKeyPress="return isNumber(event)" onkeyup="calculateSelling_priceFun()"/>
				</div>
			</div>
			
			
			
		</div>
		
		
		
		
		<div class="row">
			
				<div class="col-md-5 col-md-offset-1"> 
					<div class="form-group">	
						<label class="control-label">Current Stock count
						</label>
						<input class="form-control" name="edit_stock" id="edit_stock" type="number"  value="<?php echo $inventory_data_arr["stock"];?>"   onkeyup="assign_filter_values_stock();stock_count_validation()"/>
						<small id="edit_stock_err"></small>
						<script>
						$(document).ready(function(){
							assign_filter_values_stock();
						});
						</script>
					</div>
				</div>	
				
					
					<div class="col-md-5"> 
				<div class="form-group">
					<label class="control-label">Company Cost Price (<?php echo curr_sym;?>)
					</label>
					
					<input name="edit_purchased_price" id="edit_purchased_price" type="text" class="form-control"   value="<?php echo $inventory_data_arr["purchased_price"];?>" onKeyPress="return isNumber(event)" readonly/>
				</div>
				<script>
				$(document).ready(function(){
					assign_filter_values('<?php echo  $inventory_data_arr["purchased_price"];?>','price');
				});
				</script>
			</div>
			
			
			
		</div>
		
		
		
		
		<div class="row">
		
			
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group">
					<label class="control-label">Pre-GST price {company base price} (<?php echo curr_sym;?>)
					</label>
					<input name="edit_base_price" id="edit_base_price" type="text" class="form-control"   value="<?php echo $inventory_data_arr["base_price"];?>" onKeyPress="return isNumber(event)" readonly/>
				</div>
				<script>
				//$(document).ready(function(){
					//assign_filter_values('<?php echo  $inventory_data_arr["selling_price"];?>','price');
				//});
				</script>
			</div>
			
			
			
			
			
		
			<div class="col-md-5"> 
				<div class="form-group">
					<label class="control-label">
					Tax Percent (GST %)</label>
					<input name="edit_tax" id="edit_tax" type="text" class="form-control"   value="<?php echo $inventory_data_arr["tax_percent"];?>" onKeyPress="return isNumber(event)"  readonly/>
				</div>
			</div>
			
		</div>
		
		
		
		
		
		<!------------------------------------>
		
		<div class="row">
			
			
			
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group">
					<label class="control-label">Promo Discount (%)</label>
					
					
					
					<input name="edit_promo_discount" id="edit_promo_discount" type="text" class="form-control"   value="<?php echo $edit_promo_discount;?>" onKeyPress="return isNumber(event)" readonly/>
				</div>
			</div>
			
			
			
			<div class="col-md-5"> 
				<div class="form-group">
					<label class="control-label" id="edit_selling_price_div">Web Selling Price (<?php echo curr_sym;?>) {MRP - without any discount applied}
					</label>
					<input name="edit_selling_price" id="edit_selling_price" type="text" class="form-control"   value="<?php echo $inventory_data_arr["selling_price"];?>" onKeyPress="return isNumber(event)" readonly/>
				</div>
				<script>
				$(document).ready(function(){
					assign_filter_values('<?php echo  $inventory_data_arr["selling_price"];?>','price');
				});
				</script>
			</div>
			
			
			
			
		</div>
		
		
		<!------------------------------------>
		
		
		
		
		
		
		
		
		
		<?php
		/*
		?>
		<div class="row">
			
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group">
					<label class="control-label">Quantity
					</label>
					<input name="edit_mrp_quantity" id="edit_mrp_quantity" type="text" class="form-control"   value="<?php echo $inventory_data_arr["mrp_quantity"];?>"/>
				</div>
				<script>
				$(document).ready(function(){
					assign_filter_values('<?php echo  $inventory_data_arr["selling_price"];?>','price');
				});
				</script>
			</div>
		</div>
		<?php
		*/
		?>
		<hr>
		<div class="row">
			<div class="col-md-3 col-md-offset-1"> 
			
				<div class="form-group">
					<label class="control-label">Enter Minimum Order Quantity
					</label>
					<input name="edit_moq" id="edit_moq" type="number" class="form-control" min="1"   value="<?php echo $inventory_data_arr["moq"];?>" readonly/>
				</div>
			</div>		  
			<div class="col-md-3"> 	
				<div class="form-group">
				<label class="control-label">Enter Maximum Order Quantity
				</label>
				<input name="edit_max_oq" id="edit_max_oq" type="number" class="form-control" min="1"   value="<?php echo $inventory_data_arr["max_oq"];?>"  readonly/>
				</div>
			</div>
			
			<div class="col-md-4"> 
					<div class="form-group">
						<label class="control-label">-Select Stock Status-
						</label>
						<select name="edit_product_status" id="edit_product_status" class="form-control search_select">
							<option value="" selected></option>
							<option value="In Stock" <?php if($inventory_data_arr["product_status"]=="In Stock"){echo "selected";}?>>In Stock</option>
							<option value="Out Of Stock" <?php if($inventory_data_arr["product_status"]=="Out Of Stock"){echo "selected";}?>>Out Of Stock</option>
							<option value="Imported Stock" <?php if($inventory_data_arr["product_status"]=="Imported Stock"){echo "selected";}?>>Imported Stock</option>
						</select>
					</div>
				</div>
				
				
		</div>	
		
			
			<div class="row">
				<div class="col-md-5 col-md-offset-1"> 
					<div class="form-group">
						<label class="control-label">Low Stock Count (Warning Qty)
						</label>    
						<input class="form-control" name="edit_low_stock" id="edit_low_stock" type="number"  value="<?php echo $inventory_data_arr["low_stock"];?>"  onkeyup="low_stock_count_validation();"/>
						<small id="edit_low_stock_err"></small>
					</div>
				</div>
				<div class="col-md-5"> 
					<div class="form-group">
						<label class="control-label">Cutoff Stock Count
						</label>     
						<input class="form-control" name="edit_cutoff_stock" id="edit_cutoff_stock" type="number"   value="<?php echo $inventory_data_arr["cutoff_stock"];?>"   onkeyup="assign_filter_values_stock();stock_count_validation();low_stock_count_validation();"/>
					</div>
				</div>
			</div>
			
			<script>
			$(document).ready(function(){
				assign_filter_values_stock();
			});
		function low_stock_count_validation(){
			low_stock=parseInt($('#edit_low_stock').val());
			cutoff_stock=parseInt($('#edit_cutoff_stock').val());
			
			if(low_stock!=='' && cutoff_stock!==''){
				if(low_stock<cutoff_stock){
					$("#edit_low_stock_err").html('Low stock count should be greater than cutoff stock count');
					$('#edit_cutoff_stock').val('<?php echo $inventory_data_arr["cutoff_stock"];?>');
				}
				else{
					$("#edit_low_stock_err").html('');
				}
			}	
			
			
			if($("#edit_low_stock_err").html()=="" && $("#edit_stock_err").html()==""){
				$("#edit_inventory_next_btn").attr({"disabled":false})
			}
			else{
				$("#edit_inventory_next_btn").attr({"disabled":true})
			}

			
		}
		function stock_count_validation(){
			stock=parseInt($('#edit_stock').val());
			cutoff_stock=parseInt($('#edit_cutoff_stock').val());
			
			
			if(stock!=='' && cutoff_stock!==''){
				if(stock<cutoff_stock){
					$("#edit_stock_err").html('Stock count should be greater than cutoff stock count');
					$('#edit_cutoff_stock').val('<?php echo $inventory_data_arr["cutoff_stock"];?>');
				}
				else{
					$("#edit_stock_err").html('');
				}
			}
			
			
			if($("#edit_low_stock_err").html()=="" && $("#edit_stock_err").html()==""){
				$("#edit_inventory_next_btn").attr({"disabled":false})
			}
			else{
				$("#edit_inventory_next_btn").attr({"disabled":true})
			}



		}
		</script>
			
		
	</div>
	<div class="tab-pane" id="edit_step-71">
	 <div class="row">
				<div class="col-md-12"> 	
						<textarea class="form-control" name="highlight" id="highlight"></textarea>
				</div>
			</div>
			</div>
	
	<?php $selected_val=''; ?>

 


	
	<script>
	
	var typingTimer;                //timer identifier
	var doneTypingInterval = 5000;  //time in ms, 5 second for example
	var $input = $('#edit_selling_price');

	//on keyup, start the countdown
	$input.on('keyup', function () {
	  clearTimeout(typingTimer);
	  val=this.value;
	  typingTimer = setTimeout(doneTyping(val), doneTypingInterval);
	});

	//on keydown, clear the countdown 
	$input.on('keydown', function () {
	  clearTimeout(typingTimer);
	});

	//user is "finished typing," do something
	function doneTyping (val) {
	  if(val!=''){
		  assign_filter_values(val,'price')
	  }
	}
	
	
	function assign_filter_values_stock(){
		
		$("#availability > option").each(function() {
			var attr = $(this).attr('selected');
			if (typeof attr !== typeof undefined && attr !== false) {
				$(this).removeAttr("selected");
			}
	   })
			  
			  
		elem="#availability";
		stock=parseInt($("#edit_stock").val().trim());
		cutoff_stock=parseInt($("#edit_cutoff_stock").val().trim());
		if(stock!="" && cutoff_stock!=""){
			if(stock>cutoff_stock){
				stock_availability="yes";
			}
			else{
				stock_availability="no";
			}
			
			if(stock_availability=="yes"){
				$("#availability > option").each(function(){
					if($(this).val()=="0"){
						//$(this).attr({"selected":true});
						$("#availability").val($(this).val());
						$("#availability").attr({"disabled":true});
						$("#availability").parent('div').removeAttr("class");
						$("#availability").parent('div').addClass("label-floating form-group");
						
						$("input[name='"+$(elem).attr("name")+"'].dynamic_input").val(0);
						
						
					}
				});
			}
			if(stock_availability=="no"){
				$("#availability > option").each(function(){
					
					if($(this).text().toLowerCase()=="include out of stock"){
						$(this).attr({"selected":true});
						$("#availability").val($(this).val());
						
						$("#availability").attr({"disabled":true});
						$("#availability").parent('div').removeAttr("class");
						$("#availability").parent('div').addClass("label-floating form-group");
						
						$("input[name='"+$(elem).attr("name")+"'].dynamic_input").val($(this).val());
						
								
					}
				});
			}
			
		}
		
		
	}
	
	
	function assign_filter_values(data,name,color=''){

		$(".filterbox").each(function(index, elem) {
		   valu = $(elem).val();
		   filterbox_name = $(elem).attr("filterbox_name");
		   id=$(elem).attr('id');
		  
		  //id is elem
		  // alert(filterbox_name);
		   if(name==filterbox_name){
			  //alert(filterbox_name);
			  
			  $("#"+id+" > option").each(function() {
				  if($(this).attr("selected")==true){
					  $(this).removeAttr("selected");
				  }
			  })
				$("#"+id+" > option").each(function() {
					
					fil_text_org=this.value;
					//alert(fil_text_org);
					fil_text=this.text.toLowerCase();
					//alert(fil_text);
					data=data.toLowerCase();
					
					if(color=="color"){
						colors_arr=data.split(":");
						code=colors_arr[1];
						name=colors_arr[0];
						data=name;
					}
					
					//alert(fil_text + ' ' + data);
					
					if(name=="price"){
						if(fil_text!=''){
							fil_text_arr=fil_text.split("-");
							lprice=parseInt(fil_text_arr[0].trim());
							hprice=parseInt(fil_text_arr[1].trim());
	
							if(data>=lprice && data<=hprice){	
								$(this).attr({"selected":true});
								$(elem).val(fil_text_org);
								
								$(elem).attr({"disabled":true});
								$(elem).parent('div').removeAttr("class");
								$(elem).parent('div').addClass("label-floating form-group");
								
								$("input[name='"+$(elem).attr("name")+"'].dynamic_input").val(fil_text_org);
								
							}
						}
					}
					if(name!="price"){
						if(fil_text==data && fil_text_org!=''){
							//alert(fil_text);
							//$(this).attr('selected',"true");
							$(this).attr({"selected":true});
							$(elem).val(fil_text_org);
							
							$(elem).attr({"disabled":true});
							$(elem).parent('div').removeAttr("class");
							$(elem).parent('div').addClass("label-floating form-group");
							
							$("input[name='"+$(elem).attr("name")+"'].dynamic_input").val(fil_text_org);
								
							

							
							
						}
					}
					
				});
		   }
		});

	}
	</script>
	
	<div class="tab-pane" id="edit_step-61">
		
		<!---common--image--->
		<div class="row">
			<div class="col-md-5 col-md-offset-3" id="inv_images_div">
			<div class="col-md-12 text-center"><b>Common (300x366)</b></div>
				<div class="col-md-12">
				<div class="picture-container">
					<div class="picture" id="edit_commonPic">
						<img src="<?php echo base_url().$inventory_data_arr["common_image"]?>" class="picture-src" id="edit_wizardPictureCommon" title=""/>
				
						<input type="file" name="edit_common_image" id="edit_wizard-picture-Common"/>
					</div>
				<h6 id="edit_wizardPictureCommon_picname">Common Image</h6>
				</div>
				</div>	
			</div>
		</div>
		<!---common--image--->
		
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
			
			<button name="add_image_inventory" id="add_image_inventory_btn" type="button" class="btn btn-success" disabled onclick="add_image_inventory_fun()">Add</button>
			
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 col-md-offset-2"  id="inv_images_div_1">	
			<div class="col-md-10 col-md-offset-1"><b>FIRST PAIR</b></div>
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic1">
					<img class="picture-src" id="edit_wizardPictureSmall1" title=""/>
		            <input type="file" name="edit_thumbnail1" id="edit_wizard-picture-small1" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["thumbnail"]?>" alt="Image Not Found" id="previous_thumbnail_view1" name="previous_thumbnail_view1" width="100"/>
					
					
					
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname1">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic1">
					<img class="picture-src" id="edit_wizardPictureBig1" title=""/>
		            <input type="file" name="edit_image1" id="edit_wizard-picture-big1" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["image"]?>" alt="Image Not Found" id="previous_image_view1" name="previous_image_view1" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname1">Big Image (420x512)</h6>
				</div>
				

				
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic1">
					<img class="picture-src" id="edit_wizardPictureLarge1" title=""/>
		            <input type="file" name="edit_largeimage1" id="edit_wizard-picture-large1" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["largeimage"]?>" alt="Image Not Found" id="previous_largeimage_view1" name="previous_largeimage_view1" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname1">Large Image (850x1036)</h6>
				</div>
				

				
			</div>
			
			</div>
			<div class="col-md-5"  id="inv_images_div_2">	
			<div class="col-md-10 col-md-offset-1"><b>SECOND PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(2)"></i></b></div>
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic2">
					<img class="picture-src" id="edit_wizardPictureSmall2" title=""/>
		            <input type="file" name="edit_thumbnail2" id="edit_wizard-picture-small2" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["thumbnail2"]?>" alt="Image Not Found" id="previous_thumbnail_view2" name="previous_thumbnail_view2" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname2">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic2">
					<img class="picture-src" id="edit_wizardPictureBig2" title=""/>
		            <input type="file" name="edit_image2" id="edit_wizard-picture-big2" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["image2"]?>" alt="Image Not Found" id="previous_image_view2" name="previous_image_view2" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname2">Big Image (420x512)</h6>
				</div>
			</div>
			
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic2">
					<img class="picture-src" id="edit_wizardPictureLarge2" title=""/>
		            <input type="file" name="edit_largeimage2" id="edit_wizard-picture-large2" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["largeimage2"]?>" alt="Image Not Found" id="previous_largeimage_view2" name="previous_largeimage_view2" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname2">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 col-md-offset-2" id="inv_images_div_3">
<div class="col-md-10 col-md-offset-1"><b>THIRD PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(3)"></i></b></div>			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic3">
					<img class="picture-src" id="edit_wizardPictureSmall3" title=""/>
		            <input type="file" name="edit_thumbnail3" id="edit_wizard-picture-small3" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["thumbnail3"]?>" alt="Image Not Found" id="previous_thumbnail_view3" name="previous_thumbnail_view3" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname3">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic3">
					<img class="picture-src" id="edit_wizardPictureBig3" title=""/>
		            <input type="file" name="edit_image3" id="edit_wizard-picture-big3" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["image3"]?>" alt="Image Not Found" id="previous_image_view3" name="previous_image_view3" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname3">Big Image (420x512)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic3">
					<img class="picture-src" id="edit_wizardPictureLarge3" title=""/>
		            <input type="file" name="edit_largeimage3" id="edit_wizard-picture-large3" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["largeimage3"]?>" alt="Image Not Found" id="previous_largeimage_view3" name="previous_largeimage_view3" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname3">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
			<div class="col-md-5" id="inv_images_div_4">	
			<div class="col-md-10 col-md-offset-1"><b>FOURTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(4)"></i></b></div>
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic4">
					<img class="picture-src" id="edit_wizardPictureSmall4" title=""/>
		            <input type="file" name="edit_thumbnail4" id="edit_wizard-picture-small4" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["thumbnail4"]?>" alt="Image Not Found" id="previous_thumbnail_view4" name="previous_thumbnail_view4" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname4">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic4">
					<img class="picture-src" id="edit_wizardPictureBig4" title=""/>
		            <input type="file" name="edit_image4" id="edit_wizard-picture-big4" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["image4"]?>" alt="Image Not Found" id="previous_image_view4" name="previous_image_view4" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname4">Big Image (420x512)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic4">
					<img class="picture-src" id="edit_wizardPictureLarge4" title=""/>
		            <input type="file" name="edit_largeimage4" id="edit_wizard-picture-large4" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["largeimage4"]?>" alt="Image Not Found" id="previous_largeimage_view4" name="previous_largeimage_view4" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname4">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 col-md-offset-2" id="inv_images_div_5">
<div class="col-md-10 col-md-offset-1"><b>FIFTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(5)"></i></b></div>			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic5">
					<img class="picture-src" id="edit_wizardPictureSmall5" title=""/>
		            <input type="file" name="edit_thumbnail5" id="edit_wizard-picture-small5" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["thumbnail5"]?>" alt="Image Not Found" id="previous_thumbnail_view5" name="previous_thumbnail_view5" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname5">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic5">
					<img class="picture-src" id="edit_wizardPictureBig5" title=""/>
		            <input type="file" name="edit_image5" id="edit_wizard-picture-big5" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["image5"]?>" alt="Image Not Found" id="previous_image_view5" name="previous_image_view5" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname5">Big Image (420x512)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic5">
					<img class="picture-src" id="edit_wizardPictureLarge5" title=""/>
		            <input type="file" name="edit_largeimage5" id="edit_wizard-picture-large5" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["largeimage5"]?>" alt="Image Not Found" id="previous_largeimage_view5" name="previous_largeimage_view5" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname5">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
			
		</div>
	</div>
	
	</div>
	<div class="wizard-footer">
		<div class="pull-right">
			<input type='button' class='btn btn-next btn-fill btn-success btn-wd' name='next' value='Next' id="edit_inventory_next_btn"/>
			<input type='button' class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='Finish' id="edit_inventory_finish_btn" onClick="edit_filter_details_form_valid()"/>
		</div>
		<div class="pull-left">
			<input type='button' class='btn btn-previous btn-fill btn-default btn-wd' id="previous_button" name='previous' value='Previous' />
			<input type='button' class='btn btn-previouspage btn-fill btn-default btn-wd' id="back" value='Previous' />
		</div>
		<div class="clearfix">
		</div>
	</div>	
</form>

<?php
		if($inventory_data_arr["image"]==""){ $total_no_of_images=0;}
		else if($inventory_data_arr["image2"]==""){ $total_no_of_images=1;}
		else if($inventory_data_arr["image3"]==""){ $total_no_of_images=2;}
		else if($inventory_data_arr["image4"]==""){ $total_no_of_images=3;}
		else if($inventory_data_arr["image5"]==""){ $total_no_of_images=4;}
		else{$total_no_of_images=5;}
	?>	
	<script>
		function hide_inv_imagesFun(div_index_id){
			
			document.getElementById("previous_largeimage"+div_index_id).value="";
			document.getElementById("previous_image"+div_index_id).value="";
			document.getElementById("previous_thumbnail"+div_index_id).value="";
			
			document.getElementById("edit_wizard-picture-small"+div_index_id).value="";
			document.getElementById("edit_wizard-picture-big"+div_index_id).value="";
			document.getElementById("edit_wizard-picture-large"+div_index_id).value="";
			
			
			document.getElementById("edit_wizardPictureSmall"+div_index_id).removeAttribute("src");
			document.getElementById("edit_wizardPictureBig"+div_index_id).removeAttribute("src");
			document.getElementById("edit_wizardPictureLarge"+div_index_id).removeAttribute("src");
			
			document.getElementById("edit_wizardPictureSmall_picname"+div_index_id).innerHTML="Small Image (100x122)";
			document.getElementById("edit_wizardPictureBig_picname"+div_index_id).innerHTML="Big Image (420x512)";
			document.getElementById("edit_wizardPictureLarge_picname"+div_index_id).innerHTML="Large Image (850x1036)";
			
			document.getElementById("inv_images_div_"+div_index_id).style.display="none";
			enableDisabledAddInventoryButtonFun();
		}
		function enableDisabledAddInventoryButtonFun(){
			small_image_count=0;
			big_image_count=0;
			large_image_count=0;
			for(i=1;i<=5;i++){
				if(document.getElementById("inv_images_div_"+i).style.display==""){
					if(document.getElementById("edit_wizard-picture-small"+i).value!="" || document.getElementById("previous_thumbnail_view"+i).value!=""){
						small_image_count++;
					}
					if(document.getElementById("edit_wizard-picture-big"+i).value!="" || document.getElementById("previous_image_view"+i).value!=""){
						big_image_count++;
					}
					if(document.getElementById("edit_wizard-picture-large"+i).value!="" || document.getElementById("previous_largeimage_view"+i).value!=""){
						large_image_count++;
					}
				}
			}
			
			if(small_image_count==0 && big_image_count==0 && large_image_count==0){
				document.getElementById("add_image_inventory_btn").disabled=true;
			}
			else{
				if(small_image_count==big_image_count && big_image_count==large_image_count){
					document.getElementById("add_image_inventory_btn").disabled=false;
					document.getElementById("edit_inventory_finish_btn").disabled=false;
				}
				else{
					document.getElementById("add_image_inventory_btn").disabled=true;
					document.getElementById("edit_inventory_finish_btn").disabled=true;
					
				}
			}
		}
		function initializeInventoryImageDivFun(){
			for(i=<?php echo ($total_no_of_images+1);?>;i<=5;i++){
				document.getElementById("inv_images_div_"+i).style.display="none";
			}
		}
		function add_image_inventory_fun(){
			for(i=2;i<=5;i++){
				if(document.getElementById("inv_images_div_"+i).style.display=="none"){
					document.getElementById("inv_images_div_"+i).style.display="";
					document.getElementById("add_image_inventory_btn").disabled=true;
					break;
				}
			}
		}
		initializeInventoryImageDivFun();
		enableDisabledAddInventoryButtonFun();
		
	</script>
	
</div>
</div> <!-- wizard container -->
</div>
</div>
 
</div>
<div class="footer">
<form name="back_to_inventory" id="back_to_inventory" method="post">
		<input type="hidden" value="<?php echo $pcat_id; ?>" name="parent_category">
		<input type="hidden" value="<?php echo $cat_id; ?>" name="categories">
		<input type="hidden" value="<?php echo $subcat_id; ?>" name="subcategories">
		<input type="hidden" value="<?php echo $brand_id; ?>" name="brands">
		<input type="hidden" value="<?php echo $product_id; ?>" name="products">
		<input value="view" type="hidden" name="create_editview">
</form>
</div>
</div>
<?php		
//$highlight=str_replace("\n", '', addslashes($inventory_data_arr["highlight"]));
$highlight=$inventory_data_arr["highlight"];
$highlight1=str_replace("\n", '', $highlight);

$string = trim(preg_replace('/\s+/', ' ', $highlight1));
?>

 
<script  type="text/javascript" src="<?php echo base_url();?>assets/richte/editor.js" ></script>
<script>
function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}
$(document).ready(function() {
	var textarea = $("#highlight");
	textarea.Editor();
	str='<?php echo $string; ?>';
	data=decodeHtml(str);
	$('#highlight').siblings("div").children("div.Editor-editor").html(data);
 });
 
 function calculateSelling_priceFun(){
	 /*edit_base_price=$("#edit_base_price").val();
	 edit_tax=$("#edit_tax").val();
	 if(edit_base_price!="" && edit_tax!=""){
		edit_selling_price=Math.round(parseFloat(edit_base_price)+((edit_tax/100)*parseFloat(edit_base_price)));
		$("#edit_selling_price").val(edit_selling_price);
	 }
	 if(edit_base_price=="" || edit_tax==""){
		 $("#edit_selling_price").val("");
	 }*/
	 
	 edit_selling_price_after_promotion=$("#edit_selling_price_after_promotion").val();
	 edit_promo_discount=$("#edit_promo_discount").val();
	 edit_tax=$("#edit_tax").val();
	 if(edit_selling_price_after_promotion=="" && edit_tax==""){
		 $("#edit_selling_price").val('');
		 $("#edit_base_price").val('');
	 }
	 else{
		 edit_selling_price_without_round=(edit_selling_price_after_promotion/(1-(edit_promo_discount/100)));
		 edit_selling_price_with_round=Math.round(edit_selling_price_without_round*100)/100;
		
		 edit_base_price_without_round=edit_selling_price_with_round/(1+(edit_tax/100))
		 edit_base_price_with_round=Math.round(edit_base_price_without_round*100)/100;
		 $("#edit_selling_price").val(edit_selling_price_with_round);
		 $("#edit_base_price").val(edit_base_price_with_round);
	 }

 }


 </script>
</body>
</html>

