
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo base_url();?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <a class="home" href="<?php echo base_url(); ?>blog" title="Blog">Blog</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span> <?php echo $blog_data['blog_title']?></span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- Blog category -->
                <div class="block left-module">
                    <p class="title_block">Blog Categories</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <?php  if(!empty($topic_sel)){
                                  foreach($topic_sel as $topic){?>
                                      <li><span></span><a href="<?php echo base_url()?>get_blogs_of_topic/<?php echo $topic['topic_sel']?>"><?php echo $topic['topic_sel']?></a></li>
                                 <?php }
                                  }?>
    
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./blog category  -->
                    <?php if(!empty($popular_blogs)){?>
                <!-- Popular Posts -->
                <div class="block left-module">
                    <p class="title_block">Popular Posts</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered">
                            <div class="layered-content">
                                <ul class="blog-list-sidebar clearfix">
                                    <?php foreach($popular_blogs as $popular_blog){?>
                                  <li>
                                    <?php /*<div class="post-thumb">
                                        <a href="#"><!--<img src="assets/data/blog-thumb1.jpg" alt="Blog">--></a>
                                    </div>*/?>
                                    <div class="post-info" style="margin-left:0px">
                                        <h5 class="entry_title"><a href="#"><?php echo $popular_blog['blog_title'] ?></a></h5>
                                        <div class="post-meta">
                                            <span class="date"><i class="fa fa-calendar"></i> <?php echo $popular_blog['timestamp'] ?></span>
                                            <span class="comment-count">
                                                <i class="fa fa-thumbs-up"></i> <?php echo $popular_blog['count_likes'] ?>
                                            </span>
                                        </div>
                                    </div>
                                </li>      
                                   <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./Popular Posts -->
                    <?php }?>
  
                <!-- tags -->
                <div class="block left-module">
                    <p class="title_block">TAGS</p>
                    <div class="block_content">
                        <div class="tags">
                            <?php if(!empty($tags)){
                              foreach($tags as $tag){?>
                                  <a href="<?php echo base_url()?>get_blogs_of_tag/<?php echo $tag['tags']?>"><span class="level<?php echo rand ( 1 , 5 ) ?>"><?php echo $tag['tags']?></span></a>
                             <?php }
                              }?>
                            
                            
                            
                        </div>
                    </div>
                </div>
                <!-- ./tags -->
                <!-- Banner -->
                <div class="block left-module">
                    <div class="banner-opacity">
                        <a href="#"></a>
                    </div>
                </div>
                <!-- ./Banner -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <?php //print_r($blog_data);?>
                
                
                 <div class="entry-meta-data">
                        <span class="author">
                        <i class="fa fa-user"></i> 
                        <a href="#"><?php echo $blog_data['name']?></a></span>
                        <span class="cat">
                            <i class="fa fa-folder-o"></i>
                            <?php  if(!empty($topic_sel)){
                          foreach($topic_sel as $topic){
                              if($topic['topic_id']==$blog_data['topic_sel']){?>
                                  <a href="<?php echo base_url()?>get_blogs_of_topic/<?php echo $topic['topic_sel']?>"><?php echo $topic['topic_sel']?></a>
                             <?php break;}
                              }
                          }?>
                            
                        </span>
                        |
                        <span class="comment-count">
                            <a href="#comment-box"><i class="fa fa-comment-o" ></i></a> <?php if(!empty($blog_comment_data)){echo count($blog_comment_data);}else{ echo 0;}?>
                        </span>
                       |
                        <span class="date"><i class="fa fa-calendar"></i> <?php echo $blog_data['timestamp']?></span>
                       
                        
                       
                   <?php 
				   if($this->session->userdata("customer_id")!=""){
				   if(!empty($blog_data['likes'])){
                       if(in_array($this->session->userdata("logged_in"),json_decode('["'.$blog_data['likes'].'"]'))){ ?>
          
                        <i class="fa fa-thumbs-up" style="color:#ff0000"></i>
 
                       <?php } 
                       else{ ?>
     
                        <i id="message_status"><i class="fa fa-thumbs-up" onclick="submit_like()" style="cursor: pointer"></i></i>
           
                    <?php }
                    }
                     else{ ?>
      
                        <i id="message_status"><i class="fa fa-thumbs-up" onclick="submit_like()" style="cursor: pointer"></i></i>
   
                    <?php }
					}else{
                    ?>
                       <i class="fa fa-thumbs-up"></i> 
                      <?php } ?>  
                        <span> &nbsp;<?php echo "<span id='like_counter'>".(count(json_decode('["'.$blog_data['likes'].'"]'))-1)."</span>";?></span> <br>
                        <?php /*<span class="post-star">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                            <span>(7 votes)</span>
                        </span> */?>
                        <?php 
                          $postContent = $blog_data['blog_content']; 
                          $word = str_word_count(strip_tags($postContent));
                          $m = floor($word / 200);
                          $s = floor($word % 200 / (200 / 60));
                          $est = $m . ' minute' . ($m == 1 ? '' : 's') . ' ' . $s . ' second' . ($s == 1 ? '' : 's');
                        ?>
                    
                       <small style="color:#f30">Estimated reading time: <?php echo $est; ?></small>
                    </div>
                <hr style="margin:5px 0 5px 0">
                
                
                    <span class="page-heading-title2" style="font-size:2em; "><?php echo $blog_data['blog_title']?></span>
                
                
                
                <article class="entry-detail" style="margin-top:0px;">
                   
                    <!--<div class="entry-photo">
                        <img src="assets/data/blog-full.jpg" alt="Blog">
                    </div>-->
                    <div class="content-text clearfix" style="padding-top:0px;">
                        
                        <?php echo $blog_data['blog_content'];?>
                    </div>
                    <div class="entry-tags" id="comment-box">
                        <span><i class="fa fa-tags"></i>Tags :</span>
                        <?php  $tagsb=json_decode($blog_data['tag_sel'],true);
                            if(!empty($tagsb)){
                                $i=1;
                                foreach($tagsb as $v){?>
                                    <?php if(!empty($tags)){
                                      foreach($tags as $tag){
                                          if($tag['tags_id']==$v){?>
                                               <a href="<?php echo base_url()?>get_blogs_of_tag/<?php echo $tag['tags']?>"><?php echo $tag['tags']; echo count($tagsb)>$i? ",":"";?> </a>
                                              <?php }
                                          }
                                      }$i++;}
                            }
                        ?>
                    </div>
                    
                    
                    
                    
                </article>
              
                <div class="single-box" >
                    <?php 
					
                    if($this->session->userdata('user_type')!='customer'){?>
                        
                        <div class="coment-form">
                        
                        <a href="<?php echo base_url() ?>login"><button class="btn-comment">Login to give comments/like</button></a>
                        </div>
                    <?php }
                     ?>
                    
                </div>
                <hr>
                <!-- Comment -->
                <div class="single-box">
                    <h2 class="">Comments</h2>
                    <div class="comment-list">
                        <ul>
                            <?php 
                            if(!empty($blog_comment_data)){
                                foreach($blog_comment_data as $comment_data){?>
                            <li>
                                <div class="avartar">
                                    <img src="<?php echo base_url()?><?php echo $comment_data['customer_image']?>" alt="Avatar">
                                </div>
                                <div class="comment-body">
                                    <div class="comment-meta">
                                        <span class="author"><a href="#"><?php echo $comment_data['customer_name']?></a></span>
                                        <span class="date"><?php echo $comment_data['timestamp']?></span>
                                    </div>
                                    <div class="comment">
                                        <?php echo $comment_data['message']?> 
                                    </div>
                                </div>
                            </li>
                               <?php }
                            }else{ echo '<span style="color:#6a9ff2">No comments to display</span>';}
                            ?>
                            
                            
                        </ul>
                    </div>
                </div>
                <hr>
                <div class="single-box">
                    
                    <?php 
                    if($this->session->userdata('logged_in')==1 && $this->session->userdata('customer_id')!=''){?>
                        <h2>Write a Comment</h2>
                        <div class="coment-form" >
                            <div class="row">
                                 <style>
                                     .xx *{
                                         vertical-align:bottom
                                     }
                                     
                                 </style>
                                <div class="col-sm-12 xx">
                                    <img height="60vh" width="60vh" src="<?php echo base_url()?><?php echo $this->session->userdata('customer_image')?>" class="img-responsive">
                                   <?php echo $this->session->userdata('customer_name')?>
                                </div>
                             
                            </div>
                            <form id="comment_form" action="<?php echo base_url()?>add_blogs_comments" method="post">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input name="customer_image" type="hidden" value="<?php echo $this->session->userdata('customer_image')?>" class="form-control">
                                    <input name="customer_id" type="hidden" value="<?php echo $this->session->userdata('customer_id')?>" class="form-control">
                                    <input name="blog_id" type="hidden" value="<?php echo $blog_data['blog_id']?>" class="form-control">
                                    <input name="customer_name" type="hidden" class="form-control" readonly="" value="<?php echo $this->session->userdata('customer_name')?>">
                                </div>

                                <div class="col-sm-12">
                                  
                                    <textarea name="message" id="message" rows="8" class="form-control" maxlength="500"></textarea>
                                    <span class="help-block" id="comment_text_limit"></span>
                                </div>
                            </div>
                            <p id="message_status"></p>
                            <button class="btn-comment">Submit</button>
                            </form>
                        </div>
                    <?php }
                     ?>
                    
                </div>
                <!-- ./Comment -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#message').on('keypress mouseup keyup', function(){
            if($(this).val().length >= $(this).attr('maxlength')){
                $('#comment_text_limit').html('<span style="color:#e53b4c">Max characters allowed : '+$(this).attr('maxlength')+' </span>')
                return !($(this).val().length >= $(this).attr('maxlength'));
            }else{
                
                $('#comment_text_limit').html('')
            }
            
        });
        
        $('#comment_form').on('submit',function(event){
            event.preventDefault();        
            $.ajax({
                url: $(this).attr("action"),
                type: $(this).attr("method"),
                
                data: new FormData(this),
                processData: false,
                contentType: false,
                beforeSend:function(){
                    $('#message_status').html('Submitting <i class="fa fa-refresh fa-spin" style="color:#2eb332"></i>').fadeIn()
                 },
                success: function (data,status)
                {
					$('#message_status').html('<span style="color:#2eb332">'+data+'</span>').fadeIn('slow').delay(1000).fadeOut('slow');
                    $('#message').val('');
                },
                error: function (xhr, desc, err)
                {
					$('#message_status').html('<span style="color:#e03e0d">'+xhr.responseText+'</span>').fadeIn('slow').delay(1000).fadeOut('slow');
                }
            });
        })
    })
</script>
 <?php
                    if($this->session->userdata("logged_in")==1){ ?>
                        <form id="submit_like" action="<?php echo base_url()?>blog_like_feed_back" method="post">
                            <input type="hidden" value="<?php echo $this->session->userdata('customer_id')?>" name="customer_id">
                            <input type="hidden" value="<?php echo $blog_data['blog_id']?>" name="blog_id">
                        </form>
                     <script>
                         function submit_like(){
                             $('#submit_like').submit()
                         }
                         $(document).ready(function(){
                             $('#submit_like').on('submit',function(e){
                                 e.preventDefault();
                                 $.ajax({
                                    url: $(this).attr("action"),
                                    type: $(this).attr("method"),
                                    
                                    data: new FormData(this),
                                    processData: false,
                                    contentType: false,
                                    beforeSend:function(){
                                        $('#message_status').html(' <i class="fa fa-refresh fa-spin" style="color:#2eb332"></i>').fadeIn()
                                     },
                                    success: function (data,status)
                                    {
                                        $('#message_status').html(data)
                                        document.getElementById('like_counter').innerHTML=Number(document.getElementById('like_counter').innerHTML)+1
                                    },
                                    error: function (xhr, desc, err)
                                    {
                                         $('#message_status').html('<span style="color:#e03e0d">'+xhr.responseText+'</span>')
                                    }
                                });
                             })
                         })
                     </script>
                     <?php } ?>
