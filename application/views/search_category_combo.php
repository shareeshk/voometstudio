<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/skins/jquery-ui-like/progressbar.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/product_comparision/style.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/product_comparision/w3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/Ion.RangeSlider/Ion.RangeSlider.css"/>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/Ion.RangeSlider/Ion.RangeSlider.js"/></script>
<style type="text/css">

.product-list li .price-percent-reduction2 {
    line-height: 33px;
}
.product-list li .price-percent-reduction_offers {
    line-height: 17px;
}
/* qty field change */
.sp-qty-text{
    width: 20px;
    float: left;
    margin-right: 20px;
}
.sp-quantity {
    width:200px;
    height:42px;
    font-family:"ProximaNova Bold", Helvetica, Arial;
    margin-top: 10px;
}
.sp-minus {
    width:40px;
    height:40px;
    border:1px solid #e1e1e1;
    float:left;
    text-align:center;
}
.sp-input {
    width:40px;
    height:40px;
    border:1px solid #e1e1e1;
    border-left:0px solid black;
    float:left;
    padding-top: 2px;
}
.sp-plus {
    width:40px;
    height:40px;
    border:1px solid #e1e1e1;
    border-left:0px solid #e1e1e1;
    float:left;
    text-align:center;
}
.sp-input input {
    width:30px;
    height:34px;
    text-align:center;
    font-family:"ProximaNova Bold", Helvetica, Arial;
    border: none;
}
.sp-input input:focus {
    border:1px solid #e1e1e1;
    border: none;
}
.sp-minus a, .sp-plus a {
    display: block;
    width: 100%;
    height: 100%;
    padding-top: 2px;
}
/* qty field change */


    @media only screen and (max-width: 600px) {
        .heightmatched {
            font-size: 1em !important;
        }
    }
    @media (max-width: 480px) {
        .product-list.grid li {
            padding-right: 5px;
            padding-left: 5px;
        }
    }

    .checkbox label:after {
        content: '';
        display: table;
        clear: both;
    }

    .checkbox .cr {
        position: relative;
        display: inline-block;
        border: 1px solid #a9a9a9;
        border-radius: .25em;
        width: 1.3em;
        height: 1.3em;
        float: left;
        margin-right: .5em;
    }


    .checkbox .cr .cr-icon {
        position: absolute;
        font-size: .8em;
        line-height: 0;
        top: 50%;
        left: 20%;
    }

    .radio .cr .cr-icon {
        margin-left: 0.04em;
    }

    .checkbox label {
        padding-left: 0px;
        line-height: 1.3;
    }

    .checkbox label input[type="checkbox"] {
        display: none;
    }

    .checkbox label input[type="checkbox"] + .cr > .cr-icon,
    .radio label input[type="radio"] + .cr > .cr-icon {
        transform: scale(3) rotateZ(-20deg);
        opacity: 0;
        transition: all .3s ease-in;
    }

    .checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
    .radio label input[type="radio"]:checked + .cr > .cr-icon {
        transform: scale(1) rotateZ(0deg);
        opacity: 1;
    }

    .checkbox label input[type="checkbox"]:disabled + .cr,
    .radio label input[type="radio"]:disabled + .cr {
        opacity: .5;
    }

    .product-list li .product-star {
        width: auto;
        float: left;
        color: #eda900;
        text-align: center;
        display: inline-block;
        padding-top: 12px;
        font-size: 0.9em;
    }

    .compare_close {
        margin-top: -8px;
        background-color: #a39999;
        border-radius: 11px;
        width: 18px;
        height: 18px;
        line-height: 17px;
        text-align: center;
        color: #f9ecec;
        text-decoration: none;
        float: right;
        font-size: 18px;
    }
    .compare_lineheight {
        line-height: 80px;
    }
    
    .comparePanle {
        width: 100%;
        background-color: #fff;
        border-top: 1px solid #eee;
        bottom: 0px;
        padding: 10px;
    }
    
    .combo_close {
        /* margin-top: -8px; */
        background-color: #a39999;
        border-radius: 11px;
        width: 18px;
        height: 18px;
        line-height: 17px;
        text-align: center;
        color: #f9ecec;
        text-decoration: none;
        float: right;
        font-size: 18px;
    }
    .combo_lineheight {
        line-height: 20px;
    }
    
    .comboPanle {
        width: 100%;
        background-color: #fff;
        border-top: 1px solid #eee;
        bottom: 0px;
        padding: 10px;
    }
.comboPanle{
    width: 100%;
    background-color: #fff;
    border-top: 1px solid #eee;
    bottom: 0px;
    padding: 10px;
    position: fixed;
    z-index: 1;
    border-top: 2px solid #ff9f00 !important;
}
.comboPanle .button {
    padding: 10px 20px;
    border: 1px solid #ff3300;
    background: #ff3300;
    color: #fff;
}
.comboPan {
    /* width: 100%; */
    overflow-y: auto;
    max-height: 100px;
}
    
    .horizontal_bar {
        width: 60%;
        float: left;
        margin-bottom: 10px;
        color: #ff9900;
    }

    .star_rate_1 {
        float: left;
        width: 100%;
    }

    .star_rate_2 {
        float: left;
        width: 30%;
    }

    .star_rate_3 {
        float: left;
        width: 10%;
    }

    .star_rate {
        width: 30%;
        float: left;
        margin-right: 6px;
    }

    .popover {
        width: 700px;
    }

    div.rater {
        width: 200px;
        min-height: 40px;
        background: black;
        color: white;
        padding: 5px;
        position: relative;
        word-wrap: break-word;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        border-radius: 5px;
        margin-bottom: 2em;
        margin-top: 8px;
    }

    div.rater:after {
        content: '';
        display: block;
        position: absolute;
        top: -20px;
        left: 30px;
        width: 0;
        height: 0;
        border-color: transparent transparent black transparent;
        border-style: solid;
        border-width: 10px;
    }

    .img-cat-display {
        width: 300px;
        height: 366px;
    }

    /*.display-sortP-option{
position: absolute;
top: 0;
right: 63px;
min-width: 200px;
height: 30px;
margin-top: 0px;
margin-right: 5px;
}
#top-sortPagi .sortPagiBar .sort-product,#top-sortPagi .sortPagiBar .show-product-item {
margin-left: 21px;
}
@media (max-width: 480px) {
.display-sortP-option{
position: relative;
top: 0;
right: 0px;
min-width: 200px;
height: 30px;
margin-top: -10px;
margin-right: 0px;
margin-bottom: 10px;
}
}*/
    #WarningModal {
        z-index: 9999;
    }

    .line-break {
        margin-top: 10px;
        margin-bottom: 0;
    }

    body {
        background-color: #f1f3f6;
    }

    .breadcrumb {
        padding: 15px 0 0 15px;
    }

    @media (min-width: 768px) {
        .center_column {
            padding-left: 0;
        }

    }

    .list-inline li {
        line-height: 24px;
    }

    .list-inline li a {
        cursor: pointer;
    }

    .list-inline li a.active {
        font-weight: 600;
        color: #eda900;
    }

    .product-name a {
        color: #000;
    }

    .product-name a:hover {
        color: #777;
    }

    .attribute-col {
        font-size: 0.9em;
        color: #777;
        margin-bottom: 0.2em;
    }

    .content_price {
        padding-top: 5px;
    }

    .clearallfilterbtn {
        font-size: 12px;
        color: #0000ff;
        margin-top: 2px;
        text-transform: capitalize;
        margin-right: 0.5rem;
    }

    .layered .layered-content {
        border-bottom: 0px;
    }
    .price-percent-reduction2{
        /* display: none; */
    }
    .price-percent-reduction_offers{
        display: none;

    }
    .new_banner{
        padding: 20px;
        border: 1px solid #ccc;
        border-radius: 10px;
        line-height: 20px;
    }
    .new_banner_container{
        padding:20px;
    }
    .margin-top {
        margin-top: 10px;
    }
</style>

<script>
    base_url = "<?php echo base_url();?>";
</script>
<script>
    $(document).ready(function () {
        $(".star-rating").find("a").each(function () {
            $(this).html("");
        })
//	$(".jquery-ui-like").find(".hbar").each(function(){
//		$(this).html("");
//	})

    })
</script>

<?php
require_once 'assets/elastic/app/init_customer.php';
$total_count_skus_indexed_in_elasticsearch=$controller->get_total_count_skus_indexed_in_elasticsearch();
////////////////////////////////////////////////////////////////////////////////////////////

if(!isset($result_catalog_tree_view_inventory_id_arr)){
	$result_catalog_tree_view_inventory_id_arr=array();
}
$is_filter_applied="no";
if(isset($_REQUEST["result_catalog_tree_view"])){
	$request_result_catalog_tree_view=$_REQUEST["result_catalog_tree_view"];
	$result_catalog_tree_view_str=$_REQUEST["result_catalog_tree_view"];
	$result_catalog_tree_view_arr=explode(",",$result_catalog_tree_view_str);
	$result_catalog_tree_view_subcat_id_arr=[];
	foreach($result_catalog_tree_view_arr as $tree_view_str){
		if (strpos($tree_view_str, "subcat_id_") !== false) {
			$result_catalog_tree_view_subcat_id_arr[]=str_replace("subcat_id_","",$tree_view_str);
		}
	}
	if(!empty($result_catalog_tree_view_subcat_id_arr)){
		$result_catalog_tree_view_inventory_id_arr=$controller->get_inventories_of_catalog_tree_view_by_subcat_id_fun($result_catalog_tree_view_subcat_id_arr);
	}
	//print_r($result_catalog_tree_view_inventory_id_arr);
	$is_filter_applied="yes";
}
else{
	$is_filter_applied="no";
}
////////////////////////////////////////////////////////////////////////////////////////////



$veryfirst_condition_of_all_filters = "yes";

$filter_box_infoRes = $controller->get_filter_box_info($cat_current, $cat_current_id, $category_tree);
$filterbox_name_chosen_arr=[];
foreach ($filter_box_infoRes as $filter_box_infoObj) {
	$filterbox_name_chosen_arr[]=$filter_box_infoObj->filterbox_name;
}

									 
//$filter_box_infoRes="";
if (isset($cat_current)) {


    if ($cat_current == "all") {
        $params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1'];
    }
	
	if ($cat_current == "subcat_id") {
        
		
		
		$params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' =>
            [
                'query' => [
                    'bool' => [
                        
                        "must"=> [
                            ["match"=> [
                              "subcat_name"=> $searched_key
                            ]]
                        ],
                        'filter' => [
							['term' => [
                                'pcat_id' => $pcat_id,
                            ]],
                            ['term' => [
                                'cat_id' => $cat_id,
                            ]],
                            ['term' => [
                                'subcat_id' => $subcat_id,
                            ]],
                        ],
                    ],
                ],
            ],
        ];
				
    }
    if ($cat_current == "cat_id") {
       
		
		$params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' =>
            [
                'query' => [
                    'bool' => [
                        
                        "must"=> [
                            ["match"=> [
                              "cat_name"=> $searched_key
                            ]]
                        ],
                        'filter' => [
                            ['term' => [
                                'pcat_id' => $pcat_id,
                            ]],
                            ['term' => [
                                'cat_id' => $cat_id,
                            ]]
                           
                        ],
                    ],
                ],
            ],
        ];
    }
    if ($cat_current == "pcat_id") {
        
		$params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' =>

            [
                'query' => [
                    'bool' => [
                        
                        "must"=> [
                            ["match"=> [
                              "pcat_name"=> $searched_key
                            ]]
                        ],
                        'filter' => [
                            ['term' => [
                                'pcat_id' => $pcat_id,
                            ]]
                           
                        ],
                    ],
                ],
            ],
        
        ];
    }
    if ($cat_current == "brand_id") {
        
		
		  $params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' =>

            [
                'query' => [
                    'bool' => [
                        
                        "must"=> [
                            ["match"=> [
                              "brand_name"=> $searched_key
                            ]]
                        ],
                        'filter' => [
                            ['term' => [
                                'pcat_id' => $pcat_id,
                            ]],
                            ['term' => [
                                'cat_id' => $cat_id,
                            ]],
                            ['term' => [
                                'subcat_id' => $subcat_id,
                            ]],
							['term' => [
                                'brand_id' => $brand_id,
                            ]],
                        ],
                    ],
                ],
            ],
        
        ];
    }
    if ($cat_current == "product_id") {
        
			
			 $params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' =>

                [
                    'query' => [
                        'bool' => [
                            ["must"=> [
                                "match"=> [
                                  "product_name"=> $searched_key
                                ]
                            ]],
                            'filter' => [
                                ['term' => [
                                    'pcat_id' => $pcat_id,
                                ]],
                                ['term' => [
                                    'cat_id' => $cat_id,
                                ]],
                                ['term' => [
                                    'subcat_id' => $subcat_id,
                                ]],
                                ['term' => [
                                    'brand_id' => $brand_id,
                                ]],
								['term' => [
                                    'product_id' => $product_id,
                                ]],
                            ],
                        ],
                    ],
                ],
            
            
            ];
    }
}
/////////////////////////////////////


foreach ($filter_box_infoRes as $filter_box_infoObj) {
    $filter_box_infoObj->filterbox_name = preg_replace('/\s+/', '_', $filter_box_infoObj->filterbox_name);
    if (isset($_REQUEST[$filter_box_infoObj->filterbox_name . '_filter']) && $_REQUEST[$filter_box_infoObj->filterbox_name . '_filter'] != "") {
        $veryfirst_condition_of_all_filters = "no";
        //$filter_box_infoObj->filterbox_name . '_filter';
		//echo $_REQUEST[$filter_box_infoObj->filterbox_name . '_filter'];
        $color_filter = $_REQUEST[$filter_box_infoObj->filterbox_name . '_filter'];
		//echo $color_filter;
		if($filter_box_infoObj->type==="checkbox"){
		
			$color_filter_arr = explode("|", $color_filter);
			
			$color_filter_param_arr = array();
			foreach ($color_filter_arr as $colorFilterName) {
				$colorFilterName_objRes=$controller->get_filter_id_by_filter_name($colorFilterName);
				foreach ($colorFilterName_objRes as $colorFilterName_obj) {
					//$params["body"]["query"]["bool"]["must"][]=array("match"=>array("filter_" . $filter_box_infoObj->filterbox_name . "_id"=>$colorFilterName_obj->filter_id));
					
					$color_filter_param_arr[]["bool"]["must"][]["match"]["filter_" . $filter_box_infoObj->filterbox_name . "_id"] = $colorFilterName_obj->filter_id;
					
				}
			}
			$params["body"]["query"]["bool"]["must"][]["bool"]["should"] = $color_filter_param_arr;
		}
		
		
    }
}
//echo "<pre>";
///print_r($params);
//echo "</pre>";
$params["from"] = 0;
$params["size"] = 10000;

$query = $es->search($params);

if ($query['hits']['total'] >= 1) {
    $results = $query['hits']['hits'];
} else {
    $results = $query['hits']['hits'];
}
//echo "<pre>";
//print_r($results);
//echo "</pre>";

/*
if (isset($results[0]['_type'])) {
    if ($results[0]['_type'] == "flamingoCategory") {
        $params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' => 
        
        [
            'query' => [
                'bool' => [
                    
                    "must"=> [
                        "match"=> [
                          "subcat_name"=>$results[0]['_source']['subcat_name']
                        ]
                    ],
                    'filter' => [
                        'term' => [
                            'pcat_id' => $results[0]['_source']['pcat_id'],
                        ],
                        'term' => [
                            'cat_id' => $results[0]['_source']['cat_id'],
                        ],
                        'term' => [
                            'subcat_id' =>$results[0]['_source']['subcat_id'],
                        ]
                    ],
                ],
            ],
        ],
    
    
    

    ];


        foreach ($filter_box_infoRes as $filter_box_infoObj) {
            $filter_box_infoObj->filterbox_name = preg_replace('/\s+/', '_', $filter_box_infoObj->filterbox_name);
            if (isset($_REQUEST[$filter_box_infoObj->filterbox_name . '_filter']) && $_REQUEST[$filter_box_infoObj->filterbox_name . '_filter'] != "") {
                $filter_box_infoObj->filterbox_name . '_filter';
                $color_filter = $_REQUEST[$filter_box_infoObj->filterbox_name . '_filter'];
                $color_filter_arr = explode("|", $color_filter);
                $color_filter_param_arr = array();

                foreach ($color_filter_arr as $colorFilterName) {
                    $color_filter_param_arr[]["term"]["filter_" . $filter_box_infoObj->filterbox_name . "_id"] = $colorFilterName;
                }

                $params["body"]["query"]["filtered"]["filter"]["bool"]["must"][]["bool"]["should"] = $color_filter_param_arr;
            }
        }


        $params["from"] = 0;
        $params["size"] = 10000;

        $query = $es->search($params);
        if ($query['hits']['total'] >= 1) {
            $results = $query['hits']['hits'];
        } else {
            $results = $query['hits']['hits'];
        }
    }

}
*/
//////////////////////////////// this is for filtering according to price filter added manually starts //////////////////////////////////////////////////











$singleslider_arr=[];
$doubleslider_arr=[];



//print_r($params);
//print_r($results);

$rows = count($results);
if (isset($_REQUEST["page_rows"])) {
    $page_rows = $_REQUEST["page_rows"];
} else {
    if (isMobileDevice()) {
        $page_rows = 16;
    } else {
        $page_rows = 24;
    }
}


/////////////////////////
if (isset($_REQUEST["sorting_by"])) {
    $sorting_by = $_REQUEST["sorting_by"];

    if ($sorting_by == "default|SORT_ASC") {
        $sort_field_name = "inventory_id";
        $sort_flag = "SORT_ASC";
    } else {
//echo $sorting_by;
        $sorting_by_arr = explode("|", $sorting_by);
        $sort_field_name = $sorting_by_arr[0];
        if (isset($sorting_by_arr[1])) {
            $sort_flag = $sorting_by_arr[1];
        } else {
            $sort_flag = "SORT_ASC";
        }


    }
} else {
    $sorting_by = "default|SORT_ASC";
    $sort_field_name = "inventory_id";
    $sort_flag = "SORT_ASC";
}

$sort_fields = array();
if (!empty($results)) {
    foreach ($results as $resObj) {
        if (isset($resObj["_source"]["inventory_id"])) {
            if (isset($resObj["_source"][$sort_field_name]))
                $sort_fields[] = $resObj["_source"][$sort_field_name];
				
        }
    }
}

//print_r($sort_fields);

//echo $sort_field_name;
//echo $sorting_by;
//if($pagenum==1){

if (count($sort_fields) > 0) {
    if ($sort_flag == "SORT_DESC") {
        array_multisort($sort_fields, SORT_DESC, $results);
    }
    if ($sort_flag == "SORT_ASC") {
        array_multisort($sort_fields, SORT_ASC, $results);
    }
}
//}
//else{
//array_multisort($sort_fields,$sort_flag , $results);
//}

///////////////////////////

?>
<?php


//echo '<pre>';
//print_r($results);
//echo '</pre>';
//print_r($_REQUEST);
//echo $_SERVER["PHP_SELF"];
foreach ($results as $resObj1) {
    $selling_price_for_price_filter_arr[] = $resObj1["_source"]["combo_sel_price"];
	
	/* take the value of singleslider values starts */
	foreach($resObj1["_source"] as $invkey => $invvalue){
		if($invvalue==="singleslider"){
			$remove_type=str_replace("_type","",$invkey);
			$remove_type_filter=str_replace("filter_","",$remove_type);
			$singleslider_arr[$remove_type_filter][]=$resObj1["_source"]["filter_".$remove_type_filter."_filter_options"];
			//if (!isset($_REQUEST["type_of_filter"])) {
				//setcookie("singleslider", json_encode($singleslider_arr));
			//}
			
		}
		if($invvalue==="doubleslider"){
			$remove_type=str_replace("_type","",$invkey);
			$remove_type_filter=str_replace("filter_","",$remove_type);
			$doubleslider_arr[$remove_type_filter][]=$resObj1["_source"]["filter_".$remove_type_filter."_filter_options"];
			//if (!isset($_REQUEST["type_of_filter"])) {
				//setcookie("doubleslider", json_encode($doubleslider_arr));
			//}
		}
	}
	/* take the value of singleslider values ends */
	
	
    
}
//print_r($selling_price_for_price_filter_arr);
//print_r($singleslider_arr);echo "yyyyyyyyyyyy";
////////// newly added code for discount promotion price filter starts /////////////////

$discount_is_exists = "no";
//$cur_result = array_slice($results, ($pagenum - 1) * $page_rows, $page_rows);
$selling_price_for_price_filter_discount_arr = [];

$inv_dis=array();$inv_qty_discount_data=array();

foreach ($results as $searchRes) {
    if (isset($searchRes['_source']['inventory_id'])) {

        $inventory_id = $searchRes['_source']['inventory_id'];
        $product_id = $searchRes['_source']['product_id'];
        $selling_price = $searchRes["_source"]["selling_price"];
        $max_selling_price = $searchRes["_source"]["max_selling_price"];
        $selling_discount = $searchRes["_source"]["selling_discount"];

        $inv_discount_data = $controller->get_default_discount_for_inv($inventory_id, $max_selling_price, $product_id);
        $inv_qty_discount_data[$inventory_id] = $controller->get_default_discount_for_inv($inventory_id, $max_selling_price, $product_id,'qty_based');

        if (!empty($inv_discount_data)) {
            $discount_is_exists = "yes";
            $selling_price_for_price_filter_discount_arr[] = $inv_discount_data["current_price"];
        }
        


    }
}

if ($discount_is_exists == "yes") {
    $selling_price_for_price_filter_arr = $selling_price_for_price_filter_discount_arr;
}

////////// newly added code for discount promotion price filter starts /////////////////








//////////////////////////////// this is for filtering according to price filter added manually starts //////////////////////////////////////////////////

//print_r($results);
if (!isset($_REQUEST["type_of_filter"])) {
    $type_of_filter = "";
} else {
    $type_of_filter = $_REQUEST["type_of_filter"];
}




if ($type_of_filter != "") {
    $Price_filter_info_for_populating_inventories = $_REQUEST["Price_filter"];
    $Price_filter_info_for_populating_inventories_arr = explode("-", $Price_filter_info_for_populating_inventories);
    $min_Price_filter_info_for_populating_inventories = $Price_filter_info_for_populating_inventories_arr[0];
    $max_Price_filter_info_for_populating_inventories = $Price_filter_info_for_populating_inventories_arr[1];
    $results_temp_arr = array();
///echo "<pre>";
//print_r($results);
//echo "</pre>";
    foreach ($results as $ind => $results_arr) {


        ////////// newly added code for discount promotion price filter starts /////////////////
        $inventory_id = $results[$ind]['_source']['inventory_id'];
        $product_id = $results[$ind]['_source']['product_id'];
        $selling_price = $results[$ind]["_source"]["selling_price"];
        $max_selling_price = $results[$ind]["_source"]["max_selling_price"];
        $selling_discount = $results[$ind]["_source"]["selling_discount"];

        $inv_discount_data = $controller->get_default_discount_for_inv($inventory_id, $max_selling_price, $product_id);
        if (!empty($inv_discount_data)) {
            if ($inv_discount_data["current_price"] >= $min_Price_filter_info_for_populating_inventories && $inv_discount_data["current_price"] <= $max_Price_filter_info_for_populating_inventories) {
                $results_temp_arr[] = $results_arr;
            }
        } ////////// newly added code for discount promotion price filter starts /////////////////
        else {


            if ($results[$ind]["_source"]["combo_sel_price"] >= $min_Price_filter_info_for_populating_inventories && $results[$ind]["_source"]["combo_sel_price"] <= $max_Price_filter_info_for_populating_inventories) {
                $results_temp_arr[] = $results_arr;
            }
        }
    }


    $results = $results_temp_arr;
}

//////////////////////////////// this is for filtering according to price filter added manually ends //////////////////////////////////////////////




/* this block is for setting the min and max default price filter values starts */
if ($type_of_filter == "") {
    if (!empty($selling_price_for_price_filter_arr)) {
        $min_val_of_price_filter = min($selling_price_for_price_filter_arr);
        $max_val_of_price_filter = max($selling_price_for_price_filter_arr);
        if ($min_val_of_price_filter == $max_val_of_price_filter) {
            $min_val_of_price_filter = 0;
        }
    } else {
        $min_val_of_price_filter = 0;
        $max_val_of_price_filter = 0;
    }
    $default_min_val_of_price_filter = $min_val_of_price_filter;
    $default_max_val_of_price_filter = $max_val_of_price_filter;
} else {
    $Price_filter_info_for_populating_inventories = $_REQUEST["Price_filter"];
    $Price_filter_info_for_populating_inventories_arr = explode("-", $Price_filter_info_for_populating_inventories);
    $min_val_of_price_filter = $Price_filter_info_for_populating_inventories_arr[0];
    $max_val_of_price_filter = $Price_filter_info_for_populating_inventories_arr[1];
    $Price_default_info_arr = explode("-", $_REQUEST["Price_default_info"]);
    $default_min_val_of_price_filter = $Price_default_info_arr[0];
    $default_max_val_of_price_filter = $Price_default_info_arr[1];
}
/* this block is for setting the min and max default price filter values ends */





/* this block is for setting the min and max default singleslider_arr filter values starts */
$singleslider_filter_arr=[];
if ($type_of_filter == "") {
	//if(isset($_COOKIE["singleslider"])){
		//$singleslider_arr=json_decode($_COOKIE["singleslider"],true);
	//}
	foreach($singleslider_arr as $key => $value_arr){
		if(!empty($value_arr)){
			$singleslider_filter_arr[$key]["min"] = min($value_arr);
			$singleslider_filter_arr[$key]["max"]= max($value_arr);
			if ($singleslider_filter_arr[$key]["min"] == $singleslider_filter_arr[$key]["max"]) {
				$singleslider_filter_arr[$key]["min"] = 0;
			}
		}
		else {
			$singleslider_filter_arr[$key]["min"] = 0;
			$singleslider_filter_arr[$key]["max"]= 0;
		}
		$singleslider_filter_arr[$key]["default_min"] = min($value_arr);
		$singleslider_filter_arr[$key]["default_max"]= max($value_arr);
	}
} else {
		//print_r($singleslider_arr);echo "xxxxxxxxxxxxx";
		
	foreach($singleslider_arr as $key => $value_arr){
		//echo $_REQUEST[$key."_default_info"];
		if(isset($_REQUEST[$key."_default_info"])){
			$singleslider_default_info_arr = explode("-", $_REQUEST[$key."_default_info"]);
			$singleslider_filter_arr[$key]["default_min"] = $singleslider_default_info_arr[0];
			$singleslider_filter_arr[$key]["default_max"] = $singleslider_default_info_arr[1];
			
			
			$singleslider_filter_info_for_populating_inventories = $_REQUEST[$key."_filter"];
			$singleslider_filter_info_for_populating_inventories_arr = explode("-", $singleslider_filter_info_for_populating_inventories);
			$singleslider_filter_arr[$key]["min"] = $singleslider_filter_arr[$key]["default_min"];
			$singleslider_filter_arr[$key]["max"] = $singleslider_filter_info_for_populating_inventories_arr[1];
			
			//echo $_REQUEST[$key.'_filter'];
			//echo "<br><br>1111111111";
			
			//echo $singleslider_filter_arr[$key]["default_min"]."-".$singleslider_filter_info_for_populating_inventories_arr[0];
		}
		
	}
}

/* this block is for setting the min and max default singleslider_arr filter values ends */





/* this block is for setting the min and max default doubleslider_arr filter values starts */

if ($type_of_filter == "") {
	//if(isset($_COOKIE["doubleslider"])){
		//$doubleslider_arr=json_decode($_COOKIE["doubleslider"],true);
	//}
	foreach($doubleslider_arr as $key => $value_arr){
		if(!empty($value_arr)){
			$doubleslider_filter_arr[$key]["min"] = min($value_arr);
			$doubleslider_filter_arr[$key]["max"]= max($value_arr);
			if ($doubleslider_filter_arr[$key]["min"] == $doubleslider_filter_arr[$key]["max"]) {
				$doubleslider_filter_arr[$key]["min"] = 0;
			}
		}
		else {
			$doubleslider_filter_arr[$key]["min"] = 0;
			$doubleslider_filter_arr[$key]["max"]= 0;
		}
		$doubleslider_filter_arr[$key]["default_min"] = min($value_arr);
		$doubleslider_filter_arr[$key]["default_max"]= max($value_arr);
	}
} else {
	//print_r($doubleslider_arr);
	foreach($doubleslider_arr as $key => $value_arr){
		$doubleslider_default_info_arr = explode("-", $_REQUEST[$key."_default_info"]);
		$doubleslider_filter_arr[$key]["default_min"] = $doubleslider_default_info_arr[0];
		$doubleslider_filter_arr[$key]["default_max"] = $doubleslider_default_info_arr[1];
		
		
		$doubleslider_filter_info_for_populating_inventories = $_REQUEST[$key."_filter"];
		$doubleslider_filter_info_for_populating_inventories_arr = explode("-", $doubleslider_filter_info_for_populating_inventories);
		$doubleslider_filter_arr[$key]["min"] = $doubleslider_filter_info_for_populating_inventories_arr[0];
		$doubleslider_filter_arr[$key]["max"] = $doubleslider_filter_info_for_populating_inventories_arr[1];
		
		//echo $_REQUEST[$key.'_filter'];
		//echo "<br><br>1111111111";
		
		//$doubleslider_filter_arr[$key]["default_min"]."-".$doubleslider_filter_info_for_populating_inventories_arr[0];
		
	}
}

/* this block is for setting the min and max default doubleslider_arr filter values ends */




////////////////////////////////////////


?>













<?php



//////////////////////////////// this is for filtering according to singleslider filter added manually starts //////////////////////////////////////////////////

//print_r($results);
if (!isset($_REQUEST["filterbox_type"])) {
    $filterbox_type = "";
} else {
    $filterbox_type = $_REQUEST["filterbox_type"];
}



//if ($filterbox_type === "singleslider") {
	//print_r($singleslider_arr);
	//print_r($singleslider_filter_arr);
	if(!empty($singleslider_arr)){
		foreach($singleslider_arr as $singleslider_key => $singleslider_value){
			$results_temp_arr = array();
			
				$singleslider_filtering = $singleslider_filter_arr[$singleslider_key]["min"]."-".$singleslider_filter_arr[$singleslider_key]["max"];
				$singleslider_filtering_arr = explode("-", $singleslider_filtering);
				//print_r($singleslider_filtering_arr);
				$minvalue = $singleslider_filtering_arr[0];
				$maxvalue = $singleslider_filtering_arr[1];
				
				
				foreach ($results as $ind => $results_arr) {


					
					$inventory_id = $results[$ind]['_source']['inventory_id'];
					$product_id = $results[$ind]['_source']['product_id'];
					$selling_price = $results[$ind]["_source"]["selling_price"];
					$max_selling_price = $results[$ind]["_source"]["max_selling_price"];
					$selling_discount = $results[$ind]["_source"]["selling_discount"];

					if(isset($results[$ind]["_source"]["filter_".$singleslider_key."_filter_options"])){
						if ($results[$ind]["_source"]["filter_".$singleslider_key."_filter_options"] >= $minvalue && $results[$ind]["_source"]["filter_".$singleslider_key."_filter_options"] <= $maxvalue) {
							$results_temp_arr[] = $results_arr;
						}
					}
					else{
						$results_temp_arr[] = $results_arr;	
					}
				
				}


				$results = $results_temp_arr;
			
		}
	}

//}

//////////////////////////////// this is for filtering according to singleslider filter added manually ends //////////////////////////////////////////////




//////////////////////////////// this is for filtering according to doubleslider filter added manually starts //////////////////////////////////////////////////

//print_r($results);
if (!isset($_REQUEST["filterbox_type"])) {
    $filterbox_type = "";
} else {
    $filterbox_type = $_REQUEST["filterbox_type"];
}



//if ($filterbox_type === "doubleslider") {
	
	
	if(!empty($doubleslider_arr)){
		foreach($doubleslider_arr as $doubleslider_key => $doubleslider_value){
			$doubleslider_filtering = $doubleslider_filter_arr[$doubleslider_key]["min"]."-".$doubleslider_filter_arr[$doubleslider_key]["max"];
			$doubleslider_filtering_arr = explode("-", $doubleslider_filtering);
			//print_r($doubleslider_filtering_arr);
			$minvalue = $doubleslider_filtering_arr[0];
			$maxvalue = $doubleslider_filtering_arr[1];
			$results_temp_arr = array();


			foreach ($results as $ind => $results_arr) {


				
				$inventory_id = $results[$ind]['_source']['inventory_id'];
				$product_id = $results[$ind]['_source']['product_id'];
				$selling_price = $results[$ind]["_source"]["selling_price"];
				$max_selling_price = $results[$ind]["_source"]["max_selling_price"];
				$selling_discount = $results[$ind]["_source"]["selling_discount"];

				
				if(isset($results[$ind]["_source"]["filter_".$doubleslider_key."_filter_options"])){
					if ($results[$ind]["_source"]["filter_".$doubleslider_key."_filter_options"] >= $minvalue && $results[$ind]["_source"]["filter_".$doubleslider_key."_filter_options"] <= $maxvalue) {
						$results_temp_arr[] = $results_arr;
					}
				}
			
			}


			$results = $results_temp_arr;
		}
	}
//}

//////////////////////////////// this is for filtering according to singleslider filter added manually ends //////////////////////////////////////////////


/**** brand_customfilterbox_filter starts ****/

if (isset($_REQUEST['brand_customfilterbox_filter']) && $_REQUEST['brand_customfilterbox_filter'] != "") {
	$brand_filtered_arr=[];
	$brand_customfilterbox_filter = $_REQUEST['brand_customfilterbox_filter'];
	$brand_customfilterbox_filter_arr = explode("|", $brand_customfilterbox_filter);
	$brand_filter_param_arr = array();
	$results_temp_arr = array();
	foreach ($brand_customfilterbox_filter_arr as $brand_name_custom) {
		$brand_filtered_arr[]=$brand_name_custom;
	}
	foreach ($results as $ind => $results_arr) {
		if(in_array($results[$ind]["_source"]["brand_name"],$brand_filtered_arr)){
			$results_temp_arr[] = $results_arr;
		}
	}
	$results = $results_temp_arr;
}

/**** brand_customfilterbox_filter ends ****/

?>







					
<?php
/*********** catalog tree view starts ***/
	$result_catalog_tree_view_temp=array();
	$inventories_display_count=0;
	foreach ($results as $searchRes) {
		if (isset($searchRes['_source']['inventory_id'])) {
				if(!empty($result_catalog_tree_view_inventory_id_arr)){
					if(!in_array($searchRes['_source']['inventory_id'],$result_catalog_tree_view_inventory_id_arr)){
						continue;
					}
				}

			if(isset($searchRes["_source"]["selling_price"])){
				$result_catalog_tree_view_temp[]=$searchRes;
				$inventories_display_count++;
			}
		}
	}
	if($inventories_display_count>0){
		$results=$result_catalog_tree_view_temp;
	}
	//echo "<pre>";
	//print_r($cur_result);
	//echo "</pre>";
/*********** catalog tree view ends ***/
?>


<?php


$last = ceil(count($results) / $page_rows);

if ($last < 1) {
    $last = 1;
}

// Establish the $pagenum variable

// Get pagenum from URL vars if it is present, else it is = 1

if (isset($_REQUEST['pagenum'])) {
//$pagenum = preg_replace('#[^0-9]#', '', $_REQUEST['pagenum']);
    $pagenum = $_REQUEST['pagenum'];
} else {
    $pagenum = 1;
}

// This makes sure the page number isn't below 1, or more than our $last page
if ($pagenum < 1) {
    $pagenum = 1;
} else if ($pagenum > $last) {
    $pagenum = $last;
}




$cur_result = array_slice($results, ($pagenum - 1) * $page_rows, $page_rows);

////////////////////////////////////////////////////////////////////////
// Establish the $paginationCtrls variable
$paginationCtrls = '';
if ($last != 1) {
    if ($pagenum > 1) {
        $previous = $pagenum - 1;
//$paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$previous.'" aria-label="Previous"><span aria-hidden="true">&laquo; Previous</span></a></li>';
        $paginationCtrls .= '<li><a href="javascript:paginationFun(' . $previous . ')" aria-label="Previous"><span aria-hidden="true">&laquo; Previous</span></a></li>';

        for ($i = $pagenum - 4; $i < $pagenum; $i++) {
            if ($i > 0) {
// $paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'">'.$i.'</a></li>';
                $paginationCtrls .= '<li><a href="javascript:paginationFun(' . $i . ')">' . $i . '</a></li>';
            }
        }
    }
    $paginationCtrls .= '<li class="active"><a href="#">' . $pagenum . '</a></li>';
    for ($i = $pagenum + 1; $i <= $last; $i++) {
//$paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'">'.$i.'</a></li>';
        $paginationCtrls .= '<li><a href="javascript:paginationFun(' . $i . ')">' . $i . '</a></li>';
        if ($i >= $pagenum + 4) {
            break;
        }
    }
    if ($pagenum != $last) {
        $next = $pagenum + 1;
//$paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$next.'" aria-label="Next"><span aria-hidden="true">Next &raquo;</span></a></li>';
        $paginationCtrls .= '<li><a href="javascript:paginationFun(' . $next . ')" aria-label="Next"><span aria-hidden="true">Next &raquo;</span></a></li>';
    }
}
//////////////////////////////////////////////////////////////////////
/*echo '<pre>';
print_r($results);
echo '</pre>';*/
//exit;


?>



<?php
	
/* admin combo pack setup */
$combo_pack_min_items=2;
$combo_pack_discount=0;
$combo_pack_shipping_charge=0;
$combo_pack_status='active';

if(!empty($adm_settings)){
    $combo_pack_min_items=$adm_settings->adm_combo_pack_min_items;
    $combo_pack_discount=$adm_settings->adm_combo_pack_discount;
    $combo_pack_shipping_charge=$adm_settings->adm_combo_pack_shipping_charge;
    $combo_pack_status=$adm_settings->adm_combo_pack_status;
}

/* admin combo pack setup */
?>
<input type="hidden" name="combo_pack_min_items" value="<?php echo $combo_pack_min_items; ?>" id="combo_pack_min_items">
<input type="hidden" name="combo_pack_discount" value="<?php echo $combo_pack_discount; ?>" id="combo_pack_min_items">
<input type="hidden" name="combo_pack_shipping_charge" value="<?php echo $combo_pack_shipping_charge; ?>" id="combo_pack_shipping_charge">
<input type="hidden" name="combo_pack_status" value="<?php echo $combo_pack_status; ?>" id="combo_pack_status">

<?php 
$url = base_url();
$var = "search_category_combo";
$rand1 = $controller->sample_code();
$rand2 = $controller->sample_code();
$rand3 = $controller->sample_code();
$s = "/";
$temp = "search";
$home = "{$url}{$temp}";
?>
<div class="columns-container">
    <div class="container-fluid" id="columns">
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <?php
                /* menu filter starts*/
                ?>
                <!-- block category -->
                <div class="block left-module hidden-xs">
                    
					
					
					
					
																				
																				
							<div class="row">
					<div class="col-md-7 title_block" style="font-size:1em;">Categories 
					   
					   
					
					   </div>
					   <div class="col-md-5 title_block">
					   <?php
						if($type_of_filter==""){
					?>
						
							<a class="pull-right clearallfilterbtn" style="cursor:default;color:#b3b3b3;">Reset Filters&nbsp;</a>
						<?php
							}
							else{
								?>
								<a class="pull-right clearallfilterbtn" onclick="clearAllFiltersfun()" style="cursor:pointer;">Reset Filters&nbsp;</a>
								<?php
							}
						?>
						</div>
					</div>													
																				
																				
																				
																				
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content" style="border-bottom: 1px solid #eaeaea;">
                                <ul class="tree-menu">

                                    <?php
                                    $menu = '';

                                    $parent_cat_infoObj = $controller->get_parent_cat_info_all_combo('');// always show all the pcats

                                    if(!empty($parent_cat_infoObj)){

                                        foreach($parent_cat_infoObj as $key=> $pcat_values){

                                            $p_id=$pcat_values->pcat_id;

                                    $rand_1 = $controller->sample_code();
                                    $rand_1 = $controller->sample_code();
                                    $cl = '';
                                    if ($p_id == 0) {
                                        $cl = 'active';
                                    }

                                    $cat = $controller->provide_category($p_id);

                                    //print_r($cat);

                                    if ($p_id != '' && $p_id != 0) {

                                        if (!empty($pcat_values)) {
                                            if (($pcat_id == $p_id)) {
                                                $cl = 'active';
                                            }
                                            $menu .= '<li class="' . $cl . '">';

                                            if (count($cat) > 0 && $cl == "active") {

                                                $menu .= ' <span class="open"></span>';
                                            } else {

                                                $menu .= ' <span></span>';
                                            }
                                            $menu .= '<a href="' . base_url() . "search_category_combo/{$rand_1}{$p_id}" . '">' . $pcat_values->pcat_name . '</a>'; // category li starts
                                            $menu .= '<small class="pull-right" style="color:#797979;">(' . $controller->get_skus_under_chain($p_id, "pcat") . ')</small>';
                                        }
                                    }


                                    if (count($cat) > 0) {
                                        $style1 = '';
                                        if ($cl == "active") {
                                            $style1 = 'style="display: block;"';
                                        } else {
                                            $style1 = 'style="display: none;"';
                                        }
                                        $menu .= ' <li>';
                                        $menu .= '<ul ' . $style1 . '>'; // categories list starts


                                        foreach ($cat as $cat_value) {
                                            $rand_2 = $controller->sample_code();
                                            $cat_name = $cat_value->cat_name;
                                            $class = '';
                                            $class1 = '';

                                            $cat_id_m = $cat_value->cat_id;
                                            if (isset($cat_id)) {
                                                if ($cat_id == $cat_id_m) {
                                                    $class1 = "active";
                                                }
                                                if (($subcat_id == '') && ($cat_id == $cat_id_m)) {
                                                    $class = 'active';
                                                }
                                            }
                                            $sub_cat = $controller->provide_sub_category($cat_id_m);
                                            $menu .= ' <li class="' . $class . '">'; // subcategory li starts
                                            if (count($sub_cat) > 0 && $class1 == "active") {
                                                $menu .= ' <span class="open"></span>';
                                            } else {
                                                $menu .= ' <span></span>';
                                            }
                                            $menu .= '<a href="' . base_url() . "search_category_combo/{$rand_1}{$p_id}/{$rand_2}{$cat_id_m}" . '">' . $cat_name . '</a>';
                                            $menu .= '<small class="pull-right" style="color:#797979;">(' . $controller->get_skus_under_chain($cat_id_m, "cat") . ')</small>';


                                            if (count($sub_cat) > 0) {
                                                $style = '';
                                                if ($class1 == "active") {
                                                    $style = 'style="display: block;"';
                                                } else {
                                                    $style = 'style="display: none;"';
                                                }
                                                $menu .= '<ul ' . $style . '>'; // subcategories list starts

                                                foreach ($sub_cat as $sub_cat_value) {
                                                    $class2 = '';
                                                    $rand_3 = $controller->sample_code();
                                                    $subcat_id_m = $sub_cat_value["subcat_id"];
                                                    $subcat_value = $sub_cat_value["subcat_name"];
                                                    if (isset($subcat_id)) {
                                                        if ($subcat_id == $subcat_id_m) {
                                                            $class2 = "active";
                                                        }
                                                    }

                                                    $menu .= '<li class="' . $class2 . '"><span></span><a href="' . base_url() . "search_category_combo/{$rand_1}{$p_id}/{$rand_2}{$cat_id_m}/{$rand_3}{$subcat_id_m}" . '">' . $subcat_value . '</a>';
                                                    $menu .= '<small class="pull-right" style="color:#797979;">(' . $controller->get_skus_under_chain($subcat_id_m, "subcat") . ')</small>';
                                                    $menu .= '</li>';
                                                }
                                                $menu .= '</ul>'; // subcategories list ends
                                            }

                                            $menu .= '</li>'; // subcategory li ends
                                        }
                                        $menu .= '</ul>'; // categories list ends
                                        $menu .= ' </li>';
                                    }
                                    $menu .= '</li>';  // category li ends

                                } //foreach

                                    echo $menu;

                                

                            } //if 
                                    ?>

                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
                <?php
                /* menu filter ends*/
                ?>
				
				
				
				<!-------------------------- brand filter starts ------------------------->
				<div class="block left-module" id="filterselection_div">
                    
																				
																				
 <div class="row">
					<div class="col-md-7 title_block" style="font-size:1em;cursor:pointer" data-toggle="collapse"
                       data-target="#demo" id="filterselection_heading">Brand 
					   
					   
					
					   </div>
					   <div class="col-md-5 title_block">
					   <?php
						if($type_of_filter==""){
					?>
						
							<a class="pull-right clearallfilterbtn" style="cursor:default;color:#b3b3b3;">Reset Filters&nbsp;</a>
						<?php
							}
							else{
								?>
								<a class="pull-right clearallfilterbtn" onclick="clearAllFiltersfun()" style="cursor:pointer;">Reset Filters&nbsp;</a>
								<?php
							}
						?>
						</div>
					</div>
					
					
					
					
                    <?php
                    //echo $cat_current;
                    //echo $cat_current_id;
                    //$filter_box_infoRes=$controller->get_filter_box_info($cat_current,$cat_current_id);
                   // echo "<pre>";
				//	print_r($filter_box_infoRes);
					//echo "</pre>";
                    ?>
                    <div class="block_content collapse in" id="demo">
                        <!-- layered -->
                        <div class="layered">
						
						

                                    <div class="layered-content">
                                      
                                        <ul class="check-box-list row">
                                             <?php
											 
											 
														
														
											$result_all_brand_names_arr=$controller->get_all_brand_names();
											foreach($result_all_brand_names_arr as $arr){
													$specific_brand_name=$arr["brand_name"];
												
												
														$checked = "";
														if(isset($_REQUEST["brand_customfilterbox_filter"])){
															$fil_arr = explode("|", $_REQUEST["brand_customfilterbox_filter"]);


															if (isset($_REQUEST["brand_customfilterbox_filter"])) {
																if (in_array($specific_brand_name, $fil_arr)) {
																	$checked = "checked";
																}
															}
															
														}
														
														
														
									   ?>
                                                        <li class="col-md-6">
															
																<input type="checkbox" id
																="<?php echo $specific_brand_name; ?>"
																	   name="brand_customfilterbox"
																	   value="<?php echo $specific_brand_name; ?>" onchange="filterColorFun('brand_customfilterbox','brand')" <?php echo $checked; ?>>


																<label 
																		for="<?php echo $specific_brand_name; ?>"><span
																			class="button"></span><?php
																	
																		echo ucfirst($specific_brand_name);
																	 ?></label>
																	
																
															</li>
														
														
														
														
														
														
														
															
                                                        <?php
                                                    
											}
												 

                                            

                                            
                                            ?>

                                        </ul>
                                    </div>
                                    <?php
								
                                


                            
                            ?>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
				
				<!-------------------------- brand filter ends ------------------------->
				
				
				
                <!-- block filter -->
                <div class="block left-module" id="filterselection_div">
                    															
					<div class="row">
						<div class="col-md-7 title_block" style="font-size:1em;cursor:pointer" data-toggle="collapse"
                       data-target="#demo" id="filterselection_heading">Filters 
					   </div>
					   
							<div class="col-md-5 title_block">
						   <?php
							if($type_of_filter==""){
						?>
							
								<a class="pull-right clearallfilterbtn" style="cursor:default;color:#b3b3b3;">Reset Filters&nbsp;</a>
							<?php
								}
								else{
									?>
									<a class="pull-right clearallfilterbtn" onclick="clearAllFiltersfun()" style="cursor:pointer;">Reset Filters&nbsp;</a>
									<?php
								}
							?>
							</div>
						</div>
						
						
						
                    <?php
                    //echo $cat_current;
                    //echo $cat_current_id;
                    //$filter_box_infoRes=$controller->get_filter_box_info($cat_current,$cat_current_id);
                   // echo "<pre>";
				//	print_r($filter_box_infoRes);
					//echo "</pre>";
                    ?>
                    <div class="block_content collapse in" id="demo">
                        <!-- layered -->
                        <div class="layered">
						
						
						
						
						
						
						<?php
						
							$filter_prod_arr=array();
$filter_prod_arr_with_filter_arr = array();
	
 foreach ($cur_result as $searchRes) {
	$filter_prod_arr[$searchRes['_source']['inventory_id']]=$searchRes['_source']['inventory_id'];
	$filter_prod_arr_with_filter_arr[$searchRes['_source']['inventory_id']]=$searchRes['_source']['inventory_id'];
 }
						?>
						
						
						



                            <?php
							$checkbox_filterbox_display_name_arr=[];
							

                            //echo $type_of_filter;
                            $color_prev_arr = array();
                            
                            if (!empty($filter_box_infoRes)) {
                                foreach ($filter_box_infoRes as $filter_box_infoObj) {
                                    $filter_box_infoObj->filterbox_name = preg_replace('/\s+/', '_', $filter_box_infoObj->filterbox_name);
//print_r($filter_box_infoObj);
								if($filter_box_infoObj->type=="checkbox"){
															
                                    $count_filterbox_title = 0;
									if (strpos($filter_box_infoObj->filterbox_name, "_") !== false) {
										$filterbox_name_arr = explode("_", $filter_box_infoObj->filterbox_name);
										$filterbox_display_name=implode(" ", $filterbox_name_arr);
									} else {
										$filterbox_display_name=$filter_box_infoObj->filterbox_name;
									}
									
									if(in_array($filterbox_display_name,$checkbox_filterbox_display_name_arr)){
										continue;
									}
									else{
										$checkbox_filterbox_display_name_arr[]=$filterbox_display_name;
									}
									//echo $filterbox_display_name;
                                    ?>
                                    <div class="layered_subtitle <?php echo $filter_box_infoObj->filterbox_name . "_title"; ?>">
                                        <?php
											echo $filterbox_display_name;
										?>
										 <?php
										 
											$filterbox_units=$controller->get_filterbox_units(str_replace("_"," ",$filter_box_infoObj->filterbox_name)); 
											if($filterbox_units!=""){
												echo "<span style='text-transform:lowercase'>(".$filterbox_units.")</span>";
											}
										 
										 ?>
										
										
                                        

                                    </div>

                                    <div class="layered-content filter-<?php echo strtolower($filter_box_infoObj->filterbox_name); ?>">
                                        <?php
                                        $filter_infoRes = $controller->get_filter_info_by_filterboxname(str_replace("_"," ",$filter_box_infoObj->filterbox_name));
                                        ?>
                                        <ul class="check-box-list">
                                            <?php
											$checkbox_filter_display_name_arr=[];
                                            foreach ($filter_infoRes as $filter_infoObj) {
												
                                                if (strtolower($filter_box_infoObj->filterbox_name) == "color") {

                                                    $color_arr = explode(':', $filter_infoObj->filter_options);
                                                    $background = $color_arr[1];
                                                    $title = ucfirst($color_arr[0]);
                                                } else {

                                                    $background = ucfirst($filter_infoObj->filter_options);
                                                    $title = ucfirst($filter_infoObj->filter_options);
                                                }
                                                if (!isset($_REQUEST[$filter_box_infoObj->filterbox_name . "_filter"])) {
                                                    $color_filter = "";
                                                } else {
                                                    $color_filter = $_REQUEST[$filter_box_infoObj->filterbox_name . "_filter"];
                                                }


												
												if ($type_of_filter == $filter_box_infoObj->filterbox_name) {

                                                    $filter_box_infoObj->filterbox_name = preg_replace('/\s+/', '_', $filter_box_infoObj->filterbox_name);

                                                    $color_current_arr = explode(",", $_REQUEST[$filter_box_infoObj->filterbox_name . "_prev"]);

                                                    if ($veryfirst_condition_of_all_filters == "no") {
                                                        $check_current_arr_condition = in_array($filter_infoObj->filter_id, $color_current_arr);
                                                    } else {
                                                        $check_current_arr_condition = true;
                                                    }
                                                    if ($check_current_arr_condition) {
                                                        $color_prev_arr[$filter_box_infoObj->filterbox_name][$filter_infoObj->filter_id] = $filter_infoObj->filter_id;
                                                        $count_filterbox_title++;

                                                        $checked = "";
                                                        $fil_arr = explode("|", $_REQUEST[$filter_box_infoObj->filterbox_name . "_filter"]);


                                                        if (isset($_REQUEST[$filter_box_infoObj->filterbox_name . "_filter"])) {
                                                            if (in_array($filter_infoObj->filter_options, $fil_arr)) {
                                                                $checked = "checked";
                                                            }
                                                        }
														
														
														if(in_array($filter_infoObj->filter_options,$checkbox_filter_display_name_arr)){
															continue;
															
														}
														else{
															$checkbox_filter_display_name_arr[]=$filter_infoObj->filter_options;
														}
														

                                                        ?>
                                                        <li>
															<?php
																if($filter_box_infoObj->type=="checkbox"){
															?>
                                                            <input type="checkbox" id
                                                            ="<?php echo $filter_infoObj->filter_options; ?>"
                                                                   name="<?php echo $filter_box_infoObj->filterbox_name; ?>"
                                                                   value="<?php echo $filter_infoObj->filter_options; ?>"
                                                                   onchange="filterColorFun('<?php echo $filter_box_infoObj->filterbox_name; ?>','checkbox')" <?php echo $checked; ?> />


                                                            <label <?php if ($filter_box_infoObj->type == "select") { ?> style="background:<?php echo $background; ?>" <?php } ?>
                                                                    for="<?php echo $filter_infoObj->filter_options; ?>"><span
                                                                        class="button"></span><?php if ($filter_box_infoObj->type != "select") {
                                                                    echo ucfirst($filter_infoObj->filter_options);
                                                                } ?></label>
															<?php
																}
															?>

                                                        </li>
                                                        <?php
                                                    }

                                                } else {
													
													
                                               

                                                    $count_filter_id_for_products = $controller->get_filter_availability_for_products($cat_current, $cat_current_id, $filter_infoObj->filter_id, $filter_prod_arr, $filter_prod_arr_with_filter_arr, $type_of_filter, $color_filter);

                                                    $filter_box_infoObj->filterbox_name = preg_replace('/\s+/', '_', $filter_box_infoObj->filterbox_name);

                                                    if ($count_filter_id_for_products != 0) {

                                                        $color_prev_arr[$filter_box_infoObj->filterbox_name][$filter_infoObj->filter_id] = $filter_infoObj->filter_id;
                                                        $count_filterbox_title++;

                                                        $checked = "";

                                                        if (isset($_REQUEST[$filter_box_infoObj->filterbox_name . "_filter"])) {
                                                            $fil_arr = explode("|", $_REQUEST[$filter_box_infoObj->filterbox_name . "_filter"]);

                                                            if (in_array($filter_infoObj->filter_options, $fil_arr)) {
                                                                $checked = "checked";

                                                            }
                                                        }

									
														if(in_array($filter_infoObj->filter_options,$checkbox_filter_display_name_arr)){
															continue;
															
														}
														else{
															$checkbox_filter_display_name_arr[]=$filter_infoObj->filter_options;
														}
									
									

                                                        ?>
                                                        <li>
															<?php
																if($filter_box_infoObj->type=="checkbox"){
															?>
																<input type="checkbox" id
																="<?php echo $filter_infoObj->filter_options; ?>"
																	   name="<?php echo $filter_box_infoObj->filterbox_name; ?>"
																	   value="<?php echo $filter_infoObj->filter_options; ?>"
																	   onchange="filterColorFun('<?php echo $filter_box_infoObj->filterbox_name; ?>','checkbox')" <?php echo $checked; ?> <?php if (strtolower($filter_infoObj->filter_options) == strtolower($searched_key)) {
																	echo "checked disabled";
																} ?> />


																<label <?php if ($filter_box_infoObj->type == "select") { ?> style="background:<?php echo $background; ?>" title="<?php echo $title; ?>"  <?php } ?>
																		for="<?php echo $filter_infoObj->filter_options; ?>"><span
																			class="button"></span><?php
																	//echo strtoupper($filter_box_infoObj->type);
																	if ($filter_box_infoObj->type != "select") {
																		echo ucfirst($filter_infoObj->filter_options);
																	} ?></label>
															</li>
														<?php
																}
														?>
														
														
														
														
														
														
															
                                                        <?php
                                                    }
                                                
												 }

                                            }

                                            if ($count_filterbox_title == 0) {
                                                ?>
                                                <script>
                                                    document.getElementsByClassName("<?php echo $filter_box_infoObj->filterbox_name; ?>_title")[0].style.display = "none";
                                                    document.getElementsByClassName("<?php echo 'filter-' . strtolower($filter_box_infoObj->filterbox_name); ?>")[0].style.border = "0px solid #eaeaea;";
                                                    document.getElementsByClassName("<?php echo 'filter-' . strtolower($filter_box_infoObj->filterbox_name); ?>")[0].style.display = "none";
                                                </script>
                                            <?php
                                            }
                                            else{
                                            ?>
                                                <script>
                                                    document.getElementsByClassName("<?php echo 'filter-' . strtolower($filter_box_infoObj->filterbox_name); ?>")[0].style.border = "1px solid #eaeaea;";
                                                </script>
                                                <?php
                                            }
                                            ?>

                                        </ul>
                                    </div>
                                    <?php
								}
                                }


                            }
                            ?>
							
							
							
							

                            <?php

                            /* Price filter starts */
                            //echo "|".$type_of_filter."|";

                            ?>
                            <div class="layered_subtitle">
                                Price (<?php echo curr_sym; ?>)
                            </div>
                            <div class="range-slider">
                                <input type="text" class="js-range-slider" value=""/>
                            </div>
                            <hr>
                            <div class="extra-controls form-inline">
                                <div class="form-group">
                                    <input type="hidden" class="js-input-from form-control" id="min_val_of_price_filter"
                                           value=""/>
                                    <input type="hidden" class="js-input-to form-control" id="max_val_of_price_filter"
                                           value=""/>
                                </div>
                            </div>

                            <script>
                                function saveResult() {
									
//alert($("#min_val_of_price_filter").val()+" "+$("#max_val_of_price_filter").val())
                                    filterColorFun('Price','priceslider');
                                }

                                // Trigger

                                $(function () {

                                    var $range = $(".js-range-slider"),
                                        $inputFrom = $(".js-input-from"),
                                        $inputTo = $(".js-input-to"),
                                        instance,
                                        min = '<?php echo $default_min_val_of_price_filter;?>',
                                        max = '<?php echo $default_max_val_of_price_filter;?>',
                                        from = 0,
                                        to = 0;

                                    $range.ionRangeSlider({
                                        type: "double",
                                        min: min,
                                        max: max,
                                        from: '<?php echo $min_val_of_price_filter;?>',
                                        to: '<?php echo $max_val_of_price_filter;?>',
                                        prefix: '',
                                        onStart: updateInputs,
                                        onChange: updateInputs,
                                        onFinish: saveResult,
                                        step: 1,
                                        prettify_enabled: true,
                                        prettify_separator: ",",
                                        values_separator: " - ",
                                        force_edges: true


                                    });

                                    instance = $range.data("ionRangeSlider");

                                    function updateInputs(data) {
                                        from = data.from;
                                        to = data.to;

                                        $inputFrom.prop("value", from);
                                        $inputTo.prop("value", to);
                                    }

                                    /*$inputFrom.on("input", function () {
                                        var val = $(this).prop("value");

// validate
                                        if (val < min) {
                                            val = min;
                                        } else if (val > to) {
                                            val = to;
                                        }

                                        instance.update({
                                            from: val
                                        });
                                    });

                                    $inputTo.on("input", function () {
                                        var val = $(this).prop("value");

// validate
                                        if (val < from) {
                                            val = from;
                                        } else if (val > max) {
                                            val = max;
                                        }

                                        instance.update({
                                            to: val
                                        });
                                    });*/

                                });


                            </script>


                            <!--- Price filter ends --->







  <?php

                            /* singleslider filter starts */
                           
						   
								foreach($singleslider_arr as $singleslider_key => $singleslider_value){
									
									 if(!in_array(str_replace("_"," ",$singleslider_key),$filterbox_name_chosen_arr)){
										 continue;
									 }
							

                            ?>
                            <div class="layered_subtitle">
                               <?php echo str_replace("_"," ",$singleslider_key);?> <span style="text-transform:lowercase">(<?php echo $controller->get_filterbox_units(str_replace("_"," ",$singleslider_key)); ?>)</span>
                            </div>
                            <div class="range-slider">
                                <input type="text" class="js-range-slider-<?php echo $singleslider_key;?>" value=""/>
                            </div>
                            <hr>
                            <div class="extra-controls form-inline">
                                <div class="form-group">
                                    <input type="hidden" class="js-input-from-<?php echo $singleslider_key;?> form-control" id="min_val_of_<?php echo $singleslider_key;?>_filter"
                                           value=""/>
                                    <input type="hidden" class="js-input-to-<?php echo $singleslider_key;?> form-control" id="max_val_of_<?php echo $singleslider_key;?>_filter"
                                           value=""/>
                                </div>
                            </div>

                            <script>
                                
                                $(function () {

                                    var $range = $(".js-range-slider-<?php echo $singleslider_key;?>"),
                                        $inputFrom = $(".js-input-from-<?php echo $singleslider_key;?>"),
                                        $inputTo = $(".js-input-to-<?php echo $singleslider_key;?>"),
                                        instance,
                                        min = '<?php echo $singleslider_filter_arr[$singleslider_key]["default_min"];?>',
                                        max = '<?php echo $singleslider_filter_arr[$singleslider_key]["default_max"];?>',
                                        from = 0,
                                        to = 0;

                                    $range.ionRangeSlider({
                                        type: "single",
                                        min: min,
                                        max: max,
                                        from: '<?php echo $singleslider_filter_arr[$singleslider_key]["max"];?>',
                                        //to: '<?php echo $singleslider_filter_arr[$singleslider_key]["max"];?>',
                                        prefix: '',
                                        onStart: function (data) {
											from = data.from;
											to = data.to;
											$inputFrom.prop("value", '<?php echo $singleslider_filter_arr[$singleslider_key]["default_min"];?>');
											$inputTo.prop("value", from);
										},
                                        onChange: function (data) {
											from = data.from;
											to = data.to;
											$inputFrom.prop("value", '<?php echo $singleslider_filter_arr[$singleslider_key]["default_min"];?>');
											$inputTo.prop("value", from);
										},
                                        onFinish: function (data) {
											//alert($("#min_val_of_<?php echo $singleslider_key;?>_filter").val()+" "+$("#max_val_of_<?php echo $singleslider_key;?>_filter").val())
											filterColorFun('<?php echo $singleslider_key;?>','singleslider');
										},
                                        step: 1,
                                        prettify_enabled: true,
                                        prettify_separator: ",",
                                        values_separator: " - ",
                                        force_edges: true


                                    });

                                    instance = $range.data("ionRangeSlider");

                                  


                                });


                            </script>


                            <!--- singleslider filter ends --->
<?php
								}
?>










  <?php

                            /* doubleslider filter starts */
                             
								foreach($doubleslider_arr as $doubleslider_key => $doubleslider_value){
									if(!in_array(str_replace("_"," ",$doubleslider_key),$filterbox_name_chosen_arr)){
										 continue;
									 }

                            ?>
                            <div class="layered_subtitle">
                               <?php echo str_replace("_"," ",$doubleslider_key);?> <span style="text-transform:lowercase">(<?php echo $controller->get_filterbox_units(str_replace("_"," ",$doubleslider_key)); ?>)</span>
                            </div>
                            <div class="range-slider">
                                <input type="text" class="js-range-slider-<?php echo $doubleslider_key;?>" value=""/>
                            </div>
                            <hr>
                            <div class="extra-controls form-inline">
                                <div class="form-group">
                                    <input type="hidden" class="js-input-from-<?php echo $doubleslider_key;?> form-control" id="min_val_of_<?php echo $doubleslider_key;?>_filter"
                                           value=""/>
                                    <input type="hidden" class="js-input-to-<?php echo $doubleslider_key;?> form-control" id="max_val_of_<?php echo $doubleslider_key;?>_filter"
                                           value=""/>
                                </div>
                            </div>

                            <script>
                                
                                $(function () {

                                    var $range = $(".js-range-slider-<?php echo $doubleslider_key;?>"),
                                        $inputFrom = $(".js-input-from-<?php echo $doubleslider_key;?>"),
                                        $inputTo = $(".js-input-to-<?php echo $doubleslider_key;?>"),
                                        instance,
                                        min = '<?php echo $doubleslider_filter_arr[$doubleslider_key]["default_min"];?>',
                                        max = '<?php echo $doubleslider_filter_arr[$doubleslider_key]["default_max"];?>',
                                        from = 0,
                                        to = 0;

                                    $range.ionRangeSlider({
                                        type: "double",
                                        min: min,
                                        max: max,
                                        from: '<?php echo $doubleslider_filter_arr[$doubleslider_key]["min"];?>',
                                        to: '<?php echo $doubleslider_filter_arr[$doubleslider_key]["max"];?>',
                                        prefix: '',
                                        onStart: function (data) {
											from = data.from;
											to = data.to;
											$inputFrom.prop("value", from);
											$inputTo.prop("value", to);
										},
                                        onChange: function (data) {
											from = data.from;
											to = data.to;
											$inputFrom.prop("value", from);
											$inputTo.prop("value", to);
										},
                                        onFinish: function (data) {
											//alert($("#min_val_of_<?php echo $doubleslider_key;?>_filter").val()+" "+$("#max_val_of_<?php echo $doubleslider_key;?>_filter").val())
											filterColorFun('<?php echo $doubleslider_key;?>','doubleslider');
										},
                                        step: 1,
                                        prettify_enabled: true,
                                        prettify_separator: ",",
                                        values_separator: " - ",
                                        force_edges: true


                                    });

                                    instance = $range.data("ionRangeSlider");

                                  


                                });


                            </script>


                            <!--- doubleslider filter ends --->
<?php
								}
?>







                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block filter  -->

            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <?php
				
				$inventories_display_count=0;
				foreach ($cur_result as $searchRes) {
					if (isset($searchRes['_source']['inventory_id'])) {
						if(!empty($result_catalog_tree_view_inventory_id_arr)){
							if(!in_array($searchRes['_source']['inventory_id'],$result_catalog_tree_view_inventory_id_arr)){
								continue;
							}
						}
						if(isset($searchRes["_source"]["selling_price"])){
							$inventories_display_count++;
						}
					}
				}
				
				
                if (empty($cur_result)) {
                    ?>
                    <div id="view-product-list" class="view-product-list">
                        <div class="row" style="height:100vh;display:flex;align-items:center;text-align:center;">
                            <div class="col-md-12">
                                <h4>
                                    Your search did not yield any results. Try another one!
                                </h4>
                            </div>
                        </div>
                    </div>
                    <?php
                } 
				else if($inventories_display_count==0){
					?>
                    <div id="view-product-list" class="view-product-list">
                        <div class="row" style="height:100vh;display:flex;align-items:center;text-align:center;">
                            <div class="col-md-12">
                                <h4>
                                    Your search did not yield any results. Try another one!
                                </h4>
                            </div>
                        </div>
                    </div>
                    <?php
				}
				else {
										
                    ?>
                    <!-- view-product-list-->
                    <div id="view-product-list" class="view-product-list">

                    <div class="new_banner_container">
                        <div class="new_banner">
							<h4 class="text-center">Create your own basket of products with Quick Checkout</h4>
								<!--<p class="margin-top">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting </p>
							-->
                        </div>
                    </div>

                        <!-- breadcrumb -->

                        <div class="breadcrumb clearfix" style="display: none;">
                            <a class="home" href="<?php echo base_url(); ?>" title="Return to Home">Home</a>
                            <?php
                            if ($pcat_id != "") {
                                ?>

                                <?php

                                $parent_cat_infoObj = $controller->get_parent_cat_info($pcat_id);
                                if (!empty($parent_cat_infoObj)) {

                                    ?>
                                    <span class="navigation-pipe">&nbsp;</span>
                                    <span class="navigation_page">
<?php
$url1 = "{$url}{$var}{$s}{$rand1}{$parent_cat_infoObj->pcat_id}";
echo '<a href="' . $url1 . '" title="' . $parent_cat_infoObj->pcat_name . '">' . $parent_cat_infoObj->pcat_name . '</a>';
?>
</span>
                                    <?php
                                } else {
                                    $url1 = "{$url}{$var}{$s}{$rand1}{$pcat_id}";
//echo "All Categories";
                                }
                                ?>

                                <?php
                            }
                            ?>
                            <?php
                            if ($cat_id != "") {
                                ?>
                                <span class="navigation-pipe">&nbsp;</span>
                                <span class="navigation_page">
<?php
$cat_infoObj = $controller->get_cat_info($cat_id);
$url2 = "{$url1}{$s}{$rand2}{$cat_infoObj->cat_id}";
//echo $cat_infoObj->cat_name;
echo '<a href="' . $url2 . '" title="' . $cat_infoObj->cat_name . '">' . $cat_infoObj->cat_name . '</a>';
?>
</span>
                                <?php
                            }
                            ?>
                            <?php
                            if ($subcat_id != "") {
                                ?>
                                <span class="navigation-pipe">&nbsp;</span>
                                <span class="navigation_page">
<?php
$subcat_infoObj = $controller->get_subcat_info($subcat_id);
$url3 = "{$url2}{$s}{$rand3}{$subcat_infoObj->subcat_id}";
//echo $subcat_infoObj->subcat_name;
echo '<a href="' . $url3 . '" title="' . $subcat_infoObj->subcat_name . '">' . $subcat_infoObj->subcat_name . '</a>';
?>
</span>
                                <?php
                            }
                            ?>
                        </div>
                        <!-- ./breadcrumb -->

                        <h2 class="page-heading" style="display: none;">
<span class="page-heading-title">
<!-- page title starts -->
<?php
if ($subcat_id != "") {

    $subcat_infoObj = $controller->get_subcat_info($subcat_id);
    echo $subcat_infoObj->subcat_name;

} else if ($cat_id != "") {

    $cat_infoObj = $controller->get_cat_info($cat_id);
    echo $cat_infoObj->cat_name;

} else if ($pcat_id != "") {
    $parent_cat_infoObj = $controller->get_parent_cat_info($pcat_id);
    if (!empty($parent_cat_infoObj)) {
        echo $parent_cat_infoObj->pcat_name;
    } else {
        echo "All Categories";
    }
}

?>
<!-- page title ends -->
</span>
                        </h2>
						
						
						
						
	




                        <div id="top-sortPagi">
                            <div class="sortPagiBar display-sortP-option">
                                <!--<div class="show-product-item">
<select name="page_rows_limit" onchange="page_rows_limitFun(this)">
<option value="5" <?php //if($page_rows==5){echo "selected";}
                                ?>>Show 5</option>
<option value="10" <?php //if($page_rows==10){echo "selected";}
                                ?>>Show 10</option>
<option value="15" <?php //if($page_rows==15){echo "selected";}
                                ?>>Show 15</option>
<option value="20" <?php //if($page_rows==20){echo "selected";}
                                ?>>Show 20</option>
</select>
</div>-->
                                <div class="sort-product">
                                    <ul class="list-inline">
                                        <li><strong>Sort By</strong></li>
                                        <!-- <li>
                                            <a onclick="sorting_byFun('default|SORT_ASC')" <?php //if ($sorting_by == "default|SORT_ASC") {
                                                //echo "class='active'";
                                            //} ?>>Product Name</a></li>
                                        <li> -->
										<li>
                                            <a href="javascript:void(0)" onclick="sorting_byFun('selling_price|SORT_ASC')" <?php if ($sorting_by == "selling_price|SORT_ASC") {
                                                echo "class='active'";
                                            } ?>>Price Low-High</a></li>
                                        <li>
                                            <a href="javascript:void(0)" onclick="sorting_byFun('selling_price|SORT_DESC')" <?php if ($sorting_by == "selling_price|SORT_DESC") {
                                                echo "class='active'";
                                            } ?>>Price High-Low</a></li>
										<li><a><?php
											echo "Total Products(".$total_count_skus_indexed_in_elasticsearch.")";
										?></a></li>
										<li><a><?php
											echo "Filtered Products(".count($results).")";
										?></a></li>
										<li><a><?php echo "Current Display(".$inventories_display_count.")"; ?></a></li>
										<li class="pull-right" style="padding-right:2rem;">
											<?php if($is_filter_applied=="no"){ ?>
												<button class="btn btn-primary btn-sm pull-right text-right" type="button" onclick="chooseCategoryTreeViewFun()">FILTERS</button>
											<?php } ?>
											<?php if($is_filter_applied=="yes"){ ?>
												<button class="btn btn-danger btn-sm pull-right text-right" type="button" onclick="chooseCategoryTreeViewClearFun()">CLEAR FILTERS</button>
											<?php } ?>
											
										</li>
											
											
                                    </ul>
                                    <!--<select name="sorting_by_field" onchange="sorting_byFun(this)">
<option value="default|SORT_ASC"  <?php //if($sorting_by=="default|SORT_ASC"){echo "selected";}
                                    ?>>Sort By Name</option>
<option value="product_name|SORT_ASC" <?php //if($sorting_by=="product_name|SORT_ASC"){echo "selected";}
                                    ?>>Product Name</option>
<option value="selling_price|SORT_ASC" <?php //if($sorting_by=="selling_price|SORT_ASC"){echo "selected";}
                                    ?>>Price Low-High</option>
<option value="selling_price|SORT_DESC" <?php //if($sorting_by=="selling_price|SORT_DESC"){echo "selected";}
                                    ?>>Price High-Low</option>
</select>-->
                                </div>
                            </div>
                        </div>
                        <!---ff--->
                        <hr class="line-break">
                        <!---ff--->

                        <!-- <ul class="display-product-option">
<li class="view-as-grid selected">
<span>grid</span>
</li>
<li class="view-as-list">
<span>list</span>
</li>
</ul>-->





                        <!-- PRODUCT LIST -->
                        <ul class="row product-list grid">
                            <?php
                            //shuffle($cur_result);
							
                            foreach ($cur_result as $searchRes) {
                                if (isset($searchRes['_source']['inventory_id'])) {
										/*if(!empty($result_catalog_tree_view_inventory_id_arr)){
											if(!in_array($searchRes['_source']['inventory_id'],$result_catalog_tree_view_inventory_id_arr)){
												continue;
											}
										}*/

                                    if(isset($searchRes["_source"]["selling_price"])){

                                    $inventory_id = $searchRes['_source']['inventory_id'];
                                    $product_id = $searchRes['_source']['product_id'];
                                    $selling_price = $searchRes["_source"]["selling_price"];
                                    $max_selling_price = $searchRes["_source"]["max_selling_price"];
                                    $selling_discount = $searchRes["_source"]["selling_discount"];

                                    $inv_discount_data = $controller->get_default_discount_for_inv($inventory_id, $max_selling_price, $product_id);
///////////////  No of Offers starts ///////////////
                                    $promo_numbers = 0;
                                    $promotions = $controller->get_number_of_offers($inventory_id);
                                    //echo $inventory_id;
                                    $inventory_product_info_obj = $controller->get_inventory_product_info_by_inventory_id($inventory_id);

                                    if (!empty($promotions) && ($inventory_product_info_obj->stock_available > 0)) {
                                        $i = 1;
                                        $defaultDiscounts_num = 0;
                                        foreach ($promotions as $promotion) {
                                            foreach ($promotion as $promo) {

                                                if (preg_match_all('/\d+(?=%)/', $promo->get_type, $match) && ($promo->to_buy == 1)) {
                                                    $i++;
                                                    if ($promo->to_buy == 1) {
                                                        $defaultDiscounts_num = 1;
                                                    }
                                                }
                                            }
                                        }

                                        if ($defaultDiscounts_num != 0) {
                                            $promo_numbers = count($promotions) - $defaultDiscounts_num;
                                        } else {
                                            $promo_numbers = count($promotions);
                                        }

                                    }
                                    
                                    /*added*/
                                    if (empty($inv_discount_data) && ($selling_discount>0)){
                                        //$promo_numbers=($promo_numbers+1);
                                    }
                                    /*added*/
//////////////   No of offers ends    ///////////////
									
                                    ?>
                                    <li class="col-xs-6 col-sm-3">
                                        <div class="product-container selectProduct"
                                             data-id="<?php echo ucwords($searchRes["_source"]["product_name"]) ?>"
                                             data-title="<?php echo $searchRes['_source']['inventory_id']; ?>">
                                            <div class="left-block">
                                                <?php
                                                //echo $searchRes['_id'];
                                                $rand_1 = $controller->sample_code();
                                                $rand_2 = $controller->sample_code();
                                                ?>

                                                <a href="<?php echo base_url() ?>detail/<?php echo "{$rand_1}{$searchRes['_source']['inventory_id']}"; ?>">


                                                    <img class="img-responsive productImg_<?php echo $searchRes['_source']['inventory_id']; ?>"
                                                         alt="productsku"
                                                         src="<?php echo base_url() . $searchRes["_source"]["common_image"] ?>"/>

                                                </a>
                                                <div class="quick-view">
                                                    <?php
                                                    if ($this->session->userdata("customer_id")) {
                                                        if ($controller->check_wishlist($searchRes['_source']['inventory_id']) > 0) {
                                                            ?>
                                                            <script>
                                                                $(document).ready(function () {
                                                                    $("#wishlist_" +<?php echo $searchRes['_source']['inventory_id'];?>).css({"color": ""});
                                                                    $("#wishlist_" +<?php echo $searchRes['_source']['inventory_id'];?>).css({"color": "#ff4343;"});
                                                                });
                                                            </script>
                                                            <?php
                                                            $val = "Added to Wishlist";
                                                            $style_wishlist_label = "style='color:#eda900;'";
                                                        } else {
                                                            $val = "Add to Wishlist";
                                                            $style_wishlist_label = "";
                                                        }
                                                    } else {
                                                        $val = "Add to Wishlist";
                                                        $style_wishlist_label = "";
                                                    }
                                                    ?>
                                                    <?php
                                                    if ($this->session->userdata("customer_id")) {
                                                        ?>
                                                        <span style="cursor:pointer;"
                                                              id="anchor_<?php echo $searchRes['_source']['inventory_id']; ?>"
                                                              onclick="add_to_wishlistFun(<?php echo $searchRes['_source']['inventory_id']; ?>);return false;"
                                                              title="<?php echo $val; ?>" class="wishlist">
<i id="<?php echo "wishlist_" . $searchRes['_source']['inventory_id']; ?>" class="fa fa-heart"
   style='color:#c2c2c2;font-size: 1.2em;'></i>
</span>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <span style="cursor:pointer"
                                                              id="anchor_<?php echo $searchRes['_source']['inventory_id']; ?>"
                                                              onclick="add_to_wishlistFun(<?php echo $searchRes['_source']['inventory_id']; ?>);return false;"
                                                              title="<?php echo $val; ?>" class="wishlist"><i
                                                                    id="<?php echo "wishlist_" . $searchRes['_source']['inventory_id']; ?>"
                                                                    class="fa fa-heart"
                                                                    style='color:#c2c2c2;font-size: 1.2em;'></i></span>
                                                        <?php
                                                    }
                                                    ?>

                                                </div>

                                                <?php //if (!empty($inv_discount_data)) { ?>
                                                <?php if (0) { ?>
                                                    <div class="price-percent-reduction2" <?php if ($inv_discount_data['discount'] < 10) {
                                                        echo "style='padding-top:18px'";
                                                    } ?>>
                                                        <?php echo $inv_discount_data['discount'] ?>% OFF


                                                    </div>
                                                <?php }else{ ?>

                                                <?php
                                                if($combo_pack_discount>0){
                                                            ?>
                                                    <div class="price-percent-reduction2" <?php if ($combo_pack_discount < 10) {
                                                        //echo "style='padding-top:18px'";
                                                    } ?>>
                                                        <?php echo round($combo_pack_discount); ?>% OFF
                                                    </div>

                                                        <?php
                                                }
                                                ?>
                                                <?php
                                                }
                                                if ($promo_numbers > 0) {
                                                    ?>
                                                    <div class="price-percent-reduction_offers">
                                                        <?php

                                                        if ($promo_numbers > 1) {
                                                            echo $promo_numbers . ' Offers';
                                                        } else {
                                                            echo $promo_numbers . ' Offer';
                                                        }
                                                        ?>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                            </div>

                                            <div class="right-block heightmatched_sku">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <h5 id="product_name_<?php echo $searchRes['_source']['inventory_id']; ?>" class="product-name heightmatched"
                                                            style="font-size:1.05em;padding-bottom:0!important;word-break:break-word;line-height:1.3;">
                                                            <a href="<?php echo base_url() ?>detail/<?php echo "{$rand_1}{$searchRes['_source']['inventory_id']}"; ?>"
                                                               title="<?php
                                                               //$str = $searchRes["_source"]["product_name"];
                                                               $str = ($searchRes["_source"]["sku_name"] !='') ? $searchRes["_source"]["sku_name"] : $searchRes["_source"]["product_name"];
                                                               $str = str_replace("\r\n", '', $str);
                                                               $str = str_replace('\r\n', '', $str);
                                                               echo ucwords($str);

                                                               ?>">

                                                                <?php
                                                                
                                                                //$str = $searchRes["_source"]["product_name"];
                                                                $str = ($searchRes["_source"]["sku_name"] !='') ? $searchRes["_source"]["sku_name"] : $searchRes["_source"]["product_name"];
                                                                $str = str_replace("\r\n", '', $str);
                                                                $str = str_replace('\r\n', '', $str);
                                                                echo ucwords($str);
                                                                ?>

                                                                <!---added attributes ---->
                                                                <span style="<?php echo ($searchRes["_source"]["sku_name"]!='') ? 'display:none;' : ''; ?>">
                                                                <?php
                                                                $attr_str='';
                                                                $color_attribute_is_there = "no";

                                                                if(isset($inventory_product_info_obj->attribute_1)){
                                                                if (strtolower($inventory_product_info_obj->attribute_1) == "color") {
                                                                    $color_attribute_is_there = "yes";
                                                                    $attr_str.= " - " . explode(":", $inventory_product_info_obj->attribute_1_value)[0];
                                                                } if (strtolower($inventory_product_info_obj->attribute_2) == "color") {
                                                                    $color_attribute_is_there = "yes";
                                                                    $attr_str.= " - " . explode(":", $inventory_product_info_obj->attribute_2_value)[0];
                                                                } if (strtolower($inventory_product_info_obj->attribute_3) == "color") {
                                                                    $color_attribute_is_there = "yes";
                                                                    $attr_str.= " - " . explode(":", $inventory_product_info_obj->attribute_3_value)[0];
                                                                } if (strtolower($inventory_product_info_obj->attribute_4) == "color") {
                                                                    $color_attribute_is_there = "yes";
                                                                    $attr_str.= " - " . explode(":", $inventory_product_info_obj->attribute_4_value)[0];
                                                                }
                                                                ?>
                                                                <?php
                                                                if ($color_attribute_is_there == "yes") {
                                                                    if (strtolower($inventory_product_info_obj->attribute_1) != "color") {
                                                                        if ($inventory_product_info_obj->attribute_1_value != "") {
                                                                            $attr_str.= " - " . $inventory_product_info_obj->attribute_1_value;
                                                                        }
                                                                    } if (strtolower($inventory_product_info_obj->attribute_2) != "color") {
                                                                        if ($inventory_product_info_obj->attribute_2_value != "") {
                                                                            $attr_str.= " - " . $inventory_product_info_obj->attribute_2_value;
                                                                        }
                                                                    } if (strtolower($inventory_product_info_obj->attribute_3) != "color") {
                                                                        if ($inventory_product_info_obj->attribute_3_value != "") {
                                                                            $attr_str.= " - " . $inventory_product_info_obj->attribute_3_value;
                                                                        }
                                                                    } if (strtolower($inventory_product_info_obj->attribute_4) != "color") {
                                                                        if ($inventory_product_info_obj->attribute_4_value != "") {
                                                                            $attr_str.= " - " . $inventory_product_info_obj->attribute_4_value;
                                                                        }
                                                                    }
                                                                } else {
                                                                    $noncolor_attribute_first_is_there = "no";
                                                                    $noncolor_attribute_second_is_there = "no";

                                                                    if (strtolower($inventory_product_info_obj->attribute_1) != "color") {
                                                                        $noncolor_attribute_first_is_there = "yes";
                                                                        $attr_str.= " - " . $inventory_product_info_obj->attribute_1_value;
                                                                    }
                                                                    if (strtolower($inventory_product_info_obj->attribute_2) != "color") {
                                                                        if ($noncolor_attribute_first_is_there == "yes") {
                                                                            $noncolor_attribute_second_is_there = "yes";
                                                                            if ($inventory_product_info_obj->attribute_2_value != "") {
                                                                                $attr_str.= " - " . $inventory_product_info_obj->attribute_2_value;
                                                                            }
                                                                        } else {
                                                                            $noncolor_attribute_first_is_there = "yes";
                                                                            $attr_str.= " - " . $inventory_product_info_obj->attribute_2_value;
                                                                        }
                                                                    }
                                                                    if (strtolower($inventory_product_info_obj->attribute_3) != "color") {
                                                                        if ($noncolor_attribute_first_is_there == "no" || $noncolor_attribute_second_is_there == "no") {
                                                                            if ($noncolor_attribute_first_is_there == "yes") {
                                                                                $noncolor_attribute_second_is_there = "yes";
                                                                                if ($inventory_product_info_obj->attribute_3_value != "") {
                                                                                    $attr_str.= " - " . $inventory_product_info_obj->attribute_3_value;
                                                                                }
                                                                            } else {
                                                                                $noncolor_attribute_first_is_there = "yes";
                                                                                $attr_str.= " - " . $inventory_product_info_obj->attribute_3_value;
                                                                            }
                                                                        }
																		else{
																			if ($inventory_product_info_obj->attribute_3_value != "") {
																				$attr_str.= " - " . $inventory_product_info_obj->attribute_3_value;
																			}
																		}
                                                                    }
                                                                    if (strtolower($inventory_product_info_obj->attribute_4) != "color") {
                                                                        if ($noncolor_attribute_first_is_there == "no" || $noncolor_attribute_second_is_there == "no") {
                                                                            if ($noncolor_attribute_first_is_there == "yes") {
                                                                                $noncolor_attribute_second_is_there = "yes";
                                                                                if ($inventory_product_info_obj->attribute_4_value != "") {
                                                                                    $attr_str.= " - " . $inventory_product_info_obj->attribute_4_value;
                                                                                }
                                                                            } else {
                                                                                $noncolor_attribute_first_is_there = "yes";
                                                                                $attr_str.= " - " . $inventory_product_info_obj->attribute_4_value;
                                                                            }
                                                                        }
																		else{
																			if ($inventory_product_info_obj->attribute_4_value != "") {
																				$attr_str.= " - " . $inventory_product_info_obj->attribute_4_value;
																			}
																		}
                                                                    }
                                                                }
                                                                }
                                                                $color_attribute_is_there = "no";
                                                                $noncolor_attribute_first_is_there = "no";
                                                                $noncolor_attribute_second_is_there = "no";

                                                                echo $attr_str;
                                                                ?>
                                                                <!---added attributes ---->

                                                                </span>
                                                            </a>
                                                        </h5>
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <h5 class="product-name pb-2"
                                                            style="font-size:1em;color:#95918c;">
                                                            <b><?php echo "{$searchRes['_source']['brand_name']}"; ?></b>
                                                        </h5>
                                                        <?php
                                                        /*
<h5 class="attribute-col">
<?php

if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_1"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_1_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_2"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_2_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_3"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_3_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_4"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_4_value"])[0];
}



if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_1"])!="color"){
if($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_1_value"]!=""){
echo "<h5 class='attribute-col' style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_1_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_2"])!="color"){
if($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_2_value"]!=""){
echo "<h5 class='attribute-col'  style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_2_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_3"])!="color"){
if($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_3_value"]!=""){
echo "<h5 class='attribute-col'  style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_3_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_4"])!="color"){
if($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_4_value"]!=""){
echo "<h5 class='attribute-col' style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_4_value"])."</h5>";
}
}



?>
</h5>
*/
                                                        ?>
                                                    </div>
                                                </div>


                                                <div class="content_price">
                                                    <?php //if (!empty($inv_discount_data)) { ?>
                                                    <?php if (0) { ?>
                                                        <span class="price product-price"><?php echo curr_sym; ?><?php echo $inv_discount_data['current_price'] ?></span>
                                                        <span class="price old-price"><?php echo curr_sym; ?><?php echo $inv_discount_data['inventory_price'] ?></span>
                                                        <small style="line-height: 1.8;">(Incl. of Taxes)</small>
                                                    <?php } else { 

                                                        $max_selling_price =$searchRes["_source"]["max_selling_price"];
                                                        $selling_price =$searchRes["_source"]["selling_price"];
                                                        $selling_discount =$searchRes["_source"]["selling_discount"];

                                                        $inventory_id=$searchRes["_source"]["inventory_id"];
                                                        $product_id=$searchRes["_source"]["product_id"];

                                                        ?>

                                                        <input type="hidden" value="<?php echo $max_selling_price; ?>" id="max_selling_price_<?php echo $inventory_id; ?>">
                                                        <input type="hidden" value="<?php echo $selling_price; ?>" id="selling_price_<?php echo $inventory_id; ?>">
                                                        <input type="hidden" value="<?php echo $selling_discount; ?>" id="selling_discount_<?php echo $inventory_id; ?>">

                                                        <?php
                                                        //echo $inventory_id.'||'.$product_id;
                                                        
                                                        $qty_promo_dis=array(); $qty_arr=array();$qty_dis=array();$promo_uid_arr=array();$promo_quote_arr=array();

                                                       if(!empty($inv_qty_discount_data[$inventory_id])){

                                                        $qty_promo_dis=$inv_qty_discount_data[$inventory_id];

                                                        // echo '<pre>';
                                                        // print_r($qty_promo_dis);
                                                        // echo '</pre>';


                                                        foreach($qty_promo_dis as $qp){
                                                            //print_r($qp);

                                                            $promo_quote=$qp["promo_quote"];
                                                            $promo_uid=$qp["promo_uid"];
                                                            $to_buy=$qp["promo_list_to_buy"];
                                                            $to_get=$qp["promo_list_to_get"];
                                                            $buy_type=$qp["promo_list_to_buy_type"];
                                                            $get_type=$qp["promo_list_to_get_type"];
                                                            $discount=$qp["promo_list_discount"];
                                                            $promo_uid_arr[]=$promo_uid;
                                                            $promo_quote_arr[]=$promo_quote;
                                                            $qty_arr[]=$to_buy;
                                                            $qty_dis[]=$discount;

                                                        ?>

                                
                                <input type="hidden" class="promo_uid_<?php echo $inventory_id; ?>" name="promo_uid_<?php echo $inventory_id; ?>_<?php echo $promo_uid;?>" id="promo_uid_<?php echo $inventory_id; ?>_<?php echo $promo_uid?>" value="<?php echo $promo_uid?>">
                                
                                <input type="hidden" class="promo_quote_<?php echo $inventory_id; ?>" name="promo_quote_<?php echo $inventory_id; ?>_<?php echo $promo_uid;?>" id="promo_quote_<?php echo $inventory_id; ?>_<?php echo $promo_uid?>" value="<?php echo $promo_quote;?>">
                                
                                <input type="hidden" class="promo_list_to_buy_<?php echo $inventory_id; ?>" name="promo_list_to_buy_<?php echo $inventory_id; ?>_<?php echo $promo_uid;?>" id="promo_list_to_buy_<?php echo $inventory_id; ?>_<?php echo $promo_uid?>" value="<?php echo $to_buy?>">

								 <input type="hidden" class="promo_list_to_get_<?php echo $inventory_id; ?>" name="promo_list_to_get_<?php echo $inventory_id; ?>_<?php echo $promo_uid?>" id="promo_list_to_get_<?php echo $inventory_id; ?>_<?php echo $promo_uid?>" value="<?php echo $to_get?>">

								 <input type="hidden" class="promo_list_to_buy_type_<?php echo $inventory_id; ?>" name="promo_list_to_buy_type_<?php echo $inventory_id; ?>_<?php echo $promo_uid?>" id="promo_list_to_buy_type_<?php echo $inventory_id; ?>_<?php echo $promo_uid?>" value="<?php echo $buy_type?>">

								 <input type="hidden" class="promo_list_to_get_type_<?php echo $inventory_id; ?>" name="promo_list_to_get_type_<?php echo $inventory_id; ?>_<?php echo $promo_uid?>" id="promo_list_to_get_type_<?php echo $inventory_id; ?>_<?php echo $promo_uid?>" value="<?php echo $get_type?>">
                                 
                                 <input type="hidden" class="promo_list_discount_<?php echo $inventory_id; ?>" name="promo_list_discount_<?php echo $inventory_id; ?>_<?php echo $promo_uid?>" id="promo_list_discount_<?php echo $inventory_id; ?>_<?php echo $promo_uid?>" value="<?php echo $discount?>">

                                 <?php 
                                 

                                 $st='style="display:none"';

                                 ?>
                                                            <div class="text-info quote_class_<?php echo $inventory_id; ?>" id="quote_<?php echo $inventory_id; ?>_<?php echo $promo_uid?>"><small><i id="fa_quote_<?php echo $inventory_id; ?>_<?php echo $promo_uid?>" <?php echo $st ?> class="fa fa-check-circle  text-success fa_class_<?php echo $inventory_id; ?>" aria-hidden="true" ></i> <?php echo $promo_quote; ?></small></div>
                                                        
                                                        <?php

                                                        }

                                                       }
 

                                                        $original_price=$max_selling_price;
                                                        $promo_qts_str='';$promo_dis_str='';$promo_ids_str='';

                                                        /* onload - check moq matches qty promo */
                                                        $dis_percentage=0;$promo_uid_selected=''; $multiplier=0;$residue=0;$p_qty=0;$promo_quote_selected='';
                                                        $moq=$inventory_product_info_obj->moq;
                                                        if(!empty($qty_arr)){
                                                            
                                                            if(in_array($moq,$qty_arr)){
                                                                $key=array_search($moq, $qty_arr);
                                                                if(isset($qty_dis[$key])){
                                                                    $dis_percentage=$qty_dis[$key];
                                                                    $promo_uid_selected=$promo_uid_arr[$key];
                                                                    $promo_quote_selected=$promo_quote_arr[$key];
                                                                    $multiplier=1; //promo and qty input is same
                                                                    $p_qty=$moq;
                                                                }
                                                            }else{
                                                                
                                                                $newNumbers = array_filter(
                                                                    $qty_arr,
                                                                    function ($value) use($moq) {
                                                                        return ($moq > $value);
                                                                    }
                                                                );

                                                                //print_r($newNumbers);

                                                                if(!empty($newNumbers)){
                                                                    rsort($newNumbers);

                                                                    if(isset($newNumbers[0])){
                                                                        $p_qty=$newNumbers[0];

                                                                        if(in_array($p_qty,$qty_arr)){
                                                                            $key=array_search($p_qty, $qty_arr);
                                                                            if(isset($qty_dis[$key])){
                                                                                $dis_percentage=$qty_dis[$key];
                                                                                $promo_uid_selected=$promo_uid_arr[$key];
                                                                                $promo_quote_selected=$promo_quote_arr[$key];
                                                                                $residue = ($moq % $p_qty);
                                                                                $multiplier = (($moq - $residue) / $p_qty);

                                                                            }
                                                                        }
                                                                    }



                                                                }
                                                                
                                                            }

                                                            
                                                            $promo_qts_str='';
                                                            $promo_dis_str.=$dis_percentage;
                                                            $promo_ids_str='';

                                                        }else{
                                                            /* No qty promotion */
                                                            $residue=$moq;
                                                            /* No qty promotion */
                                                        }
                                                        $max_price=$max_selling_price;

                                                        //echo "dis = ".$dis_percentage;

                                                        if($dis_percentage>0){
                                                            if($dis_percentage>0){

                                                                //echo $residue."--".$multiplier;

                                                                
                                                                if($multiplier>0 && $residue==0){
                                                                    $sel_price=($p_qty*round($max_price-($max_price*$dis_percentage/100)));

                                                                }else{

                                                                    $sel_price1=($p_qty*round($max_price-($max_price*$dis_percentage/100)));
                                                                    $sel_price2=($residue*$searchRes["_source"]["selling_price"]);

                                                                    $sel_price=($sel_price1+$sel_price2);
                                                                }

                                                            }else{
                                                                $sel_price= ($moq*$searchRes["_source"]["selling_price"]);
                                                            }

                                                        }else{

                                                            $sel_price=($moq*$searchRes["_source"]["selling_price"]);
                                                           
                                                        }

                                                        //$sel_price=($sel_price*$moq);
                                                        $max_price=($max_price*$moq);

                                                        $selling_discount=$searchRes["_source"]["selling_discount"];

                                                        /* onload - check moq matches qty promo */
                                                        
                                                        if (get_pricetobeshown() == "selling_price") {
                                                            ?>
                                                            <span class="price product-price"><?php echo curr_sym; ?><span id="sellingprice_<?php echo $inventory_product_info_obj->id; ?>"><?php echo $sel_price; ?></span></span>
                                                            
                                                            <?php
                                                            if(intval($sel_price)<intval($max_price)){

                                                            ?>
                                                            <span class="price old-price" id="strike_price_<?php echo $inventory_id; ?>"><?php echo curr_sym; ?><?php echo round($max_price); ?></span>
                                                            <?php

                                                            }
                                                            ?>
                                                            <small>(Incl. of Taxes)</small>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <span class="price product-price"><?php echo curr_sym; ?><?php echo $searchRes["_source"]["base_price"] ?> <font
                                                                        style="font-size:0.6em;line-height:2.4"> + GST</font></span>
                                                            <small>(Incl. of Taxes)</small>
                                                            <?php
                                                        }
                                                        ?>

                                                    <?php } ?>



                                                    <?php if($promo_uid_selected!=''){
?>
                                                            <script type="text/javascript">

                                                            $(document).ready(function(){
                                                                //select_promo_onload('<?php echo $inventory_id ?>','<?php echo $promo_uid_selected; ?>');
                                                                inventory_id='<?php echo $inventory_id ?>';
                                                                promo_uid_selected='<?php echo $promo_uid_selected ?>';
                                                                $('#fa_quote_'+inventory_id+"_"+promo_uid_selected).show();
                                                                $('#quote_'+inventory_id+"_"+promo_uid_selected).css({"color": "green","font-weight":"500"});
                                                            })
                                                            </script>

                                                                <?php 

                                                    } ?>

                                                </div>
												<div class="attributes row mb-10" style="line-height: 2.4;">

                                                <div class="col-md-12">
                                                    <div class="attribute-label widthmatched" style="display:inline-block;"></div>

                                                    <div class="input-group qty-arrange" style="display:inline-block;">
                                                        <!-- <input onkeyup="incrementdecrementtimerfun(<?php echo $inventory_product_info_obj->moq; ?>,<?php echo $inventory_product_info_obj->max_oq; ?>,this,<?php echo $inventory_id; ?>,'<?php echo $sel_price; ?>');"
                                                               class="form-control input-number text-center"
                                                               id="purchase_qty_input_<?php echo $inventory_id; ?>" allow-only-numbers type="number"
                                                               value="<?php echo $inventory_product_info_obj->moq; ?>" style="max-width:5em"
                                                               ng-paste="$event.preventDefault()"
                                                               maxlength="{{qty_maxLength}}"
                                                               onkeypress="return isNumber(event)"
                                                               length-checker="<?php echo $inventory_product_info_obj->moq; ?>"
                                                               onblur="inventory_moq_resetFun(<?php echo $inventory_product_info_obj->moq; ?>,<?php echo $inventory_id; ?>);incrementdecrementtimerfun(<?php echo $inventory_product_info_obj->moq; ?>,<?php echo $inventory_product_info_obj->max_oq; ?>,this,<?php echo $inventory_id; ?>,'<?php echo $sel_price; ?>');"> -->

                                                        <!-- quantity design changes--->

                                                        <div class="row ">
                                                        <!-- <div class="sp-qty-text col-md-12 col-xs-12 text-center" >Qty</div> -->
                                                        <div class="sp-quantity col-md-12  col-xs-12 text-center">
                                                            

                                                            <div class="sp-minus fff"> <a class="qty_sym" min_oq="<?php echo $inventory_product_info_obj->moq; ?>" max_oq="<?php echo $inventory_product_info_obj->max_oq; ?>" href="#" inv_id="<?php echo $inventory_id; ?>" sel_price="<?php echo $sel_price; ?>" selling_discount="<?php echo $selling_discount; ?>">-</a>
                                                            </div>
                                                            <div class="sp-input">
                                                                <input type="text" class="quntity-input" 
                                                                onkeyup="incrementdecrementtimerfun(<?php echo $inventory_product_info_obj->moq; ?>,<?php echo $inventory_product_info_obj->max_oq; ?>,this.value,<?php echo $inventory_id; ?>,'<?php echo $sel_price; ?>');"
                                                                id="purchase_qty_input_<?php echo $inventory_id; ?>" 
                                                                allow-only-numbers 
                                                                value="<?php echo $inventory_product_info_obj->moq; ?>" 
                                                                ng-paste="$event.preventDefault()" maxlength="{{qty_maxLength}}"
                                                               onkeypress="return isNumber(event)"
                                                               length-checker="<?php echo $inventory_product_info_obj->moq; ?>"
                                                               onblur="inventory_moq_resetFun(<?php echo $inventory_product_info_obj->moq; ?>,<?php echo $inventory_id; ?>);incrementdecrementtimerfun(<?php echo $inventory_product_info_obj->moq; ?>,<?php echo $inventory_product_info_obj->max_oq; ?>,this.value,<?php echo $inventory_id; ?>,'<?php echo $sel_price; ?>');" />

                                                            </div>
                                                            <div class="sp-plus fff"> <a class="qty_sym" href="#" min_oq="<?php echo $inventory_product_info_obj->moq; ?>" max_oq="<?php echo $inventory_product_info_obj->max_oq; ?>" inv_id="<?php echo $inventory_id; ?>" sel_price="<?php echo $sel_price; ?>" selling_discount="<?php echo $selling_discount; ?>">+</a>
                                                            </div>
                                                        </div>
                                                        </div><!---row-->

                                                        <!-- quantity design changes--->

                                                    </div>
												<p style="color:#f00" id="quantity_status_info_<?php echo $inventory_id; ?>">
										<span style="color:#ff8000;"><small>Enter qty between <?php echo $inventory_product_info_obj->moq;?> and <?php echo $inventory_product_info_obj->max_oq;?></small></span>

									</p>
                                                </div>
                                            </div>
                                                <?php
                                                if (get_pricetobeshown() == "selling_price") {
                                                    ?>

                                                    <!-- <div class="row" style="display:none;">
                                                        <div class="col-md-12">
                                                          
                                                                <div class="checkbox mb-0">
                                                                    <label for="inventory_<?php echo $inventory_id; ?>">
                                                                        <input type="checkbox" value=""
                                                                               class="compare addToCompare"
                                                                               type="checkbox"
                                                                               id="inventory_<?php echo $inventory_id; ?>">
                                                                        <span class="cr"><i
                                                                                    class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                        <b>Add to Compare</b> 
                                                                    </label>
                                                                </div>


                                                        </div>
                                                    </div> -->
                                                    
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                                <div class="checkbox mb-0">
                                                                    <label for="combo_inventory_<?php echo $inventory_id; ?>">

                                                                    
                                                                        <input type="checkbox" value=""
                                                                               class="addToCombo" onclick="addToComboFun(<?php echo $inventory_id; ?>)"
                                                                               type="checkbox"
                                                                               id="combo_inventory_<?php echo $inventory_id; ?>" data_id="<?php echo $inventory_id; ?>" 
                                                                               
                                                                                data_promotion_id="<?php echo $promo_uid_selected; ?>"
                                                                                
                                                                                data_promo_qty="<?php echo $p_qty; ?>"
                                                                                data_promo_remain_qty="<?php echo $residue; ?>"
                                                                                data_promo_dis="<?php echo $dis_percentage; ?>"
                                                                                data_promo_quote="<?php echo $promo_quote_selected; ?>"

                                                                               data_price="<?php echo round($sel_price); ?>" data_max_price="<?php echo round($max_price); ?>" data_saved_price="<?php echo round($max_price-$sel_price); ?>" data_sku_id="<?php echo $searchRes["_source"]["sku_id"] ?>" data_image='<?php echo base_url().$searchRes["_source"]["common_image"]; ?>' data_name='<?php echo $str.$attr_str; ?>' >
                                                                        <span class="cr"><i
                                                                                    class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                        <b>Add to Basket</b> 
                                                                    </label>
                                                                </div>
                                                        </div>
                                                    </div>


                                                <?php } ?>

                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }

                                }
                            }
                            ?>
                        </ul>
						
						
						<?php
							if($inventories_display_count==0){
						?>
						 <div class="row" style="height:100vh;display:flex;align-items:center;text-align:center;">
                            <div class="col-md-12">
                                <h4>
                                    Your search did not yield any results. Try another one!
                                </h4>
                            </div>
                        </div>
						<?php
								}
						?>
						
						
                        <!-- ./PRODUCT LIST -->
                    </div>
                    <!-- ./view-product-list-->
                    <div class="sortPagiBar">
                        <div class="bottom-pagination">
                            <nav>
                                <ul class="pagination">
                                    <?php echo $paginationCtrls; ?>
                                </ul>
                            </nav>
                        </div>
                    </div>

                    <?php
                }
                ?>

            </div>


            <?php include 'recently_viewed.php'; ?>

            <script>
                function add_to_wishlistFun(inventory_id) {
                    document.getElementById('anchor_' + inventory_id).style.pointerEvents = 'none';

                <?php
                    if(!$this->session->userdata("customer_id")){
                    $this->session->set_userdata("cur_page", current_url());
                    ?>
                    if (confirm("Please login to add wishlist!")) {
                        location.href = "<?php echo base_url()?>login";
                    }
                    /*bootbox.confirm({
message: "Please login to add wishlist",
size: "small",
callback: function (result) {
if(result){
location.href="<?php echo base_url()?>login";
}
}
});*/

                    <?php
                    }else{
                    ?>
                    $.ajax({
                        url: "<?php echo base_url()?>add_to_wishlist",
                        type: "post",
                        dataType: "json",
                        data: "inventory_id=" + inventory_id,
                        success: function (data) {
                            if (data.exists == "no") {
                                alert('Added to wishlist');

                                /*bootbox.alert({
                                    size: "small",
                                    message: 'Added to wishlist',
                                });*/
                                $("#wishlist_" + inventory_id).css({"color": ""});
                                $("#wishlist_" + inventory_id).css({"color": "#ff4343;"});
                                $("#wishlist_label_" + inventory_id).html("Added to wishlist");
                                $("#anchor_" + inventory_id).attr({"title": "Added to wishlist"});
                                $("#wishlist_label_" + inventory_id).css({"color": "#ff4343;"});

                            } else if (data.exists == "yes") {

                                alert('Removed from wishlist');

                               /* bootbox.alert({
                                    size: "small",
                                    message: 'Removed from wishlist',
                                });*/

                                $("#wishlist_" + inventory_id).css({"color": ""});
                                $("#wishlist_" + inventory_id).css({"color": "#C2C2C2"});
                                $("#wishlist_label_" + inventory_id).html("Removed from wishlist");
                                $("#anchor_" + inventory_id).attr({"title": "Removed from wishlist"});
                                $("#wishlist_label_" + inventory_id).css({"color": "#C2C2C2;"});


                            } else {

                                alert('Error');

                               /* bootbox.alert({
                                    size: "small",
                                    message: 'Error',
                                });*/
                            }
                            document.getElementById('anchor_' + inventory_id).style.pointerEvents = 'auto';
                        }
                    });

                    <?php } ?>
                }
            </script>


            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>


<script>
    function page_rows_limitFun(obj) {
		result_catalog_tree_view=document.getElementById("result_catalog_tree_view").value;
		
        $("input[name='page_rows']").val(obj.value);
        document.searchForm.submit();
    }

    function sorting_byFun(value) {
		result_catalog_tree_view=document.getElementById("result_catalog_tree_view").value;
        $("input[name='sorting_by']").val(value);
        document.searchForm.submit();
    }

    function paginationFun(pn) {
        $("input[name='pagenum']").val(pn);
        document.searchForm.submit();
    }

    function filterColorFun(type_of_filter,filterbox_type) {
		result_catalog_tree_view=document.getElementById("result_catalog_tree_view").value;
		//alert(result_catalog_tree_view)
		//document.getElementById("result_catalog_tree_view").value=result_catalog_tree_view;
        if (filterbox_type == "priceslider") {
            $("input[name='" + type_of_filter + "_filter']").val($("#min_val_of_price_filter").val() + "-" + $("#max_val_of_price_filter").val());
            $("input[name='type_of_filter']").val(type_of_filter);
			$("input[name='filterbox_type']").val(filterbox_type);
//alert($("input[name='"+type_of_filter+"_filter']").val())
        } 
		
		else if (filterbox_type == "singleslider") {
			//alert($("#min_val_of_"+type_of_filter+"_filter").val() + "-" + $("#max_val_of_"+type_of_filter+"_filter").val())
            $("input[name='" + type_of_filter + "_filter']").val($("#min_val_of_"+type_of_filter+"_filter").val() + "-" + $("#max_val_of_"+type_of_filter+"_filter").val());
            $("input[name='type_of_filter']").val(type_of_filter);
			$("input[name='filterbox_type']").val(filterbox_type);
//alert($("input[name='"+type_of_filter+"_filter']").val())
        }
		
		


		else if (filterbox_type == "doubleslider") {
			//alert($("#min_val_of_"+type_of_filter+"_filter").val() + "-" + $("#max_val_of_"+type_of_filter+"_filter").val())
            $("input[name='" + type_of_filter + "_filter']").val($("#min_val_of_"+type_of_filter+"_filter").val() + "-" + $("#max_val_of_"+type_of_filter+"_filter").val());
            $("input[name='type_of_filter']").val(type_of_filter);
			$("input[name='filterbox_type']").val(filterbox_type);
//alert($("input[name='"+type_of_filter+"_filter']").val())
        }
		else if (filterbox_type == "brand") {			 
			  var filter = "";
				var filter_name = "";
				$("input[name='"+type_of_filter+"']").each(function () {
					if ($(this).is(":checked")) {
						filter += $(this).val() + "|";
						filter_name += $(this).attr('id') + "|";
					}
				});
				filter = filter.substr(0, filter.length - 1);
				filter_name = filter_name.substr(0, filter_name.length - 1);


				$("input[name='" + type_of_filter + "_filter']").val(filter);
				$("input[name='type_of_filter']").val(type_of_filter);
				$("input[name='filterbox_type']").val(filterbox_type);
				
		}
		
		else {
            var filter = "";
            var filter_name = "";
            $("input[name='" + type_of_filter + "']").each(function () {
                if ($(this).is(":checked")) {
                    filter += $(this).val() + "|";
                    filter_name += $(this).attr('id') + "|";
                }
            });
            filter = filter.substr(0, filter.length - 1);
            filter_name = filter_name.substr(0, filter_name.length - 1);

//alert(type_of_filter);
//alert(filter_name);

            $("input[name='" + type_of_filter + "_filter']").val(filter);
            $("input[name='type_of_filter']").val(type_of_filter);
			$("input[name='filterbox_type']").val(filterbox_type);

            if (filter_name != "") {
               // filter_activity(type_of_filter, filter_name);
            }
        }
        document.searchForm.submit();
    }

    function filter_activity(type_of_filter, filter_names) {
        /*user filter activity*/

        var start = new Date();
        start = start.getTime();

        level_type = '<?php echo (isset($level_type)) ? $level_type : ''; ?>';
        level_name = '<?php echo (isset($searched_key)) ? $searched_key : ''; ?>';
        cat_current = '<?php echo (isset($cat_current)) ? $cat_current : ''; ?>';
        cat_current_id = '<?php echo (isset($cat_current_id)) ? $cat_current_id : ''; ?>';

        if (level_type == "pcat_id") {
            level_type = "pcat";
        }
        if (level_type == "cat_id") {
            level_type = "cat";
        }
        if (level_type == "subcat_id") {
            level_type = "subcat";
        }

        /*alert(level_type);
alert(level_name);
alert(cat_current);
alert(cat_current_id);*/

        temp = {
            "level_type": level_type,
            "level_name": level_name,
            "name": cat_current,
            "id": cat_current_id,
            "filter_group": type_of_filter,
            "checked_filters": filter_names,
            "time": start
        };

        var r_final = localStorage.getItem("filter");

        if (r_final != null) {
            var stored = JSON.parse(localStorage.getItem("filter"));
            stored.push(temp);
            localStorage.setItem("filter", JSON.stringify(stored));
        } else {
            var obj = [];
            obj.push(temp);
            localStorage.setItem("filter", JSON.stringify(obj));
        }

        var result = JSON.parse(localStorage.getItem("filter"));
//alert(JSON.stringify(result));

//return false;

        /*user filter activity*/
    }

</script>

<form method="post" name="searchForm">



	<input type="hidden" id="result_catalog_tree_view" name="result_catalog_tree_view" value="<?php if (isset($_REQUEST["result_catalog_tree_view"]) && $_REQUEST["result_catalog_tree_view"] != "") {
               echo $_REQUEST["result_catalog_tree_view"];
           } ?>">
	
	
    <input type="hidden" name="search_keyword" value="<?php if (isset($_REQUEST["search_keyword"])) {
        echo $_REQUEST["search_keyword"];
    } ?>">
    <input type="hidden" name="search_cat" value="<?php if (isset($_REQUEST["search_cat"])) {
        echo $_REQUEST["search_cat"];
    } ?>">
    <input type="hidden" name="page_rows" value="<?php echo $page_rows; ?>">
    <input type="hidden" name="pagenum" value="<?php echo $pagenum; ?>">
    <input type="hidden" name="sorting_by" value="<?php echo $sorting_by; ?>">

    <input type="hidden" name="pcat_id" value="<?php echo $pcat_id; ?>">
    <input type="hidden" name="cat_id" value="<?php echo $cat_id; ?>">
    <input type="hidden" name="subcat_id" value="<?php echo $subcat_id; ?>">
    <input type="hidden" name="brand_id" value="<?php echo $brand_id; ?>">
    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
    <input type="hidden" name="inventory_id" value="<?php echo $inventory_id; ?>">
    <input type="hidden" name="cat_current" value="<?php echo $cat_current; ?>">
    <input type="hidden" name="cat_current_id" value="<?php echo $cat_current_id; ?>">
    <input type="hidden" name="level_type" value="<?php echo $level_type; ?>">
    <input type="hidden" name="level_name" value="<?php echo $searched_key; ?>">
    <input type="hidden" name="level_value" value="<?php echo $level_value; ?>">
	
	
	<input type="hidden" name="filterbox_type" id="filterbox_type">

    
	<input type="hidden" name="type_of_filter" id="type_of_filter"
           value="<?php if (isset($_REQUEST["type_of_filter"]) && $_REQUEST["type_of_filter"] != "") {
               echo $_REQUEST["type_of_filter"];
           } ?>">
		   
		   
	
<input type="hidden" name="brand_customfilterbox_filter"
                   value="<?php if (isset($_REQUEST['brand_customfilterbox_filter']) && $_REQUEST['brand_customfilterbox_filter'] != "") {
                       echo $_REQUEST['brand_customfilterbox_filter'];
                   } ?>">
		   

    <?php
    if (!empty($filter_box_infoRes)) {
        foreach ($filter_box_infoRes as $filter_box_infoObj) {
            $filter_box_infoObj->filterbox_name = preg_replace('/\s+/', '_', $filter_box_infoObj->filterbox_name);

            ?>
            <input type="hidden" name="<?php echo $filter_box_infoObj->filterbox_name; ?>_filter"
                   value="<?php if (isset($_REQUEST[$filter_box_infoObj->filterbox_name . '_filter']) && $_REQUEST[$filter_box_infoObj->filterbox_name . '_filter'] != "") {
                       echo $_REQUEST[$filter_box_infoObj->filterbox_name . "_filter"];
                   } ?>">

            <input type="hidden" name="<?php echo $filter_box_infoObj->filterbox_name; ?>_prev"
                   value="<?php if (isset($color_prev_arr[$filter_box_infoObj->filterbox_name])) {
                       echo implode(",", $color_prev_arr[$filter_box_infoObj->filterbox_name]);
                   } ?>">
            <?php

        }
    }
    ?>
	
	<?php
		/* Price filter hidden fields after form submit starts */
	?>
    <?php
    if ($type_of_filter == "") {
        $Price_default_info = $default_min_val_of_price_filter . "-" . $default_max_val_of_price_filter;
    } else {
        $Price_default_info = $_REQUEST["Price_default_info"];
    }
    ?>
    <input type="hidden" name="Price_default_info" value="<?php echo $Price_default_info; ?>">
    <input type="hidden" name="Price_filter"
           value="<?php if (isset($_REQUEST['Price_filter']) && $_REQUEST['Price_filter'] != "") {
               echo $_REQUEST['Price_filter'];
           } else {
               echo $Price_default_info;
           } ?>">
		   
		<?php
		/* Price filter hidden fields after form submit ends */
	?>
	
	
	
	
	
<?php
	
		/* Single slider filter hidden fields after form submit starts */
		//if(isset($_COOKIE["singleslider"])){
			//$singleslider_arr=json_decode($_COOKIE["singleslider"],true);
		//}
		foreach($singleslider_arr as $singleslider_key => $singleslider_value_arr){
	?>
    <?php
    if ($type_of_filter == "") {
        $singleslider_default_info = $singleslider_filter_arr[$singleslider_key]["default_min"] . "-" . $singleslider_filter_arr[$singleslider_key]["default_max"];
    } else {
        $singleslider_default_info = $_REQUEST[$singleslider_key."_default_info"];
    }
    ?>
    <input type="hidden" name="<?php echo $singleslider_key;?>_default_info" value="<?php echo $singleslider_default_info; ?>">
    <input type="hidden" name="<?php echo $singleslider_key;?>_filter"
           value="<?php if (isset($_REQUEST[$singleslider_key.'_filter']) && $_REQUEST[$singleslider_key.'_filter'] != "") {
               echo $_REQUEST[$singleslider_key.'_filter'];
           } else {
               echo $singleslider_default_info;
           } ?>">
		   
		<?php
		}
		
		/* Single slider filter hidden fields after form submit ends */
	?>
	
	
	
<?php
	
		/* Double slider filter hidden fields after form submit starts */
		//if(isset($_COOKIE["doubleslider"])){
			//$doubleslider_arr=json_decode($_COOKIE["doubleslider"],true);
		//}
		foreach($doubleslider_arr as $doubleslider_key => $doubleslider_value_arr){
	?>
    <?php
    if ($type_of_filter == "") {
        $doubleslider_default_info = $doubleslider_filter_arr[$doubleslider_key]["default_min"] . "-" . $doubleslider_filter_arr[$doubleslider_key]["default_max"];
    } else {
        $doubleslider_default_info = $_REQUEST[$doubleslider_key."_default_info"];
    }
    ?>
    <input type="hidden" name="<?php echo $doubleslider_key;?>_default_info" value="<?php echo $doubleslider_default_info; ?>">
    <input type="hidden" name="<?php echo $doubleslider_key;?>_filter"
           value="<?php if (isset($_REQUEST[$doubleslider_key.'_filter']) && $_REQUEST[$doubleslider_key.'_filter'] != "") {
               echo $_REQUEST[$doubleslider_key.'_filter'];
           } else {
               echo $doubleslider_default_info;
           } ?>">
		   
		<?php
		}
		
		/* Double slider filter hidden fields after form submit ends */
	?>
	
	

</form>

<!--preview combo pack checkout panel-->

<div class="container" style="display:none;width:100%;" id="combo_products_div">
    <div class="comboPanle">
        <div class="row">
            <div class="col-md-3 text-center">
                <span class="combo_lineheight"><h4>Quick Buy Basket</h4> 
                <span class="combo_lineheight" id="combo_line_text"><b><span id="no_of_products_count_combo">0</span></b> product(s) added to the cart</small>
                <br>
                    <small class="combo_lineheight text-danger" id="combo_line_text1"> <i class="fa fa-info-circle" aria-hidden="true"></i> Minimum <?php echo $combo_pack_min_items; ?> Products should be added</small>
                </span>
            </div>
            <div class="comboPan col-md-4 cursor-pointer">
            </div>
            <div class="col-md-5 text-center">
                <div class="col-md-6">
                   
                    Total Price : <?php echo curr_sym; ?><span id="combo_total"></span>
                    
                    
                    <?php echo curr_sym; ?><del id="combo_max_total"></del>
                    <br>

                    <small class="text-warning"><b>(Shipping Price will be Calculated at Checkout)</b><small>

                    <span style="display:none;">
                    <br>Grand Total : <b><?php echo curr_sym; ?><span id="combo_grand_total"></span></b>
                    </span>
                    
                </div>
                <div class="col-md-6">
                &nbsp;
                <!-- <form method="post" action="<?php echo base_url() ?>combo_inventories"> -->
                    <button type="button" class="button btn-lg notActive comboBtn" disabled onclick="proceedCheckout()"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> Checkout <span id="no_of_products_combo" class="badge"></span>
                    </button>
                    <a href="#" class="btn preventDflt" type="button" onclick="clearComboPanel()" title="Clear all items in the Combo Pack list"><small style="color: red;"><u>Clear All</u></small></a>
                    <input type="hidden" name="inventory_id_list_for_combo" id="inventory_id_list_for_combo" value="">
                <!-- </form> -->
                </div>
            </div>

        </div>

    </div>
</div>

<!--end of preview combo pack checkout panel-->


<!--preview panel-->
<div class="container" style="display:none; ;">
    <div class="comparePanle">
        <div class="row">
            <div class="col-md-3 text-center">
                <span class="compare_lineheight">Compare Products <small class="compare_lineheight"
                                                                         id="compare_line_text"> (Add <span
                                id="no_of_products_count_compare">0</span> more to compare )</small></span>
            </div>
            <div class="comparePan col-md-5 cursor-pointer">
            </div>
            <div class="col-md-4 text-center">
                &nbsp;
                <form method="post" action="<?php echo base_url() ?>compare_inventories">
                    <button type="submit" class="button btn-sm notActive cmprBtn" disabled>Compare <span
                                id="no_of_products_compare" class="badge"></span></button>
                    <a href="#" class="btn preventDflt" type="button" onclick="clearComparePanel()"><small>Clear
                            All</small></a>
                    <input type="hidden" name="inventory_id_list_for_compare" id="inventory_id_list_for_compare"
                           value="">
                </form>
            </div>

        </div>

    </div>
</div>
<!--end of preview panel-->

<!-- comparision popup-->
<div id="id01" class="w3-animate-zoom w3-white w3-modal modPos">
    <div class="w3-container">
        <a onclick="document.getElementById('id01').style.display='none'"
           class="whiteFont w3-padding w3-closebtn closeBtn">&times;</a>
    </div>
    <div class="w3-row contentPop w3-margin-top">
    </div>

</div>
<!--end of comparision popup-->

<!--  warning model  -->
<div id="WarningModal" class="w3-modal">
    <div class="w3-modal-content warningModal">
        <header class="w3-teal panel-heading">
            <h3><span>&#x26a0;</span>&nbsp;Error</h3>
        </header>
        <div class="w3-container">
            <p class="lead text-center">Maximum of Three products are allowed for comparision</p>
        </div>
        <footer class="w3-container w3-right-align">
            <button id="warningModalClose" onclick="document.getElementById('id01').style.display='none'"
                    class="w3-btn w3-hexagonBlue w3-margin-bottom preventDflt">Close
            </button>
        </footer>
    </div>
</div>
<!--  end of warning model  -->



<form action="<?php echo  base_url();?>goto_tagged_inventory_combo" method="post" name="goto_tagged_inventory_combo_form">
	<input type="text" name="tagged_main_inventory_id_list_in" id="tagged_main_inventory_id_list_in" value="">
</form>



<script>
    if (localStorage.getItem("comparePan") === null || localStorage.getItem("comparePan").trim() == "") {
    } else {
        $(".comparePanle").show();
        $("#inventory_id_list_for_compare").val(localStorage.getItem("comparePan_inventory_ids"));
        inventory_id_list_for_compare_str = $("#inventory_id_list_for_compare").val();
        inventory_id_list_for_compare_arr = inventory_id_list_for_compare_str.split(",");
        if (inventory_id_list_for_compare_arr.length > 0) {
            for (x in inventory_id_list_for_compare_arr) {
                $("#inventory_" + inventory_id_list_for_compare_arr[x]).attr("checked", true);
            }
        }
        $(".comparePan").html(localStorage.getItem("comparePan"));
        if ($("#inventory_id_list_for_compare").val().split(",").length > 1) {
            $(".cmprBtn").attr({"disabled": false});
        }
        $("#no_of_products_compare").html($("#inventory_id_list_for_compare").val().split(",").length);
        $("#no_of_products_count_compare").html(3 - $("#inventory_id_list_for_compare").val().split(",").length);
        if ($("#no_of_products_count_compare")[0].innerHTML == 0) {
            $("#compare_line_text").css("visibility", "hidden");
        } else {
            $("#compare_line_text").css("visibility", "visible");
        }
    }

    function clearComparePanel() {
        $(".addToCompare").each(function () {
            $(this).attr("checked", false);
        });

        localStorage.removeItem("comparePan");
        localStorage.removeItem("comparePan_inventory_ids");
        $(".comparePan").html("");
        $("#inventory_id_list_for_compare").val("");
        $(".comparePanle").hide();

    }

    $(document).ready(function () {
        showChar(200);
    });


    $(document).ready(function () {
        if ($(window).width() <= 480) {
            $(".collapse").removeClass("in");
        }

        toAdapt();
        toAdapt_sku();
        window.onresize = function () {
            toAdapt;
            toAdapt_sku;
        };

    });


    function toAdapt() {
        var heights = $(".heightmatched").map(function () {
                return $(this).height();
            }).get(),

            maxHeight = Math.max.apply(null, heights);

        $(".heightmatched").height(maxHeight);
    }

    function toAdapt_sku() {
        var heights_sku = $(".heightmatched_sku").map(function () {
                return $(this).height();
            }).get(),

            maxHeight_sku = Math.max.apply(null, heights_sku);

        $(".heightmatched_sku").height(maxHeight_sku);
    }

    function clearAllFiltersfun() {
        window.location.href = location.href;
    }

    /* add to combo/pack */
    
    function sum_price(data){
        var sum=0;
        $.each(data, function(index, value) { 
            //console.log(value.inv_price+"price");
            var capacity = parseInt(value.inv_price, 10);
            sum += capacity;        
        });

        return sum;
    }
    
    function sum_max_price(data){
        var sum=0;
        $.each(data, function(index, value) { 
            //console.log(value.inv_price+"price");
            var prc = parseInt(value.inv_max_price, 10);
            sum += prc;        
        });

        return sum;
    }
    function saved_price(data){
        var sum=0;
        $.each(data, function(index, value) { 
            //console.log(value.inv_price+"price");
            var capacity = parseInt(value.inv_saved_price, 10);
            sum += capacity;        
        });

        return sum;
    }

    function update_db_for_combo(clear_status=''){
        c_id = '<?php echo $this->session->userdata("customer_id");?>';
        if (c_id != '' && c_id != null) {

            if(clear_status==1){
                $.ajax({
                    url: "<?php echo base_url()?>Account/carttable_dump_combo_search_delete",
                    type: "post",
                    dataType: "json",
                    data:"clear="+clear_status,
                    success: function (result) {
                        
                        if(result){
                            alert('updated successfully');
                        }else{
                            alert('error');
                        }
                    }
                        
                });

            }else{
                //alert('Inside');
                
                localstorage_products=JSON.parse(localStorage.LocalCustomerComboCart);
                localstorage_products=JSON.stringify(localStorage.LocalCustomerComboCart);
                
                //console.log('--------');
                //console.log(localstorage_products);
                //console.log('--------');

                $.ajax({
                    url: "<?php echo base_url()?>Account/carttable_dump_combo_search",
                    type: "post",
                    dataType: "json",
                    data:"combo_products="+localstorage_products,
                    success: function (result) {
                        
                        if(result){
                            alert('updated successfully');
                        }else{
                            alert('error');
                        }
                    }
                        
                    });
            }
        }
    }


    var selected_products=[]; var combo_product_count=0;
    var inv_text=''; var total_combo_price=0;

    //localStorage.setItem("LocalCustomerComboCart",'');
    //$(document).on('click', '.addToCombo', function () {
    
    function addToComboFun(inventory_id){
        //this=obj;

        obj=$('#combo_inventory_'+inventory_id);
        cked= $(obj).is(":checked");
        if(localStorage.LocalCustomerComboCart != null && localStorage.LocalCustomerComboCart != "[]" && localStorage.LocalCustomerComboCart!=''){
            selected_products=JSON.parse(localStorage.LocalCustomerComboCart);
        }
        
        

        inv_id=$(obj).attr("data_id");

        promotion_id=$(obj).attr("data_promotion_id");
        promo_qty=$(obj).attr("data_promo_qty");
        promo_remain_qty=$(obj).attr("data_promo_remain_qty");
        promo_dis=$(obj).attr("data_promo_dis");
        promo_quote=$(obj).attr("data_promo_quote");

        //alert(promo_quote);

        //inv_price=$(this).attr("data_price");
		sel_price_db_2_decimals=(Math.round($("#sellingprice_"+$(obj).attr("data_id")).html() * 100) / 100).toFixed(2);

        //alert(sel_price_db_2_decimals);

		inv_price=parseFloat(sel_price_db_2_decimals); //setting the product price
        
        inv_max_price=$(obj).attr("data_max_price");

        //alert(inv_max_price);

        inv_saved_price=$(obj).attr("data_saved_price");
        inv_sku_id=$(obj).attr("data_sku_id");
        inv_image=$(obj).attr("data_image");
        inv_name=$(obj).attr("data_name");

		inv_quantity=document.getElementById('purchase_qty_input_'+inv_id).value;
        //alert('inv_id='+inv_id+' inv_price='+inv_price+' inv_sku_id='+inv_sku_id+' inv_image='+inv_image+' inv_name='+inv_name);

        if(cked==true){
            //alert();
            var index = selected_products.findIndex(x => x.inv_id==inv_id); 
            if(index === -1){
                
                selected_products.push({'inv_id':inv_id,'inv_price':inv_price,'inv_sku_id':inv_sku_id,'inv_name':inv_name,'inv_image':inv_image,'inv_max_price':inv_max_price,'inv_saved_price':inv_saved_price,'inv_quantity':inv_quantity,'promotion_id':promotion_id,'promo_qty':promo_qty,'promo_remain_qty':promo_remain_qty,'promo_dis':promo_dis,"promo_quote":promo_quote});

                inv_text+=inv_id+',';
            }else{

                //alert(index+'else');
                selected_products[index].inv_price=inv_price;
                selected_products[index].inv_max_price=inv_max_price;
                selected_products[index].inv_saved_price=inv_saved_price;
                selected_products[index].inv_quantity=inv_quantity;
                selected_products[index].promotion_id=promotion_id;
                selected_products[index].promo_qty=promo_qty;
                selected_products[index].promo_remain_qty=promo_remain_qty;
                selected_products[index].promo_dis=promo_dis;
                selected_products[index].promo_quote=promo_quote;

            }
        }else{
            let index1 =selected_products.findIndex((element) => element["inv_id"] == inv_id);
            selected_products.splice(index1, 1); 
        }
        
        combo_product_count = selected_products.length;

        if (selected_products != null && selected_products != "[]") {
            //alert(selected_products);
            localStorage.setItem("LocalCustomerComboCart", JSON.stringify(selected_products));
            localStorage.setItem("LocalCustomerComboCartLength", combo_product_count);
            total=sum_price(selected_products);
            max_total=sum_max_price(selected_products);
            localStorage.setItem("LocalCustomerComboCartTotal", total);
            localStorage.setItem("LocalCustomerComboCartMaxTotal", max_total);
            construct_html_for_combo(selected_products);
            $('#combo_total').html(total);
            $('#combo_max_total').html(max_total);
            combo_shipping_charge=$('#combo_pack_shipping_charge').val();
            grand_total=(parseInt(total)+parseInt(combo_shipping_charge));
            $('#combo_grand_total').html(grand_total);
            localStorage.setItem("LocalCustomerComboCartGrandTotal", grand_total);
            saved_amount=saved_price(selected_products);
            localStorage.setItem("LocalCustomerComboCartSavedTotal", saved_amount);
            $('#no_of_products_count_combo').html(localStorage.LocalCustomerComboCartLength)
            $('#combo_products_div').show();
            min_combo_length=$('#combo_pack_min_items').val();
            if(localStorage.LocalCustomerComboCartLength>=min_combo_length){
                $(".comboBtn").attr({"disabled": false});
            }else{
                $(".comboBtn").attr({"disabled": true});
            }
			
			if(combo_product_count==0){
				$(".comboPan").html('');
				clearComboPanel(); // this code for last item
			}
        }
        //console.log(localStorage.LocalCustomerComboCart);
        //console.log(localStorage.LocalCustomerComboCartLength);
        //console.log(localStorage.LocalCustomerComboCartTotal);
        //console.log(localStorage.LocalCustomerComboCartGrandTotal);


        update_db_for_combo();

        console.log("localStorage")
        console.log(localStorage);
        console.log("selected_products")
        console.log(selected_products)
       
    }
    //});

    function construct_html_for_combo(data){
        var sum;
        if(data!=''){
            $(".comboPan").html('');
            $.each(data, function(index, value) { 
                //console.log(value.inv_price+"price");
                var price = parseInt(value.inv_price, 10);
                sum += price;       
                displayTitle=value.inv_name;
                displayContent=displayTitle+' - <?php echo curr_sym; ?>'+price;
                $(".comboPan").append('<div id="' + value.inv_id + '" class="col-sm-3" title="'+displayContent+'"><div class="w3-white"><div class="f-row text-center"><a class="selectedItemCloseBtn_combo combo_close cursor">&times</a><img src="' + value.inv_image + '" alt="image" style="height:58px;"/><p id="prod_' +  value.inv_id + '" class="titleMargin1" title="'+displayTitle+'"><span class="small-text">' + displayTitle.substring(0,8) + '...</span></p></div></div></div>');
            });
        }
    }

    $(document).on('click', '.selectedItemCloseBtn_combo', function () {
		
        var test = $(this).siblings("p").attr('id');
        //alert(test);
		test_arr=test.split("_");
        inv_id=test_arr[1];
		var myEle = document.getElementById("inventory_" + inv_id);
		if(myEle) {
			document.getElementById("inventory_" + inv_id).checked = false;
		}
        
        if(localStorage.LocalCustomerComboCart != null && localStorage.LocalCustomerComboCart != "[]" && localStorage.LocalCustomerComboCart!=''){
            selected_products=JSON.parse(localStorage.LocalCustomerComboCart);
            let index1 =selected_products.findIndex((element) => element["inv_id"] == inv_id);
            selected_products.splice(index1, 1); 
            localStorage.setItem("LocalCustomerComboCart", JSON.stringify(selected_products));
            construct_combo_products(selected_products);
            update_db_for_combo();
        }

    });

   

    function clearComboPanel() {
        $(".addToCombo").each(function () {
            $(this).attr("checked", false);
        });

        // localStorage.removeItem("comboPan");
        localStorage.removeItem("LocalCustomerComboCart");
        localStorage.removeItem("LocalCustomerComboCartLength");
        localStorage.removeItem("LocalCustomerComboCartTotal");
        localStorage.removeItem("LocalCustomerComboCartMaxTotal");
        localStorage.removeItem("LocalCustomerComboCartGrandTotal");
        localStorage.removeItem("LocalCustomerComboCartSavedTotal");

        $(".comboPan").html("");
        $("#inventory_id_list_for_combo").val("");
        $("#combo_products_div").hide();
        var clear_status=1;
		selected_products=[];
        update_db_for_combo(clear_status);

    }
    //clearComboPanel();


    function construct_combo_products(selected_products=''){

        if(localStorage.LocalCustomerComboCart != null && localStorage.LocalCustomerComboCart != "[]" && localStorage.LocalCustomerComboCart!=''){
            selected_products=JSON.parse(localStorage.LocalCustomerComboCart);
        }

        if(selected_products!= null && selected_products!= "[]" && selected_products!=""){
            $(".addToCombo").each(function () {
                inv_id=$(this).attr("data_id");
				    var index = selected_products.findIndex(x => x.inv_id==inv_id); 
					if(index !== -1){
						$(this).attr("checked", true); // when it exists on localstorage
						inv_price=$(this).attr("data_price");
						inv_max_price=$(this).attr("data_max_price");
						inv_saved_price=$(this).attr("data_saved_price");

						selected_products[index].inv_price=inv_price;
						selected_products[index].inv_max_price=inv_max_price;
						selected_products[index].inv_saved_price=inv_saved_price;
						
					}else{
						$(this).attr("checked", false);
					}
            });

            construct_html_for_combo(selected_products);

            total=sum_price(selected_products);
            max_total=sum_max_price(selected_products);
            localStorage.setItem("LocalCustomerComboCartTotal", total);
            
            $('#combo_total').html(total);
            $('#combo_max_total').html(max_total);
            combo_shipping_charge=$('#combo_pack_shipping_charge').val();
            grand_total=(parseInt(total)+parseInt(combo_shipping_charge));
            $('#combo_grand_total').html(grand_total);
            localStorage.setItem("LocalCustomerComboCartGrandTotal", grand_total);
            localStorage.setItem("LocalCustomerComboCartMaxTotal", max_total);
            saved_amount=saved_price(selected_products);
            localStorage.setItem("LocalCustomerComboCartSavedTotal", saved_amount);


            $('#no_of_products_count_combo').html(localStorage.LocalCustomerComboCartLength)
            $('#combo_total').html(localStorage.LocalCustomerComboCartTotal);
            $('#combo_grand_total').html(localStorage.LocalCustomerComboCartGrandTotal);

            $("#combo_products_div").show();

            min_combo_length=$('#combo_pack_min_items').val();
            if(localStorage.LocalCustomerComboCartLength>=min_combo_length){
                $(".comboBtn").attr({"disabled": false});
            }else{
                $(".comboBtn").attr({"disabled": true});
            }
        }else{
			
			$(".addToCombo").each(function () { // this code for last item
				$(this).attr("checked", false); // this code for last item
			}); // this code for last item
			
			
            $("#combo_products_div").hide();

        }


    }


    $(document).ready(function () {
        //construct_combo_products();

        c_id = '<?php echo $this->session->userdata("customer_id");?>';
        if (c_id != '' && c_id != null) {
            $.ajax({
                url: "<?php echo base_url()?>Account/get_combo_cart",
                type: "post",
                dataType: "json",
                data:"1=2",
                success: function (result) {
                    //alert(result);
                    //console.log('Combo products');
                    //console.log(result);
                    
                    if(result){
                        if(result.combo_products!=''){
                            var combo_products=JSON.parse(JSON.stringify(result.combo_products));
                        }else{
                            var combo_products=[];
                        }
                       
                        var combo_number_of_products=result.combo_number_of_products;
                        var combo_total_price=Math.round(result.combo_total_price);
                        var combo_max_total_price=Math.round(result.combo_max_total_price);
                        var combo_shipping_price=Math.round(result.combo_shipping_price);
                        var combo_grand_total=Math.round(result.combo_grand_total);
                        var combo_discount=Math.round(result.combo_discount);
                        
                        localStorage.setItem("LocalCustomerComboCart",combo_products);
                        localStorage.setItem("LocalCustomerComboCartLength",combo_number_of_products);
                        localStorage.setItem("LocalCustomerComboCartTotal",combo_total_price);
                        localStorage.setItem("LocalCustomerComboCartMaxTotal",combo_max_total_price);
                        localStorage.setItem("LocalCustomerComboCartGrandTotal",combo_grand_total);
                        construct_combo_products();
                    }
                }
            });

        }else{
            construct_combo_products();
        }

    });

    function proceedCheckout() {
            min_combo_length=$('#combo_pack_min_items').val();
            if(localStorage.LocalCustomerComboCartLength>=min_combo_length){
                $(".comboBtn").attr({"disabled": false});
            }else{
                $(".comboBtn").attr({"disabled": true});
                swal({
                    title: '',
                    html:"Please add minimum "+min_combo_length+ " Products",
                    type: 'info',
                    showConfirmButton: true
                });
                return false;
            }
			
            // tagged_main_inventory_id_list_in_arr=[];
			// selected_products=JSON.parse(localStorage.LocalCustomerComboCart);

			// for(selected_products_index in selected_products){
			// 	tagged_main_inventory_id_list_in_arr.push(selected_products[selected_products_index].inv_id);
			// }
			// if(tagged_main_inventory_id_list_in_arr.length>0){
			// 	document.getElementById("tagged_main_inventory_id_list_in").value=tagged_main_inventory_id_list_in_arr.join(",");
			// }
			// document.goto_tagged_inventory_combo_form.submit();
            // return false;

            /* changed to quick buy */
            c_id = '<?php echo $this->session->userdata("customer_id");?>';
            if (c_id != '' && c_id != null) {
                location.href = "<?php echo base_url()?>checkout_combo";
            } else {
                <?php
                $this->session->set_userdata("cur_page", base_url() . "checkout_combo");
                ?>
                location.href = "<?php echo base_url()?>login";
            }
            /* changed to quick buy */
			
			
			
			//location.href = "<?php echo base_url()?>tagged_inventory_combo";
            /*c_id = '<?php echo $this->session->userdata("customer_id");?>';
            if (c_id != '' && c_id != null) {
                location.href = "<?php echo base_url()?>checkout_combo";
            } else {
                <?php
                $this->session->set_userdata("cur_page", base_url() . "checkout_combo");
                ?>
                location.href = "<?php echo base_url()?>login";
            }*/
        }
    /* add to combo/pack */

</script>

    <link href="https://kendo.cdn.telerik.com/themes/6.7.0/default/default-main.css" rel="stylesheet" />
    <!--<script src="https://kendo.cdn.telerik.com/2023.2.829/js/jquery.min.js"></script>-->
    
    
    <script src="https://kendo.cdn.telerik.com/2023.2.829/js/kendo.all.min.js"></script>
    
  
	
<!--  warning model  -->
<div class="modal" id="category_treeview_order_modal">
     <div class="modal-dialog modal-md">
        <div class="modal-content">
           <div class="modal-header" style="border-bottom:0px;">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
			 <style>
			 #category_treeview{
				 border:1px solid #e1e1e1;
				 padding:1rem;
				 margin-top:1rem;
				 margin-bottom:1rem;
			 }
			 </style>
			 
              <h4 class="modal-title">Choose the products to buy</h4>
           </div>
           <div class="modal-body">
		   
			<div class="row">
				<div class="col-md-12 text-right">
					<button type="button" class="btn btn-primary" name="category_treeview_order_submit_1" onclick="category_treeview_order_modalFun()">Submit</button>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12" id="category_treeview">
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-right">
					<button type="button" class="btn btn-primary" name="category_treeview_order_submit_2" onclick="category_treeview_order_modalFun()">Submit</button>
				</div>
			</div>
			
    

    <script>
		function category_treeview_order_modalFun(){
			document.searchForm.action="<?php echo base_url()?>catalog_combo";
			document.searchForm.submit();
		}
		function treeviewData(pcat_cat_data){
			$("#category_treeview").kendoTreeView({
				checkboxes: {
					checkChildren: true
				},

				check: onCheck,

				dataSource: pcat_cat_data
			});
		}

        // function that gathers IDs of checked nodes
        function checkedNodeIds(nodes, checkedNodes) {
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].checked) {
                    checkedNodes.push(nodes[i].id);
                }

                if (nodes[i].hasChildren) {
                    checkedNodeIds(nodes[i].children.view(), checkedNodes);
                }
            }
        }

        // show checked node IDs on datasource change
        function onCheck() {
            var checkedNodes = [],
                category_treeview = $("#category_treeview").data("kendoTreeView"),
                message;

            checkedNodeIds(category_treeview.dataSource.view(), checkedNodes);

            if (checkedNodes.length > 0) {
                message = checkedNodes.join(",");
            } else {
                message = "";
            }

            //$("#result").html(message);
			$("#result_catalog_tree_view").val(message);
        }
    </script>

   



  
  
  
           </div>
       
        <!--<div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>-->
     </div>
    </div>
  </div>
<!--  end of warning model  -->

<script>
	function chooseCategoryTreeViewFun(){
		result_catalog_tree_view=document.getElementById("result_catalog_tree_view").value;
		$.ajax({
			url: "<?php echo base_url()?>get_treeview_fun",
			type: "post",
			dataType: "json",
			data: "result_catalog_tree_view="+result_catalog_tree_view,
			success: function (data) {
				$("#category_treeview_order_modal").modal();
				treeviewData(data);
			}
		});
	}
	function chooseCategoryTreeViewClearFun(){
		window.location.href = location.href;
	}
	
		function incrementdecrementtimerfun(moq_original, max_oq,input_value,inventory_id,sel_price_db,selling_discount='',promo_qts_str='',promo_dis_str='',promo_ids_str=''){
            

            /* input validation */

            var moq=input_value;

			if(moq==moq_original ){
				
			}else if(moq!=""){
				
			}
			
			
            if (parseInt(moq) > parseInt(max_oq) && isNaN(moq)==false) {
				//alert(max_oq)
                document.getElementById('purchase_qty_input_'+inventory_id).value = max_oq;
                input_value=max_oq;
            }
			if (parseInt(moq) < parseInt(moq_original) && isNaN(moq)==false) {
                document.getElementById('purchase_qty_input_'+inventory_id).value = moq_original;
                input_value=moq_original;
            }
            if (moq == '' || isNaN(moq)==true) {
                $("#quantity_status_info_"+inventory_id).html('<span style="color:red">Enter between ' + moq_original + ' and ' + max_oq + '</span>');
				
                return false;// retain the previos calculation for the input
            }else{
				
                if (moq != "") {
                    var moq_previous = moq;
                }

            }

            $("#quantity_status_info_"+inventory_id + " span").css({"color":"#ff8000"});

			if ((moq == "" || parseInt(moq) == 0 || moq == null) && isNaN(moq)==true) {
                $("#quantity_status_info_"+inventory_id).html('<span style="color:red"><small>Enter qty between ' + moq_original + ' and ' + max_oq + '</small></span>');
                document.getElementById('purchase_qty_input_'+inventory_id).value = moq_original;
                input_value=moq_original;
            }else {
                if (parseInt(moq) < parseInt(moq_original)) {
                    $("#quantity_status_info_"+inventory_id).html('<span style="color:blue"><small>Enter qty between ' + moq_original + ' and ' + max_oq + '</small></span>');
					var stock_available="<?php echo $inventory_product_info_obj->stock_available;?>";
                } else if (parseInt(moq) <= parseInt(max_oq) && parseInt(moq) > parseInt(stock_available)) {
                    $("#quantity_status_info_"+inventory_id).html('Out of Stock!');
                    return false;
                } else if (parseInt(moq) > parseInt(max_oq)) {
                    $("#quantity_status_info_"+inventory_id).html('<span style="color:qty"><small>Enter qty between ' + moq_original + ' and ' + max_oq + '</small></span>');
                    document.getElementById('purchase_qty_input_'+inventory_id).value = max_oq;
                    input_value=max_oq;
                }
            }
            

            /* input validation */

            /* checking promo availability */

            var discount_array=[];
            h_inventory_id=inventory_id;
            h_promo_id='';
            h_qty=0;
            h_dis=0;

            sel_price_db=$('#selling_price_'+inventory_id).val();
            original_price_max=$('#max_selling_price_'+inventory_id).val();
            original_price=$('#selling_price_'+inventory_id).val();
            selling_discount=$('#selling_discount_'+inventory_id).val();

            qty_promo=$('.promo_uid_'+inventory_id).length;
            inv_min_qty=parseInt(input_value);
            /* default */
        
            $('.quote_class_'+inventory_id).css({"color": "#31708f","font-weight":"normal"});
            $('.fa_class_'+inventory_id).hide();

            if(parseInt(qty_promo)>0){
            
                if(discount_array.length==0){
                    $('.promo_uid_'+inventory_id).each(function(){
                        var q = $(this).val();
                        promo_id = q.replace(/,/g, '');      

                        //alert('#promo_list_to_buy_'+inventory_id+'_'+promo_id);

                        qty=$('#promo_list_to_buy_'+inventory_id+'_'+promo_id).val();
                        discount=$('#promo_list_discount_'+inventory_id+'_'+promo_id).val();
                        promo_quote=$('#promo_quote_'+inventory_id+'_'+promo_id).val();
                       
                        discount_array.push({'inventory_id':inventory_id,'qty':parseInt(qty),'discount':parseInt(discount),"promo_id":promo_id,"promo_quote":promo_quote,"checked":0});

                    });

                    //alert(discount_array);
                    //console.log(discount_array);

                        /* apply dis */
                        inv_min_qty=parseInt(input_value);
                        if(discount_array.length>0){
                    
                            discount_array.sort((a, b) => parseInt(b.qty) - parseInt(a.qty));

                            inx=discount_array.findIndex(x => parseInt(inv_min_qty)>=x.qty); 

                           // alert(inx);

                    if(inx !== -1){
                            
                        if(1){

                            index_1 = discount_array.findIndex(x => x.qty <= inv_min_qty);

                                if(index_1 !== -1){

                                    /* newly added  */
                                    
                                        index = discount_array.findIndex(x => x.qty==inv_min_qty);

                                        if(index !== -1){
                                            discount_array[index].checked="1";
                                            var maxArr=discount_array[index];
                                        }else{
                                           
                                            index_min = discount_array.findIndex(x => x.qty<inv_min_qty);

                                            if(index_min !== -1){
                                                discount_array[index_min].checked="1";
                                                var maxArr=discount_array[index_min];

                                            }else{

                                                index1 = discount_array.findIndex(x => (inv_min_qty%x.qty)==0);

                                                //alert("comming hrere"+index1);

                                                if(index1 !== -1){
                                                    discount_array[index1].checked="1";
                                                    var maxArr=discount_array[index1];
                                                }else{
                                                    discount_array[index_1].checked="1";
                                                    var maxArr=discount_array[index_1];
                                                }
                                            }

                                        }
                                    /* newly added  */

                                    
                                   
                                }else{
                                    

                                    index1 = discount_array.findIndex(x => (inv_min_qty%x.qty)==0);

                                    if(index1 !== -1){
                                        var maxArr=discount_array[index1];
                                    }else{
                                        var maxArr = discount_array.reduce(function(prev, current) {
                                            if (+current.qty > +prev.qty) {
                                                current.checked="1";
                                                return current;
                                            } else {
                                                prev.checked="1";
                                                return prev;
                                            }
                                        });
                                    }
                                    
                                }

                            
                        }else{
                           
                            index = discount_array.findIndex(x => x.qty==inv_min_qty);

                            if(index !== -1){
                                discount_array[index].checked="1";
                                var maxArr=discount_array[index];
                            }else{
                                index_1 = discount_array.findIndex(x => x.qty>inv_min_qty);
                                
                                if(index_1 !== -1){

                                    index1 = discount_array.findIndex(x => (inv_min_qty%x.qty)==0);

                                    //alert("comming hrere"+index1);

                                    if(index1 !== -1){
                                        discount_array[index1].checked="1";
                                        var maxArr=discount_array[index1];
                                    }else{
                                        discount_array[index_1].checked="1";
                                        var maxArr=discount_array[index_1];
                                    }

                                   
                                }else{
                                    //alert(4);
                                    index_2 = discount_array.findIndex(x => (inv_min_qty%x.qty)==0);
                                    if(index_2 !== -1){
                                        discount_array[index_2].checked="1";
                                        var maxArr=discount_array[index_2];
                                    }else{

                                    }
                                   
                                }
                            }

                        }
                        
                        //alert(maxArr);

                        if(maxArr){
                            h_inventory_id=maxArr.inventory_id;
                            h_promo_id=maxArr.promo_id;
                            h_qty=maxArr.qty;
                            h_dis=maxArr.discount;
                            promo_quote=maxArr.promo_quote;

                            $('#combo_inventory_'+inventory_id).attr('data_promotion_id',h_promo_id);
                            $('#combo_inventory_'+inventory_id).attr('data_promo_dis',h_dis);
                            $('#combo_inventory_'+inventory_id).attr('data_promo_quote',promo_quote);
                            $('#quote_'+inventory_id+"_"+h_promo_id).css({"color": "green","font-weight":"500"});
                            $('#fa_quote_'+inventory_id+"_"+h_promo_id).show();

                            //alert(h_inventory_id+"||"+h_promo_id+"||"+h_qty+"||"+h_dis);

                            //add_promotion(h_inventory_id,h_promo_id,h_qty,h_dis);
                           
                        }else{

                            //$('.quote_class_'+inventory_id).css("color", "#31708f");
                            $('.quote_class_'+inventory_id).css({"color": "#31708f","font-weight":"normal"});
                            $('.fa_class_'+inventory_id).hide();
                            /* unselect */

                            //console.log('else');
                            //$('input[name="dependent"]').prop('checked', false);

                            $('#combo_inventory_'+inventory_id).attr('data_promotion_id','');
                            $('#combo_inventory_'+inventory_id).attr('data_promo_dis','');
                            $('#combo_inventory_'+inventory_id).attr('data_promo_quote','');

                        }

                    }else{

                        //alert("ultimate else"); qty not exists  - no promotion                        
                        //quick_buy_option_selected
                    }

                }

                        /* apply dis */

                    
                }

                /* check any one if promo qty is greater */
            }
            /* checking promo availability */



            /* dis calculation */

            //alert("h_dis = "+h_dis);
            price2=0;
            if(parseInt(h_dis)>0){

            multiplier = (inv_min_qty / h_qty);
            multiplier = Math.floor(multiplier)
            residue = (inv_min_qty % h_qty);

            straight_dis_hidden=0;// for straight dis promotion
            total_price_of_product_with_promotion=0;
            if(multiplier>=1 && h_dis>0){
                price1 = Math.round((h_dis * original_price_max) / 100);
                discounted_price1 = Math.round((original_price_max - price1) * (inv_min_qty - residue))
                quantity_with_promotion = (inv_min_qty - residue);
                individual_price_of_product_with_promotion = (original_price_max - price1);
                total_price_of_product_with_promotion = (quantity_with_promotion * individual_price_of_product_with_promotion);
            }
            if (residue>0) {
                if(parseInt(straight_dis_hidden)>0){
                    price2 = residue * (original_price);
                }else{
                    if(parseInt(selling_discount)>0 ){
                        price_default_dis=Math.round(original_price_max-((selling_discount*original_price_max)/100));
                        price2 = residue * (price_default_dis);
                    }else{
                        price2 = residue * (original_price);
                    }
                }
            }

            $('#combo_inventory_'+inventory_id).attr('data_promo_remain_qty',residue);
            $('#combo_inventory_'+inventory_id).attr('data_promo_qty',(parseInt(inv_min_qty)-parseInt(residue)));

            total_discounted_price = Math.round(total_price_of_product_with_promotion + price2);
            inv_price_tot=total_discounted_price;
            $("#sellingprice_"+inventory_id).html(total_discounted_price);

        }else{
            //alert(sel_price_db);
            /* Normal -inventory DB price - no promo applied */
            sel_price_db=(Math.round((inv_min_qty*sel_price_db) * 100) / 100);
            inv_price_tot=sel_price_db;
            $("#sellingprice_"+inventory_id).html(sel_price_db);
            $('#combo_inventory_'+inventory_id).attr('data_promo_remain_qty',0);
            $('#combo_inventory_'+inventory_id).attr('data_promo_qty',0);
        }

        strike_price=(parseInt(original_price_max)*parseInt(inv_min_qty));
        //alert(strike_price);
        $("#strike_price_"+inventory_id).html(strike_price);

        $('#combo_inventory_'+inventory_id).attr('data_max_price',strike_price);

        saved_price_inv=(parseInt(strike_price)-parseInt(inv_price_tot));

        $('#combo_inventory_'+inventory_id).attr('data_saved_price',saved_price_inv);

        /* check checbox is checked */

        ss=$('#combo_inventory_'+inventory_id).is(":checked");
        if(ss){
            addToComboFun(inventory_id);
            //$('#combo_inventory_'+inventory_id).trigger('click');
            //$('#combo_inventory_'+inventory_id).attr('checked',true);
        }

        /* check checbox is checked */

            /* dis calculation */

            return false;

			var moq=input_value;

			if(moq==moq_original ){
				sel_price_db_2_decimals=(Math.round(moq*sel_price_db * 100) / 100).toFixed(2);
				$("#sellingprice_"+inventory_id).html(sel_price_db_2_decimals); //setting the product price
			}
			else if(moq!=""){
				sel_price_db_2_decimals=(Math.round((moq*sel_price_db) * 100) / 100).toFixed(2);
				$("#sellingprice_"+inventory_id).html(sel_price_db_2_decimals); //setting the product price
			}
			
			
            if (parseInt(moq) > parseInt(max_oq) && isNaN(moq)==false) {
				//alert(max_oq)
                document.getElementById('purchase_qty_input_'+inventory_id).value = max_oq;
				sel_price_db_2_decimals=(Math.round(max_oq*sel_price_db * 100) / 100).toFixed(2);
				$("#sellingprice_"+inventory_id).html(sel_price_db_2_decimals); //setting the product price
            }
			if (parseInt(moq) < parseInt(moq_original) && isNaN(moq)==false) {
                document.getElementById('purchase_qty_input_'+inventory_id).value = moq_original;
				sel_price_db_2_decimals=(Math.round(moq_original*sel_price_db * 100) / 100).toFixed(2);
				$("#sellingprice_"+inventory_id).html(sel_price_db_2_decimals); //setting the product price
            }
            if (moq == '' || isNaN(moq)==true) {
                $("#quantity_status_info").html('<span style="color:red">Enter qty between ' + moq_original + ' and ' + max_oq + '</span>');
				sel_price_db_2_decimals=(Math.round(moq_original*sel_price_db * 100) / 100).toFixed(2);
				$("#sellingprice_"+inventory_id).html(sel_price_db_2_decimals); //setting the product price
                return false;// retain the previos calculation for the input
            }else{
				
                if (moq != "") {
                    var moq_previous = moq;
                }

            }
			if ((moq == "" || parseInt(moq) == 0 || moq == null) && isNaN(moq)==true) {
                $("#quantity_status_info").html('<span style="color:red">Enter between ' + moq_original + ' and ' + max_oq + '</span>');
                document.getElementById('purchase_qty_input_'+inventory_id).value = moq_original;
				sel_price_db_2_decimals=(Math.round(moq_original*sel_price_db * 100) / 100).toFixed(2);
				$("#sellingprice_"+inventory_id).html(sel_price_db_2_decimals); //setting the product price
            }else {
                if (parseInt(moq) < parseInt(moq_original)) {
                    $("#quantity_status_info").html('<span style="color:blue">Enter between ' + moq_original + ' and ' + max_oq + '</span>');
					var stock_available="<?php echo $inventory_product_info_obj->stock_available;?>";
                } else if (parseInt(moq) <= parseInt(max_oq) && parseInt(moq) > parseInt(stock_available)) {
                    $("#quantity_status_info").html('Out of Stock!');
                } else if (parseInt(moq) > parseInt(max_oq)) {
                    $("#quantity_status_info").html('<span style="color:qty">Enter between ' + moq_original + ' and ' + max_oq + '</span>');
                    document.getElementById('purchase_qty_input_'+inventory_id).value = max_oq;
					sel_price_db_2_decimals=(Math.round(max_oq*sel_price_db * 100) / 100).toFixed(2);
				$("#sellingprice_"+inventory_id).html(sel_price_db_2_decimals); //setting the product price

                }
            }
        
        };

		function inventory_moq_resetFun(moq,inventory_id) {
			var purchase_qty_input=document.getElementById('purchase_qty_input_'+inventory_id).value;
            if (purchase_qty_input == "") {
                document.getElementById('purchase_qty_input_'+inventory_id).value = moq;
            }
        }
	 
	 
	 function isNumber(evt) {
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
		return false;
	}
	return true;
}

function select_promo_onload(inv_id,promo_id){
    $('#fa_quote_'+inventory_id+"_"+promo_id).show();
}

$(".qty_sym").on("click", function () {

    var $button = $(this);
    var min_oq = $(this).attr('min_oq');
    var max_oq = $(this).attr('max_oq');
    var inv_id = $(this).attr('inv_id');
    var sel_price = $(this).attr('sel_price');
    var selling_discount = $(this).attr('selling_discount');

    var oldValue = $button.closest('.sp-quantity').find("input.quntity-input").val();

    if ($button.text() == "+") {
        var newVal = parseFloat(oldValue) + 1;
    } else {
        // Don't allow decrementing below zero
        if (oldValue > 0) {
            var newVal = parseFloat(oldValue) - 1;
        } else {
            newVal = 0;
        }
    }

    if(newVal<=max_oq && newVal>=min_oq){
        $button.closest('.sp-quantity').find("input.quntity-input").val(newVal);
        incrementdecrementtimerfun(parseInt(min_oq),parseInt(max_oq),parseInt(newVal),parseInt(inv_id),parseInt(sel_price));
    }else{
        //alert(newVal);
        alert('Please enter qty between '+min_oq+' and '+max_oq);
    }

});

</script>
