<?php
	$email_stuff_arr=email_stuff();
?>
<style>
 .center_column  a {
    color: #124b9b;
}  
</style>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        
        <!-- row -->
        <div class="row">
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading text-center">
                    <span class="page-heading-title2">Privacy Policy</span>
                </h2>
                <!-- Content page -->
                <div class="content-text clearfix">
               
					<p>Effective as of 1st of January 2021.</p>
					
					<p>
						This “Privacy Policy” describes how M/S. <?php echo SITE_NAME; ?> collects, uses, discloses and otherwise processes personal data in connection with this website or any of our other websites or any of our other online services that link to this Privacy Policy (collectively referred to as the “Sites”).
					</p>
					
					<h4>The Personal Data We Collect</h4>
					
					<p class="mt-10">
						We collect personal data about you on our Sites in the following ways:
					</p>
					
					<h4>Data you choose to provide</h4>
					
					<p class="mt-10">
						Personal data that you may provide through our Sites or otherwise share with us includes, but is not limited to:
					</p>
					
					<ul class="mt-10"  style="list-style-type: circle;margin-left:1em;list-style-position:inside;">
						<li>
							<b>Personal and business contact information,</b> such as your first name, last name, postal address, email address, telephone number, job title and employer name;
						</li>
						<li>
							<b>Professional credentials,</b> such as educational and work history, institutional affiliations and other types of information that would be included on a resume or curriculum vitae;
						</li>
						<li>
							<b>Profile information,</b> such as your username and password, industry, interests and preferences;
						</li>
						<li>
							<b>Feedback and correspondence,</b> such as information you provide in your responses to surveys, when you participate in market research activities, report a problem with the Sites, receive customer support or otherwise correspond with us;
						</li>
						<li>
							<b>Transaction information,</b> such as details about programs, events or other activities you register for through the Sites;
						</li>
						<li>
							<b>Usage information,</b> such as information about how you use the Sites and interact with us; and
						</li>
						<li>
							<b>Marketing information,</b> such as your preferences for receiving marketing communications.
						</li>
					</ul>
					<h4 class="mt-10">Information from social networking sites</h4>
					<p class="mt-10">
						Our Sites may include interfaces that allow you to connect with social networking sites (each a “SNS”). If you connect to a SNS through our Sites you authorize us to access, use and store the information that you agreed the SNS could provide to us based on your settings on that SNS. We will access, use and store that information in accordance with this Privacy Policy. You can revoke our future access to the information you provide to us through an SNS at any time by amending the appropriate settings within your account settings on the applicable SNS.
					</p>
					
					<h4>Information we get from others</h4>
					<p class="mt-10">
						We may also get information about you from other sources, and we may add this to information we get from our Sites. We may combine other publicly available information, such as information related to the organization for which you work, with the personal data that you provide to us through our Sites.
					</p>
					<h4>Information automatically collected</h4>
					<p class="mt-10">
						We may automatically log information about you and your computer or mobile device when you access our Sites. For example, we may log your computer or mobile device operating system name and version, manufacturer and model, browser type, browser language, screen resolution, the website you visited before browsing to our Sites, pages you viewed, how long you spent on a page, and access times and information about your use of, and actions on, our Sites. We collect this information about you using cookies. Please refer to the section below for more details.
					</p>
					
					<h4>How We Use Your Personal Data</h4>
					<h5 class="mt-10"><b> To provide you with information and administer our Sites</b></h5>
					
					<p class="mt-10">
						If you register to participate in our programs, events or other activities, or use our Sites, we use your personal data to:
					</p>
					
					<ul class="mt-10"  style="list-style-type: circle;margin-left:1em;list-style-position:inside;">
						<li>
							Operate, maintain, administer and improve the Sites;
						</li>
						<li>
							Process and manage registrations you make through the Sites;
						</li>
						<li>
							Communicate with you regarding our programs, events, or activities for which you may have registered, including by sending you technical notices, updates, security alerts, and support and administrative messages;
						</li>
						<li>
							Better understand your needs and interests, and personalize your experience with the Sites;
						</li>
						<li>
							Provide support and maintenance for the Sites and our services; and
						</li>
						<li>
							Respond to your service-related requests, questions and feedback.
						</li>
					</ul>
					
					<h5 class="mt-10"><b> To communicate with you</b></h5>
					<p class="mt-10">
						If you request information from us, register on the Sites, or participate in our surveys, programs, or events, we may send you Intercept-related marketing communications as permitted by law. You will have the ability to opt out of such communications.
					</p>
					<h5 class="mt-10"><b>To comply with law</b></h5>
					<p class="mt-10">
						We use your personal data as we believe necessary or appropriate to comply with applicable laws, lawful requests and legal processes, such as to respond to requests from government authorities.
					</p>
					<h5 class="mt-10"><b>With your consent</b></h5>
					<p class="mt-10">
						We may use or share your personal data with your consent, such as when you consent to let us post your testimonials or endorsements on our Sites, you instruct us to take a specific action with respect to your personal data, or you opt into third party marketing communications.
					</p>
					<h5 class="mt-10"><b>For compliance, fraud prevention and safety</b></h5>
					<p class="mt-10">			
We use your personal data as we believe necessary or appropriate to (a) enforce the terms and conditions that govern use of our Sites; (b) protect our rights, privacy, safety or property; and (c) protect, investigate and deter against fraudulent, harmful, unauthorized, unethical or illegal activity.

					</p>
					<h4 class="mt-10">How We Share Your Personal Data</h4>
					<p class="mt-10">			
						We disclose personal data to third parties under the following circumstances:
					</p>
					
					<ul>
						<li>
							<b>Affiliates.</b> We may disclose your personal data to our subsidiaries and corporate affiliates for purposes consistent with this Privacy Policy.
						</li>
						<li>
							<b>Service providers.</b> We may employ third party companies and individuals to administer and provide services on our behalf (such as training, customer support, website hosting, email delivery and database management services). These third parties use personal data as directed by us and in a manner consistent with this Privacy Policy.
						</li>
						<li>
							<b>Professional advisors.</b> We may disclose your personal data to professional advisors, such as lawyers, auditors and insurers, where necessary in the course of the professional services that they render to us.
						</li>
						<li>
							<b>Compliance with laws and law enforcement; protection and safety.</b> We may disclose information about you to government or law enforcement officials or private parties as required by law, and disclose and use such information as we believe necessary or appropriate to (a) comply with applicable laws and lawful requests, such as to respond to requests from government authorities; (b) enforce the terms and conditions that govern use of the Sites; (c) protect the rights, privacy, safety or property of users of our Sites and (d) protect, investigate and deter against fraudulent, harmful, unauthorized, unethical or illegal activity.
						</li>
						<li>
							<b>Business transfers.</b> We may sell, transfer or otherwise share some or all of our business or assets, including your personal data, in connection with a business deal (or potential business deal) such as a merger, consolidation, acquisition, reorganization or sale of assets or in the event of bankruptcy, in which case we will make reasonable efforts to require the recipient to honor this Privacy Policy.
						</li>
					</ul>
					<h4 class="mt-10">Your Rights</h4>
					<p class="mt-10">
						Regardless of where you reside, you can submit privacy inquiries and requests by email to <a href="mailto:<?php echo SITE_EMAIL_ENQ; ?>
"><?php echo SITE_EMAIL_ENQ; ?></a> or to our postal address provided below. If you reside in the EU, you may  request  that  we  take  the  following  actions  in  relation  to  your  personal  data:
					</p>
					<ul class="mt-10"  style="list-style-type: circle;margin-left:1em;list-style-position:inside;">
						<li>
							<b>Access.</b> Provide you with information about our processing of your personal data and give you access to your personal data.
						</li>
						<li>
							<b>Correct.</b> Update or correct inaccuracies in your personal data.
						</li>
						<li>
							<b>Delete.</b> Delete your personal data.
						</li>
						<li>
							<b>Transfer.</b> Transfer a machine-readable copy of your personal data to you or a third party of your choice.
						</li>
						<li>
							<b>Restrict.</b> Restrict the processing of your personal data.
						</li>
						<li>
							<b>Object.</b> Object to our legitimate interests as the basis of our processing of your personal data.
						</li>
					</ul>
					<p class="mt-10">	
						<strong>We may request specific information from you to help us confirm your identity and process your request.</strong> Applicable law may require or permit us to decline your request. If we decline your request, we will tell you why, subject to legal restrictions. If you reside in the EU and would like to submit a complaint about our use of your personal data or response to your requests regarding your personal data, you may contact us or submit a complaint to the data protection regulatory authority in your country.
					</p>
					<h4 class="mt-10">Changes to This Privacy Policy</h4>
					<p class="mt-10">
						We reserve the right to modify this Privacy Policy at any time. We encourage you to periodically review this page for the latest information on our privacy practices.
					</p>
					<h4 class="mt-10">Contact Us</h4>
					<p class="mt-10">
						If you have any questions or concerns about our Privacy Policy, please feel free to email us at <a href="mailto:<?php echo SITE_EMAIL_ENQ; ?>
"><?php echo SITE_EMAIL_ENQ; ?></a>
					</p>
					<p> or write to us at:</p>
					<ul class="mt-10">
						<li>
						<?php echo SITE_ADDR; ?>, </li>
						<li>Fully owned by </li>
						<li><strong>Livingseed Ayurveda,</strong> </li>
						<li>No. 988, Ground Floor,1st Cross,</li>
						<li>HAL 2nd Stage, Indira Nagar</li>
						<li>Bangalore – 560 008 </li>
					</ul>
					
					<ul class="mt-20">
						<li>Phone /Whatsapp:<a href="tel:+918050504950"> +91 – 80505 04950 </a></li>
						<li>Email: <a href="mailto:<?php echo SITE_EMAIL_ENQ; ?>"><?php echo SITE_EMAIL_ENQ; ?></a> </li>
						<li>Work Hours: Mon – Sat (09:00 – 05:30pm) | Please check with us on public / company list of holidays </li>
					</ul>
					
                </div>
                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<!-- ./page wapper-->
