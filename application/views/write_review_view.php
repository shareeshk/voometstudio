<style type="text/css">
.align_top{
	margin-top:10px;
}
.gly-star{
    margin-right: 5px;
    font-size: 20px;
    cursor: pointer;
}
.selected_text{
    color:#eda900 !important;
}
.unselected_text{
    color:#cccccc !important;
}
</style>
<link href="<?php echo base_url();?>assets/js/rating1/rateit.css" rel="stylesheet" type="text/css">

<script src='<?php echo base_url();?>assets/js/rating2/jquery.MetaData.js' type="text/javascript" language="javascript"></script>
<script src='<?php echo base_url();?>assets/js/rating2/jquery.rating.js' type="text/javascript" language="javascript"></script>
<link href='<?php echo base_url();?>assets/js/rating2/jquery.rating.css' type="text/css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/js/rating1/jquery.rateit.js" type="text/javascript"></script>

<script type="text/javascript"> 

	$(document).ready(function(){
		$(".star-rating").find("a").each(function(){
			$(this).html("");
		})
	});
</script>

<?php
$customer_id=$this->session->userdata('customer_id');
$order_item_arr=$controller->get_order_item_id($inventory_id,$customer_id);

//print_r($order_item_arr);

if(!empty($order_item_arr)){
	
	$hour=date("H:i:s", strtotime($order_item_arr["order_delivered_time"]));
	$date=date("Y:m:d", strtotime($order_item_arr["order_delivered_date"]));
	$purchased_timestamp=$date." ".$hour;
	$order_item_id=$order_item_arr["order_item_id"];
	$rated_data=$controller->get_data_from_rated_inventory($inventory_id,$customer_id);
}else{
	header('Location:'.base_url());
	exit;
}

//print_r($rated_data);

if(!empty($rated_data)){
	$rating=$rated_data["rating"];
	$review_title=$rated_data["review_title"];
	$review=$rated_data["review"];
}else{
	$rating=0;
	$review_title="";
	$review="";
}

 ?>
<div class="columns-container">
	<div class="container" id="columns">

		<div class="page-content">
<div class="row">
<div id="lightboxform_<?php echo $inventory_id; ?>" tabindex="-1" data-width="500">
	
	
	<div class="col-md-10">
	<h2 class="margin-bottom">
            <span class="product-name">Write a Review for <?php echo $product_name; ?></span>
    </h2>
		<div class="box-authentication">
		<form name="create_review" id="create_review" class="form-horizontal">
		<input type="hidden" name="purchased_timestamp" value="<?php echo $purchased_timestamp; ?>" id="purchased_timestamp">
		<input type="hidden" name="order_item_id" value="<?php echo $order_item_id; ?>" id="order_item_id">

            <div class="form-group">
                <div class="col-md-3">
                    <label for="inputPassword">Rate the Product</label>
                </div>
                <!--<div class="col-md-6">
                    <div class="controls1"></div>
                    <div class="rateit" style="margin-top: 10px;" id="rateit13_<?php /*echo $inventory_id; */?>"></div>
                </div>-->
                <div class="col-md-4">
                    <span class="glyphicon glyphicon-star gly-star g1" style="<?php echo ($rating>=1) ? 'color: #eda900; ' : 'color: #ccc; '; ?>"></span>
                    <span class="glyphicon glyphicon-star gly-star g2" style="<?php echo ($rating>=2) ? 'color: #eda900; ' : 'color: #ccc; '; ?>"></span>
                    <span class="glyphicon glyphicon-star gly-star g3" style="<?php echo ($rating>=3) ? 'color: #eda900; ' : 'color: #ccc; '; ?>"></span>
                    <span class="glyphicon glyphicon-star gly-star g4" style="<?php echo ($rating>=4) ? 'color: #eda900; ' : 'color: #ccc; '; ?>"></span>
                    <span class="glyphicon glyphicon-star gly-star g5" style="<?php echo ($rating>=5) ? 'color: #eda900; ' : 'color: #ccc; '; ?>"></span>
                </div>
                <div class="col-md-2 text-right">
                    <span id="star_text"></span>
                    <input type="hidden" value="" id="rate" name="rate" value="<?php echo $rating; ?>">
                </div>

            </div>
			<div class="form-group">
				 <div class="col-md-3">
					<label for="review_title">Review Title</label>
                 </div>
				<div class="col-md-6">				 
					<input id="review_title" name="review_title" value="<?php echo $review_title; ?>" type="text" class="form-control" maxlength="50">
				</div>
			</div>
				<div class="form-group">
				<div class="col-md-3">
					<label for="review">Write a Review</label>
				</div>
				<div class="col-md-6">
					<textarea cols="20" rows="5" name="review" class="form-control"><?php echo $review; ?></textarea>
				</div>
				</div>

			
				<input type="hidden"  name="inventory_id" value="<?php echo $inventory_id; ?>" id="inventory_id">
				<input type="hidden"  name="rate1"  id="rate1_<?php echo $inventory_id; ?>" value="<?php echo $rating; ?>">
				<input type="hidden" value="<?php echo $this->session->userdata('customer_id') ?>" name="customer_id">
				<input type="hidden" value="<?php echo $this->session->userdata('customer_name'); ?>" name="customer_name"> 
				<div class="col-md-12 text-center">
					<button class="button preventDflt" id="submit-btn" type="submit" onclick="form_validation()">Submit</button>
				</div>
			</form>
			</div>
				
		</div>
		</div>
</div>



		</div>
	</div>
</div>


<script type="text/javascript">

$(document).ready(function(){
/*	document.getElementById('rateit-range-2').setAttribute('aria-valuenow',<?php echo $rating?>);
	var width=<?php echo $rating?>*16;
	document.getElementsByClassName('rateit-selected')[0].setAttribute('style','height:16px;width: '+width+'px; display: block')
	
	});
	
	
	$('#rateit13_<?php echo $inventory_id; ?>').on('beforerated', function (e, value) {
	$('#rate1_<?php echo $inventory_id; ?>').val(value);
	});
	$('#rateit13_<?php echo $inventory_id; ?>').on('beforereset', function (e) {
		
	if (!confirm('Are you sure you want to reset the rating?')) {
	e.preventDefault();
	}
*/
	});
		

		
		
function form_validation(){
	
	var review_title = $('input[name="review_title"]').val().trim();
	var review = $('textarea[name="review"]').val().trim();
	var rate = $('input[name="rate"]').val();

	   var err = 0;
		/*if(!(review_title.length>=3)){
		   $('input[name="review_title"]').css("border", "1px solid red");	   
		   err = 1;
		}else{			
			$('input[name="review_title"]').css({"border": "1px solid #ccc"});
		}
		if(review==''){
		   $('textarea[name="review"]').css("border", "1px solid red");	   
		   err = 1;
		}else{			
			$('textarea[name="review"]').css({"border": "1px solid #ccc"});
		}
		*/


        if(rate=='' || parseFloat(rate)==0){
		  //$('#rate_error') .show();
		   err = 1;
		}else{
			//$('#rate_error') .hide();
		}
	
		if(err==1){
            /*bootbox.alert({
                size: "small",
                message:'<span style="color: red;">Please rate the product</span>',
            });*/

            alert('Please rate the product');

            $('#error_result_id').show();
            $('#result').hide();
		}else{

            $('#submit-btn').prop('disabled', true);
			var form_status = $('<div class="form_status"></div>');		
			var form = $('#create_review');		
			
			url='<?php echo base_url()."rated_inventory"?>';
			
					 $.ajax({
						url: url,
						type: 'POST',
						data: $('#create_review').serialize(),
						dataType: 'html',
						beforeSend: function(){
						form.prepend( form_status.html('<p><i class="fa fa-refresh fa-spin"></i> Processing...</p>').fadeOut() );
						}
					}).done(function(data){
						
							if(data=="no_access"){
								/*bootbox.alert({
								  size: "small",
								  message: "Sorry..! So you can't write a review",
								});*/
                                alert("Sorry..! So you can't write a review");
								$('#create_review')[0].reset();
                                $('#submit-btn').prop('disabled', true);
								return false;
							}
							if(data){
								/*bootbox.alert({
								  size: "small",
								  message:'Thank You..! Review has been submitted successfully',
								});*/
                                alert('Thank You..! Review has been submitted successfully');
								$('#create_review')[0].reset();
								form_status.html('<p class="text-success">Review has been successfully submitted</p>').fadeOut("fast",function(){	  
									window.location.href=data;
								});  							  
								 
							}else{	
								
								/*bootbox.alert({
								  size: "small",
								  message:'Sorry , Some error',
								});*/
                                alert('Sorry , Some error');
								$('#create_review')[0].reset();
                                $('#submit-btn').prop('disabled', false);
							}	
						
					}); 
			}
	return false;
}
</script>

<script>
/*
<?php
					if(!empty($rated_data)){
					?>
				<div class="col-md-6" style="margin-left:25%;">
				
                    <form>
                    <input class="a star {split:2}" type="radio"  value="0.5" disabled="disabled"  <?php if($rating==0.5){echo "checked='checked'";}?>/>
                    <input class="b star {split:2}" type="radio"  value="1" disabled="disabled"  <?php if($rating==1){echo "checked='checked'";}?>/>
                    <input class="a star {split:2}" type="radio"  value="1.5" disabled="disabled"  <?php if($rating==1.5){echo "checked='checked'";}?>/>
                    <input class="b star {split:2}" type="radio"  value="2" disabled="disabled"  <?php if($rating==2){echo "checked='checked'";}?>/>
                    <input class="a star {split:2}" type="radio"  value="2.5" disabled="disabled"  <?php if($rating==2.5){echo "checked='checked'";}?>/>
                    <input class="b star {split:2}" type="radio"  value="3" disabled="disabled"  <?php if($rating==3){echo "checked='checked'";}?>/>
                    <input class="a star {split:2}" type="radio"  value="3.5" disabled="disabled"  <?php if($rating==3.5){echo "checked='checked'";}?>/>
                    <input class="b star {split:2}" type="radio"  value="4" disabled="disabled"  <?php if($rating==4){echo "checked='checked'";}?>/>
                    <input class="b star {split:2}" type="radio"  value="4.5" disabled="disabled"  <?php if($rating==4.5){echo "checked='checked'";}?>/>
                    <input class="b star {split:2}" type="radio"  value="5" disabled="disabled"  <?php if($rating==5){echo "checked='checked'";}?>/>
                    </form>
                    </div>
					
					<?php 
					}else{
					?>
					
					<?php }?>
					
*/

rate=$('#rate').val();

$(".g1" ).hover(function() {

        if ($('.g1').hasClass("selected_text")) {
            if(parseInt($('#rate').val())!=1) {
                $(".g1").removeClass("selected_text");
                $("#star_text").html('');
            }
        } else {
            $(".g1").removeClass("unselected_text");
            $(".g1").addClass("selected_text");
            $("#star_text").html('<span style="color:red"> Very bad</span>');
        }
});

$(".g2" ).hover(function() {

        if ($('.g2').hasClass("selected_text")) {
            if(parseInt($('#rate').val())!=2) {
                $(".g1,.g2").removeClass("selected_text");
                $("#star_text").html('');
            }
        } else {
            $(".g1,.g2").removeClass("unselected_text");
            $(".g1,.g2").addClass("selected_text");
            $("#star_text").html('<span style="color:red">Bad</span>');
        }
        //$(".g1,.g2").addClass("selected_text");
});
$(".g3" ).hover(function() {

        if ($('.g3').hasClass("selected_text")) {
            if(parseInt($('#rate').val())!=3) {
                $(".g1,.g2,.g3").removeClass("selected_text");
                $("#star_text").html('');
            }
        } else {
            $(".g1,.g2,.g3").removeClass("unselected_text");
            $(".g1,.g2,.g3").addClass("selected_text");
            $("#star_text").html('<span style="color:green">Good</span>');
        }
        // $(".g1,.g2,.g3").addClass("selected_text");
});
$(".g4" ).hover(function() {

        if ($('.g4').hasClass("selected_text")) {
            if(parseInt($('#rate').val())!=4) {
                $(".g1,.g2,.g3,.g4").removeClass("selected_text");
                $("#star_text").html('');
            }
        } else {
            $(".g1,.g2,.g3,.g4").removeClass("unselected_text");
            $(".g1,.g2,.g3,.g4").addClass("selected_text");
            $("#star_text").html('<span style="color:green">Very Good</span>');
        }
        //$(".g1,.g2,.g3,.g4").addClass("selected_text");
});
$(".g5" ).hover(function() {

        if ($('.g5').hasClass("selected_text")) {
            if(parseInt($('#rate').val())!=5) {
                $(".g1,.g2,.g3,.g4,.g5").removeClass("selected_text");
                $("#star_text").html('');
            }
        } else {
            $(".g1,.g2,.g3,.g4,.g5").removeClass("unselected_text");
            $(".g1,.g2,.g3,.g4,.g5").addClass("selected_text");
            $("#star_text").html('<span style="color:green">Excellent</span>');
        }
        //$(".g1,.g2,.g3,.g4,.g5").addClass("selected_text");

});

$('.g1').click(function () {
    $(".g1").removeClass("unselected_text");
    $(".g1").addClass("selected_text");
    $("#star_text").html('Very bad');
    $('#rate').val(1);
    $('.g2,.g3,.g4,.g5').addClass("unselected_text");
});
$('.g2').click(function () {
    $(".g1,.g2").removeClass("unselected_text");
    $(".g1,.g2").addClass("selected_text");
    $("#star_text").html('Bad');
    $('#rate').val(2);
    $('.g3,.g4,.g5').addClass("unselected_text");
});
$('.g3').click(function () {
    $(".g1,.g2,.g3").removeClass("unselected_text");
    $(".g1,.g2,.g3").addClass("selected_text");
    $("#star_text").html('Good');
    $('#rate').val(3);
    $('.g4,.g5').addClass("unselected_text");
});

$('.g4').click(function () {
    $(".g1,.g2,.g3,.g4").removeClass("unselected_text");
    $(".g1,.g2,.g3,.g4").addClass("selected_text");
    $("#star_text").html('Very Good');
    $('#rate').val(4);
    $('.g5').addClass("unselected_text");
});
$('.g5').click(function () {
    $(".g1,.g2,.g3,.g4,.g5").removeClass("unselected_text");
    $(".g1,.g2,.g3,.g4,.g5").addClass("selected_text");
    $("#star_text").html('Excellent');
    $('#rate').val(5);
    //alert('5 clicked');
    return false;
});

$(document).ready(function (){
});

</script>
			