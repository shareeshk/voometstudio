<?php 
		$bg="";
		$ft="";
		if(!empty($ui_arr["bg_img"])){
			$bg=$ui_arr["bg_img"]->background_image;
		}
		if(!empty($ui_arr["ft_img"])){
			$ft=$ui_arr["ft_img"]->background_image;
		}
	?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/skins/jquery-ui-like/progressbar.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/product_comparision/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/product_comparision/w3.css" />

<link rel="stylesheet" href="assets/css/jquery.fancybox.css" media="screen">
<script src="assets/js/jquery.fancybox.js"></script>

<style type="text/css">

@media only screen and (max-width: 600px) {
  body {
    background-color: lightblue;
  }
  
}

body{
background-color: #f1f3f6;
}

.section-title{
	line-height: 30px !important;
}
.our_partner1 {
    margin: 0 auto;
    max-width: 100%;
    padding: 30px 0;
}

.our_partner {
    -moz-box-pack: center;
    display: -moz-box;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
}
.our_partner .our_partner_inner {
    -moz-box-flex: 0;
    grid-column-gap: 20px;
    -moz-box-align: center;
    align-items: center;
    background-color: #fff;
    border: 1px solid #ccc;
    border-radius: 8px;
    display: -moz-box;
    display: flex;
    flex: 0 1 48%;
    grid-template-columns: -webkit-max-content auto;
    grid-template-columns: max-content auto;
    margin-bottom: 2%;
    margin-right: 2%;
    outline: none;
    padding: 24px;
}
.our_partner_img{
	/* background-color: #eee; */
    /* border-radius: 50%;
	vertical-align: top;
	width: 50%; */
	text-align:center;
   
}
.our_partner_img img{
	background-color: #eee;
    border-radius: 50%;
	vertical-align: top;
}
.designation{
	color: #9b9b9b;
    display: block;
    font-size: 13px;
    letter-spacing: 0;
    line-height: 16px;
}
.name{
	display: block;
    font-size: 14px;
    font-weight: 600;
    letter-spacing: 0;
    line-height: 17px;
    margin-top: 12px;
}

.bt_20{
	margin-bottom: 20px;
}
.img_side{
	border-radius: 8px;
    height: auto;
    width: 100%;
}
.p_top{
	margin-top:10px;line-height:25px;
}
.m_top{
	margin-top:20px;
}
.well {
    -webkit-box-shadow: 0 8px 20px rgba(0, 0, 0, 0.1);
    -moz-box-shadow: 0 8px 20px rgba(0, 0, 0, 0.1);
    box-shadow: 0 8px 20px rgba(0, 0, 0, 0.1);
    border-top: 2px solid #ccc;
    background-color: #f7f7f7;
    transition: all 0.3s ease-in-out 0s;
}


#demo {
  height:100%;
  position:relative;
  overflow:hidden;
}


.green{
  background-color:#6fb936;
}
        .thumb{
            margin-bottom: 30px;
        }
        
        .page-top{
            margin-top:85px;
        }

   .gal_img{
	padding-bottom: 32%;
    border-radius: 8px;
    display: inline-block;
    position: relative;
    width: 31%;
    text-align: center;
    margin: 1%;
   }
img.zoom {
    width: 100%;
    height: 200px;
    border-radius:5px;
    object-fit:cover;
    -webkit-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
    -ms-transition: all .3s ease-in-out;
}
        
 
.transition {
    -webkit-transform: scale(1.2); 
    -moz-transform: scale(1.2);
    -o-transform: scale(1.2);
    transform: scale(1.2);
}
    .modal-header {
   
     border-bottom: none;
}
    .modal-title {
        color:#000;
    }
    .modal-footer{
      display:none;  
    }
/*faq*/

.faqHeader {
        font-size: 27px;
        margin: 20px;
    }

    .panel-heading [data-toggle="collapse"]:after {
        font-family: 'fontawesome';
        content: "\f054"; /* "play" icon */
        float: right;
        color: #F58723;
        font-size: 18px;
        line-height: 22px;
        /* rotate "play" icon from > (right arrow) to down arrow */
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        -ms-transform: rotate(-90deg);
        -o-transform: rotate(-90deg);
        transform: rotate(-90deg);
    }

    .panel-heading [data-toggle="collapse"].collapsed:after {
        /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        transform: rotate(90deg);
        color: #454444;
    }

	.services_icon{
		margin: 0 auto;
		max-width: 100%;
		padding: 30px 0;
		display: flex;
		flex-wrap: wrap;
		justify-content: center;
	}
	.img_text{
		position: absolute;
    bottom: 0;
   /* right: 0;*/
    color: #fff;
    font-size: 13px;
    font-weight:  500;
    text-align: center;
    width: 85%;
    background: linear-gradient(to top, #120e0e42 100%, #333230 76%);
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
	}

    @media (max-width: 420px){
.our_partner .our_partner_inner
  {
	display: block;
    flex: 0 1 100%;
    text-align: center;
  }
}
</style>

<style>
.services2 .services2-item,.services2{
border:0px;
}
.services2 .services2-item .image{
padding-left:120px;
}
.form-group .help-block {
display: none;
}
.form-group.has-error .help-block {
display: block;
}
.block-trending{
margin-top:0px !important;
}

.section-count {
    grid-column-gap: 80px;
    -moz-box-pack: center;

    grid-template-columns: -webkit-max-content -webkit-max-content -webkit-max-content -webkit-max-content;
    grid-template-columns: max-content max-content max-content max-content;
    justify-content: center;
    padding: 40px;
    text-align: center;

    font-size: 24px;
    font-weight: 700;
    letter-spacing: 0;
    line-height: 40px;
    text-transform: uppercase;

}
.colr_update{
	color:#ff3300;
}
.font_big{
    font-size: 48px;
}
.review_text{
	font-size:20px;
}
.yellow{
	color:gold;
}
.link_color{
	color:#ff3300;
}
.d-flex {
    display: -ms-flexbox !important;
    display: flex !important;
}
.col{
	padding-right: 15px;
    padding-left: 15px;
}
.page-heading {
   
    padding: 0px;
}
</style>

	
	<style type="text/css">

	.bodyimage {
	<?php if($bg!=''){
		?>
	background: url('<?php echo base_url().$bg;?>') no-repeat;	
	
		<?php 
	}else{
		?>
		background:#b3c95c;
		<?php
		
	}?>
	background-size: 100% auto;
	  z-index:-100;
	}
	</style>
	
	
	<style type="text/css">
.form-group .help-block {
  display: none;
}
 
.form-group.has-error .help-block {
  display: block;
}
</style>


<style>
.swal2-modal .swal2-styled{
	background-color:#1429a0 !important;
}
</style>




<!-- Prakruthi Pariksha Details Starts -->
<section class="section8  content-gap  block-trending" id="book_a_doctor_appointment">
<!--<section class="section8 block-trending" id="products_range">--->
    <div class="section-container">
        
	</div>
        <div class="section-content">
			<a id="schedule_appointment"></a>
            <div class="container">
				<div class="row bt_20">
	<h3 class="section-title text-center align-bottom" style="margin-bottom:2rem;line-height:30px;">Become Our Franchise</h3>
					<div class="col-md-6">
   <h2 class="page-heading">
                    <span class="page-heading-title2">Become the proud owner of  <?php echo SITE_NAME; ?></span>
                </h2>
						
						<div class="align-top" style="margin-top:20px;line-height:25px;">
							<p class="p_top">
							Introducing Voomet Studio - where innovation meets elegance in interior design. As India’s premier design house and interior design firm, we redefine spaces with our unmatched expertise in home interiors and redesign services. With over a decade of experience and a team of seasoned professionals, we bring a fresh perspective to every project.</p>
<p class="p_top">We pride ourselves on being trendsetters, setting new standards in the industry. Our commitment to quality is reflected in our ever-expanding client base, which speaks volumes about our excellence.</p>
<p class="p_top">Each project at Voomet Studio is handled by small, dynamic interdisciplinary teams, ensuring personalized attention and meticulous execution. With a track record of over 2300 homes transformed, we offer end-to-end interior design solutions that exceed expectations.</p>
<p class="p_top">Experience the difference with Voomet Studio – where creativity meets functionality, and every space tells a unique story.</p>

							</p>
						</div>
					</div>

					<div class="col-md-4 col-md-offset-1">
				
						<div class="box-authentication pb-0"  ng-controller="beomeourfranchiseController">
							<form name="becomeourfranchiseForm" id="createuser" novalidate>
							
								<!-- <div class="form-group" show-errors>
									<input ng-model="formData_enquiry.beomeourfranchise_companyname" name="beomeourfranchise_companyname" type="text" class="form-control" placeholder="Enter Your Company Name" required>
									<p class="help-block" ng-if="becomeourfranchiseForm.beomeourfranchise_companyname.$error.required">Name is required</p>
								</div>
								 -->
								
								<div class="form-group" show-errors>
									<input ng-model="formData_enquiry.beomeourfranchise_name" name="beomeourfranchise_name" type="text" class="form-control" placeholder="Enter Name" required >
									<p class="help-block" ng-if="becomeourfranchiseForm.beomeourfranchise_name.$error.required">Name is required</p>
								</div>
								
			
								
								<div class="form-group" show-errors>
										<input type="email" name="email" class="form-control" ng-model="formData_enquiry.email" required placeholder="Enter Your Email"  email-exist-c="formData_enquiry.email" >
				   	
				
                            <p class="help-block" ng-if="becomeourfranchiseForm.email.$error.emailexistValidator">This Email is  registered already.</p>
				  
							<p class="help-block" ng-if="becomeourfranchiseForm.email.$error.required">The user's email is required</p>
							<p class="help-block" ng-if="becomeourfranchiseForm.email.$error.email">The email address is invalid</p>	
					
					
								</div>
								
								<div class="form-group" show-errors>
										
                   <input id="mobile_number" type="number"  ng-minlength="10" 	
                   ng-maxlength="10" min="0" onpaste="return false;" name="mobile" class="form-control" ng-model="formData_enquiry.mobile" required allow-only-numbers mobile-exist-c="formData_enquiry.mobile" placeholder="Mobile No" maxlength="10" onkeypress="return isNumber(event)">	
								
								<p class="help-block" ng-show="becomeourfranchiseForm.beomeourfranchise_mobile.$error.required || becomeourfranchiseForm.beomeourfranchise_mobile.$error.number" >Valid mobile number is required
								</p>

                                <p class="help-block" ng-show="becomeourfranchiseForm.mobile.$error.mobileexistValidator">This mobile is registered already.</p>
							
								<p class="help-block" ng-show="((becomeourfranchiseForm.beomeourfranchise_mobile.$error.minlength ||
								becomeourfranchiseForm.beomeourfranchise_mobile.$error.maxlength) && 
								becomeourfranchiseForm.beomeourfranchise_mobile.$dirty) ">
											Mobile number should be 10 digits
								</p>
								
								</div>
								
								<!---- <div class="form-group" show-errors>
									<input type="text" name="beomeourfranchise_city" class="form-control" ng-model="formData_enquiry.beomeourfranchise_city" placeholder="Enter Your City" required>
									<p class="help-block" ng-if="becomeourfranchiseForm.beomeourfranchise_city.$error.required">City is required</p>
								</div> -->

                            <!---- pincode  ---->

                            <div class="form-group" show-errors id="pin_code_check">
								
								<input type="text" ng-minlength="6"
								   ng-maxlength="6" name="pin" class="form-control" ng-model="formData_enquiry.pin" required allow-only-numbers  placeholder='PIN/ZIP Code'  onkeypress="return isNumber(event)"  ng-paste="$event.preventDefault()" ng-pattern="/^[0-9]*$/" oninput="if(value.length>6)value=value.slice(0,6)" ng-keyup="get_city_state_details_by_pincodeFun()">
								   
								   <p class="help-block" ng-show="becomeourfranchiseForm.pin.$error.required || becomeourfranchiseForm.pin.$error.number">Valid pin code is required
									</p>
								
									<p class="help-block" ng-show="((becomeourfranchiseForm.pin.$error.minlength ||
                                    becomeourfranchiseForm.pin.$error.maxlength) && 
                                    becomeourfranchiseForm.pin.$dirty) ">
												   Pincode should be 6 digits
									</p>
                                    <span class="delivery_address_pin_error" style="display:none;color:#a94442">
                                        Please enter valid pincode
                                    </span>
											
							</div>
							
							
							<div class="form-group"  show-errors>
								
								<input  name="city" ng-model="formData_enquiry.city" required placeholder="City" type="text" class="form-control" readonly>
								<p class="help-block" ng-if="becomeourfranchiseForm.city.$error.required">The City is required</p>
							</div>
						
							<div class="form-group"  show-errors>
							<input  name="state" ng-model="formData_enquiry.state" required placeholder="State/Province" type="text" class="form-control" readonly>
								
								
								<p class="help-block" ng-if="becomeourfranchiseForm.state.$error.required">The State is required</p>
							</div>
														
							
							<div class="form-group"  show-errors>
								<input  name="country" ng-model="formData_enquiry.country" required placeholder="Country" type="text" class="form-control" readonly>
								<p class="help-block" ng-if="becomeourfranchiseForm.country.$error.required">The Country is required</p>
							</div>
						
<!--- address of parter--->


                                <!---- pincode  ---->
								

								<div class="form-group">
									<button class="btn btn-sm btn-block"  ng-click="save_becomeourfranchise();$event.stopPropagation();" ng-disabled="becomeourfranchiseForm.$invalid"  style="background-color:#1429a0;color:#fff;font-size:1 em;opacity:1" id="becomeourfranchiseForm_btn"> Join Our Network </button>
									
									<p><br><small>By submitting this form, you agree to the  <a href="policy" class="link_color">privacy policy</a> and <a href="termsandconditions" class="link_color">terms of use</a></small></p>
								</div>
							</form>
						</div>
					</div>


                
                </div><!---row--->
<h3 class="section-title text-center align-bottom align-top"></h3>
				<div class="row bt_20 mt-60">
		
				<div class="col-md-6">
					<div class="d-flex">
                                <div class="col">
                                    <!-- ttm_single_image-wrapper -->
                                    <div class="ttm_single_image-wrapper pt-60">
                                        <img width="270" class="img-fluid" src="<?php echo base_url()?>assets/images/founder1.jpg" alt="Interior Designers in Bangalore">										
                                    </div>
                                </div>
                                <div class="col">
                                    <!-- ttm_single_image-wrapper -->
                                    <div class="ttm_single_image-wrapper">
                                        <img width="270" class="img-fluid" src="<?php echo base_url()?>assets/images/founder.jpg" alt="Interior Designers in Bangalore">
                                    </div>
                                </div>
                            </div>
				</div>
				
				<div class="col-md-6">
					<p class="p_top"><?php echo SITE_NAME; ?> is India’s fastest growing home interiors brand. We help homeowners set up their dream homes, by bringing professionalism, predictability, convenience and transparency to this otherwise unorganised sector. Since our inception in 2014, we have grown rapidly, having served 25000+ happy homeowners, and currently operate in 10 major cities across the country.</p>
					<p class="p_top">Start your entrepreneurs journey through our well known brand. We will take care of everything from payments, manufacturing to delivery and installation. 
</p>
				</div>

				</div>

				<div class="row bt_20">
				<div class="col-md-12 m_top">
				<h3 class="section-title text-center align-bottom align-top">Build the future of home interiors. Franchise with <?php echo SITE_NAME; ?>.</h3>

				</div>
				</div>

				<div class="row bt_20 m_top" style="margin-bottom:35px;">
				<div class="col-md-6">
				<img class="img_side" src="<?php echo base_url()?>assets/images/slider2.png" />
				</div>
				<div class="col-md-6">
					 <h2 class="page-heading">
                    <span class="page-heading-title2">What you'll need</span>
                </h2>
					
					<p class="p_top">
As a <?php echo SITE_NAME; ?> Franchisee you’ll need to ensure a floor space of 1200 - 3500 sq. ft and make a financial investment of 50 Lacs - 1.25 Cr to set up the Studio, depending on the market potential. A franchisee plays an important role in providing customers a seamless home interiors journey and is mainly responsible for undertaking installation services, providing and maintaining Studio infrastructure and facilities, making sure everything is aligned with the brand's purpose.</p>
				</div>

				</div>
				
				<div class="row bt_20  m_top p_top">
				
				<div class="col-md-6">
					 <h2 class="page-heading">
                    <span class="page-heading-title2">What you'll get</span>
                </h2>
					
					<p class="p_top">
					<p>We are the fastest growing home interiors brand in the country, and we offer the best operating network and world-class support to each one of our Franchisees. We also make sure you are backed up by a fantastic supply chain and the brand value, to get you started on the right track from the very first day.</p>

 
					<p><?php echo SITE_NAME; ?>
 will hand-hold you through the entire journey of drawing up and signing of the franchise agreement, property finalization, training the installation team, handling day-to-day operations of the Studio infrastructure, etc. As a Franchisee you can expect to do operational break-even in 7 months with an ROI of 400% over a period of 5 yrs of agreement.</p>
				</div>

				<div class="col-md-6">
				<img class="img_side" src="<?php echo base_url()?>assets/images/slider3.png" />
				</div>

				</div>

				<div class="row bt_20">
				<div class="col-md-12 m_top">
				<h3 class="section-title text-center align-bottom align-top">Our partners' success stories</h3>

				</div>
				</div>

				<div class="row bt_20 our_partner1">
				<div class="col-md-12 our_partner">

				<div class="our_partner_inner"><div class="our_partner_img"><img alt="" src="https://super.homelane.com/squarepartner/20190929_144437.png" data-srcset="https://super.homelane.com/squarepartner/20190929_144437.png" style="opacity: 1;"></div><div class="squarePartnerStory_storyComment__IDyCs"><p class="squarePartnerStory_comments__1teHV">“I believe in doing business that focuses on providing positive customer experience, both at the point of sale and after. HomeLane has enabled that, keeping profits in mind.”</p><span class="name">Lokesh</span><span class="designation">Experience Partner, HomeLane Bengaluru and Pune</span></div></div>

				<div class="our_partner_inner"><div class="our_partner_img"><img alt="" src="https://super.homelane.com/squarepartner/20190929_144437.png" data-srcset="https://super.homelane.com/squarepartner/20190929_144437.png" style="opacity: 1;"></div><div class="squarePartnerStory_storyComment__IDyCs"><p class="squarePartnerStory_comments__1teHV">“I believe in doing business that focuses on providing positive customer experience, both at the point of sale and after. HomeLane has enabled that, keeping profits in mind.”</p><span class="name">Lokesh</span><span class="designation">Experience Partner, HomeLane Bengaluru and Pune</span></div></div>

				<div class="our_partner_inner"><div class="our_partner_img"><img alt="" src="https://super.homelane.com/squarepartner/20190929_144437.png" data-srcset="https://super.homelane.com/squarepartner/20190929_144437.png" style="opacity: 1;"></div><div class="squarePartnerStory_storyComment__IDyCs"><p class="squarePartnerStory_comments__1teHV">“I believe in doing business that focuses on providing positive customer experience, both at the point of sale and after. HomeLane has enabled that, keeping profits in mind.”</p><span class="name">Lokesh</span><span class="designation">Experience Partner, HomeLane Bengaluru and Pune</span></div></div>


				</div>

				</div><!--row--->


				<!---count ---->
				
				<div class="row m_top" style="background-color: #fff;">
					<div class="col-md-8 col-md-offset-2 text-center section-count">
						<div class="col-md-4 text-center">
							<div class="colr_update font_big">17</div>
							<div>Cities</div>

						</div>
						<div class="col-md-4 text-center">
							<div class="colr_update font_big">36</div>
							<div>STUDIOS</div>

						</div>
						<div class="col-md-4 text-center">
							<div class="colr_update font_big">25000+</div>
							<div>HAPPY CUSTOMERS</div>

						</div>
					</div>
				</div>
				<!---count ---->


				<!--gallery--->

				<div class="row m_top">

<div class="col-md-6">
            <div class="col-lg-4 col-md-4 col-xs-6 thumb">
                <a href="https://images.pexels.com/photos/62307/air-bubbles-diving-underwater-blow-62307.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" class="fancybox" rel="ligthbox">
                    <img  src="https://images.pexels.com/photos/62307/air-bubbles-diving-underwater-blow-62307.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" class="zoom img-fluid "  alt="">
                   <div class="text-center img_text">Bangalore</div> 
                </a>
            </div>
            <div class="col-lg-4 col-md-4 col-xs-6 thumb">
                <a href="https://images.pexels.com/photos/38238/maldives-ile-beach-sun-38238.jpeg?auto=compress&cs=tinysrgb&h=650&w=940"  class="fancybox" rel="ligthbox">
                    <img  src="https://images.pexels.com/photos/38238/maldives-ile-beach-sun-38238.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" class="zoom img-fluid"  alt="">
					<div class="text-center img_text">Chennai</div>
                </a>
            </div>
            
            <div class="col-lg-4 col-md-4 col-xs-6 thumb">
                <a href="https://images.pexels.com/photos/158827/field-corn-air-frisch-158827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="fancybox" rel="ligthbox">
                    <img  src="https://images.pexels.com/photos/158827/field-corn-air-frisch-158827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="zoom img-fluid "  alt="">
					<div class="text-center img_text">Hyderabad</div>
                </a>
            </div>
            
            <div class="col-lg-4 col-md-4 col-xs-6 thumb">
                <a href="https://images.pexels.com/photos/302804/pexels-photo-302804.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="fancybox" rel="ligthbox">
                    <img  src="https://images.pexels.com/photos/302804/pexels-photo-302804.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="zoom img-fluid "  alt="">
					<div class="text-center img_text">Mumbai</div>
                </a>
            </div>
            
             <div class="col-lg-4 col-md-4 col-xs-6 thumb">
                <a href="https://images.pexels.com/photos/1038914/pexels-photo-1038914.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" class="fancybox" rel="ligthbox">
                    <img  src="https://images.pexels.com/photos/1038914/pexels-photo-1038914.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" class="zoom img-fluid "  alt="">
					<div class="text-center img_text">Kolkata</div>
                </a>
            </div>
            
             <div class="col-lg-4 col-md-4 col-xs-6 thumb">
                <a href="https://images.pexels.com/photos/414645/pexels-photo-414645.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" class="fancybox" rel="ligthbox">
                    <img  src="https://images.pexels.com/photos/414645/pexels-photo-414645.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" class="zoom img-fluid "  alt="">
					<div class="text-center img_text">Coimbatore</div>
                </a>


            </div>
            
             
            
</div>
            
           <!---google reviews--->

		   <div class="col-md-6">

		   <div class="row">

		   <div class="col-md-2">

					<div class="googleRating_googleLogo__3B3_2"><svg class="googleRating_googleLogoSvg__1pkbS" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="72px" height="72px" viewBox="0 0 72 72" version="1.1"><title>01364BA2-54A7-4CC9-9DC7-F3A87EBF60F3</title><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="D1" transform="translate(-799.000000, -4203.000000)"><g id="Group-28-Copy-2" transform="translate(0.000000, 3992.000000)"><g id="Group-23-Copy-2"><g id="Group-21" transform="translate(799.000000, 211.000000)"><g id="Group-62"><g id="Group-61"><g id="Group-40-Copy-4"><rect id="Rectangle" x="0" y="0" width="72" height="72"></rect><g id="icons8-google-(1)" transform="translate(6.000000, 6.000000)" fill-rule="nonzero"><path d="M59.4165,24.1245 L57,24.1245 L57,24 L30,24 L30,36 L46.9545,36 C44.481,42.9855 37.8345,48 30,48 C20.0595,48 12,39.9405 12,30 C12,20.0595 20.0595,12 30,12 C34.5885,12 38.763,13.731 41.9415,16.5585 L50.427,8.073 C45.069,3.0795 37.902,0 30,0 C13.4325,0 0,13.4325 0,30 C0,46.5675 13.4325,60 30,60 C46.5675,60 60,46.5675 60,30 C60,27.9885 59.793,26.025 59.4165,24.1245 Z" id="Path" fill="#FFC107"></path><path d="M3.459,16.0365 L13.3155,23.265 C15.9825,16.662 22.4415,12 30,12 C34.5885,12 38.763,13.731 41.9415,16.5585 L50.427,8.073 C45.069,3.0795 37.902,0 30,0 C18.477,0 8.484,6.5055 3.459,16.0365 Z" id="Path" fill="#FF3D00"></path><path d="M30,60 C37.749,60 44.79,57.0345 50.1135,52.212 L40.8285,44.355 C37.8165,46.6365 34.0725,48 30,48 C22.197,48 15.5715,43.0245 13.0755,36.081 L3.2925,43.6185 C8.2575,53.334 18.3405,60 30,60 Z" id="Path" fill="#4CAF50"></path><path d="M59.4165,24.1245 L57,24.1245 L57,24 L30,24 L30,36 L46.9545,36 C45.7665,39.3555 43.608,42.249 40.824,44.3565 C40.8255,44.355 40.827,44.355 40.8285,44.3535 L50.1135,52.2105 C49.4565,52.8075 60,45 60,30 C60,27.9885 59.793,26.025 59.4165,24.1245 Z" id="Path" fill="#1976D2"></path></g></g></g></g></g></g></g></g></g></svg>
					
					
					</div>
			</div>

			<div class="col-md-6">
			<span class="review_text m_top">4.5/5 (1000+ Reviews)</span>
			<br>
			<span class="review_text"><i class="fa fa-star yellow" aria-hidden="true"></i><i class="fa fa-star yellow" aria-hidden="true"></i><i class="fa fa-star yellow" aria-hidden="true"></i><i class="fa fa-star yellow" aria-hidden="true"></i><i class="fa fa-star-half-o yellow" aria-hidden="true"></i></span>
			</div>
		   
		  

		</div>


			<div class="row">
			<div class="col-md-12 m_top">
			<h4 class="m_top">24 Cities | 52 Studios</h4>
			<br>
			<p>Bangalore &nbsp;|&nbsp; Chennai &nbsp;|&nbsp; Hyderabad &nbsp;|&nbsp; Mumbai &nbsp;|&nbsp; Thane &nbsp;|&nbsp; Pune</p>
			<p>Delhi-NCR &nbsp;|&nbsp; Kolkata &nbsp;|&nbsp; Coimbatore &nbsp;|&nbsp; Ranchi &nbsp;|&nbsp; Ghaziabad &nbsp;|&nbsp; Surat</p>
			<p>Vizag &nbsp;|&nbsp; Lucknow&nbsp;|&nbsp; Mangalore &nbsp;|&nbsp; Mysore &nbsp;|&nbsp; Ahmedabad &nbsp;|&nbsp; Kochi</p>
			
			</div>
			</div>

		   </div>
           <!---google reviews--->
           
       </div>
				<!--gallery--->

				<!-------->

				<div class="row m_top">
					<div class="col-md-8 col-md-offset-2">

					<div class="panel-group" id="accordion">
        <!-- <div class="faqHeader"></div> -->
		<h3 class="section-title text-center align-bottom" style="margin-bottom:2rem;line-height:30px;">Frequently Asked Questions</h3>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">How many years is the franchise contract drawn for?</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                     The Contract is drawn for an initial term of 5 years.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">Can I submit my own Bootstrap templates or themes?</a>
                </h4>
            </div>
            <div id="collapseTen" class="panel-collapse collapse">
                <div class="panel-body">
                    A lot of the content of the site has been submitted by the community. Whether it is a commercial element/template/theme 
                    or a free one, you are encouraged to contribute. All credits are published along with the resources. 
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">What is the currency used for all transactions?</a>
                </h4>
            </div>
            <div id="collapseEleven" class="panel-collapse collapse">
                <div class="panel-body">
                    All prices for themes, templates and other items, including each seller's or buyer's account balance are in <strong>USD</strong>
                </div>
            </div>
        </div>
					</div>
        
				<!-------->

				
				
            </div>
        </div><!--row-->

		<div class="row m_top">
			<div class="col-md-12">

			<div class="service-wapper">
                        <div class="row services_icon">
                            <div class="col-sm-3 col-half-offset text-center">
							<div class="well">
                                <div class="icon ">
                                    <img width="70%" src="https://paperone.market/assets/data/whypaperone/Genuine.jpg" alt="service">
                                </div>

								<!-- <div> Section name</div> -->
                           
                        </div>
						</div>
						<div class="col-sm-3 col-half-offset text-center">
						<div class="well">
                                <div class="icon">
                                    <img width="70%" src="https://paperone.market/assets/data/whypaperone/secured-shipping.jpg" alt="service">
                                </div>
                           
                        </div>
						</div>
						<div class="col-sm-3 col-half-offset text-center">
						<div class="well">
                                <div class="icon">
                                    <img width="70%" src="https://paperone.market/assets/data/whypaperone/fast-shipping.jpg" alt="service">
                                </div>
                           
                        </div>
						</div>
						<div class="col-sm-3 col-half-offset text-center">
						<div class="well">
                                <div class="icon">
                                    <img width="70%" src="https://paperone.market/assets/data/whypaperone/Easy-returns.jpg" alt="service">
                                </div>
                           
                        </div>
						</div>
						<div class="col-sm-3 col-half-offset text-center">
						<div class="well">
                                <div class="icon">
                                    <img width="70%" src="https://paperone.market/assets/data/whypaperone/cash-on-delivery.jpg" alt="service">
                                </div>
                           
                        </div>
						</div>
                    </div>
					</div> <!---service section--->
				
			</div>
		</div><!---row--->
    
</section>


<!-- Prakruthi Pariksha Details Ends -->








<script>


var module = angular.module('app', ['ui.bootstrap', 'angular-responsive']);

module.factory('Scopes', function ($rootScope) {
var mem = {};
return {
store: function (key, value) {
$rootScope.$emit('scope.stored', key);
mem[key] = value;
},
get: function (key) {
return mem[key];
}
};
});



module.directive('showErrors', function($timeout) {
return {
restrict: 'A',
require: '^form',
link: function (scope, el, attrs, formCtrl) {
var inputEl   = el[0].querySelector("[name]");
var inputNgEl = angular.element(inputEl);
var inputName = inputNgEl.attr('name');
var blurred = false;
inputNgEl.bind('blur', function() {
blurred = true;
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$watch(function() {
return formCtrl[inputName].$invalid
}, function(invalid) {
if (!blurred && invalid) { return }
el.toggleClass('has-error', invalid);
});

scope.$on('show-errors-check-validity', function() {
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$on('show-errors-reset', function() {
$timeout(function() {
el.removeClass('has-error');
}, 0, false);
});
}
}
});


</script>








<script>

var module = angular.module('app', ['ui.bootstrap', 'angular-responsive']);
module.factory('Scopes', function ($rootScope) {
var mem = {};
return {
store: function (key, value) {
$rootScope.$emit('scope.stored', key);
mem[key] = value;
},
get: function (key) {
return mem[key];
}
};
});

 
function beomeourfranchiseController($scope,$rootScope,Scopes,$http) {


Scopes.store('beomeourfranchiseController', $scope);


$scope.save_becomeourfranchise = function() {
$scope.$broadcast('show-errors-check-validity');
if ($scope.becomeourfranchiseForm.$valid) {


$data=$scope.formData_enquiry;
url="<?php echo base_url();?>add_save_beomeourfranchise";
$("#becomeourfranchiseForm_btn").html('<i class="fa fa-spin fa-refresh"></i> Processing');
$http({
method : "POST",
url : url,
data: $data,
dataType:"JSON",
}).success(function mySucces(res) {
$("#becomeourfranchiseForm_btn").html('<i class="fa fa-user"></i> Create');
if(res.status=="yes"){
	secure_id=res.secure_id;
	swal({
		title: '',
		html: "<p>Thank you for showing interest with <?php echo SITE_NAME;?>. Our executive will get in touch with you soon to further the next steps</p>",
		type: 'success',
		showCancelButton: false,
		showConfirmButton: true,
		confirmButtonText: 'Ok',
	}).then(function () {
		location.reload();
	});


}else{
   // alert('Not Sent');
	bootbox.alert({
	size: "small",
	message: 'Oops Error',
	});	
}

},function myError(response) {

});	


}
}

$scope.get_city_state_details_by_pincodeFun=function(){
		pin=$scope.formData_enquiry.pin;
		
		if(pin===undefined){
		}
		else{
		$http({
				method : "POST",
				url : "<?php echo base_url();?>Account/get_city_state_details_by_pincode",
				data:{'pin' : pin},
				dataType:"JSON",
				}).success(function mySucces(result) {
					$scope.formData_enquiry.city=result.city;
					$scope.formData_enquiry.state=result.state;
					$scope.formData_enquiry.country="India";
					
					if(parseInt(result.count)==0){
                        $scope.delivery_address_pin_error=1;
                        $('.delivery_address_pin_error').show();
                        $('#pin_code_check').addClass('has-error');
                    }else{
                        //alert(result.count+"no error");
                        $scope.delivery_address_pin_error=0;
                        $('.delivery_address_pin_error').hide();
                        $('#pin_code_check').removeClass('has-error');
                    }

				}, function myError(response) {
					
				});
		}	
	 }

}



module.directive('allowOnlyNumbers', function () {  
            return {  
                restrict: 'A',  
                link: function (scope, elm, attrs, ctrl) {  
                    elm.on('keydown', function (event) {  
                        if (event.which == 64 || event.which == 16 || event.which == 46 || event.which == 86) {  
                            // to allow numbers  
                            return false;  
                        } else if (event.which >= 48 && event.which <= 57) {  
                            // to allow numbers  
                            return true;  
                        } else if (event.which >= 96 && event.which <= 105) {  
                            // to allow numpad number  
                            return true;  
                        } else if ([8, 9, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {  
                            // to allow backspace, enter, escape, arrows  
                            return true;  
                        } else {  
                            event.preventDefault();  
                            // to stop others  
                            return false;  
                        }  
                    });  
                }  
            }  
        });



        module.directive('emailExistC', function($http) {
    //alert(22323);
	return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attr, ctrl) {
			function customValidator(ngModelValue)
			{
				ctrl.$setValidity('emailexistValidator', true);
				var FormData = {'email' : ngModelValue};

				 $http({
					method : "POST",
					url : "<?php echo base_url();?>Franchiseloginsession/check_email",
					data:FormData,
					dataType: 'json',
                                        async:false
					}).success(function mySucces(res) {
						if(res.valid!=null)
						{
							if(res.valid==true)
							{
								ctrl.$setValidity('emailexistValidator',true );
							}
							if(res.valid==false){
								ctrl.$setValidity('emailexistValidator', false);
							}
						}
						else{
							ctrl.$setValidity('emailexistValidator', true);
						}

					}, function myError(response) {
							/*bootbox.alert({
							  size: "small",
							  message: 'Error',
							});*/
							alert('Error');
					});

				return ngModelValue;
			}
			ctrl.$parsers.push(customValidator);
      }
    };
});


module.directive('mobileExistC', function($http) {
	return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attr, ctrl) {

			function custommValidator(ngModelValue)
			{
				ctrl.$setValidity('mobileexistValidator',true );
				var FormData = {'mobile' : ngModelValue};
				 $http({
					method : "POST",
					url : "<?php echo base_url();?>Franchiseloginsession/check_mobile",
					data:FormData,
					dataType: 'json',
                                        async:false
					}).success(function mySucces(res) {
						if(res.valid!=null)
						{
							if(res.valid==true)
							{
								ctrl.$setValidity('mobileexistValidator',true );
							}
							if(res.valid==false){
								ctrl.$setValidity('mobileexistValidator',false);
							}
						}else{
							ctrl.$setValidity('mobileexistValidator',true );
						}

					}, function myError(response) {
						/*/bootbox.alert({
						  size: "small",
						  message: 'Error',
						});*/
						alert('Error');
					});

				return ngModelValue;
			}
			ctrl.$parsers.push(custommValidator);
      }
    };
});



module.directive('showErrors', function($timeout) {
return {
restrict: 'A',
require: '^form',
link: function (scope, el, attrs, formCtrl) {
var inputEl   = el[0].querySelector("[name]");
var inputNgEl = angular.element(inputEl);
var inputName = inputNgEl.attr('name');
var blurred = false;
inputNgEl.bind('blur', function() {
blurred = true;
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$watch(function() {
return formCtrl[inputName].$invalid
}, function(invalid) {
if (!blurred && invalid) { return }
el.toggleClass('has-error', invalid);
});

scope.$on('show-errors-check-validity', function() {
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$on('show-errors-reset', function() {
$timeout(function() {
el.removeClass('has-error');
}, 0, false);
});
}
}
});

$(document).ready(function () {
// $('#patient_dob').datepicker({
// format: "dd/mm/yyyy"
// });

// $('#patient_dob').bootstrapMaterialDatePicker
// 			({
// 				time: false,
// 				clearButton: true,
// 				weekStart: 0, 
// 				format: 'DD/MM/YYYY'
// 			});

});
function isNumber(evt) {

evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;

if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	return false;
}

return true;
}

</script>

<script>

$(document).ready(function(){
  $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });
    
    $(".zoom").hover(function(){
		
		$(this).addClass('transition');
	}, function(){
        
		$(this).removeClass('transition');
	});
});
    
</script>

