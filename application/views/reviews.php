<style type="text/css">
.reviews_details{
padding:10px;
}
.align_top
{
margin-top:10px;
}
.horizontal_bar{
width:20%;float:left;margin-bottom: 10px;
}
.star_rate_3{
float:left;width:10%;
}
.star_rate{
width: 30%;
float: left;
margin-right: 6px;
}
.star_count{
display: inline-block;
font-size: 32px;
color: #212121;
margin-top: 4px;
}
.star_count1{
font-size: 28px;
padding: 3px 0 0 7px;
display: inline-block;
color: #212121;
}
.jquery-ui-like {
height: 8px !important;
margin-top:3px !important;
}
.rcount{
font-size: 12px;
font-weight: 500;
color: #212121;
}
.g{
padding: 7px 0 0 0;
}
.rating_numbers {
float: left;
margin-right: 2%;
}
.mb-5{
margin-bottom:0.8em;
}
.rate_box{
background-color: #388e3c;
color: #fff;
padding: 0px 5px;
border-radius: 3px;
width: auto;
float: left;
text-align: right;
display: inline-block;
font-size: 12px;
margin-right: 10px;
}
.rate_box1{
line-height: normal;
display: inline-block;
color: #fff;
padding: 0.2em 0.5em;
border-radius: 3px;
font-weight: 500;
font-size: 1em;
vertical-align: middle;
background-color: #388e3c;
}
.rate_text{
font-weight: 500;
color: #878787;
line-height: 2.4;
}
.bottom_note{
font-size: 12px;
color: #878787;
margin-right: 8px;
font-weight: 500;
}
.review_title{
margin-top: 5px;
text-transform: capitalize;
}
.review_desc{
line-height: 20px;
font-size: 14px;
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/skins/jquery-ui-like/progressbar.css" >
<script>
$(document).ready(function(){
$(".star-rating").find("a").each(function(){
$(this).html("");
})
})

</script>
<div class="columns-container">
<div class="container" id="columns">
<div class="page-content mt-0">
<div class="row mb-20">
<div class="col-sm-12">
<h4 class="mb-0" style="font-size:1.4em;">
<span id="product_names_for_detail"><?php echo $product_name; ?>
<?php
$inventory_product_info_obj = $controller->get_inventory_product_info_by_inventory_id($inventory_id);
$color_attribute_is_there = "no";
if (strtolower($inventory_product_info_obj->attribute_1) == "color") {
$color_attribute_is_there = "yes";
echo " - " . explode(":", $inventory_product_info_obj->attribute_1_value)[0];
} else if (strtolower($inventory_product_info_obj->attribute_2) == "color") {
$color_attribute_is_there = "yes";
echo " - " . explode(":", $inventory_product_info_obj->attribute_2_value)[0];
} else if (strtolower($inventory_product_info_obj->attribute_3) == "color") {
$color_attribute_is_there = "yes";
echo " - " . explode(":", $inventory_product_info_obj->attribute_3_value)[0];
} else if (strtolower($inventory_product_info_obj->attribute_4) == "color") {
$color_attribute_is_there = "yes";
echo " - " . explode(":", $inventory_product_info_obj->attribute_4_value)[0];
}
?>
<?php
if ($color_attribute_is_there == "yes") {
if (strtolower($inventory_product_info_obj->attribute_1) != "color") {
if ($inventory_product_info_obj->attribute_1_value != "") {
echo " - " . $inventory_product_info_obj->attribute_1_value;
}
}

if (strtolower($inventory_product_info_obj->attribute_2) != "color") {
if ($inventory_product_info_obj->attribute_2_value != "") {
echo " - " . $inventory_product_info_obj->attribute_2_value ;
}
}

if (strtolower($inventory_product_info_obj->attribute_3) != "color") {
if ($inventory_product_info_obj->attribute_3_value != "") {
echo " - " . $inventory_product_info_obj->attribute_3_value;
}
}

if (strtolower($inventory_product_info_obj->attribute_4) != "color") {
if ($inventory_product_info_obj->attribute_4_value != "") {
echo " - " . $inventory_product_info_obj->attribute_4_value;
}
}
} else {
$noncolor_attribute_first_is_there = "no";
$noncolor_attribute_second_is_there = "no";

if (strtolower($inventory_product_info_obj->attribute_1) != "color") {
$noncolor_attribute_first_is_there = "yes";
echo " - " . $inventory_product_info_obj->attribute_1_value;
}
if (strtolower($inventory_product_info_obj->attribute_2) != "color") {
if ($noncolor_attribute_first_is_there == "yes") {
$noncolor_attribute_second_is_there = "yes";
if ($inventory_product_info_obj->attribute_2_value != "") {
echo " - " . $inventory_product_info_obj->attribute_2_value ;
}
} else {
$noncolor_attribute_first_is_there = "yes";
echo " - " . $inventory_product_info_obj->attribute_2_value;
}
}
if (strtolower($inventory_product_info_obj->attribute_3) != "color") {
if ($noncolor_attribute_first_is_there == "no" || $noncolor_attribute_second_is_there == "no") {
if ($noncolor_attribute_first_is_there == "yes") {
$noncolor_attribute_second_is_there = "yes";
if ($inventory_product_info_obj->attribute_3_value != "") {
echo " - " . $inventory_product_info_obj->attribute_3_value;
}
} else {
$noncolor_attribute_first_is_there = "yes";
echo " - " . $inventory_product_info_obj->attribute_3_value;
}
}
else{
if ($inventory_product_info_obj->attribute_3_value != "") {
echo " - " . $inventory_product_info_obj->attribute_3_value;
}
}
}
if (strtolower($inventory_product_info_obj->attribute_4) != "color") {
if ($noncolor_attribute_first_is_there == "no" || $noncolor_attribute_second_is_there == "no") {
if ($noncolor_attribute_first_is_there == "yes") {
$noncolor_attribute_second_is_there = "yes";
if ($inventory_product_info_obj->attribute_4_value != "") {
echo " - " . $inventory_product_info_obj->attribute_4_value;
}
} else {
$noncolor_attribute_first_is_there = "yes";
echo " - " . $inventory_product_info_obj->attribute_4_value;
}
}
else{
if ($inventory_product_info_obj->attribute_4_value != "") {
echo " - " . $inventory_product_info_obj->attribute_4_value;
}
}
}


}
$color_attribute_is_there = "no";
$noncolor_attribute_first_is_there = "no";
$noncolor_attribute_second_is_there = "no";
?>

Reviews
</span>
</h4>
</div>
</div>
<div class="row mb-20">
<div class="col-sm-12">
<?php
$number_ratings_one_to_five=$controller->number_ratings_one_to_five($inventory_id);
$total_ratings=$total_reviews;
$avg_value=$controller->get_average_value_of_ratings($inventory_id);
if($total_ratings!=0){
foreach($number_ratings_one_to_five as $rating=>$rating_count){
if($rating=="one"){
$one_rate_percent=($rating_count/$total_ratings)*100;
$one_rate_count=$rating_count;
}
if($rating=="two"){
$two_rate_percent=($rating_count/$total_ratings)*100;
$two_rate_count=$rating_count;
}
if($rating=="three"){
$three_rate_percent=($rating_count/$total_ratings)*100;
$three_rate_count=$rating_count;
}
if($rating=="four"){
$four_rate_percent=($rating_count/$total_ratings)*100;
$four_rate_count=$rating_count;
}
if($rating=="five"){
$five_rate_percent=($rating_count/$total_ratings)*100;
$five_rate_count=$rating_count;
}
}
}else{
$one_rate_percent=0;
$two_rate_percent=0;
$three_rate_percent=0;
$four_rate_percent=0;
$five_rate_percent=0;
$one_rate_count=0;
$two_rate_count=0;
$three_rate_count=0;
$four_rate_count=0;
$five_rate_count=0;
}
?>
<script>
$(function () {
progressBar(<?php echo $five_rate_percent; ?>, $('#progressBar_<?php echo $inventory_id; ?>_5'),5);
progressBar(<?php echo $four_rate_percent; ?>, $('#progressBar_<?php echo $inventory_id; ?>_4'),4);
progressBar(<?php echo $three_rate_percent; ?>, $('#progressBar_<?php echo $inventory_id; ?>_3'),3);
progressBar(<?php echo $two_rate_percent; ?>, $('#progressBar_<?php echo $inventory_id; ?>_2'),2);
progressBar(<?php echo $one_rate_percent; ?>, $('#progressBar_<?php echo $inventory_id; ?>_1'),1);
});

function progressBar(percent, $element,dif) {
var progressBarWidth = percent * $element.width() / 100;
var background_color='';
if(dif=='5' || dif=='4' || dif=='3'){
background_color='#388e3c';
}if(dif=='2'){
background_color='#ff9f00';
}if(dif=='1'){
background_color='#ff6161';
}
$element.find('div').css({width: progressBarWidth,"background-color":background_color}).html("&nbsp;");
}

</script>
<?php $rand=$controller->sample_code(); ?>

<div id="ratings_<?php echo $inventory_id; ?>" class="reviews_details row mb-20" style="display:flex;align-items:center;border:1px solid #e5e5e5;">
<div class="col-md-3">
<div class="row">
<div class="col-md-12 text-center">
<span class="star_count"><?php echo $avg_value; ?></span>
<span style="line-height:3.2;font-size:1.2em;" class="glyphicon glyphicon-star"></span>
<p style="margin-block-start:0;margin-block-end:0;">
<?php echo $total_rate_count; ?> Ratings & 
<?php echo $total_review_count; ?> Reviews</p>
</div>
</div>
</div>
<div class="col-md-6">
<div class="row mb-5">
<div class="col-md-12">
<a class="rcount rating_numbers" href="<?php echo base_url(); ?>show_all_reviews/<?php echo $rand.$inventory_id?>/1/5">
<span>5</span>
<span class="glyphicon glyphicon-star"></span>
</a>
<div id="progressBar_<?php echo $inventory_id; ?>_5" class="jquery-ui-like">
<div class="hbar"></div>
</div>
<div class="star_rate_3 rcount"><font style="color:#797979;">&nbsp;&nbsp;</font><?php echo $five_rate_count ?></div>
</div>
</div>
<div class="row mb-5">
<div class="col-md-12">
<a class="rcount rating_numbers" href="<?php echo base_url(); ?>show_all_reviews/<?php echo $rand.$inventory_id?>/1/4">
<span>4</span>
<span class="glyphicon glyphicon-star"></span>
</a>
<div id="progressBar_<?php echo $inventory_id; ?>_4" class="jquery-ui-like">
<div class="hbar"></div>
</div>
<div class="star_rate_3 rcount"><font style="color:#797979;">&nbsp;&nbsp;</font><?php echo $four_rate_count ?></div>
</div>
</div>
<div class="row mb-5">
<div class="col-md-12">
<a class="rcount rating_numbers" href="<?php echo base_url(); ?>show_all_reviews/<?php echo $rand.$inventory_id?>/1/3">
<span>3</span>
<span class="glyphicon glyphicon-star"></span>
</a>
<div id="progressBar_<?php echo $inventory_id; ?>_3" class="jquery-ui-like">
<div class="hbar"></div>
</div>
<div class="star_rate_3 rcount"><font style="color:#797979;">&nbsp;&nbsp;</font><?php echo $three_rate_count ?></div>
</div>
</div>
<div class="row mb-5">
<div class="col-md-12">
<a class="rcount rating_numbers" href="<?php echo base_url(); ?>show_all_reviews/<?php echo $rand.$inventory_id?>/1/2">
<span>2</span>
<span class="glyphicon glyphicon-star"></span>
</a>
<div id="progressBar_<?php echo $inventory_id; ?>_2" class="jquery-ui-like">
<div class="hbar"></div>
</div>
<div class="star_rate_3 rcount"><font style="color:#797979;">&nbsp;&nbsp;</font><?php echo $two_rate_count ?></div>
</div>
</div>
<div class="row mb-5">
<div class="col-md-12">
<a class="rcount rating_numbers" href="<?php echo base_url(); ?>show_all_reviews/<?php echo $rand.$inventory_id?>/1/1">
<span>1</span>
<span class="glyphicon glyphicon-star"></span>
</a>
<div id="progressBar_<?php echo $inventory_id; ?>_1" class="jquery-ui-like">
<div class="hbar"></div>
</div>
<div class="star_rate_3 rcount"><font style="color:#797979;">&nbsp;&nbsp;</font><?php echo $one_rate_count ?></div>
</div>
</div>
</div>	
<div class="col-md-3">
<form id="write_review" action="" method="post" class="col-md-12">
<input type="hidden" value="<?php echo $inventory_id; ?>" name="inv_id" id="inv_id">
<input type="hidden" value="<?php echo $product_name;?>" name="pro_name">
<input type="hidden" value="<?php echo $this->session->userdata('customer_id') ?>" name="customer_id" id="customer_id"> 
<button class="btn btn-default" type="submit" onclick="writeReviewFun();">Rate a Product</button>
</form>
</div>
</div>
<!---addded---------->
<div class="row" class="reviews_details row mb-20" style="border:1px solid #e5e5e5;padding:1em 0;">
<div class="col-md-12">
<?php
if(count($all_reviews)!=0){
$i=0;
foreach($all_reviews as $reviews){
$rating = $reviews->rating;
$posteddate = date("Y-m-d", strtotime($reviews->posteddate)); 
?>
<div class="row">
<div class="col-md-12">
<span class="rate_box1"><?php echo $rating; ?>
<span class="glyphicon glyphicon-star" style="margin-left:0.1em;font-size: 0.8em;line-height: 1.2;"></span>
</span>
<span class="review_title" style="line-height: 1.8;margin-left:0.4em;"><b><?php echo $reviews->review_title ?></b></span>
</div>
</div>
<?php if ($reviews->review !="") { ?>
<div class="row mb-10">
<div class="col-md-12">
<div class="commnet-dettail more margin-top review_desc">
<?php echo $reviews->review; ?>
</div>
</div>
</div>
<?php } ?>
<div class="row bottom_note">
<div class="col-md-12">
<span><strong style="margin-right:0.6em;"><?php echo $reviews->customer_name; ?></strong>
<?php echo $controller->time_elapsed_string($posteddate); ?>
</span>
</div>
</div>
<?php
	if(($i+1)!=count($all_reviews)){
?>
<hr style="margin:1em auto;width:50%;display:block;">
<?php
	}
?>
<?php
if($i>5){
$style='';
}else{
$style='style="display:none;"';
}
$i++;
}
?>
<?php
}
?>
</div>
</div>
<div class="f-row margin-top" id="pagination_div" <?php echo $style; ?>>
<div class="sortPagiBar">
<div class="bottom-pagination">
<span class="nav_pag"></span>
</div>
</div>
</div>


<!---addded---------->



</div>
</div>
</div>
</div>
</div>
<br>

<script type="text/javascript">
    function writeReviewFun() {
        <?php
        if(!$this->session->userdata("customer_id")){
        $this->session->set_userdata("cur_page", current_url());
        ?>
        /*bootbox.confirm({
            message: "Please login to write review",
            size: "small",
            callback: function (result) {
                if (result) {
                    location.href = "<?php echo base_url()?>login";
                }
            }
        });*/

        if(confirm("Please login to write review")){
            location.href = "<?php echo base_url()?>login";
        }

        <?php

        }else{

        ?>

        inv_id = $("#inv_id").val();
        customer_id = $("#customer_id").val();

        var url_u = '<?php echo base_url('check_review_exists');?>';
        $.ajax({
            url: url_u,
            type: 'POST',
            data: "inventory_id=" + inv_id + "&customer_id=" + customer_id,
            success: function (res) {

                if (res == "yes") {
                    /*bootbox.confirm({
                        message: "Already You have submitted the Reviews and Ratings. Are you Sure to Overwrite the Reviews and Ratings?",
                        size: "small",
                        callback: function (result) {
                            if (result) {
                                document.getElementById("write_review").action = "<?php echo site_url('write_review');?>";
                                document.getElementById("write_review").submit();
                            }
                        }
                    });*/

                    if(confirm("Already You have submitted the Reviews and Ratings. Are you Sure to Overwrite the Reviews and Ratings?")){
                        document.getElementById("write_review").action = "<?php echo site_url('write_review');?>";
                        document.getElementById("write_review").submit();
                    }

                } else if (res == "no_access") {
                    /*bootbox.alert({
                    size: "small",
                    message: "Sorry You didn't made any purchase of this product.So you can't write a review",
                    });*/

                    alert("Sorry You didn't made any purchase of this product.So you can't write a review");
                    return false;

                } else {
                    document.getElementById("write_review").action = "<?php echo site_url('write_review');?>";
                    document.getElementById("write_review").submit();
                }
            }
        });
        <?php } ?>
//document.getElementById("write_review").submit();
    }

    jQuery(document).ready(function ($) {
        $('.common').paginathing({
            perPage: 5,
            insertAfter: '.nav_pag'
        });
    });
    $(document).ready(function () {
        showChar(500);
    });

    $(document).ready(function () {
        activity_on_reviews();
    });

    function activity_on_reviews() {

        var r_inside = [];

        $(".review:visible").each(function () {
            var id = this.id;
            var start = new Date();
            start = start.getTime();
            var r_view = {"review_id": id, "time": start};
            r_inside.push(r_view);
        });

        var start = new Date();
        start = start.getTime();
        temp = {"start": start, "inventory_id": '<?php echo $inventory_id; ?>', "reviews": r_inside};

        var r_final = localStorage.getItem("reviews");
        if (r_final != null) {
            var stored = JSON.parse(localStorage.getItem("reviews"));
            stored.push(temp);
            localStorage.setItem("reviews", JSON.stringify(stored));
        } else {
            var obj = [];
            obj.push(temp);
            localStorage.setItem("reviews", JSON.stringify(obj));
        }

        var result = JSON.parse(localStorage.getItem("reviews"));
//alert(JSON.stringify(result));	
    }

    $(document).ready(function () {
        $("ul.pagination li:not(.disabled):not(.active)").attr("onclick", "go()");
        $("ul.pagination li.disabled,.active").attr("onclick", "go2()");
    });

    function go() {
        $("ul.pagination li:not(.disabled):not(.active)").attr("onclick", "go()");
        $("ul.pagination li.disabled,.active").attr("onclick", "go2()");
        activity_on_reviews();
    }

    function go2() {
        $("ul.pagination li:not(.disabled):not(.active)").attr("onclick", "go()");
        $("ul.pagination li.disabled,.active").attr("onclick", "go2()");
    }

</script>
