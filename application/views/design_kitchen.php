<?php 
		$bg="";
		$ft="";
		if(!empty($ui_arr["bg_img"])){
			$bg=$ui_arr["bg_img"]->background_image;
		}
		if(!empty($ui_arr["ft_img"])){
			$ft=$ui_arr["ft_img"]->background_image;
		}
	?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/skins/jquery-ui-like/progressbar.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/product_comparision/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/product_comparision/w3.css" />



<style type="text/css">
.step h3{
    text-transform: capitalize;
    margin-top: 60px !important;
}
.let_us_cl{
    color:#ff5a00;
}
.option-shadow {
box-shadow: 0 2px 4px 0 rgba(0,0,0,.1);
}
.interior_image{
    border-radius:5px;
}
.card-list>li {
    list-style-image: url('../assets/images/tick.png');
}
.card-list {
    padding-left: 20px;
}
.body_small, .subtitle_small {
    font-size: 14px;
    line-height: 20px;
    margin-top: 10px;
}
.size_of_home_des_label
{
    border: 2px solid #efebeb;
    background: linear-gradient(180deg,rgba(253,238,239,.4),rgba(253,238,239,.4));
    height: 100%;
    box-shadow: 0 2px 4px 0 rgba(0,0,0,.1);
    position: relative;
    padding: 20px;
    border-radius: 10px;
    margin-right: 20px;
    margin-bottom: 20px !important;
    line-height:25px;
    text-align: left;
    text-transform: capitalize;
}

.size_of_home_des_label p
{
    margin-left: 40px;
    /* min-height: 50px; */
    vertical-align: middle;
    text-align: left;
}

.size_of_home_des_label_selected{
    border: 2px solid #002770;
}

.size_of_home_des_label:hover,.size_of_home_des_label:active,.size_of_home_des_label:focus
{
   /* border: 2px solid #002770;*/
}

.para{
    text-align: left;
    margin-top: 10px;
    font-size: 14px;
}
#form { 
    text-align: center; 
    position: relative; 
    margin-top: 20px
} 
  
#form fieldset { 
    background: white; 
    border: 0 none; 
    border-radius: 0.5rem; 
    box-sizing: border-box; 
    width: 70%; 
    /* margin: 0;  */
    padding-bottom: 20px; 
    position: relative;
    margin-left:auto;
    margin-right:auto;
} 
  
.finish { 
    text-align: center;
    text-align: center;
    margin-bottom: 20px;
    margin-top: 20px;
} 
  
fieldset{
    min-height: 400px;
}
#form fieldset:not(:first-of-type) { 
    display: none
} 
  
#form .previous-step, .next-step { 
    width: 100px; 
    font-weight: bold; 
    color: white; 
    border: 0 none; 
    border-radius:10px;
    cursor: pointer; 
    padding: 10px 5px; 
    margin: 10px 10px 10px 10px; 
    float: right
} 
  
.form, .previous-step { 
    background: #616161; 
    border-radius:10px;
} 
  
.form, .next-step { 
    background: #002770; 
    border-radius:10px;
} 
  
#form .previous-step:hover, 
#form .previous-step:focus { 
    background-color: #000000
} 
  
#form .next-step:hover, 
#form .next-step:focus { 
    /*background-color: #2F8D46*/
    background-color: #002770
} 
/* new button */  
.top_button{
    background: none !important;
    color: #a19a9a !important;
    border: 1px solid #ccc !important;
    padding: 5px !important;
    width: 80px !important;
}
.text { 
    /*color: #2F8D46; */
    color: #002770; 
    font-weight: normal
} 
  
#progressbar { 
    margin-bottom: 30px; 
    overflow: hidden; 
    color: lightgrey 
} 
  
#progressbar .active { 
    /*color: #2F8D46*/
    color: #002770
} 
  
#progressbar li { 
    list-style-type: none; 
    font-size: 15px; 
    width: 16%; 
    float: left; 
    position: relative; 
    font-weight: 400
} 
  
#progressbar #step1:before { 
    content: "1"
} 
  
#progressbar #step2:before { 
    content: "2"
} 
  
#progressbar #step3:before { 
    content: "3"
} 
  
#progressbar #step4:before { 
    content: "4"
} 

#progressbar #step5:before { 
    content: "5"
} 

#progressbar #step6:before { 
    content: "6"
} 
  
#progressbar li:before { 
    width: 50px; 
    height: 50px; 
    line-height: 45px; 
    display: block; 
    font-size: 20px; 
    color: #ffffff; 
    background: lightgray; 
    border-radius: 50%; 
    margin: 0 auto 10px auto; 
    padding: 2px
} 
  
#progressbar li:after { 
    content: ''; 
    width: 100%; 
    height: 2px; 
    background: lightgray; 
    position: absolute; 
    left: 0; 
    top: 25px; 
    z-index: -1
} 
  
#progressbar li.active:before, 
#progressbar li.active:after { 
    /*background: #2F8D46*/
    background: #002770
} 
  
.progress { 
    height: 20px
} 
  
.progress-bar { 
    background-color: #2F8D46;
    display:none;
}
/* css for check box radio */
.radio_inside{
    padding: 10px;
    vertical-align: middle;
}
.checkbox label:after, 
.radio label:after {
    content: '';
    display: table;
    clear: both;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: 1.5em;
    margin-top:3px;
}
.radio label{
    text-align: left;
    margin-bottom: 10px;
    background-color: #edf0f5;
    border-color: #e1e1e1;
}
.radio .cr {
    border-radius: 50%;
    color:green;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .8em;
    line-height: 0;
    top: 50%;
    left: 17%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
  /* css for check box radio */

@media only screen and (max-width: 600px) {
  body {
    background-color: lightblue;
  }
  
}

body{
background-color: #f1f3f6;
}

.section-title{
	line-height: 30px !important;
}
.our_partner1 {
    margin: 0 auto;
    max-width: 100%;
    padding: 30px 0;
}

.our_partner {
    -moz-box-pack: center;
    display: -moz-box;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
}
.our_partner .our_partner_inner {
    -moz-box-flex: 0;
    grid-column-gap: 20px;
    -moz-box-align: center;
    align-items: center;
    background-color: #fff;
    border: 1px solid #ccc;
    border-radius: 8px;
    display: -moz-box;
    display: flex;
    flex: 0 1 48%;
    grid-template-columns: -webkit-max-content auto;
    grid-template-columns: max-content auto;
    margin-bottom: 2%;
    margin-right: 2%;
    outline: none;
    padding: 24px;
}
.our_partner_img{
	/* background-color: #eee; */
    /* border-radius: 50%;
	vertical-align: top;
	width: 50%; */
	text-align:center;
   
}
.our_partner_img img{
	background-color: #eee;
    border-radius: 50%;
	vertical-align: top;
}
.designation{
	color: #9b9b9b;
    display: block;
    font-size: 13px;
    letter-spacing: 0;
    line-height: 16px;
}
.name{
	display: block;
    font-size: 14px;
    font-weight: 600;
    letter-spacing: 0;
    line-height: 17px;
    margin-top: 12px;
}

.bt_20{
	margin-bottom: 20px;
}
.img_side{
	border-radius: 8px;
    height: auto;
    width: 100%;
}
.p_top{
	margin-top:20px;line-height:25px;
}
.m_top{
	margin-top:20px;
}
.well {
    -webkit-box-shadow: 0 8px 20px rgba(0, 0, 0, 0.1);
    -moz-box-shadow: 0 8px 20px rgba(0, 0, 0, 0.1);
    box-shadow: 0 8px 20px rgba(0, 0, 0, 0.1);
    border-top: 2px solid #ccc;
    background-color: #f7f7f7;
    transition: all 0.3s ease-in-out 0s;
}


#demo {
  height:100%;
  position:relative;
  overflow:hidden;
}


.green{
  background-color:#6fb936;
}
        .thumb{
            margin-bottom: 30px;
        }
        
        .page-top{
            margin-top:85px;
        }

   .gal_img{
	padding-bottom: 32%;
    border-radius: 8px;
    display: inline-block;
    position: relative;
    width: 31%;
    text-align: center;
    margin: 1%;
   }
img.zoom {
    width: 100%;
    height: 200px;
    border-radius:5px;
    object-fit:cover;
    -webkit-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
    -ms-transition: all .3s ease-in-out;
}
        
 
.transition {
    -webkit-transform: scale(1.2); 
    -moz-transform: scale(1.2);
    -o-transform: scale(1.2);
    transform: scale(1.2);
}
    .modal-header {
   
     border-bottom: none;
}
    .modal-title {
        color:#000;
    }
    .modal-footer{
      display:none;  
    }
/*faq*/

.faqHeader {
        font-size: 27px;
        margin: 20px;
    }

    .panel-heading [data-toggle="collapse"]:after {
        font-family: 'fontawesome';
        content: "\f054"; /* "play" icon */
        float: right;
        /*color: #F58723;*/
        color: #002770;
        font-size: 18px;
        line-height: 22px;
        /* rotate "play" icon from > (right arrow) to down arrow */
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        -ms-transform: rotate(-90deg);
        -o-transform: rotate(-90deg);
        transform: rotate(-90deg);
    }

    .panel-heading [data-toggle="collapse"].collapsed:after {
        /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        transform: rotate(90deg);
        color: #454444;
    }

	.services_icon{
		margin: 0 auto;
		max-width: 100%;
		padding: 30px 0;
		display: flex;
		flex-wrap: wrap;
		justify-content: center;
	}
	.img_text{
		position: absolute;
    bottom: 0;
   /* right: 0;*/
    color: #fff;
    font-size: 13px;
    font-weight:  500;
    text-align: center;
    width: 85%;
    background: linear-gradient(to top, #120e0e42 100%, #333230 76%);
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
	}

    @media (max-width: 420px){
.our_partner .our_partner_inner
  {
	display: block;
    flex: 0 1 100%;
    text-align: center;
  }
}
</style>

<style>
.services2 .services2-item,.services2{
border:0px;
}
.services2 .services2-item .image{
padding-left:120px;
}
.form-group .help-block {
display: none;
}
.form-group.has-error .help-block {
display: block;
}
.block-trending{
margin-top:0px !important;
}

.section-count {
    grid-column-gap: 80px;
    -moz-box-pack: center;

    grid-template-columns: -webkit-max-content -webkit-max-content -webkit-max-content -webkit-max-content;
    grid-template-columns: max-content max-content max-content max-content;
    justify-content: center;
    padding: 40px;
    text-align: center;

    font-size: 24px;
    font-weight: 700;
    letter-spacing: 0;
    line-height: 40px;
    text-transform: uppercase;

}
.colr_update{
	color:#ff3300;
}
.font_big{
    font-size: 48px;
}
.review_text{
	font-size:20px;
}
.yellow{
	color:gold;
}
.link_color{
	color:#ff3300;
}
</style>

	
	<style type="text/css">

	.bodyimage {
	<?php if($bg!=''){
		?>
	background: url('<?php echo base_url().$bg;?>') no-repeat;	
	
		<?php 
	}else{
		?>
		background:#b3c95c;
		<?php
		
	}?>
	background-size: 100% auto;
	  z-index:-100;
	}
	</style>
	
	
	<style type="text/css">
.form-group .help-block {
  display: none;
}
 
.form-group.has-error .help-block {
  display: block;
}
.step{
    padding: 0px 50px;
}
</style>


<style>
.swal2-modal .swal2-styled{
	background-color:#1429a0 !important;
}

.kitchen_layout label{
    cursor: pointer;
}
.kitchen_layout{
    margin-top: 30px;
}

.kitchen_layout_title{
    margin-bottom: 20px;
}

.kitchen_layout .input-hidden {
  position: absolute;
  left: -9999px;
}

.kitchen_layout input[type=radio]:checked + label>img {
  border: 1px solid #fff;
  box-shadow: 0 0 3px 3px #090;
  border-radius: 10px;
}

/* Stuff after this is only to make things more pretty */
.kitchen_layout input[type=radio] + label>img {
    margin-bottom: 10px;
}

.kitchen_layout input[type=radio]:checked + label>img {
  transform: 
   
}
</style>


	

<!--- new design --->

<div class="row justify-content-center section8 " ng-controller="designController"> 
            <div class="col-11 col-sm-12 col-md-10 col-md-offset-1 
                col-lg-10 col-xl-5 text-center p-0 mt-50 mb-2"> 

                <h3 class="section-title text-center align-bottom">Request to Design <?php echo $design_type; ?></h3>

                <div class="px-0 pt-4 pb-0 mt-50 mb-3"> 
                    <form id="form" name="designform" novalidate > 
                        <ul id="progressbar"> 
                            <li class="active" id="step1"> 
                                <strong>LAYOUT TYPE</strong> 
                            </li> 
                            <li id="step2"><strong>AREA</strong></li> 
                            <li id="step3"><strong>KITCHEN TOP</strong></li> 
                            <li id="step4"><strong>BUDGET</strong></li> 
                            <li id="step5"><strong>GET QUOTE</strong></li> 
                            <li id="step6"><strong>SUCCESS</strong></li> 
                        </ul> 
                        <!-- <div class="progress"> 
                            <div class="progress-bar"></div> 
                        </div> <br>  -->
                        <fieldset> 

                        <button type="button" name="next-step" 
                                class="next-step next-step1 btn-sm top_button" > Next <i class="fa fa-arrow-right" aria-hidden="true"></i></button> 
                            
                        
<!---new --->
    <div class="step step1">

     <h3 class="mt-10 mb-10">Select Your Kitchen's Layout</h3>

        <div class="form-group" show-errors>


            <!---layout--->

            <div class="kitchen_layout">


                <div class="col-md-4">
                <input type="radio" name="kitchen_layout" ng-model="formData.kitchen_layout" id="parallel_type" value="Parallel Type" class="input-hidden" />
                <label for="parallel_type">
                <img src="<?php echo base_url() ?>assets/images/kitchen/paralleltype.png" alt="Parallel" />
                </label>
                <h4 class="text-center kitchen_layout_title">Parallel Type</h4>
                </div>

                <div class="col-md-4">
                <input type="radio" name="kitchen_layout" ng-model="formData.kitchen_layout" id="inland_type" value="Inland Type" class="input-hidden" />
                <label for="inland_type">
                <img src="<?php echo base_url() ?>assets/images/kitchen/inlandtype.png" alt="Inland" />
                </label>
                <h4 class="text-center kitchen_layout_title">Inland Type</h4>
                </div>

                <div class="col-md-4">
                <input type="radio" name="kitchen_layout" ng-model="formData.kitchen_layout" value="Single Side Type" id="single_side_type" class="input-hidden" />
                <label for="single_side_type">
                <img src="<?php echo base_url() ?>assets/images/kitchen/straighttype.png" alt="Single Side Type" />
                </label>
                <h4 class="text-center kitchen_layout_title">Single Side Type</h4>
                </div>

                <div class="col-md-4">
                <input type="radio" name="kitchen_layout" ng-model="formData.kitchen_layout" value="Open Type" id="open_type" class="input-hidden" />
                <label for="open_type">
                <img src="<?php echo base_url() ?>assets/images/kitchen/opentype.png" alt="Open Type" />
                </label>
                <h4 class="text-center kitchen_layout_title">Open Type</h4>
                </div>

                <div class="col-md-4">
                <input type="radio" name="kitchen_layout" ng-model="formData.kitchen_layout" value="U Shape Type" id="u_type" class="input-hidden" />
                <label for="u_type">
                <img src="<?php echo base_url() ?>assets/images/kitchen/utype.png" alt="U Shape Type" />
                </label>
                <h4 class="text-center kitchen_layout_title">U Shape Type</h4>
                </div>

                <div class="col-md-4">
                <input type="radio" name="kitchen_layout"  ng-model="formData.kitchen_layout" value="L Shape Type" id="l_type" class="input-hidden" />
                <label for="l_type">
                <img src="<?php echo base_url() ?>assets/images/kitchen/ltype.png" alt="L Shape Type" />
                </label>
                <h4 class="text-center kitchen_layout_title">L Shape Type</h4>
                </div>

            </div>
            <!---layout--->


        <input type="hidden" name="design_type" ng-model="formData.design_type" value="<?php echo $design_type; ?>">

        <!---new --->

        </div><!--form group-->

    </div><!---step1-->

                            <input type="button" name="next-step" 
                                class="next-step next-step1" value="Next"/> 

                        </fieldset> 

                        <fieldset> 
                        
                        <button type="button" name="next-step" 
                                class="next-step next-step2 btn-sm top_button" > Next <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                            <button type="button" name="previous-step" 
                                class="previous-step pull-left btn-sm top_button" 
                                 ><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
                                
    <div class="step step2">

                      
    <h3 class="mt-10 mb-10">What is an approximate area of your kitchen (select the most apt option?)</h3>
   
    

	<div class="form-group" show-errors>


    <div class="radio radio_inside">

            <label class="size_of_home_des_label">
            <input type="radio" checked name="area_of_kitchen"  value="Less than 100 sq. ft" ng-model="formData.area_of_kitchen" >
            <span class="cr"><i class="cr-icon fa fa-circle"></i></span><p>Less than <br> 100 sq. ft</p>
            </label>

            <label class="size_of_home_des_label">
            <input type="radio" checked name="area_of_kitchen"  value="More than 100 sq. ft" ng-model="formData.area_of_kitchen" >
            <span class="cr"><i class="cr-icon fa fa-circle"></i></span><p> More than<br>100 sq. ft</p>
            </label>
            
            <label class="size_of_home_des_label">
            <input type="radio" checked name="area_of_kitchen"  value="More than 150 sq. ft" ng-model="formData.area_of_kitchen" >
            <span class="cr"><i class="cr-icon fa fa-circle"></i></span> <p>More than<br>150 sq. ft</p>
            </label>

            <label class="size_of_home_des_label">
            <input type="radio" checked name="area_of_kitchen"  value="More than 200 sq. ft" ng-model="formData.area_of_kitchen" >
            <span class="cr"><i class="cr-icon fa fa-circle"></i></span> <p>More than<br>200 sq. ft</p>
            </label>
    </div>
    
    </div>

</div><!--step2-->

                            <input type="button" name="next-step" 
                                class="next-step next-step2" value="Next"/> 
                            <input type="button" name="previous-step" 
                                class="previous-step pull-left" 
                                value="Back" /> 

                        </fieldset> 

                        <fieldset> 
                        
                        <button type="button" name="next-step" 
                                class="next-step next-step3 btn-sm top_button" > Next <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                            <button type="button" name="previous-step" 
                                class="previous-step pull-left btn-sm top_button" 
                                 ><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
                                
    <div class="step step3" >

                      
    <h3 class="mt-10 mb-10">What kind of kitchen top you prefer?</h3>

	<div class="form-group" show-errors>


    <div class="radio radio_inside">

            <label class="size_of_home_des_label">
            <input type="radio" name="kitchen_top"  value="Marble" ng-model="formData.kitchen_top" >
            <span class="cr"><i class="cr-icon fa fa-circle"></i></span><p>Marble</p>
            </label>

            <label class="size_of_home_des_label">
            <input type="radio" name="kitchen_top"  value="Laminate" ng-model="formData.kitchen_top" >
            <span class="cr"><i class="cr-icon fa fa-circle"></i></span><p>Laminate</p>
            </label>
            
            <label class="size_of_home_des_label">
            <input type="radio" name="kitchen_top"  value="Heat Resistant Wood" ng-model="formData.kitchen_top" >
            <span class="cr"><i class="cr-icon fa fa-circle"></i></span><p>Heat Resistant Wood</p>
            </label>

            <label class="size_of_home_des_label" style="margin-left:auto;margin-right:auto;width: 80%;margin-left: -15px;">
            <input type="radio" name="kitchen_top"  value="I am not Sure" ng-model="formData.kitchen_top" >
            <span class="cr"><i class="cr-icon fa fa-circle"></i></span><p>I am not Sure</p>
            </label>
    </div>


</div><!--step3-->

    </div>
    

                            <input type="button" name="next-step" 
                                class="next-step next-step3" value="Next"/> 
                            <input type="button" name="previous-step" 
                                class="previous-step pull-left" 
                                value="Back" /> 

                        </fieldset> 
                        <fieldset> 
                        
                        <button type="button" name="next-step" 
                                class="next-step next-step4 btn-sm top_button" ng-show="formData.interior_budget"> Next <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                            <button type="button" name="previous-step" 
                                class="previous-step pull-left btn-sm top_button" 
                                 ><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
                                
                        <div class="step step4" >

                      
    <h3 class="mt-10 mb-10">What Kind of Interiors have your budgeted for ?</h2>
   
	<div class="form-group" show-errors>

        <div class="radio radio_inside row">
                <label class="size_of_home_des_label col-md-4 col-md-offset-4">
                    <input type="radio" name="interior_budget" onchange="click_nxt(4)" value="Lowest Cost" ng-model="formData.interior_budget" >
                    <span class="cr"><i class="cr-icon fa fa-circle"></i></span>Lowest Cost
                </label>
            
                <label class="size_of_home_des_label col-md-4 col-md-offset-4">  
                <input type="radio" name="interior_budget" onchange="click_nxt(4)" value="Economical" ng-model="formData.interior_budget" >
                <span class="cr"><i class="cr-icon fa fa-circle"></i></span>Economical
                </label>
        
                <label class="size_of_home_des_label col-md-4 col-md-offset-4">  
                <input type="radio" name="interior_budget" onchange="click_nxt(4)" value="Luxury" ng-model="formData.interior_budget" >
                <span class="cr"><i class="cr-icon fa fa-circle"></i></span>Luxury
                </label>
            
                <label class="size_of_home_des_label col-md-4 col-md-offset-4">  
                <input type="radio" name="interior_budget" onchange="click_nxt(4)" value="Super Luxury" ng-model="formData.interior_budget" >
                <span class="cr"><i class="cr-icon fa fa-circle"></i></span>Super Luxury
                </label>
        </div>

    </div>

</div><!--step2-->

                            <input type="button" name="next-step" 
                                class="next-step next-step4" value="Next" ng-show="formData.interior_budget"/> 
                            <input type="button" name="previous-step" 
                                class="previous-step pull-left" 
                                value="Back" /> 

                        </fieldset> 

                        <fieldset> 

                        <button type="button" name="previous-step" 
                                class="previous-step pull-left btn-sm top_button" 
                                 ><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>

                        <div class="step step5">

<div class="para" style="">
    <h3 class="mb-10 mt-10 text-center">Your estimate is almost ready</h3>
    
    <div class="mt-20" ng-if="formData.interior_budget=='Lowest Cost'">
    <p class="text-left">Nothing to worry, we can bring you budget friendly options without having to compromise on quality.At Voomet Studio, we deal with vast variety of material and skilled workmanship that is sure to give you the best deal you can find in the market.</p>
<br>
    <p class="let_us_cl"><i>Let us know your contacts so that we can have our specialist budget friendly  design team connect with you soonest!</i></p>
</div>
    <div ng-if="formData.interior_budget=='Luxury'">
   <p class="text-left"> At Voomet Studio, luxury means a blend of design and material that is seen rarely.  We understand that your <?php echo $design_type; ?> should be the choicest one and stand apart in your inner circle!</p>
<br>
    <p class="let_us_cl"><i>Let us know your contacts so that we can have our luxury design team connect with you soonest!</i></p>

</div>
    <div ng-if="formData.interior_budget=='Economical'">
    <p class="text-left">We got your choice! Yes at Voomet Studio, we have a lot of options to suite the economical range yet not compromising on the feel and quality of the interiors.You will be thrilled to look at the variety of choices we will provide you that will make you absolutely happy to have chosen us.</p>

<br>
<p class="let_us_cl"><i>Let us know your contacts so that we can have our '<b>best value</b>' design team connect with you soonest!</i></p>

</div>
    <div ng-if="formData.interior_budget=='Super Luxury'">
    <p class="text-left">Wow! Super luxury is rare and we are thrilled that we got you today. At Voomet Studio, our exclusive and skilled design team comes from elite design experience that would for sure offer an super exclusive design, materials and finish to your most exclusive <?php echo $design_type; ?>. </p>
<br>
<p class="let_us_cl"><i>Let us know your contacts so that we can have our "<b>most excusive</b>" design team connect with you soonest!</i></p>

</div>

</div>

<br>

								<div class="form-group" show-errors>
									<input ng-model="formData.name" name="name" type="text" class="form-control" placeholder="Enter Name" required >
									<p class="help-block" ng-if="designform.name.$error.required">Name is required</p>
								</div>
								
										
								<div class="form-group" show-errors>
										<input type="email" name="email" class="form-control" ng-model="formData.email" required placeholder="Enter Your Email" >
				   	
							<p class="help-block" ng-if="designform.email.$error.required">The user's email is required</p>
							<p class="help-block" ng-if="designform.email.$error.email">The email address is invalid</p>	
					
					
								</div>
								
								<div class="form-group" show-errors>
										
                   <input id="mobile_number" type="number"  ng-minlength="10" 	
                   ng-maxlength="10" min="0" onpaste="return false;" name="mobile" class="form-control" ng-model="formData.mobile" required allow-only-numbers placeholder="Mobile No" maxlength="10" onkeypress="return isNumber(event)">	
								
								<p class="help-block" ng-show="designform.mobile.$error.required || designform.mobile.$error.number" >Valid mobile number is required
								</p>

                                
							
								<p class="help-block" ng-show="((designform.mobile.$error.minlength ||
								designform.mobile.$error.maxlength) && 
								designform.mobile.$dirty) ">
											Mobile number should be 10 digits
								</p>
								
								</div>
								
							
                            <!---- pincode  ---->

                            <div class="form-group" show-errors id="pin_code_check">
								
								<input type="text" ng-minlength="6"
								   ng-maxlength="6" name="pin" class="form-control" ng-model="formData.pin" required allow-only-numbers  placeholder='PIN/ZIP Code'  onkeypress="return isNumber(event)"  ng-paste="$event.preventDefault()" ng-pattern="/^[0-9]*$/" oninput="if(value.length>6)value=value.slice(0,6)" ng-keyup="get_city_state_details_by_pincodeFun()">
								   
								   <p class="help-block" ng-show="designform.pin.$error.required || designform.pin.$error.number">Valid pin code is required
									</p>
								
									<p class="help-block" ng-show="((designform.pin.$error.minlength ||
                                    designform.pin.$error.maxlength) && 
                                    designform.pin.$dirty) ">
												   Pincode should be 6 digits
									</p>
                                    <span class="delivery_address_pin_error" style="display:none;color:#a94442">
                                        Please enter valid pincode
                                    </span>
											
							</div>
							
							
							<div class="form-group"  show-errors>
								
								<input  name="city" ng-model="formData.city" required placeholder="City" type="text" class="form-control" readonly>
								<p class="help-block" ng-if="designform.city.$error.required">The City is required</p>
							</div>
						
							<div class="form-group"  show-errors>
							<input  name="state" ng-model="formData.state" required placeholder="State/Province" type="text" class="form-control" readonly>
								
								
								<p class="help-block" ng-if="designform.state.$error.required">The State is required</p>
							</div>
														
							
							<div class="form-group"  show-errors>
								<input  name="country" ng-model="formData.country" required placeholder="Country" type="text" class="form-control" readonly>
								<p class="help-block" ng-if="designform.country.$error.required">The Country is required</p>
							</div>
						
<!--- address of parter--->



                                <!---- pincode  ---->
								

								<div class="form-group">
									<button class="btn btn-block"  ng-click="save();$event.stopPropagation();" ng-disabled="designform.$invalid"  style="background-color:#1429a0;color:#fff;font-size:1 em;opacity:1" id="designform_btn"> Submit </button>
									
									<p><br><small>By submitting this form, you agree to the  <a href="<?php echo base_url(); ?>policy" class="link_color">privacy policy</a> and <a href="<?php echo base_url(); ?>termsandconditions" class="link_color">terms of use</a></small></p>
								</div>

    <!-- <div>
	<button class="btn btn-info pull-left" ng-click="prev('step2',2)">< Prev</button>
	
	</div> -->

</div><!--step3-->
                            <input type="button" name="next-step" 
                                class="next-step next-step5" value="Final Step" style="display:none;"/> 

                            <input type="button" name="previous-step" 
                                class="previous-step pull-left" 
                                value="Back" /> 
                        </fieldset> 
                        <fieldset> 
                            <div class="finish"> 
                                <h4 class="text text-center"> 
                                <div class="row align-top mt-" style="margin-bottom: 20px;">
                                    <div class="col-md-12">
                                        
                                        <br><br>
                                        <h3 class="text-center" style="color:#2F8D46;font-size:50px;"><i class="fa fa-check-circle" aria-hidden="true"></i></h3>

                                        <br>
                                        <p style="line-height:30px;color:#333;">Thank you for showing interest with <?php echo SITE_NAME; ?>. <br>
                                        Our executive will get in touch with you soon to further the nexts</p>

                                        <br>

                                        <a href="<?php echo base_url(); ?>"  class="btn btn-lg btn-default" >Go Home</a>

                                    </div>
                                </div>
                                </h4> 
                            </div> 
                            <!-- <input type="button" name="previous-step" 
                                class="previous-step pull-left" 
                                value="back" />  -->
                        </fieldset> 
                    </form> 
                </div> 
            </div> 
        </div> 
<!--- new design --->







<script>



var module = angular.module('app', ['ui.bootstrap', 'angular-responsive']);
module.factory('Scopes', function ($rootScope) {
var mem = {};
return {
store: function (key, value) {
$rootScope.$emit('scope.stored', key);
mem[key] = value;
},
get: function (key) {
return mem[key];
}
};
});

 
function designController($scope,$rootScope,Scopes,$http) {


Scopes.store('designController', $scope);

$scope.number=8;
$scope.show_sub=0;
$scope.design=[1,2,3,4,5,6,7,8];

$scope.show_sub_fun = function(num) {
    $scope.show_sub=num;  
    //$scope.formData.size_of_home=num;  
}
$scope.setVal = function(txt) {
    $scope.formData.size_of_home_des=txt;  
}
$scope.getNumber = function(num) {
    return new Array(num);   
}
$scope.budget = '';
$scope.getVal = function(budg) {
    //alert(budg);
    $scope.budget = angular.copy(budg);
};
$scope.next = function(cl_name,numc){
    //alert('.'+cl_name+numc);
    $('.step'+numc).show();
    $('.step'+(numc-1)).hide();
}
$scope.prev = function(cl_name,numc){
    $('.step'+numc).show();
    $('.step'+(numc+1)).hide();
}

$scope.save = function() {
$scope.$broadcast('show-errors-check-validity');
if ($scope.designform.$valid) {

$scope.formData.design_type='<?php echo $design_type; ?>';
$data=$scope.formData;

url="<?php echo base_url();?>add_design_kitchen";
$("#designform_btn").html('<i class="fa fa-spin fa-refresh"></i> Processing');
$http({
method : "POST",
url : url,
data: $data,
dataType:"JSON",
}).success(function mySucces(res) {
$("#designform_btn").html('<i class="fa fa-user"></i> Create');
if(res.status=="yes"){
	
    click_nxt(5);
    
    /*
	swal({
		title: '',
		html: "<p>Thank you for showing interest with <?php echo SITE_NAME;?>. We will contact you soon to further the nexts</p>",
		type: 'success',
		showCancelButton: false,
		showConfirmButton: true,
		confirmButtonText: 'Ok',
	}).then(function () {
		location.reload();
	});*/


}else{
   // alert('Not Sent');
	bootbox.alert({
	size: "small",
	message: 'Oops Error',
	});	
}

},function myError(response) {

});	


}
}

$scope.get_city_state_details_by_pincodeFun=function(){
		pin=$scope.formData.pin;
		
		if(pin===undefined){
		}
		else{
		$http({
				method : "POST",
				url : "<?php echo base_url();?>Account/get_city_state_details_by_pincode",
				data:{'pin' : pin},
				dataType:"JSON",
				}).success(function mySucces(result) {
					$scope.formData.city=result.city;
					$scope.formData.state=result.state;
					$scope.formData.country="India";
					
					if(parseInt(result.count)==0){
                        $scope.delivery_address_pin_error=1;
                        $('.delivery_address_pin_error').show();
                        $('#pin_code_check').addClass('has-error');
                    }else{
                        //alert(result.count+"no error");
                        $scope.delivery_address_pin_error=0;
                        $('.delivery_address_pin_error').hide();
                        $('#pin_code_check').removeClass('has-error');
                    }

				}, function myError(response) {
					
				});
		}	
	 }

}



module.directive('allowOnlyNumbers', function () {  
            return {  
                restrict: 'A',  
                link: function (scope, elm, attrs, ctrl) {  
                    elm.on('keydown', function (event) {  
                        if (event.which == 64 || event.which == 16 || event.which == 46 || event.which == 86) {  
                            // to allow numbers  
                            return false;  
                        } else if (event.which >= 48 && event.which <= 57) {  
                            // to allow numbers  
                            return true;  
                        } else if (event.which >= 96 && event.which <= 105) {  
                            // to allow numpad number  
                            return true;  
                        } else if ([8, 9, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {  
                            // to allow backspace, enter, escape, arrows  
                            return true;  
                        } else {  
                            event.preventDefault();  
                            // to stop others  
                            return false;  
                        }  
                    });  
                }  
            }  
        });

        module.directive('showErrors', function($timeout) {
return {
restrict: 'A',
require: '^form',
link: function (scope, el, attrs, formCtrl) {
var inputEl   = el[0].querySelector("[name]");
var inputNgEl = angular.element(inputEl);
var inputName = inputNgEl.attr('name');
var blurred = false;
inputNgEl.bind('blur', function() {
blurred = true;
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$watch(function() {
return formCtrl[inputName].$invalid
}, function(invalid) {
if (!blurred && invalid) { return }
el.toggleClass('has-error', invalid);
});

scope.$on('show-errors-check-validity', function() {
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$on('show-errors-reset', function() {
$timeout(function() {
el.removeClass('has-error');
}, 0, false);
});
}
}
});




$(document).ready(function () {
// $('#patient_dob').datepicker({
// format: "dd/mm/yyyy"
// });

// $('#patient_dob').bootstrapMaterialDatePicker
// 			({
// 				time: false,
// 				clearButton: true,
// 				weekStart: 0, 
// 				format: 'DD/MM/YYYY'
// 			});

});
function isNumber(evt) {

evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;

if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	return false;
}

return true;
}

$('.rr').on('click', function(){
  $(this).parent().find('a').trigger('click')
})
$('#r11').on('click', function(){
  $(this).parent().find('a').trigger('click')
})

$('#r12').on('click', function(){
  $(this).parent().find('a').trigger('click')
})

/* new design  */
function setProgressBar(currentStep) { 
		var percent = parseFloat(100 / steps) * current; 
		percent = percent.toFixed(); 
		$(".progress-bar") 
			.css("width", percent + "%") 
	} 

function re_initialize(){
    $(".next-step").click(function () { 

currentGfgStep = $(this).parent(); 
nextGfgStep = $(this).parent().next(); 

$("#progressbar li").eq($("fieldset") 
    .index(nextGfgStep)).addClass("active"); 

nextGfgStep.show(); 
currentGfgStep.animate({ opacity: 0 }, { 
    step: function (now) { 
        opacity = 1 - now; 

        currentGfgStep.css({ 
            'display': 'none', 
            'position': 'relative'
        }); 
        nextGfgStep.css({ 'opacity': opacity }); 
    }, 
    duration: 500 
}); 
setProgressBar(++current); 
}); 

$(".previous-step").click(function () { 

currentGfgStep = $(this).parent(); 
previousGfgStep = $(this).parent().prev(); 

$("#progressbar li").eq($("fieldset") 
    .index(currentGfgStep)).removeClass("active"); 

previousGfgStep.show(); 

currentGfgStep.animate({ opacity: 0 }, { 
    step: function (now) { 
        opacity = 1 - now; 

        currentGfgStep.css({ 
            'display': 'none', 
            'position': 'relative'
        }); 
        previousGfgStep.css({ 'opacity': opacity }); 
    }, 
    duration: 500 
}); 
setProgressBar(--current); 
}); 
}

var currentGfgStep, nextGfgStep, previousGfgStep; 
	var opacity; 
	var current = 1; 
	var steps = $("fieldset").length; 

$(document).ready(function () { 

	
    re_initialize();
    setProgressBar(current); 

	$(".submit").click(function () { 
		return false; 
	})
    
   
}); 



function click_nxt(n){
    $(".next-step"+n).click();
}
/* new design  */
</script>



