<style>
.common_promo{
	padding:10px;
}
.offer-card{
    border-radius: 10px;
    background-color: #fff;
    box-shadow: 0 1px 2px 1px rgb(0 0 0 / 30%);
}
.card-bg{
	border-radius: 10px;
    background: #f0f0f0;
}
.card-body{
	padding:0px 20px 0px 20px;
}
.product-descs
{
padding:10px 20px 10px 0px;
border-bottom:2px solid #efefef;
border-top:none!important;
}
.product-descs:last-child
{
border-bottom:none;
border-top:none!important;
}
@media only screen and (max-width: 767px) {
.form-option-title{
	text-align:center;
	padding:5px 15px 5px 15px;
}
.form-option-expire{
	text-align:center;
	padding:5px 15px 5px 15px;
}
.expiring-box{
	margin-bottom:1em;
	margin-left: 25%;
}
}
@media only screen and (min-width: 768px) {
.form-option-title{
	text-align:left;
	padding:15px;
}
.form-option-expire{
	text-align:right;
	padding:15px;
}
.expiring-box{
	padding-right:3em;
	padding-left:0;
}
}

</style>
<?php 	
	$current_offers_quantity_based_card_count=0;$key=0;
	if(!empty($promotions) && ($inventory_product_info_obj->stock_available>0)){ 
	
	?> 

			<?php
			if($defaultDiscounts_num!=0){
				$promo_numbers=count($promotions)-$defaultDiscounts_num;
			}else{
				 $promo_numbers=count($promotions);
			}
			?>
			
			<div class="product-price-group mb-20" id="promotions_container_div">
			<?php
			/*
			?>

				<?php if($promo_numbers>0){ ?> 
				
				
				
				<span class="price"><span class="badge pull-right"><?php if($promo_numbers>1){echo $promo_numbers.' offers';}else{echo $promo_numbers.' offer';}?> </span></span><br>
				
				<?php }?>
				
				<?php
			*/
			?>
			
			
				<?php
				$i=1;     
													
			if(!empty($promotions)){
				$i=0; 

				
				echo '
				
						<div class="row common_promo">
				
							<div class="col-md-12">	
								<h4 id="new_offer_title" class="form-option-title" style="font-weight:bold;margin-left: -35px;
							}">Hey! We\'ve got offers for you</h4>
							</div>
							
				';

			
				
				foreach($promotions as $promotion){
					$key=$i;
					$str_free_inv="";
					$str_free_qty="";
					$applied_at_invoice=$promotion[0]->applied_at_invoice;
					
					
					if($applied_at_invoice!=1){
						
						$promo_uid = $promotion[0]->promo_uid;
						$offer = $promotion[0]->promo_quote;
						$to_buy = $promotion[0]->to_buy;
						$to_get = $promotion[0]->to_get;
						$buy_type = $promotion[0]->buy_type;
						$get_type = $promotion[0]->get_type;
						$offer = preg_replace("/\([^)]+\)/","",$offer);
						$offerDatas = $promotion[0]->promo_quote;
						$promo_end_date=$promotion[0]->promo_end_date;
							$offerss=strtolower($offer);
								if (preg_match("/\bsurprise\b/i", $offerss, $match)) {
										$buy_type="surprise_gift";
								}
						 if($get_type=="cash back"){
							 $buy_type="cash back";
						 }       
						
						if(preg_match('[(|)]', $offerDatas)){
							preg_match('#\((.*?)\)#', $offerDatas, $offerData);
							if(count($offerData)!=0){
								$offers=$offerData[1];
								if($offerData[1]=="Same SKUs"){
									$str_free_inv="";
									$str_free_qty="";
									for ($i = 1; $i <= $to_get; $i++) {
										if($promotion[0]->purchasing_inv_id!=""){
											$str_free_inv=$promotion[0]->purchasing_inv_id;
										}
										else{
											$str_free_inv=$inventory_id;
										}
											
											$str_free_qty=$promotion[0]->to_get;
										}
								}
							}else{
								$offers="";
							}
						}
						$validAt="";
						if((!($promotion[0]->buy_type=="<?php echo curr_code; ?>"))&&(!($promotion[0]->buy_type==""))){
							
							if($promotion[0]->to_buy>0){ //changed from 1
								$transition="quantity_based";
								if($moq>=$promotion[0]->to_buy){
									$disabled="";
								}else{
									$disabled="disabled";
								}
								
								if(!(($promotion[0]->to_buy=="1")&&($promotion[0]->to_get==""))){
									$validAt='Valid when Purchase Quantity is increased above '.($promotion[0]->to_buy-1);
								}
								$to_buy=$promotion[0]->to_buy;
							}
							else{
								$disabled="";
								$transition="non_quantity_based";
								$to_buy=1;
							}
						}
						
						else{
							
							$transition="non_quantity_based";
							$to_buy=1;
						}
						if(($promotion[0]->to_buy=="1")&&($promotion[0]->to_get=="")){
							$apply_at_single_only="single_only";
						}
						else{
							$apply_at_single_only="none";
						}	
						
						
						$strs="<div class='col-sm-12'>";
						$strs.='<div id="myCarousel_'.$i.'" class="carousel slide"  data-ride="carousel"><div class="carousel-inner" role="listbox">';
						$c=0;
						$free_sku='';
						//print_r($promotion);
						foreach($promotion as $promo){
							if(!empty($promo->purchasing_free_inv_id)){
								if($promo->quantity_def>0){
									$str_free_inv.=$promo->purchasing_free_inv_id.',';
									$str_free_qty.=$promo->quantity_def.',';
								}
								if($c==0){
									$active="active";
								}else{
									$active="";
								}
								$strs.='<div class="item '.$active.'">';
								if($promo->quantity_def>0){
									$offerss=strtolower($offer);
								if (preg_match("/\bsurprise\b/i", $offerss, $match)) {
									//$strs.=$controller->get_free_item_data($promo->purchasing_free_inv_id,$promo_uid,$promo->quantity_def);
									$free_sku='';
								}else{
									$free_sku=$controller->get_free_item_data($promo->purchasing_free_inv_id,$promo_uid,$promo->quantity_def);
									$get_free_item_data_image_path_attributes_arr=$controller->get_free_item_data_image_path_attributes($promo->purchasing_free_inv_id);
									
								}
							   
								}
							   //$strs.='</div>';
							}
							$c++;
						}

						
						$strs.='</div></div><!-- Controls --><a class="left carousel-control" ng-non-bindable href="#myCarousel_'.$i.'" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						<span class="sr-only">Previous</span></a><a class="right carousel-control" href="#myCarousel_'.$i.'" ng-non-bindable role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span><span class="sr-only">Next</span>  </a></div>';                                          
						

						if($promotion[0]->promotion_dependency==1){ ?>
					   <?php
					   
						//if(!empty($offers)){
										if($transition=='quantity_based' ){
											
											?>
											<input class="qty_promo qty_promo_available_<?php echo $promo_uid; ?>" id="qty_promo_available_<?php echo $promo_uid; ?>" name="qty_promo_available_[]" value="<?php echo $promo_uid; ?>" type="hidden">
        <input value="<?php echo $key; ?>" id="promo_key_<?php echo $promo_uid; ?>" name="key[]" type="hidden">
        <input class="buy_qty_promo" id="buy_qty_<?php echo $promo_uid; ?>" name="buy_qty[]" value="<?php echo $to_buy; ?>" type="hidden">
        <input class="buy_dis_promo" id="buy_discount_<?php echo $promo_uid; ?>" name="buy_discount[]" value="<?php echo $to_buy; ?>" type="hidden">
        <input class="promo_uid" id="promo_uid_<?php echo $promo_uid; ?>" name="promo_uid[]" value="<?php echo $promo_uid; ?>" type="hidden">
											<?php
											
											if($current_offers_quantity_based_card_count==0){
												?>
												<!-- card open -->
												<div class="offer-card">
												<div class="card-bg">
					   

                    <div class="row">
                        <div class="col-md-5">
                            <h4 class="form-option-title" style="font-weight:bold;" id="current_offers_heading">Current Offers</h4>
                        </div>
						<div class="col-md-3 pr-0">
                            <h4 class="form-option-expire" style="font-weight:bold;">Expiring by</h4>
                        </div>
                        <div class="col-md-4 col-xs-6 expiring-box">
						<div class="limited_cl_time">
                            <div class="text-center col-md-3 col-xs-3 count_div">
                                <div id="days_<?php echo $key; ?>">00 </div>
                                <div class="count_text">Days</div>
                            </div>
                            <div class="text-center col-md-3 col-xs-3 count_div">
                                <div id="hours_<?php echo $key; ?>">
                                    00 
                                </div>
                                <div class="count_text">Hours</div>
                            </div>
                            <div class="text-center col-md-3 col-xs-3 count_div">
                                <div id="min_<?php echo $key; ?>">
                                    00 
                                </div>
                                <div class="count_text">Min</div >
                            </div>
                            <div class="text-center col-md-3 col-xs-3 count_div">
                                <div id="seconds_<?php echo $key; ?>">
                                    00 
                                </div>
                                <div class="count_text">Sec</div>
                            </div>
							</div>
                        </div>
                     </div>


					<script>
$(document).ready(function(){
var count="<?php echo $key; ?>";
if(count==0){
	
promo_countdown('<?php echo $promo_end_date; ?>','<?php echo $key; ?>');
}
});
            
            </script>
						</div>	
						<div class="card-body">
												<?php
											}
											$current_offers_quantity_based_card_count++;
											?>
											<div class="product-descs">

											

											<?php
						}
						//}
								?>
								
								
							<?php	
							$promoquote_display_checktotalcostincludingshippingcharge="";
								if($transition=='quantity_based' && strtolower($offers)=="different skus"){
									$promoquote_display_checktotalcostincludingshippingcharge="";
									$promoquote_display_checktotalcostincludingshippingcharge.=$offer;
										$promoquote_display_checktotalcostincludingshippingcharge.=" of ";
										
										 $promoquote_display_checktotalcostincludingshippingcharge.=$get_free_item_data_image_path_attributes_arr["product_name"];?>
		<?php
			$color_attribute_is_there="no";
			if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_1"])=="color"){
				$color_attribute_is_there="yes";
				$promoquote_display_checktotalcostincludingshippingcharge.=" - ".explode(":",$get_free_item_data_image_path_attributes_arr["attribute_1_value"])[0];
			}
			else if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_2"])=="color"){
				$color_attribute_is_there="yes";
				$promoquote_display_checktotalcostincludingshippingcharge.=" - ".explode(":",$get_free_item_data_image_path_attributes_arr["attribute_2_value"])[0];
			}
			else if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_3"])=="color"){
				$color_attribute_is_there="yes";
				$promoquote_display_checktotalcostincludingshippingcharge.=" - ".explode(":",$get_free_item_data_image_path_attributes_arr["attribute_3_value"])[0];
			}
			else if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_4"])=="color"){
				$color_attribute_is_there="yes";
				$promoquote_display_checktotalcostincludingshippingcharge.=" - ".explode(":",$get_free_item_data_image_path_attributes_arr["attribute_4_value"])[0];
			}
		?>
		<?php
			if($color_attribute_is_there=="yes"){
				if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_1"])!="color"){
					if($get_free_item_data_image_path_attributes_arr["attribute_1_value"]!=""){
						$promoquote_display_checktotalcostincludingshippingcharge.="<font class='text-warning'> (".$get_free_item_data_image_path_attributes_arr["attribute_1_value"].")</font>";
					}
				}
				else if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_2"])!="color"){
					if($get_free_item_data_image_path_attributes_arr["attribute_2_value"]!=""){
						$promoquote_display_checktotalcostincludingshippingcharge.="<font class='text-warning'> (".$get_free_item_data_image_path_attributes_arr["attribute_2_value"].")</font>";
					}
				}
				else if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_3"])!="color"){
					if($get_free_item_data_image_path_attributes_arr["attribute_3_value"]!=""){
						$promoquote_display_checktotalcostincludingshippingcharge.="<font class='text-warning'> (".$get_free_item_data_image_path_attributes_arr["attribute_3_value"].")</font>";
					}
				}
				else if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_4"])!="color"){
					if($get_free_item_data_image_path_attributes_arr["attribute_4_value"]!=""){
						$promoquote_display_checktotalcostincludingshippingcharge.="<font class='text-warning'> (".$get_free_item_data_image_path_attributes_arr["attribute_4_value"].")</font>";
					}
				}
			}
			else{
				$noncolor_attribute_first_is_there="no";
				$noncolor_attribute_second_is_there="no";
				if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_1"])!="color"){
					$noncolor_attribute_first_is_there="yes";
					$promoquote_display_checktotalcostincludingshippingcharge.=" - ".$get_free_item_data_image_path_attributes_arr["attribute_1_value"];
				}
				if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_2"])!="color"){
					if($noncolor_attribute_first_is_there=="yes"){
						$noncolor_attribute_second_is_there="yes";
						if($get_free_item_data_image_path_attributes_arr["attribute_2_value"]!=""){
							$promoquote_display_checktotalcostincludingshippingcharge.="<font class='text-warning'> (".$get_free_item_data_image_path_attributes_arr["attribute_2_value"].")</font>";
						}
					}
					else{
						$noncolor_attribute_first_is_there="yes";
						$promoquote_display_checktotalcostincludingshippingcharge.=" - ".$get_free_item_data_image_path_attributes_arr["attribute_2_value"];
					}
				}
				if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_3"])!="color"){
					if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
						if($noncolor_attribute_first_is_there=="yes"){
							$noncolor_attribute_second_is_there="yes";
							if($get_free_item_data_image_path_attributes_arr["attribute_3_value"]!=""){
								$promoquote_display_checktotalcostincludingshippingcharge.="<font class='text-warning'> (".$get_free_item_data_image_path_attributes_arr["attribute_3_value"].")</font>";
							}
						}
						else{
							$noncolor_attribute_first_is_there="yes";
							$promoquote_display_checktotalcostincludingshippingcharge.=" - ".$get_free_item_data_image_path_attributes_arr["attribute_3_value"];
						}
					}
				}
				if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_4"])!="color"){
					if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
						if($noncolor_attribute_first_is_there=="yes"){
							$noncolor_attribute_second_is_there="yes";
							if($get_free_item_data_image_path_attributes_arr["attribute_4_value"]!=""){
								$promoquote_display_checktotalcostincludingshippingcharge.="<font class='text-warning'> (".$get_free_item_data_image_path_attributes_arr["attribute_4_value"].")</font>";
							}
						}
						else{
							$noncolor_attribute_first_is_there="yes";
							$promoquote_display_checktotalcostincludingshippingcharge.=" - ".$get_free_item_data_image_path_attributes_arr["attribute_4_value"];
						}
					}
				}
			}
			$color_attribute_is_there="no";
			$noncolor_attribute_first_is_there="no";
			$noncolor_attribute_second_is_there="no";
			
		?>
						
		<?php
		
									}
									else{
										$promoquote_display_checktotalcostincludingshippingcharge=$offer;
									}
									?>
									
								<input type="radio"  onclick="update_promotion_data(this,'from_click_of_radio',11,event)" free_invs="<?php echo $str_free_inv ?>" free_invs_qty="<?php echo $str_free_qty ?>" to_buy="<?php echo $to_buy?>" 
										offers_attached_to_radio="<?php echo $offers ?>" 
										buy_type="<?php if($buy_type){echo $buy_type;}else{echo "quantity_free";}?>"
										to_get="<?php if($to_get){echo $to_get;}else{echo "discount";}?>" 
										promo_selector="promo_selector_<?php echo $promotion[0]->promo_uid ?>" 
										apply_at_single_only="<?php echo $apply_at_single_only?>" 
										<?php if($apply_at_single_only=="single_only"){ echo 'class="hiddenRadioButton promo_class" checked="checked"';}else{ echo 'class="promo_class"'; }?>
										depend_qty="<?php echo $transition ?>" 
										<?php if($validAt){ echo 'title="'.$validAt.'"';}?> 
										<?php //if($transition!="non_quantity_based"){echo $disabled;} ?> 
										id="promotions_<?php echo $promotion[0]->promo_uid ?>" promoquote_display_checktotalcostincludingshippingcharge="<?php echo $promoquote_display_checktotalcostincludingshippingcharge;?>" 
										inventorymoq_promotions="<?php echo $inventory_product_info_obj->moq; ?>"
										get_type="<?php echo $promotion[0]->get_type; ?>" 
										name="dependent" style="
    position: relative;
    visibility: inherit;
"
quote_selector="<?php echo $promotion[0]->quote_selector ?>"
> <!---radio button not displaying--->
										
										 <a href="javascript:unselect_promo_radioFun('<?php echo $promotion[0]->quote_selector ?>','<?php echo $promotion[0]->promo_uid ?>')" id="promotionsunselect_<?php echo $promotion[0]->promo_uid ?>" quote_selector="<?php echo $promotion[0]->quote_selector ?>" style="display:none;">&nbsp;<u style="color:#31708f;text-decoration-style:dotted;">Unselect</u></a>
										
										
								<label for="promotions_<?php echo $promotion[0]->promo_uid ?>" <?php if($apply_at_single_only=="single_only"){ echo 'class="hiddenRadioButton all_promo_list" ';}?> class="all_promo_list" id="<?php echo $promo_uid ?>"> &nbsp;<?php echo $offer;?> 
								
								<?php
								
									if($transition=='quantity_based'){
										if($free_sku!=''){
											echo " of ".$free_sku;
										}
									}
								?>
								
									 <input type="hidden" name="promo_list_to_buy_<?php echo $promo_uid?>" id="promo_list_to_buy_<?php echo $promo_uid?>" value="<?php echo $to_buy?>">
									 <input type="hidden" name="promo_list_to_get_<?php echo $promo_uid?>" id="promo_list_to_get_<?php echo $promo_uid?>" value="<?php echo $to_get?>">
									 <input type="hidden" name="promo_list_to_buy_type_<?php echo $promo_uid?>" id="promo_list_to_buy_type_<?php echo $promo_uid?>" value="<?php echo $buy_type?>">
									 <input type="hidden" name="promo_list_to_get_type_<?php echo $promo_uid?>" id="promo_list_to_get_type_<?php echo $promo_uid?>" value="<?php echo $get_type?>">
								</label>
								  
								  
								   
								  <?php
								  //echo $transition.'||'.$offers.'<br>';
									if($transition=='quantity_based' && strtolower($offers)=="different skus"){
										$free_sku_image=$get_free_item_data_image_path_attributes_arr["thumbnail"];
										$free_sku_attr_details="";
										 $free_sku_attr_details.=$get_free_item_data_image_path_attributes_arr["product_name"];?>
		<?php
			$color_attribute_is_there="no";
			if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_1"])=="color"){
				$color_attribute_is_there="yes";
				$free_sku_attr_details.=" - ".explode(":",$get_free_item_data_image_path_attributes_arr["attribute_1_value"])[0];
			}
			else if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_2"])=="color"){
				$color_attribute_is_there="yes";
				$free_sku_attr_details.=" - ".explode(":",$get_free_item_data_image_path_attributes_arr["attribute_2_value"])[0];
			}
			else if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_3"])=="color"){
				$color_attribute_is_there="yes";
				$free_sku_attr_details.=" - ".explode(":",$get_free_item_data_image_path_attributes_arr["attribute_3_value"])[0];
			}
			else if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_4"])=="color"){
				$color_attribute_is_there="yes";
				$free_sku_attr_details.=" - ".explode(":",$get_free_item_data_image_path_attributes_arr["attribute_4_value"])[0];
			}
		?>
		<?php
		if($color_attribute_is_there=="yes"){
			if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_1"])!="color"){
				if($get_free_item_data_image_path_attributes_arr["attribute_1_value"]!=""){
					$free_sku_attr_details.="<font class='text-warning'> (".$get_free_item_data_image_path_attributes_arr["attribute_1_value"].")</font>";
				}
			}
			else if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_2"])!="color"){
				if($get_free_item_data_image_path_attributes_arr["attribute_2_value"]!=""){
					$free_sku_attr_details.="<font class='text-warning'> (".$get_free_item_data_image_path_attributes_arr["attribute_2_value"].")</font>";
				}
			}
			else if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_3"])!="color"){
				if($get_free_item_data_image_path_attributes_arr["attribute_3_value"]!=""){
					$free_sku_attr_details.="<font class='text-warning'> (".$get_free_item_data_image_path_attributes_arr["attribute_3_value"].")</font>";
				}
			}
			else if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_4"])!="color"){
				if($get_free_item_data_image_path_attributes_arr["attribute_4_value"]!=""){
					$free_sku_attr_details.="<font class='text-warning'> (".$get_free_item_data_image_path_attributes_arr["attribute_4_value"].")</font>";
				}
			}
		}
		else{
				$noncolor_attribute_first_is_there="no";
				$noncolor_attribute_second_is_there="no";
				if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_1"])!="color"){
					$noncolor_attribute_first_is_there="yes";
					$free_sku_attr_details.=" - ".$get_free_item_data_image_path_attributes_arr["attribute_1_value"];
				}
				if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_2"])!="color"){
					if($noncolor_attribute_first_is_there=="yes"){
						$noncolor_attribute_second_is_there="yes";
						if($get_free_item_data_image_path_attributes_arr["attribute_2_value"]!=""){
							$free_sku_attr_details.="<font class='text-warning'> (".$get_free_item_data_image_path_attributes_arr["attribute_2_value"].")</font>";
						}
					}
					else{
						$noncolor_attribute_first_is_there="yes";
						$free_sku_attr_details.=" - ".$get_free_item_data_image_path_attributes_arr["attribute_2_value"];
					}
				}
				if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_3"])!="color"){
					if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
						if($noncolor_attribute_first_is_there=="yes"){
							$noncolor_attribute_second_is_there="yes";
							if($get_free_item_data_image_path_attributes_arr["attribute_3_value"]!=""){
								$free_sku_attr_details.="<font class='text-warning'> (".$get_free_item_data_image_path_attributes_arr["attribute_3_value"].")</font>";
							}
						}
						else{
							$noncolor_attribute_first_is_there="yes";
							$free_sku_attr_details.=" - ".$get_free_item_data_image_path_attributes_arr["attribute_3_value"];
						}
					}
				}
				if(strtolower($get_free_item_data_image_path_attributes_arr["attribute_4"])!="color"){
					if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
						if($noncolor_attribute_first_is_there=="yes"){
							$noncolor_attribute_second_is_there="yes";
							if($get_free_item_data_image_path_attributes_arr["attribute_4_value"]!=""){
								$free_sku_attr_details.="<font class='text-warning'> (".$get_free_item_data_image_path_attributes_arr["attribute_4_value"].")</font>";
							}
						}
						else{
							$noncolor_attribute_first_is_there="yes";
							$free_sku_attr_details.=" - ".$get_free_item_data_image_path_attributes_arr["attribute_4_value"];
						}
					}
				}
			}
			$color_attribute_is_there="no";
			$noncolor_attribute_first_is_there="no";
			$noncolor_attribute_second_is_there="no";
			$free_sku_attr_details.="<br><p class='text-center'><img src=".base_url().$free_sku_image." style='width:25%;'></p>";
		?>
						<a href="#" title="" data-placement="top" data-toggle="popover" data-trigger="hover" data-html="true" 
									<?php //if($free_sku!=''){ echo 'id="free_item_toogle"';} ?> 
									data-content="<?php echo $free_sku_attr_details;  ?>" style="color:#ff9900;cursor:pointer;" ><i class="fa fa-question-circle" aria-hidden="true"></i></a> 
		<?php
		
									}
									else {
										/*
										?>
		<a href="#" title="" data-placement="top" data-toggle="popover" data-trigger="hover" data-html="true" 
									<?php if($free_sku!=''){ echo 'id="free_item_toogle"';} ?> 
									data-content="<?php echo $offers;  ?>" style="color:#ff9900;cursor:pointer;" ><i class="fa fa-question-circle" aria-hidden="true"></i></a> 
		<?php
		*/
									}
									
						?>		 
								  
									 
									
									
								
								
							
								
								<?php if($apply_at_single_only!="single_only"){ echo '<br>';}?>
								<!-- Quick buy design -->

											<?php 

	

													
												// echo 'true';
													
													preg_match_all('!\d+!', $get_type, $matches);
													// print_r( $matches);
													// print_r( $matches[0]);

													$per = implode(' ', $matches[0]);
												
											
														$max_selling_price=$inventory_product_info_obj->max_selling_price;
														$selling_price=$inventory_product_info_obj->selling_price;

														$actual_price=($max_selling_price*$to_buy);

														//echo $per."||".$to_buy."||".$max_selling_price;

														if(!empty($per)){
															$dis_price1 = ($to_buy*round(($per * $max_selling_price) / 100));
															$price2 = ($actual_price-$dis_price1);
														}else{
															//$dis_price1=0;
															$dis_price1=($actual_price-($to_buy*$selling_price));
															//$actual_price=($to_buy*$selling_price);
															$price2 = ($actual_price-$dis_price1);
														}

														//echo "<br>".$actual_price."ff".$dis_price1;

														
														
														$saved=$dis_price1;
														

														$pc=get_purchased_count($promo_uid);

														$txt='Best Value';$cl='';
														if($pc> 0){
															$txt='Most Purchased';
															$cl="quick_buy_tag1_m";
														}
														if($key==(count($promotions)-1)){
															$txt='Best Value';
														}


														?>
							<?php if(intval($saved)>0){ ?>
								<span class="saving_text mr-10">
								Saving <?php echo curr_sym; ?><?php echo $saved; ?>
								</span>
							<?php } ?>

							<span class="price_text"><b><?php echo curr_sym; ?> <?php echo $price2; ?> /-</b></span> 

							<?php if(intval($saved)>0){ ?>
                            	<span class="strike-class"><b><strike><?php echo curr_sym; ?> <?php echo $actual_price; ?> </strike></b></span>
							<?php } ?>

                            


                       
  
					<?php /*
								<small <?php if($apply_at_single_only=="single_only"){ echo 'class="hiddenRadioButton" ';}?>>Valid upto <span style="color:#e46c0a;"><?php echo date("D,j M Y h:i A", strtotime($promotion[0]->promo_end_date))?> </span></small>
								*/ ?>
									<?php if($apply_at_single_only!="single_only"){ echo '<br>';}
									if($free_sku!=''){
										echo '<div id="free_sku_div" style="display:none;">';
										echo '<h5><u>Free Items</u></h5>';
										echo $free_sku;
										echo '</div>';
									}
									
									
								//if(!empty($offers)){
										if($transition=='quantity_based'){
									?>
									</div>
									<?php
								}									
								//}
						} ?>
						   
					<?php
						
						if($promotion[0]->promotion_dependency==0){ ?>
						
						 <input type="radio"  onclick="update_promotion_data(this,'',12,event)" free_invs="<?php echo $str_free_inv ?>" free_invs_qty="<?php echo $str_free_qty ?>" to_buy="<?php echo $to_buy?>" 

								buy_type="<?php if($buy_type){echo $buy_type;}else{echo "quantity_free";}?>"
								to_get="<?php if($to_get){echo $to_get;}else{echo "discount";}?>" 
								promo_selector="promo_selector_<?php echo $promotion[0]->promo_uid ?>" 
								apply_at_single_only="<?php echo $apply_at_single_only?>" 
								<?php if($apply_at_single_only=="single_only"){ echo 'class="hiddenRadioButton promo_class" checked="checked"';}else{ echo 'class="promo_class"';}?>
								depend_qty="<?php echo $transition ?>" 
								<?php if($validAt){ echo 'title="'.$validAt.'"';}?> 
								<?php if($transition!="non_quantity_based"){echo $disabled;} ?> 
								id="promotions_<?php echo $promotion[0]->promo_uid ?>" name="dependent">
                                                 
                                                <a href="javascript:unselect_promo_radioFun('<?php echo $promotion[0]->quote_selector ?>','<?php echo $promotion[0]->promo_uid ?>')" id="promotionsunselect_<?php echo $promotion[0]->promo_uid ?>" style="display:none;">&nbsp;<u style="color:#31708f;text-decoration-style:dotted;">Unselect</u></a>
						
							<label for="promotions_<?php echo $promotion[0]->promo_uid ?>" <?php if($apply_at_single_only=="single_only"){ echo 'class="hiddenRadioButton all_promo_list" ';}?> class="all_promo_list" id="<?php echo $promo_uid ?>"><?php echo $offer;?> 
								 <input type="hidden" name="promo_list_to_buy_<?php echo $promo_uid?>" id="promo_list_to_buy_<?php echo $promo_uid?>" value="<?php echo $to_buy?>">
								 <input type="hidden" name="promo_list_to_get_<?php echo $promo_uid?>" id="promo_list_to_get_<?php echo $promo_uid?>" value="<?php echo $to_get?>">
								 <input type="hidden" name="promo_list_to_buy_type_<?php echo $promo_uid?>" id="promo_list_to_buy_type_<?php echo $promo_uid?>" value="<?php echo $buy_type?>">
								 <input type="hidden" name="promo_list_to_get_type_<?php echo $promo_uid?>" id="promo_list_to_get_type_<?php echo $promo_uid?>" value="<?php echo $get_type?>">
							</label>
						 
                                                 
							<?php 
							if(strlen($strs)>848){ ?>
								<div class="button-group">
									<button class="accordion1"></button>
									<div class="panel">
										<?php echo $strs;?>
									</div>
								</div>
								<?php                               
							}
						}
					
						$i++;
					}
				}
				
if($current_offers_quantity_based_card_count>0){
	?>
	</div>
	</div>
	<!--- card close--->
	<?php
}	

echo '</div>';
					 ?>
					
			</div><!--product-price-group-->	

			<script text="text/javascript">
				$(document).ready(function(){
				var acc = document.getElementsByClassName("accordion1");
				var i;
				
				for (i = 0; i < acc.length; i++) {
					acc[i].onclick = function(){
						this.classList.toggle("active");
						this.nextElementSibling.classList.toggle("show");
					}
				}
				});
			</script> 

			<?php
			
			}
		
	}
		?>							

	<!---->
	<script>
	$("#free_item_toogle").on("click", function(){
	  $("#free_sku_div").toggle();
	});
	</script>
	
	<script>
		//* set the current offers heading based on multiple radio offers and single radio offer starts ////
		current_offers_quantity_based_card_count="<?php echo $current_offers_quantity_based_card_count;?>";

		$('#new_offer_title').hide();
		
		if($("#current_offers_heading").length == 0){
			if(current_offers_quantity_based_card_count==1){
				$("#current_offers_heading").html("Choose Offer");
			}
			else{
				$("#current_offers_heading").html("Hey! We've got offers for you");
			}
		}else{
			if(current_offers_quantity_based_card_count==0){
				$('#new_offer_title').show();
			}else{
				$('#new_offer_title').hide();
			}
		}
		//* set the current offers heading based on multiple radio offers and single radio offer starts ////
	</script>
