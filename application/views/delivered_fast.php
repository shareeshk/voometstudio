<?php 
		$bg="";
		$ft="";
		if(!empty($ui_arr["bg_img"])){
			$bg=$ui_arr["bg_img"]->background_image;
		}
		if(!empty($ui_arr["ft_img"])){
			$ft=$ui_arr["ft_img"]->background_image;
		}
		
	?>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/home_page.css"> -->

<style type="text/css">

.btm-right{
	padding-right: 5px;
}
.btm-left{
	padding-left: 0px;
}
.btm-banner{
	width: 100%;
	height: 400px;
}
.btm-banner2{
	width: 100%;
	height: 400px;
}
.rb2-btn{
	padding: 0px !important;
    border: none !important;
    margin-top: 20px;
    border-bottom: 2px solid !important;
}
.rb-dd-banner-content {
    padding: 20px 15px 30px;
	left: 40px;
    right: 40px;
    margin: 0 auto;
    text-align: center;
	
	position: absolute;
    z-index: 9;
	top: 50%;
    left: 10%;
	transform: translateY(-50%);
}
.border-2{
	border: 2px solid #FFF;
}
	.bottom-banner-content{
		position: absolute;
		top: 38%;
		bottom: 15%;
		/* background: #fff; */
		left: 15%;
		line-height: 45px;
		text-align: left;
		padding: 20px;
		color:#fff;
	}
	.rb-dd-banner-subtitle {
		line-height: 29px;
		padding-left: 15px;
		display: inline-block;
		background: #ff6f00;
		color: #FFF;
		position: relative;
		font-size: 14px;
	}
	.rb-dd-banner-title {
    font-size: 40px;
    margin-top: 11px;
	color:#052670;
	}

	.rb-dd-banner-title1 {
    font-size: 40px;
    margin-top: 11px;
	color:#fff;
	}
	.rb-dd-banner-description {
    font-size: 20px;
	margin-bottom: 10px;
	}

	.rb-button1:before {
    content: "";
    position: absolute;
    bottom: 0;
    left: 50%;
    width: 100%;
    height: 2px;
    background: #FFF;
	}
	.rb-button {
    background: transparent;
    border: 2px solid #FFF;
    border-radius: 0;
	}
	.rb-button {
    font-size: 16px;
    line-height: 38px;
    display: inline-block;
    padding: 0 28px;
    color: #FFF;
	}
	.rb-dd-banner-subtitle:before {
    content: "";
    position: absolute;
    top: 0;
    left: 100%;
    border-width: 15px 14px;
    border-style: solid;
    border-color: #ff6f00;
    border-right-color: transparent;
	}
	.img-block{
	    width: 100%;
	}
	.img-block img{	
	background-color: #fff;
    border-radius: 50%;
	padding: 7px;
	margin:auto;
	}
.section-top{
	margin-top:30px;
}
.product-container{
	border: 1px solid #e1e1e1;
    margin-left: 5px;
    border-radius: 5px;
}
.product-container img{
	border-radius: 5px;
}
.col_last_right{
padding-right: 20px !important;
}

.col_first_left{
padding-left: 20px !important;
}
.min_padding{
	padding-right: 5px !important;
    padding-left: 5px !important;
}
.block-trending .owl-controls .owl-prev, .block-trending .owl-controls .owl-next {
    background: #fc6938;
    border-color: #fc6938;
    color: #fff;
}
.block-trending .owl-carousel .owl-controls .owl-prev{
    opacity: 1 !important;
    visibility: inherit;
}
.block-trending .owl-carousel .owl-controls .owl-next {
    opacity: 1 !important;
    visibility: inherit;
}
.interior_image{
	/* border-radius:10px; */
	border-radius: 50% !important;
	width: 122px;
    height: 122px;
}
.icon h4{
	line-height:30px;
	min-height: 30px;
}

.t_padding{
	/* padding-top: 13px; */
}
@media only screen and (max-width: 600px) {
  body {
    background-color: #fff;
  }
  
}

.section-title{
	line-height: 30px !important;
}
.our_partner1 {
    margin: 0 auto;
    max-width: 100%;
    padding: 30px 0;
}

.our_partner {
    -moz-box-pack: center;
    display: -moz-box;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
}
.our_partner .our_partner_inner {
    -moz-box-flex: 0;
    grid-column-gap: 20px;
    -moz-box-align: center;
    align-items: center;
    background-color: #fff;
    border: 1px solid #ccc;
    border-radius: 8px;
    display: -moz-box;
    display: flex;
    flex: 0 1 48%;
    grid-template-columns: -webkit-max-content auto;
    grid-template-columns: max-content auto;
    margin-bottom: 2%;
    margin-right: 2%;
    outline: none;
    padding: 24px;
}
.our_partner_img{
	/* background-color: #eee; */
    /* border-radius: 50%;
	vertical-align: top;
	width: 50%; */
	text-align:center;
   
}
.our_partner_img img{
	background-color: #eee;
    border-radius: 50%;
	vertical-align: top;
}
.designation{
	color: #9b9b9b;
    display: block;
    font-size: 13px;
    letter-spacing: 0;
    line-height: 16px;
}
.name{
	display: block;
    font-size: 14px;
    font-weight: 600;
    letter-spacing: 0;
    line-height: 17px;
    margin-top: 12px;
}

.bt_20{
	margin-bottom: 20px;
}
.img_side{
	border-radius: 8px;
    height: auto;
    width: 100%;
}
.p_top{
	margin-top:20px;line-height:25px;
}
.m_top{
	margin-top:20px;
}
.well {
    /* -webkit-box-shadow: 0 8px 20px rgba(0, 0, 0, 0.1);
    -moz-box-shadow: 0 8px 20px rgba(0, 0, 0, 0.1); */
    /* box-shadow: 0 8px 20px rgba(0, 0, 0, 0.1); */
	box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;
    /* border-top: 2px solid #ccc; */
    background-color: #f7f7f7;
    transition: all 0.3s ease-in-out 0s;

}
.back_image{
	/* background:url("<?php echo base_url() ?>assets/images/interior.jpg") no-repeat center center; */
    background-size:cover;
    width:100%;
    /* padding-top:20%;
    padding-bottom:20%; */
}

.back_image_kitchen{
	background:url("<?php echo base_url() ?>assets/images/modular_kitchen.png") no-repeat center center;
}
.back_image_bed_room{
	background:url("<?php echo base_url() ?>assets/images/modular_bed_room.png") no-repeat center center;
}
.back_image_home{
	background:url("<?php echo base_url() ?>assets/images/home.png") no-repeat center center;
}
.back_image_office{
	background:url("<?php echo base_url() ?>assets/images/modern_office.png") no-repeat center center;
}
.back_image_meeting_room{
	background:url("<?php echo base_url() ?>assets/images/meeting_room.png") no-repeat center center;
}
.back_image_work_stations{
	background:url("<?php echo base_url() ?>assets/images/work_stations.png") no-repeat center center;
}
.back_image .icon{
	/* background: linear-gradient(to top, #120e0e42 100%, #333230 76%); */
    color: #fff;
	border-radius:5px;
	background-color:rgba(0,0,0,0.6);
	padding:10px;
}

#demo {
  height:100%;
  position:relative;
  overflow:hidden;
}


.green{
  background-color:#6fb936;
}
        .thumb{
            margin-bottom: 30px;
        }
        
        .page-top{
            margin-top:85px;
        }

   .gal_img{
	padding-bottom: 32%;
    border-radius: 8px;
    display: inline-block;
    position: relative;
    width: 31%;
    text-align: center;
    margin: 1%;
   }
img.zoom {
    width: 100%;
    height: 200px;
    border-radius:5px;
    object-fit:cover;
    -webkit-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
    -ms-transition: all .3s ease-in-out;
}
        
 
.transition {
    -webkit-transform: scale(1.2); 
    -moz-transform: scale(1.2);
    -o-transform: scale(1.2);
    transform: scale(1.2);
}
    .modal-header {
   
     border-bottom: none;
}
    .modal-title {
        color:#000;
    }
    .modal-footer{
      display:none;  
    }
/*faq*/

.faqHeader {
        font-size: 27px;
        margin: 20px;
    }

    .panel-heading [data-toggle="collapse"]:after {
        font-family: 'fontawesome';
        content: "\f054"; /* "play" icon */
        float: right;
        color: #F58723;
        font-size: 18px;
        line-height: 22px;
        /* rotate "play" icon from > (right arrow) to down arrow */
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        -ms-transform: rotate(-90deg);
        -o-transform: rotate(-90deg);
        transform: rotate(-90deg);
    }

    .panel-heading [data-toggle="collapse"].collapsed:after {
        /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        transform: rotate(90deg);
        color: #454444;
    }

	.services_icon{
		margin: 0 auto;
		max-width: 100%;
		padding: 10px 0;
		/* display: flex; */
		flex-wrap: wrap;
		justify-content: center;
	}
	.img_text{
		position: absolute;
    bottom: 0;
   /* right: 0;*/
    color: #fff;
    font-size: 13px;
    font-weight:  500;
    text-align: center;
    width: 85%;
    background: linear-gradient(to top, #120e0e42 100%, #333230 76%);
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
	}

    @media (max-width: 420px){
.our_partner .our_partner_inner
  {
	display: block;
    flex: 0 1 100%;
    text-align: center;
  }
}
</style>

<style>
.services2 .services2-item,.services2{
border:0px;
}
.services2 .services2-item .image{
padding-left:120px;
}
.form-group .help-block {
display: none;
}
.form-group.has-error .help-block {
display: block;
}
.block-trending{
margin-top:0px !important;
}

.section-count {
    grid-column-gap: 80px;
    -moz-box-pack: center;

    grid-template-columns: -webkit-max-content -webkit-max-content -webkit-max-content -webkit-max-content;
    grid-template-columns: max-content max-content max-content max-content;
    justify-content: center;
    padding: 40px;
    text-align: center;

    font-size: 24px;
    font-weight: 700;
    letter-spacing: 0;
    line-height: 40px;
    text-transform: uppercase;

}
.colr_update{
	color:#ff3300;
}
.font_big{
    font-size: 48px;
}
.review_text{
	font-size:20px;
}
.yellow{
	color:gold;
}
.link_color{
	color:#ff3300;
}
</style>

	
	<style type="text/css">

	.bodyimage {
	<?php if($bg!=''){
		?>
	background: url('<?php echo base_url().$bg;?>') no-repeat;	
	
		<?php 
	}else{
		?>
		background:#b3c95c;
		<?php
		
	}?>
	background-size: 100% auto;
	  z-index:-100;
	}
	</style>
	
	
	<style type="text/css">
.form-group .help-block {
  display: none;
}
 
.form-group.has-error .help-block {
  display: block;
}
</style>


<style>
.swal2-modal .swal2-styled{
	background-color:#1429a0 !important;
}
.services_icon .well{
	
	min-height:100px;
	border-radius:5px;
	margin: auto;
}
.image{
	margin-top:10px;
	border-radius:20px;
}
.min_padd{
	padding-right: 5px !important;
    padding-left: 5px !important;
    padding-top: 5px !important;
    padding-bottom: 5px !important;
}
.matop{
    margin-top: 7px !important;
}
.office_align{
	margin-top: 5px;
    padding-left: 10px;
    padding-right: 5px;
}
.home_align{
	margin-top: 5px !important;
    padding-left: 5px !important;
    padding-right:5px !important;
}
.name-block{
	font-size: 20px;
    margin: 25px 0 25px 0;
}

.section_padding{
	padding: 0 10px 0 10px;
}
.deals_outside{
	border: 2px solid #ff6f00;
    padding: 0 35px 15px;
    border-radius: 8px;
}
.deals_title{
	font-weight: bold;
    font-size: 26px;
    background-color: #fff;
    text-align: left;
	margin-top: -40px;
    width: 40%;
    left: 10%;
    position: absolute;
    z-index: 1;
    padding: 20px;
	border-radius:1em;
	border:2px solid #ff6f00;

}
.deals_title .deals_sub{
    background: #ff6f00;
    line-height: 45px;
    padding: 0px; 
    border-radius: 30px;
    font-size: 16px;
    color: #fff;
	float: right;
    width: 250px;
    text-align: center;
}

/* media for mobile */
@media only screen and (max-width: 600px) {
.img-block
{
	text-align: center;
    margin-left: auto;
    margin-right: auto;
    width: 70%;
}
.deals_title{
	width: 80%;
    text-align: center;
	margin-top: -70px;
}
.rb-dd-banner-subtitle1{
	width:50%;
}
}
/* media for mobile */

</style>


<section class="section8" style="margin-bottom: 40px;">
	<div class="section-container">
			<br>
			<br>
			<div class="row" style="background-color: #f8f8f8;">
				<div class="col-md-12 section-top mb-20">
				<h3 class="mb-30 section-title text-center" style="text-transform:capitalize;">Custom Design Your Spaces With Voomet</h3>

				<div class="col-md-2 text-center">
					<a href="<?php echo base_url(); ?>design/home">
						<div class="img-block">
						<img class="img-responsive"src="<?php echo base_url(); ?>assets/images/design-ur-space/home.png" alt="Design Living Room Sofa"/>
						
						</div>
						<div class="name-block text-center">
							Design My<br> Home
						</div>
					</a>
				</div>

				<div class="col-md-2 text-center">
					<a href="<?php echo base_url(); ?>design/modular_kitchen">
						<div class="img-block">
						<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/design-ur-space/kitchen.png" alt="Design Living Room Sofa"/>
						</div>
						<div class="name-block text-center">
							Design My<br> Kitchen
						</div>
					</a>
				</div>
				<div class="col-md-2 text-center">
					<a href="<?php echo base_url(); ?>design/bedroom">
						<div class="img-block">
						<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/design-ur-space/bedroom.png" alt="Design Living Room Sofa"/>
						</div>
						<div class="name-block text-center">
							Design My<br> Bedroom
						</div>
					</a>
				</div>
				<div class="col-md-2 text-center">
					<a href="<?php echo base_url(); ?>design/office_room">
						<div class="img-block">
						<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/design-ur-space/office.png" alt="Design Living Room Sofa"/>
						</div>
						<div class="name-block text-center">
							Design Our<br> Office
						</div>
					</a>
				</div>
				<div class="col-md-2 text-center">
					<a href="<?php echo base_url(); ?>design/conference_room">
						<div class="img-block">
						<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/design-ur-space/conferenceroom.png" alt="Design Living Room Sofa"/>
						</div>
						<div class="name-block text-center">
							Design Our<br> Conference Room
						</div>
					</a>
				</div>

				<div class="col-md-2 text-center">
					<a href="<?php echo base_url(); ?>design/workstations">
						<div class="img-block">
						<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/design-ur-space/workstations.png" alt="Design Living Room Sofa"/>
						</div>
						<div class="name-block text-center">
							Design Our<br> Workstations
						</div>
					</a>
				</div>

				<!---->

				</div>
			</div>
	</div>
</section>


<section class="section8 " style="display:none;">
<div class="section-container">
        
		<div class="row " style="background-color: #fff;">
		
			<div class="col-md-12 ">

			<div class="service-wapper">
                        <div class="row services_icon">

						<div class="col-md-3 col_first_left">
						
						<div class="row col-half-offset text-center min_padd">
							<div class="well back_image back_image_kitchen">
                                <a href="<?php echo base_url(); ?>design/modular_kitchen">
								<div class="icon ">
									<h4 class="t_padding">I want to Design </h4><b><h3>My Modular Kitchen</h3></b>

									
									<!-- <i class="fa fa-home fa-5x" aria-hidden="true"></i> -->
                                </div>
								</a>

								<!-- <div> Section name</div> -->
                           
                        </div>
						</div>

						<div class="row col-half-offset text-center min_padd">
						<div class="well matop back_image back_image_bed_room">
								<a href="<?php echo base_url(); ?>design/modular_bedroom">
								<div class="icon ">	
								
								<h4 class="t_padding">I want to Design </h4><b><h3>My Bedroom</h3></b>

											
											<!-- <i class="fa fa-building fa-5x" aria-hidden="true"></i> -->
								</div>
								</div>
								</a>
						</div>

</div>


<div class="col-md-6 min_padding">
	

						<div class="col-sm-6 col-half-offset text-center home_align">
						<div class="well back_image back_image_home ">
						<a href="<?php echo base_url(); ?>design/home">
                                <div class="icon">
								<h4>We want to Design </h4><b><h3>Our Home</b></h3> 

									<!-- <i class="fa fa-bed fa-5x" aria-hidden="true"></i> -->

			<div class="image">
            <img class="interior_image" src="<?php echo base_url() ?>assets/images/home_icon1.png">
            </div>
                                </div>
						</a>
                           
                        </div>
						</div>

						<div class="col-sm-6 col-half-offset text-center office_align">
						<div class="well back_image back_image_office ">
						<a href="<?php echo base_url(); ?>design/our_office">
                                <div class="icon">
								<h4>We want to Design </h4><b><h3>Our Office</b></h3> 

									<!-- <i class="fa fa-bed fa-5x" aria-hidden="true"></i> -->

			<div class="image">
            <img class="interior_image" src="<?php echo base_url() ?>assets/images/office_icon1.png">
            </div>
                                </div>
						</a>
                           
                        </div>
						</div>

</div>

<div class="col-md-3 col_last_right">

						<div class="row col-half-offset text-center min_padd">
						<div class="well back_image back_image_meeting_room">
						<a href="<?php echo base_url(); ?>design/meeting_room">
                                <div class="icon">
								<h4 class="t_padding">We want to Design </h4><b><h3>Our Meeting Room</h3></b>

									
									<!-- <i class="fa fa-building-o fa-5x" aria-hidden="true"></i> -->
                                </div>
						</a>
                           
                        </div>
						</div>
						<div class="row col-half-offset text-center min_padd">
						<div class="well matop back_image back_image_work_stations">
						<a href="<?php echo base_url(); ?>design/work_station">
                                <div class="icon">
								<h4 class="t_padding">We want to Design </h4><b><h3>Our WorkStations</b></h3> 
</h4>
								
									<!-- <i class="fa fa-industry fa-5x" aria-hidden="true"></i> -->
                                </div>
						</a>
                        </div>
						</div>
</div>
                    </div>
					</div> <!---service section--->
				
			</div>
		</div><!---row--->
		</div>
</section>		   
<!-- </section> -->


<!-------- shopbyparentcategory section starts -------------->

<section class="section8 " style="margin-top:60px;">
<div class="section-container">

<section class="section8 block-trending assignproducts_section section_padding" id="shopbyparentcategory_range" >
<div class="section-container deals_outside">

<div class="deals_title" style="font-weight:normal">
	Deal Of The Day
	<span class="deals_sub"> Ends in  <span id="demo"></span></span>
	<!-- 601d 23h 55m 35s -->
</div>

<?php 
$parent_categories_all=$parent_categories;
if(!empty($parent_categories_all)){
	foreach($parent_categories_all as $parent_category){
		$pcat_id=$parent_category->pcat_id;
		$products_obj=front_product_display($pcat_id);
		?>		
<?php if(!empty($products_obj)){ 

?>


<!--<h3 class="mb-30 section-title new_title text-center" style="margin-top:50px;"><?php //echo $parent_category->pcat_name; ?></h3>-->
<!-- Parent Categories -->
<div class="section-content mt-20">
<div class="container">

<div class="row">


<div class="col-md-12">


<ul class="products-style8 owl-carousel"  data-dots="false" data-loop="true" data-nav = "true" data-margin = "29" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":4},"1000":{"items":4}}'>-
<!-- <ul class="row product-list grid"> -->

<?php 

foreach($products_obj as $pro){
?>

<li class="">
<div class="product-container selectProduct text-right" style="margin-bottom: 6px;">
<div class="left-block">
<?php 
$rand_1=$controller->sample_code();
$rand_2=$controller->sample_code();
?>
<a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pro->pcat_id}"; ?>">


<img class="img-responsive" style="width:75%;margin:2rem;" alt="productsku" src="<?php echo base_url().$pro->image;?>"/>

</a>

<div class="quick-view">

</div>


</div>




<div class="right-block">
<div class="text-center">
<h4 class="mb-10 text-center" style="margin-top:10px;"><?php echo $pro->pcat_name; ?></h4>
<h5 class="product-name heightmatched_shopbyparentcategory mb-20" style="font-size:1.05em;padding-bottom:0!important;word-break:break-word;line-height:1.3;"><a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pro->pcat_id}"; ?>"  title="<?php echo $pro->pcat_name; ?>">

<?php
echo $pro->pcat_name; 
?>

<?php
echo $pro->sku_name; 
?> <br> 
<?php
echo $pro->sku_id; 
?>
</a></h5>
</div>
<h5 class="product-name pb-2 text-center" style="font-size:0.9em;">
<b>


</b>


</h5>
<style>
.badge1 {
  border-radius: 0;
  font-size: 1.4rem;
  padding: 0.3rem 0.5rem;
  margin-bottom:1em;
}
.badge1--sales {
  position: relative;
  display: inline-block;
  background-color: #052670;
  width: auto;
  margin-left: 20px;
  height: 36px;
  line-height: 2.8rem;
  color:#fff;
}
.badge1--sales::before {
  position: absolute;
  left: -20px;
  top: 0;
  content: '';
  display: block;
  width: 0;
  height: 0;
  border-top: 20px solid transparent;
  border-right: 20px solid #052670;
  border-bottom: 16px solid transparent;
}
</style>								
<div class="badge1 badge1--sales"><?php echo  round($pro->selling_discount); ?>% discount <?php echo  $pro->stock; ?> Units Left</div>
<!--<div class="" style="background-color:#0088cc;padding:2px;color:#fff;border-radius:5px;text-align:center;">
<?php //echo  $pro->stock; ?> <?php //echo $pro->product_status; ?> at <?php //echo  round($pro->selling_discount); ?>% discount

</div>-->




</div>
</li>
<?php	
}

?>



</ul>


</div><!---col-md-10-->

</div>
</div>


<?php } ?>

<?php
	}
}


?>
</div>
</section>


<!-------- shopbyparentcategory section ends -------------->

</div>
</section>


<section class="section8" style="margin-bottom: 0px;display:none">
	<div class="section-container">
			
			<div class="row" style="background-color: #fff;">
				<div class="col-md-9 section-top btm-right">

<img class="btm-banner" src="<?php echo base_url() ?>assets/images/banimagebig.png" alt="">


					<div class="rb-dd-banner-content bottom-banner-content">
						<span class="rb-dd-banner-subtitle">20%
							off</span>
						<h4 class="rb-dd-banner-title">
							TALK TO US&ZeroWidthSpace;</h4>
						<div class="rb-dd-banner-description">
							Get Organized With Our
							Designs </div>
						<div><span class="rb-button-link rb-button "><span class="rb-button-text">Enquire</span></span>
						</div>
					</div>

				</div>
				<div class="col-md-3 section-top btm-left">

<img class="btm-banner2" src="<?php echo base_url() ?>assets/images/banimagesmall.png" alt="">

				<div class="rb-dd-banner-content border-2">
						<h4 class="rb-dd-banner-title1 ">
							Living in style</h4>
						<div><span class="rb-button-link rb-button rb2-btn"><span class="rb-button-text">Shop
									now</span></span>
						</div>
					</div>
				</div>


			</div>
	</div>
</section>


<section class="section8" style="margin-bottom: 0px;">
	<div class="section-container">
			
			<div class="row" style="background-color: #fff;">
				<div class="col-md-6 section-top btm-right">

<img class="btm-banner" src="<?php echo base_url() ?>assets/images/demo-furniture-banner-06.jpg" alt="">


					<div class="rb-dd-banner-content bottom-banner-content">
						<span class="rb-dd-banner-subtitle">20%
							off</span>
						<h4 class="rb-dd-banner-title">
							TALK TO US&ZeroWidthSpace;</h4>
						<div class="rb-dd-banner-description">
							Get Organized With Our
							Designs </div>
						<div><span class="rb-button-link rb-button "><span class="rb-button-text">Enquire</span></span>
						</div>
					</div>

				</div>
				<div class="col-md-6 section-top btm-left">

<img class="btm-banner2" src="<?php echo base_url() ?>assets/images/111111.png" alt="">

				<div class="rb-dd-banner-content border-2">
						<h4 class="rb-dd-banner-title1 ">
							Living in style</h4>
						<div><span class="rb-button-link rb-button rb2-btn"><span class="rb-button-text">Shop
									now</span></span>
						</div>
					</div>
				</div>


			</div>
	</div>
</section>

<script>


var module = angular.module('app', ['ui.bootstrap', 'angular-responsive']);

module.factory('Scopes', function ($rootScope) {
var mem = {};
return {
store: function (key, value) {
$rootScope.$emit('scope.stored', key);
mem[key] = value;
},
get: function (key) {
return mem[key];
}
};
});



module.directive('showErrors', function($timeout) {
return {
restrict: 'A',
require: '^form',
link: function (scope, el, attrs, formCtrl) {
var inputEl   = el[0].querySelector("[name]");
var inputNgEl = angular.element(inputEl);
var inputName = inputNgEl.attr('name');
var blurred = false;
inputNgEl.bind('blur', function() {
blurred = true;
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$watch(function() {
return formCtrl[inputName].$invalid
}, function(invalid) {
if (!blurred && invalid) { return }
el.toggleClass('has-error', invalid);
});

scope.$on('show-errors-check-validity', function() {
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$on('show-errors-reset', function() {
$timeout(function() {
el.removeClass('has-error');
}, 0, false);
});
}
}
});


</script>








<script>

var module = angular.module('app', ['ui.bootstrap', 'angular-responsive']);
module.factory('Scopes', function ($rootScope) {
var mem = {};
return {
store: function (key, value) {
$rootScope.$emit('scope.stored', key);
mem[key] = value;
},
get: function (key) {
return mem[key];
}
};
});

 
function beomeourfranchiseController($scope,$rootScope,Scopes,$http) {


Scopes.store('beomeourfranchiseController', $scope);




$scope.get_city_state_details_by_pincodeFun=function(){
		pin=$scope.formData_enquiry.pin;
		
		if(pin===undefined){
		}
		else{
		$http({
				method : "POST",
				url : "<?php echo base_url();?>Account/get_city_state_details_by_pincode",
				data:{'pin' : pin},
				dataType:"JSON",
				}).success(function mySucces(result) {
					$scope.formData_enquiry.city=result.city;
					$scope.formData_enquiry.state=result.state;
					$scope.formData_enquiry.country="India";
					
					if(parseInt(result.count)==0){
                        $scope.delivery_address_pin_error=1;
                        $('.delivery_address_pin_error').show();
                        $('#pin_code_check').addClass('has-error');
                    }else{
                        //alert(result.count+"no error");
                        $scope.delivery_address_pin_error=0;
                        $('.delivery_address_pin_error').hide();
                        $('#pin_code_check').removeClass('has-error');
                    }

				}, function myError(response) {
					
				});
		}	
	 }

}



module.directive('allowOnlyNumbers', function () {  
            return {  
                restrict: 'A',  
                link: function (scope, elm, attrs, ctrl) {  
                    elm.on('keydown', function (event) {  
                        if (event.which == 64 || event.which == 16 || event.which == 46 || event.which == 86) {  
                            // to allow numbers  
                            return false;  
                        } else if (event.which >= 48 && event.which <= 57) {  
                            // to allow numbers  
                            return true;  
                        } else if (event.which >= 96 && event.which <= 105) {  
                            // to allow numpad number  
                            return true;  
                        } else if ([8, 9, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {  
                            // to allow backspace, enter, escape, arrows  
                            return true;  
                        } else {  
                            event.preventDefault();  
                            // to stop others  
                            return false;  
                        }  
                    });  
                }  
            }  
        });



        module.directive('emailExistC', function($http) {
    //alert(22323);
	return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attr, ctrl) {
			function customValidator(ngModelValue)
			{
				ctrl.$setValidity('emailexistValidator', true);
				var FormData = {'email' : ngModelValue};

				 $http({
					method : "POST",
					url : "<?php echo base_url();?>Franchiseloginsession/check_email",
					data:FormData,
					dataType: 'json',
                                        async:false
					}).success(function mySucces(res) {
						if(res.valid!=null)
						{
							if(res.valid==true)
							{
								ctrl.$setValidity('emailexistValidator',true );
							}
							if(res.valid==false){
								ctrl.$setValidity('emailexistValidator', false);
							}
						}
						else{
							ctrl.$setValidity('emailexistValidator', true);
						}

					}, function myError(response) {
							/*bootbox.alert({
							  size: "small",
							  message: 'Error',
							});*/
							alert('Error');
					});

				return ngModelValue;
			}
			ctrl.$parsers.push(customValidator);
      }
    };
});


module.directive('mobileExistC', function($http) {
	return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attr, ctrl) {

			function custommValidator(ngModelValue)
			{
				ctrl.$setValidity('mobileexistValidator',true );
				var FormData = {'mobile' : ngModelValue};
				 $http({
					method : "POST",
					url : "<?php echo base_url();?>Franchiseloginsession/check_mobile",
					data:FormData,
					dataType: 'json',
                                        async:false
					}).success(function mySucces(res) {
						if(res.valid!=null)
						{
							if(res.valid==true)
							{
								ctrl.$setValidity('mobileexistValidator',true );
							}
							if(res.valid==false){
								ctrl.$setValidity('mobileexistValidator',false);
							}
						}else{
							ctrl.$setValidity('mobileexistValidator',true );
						}

					}, function myError(response) {
						/*/bootbox.alert({
						  size: "small",
						  message: 'Error',
						});*/
						alert('Error');
					});

				return ngModelValue;
			}
			ctrl.$parsers.push(custommValidator);
      }
    };
});



module.directive('showErrors', function($timeout) {
return {
restrict: 'A',
require: '^form',
link: function (scope, el, attrs, formCtrl) {
var inputEl   = el[0].querySelector("[name]");
var inputNgEl = angular.element(inputEl);
var inputName = inputNgEl.attr('name');
var blurred = false;
inputNgEl.bind('blur', function() {
blurred = true;
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$watch(function() {
return formCtrl[inputName].$invalid
}, function(invalid) {
if (!blurred && invalid) { return }
el.toggleClass('has-error', invalid);
});

scope.$on('show-errors-check-validity', function() {
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$on('show-errors-reset', function() {
$timeout(function() {
el.removeClass('has-error');
}, 0, false);
});
}
}
});

$(document).ready(function () {
// $('#patient_dob').datepicker({
// format: "dd/mm/yyyy"
// });

// $('#patient_dob').bootstrapMaterialDatePicker
// 			({
// 				time: false,
// 				clearButton: true,
// 				weekStart: 0, 
// 				format: 'DD/MM/YYYY'
// 			});

});
function isNumber(evt) {

evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;

if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	return false;
}

return true;
}

</script>

<script>

$(document).ready(function(){
  $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });
    
    $(".zoom").hover(function(){
		
		$(this).addClass('transition');
	}, function(){
        
		$(this).removeClass('transition');
	});
});

// Set the date we're counting down to
var countDownDate = new Date("July 5, 2024 15:37:25").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("demo").innerHTML = days + " Days by 5th July";
  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);
</script>