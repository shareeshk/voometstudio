<?php 
	if($this->session->userdata("customer_name")){

		if($this->session->userdata("cur_page")){
			$cur_page=$this->session->userdata("cur_page");
			header("Location: ".$cur_page."");
		}else{	
			header("Location: ".base_url()."");
		}
	}

?>
<?php
	$email_stuff_arr=email_stuff();
?>
<style type="text/css">

  /* css for check box radio */
  .checkbox label:after, 
.radio label:after {
    content: '';
    display: table;
    clear: both;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .8em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
  /* css for check box radio */

.form-group .help-block {
  display: none;
}
.form-group.has-error .help-block {
  display: block;
}
#mydiv {  
    position:absolute;
    top:0;
    left:0;
    width:100%;
    height:100%;
    z-index:1000;
   /* background-color:#fff;*/
    opacity: .8;
 }

.ajax-loader {
    position: absolute;
    left: 50%;
    top: 50%;
    margin-left: -32px; /* -1 * image width / 2 */
    margin-top: -32px;  /* -1 * image height / 2 */
    display: block;     
}
#mydiv_re {  
    position:absolute;
    top:0;
    left:0;
    width:100%;
    height:100%;
    z-index:1000;
   /* background-color:#fff;*/
    opacity: .8;
 }

.ajax-loader_re {
    position: absolute;
    left: 50%;
    top: 32%;
    margin-left: -32px; /* -1 * image width / 2 */
    margin-top: -32px;  /* -1 * image height / 2 */
    display: block;     
}
</style>
<style>
.mand{
	color:#ff0000;
	font-weight:bold;
}
</style>

<script type="text/javascript">



module.directive('emailExistVendor', function($http) {
	return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attr, ctrl) {
			function customValidator(ngModelValue)
			{
				ctrl.$setValidity('emailexistValidatorVendor', true);
				var FormData = {'email' : ngModelValue};
                               
				 $http({
					method : "POST",
					url : "<?php echo base_url();?>Loginsession/check_email_vendor",
					data:FormData,
					dataType: 'json',
                                        async:false
					}).success(function mySucces(res) {
						if(res.valid!=null)
						{
							if(res.valid==true)
							{
								ctrl.$setValidity('emailexistValidatorVendor',true );
							}
							if(res.valid==false){
								ctrl.$setValidity('emailexistValidatorVendor', false);
							}
						}
						else{
							ctrl.$setValidity('emailexistValidatorVendor', true);
						}

					}, function myError(response) {
							/*bootbox.alert({
							  size: "small",
							  message: 'Error',
							});*/
							alert('Error');
					});

				return ngModelValue;
			}
			ctrl.$parsers.push(customValidator);
      }
    };
});

module.directive('mobileExistVendor', function($http) {
	return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attr, ctrl) {

			function custommValidator(ngModelValue)
			{
				ctrl.$setValidity('mobileexistValidatorVendor',true );
				var FormData = {'mobile' : ngModelValue};
				 $http({
					method : "POST",
					url : "<?php echo base_url();?>Loginsession/check_mobile_vendor",
					data:FormData,
					dataType: 'json',
                                        async:false
					}).success(function mySucces(res) {
						if(res.valid!=null)
						{
							if(res.valid==true)
							{
								ctrl.$setValidity('mobileexistValidatorVendor',true );
							}
							if(res.valid==false){
								ctrl.$setValidity('mobileexistValidatorVendor',false);
							}
						}else{
							ctrl.$setValidity('mobileexistValidatorVendor',true );
						}

					}, function myError(response) {
						/*/bootbox.alert({
						  size: "small",
						  message: 'Error',
						});*/
						alert('Error');
					});

				return ngModelValue;
			}
			ctrl.$parsers.push(custommValidator);
      }
    };
});


module.directive('validPasswordC', function() {

  return {
    require: 'ngModel',
    scope: {
      reference: '=validPasswordC'
    },
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$setValidity('noMatch', true);
      ctrl.$parsers.unshift(function(viewValue, $scope) {

        var noMatch = viewValue != scope.reference
        ctrl.$setValidity('noMatch', !noMatch);
        return (noMatch)?noMatch:!noMatch;
      });

      // scope.$watch("reference", function(value) {;
      //   ctrl.$setValidity('noMatch', value === ctrl.$viewValue);
      // });
    }
  }
});

module.directive('allowOnlyNumbers', function () {
            return {  
                restrict: 'A',  
                link: function (scope, elm, attrs, ctrl) {  
                    elm.on('keydown', function (event) {  
                        if (event.which == 64 || event.which == 16 || event.which == 46 || event.which == 86) {  
                            // to allow numbers  
                            return false;  
                        } else if (event.which >= 48 && event.which <= 57) {  
                            // to allow numbers  
                            return true;  
                        } else if (event.which >= 96 && event.which <= 105) {  
                            // to allow numpad number  
                            return true;  
                        } else if ([8, 9, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {  
                            // to allow backspace, enter, escape, arrows  
                            return true;  
                        } else {  
                            event.preventDefault();  
                            // to stop others  
                            return false;  
                        }  
                    });  
                }  
            }  
        });
module.directive('showErrors', function($timeout) {
    return {
      restrict: 'A',
      require: '^form',
      link: function (scope, el, attrs, formCtrl) {
        // find the text box element, which has the 'name' attribute
        var inputEl   = el[0].querySelector("[name]");
        // convert the native text box element to an angular element
        var inputNgEl = angular.element(inputEl);
        // get the name on the text box
        var inputName = inputNgEl.attr('name');

        // only apply the has-error class after the user leaves the text box
        var blurred = false;
        inputNgEl.bind('blur', function() {
          blurred = true;
          el.toggleClass('has-error', formCtrl[inputName].$invalid);
        });

        // scope.$watch(function() {
        //   return formCtrl[inputName].$invalid
        // }, function(invalid) {
        //   // we only want to toggle the has-error class after the blur
        //   // event or if the control becomes valid
        //   if (!blurred && invalid) { return }
        //   el.toggleClass('has-error', invalid);
        // });

        scope.$on('show-errors-check-validity', function() {
          el.toggleClass('has-error', formCtrl[inputName].$invalid);
        });

        scope.$on('show-errors-reset', function() {
          $timeout(function() {
            el.removeClass('has-error');
          }, 0, false);
        });
      }
    }
  });



function generate_random_number()
{
	//localStorage.removeItem('random_no');
	ra=Math.floor(100000 + Math.random() * 900000);
	//localStorage.setItem('random_no', ra);
	return ra;
}

module.directive('showOtperrors', function($timeout) {
    return {
      restrict: 'A',
      require: '^form',
      link: function (scope, el, attrs, formCtrl) {
        var inputEl   = el[0].querySelector("[name]");
        var inputNgEl = angular.element(inputEl);
        var inputName = inputNgEl.attr('name');
        var blurred = false;
        inputNgEl.bind('blur', function() {
          blurred = true;
          el.toggleClass('has-error', formCtrl[inputName].$invalid);
        });
        
        // scope.$watch(function() {
        //   return formCtrl[inputName].$invalid
        // }, function(invalid) {
        //   if (!blurred && invalid) { return }
        //   el.toggleClass('has-error', invalid);
        // });
        
        scope.$on('show-errors-check-validity', function() {
          el.toggleClass('has-error', formCtrl[inputName].$invalid);
        });
        
        scope.$on('show-errors-reset', function() {
          $timeout(function() {
            el.removeClass('has-error');
          }, 0, false);
        });
      }
    }
  });
module.directive('userExistC', function($http) {
	return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attr, ctrl) {
			function custmValidator(ngModelValue)
			{
				ctrl.$setValidity('userexistValidator', true);

				return ngModelValue;
			}
			ctrl.$parsers.push(custmValidator);
      }
    };
});

module.directive('passwordCheckC', function($http) {
	return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attr, ctrl) {
			function custmmValidator(ngModelValue)
			{
				ctrl.$setValidity('passwordwrongValidator', true);	
				return ngModelValue;
			}
			ctrl.$parsers.push(custmmValidator);     
      }
    };
});
module.service('myservice', function() {
      this.add = function(mobile){
			this.mobile_t = mobile;
		  };
		this.get=function(){
			//return mobile_t;
			this.setmobileno=this.mobile_t;
		}
    });
function NewUserController($scope,Scopes,$http, myservice) {
	Scopes.store('NewUserController', $scope);
	//$scope.formData = MyService.formData;
	$scope.myservice = myservice;
	$scope.regex=/(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z]).{6,20}/;
        $scope.vendor_reg=1;
  
  $scope.about_furniture_operations_manufacturer=false;
  $scope.about_furniture_operations_importer=false;
  $scope.about_furniture_operations_trader=false;
  
  $scope.applicable_media_website=true;
  $scope.applicable_media_instagram=false;
  $scope.applicable_media_facebook=false;
  
  $scope.kind_of_furniture_lowest_cost=false;
  $scope.kind_of_furniture_economical=false;
  $scope.kind_of_furniture_luxury=false;
  $scope.kind_of_furniture_super_luxury=false;
  
  $scope.size_of_team="";
  $scope.years_of_business="";
  $scope.shipping_capabilities="";

  $scope.copy_mobile_number = function(){
    //alert($scope.formData.use_mobile_number);
    if($scope.formData.mobile!='' && $scope.formData.use_mobile_number==true){
      $scope.formData.whatsapp_number=$scope.formData.mobile;
      $scope.use_number=true;
    }else{
      $scope.use_number=false;
    }
  }
	$scope.save = function($event) {
            //alert($scope.vendor_reg);
    $scope.$broadcast('show-errors-check-validity');
	if ($scope.userForm.$valid) {
                //alert($scope.formData.email + "before");
                if($scope.formData.email==null || $scope.formData.email===undefined){
                    //alert('went inside')
                    $scope.formData.email=$("#email").val();
                }
                //alert($scope.formData.email + "after");
		myservice.add($scope.formData.mobile);
                password=$scope.formData.password;
                original_password=$scope.formData.password;

                var temppwd="";
                for(i=0;i<password.length;i++){
                temppwd+=String.fromCharCode(password.charCodeAt(i)-6);
                }

                $data=$scope.formData;
                $data.temppassword=temppwd;

                url="<?php echo base_url();?>Loginsession/add_vendor";
                
		$scope.formData.password=original_password;

		$("#reg_btn").html('<i class="fa fa-spin fa-refresh"></i> Processing');
                document.getElementById("reg_btn").disabled = true;
		$http({
    method : "POST",
    url : url,
		data: $data,
		dataType: 'json',
    async:false
		}).success(function mySucces(res) {

//console.log(res);
                    if(res.data==true){
                        myservice.get();
                        $scope.c_id=res.c_id;
                        $scope.setvariables();
                        //$scope.reset_for_change();

    
	swal({
		title: 'Registration Successful!',
		html: "<p>We have sent you an email at "+$scope.formData.email+" with directions on how to login to your account.Kindly login and start your journey of success.</p>",
		type: 'success',
		showCancelButton: false,
		showConfirmButton: true,
		confirmButtonText: 'Ok',
	}).then(function () {
    //window.location.href="<?php echo base_url();?>Administrator";
	location.reload();
	});



                       
                    }else{
                        //alert('New User not created');
						
						
						swal({
		title: 'Error!',
		html: "<p>New User not created</p>",
		type: 'error',
		showCancelButton: false,
		showConfirmButton: true,
		confirmButtonText: 'Ok',
	}).then(function () {
    
	});
	
	
                    }
                    $("#reg_btn").html('<i class="fa fa-user"></i> Create');
                    document.getElementById("reg_btn").disabled = false;

		},function myError(response) {

		});

    }
        $event.stopPropagation();
  };

    $scope.setvariables = function() {
        if(Scopes.get('NewUserController').c_id != null)
        {
              c_id=Scopes.get('NewUserController').c_id;
              mobile=Scopes.get('NewUserController').formData.mobile;
              email=Scopes.get('NewUserController').formData.email;
              name=Scopes.get('NewUserController').formData.name;
              //alert('c_id='+c_id+',mobile='+mobile+',email='+email);
              $scope.$broadcast('show-errors-reset');
              $scope.formData = {name: name, email: email,mobile: mobile,password: '',password_c: ''};
        }
    }

    $scope.get_city_state_details_by_pincodeFun=function(){
		pin=$scope.formData.pincode;
		
		if(pin===undefined){
		}
		else{
		$http({
				method : "POST",
				url : "<?php echo base_url();?>Account/get_city_state_details_by_pincode",
				data:{'pin' : pin},
				dataType:"JSON",
				}).success(function mySucces(result) {
					$scope.formData.city=result.city;
					$scope.formData.state=result.state;
					$scope.formData.country="India";
					
					if(parseInt(result.count)==0){
                        $scope.delivery_address_pin_error=1;
                        $('.delivery_address_pin_error').show();
                        $('#pin_code_check').addClass('has-error');
                    }else{
                        //alert(result.count+"no error");
                        $scope.delivery_address_pin_error=0;
                        $('.delivery_address_pin_error').hide();
                        $('#pin_code_check').removeClass('has-error');
                    }

				}, function myError(response) {
					
				});
		}	
	 }


  $scope.hide_show_checkbox = function(id_name) {

    //alert($scope.formData.applicable_media.website);
    
    if($scope.formData.applicable_media.website==true){
      $('#website_div').show();
    }else{
      $('#website_div').hide();
    }
    if($scope.formData.applicable_media.instagram==true){
      $('#instagram_div').show();
    }else{
      $('#instagram_div').hide();
    }
    if($scope.formData.applicable_media.facebook==true){
      $('#facebook_div').show();
    }else{
      $('#facebook_div').hide();
    }

  }
  $scope.hide_show = function(value,id_name) {
    //alert(value);
    //alert(id_name);
    if(value=='yes'){
      $('#'+id_name).show();
    }
    if(value=='no'){
      $('#'+id_name).hide();
    }
    if(value=='website'){
      $('#'+id_name).show();
    }
    if(value=='instagram'){
      $('#'+id_name).show();
    }
    if(value=='facebook'){
      $('#'+id_name).show();
    }


  }

    $scope.reset = function() {
    $scope.$broadcast('show-errors-reset');
    $scope.formData = {name: '', email: '',mobile: '',password: '',password_c: ''};
    
  }

	
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
        return false;
    }
    return true;
}
$('#mobile_number').on('keydown', function(e){
    evt = (e) ? e : window.e;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
        return false;
    }
    return true;
});
</script>
<!-- page wapper-->

<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb 
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php //echo base_url()?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Login</span>
        </div>-->
        <!-- ./breadcrumb -->
        <!-- page heading-->
        
        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
			<div class="col-md-6">
			<div class="card section8" style="box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;padding:15px 20px">
			
			<h3 class="section-title align-bottom align-top text-center" style="font-size: 20px;line-height: 30px;height: 36px;padding-bottom: 10px;">Expand Your Reach with Voomet Studio : <br>A Partnership for Growth</h3>
			<p class="mt-10" style="line-height: 25px;">As a seller, you can access a vast audience through this ecommerce platform,  eager for high-quality, stylish furniture. Enjoy seamless integration, competitive fees, and dedicated support. Start showcasing your products today and grow with us. Together, let's transform homes and lives. Register with us now!</p>
			
			</div>
			<div class="card accordion mt-20" id="accordionhowtouse">
		<h4 class="desc ccap text-center" style="margin-bottom:20px;font-weight: bold;">Frequently Asked Questions</h4>	


<div class="card section8" style="box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;padding:15px 20px">	
<h4 style="padding-bottom:10px;color:#6191d9">About Voomet Studio Ecommerce (VSE)</h4>			
  <div class="align_top product-desc editor_content mb-10">
    <div class=" pl-10 pr-10 pt-10 pb-10" id="headinghowtouse" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse" aria-expanded="false" aria-controls="collapsehowtouse" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">
           What is Voomet Studio Ecommerce?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse" class="collapse" aria-labelledby="headinghowtouse" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
      Voomet Studio is an aggregator ecommerce platform with a difference. Unlike other aggregators, which focus on maximizing sales irrespective of vendor specific needs, Voomet Studio ecommerce helps vendors with Visibility, Pitch, Positioning, AI driven Search & Sales.						
      </div>
    </div>
  </div>
 
  <div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_2" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_2" aria-expanded="false" aria-controls="collapsehowtouse_2" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">
           What is the objective of Voomet Studio Ecommerce?
 <i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_2" class="collapse" aria-labelledby="headinghowtouse_2" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
      <p>Voomet Studio's objective is to bring in various manufacturers/traders of home related categories to the market with an 'exceptional ease of searching & buying' process all driven by the AI engine of Voomet Studio. Thereby helping manufacturers increase their sales and their own brand value. </p>
							
      </div>
    </div>
  </div>
	
 <div class=" align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_3" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_3" aria-expanded="false" aria-controls="collapsehowtouse_3" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">
          How does selling in Voomet Studio Ecommerce works?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_3" class="collapse" aria-labelledby="headinghowtouse_3" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
       Selling on Voomet Studio platform is easy. As a vendor, you will first register on the platform and list your products that you want to sell on Voomet Studio platform. Once the admin approves those listings, the products will start to show in the ecommerce platform and customers can start to search and buy. 					
      </div>
    </div>
  </div>
  <div class="align_top product-desc editor_content mb-10">
    <div class=" pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_6" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_6" aria-expanded="false" aria-controls="collapsehowtouse_3" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">Will the seller have their own Admin panel? <i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_6" class="collapse" aria-labelledby="headinghowtouse_6" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
Yes, each seller has their own Admin panel which gets activated once the Voomet Studio Administrator reviews and approves it. As a seller, you will have access to listings, adding, deleting, out of stock, order management flow, 11 kinds of sales promotions and a complete suite of analytics to review the market traction for your brand. 
								
      </div>
    </div>
  </div>
</div>
<div class="card section8" style="box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;padding:15px 20px; margin-top:25px">

<h4 style="padding-bottom:10px;color:#6191d9">About Products & End Customers </h4>	

<div class=" align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_4" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_4" aria-expanded="false" aria-controls="collapsehowtouse_4" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">What Products can I sell in Voomet Studio Ecommerce?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_4" class="collapse" aria-labelledby="headinghowtouse_4" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
     <p>As a seller on Voomet Studio ecommerce, you can offer a variety of products that cater to home and office needs. You might consider selling – furniture’s, home decors, office furniture, outdoor furniture, rugs, carpets, home textiles, wall decors, storage solutions, tableware, serve ware, home accessories, vintage/ antique items, smart home devices etc.</p>
<p>Offering a diverse range of high-quality, stylish, and functional products can attract a broad customer base. Additionally, focusing on unique items such as custom-made or eco-friendly products can help differentiate your offerings from competitors.
Please note that every category of products that are uploaded goes through verification and certain categories are restricted and require prior approval before you can start selling.</p>
 </div>
    </div>
  </div>

<div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_5" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_5" aria-expanded="false" aria-controls="collapsehowtouse_5" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">Are the products sold on my brand name or Voomet Studio brand name?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_5" class="collapse" aria-labelledby="headinghowtouse_5" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
     No. The products are sold in your brand name only. Whatever you define gets sold like that. Complete control is in the hands of sellers. 								
      </div>
    </div>
  </div>

<div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_61" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_61" aria-expanded="false" aria-controls="collapsehowtouse_61" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">Do I get to know the end-customer details?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_61" class="collapse" aria-labelledby="headinghowtouse_61" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
       <p>Yes. Voomet Studio is transparent end to end. All customer details are shown in your own Admin panel and you can directly connect with them as well, in case needed.</p>
									
      </div>
    </div>
  </div>
  <div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_7" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_7" aria-expanded="false" aria-controls="collapsehowtouse_7" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">Can customers leave feedback and why is customer feedback important?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_7" class="collapse" aria-labelledby="headinghowtouse_7" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
       Yes. Customers can leave feedback. Maintaining a high feedback rating is a critical factor for success on Voometstudio.com. It’s the best way for customers to identify you as a trustworthy seller. Your rating appears on multiple relevant places. In other marketplaces, we have observed that customers are more likely to purchase products from sellers with higher ratings. Your feedback rating is a key metric used by Voometstudio.com to measure your quality process.							
      </div>
    </div>
  </div>
  </div>
  <div class="card section8" style="box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;padding:15px 20px; margin-top:25px">

<h4 style="padding-bottom:10px;color:#6191d9">About Order Management </h4>	
  <div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_8" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_8" aria-expanded="false" aria-controls="collapsehowtouse_8" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">How does the order management work? <i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_8" class="collapse" aria-labelledby="headinghowtouse_8" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
     Every order received from end-customers is notified to Voomet Studio administrators as well as sellers. The sellers can review the order and either choose to fulfill or reject with a reason. If a seller chooses to fulfill they will have to accept the order in their own Admin panel. If they choose to reject, they can also do the same by submitting a reason for rejection.								
      </div>
    </div>
  </div>
  <div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_9" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_9" aria-expanded="false" aria-controls="collapsehowtouse_9" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">Who takes care of packaging & shipping?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_9" class="collapse" aria-labelledby="headinghowtouse_9" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
      The sellers directly are responsible for packaging and shipping their material. The sellers can either use their own shipping partners or if they do not have, then they can use the Voomet Studio shipping partners.								
      </div>
    </div>
  </div>
  <div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_10" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_10" aria-expanded="false" aria-controls="collapsehowtouse_10" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">What happens if the product is cancelled or notified as damaged by the end-customer?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_10" class="collapse" aria-labelledby="headinghowtouse_10" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
       <p>End-customers have a right to either cancel the order ask for refund or report the product as delivered damaged. Cancelling the order is permitted only if the seller has marked that particular product as 'cancellable' in their Admin panel. Upon cancellation by end-customer, the refund is initiated by Voomet Studio to the end-customer. </p>
<p>If an end-customer notifies an order as 'damaged on arrival' then the seller has to decided if they want to replace the products (at their own cost) or initiate a refund (bear the cost of inward transportation). Both choices are available for the sellers.</p>
							
      </div>
    </div>
  </div>
  <div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_11" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_11" aria-expanded="false" aria-controls="collapsehowtouse_11" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">How will I be notified of new orders?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_11" class="collapse" aria-labelledby="headinghowtouse_11" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
       You will receive email notifications for new orders. Additionally, you can view and manage orders through your Admin panel. Each order is also notified on WhatsApp if you have a valid WhatsApp number.									
      </div>
    </div>
  </div>
  <div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_12" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_12" aria-expanded="false" aria-controls="collapsehowtouse_12" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">Who handles customer service inquiries?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_12" class="collapse" aria-labelledby="headinghowtouse_12" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
     Primary customer service is handled by Voomet Studio team. The Voomet Studio team will co-ordinate with the sellers where required and needed. Each seller is required to co-operate with the Voomet Studio customer service team so that the customers query/complains can be resolved quickly.							
      </div>
    </div>
  </div>
  <div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_13" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_13" aria-expanded="false" aria-controls="collapsehowtouse_13" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">How can I improve my sales on Voomet Studio?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_13" class="collapse" aria-labelledby="headinghowtouse_13" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
     Voomet Studio has put together a whole army of people to help sellers improve their sales. You can reach out to the Voomet Studio sales team to get suggestions on various activities that can help improve your sales. </div>
    </div>
  </div>
  </div>
  
    <div class="card section8" style="box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;padding:15px 20px; margin-top:25px">

<h4 style="padding-bottom:10px;color:#6191d9">About Fee & Payment  </h4>	
  
  <div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_14" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_14" aria-expanded="false" aria-controls="collapsehowtouse_14" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">Is there a fee to register as a seller?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_14" class="collapse" aria-labelledby="headinghowtouse_14" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
     No. Registration as a seller is free for life.								
      </div>
    </div>
  </div>
  <div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_15" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_15" aria-expanded="false" aria-controls="collapsehowtouse_15" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">Is there a fee to list my products on Voomet Studio ecommerce?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_15" class="collapse" aria-labelledby="headinghowtouse_15" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
      Each seller can list up to 10 SKU’s free of cost. 								
      </div>
    </div>
  </div>
  <div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_16" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_16" aria-expanded="false" aria-controls="collapsehowtouse_16" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">What if I want to list more than 10 SKUs?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_16" class="collapse" aria-labelledby="headinghowtouse_16" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
      Voomet Studio has various schemes for the sellers who wish to list more than 10 SKUs. They can either choose a listing fee per SKU fee per month or a monthly fixed listing fee. Talk to Voomet Studio sales team to get to know the various options.  								
      </div>
    </div>
  </div>
  <div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_17" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_17" aria-expanded="false" aria-controls="collapsehowtouse_17" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">How and When do I get paid? <i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_17" class="collapse" aria-labelledby="headinghowtouse_17" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
     Each seller account is settled bi-monthly; this means once in 15 days. The seller can see their complete account statement of sales/cancellation/returns etc. in their Admin panel. All settlements are notified to the seller promptly.								
      </div>
    </div>
  </div>
  </div>
      <div class="card section8" style="box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;padding:15px 20px; margin-top:25px">

<h4 style="padding-bottom:10px;color:#6191d9">About Getting Started   </h4>	
  <div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_18" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_18" aria-expanded="false" aria-controls="collapsehowtouse_18" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">How do I register as a seller?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_18" class="collapse" aria-labelledby="headinghowtouse_18" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
 You will need the following information to register:
   <ul><li>Your business details.</li>
<li>- Your contact details - email and phone number.</li>
<li>- Basic information about your business.</li>
<li>- Tax Registration Details (PAN and GST). GST Details are mandatory if you are listing taxable goods and need to be provided at the time of registration.</li>
<li>- You can check the seller registration form in the link <a href="https://voometstudio.com/seller_registration">https://voometstudio.com/seller_registration</a></li>
</ul>							
      </div>
    </div>
  </div>
  <div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_19" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_19" aria-expanded="false" aria-controls="collapsehowtouse_19" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">Are there any guidelines for listing of products?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_19" class="collapse" aria-labelledby="headinghowtouse_19" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
Yes, product images should be high-resolution, clear, and professionally taken. It will work in your favor and increasing more sales. There is specific size of images to be uploaded in the Voomet Studio ecommerce. Those are mentioned in the admin panel where you start uploading your inventory/products. 						
      </div>
    </div>
  </div>
  <div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_20" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_20" aria-expanded="false" aria-controls="collapsehowtouse_20" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">Can I get help for capturing images and creating digital catalogs as per Voomet Studio guidelines?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_20" class="collapse" aria-labelledby="headinghowtouse_20" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
We have 3rd party providers who are trained on Voomet Studio imaging and cataloging guidelines and can assist you in creating high impact listings. They also have preferential rates and offers for Voomet Studio sellers. You can reach out to us in case you need such a service. 						
      </div>
    </div>
  </div>
  <div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_21" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_21" aria-expanded="false" aria-controls="collapsehowtouse_21" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">How long does it take to get approved as a seller?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_21" class="collapse" aria-labelledby="headinghowtouse_21" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
<p>The approval process typically takes 2-3 business days. You will be notified via email & phone call or WhatsApp once your registration is completed.</p>
<p>There are other product details required like description, attribute, unique selling points etc.</p>
<p>The more details you put, the easier you make for your customers to buy. </p>
 						
      </div>
    </div>
  </div>
  <div class="align_top product-desc editor_content mb-10">
    <div class="pl-10 pr-10 pt-10 pb-10" id="headinghowtouse_22" style="background-color:#fff;border:1px solid #ccc;cursor:pointer;" data-toggle="collapse" data-target="#collapsehowtouse_22" aria-expanded="false" aria-controls="collapsehowtouse_22" onclick="howtouseAccordian(this,'collapsehowtouse_icon')">
        <span class="text-left" style="width:100%;">Can I update or edit my product listings?<i class="fa fa-plus pull-right" id="collapsehowtouse_icon" style=""></i>
        </span>

    </div>

    <div id="collapsehowtouse_22" class="collapse" aria-labelledby="headinghowtouse_22" data-parent="#accordionhowtouse">
      <div class="" style="padding: 20px 10px 20px 10px;background-color:#fff;border:1px solid #ccc;">
<p>Yes, you can update or edit your product listings at any time through your Admin panel.</p>
 						
      </div>
    </div>
  </div>
  </div>
</div>		
			</div>
			<div class="col-md-6 ">
			<div class="well">
			<div class="row">
                <div class="col-md-12" id="createuser" >
                    <div class="box-authentication" id="NewUserController_div"  ng-controller="NewUserController" style="border:none;">
                        <h3 class="text-muted text-center">Seller Registration</h3>
					<form name="userForm" novalidate >
						<div class="form-group" show-errors>
							
							<input name="name" ng-model="formData.name" required placeholder="Name" type="text" class="form-control">
							<p class="help-block" ng-if="userForm.name.$error.required">The user's name is required</p>
						</div>
						
						
						<div class="form-group" show-errors >
						
                          <input type="email" class="form-control" name="email" ng-model="formData.email" required placeholder="Email" email-exist-vendor="formData.email" id="email" />
				  
							<p class="help-block" ng-if="userForm.email.$error.emailexistValidatorVendor">This Email is  registered already.</p>
				  
							<p class="help-block" ng-if="userForm.email.$error.required">The user's email is required</p>
							<p class="help-block" ng-if="userForm.email.$error.email">The email address is invalid</p>				
	 
						</div>
						
						
						<div class="form-group" show-errors>
							
							<input id="mobile_number" type="number"  ng-minlength="10" 
                   ng-maxlength="10" min="0" onpaste="return false;" name="mobile" class="form-control" ng-model="formData.mobile" required allow-only-numbers mobile-exist-vendor="formData.mobile" placeholder="Mobile No" maxlength="10" onkeypress="return isNumber(event)">
				   
				   <p class="help-block" ng-show="(userForm.mobile.$error.required || userForm.mobile.$error.number) || (userForm.mobile.$invalid && !userForm.mobile.$pristine)">Valid mobile number is required
					</p>
                                        
                                        <p class="help-block" ng-show="userForm.mobile.$error.mobileexistValidatorVendor">This mobile is registered already.</p>
				
					<p class="help-block" ng-show="((userForm.mobile.$error.minlength ||
								   userForm.mobile.$error.maxlength) && 
								   userForm.mobile.$dirty) ">
								   mobile number should be 10 digits
					</p>
							
						</div>

            <div class="form-group" show-errors>
							
							<input id="whatsapp_number" type="number"  ng-minlength="10" 
                   ng-maxlength="10" min="0" onpaste="return false;" name="whatsapp_number" class="form-control" ng-model="formData.whatsapp_number" required allow-only-numbers placeholder="Whatsapp Number" maxlength="10" onkeypress="return isNumber(event)">
				   
          <p class="help-block" ng-show="(userForm.whatsapp_number.$error.required || userForm.whatsapp_number.$error.number) || (userForm.whatsapp_number.$invalid && !userForm.whatsapp_number.$pristine)">Valid mobile number is required
					</p>
                                
					<p class="help-block" ng-show="((userForm.whatsapp_number.$error.minlength ||
								   userForm.whatsapp_number.$error.maxlength) && 
								   userForm.whatsapp_number.$dirty) ">
								   mobile number should be 10 digits
					</p>
							

                   <p>
                   
        <div class="checkbox">
          <label>
            <input type="checkbox" ng-change="copy_mobile_number()" value="1" ng-checked="use_number"  name="use_mobile_number" ng-model="formData.use_mobile_number">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            Use mobile number for Whatsapp
          </label>
        </div></p>

				   
          
						</div>

						
						<div class="form-group" show-errors>
						
                        <input type="password" name="password" ng-model="formData.password" required class="form-control" ng-minlength="6" ng-maxlength="20"  placeholder="Password" ng-pattern="regex" >
						<p class="help-block" ng-if="userForm.password.$error.required">The password is required</p>
												
		<!--<p ng-show="userForm.password.$error.minlength" class="help-block">
          Passwords must be between 6 and 10 characters.</p>-->
		   <p ng-show="!(userForm.password.$valid)" class="help-block">
          1) Must contain one lower & uppercase letter <br> 2) One non-alpha character (a number or a symbol.) <br> 3) Min six characters to max twenty characters</p>
		  
		  
						</div>
						
    <div class="form-group" show-errors ng-class="{'has-error':formData.password_c.$invalid && !formData.password_c.$pristine}">
        
        <input type="password" id="password_c" name="password_c" ng-model="formData.password_c" valid-password-c="formData.password" required class="form-control" placeholder="Confirm Password"/>
		
        <p ng-if="userForm.password_c.$error.noMatch && userForm.password_c.$error.required==false" class="help-block">Passwords do not match.</p>
          <p ng-if="userForm.password_c.$error.required" class="help-block">Confirm Password Required</p>
		
    </div>
                                            
                  <!--- new fields--->  
                                                <div class="form-group" show-errors>
							
							<input name="address1" ng-model="formData.address1" placeholder="Address line 1" type="text" class="form-control">
							<p class="help-block" ng-if="userForm.address1.$error.required">Address line 1 is required</p>
						</div>
                                                <div class="form-group" show-errors>
							
							<input name="address2" ng-model="formData.address2" placeholder="Address line 2" type="text" class="form-control">
							<p class="help-block" ng-if="userForm.address2.$error.required">Address line 2 is required</p>
						</div>
            <!-- <div class="form-group" show-errors>
							
							<input name="pincode" ng-model="formData.pincode" required placeholder="Pincode" type="text" class="form-control">
							<p class="help-block" ng-if="userForm.pincode.$error.required">Pincode is required</p>
						</div> -->

            <!---- pincode  ---->

            <div class="form-group" show-errors id="pin_code_check">
								
								<input type="text" ng-minlength="6"
								   ng-maxlength="6" name="pin" class="form-control" ng-model="formData.pincode" allow-only-numbers  placeholder='PIN/ZIP Code'  onkeypress="return isNumber(event)"  ng-paste="$event.preventDefault()" ng-pattern="/^[0-9]*$/" oninput="if(value.length>6)value=value.slice(0,6)" ng-keyup="get_city_state_details_by_pincodeFun()" required>
								   
								   <p class="help-block" ng-show="userForm.pin.$error.required || userForm.pin.$error.number">Valid pin code is required
									</p>
								
									<p class="help-block" ng-show="((userForm.pin.$error.minlength ||
                                    userForm.pin.$error.maxlength) && 
                                    userForm.pin.$dirty) ">
												   Pincode should be 6 digits
									</p>
                                    <span class="delivery_address_pin_error" style="display:none;color:#a94442">
                                        Please enter valid pincode
                                    </span>
											
							</div>
							
							
							<div class="form-group"  show-errors>
								
								<input  name="city" ng-model="formData.city"  placeholder="City" type="text" class="form-control" readonly>
								<p class="help-block" ng-if="userForm.city.$error.required">The City is required</p>
							</div>
						
							<div class="form-group"  show-errors>
							<input  name="state" ng-model="formData.state" placeholder="State/Province" type="text" class="form-control" readonly>
								
								
								<p class="help-block" ng-if="userForm.state.$error.required">The State is required</p>
							</div>
														
							
							<div class="form-group"  show-errors>
								<input  name="country" ng-model="formData.country" placeholder="Country" type="text" class="form-control" readonly>
								<p class="help-block" ng-if="userForm.country.$error.required">The Country is required</p>
							</div>
						
<!--- address of parter--->


                                <!---- pincode  ---->
								
								
								
                                <!--- new fields ---->
	  <p class="help-block mt-2 mand">Questions marked with * are mandatory</p>
      <div class="form-group" show-errors>
        <label>1) Tell us about your furniture-operations <span class="mand">*</span></label>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="manufacturer" name="about_furniture_operations[]" ng-model="formData.about_furniture_operations.manufacturer" ng-change="about_furniture_operations_manufacturer=formData.about_furniture_operations.manufacturer">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            We are a manufacturer
          </label>
        </div>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="importer" name="about_furniture_operations[]" ng-model="formData.about_furniture_operations.importer" ng-change="about_furniture_operations_importer=formData.about_furniture_operations.importer">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            We are a importer
          </label>
          </label>
        </div>
        <div class="checkbox ">
          <label>
            <input type="checkbox" value="traders" name="about_furniture_operations[]" ng-model="formData.about_furniture_operations.trader" ng-change="about_furniture_operations_trader=formData.about_furniture_operations.trader">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            We are furniture traders
          </label>
        </div>
    </div>

    <!--- -->
	
	
    <div class="form-group" show-errors>
        <label>2) Select what is applicable please (skip what is not applicable )
</label>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="website" ng-checked="true" name="applicable_media[]" ng-model="formData.applicable_media.website" ng-change="hide_show_checkbox('website');applicable_media_website=formData.applicable_media.website">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            Our website URL is 

           

          </label>
          <div id="website_div">
          <!-- ng-required="formData.applicable_media.website == true"  -->
            <input type="text"  class="form-control"  value="" name="website_url" ng-model="formData.applicable_media.website_url">
            <p class="help-block" ng-if="userForm.applicable_media.website_url.$error.required">The website url is required</p>
</div>
        </div>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="instagram" name="applicable_media[]" ng-model="formData.applicable_media.instagram" ng-change="hide_show_checkbox('instagram');applicable_media_instagram=formData.applicable_media.instagram">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            Our Instagram handle is
           
          </label>
          <div id="instagram_div" style="display:none;">
          <!-- ng-required="formData.applicable_media.instagram == true"  -->
            <input type="text"  class="form-control" value="" name="instagram_url" ng-model="formData.applicable_media.instagram_url">
            <p class="help-block" ng-if="userForm.applicable_media.instagram_url.$error.required">The instagram url is required</p>
          </div>
        </div>
        <div class="checkbox">
          <label>
            <input type="checkbox"  value="facebook" name="applicable_media[]" ng-model="formData.applicable_media.facebook" ng-change="hide_show_checkbox('facebook');applicable_media_facebook=formData.applicable_media.facebook">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            Our Facebook handle is
            
          </label>
          <div id="facebook_div" style="display:none;">
          <!-- ng-required="formData.applicable_media.facebook == true"  -->
            <input type="text"  class="form-control" value="" name="facebook_url" ng-model="formData.applicable_media.facebook_url">
            <p class="help-block" ng-if="userForm.applicable_media.facebook_url.$error.required">The facebook url is required</p>
</div>
        </div>
    </div>

    <!----->
	
	
	
    <div class="form-group" show-errors>
    <label>3) What is the size of your team? <span class="mand">*</span></label>
        <div class="radio">
          <label>
            <input type="radio" name="size_of_team" value="<5" ng-model="formData.size_of_team" ng-change="size_of_team=formData.size_of_team">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
            < 5 People
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="size_of_team" value=">5<25" ng-model="formData.size_of_team" ng-change="size_of_team=formData.size_of_team">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
            > 5 Less than 25 people

          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="size_of_team" value=">25" ng-model="formData.size_of_team" ng-change="size_of_team=formData.size_of_team">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
            > 25 People

          </label>
        </div>
    </div>

    <!----->

    
    <div class="form-group" show-errors>
        <label>4) What kind of furniture you deal in ? <span class="mand">*</span>
</label>
       

        <div class="checkbox ">
          <label>
            <input type="checkbox" value="lowest_cost" name="kind_of_furniture[lowest_cost]" ng-model="formData.kind_of_furniture.lowest_cost" ng-change="kind_of_furniture_lowest_cost=formData.kind_of_furniture.lowest_cost">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            Budget Friendly
            
          </label>
        </div>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="economical" name="kind_of_furniture[economical]" ng-model="formData.kind_of_furniture.economical" ng-change="kind_of_furniture_economical=formData.kind_of_furniture.economical">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            Economical
          </label>
        </div>
        
        <div class="checkbox ">
          <label>
            <input type="checkbox" value="Luxury" name="kind_of_furniture[luxury]" ng-model="formData.kind_of_furniture.luxury" ng-change="kind_of_furniture_luxury=formData.kind_of_furniture.luxury">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            Luxury
            
          </label>
        </div>
        <div class="checkbox ">
          <label>
            <input type="checkbox" value="super_Luxury" name="kind_of_furniture[super_luxury]" ng-model="formData.kind_of_furniture.super_luxury" ng-change="kind_of_furniture_super_luxury=formData.kind_of_furniture.super_luxury">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            Super Luxury
            
          </label>
        </div>
    </div>
    <!----->
	
    <div class="form-group" show-errors>
    <label>5) How many years have you been in furniture business? <span class="mand">*</span>
</label>
        <div class="radio">
          <label>
            <input type="radio" name="years_of_business" value="<2" ng-model="formData.years_of_business" ng-change="years_of_business=formData.years_of_business">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
            < 2 Years
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="years_of_business" value=">2<5" ng-model="formData.years_of_business" ng-change="years_of_business=formData.years_of_business">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
            > 2 Less than 5 years

          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="years_of_business" value=">5" ng-model="formData.years_of_business" ng-change="years_of_business=formData.years_of_business">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
            > 5 Years

          </label>
        </div>
    </div>

    <!----->
	
    <div class="form-group" show-errors>
    <label>6) Do you have shipping capabilities from your office/warehouse throughout India ? <span class="mand">*</span>
</label>
        <div class="radio">
          <label>
            <input type="radio" name="shipping_capabilities" value="yes" ng-model="formData.shipping_capabilities" ng-change="hide_show(formData.shipping_capabilities,'shipping_capabilities_description_div');shipping_capabilities=formData.shipping_capabilities">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
            Yes

           
          </label>

         
          <div id="shipping_capabilities_description_div" style="display:none;">  
          <label>Tell us more about it. Who is your shipping & logistics partner and what is your delivery timeline ?
          </label>
          <!-- ng-required="formData.shipping_capabilities == yes"  -->
          <textarea   class="form-control" name="shipping_capabilities_description" Placeholder="About shipping" ng-model="formData.shipping_capabilities_description"></textarea>
          <p class="help-block" ng-if="userForm.shipping_capabilities.$error.required">Shipping Capabilities Description is required</p>
          
          </div>


        </div>
        <div class="radio">
          <label>
            <input type="radio" name="shipping_capabilities" value="no" ng-model="formData.shipping_capabilities" ng-change="hide_show(formData.shipping_capabilities,'shipping_capabilities_description_div');shipping_capabilities=formData.shipping_capabilities">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
            No

          </label>
        </div>
        
    </div>
                                <!--- new fields ---->
								
                  <!--- new fields--->                          
                  
		<div class="form-group">
		
		<button class="btn btn-sm btn-block preventDflt" ng-click="save($event);" ng-disabled="(about_furniture_operations_manufacturer==false && about_furniture_operations_importer==false && about_furniture_operations_trader==false) || (size_of_team=='') || (kind_of_furniture_lowest_cost==false && kind_of_furniture_economical==false && kind_of_furniture_luxury==false && kind_of_furniture_super_luxury==false) || (years_of_business=='') || (shipping_capabilities=='') || userForm.$invalid" style="background-color:#f5540e;color:#fff;" 
			id="reg_btn"><i class="fa fa-user"></i> Create</button>
		<button class="btn btn-sm btn-block preventDflt" ng-click="reset()" style="background-color:#1e1c10;color:#fff;">Reset</button>
		
		<!--
		
		<button class="btn btn-sm btn-block preventDflt" ng-click="save($event);" ng-disabled="userForm.$invalid"  ng-style="{ (userForm.$invalid) ? 'cursor:not-allowed;background-color:#f5540e;color:#fff;' : 'cursor:pointer;background-color:#f5540e;color:#fff;' }"
			id="reg_btn"><i class="fa fa-user"></i> Create</button>
			
			-->
		
		</div>
		
	
					</form>
					
                    </div>
                </div>
           
		
			
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->
