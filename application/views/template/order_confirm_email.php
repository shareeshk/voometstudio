<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php echo SITE_NAME; ?> Email Notification</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta  name="viewport" content="width=display-width, initial-scale=1.0, maximum-scale=1.0," />
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
		
		<style type="text/css">		
			html { width: 100%; }
			body {margin:0; padding:0; width:100%; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
			img { display: block !important; border:0; -ms-interpolation-mode:bicubic;}

			.ReadMsgBody { width: 100%;}
			.ExternalClass {width: 100%;}
			.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
			.images {display:block !important; width:100% !important;}
				
			.MsoNormal {font-family: Arial, Helvetica Neue, Helvetica, sans-serif !important;}
			p {margin:0 !important; padding:0 !important;}					
			.display-button td, .display-button a  {font-family: Arial, Helvetica Neue, Helvetica, sans-serif !important;}
			.display-button a:hover {text-decoration:none !important;}
			.vertchartbar{ box-shadow: 5px 0px 6px 0px #888888;}
			
			/* MEDIA QUIRES */
			 @media only screen and (min-width:799px)
            {
				.saf-table {display:table !important;}
				.width-auto {width: auto !important;}
				.width600 {width:600px;}	
                .width800 {width:800px !important; max-width:800px !important;}
				.width50percent {width:50% !important; max-width:50% !important;}
			}
			
			 @media only screen and (max-width:799px)
            {
                 body {width:auto !important;}
				.display-width {width:100% !important;}	
				.res-padding {padding:0 20px !important;}	
				.display-width-inner {width:600px !important;}
				.res-center {text-align:center !important; width:100% !important; }
				.width800 {width:100% !important; max-width:100% !important;}
				.width50percent {width:50% !important; max-width:50% !important;}
		   }	
			
			@media only screen and (max-width:639px)
			{
				.display-width-inner, .display-width-child {width:100% !important;}
				.hide-bar {display:none !important;}
				td[class="height-hidden"] {display:none !important;}
				span.unsub-width {width:100% !important;
				display:block !important;}
				span.txt-copyright{ padding-bottom:10px !important;}
				.txt-center {text-align:center !important;}
				.image-center{margin:0 auto !important; display:table !important;}
				.butn-center{margin:0 auto; display:table;}
				.butn-left{margin:0 auto 0 0; display:table;}
				.footer-width {width:170px !important;}
				.feature-width {width:260px !important;}
				.height30 {height:30px !important; line-height:30px !important;}
				.width292 {width:292px !important;}
				.width282 {width:282px !important;}
				.width272 {max-width:272px !important;}
				.width290 {max-width:290px !important;}
				.div-width {display: block !important; width: 100% !important; max-width: 100% !important;}
				.saf-table {display:block !important;}
				.width50percent {width:100% !important; max-width:100% !important;}
			}
			
			@media only screen and (max-width:480px) 
			{
			   .button-width .display-button {width:auto !important;}
			   .div-width {display: block !important; width: 100% !important; max-width: 100% !important;}	
			   .display-width-child .footer-width {width:170px !important;}
			   .display-width-child .feature-width {width:260px !important;}
			   .display-width .butn-header {max-width:260px !important;}
			   .width292 {width:292px !important;}
			   .width282 {width:282px !important;}
			   .display-width .width272 {width:272px !important;}
			   .display-width .width290 {width:290px !important;}
			
			}	
			
			@media only screen and (max-width:380px)
			{
				.display-width-child .width272 { width:100% !important; max-width:100% !important;}
				.display-width-child .width290 { width:100% !important; max-width:100% !important;}
				.display-width-child .width282 { width:100% !important;}
			    .display-width-child .width292 { width:100% !important;}
				
			}
			
			@media only screen and (max-width:360px)
			{
				.header1-font {font-size: 23px !important;}
				.display-width-child .width272 { width:100% !important; max-width:100% !important;}	
				.display-width-child .width290 { width:100% !important; max-width:100% !important;}
				
			}
			
			
			@media only screen and (max-width:320px)
			{
				.header1-font {font-size: 21px !important;}
				.display-width-child .width290 { width:100% !important; max-width:100% !important;}
				.display-width-child .feature-width { width:100% !important; max-width:100% !important;}
			}

			
			
		</style>
			
	</head>
	<body>
		<!--[if (gte mso 9)|(IE)]>
			<style >
				.Heading {font-family: Arial, Helvetica Neue, Helvetica, sans-serif !important;}
				.MsoNormal {font-family: Arial, Helvetica Neue, Helvetica, sans-serif !important;}
				.display-button td, .display-button a, a {font-family: Arial, Helvetica Neue, Helvetica, sans-serif !important;}			
			</style>
		<![endif]-->
			<!-- VIEW IN BROWSER STARTS -->
			<table align="center" bgcolor="#333333" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td align="center">
							<!--[if (gte mso 9)|(IE)]>
								<table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="800" style="width: 800px;">
									<tr>
										<td align="center" valign="top" width="100%" style="max-width:800px;">
											<![endif]-->
												<div style="display:inline-block; width:100%; max-width:800px; vertical-align:top;" class="width800">
													<!-- ID:BG VIEW OPTIONAL -->
													<table align="center" bgcolor="#f6f6f6" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%" style="max-width:800px;">
														<tbody>	
															<tr>
																<td align="center" class="res-padding">
																	<!--[if (gte mso 9)|(IE)]>
																	<table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="600" style="width:600px;">
																		<tr>
																			<td align="center">
																					<![endif]-->
																					<div style="display:inline-block; width:100%; max-width:600px; vertical-align:top;" class="width600">
																						<table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width-inner" width="100%" style="max-width:600px;">
																							<tbody>
																								<tr>
																									<td height="4" style="mso-line-height-rule:exactly; line-height:4px; font-size:0;">&nbsp;</td>
																								</tr>
																								<tr>
																									<!-- ID:TXT VIEW BROWSER -->
																									<td align="right" class="MsoNormal" style="padding:0 10px; color:#666666; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-size:12px; font-weight:400; letter-spacing:0px;">
																										<a href="https://axonlabs.ai/business/brandedstores_cronjob_daily_status_companyzebu_fin/view_browser/8964a8bfda92868703c309f35a6e438c" style="color:#666666; text-decoration:none;">
																											
																										</a>
																									</td>
																								</tr>
																								<tr>
																									<td height="4" style="mso-line-height-rule:exactly; line-height:4px; font-size:0;">&nbsp;</td>
																								</tr>	
																							</tbody>	
																						</table>
																					</div>
																					<!--[if (gte mso 9)|(IE)]>
																				</td>
																			</tr>
																		</table>
																	<![endif]-->
																</td>
															</tr>
														</tbody>	
													</table>
												</div>
											<!--[if (gte mso 9)|(IE)]>
										</td>
									</tr>
								</table>
							<![endif]-->
						</td>
					</tr>					
				</tbody>	
			</table>
			<!-- VIEW IN BROWSER ENDS -->
			
			<!-- MENU STARTS -->
			<table align="center" bgcolor="#333333" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td align="center">
							<!--[if (gte mso 9)|(IE)]>
								<table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="800" style="width: 800px;">
									<tr>
										<td align="center" valign="top" width="100%" style="max-width:800px;">
											<![endif]-->
												<div style="display:inline-block; width:100%; max-width:800px; vertical-align:top;" class="width800">
													<!-- ID:BG MENU OPTIONAL -->
													<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%" style="max-width:800px;">
														<tbody>	
															<tr>
																<td align="center" class="res-padding">
																	<!--[if (gte mso 9)|(IE)]>
																	<table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="600" style="width:600px;">
																		<tr>
																			<td align="center" valign="top" width="600">
																				<![endif]-->
																				<div style="display:inline-block; width:100%; max-width:600px; vertical-align:top;" class="width600">
																					<table align="center" border="0" class="display-width-inner" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;"> 
																						<tr>
																							<td height="15" class="height30" style="mso-line-height-rule:exactly; line-height:15px; font-size:0;">&nbsp;</td>
																						</tr>
																						<tr>
																							<td align="center"  style="width:100%; max-width:100%; font-size:0;">
																								<!--[if (gte mso 9)|(IE)]>
																								<table  aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="100%" style="width:100%;">
																									<tr>
																										<td align="center" valign="top" width="150">
																											<![endif]-->
																											<div style="display:inline-block; max-width:150px; width:100%; vertical-align:top; " class="div-width">
																												<!--TABLE LEFT-->
																												<table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%; max-width:100%;">
																													<tr>
																														<td align="center">
																															<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:auto !important;">
																																<tr>
																																	<!-- ID:TXT MENU -->
																																	<td align="center" style="color:#666666;">
																																		<a href="#" style="color:#666666; text-decoration:none;"><img src="<?php echo base_url()?>assets/data/option8/logo.png" alt="paperonelogo" width="175" height="64" style="margin:0; border:0; padding:0; display:block;"/></a>
																																	</td>
																																</tr>
																															</table>
																														</td>
																													</tr>
																												</table>
																											</div>
																											<!--[if (gte mso 9)|(IE)]>
																										</td>
																										<td align="center" valign="top" width="440">
																										<![endif]-->
																											
																											<!--[if (gte mso 9)|(IE)]>
																										</td>
																									</tr>
																								</table>
																								<![endif]-->
																							</td>
																						</tr>
																						<tr>
																							<td height="15" class="height30" style="mso-line-height-rule:exactly; line-height:15px; font-size:0;">&nbsp;</td>
																						</tr>
																					</table>
																				</div>
																				<!--[if (gte mso 9)|(IE)]>
																			</td>
																		</tr>
																	</table>
																	<![endif]-->
																</td>
															</tr>
														</tbody>	
													</table>
												</div>
											<!--[if (gte mso 9)|(IE)]>
										</td>
									</tr>
								</table>
							<![endif]-->
						</td>
					</tr>					
				</tbody>	
			</table>
			<!-- MENU ENDS -->
			
			
			<table align="center" bgcolor="#333333" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td align="center">
							<!--[if (gte mso 9)|(IE)]>
							<table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="800" style="width: 800px;">
								<tr>
									<td align="center" valign="top" width="800">
										<![endif]-->
										<div style="display:inline-block; width:100%; max-width:800px; vertical-align:top;" class="width800">
											<!-- ID:BG CTA OPTIONAL -->
											<table align="center" bgcolor="#fafafa" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%" style="max-width:800px;">
												<tr>
													<td align="center">
														<div style="margin:auto;">
															<table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%">
																<tr>
																	<td align="center" class="res-padding" style="font-size:0;">
																		<!--[if (gte mso 9)|(IE)]>
																		<table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="600" style="width: 600px;">
																			<tr>
																				<td align="center" valign="top" width="600">
																					<![endif]-->
																					<div style="display:inline-block;width:100%; max-width:600px; vertical-align:top;" class="width600">
																						<table align="center" border="0" class="display-width-inner" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
																							<tr>
																								<td height="10" style="mso-line-height-rule:exactly; line-height:10px;">&nbsp;</td>
																							</tr>
																							<tr>
																								<td height="2" style="mso-line-height-rule: exactly; line-height:2px;">&nbsp;</td>
																							</tr>
																							<tr>
																								<td align="center">
																									<table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" >
																										<tr>
																											<!-- ID:TXT CTA HEADING -->
																											<td align="center" class="MsoNormal" style="color:#000; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-weight:700;  font-size:30px; line-height:40px; letter-spacing:0px; mso-line-height-rule: exactly;">Order Successful!
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<td height="5" style="mso-line-height-rule:exactly; line-height:5px;">&nbsp;</td>
																							</tr>
																							
																							<tr>
																							<table align="center" bgcolor="#fafafa" height="150px">
																						<tr>

<td width="100%" align="justify" class="MsoNormal" style="vertical-align:top; color:#000; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-size:15px; line-height:20px; letter-spacing:0px; mso-line-height-rule: exactly;">

Hello <b><?php echo ucfirst($customer_name); ?></b>,
<br><br>
Thank you for ordering via <a target="_new" href="<?php echo base_url(); ?>" style="color:#f6a900;text-decoration:none;"><?php echo SITE_ADDR; ?></a>
<br><br>
You will receive another  email notification once your order is processed. The status of your order will also be displayed in your respective account.
<br><br>
</td>
</tr>



<tr>


<td width="100%" align="justify" class="MsoNormal" style="vertical-align:top; color:#000; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-size:15px; line-height:15px; letter-spacing:0px; mso-line-height-rule: exactly;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%" bgcolor="#f6a900" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:95%; max-width:95%;">

<tr>
<td align="center" style="line-height:30px;color:#001e62">Your <b>order ID <?php echo $order_id; ?></b> dated <?php echo $order_placed_date_format; ?>

<?php //echo $order_track; ?>
    
</td>
</tr>
</table>
    <br><br>
 <?php echo $new_order_details; ?>
 <?php //echo $invoice_offer; ?>
<!--<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:95%; max-width:95%;">

<tr>
<td height="25" style="mso-line-height-rule:exactly; line-height:5px;">&nbsp;</td>
</tr>

<tr>
<td style="font-weight:700">Vendor</td>
<td style="font-weight:700">Product Ordered</td>
<td style="font-weight:700">Qty/Units</td>

</tr>

<tr>
<td height="25" style="mso-line-height-rule:exactly; line-height:5px;">&nbsp;</td>
</tr>

<tr>
<td>Navkar Paper Impex</td>
<td>P1OF<br>
GSM : 55 GSM<br>
Size : 635(W)X 910(L) mm<br>
SKU : PAF55OS04
</td>
<td>10 Reams</td>

</tr>

<tr>
<td height="25" style="mso-line-height-rule:exactly; line-height:5px;">&nbsp;</td>
</tr>
</table>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%" bgcolor="#f6a900" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:95%; max-width:95%;">

<tr>
<td align="center" style="line-height:30px;color:#001e62">Delivery scheduled by <b>Fri 17 Jun</b>, 2022</td>
</tr>
</table>-->


</td>
</tr>

<tr>
<td height="25" style="mso-line-height-rule:exactly; line-height:5px;">&nbsp;</td>
</tr>

<tr>

<td width="100%" align="justify" class="MsoNormal" style="vertical-align:top; color:#000; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-size:15px; line-height:15px; letter-spacing:0px; mso-line-height-rule: exactly;">
<b>Order will be shipped to,</b><br><br>
<!--Nanthini<br>
Mob : 6384770034<br>
No: 507, Silver Oak, Block 7,<br>
cvcv,<br>
Hyderabad, 500001<br>
Telangana, India<br>-->

<?php echo $shipping_address; ?>

<br><br>
Team,<br>
<b><?php echo SITE_ADDR; ?></b>



</td>

																							</tr>
																							<tr>
																								<td height="25" style="mso-line-height-rule:exactly; line-height:5px;">&nbsp;</td>
																							</tr>
																							
																							
																							</table>
																							
																							
																							
																							
																							</tr>
																						</table>
																					</div>
																					<!--[if (gte mso 9)|(IE)]>
																				</td>
																			</tr>
																		</table>
																		<![endif]-->
																	</td>
																</tr>
															</table>
														</div>
														<!--[if gte mso 9]> </v:textbox> </v:rect> <![endif]-->	
													</td>
												</tr>
											</table>
										</div>
										<!--[if (gte mso 9)|(IE)]>
									</td>
								</tr>
							</table>
							<![endif]-->
						</td>
					</tr>
				</tbody>
			</table>	
			
		
			
			<table align="center" bgcolor="#333333" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td align="center">
							<!--[if (gte mso 9)|(IE)]>
								<table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="800" style="width: 800px;">
									<tr>
										<td align="center" valign="top" width="100%" style="max-width:800px;">
											<![endif]-->
												<div style="display:inline-block; width:100%; max-width:800px; vertical-align:top;" class="width800">
													<!-- ID:BG SECTION-2 -->
													<table align="center" bgcolor="#f6f6f6" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%" style="max-width:800px;">
														<tbody>	
															<tr>
																<td align="center" class="res-padding">
																	<!--[if (gte mso 9)|(IE)]>
																	<table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="600" style="width:600px;">
																		<tr>
																			<td align="center">
																					<![endif]-->
																						<div style="display:inline-block; width:100%; max-width:600px; vertical-align:top;" class="width600">
																							<table align="center" border="0" class="display-width-inner" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
																								<tr>
																									<td height="10" style="mso-line-height-rule:exactly; line-height:10px;">&nbsp;</td>
																								</tr>
																								
																								<tr>
																									<td height="2" style="font-size:0; mso-line-height-rule:exactly; line-height:2px;">&nbsp;</td>
																								</tr>
																								<tr>
																									<td align="center">
																										<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:95%; max-width:95%;">
																											<tr>
																												<!-- ID:TXT CONTENT -->
																												<td align="left" class="MsoNormal txt-center" style="padding:10px;color:#666666; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-size:14px; line-height:24px;border:1px solid #e1e1e1">
																													<b>What Next?</b> <br>You will receive an email with your courier Tracking ID.
																												</td>
																											</tr>
																											<tr>
																												<td height="15" style="font-size:0; mso-line-height-rule:exactly; line-height:15px;">&nbsp;</td>
																											</tr>
																										
																										</table>
																									</td>
																									
																									<td align="center">
																										<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:95%; max-width:95%;">
																											<tr>
																												<!-- ID:TXT CONTENT -->
																												<td align="left" class="MsoNormal txt-center" style="padding:10px;color:#666666; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-size:14px; line-height:24px; border:1px solid #e1e1e1">
																													<b>Any Questions?</b> <br>Get in touch with our customer care team at <?php echo SITE_EMAIL; ?>
																												</td>
																											</tr>
																											<tr>
																												<td height="15" style="font-size:0; mso-line-height-rule:exactly; line-height:15px;">&nbsp;</td>
																											</tr>
																										
																										</table>
																									</td>
																									
																									
																								</tr>
																								<tr>
																									<td height="2" style="font-size:0; mso-line-height-rule:exactly; line-height:2px;">&nbsp;</td>
																								</tr>
																								
																								
																								<tr>
																									<td height="10" style="mso-line-height-rule:exactly; line-height:10px;">&nbsp;</td>
																								</tr>
																							</table>
																						</div>
																					<!--[if (gte mso 9)|(IE)]>
																				</td>
																			</tr>
																		</table>
																	<![endif]-->
																</td>
															</tr>
														</tbody>	
													</table>
												</div>
											<!--[if (gte mso 9)|(IE)]>
										</td>
									</tr>
								</table>
							<![endif]-->
						</td>
					</tr>					
				</tbody>	
			</table>
			<!-- Zebu TRADING TRICKS ENDS -->
			
			<!-- FOOTER-1 STARTS -->
			<table align="center" bgcolor="#333333" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td align="center">
							<!--[if (gte mso 9)|(IE)]>
								<table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="800" style="width: 800px;">
									<tr>
										<td align="center" valign="top" width="100%" style="max-width:800px;">
											<![endif]-->
												<div style="display:inline-block; width:100%; max-width:800px; vertical-align:top;" class="width800">
													<!-- ID:BG FOOTER-1 OPTIONAL -->
													<table align="center" bgcolor="#001e62" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%" style="max-width:800px;">
														<tbody>	
															<tr>
																<td align="center" class="res-padding">
																	<!--[if (gte mso 9)|(IE)]>
																	<table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="600" style="width:600px;">
																		<tr>
																			<td align="center">
																					<![endif]-->
																						<div style="display:inline-block; width:100%; max-width:600px; vertical-align:top;" class="width600">
																							<table align="center" border="0" class="display-width-inner" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
																								<tr>
																									<td height="60" style="mso-line-height-rule:exactly; line-height:60px;">&nbsp;</td>
																								</tr>
																								<tr>		
																									<td align="center">
																										<table align="center" border="0" cellspacing="0" cellpadding="0" width="40%" style="width:auto !important;">
																											<tr>
<!-- ID:TXT FOOTER-1 HEADING -->
																												<td align="left" valign="middle" width="32">
																													<a  target="_new" href="https://www.facebook.com/" style="color:#f6a900;text-decoration:none;">
<!--																														 <i class="fab fa-facebook-f"></i>-->
                                                                                                                                                                <img style="border: 0;" src="<?php echo base_url(); ?>assets/images/underconstruction/facebook.png" alt="Facebook" height="30" width="30" cm_dontimportimage="" />
																													</a>
																												</td>
																												
																												<td width="20"></td>
																												<td align="left" valign="middle" width="32">
																													<a target="_new" href="https://www.instagram.com/" style="color:#f6a900;text-decoration:none;">
<!--																														 <i class="fab fa-instagram"></i>-->
                                                                                                                                                                <img style="border: 0;" src="<?php echo base_url(); ?>assets/images/underconstruction/instagram.png" alt="Instagram" height="30" width="30" cm_dontimportimage="" />
																													</a>
																												</td>
																												<td width="20"></td>
																												<td align="left" valign="middle" width="32">
																													<a target="_new" href="https://www.linkedin.com/company/" style="color:#f6a900;text-decoration:none;">
<!--																													<i class="fab fa-linkedin"></i>-->
                                                                                                                                                                <img style="border: 0;" src="<?php echo base_url(); ?>assets/images/underconstruction/linkedin.png" alt="LinkedIn" height="30" width="30" cm_dontimportimage="" />
																													</a>
																												</td>
																												<td width="20"></td>
																												<td align="left" valign="middle" width="32">
																													<a target="_new" href="https://www.youtube.com/channel/UC7qnnEMc8foQzi67zUxGpXg" style="color:#f6a900;text-decoration:none;">
<!--																													  <i class="fab fa-youtube"></i>-->
                                                                                                                                                                                                        <img style="border: 0;" src="<?php echo base_url(); ?>assets/images/underconstruction/youtube.ico" alt="Youtube" height="30" width="30" cm_dontimportimage="" />
																													</a>
																												</td>
																											</tr>
																										</table>
																									</td>
																								</tr>
																								<tr>
																									<!-- ID:BR FOOTER-1 BORDER -->
																									<td height="30" style="border-bottom:1px solid #cccccc; mso-line-height-rule: exactly; line-height: 30px;"></td>
																								</tr>
																								<tr>
																									<td height="15" style="mso-line-height-rule: exactly; line-height: 15px;"></td>
																								</tr>
																								<tr>
																									<td>
																										<table align="center" border="0" class="display-width" cellpadding="0" cellspacing="0" width="100%">
																											<tr>
																												<td align="center" style="font-size:0;">
																													<!--[if (gte mso 9)|(IE)]>
																													<table  aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="100%" style="width:100%;">
																														<tr>
																															<td align="center" valign="top" width="200">
																																<![endif]-->
																																<div style="display:inline-block; max-width:600px; vertical-align:top; width:100%;" class="div-width">
																																	<!-- TABLE LEFT -->	
																																	<table align="left" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; max-width:100%; width:100%;">
																																		<tr>
																																			<td align="center" style="padding:15px 10px;"> 
																																				<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																																					<tr>
																																						<!-- ID:TXT FOOTER-1 HEADING -->
																																							<td align="left" class="MsoNormal txt-center" style="color:#333333; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-weight:700; font-size:18px; line-height:28px; letter-spacing:0px; mso-line-height-rule: exactly;">
																																							<i class="fa fa-paper-plane" style="font-size:20px;color:#f6a900;"></i>																																			</td>	
																																					</tr>
																																					<tr>
																																						<td height="10" style="font-size:0; mso-line-height-rule:exactly; line-height:10px;">&nbsp;</td>
																																					</tr>
																																					<tr>
																																						<!-- ID:TXT FOOTER-1 ADDRESS -->
																																						<td align="left" class="MsoNormal txt-center" style="color:#fff; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-weight:400; font-size:14px; line-height:24px; letter-spacing:0px; mso-line-height-rule: exactly;">
                                                                                                                                                                                                        <h4>Contact Us </h4>
																																																		<?php echo SITE_ADDR; ?>

3rd floor#20, 6th Cross, 8th Main Rd, Vasanth Nagar, Bengaluru, Karnataka 560052
																																						</td>
																																					</tr>
																																				</table>
																																			</td>
																																		</tr>
																																	</table>
																																</div>	
																													
																																<!--[if (gte mso 9)|(IE)]>
																															</td>
																															<td align="center" valign="top" width="200">
																																<![endif]-->
																																<div style="display:inline-block; max-width:600px; vertical-align:top; width:100%;" class="div-width">
																																	<!-- TABLE CENTER -->	
																																	<table align="left" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; max-width:100%; width:100%;">
																																		<tr>
																																			<td align="center" style="padding:15px 10px;"> 
																																				<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																																					<tr>
																																						<!-- ID:TXT FOOTER-1 HEADING -->
																																							<td align="left" class="MsoNormal txt-center" style="color:#333333; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-weight:700; font-size:18px; line-height:28px; letter-spacing:0px; mso-line-height-rule: exactly;">
																																							<i class="fa fa-envelope" style="font-size:20px;color:#f6a900;"></i>
																																						</td>	
																																					</tr>
																																					<tr>
																																						<td height="10" style="font-size:0; mso-line-height-rule:exactly; line-height:10px;">&nbsp;</td>
																																					</tr>
																																					<tr>
																																						<!-- ID:TXT FOOTER-1 MAIL -->
																																						<td align="left" class="MsoNormal txt-center" style="color:#fff; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-size:15px; font-weight:400; line-height:24px; letter-spacing:1px;"><?php echo SITE_EMAIL_ENQ; ?></td>
																																					</tr>
																																					<tr>
																																						<td height="10" style="font-size:0; mso-line-height-rule:exactly; line-height:10px;">&nbsp;</td>
																																					</tr>
																																					<tr>
																																						<!-- ID:TXT FOOTER-1 WEB -->
																																						<td align="left" class="MsoNormal txt-center" style="color:#666666; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-size:15px; font-weight:400; line-height:24px; letter-spacing:0px;"><a href="<?php echo base_url(); ?>" target="_blank" style="color:#fff; text-decoration:none;"><?php echo SITE_ADDR; ?></a></td>
																																					</tr>
																																				</table>
																																			</td>
																																		</tr>
																																	</table>
																																</div>
																																<!--[if (gte mso 9)|(IE)]>
																															</td>
																															<td align="center" valign="top" width="200">
																																<![endif]-->
																																<div style="display:inline-block; max-width:600px; vertical-align:top; width:100%;" class="div-width">
																																	<!-- TABLE RIGHT -->	
																																	<table align="right" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; max-width:100%; width:100%;">
																																		<tr>
																																			<td align="center" style="padding:15px 10px;">
																																				<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
																																					<tr>
																																						<!-- ID:TXT FOOTER-1 HEADING -->
																																						<td align="left" class="MsoNormal txt-center" style="color:#333333; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-weight:700; font-size:18px; line-height:28px; letter-spacing:0px; mso-line-height-rule: exactly;">
																																							<i class="fa fa-phone" style="font-size:20px;color:#f6a900;"></i>
																																						</td>	
																																					</tr>
																																					<tr>
																																						<td height="10" style="font-size:0; mso-line-height-rule:exactly; line-height:10px;">&nbsp;</td>
																																					</tr>
																																																																							<tr>
																																						<!-- ID:TXT FOOTER-1 PHONE -->
																																						<td align="left" class="MsoNormal txt-center" style="color:#666666; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-size:15px; font-weight:400; line-height:24px; letter-spacing:0px;"></td>
																																					</tr>
																																				</table>
																																			</td>
																																		</tr>
																																	</table>
																																</div>
																																<!--[if (gte mso 9)|(IE)]>
																															</td>
																														</tr>
																													</table>
																													<![endif]-->
																												</td>
																											</tr>
																										</table>
																									</td>
																								</tr>
																								<tr>
																									<!-- ID:BR FOOTER-1 BORDER -->
																									<td style="border-bottom:1px solid #cccccc;" height="15"></td>
																								</tr>
																								<tr>
																									<td height="25" style="font-size:0; mso-line-height-rule:exactly; line-height:25px;">&nbsp;</td>
																								</tr>
																								<tr>
																									<td align="center">
																										<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%; max-width:100%;">
																											<tr>
																												<td align="center" style="width:100%; max-width:100%; font-size:0;">
																													<!--[if (gte mso 9)|(IE)]>
																													<table  aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="100%" style="width:100%;">
																														<tr>
																															<td align="center" valign="top" width="250">
																																<![endif]-->
																																<div style="display:inline-block; max-width:600px; width:100%; vertical-align:top;" class="div-width">
																																	<!--TABLE LEFT-->
																																	<table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%; max-width:100%;">
																																		<tbody>													
																																			<tr>
																																				<td align="center" style="padding:0px 10px;">
																																					<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
																																						<tr>
																																							<!-- ID:TXT FOOTER-1 COPY RIGHTS -->
																																							<td align="center" class="MsoNormal" style="color:#fff; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-size:12px; line-height:25px; letter-spacing:0px;">
																																								Generated via <a href="https://axonlabs.ai" target="_blank" style="color:#fff; text-decoration:none;">Flamingo Ecommerce</a> for <?php echo SITE_NAME; ?> E-Market Place<br>
																																								© 2023
																																							</td>
																																						</tr>
																																					</table>
																																				</td>
																																			</tr>										
																																		</tbody>
																																	</table>
																																</div>
																																<!--[if (gte mso 9)|(IE)]>
																															</td>
																															<td align="center" valign="top" width="340">
																															<![endif]-->
																															
																																<!--[if (gte mso 9)|(IE)]>
																															</td>
																														</tr>
																													</table>
																													<![endif]-->
																												</td>
																											</tr>
																										</table>
																									</td>			
																								</tr>	
																								<tr>
																									<td height="20" style="mso-line-height-rule:exactly; line-height:20px;">&nbsp;</td>
																								</tr>
																							</table>
																						</div>
																					<!--[if (gte mso 9)|(IE)]>
																				</td>
																			</tr>
																		</table>
																	<![endif]-->
																</td>
															</tr>
														</tbody>	
													</table>
												</div>
											<!--[if (gte mso 9)|(IE)]>
										</td>
									</tr>
								</table>
							<![endif]-->
						</td>
					</tr>					
				</tbody>	
			</table>
			<!-- FOOTER-1 ENDS -->
			
				
	</body>
</html>	