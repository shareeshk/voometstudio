  <!---- New Quick buy section ---->

  <div class="col-md-8 total_price_box" id="Quickbuy_Div" style="display:none;">

                                                        
<div class="Quick_buy_section">


            <!---quantity based promotions--->

            <?php 

            if(!empty($promotions) && ($inventory_product_info_obj->stock_available>0)){ 
                
               // echo '<pre>';
               
                foreach($promotions as $key =>$promotion){
                  
                    $promotion=$promotion[0];
                    //print_r($promotion);
                    $buy_type=$promotion->buy_type;
                    $get_type=$promotion->get_type;
                    $promo_quote=$promotion->promo_quote;
                    $to_buy=$promotion->to_buy;
                    $promo_end_date=$promotion->promo_end_date;
                    $promo_uid=$promotion->promo_uid;

                    if($buy_type=='Qty'){
                        if (strpos($get_type, '%') !== false) {
                           // echo 'true';
                            
                            preg_match_all('!\d+!', $get_type, $matches);
                            $per = implode(' ', $matches[0]);
                           
                            if($per>0){
                                $max_selling_price=$inventory_product_info_obj->max_selling_price;

                                $actual_price=($max_selling_price*$to_buy);

                                $dis_price1 = ($to_buy*round(($per * $max_selling_price) / 100));
                                $price2 = ($actual_price-$dis_price1);
                                $saved=$dis_price1;
                                 

                                $pc=get_purchased_count($promo_uid);

                                $txt='Best Value';
                                if($pc> 0){
                                    $txt='Most Purchased';
                                }
                                if($key==(count($promotions)-1)){
                                    $txt='Best Value';
                                }
                                
                        
                                ?>

                                <?php /*free_invs="" free_invs_qty=""
                                name="dependent"
                                offers_attached_to_radio="" 
                                buy_type="Qty"
                                to_get="discount" 
                                promo_selector="promo_selector_<?php echo $promo_uid ?>" 
                                apply_at_single_only="none" 
                                depend_qty="quantity_based" 
                                inventorymoq_promotions="<?php echo $inventory_product_info_obj->moq; ?>"
                                get_type="<?php echo $get_type; ?>" */?>


        <input class="qty_promo qty_promo_available_<?php echo $promo_uid; ?>" id="qty_promo_available_<?php echo $promo_uid; ?>" name="qty_promo_available_[]" value="<?php echo $promo_uid; ?>" type="hidden">
        <input value="<?php echo $key; ?>" id="promo_key_<?php echo $promo_uid; ?>" name="key[]" type="hidden">
        <input class="buy_qty_promo" id="buy_qty_<?php echo $promo_uid; ?>" name="buy_qty[]" value="<?php echo $to_buy; ?>" type="hidden">
        <input class="buy_dis_promo" id="buy_discount_<?php echo $promo_uid; ?>" name="buy_discount[]" value="<?php echo $to_buy; ?>" type="hidden">
        <input class="promo_uid" id="promo_uid_<?php echo $promo_uid; ?>" name="promo_uid[]" value="<?php echo $promo_uid; ?>" type="hidden">

        <div class="row">
            <div class="col-xs-12 col-sm-10 quick_buy_option" id="qb_promo_<?php echo $key ?>" onclick="apply_qb_promo('<?php echo $key; ?>','<?php echo $promo_uid; ?>','<?php echo $to_buy; ?>')">
                
                <div class="border_tx">
                <div class="quick_buy_tag"><?php echo $txt; ?></div> 
                    <div class="col-sm-7 pull-left quantity_sec"> <?php //echo $promo_quote; ?> 
                                Buy <?php echo $to_buy; ?> Qty Get <?php echo $get_type ?> Off
                </div>
                   
                    <div class="col-sm-5 text-right">
                        <div class="saving_div">
                            <span class="saving_text">
                            Saving <?php echo curr_sym; ?><?php echo $saved; ?>
                            </span>
                        </div>
                        <div class="text-right">

                            <span class="strike-class"><b><strike><?php echo curr_sym; ?> <?php echo $actual_price; ?> </strike></b></span>

                            <span class=" price_text"><b><?php echo curr_sym; ?> <?php echo $price2; ?> /-</b></span> 

                        </div>
                       
                    </div>
                </div>
            </div>
        </div>

        <div class="row ">
            
            <div class="col-xs-12 col-sm-10 limited_time_div_above mb-20">
                    <div class="row limited_time_div">
                        <div class="col-md-7 text-left limited_time_txt">
                            Limited Time Offer:
                        </div>
                        <div class="text-center col-md-5 limited_cl_time countdown_timer_<?php echo $key; ?>">
                            <div class="text-center col-md-3 count_div">
                                <div id="days_<?php echo $key; ?>">00 </div>
                                <div class="count_text">Days</div>
                            </div>
                            <div class="text-center col-md-3 count_div">
                                <div id="hours_<?php echo $key; ?>">
                                    00 
                                </div>
                                <div class="count_text">Hours</div>
                            </div>
                            <div class="text-center col-md-3 count_div">
                                <div id="min_<?php echo $key; ?>">
                                    00 
                                </div>
                                <div class="count_text">Min</div >
                            </div>
                            <div class="text-center col-md-3 count_div">
                                <div id="seconds_<?php echo $key; ?>">
                                    00 
                                </div>
                                <div class="count_text">Sec</div>
                            </div>
                        </div>
                     </div>

            </div>

        </div><!---row-->

        <script>
$(document).ready(function(){


promo_countdown('<?php echo $promo_end_date; ?>','<?php echo $key; ?>');

});
            
            </script>

                                <?php


                            }
                        }
                    }

                }
                
            }

            ?>

            <!---quantity based promotions--->


</div>
</div>



<!---- New Quick buy section ---->
