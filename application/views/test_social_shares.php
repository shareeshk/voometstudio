<?php
	$email_stuff_arr=email_stuff();
?>

<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo base_url()?>search" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">About Us</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">Links</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
									<li class="active"><span></span><a href="<?php echo base_url();?>aboutus">Test</a></li>
                                   
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
                
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading">
                    <span class="page-heading-title2">Test</span>
                </h2>
                <!-- Content page -->
                <div class="content-text clearfix">
               
					
					<!-- facebook share check-->

 <h1>Sharing using FB.ui() Dialogs</h1>

<p>Below are some simple examples of how to use UI dialogs in a web page.</p>

<h3>Sharing Links</h3>

<div id="shareBtn" class="btn btn-success clearfix">Share Dialog</div>

<p>The Share Dialog enables you to share links to a person's profile without them having to use Facebook Login. <a href="https://developers.facebook.com/docs/sharing/reference/share-dialog">Read our Share Dialog guide</a> to learn more about how it works.</p>


<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js"></script>


<script>

window.fbAsyncInit = function() {
    FB.init({
      appId            : '2352093934957298',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v15.0'
    });
  };

document.getElementById('shareBtn').onclick = function() {
  FB.ui({
    display: 'popup',
    method: 'share',
    href: 'https://developers.facebook.com/docs/',
  }, function(response){});
}
</script>

<h3>Publishing Open Graph Stories</h3>

<p>The Share Dialog can also be used to publish Open Graph stories without using Facebook Login or the Graph API. <a href="https://developers.facebook.com/docs/sharing/reference/share-dialog">Read our Share Dialog guide</a> to learn more about how it works.</p>

<div id="ogBtn" class="btn btn-success clearfix">Simple OG Dialog</div>
<div id="ogBtn_feed" class="btn btn-success clearfix">Simple OG Dialog</div>
<div id="ogBtn_api" class="btn btn-warning clearfix">Simple SSS</div>

<script>
document.getElementById('ogBtn').onclick = function() {
  
  /** method 1 */
    FB.ui({
    display: 'popup',
    method: 'share_open_graph',
    //action_type: 'og.likes',
    action_type: 'og.likes',
    action_properties: JSON.stringify({
        object:{
        'og:url': '<?php echo SITE_HREF; ?>assets/blog_images/3/3.png', // your url to share
        'og:title': 'Here my custom title',
        'og:description': 'here custom description',
        'og:image': '<?php echo SITE_HREF; ?>assets/blog_images/3/3.png'}
    })
  }, function(response){});
  
}
// // object:'https://developers.facebook.com/docs/',
// object:'<?php echo SITE_HREF; ?>/assets/blog_images/3/3.png',

document.getElementById('ogBtn_feed').onclick = function() {
  
  /** method 2 */
  //picture
    FB.ui({
    method: 'feed',
    display: 'popup',
    name: "I got ! Which European are you destined to date?",
    link: "<?php echo SITE_HREF; ?>assets/blog_images/3/3.png",
    description: 'here custom description',
   
  }, function(response){
    if (response && !response.error_message) {
      alert('Posting completed.');
    } else {
      alert('Error while posting.');
    }
  });

}

document.getElementById('ogBtn_api').onclick = function() {
  
  /** method 2 */
  FB.ui(
   {
     method: 'feed',
     name: 'Facebook Dialogs',
     link: '<?php echo SITE_HREF; ?>assets/blog_images/3/3.png',
     picture: '<?php echo SITE_HREF; ?>assets/blog_images/3/3.png',
     caption: 'Reference Documentation',
     description: 'Dialogs provide a simple, consistent interface for applications to interface with users.',
     message: 'Facebook Dialogs are easy!'
   },
   function(response) {
     if (response && response.post_id) {
       alert('Post was published.');
     } else {
       alert('Post was not published.');
     }
   }
 );s

}


</script>


                </div>
                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<!-- ./page wapper-->
