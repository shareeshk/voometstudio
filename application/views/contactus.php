<?php
$email_stuff_arr = email_stuff();
?>
<style>
    .well {
        box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
        background-color: #fff;
        transition: all 0.3s ease-in-out 0s;
    }
    #map {
        height: 400px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
    }
</style>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo base_url() ?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Contact Us</span>
        </div>
        <!-- ./breadcrumb -->
        <div id="contact" class="page-content page-contact">
            <div id="message-box-conact"></div>
            <div class="row">
                <div class="col-sm-6">
                    <h3 class="page-subheading">CONTACT FORM</h3>
                    <div class="contact-form-box">
                        <form name="contactus" id="contactus" class="form-horizontal">
                        <div class="form-selector">
                            <label>Name</label>
                            <input name="name" type="text" class="form-control input-sm" id="name"/>
                        </div>
                        <div class="form-selector">
                            <label>Email address</label>
                            <input name="email" type="email" class="form-control input-sm" id="email"/>
                        </div>
                        <div class="form-selector">
                            <label>Mobile Number</label>
                            <input name="mobile" type="number" maxlength="10" class="form-control input-sm" id="mobile"/>
                        </div>
                        <div class="form-selector">
                            <label>Subject</label>
                            <input name="subject" type="text" class="form-control input-sm" id="subject"/>
                        </div>
                        <div class="form-selector">
                            <label>Message</label>
                            <textarea cols="20" rows="10"   style="resize: none;" name="message" class="form-control input-sm" id="message"></textarea>
                        </div>
                        <div class="form-selector">
                            <button id="contact-new" class="button preventDflt" onclick="form_validation()">Send</button>
                        </div>
                        </form>
                    </div>
                </div>

                <!---map--->

                <div class="col-sm-6">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1944.4997275402739!2d77.62477325785365!3d12.907756262158676!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae14ec204daf51%3A0xb80d8469fed94d9a!2sPacks%20Bazaar!5e0!3m2!1sen!2sin!4v1633665434734!5m2!1sen!2sin" width="600" height="650" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->
<script type="text/javascript">
    function form_validation() {

        var name = $('input[name="name"]').val().trim();
        var email = $('input[name="email"]').val().trim();
        var mobile = $('input[name="mobile"]').val();
        var subject = $('input[name="subject"]').val();
        var message = $('textarea[name="message"]').val();

        var err = 0;
        if(!(name.length>=3)){
            $('input[name="name"]').css("border", "1px solid red");
            err = 1;
        }else{
            $('input[name="name"]').css({"border": "1px solid #ccc"});
        }
        if(email==''){
            $('input[name="email"]').css("border", "1px solid red");
            err = 1;
        }else{
            $('input[name="email"]').css({"border": "1px solid #ccc"});
        }
        if(mobile==''){
            $('input[name="mobile"]').css("border", "1px solid red");
            err = 1;
        }else{
            $('input[name="mobile"]').css({"border": "1px solid #ccc"});
        }
        if(subject==''){
            $('input[name="subject"]').css("border", "1px solid red");
            err = 1;
        }else{
            $('input[name="subject"]').css({"border": "1px solid #ccc"});
        }
        if(message==''){
            $('textarea[name="message"]').css("border", "1px solid red");
            err = 1;
        }else{
            $('textarea[name="message"]').css({"border": "1px solid #ccc"});
        }

        if (err == 1) {

            alert('Please Fill all required fields');
            /*
            bootbox.alert({
                size: "small",
                message: '<span style="color: red;">Please Fill all required fields</span>',
            });*/
        } else {

            var form_status = $('<div class="form_status"></div>');
            var form = $('#contactus');

            url = '<?php echo base_url() . "contactus_submit"?>';

            $.ajax({
                url: url,
                type: 'POST',
                data: $('#contactus').serialize(),
                dataType: 'html',
                beforeSend: function () {
                    form.prepend(form_status.html('<p><i class="fa fa-refresh fa-spin"></i> Processing...</p>').fadeOut());
                }
            }).done(function (data) {

                if (1) {

                    alert('Thank You..! Your query submitted successfully');

                    /*bootbox.alert({
                        size: "small",
                        message: 'Thank You..! Your query submitted successfully',
                    });*/
                    form_status.html('<p class="text-success">Thank You..! Your query submitted successfully</p>').fadeOut("fast", function () {
                        //window.location.href=data;
                    });
                    $('#contactus')[0].reset();

                }

            });
        }
        return false;
    }
</script>
