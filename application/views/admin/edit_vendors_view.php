<html lang="en">
<head>

<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>


@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script type="text/javascript">

function isNumber(evt) {

    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
	
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
	
    return true;
}	   
</script>
<script>

function showDivPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/vendors';
}
</script>

<?php //print_r($vendor);?>

<style type="text/css">
.is-empty{
	text-align:left !important
}
</style>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row-fluid" id="editDiv3">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="edit_vendors" id="edit_vendors" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_name: {
					required: true,
				},
				edit_email: {
					required: true,
				},
				edit_mobile: {
					required: true,
				},
				// edit_landline: {
				// 	//required: true,
				// },
				// edit_address1: {
				// 	required: true,
				// },
				// edit_address2: {
				// 	required: true,
				// },
				// edit_pincode: {
				// 	required: true,
				// }
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>				
		<input type="hidden" name="edit_vendor_id" id="edit_vendor_id" value="<?php echo $get_vendors_data["vendor_id"];?>">
		
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Vendor Data</h3>
		<div>	
		</div>
		<div class="tab-content">
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Name</label>
					<input id="edit_name" name="edit_name" type="text" class="form-control" value="<?php echo $get_vendors_data["name"];?>"/>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter email</label>
					<input id="edit_email" name="edit_email" type="email" class="form-control" value="<?php echo $get_vendors_data["email"];?>"/>
					<input id="edit_email_default" name="edit_email_default" type="hidden" class="form-control" value="<?php echo $get_vendors_data["email"];?>"/>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Mobile</label>
					<input id="edit_mobile" name="edit_mobile" type="text" class="form-control" maxlength="13" onkeypress="return isNumber(event)" value="<?php echo $get_vendors_data["mobile"];?>"/>
					<input id="edit_mobile_default" name="edit_mobile_default" type="hidden" class="form-control" value="<?php echo $get_vendors_data["mobile"];?>"/>
				</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Whatsapp Number</label>
					<input id="whatsapp_number" name="whatsapp_number" type="text" class="form-control" maxlength="13" onkeypress="return isNumber(event)" value="<?php echo $get_vendors_data["whatsapp_number"];?>"/>
				
				</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Landline Number</label>
					<input id="edit_landline" name="edit_landline" type="text" class="form-control" maxlength="13" onkeypress="return isNumber(event)" value="<?php echo $get_vendors_data["landline"];?>"/>
					<input id="edit_landline_default" name="edit_landline_default" type="hidden" class="form-control" value="<?php echo $get_vendors_data["landline"];?>"/>
				</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Address line 1</label>
					<input id="edit_address1" name="edit_address1" type="text" class="form-control" value="<?php echo $get_vendors_data["address1"];?>"/>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Address line 2</label>
					<input id="edit_address2" name="edit_address2" type="text" class="form-control" value="<?php echo $get_vendors_data["address2"];?>"/>
				</div>
				</div>
			</div>			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Pincode</label>
					<input id="edit_pincode" name="edit_pincode" type="text" class="form-control" value="<?php echo $get_vendors_data["pincode"];?>" onkeyup="get_city_state_details_by_pincodeFun()"/>
					<span class="delivery_address_pin_error" style="display:none;color:#a94442">
                                        Please enter valid pincode
                                    </span>
											
				</div>
				</div>
			</div>
			
			<!---new fields--->
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter city</label>
								
								<input id="city" value="<?php echo $get_vendors_data["city"];?>" name="city" type="text" class="form-control" readonly>
								
							</div>
				</div>
			</div>

		<div class="row">
			<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter state</label>					
					<input id="state" name="state" type="text" class="form-control" value="<?php echo $get_vendors_data["state"];?>" readonly>
								
				</div>
														
			</div>
		</div>
			

		<div class="row">
			<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Country</label>		
					<input id="country" name="country" type="text" class="form-control" value="<?php echo $get_vendors_data["country"];?>" readonly>
								
				</div>
														
			</div>
		</div>	

		<div class="row">

		<?php
		$about_furniture_operations=$get_vendors_data["about_furniture_operations"];
		$about_furniture_operations_arr=($about_furniture_operations!='') ? explode(',',$about_furniture_operations) : [];

		?>
				<div class="col-md-10 col-md-offset-1 text-left">
					<div class="form-group">
						<p>Tell us about your furniture-operations</p>
						<div class="">
							<input id="manufacturer" type="checkbox" value="manufacturer" name="about_furniture_operations[manufacturer]" <?php echo in_array('manufacturer',$about_furniture_operations_arr) ? 'checked' : '' ?>>
							<label for="manufacturer">We are a manufacturer</label>
						</div>
						<div class="">
							<input id="importer"  type="checkbox" value="importer" name="about_furniture_operations[importer]"  <?php echo in_array('importer',$about_furniture_operations_arr) ? 'checked' : '' ?>>
							<label for="importer">We are a importer</label>
						</div>
						<div class=" ">
							<input id="traders" type="checkbox" value="traders" name="about_furniture_operations[trader]" <?php echo in_array('traders',$about_furniture_operations_arr) ? 'checked' : '' ?>>
							<label for="traders">We are furniture traders</label>
						</div>
    				</div>
				</div>
		</div>

    <!--- -->

	<div class="row">
				<div class="col-md-10 col-md-offset-1 text-left">
					<div class="form-group">
						<p>Select what is applicable please (skip what is not applicable )</p>
						<div class="">
							<input id="website" type="checkbox" value="website" name="applicable_media[website]" <?php echo ($get_vendors_data["website_url"]!='') ? 'checked': '' ?> >
							<label for="website">Our website URL is </label>
						</div>

						<div class="website_div">
							<input class="form-control" id="website_url" value="<?php echo $get_vendors_data["website_url"];?>" type="text" name="applicable_media[website_url]" >
						</div>

						<div class="">
							<input id="instagram"  type="checkbox" value="instagram" name="applicable_media[instagram]" <?php echo ($get_vendors_data["instagram_url"]!='') ? 'checked': '' ?>>
							<label for="instagram">Our Instagram handle is</label>
						</div>
						<div class="instagram_div">
							<input class="form-control" id="instagram_url"  value="<?php echo $get_vendors_data["instagram_url"];?>" type="text" name="applicable_media[instagram_url]" >
						</div>
						<div class=" ">
							<input id="facebook" type="checkbox" value="facebook" name="applicable_media[facebook]" <?php echo ($get_vendors_data["facebook_url"]!='') ? 'checked': '' ?>>
							<label for="facebook"> Our Facebook handle is</label>
						</div>
						<div class="facebook_div">
							<input class="form-control" id="facebook_url" type="text" value="<?php echo $get_vendors_data["facebook_url"];?>" name="applicable_media[facebook_url]" >
						</div>
    				</div>
				</div>
		</div>
		<div class="row">
				<div class="col-md-10 col-md-offset-1 text-left">
		<div class="form-group" >
    What is the size of your team?
        <div class="radio">
          <label>
            <input type="radio" name="size_of_team" value="<5" <?php echo ($get_vendors_data["size_of_team"]=='<5') ? 'checked': '';?>>
            
            < 5 people
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="size_of_team" value=">5<25" <?php echo ($get_vendors_data["size_of_team"]=='>5<25') ? 'checked': '';?>>
            
            > 5 less than 25 people

          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="size_of_team" value=">25" <?php echo ($get_vendors_data["size_of_team"]=='>25') ? 'checked': '';?>>
           
            > 25 people

          </label>
        </div>
    </div>

		</div>
		</div>


		<div class="row">
		<?php
		$kind_of_furniture=$get_vendors_data["kind_of_furniture"];
		$kind_of_furniture_arr=($kind_of_furniture!='') ? explode(',',$kind_of_furniture) : [];

		?>
				<div class="col-md-10 col-md-offset-1 text-left">
					<div class="form-group">
						<p>What kind of furniture you deal in ?</p>
						<div class="">
							<input id="lowest_cost" type="checkbox" value="lowest_cost" name="kind_of_furniture[lowest_cost]" <?php echo in_array('lowest_cost',$kind_of_furniture_arr) ? 'checked' : '' ?>>
							<label for="lowest_cost">Lowest cost</label>
						</div>
						<div class="">
							<input id="economical"  type="checkbox" value="economical" name="kind_of_furniture[economical]"  <?php echo in_array('economical',$kind_of_furniture_arr) ? 'checked' : '' ?>>
							<label for="economical">Economical</label>
						</div>
						<div class=" ">
							<input id="luxury" type="checkbox" value="luxury" name="kind_of_furniture[luxury]" <?php echo in_array('luxury',$kind_of_furniture_arr) ? 'checked' : '' ?>>
							<label for="luxury">Luxury</label>
						</div>

						<div class=" ">
							<input id="super_luxury" type="checkbox" value="super_luxury" name="kind_of_furniture[super_luxury]" <?php echo in_array('super_luxury',$kind_of_furniture_arr) ? 'checked' : '' ?>>
							<label for="super_luxury">Super Luxury</label>
						</div>
    				</div>
				</div>
		</div>

		<div class="row">
				<div class="col-md-10 col-md-offset-1 text-left">
		<div class="form-group" >
		How many years have you been in furniture business?
        <div class="radio">
          <label>
            <input type="radio" name="years_of_business" value="<2" <?php echo ($get_vendors_data["years_of_business"]=='<2') ? 'checked': '';?>>
            
            < 2 years
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="years_of_business" value=">2<5" <?php echo ($get_vendors_data["years_of_business"]=='>2<5') ? 'checked': '';?>>
            
            > 2 less than 5 years
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="years_of_business" value=">5" <?php echo ($get_vendors_data["years_of_business"]=='>5') ? 'checked': '';?>>
           
            > 5 years

          </label>
        </div>
    </div>

		</div>
		</div>

		<div class="row">
				<div class="col-md-10 col-md-offset-1 text-left">
		<div class="form-group" >
		Do you shipping capabilities from your office/warehouse throughout India ?
        <div class="radio">
          <label>
            <input type="radio" name="shipping_capabilities" value="yes" <?php echo ($get_vendors_data["shipping_capabilities"]=='yes') ? 'checked': '';?>>
            
            Yes
          </label>

		  <div id="shipping_capabilities_div">
			  <textarea  class="form-control" name="shipping_capabilities_description" Placeholder="About shipping"><?php echo  $get_vendors_data["shipping_capabilities_description"];?></textarea>
		</div>

        </div>
        <div class="radio">
          <label>
            <input type="radio" name="shipping_capabilities" value="no" <?php echo ($get_vendors_data["shipping_capabilities"]=='no') ? 'checked': '';?>>
            
            No
          </label>
        </div>
        
    </div>

		</div>
		</div>



			<!---new fields--->
			

			<div class="row">
				<div class="col-md-10 col-md-offset-1">
				<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit-data" disabled=""></i>Submit</button>
					<?php
					$admin_user_type=$this->session->userdata("user_type");
					if($admin_user_type!="vendor"){ ?>
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
					<?php } ?>
				</div>	
				</div>
			</div>		
	
	</div>
</form>

</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>
</div>
</body>
</html>
<script>

$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_vendors'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_vendors"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_vendors'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', false);//disable
});

});

function form_validation_edit()
{
	var name = $('input[name="edit_name"]').val().trim();
	var email = $('input[name="edit_email"]').val().trim();
	var mobile = $('input[name="edit_mobile"]').val().trim();
	var pincode = $('input[name="edit_pincode"]').val().trim();
	var edit_address1 = $('input[name="edit_address1"]').val().trim();
	var edit_address2 = $('input[name="edit_address2"]').val().trim();
	var edit_landline = $('input[name="edit_landline"]').val().trim();

	   var err = 0;
	   if((name=='') || (email=='') || (mobile=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
var form_status = $('<div class="form_status"></div>');		
var form = $('#edit_vendors');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();		
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_vendors/"?>',
				type: 'POST',
				data: $('#edit_vendors').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				});
				  }  
				if(data==1)
				{
										  
					swal({
						title:"Success", 
						text:"Your Profile is successfully updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				if(data==0)
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
			});
}
}]);			
			}
	
	return false;
}

function get_city_state_details_by_pincodeFun(){

		pin=$("#edit_pincode").val();
		
		if(pin===undefined){
		}
		else{
		$.ajax({
				method : "POST",
				url : "<?php echo base_url();?>Account/get_city_state_details_by_pincode",
				data:{'pin' : pin},
				dataType:"JSON",
				}).success(function mySucces(result) {
					$("#city").val(result.city);
					$("#state").val(result.state);
					$("#country").val("India");
					
					if(parseInt(result.count)==0){
                        
                        $('.delivery_address_pin_error').show();
                        $('#pin_code_check').addClass('has-error');
                    }else{
                        //alert(result.count+"no error");
                        
                        $('.delivery_address_pin_error').hide();
                        $('#pin_code_check').removeClass('has-error');
                    }

				}, function myError(response) {
					
				});
		}	
	 }
</script>
