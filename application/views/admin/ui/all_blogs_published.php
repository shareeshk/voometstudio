<html lang="en">
<head>
<style>
.nav > li > a {
    position: relative;
    display: block;
    padding: 2px 15px;
}
.top_align
{
	margin-bottom: 10px;
}
a:hover {
    color: #ccc;
    text-decoration: none;
    transition: all 0.25s;
}
</style>

<style>
.btn-file {
	position: relative;
	overflow: hidden;
}
.btn-file input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	min-width: 100%;
	min-height: 100%;
	font-size: 100px;
	text-align: right;
	filter: alpha(opacity=0);
	opacity: 0;
	outline: none;
	background: white;
	cursor: inherit;
	display: block;
}
 </style>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js"></script>
<body>

<div class="columns-container">
    <div class="container" id="columns">

        <div class="row">

            <div class="center_column col-xs-12 col-sm-12" id="center_column">
				<div class="col-md-12">
				<div class="well">
				<div class="panel panel-default">
				  <div class="panel-heading">All Blogs</div>
				  <div class="panel-body">
				      <div class="col-sm-12">
				          <table id="all_blogs_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Sl.No</th>
                            <th>Title</th>
                            <th>Topic</th>
                            <th>Modified</th>
                            <th>Status</th>
                            <th>Actions</th>
							<th>Upload Blog Image</th>
							<th>Post To Frontend</th>
                        </tr>
                    </thead>
                    <tbody>
  
                    </tbody>
                </table>
				      </div>
				  </div>
				</div>
				</div>
				</div>
               
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   
   var table = $('#all_blogs_table').DataTable({
       
        "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Ui_content/all_blogs_published_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#all_blogs_table_processing").css("display","none");
            }
          },
          
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'1%',
         'className': 'dt-body-center'
      }],
      'order': [0, 'desc']
   });
}); 
</script> 
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>


	 <!------- blog image modal starts ------->
	  	  
					<div class="modal fade" id="blog_image_modal">
							  <div class="modal-dialog modal-lg">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times fa-1x"></i></span></button>
									<h4 class="modal-title">Upload</h4>
								  </div>
								  
								 	<script>
										$(document).ready(function(){
												$("#blog_image_form").on('submit',(function(e) { 
													e.preventDefault();
													
													//////////////////
													
														
														$.ajax({
															url:"<?php echo base_url();?>admin/Ui_content/add_blog_image",
															type: "POST",      				// Type of request to be send, called as method
															data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
															contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
															cache: false,					// To unable request pages to be cached
															processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
															beforeSend:function(){
						 $("#blog_image_form_btn").html('<i class="fa fa-spin fa-refresh"></i> Submitting');
					},
															success: function(data)  		// A function to be called if request succeeds
															{
																$("#blog_image_form_btn").html('<i class="fa fa-spin fa-refresh"></i> Submit');
																
																
																
																
																if(data==true){
																	
																	swal({
						title:"Success", 
						text:"Uploaded!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
							location.reload();											
					});
					return false;
					
																	
																}
																if(data==false){
																	swal({
					title:"Error", 
					text:"Not yet uploaded!", 
					type: "error",
					allowOutsideClick: false
				}).then(function () {
					return false;

				});
					return false;												
																}
															}	        
													   });
														
													//////////////////
													
												   
												   
											}));
									});
									
									</script>									
								<form method="post" id="blog_image_form" enctype="multipart/form-data">
								
								
								 <input type="hidden" name="blog_id_for_image" id="blog_id_for_image">
								 
								  <div class="modal-body" style="padding:5%;">
								 
								 
			
			<div class="row">
			<div class="col-md-5">
				<div class="form-group">
					
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-info btn-file">
                                        Upload Blog Image&hellip; <input type="file"  name="blog_image" class="www form-control input-md" >
                                    </span>
                                </span>
                            <div style="padding:0px">
                            <input type="text" class="form-control input-md" readonly>
                            </div>
                            </div> 
                             
                          
					
				</div>
			</div><!-- /.col -->
		  </div><!-- /.row -->
		  
		  
		  
			
												 
								  </div>
								  <div class="modal-footer">
									
									
									<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary" id="blog_image_form_btn">Submit</button>


								  </div>
								   </form>
								</div><!-- /.modal-content -->
							  </div><!-- /.modal-dialog -->
							</div><!-- /.modal -->
<!------- blog image modal ends ------->



<script>
function uploadBlogImage(blog_id){
	$("#blog_id_for_image").val(blog_id);
	$("#blog_image_modal").modal("show");
}
function post_to_frontend_fun(obj,blog_id){
	if(obj.checked==true){
		post="post";
	}
	else{
		post="unpost";
	}
	$.ajax({
															url:"<?php echo base_url();?>admin/Ui_content/post_to_frontend",
															type: "POST",      				// Type of request to be send, called as method
															data:  "blog_id="+blog_id+"&post="+post,	// Data sent to server, a set of key/value pairs representing form fields and values 
															
															
															success: function(data)  		// A function to be called if request succeeds
															{
																if(data==true){
																	if(post=="post"){
																		swal({
																			title:"Success", 
																			text:"Posted to frontend!", 
																			type: "success",
																			allowOutsideClick: false
																		}).then(function () {
																				location.reload();											
																		});
																	}
																	if(post=="unpost"){
																		swal({
																			title:"Success", 
																			text:"Unposted to frontend!", 
																			type: "success",
																			allowOutsideClick: false
																		}).then(function () {
																				location.reload();											
																		});
																	}
																	return false;
					
																	
																}
																else{
																	
																	swal({
																		title:"Error", 
																		text:"Not yet posted to frontend!", 
																		type: "error",
																		allowOutsideClick: false
																	}).then(function () {
																			
																	});
																	return false;
					
																	
																}
																
															}	        
													   });
}
</script>
</body>
</html>