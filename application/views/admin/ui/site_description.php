<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 

<style type="text/css">
#text-editor .btn-group, .btn-group-vertical {
    position: relative;
    margin: 0px 1px;
}
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}

.wizard-card .tab-content {
	 min-height: 400px;
	 padding:0px;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
.wizard-card input[type="file"]
{
	opacity: 0 !important;
	cursor: pointer;
	display: block;
	height: 100%;
	left: 0;
	opacity: 0 !important;
	position: absolute;
	top: 0;
	width: 100%;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js"></script>

<script>
function showDivCreate(){
	document.getElementById('viewDiv1').style.display = "none";
	document.getElementById('createDiv2').style.display = "block";
}
function showDivEdit(){
	document.getElementById('viewDiv1').style.display = "block";
	document.getElementById('createDiv2').style.display = "none";
}

</script>



<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container containerpage">		
<div class="row" id="viewDiv1">
	<div class="wizard-header text-center">
		<h5> Content for index page	<span class="badge" id="count"></span></h5>                        		                     	
	</div>

<table id="myTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
		
			<tr>
				<th colspan="6">
				<div class="row">				
					<div class="col-md-3">
			
						<button class="btn btn-info btn-xs" onclick="showDivCreate()">Create content</button>
						
					</div>
				</div>

					</div>
				</th>
			</tr>
			
			<tr>
				<th>S.no</th>
				<th>Title</th>
				<th>Description</th>
				<th>Bg-Image</th>
				<th>view_order</th>
				<th>active</th>
				<th>Last Updated</th>
				<th>Action</th>
			</tr>
</thead>
<tbody>
<?php 
$i=0;
if(count($data_obj)>0){
	foreach($data_obj as $data){
		$i++;
		?>
		<tr>
		<td><?php echo $i;?></td>
		<td><?php echo $data->title;?></td>
		<td><?php echo $data->description;
		
		if(strlen($data->description)>200){
			echo '<div class="f-row margin-top more">'.str_split($data->description,200)[0].'<span class="moreellipses">... </span><span class="morecontent"><span>' .substr($data->description,201). '</span> <a class="morelink cursor_pointer" onclick="morelinkfun(this)">more</a></span></div>';
		}else{
			echo '<div class="f-row margin-top more">'.$data->description.'</div>';
		}
		
		?></td>
		<td><img src="<?php echo base_url().$data->background_image;?>" class="img-responsive" style="width: 50%;"></td>
		<td><?php echo $data->view_order;?></td>
		<td><?php echo $data->active;?></td>
		<td><?php echo $data->timestamp;?></td>
		<td>
		<form action="<?php echo base_url()?>admin/Ui_content/edit_site_description" method="post"><input type="hidden" value="<?php echo $data ->id;?>" name="id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit">
		</form>
		<button id="multiple_delete_button3" class="btn btn-danger btn-xs btn-block " onclick="delete_site_des('<?php echo $data->id;?>')">Delete</button>
		</td>
		</tr>
		
		<?php 
	}
}
?>

</tbody>
</table>


</div>

<div class="row" id="createDiv2" style="display:none;">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
 	
		<form name="create_sd" id="create_sd" method="post" action="#" onsubmit="return form_validation();" enctype="multipart/form-data">
		
		<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				title: {
					required: true,
				},
				active: {
					required: true,
				},
				view_order: {
					required: true,
				}
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
			<div class="wizard-header">
			<div align="center">
				<h3 class="wizard-title formheader">Create Content</h3>
			</div>
			</div>
		
		<div class="tab-content">

				<div class="col-md-8 col-md-offset-2">
					<div class="form-group label-floating">
					<label class="control-label">Title</label>
					<input type="text" name="title" id="title" class="form-control"//>
					</div>
				</div>
				
				<div class="col-md-8 col-md-offset-2">
					<div class="form-group label-floating">
					<label class="control-label">View Order</label>
					<input type="text" name="view_order" id="view_order" class="form-control"//>
					</div>
				</div>

			<div class="col-md-12 text-center"><b>size (400x400)</b></div>
			<div class="col-md-12">
			<div class="picture-container">
				<div class="" id="commonPic">
					<img class="img-responsive" id="wizardPictureContent" title=""/>
					<input type="file" name="background_image" id="wizard-picture-content"/>
				</div>
			<h6 id="wizardPictureContent_picname"><i class="fa fa-upload fa-2x" aria-hidden="true"></i> Upload Image</h6>
			</div>
			</div>	
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<div class="form-group label-floating" id="text-editor">	
						<textarea id="description" name="description" class="form-control"/></textarea>
					</div>
				</div>
				</div>
			</div>
			
			<div class="col-md-8 col-md-offset-2"> 
				
					<div class="form-group">
					
					<div class="col-md-5 col-md-offset-1">Status</div>
					<div class="col-md-3">
						<label class="radio-inline"><input type="radio" name="active" id="active" value="1" checked="checked"/>show</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input type="radio" name="active" id="active" value="0"/>Hide</label>
					</div>
					</div>
				</div>
				
			<div class="row">
				<div class="col-md-8 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit_create_content">Submit</button>
					<button class="btn btn-danger btn-sm" id="reset_create_content" type="reset" onclick="reset_image()">Reset</button>
					<button class="btn btn-primary btn-sm" type="button" onclick="showDivEdit()">Go to View</button>
					</div>
				</div>
			</div>
		</div>
		
		</form>
				
</div>
</div> <!-- wizard container -->
</div>
</div>		
		
<div class="footer">
</div>
</div>
</div>
</body>
</html>

<script type="text/javascript">


$(document).ready(function(){
   $('#myTable').dataTable({ stateSave: true });
});
function reset_image(){
	$("#wizardPictureContent_picname").html('<i class="fa fa-upload fa-2x" aria-hidden="true"></i> Upload Image');
	$('#wizardPictureContent').attr('src', '');
	$('#commonPic').attr('style', ' ');
	$('input[name="background_image"]').val('');
}
function form_validation(){
	var data = $("#description").Editor("getText");
	$("#description").val(data);
	var text = $('textarea[name="description"]').val().trim();
	
	if(text==''){
		swal(
			'',
			'Please fill text',
			'error'
		)
	}
	var background_image = $('input[name="background_image"]').val().trim();
	/*if(background_image==''){
		swal(
			'',
			'Please upload Image',
			'error'
		)
	}*/
	
	   var err = 0;
		if((text=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		if(err==0){
		
			var form_status = $('<div class="form_status"></div>');		

			/////////////////////////
			
			
			
			var form = $('#create_sd');	
			var form_create = document.forms.namedItem("create_sd");
			var ajaxData = new FormData(form_create);
			
			swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();
    $.ajax({
				url: '<?php echo base_url()."admin/Ui_content/add_site_description/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false,

			}).done(function(data){	
				form_status.html('');
				
				if(data=="exists"){
					swal(
					'Oops...',
					'Banner created already  for this category..!',
					'info'
					);
					return false;
				}
				
				
			if(data){
				swal({
					title:"Success", 
					text:"Banner content is successfully added!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}
			if(data=="no")
			{
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
				
			});
  }
}]);
			
			////////////////////////
			
			}
	
	return false;
}

function delete_site_des(id,type,page){
	if(id!=''){
		
		swal({
  title: 'Are you sure?',
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then(function () {
	
		
		 $.ajax({
				url: '<?php echo base_url()."admin/Ui_content/delete_site_des/"?>',
				type: 'POST',
				data: "id="+id+"&type="+type+"&page="+page
			}).done(function(data){	

			if(data){
				swal({
					title:"Success", 
					text:"Deleted!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}
			if(data=="no")
			{
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
				
			});
	
	})
	}
}

</script>

<script  type="text/javascript" src="<?php echo base_url();?>assets/richte/editor.js" ></script>
<script>
$(document).ready(function() {
	var textarea = $("#description");
	textarea.Editor();
	
 });

 </script>