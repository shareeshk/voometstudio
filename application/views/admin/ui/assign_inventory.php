<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 

<style type="text/css">
#text-editor .btn-group, .btn-group-vertical {
    position: relative;
    margin: 0px 1px;
}
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}

.wizard-card .tab-content {
	 min-height: 400px;
	 padding:0px;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
.wizard-card input[type="file"]
{
	opacity: 0 !important;
	cursor: pointer;
	display: block;
	height: 100%;
	left: 0;
	opacity: 0 !important;
	position: absolute;
	top: 0;
	width: 100%;
}
#c_head{
	width:300px !important;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js"></script>

<script>
function showDivCreate(){
	document.getElementById('viewDiv1').style.display = "none";
	document.getElementById('createDiv2').style.display = "block";
	document.getElementById('createDiv2_shopbycategory').style.display = "none";
	document.getElementById('createDiv2_shopbyparentcategory').style.display = "none";
	document.getElementById('createDiv2_shopbybrand').style.display = "none";
}
function showDivEdit(){
	document.getElementById('viewDiv1').style.display = "block";
	document.getElementById('createDiv2').style.display = "none";
}
function showDivCreate_shopbycategory(){
	document.getElementById('viewDiv1').style.display = "none";
	document.getElementById('createDiv2').style.display = "none";
	document.getElementById('createDiv2_shopbycategory').style.display = "block";
	document.getElementById('createDiv2_shopbyparentcategory').style.display = "none";
	document.getElementById('createDiv2_shopbybrand').style.display = "none";
}
function showDivCreate_shopbyparentcategory(){
	document.getElementById('viewDiv1').style.display = "none";
	document.getElementById('createDiv2').style.display = "none";
	document.getElementById('createDiv2_shopbycategory').style.display = "none";
	document.getElementById('createDiv2_shopbyparentcategory').style.display = "block";
	document.getElementById('createDiv2_shopbybrand').style.display = "none";
}
function showDivCreate_shopbybrand(){
	document.getElementById('viewDiv1').style.display = "none";
	document.getElementById('createDiv2').style.display = "none";
	document.getElementById('createDiv2_shopbycategory').style.display = "none";
	document.getElementById('createDiv2_shopbyparentcategory').style.display = "none";
	document.getElementById('createDiv2_shopbybrand').style.display = "block";
}
</script>

<script type="text/javascript">
var table;
$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   table = $('#assign_inv_table').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Ui_content/assign_inv_processing", // json datasource
			data: function (d) { d.page = $('#s_page').val();d.section_name_in_page = $('#s_section_name_in_page').val();d.pcat_id = $('#pcat_id').val();d.cat_id = $('#categories1').val(); },
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#assign_inv_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("count").innerHTML=json.recordsFiltered;
				return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'5%',
         'className': 'dt-body-center'
      },{
         'targets': 1,
         'width':'35%',
      },{
         'targets': 2,
         'width':'25%',
      },{
         'targets': 3,
         'width':'15%',
      },{
         'targets': 4,
         'width':'10%',
      },{
         'targets': 5,
         'width':'10%',
      }],
      'order': [0, 'desc']
   });
});	

function draw_table(){
	table.draw();
}
$('#reset_form_button').on('click',function(){
	table.draw();
});

$('#submit_form_button').on('click',function(){
	table.draw();
});
</script>

<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container containerpage">		
<div class="row" id="viewDiv1">
	<div class="wizard-header text-center">
		<h5> Content for index page	<span class="badge" id="count"></span></h5>                        		                     	
	</div>

<table id="assign_inv_table" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
		
			<tr>
				<th colspan="6">
				<div class="row">				
					<div class="col-md-2">				
						<button class="btn btn-info btn-xs" onclick="showDivCreate()">Assign Products</button>
						<button class="btn btn-info btn-xs" onclick="showDivCreate_shopbycategory()">Assign Shop by Category</button>
						<button class="btn btn-info btn-xs" onclick="showDivCreate_shopbyparentcategory()">Assign Shop by Parent Category</button>
						<button class="btn btn-info btn-xs" onclick="showDivCreate_shopbybrand()">Assign Shop by Brand</button>
						<button id="multiple_delete_button3" class="btn btn-danger btn-xs" onclick="multilpe_delete_no_data_fun()">Delete</button>
					</div>
					
					<form id="search" class="form-inline">
					<div class="col-md-2">
					<label>Select page</label>
					
						<select class="browser-default" name="s_page" id="s_page" onchange="draw_table();" style="width: 100%;">
							<option value="common">Index</option>
							<option value="category">Category</option>
						</select>
					</div>
					
					<div class="col-md-2">
					<label>Select Section Name</label>
					
						<select class="browser-default" name="s_section_name_in_page" id="s_section_name_in_page" onchange="draw_table();" style="width: 100%;">
							<option value="Quick Buy">Quick Buy</option>
							<option value="Most Purchased">Most Purchased</option>
							<option value="Shop by Category">Shop by Category</option>
							<option value="Shop by Parent Category">Shop by Parent Category</option>
							<option value="Shop by Brand">Shop by Brand</option>
							<option value="Knockout Deals">Knockout Deals</option>
							<option value="Trending Products">Trending Products</option>
							<option value="Deal Of the day">Deal Of the day</option>
						</select>
					</div>
					
					
					
					<div class="col-md-2">
					<label>Select Parent category</label>
						<select class="browser-default" name="pcat_id" id="pcat_id" onchange="showAvailableCategories1(this);" style="width: 100%;">
							<option value=""></option>
							<?php foreach ($parent_category as $parent_category_value) {  ?>
							<option value="<?php echo $parent_category_value->pcat_id; ?>" <?php echo ($parent_category_value->pcat_id==$pcat_id)? "selected":''; ?> ><?php echo $parent_category_value->pcat_name; ?></option>
							<?php } ?>
							<option value="0">--None--</option>
						</select>
					</div>
					<div class="col-md-2">
					<label>Select category</label>
						<select name="cat_id" id="categories1" class="browser-default" onchange="draw_table();" style="width: 100%;">
						<option value=""></option>
						</select>
					</div>
					<button type="button" class="btn btn-sm btn-primary" id="submit_form_button">Submit</button>
					<button type="reset" class="btn btn-sm btn-info" id="reset_form_button">Reset</button>
					</form>
				</div>

				
				</th>
			</tr>
			
			<tr>
				<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
				<th id="c_head">Inv</th>
				<th>Vieworder</th>
				<th>Brand(s) / Product</th>
				<th>Parent Category / Category</th>
				<th>Page</th>
				<th>Last Updated</th>
				
			</tr>
</thead>
<tbody>

</tbody>
</table>


</div>

<div class="row" id="createDiv2" style="display:none;">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
 	
		<form name="assign_inv" id="assign_inv" method="post" action="#" onsubmit="return form_validation();" enctype="multipart/form-data">
		
		<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				parent_category: {
					required: true,
				},
				categories: {
					required: true,
				},
				subcategories: {
					required: true,
				},
				brands: {
					required: true,
				},
				products: {
					required: true,
				},
				page: {
					required: true,
				},
				'inventory[]': {
					required: true,
				}
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
		
		<div class="row">
			
			<div class="col-md-offset-3 col-md-6">
			
			<div class="col-md-12">
				<div class="form-group">
					<select name="parent_category" id="parent_category" class="form-control search_select" onchange="showAvailableCategories(this)">
						<option value="" selected>-Select Parent Category-</option>
						<?php
						
						foreach ($parent_catagory as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>"><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<option value="0">--None--</option>
						
					</select>
				</div>
			</div>
			<div class="col-md-12">
			<div class="form-group">
			<select name="categories" id="categories" class="form-control search_select" onchange="showAvailableSubCategories(this)">
						<option value="">-Select Category-</option>
			</select>
				</div>
			</div>

		
			<div class="col-md-12">
				 <div class="form-group">
					
					<select name="subcategories" id="subcategories" class="form-control search_select" onchange="showAvailableBrands(this)">
						<option value="">-Select Sub Category-</option>
					</select>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					
					<select name="brands" id="brands" class="form-control search_select" onchange="showAvailableProducts(this)">
						<option value="">-Select Brands-</option>
					</select>
				</div>
			</div>
			
			
			<div class="col-md-12">
				<div class="form-group">
					
					<select name="products" id="products" class="form-control search_select" onchange="showAvailableInventory(this)">
						<option value="">-Select Products-</option>
					</select>
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					
					<label class="control-label">-Select Inventory to assign-</label>
					<select name="inventory[]" id="inventory" class="form-control" multiple="multiple">
					
					</select>
				</div>
			</div>
		
			<div class="col-md-12">
				<div class="form-group">
					
					<select name="page" id="page" class="form-control search_select" required>
						<option value="">-Select Page type-</option>
						<option value="common">Index</option>
						<option value="category">Category</option>
					</select>
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<select name="section_name_in_page" id="section_name_in_page" class="form-control search_select">
						<option value="">-Select Section Name-</option>
						<option value="Quick Buy">Quick Buy</option>
						<option value="Most Purchased">Most Purchased</option>
						<option value="Knockout Deals">Knockout Deals</option>
						<option value="Trending Products">Trending Products</option>
						<option value="Deal Of the day">Deal Of the day</option>
					</select>
				</div>
			</div>
			
		
		<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-info btn-xs" id="submit_button">Submit</button>
			<button class="btn btn-warning btn-xs" type="reset" id="reset_form_button">Reset</button>
		</div>
			
			</div>
		
		</div>
		
		</form>
				
</div>
</div> <!-- wizard container -->
</div>
</div>	



<!--- shopbycategory div starts ------->
<div class="row" id="createDiv2_shopbycategory" style="display:none;">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
 	
		<form name="assign_inv_shopbycategory" id="assign_inv_shopbycategory" method="post" action="#" onsubmit="return form_validation_shopbycategory();" enctype="multipart/form-data">
		
		<script type="text/javascript">
			var $validator = $('.wizard-card #assign_inv_shopbycategory').validate({
			rules: {
				parent_category_shopbycategory: {
					required: true,
				},
				categories_shopbycategory: {
					required: true,
				},
				page_shopbycategory: {
					required: true,
				},
				'brands_shopbycategory[]': {
					required: true,
				}
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
		
		<div class="row">
			
			<div class="col-md-offset-3 col-md-6">
			
			<div class="col-md-12">
				<div class="form-group">
					<select name="parent_category_shopbycategory" id="parent_category_shopbycategory" class="form-control search_select" onchange="showAvailableCategories_shopbycategory(this)">
						<option value="" selected>-Select Parent Category-</option>
						<?php
						
						foreach ($parent_catagory as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>"><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<option value="0">--None--</option>
						
					</select>
				</div>
			</div>
			<div class="col-md-12">
			<div class="form-group">
			<select name="categories_shopbycategory" id="categories_shopbycategory" class="form-control search_select" onchange="showAvailableBrands_shopbycategory(this)">
						<option value="">-Select Category-</option>
			</select>
				</div>
			</div>

		
			
			
			
			
			<div class="col-md-12">
				<div class="form-group">
					
					<label class="control-label">-Select Brands to assign-</label>
					<select name="brand_shopbycategory[]" id="brand_shopbycategory" class="form-control" multiple="multiple">
					
					</select>
				</div>
			</div>
			
			
			
			
			
			<!---common--image--->
		
			<div class="col-md-5 col-md-offset-3">
			<div class="col-md-12 text-center"><b>Image (300x366)</b></div>
				<div class="col-md-12">
				<div class="picture-container">
					<div class="picture">
						<img class="picture-src" id="wizardPictureCommon" title=""/>
				
						<input type="file" name="image_shopbycategory" id="wizard-picture-Common" required/>
					</div>
				
				</div>
				</div>	
			</div>
		
		<!---common--image--->
		
		
			
		
		
			
			
			<div class="col-md-12">
				<div class="form-group">
					<input type="text" class="form-control" name="legend1_shopbycategory" required placeholder="Legend 1">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<input type="text" class="form-control"  name="legend2_shopbycategory" required placeholder="Legend 2">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<input type="text" class="form-control"  name="legend3_shopbycategory" required placeholder="Legend 3">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					
					<select name="page_shopbycategory" id="page_shopbycategory" class="form-control search_select" required>
						<option value="">-Select Page type-</option>
						<option value="common">Index</option>
						<option value="category">Category</option>
					</select>
				</div>
			</div>
			
			
		
		<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-info btn-xs" id="submit_shopbycategory_button">Submit</button>
			<button class="btn btn-warning btn-xs" type="reset" id="reset_form_shopbycategory_button">Reset</button>
		</div>
			
			</div>
		
		</div>
		
		</form>
				
</div>
</div> <!-- wizard container -->
</div>
</div>
<!--- shopbycategory div ends ----->







<!--- shopbyparentcategory div starts ------->
<div class="row" id="createDiv2_shopbyparentcategory" style="display:none;">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
 	
		<form name="assign_inv_shopbyparentcategory" id="assign_inv_shopbyparentcategory" method="post" action="#" onsubmit="return form_validation_shopbyparentcategory();" enctype="multipart/form-data">
		
		<script type="text/javascript">
			var $validator = $('.wizard-card #assign_inv_shopbyparentcategory').validate({
			rules: {
				parent_parentcategory_shopbyparentcategory: {
					required: true,
				},
				categories_shopbyparentcategory: {
					required: true,
				},
				page_shopbyparentcategory: {
					required: true,
				},
				
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
		
		<div class="row">
			
			<div class="col-md-offset-3 col-md-6">
			
			<div class="col-md-12">
				<div class="form-group">
					<select name="parent_parentcategory_shopbyparentcategory" id="parent_parentcategory_shopbyparentcategory" class="form-control search_select" onchange="showAvailableCategories_shopbyparentcategory(this)">
						<option value="" selected>-Select Parent Category-</option>
						<?php
						
						foreach ($parent_catagory as $parent_parentcategory_value) {  ?>
						<option value="<?php echo $parent_parentcategory_value->pcat_id; ?>"><?php echo $parent_parentcategory_value->pcat_name; ?></option>
						<?php } ?>
						<option value="0">--None--</option>
						
					</select>
				</div>
			</div>
			

		
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">-Select Categories to assign-</label>
					<select name="categories_shopbyparentcategory[]" id="categories_shopbyparentcategory" class="form-control" multiple="multiple">
								
					</select>
				</div>
			</div>
			
			
		
			
			
			
			
			
			<!---common-parent--image--->
		
			<div class="col-md-5 col-md-offset-3">
			<div class="col-md-12 text-center"><b>Image (300x366)</b></div>
				<div class="col-md-12">
				<div class="picture-container">
					<div class="picture">
						<img class="picture-src" id="wizardPictureCommonParent" title=""/>
				
						<input type="file" name="image_shopbyparentcategory" id="wizard-picture-Common-parent" required/>
					</div>
				
				</div>
				</div>	
			</div>
		
		<!---common-parent--image--->
		
		
			
		
		
			
			
			<div class="col-md-12">
				<div class="form-group">
					<input type="text" class="form-control" name="legend1_shopbyparentcategory" required placeholder="Legend 1">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<input type="text" class="form-control"  name="legend2_shopbyparentcategory" required placeholder="Legend 2">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<input type="text" class="form-control"  name="legend3_shopbyparentcategory" required placeholder="Legend 3">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					
					<select name="page_shopbyparentcategory" id="page_shopbyparentcategory" class="form-control search_select" required>
						<option value="">-Select Page type-</option>
						<option value="common">Index</option>
						<option value="parentcategory">Category</option>
					</select>
				</div>
			</div>
			
			
		
		<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-info btn-xs" id="submit_shopbyparentcategory_button">Submit</button>
			<button class="btn btn-warning btn-xs" type="reset" id="reset_form_shopbyparentcategory_button">Reset</button>
		</div>
			
			</div>
		
		</div>
		
		</form>
				
</div>
</div> <!-- wizard container -->
</div>
</div>
<!--- shopbyparentcategory div ends ----->




<!--- shopbybrand div starts ------->
<div class="row" id="createDiv2_shopbybrand" style="display:none;">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
 	
		<form name="assign_inv_shopbybrand" id="assign_inv_shopbybrand" method="post" action="#" onsubmit="return form_validation_shopbybrand();" enctype="multipart/form-data">
		
		<script type="text/javascript">
			var $validator = $('.wizard-card #assign_inv_shopbybrand').validate({
			rules: {
				brands_shopbybrand: {
					required: true,
				},
				page_shopbybrand: {
					required: true,
				},
				
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
		
		<div class="row">
			
			<div class="col-md-offset-3 col-md-6">
			
			<div class="col-md-12">
				<div class="form-group">
					<select name="brands_shopbybrand" id="brands_shopbybrand" class="form-control search_select">
						<option value="" selected>-Select Brand-</option>
						<?php
						
						foreach ($all_brand_names as $brand_names_obj) {  ?>
						<option value="<?php echo $brand_names_obj->brand_name; ?>"><?php echo $brand_names_obj->brand_name; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			

	
			
			
		
			
			
			
			
			
			<!---common-parent--image--->
		
			<div class="col-md-5 col-md-offset-3">
			<div class="col-md-12 text-center"><b>Image (300x366)</b></div>
				<div class="col-md-12">
				<div class="picture-container">
					<div class="picture">
						<img class="picture-src" id="wizardPictureCommonBrand" title=""/>
				
						<input type="file" name="image_shopbybrand" id="wizard-picture-Common-brand" required/>
					</div>
				
				</div>
				</div>	
			</div>
		
		<!---common-parent--image--->
		
		
			
		
		
			
			
			<div class="col-md-12">
				<div class="form-group">
					<input type="text" class="form-control" name="legend1_shopbybrand" required placeholder="Legend 1">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<input type="text" class="form-control"  name="legend2_shopbybrand" required placeholder="Legend 2">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<input type="text" class="form-control"  name="legend3_shopbybrand" required placeholder="Legend 3">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					
					<select name="page_shopbybrand" id="page_shopbybrand" class="form-control search_select" required>
						<option value="">-Select Page type-</option>
						<option value="common">Index</option>
						<option value="parentcategory">Category</option>
					</select>
				</div>
			</div>
			
			
		
		<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-info btn-xs" id="submit_shopbybrand_button">Submit</button>
			<button class="btn btn-warning btn-xs" type="reset" id="reset_form_shopbybrand_button">Reset</button>
		</div>
			
			</div>
		
		</div>
		
		</form>
				
</div>
</div> <!-- wizard container -->
</div>
</div>
<!--- shopbybrand div ends ----->






	
		
<div class="footer">
</div>
</div>
</div>
</body>
</html>

<script type="text/javascript">


$(document).ready(function(){
  // $('#myTable').dataTable({ stateSave: true });
});

function form_validation(){

	var parent_category = $('select[name="parent_category"]').val().trim();
	var categories = $('select[name="categories"]').val().trim();
	var subcategories = $('select[name="subcategories"]').val().trim();
	var brands = $('select[name="brands"]').val().trim();
	var products = $('select[name="products"]').val().trim();
	var page = $('select[name="page"]').val().trim();
	//var inventory = $('select[name="inventory"]').val().trim();

	    var err = 0;
		if(categories=='')
		{
		   $('select[name="categories"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('select[name="categories"]').css({"border": "1px solid #ccc"});
		}
		
		if(subcategories=='')
		{
		   $('select[name="subcategories"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{
			$('select[name="subcategories"]').css({"border": "1px solid #ccc"});
		}
		if(page=='')
		{
		   $('select[name="page"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{
			$('select[name="page"]').css({"border": "1px solid #ccc"});
		}
		
		if(err==0){
		
			var form_status = $('<div class="form_status"></div>');		

			/////////////////////////

			var form = $('#assign_inv');	
			var form_create = document.forms.namedItem("assign_inv");
			var ajaxData = new FormData(form_create);
			
			swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();
    $.ajax({
				url: '<?php echo base_url()."admin/Ui_content/add_assign_inventory/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false,

			}).done(function(data){	
				form_status.html('');
				
				
			if(data){
				swal({
					title:"Success", 
					text:"Inventories assigned successfully!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}
			if(data=="no")
			{
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
				
			});
  }
}]);
			
			////////////////////////
			
			}
	
	return false;
}

function update_view_order(id,val,section_name_in_page){
	
	view_order=$('#view_order_'+id).val();
	
	if((view_order!=val) && id!=''){
		
		$.ajax({
				url: '<?php echo base_url()."admin/Ui_content/update_view_order/"?>',
				type: 'POST',
				data: "id="+id+"&view_order="+view_order+"&section_name_in_page="+section_name_in_page
			}).done(function(data){	
//alert(data);
//return false;
			if(data){
				swal({
					title:"Success", 
					text:"Updated", 
					type: "success",
					allowOutsideClick: true
				}).then(function () {
					//location.reload();
					draw_table();

				});
			}else{
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
				
			});
	}else{
		swal(
					'',
					'change value and update',
					'info'
				)
	}
	
	
}

function delete_site_des(id,type,page){
	if(id!=''){
		
		swal({
  title: 'Are you sure?',
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then(function () {
	
		
		 $.ajax({
				url: '<?php echo base_url()."admin/Ui_content/delete_site_des/"?>',
				type: 'POST',
				data: "id="+id+"&type="+type+"&page="+page
			}).done(function(data){	

			if(data){
				swal({
					title:"Success", 
					text:"Deleted!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					//location.reload();
					draw_table();
				});
			}
			if(data=="no")
			{
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
				
			});
	
	})
	}
}

</script>
<script type="text/javascript">

function showAvailableInventory(obj){
	
	if(obj.value!="" && obj.value!="None"){
		product_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ui_content/show_available_inventory",
				type:"post",
				data:"product_id="+product_id,
				success:function(data){
					if(data!=0){
						$("#inventory").html(data);
					}
					else{
						$("#inventory").html('<option value="">-None-</option>');
					}
				}
			});
	}
	
}
function showAvailableProducts(obj){
	
	if(obj.value!="" && obj.value!="None"){
		brand_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_products",
				type:"post",
				data:"brand_id="+brand_id,
				success:function(data){
					if(data!=0){
						$("#products").html(data);
					}
					else{
						$("#products").html('<option value="">-None-</option>');
					}
				}
			});
	}
	
}
function showAvailableBrands(obj){
	
	if(obj.value!="" && obj.value!="None"){
		subcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_brands",
				type:"post",
				data:"subcat_id="+subcat_id,
				success:function(data){
					if(data!=0){
						$("#brands").html(data);
					}
					else{
						$("#brands").html('<option value="">-None-</option>');
					}
				}
			});
	}
	
}
function showAvailableSubCategories(obj){
	
	if(obj.value!="" && obj.value!="None"){
		cat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id,
				success:function(data){
					if(data!=0){
						$("#subcategories").html(data);
					}
					else{
						$("#subcategories").html('<option value="">-None-</option>')
						$("#brands").html('<option value="">-None-</option>')

					}
				}
			});
	}
	
}

function showAvailableCategories(obj){
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id,
				success:function(data){
					if(data!=0){
						$("#categories").html(data);
					}
					else{
						$("#categories").html('<option value="">-None-</option>')
						$("#brands").html('<option value="">-None-</option>')

					}
				}
			});
	}
	
}


function showAvailableCategories1(obj){
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id,
				success:function(data){
					if(data!=0){
						$("#categories1").html(data);
					}
					else{
						$("#categories1").html('<option value="">-None-</option>');
					}
				}
			});
	}else{
		$("#categories1").html('<option value=""></option>')
	}
	
}
function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_delete_no_data_fun(){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one assigned inventory!");
		return false;
	}
	var s_section_name_in_page=document.getElementById("s_section_name_in_page").value;
	if(s_section_name_in_page==""){
		alert("Choose Section in page!");
		return false;
	}
	selected_list=selected_list_arr.join(",");
	swal({
			title: 'Are you sure?',
			text: "Delete Assigned Inventory",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
			
	$.ajax({
		url:"<?php echo base_url()?>admin/Ui_content/delete_assigned_inventory",
		type:"post",
		data:"selected_list="+selected_list+"&section_name_in_page="+s_section_name_in_page,
		
		success:function(data){
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					draw_table();
				});
			}else {
				swal("Error", "Oops ..! You can't delete it.", "error");
			}
			//location.reload();
			
		}
	});
	
			})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
	
}

/************** shopbycategory js functions starts ****/
function showAvailableCategories_shopbycategory(obj){
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id,
				success:function(data){
					if(data!=0){
						$("#categories_shopbycategory").html(data);
					}
					else{
						$("#categories_shopbycategory").html('<option value="">-None-</option>')
						$("#brands_shopbycategory").html('<option value="">-None-</option>')

					}
				}
			});
	}
	
}

function showAvailableBrands_shopbycategory(obj){
	
	if(obj.value!="" && obj.value!="None"){
		category_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ui_content/show_available_brands_under_category",
				type:"post",
				data:"category_id="+category_id,
				success:function(data){
					if(data!=0){
						$("#brand_shopbycategory").html(data);
					}
					else{
						$("#brand_shopbycategory").html('<option value="">-None-</option>');
					}
				}
			});
	}
	
}
function form_validation_shopbycategory(){

	var parent_category = $('select[name="parent_category_shopbycategory"]').val().trim();
	var categories = $('select[name="categories_shopbycategory"]').val().trim();
	var brands = $('select[name="brand_shopbycategory"]').val();
	var page_shopbycategory = $('select[name="page_shopbycategory"]').val().trim();
	
	
	//var inventory = $('select[name="inventory"]').val().trim();

	    var err = 0;
		if(categories=='')
		{
		   $('select[name="categories_shopbycategory"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('select[name="categories_shopbycategory"]').css({"border": "1px solid #ccc"});
		}
		if(brands=='')
		{
		   $('select[name="brand_shopbycategory"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('select[name="brand_shopbycategory"]').css({"border": "1px solid #ccc"});
		}
		if(page_shopbycategory=='')
		{
		   $('select[name="page_shopbycategory"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('select[name="page_shopbycategory"]').css({"border": "1px solid #ccc"});
		}

		
		if(err==0){
		
			var form_status = $('<div class="form_status"></div>');		

			/////////////////////////

			var form = $('#assign_inv_shopbycategory');	
			var form_create = document.forms.namedItem("assign_inv_shopbycategory");
			var ajaxData = new FormData(form_create);
			
			swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();
    $.ajax({
				url: '<?php echo base_url()."admin/Ui_content/add_assign_brand_shopbycategory/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false,

			}).done(function(data){	
				form_status.html('');
				
				
			if(data){
				swal({
					title:"Success", 
					text:"Assigned successfully!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}
			if(data=="no")
			{
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
				
			});
  }
}]);
			
			////////////////////////
			
			}
	
	return false;
}
/************** shopbycategory js functions starts ****/




/************** shopbyparentcategory js functions starts ****/


function showAvailableCategories_shopbyparentcategory(obj){
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_categories_for_shop_by_parentcategory",
				type:"post",
				data:"pcat_id="+pcat_id,
				success:function(data){
					if(data!=0){
						$("#categories_shopbyparentcategory").html(data);
					}
				}
			});
	}
	
}


function form_validation_shopbyparentcategory(){

	var parent_parentcategory = $('select[name="parent_parentcategory_shopbyparentcategory"]').val().trim();
	var categories = $('select[name="categories_shopbyparentcategory"]').val();
	
	var page_shopbyparentcategory = $('select[name="page_shopbyparentcategory"]').val().trim();
	
	
	//var inventory = $('select[name="inventory"]').val().trim();

	    var err = 0;
		if(parent_parentcategory=='')
		{
		   $('select[name="parent_parentcategory_shopbyparentcategory"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('select[name="parent_parentcategory_shopbyparentcategory"]').css({"border": "1px solid #ccc"});
		}
		if(categories=='')
		{
		   $('select[name="categories_shopbyparentcategory"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('select[name="categories_shopbyparentcategory"]').css({"border": "1px solid #ccc"});
		}
		if(page_shopbyparentcategory=='')
		{
		   $('select[name="page_shopbyparentcategory"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('select[name="page_shopbyparentcategory"]').css({"border": "1px solid #ccc"});
		}

		
		if(err==0){
		
			var form_status = $('<div class="form_status"></div>');		

			/////////////////////////

			var form = $('#assign_inv_shopbyparentcategory');	
			var form_create = document.forms.namedItem("assign_inv_shopbyparentcategory");
			var ajaxData = new FormData(form_create);
			
			swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();
    $.ajax({
				url: '<?php echo base_url()."admin/Ui_content/add_assign_brand_shopbyparentcategory/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false,

			}).done(function(data){	
				form_status.html('');
				
				
			if(data){
				swal({
					title:"Success", 
					text:"Assigned successfully!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}
			if(data=="no")
			{
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
				
			});
  }
}]);
			
			////////////////////////
			
			}
	
	return false;
}
/************** shopbyparentcategory js functions starts ****/



/************** shopbybrand js functions starts ****/

function form_validation_shopbybrand(){

	var brand = $('select[name="brands_shopbybrand"]').val().trim();
	var page_shopbybrand = $('select[name="page_shopbybrand"]').val().trim();
	
	
	//var inventory = $('select[name="inventory"]').val().trim();

	    var err = 0;
		if(brand=='')
		{
		   $('select[name="brands_shopbybrand"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('select[name="brands_shopbybrand"]').css({"border": "1px solid #ccc"});
		}
		if(page_shopbybrand=='')
		{
		   $('select[name="page_shopbybrand"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('select[name="page_shopbybrand"]').css({"border": "1px solid #ccc"});
		}

		
		if(err==0){
		
			var form_status = $('<div class="form_status"></div>');		

			/////////////////////////

			var form = $('#assign_inv_shopbybrand');	
			var form_create = document.forms.namedItem("assign_inv_shopbybrand");
			var ajaxData = new FormData(form_create);
			
			swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();
    $.ajax({
				url: '<?php echo base_url()."admin/Ui_content/add_assign_brand_shopbybrand/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false,

			}).done(function(data){	
				form_status.html('');
				
				
			if(data){
				swal({
					title:"Success", 
					text:"Assigned successfully!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}
			if(data=="no")
			{
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
				
			});
  }
}]);
			
			////////////////////////
			
			}
	
	return false;
}
/************** shopbybrand js functions ends ****/



</script>