<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 
<style type="text/css">
#text-editor .btn-group, .btn-group-vertical {
    position: relative;
    margin: 0px 1px;
}
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}

.wizard-card .tab-content {
	 min-height: 400px;
	 padding:0px;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
.wizard-card input[type="file"]
{
	opacity: 0 !important;
	cursor: pointer;
	display: block;
	height: 100%;
	left: 0;
	opacity: 0 !important;
	position: absolute;
	top: 0;
	width: 100%;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js"></script>


<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container containerpage">		

<div class="row">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">

<div class="card wizard-card" data-color="green" id="wizardProfile">
 	
		<form name="edit_content" id="edit_content" method="post" action="#" onsubmit="return form_validation();" enctype="multipart/form-data">
		
			<input type="hidden" name="type" value="banner">
			<input type="hidden" name="page" value="category">
			<input type="hidden" name="id" value="<?php echo $id; ?>">
			
		<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				cat_id: {
					required: true,
				},
				pcat_id: {
					required: true,
				},
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
			<div class="wizard-header">
			<div align="center">
				<h3 class="wizard-title formheader">Edit Banner Content</h3>
			</div>
			</div>
		
		<div class="tab-content">
			
			<div class="col-md-8 col-md-offset-2">
					<div class="form-group label-floating">
					<label class="control-label">-Select Parent Category-</label>
					<select name="pcat_id" id="parent_category" class="form-control" onchange="showAvailableCategories(this)">
						<option value=""></option>
						<?php foreach ($parent_category as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>" <?php echo ($parent_category_value->pcat_id==$pcat_id)? "selected":''; ?> ><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<option value="0" <?php echo ($pcat_id==0)? "selected":''; ?>>--None--</option>
					</select>
					</div>
				</div>
			
				<div class="col-md-8 col-md-offset-2">
					<div class="form-group label-floating">
					<label class="control-label">-Select Category-</label>
					<select name="cat_id" id="categories" class="form-control">
						<option value=""></option>
						<?php foreach ($catagories as $catagories_value) {  ?>
						<option value="<?php echo $catagories_value->cat_id; ?>" <?php echo ($catagories_value->cat_id==$cat_id)? "selected":''; ?> ><?php echo $catagories_value->cat_name; ?></option>
						<?php } ?>
					</select>
					</div>
				</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="col-md-12 text-left"><b>Enter Text</b></div>
				<div class="form-group label-floating">
				
					<div class="form-group label-floating" id="text-editor">	
						<textarea id="cat_text" name="cat_text" class="form-control"/></textarea>
					</div>
				</div>
				</div>
			</div>

			<div class="col-md-12 text-center"><b>size (1920x601)</b></div>
			<div class="col-md-12">
			<div class="picture-container">
				<div class="" id="commonPic">
					<img class="img-responsive" src="<?php echo base_url().$data->background_image;?>" id="wizardPictureBanner" title=""/>
					<input type="file" name="background_image" id="wizard-picture-banner"/>
				</div>
			<h6 id="wizardPictureBanner_picname"><i class="fa fa-upload fa-2x" aria-hidden="true"></i> Upload Image</h6>
			</div>
			</div>	
			
			<input name="previous_image" id="previous_image" type="hidden" value="<?php echo $data->background_image;?>"/>
			<input name="id" id="id" type="hidden" value="<?php echo $data->id;?>"/>
				
			<div class="row">
				<div class="col-md-8 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit_create_banner">Submit</button>
					<button class="btn btn-danger btn-sm" id="reset_create_banner" type="reset" onclick="reset_image()">Reset</button>
					<button class="btn btn-primary btn-sm" type="button" onclick="go_back()">Go to View</button>
					</div>
				</div>
			</div>
		</div>
		
		</form>
				
</div>


</div> <!-- wizard container -->
</div>
</div>				
<div class="footer">
</div>
</div>
</div>
</body>
</html>

<?php		
//$highlight=str_replace("\n", '', addslashes($inventory_data_arr["highlight"]));
$cat_text=$data->cat_text;
$cat_text=addslashes($cat_text);
$cat_text1=str_replace("\n", '', $cat_text);

$string = trim(preg_replace('/\s+/', ' ', $cat_text1));
?>

<script type="text/javascript">

function go_back(){
	location.href='<?php echo base_url()."admin/Ui_content/banner_category";?>';
}
function reset_image(){
	$("#wizardPictureBanner_picname").html('<i class="fa fa-upload fa-2x" aria-hidden="true"></i> Upload Image');
	$('#wizardPictureBanner').attr('src', '<?php echo base_url().$data->background_image; ?>');
	$('#commonPic').attr('style', ' ');
	$('input[name="background_image"]').val('');
	reset_editor();
}
function reset_editor(){
	str='<?php echo $string; ?>';
	data=decodeHtml(str);
	$('#cat_text').siblings("div").children("div.Editor-editor").html(data);
}
function form_validation(){
	var data = $("#cat_text").Editor("getText");
	$("#cat_text").val(data);
	var text = $('textarea[name="cat_text"]').val().trim();
	
	if(text==''){
		swal(
			'',
			'Please fill text',
			'error'
		)
	}
	
		var err = 0;
		
		if(err==0){
		
			var form_status = $('<div class="form_status"></div>');		
			
			
			var form = $('#edit_content');	
	
			/////////////////////////
			var form = $('#edit_content');	
			var form_create = document.forms.namedItem("edit_content");
			var ajaxData = new FormData(form_create);
			
			swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();
    $.ajax({
				url: '<?php echo base_url()."admin/Ui_content/update_banner_category/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false,

			}).done(function(data){	
				form_status.html('');

			if(data){
				swal({
					title:"Success", 
					text:"Content is successfully updated!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					go_back();

				});
			}
			if(data=="no")
			{
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
				
			});
  }
}]);
			
			////////////////////////
			
			}
	
	return false;
}

function showAvailableCategories(obj){
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						//data='<option value="" selected>--Select category---</option>'+data;
						$("#categories").html(data);
						$("#categories").html(data);
						draw_table();
					}
					else{
						$("#categories").html('<option value=""></option>')
						
					}
				}
			});
	}else{
		$("#categories").html('<option value=""></option>')
	}
	
}

</script>


<script  type="text/javascript" src="<?php echo base_url();?>assets/richte/editor.js" ></script>
<script>

function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}
$(document).ready(function() {
	var textarea = $("#cat_text");
	textarea.Editor();
	str='<?php echo $string; ?>';
	data=decodeHtml(str);
	$('#cat_text').siblings("div").children("div.Editor-editor").html(data);
 });
 </script>