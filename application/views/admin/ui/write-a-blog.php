<link rel="stylesheet" href="<?php echo base_url();?>assets/select2/select2.min.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 
<style>
.top_align{
margin-bottom: 10px;
}
select2 select2-container select2-container--default{
width:100%;
}
</style>
<script>
$(document).ready(function(){
$('#blog_date').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		weekStart: 0, 
		format: 'YYYY-MM-DD', 
		shortTime : true
	});
});
</script>
<div class="container">
<div class="row">
<div class="page-header" style="margin-top:0;"><h2 class="text-center text-info">Write a New Blog</h2></div>
<form method="post" name="write_a_blog_form" id="write_a_blog_form" enctype="multipart/form-data" class="form-horizontal"> 
<input type="hidden" name="write_a_blog" value="addinitials">
<div class="form-group">
<div class="col-md-4 col-md-offset-4">
<select class="form-control" id="topic_sel" name="topic_sel" required>
<option value="">Select Topic</option>
<?php
if(!empty($blog_topic_result)){ 
foreach($blog_topic_result as $blog_topic_row){
?>
<option value="<?php echo $blog_topic_row['topic_id']?>"><?php echo $blog_topic_row['topic_sel']?></option>
<?php }
}?>
</select>
</div>
</div>
<div class="form-group">
<div class="col-md-4 col-md-offset-4">
<select class="form-control" id="tag_sel" multiple="" name="tag_sel[]" required>
<?php if(!empty($blog_tags_result)){
foreach($blog_tags_result as $blog_tags_row){?>
<option value="<?php echo $blog_tags_row['tags_id']?>"><?php echo $blog_tags_row['tags']?></option>
<?php }
}?>
</select>
</div>
</div>
<div class="form-group">
<div class="col-md-4 col-md-offset-4">
<input type="text" class="form-control" placeholder="Add Title here" name="blog_title" required>
</div>
</div>
<div class="form-group">
<div class="col-md-4 col-md-offset-4">
<input class="form-control" name="blog_date" id="blog_date" placeholder="Enter date">
</div>
</div>


<div class="form-group">
<div class="col-md-4 col-md-offset-4">
<button type="submit" class="btn btn-success btn-block btn-xs">Submit</button>
</div>
</div>
</form>
</div>
</div>
<script src="<?php echo base_url();?>assets/js/select2.min.js"></script>
<script>
$(window).on('load',function(){
$("#topic_sel").select2();
$("#tag_sel").select2();
$(".select2-selection").css("height", "42px");
$(".select2-container").css({"width":"100%"});
});
$(document).ready(function(){
$("#write_a_blog_form").on('submit',(function(e) { 
e.preventDefault();
//////////////////
	$.ajax({
	url:"<?php echo base_url()?>admin/Ui_content/write_a_blog_action",
	type: "POST",   
	dataType:"JSON",   				// Type of request to be send, called as method
	data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
	contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
	cache: false,					// To unable request pages to be cached
	processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
	beforeSend:function(){
	$("#home_quotesection_form_btn").html("Processing <i class='fa fa-refresh fa-spin'></i>");
	},
	success: function(data)  		// A function to be called if request succeeds
	{
	$("#home_quotesection_form_btn").html("Add");
		if(data.status=="yes"){
			swal({
			title:"Success", 
			text:"Initials Of Blogs Created!", 
			type: "success",
			allowOutsideClick: false
			}).then(function () {
			window.location.href="<?php echo base_url()?>admin/Ui_content/updateblog/"+data.blog_id
			return false;												
			});
		}
		else{
			swal({
			title:"Error", 
			text:"Not Added", 
			type: "error",
			allowOutsideClick: false
			}).then(function () {
			return false;

			});
		}
	}	        
	});
//////////////////
}));
});
</script>