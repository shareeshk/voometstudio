<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 
<style type="text/css">
#text-editor .btn-group, .btn-group-vertical {
    position: relative;
    margin: 0px 1px;
}
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}

.wizard-card .tab-content {
	 min-height: 400px;
	 padding:0px;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
.wizard-card input[type="file"]
{
	opacity: 0 !important;
	cursor: pointer;
	display: block;
	height: 100%;
	left: 0;
	opacity: 0 !important;
	position: absolute;
	top: 0;
	width: 100%;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js"></script>

<script>
function showDivCreate(){
	
	document.getElementById('viewDiv1').style.display = "none";
	document.getElementById('createDiv2').style.display = "block";
	document.getElementById('createDiv3').style.display = "none";
}
function showDivCreate2(){
	
	document.getElementById('viewDiv1').style.display = "none";
	document.getElementById('createDiv2').style.display = "none";
	document.getElementById('createDiv3').style.display = "block";
}
function showDivEdit(){
	
	document.getElementById('viewDiv1').style.display = "block";
	document.getElementById('createDiv2').style.display = "none";
	document.getElementById('createDiv3').style.display = "none";
	
}
</script>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container containerpage">		
<div class="row" id="viewDiv1">
	<div class="wizard-header text-center">
		<h5> Content for index page	</h5>                        		
		                       	
	</div>

<table id="myTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
		
			<tr>
				<th colspan="6">	
				<?php if(empty($data)){
						?>				
					<button class="btn btn-info btn-xs" onclick="showDivCreate()">Create Banner</button>
				<?php } if(empty($f_data)){
						?>
					
					<button id="multiple_delete_button4" class="btn btn-info btn-xs" onclick="showDivCreate2()">Create Footer</button>	
						<?php
						} ?>
				</th>
			</tr>
			
			<tr>
				<th>S. No</th>
				<th>bg-image </th>
				<th>Text</th>
				<th>Type</th>
				<th>Last Updated</th>
				<th>Action</th>
			</tr>
</thead>
<tbody>
<?php 
$i=0;
foreach($data_obj as $data){ 
$i++;
?>

<?php if(!empty($data)){ ?>
<tr>
	<td><?php echo $i;?></td>
	<td width="40%"><img src="<?php echo base_url().$data -> background_image; ?>" class="img-responsive"></td>
	<td>
	<?php 
	echo $data -> text.'<br>';
	 ?>
	</td>
	<td>
	<?php 
	echo $data -> type.'<br>';
	 ?>
	</td>
	<td><?php echo $data -> timestamp?></td>

	<td>
	<?php if($data ->type=="banner"){ ?>
	<form action="<?php echo base_url()?>admin/Ui_content/edit_content" method="post">
	<input type="hidden" value="<?php echo $data ->id;?>" name="id">
	<input type="hidden" value="banner" name="type">
	<input type="hidden" value="common" name="page">
	<input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit">
	</form>
	
	<button id="multiple_delete_button3" class="btn btn-danger btn-xs btn-block" onclick="delete_content('<?php echo $data ->id;?>','banner','common')">Delete</button>
	 <?php }?>
	 
	 <?php if($data->type=="footer"){?>
	 
	 <form action="<?php echo base_url()?>admin/Ui_content/edit_content" method="post">
	
	<input type="hidden" value="<?php echo $f_data ->id;?>" name="id">
	<input type="hidden" value="footer" name="type">
	<input type="hidden" value="common" name="page">
	<input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit">
	
	</form>
	
	<button id="multiple_delete_button3" class="btn btn-danger btn-xs btn-block" onclick="delete_content('<?php echo $f_data ->id;?>','footer','common')">Delete</button>
<?php }?>
	</td>

</tr>

<?php } ?>

<?php } ?>

</tbody>
</table>


</div>

<div class="row" id="createDiv2" style="display:none;">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
 	
		<form name="create_banner" id="create_banner" method="post" action="#" onsubmit="return form_validation();" enctype="multipart/form-data">
		
		<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				background_image: {
					required: true,
				}
				
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
			<div class="wizard-header">
			<div align="center">
				<h3 class="wizard-title formheader">Create Banner Content</h3>
			</div>
			</div>
		
		<div class="tab-content">
				
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="col-md-12"><label>Enter text</label></div>
				<div class="form-group label-floating" id="text-editor">	
					<textarea id="text" name="text" class="form-control"/></textarea>
				</div>
				</div>
			</div>
			
			<div class="col-md-12 text-center"><b>size (1920x601)</b></div>
			<div class="col-md-12">
			<div class="picture-container">
				<div class="" id="commonPic">
					<img class="img-responsive" id="wizardPictureBanner" title=""/>
					<input type="file" name="background_image" id="wizard-picture-banner"/>
				</div>
			<h6 id="wizardPictureBanner_picname"><i class="fa fa-upload fa-2x" aria-hidden="true"></i> Upload Image</h6>
			</div>
			</div>	
			

			<div class="row">
				<div class="col-md-8 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit_create_banner">Submit</button>
					<button class="btn btn-danger btn-sm" id="reset_create_banner" type="reset" onclick="reset_image()">Reset</button>
					<button class="btn btn-primary btn-sm" type="button" onclick="showDivEdit()">Go to View</button>
					</div>
				</div>
			</div>
		</div>
		
		</form>
				
</div>
</div> <!-- wizard container -->
</div>
</div>		

<div class="row" id="createDiv3" style="display:none;">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
 	
		<form name="create_footer" id="create_footer" method="post" action="#" onsubmit="return form_validation_footer();" enctype="multipart/form-data">
		
		<script type="text/javascript">
			var $validator = $('.wizard-card #create_footer').validate({
			rules: {
				background_image: {
					required: true,
				}
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
			<div class="wizard-header">
			<div align="center">
				<h3 class="wizard-title formheader">Create Footer Content</h3>
			</div>
			</div>
		
		<div class="tab-content">
				
			
			<div class="col-md-12 text-center"><b>size (1920x1000)</b></div>
			<div class="col-md-12">
			<div class="picture-container">
				<div class="" id="commonPic">
					<img class="img-responsive" id="wizardPictureFooter" title=""/>
					<input type="file" name="background_image" id="wizard-picture-footer"/>
				</div>
			<h6 id="wizardPictureFooter_picname"><i class="fa fa-upload fa-2x" aria-hidden="true"></i> Upload Image</h6>
			</div>
			</div>	
			

			<div class="row">
				<div class="col-md-8 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit_create_footer">Submit</button>
					<button class="btn btn-danger btn-sm" id="reset_create_footer" type="reset" onclick="reset_imag_footer()">Reset</button>
					<button class="btn btn-primary btn-sm" type="button" onclick="showDivEdit()">Go to View</button>
					</div>
				</div>
			</div>
		</div>
		
		</form>
				
</div>
</div> <!-- wizard container -->
</div>
</div>	

			
<div class="footer">
</div>
</div>
</div>
</body>
</html>

<script type="text/javascript">


$(document).ready(function(){
    $('#myTable').dataTable({ stateSave: true });
    $('#myTable_footer').dataTable({ stateSave: true });
});
function reset_image(){
	$("#wizardPictureBanner_picname").html('<i class="fa fa-upload fa-2x" aria-hidden="true"></i> Upload Image');
	$('#wizardPictureBanner').attr('src', '');
	$('#commonPic').attr('style', ' ');
	$('input[name="background_image"]').val('');
}
function form_validation(){
	
	var data = $("#text").Editor("getText");
	$("#text").val(data);
	var text = $('textarea[name="text"]').val().trim();
	
	var background_image = $('input[name="background_image"]').val().trim();
	   var err = 0;
		if((text=='') || (background_image=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		if(err==0){
		
			var form_status = $('<div class="form_status"></div>');		
			var form = $('#create_banner');	

				
			/////////////////////////
			var form = $('#create_banner');	
			var form_create = document.forms.namedItem("create_banner");
			var ajaxData = new FormData(form_create);
			
			swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();
    $.ajax({
				url: '<?php echo base_url()."admin/Ui_content/add_banner/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false,

			}).done(function(data){	
				form_status.html('');
				
			if(data){
				swal({
					title:"Success", 
					text:"Banner content is successfully added!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}
			if(data=="no")
			{
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
				
			});
  }
}]);
			
			////////////////////////
			
			}
	
	return false;
}
function form_validation_footer(){
	var data = $("#text").Editor("getText");
	$("#text").val(data);
	var text = $('textarea[name="text"]').val().trim();
	
	
	var background_image = $('#wizard-picture-footer').val().trim();
	   var err = 0;
		if((background_image=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		if(err==0){
		
			var form_status = $('<div class="form_status"></div>');		
			var form = $('#create_footer');	

				
			/////////////////////////
			var form = $('#create_footer');	
			var form_create = document.forms.namedItem("create_footer");
			var ajaxData = new FormData(form_create);
			
			swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();
    $.ajax({
				url: '<?php echo base_url()."admin/Ui_content/add_footer/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false,

			}).done(function(data){	
				form_status.html('');

			if(data){
				swal({
					title:"Success", 
					text:"Footer Image is successfully added!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}
			if(data=="no")
			{
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
				
			});
  }
}]);
			
			////////////////////////
			
			}
	
	return false;
}

function delete_content(id,type,page){
	if(id!=''){
		
				swal({
  title: 'Are you sure?',
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then(function () {
	
		 $.ajax({
				url: '<?php echo base_url()."admin/Ui_content/delete_content/"?>',
				type: 'POST',
				data: "id="+id+"&type="+type+"&page="+page
			}).done(function(data){	

			if(data){
				swal({
					title:"Success", 
					text:"Deleted!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}
			if(data=="no")
			{
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
				
			});
			
});
	}
}

</script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/richte/editor.js" ></script>
<script>

function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}
$(document).ready(function() {
	var textarea = $("#text");
	textarea.Editor();
 });
 </script>
