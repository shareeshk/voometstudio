<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 

<style type="text/css">
#text-editor .btn-group, .btn-group-vertical {
    position: relative;
    margin: 0px 1px;
}
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}

.wizard-card .tab-content {
	 min-height: 400px;
	 padding:0px;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
.wizard-card input[type="file"]
{
	opacity: 0 !important;
	cursor: pointer;
	display: block;
	height: 100%;
	left: 0;
	opacity: 0 !important;
	position: absolute;
	top: 0;
	width: 100%;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js"></script>

<script>
function showDivCreate(){
	document.getElementById('viewDiv1').style.display = "none";
	document.getElementById('createDiv2').style.display = "block";
}
function showDivEdit(){
	document.getElementById('viewDiv1').style.display = "block";
	document.getElementById('createDiv2').style.display = "none";
}

</script>


<script type="text/javascript">
var table;
$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   table = $('#banner_table').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Ui_content/banner_category_processing", // json datasource
			data: function (d) { d.pcat_id = $('#pcat_id').val();d.cat_id = $('#categories').val(); },
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#banner_table_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("count").innerHTML=json.recordsFiltered;
				return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'5%',
         'className': 'dt-body-center'
      },{
         'targets': 1,
         'width':'35%',
      },{
         'targets': 2,
         'width':'25%',
      },{
         'targets': 3,
         'width':'15%',
      },{
         'targets': 4,
         'width':'10%',
      },{
         'targets': 5,
         'width':'10%',
      }],
      'order': [0, 'desc']
   });
});	

function draw_table(){
	table.draw();
}
$('#reset_form_button').on('click',function(){
	table.draw();
});

$('#submit_form_button').on('click',function(){
	table.draw();
});
</script>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container containerpage">		
<div class="row" id="viewDiv1">
	<div class="wizard-header text-center">
		<h5> Content for index page	<span class="badge" id="count"></span></h5>                        		                     	
	</div>

<table id="banner_table" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
		
			<tr>
				<th colspan="6">
				<div class="row">				
					<div class="col-md-3">
									
						<button class="btn btn-info btn-xs" onclick="showDivCreate()">Create Banner</button>
						
					</div>
					
					<form id="search" class="form-inline">
					<div class="col-md-3 col-md-offset-1">
					<label>Select Parent category</label>
						<select class="browser-default" name="pcat_id" id="pcat_id" onchange="showAvailableCategories(this);" style="width: 100%;">
							<option value=""></option>
							<?php foreach ($parent_category as $parent_category_value) {  ?>
							<option value="<?php echo $parent_category_value->pcat_id; ?>" <?php echo ($parent_category_value->pcat_id==$pcat_id)? "selected":''; ?> ><?php echo $parent_category_value->pcat_name; ?></option>
							<?php } ?>
							<option value="0">--None--</option>
						</select>
					</div>
					<div class="col-md-3">
					<label>Select category</label>
						<select name="cat_id" id="categories" class="browser-default" onchange="draw_table();" style="width: 100%;">
						<option value=""></option>
						</select>
					</div>
					<button type="button" class="btn btn-sm btn-primary" id="submit_form_button">Submit</button>
					<button type="reset" class="btn btn-sm btn-info" id="reset_form_button">Reset</button>
					</form>
				</div>

					<!--<button type="submit" class="btn btn-primary">Search</button>-->
					
					
				</th>
			</tr>
			
			<tr>
				<th>Category</th>
				<th>bg-image </th>
				<th>Text</th>
				<th>Type</th>
				<th>Last Updated</th>
				<th>Action</th>
			</tr>
</thead>
<tbody>


</tbody>
</table>


</div>

<div class="row" id="createDiv2" style="display:none;">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
 	
		<form name="create_banner" id="create_banner" method="post" action="#" onsubmit="return form_validation();" enctype="multipart/form-data">
		
		<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				cat_text: {
					required: true,
				},
				pcat_id: {
					required: true,
				},
				cat_id: {
					required: true,
				}
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
			<div class="wizard-header">
			<div align="center">
				<h3 class="wizard-title formheader">Create Banner Content</h3>
			</div>
			</div>
		
		<div class="tab-content">
				
			
				<div class="col-md-8 col-md-offset-2">
					<div class="form-group label-floating">
					<label class="control-label">-Select Parent Category-</label>
					<select name="pcat_id" id="parent_category1" class="form-control" onchange="showAvailableCategories1(this)">
						<option value=""></option>
						<?php foreach ($parent_category as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>" <?php echo ($parent_category_value->pcat_id==$pcat_id)? "selected":''; ?> ><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<option value="0">--None--</option>
						
					</select>
					</div>
				</div>
			
				<div class="col-md-8 col-md-offset-2">
					<div class="form-group label-floating">
					<label class="control-label">-Select Category-</label>
					<select name="cat_id" id="categories1" class="form-control">
						<option value=""></option>
					</select>
					</div>
				</div>
			<div class="col-md-12 text-center"><b>size (1920x601)</b></div>
			<div class="col-md-12">
			<div class="picture-container">
				<div class="" id="commonPic">
					<img class="img-responsive" id="wizardPictureBanner" title=""/>
					<input type="file" name="background_image" id="wizard-picture-banner"/>
				</div>
			<h6 id="wizardPictureBanner_picname"><i class="fa fa-upload fa-2x" aria-hidden="true"></i> Upload Image</h6>
			</div>
			</div>	
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<div class="form-group label-floating" id="text-editor">	
						<textarea id="cat_text" name="cat_text" class="form-control"/></textarea>
					</div>
				</div>
				</div>
			</div>
				
		
			<div class="row">
				<div class="col-md-8 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit_create_banner">Submit</button>
					<button class="btn btn-danger btn-sm" id="reset_create_banner" type="reset" onclick="reset_image()">Reset</button>
					<button class="btn btn-primary btn-sm" type="button" onclick="showDivEdit()">Go to View</button>
					</div>
				</div>
			</div>
		</div>
		
		</form>
				
</div>
</div> <!-- wizard container -->
</div>
</div>		
		
<div class="footer">
</div>
</div>
</div>
</body>
</html>

<script type="text/javascript">


$(document).ready(function(){
   // $('#myTable').dataTable({ stateSave: true });
});
function reset_image(){
	$("#wizardPictureBanner_picname").html('<i class="fa fa-upload fa-2x" aria-hidden="true"></i> Upload Image');
	$('#wizardPictureBanner').attr('src', '');
	$('#commonPic').attr('style', ' ');
	$('input[name="background_image"]').val('');
}
function form_validation(){
	var data = $("#cat_text").Editor("getText");
	$("#cat_text").val(data);
	var text = $('textarea[name="cat_text"]').val().trim();
	
	if(text==''){
		swal(
			'',
			'Please fill text',
			'error'
		)
	}
	var background_image = $('input[name="background_image"]').val().trim();
	if(background_image==''){
		swal(
			'',
			'Please upload Image',
			'error'
		)
	}
	
	   var err = 0;
		if((text=='') || (background_image=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		if(err==0){
		
			var form_status = $('<div class="form_status"></div>');		

			/////////////////////////
			
			
			
			var form = $('#create_banner');	
			var form_create = document.forms.namedItem("create_banner");
			var ajaxData = new FormData(form_create);
			
			swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();
    $.ajax({
				url: '<?php echo base_url()."admin/Ui_content/add_banner_category/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false,

			}).done(function(data){	
				form_status.html('');
				
				if(data=="exists"){
					swal(
					'Oops...',
					'Banner created already  for this category..!',
					'info'
					);
					return false;
				}
				
				
			if(data){
				swal({
					title:"Success", 
					text:"Banner content is successfully added!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}
			if(data=="no")
			{
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
				
			});
  }
}]);
			
			////////////////////////
			
			}
	
	return false;
}

function delete_content(id,type,page){
	if(id!=''){
		
		swal({
  title: 'Are you sure?',
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then(function () {
	
		
		 $.ajax({
				url: '<?php echo base_url()."admin/Ui_content/delete_content/"?>',
				type: 'POST',
				data: "id="+id+"&type="+type+"&page="+page
			}).done(function(data){	

			if(data){
				swal({
					title:"Success", 
					text:"Deleted!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}
			if(data=="no")
			{
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
				
			});
	
	})
	}
}
function showAvailableCategories(obj){
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						//data='<option value="" selected>--Select category---</option>'+data;
						$("#categories").html(data);
						$("#categories").html(data);
						draw_table();
					}
					else{
						$("#categories").html('<option value=""></option>')
						
					}
				}
			});
	}else{
		$("#categories").html('<option value=""></option>')
	}
	
}
function showAvailableCategories1(obj){
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						//data='<option value="" selected>--Select category---</option>'+data;
						$("#categories1").html(data);
						$("#categories1").html(data);
						draw_table();
					}
					else{
						$("#categories1").html('<option value=""></option>')
						
					}
				}
			});
	}else{
		$("#categories1").html('<option value=""></option>')
	}
	
}
</script>

<script  type="text/javascript" src="<?php echo base_url();?>assets/richte/editor.js" ></script>
<script>
$(document).ready(function() {
	var textarea = $("#cat_text");
	textarea.Editor();
	//for edit
	//var sop='';
	//$('#highlight').siblings("div").children("div.Editor-editor").html(sop);
 });

 </script>