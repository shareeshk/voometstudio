
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js"></script>

<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="columns-container">
    <div class="container" id="columns">

        <div class="row">

            <div class="center_column col-xs-12 col-sm-12" id="center_column">
				<div class="col-md-12">
				<div class="well">
				<div class="panel panel-default">
				  <div class="panel-heading">All Published COmments</div>
				  <div class="panel-body">
				      <div class="col-sm-12">
				          <table id="all_comments_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Sl.No</th>
                            <th>Title</th>
                            <th>Comment</th>
                            <th>By</th>
                            <th>Time</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
  
                    </tbody>
                </table>
				      </div>
				  </div>
				</div>
				</div>
				</div>
  <!-- Modal -->
<div id="commentModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p id="commentHolder" style="overflow:auto"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>             
<script type="text/javascript">
function showFullMessage(obj){
var msg=obj.getAttribute('messages');
	$('#commentModal').modal('show');
	$('#commentHolder').html(msg);
}

	function deleteComment(obj){
	var msg_id=obj.getAttribute('msg_id');
	swal({
			title: 'Are you sure?',
			text: "Delete Comment",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Ui_content/deleteComment",
		type:"post",
		data:"msg_id="+msg_id,
		
		success:function(data){
			if(data=='true'){
				swal({
						title:"Deleted!", 
						text:"Comment is deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this comment", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					//location.reload();
				});
			  }
			});
	}
	function unPublishComment(obj){
		var msg_id=obj.getAttribute('msg_id');
		$.ajax({
            type: 'post',
            url: '<?php echo base_url()?>admin/Ui_content/unPublishComment',
            data: 'ids='+msg_id,
            success: function (data) {
                swal({
						title:"Un Published", 
						//text:"Logistics Weight is successfully updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
            },
            error: function (data) {
                 swal({
					title: "Server Error!",
					text: "Try After Some Time!",
					type: "error"
				}).then(function () {
						location.reload();

					});
            },
        });
	}
	
$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   
   var table = $('#all_comments_table').DataTable({
       
        "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Ui_content/all_blogs_published_comments_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#all_comments_table_processing").css("display","none");
            }
          },
          
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'1%',
         'className': 'dt-body-center'
      }],
      'order': [0, 'desc']
   });
}); 
</script> 
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
</div>