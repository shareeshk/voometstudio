<?php
if(empty($blog_data)){
$this->session->set_flashdata('error','This Blog Does not Exists');
redirect("admin/Ui_content/write_a_blog");
}
?>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="<?php echo base_url();?>assets/select2/select2.min.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 
<style>
.top_align{
margin-bottom: 10px;
}
select2 select2-container select2-container--default{
width:100%;
}
.form-group input[type=file]{
	opacity:1;
	height:5em;
}
</style>
<script>
$(document).ready(function(){
$('#blog_date').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		weekStart: 0, 
		format: 'YYYY-MM-DD', 
		shortTime : true
	});
});
</script>
<style>
.top_align{
margin-bottom: 10px;
}
</style>
<link rel="stylesheet" href="<?php echo base_url();?>assets/dist/summernote.css">
<script type="text/javascript" src="<?php echo base_url();?>assets/dist/summernote.js"></script>
<script type="text/javascript">
$(function() {
$('.summernote').summernote({

height: 500,
toolbar: [
['img', ['picture']],
['style', ['style', 'addclass', 'clear']],
['fontstyle', ['bold', 'italic', 'ul', 'ol', 'link', 'paragraph']],
['fontsize', ['fontsize']],
['fontstyleextra', ['strikethrough', 'underline', 'hr', 'color', 'superscript', 'subscript']],
['extra', ['video', 'table', 'height']],
['misc', ['undo', 'redo', 'codeview', 'help']]


]
});
});
</script>

<div class="container">
<h4 class="text-center">Update Blog</h4>
<div class="row">
<div class="col-md-12">
<form method="post" name="update_initial_blog" id="update_initial_blog" enctype="multipart/form-data" class="form-horizontal">
<input type="hidden" name="write_a_blog" value="updateinitials">
<div class="form-group">
<label for="sel1" class="col-md-3 control-label">Select Topic:</label>
<div class="col-md-6">
<select class="form-control" id="topic_sel" name="topic_sel">
<option value="">Choose</option>
<?php  if(!empty($blog_topic_result)){
	foreach($blog_topic_result as $blog_topic_row){?>
<option value="<?php echo $blog_topic_row['topic_id']?>" <?php echo $blog_data['topic_sel']==$blog_topic_row['topic_id'] ? "selected":""?>><?php echo $blog_topic_row['topic_sel']?></option>
<?php }
}?>
</select>
</div>
</div>
<div class="form-group">
<label for="sel1" class="col-md-3 control-label">Select Tags:</label>
<div class="col-md-6">
<?php $tag_sel=json_decode($blog_data['tag_sel'],true)?>
<select class="form-control" id="tag_sel" multiple="" name="tag_sel[]">
<option value="">Choose</option>
<?php if(!empty($blog_tags_result)){
	foreach($blog_tags_result as $blog_tags_row){?>
<option value="<?php echo $blog_tags_row['tags_id']?>" <?php echo in_array($blog_tags_row['tags_id'],$tag_sel) ? "selected":""?>><?php echo $blog_tags_row['tags']?></option>
<?php }
}?>
</select>
</div>
</div>

<div class="form-group">
<label for="email" class="col-md-3 control-label">Add Title:</label>
<div class="col-md-6">
<input type="text" class="form-control" placeholder="title here" name="blog_title" value="<?php echo $blog_data['blog_title']?>" >
</div>
</div>


<div class="form-group">
<label for="Upload" class="col-md-3 control-label">Upload:</label>
<div class="col-md-6">
	<input type="file" name="blog_image_for_content" class="form-control">
</div>



<?php
$blog_image_for_content_exists="no";
	if($blog_data["blog_image_for_content"]!=""){
		$blog_image_for_content_exists="yes";
?>
<div class="col-md-1">
	<img src="<?php echo base_url().$blog_data["blog_image_for_content"];?>" width="100" height="100">
</div>
<?php
	}
?>


</div>

<input type="hidden" class="form-control"  name="blog_image_for_content_exists" value="<?php echo $blog_image_for_content_exists?>" >

<input type="hidden" class="form-control"  name="blog_id" value="<?php echo $blog_data['blog_id']?>" >
<div class="form-group" id="text-editor">   
<div class="col-md-12">							  
<textarea name="blog_content" class="form-control summernote" id="text" title="Contents"></textarea>
</div>
</div>
<div class="form-group">
<div class="col-md-6 col-md-offset-3">
<?php if($blog_data['admin_publish']!=1){?>
<button type="submit" class="btn btn-default btn-xs btn-block" id="blog_button">Update</button>
<?php }?>
</div>
</div>
</form>
</div>
</div>
</div>





<script src="<?php echo base_url();?>assets/js/select2.min.js"></script>
<script>
$(window).on('load',function(){
$("#topic_sel").select2();
$("#tag_sel").select2();
$(".select2-selection").css("height", "42px");
$(".select2-container").css({"width":"100%"});
});
</script>

<script>
$(document).ready(function(){
 $('#update_initial_blog').on('submit',(function(event){
var data = $('#text').summernote('code')
$("#text").val(data);

event.preventDefault();

	$.ajax({
	url:"<?php echo base_url()?>admin/Ui_content/write_a_blog_action",
	type: "POST",   
	dataType:"JSON",
	data:  new FormData(this),	 
	contentType: false,       		
	cache: false,
	processData:false,
	beforeSend:function(){
	$("#blog_button").html("Processing <i class='fa fa-refresh fa-spin'></i>");
	},
	success: function(data){
	$("#blog_button").html("Update Initials");
		if(data.status=="yes"){
			swal({
			title:"Success", 
			text:"Initials of Blogs Updated!", 
			type: "success",
			allowOutsideClick: false
			}).then(function () {
			window.location.href="<?php echo base_url();?>admin/Ui_content/all_blogs_unpublished";
			return false;												
			});
		}
		if(data.status=="no"){
				if(data.reason=="wrong_dimensions"){
					swal({
				title:"Info", 
				text:"Image Height and Width must be 1200px * 500px.", 
				type: "info",
				allowOutsideClick: false
				}).then(function () {
				return false;
				});
				}
				else{
						swal({
						title:"Error", 
						text:"Not Added", 
						type: "error",
						allowOutsideClick: false
						}).then(function () {
							return false;

						});
				}
			}
	}	        
	});
//////////////////
}));
});

function decodeHtml(html) {
var txt = document.createElement("textarea");
txt.innerHTML = html;
return txt.value;
}
$(document).ready(function() {
<?php       
$description=$blog_data['blog_content'];
$description=addslashes($description);
$description1=str_replace("\n", '', $description);

$string = trim(preg_replace('/\s+/', ' ', $description1));
?>
str='<?php echo $string; ?>';

data=decodeHtml(str);
$("#text").summernote('code',str); 
});
$(document).ready(function(){
$(".select2-container").css({"width":"100%"});
});
</script>