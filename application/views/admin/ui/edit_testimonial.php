<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 
<style type="text/css">
.text-editor .btn-group, .btn-group-vertical {
    position: relative;
    margin: 0px 1px;
}
#text-editor .btn-group, .btn-group-vertical {
    position: relative;
    margin: 0px 1px;
}
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}

.wizard-card .tab-content {
	 min-height: 400px;
	 padding:0px;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
.wizard-card input[type="file"]
{
	opacity: 0 !important;
	cursor: pointer;
	display: block;
	height: 100%;
	left: 0;
	opacity: 0 !important;
	position: absolute;
	top: 0;
	width: 100%;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js"></script>


<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container containerpage">		

<div class="row">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">

<div class="card wizard-card" data-color="green" id="wizardProfile">
 	
		<form name="edit_content" id="edit_content" method="post" action="#" onsubmit="return form_validation();" enctype="multipart/form-data">
		
			<input type="hidden" name="page" value="<?php echo $data->page; ?>">
			<input type="hidden" name="id" value="<?php echo $id; ?>">
			
		<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				name: {
					required: true,
				},
				designation: {
					required: true,
				},
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
			<div class="wizard-header">
			<div align="center">
				<h3 class="wizard-title formheader">Edit Testimonial</h3>
			</div>
			</div>
		
		<div class="tab-content">
			
			<div class="col-md-8 col-md-offset-2">
					<div class="form-group label-floating">
					<label class="control-label">-Select Parent Category-</label>
					<select name="pcat_id" id="parent_category" class="form-control" onchange="showAvailableCategories(this)">
						<option value=""></option>
						<?php foreach ($parent_category as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>" <?php echo ($parent_category_value->pcat_id==$pcat_id)? "selected":''; ?> ><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<option value="0" <?php echo ($pcat_id==0)? "selected":''; ?>>--None--</option>
					</select>
					</div>
				</div>
			
				<div class="col-md-8 col-md-offset-2">
					<div class="form-group label-floating">
					<label class="control-label">-Select Category-</label>
					<select name="cat_id" id="categories" class="form-control">
						<option value=""></option>
						<?php 
						if(isset($catagories)){
						foreach ($catagories as $catagories_value) {  ?>
						<option value="<?php echo $catagories_value->cat_id; ?>" <?php echo ($catagories_value->cat_id==$cat_id)? "selected":''; ?> ><?php echo $catagories_value->cat_name; ?></option>
						<?php } 
						}?>
					</select>
					</div>
				</div>
			
			<div class="col-md-12 text-center"><b>size (120x120)</b></div>
			<div class="col-md-12">
			<div class="picture-container">
				<div class="picture" id="commonPic">
					<img class="img-responsive" src="<?php echo base_url().$data->image;?>" id="wizardPictureTesti" title=""/>
					<input type="file" name="image" id="wizard-picture-testi"/>
				</div>
			<h6 id="wizardPictureTesti_picname"><i class="fa fa-upload fa-2x" aria-hidden="true"></i> Upload Image</h6>
			</div>
			</div>
			
			<div class="row">
				<div class="col-md-8 col-md-offset-2">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Name</label>
						<input id="name" name="name" value="<?php echo $data->name;?>" class="form-control"/>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">  
					
					<div class="form-group label-floating">	
					<label class="control-label">Enter designation</label>
						<input id="designation" value="<?php echo $data->designation;?>" name="designation" class="form-control"/>
					</div>
				
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-8 col-md-offset-2">  
					
					<div class="form-group label-floating">	
					<label class="control-label">Enter Heading</label>
						<input id="heading" name="heading" value="<?php echo $data->heading;?>" class="form-control"/>
					</div>
				
				</div>
			</div>
			
			<div class="row">
			
				<div class="col-md-8 col-md-offset-2">  
				<div class="col-md-12 text-left">Enter Content</div>
				<div class="form-group label-floating text-editor">
						<textarea id="content" name="content" class="form-control"/></textarea>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-8 col-md-offset-2">  
					
					<div class="form-group label-floating">	
					<label class="control-label">Enter Heading2</label>
						<input id="heading2" name="heading2" value="<?php echo $data->heading2;?>" class="form-control"/>
					</div>
				
				</div>
			</div>
			
			<div class="row">
			
				<div class="col-md-8 col-md-offset-2">  
				<div class="col-md-12 text-left">Enter Content2</div>
				<div class="form-group label-floating text-editor">
						<textarea id="content2" name="content2" class="form-control"/></textarea>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-8 col-md-offset-2">  
					
					<div class="form-group label-floating">	
					<label class="control-label">Enter Heading3</label>
						<input id="heading3" name="heading3" value="<?php echo $data->heading3;?>" class="form-control"/>
					</div>
				
				</div>
			</div>
			
			<div class="row">
			
				<div class="col-md-8 col-md-offset-2">  
				<div class="col-md-12 text-left">Enter Content3</div>
				<div class="form-group label-floating text-editor">
						<textarea id="content3" name="content3" class="form-control"/></textarea>
				</div>
				</div>
			</div>
			

			<input name="previous_image" id="previous_image" type="hidden" value="<?php echo $data->image;?>"/>
			<input name="id" id="id" type="hidden" value="<?php echo $data->id;?>"/>
				
			<div class="row">
				<div class="col-md-8 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit_create_banner">Submit</button>
					<button class="btn btn-danger btn-sm" id="reset_create_banner" type="reset" onclick="reset_image()">Reset</button>
					<button class="btn btn-primary btn-sm" type="button" onclick="go_back()">Go to View</button>
					</div>
				</div>
			</div>
		</div>
		
		</form>
				
</div>


</div> <!-- wizard container -->
</div>
</div>				
<div class="footer">
</div>
</div>
</div>
</body>
</html>

<?php	
$content=$data->content;	
$content=addslashes($content);
$content1=str_replace("\n", '', $content);
$string = trim(preg_replace('/\s+/', ' ', $content1));

$content=$data->content2;	
$content=addslashes($content);
$content1=str_replace("\n", '', $content);
$string2 = trim(preg_replace('/\s+/', ' ', $content1));

$content=$data->content3;	
$content=addslashes($content);
$content1=str_replace("\n", '', $content);
$string3 = trim(preg_replace('/\s+/', ' ', $content1));


?>

<script type="text/javascript">

function go_back(){
	location.href='<?php echo base_url()."admin/Ui_content/testimonial";?>';
}

function reset_image(){
	$("#wizardPictureTesti_picname").html('<i class="fa fa-upload fa-2x" aria-hidden="true"></i> Upload Image');
	$('#wizardPictureTesti').attr('src', '<?php echo base_url().$data->image; ?>');
	$('#commonPic').attr('style', ' ');
	$('input[name="image"]').val('');
	reset_editor();
}

function reset_editor(){
	str='<?php echo $string; ?>';
	data=decodeHtml(str);
	$('#content').siblings("div").children("div.Editor-editor").html(data);
	
	str2='<?php echo $string2; ?>';
	data2=decodeHtml(str2);
	$('#content2').siblings("div").children("div.Editor-editor").html(data2);
	
	str3='<?php echo $string3; ?>';
	data3=decodeHtml(str3);
	$('#content3').siblings("div").children("div.Editor-editor").html(data3);
}
function form_validation(){
	var data = $("#content").Editor("getText");
	$("#content").val(data);
	var text = $('textarea[name="content"]').val().trim();
	
	var data2 = $("#content2").Editor("getText");
	$("#content2").val(data2);
	var text2 = $('textarea[name="content2"]').val().trim();

	var data3 = $("#content3").Editor("getText");
	$("#content3").val(data3);
	var text3 = $('textarea[name="content3"]').val().trim();
	
	if(text==''){
		swal(
			'',
			'Please fill text',
			'error'
		)
	}
	var image = $('input[name="image"]').val().trim();
	if(image==''){
		swal(
			'',
			'Please upload Image',
			'error'
		)
	}
	
	
		var err = 0;
		
		if(err==0){
		
			var form_status = $('<div class="form_status"></div>');		
			
			
			var form = $('#edit_content');	
	
			/////////////////////////
			var form = $('#edit_content');	
			var form_create = document.forms.namedItem("edit_content");
			var ajaxData = new FormData(form_create);
			
			swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();
    $.ajax({
				url: '<?php echo base_url()."admin/Ui_content/update_testimonial/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false,

			}).done(function(data){	
				form_status.html('');

			if(data){
				swal({
					title:"Success", 
					text:"Content is successfully updated!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					go_back();

				});
			}
			if(data=="no")
			{
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
				
			});
  }
}]);
			
			////////////////////////
			
			}
	
	return false;
}

function showAvailableCategories(obj){
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						//data='<option value="" selected>--Select category---</option>'+data;
						$("#categories").html(data);
						$("#categories").html(data);
						//draw_table();
					}
					else{
						$("#categories").html('<option value=""></option>')
						
					}
				}
			});
	}else{
		$("#categories").html('<option value=""></option>')
	}
	
}

</script>


<script  type="text/javascript" src="<?php echo base_url();?>assets/richte/editor.js" ></script>
<script>

function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}
$(document).ready(function() {
	var textarea = $("#content");
	textarea.Editor();
	str='<?php echo $string; ?>';
	data=decodeHtml(str);
	$('#content').siblings("div").children("div.Editor-editor").html(data);
	
	var textarea2 = $("#content2");
	textarea2.Editor();
	str2='<?php echo $string2; ?>';
	data2=decodeHtml(str2);
	$('#content2').siblings("div").children("div.Editor-editor").html(data2);
	
	var textarea3 = $("#content3");
	textarea3.Editor();
	str3='<?php echo $string3; ?>';
	data3=decodeHtml(str3);
	$('#content3').siblings("div").children("div.Editor-editor").html(data3);

 });
 </script>