<html lang="en">
<head>
<title>Add Delivery Staff</title>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrapValidator.css"/>
<link href="<?php echo base_url();?>assets/css/datepicker.css" rel="stylesheet" /> 
<style>
#datetimePicker .has-feedback .form-control-feedback {
top: 0;
right: -20px;
}
#datetimePicker .has-feedback .input-group .form-control-feedback {
top: 0;
right: -30px;
}
[required]{
border-left:5px solid red;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js" ></script>
<script type="text/javascript">
// When the document is ready
$(document).ready(function () {
$('#dob').datepicker({
format: "dd/mm/yyyy"
});  

});
</script>

<script>
$(document).ready(function(){	
    $('#add_subadmins_form').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'The Name is required and cannot be empty'
                    },
					stringLength: {
                        min: 3,
                        max: 15,
                        message: 'The Name must be more than 3 and less than 15 characters long'
                    },
                    regexp: {
                        regexp: /^[A-Z\s]+$/i,
                        message: 'The Name can only consist of alphabetical characters and spaces'
                    }
                }
            },
			email: {
				validators: {
					notEmpty: {
						message: ''
					},
					emailAddress: {
						message: ''
					},
					remote: {
						type: 'POST',
						url: '<?php echo base_url()?>admin/Admin_manage/check_email_subadmins',
						message: 'The email is already in use',
						delay: 2000
					}
				}
			},
			mobile: {
				validators: {
					digits: {
						message: 'The value can contain only digits'
					},
					stringLength: {
						min: 10,
						max: 12,
						message: 'The Mobile must be more than 10 and less than 12 digits long'
					},
					notEmpty: {
						message: 'The Mobile must not be empty'
					},
					remote: {
						type: 'POST',
						url: '<?php echo base_url()?>admin/Admin_manage/check_mobile_subadmins',
						message: 'The mobile is already in use',
						delay: 2000
					}
				}
            },
			landline: {
				validators: {
					digits: {
						message: 'The value can contain only digits'
					},
					stringLength: {
						min: 10,
						max: 15,
						message: 'The Landline must be more than 10 and less than 15 digits long'
					}
				}
            },
			dob: {
                validators: {
                    notEmpty: {
                        message: 'The DOB is required'
                    }
                }
            },
			user_type: {
                validators: {
                    notEmpty: {
                        message: 'The User Type is required'
                    }
                }
            },
			panel_access: {
                validators: {
                    notEmpty: {
                        message: 'The Panel Access is required'
                    }
                }
            },
			staff_profile_pic: {
                validators: {
                    notEmpty: {
                        message: 'The Profile pic is required'
                    }
                }
            }
        }
    })
	.on('success.form.bv', function(e) {
	
		
    var form = $('#add_subadmins_form');
	form.submit(function(event){
		event.preventDefault();
		
				var valuesToSubmit = $(this).serialize();
				
				$.ajax({
						url: "<?php echo base_url();?>admin/Admin_manage/add_subadmins_action",
						type:"POST",
						data: new FormData(this),
						contentType: false,      
						cache: false,				
						processData:false,
						beforeSend: function(){
							$("#upload_subadmin_profile_btn").html('<i class="fa fa-refresh fa-spin"></i> Processing');
						}
					}).done(function(data){
						$("#upload_subadmin_profile_btn").html('Submit');
						//alert(data);
						if(data==true){
							alert("profile created successfully!");
							location.href="<?php echo base_url();?>admin/Admin_manage/view_subadmins";
						}
						else{
							alert("profile not yet created!");
						}
						
					});
				
	});
   });
		 });
</script>
</head>
<body>
<div class="container-fluid">
<div class="row-fluid">
<div class="page-header"><h4>Add Sub Admin For Country</h4></div>
<div class="col-sm-12">
<form id="add_subadmins_form" enctype="multipart/form-data" class="form-horizontal" method="post">
<div class="form-group">
<div class="col-sm-6">
<input type="text" class="form-control" name="name" placeholder="Enter Name" required>
</div>
</div>
<div class="form-group">
<div class="col-sm-6">
<input type="text" class="form-control" name="email" placeholder="Enter Email" required>
</div>
</div>
<div class="form-group">
<div class="col-sm-6">
<input type="text" class="form-control" name="password" placeholder="Enter Password" required>
</div>
</div>
<div class="form-group">
<div class="col-sm-6">
<input type="text" class="form-control" name="mobile" placeholder="Enter Mobile" required>
</div>
</div>
<div class="form-group">
<div class="col-sm-6">
<input type="text" class="form-control" name="landline" placeholder="Enter Landline">
</div>
</div>
<div class="form-group">
<div class="col-sm-6">
<input type="radio" name="gender"  value="Male" checked> Male
<input type="radio" name="gender"  value="Female"> Female
</div>
</div>
<div class="form-group">
<div class="col-sm-6">
<input type="text" class="form-control" name="dob" id="dob"  placeholder="Date of Birth" value="" required>
</div>
</div>
<div class="form-group">
	<div class="col-sm-6">
		<select name="user_type" class="form-control" required>
			<option value="">-Select User Type-</option>
			<option value="Sub World">Sub Admin For World</option>
			<option value="Master Country">Master Admin For Country</option>
			<option value="Sub Country">Sub Admin For Country</option>
		</select>
	</div>
</div>
<div class="form-group">
<div class="col-sm-6">
	<select name="panel_access[]" class="form-control" required multiple>
			<option value="1">Orders</option>
			<option value="2">Complains</option>
			<option value="3">Finance</option>
		</select>
</div>
</div>




<div class="form-group">
<div class="col-sm-6">
<input type="file" name="profile_pic"  id="profile_pic" class="form-control" required/>
</div>
</div>

<div class="form-group">
<div class="col-sm-3">
<button type="submit" class="col-md-12 btn btn-success btn-lg" id="upload_subadmin_profile_btn">Submit</button>
</div>
</div>
</form>
</div>
</div>
</div>
</body>

</html>