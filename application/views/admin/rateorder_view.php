<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 
	
</head>
<body>
<script type="text/javascript">
var table;

$(document).ready(function (){

	$("#reset_form_button").click(function() {
		$(this).closest('form').find("input[type=text]").val("");
		$(this).closest('form').find("select").val("");
	});
	
	/*var from_date_obj=$('#from_date').datepicker({
	format: "yyyy/mm/dd"
	}).on('changeDate', function(ev) {
		from_date_obj.hide();
	}).data('datepicker');

	var to_date_obj=$('#to_date').datepicker({
	format: "yyyy/mm/dd"
	}).on('changeDate', function(ev) {
		to_date_obj.hide();
	}).data('datepicker');

	var delivered_date_from_obj=$('#delivered_date_from').datepicker({
	format: "yyyy/mm/dd"
	}).on('changeDate', function(ev) {
		delivered_date_from_obj.hide();
	}).data('datepicker');  	
	var delivered_date_to_obj=$('#delivered_date_to').datepicker({
	format: "yyyy/mm/dd"
	}).on('changeDate', function(ev) {
		delivered_date_to_obj.hide();
	}).data('datepicker'); */ 	

		$('#to_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY'
			});
			
		$('#from_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY', 
				shortTime : true
			}).on('change', function(e, date)
			{
				$('#to_date').bootstrapMaterialDatePicker('setMinDate', date);
			});
			
		$('#delivered_date_to').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY'
			});
			
		$('#delivered_date_from').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY', 
				shortTime : true
			}).on('change', function(e, date)
			{
				$('#delivered_date_to').bootstrapMaterialDatePicker('setMinDate', date);
			});	
		
    var rows_selected = [];
     table = $('#table_rateorder').DataTable({
	  
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Ratemanaging/ratings_for_service_processing",
			data: function (d) { d.rating = $('#rating').val(); d.from_date = $('#from_date').val(); d.to_date = $('#to_date').val();d.delivered_date_from = $('#delivered_date_from').val();d.delivered_date_to = $('#delivered_date_to').val(); d.pcat_id = $('#parent_category').val(); d.cat_id = $('#categories').val(); d.subcat_id = $('#subcategories').val(); d.brand_id = $('#brands').val(); d.product_id = $('#products').val(); d.inventory_id = $('#inventory').val();},
            type: "post",
            error: function(){
              $("#table_rateorder_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				
				document.getElementById("rateorder_count").innerHTML=json.recordsFiltered;
				
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
  
  
	$("#from_date").bootstrapMaterialDatePicker().on('change', function(e, date) {
		table.draw();
	});

	$("#to_date").bootstrapMaterialDatePicker().on('change', function(e, date) {
		table.draw();
	});
	$("#delivered_date_from").bootstrapMaterialDatePicker().on('change', function(e, date) {
		table.draw();
	});
	$("#delivered_date_to").bootstrapMaterialDatePicker().on('change', function(e, date) {
		table.draw();
	});

    $("#rating,#sku_id,#from_date,#to_date,#delivered_date").on("keyup", function(){
		table.draw();});
	
	$('#reset_form_button').on('click',function(){
		table.draw();});
	$("#submit_button").click(function() {
		rating=$('#rating').val();
		if(rating!='' && rating!=null){
			table.draw();
		}else{
			alert('Please Enter Rating Value');
		}
	});
	
	$('#back').on('click',function(){
		//location.href="<?php echo base_url(); ?>admin/Ratemanaging/rated_inventory_filter/";
		var form = document.getElementById('search_for_rateorer');		
		form.action='<?php echo base_url()."admin/Ratemanaging/rateorder_filter"; ?>';
		form.submit();
	});
	
	/*$('#search_toggle').on('click',function(){	
		$('#search_block').toggle();
	});*/
	
	
});	

function showSurveyFun(rateorder_id,inventory_id,msg){

	$.ajax({
		url:"<?php echo base_url()?>admin/Ratemanaging/show_survey_result",
		type:"post",
		data:{rateorder_id:rateorder_id,inventory_id:inventory_id},
		success:function(data){
			$("#survey_result").html(data);
		}
	});
} 
</script>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container" id="get">
<div class="page-header">
<h4 class="text-center bold">Service ratings count <span class="badge"><div id="rateorder_count"></div></span> <a class="text-info bold" id="back" style="cursor:pointer">Go Back</a></h4>

</div>
	
	

<div class="row">
<table id="table_rateorder" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
			<th colspan="6">
			<form name="search_for_rateorer" method="post" id="search_for_rateorer">

		<input type="hidden" value="<?php echo $pcat_id; ?>" name="pcat_id" id="parent_category">
		<input type="hidden" value="<?php echo $cat_id; ?>" name="cat_id" id="categories">
		<input type="hidden" value="<?php echo $subcat_id; ?>" name="subcat_id" id="subcategories">
		<input type="hidden" value="<?php echo $brand_id; ?>" name="brand_id" id="brands">
		<input type="hidden" value="<?php echo $product_id; ?>" name="product_id" id="products">
		<input type="hidden" value="<?php echo $inventory_id; ?>" name="inventory_id" id="inventory">
		
	
		<div class="row">
			<div class="col-md-2">
				 <select name="rating" id="rating" class="form-control search_select" onchange="showRate(this)">
						<option value="">Search By Ratings</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
				</select>
			</div>
			
			<div class="col-md-2">
				 <input type="text" class="form-control" name="from_date" value="" id="from_date" placeholder="Order Date(From)">
			</div>
			<div class="col-md-2">
				 <input type="text" class="form-control" name="to_date" value="" id="to_date" placeholder="Order Date(To)">
			</div>
			
			<div class="col-md-2">
				 <input type="text" class="form-control" name="delivered_date_from" value="" id="delivered_date_from" placeholder="Delivered Date(From)">
			</div>
			<div class="col-md-2">
				 <input type="text" class="form-control" name="delivered_date_to" value="" id="delivered_date_to" placeholder="Delivered Date(To)">
			</div>	
		<div class="col-md-2">
			<button type="button" class="btn btn-info btn-xs" id="submit_button">Submit</button>
			<button class="btn btn-warning btn-xs" type="reset" id="reset_form_button">Reset</button>
			</div>
		</div>
		
		
	</form>
			</th>
			</tr>
			<tr>
            <th class="text-primary small bold">Customer</th>
            <th class="text-primary small bold">Order Item Id</th>
            <th class="text-primary small bold">Delivered On</th>
			 <th class="text-primary small bold">Rating</th>
            <th class="text-primary small bold">Logistics</th>      
            <th class="text-primary small bold">View Details</th>
       		</tr>
        </thead>
  </table>
</div>

				 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Survey Result</h4>
        </div>
        <div class="modal-body">
          
			<div class="row">
	
				<div class="col-md-10"  id="survey_result">
				
				
				</div>
			</div>
					</div>
					<div class="modal-footer">
					  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
					
				  </div>
				  
				</div>
  </div>
   <!-- Modal -->


</div>
</div>
</body>
</html>

<script type="text/javascript">
function showRate(obj){
	table.draw();	
}
function showAvailableRatings(obj){
	table.draw();	
}
function showAvailableInventory(obj){
	
	if(obj.value!="" && obj.value!="None"){
		product_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_inventory",
				type:"post",
				data:"product_id="+product_id,
				success:function(data){
					if(data!=0){
						$("#inventory").html(data);
						table.draw();
					}
					else{
						$("#inventory").html('<option value="">-None-</option>');
					}
				}
			});
	}
	
}
function showAvailableProducts(obj){
	
	if(obj.value!="" && obj.value!="None"){
		brand_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_products",
				type:"post",
				data:"brand_id="+brand_id,
				success:function(data){
					if(data!=0){
						$("#products").html(data);
						table.draw();
					}
					else{
						$("#products").html('<option value="">-None-</option>');
					}
				}
			});
	}
	
}
function showAvailableBrands(obj){
	
	if(obj.value!="" && obj.value!="None"){
		subcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_brands",
				type:"post",
				data:"subcat_id="+subcat_id,
				success:function(data){
					if(data!=0){
						$("#brands").html(data);
						table.draw();
					}
					else{
						$("#brands").html('<option value="">-None-</option>');

					}
				}
			});
	}
	
}
function showAvailableSubCategories(obj){
	
	if(obj.value!="" && obj.value!="None"){
		cat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id,
				success:function(data){
					if(data!=0){
						$("#subcategories").html(data);
						table.draw();
					}
					else{
						$("#subcategories").html('<option value="">-None-</option>')
						$("#brands").html('<option value="">-None-</option>')

					}
				}
			});
	}
	
}

function showAvailableCategories(obj){
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id,
				success:function(data){
					if(data!=0){
						$("#categories").html(data);
						table.draw();
					}
					else{
						$("#categories").html('<option value="">-None-</option>')
						$("#brands").html('<option value="">-None-</option>')

					}
				}
			});
	}
	
}
</script>

