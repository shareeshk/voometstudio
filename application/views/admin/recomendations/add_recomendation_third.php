<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/datepicker/bootstrap-datetimepicker.css" />
      
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url();?>assets/datepicker/moment.js"></script>
    <script src="<?php echo base_url();?>assets/datepicker/bootstrap-datetimepicker.js"></script>

  <script src="<?php echo base_url()?>assets/jqueryui/jquery-ui.min.js"></script>
  <style>
.popover{
   border:none;
   border-radius:unset;
   
   width:100%;

   overflow-wrap:break-word;
}
.btn{
    border-radius:unset;
}

.btn-round-lg{
border-radius: 22.5px;
}
.btn-round{
border-radius: 17px;
}
.btn-round-sm{
border-radius: 15px;
}
.btn-round-xs{
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
}

.popover-content {
   height: 200px;
   overflow-y: scroll;
}
.table {
    font-size:.9em;
    margin-bottom: 0px;
}


/* Tooltip styling */

[data-tooltip]:after,
[data-tooltip]:before {
    content: '';
    display: none;
    font-size: .75em;
    position: absolute;
}
[data-tooltip]:after {
    border-top: .5em solid #222;
    border-top: .5em solid hsla(0,0%,0%,.9);
    border-left: .5em solid transparent;
    border-right: .5em solid transparent;
    height: 0;
    left: 40%;
    width: 0;
    z-index: 2000;
}
[data-tooltip]:before {
    background-color: #222;
    background-color: hsla(0,0%,0%,.9);
    border-radius: .25em;
    color: #f6f6f6;
    content: attr(data-tooltip);
    font-family: sans-serif;
    left: 0;
    padding: .25em .75em;
    white-space: nowrap;
}
[data-tooltip]:hover:after,
[data-tooltip]:hover:before {
    display: block;
}
[data-tooltip]:hover:after {
    top: -.5em;
}
[data-tooltip]:hover:before {
    top: -2.5em;
}


h2{
    margin-top: 0px;
    margin-bottom: 0px;
}
.page-header{
    padding-bottom: 10px; 
    margin: 0px; 
    border-bottom: 0px solid #eee; 
}
    ul.listack > li {
    display: inline-block;
    /* You can also add some margins here to make it look prettier */
    zoom:1;
    display:inline;
    /* this fix is needed for IE7- */
   margin-right: 5%;
}

.material-button {
    position: relative;
    top: 0;
    z-index: 1;
    width: 30px;
    height: 30px;
    color: #fff;
    border: none;
    border-radius: 70%;
    box-shadow: 0 3px 6px rgba(0,0,0,.275);
    outline: none;
}
.btn-xsm{
  width: 25px;
  height: 25px;
  border-radius: 60%;  
}
.list-left li, .list-right li {
    cursor: pointer;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #000;
    }
.nav-tabs>li>a {
    color: #000;
}
.nav-tabs {
    border-bottom: 1px solid #cccccc;
}

.panel-heading .accordion-toggle:after {
    /* symbol for "opening" panels */
    font-family: 'Glyphicons Halflings';  /* essential for enabling glyphicon */
    content: "\e114";    /* adjust as needed, taken from bootstrap.css */
    float: left;        /* adjust as needed */
    color: grey;         /* adjust as needed */
}
.panel-heading .accordion-toggle.collapsed:after {
    /* symbol for "collapsed" panels */
    content: "\e080";    /* adjust as needed, taken from bootstrap.css */
}
.inline_input{
 display: inline-block;
 width:50%!important   
}
.list-group{
	height:300px;
	overflow:auto;
}
</style>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
  
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="page-header">
  <h4 class="text-danger text-center">Add 3rd Degree Recomendation for <?php echo $recomendation_items ?></h4>
  </div>
     <form class="form-horizontal" action="<?php echo base_url()?>admin/Recomendations/add_items_tagged_under_recomendations/<?php echo $recomendation_uids ?>" onsubmit="return check_before_submit()" id="form_for_rec_tagg" method="post">
                <input type="hidden" name="level_recomendation" value="third_level">
                <div class="form-group">
                    <label class="control-label col-md-2" for="pwd">Tagged Item Selected:</label>
                    <div class="col-md-10">
                        <ul class="listack" id="item_picker_del_freeItem">
                        
                        
                        </ul>
                    </div>
                 </div>
                 
                    
                 
                   <div class="form-group">
                        <div class="col-md-5">
                              <select class="form-control" name="parent_category" id="parent_category_selector_freeItem" onchange="get_category_freeItem()">
                                <option value="">Choose Parent Category</option>
                                <?php foreach($parent_category as $catdata){?>
                                    <option value="<?php echo $catdata['pcat_id'] ?>"><?php echo $catdata['pcat_name'] ?></option>
                                <?php } ?>
								<option value="0">-None-</option>
                              </select>
                        </div>
                   </div>
                   <div class="form-group" id="CategoryContainer_freeItem">
                       
                   </div>
                   <div class="form-group" id="subCategoryContainer_freeItem">

                   </div>
                   <div class="form-group" id="brandContainer_freeItem">

                   </div>
                   <div class="form-group" id="attributeContainer_freeItem">

                   </div>
                   <div class="form-group" id="productContainer_freeItem">

                   </div>
                   <div class="form-group" id="productTypeContainer_freeItem">

                   </div>
                   <div class="form-group">
                <b class="lead pull-right" id="collection_of_all_sku_to_buy_freeItem"></b>
                 </div>

                    <div class="row" id="product_filter_level_selector_freeItem" style="display:none">
                        <div class="dual-list list-right col-md-6">
                            <div class="well">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <span class="input-group-addon glyphicon glyphicon-search" style="top: 0px;"></span> 
                                            <input type="text" id="right_list_search_freeItem" name="SearchDualList" class="form-control" placeholder="search"> 
                                            <span class="input-group-addon glyphicon glyphicon-unchecked selector" style="cursor: pointer; top: 0px;" title="Select All"></span>
                                            <span class="input-group-addon glyphicon glyphicon-plus move-left" style="cursor: pointer; top: 0px;" title="Add Selected"></span>
                                        </div>
                                    </div>
                                </div>
                                <ul class="list-group" id="dual-list-right_freeItem">
                                    

                                </ul>
                            </div>
                        </div>
                
                
                        <div class="dual-list list-left col-md-6">
                            <div class="well text-right">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <span class="input-group-addon glyphicon glyphicon-search" style="top: 0px;"></span> 
                                            <input type="text" id="left_list_search_freeItem" name="SearchDualList" class="form-control" placeholder="search"> 
                                            <span class="input-group-addon glyphicon glyphicon-unchecked selector" style="cursor: pointer; top: 0px;" title="Select All"></span>
                                            <span class="input-group-addon glyphicon glyphicon-minus move-right" style="cursor: pointer; top: 0px;" title="Remove Selected"></span>
                                        </div>
                                    </div>
                                </div>
                                
                                <ul class="list-group" id="dual-list-left_freeItem"></ul>
                            </div>
                        </div>
                        
                    </div>
<input type="hidden" name="products_tagged_for_recomendation" id="products_tagged_for_recomendation"/>     
<input type="hidden" name="override_previous_recomendation" id="override_previous_recomendation" value="0"/>               
  </form>                   
    
           
   

</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Message</h4>
      </div>
      <div class="modal-body" id="already_policy_display">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="continueFun()">Continue With Overide</button>
        <button type="button" class="btn btn-warning" data-dismiss="modal" onclick="resonsiderFun()">Reconsider</button>
      </div>
    </div>

</div>
<script>


$(document).ready(function(){
    $("#myModal").on("hidden.bs.modal", function () {
        resonsiderFun()
    });
})

function resonsiderFun(){
    document.getElementById('override_previous_recomendation').value="0";
}
function continueFun(){
    document.getElementById("form_for_rec_tagg").submit();
}

    function check_before_submit(){
        
        var allselectElements=document.getElementsByTagName('select');
        var recomenation_uid='<?php echo $recomendation_uids ?>';
        var products_tagged_for_recomendation=document.getElementById('products_tagged_for_recomendation').value;
        
        //alert($("select:last option:selected").val())
        var i;
        var selected_attr_one,selected_level_one,selected_attr_two,selected_level_two,selected_attr_three,selected_level_three,selected_attr_four,selected_level_four,selected_attr_five,selected_level_five='';
        
        for(var i=0;i<allselectElements.length;i++){
            if(allselectElements[i].options[allselectElements[i].selectedIndex].value){
                if(i==0){
                    selected_attr_one=allselectElements[i].options[allselectElements[i].selectedIndex].value
                    selected_level_one=allselectElements[i].getAttribute('name');
                }
                if(i==1){
                    selected_attr_two=allselectElements[i].options[allselectElements[i].selectedIndex].value
                    selected_level_two=allselectElements[i].getAttribute('name');
                }
                if(i==2){
                    selected_attr_three=allselectElements[i].options[allselectElements[i].selectedIndex].value
                    selected_level_three=allselectElements[i].getAttribute('name');
                }
                if(i==3){
                    selected_attr_four=allselectElements[i].options[allselectElements[i].selectedIndex].value
                    selected_level_four=allselectElements[i].getAttribute('name');
                }
                if(i==4){
                    selected_attr_five=allselectElements[i].options[allselectElements[i].selectedIndex].value
                    selected_level_five=allselectElements[i].getAttribute('name');
                }
                
            } 
        }

        /*for(i=allselectElements.length-1;i;i--){
            if(allselectElements[i].options[allselectElements[i].selectedIndex].value){
                selected_attr=allselectElements[i].options[allselectElements[i].selectedIndex].value
                selected_level=allselectElements[i].getAttribute('name');
                break;
            } 
        }*/
        
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                           if(xhr.responseText!=0){
                               var dats=JSON.parse(xhr.responseText);
                               document.getElementById('already_policy_display').innerHTML=dats['uls'] +"<div class='row'><div class='col-md-12'><br>Continuing will overide previous recomendation</div></div>"
                               $('#myModal').modal('show');
                               document.getElementById('override_previous_recomendation').value=dats['skus'];
                           }
                           else{
                               continueFun()
                           }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Recomendations/getExisting_recomendation_tagged", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('selected_attr_one='+selected_attr_one+"&selected_level_one="+selected_level_one+'&selected_attr_two='+selected_attr_two+"&selected_level_two="+selected_level_two+'&selected_attr_three='+selected_attr_three+"&selected_level_three="+selected_level_three+'&selected_attr_four='+selected_attr_four+"&selected_level_four="+selected_level_four+'&selected_attr_five='+selected_attr_five+"&selected_level_five="+selected_level_five+"&recomenation_uid="+recomenation_uid+"&products_tagged_for_recomendation="+products_tagged_for_recomendation);
                }
          return false;      
    }




function update_buy_promo_list_freeItem(){
    if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
        
        var uls = document.getElementById("dual-list-left_freeItem");
            var listItem = uls.getElementsByTagName("li");
            
            var newNums ="";
            var skuIds=[];
            
            for (var i=0; i < listItem.length; i++) {
                newNums+=(  listItem[i].getAttribute('data-value-name') )+"<br>" ;
                skuIds.push(  listItem[i].getAttribute('sku-id') ) ;
            }
        document.getElementById('products_tagged_for_recomendation').value=skuIds
        document.getElementById('collection_of_all_sku_to_buy_freeItem').innerHTML="";
        var buttonElement=document.createElement ("button")
            buttonElement.innerHTML="Selected "+listItem.length+" SKU"
            buttonElement.type="button"
            buttonElement.classList.add('btn','btn-success','btn-sm');
            buttonElement.setAttribute('data-popover','true')
            buttonElement.setAttribute('data-html',true)
            buttonElement.setAttribute('data-content',newNums)
            //document.getElementById('collection_of_all_sku_to_buy_freeItem').appendChild (buttonElement);
            var buttonSubmit=document.createElement ("button")
            buttonSubmit.innerHTML="Tag Item For Recomendation"
            buttonSubmit.type="submit";
            buttonSubmit.setAttribute("style","margin-left:10px");
            buttonSubmit.classList.add('btn','btn-info');
            
            document.getElementById('collection_of_all_sku_to_buy_freeItem').appendChild (buttonSubmit);
    }
    else{
        document.getElementById('collection_of_all_sku_to_buy_freeItem').innerHTML="Nothing Selected"
    }
}

    function remove_category_freeItem(){
        document.getElementById('parent_category_selector_freeItem').value=""
        document.getElementById('CategoryContainer_freeItem').innerHTML=""
        document.getElementById('parentCatSel_freeItem').remove();
            if(document.getElementById('subCatSel_freeItem')!=null){
                document.getElementById('subCatSel_freeItem').remove();
            }
            if(document.getElementById('brand_sel_freeItem')!=null){
                document.getElementById('brand_sel_freeItem').remove();
            }
            if(document.getElementById('attribute_sel_freeItem')!=null){
                document.getElementById('attribute_sel_freeItem').remove();
            }
            if(document.getElementById('product_sel_freeItem')!=null){
                document.getElementById('product_sel_freeItem').remove();
            }

            var parentDiv = document.getElementById('subCategoryContainer_freeItem');
            parentDiv.innerHTML=""
            if(document.getElementById('brandContainer_freeItem').innerHTML!=null){
                document.getElementById('brandContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('attributeContainer_freeItem').innerHTML!=null){
                document.getElementById('attributeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productContainer_freeItem').innerHTML!=null){
                document.getElementById('productContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
                document.getElementById('productTypeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
                document.getElementById('dual-list-right_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
                document.getElementById('dual-list-left_freeItem').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
                document.getElementById('product_filter_level_selector_freeItem').style.display="none"
            }
            if(document.getElementById('collection_of_all_sku_to_buy_freeItem')!=null){
                document.getElementById('collection_of_all_sku_to_buy_freeItem').innerHTML="";
            }
    }

    function remove_subcategory_freeItem(){
            document.getElementById('category_selector_freeItem').value=""
            document.getElementById('subCatSel_freeItem').remove();
            if(document.getElementById('brand_sel_freeItem')!=null){
                document.getElementById('brand_sel_freeItem').remove();
            }
            if(document.getElementById('attribute_sel_freeItem')!=null){
                document.getElementById('attribute_sel_freeItem').remove();
            }
            if(document.getElementById('product_sel_freeItem')!=null){
                document.getElementById('product_sel_freeItem').remove();
            }

            var parentDiv = document.getElementById('subCategoryContainer_freeItem');
            parentDiv.innerHTML=""
            if(document.getElementById('brandContainer_freeItem').innerHTML!=null){
                document.getElementById('brandContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('attributeContainer_freeItem').innerHTML!=null){
                document.getElementById('attributeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productContainer_freeItem').innerHTML!=null){
                document.getElementById('productContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
                document.getElementById('productTypeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
                document.getElementById('dual-list-right_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
                document.getElementById('dual-list-left_freeItem').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
                document.getElementById('product_filter_level_selector_freeItem').style.display="none"
            }
            if(document.getElementById('collection_of_all_sku_to_buy_freeItem')!=null){
                document.getElementById('collection_of_all_sku_to_buy_freeItem').innerHTML="";
            }
            update_buy_promo_list_freeItem()
    }
    function remove_brand_freeItem(){
        document.getElementById('sub_category_selector_freeItem').value=""
            document.getElementById('brand_sel_freeItem').remove();

            if(document.getElementById('attribute_sel_freeItem')!=null){
                document.getElementById('attribute_sel_freeItem').remove();
            }
            if(document.getElementById('product_sel_freeItem')!=null){
                document.getElementById('product_sel_freeItem').remove();
            }

            if(document.getElementById('brandContainer_freeItem').innerHTML!=null){
                document.getElementById('brandContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('attributeContainer_freeItem').innerHTML!=null){
                document.getElementById('attributeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productContainer_freeItem').innerHTML!=null){
                document.getElementById('productContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
                document.getElementById('productTypeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
                document.getElementById('dual-list-right_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
                document.getElementById('dual-list-left_freeItem').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
                document.getElementById('product_filter_level_selector_freeItem').style.display="none"
            }
            if(document.getElementById('collection_of_all_sku_to_buy_freeItem')!=null){
                document.getElementById('collection_of_all_sku_to_buy_freeItem').innerHTML="";
            }
            update_buy_promo_list_freeItem()
        
    }
    function remove_product_freeItem(){
        document.getElementById('attribute_selector_freeItem').value=""
            document.getElementById('attribute_sel_freeItem').remove();

            if(document.getElementById('product_sel_freeItem')!=null){
                document.getElementById('product_sel_freeItem').remove;
            }

            if(document.getElementById('attributeContainer_freeItem').innerHTML!=null){
                document.getElementById('attributeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productContainer_freeItem').innerHTML!=null){
                document.getElementById('productContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
                document.getElementById('productTypeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
                document.getElementById('dual-list-right_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
                document.getElementById('dual-list-left_freeItem').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
                document.getElementById('product_filter_level_selector_freeItem').style.display="none"
            }
            if(document.getElementById('collection_of_all_sku_to_buy_freeItem')!=null){
                document.getElementById('collection_of_all_sku_to_buy_freeItem').innerHTML="";
            }
            update_buy_promo_list_freeItem()
    }
    function remove_product_variant_freeItem(){
        document.getElementById('product_selector_freeItem').value=""
            document.getElementById('product_sel_freeItem').remove();


            if(document.getElementById('productContainer_freeItem').innerHTML!=null){
                document.getElementById('productContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
                document.getElementById('productTypeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
                document.getElementById('dual-list-right_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
                document.getElementById('dual-list-left_freeItem').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
                document.getElementById('product_filter_level_selector_freeItem').style.display="none"
            }
            if(document.getElementById('collection_of_all_sku_to_buy_freeItem')!=null){
                document.getElementById('collection_of_all_sku_to_buy_freeItem').innerHTML="";
            }
            update_buy_promo_list_freeItem()
    }

        function get_category_freeItem(){
        var parentCatval=document.getElementById('parent_category_selector_freeItem').value
        var x = document.getElementById("parent_category_selector_freeItem");
        var parentCatText=x.options[x.selectedIndex].text
        if(document.getElementById('parentCatSel_freeItem')!=null){
            remove_category_freeItem()
        }
        document.getElementById('parent_category_selector_freeItem').value=parentCatval
        document.getElementById("parent_category_selector_freeItem").options[x.selectedIndex].text=parentCatText
            var parentDiv = document.getElementById('CategoryContainer_freeItem');
            parentDiv.innerHTML=""
            if(document.getElementById('subCategoryContainer_freeItem').innerHTML!=null){
                document.getElementById('subCategoryContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('brandContainer_freeItem').innerHTML!=null){
                document.getElementById('brandContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('attributeContainer_freeItem').innerHTML!=null){
                document.getElementById('attributeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productContainer_freeItem').innerHTML!=null){
                document.getElementById('productContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
                document.getElementById('productTypeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
                document.getElementById('dual-list-right_freeItem').innerHTML=""
            }
            
            if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
                document.getElementById('dual-list-left_freeItem').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
                document.getElementById('product_filter_level_selector_freeItem').style.display="none"
            }

        if(parentCatval!=""){         
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                    var select_data=JSON.parse(xhr.responseText);
                                    /*var labelElement=document.createElement ("label")
                                    labelElement.classList.add('control-label','col-md-2');
                                    labelElement.innerHTML="Category:"
                                    parentDiv.appendChild (labelElement);*/
                                    
                                    if(select_data.length>0){                                   
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-5');
                                        divElement.setAttribute('id','ParentCategorySelect_freeItem')
                                        parentDiv.appendChild (divElement);
                                        
                                        var selectDiv=document.getElementById('ParentCategorySelect_freeItem');
                                        var selectElement = document.createElement ("select");
                                        selectElement.classList.add("form-control");
                                        selectElement.setAttribute('id','category_selector_freeItem');
                                        selectElement.setAttribute('onchange','get_subcategory_freeItem()');
                                        selectElement.name="category";
                                        var option = new Option ("Choose Category","");
                                        selectElement.options[selectElement.options.length] = option;
                                        for (var i=0;i < select_data.length;i++)
                                        {
                                            
                                            var option = new Option (select_data[i].cat_name,select_data[i].cat_id);
                                            selectElement.options[selectElement.options.length] = option;
                                        }
                                        selectDiv.appendChild (selectElement);
                                        if(document.getElementById('parentCatSel_freeItem')==null){
                                            var list_stack_disp=document.getElementById('item_picker_del_freeItem')
                                            var list_level_span = document.createElement ("li");
                                            list_level_span.innerHTML=parentCatText +'&nbsp';
                                            list_level_span.setAttribute('id',"parentCatSel_freeItem");
                                            list_level_span.setAttribute("selector_li_name",parentCatText)
                                            list_stack_disp.appendChild (list_level_span);
                                            var btncon=document.getElementById("parentCatSel_freeItem")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',"parentCatSel_freeItem"+parentCatval);
                                            list_level_button.setAttribute('onclick','remove_category_freeItem()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                        }
                                        else{
                                            document.getElementById('parentCatSel').innerHTML=parentCatText
                                            document.getElementById('parentCatSel').setAttribute("selector_li_name",parentCatText)
                                            var btncon=document.getElementById("parentCatSel")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',"parentCatSel_freeItem"+parentCatval);
                                            list_level_button.setAttribute('onclick','remove_category_freeItem()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                        }       
                                      }
                                     else{
                                        if(document.getElementById('parentCatSel_freeItem')){
                                            document.getElementById('parentCatSel_freeItem').remove();
                                        }
                                        if(document.getElementById('subCatSel_freeItem')){
                                            document.getElementById('subCatSel_freeItem').remove();
                                        }
                                        if(document.getElementById('brand_sel_freeItem')){
                                            document.getElementById('brand_sel_freeItem').remove();
                                        }
                                        if(document.getElementById('brand_sel_freeItem')){
                                            document.getElementById('brand_sel_freeItem').remove();
                                        }
                                        if(document.getElementById('attribute_sel_freeItem')){
                                            document.getElementById('attribute_sel_freeItem').remove();
                                        }
                                        if(document.getElementById('product_sel_freeItem')){
                                            document.getElementById('product_sel_freeItem').remove();
                                        }
                                        var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-5');
                                        labelElement.setAttribute('style','text-align: left')
                                        labelElement.innerHTML="No products to show"
                                        parentDiv.appendChild (labelElement);
                                    } 
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_category", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send("pcat_id="+parentCatval);
                }
        }
    }
    
    function get_subcategory_freeItem(){
        
        var catval=document.getElementById('category_selector_freeItem').value
        var x = document.getElementById("category_selector_freeItem");
        var catText=x.options[x.selectedIndex].text
        if(document.getElementById('subCatSel_freeItem')!=null){
            remove_subcategory_freeItem()
        }
        document.getElementById('category_selector_freeItem').value=catval
        document.getElementById("category_selector_freeItem").options[x.selectedIndex].text=catText
            var parentDiv = document.getElementById('subCategoryContainer_freeItem');
            parentDiv.innerHTML=""
            if(document.getElementById('brandContainer_freeItem').innerHTML!=null){
                document.getElementById('brandContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('attributeContainer_freeItem').innerHTML!=null){
                document.getElementById('attributeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productContainer_freeItem').innerHTML!=null){
                document.getElementById('productContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
                document.getElementById('productTypeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
                document.getElementById('dual-list-right_freeItem').innerHTML=""
            }
            
            if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
                document.getElementById('dual-list-left_freeItem').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
                document.getElementById('product_filter_level_selector_freeItem').style.display="none"
            }

        if(catval!=""){         
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                    var select_data=JSON.parse(xhr.responseText);
                                    /*var labelElement=document.createElement ("label")
                                    labelElement.classList.add('control-label','col-md-2');
                                    labelElement.innerHTML="Sub Category:"
                                    parentDiv.appendChild (labelElement);*/
                                    
                                    if(select_data.length>0){
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-5');
                                        divElement.setAttribute('id','subCategorySelect_freeItem')
                                        parentDiv.appendChild (divElement);
                                        
                                        var selectDiv=document.getElementById('subCategorySelect_freeItem');
                                        var selectElement = document.createElement ("select");
                                        selectElement.classList.add("form-control");
                                        selectElement.setAttribute('id','sub_category_selector_freeItem');
                                        selectElement.setAttribute('onchange','get_brands_freeItem()');
                                        selectElement.name="sub_category";
                                        var option = new Option ("Choose Sub Category","");
                                        selectElement.options[selectElement.options.length] = option;
                                        for (var i=0;i < select_data.length;i++)
                                        {
                                            
                                            var option = new Option (select_data[i].subcat_name,select_data[i].subcat_id);
                                            selectElement.options[selectElement.options.length] = option;
                                        }
                                        selectDiv.appendChild (selectElement);
                                        if(document.getElementById('subCatSel_freeItem')==null){
                                            var list_stack_disp=document.getElementById('item_picker_del_freeItem')
                                            var list_level_span = document.createElement ("li");
                                            list_level_span.innerHTML=catText +'&nbsp';
                                            list_level_span.setAttribute('id',"subCatSel_freeItem");
                                            list_level_span.setAttribute("selector_li_name",catText)
                                            list_stack_disp.appendChild (list_level_span);
                                            var btncon=document.getElementById("subCatSel_freeItem")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',"subCatSel_freeItem"+catval);
                                            list_level_button.setAttribute('onclick','remove_subcategory_freeItem()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                        }
                                        else{
                                            document.getElementById('subCatSel_freeItem').innerHTML=catText
                                            document.getElementById('subCatSel_freeItem').setAttribute("selector_li_name",catText)
                                            var btncon=document.getElementById("subCatSel_freeItem")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',"subCatSel_freeItem"+catval);
                                            list_level_button.setAttribute('onclick','remove_subcategory_freeItem()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                        }
                                    }
                                    else{
                                        if(document.getElementById('subCatSel_freeItem')){
                                            document.getElementById('subCatSel_freeItem').remove();
                                        }
                                        if(document.getElementById('brand_sel_freeItem')){
                                            document.getElementById('brand_sel_freeItem').remove();
                                        }
                                        if(document.getElementById('brand_sel_freeItem')){
                                            document.getElementById('brand_sel_freeItem').remove();
                                        }
                                        if(document.getElementById('attribute_sel_freeItem')){
                                            document.getElementById('attribute_sel_freeItem').remove();
                                        }
                                        if(document.getElementById('product_sel_freeItem')){
                                            document.getElementById('product_sel_freeItem').remove();
                                        }
                                        var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-5');
                                        labelElement.setAttribute('style','text-align: left')
                                        labelElement.innerHTML="No products to show"
                                        parentDiv.appendChild (labelElement);
                                    }       
                                        
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_sub_catageory", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send("cat_id="+catval);
                }
        }
    }
    
    function get_brands_freeItem(){
        var sub_cat_val=document.getElementById('sub_category_selector_freeItem').value
        var x = document.getElementById("sub_category_selector_freeItem");
        var sub_catText=x.options[x.selectedIndex].text
        var parentDiv = document.getElementById('brandContainer_freeItem');
            parentDiv.innerHTML=""

            if(document.getElementById('attributeContainer_freeItem').innerHTML!=null){
                document.getElementById('attributeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productContainer_freeItem').innerHTML!=null){
                document.getElementById('productContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
                document.getElementById('productTypeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
                document.getElementById('dual-list-right_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
                document.getElementById('dual-list-left_freeItem').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
                document.getElementById('product_filter_level_selector_freeItem').style.display="none"
            }               
        if(sub_cat_val!=""){            
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                    var select_data=JSON.parse(xhr.responseText);
                                    /*var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-2');
                                        labelElement.innerHTML="Select Brand:"
                                        parentDiv.appendChild (labelElement);*/
                                        
                                        
                                        
                                    if(select_data.length>0){
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-5');
                                        divElement.setAttribute('id','brandSelect_freeItem')
                                        parentDiv.appendChild (divElement);
                                        
                                        var selectDiv=document.getElementById('brandSelect_freeItem');
                                        var selectElement = document.createElement ("select");
                                        selectElement.classList.add("form-control");
                                        selectElement.setAttribute('id','attribute_selector_freeItem');
                                        selectElement.setAttribute('onchange','get_all_attributes_freeItem()');
                                        selectElement.name="attributes";
                                        var option = new Option ("Choose Brand","");
                                        selectElement.options[selectElement.options.length] = option;
                                        for (var i=0;i < select_data.length;i++)
                                        {
                                            var option = new Option (select_data[i].brand_name,select_data[i].brand_id);
                                            selectElement.options[selectElement.options.length] = option;
                                        }
                                        selectDiv.appendChild (selectElement);

                                        if(document.getElementById('brand_sel_freeItem')==null){
                                            var list_stack_disp=document.getElementById('item_picker_del_freeItem')
                                            var list_level_span = document.createElement ("li");
                                            list_level_span.innerHTML=sub_catText +'&nbsp';
                                            list_level_span.setAttribute('id',"brand_sel_freeItem");
                                            list_level_span.setAttribute('selector_li_name',sub_catText);
                                            list_stack_disp.appendChild (list_level_span);
                                            var btncon=document.getElementById("brand_sel_freeItem")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',"brand_sel_freeItem"+sub_cat_val);
                                            list_level_button.setAttribute('onclick','remove_brand_freeItem()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                            
                                            if(document.getElementById('attribute_sel_freeItem')){
                                            document.getElementById('attribute_sel_freeItem').remove();
                                            }
                                            if(document.getElementById('product_sel_freeItem')){
                                                document.getElementById('product_sel_freeItem').remove();
                                            }
                                        }
                                        else{
                                            document.getElementById('brand_sel_freeItem').innerHTML=sub_catText;
                                            document.getElementById('brand_sel_freeItem').setAttribute('selector_li_name',sub_catText);
                                            var btncon=document.getElementById("brand_sel_freeItem")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',"brand_sel_freeItem"+sub_cat_val);
                                            list_level_button.setAttribute('onclick','remove_brand_freeItem()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                            if(document.getElementById('attribute_sel_freeItem')){
                                            document.getElementById('attribute_sel_freeItem').remove();
                                            }
                                            if(document.getElementById('product_sel_freeItem')){
                                                document.getElementById('product_sel_freeItem').remove();
                                            }
                                        }
                                        
                                    }
                                    else{
                                        if(document.getElementById('brand_sel_freeItem')){
                                            document.getElementById('brand_sel_freeItem').remove();
                                        }
                                        if(document.getElementById('attribute_sel_freeItem')){
                                            document.getElementById('attribute_sel_freeItem').remove();
                                        }
                                        if(document.getElementById('product_sel_freeItem')){
                                            document.getElementById('product_sel_freeItem').remove();
                                        }
                                        var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-5');
                                        labelElement.setAttribute('style','text-align: left')
                                        labelElement.innerHTML="No products to show"
                                        parentDiv.appendChild (labelElement);
                                    }
                                    
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_brand", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send("sub_cat_val="+sub_cat_val);
                }
        }
    }
    
function get_all_attributes_freeItem(){
    var brands_val=document.getElementById('attribute_selector_freeItem').value
    var x = document.getElementById("attribute_selector_freeItem");
        var brands_Text=x.options[x.selectedIndex].text
        var parentDiv = document.getElementById('attributeContainer_freeItem');
            parentDiv.innerHTML=""

            if(document.getElementById('productContainer_freeItem').innerHTML!=null){
                document.getElementById('productContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
                document.getElementById('productTypeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
                document.getElementById('dual-list-right_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
                document.getElementById('dual-list-left_freeItem').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
                document.getElementById('product_filter_level_selector_freeItem').style.display="none"
            }                   
        if(brands_val!=""){         
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                    var select_data=JSON.parse(xhr.responseText);
                                    /*var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-2');
                                        labelElement.innerHTML="Select Product:"
                                        parentDiv.appendChild (labelElement);*/
                                        
                                        
                                        
                                    if(select_data.length>0){
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-5');
                                        divElement.setAttribute('id','attributeSelect_freeItem')
                                        parentDiv.appendChild (divElement);
                                        
                                        var selectDiv=document.getElementById('attributeSelect_freeItem');
                                        var selectElement = document.createElement ("select");
                                        selectElement.classList.add("form-control");
                                        selectElement.setAttribute('id','product_selector_freeItem');
                                        selectElement.setAttribute('onchange','get_all_items_freeItem()');
                                        selectElement.name="attributes_selector";
                                        var option = new Option ("Choose Product","");
                                        selectElement.options[selectElement.options.length] = option;
                                        for (var i=0;i < select_data.length;i++)
                                        {
                                            var option = new Option (select_data[i].product_name,select_data[i].product_id);
                                            selectElement.options[selectElement.options.length] = option;
                                        }
                                        selectDiv.appendChild (selectElement);
                                        
                                        if(document.getElementById('attribute_sel_freeItem')==null){
                                            var list_stack_disp=document.getElementById('item_picker_del_freeItem')
                                            var list_level_span = document.createElement ("li");
                                            list_level_span.innerHTML=brands_Text +'&nbsp';
                                            list_level_span.setAttribute('id',"attribute_sel_freeItem");
                                            list_level_span.setAttribute('selector_li_name',brands_Text);
                                            list_stack_disp.appendChild (list_level_span);
                                            var btncon=document.getElementById("attribute_sel_freeItem")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',brands_val+"attribute_sel_freeItem");
                                            list_level_button.setAttribute('onclick','remove_product_freeItem()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                            if(document.getElementById('product_sel_freeItem')){
                                                document.getElementById('product_sel_freeItem').remove();
                                            }
                                        }
                                        else{
                                            document.getElementById('attribute_sel_freeItem').innerHTML=brands_Text
                                            document.getElementById('attribute_sel_freeItem').setAttribute('selector_li_name',brands_Text);
                                            var btncon=document.getElementById("attribute_sel_freeItem")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',brands_val+"attribute_sel_freeItem");
                                            list_level_button.setAttribute('onclick','remove_product_freeItem()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                            if(document.getElementById('product_sel_freeItem')){
                                                document.getElementById('product_sel_freeItem').remove();
                                            }
                                        }
                                        
                                    }
                                    else{
                                        if(document.getElementById('attribute_sel_freeItem')){
                                            document.getElementById('attribute_sel_freeItem').remove();
                                        }
                                        if(document.getElementById('product_sel_freeItem')){
                                            document.getElementById('product_sel_freeItem').remove();
                                        }
                                        var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-5');
                                        labelElement.setAttribute('style','text-align: left')
                                        labelElement.innerHTML="No products to show"
                                        parentDiv.appendChild (labelElement);
                                    }
                                    
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_attributes", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send("brands_val="+brands_val);
                }
        }
}   

function get_all_items_freeItem(){
        var product_val=document.getElementById('product_selector_freeItem').value
        var x = document.getElementById("product_selector_freeItem");
        var product_Text=x.options[x.selectedIndex].text
        var parentDiv = document.getElementById('productContainer_freeItem');
        parentDiv.innerHTML=""

        if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
            document.getElementById('dual-list-right_freeItem').innerHTML=""
        }
        if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
            document.getElementById('dual-list-left_freeItem').innerHTML=""
            update_buy_promo_list()
        }
        if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
            document.getElementById('product_filter_level_selector_freeItem').style.display="none"
        }       
        if(product_val!=""){            
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                    var select_data=JSON.parse(xhr.responseText);
                                    document.getElementById('dual-list-left_freeItem').innerHTML=""
                                    document.getElementById('dual-list-right_freeItem').innerHTML=""
                                        
                                    if(select_data.length>0){
                                        document.getElementById('product_filter_level_selector_freeItem').style.display=""
                                        
                                        var liDiv=document.getElementById('dual-list-right_freeItem');
                                        document.getElementById('dual-list-left_freeItem').innerHTML=""
                                        document.getElementById('dual-list-right_freeItem').innerHTML=""
                                        for (var i=0;i < select_data.length;i++)
                                        {
                                            var liElement = document.createElement ("li");
                                            liElement.innerHTML=select_data[i].sku_id+'('+select_data[i].attribute_1+'='+select_data[i].attribute_1_value+', '+select_data[i].attribute_2+'='+select_data[i].attribute_2_value+')';
                                            liElement.classList.add("list-group-item");
                                            liElement.setAttribute('data-value-name',select_data[i].sku_id+'('+select_data[i].attribute_1+'='+select_data[i].attribute_1_value+', '+select_data[i].attribute_2+'='+select_data[i].attribute_2_value+')');
                                            liElement.setAttribute('sku-id',select_data[i].id);
                                            liElement.setAttribute('data-value',select_data[i].id);
                                            liElement.name="item_typs";
                                            liDiv.appendChild (liElement);
                                        }
                                        activate_li_freeItem();
                                        populate_attribute_filter_freeItem(product_val);
                                        
                                        if(document.getElementById('product_sel_freeItem')==null){
                                            if(product_Text.length > 10){
                                            product_Texts = product_Text.substring(0,10)+"...";
                                            }
                                            
                                            var list_stack_disp=document.getElementById('item_picker_del_freeItem')
                                            var list_level_span = document.createElement ("li");
                                            list_level_span.innerHTML=product_Texts +'&nbsp';
                                            list_level_span.setAttribute('id',"product_sel_freeItem");
                                            list_level_span.setAttribute('selector_li_name',product_Text);
                                            list_stack_disp.appendChild (list_level_span);
                                            var btncon=document.getElementById("product_sel_freeItem")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',product_val+"_product_sel_freeItem");
                                            list_level_button.setAttribute('onclick','remove_product_variant_freeItem()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                        }
                                        else{
                                            if(product_Text.length > 10){
                                            product_Texts = product_Text.substring(0,10)+"...";
                                            }
                                            document.getElementById('product_sel_freeItem').innerHTML=product_Texts
                                            document.getElementById('product_sel_freeItem').setAttribute('selector_li_name',product_Text)
                                            var btncon=document.getElementById("product_sel_freeItem")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',product_val+"_product_sel_freeItem");
                                            list_level_button.setAttribute('onclick','remove_product_variant_freeItem()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                        }
                                        
                                        
                                        
                                    }
                                    else{
                                        if(document.getElementById('product_sel_freeItem')){
                                            document.getElementById('product_sel_freeItem').remove();
                                        }
                                        document.getElementById('product_filter_level_selector_freeItem').style.display="none"
                                        var selectDiv=document.getElementById('attributeSelect_freeItem');
                                        var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-5');
                                        labelElement.setAttribute('style','text-align: left')
                                        labelElement.innerHTML="No products to show"
                                        selectDiv.appendChild (labelElement);
                                    }
                                    
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_items_in_type", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send("product_val="+product_val);
                }
        }
}

function populate_attribute_filter_freeItem(product_val){
    var productTypeContainer =document.getElementById('productTypeContainer_freeItem')
        productTypeContainer.innerHTML=""
        
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                    var select_data=JSON.parse(xhr.responseText);                                       
                                    //alert(select_data.attr1.length);
                                    //alert(select_data.attr2.length);
                                    if(select_data.attr1.length>0){
                                        var divElements=document.createElement('div')
                                        divElements.classList.add('col-md-12')
                                        productTypeContainer.appendChild(divElements)
                                        var labelElement=document.createElement ("div")
                                        labelElement.classList.add('col-md-12');
                                        labelElement.innerHTML="Filter "+select_data.attribute_1+" :";
                                        divElements.appendChild (labelElement);
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-12');
                                        divElement.setAttribute('id','attributeColorSelect_freeItem')
                                        divElements.appendChild (divElement);
                                        var checkDiv=document.getElementById('attributeColorSelect_freeItem');
                                    
                                        
                                        for (var i=0;i < select_data.attr1.length;i++){
                                            var labelElement=document.createElement ("label")
                                            labelElement.classList.add('checkbox-inline');
                                            labelElement.setAttribute('id','id_freeItem'+select_data.attr1[i]);
                                            checkDiv.appendChild (labelElement);
                                            
                                            var labelDiv=document.getElementById('id_freeItem'+select_data.attr1[i]);
                                            var checkbox = document.createElement('input');
                                            checkbox.type = "checkbox";
                                            checkbox.name = "filter_color";
                                            checkbox.setAttribute("onclick","applyToFilter_freeItem("+product_val+")");
                                            checkbox.value = select_data.attr1[i];
                                            checkbox.id = "id_freeItem"+select_data.attr1[i];
                                            
                                            var label = document.createElement('label')
                                            label.htmlFor = "id_freeItem"+select_data.attr1[i];
                                            label.appendChild(document.createTextNode(select_data.attr1[i]));
                                            
                                            labelDiv.appendChild(checkbox);
                                            labelDiv.appendChild(label);
                                        }
                                        
                                    }
									if(select_data.attr2!=null){
                                    if(select_data.attr2.length>0){
                                        var divElements=document.createElement('div')
                                        divElements.classList.add('col-md-12')
                                        productTypeContainer.appendChild(divElements)
                                        var labelElement=document.createElement ("div")
                                        labelElement.classList.add('col-md-12');
                                        labelElement.innerHTML="Filter "+select_data.attribute_2+" :";
                                        divElements.appendChild (labelElement);
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-12');
                                        divElement.setAttribute('id','attributeSizeSelect_freeItem')
                                        divElements.appendChild (divElement);
                                        var checkDiv=document.getElementById('attributeSizeSelect_freeItem');
                                    
                                        
                                        for (var i=0;i < select_data.attr2.length;i++){
                                            var labelElement=document.createElement ("label")
                                            labelElement.classList.add('checkbox-inline');
                                            labelElement.setAttribute('id','id_freeItem'+select_data.attr2[i]);
                                            checkDiv.appendChild (labelElement);
                                            
                                            var labelDiv=document.getElementById('id_freeItem'+select_data.attr2[i]);
                                            var checkbox = document.createElement('input');
                                            checkbox.type = "checkbox";
                                            checkbox.name = "filter_size";
                                            checkbox.setAttribute("onclick","applyToFilter_freeItem("+product_val+")");
                                            checkbox.value = select_data.attr2[i];
                                            checkbox.id = "id_"+select_data.attr2[i];
                                            
                                            var label = document.createElement('label')
                                            label.htmlFor = "id_"+select_data.attr2[i];
                                            label.appendChild(document.createTextNode(select_data.attr2[i]));
                                            
                                            labelDiv.appendChild(checkbox);
                                            labelDiv.appendChild(label);
                                        }
                                        countLeftlist_freeItem()
                                    }
									}
                                    
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_items_in_type_filter", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send("product_val="+product_val);
                }
}

function applyToFilter_freeItem(product_val){
    update_buy_promo_list()
    var colorelem=document.getElementsByName('filter_color')
    var colorelemarr = []
    for(i=0;i<colorelem.length;i++){
        if(colorelem[i].checked){
            colorelemarr.push(colorelem[i].value);
        }
    }
    var color=colorelemarr.join("-");
    var sizeelem=document.getElementsByName('filter_size')
    var sizeelemarr = []
    for(i=0;i<sizeelem.length;i++){
        if(sizeelem[i].checked){
            sizeelemarr.push(sizeelem[i].value);
        }
    }
    var size=sizeelemarr.join("-");
    
    var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                    var select_data=JSON.parse(xhr.responseText);
                                    var liDiv=document.getElementById('dual-list-right_freeItem');
                                    liDiv.innerHTML=""
                                    document.getElementById('dual-list-left_freeItem').innerHTML=""
                                        //alert(xhr.responseText);
                                    if(select_data.length>0){
                                        document.getElementById('product_filter_level_selector_freeItem').style.display=""
                                        
                                        var liDiv=document.getElementById('dual-list-right_freeItem');
                                        for (var i=0;i < select_data.length;i++)
                                        {
                                            
                                            var liElement = document.createElement ("li");
                                            liElement.innerHTML=select_data[i].sku_id+'('+select_data[i].attribute_1+'='+select_data[i].attribute_1_value+', '+select_data[i].attribute_2+'='+select_data[i].attribute_2_value+')';
                                            liElement.classList.add("list-group-item");
                                            liElement.setAttribute('data-value-name',select_data[i].sku_id+'('+select_data[i].attribute_1+'='+select_data[i].attribute_1_value+', '+select_data[i].attribute_2+'='+select_data[i].attribute_2_value+')');
                                            liElement.setAttribute('sku-id',select_data[i].id);
                                            liElement.setAttribute('data-value',select_data[i].id);
                                            liElement.name="item_typs";
                                            liDiv.appendChild (liElement);
                                        }
                                        activate_li_freeItem();
                                        countLeftlist_freeItem()
                                        //populate_attribute_filter(product_val);
                                        
                                    }
                                    else{
                                        document.getElementById('product_filter_level_selector_freeItem').style.display="none"
                                        var selectDiv=document.getElementById('attributeSelect_freeItem');
                                        var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-5');
                                        labelElement.setAttribute('style','text-align: left')
                                        labelElement.innerHTML="No products to show"
                                        selectDiv.appendChild (labelElement);
                                    }                                       
                                    
                                    
                                    
                                    
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_items_after_filter", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send("color_val="+color+"&size_val="+size+"&product_val="+product_val);
                }
    
}
 function countLeftlist_freeItem(){
        var right_list_search=document.querySelectorAll('#dual-list-right_freeItem li').length;
        var left_list_search=document.querySelectorAll('#dual-list-left_freeItem li').length;
        document.getElementById("right_list_search_freeItem").setAttribute('placeholder','Total SKU displayed:  '+right_list_search);
        document.getElementById("left_list_search_freeItem").setAttribute('placeholder','Total SKU displayed:  '+left_list_search); 
    }

    function activate_li_freeItem() {
    var move_right = '<span class="glyphicon glyphicon-minus pull-left  dual-list-move-right" title="Remove Selected"></span>';
    var move_left  = '<span class="glyphicon glyphicon-plus  pull-right dual-list-move-left " title="Add Selected"></span>';
    
    $(".dual-list.list-left .list-group").sortable({
        stop: function( event, ui ) {
            updateSelectedOptions();
        }
    });
    
    
    $('body').on('click', '.list-group .list-group-item', function () {
        $(this).toggleClass('active');
    });
    
    
    $('body').on('click', '.dual-list-move-right', function (e) {
        e.preventDefault();

        actives = $(this).parent();
        $(this).parent().find("span").remove();
        $(move_left).clone().appendTo(actives);
        actives.clone().appendTo('.list-right ul').removeClass("active");
        actives.remove();
        
        sortUnorderedList("dual-list-right");
        
        updateSelectedOptions();
    });
    
    
    $('body').on('click', '.dual-list-move-left', function (e) {
        e.preventDefault();

        actives = $(this).parent();
        $(this).parent().find("span").remove();
        $(move_right).clone().appendTo(actives);
        actives.clone().appendTo('.list-left ul').removeClass("active");
        actives.remove();
        
        updateSelectedOptions();
    });
    
    
    $('.move-right, .move-left').click(function () {
        var $button = $(this), actives = '';
        if ($button.hasClass('move-left')) {
            actives = $('.list-right ul li.active');
            actives.find(".dual-list-move-left").remove();
            actives.append($(move_right).clone());
            actives.clone().appendTo('.list-left ul').removeClass("active");
            actives.remove();
            
            
        } else if ($button.hasClass('move-right')) {
            actives = $('.list-left ul li.active');
            actives.find(".dual-list-move-right").remove();
            actives.append($(move_left).clone());
            actives.clone().appendTo('.list-right ul').removeClass("active");
            actives.remove();

        }
        
        updateSelectedOptions();
        
    });
    
    function countLeftlist_freeItem(){
        var right_list_search=document.querySelectorAll('#dual-list-right_freeItem li').length;
        var left_list_search=document.querySelectorAll('#dual-list-left_freeItem li').length;
        document.getElementById("right_list_search_freeItem").setAttribute('placeholder','Total SKU displayed:  '+right_list_search);
        document.getElementById("left_list_search_freeItem").setAttribute('placeholder','Total SKU displayed:  '+left_list_search); 
    }

    
    function updateSelectedOptions() {
        countLeftlist_freeItem();
        update_buy_promo_list_freeItem();
        $('#dual-list-options_freeItem').find('option').remove();

        $('.list-left ul li').each(function(idx, opt) {
            $('#dual-list-options_freeItem').append($("<option></option>")
                .attr("value", $(opt).data("value"))
                .text( $(opt).text())
                .prop("selected", "selected")
            ); 
        });
    }
    
    
    $('.dual-list .selector').click(function () {
        var $checkBox = $(this);
        if (!$checkBox.hasClass('selected')) {
            $checkBox.addClass('selected').closest('.well').find('ul li:not(.active)').addClass('active');
            $checkBox.removeClass('glyphicon-unchecked').addClass('glyphicon-check');
        } else {
            $checkBox.removeClass('selected').closest('.well').find('ul li.active').removeClass('active');
            $checkBox.removeClass('glyphicon-check').addClass('glyphicon-unchecked');
        }
    });
    
    
    $('[name="SearchDualList"]').keyup(function (e) {
        var code = e.keyCode || e.which;
        if (code == '9') return;
        if (code == '27') $(this).val(null);
        var $rows = $(this).closest('.dual-list').find('.list-group li');
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
        $rows.show().filter(function () {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });
    
    
    $(".glyphicon-search").on("click", function() {
        $(this).next("input").focus();
    });
    
    
    function sortUnorderedList(ul, sortDescending) {
        $("#" + ul + "_freeItem li").sort(sort_li).appendTo("#" + ul +"_freeItem");
        
        function sort_li(a, b){
            return ($(b).data('value')) < ($(a).data('value')) ? 1 : -1;    
        }
    }
        
    
    $("#dual-list-left_freeItem li").append(move_right);
    $("#dual-list-right_freeItem li").append(move_left);
}


</script>       

</div>
</body>
</html>