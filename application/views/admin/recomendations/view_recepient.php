<?php
	$email_stuff_arr=email_stuff();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="robots" content="noindex">
<title><?php echo $email_stuff_arr["name_emailtemplate"];?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style type="text/css">
.panel-group .panel {
border-radius: 0;
box-shadow: none;
border-color: #EEEEEE;
}
.panel-default > .panel-heading {
padding: 0;
border-radius: 0;
color: #212121;
background-color: #FAFAFA;
border-color: #EEEEEE;
}
.panel-title {
font-size: 14px;
}
.panel-title > a {
display: block;
padding: 15px;
text-decoration: none;
}
.more-less {
float: right;
color: #212121;
}
.panel-default > .panel-heading + .panel-collapse > .panel-body {
border-top-color: #EEEEEE;
}
.demo {
padding-top: 60px;
padding-bottom: 60px;
}
</style>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>

<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="page-header">
  <h4 class="text-danger text-center">Update Recommendations</h4>
  
  </div>
        

       <?php foreach($recepient_data as $data){
           if($data['inv_id']!=""){ 
               $product_name=$data['name_of_items'];
               $timestamp=$data['timestamp'];
           }
       }?>
       <?php if(count($recepient_data) >1){
           ?>
           <div class="panel-group" id="accordionData" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordionData" href="#collapseRecepient" aria-expanded="true" aria-controls="collapseRecepient">
                            
                            <i class="more-less glyphicon glyphicon-plus"></i>
                            <span class="lead"><?php echo $product_name ?></span> added On <?php echo $timestamp?> count <?php echo count($recepient_data)?> <span> &nbsp;<button class="btn btn-xs btn-danger" onclick="delete_receipient('<?php echo $recomendation_uids;?>')"> Delete</button></span>
							
                        </a>
                    </h4>
                </div>
                <div id="collapseRecepient" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <ul class="list-group">
                            <?php foreach($recepient_data as $data){ 
                                if($data['inv_id']!=''){
                                ?>
                                <li class="list-group-item" style="float:left"><?php echo $controller->getSKUId($data['inv_id']) ?></li>
                            <?php } }?>
                        </ul>
                        
                    </div>
                </div>
            </div>
        </div><!-- panel-group -->
        <?php }else{ ?>
            <ul class="list-group">
                            <?php foreach($recepient_data as $data){ ?>
                                <li class="list-group-item"><?php echo $data['name_of_items']?> added On <?php echo $data['timestamp']?><span> &nbsp;<button class="btn btn-xs btn-danger" onclick="delete_receipient('<?php echo $recomendation_uids;?>')"> Delete</button></span></li>
                            <?php }?>
                        </ul>
        <?php } ?>
		
								<script>
	function delete_receipient(u_id){
			
	swal({
			title: 'Are you sure?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Delete',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Recomendations/delete_receipient",
		type:"post",
		data:"recomendation_uid="+u_id,
		success:function(data){
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Recepient has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.href='<?php echo base_url()?>admin/Recomendations/';
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
							
	}					</script>	
            
    <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
    <?php if(!empty($recepient_data_first_level)){
       $count_added_as_first_level= count($recepient_data_first_level); 
       $sub_sku_count=0;
       $sub_sku_line="";
       foreach($recepient_data_first_level as $data){
           $sub_sku_line.=$data['inv_id'];
           $sub_sku_line.=',';
       }
       $sub_sku_line_arr=explode(',',$sub_sku_line);
       $sub_sku_count=count($sub_sku_line_arr)-1;
    } ?>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <i class="more-less glyphicon glyphicon-plus"></i>
                        <?php if(!empty($recepient_data_first_level)){echo "<span id='first_level_ids_name'>1st Degree Recommendation :  ".'<b id="first_level_count">'.$count_added_as_first_level." </b> Product Category added (<b id='first_level_sub_count'>".$sub_sku_count.'</b>  SKU Count)&nbsp;</span><button recomendation_id="'.$recomendation_uids.'" class="btn btn-xs btn-success" onclick="add_first_stage(this)"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add </button>' ;}else{
                            echo " No Items Added for 1st Degree Recomendation".' <button recomendation_id="'.$recomendation_uids.'" class="btn btn-xs btn-success" onclick="add_first_stage(this)"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add </button>'; 
                        }?>
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  <?php if(!empty($recepient_data_first_level)){
                   foreach($recepient_data_first_level as $data){ ?>
                       <div class="col-sm-12">
                       <ul class="list-group"><h4 class="list-group-item-heading"><?php echo $controller->getProductName($data['product_id'])?> <button class="btn btn-xs btn-danger" product_name="<?php echo $controller->getProductName($data['product_id'])?>" recomendation_id="<?php echo $data['recomendation_uid'] ?>" recomendation_level="<?php echo $data['recomendation_level']; ?>" ids="<?php echo $data['id']?>" onclick="delete_this_recomendation(this)"> <i class="fa fa-trash" aria-hidden="true"></i> Delete </button></h4> 
                       <?php $sku_arr=explode(',',$data['inv_id']);
                       if(count($sku_arr)>0){ 
                           foreach($sku_arr as $arr){?>
                           <li class="list-group-item" style="float:left"><?php echo $controller->getSKUId($arr) ?> &nbsp; <button  class="btn btn-xs btn-danger" sku_id="<?php echo $controller->getSKUId($arr) ?>" product_name="<?php echo $controller->getProductName($data['product_id'])?>" recomendation_id="<?php echo $data['recomendation_uid'] ?>" recomendation_level="<?php echo $data['recomendation_level']; ?>" sub_item="<?php echo $arr ?>" ids="<?php echo $data['id']?>" onclick="delete_this_sub_recomendation(this)"><i class="fa fa-trash" aria-hidden="true"></i></button></li>
                       <?php }
                       }
                       ?>
                       </ul>
                      </div> 
                 <?php }
                  }?>
                </div>
            </div>
        </div>
        
        <?php if(!empty($recepient_data_second_level)){
       $count_added_as_second_level= count($recepient_data_second_level);
       $sub_sku_count=0;
       $sub_sku_line="";
       foreach($recepient_data_second_level as $data){
           $sub_sku_line.=$data['inv_id'];
           $sub_sku_line.=',';
       }
       $sub_sku_line_arr=explode(',',$sub_sku_line);
       $sub_sku_count=count($sub_sku_line_arr)-1;
       
        } ?>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                        
                        <i class="more-less glyphicon glyphicon-plus"></i>
                        <?php if(!empty($recepient_data_second_level)){echo "<span id='second_level_ids_name'>2nd Degree Recommendation :  ".'<b id="second_level_count">'.$count_added_as_second_level." </b> Product Category added (<b id='second_level_sub_count'>".$sub_sku_count.'</b>  SKU Count)</span>&nbsp;<button recomendation_id="'.$recomendation_uids.'" class="btn btn-xs btn-success" onclick="add_second_stage(this)"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add </button>' ;}else{
                            echo " No Items Added for 2nd Degree Recomendation".' <button recomendation_id="'.$recomendation_uids.'" class="btn btn-xs btn-success" onclick="add_second_stage(this)"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add </button>'; 
                        }?>
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                  <?php if(!empty($recepient_data_second_level)){
                   foreach($recepient_data_second_level as $data){ ?>
                       <div class="col-sm-12">
                       <ul class="list-group"><h4 class="list-group-item-heading"><?php echo $controller->getProductName($data['product_id'])?> <button class="btn btn-xs btn-danger" product_name="<?php echo $controller->getProductName($data['product_id'])?>" recomendation_id="<?php echo $data['recomendation_uid'] ?>" recomendation_level="<?php echo $data['recomendation_level']; ?>" ids="<?php echo $data['id']?>" onclick="delete_this_recomendation(this)"> <i class="fa fa-trash" aria-hidden="true"></i> Delete </button></h4> 
                       <?php $sku_arr=explode(',',$data['inv_id']);
                       if(count($sku_arr)>0){ 
                           foreach($sku_arr as $arr){?>
                           <li class="list-group-item" style="float:left"><?php echo $controller->getSKUId($arr) ?> &nbsp; <button  class="btn btn-xs btn-danger" product_name="<?php echo $controller->getProductName($data['product_id'])?>" recomendation_id="<?php echo $data['recomendation_uid'] ?>" sku_id="<?php echo $controller->getSKUId($arr) ?>" recomendation_level="<?php echo $data['recomendation_level']; ?>" sub_item="<?php echo $arr ?>" ids="<?php echo $data['id']?>" onclick="delete_this_sub_recomendation(this)"><i class="fa fa-trash" aria-hidden="true"></i></button></li>
                       <?php }
                       }
                       ?>
                       </ul>
                       </div>
                       
                 <?php }
                  }?>
                </div>
            </div>
        </div>
     


        <?php if(!empty($recepient_data_third_level)){
       $count_added_as_third_level= count($recepient_data_third_level); 
       $sub_sku_count=0;
       $sub_sku_line="";
       foreach($recepient_data_third_level as $data){
           $sub_sku_line.=$data['inv_id'];
           $sub_sku_line.=',';
       }
       $sub_sku_line_arr=explode(',',$sub_sku_line);
       $sub_sku_count=count($sub_sku_line_arr)-1;
        } ?>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="true" aria-controls="collapsethree">
                        <i class="more-less glyphicon glyphicon-plus"></i>
                        <?php if(!empty($recepient_data_third_level)){echo "<span id='third_level_ids_name'>3rd Degree Recommendation :  ".'<b id="third_level_count">'.$count_added_as_third_level." </b> Product Category added (<b id='third_level_sub_count'>".$sub_sku_count.'</b>  SKU Count)</span>&nbsp;<button recomendation_id="'.$recomendation_uids.'" class="btn btn-xs btn-success" onclick="add_third_stage(this)"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add </button>' ;}else{
                            echo " No Items Added for 3rd Degree Recomendation".' <button recomendation_id="'.$recomendation_uids.'" class="btn btn-xs btn-success" onclick="add_third_stage(this)"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add </button>'; 
                        }?>
                    </a>
                </h4>
            </div>
            <div id="collapsethree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                  <?php if(!empty($recepient_data_third_level)){
                   foreach($recepient_data_third_level as $data){ ?>
                       <div class="col-sm-12">
                       <ul class="list-group"><h4 class="list-group-item-heading"><?php echo $controller->getProductName($data['product_id'])?> <button class="btn btn-xs btn-danger" product_name="<?php echo $controller->getProductName($data['product_id'])?>" recomendation_id="<?php echo $data['recomendation_uid'] ?>" recomendation_level="<?php echo $data['recomendation_level']; ?>" ids="<?php echo $data['id']?>" onclick="delete_this_recomendation(this)"> <i class="fa fa-trash" aria-hidden="true"></i> Delete </button></h4> 
                       <?php $sku_arr=explode(',',$data['inv_id']);
                       if(count($sku_arr)>0){ 
                           foreach($sku_arr as $arr){?>
                           <li class="list-group-item" style="float:left"><?php echo $controller->getSKUId($arr) ?> &nbsp; <button  class="btn btn-xs btn-danger" product_name="<?php echo $controller->getProductName($data['product_id'])?>" sku_id="<?php echo $controller->getSKUId($arr) ?>" recomendation_id="<?php echo $data['recomendation_uid'] ?>" recomendation_level="<?php echo $data['recomendation_level']; ?>" sub_item="<?php echo $arr ?>" ids="<?php echo $data['id']?>" onclick="delete_this_sub_recomendation(this)"><i class="fa fa-trash" aria-hidden="true"></i></button></li>
                       <?php }
                       }
                       ?>
                       </ul>
                       </div>
                 <?php }
                  }?>
                </div>
            </div>
        </div>

    </div><!-- panel-group -->
    
    
</div><!-- container -->
<script>
    function add_first_stage(obj){
        var rec_uid=obj.getAttribute('recomendation_id');
        document.location.href ='<?php echo base_url()?>admin/Recomendations/add_first_level_recomendation/'+rec_uid
    }
    function add_second_stage(obj){
        var rec_uid=obj.getAttribute('recomendation_id');
        document.location.href ='<?php echo base_url()?>admin/Recomendations/add_second_level_recomendation/'+rec_uid
    }
    function add_third_stage(obj){
        var rec_uid=obj.getAttribute('recomendation_id');
        document.location.href ='<?php echo base_url()?>admin/Recomendations/add_third_level_recomendation/'+rec_uid
    }
</script>
    <script type="text/javascript">
    function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
    </script>
    <script>
    function delete_this_sub_recomendation(obj){
            var recomendation_level=obj.getAttribute('recomendation_level');
            var recomendation_id=obj.getAttribute('recomendation_id');
            var ids=obj.getAttribute('ids');
            var sub_item=obj.getAttribute('sub_item');
            var sku_id=obj.getAttribute('sku_id');
            var product_name=obj.getAttribute('product_name');
            var recomendations
            if(recomendation_level=="first_level"){
                recomendations="1st Degree Recomendation"
            }
            if(recomendation_level=="second_level"){
                recomendations="2nd Degree Recomendation"
            }
            if(recomendation_level=="third_level"){
                recomendations="3rd Degree Recomendation"
            }
            var answer=confirm("Do you Want To Delete "+product_name +"'s SKU ID "+ sku_id + " for "+ recomendations)
            if(answer){
            var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {
                                           
                    obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if(xhr.responseText==1){
                                var x = obj.parentNode;
                                var parent=obj.parentNode.parentNode.parentNode;
                                var parents=obj.parentNode.parentNode.parentNode.parentNode;
                                x.remove();
                                
                                //
                                var elemns=parent.parentNode.children
                                var sub_counts=0
                                for(var j=0;j<elemns.length;j++){
                                    sub_counts+=parseInt(parent.parentNode.children[j].children[0].childElementCount)-2
                                }
                                document.getElementById(recomendation_level+'_sub_count').innerHTML=sub_counts
                                
                                if(parent.children[0].childElementCount==2){
                                    parent.remove()
                                    document.getElementById(recomendation_level+'_count').innerHTML=parents.childElementCount
                                    if(parents.childElementCount==0){
                                        document.getElementById(recomendation_level+'_ids_name').innerHTML="No Items Added for "+recomendations +'&nbsp;'
                                    }
                                }
                                
                                    //x.parentNode.removeChild(x);
                                    
                           }
                           else{
                               obj.innerHTML='<i class="fa fa-trash-o" aria-hidden="true"></i>'
                           }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Recomendations/delete_group_of_recomendation_level_items_sub", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('recomendation_level='+recomendation_level+"&recomendation_id="+recomendation_id+"&ids="+ids+"&sub_item="+sub_item);
                }
        }
        else{
            return false;
        }
        }
    
        function delete_this_recomendation(obj){
            var recomendation_level=obj.getAttribute('recomendation_level');
            var recomendation_id=obj.getAttribute('recomendation_id');
            var ids=obj.getAttribute('ids');
            var product_name=obj.getAttribute('product_name');
            var recomendations
            if(recomendation_level=="first_level"){
                recomendations="1st Degree Recomendation"
            }
            if(recomendation_level=="second_level"){
                recomendations="2nd Degree Recomendation"
            }
            if(recomendation_level=="third_level"){
                recomendations="3rd Degree Recomendation"
            }
            var answer=confirm("Do you Want To Delete "+product_name +" for "+ recomendations)
            if(answer){
            var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {
                                           
                    obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if(xhr.responseText==1){
                                var x = obj.parentNode.parentNode;
                                var parent=obj.parentNode.parentNode.parentNode.parentNode;

                                x.parentNode.remove();
                               var elemns=parent.parentNode.children
                                var sub_counts=0
                                for(var j=0;j<elemns.length;j++){
                                    if(parent.parentNode.children[j].children[0]!=null){
                                        sub_counts+=parseInt(parent.parentNode.children[j].children[0].children[0].childElementCount)-2
                                    }
                                    
                                }
                                document.getElementById(recomendation_level+'_sub_count').innerHTML=sub_counts
                                document.getElementById(recomendation_level+'_count').innerHTML=parent.childElementCount
                                if(parent.childElementCount==0){
                                    document.getElementById(recomendation_level+'_ids_name').innerHTML="No Items Added for "+recomendations +'&nbsp;'
                                }
                                    //x.parentNode.removeChild(x);
                                    
                           }
                           else{
                               obj.innerHTML='<i class="fa fa-trash-o" aria-hidden="true"></i>'
                           }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Recomendations/delete_group_of_recomendation_level_items", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('recomendation_level='+recomendation_level+"&recomendation_id="+recomendation_id+"&ids="+ids);
                }
        }
        else{
            return false;
        }
        }
    
        function delete_policy(obj){
        var policy_list_id=obj.getAttribute('policy_uid');
        var policy_type=obj.getAttribute('policy_type');
        var answer=confirm("Do you Want To Delete this Policy")
        //alert(obj.parentNode.parentNode.getAttribute('class'))
        if(answer){
            var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {
                                           
                    obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if(xhr.responseText==1){
                                var x = obj.parentNode.parentNode;
                                    x.parentNode.removeChild(x);
                           }
                           else{
                               obj.innerHTML='<i class="fa fa-trash-o" aria-hidden="true"></i>'
                           }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Policys/delete_policy", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('policy_list_id='+policy_list_id+"&policy_type="+policy_type);
                }
        }
        else{
            return false;
        }
        }
       function get_all_policy_related_data(obj){
        var policy_list_id=obj
        /*var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {
                                           
                    obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if(xhr.responseText!=""){
                                var policy_list_arr=JSON.parse(xhr.responseText)
                                var str='<ul>'
                                for(var i=0;i<policy_list_arr.length;i++){
                                    str+='<li>'+policy_list_arr[i]['payment_policy_uid']+'</li>'
                                }
                                str+='<ul>';
                                //document.getElementById(policy_list_id+'_existing_policy_list').innerHTML=str;
                           }
                           else{
                               //document.getElementById(policy_list_id+'_existing_policy_list').innerHTML=""
                           }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Policys/delete_payment_policy", true);*/
                    /*xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('policy_list_id='+policy_list_id);
                }   */
        }
    </script>
	</div>
</body>
</html>
