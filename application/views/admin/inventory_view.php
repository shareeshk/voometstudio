<html lang="en">
<head>	
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />


<style type="text/css">
.cursor-pointer {
    cursor: pointer !important;
}
.small_color_box{
	width: 30px;
	height: 30px;
	border: 1px solid #ccc;
	margin-top: 1px;
}

.loading {
	text-align: center; 
	margin:0;product
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
#wizardPictureSmall_picname{
	word-wrap: break-word;
}
#wizardPictureBig_picname{
	word-wrap: break-word;
}
#wizardPictureSmall_picname2{
	word-wrap: break-word;
}
#wizardPictureBig_picname2{
	word-wrap: break-word;
}
#wizardPictureSmall_picname3{
	word-wrap: break-word;
}
#wizardPictureBig_picname3{
	word-wrap: break-word;
}
#wizardPictureSmall_picname4{
	word-wrap: break-word;
}
#wizardPictureBig_picname4{
	word-wrap: break-word;
}
#wizardPictureSmall_picname5{
	word-wrap: break-word;
}
#wizardPictureBig_picname5{
	word-wrap: break-word;
}
#wizardPictureSmall_picname6{
	word-wrap: break-word;
}
#wizardPictureBig_picname6{
	word-wrap: break-word;
}
textarea.form-control {
    height:36px;
}
.CamListDiv {
    height: 450px;
    float: left;
    overflow: scroll;
    overflow-x:hidden;
}
.well{
	width:100%;
}
</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 

<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
<script type="text/javascript">
var table;

$(document).ready(function (){
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    var rows_selected = [];
    table = $('#table_inventory').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Catalogue/inventory_processing",
			data: function (d) { d.search_attribute_1_value = '<?php echo $search_attribute_1_value;?>'; d.search_attribute_2_value = '<?php echo $search_attribute_2_value;?>';d.search_attribute_3_value = '<?php echo $search_attribute_3_value;?>';d.search_attribute_4_value = '<?php echo $search_attribute_4_value;?>';d.search_product_status = '<?php echo $search_product_status;?>';d.search_view_product_status = '<?php echo $search_view_product_status;?>';d.product_id = $('#product_id').val();d.active=1;d.inventory_type=$('#inventory_type').val()},
            type: "post",
            error: function(){
              $("#table_inventory_processing").css("display","none");
            },
			"dataSrc": function ( json ) {				
				document.getElementById("inventory_count").innerHTML=json.recordsFiltered;				
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });  
	$('#reset_form_button').on('click',function(){
		table.draw();
	});	
	$("#submit_button").click(function() {
		table.draw();
	});	
	
});	

function drawtable(obj=""){
	table.draw();
}
function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_delete_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
	swal({
			title: 'Are you sure?',
			text: "Archived Inventory",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Archive it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
		$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/update_inventory_selected",
		type:"post",
		data:"selected_list="+selected_list+"&active=0",
		
		success:function(data){
			if(data==true){
				
				swal({
						title:"Archived!", 
						text:"Given "+table+"(s) has been Moved to Trash successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}
			else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
		});
	})
		    },
	allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
function multilpe_delete_when_no_info(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
		swal({
			title: 'Are you sure?',
			text: "Delete Inventory",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/delete_inventory_when_no_info",
		type:"post",
		data:"selected_list="+selected_list,
		success:function(data){
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Given "+table+"(s) has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else if(data==false){
				swal("Error", "Error in deleting this file", "error");
			}else{
				swal("Error", "Oops ..! You can't delete it.", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}

</script>
<script type="text/javascript">
	function display_color(color_value){
	var attribute_1_value=document.getElementById("attribute_1_value").value;
		//alert(color_value);
		colors_arr=color_value.split(":");
		color_code=colors_arr[1];
		color_name=colors_arr[0];
		if(attribute_1_value!=""){
		$('#small_color_box').css({"background-color": color_code});
		$('#small_color_box').attr('title',color_name);
		}if(attribute_1_value==""){
			$('#small_color_box').css({"background-color": ""});
		}
	}
</script>
<script type="text/javascript">
	function filter_display_color(color_value){
		//alert(color_value);
		colors_arr=color_value.split(":");
		color_code=colors_arr[1];
		color_name=colors_arr[0];
		$('#filter_small_color_box').css({"background-color": color_code});
		$('#filter_small_color_box').attr('title',color_name);
	}
</script>	
<script>
		function spec_fun(obj,specification_id){
			//document.getElementsByName("specification_textarea_value["+specification_id+"]")[0].setAttribute("style", "border: 1px solid #ccc;");
			if(obj.value=="Others"){
				document.getElementsByClassName("specification_textarea_value["+specification_id+"]")[0].style.display="block";
			}
			else{
				document.getElementsByClassName("specification_textarea_value["+specification_id+"]")[0].style.display="none";
			}
		}
</script>
<script type="text/javascript">
$(document).ready(function (){
$('#back').on('click',function(){	
		var form = document.getElementById('back_to_inventory');
		form.action='<?php echo base_url(); ?>admin/Catalogue/inventory_filter';
		form.submit();
	});
});		
</script>
<script>

function changeViewPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/inventory_filter';
}
</script>
<script type="text/javascript">

function filter_details_form_valid()
{
	
	
	var form_status = $('<div class="form_status"></div>');		
	var form = $('#create_inventory');	
	var err = 0;
		var image = $('input[name="image1"]').val().trim();
		var thumbnail = $('input[name="thumbnail1"]').val().trim();
		if(image=='')
		{
		   $('div[class="picture"]').css("border", "2px solid #f44336");	   
		   err = 1;
		}
		else{
			$('div[class="picture"]').css({"border": "2px solid #ccc"});
		}
		if(thumbnail=='')
		{
		   $('div[class="picture"]').css("border", "2px solid #f44336");	   
		   err = 1;
		}
		else{
			$('div[class="picture"]').css({"border": "2px solid #ccc"});
		}
		subcat_id=<?php echo $subcat_id;?>;
		$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/get_all_filterbox_ids/"?>',
				type: 'POST',
				data:"subcat_id="+subcat_id,
				dataType: 'JSON',
			
			}).done(function(data)
			  {	

			});

	//form1=additional_details_form_valid();
	//form2=specification_details_form_valid();
	//form3=additional_details_form_valid();
	
	if(err==1)
	{
		//$('#validation_error').show();
		return false;
		
	}else{
			var data = $("#highlight").Editor("getText");
			$("#highlight").val(data);
			
			var data1 = $("#highlight_faq").Editor("getText");
			$("#highlight_faq").val(data1);
			
			var data2 = $("#highlight_whatsinpack").Editor("getText");
			$("#highlight_whatsinpack").val(data2);
			
			var data2 = $("#highlight_aboutthebrand").Editor("getText");
			$("#highlight_aboutthebrand").val(data2);

			
			var form = $('#create_inventory');	
			var form_create = document.forms.namedItem("create_inventory");
			var ajaxData = new FormData(form_create);
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_inventory/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false, 		
				
			}).done(function(data)
			  {	
				if(data==true)
				{
					  swal({
						title:"Success", 
						text:"Inventory is successfully Created!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				else
				{
					swal(
						'Oops...',
						data,
						'error'
					)	
				}
		return true;
	});
	}
}]);
				
		return true;
	}
}
var val;
function specification_details_form_valid()
{
	
	var form_status = $('<div class="form_status"></div>');		
	var form = $('#create_inventory');	
	var err = 0;	
	subcat_id=<?php echo $subcat_id;?>;
		$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/get_all_specification_ids/"?>',
				type: 'POST',
				async:false,
				data:"subcat_id="+subcat_id,
				dataType: 'JSON',	
			}).done(function(data)
			  {	
				if(data)
				{
					err = 0;
					for(i=0;i<data.length;i++){
						specification_id=data[i].specification_id;
						var specification_select_value=document.getElementsByName("specification_select_value["+specification_id+"]")[0];
						var specification_textarea_value=document.getElementsByName("specification_textarea_value["+specification_id+"]")[0];

						if(specification_select_value.value.trim() == '')
						{	
							document.getElementsByName("specification_select_value["+specification_id+"]")[0].setAttribute("style", "border: 1px solid red;");
							err++;				
						}
						else if(specification_select_value.value.trim() == 'Others' && specification_textarea_value.value.trim() == ''){	
							document.getElementsByName("specification_textarea_value["+specification_id+"]")[0].setAttribute("style", "border: 1px solid red;");
							document.getElementsByName("specification_select_value["+specification_id+"]")[0].setAttribute("style", "border: 1px solid #ccc;");err++;								
							
						}else if(specification_select_value.value.trim() == 'Others' && specification_textarea_value.value.trim() != '')
						{	
							document.getElementsByName("specification_select_value["+specification_id+"]")[0].setAttribute("style", "border: 1px solid #ccc;");	
							document.getElementsByName("specification_textarea_value["+specification_id+"]")[0].setAttribute("style", "border: 1px solid #ccc;");
							
						}else if(specification_select_value.value.trim() != '' && specification_select_value.value.trim() != 'Others')
						{	
							document.getElementsByName("specification_select_value["+specification_id+"]")[0].setAttribute("style", "border: 1px solid #ccc;");	
					
						}
						
					}
					
					if(err!=0)
					{
						val=false;
				
					}else{
						val=true;
					}
					
				}
				else
				{
					alert('error');
					
				}
	});
return val;
}


function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}
function editInventoryFun(inventory_id,product_id){
	location.href="<?php echo base_url()?>admin/Catalogue/edit_inventory_form/"+inventory_id+"/"+product_id;

}
function view_delivered_orders_by_inventory_id(inventory_id){
	location.href="<?php echo base_url()?>admin/Orders/delivered_orders_inventory_wise/"+inventory_id;
}
$(document).ready(function(){
	<?php
		if($create_editview=="create"){
			?>
			$("#viewDiv1").css({"display":"none"});
			$("#createDiv2").css({"display":""});
			<?php
		}
		else if($create_editview=="view"){
			?>
			$("#createDiv2").css({"display":"none"});
			$("#viewDiv1").css({"display":""});
			<?php
		}
	?>
});
</script>		
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">


<div class="row" id="viewDiv1" style="display:none;">
<div class="wizard-header text-center">
	<h5>
		 Number of SKUs <span class="badge"><div id="inventory_count"></div></span> 
	</h5>
	<h5><?php echo $product_details["product_name"]; ?></h5>
</div>
<table id="table_inventory" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<th colspan="5">
            <div class="float-right">
                <?php
                if(isset($import_success)){
                    if($import_success==1){
                        ?>
                        <h6><?php echo $import_success_msg; ?></h6>
                        <?php
                    }
                }
                ?>

            </div>
                    <?php 
                                        
                    $admin_user_type=$this->session->userdata("user_type");
                    $admin_module_type=$this->session->userdata("module_type");
                    if($admin_user_type="Su Master Country" && $admin_module_type=='catalog' ){
                        /*only edit - no delete*/
                        ?>
                    
                    <?php 
                    
                    }else{
                        ?>
                        <button id="multiple_delete_button1" class="btn btn-warning btn-xs" onclick="multilpe_delete_fun('inventory')">Archive</button>
			<button id="multiple_delete_button2" class="btn btn-danger btn-xs" onclick="multilpe_delete_when_no_info('inventory')">Delete</button>
                    <?php                    
                    }
                    ?>
                    
			
			<button class="btn btn-primary btn-xs" type="button" onclick="changeViewPrevious()">Change View</button>
            <form  method="post" action="#" id="export_inventory" style="display: none;" >
                <input type="hidden" value="1" name="export_inventory">


                <input type="hidden" value="<?php echo $search_attribute_1_value; ?>" name="search_attribute_1_value">
                <input type="hidden" value="<?php echo $search_attribute_2_value; ?>" name="search_attribute_2_value">
                <input type="hidden" value="<?php echo $search_attribute_3_value; ?>" name="search_attribute_3_value">
                <input type="hidden" value="<?php echo $search_attribute_4_value; ?>" name="search_attribute_4_value">
                <input type="hidden" value="<?php echo $search_product_status; ?>" name="search_product_status">
                <input type="hidden" value="<?php echo $search_view_product_status; ?>" name="search_view_product_status">
                <input type="hidden" value="<?php echo $pcat_id; ?>" name="parent_category">
                <input type="hidden" value="<?php echo $cat_id; ?>" name="categories">
                <input type="hidden" value="<?php echo $subcat_id; ?>" name="subcategories">
                <input type="hidden" value="<?php echo $brand_id; ?>" name="brands">
                <input type="hidden" value="<?php echo $product_id; ?>" name="products">
				<input type="hidden" value="<?php echo $inventory_type; ?>" name="inventory_type">
                <button id="export_inventory" class="btn btn-info btn-xs" type="submit">Export Inventory</button>
                <a href="javascript:void(0);" class="btn btn-info btn-xs" onclick="formToggle('importFrm');"><i class="plus"></i> Import Inventory</a>
            </form>
            <!---import uantity ---->

            <!-- Import link -->
            <div class="col-md-12 head">

            </div>

            <!-- File upload form -->
            <div class="col-md-12 well" id="importFrm" style="display: none;">
                <form action="<?php echo base_url('/admin/Catalogue/inventory_import'); ?>" method="post" enctype="multipart/form-data">
                    <input type="file" name="file" />
                    <input type="hidden" value="1" name="import_inventory">
                    <input type="hidden" value="<?php echo $create_editview; ?>" name="create_editview">
                    <input type="hidden" value="<?php echo $search_attribute_1_value; ?>" name="search_attribute_1_value">
                    <input type="hidden" value="<?php echo $search_attribute_2_value; ?>" name="search_attribute_2_value">
                    <input type="hidden" value="<?php echo $search_attribute_3_value; ?>" name="search_attribute_3_value">
                    <input type="hidden" value="<?php echo $search_attribute_4_value; ?>" name="search_attribute_4_value">
                    <input type="hidden" value="<?php echo $search_product_status; ?>" name="search_product_status">
                    <input type="hidden" value="<?php echo $search_view_product_status; ?>" name="search_view_product_status">
                    <input type="hidden" value="<?php echo $pcat_id; ?>" name="parent_category">
                    <input type="hidden" value="<?php echo $cat_id; ?>" name="categories">
                    <input type="hidden" value="<?php echo $subcat_id; ?>" name="subcategories">
                    <input type="hidden" value="<?php echo $brand_id; ?>" name="brands">
                    <input type="hidden" value="<?php echo $product_id; ?>" name="products">
                    <input type="submit" class="btn btn-primary" name="importSubmit" value="IMPORT">
                </form>
            </div>
            <!---import uantity ---->

		</th>
	</tr>
	<tr>
		<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
		<th>Image</th>
		<th>SKU Details</th>
		<th>Details</th>
		<th>Promo Details</th>
		<th>Action</th>		
	</tr>
</thead>

</table>
</div>


<div class="row" id="createDiv2" style="display:none;">
<div class="col-md-12">
	<div class="wizard-container">
	<div class="card wizard-card" data-color="green" id="wizardProfile">
	<form name="create_inventory" id="create_inventory" enctype="multipart/form-data">

		<?php
		foreach($get_all_specification_group as $get_all_specification_group_value)
		{
			$sp=$controller->get_all_specification($get_all_specification_group_value->specification_group_id);
			?>
			<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				sku_name: {
					required: {
						depends: function(element) {
							const enteredVal = $(this).val();
							let result = $('.result');
							let regex = /^[\s!@#$%^&*()_+{}\[\]:;<>,.?~\\/-]+$/g;
							
							if (regex.test(enteredVal)) {
								result.html(
									`<b style="color: red">
											Please enter valid text!!
										</b>`)
								return true;
							}else {
								result.html(
									`<b style="color: green">
											The entered value is correct!!
										</b>`)

								return false;
							}
						} 
					},
					minlength: 3
				},
				sku_id: {
					required: true,
					minlength: 3
				 },
				
				attribute_1_value: {
					required: true,
				},
			
				vendors: {
					required: true,
				},
//				base_price: {
//					required: true,
//				},
				selling_price: {
					required: true,
				},
				cost_price: {
					required: true,
				},
				max_selling_price: {
					required: true,
				},
//				purchased_price: {
//					required: true,
//				},
				tax: {
					required: true,
				},
				moq: {
					required: true,
		     
				},
				max_oq: {
					required: true,
		      
				},
				low_stock: {
					required: true,
		     
				},
				cutoff_stock: {
					required: true,
		      
				},
				
				inventory_weight_in_kg: {
					required: true,
		      
				},
				inventory_volume_in_kg: {
					required: true,
		       
				},
				inventory_length: {
					required: true,
		       
				},
				inventory_breadth: {
					required: true,
		       
				},
				inventory_height: {
					required: true,
		       
				},
				stock: {
					required: true,
		      
				},
				product_status: {
					required: true,
		      
				},
				product_status_view: {
					required: true,
		      
				},
				'logistics_parcel_category_id[]': {
					required: true,
		      
				},
			<?php
			
			foreach($filterbox as $filterbox_value)
			{
				//$filter=$controller->get_all_filter($filterbox_value->filterbox_id);
				//if(strtolower($filterbox_value->filterbox_name)!="color"){
					
					//foreach($filter as $filter_value)
					//{
						echo "'".'filter['.$filterbox_value->filterbox_id.']'."'".':{';
						echo 'required: true },';
				
					//}
				//}
			}
			?>
			
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
			$.validator.addMethod(
				"regex",
				function(value, element, regexp) {
					var re = new RegExp(regexp);
					return this.optional(element) || re.test(value);
				},
				"Please check your input."
				);

			</script>
		<?php } ?>	
	
		<?php $brand_name=$controller->get_brand_name($brand_id);?>
		
		<div class="wizard-header">

		<h4 class="text-left col-md-12">
			<small>
		<?php 
		
		//print_r( $inventory_data_arr);

		echo $cat_structure["pcat_name"].' > '.$cat_structure["cat_name"].' > '.$cat_structure["subcat_name"].' > '.$cat_structure["brand_name"];

		$inventory_type_txt='';
		
		if($inventory_type=='1'){
			$inventory_type_txt='Only as Main Inventory';
		}elseif($inventory_type=='2'){
			$inventory_type_txt='Only as Addon Inventory';

		}elseif($inventory_type=='3'){
			$inventory_type_txt='Both as Main & as Addon';
		}
		
		?>
		</small>

		

		</h4>

		<h3 class="wizard-title">Create Inventory of  <?php echo $product_details["product_name"]; ?>
	
	
		<?php if($inventory_type!=''){
			echo " <br> <span><small class='text-info'>".$inventory_type_txt.'</small></span>';
		}
		?>

	</h3>
		</div>    
		

	<div class="wizard-navigation">
		<ul>
			<li><a href="#step-11" data-toggle="tab">General</a></li>
			<li><a href="#step-71" data-toggle="tab">Highlights</a></li>
			<li><a href="#step-21" data-toggle="tab">Logistics</a></li>
			<li><a href="#step-31" data-toggle="tab">Attributes</a></li>
			<li><a href="#step-41" data-toggle="tab">Specifications</a></li>
			<li><a href="#step-51" data-toggle="tab">Filters</a></li>
			<li><a href="#step-61" data-toggle="tab">Images</a></li>
		</ul>
	</div>

	<input name="product_id" type="hidden" id="product_id" value="<?php echo $product_id;?>">
	<input name="inventory_type" type="hidden" id="inventory_type" value="<?php echo $inventory_type;?>">
	<div class="tab-content">
	
	<!----------Attributes-------->
	
	<div class="tab-pane" id="step-31">
			
			<?php
			foreach($attributes as $attributes_value)
			{
			?>

			<input type="hidden" name="attribute_1" value="<?php echo $attributes_value->attribute1_name;?>">
			<?php $attribute_values=explode(',',$attributes_value->attribute1_option); ?>
            
			<?php
			
			if(strtolower($attributes_value->attribute1_name)=="color"){
				?>
				
			<div class="row">
				<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">-Select <?php echo $attributes_value->attribute1_name;?>-</label>
					<select onchange="display_color(this.value);assign_filter_values(this.value,'<?php echo trim(strtolower($attributes_value->attribute1_name));?>','color')" name="attribute_1_value" id="attribute_1_value" class="form-control">
						<option value=""></option>
					
						<?php
							foreach($attribute_values as $val){
								if($val!=""){
									$color_arr=explode(':',$val);
									echo '<option value="'.$val.'">'.$color_arr[0].'</option>';
								}
							}
						?>
					
					</select>
				</div>
				</div>
			 </div>
			
				<?php
			
			}else{
			
			?>
			<div class="row">
				<div class="col-md-5 col-md-offset-1">
				<div class="form-group label-floating">
					<label class="control-label">-Select <?php echo $attributes_value->attribute1_name;?>-</label>
					<select name="attribute_1_value" class="form-control" onchange="assign_filter_values(this.value,'<?php echo trim(strtolower($attributes_value->attribute1_name));?>')">
						<option value=""></option>
						<?php
							foreach($attribute_values as $val){
								if($val!=""){
									echo '<option value="'.$val.'">'.$val.'</option>';
								}
						}
						?>
			
					</select>
				</div>
				</div>
			</div>	
			
			<?php } ?>

			<?php if(!empty($attributes_value->attribute2_name)){ ?>
		 
				<input type="hidden" name="attribute_2" value="<?php echo $attributes_value->attribute2_name;?>">

			<div class="row">
			<div class="col-md-5 col-md-offset-1">
			<div class="form-group label-floating">
				<?php $attribute_values=explode(',',$attributes_value->attribute2_option); ?>
				<label class="control-label">-Select <?php echo $attributes_value->attribute2_name;?>-</label>
				<select name="attribute_2_value" class="form-control" onchange="assign_filter_values(this.value,'<?php echo trim(strtolower($attributes_value->attribute2_name));?>')">
					<option value=""></option>
					<?php
					foreach($attribute_values as $val)
					{
						if($val!=""){
							echo '<option value="'.$val.'">'.$val.'</option>';
						}
					}
					?>
				</select>
			</div>
			</div>
			</div>
       
		<?php }else{ ?>	 
		  
			<input type="hidden" name="attribute_4" value="<?php echo $attributes_value->attribute4_name;?>">
			<input type="hidden" name="attribute_4_value" value="<?php echo $attributes_value->attribute4_name;?>">
		<?php } ?>
		  
		<?php if(!empty($attributes_value->attribute3_name)){ ?>

				<input type="hidden" name="attribute_3" value="<?php echo $attributes_value->attribute3_name;?>">

			<div class="row">
				<div class="col-md-5 col-md-offset-1">
				<div class="form-group label-floating">
				<?php $attribute_values=explode(',',$attributes_value->attribute3_option); ?>
					<label class="control-label">-Select <?php echo $attributes_value->attribute3_name;?>-</label>
					<select name="attribute_3_value" class="form-control" onchange="assign_filter_values(this.value,'<?php echo trim(strtolower($attributes_value->attribute3_name));?>')">
						<option value=""></option>
						<?php
						foreach($attribute_values as $val)
						{
							if($val!=""){
								echo '<option value="'.$val.'">'.$val.'</option>';
							}
						}
					?>
					</select>
				</div>
				</div>
			</div>
			<?php }else{ ?>	 
		  
			<input type="hidden" name="attribute_3" value="<?php echo $attributes_value->attribute3_name;?>">
			<input type="hidden" name="attribute_3_value" value="<?php echo $attributes_value->attribute3_name;?>">
			   <?php } ?>
		  
		  
			<?php if(!empty($attributes_value->attribute4_name)){ ?>

				<input type="hidden" name="attribute_4" value="<?php echo $attributes_value->attribute4_name;?>">

			<div class="row">
				<div class="col-md-5 col-md-offset-1">
				<div class="form-group label-floating">
				<?php $attribute_values=explode(',',$attributes_value->attribute4_option); ?>
				<label class="control-label">-Select <?php echo $attributes_value->attribute4_name;?>-</label>
				<select name="attribute_4_value" class="form-control" onchange="assign_filter_values(this.value,'<?php echo trim(strtolower($attributes_value->attribute4_name));?>')">
					<option value=""></option>
					<?php
						foreach($attribute_values as $val)
						{
							if($val!=""){
								echo '<option value="'.$val.'">'.$val.'</option>';
							}
						}
					?>
				</select>
				</div>
				</div>
			</div>
			
			<?php }else{ ?>	 
		  
			<input type="hidden" name="attribute_4" class="form-control" value="<?php echo $attributes_value->attribute4_name;?>">
			<input type="hidden" name="attribute_4_value"  value="<?php echo $attributes_value->attribute4_name;?>">
			   <?php
			} 
		  ?>
		  
		  <?php
		} ?>
			
		
	</div>
	
	<!----------Attributes-------->
	
	<div class="tab-pane" id="step-11">
		<div class="row">
			<div class="col-md-12">
			    <h4 class="info-text"> Product and Stock Information</h4>
			</div>
		</div>

		<div class="row">
				<div class="col-md-5 col-md-offset-1"> 
					<div class="form-group label-floating">
						<label class="control-label">Enter Display Name on Button 1</label>
						<input name="sku_display_name_on_button_1" type="text" class="form-control" />
					</div>
				</div>
				<div class="col-md-5"> 	
				
					<div class="form-group label-floating">
						<label class="control-label">Enter Display Name on Button 2</label>
						<input name="sku_display_name_on_button_2" type="text" class="form-control" value="Pick Me!"/>
					</div>
				</div>
			</div>
		

                <div class="row">

                        <div class="col-md-10 col-md-offset-1">  
                        <div class="form-group label-floating">
                                <label class="control-label">Seller SKU</label>
                                <input name="sku_id" type="text" class="form-control" />
                        </div>
                        </div>
                </div>
		<div class="row">
			<div class="col-md-10 col-md-offset-1"> 
                            <div class="form-group label-floating">
                                    <label class="control-label">SKU Name </label>
                                    <input name="sku_name" id="sku_name" type="text" class="form-control" onkeypress="return blockSpecialChar(event)"/>
									<p class="result"></p>
                            </div>
			</div>
		</div>

		
		
		

                
                
		<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Product Status-</label>
					<select name="product_status_view" id="product_status_view" class="form-control search_select">
						<option value=""></option>
						<option value="1">Active</option>
						<option value="0">Inactive</option>
					</select>
				</div>
				</div>
				
		</div>
            <div class="row" style="display:none;">
		
                    
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">Company Cost Price (<?php echo curr_sym; ?>) </label>
					<input name="purchased_price" id="purchased_price" onKeyPress="return isNumber(event)" type="text" class="form-control" />
				</div>
			</div>
			
			
			<div class="col-md-5"> 
				<div class="form-group label-floating">
					<label class="control-label">Company Base Price (<?php echo curr_sym; ?>) </label>
					<input name="base_price" id="base_price" onKeyPress="return isNumber(event)" type="text" class="form-control"  />
				</div>
			</div>
			
			
		</div>
		
		<div class="row">
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group label-floating" id="max_selling_price_div">
					<label class="control-label">MRP (<?php echo curr_sym;?>) {Including tax}</label>
					<input name="max_selling_price" id="max_selling_price" onKeyPress="return isNumber(event)" type="text" class="form-control" onkeyup="calculateTax_priceFun()"/>
				</div>
			</div>
			<div class="col-md-5"> 
			<div class="form-group label-floating">
                                        <label class="control-label" id="selling_price_div">Web Selling Price (<?php echo curr_sym;?>) {WSP - with any discount applied}
                                        </label>
                                        <input name="selling_price" id="selling_price" type="text" class="form-control" onKeyPress="return isNumber(event)" onkeyup="calculateSelling_priceDiscountFun();calculateTax_priceFun();"/>
                                </div>
			</div>
		</div>
           
                <!--added newly---->
                <div class="row">

                        <div class="col-md-5 col-md-offset-1"> 
                                <div class="form-group label-floating">
                                        <label class="control-label" id="cost_price_div">Cost Price (<?php echo curr_sym;?>) {the cost of the product to the company}
                                        </label>
                                        <input name="cost_price" id="cost_price" type="text" class="form-control" onKeyPress="return isNumber(event)"/>
                                </div>

                        </div>

                        <div class="col-md-5"> 
                                <div class="form-group label-floating" id="selling_discount_div"> 
                                        <label class="control-label">
                                        Discount Percent {% of WSP from MRP}</label>
                                        <input name="selling_discount" id="selling_discount" type="text" class="form-control" readOnly  />
                                </div>
                        </div>
                </div>
                
                <div class="row">
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group label-floating" id="tax_percent_price_div">
					<label class="control-label">Tax % Value</label>
					<input name="tax_percent_price" id="tax_percent_price" type="text" class="form-control"  readOnly />
				</div>
			</div>
			
			<div class="col-md-5"> 
				<div class="form-group label-floating" id="taxable_price_div">
					<label class="control-label">Taxable Value </label>
					<input name="taxable_price" id="taxable_price" type="text" class="form-control" readonly/>
				</div>
			</div>
		</div>
                
                <div class="row">
			<div class="col-md-2 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">CGST (%)</label>
					<input name="CGST" id="CGST" type="text" onKeyPress="return isNumber(event)" class="form-control"  onkeyup="calculateTaxPercentFun();"/>
				</div>
			</div>
			
			<div class="col-md-3"> 
				<div class="form-group label-floating" id="IGST_div">
					<label class="control-label">IGST (%)</label>
					<input name="IGST" id="IGST" onKeyPress="return isNumber(event)" type="text" class="form-control"  onkeyup="calculateTaxPercentFun();"/>
				</div>
			</div>
			<div class="col-md-3"> 
				<div class="form-group label-floating" id="SGST_div">
					<label class="control-label">SGST (%)</label>
					<input name="SGST" id="SGST" onKeyPress="return isNumber(event)" type="text" class="form-control"  onkeyup="calculateTaxPercentFun();"/>
				</div>
			</div>
			
			<div class="col-md-2 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">Tax (%)</label>
					<input name="tax" id="tax" type="text" onKeyPress="return isNumber(event)" class="form-control" value="" readonly/>
				</div>
			</div>
			
			
		</div>
                
                

                <!--added newly---->


		
		<?php
		/*
		?>
		<div class="row">
			
			
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group">
					<label class="control-label">Quantity
					</label>
					<input name="mrp_quantity" id="mrp_quantity" type="text" class="form-control"   value=""/>
				</div>
				
			</div>
		</div>
		<?php
		*/
		?>
		
		<div class="row">
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">Price validity</label>
					<input name="price_validity" id="price_validity" type="text" class="form-control"/>
				</div>
			</div>
			<div class="col-md-5"> 
				<div class="form-group label-floating">
					<label class="control-label">Material (wood/steel/etc., )</label>
					<input name="material" id="material" type="text" class="form-control"/>
				</div>
			</div>
			
		</div>
		
		<div class="row">
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">Enter Minimum Order Quantity</label>
					<input name="moq" type="number" class="form-control" min="1" value="1" />
				</div>
			</div>
			<div class="col-md-5"> 	
			
				<div class="form-group label-floating">
					<label class="control-label">Enter Maximum Order Quantity</label>
					<input name="max_oq" type="number" class="form-control" min="1" value="1000"/>
				</div>
			</div>
		</div>
		
		<div class="row">
			
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">-Select Stock Status-</label>
					<select name="product_status" id="product_status" class="form-control search_select">
						<option value="" selected></option>
						<option value="In Stock" selected>Ready In Stock</option>
						<option value="Made To Order">Made To Order</option>
						<option value="Out Of Stock">Out Of Stock</option>
						<option value="Discontinued">Discontinued</option>
					</select>
				</div>
			</div>

			<div class="col-md-5 "> 
				<div class="form-group label-floating">	
					<label class="control-label">Stock count</label>
					<input class="form-control" name="stock" id="stock" type="number" onkeyup="assign_filter_values_stock()">
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">Low Stock Count (Warning Qty)</label>    
                                        <input class="form-control" name="low_stock" type="number" onkeyup="stock_count_validation();" id="low_stock" value="100">
				</div>
			</div>
			<div class="col-md-5"> 
				<div class="form-group label-floating">
					<label class="control-label">Cutoff Stock Count</label>     
                                        <input class="form-control" name="cutoff_stock" id="cutoff_stock" type="number"  onkeyup="assign_filter_values_stock();stock_count_validation();" value="25">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 col-md-offset-1">
				<input type="hidden" name="add_to_cart" value="no"/>
				<input name="add_to_cart" id="add_to_cart"  type="checkbox" value="yes" />
				<label class="control-label">Add to Cart</label>
				<p id="buttonDisplayErrorMessage" class="text-danger"></p>
			</div>
			<div class="col-md-5"> 
				<input type="hidden" name="request_for_quotation" value="no"/>
				<input name="request_for_quotation" id="request_for_quotation"  type="checkbox" value="yes" />
				<label class="control-label">Request For Quotation</label>
			</div>
		</div>

		<script>
		function stock_count_validation(){
			low_stock=parseInt($('#low_stock').val());
			cutoff_stock=parseInt($('#cutoff_stock').val());
			if(low_stock!='' && cutoff_stock!=''){
				if(low_stock<cutoff_stock){
					alert('Low stock count should be greater than cutoff stock');
					$('#cutoff_stock').val('');
				}
			}	
		}
		</script>
		
		
	</div>
	
	<div class="tab-pane" id="step-71">
	

	<!---positioning ------->
	
	<div class="panel-group" id="positioning" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="positionheadingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#positioning_div" aria-expanded="true" aria-controls="positioning_div">
                        
                        Postioning
                    </a>
                </h4>
            </div>
            <div id="positioning_div" class="panel-collapse collapse" role="tabpanel" aria-labelledby="positionheadingOne">
                <div class="panel-body">
                      
					  	<div class="row">
							<div class="col-md-8 col-md-offset-2"> 	
								<div class='item'>
									<div class="form-group label-floating">
										<label class="control-label">Positioning title</label>   
										<input class="form-control"  type="text" name="positioning_uhave_title">
									</div>
		
								</div>
								
							</div>
							
						</div>

						<div class="row">
							<div class="col-md-8 col-md-offset-2"> 	
								<div class='item'>
									<div class="form-group label-floating">
										<label class="control-label">Positioning title 2</label>   
										<input class="form-control"  type="text" name="positioning_others_have_title">
									</div>
		
								</div>
								
							</div>
							
						</div>
						
						<div class="row">
							<div class="col-md-8 col-md-offset-2"> 	
								<div class='item'>
									<div class="form-group label-floating">
										<label class="control-label">Content</label>   
										<input class="form-control"  type="text" name="position_text[1]">
									</div>

		
								</div>
								
							</div>
							
						</div>

						<!---tt-->

	<div class="row">
		<div class="col-md-8 col-md-offset-2"> 
				<div class="form-group">
				<div class="col-md-5 col-md-offset-1">Do you have ?</div>
				<div class="col-md-3">
					<label class="radio-inline"><input name="position_uhave[1]" value="yes" type="radio" >Yes</label>
				</div>
				<div class="col-md-3">
					<label class="radio-inline"><input name="position_uhave[1]" value="no" type="radio">No</label>
				</div>
		
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 col-md-offset-2"> 
			<div class="form-group">
				<div class="col-md-5 col-md-offset-1">Do others have ?</div>
				<div class="col-md-3">
					<label class="radio-inline"><input name="position_others_have[1]" value="yes" type="radio" >Yes</label>
				</div>
				<div class="col-md-3">
					<label class="radio-inline"><input name="position_others_have[1]" value="no" type="radio">No</label>
				</div>
		
			</div>
		</div>

	</div>
	<div class="row">
		<div class="col-md-8 col-md-offset-2"> 
			<div class="form-group">
	
	
				<button type="button" id="add" class="btn btn-sm btn-success pull-right" onclick="add_items()">Add +</button>

			</div>
		</div>
	</div>


						<!---tt-->

						<div class="row">
							<div class="col-md-12 col-md-offset-1"> 	
								<div id="items"></div>
							</div>
						</div>
						
						
                </div>
            </div>

        </div>
        </div>
		
	
	<!---positioning ------->

	<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        
                        Product Details
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                      
					  <div class="row">
							<div class="col-md-12"> 	
									<textarea class="form-control" name="highlight" id="highlight"></textarea>
							</div>
						</div>
						
						
                </div>
            </div>
        </div>
        </div>
		
		
		
		<div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                        
                        FAQ
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                      
					  
					<div class="row">
						<div class="col-md-12"> 	
								<textarea class="form-control" name="highlight_faq" id="highlight_faq"></textarea>
						</div>
					</div>
						
						
                </div>
            </div>
        </div>
        </div>
	
	
	 		<div class="panel-group" id="accordion4" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                        
                        What's in the pack?
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                      
					  
					<div class="row">
						<div class="col-md-12"> 	
								<textarea class="form-control" name="highlight_whatsinpack" id="highlight_whatsinpack"></textarea>
						</div>
					</div>
						
						
                </div>
            </div>
        </div>
        </div>
		
		
		<div class="panel-group" id="accordion5" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingDownloads">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDownloads" aria-expanded="true" aria-controls="collapseDownloads">
                        Downloads
                    </a>
                </h4>
            </div>
            <div id="collapseDownloads" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingDownloads">
                <div class="panel-body">
                      
					  
					<div class="row">
						<div class="col-md-6">
							<div class="col-md-12"> 	
								<input type="text" name="file_1_name" id="file_1_name" class="form-control" placeholder="Name of file 1">
							</div>
							<div class="col-md-12"> 	
								<input type="file" name="file_1_upload" id="file_1_upload">
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="col-md-12"> 	
								<input type="text" name="file_2_name" id="file_2_name" class="form-control" placeholder="Name of file 2">
							</div>
							<div class="col-md-12"> 	
								<input type="file" name="file_2_upload" id="file_2_upload">
							</div>
						</div>
					</div>
					
					
					<div class="row">
						<div class="col-md-6">
							<div class="col-md-12"> 	
								<input type="text" name="file_3_name" id="file_3_name" class="form-control" placeholder="Name of file 3">
							</div>
							<div class="col-md-12"> 	
								<input type="file" name="file_3_upload" id="file_3_upload">
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="col-md-12"> 	
								<input type="text" name="file_4_name" id="file_4_name" class="form-control" placeholder="Name of file 4">
							</div>
							<div class="col-md-12"> 	
								<input type="file" name="file_4_upload" id="file_4_upload">
							</div>
						</div>
					</div>
					
					
					<div class="row">
						<div class="col-md-6">
							<div class="col-md-12"> 	
								<input type="text" name="file_5_name" id="file_5_name" class="form-control" placeholder="Name of file 5">
							</div>
							<div class="col-md-12"> 	
								<input type="file" name="file_5_upload" id="file_5_upload">
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="col-md-12"> 	
								<input type="text" name="file_6_name" id="file_6_name" class="form-control" placeholder="Name of file 6">
							</div>
							<div class="col-md-12"> 	
								<input type="file" name="file_6_upload" id="file_6_upload">
							</div>
						</div>
					</div>
					
						
						
                </div>
            </div>
        </div>
        </div>
		
		
		
		
		<!--- product technical specs starts -->
		
		<div class="panel-group" id="accordion6" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingProductTechnicalSpecs">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseProductTechnicalSpecs" aria-expanded="true" aria-controls="collapseProductTechnicalSpecs">
                        Product Technical Specs
                    </a>
                </h4>
            </div>
            <div id="collapseProductTechnicalSpecs" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingProductTechnicalSpecs">
                <div class="panel-body">
                      
					  <?php
						for($spec_i=1;$spec_i<=10;$spec_i++){
					  ?>
					  
					<div class="row">
						<div class="col-md-3">
							<input type="text" name="prod_tech_spec_name_<?php echo $spec_i;?>" id="prod_tech_spec_name_<?php echo $spec_i;?>" class="form-control" placeholder="Name <?php echo $spec_i;?>">
						</div>
						
						<div class="col-md-8">
							<textarea name="prod_tech_spec_desc_<?php echo $spec_i;?>" id="prod_tech_spec_desc_<?php echo $spec_i;?>" class="form-control" placeholder="Description <?php echo $spec_i;?>" rows="5"></textarea>
						</div>
					</div>
					
					
					 <?php
						}
					  ?>
						
						
                </div>
            </div>
        </div>
        </div>
		
		
		<!--- product technical specs ends -->
		
		
		<!-- About the brand starts --->
		
		<div class="panel-group" id="accordion7" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingAboutthebrand">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseAboutthebrand" aria-expanded="true" aria-controls="collapseAboutthebrand">
                        About the Brand
                    </a>
                </h4>
            </div>
            <div id="collapseAboutthebrand" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingAboutthebrand">
                <div class="panel-body">
                      
					  
					<div class="row">
						<div class="col-md-12"> 	
								<textarea class="form-control" name="highlight_aboutthebrand" id="highlight_aboutthebrand"></textarea>
						</div>
					</div>
						
						
                </div>
            </div>
        </div>
        </div>
		
		<!-- About the brand ends --->
		
		
		
		
		
		<!--- testimonial starts -->
		
		<div class="panel-group" id="accordion7" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTestimonial">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTestimonial" aria-expanded="true" aria-controls="collapseTestimonial">
                        What customers opinion about this product
                    </a>
                </h4>
            </div>
            <div id="collapseTestimonial" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTestimonial">
                <div class="panel-body">
                      
					
					 <h6>First Entry</h6> 
					<div class="row">
						<div class="col-md-12">
							<input type="text" name="testimonial_name_1" id="testimonial_name_1" class="form-control" placeholder="Name">
						</div>
						
						<div class="col-md-12">
							<textarea name="testimonial_desc_1" id="testimonial_desc_1" class="form-control" placeholder="Description" rows="5"></textarea>
						</div>
					</div>
					<h6>Second Entry</h6> 
					<div class="row">
						<div class="col-md-12">
							<input type="text" name="testimonial_name_2" id="testimonial_name_2" class="form-control" placeholder="Name">
						</div>
						
						<div class="col-md-12">
							<textarea name="testimonial_desc_2" id="testimonial_desc_2" class="form-control" placeholder="Description" rows="5"></textarea>
						</div>
					</div>
					<h6>Third Entry</h6> 
					<div class="row">
						<div class="col-md-12">
							<input type="text" name="testimonial_name_3" id="testimonial_name_3" class="form-control" placeholder="Name">
						</div>
						
						<div class="col-md-12">
							<textarea name="testimonial_desc_3" id="testimonial_desc_3" class="form-control" placeholder="Description" rows="5"></textarea>
						</div>
					</div>
					
					
					
						
                </div>
            </div>
        </div>
        </div>
		
		
		<!--- testimonial ends -->
		
		

			
		
		<!--- Warranty starts -->
		
		<div class="panel-group" id="accordion8" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingWarranty">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseWarranty" aria-expanded="true" aria-controls="collapseWarranty">
                        Warranty
                    </a>
                </h4>
            </div>
            <div id="collapseWarranty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingWarranty">
                <div class="panel-body">
                      
					<div class="row">
						<div class="col-md-12">
							<input type="text" name="warranty" id="warranty" class="form-control" placeholder="Warranty">
						</div>
					</div>
				
					
					
					
						
                </div>
            </div>
        </div>
        </div>
		
		
		<!--- Warranty ends -->

		<!--- emi starts -->
		
		<div class="panel-group" id="accordion_emi" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingemi">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsemi" aria-expanded="true" aria-controls="collapsemi">
                        EMI options
                    </a>
                </h4>
            </div>
            <div id="collapsemi" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingemi">
                <div class="panel-body">
                      
				<div class="row">
				<div class="col-md-10"> 
					<div class="form-group">
					<div class="col-md-5 col-md-offset-1">Do you have EMI options ?</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="emi_options" id="emi1" value="yes" type="radio" >Yes</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="emi_options" id="emi2" value="no" type="radio">No</label>
					</div>
				
					</div>
				</div>
			</div>	

						
                </div>
            </div>
        </div>
        </div>
		
		
		<!--- emi ends -->

		<!--- emi starts -->
		
		<div class="panel-group" id="accordion_brouchure" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading_brouchure">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collaps_brouchure" aria-expanded="true" aria-controls="collaps_brouchure">
                       Product Brouchure
                    </a>
                </h4>
            </div>
            <div id="collaps_brouchure" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_brouchure">
                <div class="panel-body">
                      
				<div class="row">
				<div class="col-md-10"> 
					<div class="form-group">
					<div class="col-md-5 col-md-offset-1"><label class="form-label" for="brouchure">Upload Product Brouchure </label></div>
					<div class="col-md-5">
						<input name="brouchure" id="brouchure" type="file" class="form-control" style="opacity: inherit;
    height: 50px;">
					</div>
				
					</div>
				</div>
			</div>	

						
                </div>
            </div>
        </div>
        </div>
		
		
		<!--- emi ends -->
		
		
		
		<!--- Shipswithin starts -->
		
		<div class="panel-group" id="accordion8" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingShipswithin">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseShipswithin" aria-expanded="true" aria-controls="collapseShipswithin">
                        Shipping Timeline
                    </a>
                </h4>
            </div>
            <div id="collapseShipswithin" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingShipswithin">
                <div class="panel-body">
                      
					<div class="row">
						<div class="col-md-6">
							<select type="text" name="shipswithin" id="shipswithin" class="form-control" >
								<option value="">Shipped In XX Number Of Days</option>
								<?php
								/*for($i=0;$i<=60;$i++){
									?>
									<option value="<?php echo $i; ?>"><?php echo $i ?> </option>
									<?php
								}*/
								?>
								<option value=""></option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="10">10</option>
								<option value="15">15</option>
								<option value="20">20</option>
								<option value="30">30</option>
								<option value="45">45</option>
								<option value="60">60</option>
							
							</select>
						</div>
					</div>
		
                </div>
            </div>
        </div>
        </div>
		
		
		<!--- Shipswithin ends -->
		
		<div class="panel-group" id="accordion_tag" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading_tag">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_tag" aria-expanded="true" aria-controls="collapse_tag">
                       Tag to Frontend
                    </a>
                </h4>
            </div>
            <div id="collapse_tag" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_tag">
                <div class="panel-body">
                      
					<div class="row">
					<div class="col-md-4 col-md-offset-1"><label class="form-label" for="brouchure">Frontend Display of Product Under  </label></div>
						<div class="col-md-6">
							
								<?php
								foreach($parent_category as $parent){
									?>
									<input type="checkbox" name="under_pcat_frontend[]" id="typ_<?php echo $parent->pcat_id; ?>" value="<?php echo $parent->pcat_id; ?>"> <label class="control-label" for="typ_<?php echo $parent->pcat_id; ?>"><?php echo $parent->pcat_name; ?> </label>
									<?php
								}
								?>
							
						</div>
					</div>
				
					
					
					
						
                </div>
            </div>
        </div>
        </div>
		
		
		
		
	</div>
	
	<div class="tab-pane" id="step-21">
		                             
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating"> 
					<label class="control-label">Vendor-</label>

					<?php 
					
					if(!empty($brand_data)){
						echo $brand_data["vendor_name"];
						echo '<input type="hidden" name="vendors" id="vendors" value="'.$brand_data["vendor_id"].'">';
					}
					
					?>
					<?php /*
					<select class="form-control" name="vendors" id="vendors">
						<option value=""></option>
						<?php foreach ($vendors as $vendors_value) {  ?>
						<option value="<?php echo $vendors_value->vendor_id; ?>"><?php echo $vendors_value->name; ?></option>
						<?php } ?>
					</select> */ ?>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group">
					<label class="control-label">-Select logistics parcel category-</label>
					<select name="logistics_parcel_category_id[]" id="logistics_parcel_category" class="form-control" multiple="multiple">
					<?php
					foreach($logistics_parcel_category as $logistics_parcel_category_value)
					{
						echo '<option value="'.$logistics_parcel_category_value->logistics_parcel_category_id.'">'.$logistics_parcel_category_value->logistics_name.' - '.$logistics_parcel_category_value->parcel_type.'</option>';
					}
					?>
					</select>
				</div>
				</div>
				
				<div class="col-md-5"> 
				<div class="form-group">
					<label class="control-label">Select Free shipping Territories</label>
					<select name="logistics_territory_id[]" id="logistics_territory" class="form-control" multiple>
				
					<?php
					foreach($logistics_all_territory_name as $logistics_all_territory_name_value)
					{
						echo '<option value="'.$logistics_all_territory_name_value->territory_name_id.'">'.$logistics_parcel_category_value->logistics_name.' - '.$logistics_all_territory_name_value->territory_name.'</option>';
					}
					?>
					</select>
				</div>
				</div>
			
			</div>
			
			
			
			<div class="row">
				<div class="col-md-5 col-md-offset-1"> 
					<div class="form-group">
					<script>
						function shipping_charge_not_applicableFun(obj){
						$("#flat_shipping_charge").val("");
						$("#inventory_weight_in_kg").val("");
						$("#inventory_volume_in_kg").val("");
						$("#inventory_length").val("");
						$("#inventory_breadth").val("");
						$("#inventory_height").val("");
						
					if(obj.checked==true){
						$("#flat_shipping_charge_div").css({"display":""});
						if(!$("#flat_shipping_charge").attr("required")){
							$("#flat_shipping_charge").attr("required",true);
						}
						
						$("#inventory_weight_in_kg_inventory_volume_in_kg_div").css({"display":"none"});
						if($("#inventory_weight_in_kg").attr("required")){
							$("#inventory_weight_in_kg").attr("required",false);
						}
						if($("#inventory_volume_in_kg").attr("required")){
							$("#inventory_volume_in_kg").attr("required",false);
						}
						
						$("#inventory_length_div").css({"display":"none"});
						if($("#inventory_length").attr("required")){
							$("#inventory_length").attr("required",false);
						}
						$("#inventory_breadth_div").css({"display":"none"});
						if($("#inventory_breadth").attr("required")){
							$("#inventory_breadth").attr("required",false);
						}
						$("#inventory_height_div").css({"display":"none"});
						if($("#inventory_height").attr("required")){
							$("#inventory_height").attr("required",false);
						}
						
						
					}
					else{
						$("#flat_shipping_charge_div").css({"display":"none"});
						if($("#flat_shipping_charge").attr("required")){
							$("#flat_shipping_charge").attr("required",false);
						}
						
						$("#inventory_weight_in_kg_inventory_volume_in_kg_div").css({"display":""});
						if(!$("#inventory_weight_in_kg").attr("required")){
							$("#inventory_weight_in_kg").attr("required");
						}
						if(!$("#inventory_volume_in_kg").attr("required")){
							$("#inventory_volume_in_kg").attr("required");
						}
						
						$("#inventory_length_div").css({"display":""});
						if(!$("#inventory_length").attr("required")){
							$("#inventory_length").attr("required");
						}
						$("#inventory_breadth_div").css({"display":""});
						if(!$("#inventory_breadth").attr("required")){
							$("#inventory_breadth").attr("required");
						}
						$("#inventory_height_div").css({"display":""});
						if(!$("#inventory_height").attr("required")){
							$("#inventory_height").attr("required");
						}
					}
				}
					</script>
						<input name="shipping_charge_not_applicable" id="shipping_charge_not_applicable"  type="checkbox" value="yes" onchange="shipping_charge_not_applicableFun(this)"/>
						<label class="control-label">Shipping Charge Not Applicable
						</label>
					</div>
				</div>
				<div class="col-md-5"  id="flat_shipping_charge_div"> 
					<div class="form-group">
						<label class="control-label">
						</label>
						
						<input class="form-control" name="flat_shipping_charge" type="text" id="flat_shipping_charge"   value="" onKeyPress="return isNumber(event)"/>
					</div>
				</div>			 
			</div>	
			
	
	
	
			<div class="row" id="inventory_weight_in_kg_inventory_volume_in_kg_div">
				<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">Enter Consignment Actual Weight in kg</label>
					<input class="form-control" name="inventory_weight_in_kg" id="inventory_weight_in_kg" type="number" step="any">
				</div>
				</div>
				<div class="col-md-5"> 
				<div class="form-group label-floating">
					<label class="control-label">Enter Volumetric Weight in cms (L x B x H in cms)</label>
					<input class="form-control" name="inventory_volume_in_kg" id="inventory_volume_in_kg" type="number" step="any">
				</div>
				</div>
			</div>
			<div class="row" id="inventory_unit_div">
				<div class="col-md-5 col-md-offset-1"> 
					<div class="form-group">
						<label class="control-label">Select Unit
						</label>
						<select class="form-control" name="inventory_unit" id="inventory_unit" >
							<option value=""></option>
							<option value="mm">mm</option>
							<option value="cm">cm</option>
							<option value="m">m</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row" id="inventory_length_div">
				<div class="col-md-5 col-md-offset-1"> 
					<div class="form-group">
						<label class="control-label">Inventory Length
						</label>
						<input class="form-control" name="inventory_length" id="inventory_length"  type="number" step="any"/>
					</div>
				</div>
			</div>

			<div class="row" id="inventory_breadth_div">
				<div class="col-md-5 col-md-offset-1"> 
					<div class="form-group">
						<label class="control-label">Inventory Breadth
						</label>
						<input class="form-control" name="inventory_breadth" id="inventory_breadth"  type="number" step="any"/>
					</div>
				</div>
			</div>	
			<div class="row" id="inventory_height_div">
				<div class="col-md-5 col-md-offset-1"> 
					<div class="form-group">
						<label class="control-label">Inventory Height
						</label>
						<input class="form-control" name="inventory_height" id="inventory_height"  type="number" step="any"/>
					</div>
				</div>
			</div>	
			
			<?php
	/*
	?>
				<div class="row">
		<div class="col-md-5 col-md-offset-1"> 
			<div class="form-group">
				<label class="control-label">Enter Volumetric Breakup Point
				</label>
				<input class="form-control" name="volumetric_breakup_point" id="volumetric_breakup_point"  type="number"/>
			</div>
		</div>
	</div>	
	<?php
	*/
	?>
			
		
	</div>


	<div class="tab-pane well CamListDiv" id="step-41">
       
		<?php
		foreach($get_all_specification_group as $get_all_specification_group_value)
		{
			?>
		<div class="panel-group">
			<div class="panel panel-default">
			
				<div class="panel-heading cursor-pointer" data-toggle="collapse" href="#collapse<?php echo $get_all_specification_group_value->specification_group_id; ?>">
					<h4 class="panel-title">
						<a href="#"><?php echo $get_all_specification_group_value->specification_group_name; ?></a>
					</h4>
				</div>
			
			 <div id="collapse<?php echo $get_all_specification_group_value->specification_group_id; ?>" class="panel-collapse collapse">
			 <div class="panel-body">
			<?php
			$sp=$controller->get_all_specification($get_all_specification_group_value->specification_group_id);
			
			$i=0;
			foreach($sp as $k =>$sp_value)
			{
				$i++;
				if($i%2==1 || $i==1){
					
					echo '</div><div class="row">'; 
				}
			?>
				
				<div class="col-md-5 <?php if($k%2==0){?>col-md-offset-1<?php } ?>">

				<div class="form-group label-floating">
			<label class="control-label">-Select <?php echo $sp_value->specification_name; ?>-</label> 
					<select name="specification_select_value[<?php echo $sp_value->specification_id;?>]" class="form-control" onchange="spec_fun(this,<?php echo $sp_value->specification_id;?>)">
					<option value=""></option>
					<?php
					$values=explode(',',$sp_value->specification_value);
					foreach($values as $val)
					{
						if($val!=""){
							echo '<option value="'.$val.'">'.$val.'</option>';
						}
					}
					?>
					<option value="Others">Others</option>
					<option value="0">-None Selected-</option>
					</select>
					
					
				</div>
				
			
				<div class="specification_textarea_value[<?php echo $sp_value->specification_id;?>]" style="display:none;">
					<div class="form-group label-floating">
					<textarea name="specification_textarea_value[<?php echo $sp_value->specification_id;?>]" id="textarea_<?php echo $get_all_specification_group_value->specification_group_id."_".$i; ?>" class="form-control" placeholder="Enter your own <?php echo $sp_value->specification_name; ?> value"></textarea>
				
					</div>
				</div>
			
				
			</div>
		
				
			<?php
				
			}
			?>
			</div>
			</div>
			</div>  
			</div>     
			<?php
			
		}

		?>
                                  
		 
	</div>
	<div class="tab-pane well CamListDiv" id="step-51">
       <div class="row">
			<?php
			foreach($filterbox as $k => $filterbox_value)
			{
			$filter=$controller->get_all_filter($filterbox_value->filterbox_id);
			?>
<div id="filter_select"></div>
			  <?php
				
				if(strtolower($filterbox_value->filterbox_name)=="color"){
					
					?>
					
			
				<div class="col-md-5 <?php if($k%2==0){?>col-md-offset-1<?php } ?>">
				<div class="label-floating form-group">
				<?php /* <input type="hidden" class="dynamic_input" name="filter[<?php echo $filterbox_value->filterbox_id; ?>]" value=""> */ ?>
				
					<label class="control-label">-Select <?php echo $filterbox_value->filterbox_name; ?>-</label>
					<select id="<?php echo trim(strtolower($filterbox_value->filterbox_name)); ?>" filterbox_name="<?php echo trim(strtolower($filterbox_value->filterbox_name)); ?>" onchange="filter_display_color(this.options[this.selectedIndex].getAttribute('filtervalues'))" name="filter[<?php echo $filterbox_value->filterbox_id; ?>]" class="form-control filterbox">
					<option value=""></option>
					<?php
					foreach($filter as $filter_value){
						$color_arr=explode(':',$filter_value->filter_options);
						if($filter_value->filter_id!=""){
							echo '<option filtervalues="'.$filter_value->filter_options.'" value="'.$filter_value->filter_id.'">'.$color_arr[0].'</option>';
						}
						}						
					?>
					<option value="0">-None Selected-</option>
					</select>
				</div>	
				</div>
			
					<?php
				
				}else{
					  
					  ?>
					  <?php
						//if(strtolower($filterbox_value->filterbox_name)=="availability" || strtolower($filterbox_value->filterbox_name)=="price"){
					  ?>
			<?php /* <input type="hidden" class="dynamic_input" name="filter[<?php echo $filterbox_value->filterbox_id; ?>]" value=""> */ ?>
			<?php
						//}
						?>
						
						
				<div class="col-md-5 <?php if($k%2==0){?>col-md-offset-1<?php } ?>">
					<div class="label-floating form-group">
						
						<label class="control-label">-Select <?php echo $filterbox_value->filterbox_name; ?>-</label> 
						<select id="<?php echo trim(strtolower($filterbox_value->filterbox_name)); ?>" filterbox_name="<?php echo trim(strtolower($filterbox_value->filterbox_name)); ?>" name="filter[<?php echo $filterbox_value->filterbox_id; ?>]" class="form-control filterbox">
							<option value=""></option>
							<?php
							foreach($filter as $filter_value){
								if(strtolower($filterbox_value->filterbox_name)=="brand"){
									if(strtolower($filter_value->filter_options)==strtolower($brand_name)){
										echo '<option value="'.$filter_value->filter_id.'" selected="true">'.$filter_value->filter_options.'</option>';
									}
									else if(strtolower($filter_value->filterbox_name)==strtolower($attributes_value->attribute1_name)){
										
										
									}else{
										echo '<option value="'.$filter_value->filter_id.'">'.$filter_value->filter_options.'</option>';
									}
									
								}else{
									echo '<option value="'.$filter_value->filter_id.'">'.$filter_value->filter_options.'</option>';
								}
							
							}
							?>
							<option value="0">-None Selected-</option>
						</select>
						
					</div>
				</div>
				
			
					  <?php
					  }
					  
					  ?>
					 
					  <?php
				  }
			?>
          </div>      
		                                
	</div>
	
	<script>
	
	var typingTimer;                //timer identifier
	var doneTypingInterval = 5000;  //time in ms, 5 second for example
	var $input = $('#tax');

	//on keyup, start the countdown
	$input.on('keyup', function () {
	  clearTimeout(typingTimer);
	  val=$('#selling_price').val();
	  typingTimer = setTimeout(doneTyping(val), doneTypingInterval);
	});

	//on keydown, clear the countdown 
	$input.on('keydown', function () {
	  clearTimeout(typingTimer);
	});

	//user is "finished typing," do something
	function doneTyping (val) {
	  if(val!=''){
		  assign_filter_values(val,'price')
	  }
	}
	function assign_filter_values_stock(){
		elem="#availability";
		stock=parseInt($("#stock").val().trim());
		cutoff_stock=parseInt($("#cutoff_stock").val().trim());
		if(stock!="" && cutoff_stock!=""){
			if(stock>cutoff_stock){
				stock_availability="yes";
			}
			else{
				stock_availability="no";
			}
			//alert(stock_availability);
			if(stock_availability=="yes"){
				$("#availability > option").each(function(){
					if($(this).val()=="0"){
						//$(this).attr({"selected":true});
						$("#availability").val($(this).val());
						$("#availability").attr({"disabled":true});
						$("#availability").parent('div').removeAttr("class");
						$("#availability").parent('div').addClass("label-floating form-group");
						$("#filter_select").append("<input type='hidden' name='"+$(elem).attr("name")+"' class='dynamic_input' value='0'>");
						//$("input[name='"+$(elem).attr("name")+"'].dynamic_input").val(0);
						
						
					}
				});
			}
			if(stock_availability=="no"){
				$("#availability > option").each(function(){
					if($(this).text()=="include out of stock"){
						//$(this).attr({"selected":true});
						$("#availability").val($(this).val());
						$("#availability").attr({"disabled":true});
						$("#availability").parent('div').removeAttr("class");
						$("#availability").parent('div').addClass("label-floating form-group");
						$("#filter_select").append("<input type='hidden' name='"+$(elem).attr("name")+"' class='dynamic_input' value='"+$(this).val()+"'>");
						//$("input[name='"+$(elem).attr("name")+"'].dynamic_input").val($(this).val());
						
								
					}
				});
			}
			
		}
		
		
	}
	function assign_filter_values(data,name,color=''){

		$(".filterbox").each(function(index, elem) {
		   filterbox_name = $(elem).attr("filterbox_name");
		   id=$(elem).attr('id');
		  
		  //id is elem
		  // alert(filterbox_name);
		   if(name==filterbox_name){
			  
				$("#"+id+" > option").each(function() {
					
					fil_text_org=this.value;
					//alert(fil_text_org);
					fil_text=this.text.toLowerCase();
					//alert(fil_text);
					data=data.toLowerCase();
					
					if(color=="color"){
						colors_arr=data.split(":");
						code=colors_arr[1];
						name=colors_arr[0];
						data=name;
					}
					
					//alert(fil_text + ' ' + data);
					
					if(name=="price"){
						//alert(fil_text);
						if(fil_text!=''){
							fil_text_arr=fil_text.split("-");
							lprice=parseInt(fil_text_arr[0].trim());
							hprice=parseInt(fil_text_arr[1].trim());
	
							if(data>=lprice && data<=hprice){	
								//$(elem).find("option[value='"+fil_text_org+"']").attr('selected',"true");
								$(elem).val(fil_text_org);
								$(elem).attr({"disabled":true});
								$(elem).parent('div').removeAttr("class");
								$(elem).parent('div').addClass("label-floating form-group");
								
								$("#filter_select").append("<input type='hidden' name='"+$(elem).attr("name")+"' class='dynamic_input' value='"+fil_text_org+"'>");
								//$("input[name='"+$(elem).attr("name")+"'].dynamic_input").val(fil_text_org);
								
								
							
								//$(elem).parent('div').addClass('is-focused');
								
							}
						}
					}
					if(name!="price"){
						if(fil_text==data && fil_text_org!=''){
							//alert(fil_text);
							//$(elem).find("option[value='"+fil_text_org+"']").attr('selected',"true");
							$(elem).val(fil_text_org);
							$(elem).attr({"disabled":true});
							$(elem).parent('div').removeAttr("class");
							$(elem).parent('div').addClass("label-floating form-group");
							
							$("#filter_select").append("<input type='hidden' name='"+$(elem).attr("name")+"' class='dynamic_input' value='"+fil_text_org+"'>");
							//$("input[name='"+$(elem).attr("name")+"'].dynamic_input").val(fil_text_org);
								
							

							
							//$(elem).parent('div').addClass('is-focused');
							
						}
						
					}
					
				});
		   }
		});

	}
	</script>
	
	
	<div class="tab-pane" id="step-61">
		
		<!---common--image--->
		<div class="row">
			<div class="col-md-5 col-md-offset-3" id="inv_images_div">
			<div class="col-md-12 text-center"><b>Common (300x366)</b></div>
				<div class="col-md-12">
				<div class="picture-container">
					<div class="picture" id="commonPic">
						<img class="picture-src" id="wizardPictureCommon" title=""/>
				
						<input type="file" name="common_image" id="wizard-picture-Common"/>
					</div>
				<h6 id="wizardPictureCommon_picname">Common Image</h6>
				</div>
				</div>	
			</div>
		</div>
		<!---common--image--->
		
		<div class="row">
			<div class="col-md-2 col-md-offset-1">
			<button name="add_image_inventory" id="add_image_inventory_btn" type="button" class="btn btn-success" disabled onclick="add_image_inventory_fun()">Add Pair</button>
			</div>
	
		</div>
		<div class="row">
		
		
			<!---- first pair starts --->
			<div class="col-md-5 col-md-offset-2" id="inv_images_div_1">
			<div class="col-md-10 col-md-offset-1"><b>FIRST PAIR</b></div>
			
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic1">
						<img class="picture-src" id="wizardPictureSmall1" title=""/>
				
						<input type="file" name="thumbnail1" id="wizard-picture-small1"onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname1">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic1">
						<img class="picture-src" id="wizardPictureBig1" title=""/>
				
						<input type="file" name="image1" id="wizard-picture-big1" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname1">Medium Image (420x512)</h6>
				</div>
				</div>
				
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic1">
						<img class="picture-src" id="wizardPictureLarge1" title=""/>
				
						<input type="file" name="largeimage1" id="wizard-picture-large1" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname1">Big Image (850x1036)</h6>
				</div>
				</div>
				
				
			</div>
			<!---- first pair ends --->
			<!---- second pair starts --->
			<div class="col-md-5"  id="inv_images_div_2">
			<div class="col-md-10 col-md-offset-1"><b>SECOND PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(2)"></i></b></div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic2">
						<img class="picture-src" id="wizardPictureSmall2" title=""/>
				
						<input type="file" name="thumbnail2" id="wizard-picture-small2" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname2">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic2">
						<img class="picture-src" id="wizardPictureBig2" title=""/>
				
						<input type="file" name="image2" id="wizard-picture-big2" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname2">Medium Image (420x512)</h6>
				</div>
				</div>
				
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic2">
						<img class="picture-src" id="wizardPictureLarge2" title=""/>
				
						<input type="file" name="largeimage2" id="wizard-picture-large2" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname2">Big Image (850x1036)</h6>
				</div>
				</div>
				
				
			</div>
			<!---- second pair ends --->
		</div>
		<div class="row">
			<!---- third pair starts --->
			<div class="col-md-5 col-md-offset-2" id="inv_images_div_3">
			<div class="col-md-10 col-md-offset-1"><b>THIRD PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(3)"></i></b></div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic3">
						<img class="picture-src" id="wizardPictureSmall3" title=""/>
				
						<input type="file" name="thumbnail3" id="wizard-picture-small3" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname3">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic3">
						<img class="picture-src" id="wizardPictureBig3" title=""/>
				
						<input type="file" name="image3" id="wizard-picture-big3" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname3">Medium Image (420x512)</h6>
				</div>
				</div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic3">
						<img class="picture-src" id="wizardPictureLarge3" title=""/>
				
						<input type="file" name="largeimage3" id="wizard-picture-large3" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname3">Big Image (850x1036)</h6>
				</div>
				</div>
			</div>
			<!---- third pair ends --->
			<!---- fourth pair starts --->
			<div class="col-md-5" id="inv_images_div_4">
			<div class="col-md-10 col-md-offset-1"><b>FOURTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;"  onclick="hide_inv_imagesFun(4)"></i></b></div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic4">
						<img class="picture-src" id="wizardPictureSmall4" title=""/>
				
						<input type="file" name="thumbnail4" id="wizard-picture-small4" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname4">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic4">
						<img class="picture-src" id="wizardPictureBig4" title=""/>
				
						<input type="file" name="image4" id="wizard-picture-big4" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname4">Medium Image (420x512)</h6>
				</div>
				</div>
				
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic4">
						<img class="picture-src" id="wizardPictureLarge4" title=""/>
				
						<input type="file" name="largeimage4" id="wizard-picture-large4" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname4">Big Image (850x1036)</h6>
				</div>
				</div>
				
			</div>
			<!---- fourth pair ends --->
			<div class="row">
			<!---- fifth pair starts --->
			<div class="col-md-5 col-md-offset-2" id="inv_images_div_5">
			<div class="col-md-10 col-md-offset-1"><b>FIFTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(5)"></i></b></div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic5">
						<img class="picture-src" id="wizardPictureSmall5" title=""/>
				
						<input type="file" name="thumbnail5" id="wizard-picture-small5" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname5">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic5">
						<img class="picture-src" id="wizardPictureBig5" title=""/>
				
						<input type="file" name="image5" id="wizard-picture-big5" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname5">Medium Image (420x512)</h6>
				</div>
				</div>
				
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic5">
						<img class="picture-src" id="wizardPictureLarge5" title=""/>
				
						<input type="file" name="largeimage5" id="wizard-picture-large5" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname5">Big Image (850x1036)</h6>
				</div>
				</div>
				
			</div>
			<!---- fifth pair ends --->
			
			
			
			<!---- sixth pair starts --->
			<div class="col-md-5" id="inv_images_div_6">
			<div class="col-md-10 col-md-offset-1"><b>SIXTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(6)"></i></b></div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic6">
						<img class="picture-src" id="wizardPictureSmall6" title=""/>
				
						<input type="file" name="thumbnail6" id="wizard-picture-small6" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname6">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic6">
						<img class="picture-src" id="wizardPictureBig6" title=""/>
				
						<input type="file" name="image6" id="wizard-picture-big6" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname6">Medium Image (420x512)</h6>
				</div>
				</div>
				
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic6">
						<img class="picture-src" id="wizardPictureLarge6" title=""/>
				
						<input type="file" name="largeimage6" id="wizard-picture-large6" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname6">Big Image (850x1036)</h6>
				</div>
				</div>
				
			</div>
			<!---- sixth pair ends --->
			
			
			
			</div>
		</div>
	</div>	 
	
	<script>
		function hide_inv_imagesFun(div_index_id){
			
			document.getElementById("wizard-picture-small"+div_index_id).value="";
			document.getElementById("wizard-picture-big"+div_index_id).value="";
			document.getElementById("wizard-picture-large"+div_index_id).value="";
			
			document.getElementById("wizardPictureSmall"+div_index_id).removeAttribute("src");
			document.getElementById("wizardPictureBig"+div_index_id).removeAttribute("src");
			document.getElementById("wizardPictureLarge"+div_index_id).removeAttribute("src");
			
			document.getElementById("wizardPictureSmall_picname"+div_index_id).innerHTML="SMALL IMAGE (100x122)";
			document.getElementById("wizardPictureBig_picname"+div_index_id).innerHTML="Medium IMAGE";
			document.getElementById("wizardPictureLarge_picname"+div_index_id).innerHTML="Big IMAGE";
			
			document.getElementById("inv_images_div_"+div_index_id).style.display="none";
			enableDisabledAddInventoryButtonFun();
		}
		function enableDisabledAddInventoryButtonFun(){
			small_image_count=0;
			big_image_count=0;
			large_image_count=0;
			for(i=1;i<=6;i++){
				if(document.getElementById("inv_images_div_"+i).style.display==""){
					if(document.getElementById("wizard-picture-small"+i).value!=""){
						small_image_count++;
					}
					if(document.getElementById("wizard-picture-big"+i).value!=""){
						big_image_count++;
					}
					if(document.getElementById("wizard-picture-large"+i).value!=""){
						large_image_count++;
					}
				}
			}
			if(small_image_count==0 && big_image_count==0 && large_image_count==0){
				document.getElementById("add_image_inventory_btn").disabled=true;
			}
			else{
				if((small_image_count==big_image_count) && (big_image_count==large_image_count)){
					document.getElementById("add_image_inventory_btn").disabled=false;
					document.getElementById("add_inventory_finish_btn").disabled=false;
				}
				else{
					document.getElementById("add_image_inventory_btn").disabled=true;
					document.getElementById("add_inventory_finish_btn").disabled=true;
					
				}
			}
		}
		function initializeInventoryImageDivFun(){
			for(i=2;i<=6;i++){
				document.getElementById("inv_images_div_"+i).style.display="none";
			}
		}
		function add_image_inventory_fun(){
			for(i=2;i<=6;i++){
				if(document.getElementById("inv_images_div_"+i).style.display=="none"){
					document.getElementById("inv_images_div_"+i).style.display="";
					document.getElementById("add_image_inventory_btn").disabled=true;
					break;
				}
			}
		}
		initializeInventoryImageDivFun();
		enableDisabledAddInventoryButtonFun();
	</script>
	
	</div>		                        
	<div class="wizard-footer">
		<div class="pull-right">
			<input type='button' class='btn btn-next btn-fill btn-success btn-wd' name='next' value='Next' onclick="enterCurrentTabFun()"/>
			<input type='button' class='btn btn-finish btn-fill btn-success btn-wd' name='finish' id="add_inventory_finish_btn" value='Finish' onClick="filter_details_form_valid()" />
		</div>

		<div class="pull-left">
			<input type='button' class='btn btn-previous btn-fill btn-default btn-wd' id="previous_button" name='previous' value='Previous' />
			<input type='button' class='btn btn-previouspage btn-fill btn-default btn-wd' id="back" value='Previous' />
		</div>
		<div class="clearfix"></div>
	</div>
	</form>
		
		
	</div>
	</div> <!-- wizard container -->
</div>
</div>
<div class="footer">

</div>
</div>

<form name="back_to_inventory" id="back_to_inventory" method="post">
		<input type="hidden" value="<?php echo $pcat_id; ?>" name="parent_category">
		<input type="hidden" value="<?php echo $cat_id; ?>" name="categories">
		<input type="hidden" value="<?php echo $subcat_id; ?>" name="subcategories">
		<input type="hidden" value="<?php echo $brand_id; ?>" name="brands">
		<input type="hidden" value="<?php echo $product_id; ?>" name="products">
		
</form>
</div>

<div class="modal" id="status_modal">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Update Inventory Status</h4>
           </div>
           <div  class="modal-body" id="model_content">
				<form method="post" id="request_form" name="request_form">
		  			<input type="hidden" name="app_id" id="app_id" value="">
		  			<input type="hidden" name="inventory_id" id="inventory_id" value="">
		  			

					<div class="row">

					<div class="col-md-11 col-md-offset-1"> 
							<div class="form-group">
								<label class="control-label">Status
								</label>
								<select name="status" id="status" type="text" class="form-control" required onchange="show_rejected_comments()"/>
								<option value=""> - Select status - </option>
								<option value="1">Publish</option>
								<option value="2">Unpublish</option>
								<option value="3">Reject</option>
								</select>
							</div>
						</div>
  
						<div class="col-md-11 col-md-offset-1" id="show_rejected_comments_div" style="display:none;" > 	
							<div class="form-group">
							<label class="control-label">Rejections Comments if any 
							</label>
							<textarea name="comments" id="comments" class="form-control"></textarea>
							</div>
						</div>

						<div class="col-md-11 col-md-offset-1"> 	
							<div class="form-group text-center">
							
							<button class="btn btn-info btn-sm" type="button" onclick="change_status()" id="submit-data"></i>Submit</button>
							</div>
						</div>
					</div>



				</form>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
</div>

<script  type="text/javascript" src="<?php echo base_url();?>assets/richte/editor.js" ></script>
<script>
$(document).ready(function() {
	var textarea = $("#highlight");
	textarea.Editor();
	
	var textarea1 = $("#highlight_faq");
	textarea1.Editor();
	
	var textarea2 = $("#highlight_whatsinpack");
	textarea2.Editor();
	
	var textarea3 = $("#highlight_aboutthebrand");
	textarea3.Editor();
 });
 
 /*function toggle_highlight(){
	 $('#highlight_div').toggle();
 }*/
 function enterCurrentTabFun(){
	$(".tab-content .tab-pane.active").each(function(){
		if($(this).attr("id")=="step-21"){
			vendor_id=$("#vendors").val();
			if($("#logistics_parcel_category").val()!=null){
				logistics_parcel_category_in=$("#logistics_parcel_category").val().join("-");
				
				$.ajax({
					url:"<?php echo base_url()?>admin/Catalogue/check_selection_in_all_parcel_categories",
					type:"post",
					data:"vendor_id="+vendor_id+"&logistics_parcel_category_in="+logistics_parcel_category_in,
					success:function(data){
						if(data=="no"){
							swal({
								title:"Info", 
								text:"Please select atleast one parcel category from logisitcs", 
								type: "info"
						}).then(function () {
							
						});
						$("#previous_button").click();
						}
						else{
							
						}
					}
				});
			}
	
		}
			if($(this).attr("id")=="step-11"){
			add_to_cart=$('#add_to_cart').is(':checked');
			request_for_quotation=$('#request_for_quotation').is(':checked');
			if(add_to_cart===false && request_for_quotation===false){
				$("#buttonDisplayErrorMessage").html("Please select any one of an option");
				swal({
						title:"Error", 
						text:"Please select Either 'Add to Cart' or 'Request for Quotation' option", 
						type: "error",
						allowOutsideClick: false
					}).then(function () {
						$("#previous_button").click();
					});
			}else{
				$("#buttonDisplayErrorMessage").html("");
			}
			}
	});
 }
 
 function calculateSelling_priceDiscountFun(){
 
        selling_price=$("#selling_price").val();
        max_selling_price=$("#max_selling_price").val();
        
        if(selling_price=="" || max_selling_price==""){
            $("#selling_discount").val("");
	}
        if(selling_price!="" && max_selling_price!=""){
            if(parseFloat(max_selling_price)>=parseFloat(selling_price)){
                wsp_dis=Math.round(((parseFloat(max_selling_price)-parseFloat(selling_price))/parseFloat(max_selling_price))*100).toFixed(2);
                $("#selling_discount").val(wsp_dis);
                $("#selling_discount_div").removeAttr("class");
		$("#selling_discount_div").attr("class","form-group label-floating is-focused");
                
            }else{
                swal({
                        title:"Error", 
                        text:"MRP should be greater or equal to WSP", 
                        type: "error",
                        allowOutsideClick: false
                }).then(function () {
                        $("#selling_discount").val(""); 
                });
               
            }
            //alert(wsp_dis);
	}
 }
  function calculateTax_priceFun(){
	 
        tax=$("#tax").val();
        msp=$("#max_selling_price").val();
        selling_price=$("#selling_price").val();
        
        if(selling_price!="" && tax!=""){
            
            //taxable_price=(parseFloat(msp)-parseFloat(tax_percent_price));
            taxable_price=(parseFloat(selling_price)/(1+parseFloat(tax)/100)).toFixed(2);
            tax_percent_price=(parseFloat(selling_price)-parseFloat(taxable_price)).toFixed(2);
            //alert(taxable_price);
            $("#tax_percent_price").val(tax_percent_price);
            $("#taxable_price").val(taxable_price);
            
            $("#tax_percent_price_div").removeAttr("class");
            $("#tax_percent_price_div").attr("class","form-group label-floating is-empty is-focused");
            $("#taxable_price_div").removeAttr("class");
            $("#taxable_price_div").attr("class","form-group label-floating is-empty is-focused");
        }
         /* calculate tax price and taxable vale */
 }

 /* Export inventory */

function export_inventory(table){

    $.ajax({
        url:"<?php echo base_url()?>admin/Catalogue/export_inventory",
        type:"post",
        data:"table="+table+"&active=0",
        success:function(data){
            if(data==true){

            } else{
                swal("Error", "Error in exporting inventories", "error");
            }
        }
    });
}
 /* Export inventory */
 /* Import inventory */
function formToggle(ID){
    var element = document.getElementById(ID);
    if(element.style.display === "none"){
        element.style.display = "block";
    }else{
        element.style.display = "none";
    }
}


 </script>
 
 
 <script>
	function calculateTaxPercentFun(){
		var CGST_value=$("#CGST").val().trim();
		var IGST_value=$("#IGST").val().trim();
		var SGST_value=$("#SGST").val().trim();
		
		if(CGST_value==""){CGST_value=0;}
		if(IGST_value==""){IGST_value=0;}
		if(SGST_value==""){SGST_value=0;}
		
		$("#tax").val(parseFloat(CGST_value)+parseFloat(IGST_value)+parseFloat(SGST_value));
		calculateTax_priceFun();
	}

	function change_status(app_id,inventory_id,status){
    
    if(app_id!='' && status!=''){
        
        swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
        $.ajax({
            url:"<?php echo base_url()?>admin/Catalogue/inventory_change_status",
            type:"POST",
            data:"vendor_id="+app_id+"&inventory_id="+inventory_id+"&status="+status,
            dataType: "JSON",                
            asyc:false,
            success:function(data){
                if (parseInt(data.flag) == 1) {
                    swal(
                        data.message,
                        '',
                        'info'
                    ) 
                    table.draw();
                } 
            }
        });
        
         }
}]);
    }
    
}

$(document).ready(function (){
	$('#price_validity').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		weekStart: 0, 
		format: 'YYYY-MM-DD', 
		shortTime : true
	}).on('change', function(e, date){
		$('#price_validity').bootstrapMaterialDatePicker('setMinDate', date);
	});
});

function view_status_modal(app_id,inventory_id,status){
	$("#app_id").val(app_id);
	$("#inventory_id").val(inventory_id);
	$("#status").val(status);
	$("#status_modal").modal("show");
}

function change_status(){

	app_id=$("#app_id").val();
	inventory_id=$("#inventory_id").val();
	status=$("#status").val();
	comments=$("#comments").val();

    if(app_id!='' && status!=''){
        
        swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
        $.ajax({
            url:"<?php echo base_url()?>admin/Catalogue/inventory_change_status",
            type:"POST",
            data:"vendor_id="+app_id+"&inventory_id="+inventory_id+"&status="+status+"&comments="+comments,
            dataType: "JSON",                
            asyc:false,
            success:function(data){
                if (parseInt(data.flag) == 1) {
                    swal(
                        data.message,
                        '',
                        'info'
                    ) 
                    drawtable();
                } 
            }
        });
        
         }
}]);
    }
    
}

function show_rejected_comments(){
	status=$("#status").val();
	if(status=='3'){
		$('#show_rejected_comments_div').show();
	}else{
		$('#show_rejected_comments_div').hide();
	}
}

function add_items(){
	var len = $('.item').length; 

let template = `<div class="item">
<div class="row">
<div class=' col-md-8 col-md-offset-1'>
	<label class="control-label">Content</label>
	<input type="text" class="form-control" name="position_text[`+(parseInt(len)+1)+`]" placeholder="" />
	</div>
</div>
<div class="row">
	<div class="col-md-8 col-md-offset-1"> 
		<div class="form-group">
		<div class="col-md-5 col-md-offset-1">Do you have ?</div>
		<div class="col-md-3">
			<label class="radio-inline"><input name="position_uhave[`+(parseInt(len)+1)+`]" value="yes" type="radio" >Yes</label>
		</div>
		<div class="col-md-3">
			<label class="radio-inline"><input name="position_uhave[`+(parseInt(len)+1)+`]" value="no" type="radio">No</label>
		</div>

	</div>
</div>

<div class="row">
	<div class="col-md-8 col-md-offset-1"> 
		<div class="form-group">
		<div class="col-md-5 col-md-offset-1">Do others have ?</div>
		<div class="col-md-3">
			<label class="radio-inline"><input name="position_others_have[`+(parseInt(len)+1)+`]" value="yes" type="radio" >Yes</label>
		</div>
		<div class="col-md-3">
			<label class="radio-inline"><input name="position_others_have[`+(parseInt(len)+1)+`]" value="no" type="radio">No</label>
		</div>

	</div>
</div>
<div class="col-md-8 col-md-offset-1"><button class="btn btn-sm btn-danger pull-right" class="remove" >Remove</a>
</div>
</div>`; 

$("#items").append(template);

}
$(document).ready(()=>{	
    $("body").on("click", ".remove", (e)=>{
        $(e.target).parents(".item").remove();
    })

	// $('#sku_name1').on('input', function (e) {
	// 	const enteredVal = $(this).val();
	// 	console.log(enteredVal)
	// 	let result = $('.result');
	// 	let regex = /^[\s!@#$%^&*()_+{}\[\]:;<>,.?~\\/-]+$/g;
	// 	//alert(regex.test(enteredVal))
	// 	if (regex.test(enteredVal)) {
	// 		result.html(
	// 			`<b style="color: green">
	// 					The entered value is correct!!
	// 				</b>`)
	// 	}
	// 	else {
	// 		result.html(
	// 			`<b style="color: red">
	// 					The entered value is incorrect!!
	// 				</b>`)
	// 	}
	// });

	//$("#sku_name").rules("add", { pattern: "^[\s!@#$%^&*()_+{}\[\]:;<>,.?~\\/-]+$" })
	//$("#sku_name").rules("add", { regex: /^[\s!@#$%^&*()_+{}\[\]:;<>,.?~\\/-]+$/g })
	

}); 
function blockSpecialChar(e){
        var k;
        document.all ? k = e.keyCode : k = e.which;
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
        }    
</script>


</body>
</html>
