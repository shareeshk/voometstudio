<html>
<head>
<meta charset="utf-8">
<title>Reviews and Ratings</title>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<style>
.wizard-card{
	box-shadow:none;
}
.form-group{
	margin-top:15px;
}

</style>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="row">
<div class="col-md-12">
<div class="wizard-card" data-color="green" id="wizardProfile">	
<form name="search_for_category" id="search_for_category" method="post" action="#" onsubmit="return form_validation();">
<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				pcat_id: {
					required: true,
					
				},
				create_editview: {
					required: true,
		      
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
			</script>

	<div class="col-md-6 col-md-offset-3">
		
		
		<div class="row">
			<div class="col-md-12 col-md-offset-1">		
			<h4>Select Parent Category to access Category</h4>
		</div>
		</div>
			<div class="row">
				<div class="col-md-10">	
					<div class="form-group label-floating">
					<label class="control-label">-Select an View-</label>
					<select name="create_editview" id="create_editview" class="form-control" onchange="viewFun(this)">
						<option value=""></option>
						<option value="create">-Go to Create-</option>
						<option value="view">-Go to View-</option>
					</select>
					</div>
				</div>
			</div>
			<div class="row" id="pcat_show" style="display:none;">
				<div class="col-md-10">
					<div class="form-group label-floating">
					<label class="control-label">-Select Parent Category-</label>
					<script>
						function viewFun(obj){
							if(obj.value=="create"){
								document.getElementById("pcat_show").style.display = "none";
							}
							if(obj.value=="view"){
								document.getElementById("pcat_show").style.display = "block";
							}
						}
					</script>
					<select name="pcat_id" id="parent_category" class="form-control" >
						<option value=""></option>
						<?php foreach ($parent_category as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>" <?php echo ($parent_category_value->pcat_id==$pcat_id)? "selected":''; ?> ><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<option value="0">--None--</option>
						
					</select>
					</div>
				</div>
			</div>	
	
			
				
				<div class="row">	
					<div class="col-md-10 text-center">
						<button type="submit" class="btn btn-info" id="submit_button">Submit</button>
						<button class="btn btn-warning" type="reset" id="reset_form_button">Reset</button>
					</div>
				</div>
		
	
	</div>

</form>
</div>
</div>

</div>
</div>
</div>
</body>
</html>
<script type="text/javascript">
$(document).ready(function (){
	pcat_id=$('#pcat_id').val();
	cat_id=$('#cat_id').val();
	subcat_id=$('#subcat_id').val();
	brand_id=$('#brand_id').val();
	product_id=$('#product_id').val();
	//inventory_id=$('#inventory_id').val();

});

function form_validation()
{
	var create_editview=document.getElementById("create_editview").value;
	if(create_editview=="view"){
		var parent_category = $('select[name="pcat_id"]').val().trim();
	}
	    var err = 0;
		
		if((parent_category=='') || (create_editview=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		if(err==0){
		

			var form_status = $('<div class="form_status"></div>');		
			var form = document.getElementById('search_for_category');		
			
			form.action='<?php echo base_url()."admin/Catalogue/category"; ?>';
			form.submit();
		}
	
	return false;
}
</script>
