<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style type="text/css">

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.wizard-card .tab-content {
	 min-height: 400px;
	 padding:0px;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">

function showDivPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/payment_options';
}
</script>
<?php
if($get_payment_options_data['sort_order']=="0"){?>
	<script>
	$(document).ready(function () {
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	document.getElementById('show_available_common_payment_options').disabled = true;
	
	});
	</script>
	<?php
}
?>
<?php //print_r($parent_catagories);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row-fluid" id="editDiv3">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="edit_payment_option" id="edit_payment_option" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_payment_option_name: {
					required: true,
				},
				edit_menu_sort_order: {
					required: true,
				}
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Payment options</h3>
		</div>	
		</div> 
		
		<div class="tab-content">
		<input type="hidden" name="id" id="id" value="<?php echo $get_payment_options_data["id"];?>">	
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Enter Payment option name</label>
					<input id="edit_payment_option_name" name="edit_payment_option_name" type="text" class="form-control" value="<?php echo $get_payment_options_data["payment_option_name"];?>"/>
					<input id="edit_payment_option_name_default" name="edit_payment_option_name_default" type="hidden" class="form-control" value="<?php echo $get_payment_options_data["payment_option_name"];?>"/>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Enter the Sort Order</label>
					
					<select name="edit_menu_sort_order" id="edit_menu_sort_order" class="form-control" onchange="show_view_validatn()">
					<option value="<?php echo $get_payment_options_data["sort_order"];?>" selected><?php echo $get_payment_options_data["sort_order"];?></option>
						<?php
							if($get_sort_order_options_for_payment_options["key"]==""){
								if($get_payment_options_data["sort_order"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php } ?>
								<option value="1">1</option>
								<?php
							}
							else{
								if($get_payment_options_data["sort_order"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php
								}
								$value_arr=explode(",",$get_sort_order_options_for_payment_options["value"]);
								foreach($value_arr as $k => $v){
									?>
										<option value="<?php echo $v;?>"><?php echo $v;?></option>
									<?php
									}
							}
						?>
					</select>
					<input id="edit_menu_sort_order_default" name="edit_menu_sort_order_default" type="hidden" value="<?php echo $get_payment_options_data["sort_order"];?>" />	
				</div>
				</div>
			</div>	
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group label-floating">
			<label class="control-label">-Swap Options-</label>
			<select class="form-control" id="show_available_common_payment_options" onchange="changeEventHandler(event)">
			<option selected value=""></option>
			<?php
			
			foreach($payment_options_sort_order_arr as $payment_options_order_arr){
				
				?>
				
					<option value="<?php echo $payment_options_order_arr["id"];?>"><?php echo $payment_options_order_arr["payment_option_name"];?></option>
					<?php
			}
			?>
			
			</select>
			</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10"> 
					<div class="form-group">
					<div class="col-md-5 col-md-offset-1">Show Payment options</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="edit_view" id="edit_view1" value="1" type="radio" <?php if($get_payment_options_data["view"]=="1"){echo "checked";}?>>View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="edit_view" id="edit_view2" value="0" type="radio" <?php if($get_payment_options_data["view"]=="0"){echo "checked";}?>>Hide</label>
					</div>
				
					</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-8 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit">Submit</button>
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
					</div>
				</div>
			</div>

		</div>		
	</form>

</div>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>
</body>
</html>
<script>
function changeEventHandler(event){
        parent_id=event.target.value;
		if(parent_id!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		}
		if(parent_id==""){
	edit_menu_sort_order=document.getElementById("edit_menu_sort_order").value;
	if(edit_menu_sort_order==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;					
	}
	if(edit_menu_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
		}
}
function show_view_validatn(){
	edit_menu_sort_order=document.getElementById("edit_menu_sort_order").value;
	show_available_common_payment_options=document.getElementById("show_available_common_payment_options").value;
	if(show_available_common_payment_options==""){
	if(edit_menu_sort_order==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	document.getElementById('show_available_common_payment_options').disabled = false;
	}
	if(edit_menu_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		//document.getElementById('show_available_common_payment_options').disabled = false;
	}
	}
	if(show_available_common_payment_options!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
	
}
</script>
<script type="text/javascript">

$('#result').hide();

function form_validation_edit(){
	var edit_payment_option_name = $('input[name="edit_payment_option_name"]').val().trim();
	var menu_sort_order = $('select[name="edit_menu_sort_order"]').val().trim();
	var view = $('input[name="edit_view"]').val();
	var menu_sort_order_default = $('input[name="edit_menu_sort_order_default"]').val().trim();
		var common_payment_options="";
			if($("#show_available_common_payment_options").length!=0){
				var common_payment_options=document.getElementById("show_available_common_payment_options").value;
			}
	   var err = 0;
		if((edit_payment_option_name=='') || (menu_sort_order=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
var form_status = $('<div class="form_status"></div>');		
var form = $('#edit_payment_option');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();		
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_payment_options/"?>',
				type: 'POST',
				data: $('#edit_payment_option').serialize()+"&common_payment_options="+common_payment_options,
				dataType: 'html',
				
			}).done(function(data)
			  {
				 if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					edit_payment_option_name_default=document.getElementById("edit_payment_option_name_default").value;
					document.getElementById("edit_payment_option_name").value=edit_payment_option_name_default;
					document.getElementById("edit_payment_option_name").focus();
					
				});
				  } 
				if(data==1)
				{
					swal({
						title:"Success", 
						text:"Payment option is successfully updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				if(data==0)
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)	
				}
				
			});
	}
}]);		
			}
	
	return false;
}


</script>
