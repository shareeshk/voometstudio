<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Replacement Request Orders - Refund Success</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<style>
.form_class{
	margin-bottom:0;
	margin-top:0;
	padding-bottom:0;
}
</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript">
 var table;
$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
    table = $('#returns_replacement_request_orders_table_refund_success').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Returns/returns_replacement_request_orders_refund_success_processing",
			data: function (d) { d.status_of_refund = $('#status_of_refund').val(); },
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#returns_replacement_request_orders_table_refund_success_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("replacement_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'30%',
         'className': 'dt-body-left'
      },{
         'targets': 1,
         'width':'20%',
         'className': 'dt-body-left'
      },{
         'targets': 2,
         'width':'20%',
         'className': 'dt-body-left'
      },{
         'targets': 3,
         'width':'15%',
         'className': 'dt-body-left'
      },{
         'targets': 4,
         'width':'15%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});	

function draw_table(){
	table.draw();
}

function open_order_replacement_pickup_prev_order(obj,orders_status_id,return_order_id,order_item_id,inventory_id){
	
	$("#orders_status_id").val(orders_status_id);
	$("#return_order_id").val(return_order_id);
	$("#order_item_id").val(order_item_id);
	$("#replacement_with_inventory_id").val(inventory_id);
	$("#return_pickup_modal").modal("show");
}

$(document).ready(function(){
	var returns_refund_orders_list_checked=0;
	$("input[name='returns_refund_orders_list']").each(function(){
		if($(this).is(":checked")){
			returns_refund_orders_list_checked++;
		}
	})
	/*if(returns_refund_orders_list_checked==0){
		document.getElementById("refund_info_div").style.display="none";
	}*/
})


function open_return_replacement_balance_clear_fun(order_item_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/return_replacement_balance_clear",
		type:"POST",
		data:"order_item_id="+order_item_id,
		success:function(data){
			
			if(data){
						
				swal({
					title:"success!", 
					text:"Refund Balance amount is initiated", 
					type: "success",
					allowOutsideClick: false
					
				}).then(function () {
					location.reload();

				});
			}else{
				swal("Error", "error", "error");
			}
			/*if(data){
				alert("Balance amound is proceeded to clear");
				location.reload();
			}
			else{
				alert("error");
			}*/
		}
	})
}


 
function invoice_returns_replacement_partialFun(order_item_id,order_id){
	
		swal({
			title: 'Are you sure?',
			text: "Send this invoice",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Send it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
		$.ajax({
			url:"<?php echo base_url()?>admin/Returns/generate_invoice_for_partial_replacement_pdf",
			type:"POST",
			data:"order_item_id="+order_item_id+"&order_id="+order_id+"&sendmail=yes",
			
		}).success( function(data){
			
					if(data){
						
						swal({
							title:"success!", 
							text:"Invoice has been generated successfully", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							window.open("<?php echo base_url()?>assets/pictures/invoices/Invoice_partialrep_"+order_id+""+order_item_id+".pdf","_target");	
							//location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
			
		});
		})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				  $.ajax({
			url:"<?php echo base_url()?>admin/Returns/generate_invoice_for_partial_replacement_pdf",
			type:"POST",
			data:"order_item_id="+order_item_id+"&order_id="+order_id+"&sendmail=no",
			/*beforeSend:function(){
					$('.invoice_sent_'+order_item_id).html('<i class="fa fa-spinner"></i> Processing');
				}*/
		}).success( function(data){
			
			$('.invoice_sent_'+order_item_id).html('Send Invoice');
			if(data){
						
						swal({
							title:"success!", 
							text:"Invoice has been generated successfully", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							window.open("<?php echo base_url()?>assets/pictures/invoices/Invoice_partialrep_"+order_id+""+order_item_id+".pdf","_target");
							//location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
			
		});
				
			  }
			});	
	
}

</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header"><h4 class="text-center">Replacement Request Success - Returned Partial <span class="badge" id="replacement_count"></span></h4></div>
	<table id="returns_replacement_request_orders_table_refund_success" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
			<th colspan="5">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<select class="form-control" name="status_of_refund" id="status_of_refund" onchange="draw_table()">
						<option value="success">Completed requests</option>
						<option value="pending">Pending requests</option>
						<option value="all">All requests</option>
					</select>
				</div>
			</div>
			</th>
			</tr>
			<tr>
				<th class="text-primary small bold">Return Summary</th>
				<th class="text-primary small bold">Return Reason</th>
				<th class="text-primary small bold">Details</th>
				<th class="text-primary small bold">Delivered To</th>
				<th class="text-primary small bold">Actions</th>
			</tr>
		</thead>
	</table>
</div>

<!------------------------------->

<div class="modal" id="return_pickup_modal" data-backdrop="static" role="dialog">
<div class="modal-dialog modal-sm">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		<div class="panel panel-info">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                  Item received form
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right" onclick="divFunction_order_rep_send_invoice()"></span></span>
               </div>
               <div aria-expanded="true" class="">
					<div class="panel-body">
		<form id="return_pickup_form" method="post" enctype="multipart/form-data" class="form-horizontal">
			<input type="hidden" name="orders_status_id" id="orders_status_id">
			<input type="hidden" name="return_order_id" id="return_order_id">
			
			<input type="hidden" name="order_item_id" id="order_item_id">
			<input type="hidden" name="replacement_with_inventory_id" id="replacement_with_inventory_id">
						<div class="form-group">
							<div class="col-md-12">
									<input name="update_pickup" checked type="checkbox">
									 Items are pickedup
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12">
									<input name="update_stock" type="radio" value="item_has_reached_warehouse">
									 Item has reached warehouse
									 
							</div>
							<div class="col-md-12">
							<input name="update_stock" type="radio" value="item_has_not_reached_warehouse">
									 Item has not reached warehouse
							</div>
						</div>
						<div class="form-group">
						<div class="col-md-12">
							<input class="btn btn-success btn-xs btn-block" value="Submit" type="submit">
						</div>
					</div>
					</form>
					</div>
                 
               </div>
            </div>
		
         </div>
      </div>
   </div>
</div>
<!------------pickup modal------->
<script>
$(document).ready(function(){
	
	$("#return_pickup_form").on('submit',(function(e) {
		
		var update_stock=$("input[name=update_stock]").is(':checked');
		var update_pickup=$("input[name=update_pickup]").is(':checked');
		
		if(update_stock==false){
			alert('Pls fill all fields');
			return false;
		}
		
		if(update_pickup==false){
			alert('Items should be picked up.');
			return false;
		}
		e.preventDefault();
		
		$.ajax({
				url:"<?php echo base_url();?>admin/Returns/update_order_replacement_pickup_prev_order",
				type: "POST",    
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){	
					if(data){
						$('#return_pickup_modal').modal('hide');
						swal({
							title:"success!", 
							text:"Successfully Updated", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#return_pickup_form").trigger("reset");
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
				
				}	        
		   });
		
		
	}));
	
	/*$("#return_pickup_modal").on('hidden.bs.modal', function () {
		$("#return_pickup_form").trigger("reset");	
		location.reload();
    });*/
	
	
});
function divFunction_order_rep_send_invoice(){
    $("#return_pickup_form").trigger("reset");	
		location.reload();
}
</script>
<!------------pickup modal------->

<!---------------------->
<div class="modal" id="order_summary">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Order Summary</h4>
           </div>
           <div  class="modal-body" id="model_content">
		   <div class="container">
				
			</div>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close"class="btn">Close</a>
        </div>
     </div>
    </div>
  </div>
  
 
  
  <script type="text/javascript">
  $("#order_summary").modal("hide");
  function view_order_summary_(order_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/get_data_from_invoice_offers",
		type:"POST",
		data:"order_id="+order_id,
		success:function(data){
			
			$("#description").val("");	
			$("#model_content").html(data);
			$("#order_summary").modal("show");
			
		}
	})
	
}
  


function go_to_rep_details(order_item_id){	
	 $('#rep_details_'+order_item_id).attr('action', '<?php echo base_url();?>admin/Returns/rep_details_of_order_item').submit();
}
  </script>
  
  </div>
</body> 
</html>  
