<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Replacement Request Orders - Return Stock Notification</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<style>
.form-group{
	margin-bottom:5px;
	margin-top:0;
	padding-bottom:0;
}
</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript">
var table;
$(document).ready(function (){
   // Array holding selected row IDs
	var rows_selected = [];
		table = $('#table_return_stock_notification').DataTable({
	   
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Returns/return_stock_notification_processing", // json datasource
			data: function (d) { d.status_of_stock = $('#status_of_stock').val(); },
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#table_return_stock_notification_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("replacement_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'40%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});	
$(document).ready(function(){
	var returns_refund_orders_list_checked=0;
	$("input[name='returns_refund_orders_list']").each(function(){
		if($(this).is(":checked")){
			returns_refund_orders_list_checked++;
		}
	})
	/*if(returns_refund_orders_list_checked==0){
		document.getElementById("refund_info_div").style.display="none";
	}*/
});


function edit_timelimit_fun(return_stock_notification_id,quantity_replacement){
	$('#timelimit_form_'+return_stock_notification_id).toggle();
}
function update_quantity_replacement_stock_notification(return_stock_notification_id){
	
	timelimitdefined=$('#timelimitdefined_'+return_stock_notification_id).val();
	edit_timelimit_comments=$('#edit_timelimit_comments_'+return_stock_notification_id).val();
	
	$.ajax({
			url:"<?php echo base_url()?>admin/Returns/update_quantity_replacement_stock_notification",
			type:"POST",
			data:"return_stock_notification_id="+return_stock_notification_id+"&timelimitdefined="+timelimitdefined+"&edit_timelimit_comments="+edit_timelimit_comments,
			success:function(data){
				alert(data);
				if(data==true){
					$('#edit_timelimit_form_'+return_stock_notification_id)[0].reset();
					$('#timelimit_form_'+return_stock_notification_id).hide();
					alert("successfully updated");
					location.reload();	
					
				}
				
			}
		})
}
function stockAvgailableFun(orders_status_id,return_stock_notification_id,quantity_replacement,replacement_with_inventory_id,reserved_stock){


if(reserved_stock==1){
	var first_time=1;
}else{
	var first_time=0;
}


	//if(confirm("Are u sure to send notification to customer?")){
		swal({
			title: 'Are you sure?',
			/*text: "send notification to customer?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Send it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {*/
			
			input: 'checkbox',
			inputValue: 1,
			inputPlaceholder:
				'&nbsp; update inventory stock',
			text: "send notification to customer?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Send it!',
			showLoaderOnConfirm: true,
			inputValidator: function(result){
			
			return new Promise(function (resolve, reject) {
					
			$.ajax({
				url:"<?php echo base_url()?>admin/Returns/update_return_stock_notification",
				type:"POST",
				data:"orders_status_id="+orders_status_id+"&return_stock_notification_id="+return_stock_notification_id+"&quantity_replacement="+quantity_replacement+"&replacement_with_inventory_id="+replacement_with_inventory_id+"&first_time="+first_time+"&update_stock="+result,
				success:function(data){
					
					if(data=true){
						swal({
							title:"Success!", 
							text:"successfully updated", 
							type: "success",
							allowOutsideClick: false
						}).then(function () {
							location.reload();

						});
					}else{
						swal("Error", "Error", "error");
					}
					
				}
			});
			})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"Not sent :)", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});	
		
	//}
}

function stockNotAvailableFun(orders_status_id,return_stock_notification_id){

return false;

	quantity_replacement='NULL';
	if(confirm("Are u sure to send notification to customer?")){
		$.ajax({
		url:"<?php echo base_url()?>admin/Returns/update_return_stock_notification",
		type:"POST",
		data:"orders_status_id="+orders_status_id+"&return_stock_notification_id="+return_stock_notification_id+"&quantity_replacement="+quantity_replacement,
		success:function(data){
			if(data=true){
				alert('successfully updated');
			}
		}
		});
	}else{
		return false;
	}

}
function move_to_returns_desired(return_stock_notification_id){
	
	//if(confirm("Are u sure to move to refund?")){
		swal({
			title: 'Are you sure?',
			text: "Move this order",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Move it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
		$.ajax({
		url:"<?php echo base_url()?>admin/Returns/move_to_returns_desired",
		type:"POST",
		data:"return_stock_notification_id="+return_stock_notification_id,
		success:function(data){
					if(data){
						swal({
							title:"success!", 
							text:"successfully moved", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
								
							location.reload();

						});
					}else{
						if(data!=false){
							alert(data);
							location.reload();
						}else{
							swal("Error", "error", "error");
						}
						
					}
			
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				return false;
			  }
			});	
	/*}else{
		
		return false;
	}*/
	
}
function cancel_refund_request(return_stock_notification_id,quantity_replacement,replacement_with_inventory_id,reserved_stock){
	

	if(reserved_stock!=null && reserved_stock!=''){
		var first_time=1;
	}else{
		var first_time=0;
	}
	
	//if(confirm("Are u sure to cancel this request?")){
		swal({
			title: 'Are you sure?',
			input: 'checkbox',
			inputValue: 1,
			inputPlaceholder:
				'&nbsp; update inventory stock',
			text: "Cancel this request",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Cancel it!',
			showLoaderOnConfirm: true,
			inputValidator: function(result){
				
				return new Promise(function (resolve, reject) {
					
		$.ajax({
		url:"<?php echo base_url()?>admin/Returns/cancel_refund_request",
		type:"POST",
		data:"return_stock_notification_id="+return_stock_notification_id+"&quantity_replacement="+quantity_replacement+"&replacement_with_inventory_id="+replacement_with_inventory_id+"&first_time="+first_time+"&update_stock="+result,
		success:function(data){
			if(data){
				swal({
					title:"success!", 
					text:"successfully cancelled", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}else{
				swal("Error", "error", "error");
			}
			
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				return false;
			  }
			});
	
}

function open_return_refund_issue_fun(order_item_id){
	location.href="<?php echo base_url()?>admin/Returns/replacement_issue_refund/"+order_item_id;
}

function update_order_return_pickupFun(obj,orders_status_id,return_order_id,order_item_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/update_order_return_pickup",
		type:"POST",
		data:"orders_status_id="+orders_status_id+"&return_order_id="+return_order_id+"&order_item_id="+order_item_id,
		success:function(data){
			if(data){
				alert("Pick Up Updated");
				location.reload();
			}
			else{
				alert("error");
			}
			
		}
	})
}

function sendReplacementOrderFun(order_item_id){
	document.getElementById("type_of_order_div_"+order_item_id).style.display="block";
}


function draw_table(){
	table.draw();
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="page-header"><h4 class="text-center">Replacement Request Orders - Return Stock Notification <span class="badge" id="replacement_count"></span></h4></div>

	


	<table id="table_return_stock_notification" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
		<tr>
			<th colspan="3">
			<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<select class="form-control" name="status_of_stock" id="status_of_stock" onchange="draw_table()">
				<option value="all" selected >All</option>
				<option value="not_available">No Stock </option>
				<option value="available">Stock Available</option>
			</select>
		</div>
		</div>
		</th>
		</tr>
			<tr>
				<th class="text-primary small bold">Return Order Summary</th>
				<th class="text-primary small bold">Reason</th>
				<th class="text-primary small bold">Actions</th>
			</tr>
		</thead>
	</table>
</div>
</div>
</body> 
</html>  