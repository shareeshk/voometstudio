<html lang="en">
<head>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>

<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	

<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>

<style>
.align_top
{
	margin-top: 20px;
}
.align_bottom {
    margin-bottom: 20px;
}
</style>
<script>

function showDiv1() {
   document.getElementById('viewDiv1').style.display = "block";
   document.getElementById('createDiv2').style.display = "none";
   document.getElementById('editDiv3').style.display = "none"; 
}
function showDiv2() {
	$('select[name="vendors"]').css({"border": "1px solid #ccc"});
	$('select[name="logistics_service"]').css({"border": "1px solid #ccc"});
	$('input[name="shipping_price"]').css({"border": "1px solid #ccc"});
	$('#create_shipping_charge')[0].reset();
	document.getElementById('viewDiv1').style.display = "none";
	document.getElementById('createDiv2').style.display = "block";
	document.getElementById('editDiv3').style.display = "none";
}
function showDiv3() {
	$('select[name="edit_vendors"]').css({"border": "1px solid #ccc"});
	$('select[name="edit_logistics_service"]').css({"border": "1px solid #ccc"});
	$('input[name="edit_shipping_price"]').css({"border": "1px solid #ccc"});
   document.getElementById('viewDiv1').style.display = "none";
   document.getElementById('createDiv2').style.display = "none";
   document.getElementById('editDiv3').style.display = "block";
}

</script>

<?php //print_r($shipping_charge);?>

<div class="container-fluid">
<div><h3>Manage Shipping Charge</h3></div>

<div class="row-fluid">
	<nav class="navbar navbar-default">
		  <ul class="nav navbar-nav">
			<li><a href="#" onClick="showDiv1()">View</a></li>
			<li><a href="#" onClick="showDiv2()">Create</a></li>
			<!--<li><a href="#" onClick="showDiv3()">Edit</a></li>-->
		  </ul>
	</nav>

</div>

<div class="row-fluid" id="viewDiv1">
<table id="myTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<th>S. No</th>
		<th>Logistics Name </th>
		<th>City</th>
		<th>Vendor</th>
		<th>Shipping Price (<?php echo curr_sym; ?>)</th>
		<th>Last Updated</th>
		<th>Action</th>
	</tr>
</thead>
<tbody>
<?php
$i=0;
foreach ($shipping_charge as $shipping_charge_value) { 
$i++; ?>

<tr>
<td><?php echo $i; ?></td>
<td><?php echo $shipping_charge_value -> logistics_name?></td>
<td><?php echo $shipping_charge_value -> city?></td>
<td><?php echo $shipping_charge_value -> name?></td>
<td><?php echo $shipping_charge_value -> shipping_price?></td>
<td><?php echo $shipping_charge_value -> timestamp?></td>

<td>
	<input type="button" class="btn btn-warning" value="Edit" onClick="editShippingChargeFun('<?php echo $shipping_charge_value -> shipping_charge_id;?>')">
	<input type="button" class="btn btn-danger" value="Delete" onClick="deleteShippingChargeFun('<?php echo $shipping_charge_value -> shipping_charge_id;?>')">
</td>

</tr>
<?php } ?>
</tbody>
</table>
</div>

<div class="row-fluid" id="createDiv2" style="display:none;">
 <div class="col-md-12 text-center"><h4>Create Shipping Charge</h4></div>
                <div class="col-md-3"></div>
				<div class="col-md-6">
				
				<form name="create_shipping_charge" id="create_shipping_charge" method="post" action="#" onsubmit="return form_validation();">
					
					<div class="form-group align_top" >
						<select name="vendors" id="vendors" class="form-control">
						<option value="">-Select Vendor-</option>
						<?php foreach ($vendors as $vendors_value) {  ?>
						<option selected value="<?php echo $vendors_value->vendor_id; ?>"><?php echo $vendors_value->name; ?></option>
						<?php } ?>
						</select>					
					</div>

					<div class="form-group align_top" >
						<select name="logistics_service" id="logistics_service" class="form-control">
						<option value="">-Select Logistics Service City-</option>
						<?php foreach ($logistics_service as $logistics_service_value) {  ?>
						<option value="<?php echo $logistics_service_value->logistics_service_id; ?>"><?php echo $logistics_service_value->city; ?></option>
						<?php } ?>
						</select>					
					</div>
					
					<div class="form-group align_top" >

					<input id="shipping_price" name="shipping_price" type="text" class="form-control" placeholder="Enter Shipping Price" >
					</div>

					<br>

				<div class="row col-md-12 align_top text-center align_bottom">
					<button class="btn btn-info" type="submit"></i>Create</button>
					<button class="btn btn-primary" type="reset"></i>Reset</button>
				</div>
				
				</form>
				</div>
				<div class="col-md-3"></div>
</div>



<!--2nd--->

<!--2----end--->
<div class="row-fluid" id="editDiv3" style="display:none;">
 <div class="col-md-12 text-center"><h4>Edit Shipping Charge</h4></div>
                <div class="col-md-3"></div>
				<div class="col-md-6">
				
				<form name="edit_shipping_charge" id="edit_shipping_charge" method="post" action="#" onsubmit="return form_validation_edit();">
					
					<input type="hidden" name="edit_shipping_charge_id" id="edit_shipping_charge_id" value="">
					
					<div class="form-group align_top row">
					
					<div class="col-md-3">Select Vendor</div>
					<div class="col-md-9">
						<select name="edit_vendors" id="edit_vendors" class="form-control">
						<option value="">-Select Vendor Name-</option>
						<?php foreach ($vendors as $vendors_value) {  ?>
						<option selected value="<?php echo $vendors_value->vendor_id; ?>"><?php echo $vendors_value->name; ?></option>
						<?php } ?>
						</select>	
					
					</div>
					</div>
					
					
					<div class="form-group align_top row">
					
					<div class="col-md-3">Select Logistics Service City</div>
					<div class="col-md-9">
						<select name="edit_logistics_service" id="edit_logistics_service" class="form-control">
						<option value="">-Select Logistics Service City-</option>
						<?php foreach ($logistics_service as $logistics_service_value) {  ?>
						<option value="<?php echo $logistics_service_value->logistics_service_id; ?>"><?php echo $logistics_service_value->city; ?></option>
						<?php } ?>
						</select>	
					
					</div>
					</div>
					
					<div class="form-group align_top row">
					
					<div class="col-md-3">Enter Shipping Charge</div>
					<div class="col-md-9">
					<input id="edit_shipping_price" name="edit_shipping_price" type="text" class="form-control" placeholder="Enter Shipping Charge" >
					
					</div>
					</div>

				<div class="row col-md-12 align_top text-center align_bottom">
					<button class="btn btn-info" type="submit"></i>Edit</button>
					<button class="btn btn-primary" type="reset"></i>Reset</button>
				</div>
				
				</form>
				</div>
				<div class="col-md-3"></div>
</div>

</div>
</body>
</html>
<script>
$(document).ready(function(){
    $('#myTable').dataTable({ stateSave: true });
});
function editShippingChargeFun(shipping_charge_id){

			$('#edit_shipping_charge')[0].reset();
  
			$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/get_shipping_charge_data"?>',
				type: 'POST',
				data: "shipping_charge_id="+shipping_charge_id,
				dataType: 'JSON',	
			}).done(function(data){

				if(data)
				{
					shipping_charge_id=data.shipping_charge_id;
					logistics_service_id=data.logistics_service_id;
					vendor_id=data.vendor_id;
					shipping_price=data.shipping_price;
					
					$("#edit_shipping_charge_id").val(shipping_charge_id);
					$("#edit_vendors").val(vendor_id);
					$("#edit_logistics_service").val(logistics_service_id);
					$("#edit_shipping_price").val(shipping_price);
					
					showDiv3();
					
				}
				else
				{
					alert('error');
				}
				
			});

}
function deleteShippingChargeFun(shipping_charge_id){
	if(confirm("Are you sure want to delete?")){
		location.href="<?php echo base_url(); ?>admin/Catalogue/delete_shipping_charge/"+shipping_charge_id;
	}
}
$('#result').hide();

function form_validation()
{
	var vendors = $('select[name="vendors"]').val().trim();
	var logistics_service = $('select[name="logistics_service"]').val().trim();
	var shipping_price = $('input[name="shipping_price"]').val().trim();	

	   var err = 0;
		
		if(vendors=='')
		{
		   $('select[name="vendors"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('select[name="vendors"]').css({"border": "1px solid #ccc"});
		}
		if(logistics_service=='')
		{
		   $('select[name="logistics_service"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('select[name="logistics_service"]').css({"border": "1px solid #ccc"});
		}
		if(shipping_price=='')
		{
		   $('input[name="shipping_price"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('input[name="shipping_price"]').css({"border": "1px solid #ccc"});
		}
		
		if(err==1)
		{
			   $('#error_result_id').show();
			   $('#result').hide();
		}
		else
			{
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#create_shipping_charge');		
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_shipping_charge/"?>',
				type: 'POST',
				data: $('#create_shipping_charge').serialize(),
				dataType: 'html',
				beforeSend: function(){
				form.prepend( form_status.html('<p><i class="fa fa-refresh fa-spin"></i> Processing...</p>').fadeOut() );
			}
			}).done(function(data)
			  {
				  
				if(data)
				{
					  form_status.html('<p class="text-success">Shipping Charge is successfully added</p>').delay(3000).fadeOut();
					  
					  $('#create_shipping_charge')[0].reset();
					  location.reload();
				}
				else
				{
					alert('error');
					$('#create_shipping_charge')[0].reset();
				}
				
			});
			}
	
	return false;
}
function form_validation_edit()
{
	var vendors = $('select[name="edit_vendors"]').val().trim();
	var logistics_service = $('select[name="edit_logistics_service"]').val().trim();
	var shipping_price = $('input[name="edit_shipping_price"]').val().trim();	

	   var err = 0;
		
		if(vendors=='')
		{
		   $('select[name="edit_vendors"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('select[name="edit_vendors"]').css({"border": "1px solid #ccc"});
		}
		if(logistics_service=='')
		{
		   $('select[name="edit_logistics_service"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('select[name="edit_logistics_service"]').css({"border": "1px solid #ccc"});
		}
		if(shipping_price=='')
		{
		   $('input[name="edit_shipping_price"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('input[name="edit_shipping_price"]').css({"border": "1px solid #ccc"});
		}
		if(err==1)
		{
			   $('#error_result_id').show();
			   $('#result').hide();
		}
		else
			{
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#edit_shipping_charge');		
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_shipping_charge/"?>',
				type: 'POST',
				data: $('#edit_shipping_charge').serialize(),
				dataType: 'html',
				beforeSend: function(){
				form.prepend( form_status.html('<p><i class="fa fa-refresh fa-spin"></i> Processing...</p>').fadeOut() );
			}
			}).done(function(data)
			  {			
				if(data)
				{
					  form_status.html('<p class="text-success">Shipping Charge is successfully Updated</p>').delay(3000).fadeOut();  
					  $('#edit_shipping_charge')[0].reset();
					 // showDiv2();
					  location.reload();
				}
				else
				{
					alert('error');
					$('#edit_shipping_charge')[0].reset();	
				}
				
			});
			}
	
	return false;
}


</script>
