<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.wizard-card .tab-content {
    min-height: 200px;
}   
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<script>

function showDivPrevious(){
	var form = document.getElementById('search_for_logistics_territory_name');		
	
	form.action='<?php echo base_url()."admin/Catalogue/logistics_all_territory_name"; ?>';
	form.submit();
}

</script>
<form name="search_for_logistics_territory_name" id="search_for_logistics_territory_name" method="post">
		<input value="<?php echo $vendor_id_in_db; ?>" type="hidden" name="vendor_id"/>
		<input value="<?php echo $get_logistics_all_territory_name_data["logistics_id"]; ?>" type="hidden" name="logistics_id"/>
		<input value="view" type="hidden" name="create_editview">
	</form>
<script>

$(document).ready(function(){
	<?php
		if($create_editview=="create"){
			?>
			$("#viewDiv1").css({"display":"none"});
			$("#createDiv2").css({"display":""});
			<?php
		}
		else if($create_editview=="view"){
			?>
			$("#createDiv2").css({"display":"none"});
			$("#viewDiv1").css({"display":""});
			<?php
		}
	?>
});
</script>
<?php //print_r($shipping_charge);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row-fluid" id="editDiv3">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="edit_logistics_all_territory_name" id="edit_logistics_all_territory_name" method="post" action="#" onsubmit="return form_validation_edit();">
		<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_vendors: {
					required: true,
				},
				edit_logistics: {
					required: true,
				},
				edit_territory_name: {
					required: true,
				}

			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>	
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Territory Name</h3>
		</div>	
		</div> 
		
		<div class="tab-content">			
		<input type="hidden" name="edit_territory_name_id" id="edit_territory_name_id" value="<?php echo $get_logistics_all_territory_name_data["territory_name_id"];?>">
				
				
				<div class="row">
				<div class="col-md-10 col-md-offset-1">                        
				<div class="form-group label-floating">
					<label class="control-label">-Select Vendor-</label>
					<select name="edit_vendors" id="edit_vendors" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($vendors as $vendors_value) {  ?>
						<option value="<?php echo $vendors_value->vendor_id; ?>" <?php if($vendors_value->vendor_id==$vendor_id_in_db){echo "selected";}?>><?php echo $vendors_value->name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                             
				<div class="form-group label-floating">
					<label class="control-label">-Select Logistics Name-</label>
					<select name="edit_logistics" id="edit_logistics" class="form-control" disabled>
						<option value="<?php echo $logistics_id_in_db;?>"><?php echo $logistics_name_in_db;?></option>
					</select>
				</div>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Territory Name</label>
					<input id="edit_territory_name" name="edit_territory_name" type="text" class="form-control" value="<?php echo $get_logistics_all_territory_name_data["territory_name"];?>"/>
				</div>
				</div>
			</div>
			<?php
				if($check_make_as_local_logistics_all_territory_name_status=="no"){
			?>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Make as Local</label>
					<select id="edit_make_as_local" name="edit_make_as_local" class="form-control">
						<option value="0" <?php if($get_logistics_all_territory_name_data["make_as_local"]=='0'){echo "selected";}?>>No</option>
						<option value="1" <?php if($get_logistics_all_territory_name_data["make_as_local"]=='1'){echo "selected";}?>>Yes</option>
					</select>
				</div>
				</div>
			</div>
			<?php
				}
			?>
			<div class="row">
				<div class="col-md-10 col-md-offset-3"> 
					<button class="btn btn-info btn-sm" id="submit-data" disabled="" type="submit">Submit</button>
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
				</div>
			</div>
				
		</div>		
	</form>

</div>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>
</body>
</html>
<script>

$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_logistics_all_territory_name'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_logistics_all_territory_name"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_logistics_all_territory_name'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});
});
$('#result').hide();


function form_validation_edit()
{
	
	var territory_name = $('input[name="edit_territory_name"]').val().trim();
	   var err = 0;
	  
	   
	   if(territory_name=='')
		{		   	   
			err = 1;
		}
		
		
		if(err==0)
			{
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#edit_logistics_all_territory_name');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();
				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_logistics_all_territory_name/"?>',
				type: 'POST',
				data: $('#edit_logistics_all_territory_name').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {	
				if(data)
				{
					  					  
					swal({
						title:"Success", 
						text:"Territory Name is successfully Updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				else
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)	
					
				}
				
			});
}
}]);			
			}
	
	return false;
}


</script>
