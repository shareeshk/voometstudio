<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<script>
function show_view_validatn_create(){
	sort_order=document.getElementById("sort_order").value;
	if(sort_order==0){
	document.getElementById("view1").disabled = true;
	document.getElementById("view2").checked = true;					
	}
	if(sort_order!=0){
		document.getElementById("view1").disabled = false;
		document.getElementById("view1").checked = true;					
	}
}
</script>

<script>
$(document).ready(function(){
$('#pcat_view').on('change', function() {
	if(this.checked){
		$('#pcat_view').not(this).prop('checked', true); 
		$('#cat_view').not(this).prop('checked', false);  
		$('#subcat_view').not(this).prop('checked', false); 
	}else{
		$('#cat_view').not(this).prop('checked', false);  
		$('#subcat_view').not(this).prop('checked', false);
	}
});
$('#cat_view').on('change', function() {
	if(this.checked){
    $('#pcat_view').not(this).prop('checked', false);
	$('#cat_view').not(this).prop('checked', true); 	
	$('#subcat_view').not(this).prop('checked', false); 
	}else{
		$('#pcat_view').not(this).prop('checked', false);
		$('#subcat_view').not(this).prop('checked', false); 
	}
});
$('#subcat_view').on('change', function() {
	if(this.checked){
    $('#pcat_view').not(this).prop('checked', false);  
	$('#cat_view').not(this).prop('checked', false);
	$('#subcat_view').not(this).prop('checked', true);
}else{
	$('#pcat_view').not(this).prop('checked', false);  
	$('#cat_view').not(this).prop('checked', false);
}	
});
});
</script>

<script type="text/javascript">
var table;

$(document).ready(function (){
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    var rows_selected = [];
    table = $('#table_specification_group').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Catalogue/specification_group_processing",
			data: function (d) { d.pcat_id = $('#pcat_id').val();d.cat_id = $('#cat_id').val();d.subcat_id = $('#subcat_id').val();d.active =1;},
            type: "post",
            error: function(){
              $("#table_specification_group_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				
				document.getElementById("specification_group_count").innerHTML=json.recordsFiltered;
				
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
   
	$('#reset_form_button').on('click',function(){
		table.draw();
	});
	
	$("#submit_button").click(function() {
		table.draw();
	});
	$('#search_toggle').on('click',function(){	
		$('#search_block').toggle();
	});

});	


function drawtable(){
	table.draw();
}

function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_delete_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
		swal({
			title: 'Are you sure?',
			text: "Archived Specification Group(entire tree)",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Archive it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/update_specification_group_selected",
		type:"post",
		data:"selected_list="+selected_list+"&active=0",
		
		success:function(data){
			if(data==true){
				swal({
						title:"Archived!", 
						text:"Given "+table+"(s) has been archived successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
function multilpe_delete_when_no_data(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
		swal({
			title: 'Are you sure?',
			text: "Delete Specification Group",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/delete_specification_group_when_no_data",
		type:"post",
		data:"selected_list="+selected_list+"&active=1",
		
		success:function(data){
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Given "+table+"(s) has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else if(data==false){
				swal("Error", "Error in deleting this file", "error");
			}else{
				swal("Error", "Oops ..! You can't delete it.", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
</script>
<script>

$(document).ready(function(){
	<?php
		if($create_editview=="create"){
			?>
			$("#viewDiv1").css({"display":"none"});
			$("#createDiv2").css({"display":""});
			<?php
		}
		else if($create_editview=="view"){
			?>
			$("#createDiv2").css({"display":"none"});
			$("#viewDiv1").css({"display":""});
			<?php
		}
	?>
});
</script>
<script>

function changeViewPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/specification_group_filter';
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<?php 
//print_r($specification_group);
?>

<div class="container">

<div class="row" id="viewDiv1" style="display:none;">
	<div class="wizard-header text-center">
		<h5> Number of Specification_group <span class="badge"><div id="specification_group_count"></div></span> 
		</h5>                 		          	
	</div>
	<input value="<?php echo $pcat_id; ?>" type="hidden" id="pcat_id">
	<input value="<?php echo $cat_id; ?>" type="hidden" id="cat_id">
	<input value="<?php echo $subcat_id; ?>" type="hidden" id="subcat_id">
	
<table id="table_specification_group" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<th colspan="7">
			<button id="multiple_delete_button1" class="btn btn-warning btn-xs" onclick="multilpe_delete_fun('Specification Group')">Archive all</button>
			<button id="multiple_delete_button2" class="btn btn-danger btn-xs" onclick="multilpe_delete_when_no_data('Specification Group')">Delete</button>
			<button class="btn btn-primary btn-xs" type="button" onclick="changeViewPrevious()">Change View</button>
		</th>
	</tr>
	<tr>
		<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
		<th>Specification Group Name</th>
		<th>Sort Order</th>
		<th>Specification to Catalog</th>
		<th>View</th>
		<th>Last Updated</th>
		<th>Action</th>		
	</tr>
</thead>

</table>
</div>

<div class="row" id="createDiv2" style="display:none;">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">

	<form name="create_specicication_group" id="create_specicication_group" method="post" action="#" onsubmit="return form_validation();">
	<input type="hidden" name="sort_order_flag" id="sort_order_flag">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				parent_category: {
					required: true,
				},
				
				sort_order: {
					required: true,
				},
				specification_group_name: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Create Specification group</h3>
		</div>	
		</div>
		<div class="tab-content">
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Parent category-</label>
					<select name="parent_category" id="parent_category" class="form-control align_top" onchange="showAvailableCategories(this)">
						<option value=""></option>
						<?php foreach ($parent_category as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>"><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<option value="0">--None--</option>
						
					</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group label-floating">
					<label class="control-label">-Select Category-</label>
					<select name="categories" id="categories" class="form-control" onchange="showAvailableSubCategories(this)">
						<option value=""></option>
					</select>
				</div>
				</div>
			</div>
		
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Sub Category-</label>
					<select name="subcategories" id="subcategories" class="form-control" onchange="showSortOrderSpecification(this)">
						<option value=""></option>
					</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Specification Group Name</label>
					<input id="specification_group_name" name="specification_group_name" type="text" class="form-control" />
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<div class="checkbox">
						<label><input name="pcat_view" id="pcat_view" value="1" type="checkbox" disabled><a href="#" data-toggle="tooltip" title="This filterbox will be shown when customer clicks on Parent Category, in order to see all the inventories under that Parent Category. In this case, this filterbox is shown under filters section of frontend.">Is this filterbox common to Parent Category?</a></label>
					</div>
				</div>
				</div>
			</div>			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<div class="checkbox">
						<label><input name="cat_view" id="cat_view" value="1" type="checkbox" disabled><a href="#" data-toggle="tooltip" title="This filterbox will be shown when customer clicks on Category, in order to see all the inventories under that Category. In this case, this filterbox is shown under filters section of frontend.">Is this filterbox common to Category?</a></label>
					</div>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<div class="checkbox">
						<label><input name="subcat_view" id="subcat_view" value="1" type="checkbox" disabled><a href="#" data-toggle="tooltip" title="This filterbox will be shown when customer clicks on Sub Category, in order to see all the inventories under that Sub Category. In this case, this filterbox is shown under filters section of frontend.">Is this filterbox common to Sub Category?</a></label>
					</div>
				</div>
				</div>
			</div>		
			<div class="row">
				
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Sort Order Value(<span id="menu_sort_order_specification_status"></span>)</label>
					
					<select name="sort_order" id="sort_order" class="form-control" onchange="show_view_validatn_create()">
						
					</select>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-6 col-md-offset-3"> 
					<div class="form-group">
					<div class="col-md-6">Show Specification Group</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="view" id="view1" value="1" type="radio" checked="checked">View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="view" id="view2" value="0" type="radio">Hide</label>
					</div>
				
					</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-6 col-md-offset-4"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit_specification_group">Submit</button>
					<button class="btn btn-warning btn-sm" type="reset">Reset</button>
					<button class="btn btn-primary btn-sm" type="button" onclick="changeViewPrevious()">Change View</button>
					</div>
				</div>
			</div>				
		</div>

	</form>

</div>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
<script type="text/javascript">
function showSortOrderSpecification(obj){
	if(obj.value!="" && obj.value!="None"){
		document.getElementById("pcat_view").checked=false;
		document.getElementById("cat_view").checked=false;
		document.getElementById("subcat_view").checked=true;
		document.getElementById("pcat_view").disabled=true;
		document.getElementById("cat_view").disabled=true;
		document.getElementById("subcat_view").disabled=false;
	}else{
		document.getElementById("pcat_view").checked=false;
		document.getElementById("cat_view").checked=true;
		document.getElementById("subcat_view").checked=false;
		document.getElementById("pcat_view").disabled=true;
		document.getElementById("cat_view").disabled=false;
		document.getElementById("subcat_view").disabled=true;
	}

	
	
	
}
function showAvailableSubCategories(obj){
	var pcat_id=document.getElementById("parent_category").value;
	var cat_id=document.getElementById("categories").value;
	var subcat_id=document.getElementById("subcategories").value;

	if(pcat_id==0){
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/show_specification_sort_order",
		type:"post",
		data:"pcat_id="+pcat_id+"&cat_id="+cat_id+"&subcat_id="+subcat_id,
		
		success:function(data){
			$("#sort_order").html(data);
		}
	});
	}
	//if(obj.value!="" && obj.value!="None"){
		//cat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id+"&active=1",
				success:function(data){
					
					if(data!=0){
						$("#subcategories").html(data);
						//$("#sort_order").html("");
						document.getElementById("pcat_view").checked=false;
						document.getElementById("cat_view").checked=true;
						document.getElementById("subcat_view").checked=false;
						document.getElementById("pcat_view").disabled=true;
						document.getElementById("cat_view").disabled=false;
						document.getElementById("subcat_view").disabled=true;
					}
					else{
						$("#subcategories").html('<option value="0"></option>')
						if(pcat_id==0){
						$("#sort_order").html("");
						}
						//$("#brands").html('<option value="0">-None-</option>')
						document.getElementById("pcat_view").checked=true;
						document.getElementById("cat_view").checked=false;
						document.getElementById("subcat_view").checked=false;
						document.getElementById("pcat_view").disabled=false;
						document.getElementById("cat_view").disabled=true;
						document.getElementById("subcat_view").disabled=true;

					}
				}
			});
	//}
	
}

function showAvailableCategories(obj){
	var pcat_id=document.getElementById("parent_category").value;

	if(pcat_id!=0){
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/show_specification_sort_order",
		type:"post",
		data:"pcat_id="+pcat_id,
		
		success:function(data){
			$("#sort_order").html(data);
		}
	});
	}
	//if(obj.value!="" && obj.value!="None"){
		//pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#categories").html(data);
						if(pcat_id==0){
						$("#sort_order").html("");
						}
						document.getElementById("pcat_view").checked=true;
						document.getElementById("cat_view").checked=false;
						document.getElementById("subcat_view").checked=false;
						document.getElementById("pcat_view").disabled=false;
						document.getElementById("cat_view").disabled=true;
						document.getElementById("subcat_view").disabled=true;
					}
					else{
						$("#categories").html('<option value="0"></option>');
						$("#sort_order").html("");
						//$("#brands").html('<option value="0">-None-</option>')
						document.getElementById("pcat_view").checked=false;
						document.getElementById("cat_view").checked=false;
						document.getElementById("subcat_view").checked=false;
						document.getElementById("pcat_view").disabled=true;
						document.getElementById("cat_view").disabled=true;
						document.getElementById("subcat_view").disabled=true;
					}
				}
			});
	//}
	
}

function form_validation(){	

	var specification_group_name = $('input[name="specification_group_name"]').val().trim();
	var parent_category = $('select[name="parent_category"]').val().trim();
	var categories = $('select[name="categories"]').val().trim();
	var subcategories = $('select[name="subcategories"]').val().trim();
	var sort_order = $('select[name="sort_order"]').val().trim();
	var view = document.querySelector('input[name="view"]:checked').value;
	var pcat_view=document.getElementById('pcat_view').checked;
	var cat_view=document.getElementById('cat_view').checked;
	var subcat_view=document.getElementById('subcat_view').checked;

		var err = 0;
		
		if(specification_group_name=='')
		{
		   //$('input[name="specification_group_name"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('input[name="specification_group_name"]').css({"border": "1px solid #ccc"});
		}
		if(parent_category=='')
		{
		   //$('select[name="parent_category"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('select[name="parent_category"]').css({"border": "1px solid #ccc"});
		}
		if(sort_order=='')
		{
		   //$('input[name="sort_order"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('input[name="sort_order"]').css({"border": "1px solid #ccc"});
		}

		if(parent_category==0 || parent_category==''){
			
			if(categories=='' || categories==0)
			{
			   //$('select[name="categories"]').css("border", "1px solid red");	   
			   err = 1;
			}
			else{			
				//$('select[name="categories"]').css({"border": "1px solid #ccc"});
			}
			
		}else{
			//$('select[name="categories"]').css({"border": "1px solid #ccc"});	
		}
		
		
	if(err==1)
		{
			   $('#error_result_id').show();
			   $('#result').hide();
		}
		else
			{	
				document.getElementById("pcat_view").disabled="";
				document.getElementById("cat_view").disabled="";
				document.getElementById("subcat_view").disabled="";
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#create_specicication_group');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();			
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_specification_group/"?>',
				type: 'POST',
				data: $('#create_specicication_group').serialize(),
				dataType: 'html',
				
				}).done(function(data){
					form_status.html('') 
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					document.getElementById("specification_group_name").value="";
					document.getElementById("specification_group_name").focus();
				});
				}
					if(data=="yes"){
						  
						swal({
							title:"Success", 
							text:"Specification Group is successfully added!", 
							type: "success",
							allowOutsideClick: false
						}).then(function () {
							location.reload();

						});

					}if(data=="no"){
						swal(
							'Oops...',
							'Error in form',
							'error'
						)
					}
					
				});
}
}]);
			}
	return false;
}

</script>
</div>
</body>

</html>