<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/bootstrap-colorpicker.min.css" rel="stylesheet">
	
<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.input-group .input-group-addon {
	background-color:#ccc;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo base_url();?>assets/js/color_classifier.js"></script>

<script>
var table;

$(document).ready(function (){
    var rows_selected = [];
    table = $('#table_attribute_expansion').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Catalogue/attribute_expansion_processing",
			data: function (d) { d.attribute_id = <?php echo $attribute_id; ?>;},
            type: "post",
            error: function(){
              $("#table_attribute_expansion_processing").css("display","none");
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
   
	

});	


function drawtable(){
	table.draw();
}


</script>

</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<?php 
	//print_r($attributes);
?>
<div class="container">
<div class="row" id="viewDiv1">
	<div class="wizard-header text-center">
		<h5> Attribute Expansions
		</h5>                 		          	
	</div>	
<table id="table_attribute_expansion" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<?php
			if($get_attrib_expansions_count==0){
		?>
		<th colspan="6">
		
		<form   action="<?php echo base_url() ?>admin/Catalogue/attrib_expansion_form1" method="post"><input type="hidden" value="<?php echo $attribute_id; ?>" name="attribute_id"><input type="submit" value="Create Attribute Expansion" name="attrib_expansion" class="btn btn-warning"></form>
		
		</th>
		<?php
			}
		?>
	</tr>
	<tr>
		<th>Attribute Name</th>
		<th>Attribute Expansion Name</th>
		<th>Attribute Expansion Value</th>
		<th>Attribute Sub Options</th>
		<th>Date/Time</th>
		<th>Actions</th>		
	</tr>
</thead>
</table>
</div>

</div>

</div>
</body>

</html>

<script>
	function delete_attrib_expansionsFun(){
		
		swal({
			title: 'Are you sure?',
			text: "You want to delete",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
					
				$.ajax({
					url: '<?php echo base_url()."admin/Catalogue/delete_attrib_expansions"?>',
					type: 'POST',
					data: "attribute_id=<?php echo $attribute_id;?>",
				}).done(function(data){
					
						if(data==true){
							location.reload();
						}else{
							swal(
								'Oops...',
								'Error in form',
								'error'
							)
						}
					}); 

			});
			
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  
			});	
			
			
		
	}
</script>