<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style type="text/css">

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	

<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script type="text/javascript">

function showDivPrevious(){
	
	var form = document.getElementById('search_for_product');		
	
	form.action='<?php echo base_url()."admin/Catalogue/products"; ?>';
	form.submit();
			
	 //window.location.href = '<?php echo base_url(); ?>admin/Catalogue/category';
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<?php 
//print_r($products);
?>

<div class="container-fluid">

<div class="row" id="editDiv3">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">

	<form name="edit_product" id="edit_product" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_parent_category: {
					required: true,
				},
				edit_categories: {
					required: true,
				},
				edit_subcategories: {
					required: true,
				},
				edit_brands: {
					required: true,
				},
				edit_product_name: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Product</h3>
		</div>	
		</div>
		
		<div class="tab-content">
			<input type="hidden" name="edit_product_id" id="edit_product_id" value="<?php echo $get_products_data["product_id"];?>">
			
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Parent category-</label>
					<select name="edit_parent_category" id="edit_parent_category" class="form-control" onchange="showAvailableCategoriesEdit(this)" disabled>
						<option value=""></option>
						<?php foreach ($parent_catagories as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>" <?php echo ($parent_category_value->pcat_id==$pcat_id)? "selected":''; ?>><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<?php if(empty($parent_catagories)){
							?>
							<option value="0" selected>--None--</option>
							<?php 
						}else{?>
						<option value="0">--None--</option>
						<?php }
						?>
					</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">-Select Category-</label>
					<select name="edit_categories" id="edit_categories" class="form-control" onchange="showAvailableSubCategoriesEdit(this)" disabled>
						<option value=""></option>
						<?php foreach ($categories as $category_value) {  ?>
						<option value="<?php echo $category_value->cat_id; ?>" <?php echo ($category_value->cat_id==$cat_id)? "selected":''; ?>><?php echo $category_value->cat_name; ?></option>
						<?php } ?>
		
					</select>
				</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Sub Category-</label>
					<select name="edit_subcategories" id="edit_subcategories" class="form-control" onchange="showAvailableBrands(this)" disabled>
						<option value=""></option>
						<?php foreach ($subcategories as $subcategory_value) {  ?>
						<option value="<?php echo $subcategory_value->subcat_id; ?>" <?php echo ($subcategory_value->subcat_id==$subcat_id)? "selected":''; ?>><?php echo $subcategory_value->subcat_name; ?></option>
						<?php } ?>
		
					</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">-Select Brands-</label>
					<select name="edit_brands" id="edit_brands" class="form-control align_top" disabled>
						<option value=""></option>
						<?php foreach ($brands as $brands_value) {  ?>
						<option value="<?php echo $brands_value->brand_id; ?>" <?php echo ($brands_value->brand_id==$brand_id)? "selected":''; ?>><?php echo $brands_value->brand_name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>			
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Enter Product Name </label>
					<textarea id="edit_product_name" name="edit_product_name" class="form-control"><?php echo $get_products_data["product_name"];?></textarea>
					<input id="edit_product_name_default" name="edit_product_name_default" type="hidden" class="form-control" value="<?php echo $get_products_data["product_name"];?>"  onkeypress="return blockSpecialChar(event)"/>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">Enter Product Description </label>
					<textarea id="edit_product_description" name="edit_product_description" class="form-control" ><?php 
					$str=$get_products_data["product_description"];
					//$str=str_replace('\r\n','<br>',$str);
					//echo nl2br($str);
					echo $str;
					?></textarea>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-12">
					<h4 class="info-text">Fill the Suggested Keywords for Searching this product</h4>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Suggested Keywords (1)</label>
					<textarea id="edit_sugg1" name="edit_sugg1" type="text" class="form-control"><?php echo $get_products_data["sugg1"];?></textarea>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">Suggested Keywords (2) </label>
					<textarea id="edit_sugg2" name="edit_sugg2" type="text" class="form-control"><?php echo $get_products_data["sugg2"];?></textarea>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">General Suggested Keywords (1) </label>
					<textarea id="edit_generalsugg1" name="edit_generalsugg1" type="text" class="form-control" ><?php echo $get_products_data["generalsugg1"];?></textarea>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">General Suggested Keywords (2) </label>
					<textarea id="edit_generalsugg2" name="edit_generalsugg2" type="text" class="form-control" ><?php echo $get_products_data["generalsugg2"];?></textarea>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">General Suggested Keywords (3) </label>
					<textarea id="edit_generalsugg3" name="edit_generalsugg3" type="text" class="form-control"><?php echo $get_products_data["generalsugg3"];?></textarea>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">General Suggested Keywords (4) </label>
					<textarea id="edit_generalsugg4" name="edit_generalsugg4" type="text" class="form-control" ><?php echo $get_products_data["generalsugg4"];?></textarea>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-3"> 
					<div class="form-group">
					<div class="col-md-3">Show Product</div>
					<div class="col-md-2">
						<label class="radio-inline"><input name="edit_view" id="edit_view1" value="1" type="radio" <?php if($get_products_data["view"]=="1"){echo "checked";}?>>View</label>
					</div>
					<div class="col-md-2">
						<label class="radio-inline"><input name="edit_view" id="edit_view2" value="0" type="radio" <?php if($get_products_data["view"]=="0"){echo "checked";}?>>Hide</label>
					</div>
				
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-4"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit-data" disabled="">Submit</button>
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
					
					</div>
				</div>
			</div>			
					

		</div>
	</form>
	<form name="search_for_product" id="search_for_product" method="post">
		<input value="<?php echo $get_products_data["pcat_id"]; ?>" type="hidden" id="pcat_id" name="pcat_id"/>
		<input value="<?php echo $get_products_data["cat_id"]; ?>" type="hidden" id="cat_id" name="cat_id"/>
		<input value="<?php echo $get_products_data["subcat_id"]; ?>" type="hidden" id="subcat_id" name="subcat_id"/>
		<input value="<?php echo $get_products_data["brand_id"]; ?>" type="hidden" id="brand_id" name="brand_id"/>
		<input value="view" type="hidden" name="create_editview">
	</form>			
</div>
</div>
</div>
</div>

</div>
<div class="footer">
</div>
<script type="text/javascript">

$(document).ready(function (){
	pcat_id=$('#f_pcat_id').val();
	cat_id=$('#f_cat_id').val();
	subcat_id=$('#f_subcat_id').val();
	brand_id=$('#f_brand_id').val();
	product_id=$('#f_product_id').val();
	inventory_id=$('#f_inventory_id').val();

	if(pcat_id!='' && pcat_id!=null){
		
		$("#pcat_id").val(pcat_id);
		
		$.ajax({
			url:"<?php echo base_url(); ?>admin/Catalogue/show_available_brands",
			type:"post",
			async:false,
			data:"subcat_id="+subcat_id+"&active=1",
			success:function(data){
				if(data!=0){
					$("#brand_id").html(data);
					$("#brand_id").val(brand_id);
				}
				else{
					$("#brand_id").html('<option value=""></option>');
				}
			}
		});
		$.ajax({
			url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
			type:"post",
			async:false,
			data:"cat_id="+cat_id+"&active=1",
			success:function(data){
				if(data!=0){
					$("#subcat_id").html(data);
					$("#subcat_id").val(subcat_id);
				}
				else{
					$("#subcat_id").html('<option value=""></option>')
					$("#brand_id").html('<option value=""></option>')

				}
			}
		});
		$.ajax({
			url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
			type:"post",
			async:false,
			data:"pcat_id="+pcat_id+"&active=1",
			success:function(data){
				
				if(data!=0){
					$("#cat_id").html(data);
					$("#cat_id").val(cat_id);
				}
				else{
					$("#cat_id").html('<option value=""></option>')
					$("#brand_id").html('<option value=""></option>')

				}
			}
		});
		table.draw();
	}
});

function manageInventoryFun(product_id){
		$.ajax({
			url: '<?php echo base_url(); ?>admin/Catalogue/inventory/',
			type: 'POST',
			data: "product_id="+product_id,
			dataType: 'JSON',	
		}).done(function(data){
			alert(data);
		});

}

function showAvailableBrands(obj){
	
	if(obj.value!="" && obj.value!="None"){
		subcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_brands",
				type:"post",
				data:"subcat_id="+subcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#brands").html(data);
						$("#edit_brands").html(data);
						
						
					}
					else{
						$("#brands").html('<option value="">-None-</option>');
						$("#edit_brands").html('<option value="">-None-</option>');

					}
				}
			});
	}
	
}

function showAvailableCategoriesEdit(obj)
{
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#edit_categories").html(data);
					}
					else{
						$("#edit_categories").html('<option value="">-None-</option>')

					}
				}
			});
	}
}
function showAvailableSubCategoriesEdit(obj)
{
	if(obj.value!="" && obj.value!="None"){
		cat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#edit_subcategories").html(data);
					}
					else{
						$("#edit_subcategories").html('<option value="">-None-</option>');

					}
				}
			});
	}
}

$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_product'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_product"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_product'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select' || type == 'textarea') {
            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});

});
function form_validation_edit()
{

	var product_id=$("#edit_product_id").val();
	var product_name = $('textarea[name="edit_product_name"]').val().trim();
	var parent_category = $('select[name="edit_parent_category"]').val().trim();
	var categories = $('select[name="edit_categories"]').val().trim();
	var subcategories = $('select[name="edit_subcategories"]').val().trim();
	var brands = $('select[name="edit_brands"]').val().trim();
	var view = document.querySelector('input[name="edit_view"]:checked').value;
	    var err = 0;
		if(!(product_name.length>=3) || (parent_category=='') || (categories=='') || (subcategories=='') || (brands=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0){
				document.getElementById("edit_parent_category").disabled="";
				document.getElementById("edit_categories").disabled="";
				document.getElementById("edit_subcategories").disabled="";
				document.getElementById("edit_brands").disabled="";
			var form_status = $('<div class="form_status"></div>');		
			var form = $('#edit_product');		
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();			
					 $.ajax({
							url: '<?php echo base_url()."admin/Catalogue/edit_products/"?>',
							type: 'POST',
							data: $('#edit_product').serialize(),
							dataType: 'html',
							
						}).done(function(data)
						  {
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					
					edit_product_name_default=document.getElementById("edit_product_name_default").value;
					document.getElementById("edit_product_name").value=edit_product_name_default;
					document.getElementById("edit_product_name").focus();
					document.getElementById("edit_parent_category").disabled=true;
					document.getElementById("edit_categories").disabled=true;
					document.getElementById("edit_subcategories").disabled=true;
					document.getElementById("edit_brands").disabled=true;
				});
				  }

								if(data==1)
								{
									  									  
									swal({
										title:"Success", 
										text:"Products is successfully updated!", 
										type: "success",
										allowOutsideClick: false
									}).then(function () {
										location.reload();

									});
								}
								if(data==0)
								{
									swal(
										'Oops...',
										'Error in form',
										'error'
									)		
								}
								
							}); 
}
}]);							
			}
	
	return false;
}

function blockSpecialChar(e){
        var k;
        document.all ? k = e.keyCode : k = e.which;
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
        } 
</script>
</div>
</body>

</html>