<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />

<style type="text/css">

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.wizard-card .tab-content {
    min-height: 400px;
    
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	

<script>
function show_view_validatn_create(){
	menu_sort_order=document.getElementById("menu_sort_order").value;
	if(menu_sort_order==0){
	document.getElementById("view1").disabled = true;
	document.getElementById("view2").checked = true;					
	}
	if(menu_sort_order!=0){
		document.getElementById("view1").disabled = false;
		document.getElementById("view1").checked = true;					
	}
}
</script>	
<script type="text/javascript">
var table;
$(document).ready(function (){

	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
	
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    var rows_selected = [];
     table = $('#table_category').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Catalogue/category_processing",
			data: function (d) { d.pcat_id = $('#pcat_id').val();d.active=1;},
            type: "post",
            error: function(){
              $("#table_category_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("category_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
   
	$('#reset_form_button').on('click',function(){
		table.draw();
	});
	
	$("#submit_button").click(function() {
		pcat_id=$('#pcat_id').val();
		table.draw();
	});

});	

function showAvailableCategories(obj){
	table.draw();
}


function myCategory(obj){
	
	if (document.getElementById("menu_level").value ==1){
		document.getElementById("menu_level_display").style.display = "none";
		document.getElementById('menu_sort_display').style.display = "block";
		
		var menu_sort_order=document.getElementById("menu_sort_order").value;
		var menu_level=document.getElementById("menu_level").value;
		$.ajax({
			url:"<?php echo base_url()?>admin/Catalogue/show_cat_sort_order",
			type:"post",
			data:"menu_sort_order="+menu_sort_order+"&menu_level="+menu_level,
			
			success:function(data){
				$("#menu_sort_order").html(data);

			}
		});	
	}
	
	
	if (document.getElementById("menu_level").value ==2){
		document.getElementById('menu_sort_display').style.display = "none";
		document.getElementById("menu_level_display").style.display = "block";
	}
	if (document.getElementById("menu_level").value ==""){
		document.getElementById('menu_sort_display').style.display = "none";
		document.getElementById("menu_level_display").style.display = "none";
	}
	
	if(obj.value!=""){
		menu_level=obj.value;
			$.ajax({
				url:"<?php echo base_url()?>admin/Catalogue/get_create_parent_category",
				type:"post",
				data:"menu_level="+menu_level,
				success:function(data){
				$("#parent_category").html(data);
				
				}				
				})
		}

}															
function myCategorysortorder(obj){
	document.getElementById('menu_sort_display').style.display = "block";
	if (document.getElementById("parent_category").value ==""){
		document.getElementById('menu_sort_display').style.display = "none";
		
	}
	
	var menu_sort_order=document.getElementById("menu_sort_order").value;
	var menu_level=document.getElementById("menu_level").value;
	if(obj.value!=""){
		pcat_id=obj.value;
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/show_cat_sort_order",
		type:"post",
		//dataType:"JSON",
		data:"menu_sort_order="+menu_sort_order+"&menu_level="+menu_level+"&pcat_id="+pcat_id,
		
		success:function(data){
			$("#menu_sort_order").html(data);
		}
	});
	}

}	

function myeditCategory(obj,type){
	pcat_id="";
	if(type=="pcat"){
		if($("#edit_menu_level").val()=="2"){
				pcat_id=obj.value;
		}
	}
	
if($("#edit_menu_level").val()!=""){
menu_level=$("#edit_menu_level").val();
menu_level_default_value=$("#edit_menu_level_default_value").val();
edit_parent_category_default_value=$("#edit_parent_category_default_value").val();

$.ajax({
	url:"<?php echo base_url()?>admin/Catalogue/get_edit_parent_category",
	type:"post",
	data:"menu_level="+menu_level+"&menu_level_default_value="+menu_level_default_value+"&edit_parent_category_default_value="+edit_parent_category_default_value+"&pcat_id="+pcat_id,
	success:function(data){
	data_arr=data.split("$$$$$");
	$("#edit_parent_category").html(data_arr[0]);
	$("#menu_edit_sort_order_status").html(data_arr[1]);
	$("#edit_menu_sort_order").val("");
	document.getElementById("myeditcat").style.display="none";
	menu_level_edit_menu_sort_order=$("#menu_level_edit_menu_sort_order").val();
	if(menu_level_edit_menu_sort_order!=""){
	menu_level_edit_menu_sort_order_arr=menu_level_edit_menu_sort_order.split("-");
	
	if(menu_level_edit_menu_sort_order_arr[0]==$("#edit_menu_level").val()){
		$("#edit_menu_sort_order").val(menu_level_edit_menu_sort_order_arr[1]);
		$("#menu_edit_sort_order_status").html(data_arr[1]);
		parent_cat_default_id=$("#edit_parent_category_default_value").val();
		
		if(parent_cat_default_id==$("#edit_parent_category").val()){
		$("#edit_menu_sort_order").val(menu_level_edit_menu_sort_order_arr[1]);
		document.getElementById("myeditcat").style.display="block";
		}
		if(parent_cat_default_id!=$("#edit_parent_category").val()){
		$("#edit_menu_sort_order").val("");
		document.getElementById("myeditcat").style.display="none";
		}
	}
	
}
	$('form[name="edit_category"]').trigger('change');
	}
	});

}
}

function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_delete_fun(table){
	

	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	selected_list=selected_list_arr.join(",");

    swal({
			title: 'Are you sure?',
			text: "Archived category(entire tree)",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Archive it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/update_category_selected",
		type:"post",
		data:"selected_list="+selected_list+"&active=0",
		
		success:function(data){	
			if(data==true){
				swal({
						title:"Archived!", 
						text:"Given "+table+"(s) has been Archived successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});

}
function multilpe_delete_when_no_data(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");

	swal({
			title: 'Are you sure?',
			text: "Delete category",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/delete_category_when_no_data",
		type:"post",
		data:"selected_list="+selected_list,
		
		success:function(data){
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Given "+table+"(s) has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else if(data==false){
				swal("Error", "Error in deleting this file", "error");
			}else{
				swal("Error", "Oops ..! You can't delete it.", "error");
			}
		}
	});
	})
		},
		allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}

</script>
<script>

$(document).ready(function(){
	<?php
		if($create_editview=="create"){
			?>
			$("#viewDiv1").css({"display":"none"});
			$("#createDiv2").css({"display":""});
			<?php
		}
		else if($create_editview=="view"){
			?>
			$("#createDiv2").css({"display":"none"});
			$("#viewDiv1").css({"display":""});
			<?php
		}
	?>
});
</script>
<script>

function changeViewPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/category_filter';
}
</script>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">

<div class="row" id="viewDiv1" style="display:none;">
	<div class="wizard-header text-center">
		<h5> Number of Category <span class="badge"><div id="category_count"></div></span> 
		</h5>                        		
		                       	
	</div>
	<input value="<?php echo $pcat_id; ?>" type="hidden" id="pcat_id">
	

<table id="table_category" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<th colspan="6">
			<button id="multiple_delete_button" class="btn btn-warning btn-xs" onclick="multilpe_delete_fun('category')">Archive all</button>
			<button id="multiple_delete_button2" class="btn btn-danger btn-xs" onclick="multilpe_delete_when_no_data('category')">Delete</button>
			<button class="btn btn-primary btn-xs" type="button" onclick="changeViewPrevious()">Change View</button>
		</th>
	</tr>
	<tr>
		<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
		<th>Category Name </th>
		<th>Parent Category</th>
		<th>Details</th>
		<th>Last Updated</th>
		<th>Action</th>
	</tr>
</thead>

</table>
</div>

<div class="row" id="createDiv2" style="display:none;">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">

	<form name="create_category" id="create_category" method="post" action="#" onsubmit="return form_validation();" enctype="multipart/form-data">
	<input type="hidden" name="sort_order_flag" id="sort_order_flag">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				menu_level: {
					required: true,
				},
				parent_category: {
					required: true,
				},
				cat_name: {
					required: true,
				},
				menu_sort_order: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Create Category</h3>
		</div>	
		</div>
		<div class="tab-content">
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Menu Level-</label>
					<select name="menu_level" id="menu_level" class="form-control" onchange="myCategory(this)">
						<option value="" selected></option>
						<option value="1">Assign as Parent Category</option>
						<option value="2">Assign under Parent Category</option>
			
					</select>
				</div>
				</div>
			</div>
		
			<div class="row" style="display:none;" id="menu_level_display">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Parent Category-</label>		
					<select name="parent_category" id="parent_category" class="form-control" onchange="myCategorysortorder(this)">
						<option value=""></option>
						<?php foreach ($parent_category as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>"><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<option value="0">--None--</option>
						
					</select>
				</div>
				</div>
			</div>
			
			<div class="row" style="display:none;" id="menu_sort_display">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter the Sort Order</label>
					<select name="menu_sort_order" id="menu_sort_order" class="form-control" onchange="show_view_validatn_create()">
						
					</select>	
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Category Name</label>
					<input id="cat_name" name="cat_name" type="text" class="form-control" />
				</div>
				</div>
			</div>		
			
							
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group">
					<div class="col-md-6">Show Category</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="view" id="view1" value="1" type="radio" checked="checked">View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="view" id="view2" value="0" type="radio">Hide</label>
					</div>
				
					</div>
				</div>
			</div>		
			
			<div class="row">
				<div class="col-md-10 col-md-offset-2"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" id="submit_parent_category" type="submit">Submit</button>
					<button class="btn btn-Warning btn-sm" type="reset">Reset</button>
					<button class="btn btn-primary btn-sm" type="button" onclick="changeViewPrevious()">Change View</button>
					</div>
				</div>
			</div>
	
		</div>		
	</form>

</div>
</div>
</div>
</div>
<div class="footer">
</div>

</div>
</div>
</body>
</html>

<script type="text/javascript">

function form_validation(){
	
	var cat_name = $('input[name="cat_name"]').val().trim();
	var parent_category = $('select[name="parent_category"]').val().trim();
	var menu_level = $('select[name="menu_level"]').val().trim();
	var menu_sort_order = $('select[name="menu_sort_order"]').val().trim();
	var view = document.querySelector('input[name="view"]:checked').value;

	var err = 0;
	if((cat_name=='') || (parent_category=='') || (menu_level=='') || (menu_sort_order=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
	if(err==0){

			var form_status = $('<div class="form_status"></div>');		
			var form = $('#create_category');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();			
			$.ajax({
			url: '<?php echo base_url()."admin/Catalogue/add_category"?>',
			type: 'POST',
			data: $('#create_category').serialize(),
			dataType: 'html',
			
		}).done(function(data)
		  {
			  form_status.html('')
			  if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					document.getElementById("cat_name").value="";
					document.getElementById("cat_name").focus();
				});
				  }
			if(data=="yes")
			{
				swal({
					title:"Success", 
					text:"Category is successfully added!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
				  
				  
			}
			if(data=="no")
			{
			
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
			
			
		});
	}
}]);
	}
	return false;
}

</script>
<script>
	$(document).ready(function(){
		if(localStorage.getItem("dropdown_pcat_id")){
			$('#pcat_id').val(localStorage.getItem("dropdown_pcat_id")).trigger('change');
			localStorage.removeItem("dropdown_pcat_id")
		}
	});
</script>