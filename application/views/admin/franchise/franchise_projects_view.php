<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script type="text/javascript">
       $(document).ready(function () {
          
		   $.ajax({
			   url:"<?php echo base_url()?>admin/Franchise/franchises_projects_count/<?php echo $id;?>",
			   type:"POST",
			   success:function(data){
				   
				   $("#franchises_projects_count").text(data);
			   }
		   })
			

       }); 

	   
</script>


<?php //print_r($franchise);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="row" id="viewDiv1" >
	<div class="wizard-header text-center">
		<h5> Number of franchise Projects <span class="badge"><div id="myTable_count"></div></span> 
		</h5>                        		
		                       	
	</div>
	
	<?php if(!empty($franchise_details)){
		?>
	<div class="wizard-header text-center">
		<h4><u>Franchise Details</u></h4>                        		
		   <p>Name:<?php echo $franchise_details->name; ?></p>                    	
		   <p>Email:<?php echo $franchise_details->email; ?></p>                    	
		   <p>Mobile:<?php echo $franchise_details->mobile; ?></p>                    	
		              <a href="<?php echo base_url().'admin/Franchise/franchises' ?>" class="btn btn-sm btn-info"  >Back</a>	
	</div>
	
	<?php 
	} ?>
<table id="myTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<!-- <tr>
		<th colspan="6">
			<button id="multiple_delete_button" class="btn btn-danger btn-xs" onclick="multilpe_delete_fun('franchises')">Delete</button>
			<button id="multiple_delete_button4" class="btn btn-info btn-xs" onclick="showDivCreate()">Go to Create</button>
		</th>
	</tr> -->
	<tr>
		<!-- <th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th> -->
		<tr>
			                <th>S.No</th>
							<th>Project </th>
							<th>Client Details</th>
							<th>Type</th>
			                <th>Requirements</th>
			                <th>Deadline</th>
			                <th>Tentative budget</th>
			                <th>Status</th>
			                <th>Last updated on</th>
			         
	</tr>
</thead>

</table>
</div>


</div>
<div class="footer">
</div>

</div>
</body>
</html>
<script type="text/javascript">
var table;
$(document).ready(function(){
   // $('#myTable').dataTable({ stateSave: true });
    table = $('#myTable').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Franchise/franchises_projects_processing",
			data: function (d) { d.active=1;d.franchise_customer_id='<?php echo $id; ?>'},
            type: "post",
            error: function(){
              $("#myTable_processing").css("display","none");
            },
            "dataSrc": function ( json ) {
                document.getElementById("myTable_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },		  
        'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});

</script>
