<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script type="text/javascript">
       $(document).ready(function () {
          
		   $.ajax({
			   url:"<?php echo base_url()?>admin/Franchise/franchises_count",
			   type:"POST",
			   success:function(data){
				   
				   $("#franchises_count").text(data);
			   }
		   })
			

       }); 

function isNumber(evt) {

    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
	
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
	
    return true;
}	   
</script>
<script>

function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_delete_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
	swal({
			title: 'Are you sure?',
			text: "Delete franchises",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Franchise/update_franchises_selected",
		type:"post",
		data:"selected_list="+selected_list+"&active=0",
		
		success:function(data){
			//$("#multiple_delete_button").html('Delete Selected Items');
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Given "+table+"(s) has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
function showDivCreate(){
	
	document.getElementById('viewDiv1').style.display = "none";
	document.getElementById('createDiv2').style.display = "block";
	
}
function showDivEdit(){
	
	document.getElementById('viewDiv1').style.display = "block";
	document.getElementById('createDiv2').style.display = "none";
	
}
</script>

<?php //print_r($franchise);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="row" id="viewDiv1" >
	<div class="wizard-header text-center">
		<h5> Number of franchise <span class="badge"><div id="myTable_count"></div></span> 
		</h5>                        		
		                       	
	</div>
<table id="myTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<!-- <tr>
		<th colspan="6">
			<button id="multiple_delete_button" class="btn btn-danger btn-xs" onclick="multilpe_delete_fun('franchises')">Delete</button>
			<button id="multiple_delete_button4" class="btn btn-info btn-xs" onclick="showDivCreate()">Go to Create</button>
		</th>
	</tr> -->
	<tr>
		<!-- <th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th> -->
		<th>S. No</th>
		<th>Details </th>
		
		<th>Projects</th>
		<th>Profile status</th>
		<th>Last Updated</th>
		<th>Action</th>
	</tr>
</thead>

</table>
</div>

<div class="row-fluid" id="createDiv2" style="display:none;">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">

	<form name="create_franchises" id="create_franchises" method="post" action="#" onsubmit="return form_validation();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				name: {
					required: true,
				},
				email: {
					required: true,
				},
				mobile: {
					required: true,
				},
				landline: {
					//required: true,
				},
				address1: {
					required: true,
				},
				address2: {
					required: true,
				},
				pincode: {
					required: true,
				}
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>	
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Create Franchise</h3>
		</div>	
		</div>
		<div class="tab-content">
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Name</label>
					<input id="name" name="name" type="text" class="form-control" />
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter email</label>
					<input id="email" name="email" type="email" class="form-control" />
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Mobile</label>
					<input id="mobile" name="mobile" type="text" class="form-control" maxlength="13" onkeypress="return isNumber(event)"/>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Landline Number</label>
					<input id="landline" name="landline" type="text" class="form-control" maxlength="13" onkeypress="return isNumber(event)"/>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Address line 1</label>
					<input id="address1" name="address1" type="text" class="form-control" />
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Address line 2</label>
					<input id="address2" name="address2" type="text" class="form-control" />
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Pincode</label>
					<input id="pincode" name="pincode" type="text" class="form-control" />
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-8 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit_create_parent_category">Submit</button>
					<button class="btn btn-warning btn-sm" id="reset_create_parent_category" type="reset">Reset</button>
					<button class="btn btn-info btn-sm" type="button" onclick="showDivEdit()">Go to View</button>
					</div>
				</div>
			</div>		

		</div>		
	</form>

</div>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>
</body>
</html>
<script type="text/javascript">
var table;
$(document).ready(function(){
   // $('#myTable').dataTable({ stateSave: true });
    table = $('#myTable').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Franchise/franchises_processing",
			data: function (d) { d.active=1;},
            type: "post",
            error: function(){
              $("#myTable_processing").css("display","none");
            },
            "dataSrc": function ( json ) {
                document.getElementById("myTable_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },		  
        'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});

function change_status(app_id,status){
    
    if(app_id!='' && status!=''){
        
        swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
        $.ajax({
            url:"<?php echo base_url()?>admin/Franchise/franchise_change_status",
            type:"POST",
            data:"id="+app_id+"&status="+status,
            dataType: "JSON",                
            asyc:false,
            success:function(data){
                if (parseInt(data.flag) == 1) {
                    swal(
                        data.message,
                        '',
                        'info'
                    ) 
                    table.draw();
                } 
            }
        });
        
         }
}]);
    }
    
}

function deletefranchisesFun(id){
	if(confirm("Are you sure want to delete?")){
		location.href="<?php echo base_url(); ?>admin/Franchise/delete_franchises/"+id;
	}
}

function form_validation()
{
	var name = $('input[name="name"]').val().trim();
	var email = $('input[name="email"]').val().trim();
	var mobile = $('input[name="mobile"]').val().trim();
	var pincode = $('input[name="pincode"]').val().trim();
	var address1 = $('input[name="address1"]').val().trim();
	var address2 = $('input[name="address2"]').val().trim();
	var landline = $('input[name="landline"]').val().trim();

	   var err = 0;
	   if((name=='') || (email=='') || (mobile=='') || (pincode=='') || (address1=='') || (address2=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
var form_status = $('<div class="form_status"></div>');		
var form = $('#create_franchises');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();		
				$.ajax({
				url: '<?php echo base_url()."admin/Franchise/add_franchises/"?>',
				type: 'POST',
				data: $('#create_franchises').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				});
			}  
				if(data=="yes")
				{
					  					  
					swal({
						title:"Success", 
						text:"franchises is successfully added!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				if(data=="no")
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
			});
}
}]);			
			}
	
	return false;
}

</script>
