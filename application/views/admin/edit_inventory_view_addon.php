<!doctype html>
<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />	
<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 
<style type="text/css">
	.nav-pills > li.active > a{
		width: fit-content;
	}
 .wizard-navigation {
        -webkit-transform: translateZ(0);
		-webkit-backface-visibility: hidden;
    }
.small_color_box{
	width: 30px;
	height: 30px;
	border: 1px solid #ccc;
	margin-top: 1px;
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}

#edit_wizardPictureSmall_picname{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname2{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname2{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname3{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname3{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname4{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname4{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname5{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname5{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname6{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname6{
	word-wrap: break-word;
}
textarea.form-control {
    height:38px;
}
.CamListDiv {
    height: 450px;
    float: left;
    overflow: scroll;
    overflow-x:hidden;

}
.well{
	width:100%;

}

</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<?php 
	if(isset($id)){
		
		$inventory_id=$id;
		$cat_id=$cat_id;
		$subcat_id=$subcat_id;
		$brand_id=$brand_id;
		$product_id=$product_id;
	}else{
		header("Location:".base_url()."admin/Catalogue/inventory/");	
	}

?>
<script type="text/javascript">
$(document).ready(function (){
$('#back').on('click',function(){	
		var form = document.getElementById('back_to_inventory');
		form.action='<?php echo base_url(); ?>admin/Catalogue/inventory';
		form.submit();
	});
});		
</script>
<script type="text/javascript">
function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

function edit_filter_details_form_valid()
{
	var err = 0;	
	var filter=document.getElementsByName("edit_filter[]");

	if(err==1)
	{
		//$('#edit_validation_error').show();
		return false;
		
	}else{
				
				
				var data = $("#highlight").Editor("getText").trim();
				$("#highlight").val(data);
				
				
				
				var form_status = $('<div class="form_status"></div>');	
				var form_edit = document.forms.namedItem("edit_inventory");
				var ajaxData = new FormData(form_edit);	
		
				var form = $('#edit_inventory');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_inventory/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false,  
				
			}).done(function(data)
			  {
				  
				if(data==true)
				{
					swal({
						title:"Success", 
						text:"Inventory is successfully Updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();
					});
				}
				else
				{
					swal({
						title:"Error", 
						text:"Inventory not yet Updated!", 
						type: "error",
						allowOutsideClick: false
					}).then(function () {
						location.reload();
					});	
				}
		return true;
	});
	}
}]);
		return true;
	}
}
var edit_val;


</script>

<div class="container">
<div class="row">			
<div class="col-md-12">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
<form action="" name="edit_inventory" id="edit_inventory" method="post" enctype="multipart/form-data">

		<div class="wizard-header"><h3 class="wizard-title"> Edit <?php echo $sku_id; ?> - (Addon) </h3></div>  

		<div class="wizard-navigation">
			<ul>
				<li><a href="#edit_step-11" data-toggle="tab">General</a></li>
				<li><a href="#edit_step-71" data-toggle="tab">Highlights</a></li>
				<li><a href="#edit_step-61" data-toggle="tab">Images</a></li>
			</ul>
		</div>
		
			<input name="edit_product_id" type="hidden" id="edit_product_id" value="<?php echo $product_id;?>">
			<input name="inventory_type" type="hidden" id="inventory_type" value="<?php echo $inventory_type;?>">
			<input name="edit_inventory_id" id="edit_inventory_id" type="hidden"  value="<?php echo $inventory_id;?>"/>

			<input name="previous_largeimage1" id="previous_largeimage1" type="hidden" value="<?php echo $inventory_data_arr["largeimage"];?>"/>
			<input name="previous_image" id="previous_image" type="hidden" value="<?php echo $inventory_data_arr["common_image"];?>"/>
			<input name="previous_image1" id="previous_image1" type="hidden" value="<?php echo $inventory_data_arr["image"];?>"/>
			<input name="previous_thumbnail1" id="previous_thumbnail1" type="hidden" value="<?php echo $inventory_data_arr["thumbnail"];?>"/>
			
			<input name="previous_largeimage2" id="previous_largeimage2" type="hidden" value="<?php echo $inventory_data_arr["largeimage2"];?>"/>
			<input name="previous_image2" id="previous_image2" type="hidden" value="<?php echo $inventory_data_arr["image2"];?>"/>
			<input name="previous_thumbnail2" id="previous_thumbnail2" type="hidden" value="<?php echo $inventory_data_arr["thumbnail2"];?>"/>
			
			<input name="previous_largeimage3" id="previous_largeimage3" type="hidden" value="<?php echo $inventory_data_arr["largeimage3"];?>"/>
			<input name="previous_image3" id="previous_image3" type="hidden" value="<?php echo $inventory_data_arr["image3"];?>"/>
			<input name="previous_thumbnail3" id="previous_thumbnail3" type="hidden" value="<?php echo $inventory_data_arr["thumbnail3"];?>"/>
			
			<input name="previous_largeimage4" id="previous_largeimage4" type="hidden" value="<?php echo $inventory_data_arr["largeimage4"];?>"/>
			<input name="previous_image4" id="previous_image4" type="hidden" value="<?php echo $inventory_data_arr["image4"];?>"/>
			<input name="previous_thumbnail4" id="previous_thumbnail4" type="hidden" value="<?php echo $inventory_data_arr["thumbnail4"];?>"/>
			
			<input name="previous_largeimage5" id="previous_largeimage5" type="hidden" value="<?php echo $inventory_data_arr["largeimage5"];?>"/>
			<input name="previous_image5" id="previous_image5" type="hidden" value="<?php echo $inventory_data_arr["image5"];?>"/>
			<input name="previous_thumbnail5" id="previous_thumbnail5" type="hidden" value="<?php echo $inventory_data_arr["thumbnail5"];?>"/>
			
			
			<input name="previous_largeimage6" id="previous_largeimage6" type="hidden" value="<?php echo $inventory_data_arr["largeimage6"];?>"/>
			<input name="previous_image6" id="previous_image6" type="hidden" value="<?php echo $inventory_data_arr["image6"];?>"/>
			<input name="previous_thumbnail6" id="previous_thumbnail6" type="hidden" value="<?php echo $inventory_data_arr["thumbnail6"];?>"/>
			
			
		
			<input name="previous_largeimage1_unlink" id="previous_largeimage1_unlink" type="hidden" value="<?php echo $inventory_data_arr["largeimage"];?>"/>
			<input name="previous_image1_unlink" id="previous_image1_unlink" type="hidden" value="<?php echo $inventory_data_arr["image"];?>"/>
			<input name="previous_thumbnail1_unlink" id="previous_thumbnail1_unlink" type="hidden" value="<?php echo $inventory_data_arr["thumbnail"];?>"/>
			
			<input name="previous_largeimage2_unlink" id="previous_largeimage2_unlink" type="hidden" value="<?php echo $inventory_data_arr["largeimage2"];?>"/>
			<input name="previous_image2_unlink" id="previous_image2_unlink" type="hidden" value="<?php echo $inventory_data_arr["image2"];?>"/>
			<input name="previous_thumbnail2_unlink" id="previous_thumbnail2_unlink" type="hidden" value="<?php echo $inventory_data_arr["thumbnail2"];?>"/>
			
			<input name="previous_largeimage3_unlink" id="previous_largeimage3_unlink" type="hidden" value="<?php echo $inventory_data_arr["largeimage3"];?>"/>
			<input name="previous_image3_unlink" id="previous_image3_unlink" type="hidden" value="<?php echo $inventory_data_arr["image3"];?>"/>
			<input name="previous_thumbnail3_unlink" id="previous_thumbnail3_unlink" type="hidden" value="<?php echo $inventory_data_arr["thumbnail3"];?>"/>
			
			<input name="previous_largeimage4_unlink" id="previous_largeimage4_unlink" type="hidden" value="<?php echo $inventory_data_arr["largeimage4"];?>"/>
			<input name="previous_image4_unlink" id="previous_image4_unlink" type="hidden" value="<?php echo $inventory_data_arr["image4"];?>"/>
			<input name="previous_thumbnail4_unlink" id="previous_thumbnail4_unlink" type="hidden" value="<?php echo $inventory_data_arr["thumbnail4"];?>"/>
			
			<input name="previous_largeimage5_unlink" id="previous_largeimage5_unlink" type="hidden" value="<?php echo $inventory_data_arr["largeimage5"];?>"/>
			<input name="previous_image5_unlink" id="previous_image5_unlink" type="hidden" value="<?php echo $inventory_data_arr["image5"];?>"/>
			<input name="previous_thumbnail5_unlink" id="previous_thumbnail5_unlink" type="hidden" value="<?php echo $inventory_data_arr["thumbnail5"];?>"/>
			
			<input name="previous_largeimage6_unlink" id="previous_largeimage6_unlink" type="hidden" value="<?php echo $inventory_data_arr["largeimage6"];?>"/>
			<input name="previous_image6_unlink" id="previous_image6_unlink" type="hidden" value="<?php echo $inventory_data_arr["image6"];?>"/>
			<input name="previous_thumbnail6_unlink" id="previous_thumbnail6_unlink" type="hidden" value="<?php echo $inventory_data_arr["thumbnail6"];?>"/>
			
			
			
	<div class="tab-content">
	<div class="tab-pane" id="edit_step-11">
		<div class="row">
			<div class="col-md-12">
			    <h4 class="info-text"> Product and Stock Information</h4>
			</div>
		</div>
                
				
				<div class="row">
				<div class="col-md-5 col-md-offset-1"> 
					<div class="form-group label-floating">
						<label class="control-label">Enter Display Name on Button 1</label>
						<input name="sku_display_name_on_button_1" type="text" class="form-control"  value="<?php echo $inventory_data_arr["sku_display_name_on_button_1"];?>"/>
					</div>
				</div>
				<div class="col-md-5"> 	
				
					<div class="form-group label-floating">
						<label class="control-label">Enter Display Name on Button 2</label>
						<input name="sku_display_name_on_button_2" type="text" class="form-control"   value="<?php echo $inventory_data_arr["sku_display_name_on_button_2"];?>"/>
					</div>
				</div>
			</div>
			
			
                <div class="row">
			
			<div class="col-md-10 col-md-offset-1">
				<div class="form-group">
					<label class="control-label">
					SKU ID</label>
					<input name="edit_sku_id" id="edit_sku_id" type="text" class="form-control" value="<?php echo $inventory_data_arr["sku_id"];?>"/>
				</div>
			</div>
		</div>
                <div class="row">
			<div class="col-md-10 col-md-offset-1"> 
                            <div class="form-group label-floating">
                                    <label class="control-label">SKU Name </label>
                                    <input name="sku_name" id="sku_name" type="text" class="form-control" value="<?php echo $inventory_data_arr["sku_name"];?>" />
                            </div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="form-group">
					<label class="control-label">-Product Status-
					</label>
					<select name="edit_product_status_view" id="edit_product_status_view" class="form-control search_select">
						<option value="" selected></option>
						<option value="1" <?php if($inventory_data_arr["product_status_view"]=="1"){echo "selected";}?>>Active</option>
						<option value="0" <?php if($inventory_data_arr["product_status_view"]=="0"){echo "selected";}?>>Inactive</option>
			
					</select>
				</div>
			</div>
			
		</div>

		<div class="row">
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group">
					<label class="control-label" id="edit_max_selling_price_div">MRP (<?php echo curr_sym;?>) {MRP - Base Price + Tax Price}
					</label>
                                    <input type="hidden" id="db_edit_max_selling_price" value="<?php echo $inventory_data_arr["max_selling_price"];?>"> 
					<input name="edit_max_selling_price" id="edit_max_selling_price" type="text" class="form-control"   value="<?php echo $inventory_data_arr["max_selling_price"];?>" onKeyPress="return isNumber(event)" onblur="calculateSelling_priceDiscountFun();calculateTax_priceFun();"/>
				</div>
				
			</div>
			<div class="col-md-5"> 
				<div class="form-group">
					<label class="control-label" id="edit_selling_price_div">Web Selling Price (<?php echo curr_sym;?>) {WSP - with any discount applied}
					</label>
                                        <input type="hidden" id="db_edit_selling_price" value="<?php echo $inventory_data_arr["selling_price"];?>">
					<input name="edit_selling_price" id="edit_selling_price" type="text" class="form-control"   value="<?php echo $inventory_data_arr["selling_price"];?>" onKeyPress="return isNumber(event)" onkeyup="calculateSelling_priceDiscountFun();calculateTax_priceFun();"/>
				</div>
			</div>
            		</div>

            <!--added newly---->
            <div class="row">
			
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group">
					<label class="control-label" id="edit_cost_price_div">Cost Price (<?php echo curr_sym;?>) {the cost of the product to the company}
					</label>
                                        
					<input name="edit_cost_price" id="edit_cost_price" type="text" class="form-control"   value="<?php echo $inventory_data_arr["cost_price"];?>" onKeyPress="return isNumber(event)"/>
				</div>
				
			</div>
                
                        <div class="col-md-5"> 
				<div class="form-group">
					<label class="control-label">
					Discount Percent {% of WSP from MRP}</label>
                                        <input type="hidden" id="db_edit_selling_discount" value="<?php echo $inventory_data_arr["selling_discount"];?>">
					<input name="edit_selling_discount" id="edit_selling_discount" type="text" class="form-control"   value="<?php echo $inventory_data_arr["selling_discount"];?>" readOnly  />
				</div>
			</div>
		</div>

        
            <!--added newly---->
	
		
		
		
			<div class="row">
			
				<div class="col-md-5 col-md-offset-1"> 
					<div class="form-group">	
						<label class="control-label">Stock count
						</label>
						<input class="form-control" name="edit_stock" id="edit_stock" type="number"  value="<?php echo $inventory_data_arr["stock"];?>"  />
						
						
					</div>
				</div>	
				<div class="col-md-5"> 
					<div class="form-group">
						<label class="control-label">-Select Stock Status-
						</label>
						<select name="edit_product_status" id="edit_product_status" class="form-control search_select">
							<option value="" selected></option>
							<option value="In Stock" <?php if($inventory_data_arr["product_status"]=="In Stock"){echo "selected";}?>>In Stock</option>
							<option value="Out Of Stock" <?php if($inventory_data_arr["product_status"]=="Out Of Stock"){echo "selected";}?>>Out Of Stock</option>
							<option value="Imported Stock" <?php if($inventory_data_arr["product_status"]=="Imported Stock"){echo "selected";}?>>Imported Stock</option>
						</select>
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-5 col-md-offset-1"> 
					<div class="form-group">
						<label class="control-label">Low Stock Count (Warning Qty)
						</label>    
						<input class="form-control" name="edit_low_stock" id="edit_low_stock" type="number"  value="<?php echo $inventory_data_arr["low_stock"];?>" />
					</div>
				</div>
				<div class="col-md-5"> 
					<div class="form-group">
						<label class="control-label">Cutoff Stock Count
						</label>     
						<input class="form-control" name="edit_cutoff_stock" id="edit_cutoff_stock" type="number"   value="<?php echo $inventory_data_arr["cutoff_stock"];?>" />
					</div>
				</div>
			</div>
			
			
			
		
	</div>
	<div class="tab-pane" id="edit_step-71">
	
		<!--- prescription drug starts -->
		
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
		<div class="panel-group" id="accordion10" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPrescription">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePrescription" aria-expanded="true" aria-controls="collapsePrescription">
                        Prescription Drug
                    </a>
                </h4>
            </div>
            <div id="collapsePrescription" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPrescription">
                <div class="panel-body">
                      
					<div class="row">
						<div class="col-md-12">
							<select name="prescription_drug_vs_otc" id="prescription_drug_vs_otc" class="form-control" onchange="prescription_drug_vs_otcFun()" required>
								<option value="">-Choose-</option>
								<option value="otc" <?php if($inventory_data_arr["prescription_drug_vs_otc"]=="otc"){echo "selected";}?>>OTC</option>
								<option value="pdo_withprice" <?php if($inventory_data_arr["prescription_drug_vs_otc"]=="pdo_withprice"){echo "selected";}?>>Prescription Drug Only ( With Price )</option>
							</select>
						</div>
					</div>
					
					
				
					
						
                </div>
            </div>
        </div>
        </div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6 col-md-offset-3">

	<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        
                        Product Details
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                      
					  <div class="row">
							<div class="col-md-12"> 	
									<textarea class="form-control" name="highlight" id="highlight"></textarea>
							</div>
						</div>
						
						
                </div>
            </div>
        </div>
        </div>

			</div>
		</div>
		
		
		
		
			<div class="row">
				<div class="col-md-6 col-md-offset-3"> 
				<div class="form-group label-floating">
					<label class="control-label">Enter Consignment Actual Weight in kg</label>
					<input class="form-control" name="edit_inventory_addon_weight_in_kg" id="edit_inventory_addon_weight_in_kg" type="number" step="any" required value="<?php echo $inventory_data_arr["inventory_addon_weight_in_kg"];?>" required>
				</div>
				</div>
			</div>
			

			
	</div>
	
	
  
	
	
	
	
	
	
	
	<div class="tab-pane" id="edit_step-61">
		
		<!---common--image--->
		<div class="row">
			<div class="col-md-5 col-md-offset-3" id="inv_images_div">
			<div class="col-md-12 text-center"><b>Common (300x366)</b></div>
				<div class="col-md-12">
				<div class="picture-container">
					<div class="picture" id="edit_commonPic">
						<img src="<?php echo base_url().$inventory_data_arr["common_image"]?>" class="picture-src" id="edit_wizardPictureCommon" title=""/>
				
						<input type="file" name="edit_common_image" id="edit_wizard-picture-Common"/>
					</div>
				<h6 id="edit_wizardPictureCommon_picname">Common Image</h6>
				</div>
				</div>	
			</div>
		</div>
		<!---common--image--->
		
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
			
			<button name="add_image_inventory" id="add_image_inventory_btn" type="button" class="btn btn-success" disabled onclick="add_image_inventory_fun()">Add</button>
			
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 col-md-offset-2"  id="inv_images_div_1">	
			<div class="col-md-10 col-md-offset-1"><b>FIRST PAIR</b></div>
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic1">
					<img class="picture-src" id="edit_wizardPictureSmall1" title=""/>
		            <input type="file" name="edit_thumbnail1" id="edit_wizard-picture-small1" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["thumbnail"]?>" alt="Image Not Found" id="previous_thumbnail_view1" name="previous_thumbnail_view1" width="100"/>
					
					
					
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname1">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic1">
					<img class="picture-src" id="edit_wizardPictureBig1" title=""/>
		            <input type="file" name="edit_image1" id="edit_wizard-picture-big1" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["image"]?>" alt="Image Not Found" id="previous_image_view1" name="previous_image_view1" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname1">Big Image (420x512)</h6>
				</div>
				

				
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic1">
					<img class="picture-src" id="edit_wizardPictureLarge1" title=""/>
		            <input type="file" name="edit_largeimage1" id="edit_wizard-picture-large1" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["largeimage"]?>" alt="Image Not Found" id="previous_largeimage_view1" name="previous_largeimage_view1" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname1">Large Image (850x1036)</h6>
				</div>
				

				
			</div>
			
			</div>
			<div class="col-md-5"  id="inv_images_div_2">	
			<div class="col-md-10 col-md-offset-1"><b>SECOND PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(2)"></i></b></div>
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic2">
					<img class="picture-src" id="edit_wizardPictureSmall2" title=""/>
		            <input type="file" name="edit_thumbnail2" id="edit_wizard-picture-small2" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["thumbnail2"]?>" alt="Image Not Found" id="previous_thumbnail_view2" name="previous_thumbnail_view2" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname2">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic2">
					<img class="picture-src" id="edit_wizardPictureBig2" title=""/>
		            <input type="file" name="edit_image2" id="edit_wizard-picture-big2" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["image2"]?>" alt="Image Not Found" id="previous_image_view2" name="previous_image_view2" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname2">Big Image (420x512)</h6>
				</div>
			</div>
			
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic2">
					<img class="picture-src" id="edit_wizardPictureLarge2" title=""/>
		            <input type="file" name="edit_largeimage2" id="edit_wizard-picture-large2" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["largeimage2"]?>" alt="Image Not Found" id="previous_largeimage_view2" name="previous_largeimage_view2" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname2">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 col-md-offset-2" id="inv_images_div_3">
<div class="col-md-10 col-md-offset-1"><b>THIRD PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(3)"></i></b></div>			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic3">
					<img class="picture-src" id="edit_wizardPictureSmall3" title=""/>
		            <input type="file" name="edit_thumbnail3" id="edit_wizard-picture-small3" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["thumbnail3"]?>" alt="Image Not Found" id="previous_thumbnail_view3" name="previous_thumbnail_view3" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname3">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic3">
					<img class="picture-src" id="edit_wizardPictureBig3" title=""/>
		            <input type="file" name="edit_image3" id="edit_wizard-picture-big3" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["image3"]?>" alt="Image Not Found" id="previous_image_view3" name="previous_image_view3" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname3">Big Image (420x512)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic3">
					<img class="picture-src" id="edit_wizardPictureLarge3" title=""/>
		            <input type="file" name="edit_largeimage3" id="edit_wizard-picture-large3" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["largeimage3"]?>" alt="Image Not Found" id="previous_largeimage_view3" name="previous_largeimage_view3" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname3">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
			<div class="col-md-5" id="inv_images_div_4">	
			<div class="col-md-10 col-md-offset-1"><b>FOURTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(4)"></i></b></div>
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic4">
					<img class="picture-src" id="edit_wizardPictureSmall4" title=""/>
		            <input type="file" name="edit_thumbnail4" id="edit_wizard-picture-small4" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["thumbnail4"]?>" alt="Image Not Found" id="previous_thumbnail_view4" name="previous_thumbnail_view4" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname4">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic4">
					<img class="picture-src" id="edit_wizardPictureBig4" title=""/>
		            <input type="file" name="edit_image4" id="edit_wizard-picture-big4" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["image4"]?>" alt="Image Not Found" id="previous_image_view4" name="previous_image_view4" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname4">Big Image (420x512)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic4">
					<img class="picture-src" id="edit_wizardPictureLarge4" title=""/>
		            <input type="file" name="edit_largeimage4" id="edit_wizard-picture-large4" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["largeimage4"]?>" alt="Image Not Found" id="previous_largeimage_view4" name="previous_largeimage_view4" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname4">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 col-md-offset-2" id="inv_images_div_5">
<div class="col-md-10 col-md-offset-1"><b>FIFTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(5)"></i></b></div>			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic5">
					<img class="picture-src" id="edit_wizardPictureSmall5" title=""/>
		            <input type="file" name="edit_thumbnail5" id="edit_wizard-picture-small5" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["thumbnail5"]?>" alt="Image Not Found" id="previous_thumbnail_view5" name="previous_thumbnail_view5" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname5">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic5">
					<img class="picture-src" id="edit_wizardPictureBig5" title=""/>
		            <input type="file" name="edit_image5" id="edit_wizard-picture-big5" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["image5"]?>" alt="Image Not Found" id="previous_image_view5" name="previous_image_view5" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname5">Big Image (420x512)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic5">
					<img class="picture-src" id="edit_wizardPictureLarge5" title=""/>
		            <input type="file" name="edit_largeimage5" id="edit_wizard-picture-large5" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["largeimage5"]?>" alt="Image Not Found" id="previous_largeimage_view5" name="previous_largeimage_view5" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname5">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
			
			
			
			
			<div class="col-md-5" id="inv_images_div_6">
<div class="col-md-10 col-md-offset-1"><b>SIXTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(6)"></i></b></div>			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic6">
					<img class="picture-src" id="edit_wizardPictureSmall6" title=""/>
		            <input type="file" name="edit_thumbnail6" id="edit_wizard-picture-small6" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["thumbnail6"]?>" alt="Image Not Found" id="previous_thumbnail_view6" name="previous_thumbnail_view6" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname6">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic6">
					<img class="picture-src" id="edit_wizardPictureBig6" title=""/>
		            <input type="file" name="edit_image6" id="edit_wizard-picture-big6" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["image6"]?>" alt="Image Not Found" id="previous_image_view6" name="previous_image_view6" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname6">Big Image (420x512)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic6">
					<img class="picture-src" id="edit_wizardPictureLarge6" title=""/>
		            <input type="file" name="edit_largeimage6" id="edit_wizard-picture-large6" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["largeimage6"]?>" alt="Image Not Found" id="previous_largeimage_view6" name="previous_largeimage_view6" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname6">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
			
			
			
			
		</div>
	</div>
	
	</div>
	<div class="wizard-footer">
		<div class="pull-right">
			<input type='button' class='btn btn-next btn-fill btn-success btn-wd' name='next' value='Next'  onclick="enterCurrentTabFun()"/>
			<input type='button' class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='Finish' id="edit_inventory_finish_btn" onClick="edit_filter_details_form_valid()"/>
		</div>
		<div class="pull-left">
			<input type='button' class='btn btn-previous btn-fill btn-default btn-wd' id="previous_button" name='previous' value='Previous' />
			<input type='button' class='btn btn-previouspage btn-fill btn-default btn-wd' id="back" value='Previous' />
		</div>
		<div class="clearfix">
		</div>
	</div>	
</form>

<?php
		if($inventory_data_arr["image"]==""){ $total_no_of_images=0;}
		else if($inventory_data_arr["image2"]==""){ $total_no_of_images=1;}
		else if($inventory_data_arr["image3"]==""){ $total_no_of_images=2;}
		else if($inventory_data_arr["image4"]==""){ $total_no_of_images=3;}
		else if($inventory_data_arr["image5"]==""){ $total_no_of_images=4;}
		else if($inventory_data_arr["image6"]==""){ $total_no_of_images=5;}
		else{$total_no_of_images=6;}
	?>	
	<script>
		function hide_inv_imagesFun(div_index_id){
			
			document.getElementById("previous_largeimage"+div_index_id).value="";
			document.getElementById("previous_image"+div_index_id).value="";
			document.getElementById("previous_thumbnail"+div_index_id).value="";
			
			document.getElementById("edit_wizard-picture-small"+div_index_id).value="";
			document.getElementById("edit_wizard-picture-big"+div_index_id).value="";
			document.getElementById("edit_wizard-picture-large"+div_index_id).value="";
			
			
			document.getElementById("edit_wizardPictureSmall"+div_index_id).removeAttribute("src");
			document.getElementById("edit_wizardPictureBig"+div_index_id).removeAttribute("src");
			document.getElementById("edit_wizardPictureLarge"+div_index_id).removeAttribute("src");
			
			document.getElementById("edit_wizardPictureSmall_picname"+div_index_id).innerHTML="Small Image (100x122)";
			document.getElementById("edit_wizardPictureBig_picname"+div_index_id).innerHTML="Big Image (420x512)";
			document.getElementById("edit_wizardPictureLarge_picname"+div_index_id).innerHTML="Large Image (850x1036)";
			
			document.getElementById("inv_images_div_"+div_index_id).style.display="none";
			enableDisabledAddInventoryButtonFun();
		}
		function enableDisabledAddInventoryButtonFun(){
			small_image_count=0;
			big_image_count=0;
			large_image_count=0;
			for(i=1;i<=5;i++){
				if(document.getElementById("inv_images_div_"+i).style.display==""){
					if(document.getElementById("edit_wizard-picture-small"+i).value!="" || document.getElementById("previous_thumbnail_view"+i).value!=""){
						small_image_count++;
					}
					if(document.getElementById("edit_wizard-picture-big"+i).value!="" || document.getElementById("previous_image_view"+i).value!=""){
						big_image_count++;
					}
					if(document.getElementById("edit_wizard-picture-large"+i).value!="" || document.getElementById("previous_largeimage_view"+i).value!=""){
						large_image_count++;
					}
				}
			}
			
			if(small_image_count==0 && big_image_count==0 && large_image_count==0){
				document.getElementById("add_image_inventory_btn").disabled=true;
			}
			else{
				if(small_image_count==big_image_count && big_image_count==large_image_count){
					document.getElementById("add_image_inventory_btn").disabled=false;
					document.getElementById("edit_inventory_finish_btn").disabled=false;
				}
				else{
					document.getElementById("add_image_inventory_btn").disabled=true;
					document.getElementById("edit_inventory_finish_btn").disabled=true;
					
				}
			}
		}
		function initializeInventoryImageDivFun(){
			for(i=<?php echo ($total_no_of_images+1);?>;i<=6;i++){
				document.getElementById("inv_images_div_"+i).style.display="none";
			}
		}
		function add_image_inventory_fun(){
			for(i=2;i<=6;i++){
				if(document.getElementById("inv_images_div_"+i).style.display=="none"){
					document.getElementById("inv_images_div_"+i).style.display="";
					document.getElementById("add_image_inventory_btn").disabled=true;
					break;
				}
			}
		}
		initializeInventoryImageDivFun();
		enableDisabledAddInventoryButtonFun();
		
	</script>
	
</div>
</div> <!-- wizard container -->
</div>
</div>
 
</div>
<div class="footer">
<form name="back_to_inventory" id="back_to_inventory" method="post">
		<input type="hidden" value="<?php echo $pcat_id; ?>" name="parent_category">
		<input type="hidden" value="<?php echo $cat_id; ?>" name="categories">
		<input type="hidden" value="<?php echo $subcat_id; ?>" name="subcategories">
		<input type="hidden" value="<?php echo $brand_id; ?>" name="brands">
		<input type="hidden" value="<?php echo $product_id; ?>" name="products">
		<input value="view" type="hidden" name="create_editview">
</form>
</div>
</div>
<?php		

//$highlight=str_replace("\n", '', addslashes($inventory_data_arr["highlight"]));



$highlight=$inventory_data_arr["highlight"];
$highlight1=str_replace("\n", '', $highlight);

$string = trim(preg_replace('/\s+/', ' ', $highlight1));

?>

 
<script  type="text/javascript" src="<?php echo base_url();?>assets/richte/editor.js" ></script>
<script>
function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}
$(document).ready(function() {
	
	
	
	
	var textarea = $("#highlight");
	textarea.Editor();
	str='<?php echo $string; ?>';
	data=decodeHtml(str);
	$('#highlight').siblings("div").children("div.Editor-editor").html(data);
	
	
 });

 function enterCurrentTabFun(){
	$(".tab-content .tab-pane.active").each(function(){
		if($(this).attr("id")=="edit_step-21"){
			vendor_id=$("#edit_vendors").val();
			if($("#edit_logistics_parcel_category").val()!=null){
				logistics_parcel_category_in=$("#edit_logistics_parcel_category").val().join("-");
				
				$.ajax({
					url:"<?php echo base_url()?>admin/Catalogue/check_selection_in_all_parcel_categories",
					type:"post",
					data:"vendor_id="+vendor_id+"&logistics_parcel_category_in="+logistics_parcel_category_in,
					success:function(data){
						if(data=="no"){
							swal({
								title:"Info", 
								text:"Please select atleast one parcel category from logisitcs", 
								type: "info"
						}).then(function () {
							
						});
							$("#previous_button").click();
						}
						else{
							
						}
					}
				});
			}
	
		}	
		
	});
 }
 
 function calculateSelling_priceDiscountFun(){
 
        edit_selling_price=$("#edit_selling_price").val();
        edit_max_selling_price=$("#edit_max_selling_price").val();
        
        if(edit_selling_price=="" || edit_max_selling_price==""){
            $("#edit_selling_discount").val("");
	}
        if(edit_selling_price!="" && edit_max_selling_price!=""){
            if(parseFloat(edit_max_selling_price)>=parseFloat(edit_selling_price)){
                wsp_dis=Math.round(((parseFloat(edit_max_selling_price)-parseFloat(edit_selling_price))/parseFloat(edit_max_selling_price))*100,2);
                $("#edit_selling_discount").val(wsp_dis);
            }else{
                swal({
                        title:"Error", 
                        text:"MRP should be greater or equal to WSP", 
                        type: "error",
                        allowOutsideClick: false
                }).then(function () {
                        db_val_dis=$("#db_edit_selling_discount").val(); 
                        db_val_price=$("#db_edit_selling_price").val(); 
                        db_val=$("#db_edit_max_selling_price").val();
                        
                        $("#edit_max_selling_price").val(db_val);
                        $("#edit_selling_price").val(db_val_price);
                        $("#edit_selling_discount").val(db_val_dis);
                        
                });
               
            }
            //alert(wsp_dis);
	}
 }
 function calculateTax_priceFun(){
	 
        tax=$("#edit_tax").val();
        msp=$("#edit_max_selling_price").val();
        selling_price=$("#edit_selling_price").val();
        
        if(selling_price!="" && tax!=""){
            
            //taxable_price=(parseFloat(msp)-parseFloat(tax_percent_price));
            taxable_price=(parseFloat(selling_price)/(1+parseFloat(tax)/100)).toFixed(2);
            tax_percent_price=(parseFloat(selling_price)-parseFloat(taxable_price)).toFixed(2);
            //alert(taxable_price);
            $("#tax_percent_price").val(tax_percent_price);
            $("#taxable_price").val(taxable_price);
            
            $("#tax_percent_price_div").removeAttr("class");
            $("#tax_percent_price_div").attr("class","form-group label-floating is-empty is-focused");
            $("#taxable_price_div").removeAttr("class");
            $("#taxable_price_div").attr("class","form-group label-floating is-empty is-focused");
        }
         /* calculate tax price and taxable vale */
 }
 function calculateSelling_priceFun(){
	 edit_base_price=$("#edit_base_price").val();
	 edit_tax=$("#edit_tax").val();
	 if(edit_base_price!="" && edit_tax!=""){
		edit_selling_price=Math.round(parseFloat(edit_base_price)+((edit_tax/100)*parseFloat(edit_base_price)));
		$("#edit_max_selling_price").val(edit_selling_price);
	 }
	 if(edit_base_price=="" || edit_tax==""){
		 $("#edit_max_selling_price").val("");
	 }
	 
 }

 </script>
 
 <script>
				function calculateTaxPercentFun(){
					var CGST_value=$("#CGST").val().trim();
					var IGST_value=$("#IGST").val().trim();
					var SGST_value=$("#SGST").val().trim();
					
					if(CGST_value==""){CGST_value=0;}
					if(IGST_value==""){IGST_value=0;}
					if(SGST_value==""){SGST_value=0;}
					
					$("#edit_tax").val(parseFloat(CGST_value)+parseFloat(IGST_value)+parseFloat(SGST_value));
					calculateSelling_priceDiscountFun();
					calculateTax_priceFun();
				}
			</script>
			<script>
				function deleteDownloadsLinkFun(delete_file_n_upload){
					
					$("#"+delete_file_n_upload).val(delete_file_n_upload);
					$("#"+delete_file_n_upload+"_span").css({"display":"none"});
				}
			</script>
			
				
<script>



function add_items(){
	var len = $('.item').length; 

let template = `<div class="item">
<div class="row">
<div class=' col-md-8 col-md-offset-1'>
	<label class="control-label">Content</label>
	<input type="text" class="form-control" name="position_text[`+(parseInt(len)+1)+`]" placeholder="" />
	</div>
</div>
<div class="row">
	<div class="col-md-8 col-md-offset-1"> 
		<div class="form-group">
		<div class="col-md-5 col-md-offset-1">Do you have ?</div>
		<div class="col-md-3">
			<label class="radio-inline"><input name="position_uhave[`+(parseInt(len)+1)+`]" value="yes" type="radio" >Yes</label>
		</div>
		<div class="col-md-3">
			<label class="radio-inline"><input name="position_uhave[`+(parseInt(len)+1)+`]" value="no" type="radio">No</label>
		</div>

	</div>
</div>

<div class="row">
	<div class="col-md-8 col-md-offset-1"> 
		<div class="form-group">
		<div class="col-md-5 col-md-offset-1">Do others have ?</div>
		<div class="col-md-3">
			<label class="radio-inline"><input name="position_others_have[`+(parseInt(len)+1)+`]" value="yes" type="radio" >Yes</label>
		</div>
		<div class="col-md-3">
			<label class="radio-inline"><input name="position_others_have[`+(parseInt(len)+1)+`]" value="no" type="radio">No</label>
		</div>

	</div>
</div>
<div class="col-md-8 col-md-offset-1"><button class="btn btn-sm btn-danger pull-right" class="remove" >Remove</a>
</div>
</div>`; 

$("#items").append(template);

}
$(document).ready(()=>{	
    $("body").on("click", ".remove", (e)=>{
        $(e.target).parents(".item").remove();
    })
});


</script>
			
</body>
</html>

