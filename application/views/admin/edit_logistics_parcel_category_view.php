<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:60%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<script>

function showDivPrevious(){
	 var form = document.getElementById('search_for_logistics_parcel_category');		
	
	form.action='<?php echo base_url()."admin/Catalogue/logistics_parcel_category"; ?>';
	form.submit();
}
</script>
<form name="search_for_logistics_parcel_category" id="search_for_logistics_parcel_category" method="post">
		<input value="<?php echo $get_logistics_parcel_category_data["vendor_id"]; ?>" type="hidden" name="vendor_id"/>
		<input value="<?php echo $get_logistics_parcel_category_data["logistics_id"]; ?>" type="hidden" name="logistics_id"/>
		<input value="view" type="hidden" name="create_editview">
	</form>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row-fluid" id="editDiv3">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
	<form name="edit_logistics_parcel_category" id="edit_logistics_parcel_category" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_vendors: {
					required: true,
				},
				edit_logistics: {
					required: true,
				},
				edit_parcel_type: {
					required: true,
				},
				edit_parcel_type_description: {
					required: true,
				}
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Logistics Parcel Category</h3>
		</div>	
		</div> 
		
		<div class="tab-content">				
		<input type="hidden" name="edit_logistics_parcel_category_id" id="edit_logistics_parcel_category_id" value="<?php echo $get_logistics_parcel_category_data["logistics_parcel_category_id"];?>">
		
		<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Vendor-</label>
					<select name="edit_vendors" id="edit_vendors" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($vendors as $vendors_value) {  ?>
						<option value="<?php echo $vendors_value->vendor_id; ?>" <?php if($get_logistics_parcel_category_data["vendor_id"]==$vendors_value->vendor_id){echo "selected";} ?>><?php echo $vendors_value->name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Logistics Name-</label>
					<select name="edit_logistics" id="edit_logistics" class="form-control" disabled>
						<option value="<?php echo $get_logistics_parcel_category_data["logistics_id"];?>"><?php echo $get_logistics_parcel_category_data["logistics_name"];?></option>
					</select>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Enter Parcel Type</label>
					<input id="edit_parcel_type" name="edit_parcel_type" type="text" class="form-control" value="<?php echo $get_logistics_parcel_category_data["parcel_type"];?>"/>
					<input id="edit_parcel_type_default" name="edit_parcel_type_default" type="hidden" class="form-control" value="<?php echo $get_logistics_parcel_category_data["parcel_type"];?>"/>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Enter Description of Parcel Type</label>
					<textarea id="edit_parcel_type_description" name="edit_parcel_type_description" class="form-control" ><?php echo $get_logistics_parcel_category_data["parcel_type_description"];?></textarea>
				</div>
				</div>
			</div>
			
				<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group">
					<div class="col-md-6">Make Default Logistics delivery Mode</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="edit_default_logistics" id="edit_default_logistics1" value="1" type="radio" <?php if($get_logistics_parcel_category_data["default_parcel_category"]=="1"){echo "checked";}?> >View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="edit_default_logistics" id="edit_default_logistics2" value="0" type="radio" <?php if($get_logistics_parcel_category_data["default_parcel_category"]=="0"){echo "checked";}?>>Hide</label>
					</div>
				
					</div>
				</div>
			</div>			
			<div class="row">
				<div class="col-md-8 col-md-offset-3">
				<div class="form-group">
					<button class="btn btn-info btn-sm" id="submit-data" disabled="" type="submit">Submit</button>
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
				</div>	
				</div>
			</div>		

		</div>		
	</form>

</div>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>
</body>
</html>
<script>

$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_logistics_parcel_category'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_logistics_parcel_category"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_logistics_parcel_category'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select' || type=='textarea') {
            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});
});

function form_validation_edit()
{
	var vendors = $('input[name="edit_vendors"]').val();
	var logistics = $('input[name="edit_logistics"]').val();
	var parcel_type = $('input[name="edit_parcel_type"]').val().trim();
	var parcel_type_description = $('textarea[name="edit_parcel_type_description"]').val().trim();	
	

	   var err = 0;
		if((parcel_type=='') || (parcel_type_description=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#edit_logistics_parcel_category');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_logistics_parcel_category/"?>',
				type: 'POST',
				data: $('#edit_logistics_parcel_category').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {	
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					edit_parcel_type_default=document.getElementById("edit_parcel_type_default").value;
					document.getElementById("edit_parcel_type").value=edit_parcel_type_default;
					document.getElementById("edit_parcel_type").focus();
					
				});
				  }
				if(data==1)
				{		
					 					 
					swal({
						title:"Success", 
						text:"Logistics Parcel Category is successfully updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					}); 
				}
				if(data==0)
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)	
				}
				
			});
}
}]);			
			}
	
	return false;
}


</script>
