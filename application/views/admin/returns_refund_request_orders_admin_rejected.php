<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Returns Refund Request Orders - Admin Rejected</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<style>
.form-group{
	margin-bottom:5px;
	margin-top:0;
	padding-bottom:0;
}
</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   var table = $('#returns_refund_request_orders_table_admin_rejected').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Returns/returns_refund_request_orders_admin_rejected_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#returns_refund_request_orders_table_admin_rejected_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("refund_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'40%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});	
$(document).ready(function(){
	var returns_refund_orders_list_checked=0;
	$("input[name='returns_refund_orders_list']").each(function(){
		if($(this).is(":checked")){
			returns_refund_orders_list_checked++;
		}
	})
	/*if(returns_refund_orders_list_checked==0){
		document.getElementById("refund_info_div").style.display="none";
	}*/
});

function open_return_refund_accept_fun(order_item_id,return_order_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/returns_refund_request_orders_admin_rejected_accept",
		type:"POST",
		data:"order_item_id="+order_item_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			//alert(data);
			$("#update_return_refund_accept_form").html(data);
			$("#return_refund_accept_modal").modal();
		}
	})
}
function open_return_refund_reject_fun(order_item_id,return_order_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/returns_refund_request_orders_admin_rejected_reject",
		type:"POST",
		data:"order_item_id="+order_item_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			//alert(data);
			$("#update_return_refund_reject_form").html(data);
			$("#return_refund_reject_modal").modal();
		}
	})
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="page-header"><h4 class="text-center">Refund Request Orders - Admin Rejected <span class="badge" id="refund_count"></span></h4></div>
	<table id="returns_refund_request_orders_table_admin_rejected" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="text-primary small bold">Return Summary</th>
				<th class="text-primary small bold">Return Reason</th>
				<th class="text-primary small bold">Actions</th>
			</tr>
		</thead>
	</table>
</div>

<!------------------------------------------->
<!--- accept model things starts -------->
<script>
$(document).ready(function(){
	$("#update_return_refund_accept_form").on('submit',(function(e) {
		if($("input[name='product_price_with_offer_percentage_chk']").is(":checked") && $("input[name='shipping_charge_with_offer_percentage_chk']").is(":checked")){
			err=0;
		}else{
			err=1;
			swal('Please tick product price and shipping price');
			return false;
		}
		
		var sms=$("input[name=sms]").is(':checked');
		var email=$("input[name=email]").is(':checked');
		
		if(sms==true || email==true){		
			swal({
				html: '<h4>Processing...</h4>',
			});	
			swal.showLoading();
		}
		e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>admin/Returns/update_return_refund_accept",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					if(data){
					$('#return_refund_accept_modal').modal('hide');
						swal({
							title:"success!", 
							text:"Successfully Updated", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#update_return_refund_accept_form").trigger("reset");
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
					
				}	        
		   });
	}));
	
});
function divFunction_order_rep(){
    $("#update_return_refund_accept_form").trigger("reset");
		location.reload();
}
</script>



<!--- modal accept starts----------------->


<div class="modal" id="return_refund_accept_modal" data-backdrop="static" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		 <script>
			function update_product_shipping_offerFun(order_item_id){
				quantity_accept=parseFloat(document.getElementById("quantity_accept_"+order_item_id).value);
				product_price=parseFloat(document.getElementById("product_price_"+order_item_id).value);
				//shipping_charge=parseFloat(document.getElementById("shipping_charge_"+order_item_id).value);
				shipping_charge_org=parseFloat(document.getElementById("shipping_charge_purchased_"+order_item_id).value);
				purchased_quantity=parseFloat(document.getElementById("purchased_quantity_"+order_item_id).value);
				var ord_addon_products_status=parseFloat(document.getElementById('ord_addon_products_status_'+order_item_id).value) || 0;// 0 or 1
				
				shipping_charge=parseFloat(parseInt(shipping_charge_org)/parseInt(purchased_quantity)).toFixed(2);
				document.getElementById("product_price_with_offer_org_"+order_item_id).value=quantity_accept*product_price;
				document.getElementById("shipping_charge_with_offer_org_"+order_item_id).value=quantity_accept*shipping_charge;
				
				document.getElementById("product_price_with_offer_"+order_item_id).value=quantity_accept*product_price;
				document.getElementById("shipping_charge_with_offer_"+order_item_id).value=quantity_accept*shipping_charge;
				
				if(document.getElementById("product_price_with_offer_percentage_chk_"+order_item_id).checked){
					product_price_with_offer_percentage=document.getElementById("product_price_with_offer_percentage_"+order_item_id).value;
				}
				else{
					product_price_with_offer_percentage=0;
				}
				product_price_with_offer_org=document.getElementById("product_price_with_offer_org_"+order_item_id).value;
				
				document.getElementById("product_price_with_offer_"+order_item_id).value=parseFloat(product_price_with_offer_org-(product_price_with_offer_org*(product_price_with_offer_percentage/100))).toFixed(2);
				
				document.getElementById("product_price_with_offer_span_"+order_item_id).innerHTML=parseFloat(product_price_with_offer_org-(product_price_with_offer_org*(product_price_with_offer_percentage/100))).toFixed(2);
				////////////////////
				if(document.getElementById("shipping_charge_with_offer_percentage_chk_"+order_item_id).checked){
					shipping_charge_with_offer_percentage=document.getElementById("shipping_charge_with_offer_percentage_"+order_item_id).value;
				}
				else{
					shipping_charge_with_offer_percentage=0;
				}
				////////////////////////
				
				shipping_charge_with_offer_org=document.getElementById("shipping_charge_with_offer_org_"+order_item_id).value;
				
				document.getElementById("shipping_charge_with_offer_"+order_item_id).value=Math.round(shipping_charge_with_offer_org-(shipping_charge_with_offer_org*(shipping_charge_with_offer_percentage/100)));
					
				document.getElementById("shipping_charge_with_offer_span_"+order_item_id).innerHTML=Math.round(shipping_charge_with_offer_org-(shipping_charge_with_offer_org*(shipping_charge_with_offer_percentage/100)));
				
			}
			function product_price_with_offer_percentageFun(obj,order_item_id){
				if(obj.checked){
					document.getElementById("product_price_with_offer_percentage_"+order_item_id).style.display="block";
				}
				else{
					document.getElementById("product_price_with_offer_percentage_"+order_item_id).style.display="none";
				}
				
			}
			
			function shipping_charge_with_offer_percentageFun(obj,order_item_id){
				if(obj.checked){
					document.getElementById("shipping_charge_with_offer_percentage_"+order_item_id).style.display="block";
				}
				else{
					document.getElementById("shipping_charge_with_offer_percentage_"+order_item_id).style.display="none";
				}
				
			}
			function get_product_price_with_offer_fun(value,order_item_id){
				if(value>100){
					alert('Please enter percentage 1-100');
					$("#product_price_with_offer_percentage_"+order_item_id).val(0);
					value=0;
				}
				product_price_with_offer=document.getElementById("product_price_with_offer_org_"+order_item_id).value;
				product_price_with_offer_res=parseFloat(product_price_with_offer-(product_price_with_offer*(value/100))).toFixed(2);
				document.getElementById("product_price_with_offer_"+order_item_id).value=product_price_with_offer_res;
				document.getElementById("product_price_with_offer_span_"+order_item_id).innerHTML=product_price_with_offer_res;
			}
			function get_shipping_charge_with_offer_fun(value,order_item_id){
				if(value>100){
					alert('Please enter percentage 1-100');
					$("#shipping_charge_with_offer_percentage_"+order_item_id).val(0);
					value=0;
				}
				shipping_charge_with_offer=document.getElementById("shipping_charge_with_offer_org_"+order_item_id).value;
				shipping_charge_with_offer_res=Math.round(shipping_charge_with_offer-(shipping_charge_with_offer*(value/100)));
				document.getElementById("shipping_charge_with_offer_"+order_item_id).value=shipping_charge_with_offer_res;
				document.getElementById("shipping_charge_with_offer_span_"+order_item_id).innerHTML=shipping_charge_with_offer_res;
			}
		 </script>
		  </script>
		<div class="panel panel-success">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                  Accept Refund Request
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right" onClick="divFunction_order_rep()"></span></span>
               </div>
               <div aria-expanded="true">
                  <div class="panel-body">
		 <form id="update_return_refund_accept_form" method="post" enctype="multipart/form-data" class="form-horizontal">
           
			</form>
			</div>

               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!--- modal accept ends----------------->


<!--- accept model things ends -------->


<!--- reject model things starts -------->
<script>

$(document).ready(function(){
	$("#return_refund_accept_modal").on('shown.bs.modal', function () {
		
			order_item_id=$("#order_item_id_accept").val();
			//alert(order_item_id);

			var promotion_available=parseInt(document.getElementById('promotion_available').value);
			var desired_quantity=parseInt(document.getElementById("quantity_accept_"+order_item_id).value);
			
			
			if(promotion_available==1){	
				refund_promo_calculation_for_reject(desired_quantity,order_item_id);
			}
			
    });
	
	$("#return_refund_reject_modal").on('shown.bs.modal', function () {
			order_item_id=$("#order_item_id_reject").val();
			//alert(order_item_id);

			var promotion_available=parseInt(document.getElementById('promotion_available').value);
			var desired_quantity=parseInt(document.getElementById("quantity_accept").value);
			
			if(promotion_available==1){	
				refund_promo_calculation_for_reject(desired_quantity,order_item_id);
			}
			
    });
	
	$("#update_return_refund_reject_form").on('submit',(function(e) {
		var sms=$("input[name=sms]").is(':checked');
		var email=$("input[name=email]").is(':checked');
		
		if(sms==true || email==true){		
			swal({
				html: '<h4>Processing...</h4>',
			});	
			swal.showLoading();
		}
		e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>admin/Returns/return_refund_reject",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					if(data){
						$('#return_refund_reject_modal').modal('hide');
						swal({
							title:"success!", 
							text:"Successfully Updated", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#update_return_refund_reject_form").trigger("reset");
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
					
				}	        
		   });
	}));
	

});
function divFunction_order_rep_reject(){
   $("#update_return_refund_reject_form").trigger("reset");
		location.reload();
}
</script>


<!--- modal reject starts----------------->

<div class="modal" id="return_refund_reject_modal" data-backdrop="static" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		<div class="panel panel-danger">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                   Reject Refund Request
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right" onClick="divFunction_order_rep_reject()"></span></span>
               </div>
               <div aria-expanded="true" class="">
                  <div class="panel-body">
		<form id="update_return_refund_reject_form" method="post" enctype="multipart/form-data" class="form-horizontal">
          
		</form>
		</div>

               </div>
            </div>
		<script type="text/javascript">
			
			function refund_promo_calculation_for_reject(desired_quantity,order_item_id){
                var curr_sym='<?php echo curr_sym; ?>';
			$("#product_price_without_promo_"+order_item_id).hide();
			
			var desired_quantity_refund=desired_quantity;
			//get the onchange quantity as a desired quantity
			var ordered_quantity=parseInt(document.getElementById('ordered_quantity').value);
			var order_item_invoice_discount_value=parseFloat(document.getElementById('order_item_invoice_discount_value_'+order_item_id).value) || 0;//"falsey" value to 0
			var order_item_invoice_discount_value_each=parseFloat(document.getElementById('order_item_invoice_discount_value_each_'+order_item_id).value) || 0;//"falsey" value to 0
				
			var requested_unitsmarker=parseInt(document.getElementById('quantity_refund').value);
			
			var promotion_available=parseInt(document.getElementById('promotion_available').value);
			var promotion_minimum_quantity=parseInt(document.getElementById('promotion_minimum_quantity').value);
			var promotion_quote=document.getElementById('promotion_quote').value
			var promotion_default_discount_promo=document.getElementById('promotion_default_discount_promo').value
			
			var promotion_item=document.getElementById('promotion_item').value
			var promotion_item_num=document.getElementById('promotion_item_num').value
			var promotion_cashback=parseInt(document.getElementById('promotion_cashback').value);
			var promotion_item_multiplier=parseInt(document.getElementById('promotion_item_multiplier').value);
			var default_discount=parseInt(document.getElementById('default_discount').value);
			var promotion_discount=parseInt(document.getElementById('promotion_discount').value);

			var cash_back_value=parseInt(document.getElementById('cash_back_value').value);
			var status_of_refund_for_cashback_on_sku=$("#status_of_refund_for_cashback_on_sku_"+order_item_id).val();
			var individual_price_of_product_with_promotion=parseFloat(document.getElementById('individual_price_of_product_with_promotion').value);
			var individual_price_of_product_without_promotion=parseInt(document.getElementById('individual_price_of_product_without_promotion').value);
			var total_price_of_product_with_promotion=parseFloat(document.getElementById('total_price_of_product_with_promotion').value);
			var total_price_of_product_without_promotion=parseInt(document.getElementById('total_price_of_product_without_promotion').value) || 0;
			
			var quotient=0;
			var modulo=0;
			var promo_str="";
			var total_amount=0;
		
		if(promotion_available==1){

			if((promotion_discount!=default_discount) && (promotion_discount>0)){
				promo_discount_available=1;
			}else{
				promo_discount_available=0;
			}
			
			modulo=parseInt(desired_quantity_refund%promotion_minimum_quantity);//remainder
			quotient=Math.floor(parseInt(desired_quantity_refund/promotion_minimum_quantity));//answer


			var number_of_free_items=0;
			if(promotion_item_num!=''){
				
				var promo_item_arr = promotion_item_num.split(',');
				var free_item_count=0;
				number_of_free_items=promo_item_arr.length;
				for(h=0;h<number_of_free_items;h++){
					free_item_count+=parseInt(promo_item_arr[h]);
				}
			}
			var promotion_surprise_gift_type=document.getElementById('promotion_surprise_gift_type').value;
			
			if(promotion_surprise_gift_type=='Qty'){
				remain=parseInt(ordered_quantity-desired_quantity_refund);
				
				if(remain<promotion_minimum_quantity){
					return_surprise_gift=1;
				}else{
					return_surprise_gift=0;
				}
			}
			var promotion_surprise_gift=document.getElementById('promotion_surprise_gift').value;

			if(promotion_surprise_gift_type=='<?php echo curr_code; ?>'){
				if(ordered_quantity==desired_quantity_refund){
					return_surprise_gift_amount=1;
				}else{
					return_surprise_gift_amount=0;
				}
			}
			$("#details_of_refund_request_"+order_item_id).show();
			
			if(desired_quantity_refund >= promotion_minimum_quantity){
				
				if(promo_discount_available != 1){
					
					if(quotient>0){
						
						indi_price_with_promo=parseInt(individual_price_of_product_with_promotion/quotient);
						
						if(number_of_free_items>0){
							count_quotient=(quotient)*promotion_minimum_quantity;
						}else{
							count_quotient=quotient*promotion_minimum_quantity;
						}
						
						//alert(quotient);
						//alert(promotion_minimum_quantity);

						total_price_with_promo=total_price_of_product_with_promotion;
						
						promo_str+='<tr><td>'+count_quotient+' * '+individual_price_of_product_with_promotion+'<br><small style="color:green;"><b><i>'+promotion_quote+' for quantity ('+count_quotient+')</i></b></small></td><td> '+curr_sym+total_price_with_promo+'</td></tr>';
						total_amount+=parseFloat(total_price_with_promo);

						if(promotion_cashback>0){
							if(status_of_refund_for_cashback_on_sku=="refunded"){
								cash_back_value_with_quotient=cash_back_value;
								promo_str+='<tr><td><span style="color:red;">Cashback value for quantity ('+quotient+')</span></td><td> <span style="color:red;"> -'+curr_sym+cash_back_value_with_quotient+'</span></td></tr>';

								total_amount-=parseFloat(cash_back_value_with_quotient);
							}
						}
					}
					
					if(number_of_free_items>0){
						
						free_skus_from_inventory=$.parseJSON($('#free_inventory_available').val());
						
						free_str="";
						
						//alert(free_skus_from_inventory);
						if(free_skus_from_inventory!=null){
							if(free_skus_from_inventory.length>0){
								
								for(y=0;y<free_skus_from_inventory.length;y++){
									
									free_str+=free_skus_from_inventory[y].str+" <small>("+quotient*parseInt(free_skus_from_inventory[y].count)+")</small> <br>";
								}
							}
							
						}

						promo_str+='<tr><td colspan="2"><b><h6 style="color:blue;"></b> Free Items to be returned with the requested Quantity of product</h6>'+free_str+'</td><tr>';
					}
					if(promotion_surprise_gift_type=='Qty'){
						if(return_surprise_gift==1){
						
							surprise_free_skus_from_inventory=$.parseJSON($('#surprise_free_inventory_available').val());
							
							surprise_free_str="";
							
							if(surprise_free_skus_from_inventory!=null){
								if(surprise_free_skus_from_inventory.length>0){
									
									for(z=0;z<surprise_free_skus_from_inventory.length;z++){
										
										surprise_free_str+=surprise_free_skus_from_inventory[z].str+" <small>("+parseInt(surprise_free_skus_from_inventory[z].count)+")</small> <br>";
									}
								}
								
							}

							promo_str+='<tr><td colspan="2"><b><h6 style="color:blue;"></b>Surprise Free Items to be returned with the requested Quantity of product</h6>'+surprise_free_str+'</td><tr>';
						}
					}
					if(promotion_surprise_gift_type=='<?php echo curr_code; ?>'){
						if(return_surprise_gift_amount==1){
							promo_str+='<tr><td><h6 style="color:blue;">'+curr_sym+promotion_surprise_gift+' was credited to customer wallet. So this amount will be reduced from the total item price.</h6> </td><td style="color:red"> - '+curr_sym+promotion_surprise_gift+'</td></tr>';
							total_amount-=parseFloat(promotion_surprise_gift);
						}
						
					}
					if(modulo>0){
						//breakup 2
						indi_price_without_promo=individual_price_of_product_without_promotion;
						
						total_price_without_promo=total_price_of_product_without_promotion;
						
						promo_str+='<tr><td>'+modulo+' * '+individual_price_of_product_without_promotion+'<br><small style="color:green;"><b><i>'+promotion_default_discount_promo+' for quantity ('+modulo+')</i></b></small></td><td> '+curr_sym+total_price_without_promo+'</td></tr>';
						total_amount+=parseFloat(total_price_without_promo);
						$("#decided_individual_price_of_product_without_promotion").val(individual_price_of_product_without_promotion);
						$("#decided_total_price_of_product_without_promotion").val(total_price_without_promo);
						$("#decided_quantity_without_promotion").val(modulo);
					}
					
				}
				if(promo_discount_available == 1){
					//calculation has been done while dumping for individual_price
					indi_price_with_promo=individual_price_of_product_with_promotion;
					total=parseFloat(total_price_of_product_with_promotion);
					promo_str+='<tr><td>'+desired_quantity_refund+' * '+indi_price_with_promo+'</td><td>'+curr_sym+total+'</td></tr>';
					total_amount+=parseFloat(total);

				}
			
				
			}else{
				if(promo_discount_available != 1){
					if(modulo>0){
						//breakup 2
						indi_price_without_promo=individual_price_of_product_without_promotion;
						
						total_price_without_promo=total_price_of_product_without_promotion;
						
						promo_str+='<tr><td>'+modulo+' * '+individual_price_of_product_without_promotion+'<br><small style="color:green;"><b><i>'+promotion_default_discount_promo+' for quantity ('+modulo+')</i></b></small></td><td> '+curr_sym+total_price_without_promo+'</td></tr>';
						
						total_amount+=parseFloat(total_price_without_promo);
						
					}
				}
				
				if(promo_discount_available == 1){
					//calculation has been done while dumping for individual_price
					indi_price_with_promo=individual_price_of_product_with_promotion;
					
					total=parseFloat(total_price_of_product_with_promotion);
					
					promo_str+='<tr><td>'+desired_quantity_refund+' * '+indi_price_with_promo+'</td><td>'+curr_sym+total+'</td></tr>';
					total_amount+=parseFloat(total);
			

				}
			}
			
		

		}//promo_available
		
			if(shipping_charge_waived=="yes"){
				shipping_price_str="";
				shipping_charge=0;
			}
			str_invoice_discount="";		

			if(order_item_invoice_discount_value>0){
				
				inv_discount=parseFloat((order_item_invoice_discount_value_each)*desired_quantity_refund).toFixed(2);
				
				total_product_price=parseFloat(total_amount-inv_discount).toFixed(2);
				
				str_invoice_discount='<tr><td> Invoice discount price which has to be deducted ('+desired_quantity_refund+' * '+order_item_invoice_discount_value_each+')</td><td style="color:red;">- '+curr_sym+inv_discount+'</td></tr>';
				
				total_amount=parseFloat(parseFloat(total_product_price)).toFixed(2);
			
			}else{
				total_amount=parseFloat(total_amount);
			}	
			str='<small><table class="table table-bordered table-hover"><thead><tr><th>Net Amount Calculations</th><th>Value</th></tr></thead><tbody>';
			
			str+=promo_str;
			str+=str_invoice_discount;
			//str+=shipping_price_str;
			
			str+='<tr><td>Total Product Price </td><td>'+curr_sym+total_amount+'</td></tr></tbody></table></small>'
			document.getElementById('details_of_refund_request_'+order_item_id).innerHTML=str;

						}
			
		</script>
			
		
         </div>
      </div>
   </div>
</div>

<!--- modal reject ends----------------->


<!--- reject model things ends -------->
	
<!---------------------->
</div>
</body> 
</html>  