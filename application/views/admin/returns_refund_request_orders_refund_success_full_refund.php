<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Refund Request Orders - Refund Success</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<style>
.form-group{
	margin-bottom:10px;
	margin-top:5px;
	padding-bottom:0;
}
</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript">
var table;
$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
    table = $('#returns_refund_request_orders_table_refund_success_full_refund').DataTable({
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Returns/returns_refund_request_orders_refund_success_full_refund_processing", // json datasource
            type: "post",  // type of method  , by default would be get
			data: function (d) { d.status_of_refund = $('#status_of_refund').val(); },
            error: function(){  // error handling code
              $("#returns_refund_request_orders_table_refund_success_full_refund_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("refund_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'40%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});	

function draw_table(){
	table.draw();
}

$(document).ready(function(){
	var returns_refund_orders_list_checked=0;
	$("input[name='returns_refund_orders_list']").each(function(){
		if($(this).is(":checked")){
			returns_refund_orders_list_checked++;
		}
	})
	/*if(returns_refund_orders_list_checked==0){
		document.getElementById("refund_info_div").style.display="none";
	}*/
});

function open_return_refund_issue_success_fun(order_item_id){
	location.href="<?php echo base_url()?>admin/Returns/returns_issue_refund_success/"+order_item_id;
}


function invoice_returns_refund_fullFun(order_item_id,order_id,quantity_refunded){
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/generate_invoice_for_partial_refunds_pdf",
		type:"POST",
		data:"order_item_id="+order_item_id+"&order_id="+order_id+"&quantity_refunded="+quantity_refunded,
		beforeSend:function(){
				$('.invoice_sent_'+order_item_id).html('<i class="fa fa-spinner"></i> Processing');
			}
	}).success( function(data){
		if(data){
			$('.invoice_sent_'+order_item_id).html('Send Invoice');
			alert("Invoice has been sent successfully!");
			window.open("<?php echo base_url()?>assets/pictures/invoices/Invoice_partialref_"+order_id+""+order_item_id+".pdf","_target");
			
		}else{
			alert("error");
		}
	});
	
}
/*
function pickup_need_to_be_updated(obj,orders_status_id,return_order_id,order_item_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/update_order_return_pickup",
		type:"POST",
		data:"orders_status_id="+orders_status_id+"&return_order_id="+return_order_id+"&order_item_id="+order_item_id,
		success:function(data){
			if(data){
				alert("Updated Successfully");
				location.reload();
			}else{
				alert("error");
			}
		}
		})
}
function pickup_again_need_to_be_updated(obj,orders_status_id,return_order_id,order_item_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/update_order_return_pickup_again",
		type:"POST",
		data:"orders_status_id="+orders_status_id+"&return_order_id="+return_order_id+"&order_item_id="+order_item_id,
		success:function(data){
			if(data){
				alert("Updated Successfully");
				location.reload();
			}
			else{
				alert("error");
			}
			
		}
		})
}
*/

function initiate_order_return_pickupFun(obj,orders_status_id,return_order_id,order_item_id){
	
	if(confirm("Do you want Customer to return the Item?! (second)")==true){
		initiate_return_pickup_again=1;
	}else{
		initiate_return_pickup_again=0;
	}
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/initiate_order_return_again_pickupFun",
		type:"POST",
		data:"orders_status_id="+orders_status_id+"&return_order_id="+return_order_id+"&order_item_id="+order_item_id+"&initiate_return_pickup_again="+initiate_return_pickup_again,
		success:function(data){
			if(data){
				alert("Pick Up has been Initiated");
				draw_table();
			}
			else{
				alert("error");
			}
			
		}
		})
	
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header"><h4 class="text-center">Refund Request Orders - Refund Success (Full Refunds) <span class="badge" id="refund_count"></span></h4></div>
	<table id="returns_refund_request_orders_table_refund_success_full_refund" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
		<tr style="display:none;">
			<th colspan="4">
			<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<select class="form-control" name="status_of_refund" id="status_of_refund" onchange="draw_table()">
				<option value="success">Success</option>
				<option value="pending">Pending</option>
				<option value="all">All</option>
			</select>
		</div>
	</div>
			</th>
			</tr>
			<tr>
				<th class="text-primary small bold">Return Summary</th>
				<th class="text-primary small bold">Return Reason</th>
				<th class="text-primary small bold">Refund Details</th>
				<th class="text-primary small bold">Invoice</th>
			</tr>
		</thead>
	</table>
</div>

<!---------------------->
<div class="modal" id="order_summary">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Order Summary</h4>
           </div>
           <div  class="modal-body" id="model_content">
		   <div class="container">
				
			</div>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
  </div>
  
  <script type="text/javascript">
  $("#order_summary").modal("hide");
  function view_order_summary_(order_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/get_data_from_invoice_offers",
		type:"POST",
		data:"order_id="+order_id,
		success:function(data){
			
			$("#description").val("");	
			$("#model_content").html(data);
			$("#order_summary").modal("show");
			
		}
	})
	
  }
  

  </script>
  
  <!--------------pickup modals--------------->
  <div class="modal" id="return_pickup_modal_second" data-backdrop="static" role="dialog">
<div class="modal-dialog modal-sm">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		<div class="panel panel-info">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                   Update Item received status
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right"></span></span>
               </div>
               <div aria-expanded="true" class="">
					<div class="panel-body">
		<form id="return_pickup_form_second" method="post" enctype="multipart/form-data" class="form-horizontal">
			<input type="hidden" name="orders_status_id" id="orders_status_id_s">
			<input type="hidden" name="return_order_id" id="return_order_id_s">
			
			<input type="hidden" name="order_item_id" id="order_item_id_s">
			<input type="hidden" name="inventory_id" id="inventory_id_s">
			<input type="hidden" name="quantity_refund" id="quantity_refund_s">

			
                    
						<div class="form-group">
							<div class="col-md-12">
									<input name="update_pickup" checked type="checkbox">
									 Items are picked up
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12">
									<input name="update_stock" type="radio" value="item_has_reached_warehouse">
									 Item has reached warehouse
									 </div>
									 <div class="col-md-12">
									 <input name="update_stock" type="radio" value="item_has_not_reached_warehouse">
									 Item has not reached warehouse
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
							<input class="btn btn-success btn-xs btn-block" value="Submit" type="submit">
						</div>
					</div>
						</form>
					</div>

               </div>
            </div>
		
         </div>
      </div>
   </div>
</div>
<!------------pickup modal------->

<script type="text/javascript">

function open_order_return_pickup_again(obj,orders_status_id,return_order_id,order_item_id,inventory_id,quantity){
	
	$("#orders_status_id_s").val(orders_status_id);
	$("#return_order_id_s").val(return_order_id);
	$("#order_item_id_s").val(order_item_id);
	$("#inventory_id_s").val(inventory_id);
	$("#quantity_refund_s").val(quantity);
	$("#return_pickup_modal_second").modal("show");
}

$(document).ready(function(){
	
	$("#return_pickup_form_second").on('submit',(function(e) {
		
		var update_stock=$("input[name=update_stock]").is(':checked');
		var update_pickup=$("input[name=update_pickup]").is(':checked');
		
		if(update_stock==false){
			alert('Pls fill all fields');
			return false;
		}
		
		if(update_pickup==false){
			alert('Items should be picked up.');
			return false;
		}
		e.preventDefault();
		
		$.ajax({
				url:"<?php echo base_url();?>admin/Returns/update_order_return_pickup_again",
				type: "POST",    
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){	

					if(data){
						alert("Successfully Updated");
						$("#return_pickup_form_second").trigger("reset");
						$("#return_pickup_modal_second").modal("hide");	
						draw_table();
					}else{
						alert("not sent");
					}
				}	        
		   });
		
		
	}));
	
	$("#return_pickup_modal_second").on('hidden.bs.modal', function () {
		$("#return_pickup_form_second").trigger("reset");	
		draw_table();
    });
	
	
});
</script>
<!------------pickup modal------->

<div class="modal" id="return_pickup_modal" role="dialog">
<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		<div class="panel panel-primary">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                   Item received update status
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right"></span></span>
               </div>
               <div aria-expanded="true" class="">
					<div class="panel-body">
		<form id="return_pickup_form" method="post" enctype="multipart/form-data" class="form-horizontal">
			<input type="hidden" name="orders_status_id" id="orders_status_id">
			<input type="hidden" name="return_order_id" id="return_order_id">
			
			<input type="hidden" name="order_item_id" id="order_item_id">
			<input type="hidden" name="inventory_id" id="inventory_id">
			<input type="hidden" name="quantity_refund" id="quantity_refund">

						<div class="form-group">
							<div class="col-md-12">
									<input name="update_pickup" checked type="checkbox">
									 Items are pickedup
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12">
									<input name="update_stock" type="radio" value="item_has_reached_warehouse">
									 Item has reached warehouse
									 </div>
									<div class="col-md-12">
									 <input name="update_stock" type="radio" value="item_has_not_reached_warehouse">
									 Item has not reached warehouse
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
							<input class="btn btn-success btn-xs btn-block" value="Submit" type="submit">
						</div>
					</div>
						</form>
					</div>
               </div>
            </div>
		
         </div>
      </div>
   </div>
</div>
<!------------pickup modal------->

<script type="text/javascript">

function open_order_return_pickup(obj,orders_status_id,return_order_id,order_item_id,inventory_id,quantity){
	
	$("#orders_status_id").val(orders_status_id);
	$("#return_order_id").val(return_order_id);
	$("#order_item_id").val(order_item_id);
	$("#inventory_id").val(inventory_id);
	$("#quantity_refund").val(quantity);
	$("#return_pickup_modal").modal("show");
}

$(document).ready(function(){
	
	$("#return_pickup_form").on('submit',(function(e) {
		
		var update_stock=$("input[name=update_stock]").is(':checked');
		var update_pickup=$("input[name=update_pickup]").is(':checked');
		
		if(update_stock==false){
			alert('Pls fill all fields');
			return false;
		}
		
		if(update_pickup==false){
			alert('Items should be picked up.');
			return false;
		}
		e.preventDefault();
		
		$.ajax({
				url:"<?php echo base_url();?>admin/Returns/update_order_return_pickup",
				type: "POST",    
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){	
				
					if(data){
						alert("Successfully Updated");
						$("#return_pickup_form").trigger("reset");
						$("#return_pickup_modal").modal("hide");	
						draw_table();
					}else{
						alert("not sent");
					}
				}	        
		   });
		
		
	}));
	
	$("#return_pickup_modal").on('hidden.bs.modal', function () {
		$("#return_pickup_form").trigger("reset");	
		draw_table();
    });
	
	
});

function go_to_ref_details(order_item_id){	
	 $('#ref_details_'+order_item_id).attr('action', '<?php echo base_url();?>admin/Returns/ref_details_of_order_item').submit();
}

</script>

	<!--------------free Items-return modal------>

<div class="modal" id="return_free_items_modal" data-backdrop="static" role="dialog">
		<div class="modal-dialog">
		  <!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body"> 
			 
					<div class="panel panel-info">
					   <div id="accordion" class="panel-heading" style="cursor:pointer;">
						  Details of Promotion
						  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right"></span></span>
					   </div>
					   <div aria-expanded="true">
							<div class="panel-body" id="return_free_items_data">
								
							</div>
					   </div>
					</div>
					<div class="modal-footer">
						<a class="text-info bold" data-dismiss="modal" style="cursor:pointer;">Close</a>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	
	<script>
	function show_freeitems_return(obj){		
		$('#return_free_items_modal').modal();
		html_str=obj.getElementsByTagName("span")[0].innerHTML;
		$('#return_free_items_data').html(html_str);
	}
	</script>

</div>
</body> 
</html>  