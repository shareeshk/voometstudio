<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Replacement Request Orders - Incoming Requests</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<!--<script type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js"></script>-->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script>
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   var table = $('#returns_replacement_request_orders_table_accounts_incoming').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Returns/returns_replacement_request_orders_accounts_incoming_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#returns_replacement_request_orders_table_accounts_incoming_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("replacement_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'30%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});	
$(document).ready(function(){
	var returns_refund_orders_list_checked=0;
	$("input[name='returns_refund_orders_list']").each(function(){
		if($(this).is(":checked")){
			returns_refund_orders_list_checked++;
		}
	})
	/*if(returns_refund_orders_list_checked==0){
		document.getElementById("refund_info_div").style.display="none";
	}*/
});

function printcontent(order_item_id){
	//Popup($("#print_"+elem).html());
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/returns_replacement_request_orders_accounts_incoming_printcontent",
		type:"POST",
		data:"order_item_id="+order_item_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			Popup($("#print").html(data))
		}
	})
	
	
}

function Popup(printdiv) {
	var mywindow = window.open('', 'Refund Details', 'height=400,width=600');
	var css = "";
        var myStylesLocation = "<?php echo base_url();?>assets/css/bootstrap.min.css";
		
        $.ajax({
            url: myStylesLocation,
            type: "POST",
            async: false
        }).done(function(data){
            css += data;
        })
	mywindow.document.write('<html><head><title>Refund Details</title>');
	mywindow.document.write('<style type="text/css">'+css+' </style>');
	mywindow.document.write('</head><body><div class="container"><div class="row"><div class="col-md-12 "><h4 class="alert alert-success text-center">Refund Details</h4></div></div>');
	mywindow.document.write('<div class="row"><div class="col-md-8 col-md-offset-2"><div class="well"><div class="row"><div class="col-md-8 col-md-offset-2">');
	mywindow.document.write(printdiv.html());
	mywindow.document.write('</div></div></div></div></div></div></body></html>');

	mywindow.document.close(); // necessary for IE >= 10
	mywindow.focus(); // necessary for IE >= 10

	mywindow.print();
	mywindow.close();

	return true;
}	

function open_return_refund_success_fun(order_item_id){
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/returns_replacement_request_orders_accounts_incoming_success",
		type:"POST",
		data:"order_item_id="+order_item_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			//alert(data);
			$("#return_refund_success_form").html(data);
			$("#return_refund_success_modal").modal("show");
		}
	})
	
}

function open_return_refund_wallet_fun(order_item_id){
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/returns_replacement_request_orders_accounts_incoming_wallet",
		type:"POST",
		data:"order_item_id="+order_item_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			//alert(data);
			$("#return_refund_wallet_form").html(data);
			$("#return_refund_wallet_modal").modal("show");
		}
	})
	
}

</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header"><h4 class="text-center">Replacement Request Orders - Incoming Requests <span class="badge" id="replacement_count"></span></h2></div>
	<table id="returns_replacement_request_orders_table_accounts_incoming" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="text-primary small bold">Return Summary</th>
				<th class="text-primary small bold">Transaction details</th>
				<th class="text-primary small bold">Return Reason</th>
				<th class="text-primary small bold">Actions</th>
			</tr>
		</thead>
	</table>
</div>
<script>
$(document).ready(function(){
	$("#return_refund_success_form").on('submit',(function(e) {
		e.preventDefault();
		//alert(new FormData(this).serialize());
			$.ajax({
				url:"<?php echo base_url();?>admin/Returns/return_replacement_success",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					if(data){
						$('#return_refund_success_modal').modal('hide');
						swal({
							title:"success!", 
							text:"Successfully sent", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#return_refund_success_form").trigger("reset");
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
					
				}	        
		   });
	}));

});
function divFunction_account_success(){
    $("#return_refund_success_form").trigger("reset");	
		location.reload();
}
</script>



<!--- modal success starts----------------->
<div class="modal" id="return_refund_success_modal" data-backdrop="static" role="dialog" tabindex="-1" aria-hidden="true">
   <div class="modal-dialog modal-sm">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		 <div class="panel panel-success">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                   Refund Success details
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right" onClick="divFunction_account_success()"></span></span>
               </div>
                  <div class="panel-body">
		 <form id="return_refund_success_form" method="post" enctype="multipart/form-data" class="form-horizontal">
		 
		 
			</form>
			 </div>
   </div>
         </div>
      </div>
   </div>
</div>

<!--- modal success ends----------------->

<!--- return_refund_success_form  ends --->
<!--- return_refund_wallet_modal starts -->
<script>

$(document).ready(function(){
	
	$("#return_refund_wallet_form").on('submit',(function(e) {
		e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>admin/Returns/return_replacement_wallet",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					if(data){
						$('#return_refund_wallet_modal').modal('hide');
						swal({
							title:"success!", 
							text:"Amount has been transfered to wallet", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#return_refund_wallet_form").trigger("reset");
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
					
				}	        
		   });
	}));

});
function divFunction_account_wallet(){
    $("#return_refund_wallet_form").trigger("reset");	
		location.reload();
}
</script>



<!--- modal wallet starts----------------->
<div class="modal" id="return_refund_wallet_modal" data-backdrop="static" role="dialog" tabindex="-1" aria-hidden="true">
   <div class="modal-dialog modal-sm">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		  <div class="panel panel-info">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                  Transfer to Wallet
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right" onClick="divFunction_account_wallet()"></span></span>
               </div>
               <div aria-expanded="true" class="">
                  <div class="panel-body">
		 <form id="return_refund_wallet_form" method="post" enctype="multipart/form-data" class="form-horizontal">
		 
		
			</form>
			</div>
      </div>
   </div>
         </div>
      </div>
   </div>
</div>

<!--- modal wallet ends----------------->


<!--- return_refund_wallet_modal ends -->


<div class="container" id="print" style="display:none;">

</div>


<!------------------------------------->

<!---------------------->
<div class="modal" id="order_summary">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Order Summary</h4>
           </div>
           <div  class="modal-body" id="model_content">
		   <div class="container">
				
			</div>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close"class="btn">Close</a>
        </div>
     </div>
    </div>
  </div>
  
  <script type="text/javascript">
  $("#order_summary").modal("hide");
  function view_order_summary_(order_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/get_data_from_invoice_offers",
		type:"POST",
		data:"order_id="+order_id,
		success:function(data){
			
			$("#description").val("");	
			$("#model_content").html(data);
			$("#order_summary").modal("show");
			
		}
	})
	
}
  </script>
  </div>
</body> 
</html>  