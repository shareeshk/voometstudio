<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script type="text/javascript">

function showDivPrevious(){
	
	var form = document.getElementById('search_for_logistics');		
	
	form.action='<?php echo base_url()."admin/Catalogue/logistics"; ?>';
	form.submit();
			
	 //window.location.href = '<?php echo base_url(); ?>admin/Catalogue/category';
}
</script>
<?php
if($get_logistics_data['logistics_priority']=="0"){?>
	<script>
	$(document).ready(function () {
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	document.getElementById('show_available_common_sub').disabled = true;
	
	});
	</script>
	<?php
}
?>
<?php //print_r($logistics);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row-fluid" id="editDiv3">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="edit_logistics" id="edit_logistics" method="post" action="#" onsubmit="return form_validation_edit();">
		<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_vendors: {
					required: true,
				},
				edit_logistics_name: {
					required: true,
				},
				edit_logistics_weblink: {
					required: true,
				},
				edit_tracking_id_generation: {
					required: true,
				},
				edit_logistics_priority:{
					required: true,
				},
				edit_logistics_volumetric_value:{
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Logistics</h3>
		</div>	
		</div>
		<div class="tab-content">
					
			<input type="hidden" name="edit_logistics_id" id="edit_logistics_id" value="<?php echo $get_logistics_data["logistics_id"];?>">
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Vendor-</label>
					<select name="edit_vendors" id="edit_vendors" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($vendors as $vendors_value) {  ?>
						<option selected value="<?php echo $vendors_value->vendor_id; ?>" <?php echo ($vendors_value->vendor_id==$vendor_id)? "selected":''; ?>><?php echo $vendors_value->name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Logistics Name</label>
					<input id="edit_logistics_name" name="edit_logistics_name" type="text" class="form-control" value="<?php echo $get_logistics_data["logistics_name"];?>"/>
				</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Logistic weblink</label>
					<textarea id="edit_logistics_weblink" name="edit_logistics_weblink" type="text" class="form-control" ><?php echo $get_logistics_data["logistics_weblink"];?></textarea>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Tracking Id Generation</label>
					<select name="edit_tracking_id_generation" id="edit_tracking_id_generation" class="form-control">
						<option value=""></option>
						<option value="yes" <?php if($get_logistics_data["tracking_id_generation"]=="yes"){echo "selected";}?>>Yes</option>
						<option value="no" <?php if($get_logistics_data["tracking_id_generation"]=="no"){echo "selected";}?>>No</option>
					</select>
					
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Enter the Sort Order</label>
					
					<select name="edit_logistics_priority" id="edit_logistics_priority" class="form-control" onchange="show_view_validatn()">
					<option value="<?php echo $get_logistics_data["logistics_priority"];?>" selected><?php echo $get_logistics_data["logistics_priority"];?></option>
						<?php
							if($get_sort_order_options_for_logistics["key"]==""){
								if($get_logistics_data["logistics_priority"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php } ?>
								<option value="1">1</option>
								<?php
							}
							else{
								if($get_logistics_data["logistics_priority"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php
								}
								$value_arr=explode(",",$get_sort_order_options_for_logistics["value"]);
								foreach($value_arr as $k => $v){
									?>
										<option value="<?php echo $v;?>"><?php echo $v;?></option>
									<?php
									}
							}
						?>
					</select>
					<input id="edit_logistics_priority_default" name="edit_logistics_priority_default" type="hidden" value="<?php echo $get_logistics_data["logistics_priority"];?>" />	
				</div>
				</div>
				
			</div>			
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group label-floating">
			<label class="control-label">-Swap Options-</label>
			<select class="form-control" id="show_available_common_sub" onchange="changeEventHandler(event)">
			<option selected value=""></option>
			<?php
			
			foreach($logistics_sort_order_arr as $logistics_order_arr){
				
				?>
				
					<option value="<?php echo $logistics_order_arr["logistics_id"];?>"><?php echo $logistics_order_arr["logistics_name"]."(".$logistics_order_arr["logistics_priority"].")";?></option>
					<?php
			}
			?>
			
			</select>
			</div>
				</div>
			</div>	
			
		
			
			<div class="row">
				<div class="col-md-10"> 
					<div class="form-group">
					<div class="col-md-5 col-md-offset-1">Show Logistics</div>
					<div class="col-md-3">
						<label class="radio-inline"><input id="edit_view1" name="edit_view" value="1" type="radio" <?php if($get_logistics_data["default_logistics"]=="1"){echo "checked";}?>>View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input id="edit_view2" name="edit_view" value="0" type="radio" <?php if($get_logistics_data["default_logistics"]=="0"){echo "checked";}?>>Hide</label>
					</div>
				
					</div>
				</div>
			</div>	
			
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Volumetric Value</label>
					<input id="edit_logistics_volumetric_value" name="edit_logistics_volumetric_value" type="text" class="form-control" value="<?php echo $get_logistics_data["logistics_volumetric_value"];?>">
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter GST</label>
					<input type="text" id="edit_logistics_gst" name="edit_logistics_gst" class="form-control" value="<?php echo $get_logistics_data["logistics_gst"];?>">
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Fuelsurcharge</label>
					<input type="text" id="edit_logistics_fuelsurcharge" name="edit_logistics_fuelsurcharge" class="form-control" value="<?php echo $get_logistics_data["logistics_fuelsurcharge"];?>">
				</div>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-md-10 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit-data" disabled="">Submit</button>
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
					</div>
				</div>
			</div>		
			
		</div>		
	</form>
	<form name="search_for_logistics" id="search_for_logistics" method="post">
		<input value="<?php echo $get_logistics_data["vendor_id"]; ?>" type="hidden" id="vendor_id" name="vendor_id">
		<input value="view" type="hidden" name="create_editview">
	</form>
</div>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>
</body>
</html>
<script type="text/javascript">

$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_logistics'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_logistics"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_logistics'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select' || type == 'textarea') {
            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});
});

function form_validation_edit()
{

	var logistics_name = $('input[name="edit_logistics_name"]').val().trim();
	var logistics_weblink = $('textarea[name="edit_logistics_weblink"]').val().trim();
	var vendors = $('select[name="edit_vendors"]').val().trim();
var logistics_priority = $('select[name="edit_logistics_priority"]').val().trim();
var common_logistics=document.getElementById("show_available_common_sub").value;
var logistics_volumetric_value = $('input[name="edit_logistics_volumetric_value"]').val().trim();
			if(common_logistics!=""){
				
				
			}
	   var err = 0;
		if((logistics_name=='') || (logistics_weblink=='') || (vendors=='') || (logistics_volumetric_value=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
				document.getElementById("edit_vendors").disabled="";
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#edit_logistics');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_logistics/"?>',
				type: 'POST',
				data: $('#edit_logistics').serialize()+"&common_logistics="+common_logistics,
				dataType: 'html',
				
			}).done(function(data)
			  {			
				if(data)
				{
					  
					swal({
						title:"Success", 
						text:"Logistic is successfully Updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				else
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
			});
}
}]);			
			}
	
	return false;
}


function changeEventHandler(event){
        parent_id=event.target.value;
		if(parent_id!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		}
		if(parent_id==""){
	edit_logistics_priority=document.getElementById("edit_logistics_priority").value;
	if(edit_logistics_priority==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;					
	}
	if(edit_logistics_priority!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
		}
}

function show_view_validatn(){
	edit_logistics_priority=document.getElementById("edit_logistics_priority").value;
	show_available_common_sub=document.getElementById("show_available_common_sub").value;
	edit_logistics_priority_default=document.getElementById("edit_logistics_priority_default").value;
	if(show_available_common_sub==""){
	if(edit_logistics_priority==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	if(edit_logistics_priority_default==0){
	document.getElementById('show_available_common_sub').disabled = true;
	}
	if(edit_logistics_priority_default!=0){
	document.getElementById('show_available_common_sub').disabled = false;
	}
	}
	if(edit_logistics_priority!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		//document.getElementById('show_available_common_sub').disabled = false;
	}
	}
	if(show_available_common_sub!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
	
}
</script>
