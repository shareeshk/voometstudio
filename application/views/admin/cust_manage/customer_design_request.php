<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Customers</title>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 
<script type="text/javascript">
var table;
$(document).ready(function (){
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("input[type=text]").val("");
	});
	
	
	$('#to_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY'
			});
			
		$('#from_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY', 
				shortTime : true
			}).on('change', function(e, date)
			{
				$('#to_date').bootstrapMaterialDatePicker('setMinDate', date);
			});
	
	 // Array holding selected row IDs
    var rows_selected = [];
     table = $('#myTable').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Manage_cust/design_request_processing",
			data: function (d) { d.from_date = $('#from_date').val(); d.to_date = $('#to_date').val();d.design_type = $('#design_type').val();},
            type: "post",
			
            error: function(){
              $("#myTable_processing").css("display","none");
			  
            },
			"dataSrc": function (json) {
				document.getElementById("customercount").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'2%',
        'className': 'dt-body-left'
      }],
      'order': [4, 'desc']
   });
   
   $( "#from_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
	table.draw();
	});

	$( "#to_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
	table.draw();
	});

$('#reset_form_button').on('click',function(){
	table.draw();
});

$('#submit_form_button').on('click',function(){
	table.draw();
});	
});	
function draw_table(){
	 
	table.draw();
	
}

</script>

</head>  
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="row">
<div class="page-header">
		<h4 class="text-center bold"> <span id="requestvalue">Number of Design Request </span> <span class="badge"><div id="customercount"></div></span> 
		</h4>                        		  				                       	
</div>

<div class="table-responsive">
<table id="myTable" class="table table-bordered table-striped" cellspacing="0" width="100%" style="font-size: 14px;">
    <thead>	
	<tr><th colspan="6">
			
			<div id="datetimediv" class="col-md-12 text-right">
				<form id="date_range" class="form-inline">
				<div class="col-md-3">
					<select class="form-control" name="design_type" id="design_type" onchange="draw_table()">
							<option value=""  selected> All Design Types </option>
							<option value="Home">Home</option>
							<option value="Modular kitchen">Modular Kitchen</option>
							<option value="Bedroom">Bedroom</option>
							<option value="Office room">Office Room</option>
							<option value="Conference room">Conference Room</option>
							<option value="Workstations">Workstations</option>
					</select>
				</div>

				<div class="col-md-3">
				<input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date">
				</div>
				<div class="col-md-3">
				<input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date">
				</div>
				<div class="col-md-3">
				<button type="button" class="btn btn-success btn-xs" id="submit_form_button">Submit</button>
				<button type="reset" class="btn btn-warning btn-xs" id="reset_form_button">Reset</button>
				</div>
				</form>
	
			</div>
		</th>
		</tr>
        <tr>
            <th class="text-primary small bold">S.No</th>
            <th class="text-primary small bold">Name</th>
            <th class="text-primary small bold">Type</th>
            <th class="text-primary small bold">Contact</th>
            <th class="text-primary small bold">Requested On</th>
            <th class="text-primary small bold">Status</th>
        </tr>
    </thead>
</table>
</div>
</div>
</div>
</div>

<div class="modal" id="status_modal">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Update Design Request Status</h4>
           </div>
           <div  class="modal-body" id="model_content">
				<form method="post" id="request_form" name="request_form">
		  			<input type="hidden" name="request_id" id="request_id" value="">

					<div class="row">

					<div class="col-md-11 col-md-offset-1"> 
							<div class="form-group">
								<label class="control-label">Status
								</label>
								<select name="status" id="status" type="text" class="form-control" required onchange="show_contacted_status()"/>
								<option value=""> - Select status - </option>
								<option value="0" selected >Pending Connect</option>
								<option value="1">Contacted</option>
								</select>
							</div>
						</div>

						<div class="col-md-11 col-md-offset-1" style="display:none;" id="contacted_status_div"> 
							<div class="form-group">
								<label class="control-label">Contacted Status
								</label>
								<select name="contacted_status" id="contacted_status" class="form-control"/>
								<option value=""> - Select cotacted status - </option>
								<option value="Just Curious">Just Curious</option>
								<option value="Warm Lead">Warm Lead</option>
								<option value="Hot Lead">Hot Lead</option>
								<option value="Proposal Submitted">Proposal Submitted</option>
								<option value="Pending Decision">Pending Decision</option>
								<option value="Won">Won</option>
								<option value="Postponed Lost">Postponed Lost</option>
								
								</select>
							</div>
						</div>		  
						<div class="col-md-11 col-md-offset-1"> 	
							<div class="form-group">
							<label class="control-label">Comments if any 
							</label>
							<textarea name="contacted_comments" id="contacted_comments" class="form-control" min="1" ></textarea>
							</div>
						</div>

						<div class="col-md-11 col-md-offset-1"> 	
							<div class="form-group text-center">
							
							<button class="btn btn-info btn-sm" type="button" onclick="submit_status()" id="submit-data"></i>Submit</button>
							</div>
						</div>
					</div>



				</form>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
</div>

<script type="text/javascript">

function view_status_modal(request_id){
	$("#request_id").val(request_id);
	$("#status_modal").modal("show");
}

	function submit_status(){
		

		contacted_status=$('#contacted_status').val();
		status=$("#status").val();
		if(status=='1'){
			if(contacted_status==''){
				alert('Please select contact status');
				return false;
			}
		}
		$.ajax({
			url:"<?php echo base_url()?>admin/Manage_cust/update_status_request_design",
			type:"POST",
			data:"request_id="+request_id+"&"+$("#request_form").serialize(),
			success:function(data){		
				if(data){
					swal({
						title:"success!", 
						text:"Design Request has been updated Successfully!", 
						type: "success",
						allowOutsideClick: false
						
					}).then(function () {
						$("#status_modal").modal("hide");
						draw_table();

					});
				}else{
					swal("Error", "not sent", "error");
				}
			}
		});
		
	}
	function show_contacted_status(){
		status=$("#status").val();
		if(status=='1'){
			$('#contacted_status_div').show();
		}else{
			$('#contacted_status_div').hide();
		}
	}

</script>
  
</body>  
</html>  