<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js"></script>

<style>
.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
</style>
<script type="text/javascript">
function showDivPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Manage_cust/customer_profile';
}

</script>
<?php //print_r($logistics);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="row" id="editDiv3">
<div class="col-md-6 col-md-offset-3">
<div class="page-header">
			<h4 class="title text-center bold">Edit Customer Profile</h4>
		</div>
		<form name="edit_profile" id="edit_profile" method="post" action="#" onsubmit="return form_validation_edit();" class="form-horizontal">
		<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_profile_name: {
					required: true,
				},
				edit_sort_order: {
					required: true,
				},
				
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
		
			
		<div class="tab-content">
			<input type="hidden" name="id" id="id" value="<?php echo $get_customer_profile_data["id"];?>">
			
			<div class="form-group">
				<div class="col-md-10 col-md-offset-1">  
					<input id="edit_profile_name" name="edit_profile_name" type="text" class="form-control" value="<?php echo $get_customer_profile_data["profile_name"];?>" placeholder="Enter Profile Name"/>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-md-10 col-md-offset-1">  
					<input id="edit_sort_order" name="edit_sort_order" type="number" min="0" class="form-control" value="<?php echo $get_customer_profile_data["sort_order"];?>" placeholder="Enter the Sort Order"/>
				</div>
			</div>
					<div class="form-group">
					<div class="col-md-5 col-md-offset-1">Show Customer profile</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="edit_view" value="1" type="radio" <?php if($get_customer_profile_data["view"]=="1"){echo "checked";}?>>View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="edit_view" value="0" type="radio" <?php if($get_customer_profile_data["view"]=="0"){echo "checked";}?>>Hide</label>
					</div>
				
					</div>

			
			<div class="form-group">
				<div class="col-md-8 col-md-offset-4"> 
					<button class="btn btn-info btn-xs" type="submit">Submit</button>
					<button class="btn btn-info btn-xs" type="button" onclick="showDivPrevious()">Go to Previous page</button>
				</div>
			</div>
		</div>
		</form>
	

</div>
</div>
</div>
</div>
</body>
</html>
<script type="text/javascript">

function form_validation_edit()
{
	var profile_name = $('input[name="edit_profile_name"]').val().trim();
	var edit_sort_order = $('input[name="edit_sort_order"]').val().trim();
	var view = $('input[name="edit_view"]').val();

	   var err = 0;
		if(!(profile_name.length>=3))
		{
		      
		   err = 1;
		}
		else{			
			
		}
		
		if(edit_sort_order==''){
			   
			err = 1;
		}else{			
			
		}
		
		if(err==1)
		{
			   $('#error_result_id').show();
			   $('#result').hide();
		}
		else
			{
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#edit_profile');	
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {				
			$.ajax({
				url: '<?php echo base_url()."admin/Manage_cust/edit_customer_profile"?>',
				type: 'POST',
				data: $('#edit_profile').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {	
			 
				if(data)
				{
					swal({
						title:"Success", 
						text:"customer profile is successfully Updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
					  
				}
				else
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)	
				}
				
			});
}
}]);			
			}
	
	return false;
}


</script>
