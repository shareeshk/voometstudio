<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Customer Bank Transfer</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 
<script type="text/javascript">
var table;
$(document).ready(function (){
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("input[type=text]").val("");
	});
	
	
		$('#to_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY'
			});
			
		$('#from_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY', 
				shortTime : true
			}).on('change', function(e, date)
			{
				$('#to_date').bootstrapMaterialDatePicker('setMinDate', date);
			});
	
   // Array holding selected row IDs
   var rows_selected = [];
   table = $('#customer_bank_tranfer_table').DataTable({
	 
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Manage_cust/customer_bank_transfer_processing", // json datasource
			data: function (d) { d.status = $('#status').val();d.from_date = $('#from_date').val(); d.to_date = $('#to_date').val(); },
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#customer_bank_tranfer_table_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				
				document.getElementById("requestcount").innerHTML=json.recordsFiltered;
				
                return json.data;
            }
			
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'5%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
   
$( "#from_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
table.draw();
} );

$( "#to_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
table.draw();
} );

$('#reset_form_button').on('click',function(){
	table.draw();
});

$('#submit_form_button').on('click',function(){
	table.draw();
});
});	
function draw_table(){
	 var gettingvalue=document.getElementById("status").value;
	 
	  if(gettingvalue=="pending"){
		document.getElementById("requestvalue").innerHTML="Incoming request";
		document.getElementById('datetimediv').style.display = "none";
	  }
	  if(gettingvalue=="accept"){
		document.getElementById("requestvalue").innerHTML="Processed request"; 
		document.getElementById('datetimediv').style.display = "none";
	  }
	  if(gettingvalue=="success"){
		document.getElementById("requestvalue").innerHTML="Successful request";  
		document.getElementById('datetimediv').style.display = "block";	
	  }
	  if(gettingvalue=="reject"){
		document.getElementById("requestvalue").innerHTML="Rejected request"; 
		document.getElementById('datetimediv').style.display = "block";	
	  }
	  if(gettingvalue=="failure"){
		document.getElementById("requestvalue").innerHTML="Failed request";  
		document.getElementById('datetimediv').style.display = "none";
	  }
	  if(gettingvalue=="all_status"){
		document.getElementById("requestvalue").innerHTML="All request";  
		document.getElementById('datetimediv').style.display = "block";
	  }
	  
	  
	table.draw();
	
	
}

function open_wallet_amount_edit_fun(wallet_transaction_bank_id){
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Manage_cust/open_wallet_amount_edit_fun",
		type:"POST",
		data:"wallet_transaction_bank_id="+wallet_transaction_bank_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			//alert(data);
			$("#wallet_bank_success_form").html(data);
			$("#wallet_bank_success_modal").modal("show");
		}
	})
	
	
}
function accept_fun(wallet_transaction_bank_id){
	$.ajax({
			url:"<?php echo base_url()?>admin/Manage_cust/wallet_transaction_bank_accept",
			type:"POST",
			data:"wallet_transaction_bank_id="+wallet_transaction_bank_id,
			success:function(data){
				alert("Processed");
				location.reload();
			}
	});
}
function reject_fun(wallet_transaction_bank_id){
	
	$("#reject_div_"+wallet_transaction_bank_id).css({"display":"block"});
}
function submit_reject_fun(wallet_transaction_bank_id){
	
	$.ajax({
			url:"<?php echo base_url()?>admin/Manage_cust/wallet_transaction_bank_reject",
			type:"POST",
			data:$('#reject_form_'+wallet_transaction_bank_id).serialize(),
			success:function(data){
				
				if(data){
					alert("Rejected Successfully");
				}else{
					alert('Something went wrong..!');
				}
				location.reload();
			}
	});
}
</script>
<script>
function resend_wallet_bank_tranfer_fun(wallet_transfer_bank_id){
	$.ajax({
			url:"<?php echo base_url()?>admin/Manage_cust/wallet_transaction_bank_accept",
			type:"POST",
			data:"wallet_transaction_bank_id="+wallet_transfer_bank_id,
			success:function(data){
				
				alert("Processed for transfer");
				location.reload();
			}
	});
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="row">
	<div class="page-header text-center">
		<h4 class="text-center bold"> <span id="requestvalue">Incoming Requests</span> <span class="badge"><div id="requestcount"></div></span> 
		</h4>                        		
		                       	
	</div>           				                       	
		
	<table id="customer_bank_tranfer_table" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
		<tr>
			<th colspan="4">
			<div class="col-md-4">
			<select class="form-control" name="status" id="status" onchange="draw_table()">
				<option selected value="pending">Incoming request</option>
				<option value="accept">Processed request</option>
				<option value="success">Successful request</option>
				<option value="reject">Rejected request</option>
				<option value="failure">Failed request</option>
				<option value="all_status">All request</option>
			</select>
			</div>
				<div id="datetimediv" class="col-md-8 pull-right">
					<form id="date_range" class="form-inline">
					<input type="text" name="from_date" id="from_date" placeholder="From Date" class="form-control">
					<input type="text" name="to_date" id="to_date" placeholder="To Date" class="form-control">
					<button type="button" class="btn btn-primary btn-xs" id="submit_form_button">Submit</button>
					<button type="reset" class="btn btn-info btn-xs" id="reset_form_button">Reset</button>
					</form>
				
				</div>
			</th>
		</tr>
			<tr>
				<th class="text-primary small bold">ID</th>
				<th class="text-primary small bold">Customer</th>
				<th class="text-primary small bold">Bank Details</th>
				<th class="text-primary small bold">Action</th>
			</tr>
		</thead>
	</table>
</div>
</div>
</div>

<!--- modal success starts----------------->
<div class="modal" id="wallet_bank_success_modal" data-backdrop="static" role="dialog" tabindex="-1" aria-hidden="true">
   <div class="modal-dialog modal-sm">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		 <div class="panel panel-success">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                  Bank transfer request - Accept
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right" onClick="divFunction_account_success()"></span></span>
               </div>
               <div aria-expanded="true" class="">
                  <div class="panel-body">
		 <form id="wallet_bank_success_form" method="post" enctype="multipart/form-data" class="form-horizontal">
		 
		 
		</form>
		</div>
      </div>
   </div>
         </div>
      </div>
   </div>
</div>

<!--- modal success ends----------------->
<script>
$(document).ready(function(){
	
	$("#wallet_bank_success_form").on('submit',(function(e) {

		e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>admin/Manage_cust/wallet_transaction_bank_accept",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					//alert(data);
					//return false;

					if(data){
						$('#wallet_bank_success_modal').modal('hide');
						swal({
							title:"success!", 
							text:"Successfully sent to accounts team", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#wallet_bank_success_form").trigger("reset");
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
					
				}	        
		   });
	}));

});

function divFunction_account_success(){
    $("#wallet_bank_success_form").trigger("reset");	
		location.reload();
}
</script>

</body> 
</html>  