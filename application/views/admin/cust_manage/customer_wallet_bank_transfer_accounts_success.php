<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Customer Bank Transfer - Success Transactions</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 

<link href="<?php echo base_url();?>assets/css/jquery.timepicker.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/js/jquery.timepicker.js"></script>


<style>
.datepicker{z-index:1151 !important;}
</style>
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   var table = $('#customer_bank_tranfer_accounts_success_table').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Manage_cust/customer_bank_tranfer_accounts_success_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#customer_bank_tranfer_accounts_success_table_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("wallet_transfer_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'40%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});	


</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="page-header"><h4 class="text-center">Customer Bank Transfer - Success Transactions <span class="badge" id="wallet_transfer_count"></span></h4></div>
	<table id="customer_bank_tranfer_accounts_success_table" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr >
				<th>Request Details</th>
				<th>Bank Details</th>
				<th>Transaction Details</th>
			</tr>
		</thead>
	</table>
</div>
</div>
</body> 
</html>  