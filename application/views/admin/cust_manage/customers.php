<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Customers</title>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 
<script type="text/javascript">
var table;
$(document).ready(function (){
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("input[type=text]").val("");
	});
	
	
	$('#to_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY'
			});
			
		$('#from_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY', 
				shortTime : true
			}).on('change', function(e, date)
			{
				$('#to_date').bootstrapMaterialDatePicker('setMinDate', date);
			});
	
	 // Array holding selected row IDs
    var rows_selected = [];
     table = $('#myTable').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Manage_cust/customers_processing",
			data: function (d) { d.customer_status = $('#customer_status').val();d.from_date = $('#from_date').val(); d.to_date = $('#to_date').val();},
            type: "post",
			
            error: function(){
              $("#myTable_processing").css("display","none");
			  
            },
			"dataSrc": function (json) {
				document.getElementById("customercount").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'2%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
   
   $( "#from_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
	table.draw();
	});

	$( "#to_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
	table.draw();
	});

$('#reset_form_button').on('click',function(){
	table.draw();
});

$('#submit_form_button').on('click',function(){
	table.draw();
});	
});	
function draw_table(){
	 var gettingvalue=document.getElementById("customer_status").value;
	 
	  if(gettingvalue=="active"){
		document.getElementById("requestvalue").innerHTML="Active customers";
		
	  }
	  if(gettingvalue=="inactive"){
		document.getElementById("requestvalue").innerHTML="Inactive customers"; 
		
	  }
	  if(gettingvalue=="all"){
		document.getElementById("requestvalue").innerHTML="All customers";  
		
	  }
	 
	table.draw();
	
	
}
function showcustomerprofile(customer_id){
	location.href="<?php echo base_url()?>admin/Manage_cust/view/"+customer_id;
}
</script>

</head>  
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="row">
<div class="page-header">
		<h4 class="text-center bold"> <span id="requestvalue">Number of Customers </span> <span class="badge"><div id="customercount"></div></span> 
		</h4>                        		  				                       	
</div>

<div class="table-responsive">
<table id="myTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
    <thead>	
	<tr><th colspan="6">
			<div class="col-md-4">
			<select class="form-control" name="customer_status" id="customer_status" onchange="draw_table()">
				<option selected value="all">All customers</option>
				<option value="active">Active</option>
				<option value="inactive">Inactive</option>
			</select>
		</div>
			<div id="datetimediv" class="col-md-8 text-right">
				<form id="date_range" class="form-inline">
				<input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date">
				<input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date">
				<button type="button" class="btn btn-success btn-xs" id="submit_form_button">Submit</button>
				<button type="reset" class="btn btn-warning btn-xs" id="reset_form_button">Reset</button>
				</form>
	
			</div>
		</th>
		</tr>
        <tr>
           
            <th class="text-primary small bold">ID</th>
            <th class="text-primary small bold">Picture</th>
            <th class="text-primary small bold">Name</th>
            <th class="text-primary small bold">Email</th>
            <th class="text-primary small bold">Mobile</th>
            <th class="text-primary small bold">Action</th>
        </tr>
    </thead>
</table>
</div>
</div>
</div>
</div>
</body>  

</html>  