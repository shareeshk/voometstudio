<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" /> 
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js"></script>
<style>
.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
</style>
<script type="text/javascript">
var table;

$(document).ready(function (){
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    var rows_selected = [];
     table = $('#table_customer_profile').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Manage_cust/customer_profile_processing",
			//data: function (d) { d.vendor_id = $('#vendor_id').val();},
            type: "post",
            error: function(){
              $("#table_customer_profile_processing").css("display","none");
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
   
	$('#reset_form_button').on('click',function(){
		table.draw();
	});
	
	$("#submit_button").click(function() {
		//pcat_id=$('#pcat_id').val();
		table.draw();
	});

});	

function drawtable(obj){
	table.draw();
}

function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_delete_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
	swal({
			title: 'Are you sure?',
			text: "Delete Customer Profile",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Manage_cust/delete_customer_profile_selected",
		type:"post",
		data:"selected_list="+selected_list,
		
		success:function(data){
			$("#multiple_delete_button").html('Delete Selected Items');
			if(data==true){
				swal({
					title:"Deleted!", 
					text:"Given "+table+"(s) has been deleted successfully", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
				
			}
			else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});	
}
</script>
<script>
function showDivCreate(){
	
	document.getElementById('viewDiv1').style.display = "none";
	document.getElementById('createDiv2').style.display = "block";
	
}
function showDivEdit(){
	
	document.getElementById('viewDiv1').style.display = "block";
	document.getElementById('createDiv2').style.display = "none";
	
}
</script>
<?php //print_r($logistics);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">

<div class="row" id="viewDiv1">
	<div class="page-header">
		<h4 class="text-center bold">Customer Profile Name
		</h4>                        		
		                       	
	</div>
<table id="table_customer_profile" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<th colspan="6">
			<div class="col-md-12"><button id="multiple_delete_button" class="btn btn-danger btn-xs" onclick="multilpe_delete_fun('logistics')">Delete</button> <button id="multiple_delete_button4" class="btn btn-info btn-xs" onclick="showDivCreate()">Go to Create</button></div>
		</th>
	</tr>
	<tr>
		<th class="text-primary small bold"><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
		<th class="text-primary small bold">Profile Name </th>
		<th class="text-primary small bold">Sort Order</th>
		<th class="text-primary small bold">View</th>
		<th class="text-primary small bold">Last Updated</th>
		<th class="text-primary small bold">Action</th>
	</tr>
</thead>

</table>
</div>

<div class="row-fluid" id="createDiv2" style="display:none;">
<div class="col-md-6 col-md-offset-3">
<div class="page-header">
				<h4 class="text-center title">Create Customer Profile</h4>
			</div>

		
		<form name="create_customer_profile" id="create_customer_profile" method="post" action="#" onsubmit="return form_validation();" class="form-horizontal">
		<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				profile_name: {
					required: true,
				},
				sort_order: {
					required: true,
				},
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>	
			
		<div class="tab-content">
		
			<div class="form-group label-floating">
				<div class="col-md-10 col-md-offset-1">  
					<input id="profile_name" name="profile_name" type="text" class="form-control" placeholder="Enter Customer Profile Name"/>
				</div>
			</div>
			
			<div class="form-group label-floating">
				<div class="col-md-10 col-md-offset-1">  
					<input id="sort_order" name="sort_order" type="number" min="0" class="form-control" placeholder="Enter the Sort Order"/>
				</div>
			</div>

				
					<div class="form-group">
					<div class="col-md-5 col-md-offset-1">Show customer profile</div>
					<div class="col-md-3">
						<label class="radio-inline"><input type="radio" name="view" id="view1" value="1" checked="checked"/>View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input type="radio" name="view" id="view2" value="0"/>Hide</label>
					</div>
					</div>

											
			<div class="form-group">
				<div class="col-md-8 col-md-offset-3"> 
					<button class="btn btn-info btn-xs" type="submit">Submit</button>
					<button class="btn btn-info btn-xs" type="reset">Reset</button>
					<button class="btn btn-info btn-xs" type="button" onclick="showDivEdit()">Go to View</button>
				</div>
			</div>
						
		</div>
		</form>

</div>
</div>
</div>
</div>
</body>
</html>
<script type="text/javascript">


function form_validation()
{
	var profile_name = $('input[name="profile_name"]').val().trim();
	var sort_order = $('input[name="sort_order"]').val().trim();
	var view = $('input[name="view"]').val();

	   var err = 0;
		if(!(profile_name.length>=3))
		{
		    
		   err = 1;
		}
		else{			
			
		}
		
		if(sort_order==''){
			
			err = 1;
		}else{			
			
		}
		
		
		if(err==1)
		{
			   $('#error_result_id').show();
			   $('#result').hide();
		}
		else
			{
var form_status = $('<div class="form_status"></div>');		
var form = $('#create_customer_profile');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {		
				$.ajax({
				url: '<?php echo base_url()."admin/Manage_cust/add_customer_profile/"?>',
				type: 'POST',
				data: $('#create_customer_profile').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {

				if(data)
				{
					swal({
						title:"Success", 
						text:"customer profile is successfully added!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();
						

					});
				}
				else
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
			});
	 }
}]);
			}
	
	return false;
}

</script>
