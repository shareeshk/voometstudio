<html>
<head>
<meta charset="utf-8">
<title>Customer Profile</title>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" /> 
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<style type="text/css">
.text-color{
	color: #0015ff;
}
.panel-body{
	overflow-y:hidden;
	height:100px;
}
</style>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="tab-content">

<div id="tab-1" class="tab-pane fade in active">
<div class="page-header">
<h4 class="text-center bold">Profile details of <?php echo $personal_info->name; ?></h4>
</div>
<div class="table-responsive">
<table class="table table-bordered table-striped">
<tr>
<td>Gender</td>
<td><?php echo $personal_info->gender;?></td>
</tr>
<tr>
<td>Email Id: </td>
<td ><?php echo $personal_info->email;?></td>
</tr>
<tr>
<td>Mobile Number:</td>
<td><?php echo $personal_info->mobile;?></td>
</tr >
<tr>
<td>Last Login:</td>
<td><?php echo $personal_info->current_login;?></td>
</tr>
<tr>
<td>Active</td>
<td><?php echo ($personal_info->active==1) ? "Active" : "In Active";?></td>
</tr>
<?php
if($skill_material){
$skill_material_arr=explode(",",$skill_material);
foreach($skill_material_arr as $sk_mat){
$get_skill_material_info_arr=$controller->get_skill_material_info($sk_mat);
?>
<tr>
<td><?php echo $get_skill_material_info_arr["profile_name"]?></td>
<td><?php echo $get_skill_material_info_arr["profile_value"]?></td>
</tr>
<?php
}
?>
<?php
}else{
?>
<tr>
<td><?php echo "Profile" ?></td>
<td><?php echo "Not updated" ?></td>
</tr>
<?php
}
?>
<tr>
<?php if($personal_info->active==1){?>
<td>De-activate</td>
<td><button type="button" class="btn btn-danger btn-xs btn-block" onclick="deactivate_user_form();">De-activate</button></td>
<?php }else{ ?>
<td>Activate</td>
<td><button type="button" class="btn btn-success btn-block" onclick="activate_user_check(<?php echo $personal_info->id; ?>);">Re-activate</button></td>
<?php } ?>
</tr>
</table>
<div class="panel-group" id="accordion">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a style="text-decoration:none;" data-toggle="collapse" data-parent="#accordion" href="#collapse1">Deactivate histroy</a>
</h4>
</div>
<div id="collapse1" class="panel-collapse collapse ">
<div class="panel-group" >
<?php foreach ($deactivate_history_details as $deactivate_history_details_value) {  ?>	
<div class="panel panel-default">
<div class="panel-heading">
<div class="col-md-6"><?php echo $deactivate_history_details_value->deactivate_reason;?></div>
<div class="col-md-3"><?php 
if($deactivate_history_details_value->blocked=='yes'){
echo "blocked";
}else if($deactivate_history_details_value->blocked=='no'){
echo "unblocked";
}
?>
</div>
<div class="col-md-3"><?php echo date("D j M, Y - h:i a",strtotime($deactivate_history_details_value->timestamp));?></div>
<div class="panel-body">
<div class="col-md-6"><b>Deactivate comments:</b> 

<?php
		
	if(strlen($deactivate_history_details_value->deactivate_comments)>200){
		echo '<div class="f-row margin-top more">'.str_split($deactivate_history_details_value->deactivate_comments,200)[0].'<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' .substr($deactivate_history_details_value->deactivate_comments,201). '</span>&nbsp;&nbsp;<a href="#" class="morelink" onclick="morelinkfun(this)">more</a></span></div>';
	}else{
		echo '<div class="f-row margin-top more">'.$deactivate_history_details_value->deactivate_comments.'</div>';
	}
	
?>

</div>
<div class="col-md-6">

<b>Reactivate comments:</b> 

<?php
		
	if(strlen($deactivate_history_details_value->reactivate_comments)>200){
		echo '<div class="f-row margin-top more">'.str_split($deactivate_history_details_value->reactivate_comments,200)[0].'<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' .substr($deactivate_history_details_value->reactivate_comments,201). '</span>&nbsp;&nbsp;<a href="#" class="morelink" onclick="morelinkfun(this)">more</a></span></div>';
	}else{
		echo '<div class="f-row margin-top more">'.$deactivate_history_details_value->reactivate_comments.'</div>';
	}
	
?>

</div>
</div>				
</div>
</div>
<?php } ?>
</div>
</div>
</div>
</div> 
<form name="deactivate_form" id="deactivate_form" method="post">
<table  id="myTablesDEACTIVATE"  style="display:none;" cellspacing="0" width="100%">
<tr>
<td><h3>DEACTIVATE ACCOUNT</h3></td>
</tr>
<tr class="col-md-12 info">
<td class="col-md-6">
<select class="form-control" name="deactivate_reason" id="deactivate_reason">
<option value="">-Select Reason-</option>
<?php foreach ($deactivate_reason as $deactivate_reason_value) {  ?>
<option value="<?php echo $deactivate_reason_value->deactivate_reason; ?>"><?php echo $deactivate_reason_value->deactivate_reason; ?></option>
<?php } ?>
</select>
</td>
</tr><br>
<tr class="col-md-12 info">
<td class="col-md-6" colspan="2">
<textarea name="comments" id="comments" class="form-control" placeholder="write comments" ></textarea>
</td>
</tr>
<tr class="info">
<td><input type="checkbox" name="block_user" id="block_user" checked>Block the account</td>
</tr>
<tr class="info">
<td><button type="button" class="btn btn-primary" onclick="deactivate_user(<?php echo $personal_info->id; ?>)">submit</button></td>
</tr>
</table>
</form>
<form name="activate_form" id="activate_form" method="post">
<table  id="myTablesREACTIVATE"  style="display:none;" cellspacing="0" width="100%">
<div class="col-md-12">
<div class="col-md-6">
<tr class="info">
<td><h3>REACTIVATE ACCOUNT</h3></td>
</tr><br>
<tr class="col-md-12 info">
<td class="col-md-6">
<textarea name="reactivate_comments" id="reactivate_comments" class="form-control" placeholder="write comments" ></textarea>
</td>
</tr>
<tr class="info">
<td><button type="button" class="btn btn-primary" onclick="activate_user(<?php echo $personal_info->id; ?>)">submit</button></td>
</tr>
</div>
</div>
</table>
</form>

<div id="reactivate_customer_cnt" style="display:none">
Customer deactivated his account,there is no option to reactivate his account by you.Customer has to login through his email_id & password & reactivate his account.
</div>
</div>
</div>
<script>
function deactivate_user_form(){
	$("#myTablesDEACTIVATE").css({"display":""});
}

</script>
<script>

function deactivate_user(id){

var url = '<?php echo base_url('admin/Manage_cust/de_activate_customer');?>';
deactivate_reason=$("#deactivate_reason").val();
comments=$("#comments").val();

if($("#block_user").is(":checked")){
	block_user="1";
}
else{
	block_user="0";
}
$.ajax({
url:url,
type:'POST',
data:'id='+id+"&deactivate_reason="+deactivate_reason+"&comments="+comments+"&block_user="+block_user,
}).success(function (data){
	//alert(data);
	if(data==true){
		
		alert('De activated Successfully');
		location.reload();
		
	}
	
});
document.getElementById("deactivate_form").reset();	
}

function activate_user(id){

var url = '<?php echo base_url('admin/Manage_cust/activate_customer');?>';
reactivate_comments=$("#reactivate_comments").val();

$.ajax({
url:url,
type:'POST',
data:'id='+id+"&reactivate_comments="+reactivate_comments,
}).success(function (data){
	if(data==true){
		alert('Activated Successfully');
		location.reload();
	}
	
});

document.getElementById("activate_form").reset();	
}


</script>
<script type="text/javascript">
function activate_user_check(id){

var url = '<?php echo base_url('admin/Manage_cust/deactivate_history_check');?>';

$.ajax({
url:url,
type:'POST',
data:'id='+id,
}).success(function (data){
	alert(data);
	if(data=="yes"){
		$("#myTablesREACTIVATE").css({"display":""});
	}
	if(data=="no"){
		$("#myTablesREACTIVATE").css({"display":"none"});
		$("#reactivate_customer_cnt").css({"display":""});
	}
	
});

}
</script>
<div id="tab-2" class="tab-pane fade">
<div class="page-header">
<h4 class="text-center bold">Addresses</h4>
</div>
	<div class="table-responsive">
		<table id="myTables2" class="table table-bordered table-striped" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th class="text-primary small bold">Sl.no</th>
				<th class="text-primary small bold">Name & Contact</th>
				<th class="text-primary small bold">Addressess</th>
			</tr>
			</thead>
			<tbody>
			<?php 
			$i=1;
			if(count($addresses)>0){
				
				foreach($addresses as $addresse_value){?>
				
					<tr>
						<td><?php echo $i ;?></td>
					<?php 
						echo '<td><ul class="list-unstyled"><li class="text-muted">Name: '.$addresse_value->customer_name.'</li> <li class="text-muted">Mobile: '.$addresse_value->mobile.'</li><li class="text-muted"> Last Updated: '.$addresse_value->timestamp.'</li></ul></td>';
						
						echo '<td><ul class="list-unstyled"><li class="text-muted">'.$addresse_value->address1.',</li><li class="text-muted">'.$addresse_value->address2.',</li><li class="text-muted">'.$addresse_value->city.' '.$addresse_value->pincode.'</li><li class="text-muted">'.$addresse_value->state.', '.$addresse_value->country.'</li></ul></td>';
						?>
					</tr>
				<?php 
				$i++ ;
				} 
			}
			?>
			</tbody>
		</table>
	</div>
</div>


<div id="tab-3" class="tab-pane fade">
<div class="page-header">
<h4 class="text-center bold">Wishlists</h4>
</div>
	<table id="myTables3" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="text-primary small bold">Product Image</th>
				<th class="text-primary small bold">Product Details</th>
				<th class="text-primary small bold">Last Added</th>											
			</tr>
		</thead>
		<tbody>
		<?php 
		if(count($wishlist>0)){
			foreach($wishlist as $wishlist_value ){
                            
                                $pro_name=($wishlist_value->sku_name!='') ? $wishlist_value->sku_name : $wishlist_value->product_name;
				echo '<tr>';
				echo '<td><img src="'.base_url().$wishlist_value->product_image.'" style="width:100px;height:100px;"></td>';
				echo '<td><ul class="list-unstyled"><li class="text-muted">Product: '.$pro_name.'</li><li class="text-muted">Price: '.$wishlist_value->product_price.'</li></ul></td>';
				echo '<td>'.$wishlist_value->timestamp.'</td>';
				echo '</tr>';
			}
		}
		?>

		</tbody>
	</table>
</div>

<div id="tab-4" class="tab-pane fade">
<div class="page-header">
<h4 class="text-center bold">Ratings</h4>
</div>
	<div class="table-responsive">
		<table id="myTables4" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="text-primary small bold">Rated Inventory</th>
				<th class="text-primary small bold">Review</th>
				<th class="text-primary small bold">Ratings and Status</th>	
			</tr>
		</thead>
		<tbody>
		<?php
		foreach($reviews_and_ratings as $reviews_and_ratings_value){
                    
                    $pro_name=($reviews_and_ratings_value->sku_name!='') ? $reviews_and_ratings_value->sku_name : $reviews_and_ratings_value->product_name;
			echo '<tr>';
			echo '<td width="30%"><ul class="list-unstyled"><li><img src="'.base_url().$reviews_and_ratings_value->thumbnail.'" style="width:100px;:height:100px;"></li> <li>Name: '.$pro_name.'</li><li>Price: '.$reviews_and_ratings_value->selling_price.'</li></ul></td>';
			echo '<td width="50%"><b><span class="text-color"> Title:&nbsp;</span>'.$reviews_and_ratings_value->review_title.'</b><br><span class="text-color"> Description:&nbsp;</span>';
	
			if(strlen($reviews_and_ratings_value->review)>200){
				echo '<div class="f-row margin-top more">'.str_split($reviews_and_ratings_value->review,200)[0].'<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' .substr($reviews_and_ratings_value->review,201). '</span>&nbsp;&nbsp;<a href="#" class="morelink" onclick="morelinkfun(this)">more</a></span></div>';
			}else{
				echo '<div class="f-row margin-top more">'.$reviews_and_ratings_value->review.'</div>';
			}
			
			echo '</td>';
			
			
			echo '<td width="10%"><ul class="list-unstyled"><li>';
				$expression = $reviews_and_ratings_value->rating;
				switch ($expression) {
					case '1':
						echo '<img src="' . base_url() . 'assets/smileys_email/0.png" height="30px" weight="30px">';
						break;
					case '2':
						echo '<img src="' . base_url() . 'assets/smileys_email/1.png" height="30px" weight="30px">';
						break;
					case '3':
						echo '<img src="' . base_url() . 'assets/smileys_email/2.png" height="30px" weight="30px">';
						break;
					case '4':
						echo '<img src="' . base_url() . 'assets/smileys_email/3.png" height="30px" weight="30px">';
						break;
					case '5':
						echo '<img src="' . base_url() . 'assets/smileys_email/4.png" height="30px" weight="30px">';
						break;
					default:
						
				}
				switch ($expression) {
					case '1':
						?>
							<span style="color:red">&#9733;</span>&#9734;&#9734;&#9734;&#9734;
						<?php
						break;
					case '2':
						?>
							<span style="color:red">&#9733;&#9733;</span>&#9734;&#9734;&#9734;
						<?php
						break;
					case '3':
						?>
							<span style="color:red">&#9733;&#9733;&#9733;</span>&#9734;&#9734;
						<?php
						break;
					case '4':
						?>
							<span style="color:red">&#9733;&#9733;&#9733;&#9733;</span>&#9734;
						<?php
						break;
					case '5':
						?>
							<span style="color:red">&#9733;&#9733;&#9733;&#9733;&#9733;</span>
						<?php
						break;
					default:
						
				}
				echo '</li>';
			$flag=$reviews_and_ratings_value->status;
			if($flag==2){$status="Pending";}if($flag==1){$status="Allowed";}if($flag==0){$status="Blocked";}
			echo '<li>'.$status.'</li><li>'.$reviews_and_ratings_value->posteddate.'</li></ul>';	
			echo '</td>';
			echo '</tr>';
		}
		?>
		</tbody>
		</table>
	</div>
</div>

<div id="tab-5" class="tab-pane fade">
<div class="page-header">
<h4 class="text-center bold">Bank Details</h4>
</div>
	<div class="table-responsive">
		<table id="myTables5" class="table table-bordered table-striped" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th class="text-primary small bold">s.no</th>
					<th class="text-primary small bold">Account Holder</th>
					<th class="text-primary small bold">Account Number</th>
					<th class="text-primary small bold">IFSC Code</th>
					<th class="text-primary small bold">Bank Name</th>
					<th class="text-primary small bold">Branch Name</th>
					<th class="text-primary small bold">City</th>
					<th class="text-primary small bold">Last Added</th>
				</tr>
			</thead>
			<tbody>

				<?php 
				$j=1;
				if(isset($bank_details)){
					foreach($bank_details as $bank_details_value){
						echo '<tr>';
						echo '<td>'.$j.'</td>';
						echo '<td><ul class="list-unstyled"><li>Name: '.$bank_details_value["account_holder_name"].'</li><li>Mobile: '.$bank_details_value["mobile_number"].'</li></ul></td>';
						echo '<td>'.$bank_details_value["account_number"].'</td>';
						echo '<td>'.$bank_details_value["ifsc_code"].'</td>';
						echo '<td>'.$bank_details_value["bank_name"].'</td>';
						echo '<td>'.$bank_details_value["branch_name"].'</td>';
						echo '<td>'.$bank_details_value["city"].'</td>';
						echo '<td>'.$bank_details_value["timestamp"].'</td>';

						echo '</tr>';
					
					$j++;
					}
				}
				
				?>
			</tbody>
		</table>
	</div>
</div>

<div id="tab-6" class="tab-pane fade">
<div class="page-header">
<h4 class="text-center bold">Saved Cards</h4>
</div>
	<div class="table-responsive">
	<table id="myTables6" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="text-primary small bold">s.no</th>
				<th class="text-primary small bold">Card No</th>
				<th class="text-primary small bold">Card Type</th>
				<th class="text-primary small bold">Name On Card</th>
				<th class="text-primary small bold">Card Label</th>
				<th class="text-primary small bold">Expiry On</th>
				<th class="text-primary small bold">Default Card</th>
				<th class="text-primary small bold">Last Added</th>
			</tr>
		</thead>
		<tbody>
		
			<?php 
			$k=1;
			if(count($saved_cards)!=0){
				foreach($saved_cards as $cards_value){
					echo '<tr>';
					echo '<td>'.$k.'</td>';
					echo '<td>'.$cards_value->card_number.'</td>';
					echo '<td>'.$cards_value->card_type.'</td>';
					echo '<td>'.$cards_value->name_on_card.'</td>';
					echo '<td>'.$cards_value->bank_name.'</td>';
					echo '<td>'.$cards_value->exp_month.'/'.$cards_value->exp_year.'</td>';
					echo '<td>'.$cards_value->card_default.'</td>';
					echo '<td>'.$cards_value->timesatmp.'</td>';
					echo '</tr>';
				
				$k++;
				}
			}
			
			?>
		</tbody>
	</table>

	</div>
</div>
<div id="tab-7" class="tab-pane fade">
<div class="page-header">
<h4 class="text-center bold">Wallet transactions</h4>
</div>
	<div class="table-responsive">
	<table id="myTables7" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="text-primary small bold">Sl.no</th>
				<th class="text-primary small bold">Wallet Info</th>
				<th class="text-primary small bold">Wallet Amount</th>
				<th class="text-primary small bold">Wallet Transactions</th>
			</tr>
		</thead>
		<tbody>
		
			<?php 
			if(count($wallet_details)!=0){
			$l=1;
			echo '<tr>';

				foreach($wallet_details as $wallet_value){
					
					echo '<td>'.$l.'</td>';
					echo '<td>'.$wallet_value->wallet_info.'</td>';
					echo '<td>'.$wallet_value->wallet_amount.'</td>';
					
					if(count($wallet_transaction_details)>0 && !empty($wallet_transaction_details)){
						echo '<td>';
						
						foreach($wallet_transaction_details as $transaction_value){
							
							echo '<span class="text-color"> Details:&nbsp;</span>'.$transaction_value->transaction_details.'<br>';
							echo '<span class="text-color"> Debit:&nbsp;</span>'.$transaction_value->debit.'<br>';
							echo '<span class="text-color"> Credit:&nbsp;</span>'.$transaction_value->credit.'<br>';
							echo '<span class="text-color"> Amount:&nbsp;</span>'.$transaction_value->amount.'<br>';
							echo '<span class="text-color"> Updated On:&nbsp;</span>'.$transaction_value->timestamp;
							echo '<hr style="border:1px dotted #333;">';
							
						}
						
						echo '</td>';
					}else{
						echo '<td>';
						echo 'No transaction details Available';
						echo '</td>';
					}

				$l++;
				}
			
			echo '</tr>';
		
			}

			?>
		</tbody>
	</table>

	</div>
</div>

<div id="tab-8" class="tab-pane fade">
<div class="page-header">
<h4 class="text-center bold">Order history</h4>
</div>
	<div class="table-responsive">
	<table id="myTables8" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="text-primary small bold">Image</th>
				<th class="text-primary small bold">Order Summary</th>
				<th class="text-primary small bold">Order Status</th>
				<th class="text-primary small bold">Quantity & Price</th>
				<th class="text-primary small bold">Delivered Address</th>
				
			</tr>
		</thead>
		<tbody>
		
		<?php	
			foreach($all_orders as $order_id=>$order_value){
				echo '<tr>';	
				
				
				foreach($order_value as $orders){
				
					foreach($orders as $order_details){
						echo '<div style="width:100%;margin-bottom: 10px;" class="col-md-12">';
		
						$completed_table_or_not="no";
						$returned_table_or_not="no";
						$replaced_table_or_not="no";
						if($order_details["status"]=="completed" || $order_details["status"]=="replaced"){
							$completed_table_or_not=$controller->get_in_completed_table_or_not($order_details["order_item_id"],$order_details["status"]);
							
							$replaced_table_or_not=$controller->get_in_replaced_table_or_not($order_details["order_item_id"],$order_details["status"]);
							
							
							if($completed_table_or_not=="yes" && $replaced_table_or_not=="yes" && $order_details["status"]=="replaced"){ continue;}else{ echo '</div><div style="width:100%;margin-bottom: 10px;" class="col-md-12">';}
						}
						
						if($order_details["status"]=="completed" || $order_details["status"]=="returned" ){
							$completed_table_or_not=$controller->get_in_completed_table_or_not($order_details["order_item_id"],$order_details["status"]);
							$returned_table_or_not=$controller->get_in_returned_table_or_not($order_details["order_item_id"],$order_details["status"]);
							
							if($completed_table_or_not=="yes" && $returned_table_or_not=="yes" && $order_details["status"]=="returned"){ continue;}else{ echo '</div><div style="width:100%;margin-bottom: 10px;" class="col-md-12">';}
						}

						$all_order_items=$controller->get_all_order_items($order_details["order_item_id"],$order_id);
						
						foreach($all_order_items as $order_item){
							
						echo '<td>';
							echo '<div style="width:130px;height:140px;float:left;"><img style="width:100px;height:100px;" src="'.base_url().$order_item['image'].'" alt="">'.'</div>';
						echo '</td>';
				
						echo '<td><ul class="list-unstyled">';
							echo '<li>SKU: '.$order_details["sku_id"].'</li>';
                                                        if($order_item['ord_sku_name']!=''){
                                                            echo '<li>'.$order_item['ord_sku_name'].'</li>';
                                                        }else{
                                                            echo '<li>'.$controller->get_product_name($order_item['product_id']).'</li>';
                                                        }
							echo '<li>'.$order_item['sku_name'].'</li>';
							echo '<li>Order Item Id: '.$order_details["order_item_id"].'</li>';
							echo '<li>Order Id: '.$order_details["order_id"].'</li>';
							echo '<li>Logistics Name: '.$order_item['logistics_name'].'</li>';
							echo '<li>Order Date: '.$order_details["timestamp"].'</li>';
						echo '</ul></td>';
						
						echo '<td><ul class="list-unstyled">';
							//////////replace///////////
							
							if($order_details["status"]=="completed" && $completed_table_or_not=="yes" && $replaced_table_or_not=="yes"){

							$replacement_decision_table=$controller->get_return_replacement_table_by_order_item_id($order_details["order_item_id"]);
							
							$replacement_desired=$controller->get_return_replacement_desired_order_item_id($order_details["order_item_id"]);
							
							echo '<li>Replaced Quantity:'.$replacement_decision_table['quantity'].'</li>';
							$str='';
							
	
									if(count($replacement_decision_table)!=0){
										
										$str.='<li>Replacement Item: '.curr_sym.($replacement_decision_table["quantity"]*$replacement_desired["replacement_value_each_inventory_id"])."</li>";
									
										$str.='<li>Total Purchased Item Price: '.curr_sym.($order_details["product_price"]*$replacement_decision_table["quantity"])."</li>";
										
										$str.='<li>Differential Item Price : '.curr_sym.($replacement_decision_table["balance_amount_item_price"])."</li>";
										
										$str.='<li>Shipping charge for '.$replacement_decision_table['quantity']." quantity : ".curr_sym.($replacement_decision_table["shipping_charge_replacement"])."</li>";
										
										$str.='<li>Balance Paid : '.curr_sym.($replacement_decision_table["balance_amount"])."</li>";
										
										
										if($replacement_desired["paid_by"]=="admin"){
											$str.='<li>Payment Mode : '.$replacement_desired["refund_method"]."</li>";
										}
										if($replacement_desired["paid_by"]=="customer"){
											$str.='<li>Payment Mode : '.$replacement_desired["balance_amount_paid_by"]."</li>";
										}
							
										echo $str;
							
									} 
								else{
								
									echo '<li>'.$controller->get_order_item_status_summary($order_item['order_item_id']).'</li>';	
								}
							}
							//////////refund/////////////
						
						if($order_details["status"]=="completed" && $completed_table_or_not=="yes" && $returned_table_or_not=="yes"){
								
							$return_refund_table=$controller->get_return_refund_table_by_order_item_id($order_details["order_item_id"]);
							
							if(count($return_refund_table)!=0){
							echo '<li>Refunded Quantity: '.$return_refund_table['quantity'].'</li>';
							
							echo '<li>Item Price Refunded: '.$return_refund_table['item_with_deductions_concession']." (".$return_refund_table['item_deduction_percentage']."% deducted)".'</li>';
							echo '<li>Shipping Price Refunded: '.$return_refund_table['shipping_with_deductions_concession']." (".$return_refund_table['shipping_deduction_percentage']."% deducted)".'</li>';
							echo '<li>Return Shipping Concession: '.$return_refund_table['return_shipping_concession'].'</li>';
							echo '<li>Other Concession: '.$return_refund_table['return_other_concession'].'</li>';
							echo '<li>Total: '.$return_refund_table['total_refund_with_concession']."</li>";
							
							echo '<li>Payment Mode : '.$return_refund_table["refund_method"]."</li>";
							
							}
						} 
							else{
							
								echo '<li>'.$controller->get_order_item_status_summary($order_item['order_item_id']).'</li>';	
							}
						echo '</ul></td>';
					
				
						echo '<td><ul class="list-unstyled">';
						echo '<li>Quantity: '.$order_item['quantity'].'</li>';
							echo '<li>Purchased Item Price: '.$order_item['product_price'].'</li>';
							echo '<li>shipping: '.$order_item['shipping_charge'].'</li>';
							echo '<li>Total: '.$order_item['grandtotal'].'</li>';
						echo '</ul></td>';
					
				
						echo '<td><ul class="list-unstyled">';
							$all_shipped_address_arr=$controller->all_shipped_address($order_item['shipping_address_id']);
							foreach($all_shipped_address_arr as $shipped_address){
								echo '<li>'.$shipped_address["customer_name"].'</li>';
								echo '<li>'.$shipped_address["address1"].'</li>';
								echo '<li>'.$shipped_address["address2"].'</li>';
								echo '<li>'.$shipped_address["city"].",".$shipped_address["state"].'</li>';
								echo '<li>'.$shipped_address["country"].'</li>';
								echo '<li>'.$shipped_address["pincode"].'</li>';
								echo '<li>'.$shipped_address["mobile"].'</li>';
						
							}
						echo '</ul></td>';
						}
						
					}
					/////////////
				
				}
				
				echo '</tr>';
			}
			
		?>
	
		</tbody>
	</table>

	</div>
</div>

<div id="tab-9" class="tab-pane fade">
<div class="page-header">
<h4 class="text-center bold">Service ratings</h4>
</div>
	<div class="table-responsive">
	<table id="myTables9" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="text-primary small bold">Shipped Customer</th>
				<th class="text-primary small bold">Order Item Id</th>
				<th class="text-primary small bold">Delivered On</th>
				<th class="text-primary small bold">Rating</th>
				<th class="text-primary small bold">Logistics</th>      
				<th class="text-primary small bold">View Details</th>
       		</tr>
        </thead>
		<tbody>
	<?php
	$str='';
	foreach($service_ratings as $service_ratingsObj){
            $pro_name=($service_ratingsObj->sku_name!='') ? $service_ratingsObj->sku_name : $service_ratingsObj->product_name;
		$str.='<tr>';
		$str.='<td><ul class="list-unstyled"><li>'.$service_ratingsObj->customer_name.'</li>';
		$str.='<li>Shipped Address</li><li>'.$service_ratingsObj->shipped_address1.'</li><li>'.$service_ratingsObj->shipped_address2.'</li><li>'.$service_ratingsObj->shipped_city.'</li><li>'.$service_ratingsObj->shipped_state.'</li><li>'.$service_ratingsObj->shipped_country.'</li>'.'</ul></td>';
	
		$str.='<td><ul class="list-unstyled"><li>Order Item ID: '.$service_ratingsObj->order_item_id.'</li>';	
		$str.='<li>SKU ID: '.$service_ratingsObj->sku_id.'</li>';
		$str.='<li>Product Name: '.$pro_name.'</li></ul></td>';
		$dt = new DateTime($service_ratingsObj->order_delivered_timestamp);
		$date = $dt->format('d-m-Y');
		$time = $dt->format('H:i:s');
		$str.='<td>'.$date. ' | '. $time.'</td>';
		$expression = $service_ratingsObj->rating;
				$rate='';
				switch ($expression) {
					case '1':
						$rate.='<img src="' . base_url().'assets/smileys_email/0.png" height="30px" weight="30px">';
						break;
					case '2':
						$rate.='<img src="' . base_url().'assets/smileys_email/1.png" height="30px" weight="30px">';
						break;
					case '3':
						$rate.='<img src="' . base_url().'assets/smileys_email/2.png" height="30px" weight="30px">';
						break;
					case '4':
						$rate.='<img src="' . base_url().'assets/smileys_email/3.png" height="30px" weight="30px">';
						break;
					case '5':
						$rate.='<img src="' . base_url().'assets/smileys_email/4.png" height="30px" weight="30px">';
						break;
					default:
						
				}
				switch ($expression) {
					case '1':
							$rate.='<span style="color:red">&#9733;</span>&#9734;&#9734;&#9734;&#9734';
						break;
					case '2':						
							$rate.='<span style="color:red">&#9733;&#9733;</span>&#9734;&#9734;&#9734';
						break;
					case '3':			
							$rate.='<span style="color:red">&#9733;&#9733;&#9733;</span>&#9734;&#9734';
						
						break;
					case '4':
							$rate.='<span style="color:red">&#9733;&#9733;&#9733;&#9733;</span>&#9734';			
						break;
					case '5':						
							$rate.='<span style="color:red">&#9733;&#9733;&#9733;&#9733;&#9733;</span>';						
						break;
					default:
						
				}
				$str.='<td>'.$rate.'</td>';
				
				$str.='<td><ul class="list-unstyled"><li>Vendor Name: '.$service_ratingsObj->name.'</li>';
				$str.='<li>Logistics Name: '.$service_ratingsObj->logistics_name.'</li></td>';
				$str.="<td><input type=\"button\" value=\"Show Survey Result\" class=\"btn btn-md btn-info btn-xs btn-block\" data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"showSurveyFun('$service_ratingsObj->rateorder_id','$service_ratingsObj->inventory_id','$service_ratingsObj->message')\"></td>";	
	
				
		$str.='</tr>';
	}
	echo $str;
	?>
		
		</tbody>
	</table>
	</div>
</div>

</div>
</div>
</div>

  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Survey Result</h4>
        </div>
        <div class="modal-body">
			<div class="row">
				<div class="col-md-10"  id="survey_result"></div>
			</div>
					</div>
					<div class="modal-footer">
					  <button type="button" class="text-primary bold" data-dismiss="modal">Close</button>
					</div>
					
				  </div>
				  
				</div>
  </div>
</div>
</div>
</body>
<script>

$(document).ready(function() {
    $('#myTables2').dataTable({"order": [[ 2, "asc" ]] });
    $('#myTables3').dataTable({"order": [[ 2, "desc" ]] });
    $('#myTables4').dataTable({"order": [[ 2, "desc" ]] });
    $('#myTables5').dataTable({"order": [[ 2, "desc" ]] });
    $('#myTables6').dataTable({"order": [[ 2, "asc" ]] });
    $('#myTables7').dataTable({"order": [[ 2, "desc" ]] });
    $('#myTables8').dataTable({"order": [[ 2, "asc" ]] });
    $('#myTables9').dataTable({"order": [[ 2, "asc" ]] });
});

function showSurveyFun(rateorder_id,inventory_id,msg){

	$.ajax({
		url:"<?php echo base_url()?>admin/Ratemanaging/show_survey_result",
		type:"post",
		data:{rateorder_id:rateorder_id,inventory_id:inventory_id},
		success:function(data){
			$("#survey_result").html(data);
		}
	});
} 
</script>

</html>