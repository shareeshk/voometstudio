<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">
       $(document).ready(function () {
          
		   $.ajax({
			   url:"<?php echo base_url()?>admin/Catalogue/archived_parent_category_count",
			   type:"POST",
			   success:function(data){
				   
				   $("#parent_category_count").text(data);
			   }
		   })
			

       });    
</script>
<script type="text/javascript">

$(document).ready(function(){
    $('#myTable').dataTable({ stateSave: true });
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
});
function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_restore_fun(table_name){
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table_name+"!");
		return false;
	}
	selected_list=selected_list_arr.join(",");
	swal({
			title: 'Are you sure?',
			text: "Restore Archived Parentcategory",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, restore it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
			setTimeout(function() {
		$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/update_parent_category_selected",
		type:"post",
		data:"selected_list="+selected_list+"&active=1",
		
		success:function(data){
			if(data==true){
				swal({
					title:"Restored!", 
					text:"Given "+table_name+"(s) has been restored successfully", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
		});
			}, 2000)
			})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
</script>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">

<div class="row">
	<div class="wizard-header text-center">
		<h5> Number of Archived Parent category <span class="badge"><div id="parent_category_count"></div></span> 
		</h5>                        		               	
	</div>
</div>
<div class="row" id="viewDiv1">
<table id="myTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
			<tr>
				<th colspan="5">
					<div class="padding_right"><button id="multiple_delete_button" class="btn btn-success btn-xs" onclick="multilpe_restore_fun('parent category')">Restore</button></div>
				</th>
			</tr>
			
			<tr>
				<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
				<th>S. No</th>
				<th>Name </th>
				<th>Details</th>
				<th>Last Updated</th>
			</tr>
</thead>
<tbody>
<?php
$i=0;
foreach($parent_catagories as $parent_catagories_value){ 
$i++; ?>
<tr>
	<td><input type="checkbox" name="selected_list" value="<?php echo $parent_catagories_value->pcat_id; ?>"></td>
	<td><?php echo $i; ?></td>
	<td><?php echo $parent_catagories_value -> pcat_name; ?></td>
	<td>
	<?php 
	echo "Menu Level :". $parent_catagories_value -> menu_level.'<br>';
	echo "Show Sub Menu :". $parent_catagories_value -> show_sub_menu.'<br>';
	echo "Sort Order :".  $parent_catagories_value -> menu_sort_order.'<br>';
	echo "View :".  $parent_catagories_value -> view; 
	 ?>
	</td>
	<td><?php echo $parent_catagories_value -> timestamp?></td>
</tr>
<?php } ?>
</tbody>
</table>
</div>

</div>
</div>
</body>
</html>