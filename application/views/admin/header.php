<?php
	if(!$this->session->userdata("user_id")){
		if(base_url(uri_string())!=base_url()."Administrator"){
			header("location:".base_url()."Administrator");
			exit;
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon/VS_favicon.ico">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min_latest.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome.min.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" />

<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.1.min.js"></script>	
    <title>Admin Login</title>
</head>
<body class="product-page">
<!-- HEADER -->
<div id="header" class="header">
  

</div>
<!-- end header -->