<html>
<head>
<meta charset="utf-8">
<title>Admin Settings</title>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<style>
.wizard-card{
	box-shadow:none;
}
.form-group{
	margin-top:15px;
}

</style>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="row">
<div class="col-md-12">
<div class="wizard-card" data-color="green" id="wizardProfile">	
<form name="admin_settings" id="admin_settings" method="post" action="#" onsubmit="return form_validation();">
<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				adm_combo_pack_min_items: {
					required: true,
					
				},
				adm_combo_pack_discount: {
					required: true,
				},
			
				adm_combo_pack_shipping_charge: {
					required: true,
				},
				
				adm_combo_pack_status: {
					required: true,
		      
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
			</script>

	<div class="col-md-6 col-md-offset-3">
		
		<div class="row">
			<div class="col-md-10 text-center">		
				<h4>Add-on Settings</h4>
			</div>
		</div>
			
			<?php 
				$adm_combo_pack_min_items='1';
				$adm_combo_pack_discount='0';
				$adm_combo_pack_shipping_charge='0';
				$adm_combo_pack_status='inactive';
				$adm_addon_product_allowed='';
				$adm_request_for_quotations_min_items='';
				$adm_request_for_quotations_max_items='';

				//print_r($adm_settings);

				if(!empty($adm_settings)){
					
					$adm_combo_pack_min_items=$adm_settings->adm_combo_pack_min_items;
					$adm_combo_pack_discount=$adm_settings->adm_combo_pack_discount;
					$adm_combo_pack_shipping_charge=$adm_settings->adm_combo_pack_shipping_charge;
					$adm_combo_pack_status=$adm_settings->adm_combo_pack_status;	
					$adm_addon_product_allowed=$adm_settings->adm_addon_product_allowed;	
					$adm_request_for_quotations_min_items=$adm_settings->adm_request_for_quotations_min_items;	
					$adm_request_for_quotations_max_items=$adm_settings->adm_request_for_quotations_max_items;	
				}
			?>

			<div class="row">
				<div class="col-md-10">	
					<div class="form-group label-floating">
					<label class="control-label">Addon Products Max Attach Limitations</label>
					<input type="text" name="adm_addon_product_allowed" id="adm_addon_product_allowed" class="form-control" value="<?php echo round($adm_addon_product_allowed); ?>" onKeyPress="return isNumber(event)">
						
					</div>
				</div>
			</div>

			
			<div class="row">
				<div class="col-md-10">	
					<h4 class="text-center">RFQ - Settings</h4>
				</div>
			</div>

			<div class="row">
				<div class="col-md-10">	
					<div class="form-group label-floating">
					<label class="control-label">Request For Quotation Min items</label>
					<input type="text" name="adm_request_for_quotations_min_items" id="adm_request_for_quotations_min_items" class="form-control" value="<?php echo round($adm_request_for_quotations_min_items); ?>" onKeyPress="return isNumber(event)">
						
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10">	
					<div class="form-group label-floating">
					<label class="control-label">Request For Quotation Max items</label>
					<input type="text" name="adm_request_for_quotations_max_items" id="adm_request_for_quotations_max_items" class="form-control" value="<?php echo round($adm_request_for_quotations_max_items); ?>" onKeyPress="return isNumber(event)">
						
					</div>
				</div>
			</div>
			
			<!--- quickbuy or combo cart settings --->

			<div class="row">
				<div class="col-md-10">	
					<h4 class="text-center">Combo or Quickbuy Cart Settings</h4>
				</div>
			</div>

			<div class="row">
				<div class="col-md-10">	
					<div class="form-group label-floating">
					<label class="control-label">Combo Pack Min items</label>
					<input type="text" name="adm_combo_pack_min_items" id="adm_combo_pack_min_items" class="form-control" value="<?php echo round($adm_combo_pack_min_items); ?>" onKeyPress="return isNumber(event)">
						
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10">	
					<div class="form-group label-floating">
					<label class="control-label">Combo Pack discount</label>
					<input type="text" name="adm_combo_pack_discount" id="adm_combo_pack_discount" class="form-control" value="<?php echo round($adm_combo_pack_discount); ?>" onKeyPress="return isNumber(event)">
						
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10">	
					<div class="form-group label-floating">
					<label class="control-label">Combo Pack Shipping Charge</label>
					<input type="text" name="adm_combo_pack_shipping_charge" id="adm_combo_pack_shipping_charge" class="form-control" value="<?php echo round($adm_combo_pack_shipping_charge); ?>" onKeyPress="return isNumber(event)">
						
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-10">	
					<div class="form-group label-floating">
					<label class="control-label">Combo Pack Status</label>
					<select name="adm_combo_pack_status" id="adm_combo_pack_status" class="form-control" >
						
						<option value="active" <?php echo ($adm_combo_pack_status=='active') ? 'selected' : ''; ?> >Active</option>
						<option value="inactive" <?php echo ($adm_combo_pack_status=='inactive') ? 'selected' : ''; ?>>In Active</option>
					</select>
						
					</div>
				</div>
			</div>

			<!--- quickbuy or combo cart settings --->

			<div class="row">
				<div class="col-md-10">	

					<h4 class="text-center">Marquee settings</h4>
				</div>
			</div>

			<?php 
			
			if(!empty($marquee)){
				for($i=0;$i<5;$i++){
					$id=$marquee[$i]['id'];
				?>

				<input type="hidden" name="id[]" value="<?php echo $marquee[$i]['id']; ?>">
			<div class="row">
				<div class="col-md-10">	
					<div class="form-group label-floating">
					<label class="control-label">Anouncement <?php echo ($i+1); ?></label>
					<input type="text" name="anouncement[<?php echo $id; ?>]" id="anouncement_<?php echo $i; ?>" class="form-control" value="<?php echo $marquee[$i]['text']; ?>">
					</div>
				</div>
			</div>

				<?php 	
				$status=$marquee[$i]['status'];
				?>
			<div class="row">
				<div class="col-md-10"> 
					<div class="form-group">
					<div class="col-md-5">Status of Anouncement <?php echo ($i+1); ?></div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="status[<?php echo $id; ?>]" id="status_<?php echo $i; ?>" value="1" type="radio" <?php if($status=="1"){echo "checked";}?>>View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="status[<?php echo $id; ?>]" id="status_<?php echo $i; ?>" value="0" type="radio" <?php if($status=="0"){echo "checked";}?>>Hide</label>
					</div>
				
					</div>
				</div>
			</div>
			<br><br>

				<?php
				}
			}

			?>

			
			<div class="row">	
				<div class="col-md-10 text-center">
					<button type="submit" class="btn btn-info" id="submit_button">Submit</button>
					<button class="btn btn-warning" type="reset" id="reset_form_button">Reset</button>
				</div>
			</div>
	
	
	</div>

</form>
</div>
</div>

</div>
</div>
</div>
</body>
</html>
<script type="text/javascript">

function isNumber(evt) {

	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;

	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	}
	return true;
}

function form_validation()
{
	
	    var err = 0;
		
		if(err==0){

			var form_status = $('<div class="form_status"></div>');		
			var form = document.getElementById('admin_settings');		
			
			form.action='<?php echo base_url()."admin/Catalogue/admin_settings_submit"; ?>';
			form.submit();
		}
	
	return false;
}
</script>
