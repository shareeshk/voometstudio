<html lang="en">
<head>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
</head>
<body>
<script>
$(document).ready(function(e) {
		
		rate_table=$('#rate_table').dataTable({
					"order": [[ 4, "desc" ]],
					"bStateSave": true,
					"fnStateSave": function (oSettings, oData) {
					localStorage.setItem('offersDataTables', JSON.stringify(oData));
				},
					"fnStateLoad": function (oSettings) {
					return JSON.parse(localStorage.getItem('offersDataTables'));
				}
				
			}).show();
		//get_allow(st);
    });  
</script>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container" id="get">
<div class="row">
<div class="col-md-2"><button type="button"  id="pend" class="btn btn-warning" onclick="get_allow('pending')">Pending Entries </button></div>
<div class="col-md-2"><button type="button" id="allow" class="btn btn-success" onclick="get_allow('allowed')">Allowed Entries</button></div>
<div class="col-md-2"><button type="button" id="block" class="btn btn-danger" onclick="get_allow('blocked')">Blocked Entries</button></div>
</div>
<div class="row-fluid">
<table id="rate_table" class="table table-striped table-bordered" style="display:none;" cellspacing="0" width="100%">
		<thead>
			<tr>
            <th><b>Customer</b></th>
            <th><b>Inventory Name</b></th>
            <th><b>Rating</b></th>
            <th><b>Review</b></th>
            <th><b>Date Posted</b></th>
            <th><b>Security</b></th>
       		</tr>
        </thead>
  </table>
</div>
</div>
</div>
</body>
</html>
<script >
function get_allow(st){
	var url="<?php echo base_url('admin/Ratemanaging/get_rating') ?>/"+st;
	$.ajax({
			url:url,
			dataType:"JSON",
			type:'POST'
		
		}).success( function(s){
			rate_table.fnClearTable();
						for(var i = 0; i < s.length; i++) {

							custom="<a href="+"<?php echo base_url();?>admin/Manage_cust/view/"+s[i][0]+">"+s[i][1]+"</a>";
							product=s[i][8];
							rating=s[i][3];	
							if(s[i][4]==''){
								review="<font color='red'><b>No Reviews</b></font>";	
							}
							else{
								review="<font color='orange'><b>"+s[i][4]+"</b></font>";	
							}
							pdate=s[i][5];
							if(s[i][6]==1){
								security="<a href="+"<?php echo base_url();?>admin/Ratemanaging/notallow/"+s[i][7]+"/"+st+" style='text-decoration:none;' data-toggle='tooltip' title='Block'><button class='btn btn-danger'><font color='#fff'>Block</font></button></a>";	
							}
							else if(s[i][6]==0){
								security="<a href="+"<?php echo base_url();?>admin/Ratemanaging/allow/"+s[i][7]+"/"+st+" style='text-decoration:none;'><button class='btn btn-success'><font color='#fff'>Allow</font></button></a>";	
							}
							else if(s[i][6]==2){
								security="<a href="+"<?php echo base_url();?>admin/Ratemanaging/allow/"+s[i][7]+"/"+st+" style='text-decoration:none;'><button class='btn btn-success'><font color='#fff'>Allow</font></button></a> <a href="+"<?php echo base_url();?>admin/Ratemanaging/notallow/"+s[i][7]+"/"+st+" style='text-decoration:none;' data-toggle='tooltip' title='Block'><button class='btn btn-danger'><font color='#fff'>Block</font></button></a>";	
							}
							
							rate_table.fnAddData([custom,product,rating,review,pdate,security]);
							
						}
			});
	
	}//setInterval("get_allow('<?php //echo $status;?>');",2000);
</script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>
<?php
	if($status!=""){
		if($status=="allowed"){
			?>
            <script>
            $("#allow").trigger("click");
			</script>
            <?php
		}
		if($status=="blocked"){
			?>
            <script>
            $("#block").trigger("click");
			</script>
            <?php
		}
		if($status=="pending"){
			?>
            <script>
            $("#pend").trigger("click");
			</script>
            <?php
		}
	}
	if($status==""){
		?>
            <script>
            $("#pend").trigger("click");
			</script>
            <?php
	}
?>