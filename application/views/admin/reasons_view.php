<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script>
function show_view_validatn_create(){
	menu_sort_order=document.getElementById("menu_sort_order").value;
	if(menu_sort_order==0){
	document.getElementById("view1").disabled = true;
	document.getElementById("view2").checked = true;					
	}
	if(menu_sort_order!=0){
		document.getElementById("view1").disabled = false;
		document.getElementById("view1").checked = true;					
	}
}
</script>
<script type="text/javascript">
var table;

$(document).ready(function (){

	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    var rows_selected = [];
    table = $('#table_reasons').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Catalogue/reasons_processing",
			data: function (d) { d.pcat_id = $('#pcat_id').val();d.cat_id = $('#cat_id').val();d.subcat_id = $('#subcat_id').val();},
            type: "post",
            error: function(){
              $("#table_reasons_processing").css("display","none");
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
   
	$('#reset_form_button').on('click',function(){
		table.draw();
	});
	
	$("#submit_button").click(function() {
		table.draw();
	});
	$('#search_toggle').on('click',function(){	
		$('#search_block').toggle();
	});

});	

function drawtable(obj){
	table.draw();
}

function show_sort_order(){
	var parent_category=document.getElementById("parent_category").value;
	var categories=document.getElementById("categories").value;
	var subcategories=document.getElementById("subcategories").value;
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/show_reason_return_sort_order",
		type:"post",
		data:"parent_category="+parent_category+"&categories="+categories+"&subcategories="+subcategories,
		
		success:function(data){
			$("#menu_sort_order").html(data);
		}
	});
}

</script>
<script>

$(document).ready(function(){
	<?php
		if($create_editview=="create"){
			?>
			$("#viewDiv1").css({"display":"none"});
			$("#createDiv2").css({"display":""});
			<?php
		}
		else if($create_editview=="view"){
			?>
			$("#createDiv2").css({"display":"none"});
			$("#viewDiv1").css({"display":""});
			<?php
		}
	?>
});
</script>
<script>

function changeViewPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/reasons_filter';
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">

<div class="row" id="viewDiv1" style="display:none;">
	<div class="wizard-header text-center">
		<h5> Customer Return Reasons 
		</h5>                 		          	
	</div>
	<input value="<?php echo $pcat_id; ?>" type="hidden" id="pcat_id">
	<input value="<?php echo $cat_id; ?>" type="hidden" id="cat_id">
	<input value="<?php echo $subcat_id; ?>" type="hidden" id="subcat_id">

<table id="table_reasons" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr><th>Id</th>
		<th>Reasons Name</th>
		<th>Sub Category Name </th>
		<th>Category Name </th>
		<th>Parent Category</th>
		<th>View</th>
		<th>Last Updated</th>
		<th>Action</th>
	</tr>
</thead>

</table>
</div>

<div class="row" id="createDiv2" style="display:none;">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="create_reason" id="create_reason" method="post" action="#" onsubmit="return form_validation();">
	
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				parent_category: {
					required: true,
				},
				categories: {
					required: true,
				},
				subcategories: {
					required: true,
				},
				menu_sort_order: {
					required: true,
				},
				reason_name: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Create Reasons</h3>
		</div>	
		</div>
		<div class="tab-content">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Parent Category-</label>
					<select name="parent_category" id="parent_category" class="form-control align_top" onchange="showAvailableCategories(this)">
						<option value=""></option>
						<?php foreach ($parent_category as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>"><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<option value="0">--None--</option>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Category-</label>
					<select name="categories" id="categories" class="form-control align_top" onchange="showAvailableSubCategories(this)">
						<option value=""></option>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Sub Category-</label>
					<select name="subcategories" id="subcategories" class="form-control align_top" onchange="show_sort_order()">
						<option value=""></option>
					</select>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">Enter Reasons Name</label>
					<input id="reason_name" name="reason_name" type="text" class="form-control" />
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">Enter the Sort Order</label>
					
					<select name="menu_sort_order" id="menu_sort_order" class="form-control" onchange="show_view_validatn_create()">
						
					</select>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group">
					<div class="col-md-6">Show Reasons</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="view" id="view1" value="1" type="radio" checked="checked">View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="view" id="view2" value="0" type="radio">Hide</label>
					</div>
				
					</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-2"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" id="submit_reason_return" type="submit">Submit</button>
					<button class="btn btn-warning btn-sm" type="reset">Reset</button>
					<button class="btn btn-primary btn-sm" type="button" onclick="changeViewPrevious()">Change View</button>
					</div>
				</div>
			</div>

		</div>		
	</form>

</div>
</div>
</div>
</div>

</div>
<div class="footer">
</div>
<script type="text/javascript">

$(document).ready(function (){
	pcat_id=$('#f_pcat_id').val();
	cat_id=$('#f_cat_id').val();
	subcat_id=$('#f_subcat_id').val();

	if(pcat_id!='' && pcat_id!=null){
		
		$("#pcat_id").val(pcat_id);
		
		$.ajax({
			url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
			type:"post",
			async:false,
			data:"cat_id="+cat_id+"&active=1",
			success:function(data){
				if(data!=0){
					$("#subcat_id").html(data);
					$("#subcat_id").val(subcat_id);
				}
				else{
					$("#subcat_id").html('<option value="">-None-</option>')
					$("#brand_id").html('<option value="">-None-</option>')

				}
			}
		});
		$.ajax({
			url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
			type:"post",
			async:false,
			data:"pcat_id="+pcat_id+"&active=1",
			success:function(data){
				
				if(data!=0){
					$("#cat_id").html(data);
					$("#cat_id").val(cat_id);
				}
				else{
					$("#cat_id").html('<option value="">-None-</option>')
					$("#brand_id").html('<option value="">-None-</option>')

				}
			}
		});
		table.draw();
	}
});

function showAvailableSubCategories(obj){
	
	if(obj.value!="" && obj.value!="None"){
		cat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#subcategories").html(data);
					}
					else{
						$("#subcategories").html('<option value="">-None-</option>')
						$("#reasons").html('<option value="">-None-</option>')

					}
				}
			});
	}
	
}

function showAvailableCategories(obj){
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#categories").html(data);
					}
					else{
						$("#categories").html('<option value="">-None-</option>')
						$("#reasons").html('<option value="">-None-</option>')

					}
				}
			});
	}
	
}

function deleteReasonFun(reason_id){
swal({
			title: 'Are you sure?',
			text: "Delete Reason",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {	


		$.ajax({
				url: "<?php echo base_url(); ?>admin/Catalogue/delete_reason/"+reason_id,
				type: 'POST',
				data: "1=2",
				dataType: 'html',
			}).done(function(data){
				if(data==true){
				swal({
						title:"Deleted!", 
						text:"Reason has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
				}else{
					swal("Error", "Error in deleting this file", "error");
				}	
			});
			})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});

}
$('#result').hide();

function form_validation()
{
	var reason_name = $('input[name="reason_name"]').val().trim();
	var parent_category = $('select[name="parent_category"]').val().trim();
	var categories = $('select[name="categories"]').val().trim();
	var subcategories = $('select[name="subcategories"]').val().trim();
	var menu_sort_order = $('select[name="menu_sort_order"]').val().trim();
	var view = $('input[name="view"]').val();

	   var err = 0;
	   if((reason_name=='') || (parent_category=='') || (categories=='') || (subcategories=='') || (menu_sort_order=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#create_reason');	
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_reason/"?>',
				type: 'POST',
				data: $('#create_reason').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {
				form_status.html('')
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					document.getElementById("reason_name").value="";
					document.getElementById("reason_name").focus();
				});
				}
				if(data=="yes")
				{
					  
					swal({
						title:"Success", 
						text:"Reason Return is successfully added!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				if(data=="no")
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
			});
}
}]);			
			}
	
	return false;
}

</script>
</div>
</body>

</html>