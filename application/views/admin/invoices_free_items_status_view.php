<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Invoice free items</title>   
<meta name="description" content="free items">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script>
<style type="text/css">
.padding_right{
	float:left;
	margin-right:15px;
	
}
.accordion-toggle:hover {
  text-decoration: none;
}
.more-less {
   /* float: left;*/
	color: #212121;
}
.free_items_style{
	padding: 10px;
	border-bottom: 1px solid #ccc;
	margin-bottom: 13px;
}
.form-inline{
	margin-bottom:0;
}
.form-group{
	margin-bottom:0;
	margin-top:0;
	padding-bottom:0;
	vertical-align:bottom;
}
.datepicker{
	z-index:1151 !important;
}
</style>

<script type="text/javascript">

var table;

$(document).ready(function (){	

	/*var order_delivered_date=$('#order_delivered_date').datepicker({
	format: "yyyy/mm/dd"
	}).on('changeDate', function(ev) {
		order_delivered_date.hide();
	}).data('datepicker');
	
            
	$('#order_delivered_time').timepicker();
	
	$('#delivered_time_btn').on('click', function (){
		$('#order_delivered_time').timepicker('setTime', new Date());
	});*/
	
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("input[type=text]").val("");
	});

	/*var from_date_obj=$('#from_date').datepicker({
	format: "yyyy/mm/dd"
	}).on('changeDate', function(ev) {
		from_date_obj.hide();
	}).data('datepicker');

	var to_date_obj=$('#to_date').datepicker({
	format: "yyyy/mm/dd"
	}).on('changeDate', function(ev) {
		to_date_obj.hide();
	}).data('datepicker');  */ 
	
	$('#to_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY'
			});
			
		$('#from_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY', 
				shortTime : true
			}).on('change', function(e, date)
			{
				$('#to_date').bootstrapMaterialDatePicker('setMinDate', date);
			});
   // Array holding selected row IDs
   var rows_selected = [];
    table = $('#invoices_free_items_status_table').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Orders/invoices_free_items_status_processing", // json datasource
			data: function (d) {d.order_id=$('#OrderID').val(); d.from_date = $('#from_date').val(); d.to_date = $('#to_date').val(); },
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#invoices_free_items_status_table_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("invoices_count").innerHTML=json.recordsFiltered;
				return json.data;
            }
          },
	
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'1%',
         'className': 'dt-body-center'
      }],
      'order': [0, 'desc']
   });
 //  $('#from_date, #to_date').keyup( function() {
//        table.draw();
//    } );

	
$( "#from_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
table.draw();
} );

$( "#to_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
table.draw();
} );

$('#reset_form_button').on('click',function(){
	table.draw();
});

$('#submit_form_button').on('click',function(){
	table.draw();
});


});	

function selectAllFun(obj){
	var obj_arr=document.getElementsByName("free_orders_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}

function update_free_item_status_fun(){
	
	var text = $('#update_free_item_status_form').find('select[name="status"]').val();
	
	if(text==""){
		alert('Please Select the status');
		return false;
	}
	if(text=="delivered"){
		
		var item_delivered_date = $('#update_free_item_status_form').find('input[name="item_delivered_date"]').val();
		var item_delivered_time= $('#update_free_item_status_form').find('input[name="item_delivered_time"]').val();
		
		if(item_delivered_date=='' || item_delivered_time==''){
			alert('Please fill Delivered Date and Deliverd Time');
			return false;
		}
	}
	if(text=="undelivered"){
		var admin_comments = $('#update_free_item_status_form').find('input[name="admin_comments"]').val();
		if(admin_comments==''){
			alert('Pls fill the comments field');
			return false;
		}
	}
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Orders/update_free_item_status_on_invoice",
		type:"post",
		data:$("#update_free_item_status_form").serialize(),
		beforeSend: function(){
			$("#update_status").html('<i class="fa fa-refresh fa-spin"></i> Processing');
		},
		success:function(data){
			//alert(data);
			
			$("#update_status").html('<i class="fa fa-check"></i> Updated');
			if(data){
				alert('Successfully submitted')
				$("#update_free_item_status_modal").modal("hide");
				table.draw();
			}else{
				alert('error');
			}
			
		}
	})
}

function check_status_fun(status){
	
	if(status=="delivered"){
		$('.delivered_status').show();
	}else{
		$('.delivered_status').hide();
	}
	
	if(status=="undelivered"){
		$('#undelivered_stock').show();	
	}else{
		$('#undelivered_stock').hide();
	}
	
}
function generate_free_item_invoice(invoices_free_items_status_id,action,order_id){
	
	if(action=="view"){
		window.open("<?php echo base_url()?>assets/pictures/invoices/Orders/Invoice_Free_"+invoices_free_items_status_id+"_"+order_id+".pdf","_blank");
		
	}else{
		$.ajax({
			type:"POST",
			url:'<?php echo base_url()?>admin/Orders/generate_free_item_invoice',
			data:"invoices_free_items_status_id="+invoices_free_items_status_id+"&order_id="+order_id,
			success:function(data){
				
				if(data){
					alert('invoice is generated successfully');
					window.open("<?php echo base_url()?>assets/pictures/invoices/Orders/Invoice_Free_"+invoices_free_items_status_id+"_"+order_id+".pdf","_blank");
				}else{
					alert('invoice is not generated');
				}
			}
		});
	}
	
}

</script>
  
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header"><h4 class="text-center">Free Items status <span class="badge" id="invoices_count"></span></h4></div>

	<table id="invoices_free_items_status_table" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th colspan="6">
					
					<div class="col-md-12">
					<form id="date_range" class="form-inline">

					<input type="text" name="order_id" id="OrderID" placeholder="Enter Order Id" class="form-control">
					<input type="text" name="from_date" id="from_date" placeholder="From Date" class="form-control">
					<input type="text" name="to_date" id="to_date" placeholder="To Date" class="form-control">
					<button type="button" class="btn btn-primary btn-sm" id="submit_form_button">Submit</button>
					<button type="reset" class="btn btn-info btn-sm" id="reset_form_button">Reset</button>
					</form>
					
					</div>
					
				</th>
			</tr>
			<tr>
				<th><input type='checkbox' onclick="selectAllFun(this)"></th>
				<th class="text-primary small bold">Item</th>
				<th class="text-primary small bold">Order Id</th>
				<th class="text-primary small bold">Invoices Offer</th>
				<th class="text-primary small bold">sku id</th>
				<th class="text-primary small bold">Update</th>
			</tr>
		</thead>
	</table>
</div>

<div class="modal" id="update_free_item_status_modal">
     <div class="modal-dialog modal-sm">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Update Status of free Items</h4>
           </div>
           <div  class="modal-body">
				<form id="update_free_item_status_form" class="form-horizontal">
				<input type="hidden" value="" id="invoices_free_items_status_id" name="invoices_free_items_status_id">
				<input type="hidden" value="" id="order_id" name="order_id">
					<div class="form-group">
					<div class="col-md-12">
					<select class="form-control" name="status" id="status" required onchange="check_status_fun(this.value)">
					<option value="">-Select Order Status-</option>
					<option value="shipped">Shipped</option>
					<option value="delivered">Delivered</option>
					<option value="undelivered">Un Delivered</option>
					</select>
					</div>
					</div>
					<div class="delivered_status" style="display:none;">
           			<div class="form-group">
						<div class="col-md-12">
						<input class="form-control" type="text" name="item_delivered_date" id="order_delivered_date" placeholder="Delivered Date" required />
						</div>
					</div>
					
           			<div class="form-group">
						<div class="col-md-12">
						<input class="form-control" type="text" name="item_delivered_time" id="order_delivered_time" placeholder="Delivered Time" required />	
						</div>
					</div>
					
					</div><!--delivered_status-->
					
					<div class="form-group">
					<div class="col-md-12">
							<textarea name="admin_comments" class="form-control" placeholder="Write Comments" required></textarea>
					</div>
					</div>
					
					<div class="form-group" id="undelivered_stock" style="display:none;">
						<label class="control-label col-md-3">Update stock</label>
						 <div class="col-md-6">
							<input name="update_stock" type="radio" value="yes">
							Yes
							 <input name="update_stock" type="radio" value="no" checked>
							No
						</div>
					
					</div>
					
           			<div class="form-group">
						<div class="col-md-12">
							<button type="button" class="btn btn-sm btn-success btn-block" onclick="update_free_item_status_fun()">Submit</button>
						</div>
           			</div>
				</form>
           </div>
				<script>
				  $(document).ready(function(){
						
						$('#order_delivered_date').bootstrapMaterialDatePicker
						({
							time: false,
							clearButton: true
						});

						$('#order_delivered_time').bootstrapMaterialDatePicker
						({
							date: false,
							shortTime: true,
							use24hours: false,
							clearButton: true,
							format: 'hh:mm A'
						});

						$.material.init()
					});
				  </script>
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
  </div>
  
  <div class="modal" id="free_item_details_modal">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Free Item Details</h4>
           </div>
           <div  class="modal-body" id="model_content" style="height:450px;overflow-y: auto;">
		   <div class="container">
				
			</div>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close"class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
  </div>
  
  <script type="text/javascript">
  $("#free_item_details_modal").modal("hide");
  function view_details_fun(invoices_free_items_status_id,order_id){
	//  alert(order_id);
	$.ajax({
		url:"<?php echo base_url()?>admin/Orders/get_data_from_free_items_status",
		type:"POST",
		data:"invoices_free_items_status_id="+invoices_free_items_status_id+"&order_id="+order_id,
		success:function(data){
			//alert(data);
			$("#model_content").html(data);
			$("#free_item_details_modal").modal("show");
		}
	})
	
  }
  
  function open_modal_to_update_status_fun(invoices_free_items_status_id,order_id,item_shipped,item_delivered,item_undelivered){
  //alert(1);
	$('.delivered_status').hide();
	document.getElementById("update_free_item_status_form").reset();
	$("#invoices_free_items_status_id").val(invoices_free_items_status_id);
	$("#order_id").val(order_id);
	$("#update_free_item_status_modal").modal("show");
	if(item_shipped==1){
		val='<option value="">Please Choose</option><option value="delivered">Delivered</option><option value="undelivered">Un Delivered</option>';
		$("#status").html(val);
	}else{
		val='<option value="">Please Choose</option><option value="shipped">Shipped</option><option value="delivered">Delivered</option><option value="undelivered">Un Delivered</option>';
		$("#status").html(val);
	}
	
  }
  function test(){
  alert(1);
    }
  </script>
</div>  
</body> 
</html>  