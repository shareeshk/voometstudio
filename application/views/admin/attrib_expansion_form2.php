<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/bootstrap-colorpicker.min.css" rel="stylesheet">
<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.input-group .input-group-addon {
	background-color:#ccc;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	

<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo base_url();?>assets/js/color_classifier.js"></script>

</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<?php 
	//print_r($attributes);
?>
<div class="container-fluid">
<div class="row" id="editDiv3">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
	
<form name="attrib_expansion_form2" id="attrib_expansion_form2" method="post">
<?php
			$get_attribute_sub_options_arr=explode(",",$get_attribute_sub_options_comma);
			foreach($get_attribute_sub_options_arr as $k => $v){
				$v=trim($v);
				$v=str_replace(" ","_",$v);
				$get_attribute_sub_options_arr[$k]=trim($v);
			}
			//print_r($get_attribute_sub_options_arr);
		?>	
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
				rules: {
			<?php
				foreach($get_attribute_sub_options_arr as $v){
					?>
					<?php echo $v;?>: {
						required: true,
					},
					<?php
				}
			?>
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Create Attribute Expansion</h3>
			<input type="hidden" name="attribute_id"  value="<?php echo $attribute_id;?>">
			<input type="hidden" name="attribute_expansions_id"  value="<?php echo $attribute_expansions_id;?>">
			
		</div>	
		</div>	
		<div class="tab-content">
		<input type="hidden" name="attribute_sub_options" value="<?php echo $get_attribute_sub_options_comma;?>"> 
		
		<div class="row">
			<div class="col-md-5 col-md-offset-2">                               
				<div class="form-group">
					<button id="sub_options_add_one_more" type="button" name="sub_options_add_one_more" class="btn btn-info sub_options_add_one_more pull-right">Add Sub Options</button>
				</div>
			</div>
		</div>
			
			
			<div class="edit-multi-fields col-md-8 col-md-offset-4">
				
			</div>
			
				
			
			
			<div class="row">
				<div class="col-md-7 col-md-offset-5"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="button" id="submit-data" onclick="form_validation_attrib()">Submit</button>
					</div>
				</div>
			</div>	
				
		</div>		
				
	</form>
	<form  id="attrib_expansion_form2_proceed" method="post"  action="<?php echo base_url()?>admin/Catalogue/attrib_expansions">
	<input type="hidden" name="attribute_id" value="<?php echo $attribute_id?>">
	</form>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>

<script>

function showAvailableCategoriesEdit(obj)
{
	//if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#edit_categories").html(data);
						
						
					}
					else{
						$("#edit_categories").html('<option value=""></option>')
						$("#brands").html('<option value=""></option>')

					}
				}
			});
	//}
}
function showAvailableSubCategoriesEdit(obj)
{
	cat_id=obj.value;
	//if(obj.value!="" && obj.value!="None"){
		
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#edit_subcategories").html(data);
							
					}
					else{
						$("#edit_subcategories").html('<option value=""></option>');

					}
				}
			});
	//}
}



$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
	
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
	
}

$("form[name='edit_attribute'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
	if($(this).attr('name')!="edit_color_name_sample[]"){
		orig[$(this).attr('id')] = tmp;
	}
});

$('form[name="edit_attribute"]').bind('change keyup click', function () {

    var disable = true;
    $("form[name='edit_attribute'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
		var name = $(this).attr('name');

		if($(this).attr('name')!="edit_color_name_sample[]"){
			if (type == 'text' || type == 'select' || type == 'textarea') {
				disable = (orig[id].value == $(this).val());

			} else if (type == 'radio') {
				disable = (orig[id].checked == $(this).is(':checked'));
			}

			if (!disable) {
				return false; // break out of loop
			}
		}
    });

    button.prop('disabled', disable);
});


});

function form_validation_attrib()
{
			
	var err=0;	

		$(".sub_name").each(function(index, elem) {
			valu = $(elem).val();
			if(valu==''){
				err++;
			}
		});	
		
		if(err==0){		
    //swal.showLoading();
					 $.ajax({
							url: '<?php echo base_url()."admin/Catalogue/attrib_expansion_form2_action"?>',
							type: 'POST',
							data: $('#attrib_expansion_form2').serialize(),
						}).done(function(data){
								if(data==true){
									document.getElementById("attrib_expansion_form2_proceed").submit();
								}else{
									swal(
										'Oops...',
										'Error in form',
										'error'
									)
								}
							}); 

		}else{
			alert('Please fill all fields');
			return false;
		}		  
			
}
function get_attribute_valueFun(attribute_id,attribute_name){
	$.ajax({
			url: '<?php echo base_url()."admin/Catalogue/get_attribute_value"?>',
			type: 'POST',
			data: "attribute_id="+attribute_id+"&attribute_name="+attribute_name,
			success:function(data){
				if(attribute_name!=""){
					$("#attribute_value").val(data);
				}
				else{
					$("#attribute_value").val("");
				}
			}
	});
}




$(function() {
	var $wrapper_edit = $('.edit-multi-fields', this);
	
    $(".sub_options_add_one_more", $(this)).click(function(e) {
				
		<?php
		$str="";
					foreach($get_attribute_sub_options_arr as $attribute_sub_options_name){
				$attribute_sub_options_name_placeholder=str_replace("_"," ",$attribute_sub_options_name);
				$str.="<div class='col-md-3'><input type='text' id='".$attribute_sub_options_name."[]' name='".$attribute_sub_options_name."[]'  class='form-control sub_name' placeholder='Enter ".ucwords($attribute_sub_options_name_placeholder)."'/></div>";
				
					}
				?>
				
		$wrapper_edit.append("<div class='edit_add_range col-md-11'><?php echo $str;?><div class='col-md-1 align_top'><button class='btn btn-danger btn-sm edit_remove_range'><i class='fa fa-times' aria-hidden='true'></i></button></div></div>");
		
		$('.edit_remove_range').show();	
		
		return false;
		
    });
	
	$($wrapper_edit).on("click",".edit_remove_range", function(e){ 
	elements_length=$('.edit_add_range', $wrapper_edit).length;
	e.preventDefault(); 
		if(elements_length==1){
			$('.edit_remove_range').hide();
			$(this).parent('div').parent('div').remove();	
		}else{
		$('.edit_remove_range').show();
		$(this).parent('div').parent('div').remove();	
		
		}
        
    })
	
 });

 
</script>
</div>
</body>

</html>