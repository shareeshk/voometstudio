<html lang="en">
<head>
<title>Add Tagged Inventory</title>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrapValidator.css"/>
<link href="<?php echo base_url();?>assets/css/datepicker.css" rel="stylesheet" /> 

<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/multiselect.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

</head>
<body>
<div class="container-fluid">
<div class="row-fluid">

<div class="col-sm-8 col-md-offset-2">

                        <!---inv details--->

                        <div class="row" id="main_div">
                            <div class="text-center">

							<input type="hidden" name="max_addon_count" id="max_addon_count" value="<?php echo $max_addon_count;?>">
							<input type="hidden" name="tagged_invs_count" id="tagged_invs_count" value="<?php echo $tagged_invs_count;?>">

                                <?php

                                    if(!empty($inv_obj)){
                                        ?>
                                        <div>
                                            <input type="hidden" id="inv_id" name="inv_id" value="<?php echo $inv_id; ?>">
                                            <img width="100" class="" src="<?php echo base_url() ?><?php echo $inv_obj -> image; ?>"/>
                                            <p>Product Name : <?php echo $inv_obj -> product_name; ?></p>
                                            <p>Sku ID : <?php echo $inv_obj -> sku_id; ?></p>
											
										
                                        </div>
										                        
										<a class="btn btn-info btn-sm" href="<?php echo base_url(); ?>admin/Catalogue/inventory_all">Back</a>
                                        
                                        <div <?php echo (count($tagged_invs)==0) ? 'style="display:none"' : ''; ?>>
                                            <button class="btn btn-success btn-sm" onclick="add_new()">Add new Inventories</button>
                                        </div>

                                        <?php
                                    }
                                ?>
                            </div>

                        </div>

                        
                                <?php 

								/*echo '<pre>';
								print_r($tagged_invs);
								echo '</pre>';*/
								$addon_product_allowed=0;

                                if(!empty($tagged_invs)){

                                    ?>

                        <hr>

                        <table class="table table-bordered">
                            <th width="10%">S.no</th>
                            <th width="40%">Cat. Stucture</th>
                            <th width="40%">SKU</th>
                            <th width="10%">Action</th>
                            <tbody>
                                    <?php 
                                    $i=0;
                                    foreach($tagged_invs as $data){
										if($data->tagged_type=="internal"){
											$i++;
											$ids=explode(',',$data->tagged_inventory_ids);
											$tagged_inventory_costprices=explode(',',$data->tagged_inventory_costprices);
											$tagged_inventory_profitabilities=explode(',',$data->tagged_inventory_profitabilities);
											$tagged_inventory_mrps=explode(',',$data->tagged_inventory_mrps);
											$tagged_inventory_offerprices=explode(',',$data->tagged_inventory_offerprices);
											$tagged_inventory_offer_discounts=explode(',',$data->tagged_inventory_discounts);
											$c_txt='';
											if(!empty($ids) && $data->tagged_inventory_ids!=''){
												for($k=0;$k<count($ids);$k++){
													$rand_1=sample_code();
													
													$inv_data=get_all_inventory_data($ids[$k]);

													if(!empty($inv_data)){
														$c_txt.='<i class="fa fa-arrow-right" aria-hidden="true"></i> '.$inv_data->sku_id.'<br>Cost Price: Rs.'.$tagged_inventory_costprices[$k].' <br> Profitability:'.$tagged_inventory_profitabilities[$k].'% <br> MRP: Rs.'.$tagged_inventory_mrps[$k].'<br> Offer Price: Rs.'.$tagged_inventory_offerprices[$k].'<br> Offer Discount: '.$tagged_inventory_offer_discounts[$k].'%<br><br>';
													}
												}
											}else{
												$c_txt.='';
											}
											?>
										<tr>
											<td><?php echo $i; ?></td>
											
											<td><?php echo $data -> pcat_name.' > '.$data -> cat_name.' > '.$data -> subcat_name.' > '.$data -> brand_name.' > '.$data->product_name; ?></td>
											<td><?php echo $c_txt; ?></td>
											<td><button class="btn btn-danger btn-sm" onclick="delete_tagged_invs('<?php echo $data->tagged_id; ?>','internal')">Delete</button></td>
											
										</tr>
                                        <?php 
										}
										
										
                                    }

                                    ?>
                            </tbody>
                        </table>
                        <hr>
                                    <?php 
                                }
                                ?>
                            
						
                        <!---inv details--->
                        <div class="wizard-card" id="add_new_section" <?php echo (count($tagged_invs)>0) ? 'style="display:none"' : ''; ?>>
							
						
							<!---- Internal Inventories starts --->
							
							<div class="panel-group" id="accordion_internal_inventory" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingInternalInventory">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseInternalInventory" aria-expanded="true" aria-controls="collapseInternalInventory">
												<i class="more-less glyphicon glyphicon-plus"></i>
												Offer products from existing inventory
											</a>
										</h4>
									</div>
									<div id="collapseInternalInventory" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingInternalInventory">
										<div class="panel-body">
											  
											  
											<form id="add_tagged_invs_form" enctype="multipart/form-data" class="form-horizontal" method="post">
												<input type="hidden" name="tagged_main_inventory_id" value="<?php echo $inv_id; ?>">

												
													<div class="form-group">
														<label for="name" class="control-label col-sm-2">Parent Category</label>
														<div class="col-sm-6">
															<select id="pcat_id" class="form-control" name="sku_pcat_id" onchange="showAllCategories(this)">
															<option value="">-Select Parent Category-</option>
																	
																<?php foreach ($parent_category as $parent_category_value) {  ?>
																<option value="<?php echo $parent_category_value->pcat_id; ?>" <?php //echo ($parent_category_value->pcat_id==$sku_pcat_id)? "selected":''; ?> ><?php echo $parent_category_value->pcat_name; ?></option>
																<?php } ?>
																<option value="0" <?php //echo (intval($sku_pcat_id)==0) ? 'selected' : ''; ?>>--None--</option>
																
																</select>
															</div>
														</div>
														
														<div class="form-group">
															<label for="name" class="control-label col-sm-2">Category</label>
															<div class="col-sm-6">
																<select  class="form-control" id="categories" name="sku_cat_id"  onchange="showAllSubCategories(this)">
																	<option value="">-Select Category-</option>
																	
																</select>
															</div>
														</div>
														<div class="form-group">
															<label for="name" class="control-label col-sm-2">Sub Category</label>
															<div class="col-sm-6">
																<select  class="form-control" id="subcategories" name="sku_subcat_id"  onchange="showAllBrands(this)">
																	<option value="">-Select Sub Category-</option>
																	
																</select>
															</div>
														</div>
								
														<div class="form-group">
															<label for="name" class="control-label col-sm-2">Brand Id </label>
															<div class="col-sm-6">
																<select  class="form-control" id="brands" name="sku_brand_id"  onchange="showAllProducts(this)">
																	<option value="">-Select Brand-</option>
																	
																</select>
															</div>
														</div>
														
														<div class="row">
															
																<label class="control-label col-sm-2">-Select Products-</label>
																<div class="col-sm-6">
																<select  class="form-control" id="products" name="sku_product_id" onchange="showAllSkus(this)">
																	<option value=""></option>
																	
																</select>
																</div>
															
														</div>
														
														
								
														<div class="form-group">
															<div class="row">
															<div class="col-md-10 col-md-offset-1 col-md-offset-right-1">  
															<div class="col-sm-5">
																<select name="from_select_list[]" id="from_select_list" class="form-control" size="8" multiple="multiple"> 
																	
																</select>
															</div>

															<div class="col-sm-2 margin_top">
																<button type="button" id="create_rightAll" class="btn btn-block btn-xs btn-info"><i class="glyphicon glyphicon-forward"></i></button>
																<button type="button" id="create_rightSelected" class="btn btn-block btn-xs btn-info"><i class="glyphicon glyphicon-chevron-right"></i></button>
																<button type="button" id="create_leftSelected" class="btn btn-block btn-xs btn-info"><i class="glyphicon glyphicon-chevron-left"></i></button>
																<button type="button" id="create_leftAll" class="btn btn-block btn-xs btn-info"><i class="glyphicon glyphicon-backward"></i></button>
															</div>

															<div class="col-sm-5">
																<select name="to_select_list[]" id="to_select_list" class="form-control" size="8" multiple="multiple"></select>
															</div>
																			</div>
														</div>
														</div>
														
														
														<script>
															function choose_addon_single_or_multiple_tagged_inventories_in_frontend_fun(obj){
																if(obj.value=="single"){
																	document.getElementById("discount_offer_div").style.display="block";
																	document.getElementById("inventory_details_list_div").innerHTML="";
																}
																else{
																	document.getElementById("discount_offer_div").style.display="block";
																	document.getElementById("inventory_details_list_div").innerHTML="";
																}
															}
														</script>
														
														<div class="form-group">
																<label for="name" class="control-label col-sm-3">Choose Single / Multiple SKUs in Frontend</label>
																<div class="col-sm-6">
																	<?php
																		$addon_single_or_multiple_tagged_inventories_in_frontend="";
																		if(!empty($tagged_invs)){
																			foreach($tagged_invs as $data){
																				if($data->tagged_type=="internal"){
																					$addon_single_or_multiple_tagged_inventories_in_frontend=$data->addon_single_or_multiple_tagged_inventories_in_frontend;
																				}
																			}
																		}
																	?>
																	<select  class="form-control" id="choose_addon_single_or_multiple_tagged_inventories_in_frontend" name="choose_addon_single_or_multiple_tagged_inventories_in_frontend" required onchange="choose_addon_single_or_multiple_tagged_inventories_in_frontend_fun(this)">
																		<option value="">-Choose-</option>
																		<option value="single" <?php if($addon_single_or_multiple_tagged_inventories_in_frontend=='single'){echo 'selected';}?>>Single</option>
																		<option value="multiple" <?php if($addon_single_or_multiple_tagged_inventories_in_frontend=='multiple'){echo 'selected';}?>>multiple</option>
																		
																	</select>
																</div>
																<div class="col-sm-3">
																	(* This is applicable to attached SKUs)
																</div>
															</div>
															
															
															
														
														 <div class="form-group text-center" id="discount_offer_div">
															<div class="col-sm-12">
																If you want to give custom offer price and discounts then <button type="button" class="btn btn-success btn-xs preventDflt" onclick="customOfferPriceFun();"> Click Here </button>.
															</div>
														</div>
														
														<div id="inventory_details_list_div">
														
															
														
														</div>
														
														
														
														
												
											
														<div class="form-group">
															<div class="col-sm-3 col-sm-offset-2">
															  <button type="submit" class="btn btn-success preventDflt" onclick="return submit_form();"> Submit </button>
															</div>
															<div class="col-sm-3">
															  <button type="reset" class="btn btn-default preventDflt" onclick="cancel_form()"> Cancel </button>
															</div>
														
														</div>
													</form>  
											  
											  
											  
										</div>
									</div>
								</div>
							</div>
							
							<!---- Internal Inventories ends --->
							
							
							
							
							
						
		
		
                        

                        </div><!---form_section-->    
                                
                                
                                
					</div>	
				</div>
    
    
                
                <!----------sku details------------------>
                
</form>
</div>
</div>
</div>
</body>

<script type="text/javascript">

var $validator = $('.wizard-card form').validate({
			rules: {
                            "sku_pcat_id" : "required",
                            "sku_cat_id" :"required",
                            "sku_subcat_id" :"required",
                            "sku_brand_id" :"required",
                            "sku_product_id" :"required",
                            
                            "to_select_list[]": {
                                required: {
                                    depends: function(element) {
                                        //console.log(parseInt($('#type_of_coupon').val()));
                                        return true; 
                                    }
                                }
                            }
                           
			},
                        messages: {
                    
                                "to_select_list[]":
                                    {
                                        required: "Please select atleast one sku",
                                        //lessthan: "Amount should be greater than flat amount."
                                    }

                        },
			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
			
			
		
function customOfferPriceFun(){
	var inventory_list_id_str="";
	var to_select_list_obj=document.getElementById("to_select_list");
	var to_select_list_arr=new Array();
	if(to_select_list_obj.length>0){
		for(i=0;i<to_select_list_obj.length;i++){
			to_select_list_obj.options[i].selected=true;
			to_select_list_arr.push(to_select_list_obj.options[i].value);
		}
	

	inventory_list_id_str=to_select_list_arr.join("-");

					$.ajax({
								url:"<?php echo base_url(); ?>admin/Catalogue/get_inventory_list_details_single_all",
								type:"post",
								data:"inventory_list_id_str="+inventory_list_id_str,
								dataType:"JSON",
								success:function(data){
									get_inventory_list_details_str="";
									
									
									if(data.length>0){
										get_inventory_list_details_str+='<div class="form-group">';
											get_inventory_list_details_str+='<div for="name" class="col-sm-1">';
											get_inventory_list_details_str+='</div>';
											
											get_inventory_list_details_str+='<div class="col-sm-1">';
												get_inventory_list_details_str+='Is Free';
											get_inventory_list_details_str+='</div>';
											
											get_inventory_list_details_str+='<div class="col-sm-2">';
												get_inventory_list_details_str+='<b>Cost Price (Rs)</b>';
											get_inventory_list_details_str+='</div>';
											
											get_inventory_list_details_str+='<div class="col-sm-2">';
												get_inventory_list_details_str+='<b>Profitability (%)</b>';
											get_inventory_list_details_str+='</div>';
											
											
											get_inventory_list_details_str+='<div class="col-sm-2">';
												get_inventory_list_details_str+='<b>Offer Price</b>';
											get_inventory_list_details_str+='</div>';
											
											get_inventory_list_details_str+='<div class="col-sm-1">';
												get_inventory_list_details_str+='<b>MRP (Rs)</b>';
											get_inventory_list_details_str+='</div>';
											
											get_inventory_list_details_str+='<div class="col-sm-2">';
												get_inventory_list_details_str+='<b>Discount (%)</b>';
											get_inventory_list_details_str+='</div>';
											
											
										get_inventory_list_details_str+='</div>';
									
									}
									
									
										
											
									
									for(i=0;i<data.length;i++){
										get_inventory_list_details_str+='<div class="form-group">';
											get_inventory_list_details_str+='<div for="name" class="col-sm-1">'+data[i].sku_id;
											if(data[i].attribute_1_value!=""){
												get_inventory_list_details_str+=data[i].attribute_1_value;
											}
											if(data[i].attribute_2_value!=""){
												get_inventory_list_details_str+=", "+data[i].attribute_2_value;
											}
										get_inventory_list_details_str+='</div>';
										
										get_inventory_list_details_str+='<div class="col-sm-1">';
												get_inventory_list_details_str+='<input type="checkbox" id="internalsinglefree_'+data[i].id+'" name="taggedinvoffers[internalsinglefree_'+data[i].id+']" value="'+data[i].cost_price+'" onchange="internalSingleTaggedFreeFun(this.checked,'+data[i].max_selling_price+','+data[i].cost_price+','+data[i].id+')">';
											get_inventory_list_details_str+='</div>';
											
										
										
										
											get_inventory_list_details_str+='<div class="col-sm-2">';
												get_inventory_list_details_str+=data[i].cost_price;
												get_inventory_list_details_str+='<input type="hidden" class="form-control" id="costprice_'+data[i].id+'" name="taggedinvoffers[costprice_'+data[i].id+']" value="'+data[i].cost_price+'">';
											get_inventory_list_details_str+='</div>';
											
											
											
											get_inventory_list_details_str+='<div class="col-sm-2">';
												get_inventory_list_details_str+='<span id="profitabilityspan_'+data[i].id+'">0</span>';
												get_inventory_list_details_str+='<input type="hidden" class="form-control" id="profitability_'+data[i].id+'" name="taggedinvoffers[profitability_'+data[i].id+']" value="0">';
											get_inventory_list_details_str+='</div>';
											
											
											
											
											
											get_inventory_list_details_str+='<div class="col-sm-2">';
												get_inventory_list_details_str+='<input type="text" class="form-control" id="offerprice_'+data[i].id+'" name="taggedinvoffers[offerprice_'+data[i].id+']" onkeyup="offerpriceComparisionFun(this,'+data[i].max_selling_price+','+data[i].cost_price+','+data[i].id+')" onkeypress="return isNumberwithdot(event);" placeholder="Enter Offer Price" value='+data[i].max_selling_price+'>';
											get_inventory_list_details_str+='</div>';
											
											
											
											
											get_inventory_list_details_str+='<div class="col-sm-2">';
												get_inventory_list_details_str+=data[i].max_selling_price;
												
												
												get_inventory_list_details_str+='<input type="hidden" class="form-control" id="mrp_'+data[i].id+'" name="taggedinvoffers[mrp_'+data[i].id+']" value="'+data[i].max_selling_price+'">';
												
												
												
												
											get_inventory_list_details_str+='</div>';
											
											get_inventory_list_details_str+='<div class="col-sm-2">';
												get_inventory_list_details_str+='<span id="discountspan_'+data[i].id+'">0</span>';
												
												get_inventory_list_details_str+='<input type="hidden" class="form-control" id="discount_'+data[i].id+'" name="taggedinvoffers[discount_'+data[i].id+']" value="0">';
												
												
											get_inventory_list_details_str+='</div>';
											
											
										get_inventory_list_details_str+='</div>';
									}

									$("#inventory_details_list_div").html(get_inventory_list_details_str);
									
									for(i=0;i<data.length;i++){
										internalSingleTaggedFreeFun(false,data[i].max_selling_price,data[i].cost_price,data[i].id); // this line is to reset the values in form
									}
										
										
								}
						});
					 
				
		}else{
			alert('Please select SKUs List');
			return false;
		}
}








function isNumberwithdot(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && ((charCode < 48 || charCode > 57) && charCode != 46)) {
                return false;
            }
            return true;
        }
		
function isNumberwithdotandMinus(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && ((charCode < 48 || charCode > 57) && charCode != 46 && charCode!=45)) {
                return false;
            }
            return true;
        }


function internalSingleTaggedFreeFun(obj_free_checked,max_selling_price,cost_price,inventory_id){
	if(obj_free_checked==true){
		$("#offerprice_"+inventory_id).val(0);
		offerprice_value=0;
		$("#discountspan_"+inventory_id).html(Math.round(100-(offerprice_value/max_selling_price)*100,2));
		$("#discount_"+inventory_id).val(Math.round(100-(offerprice_value/max_selling_price)*100,2));
		
		
		$("#profitabilityspan_"+inventory_id).html((Math.round(((offerprice_value-cost_price)/cost_price)*100,2)));
		$("#profitability_"+inventory_id).val((Math.round(((offerprice_value-cost_price)/cost_price)*100,2)));
	}
	else{
		$("#offerprice_"+inventory_id).val(max_selling_price);
		offerprice_value=max_selling_price;
		$("#discountspan_"+inventory_id).html(Math.round(100-(offerprice_value/max_selling_price)*100,2));
		$("#discount_"+inventory_id).val(Math.round(100-(offerprice_value/max_selling_price)*100,2));
		
		
		$("#profitabilityspan_"+inventory_id).html((Math.round(((offerprice_value-cost_price)/cost_price)*100,2)));
		$("#profitability_"+inventory_id).val((Math.round(((offerprice_value-cost_price)/cost_price)*100,2)));
	}	
}	

function offerpriceComparisionFun(obj_of_offerprice,max_selling_price,cost_price,inventory_id){
	if(parseFloat(obj_of_offerprice.value)!=parseFloat(max_selling_price)){
		$("#internalsinglefree_"+inventory_id).attr("checked",false);
	}
	if(obj_of_offerprice.value!=""){
		$("#discountspan_"+inventory_id).html(Math.round(100-(obj_of_offerprice.value/max_selling_price)*100,2));
		$("#discount_"+inventory_id).val(Math.round(100-(obj_of_offerprice.value/max_selling_price)*100,2));
		
		
		$("#profitabilityspan_"+inventory_id).html((Math.round(((obj_of_offerprice.value-cost_price)/cost_price)*100,2)));
		$("#profitability_"+inventory_id).val((Math.round(((obj_of_offerprice.value-cost_price)/cost_price)*100,2)));
	}
	else{
		$("#discountspan_"+inventory_id).html(0);
		$("#discount_"+inventory_id).val(0);
		
		$("#profitabilityspan_"+inventory_id).html(0);
		$("#profitability_"+inventory_id).val(0);
	}	
	if(obj_of_offerprice.value>max_selling_price){
		swal({
			title:"", 
			text:"Offer Price should not exceeds the MRP", 
			type: "info",
			allowOutsideClick: false
		}).then(function () {								
			obj_of_offerprice.value="";
			$("#discountspan_"+inventory_id).html(0);
		    $("#discount_"+inventory_id).val(0);
			
			$("#profitabilityspan_"+inventory_id).html(0);
			$("#profitability_"+inventory_id).val(0);
		});
	}
}






function submit_form() {
	var to_select_list_obj=document.getElementById("to_select_list");
	if(to_select_list_obj.length>0){
		for(i=0;i<to_select_list_obj.length;i++){
			to_select_list_obj.options[i].selected=true;
		}
	max_limit=$('#max_addon_count').val();
	tagged_invs_count=$('#tagged_invs_count').val();

	//alert(max_limit);
	//alert(tagged_invs_count);

	balance=(parseInt(max_limit)-parseInt(tagged_invs_count));

	//alert(balance);

	if(parseInt(to_select_list_obj.length)>parseInt(balance)){
		alert('Addon Maximum Attach limitations is '+max_limit);
		return false;
	}

		if($('#add_tagged_invs_form').valid()){
			document.getElementById("add_tagged_invs_form").action="<?php echo base_url() ?>admin/Catalogue/add_tagged_invs_form_submit";
			document.getElementById("add_tagged_invs_form").submit();
		}else{
			return false;
		}
		
	}
	else{
			swal({
				title:"", 
				text:"Please choose inventories", 
				type: "info",
				allowOutsideClick: false
			}).then(function () {								
				
			});
	}
}


function showAllBrands(obj){
	
	if(obj.value!="" && obj.value!="None"){
		subcat_id=obj.value;
                selected_brand_id=$('#sku_brand_id_hidden').val();
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_all_brands",
				type:"post",
				data:"subcat_id="+subcat_id+"&active=1"+"&brand="+selected_brand_id,
				success:function(data){
					if(data!=0){
						$("#brands").html(data);
					}
					else{
						$("#brands").html('<option value=""></option>');
					}
				}
			});
	}
	
}
function showAllSubCategories(obj){
	
	if(obj.value!="" && obj.value!="None"){
		cat_id=obj.value;
                selected_subcat=$('#sku_subcat_id_hidden').val();
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_all_subcategories",
				type:"post",
				data:"cat_id="+cat_id+"&active=1"+"&subcat_id="+selected_subcat,
				success:function(data){
					if(data!=0){
						$("#subcategories").html(data);
					}
					else{
						$("#subcategories").html('<option value=""></option>')
						$("#brands").html('<option value=""></option>')

					}
				}
			});
	}
	
}
function showAllCategories(obj){
//alert();
if(obj.value!="" && obj.value!="None"){
pcat_id=obj.value;
		
		selected_cat=$('#sku_cat_id_hidden').val();
		//alert(selected_cat);
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_all_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1"+"&cat_id="+selected_cat,
				success:function(data){
					if(data!=0){
                                            $("#categories").html(data);
                                            if(selected_cat!='' && selected_cat!=null){
                                                //$("#categories").val(selected_cat);
                                                
                                            }       
					}else{
                                            $("#categories").html('<option value=""></option>')
                                            $("#brands").html('<option value=""></option>')

					}
				}
			});
	}
	
}
function showAllSkus(obj){
            if(obj.value!="" && obj.value!="None"){
		product_id=obj.value;
        inv_id=$('#inv_id').val();
                $.ajax({
                            url:"<?php echo base_url(); ?>admin/Catalogue/show_all_skus",
                            type:"post",
                            data:"product_id="+product_id+"&inv_id="+inv_id+"&active=1"+"&inv_type=2",
                            success:function(data){
                                /*if(data!=0){
                                        $("#skus").html(data);
                                }else{
                                        $("#skus").html('<option value=""></option>');
                                }*/
                                
                                if(data!=0){
                                    $("#from_select_list").html(data);		
                                }else{
                                    $("#to_select_list").html("");	
                                }
                            }
                    });
            }
}

function showAllProducts(obj){
	
	if(obj.value!="" && obj.value!="None"){
		brand_id=obj.value;
        selected_product_id=$('#sku_product_id_hidden').val();
		$.ajax({
                        url:"<?php echo base_url(); ?>admin/Catalogue/show_all_products",
                        type:"post",
                        data:"brand_id="+brand_id+"&active=1&product_id="+selected_product_id,
                        success:function(data){
                                if(data!=0){
                                        $("#products").html(data);
                                        if(selected_product_id!='' &&selected_product_id!=null){
                                            $('#products').trigger("change");
                                        }
                                }
                                else{
                                        $("#products").html('<option value=""></option>');
                                }
                        }
                });
	}
	
}

    $('#from_select_list').multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
        }
    });
	

function delete_tagged_invs(tagged_id,tagged_type){


swal({
			title: 'Are you sure?',
			text: "Delete!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
				
				
				
				if(tagged_id!='') {
    $.ajax({
        url:"<?php echo base_url()?>admin/Catalogue/delete_tagged_invs",
        type:"POST",
        data:"tagged_id="+tagged_id+"&tagged_type="+tagged_type,
        dataType: "JSON",                
        asyc:false,
        success:function(data){
            if (parseInt(data.flag) == 1) {
                swal(
                    data.message,
                    '',
                    'info'
                ) 
                swal({
                        title:"", 
                        text:data.message, 
                        type: "success",
                        allowOutsideClick: false
                    }).then(function () {								
                        location.reload();
                    });
                //location.reload();
            } 
        }
    });
}



			
			})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
			

}

function add_new(){
    $('#add_new_section').show();
    $('html, body').animate({
        'scrollTop' : $("#add_new_section").position().top
    });
}

function cancel_form(){
    $('#add_new_section').hide();
    $('html, body').animate({
        'scrollTop' : $("#main_div").position().top
    });
}


</script>

</html>