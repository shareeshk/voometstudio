<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Invoice offers -Request for refund</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/jquery.timepicker.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script src="<?php echo base_url();?>assets/js/jquery.timepicker.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 

<script type="text/javascript">
var table;              
$(document).ready(function (){
   // Array holding selected row IDs
    var rows_selected = [];
    table = $('#invoice_offers_request_for_refund').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Returns/invoice_offers_request_for_refund_processing", // json datasource
            type: "post",  // type of method  , by default would be get
			data: function (d) { d.status_of_refund = $('#status_of_refund').val(); },
            error: function(){  // error handling code
              $("#invoice_offers_request_for_refund").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("inoice_offers_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'20%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});	

function draw_table(){
	table.draw();
}

function printcontent(order_item_id){
	//Popup($("#print_"+elem).html());
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/returns_refund_request_orders_accounts_incoming_printcontent",
		type:"POST",
		data:"order_item_id="+order_item_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			Popup($("#print").html(data))
		}
	})
}

function Popup(printdiv) {
	var mywindow = window.open('', 'Refund Details', 'height=400,width=600');
	mywindow.document.write('<html><head><title>Refund Details</title>');
	mywindow.document.write('</head><body >');
	mywindow.document.write(printdiv.html());
	mywindow.document.write('</body></html>');

	mywindow.document.close(); // necessary for IE >= 10
	mywindow.focus(); // necessary for IE >= 10

	mywindow.print();
	mywindow.close();

	return true;
}	
</script>
<script>

function open_return_refund_wallet_fun(order_id){
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/get_details_from_invoice_offers",
		type:"POST",
		data:"order_id="+order_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			//alert(data);
			$("#invoice_refund_wallet_form").html(data);
			$("#invoice_refund_wallet_modal").modal("show");
		}
	})
	
}
</script>
<style>
.datepicker{z-index:1151 !important;}
</style>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="page-header"><h4 class="text-center">Invoice offers -  Request for refund <span class="badge" id="inoice_offers_count"></span></h4></div>
	
	
	<table id="invoice_offers_request_for_refund" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
		<tr>
		<th colspan="4">
		<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<select class="form-control" name="status_of_refund" id="status_of_refund" onchange="draw_table()">
				<option value="pending">Pending</option>
				<option value="refunded">Refunded</option>
				<option value="reject">Rejected</option>
			</select>
		</div>
	</div>
		</th>
		</tr>
			<tr>
				<th class="text-primary small bold">Order ID</th>
				<th class="text-primary small bold">Promotion Details</th>
				<th class="text-primary small bold">Transaction Details</th>
				<th class="text-primary small bold">Actions</th>
			</tr>
		</thead>
	</table>
</div>
<script>

$(document).ready(function(){
		
	
	$("#invoice_refund_wallet_form").on('submit',(function(e) {
		e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>admin/Returns/invoice_refund_wallet",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					if(data){
						$('#invoice_refund_wallet_modal').modal('hide');
						swal({
							title:"success!", 
							text:"Amount has been transfered to wallet and Invoice has been generated", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#invoice_refund_wallet_form").trigger("reset");
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
					
				}	        
		   });
	}));

});
function divFunction_account_wallet(){
    $("#invoice_refund_wallet_form").trigger("reset");		
		location.reload();
}
</script>


<!--- modal wallet starts----------------->
<div class="modal" id="invoice_refund_wallet_modal" data-backdrop="static" role="dialog" tabindex="-1" aria-hidden="true">
   <div class="modal-dialog modal-sm">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		 <div class="panel panel-info">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                  Transfer to Wallet
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right" onClick="divFunction_account_wallet()"></span></span>
               </div>
               <div aria-expanded="true" class="">
                  <div class="panel-body">
		 <form id="invoice_refund_wallet_form" method="post" enctype="multipart/form-data" class="form-horizontal">
		 
		 
			</form>
			 </div>
      </div>
   </div>
         </div>
      </div>
   </div>
</div>
<div class="container" id="print" style="display:none;"></div>

<div class="row" style="display:none;" id="refund_info_div">
	<div class="col-md-12">
		 <ul class="nav nav-pills">
			<li class="active"><a href="#ordersummary-tab" data-toggle="tab">Order Summary</a></li>
			<li><a href="#quantityandprice-tab" data-toggle="tab">Quantity and Price</a></li>
			<li><a href="#shipmentstatus-tab" data-toggle="tab">Shipment Status</a></li>
			<li><a href="#deliveredto-tab" data-toggle="tab">Delivered To</li>
		</ul>
			<div class="tab-content">
			<!---------------- landline tab start --------------------->
			<div class="tab-pane active" id="ordersummary-tab">
				order summary Content
			</div>
			<div class="tab-pane" id="quantityandprice-tab">
				quantity and price Content
			</div>
			<div class="tab-pane" id="shipmentstatus-tab">
				shipment status Content
			</div>
			<div class="tab-pane" id="deliveredto-tab">
				delivered to Content
			</div>
			</div>
	</div>
</div>

<script type="text/javascript">
function generate_invoice_for_order_ID_offers(order_id,type_of_order){
	if(type_of_order=="cashback"){
		var str="CBK";
	}else{
		var str="SG";
	}
	window.open("<?php echo base_url()?>assets/pictures/invoices/Invoice_"+str+"_"+order_id+".pdf","_blank");
	//window.open("<?php echo base_url()?>assets/pictures/invoices/Orders/Invoice_"+str+"_"+order_id+".pdf","_blank");
	
	
}
</script>
</div>
</body>
</html>  