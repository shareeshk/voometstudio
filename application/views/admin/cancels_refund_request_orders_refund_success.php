<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Cancel Refund Request Orders - Refund Success</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   var table = $('#cancels_refund_request_orders_table_refund_success').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Orders/cancels_refund_request_orders_refund_success_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#cancels_refund_request_orders_table_refund_success_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("refund_success_count").innerHTML=json.recordsFiltered;
				return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'5%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});	


function open_cancel_refund_issue_fun(order_item_id){
	location.href="<?php echo base_url()?>admin/Orders/cancels_issue_refund/"+order_item_id;
}

</script>
  
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header"><h2>Cancels Refund Request Orders - Refund Success <span class="badge" id="refund_success_count"></span></h2></div>
	<table id="cancels_refund_request_orders_table_refund_success" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr >
				<th></th>
				<th>Order Summary</th>
				<th>Order Status</th>
				<th>Cancellation Details</th>
				<th>Refund Details</th>
				<th>Buyer Details</th>
				<th>Transaction Details</th>
			</tr>
		</thead>
	</table>
</div>
</div>
  
</body> 
</html>  