<html>
<head>
<title></title>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<style>
button.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 5px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
}

button.accordion.active, button.accordion:hover {
    background-color: #ddd;
}

button.accordion:after {
    content: 'Expand \02795';
    font-size: 13px;
    color: #777;
    margin-left: 5px;
}

button.accordion.active:after {
    content: "Collapse \2796";
}

div.panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: 0.6s ease-in-out;
    opacity: 0;
}

div.panel.show {
    opacity: 1;
    max-height: 500px;
}

</style>	
 
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   
   var table = $('#current_promotions_table').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Promotions/deactive_promotions_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#current_promotions_table_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("deactive_promotions_count").innerHTML=json.recordsFiltered;
				
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'1%',
         'className': 'dt-body-center'
      }],
      'order': [5, 'desc']
   });
});	
</script>  
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header"><h4 class="text-center bold">Deactivated Promotions <span class="badge" id="deactive_promotions_count"></span></h4></div>
<div class="row">
 <div class="col-md-12">
                	<table id="current_promotions_table" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			            	<th class="text-primary small bold">S.No</th>
			                <th class="text-primary small bold">Promo Id</th>
			                <th class="text-primary small bold">Status</th>
			                <th class="text-primary small bold">Log</th>
			                <th class="text-primary small bold">Category</th>
			                <th class="text-primary small bold">Deal Name</th>
			                <th class="text-primary small bold">Deal Quote</th>
			                <th class="text-primary small bold">Start Date</th>
			                <th class="text-primary small bold">End Date</th>
			            </tr>
			        </thead>
			        </table>

                </div>
				</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" data-backdrop="static">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Promotion Change Log</h4>
      </div>
      <div class="modal-body" id="log_data">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
	function getAllLogOfThisPromotion(obj){
		document.getElementById('log_data').innerHTML=""
		promo_id=obj.getAttribute('promo_id')
		var xhr = false;
			if (window.XMLHttpRequest) {
			    xhr = new XMLHttpRequest();
			}
			else {
			    xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
		
			 var xhr = new XMLHttpRequest();
		
					if (xhr) {
						obj.classList.remove("btn-success");
						obj.classList.add("btn-warning");
						obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
			            xhr.onreadystatechange = function () {
			                if (xhr.readyState == 4 && xhr.status == 200) {
									document.getElementById('log_data').innerHTML=xhr.responseText
			                		activateAccordian()
									obj.classList.remove("btn-warning");
									obj.classList.add("btn-success");
									obj.innerHTML='View Log'
			                	$('#myModal').modal('show');
			                }
			            }
			            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_log_data_of_promotion", true);
			            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			            xhr.send("promo_id="+promo_id);
			        }
	}
	
	function activateAccordian(){
		var acc = document.getElementsByClassName("accordion");
		var i;
		
		for (i = 0; i < acc.length; i++) {
		    acc[i].onclick = function(){
		        this.classList.toggle("active");
		        this.nextElementSibling.classList.toggle("show");
		  }
		}
	}
	
	
</script>

</div>

</body>

</html>