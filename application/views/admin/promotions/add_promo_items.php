<style>
#attributeColorSelect_freeItem{
	margin-bottom:20px;
}
.modal .modal-dialog {
  margin-top: 0px;
}
</style>

<?php if(!empty($promo_data)){
	foreach ($promo_data as $data){
		$promo_id=$data['id'];
		$get_deal_value=$data['to_get'];
		$buy_deal_value=$data['to_buy'];
		$get_deal_type=$data['get_type'];
		$buy_deal_type=$data['buy_type'];
		$promo_type=$data['promo_type'];
		$quote_selector=$data['quote_selector'];
		$promo_uid=$data['promo_uid'];
		$promo_name=$data['promo_name'];
		$promo_quote=htmlspecialchars_decode($data['promo_quote']);
		$promo_start_datev=$data['promo_start_date'];
		$promo_end_datev=$data['promo_end_date'];
		$promo_end_date="";
		$approval_sent=$data['approval_sent'];
		$promo_approval=$data['promo_approval'];
		$promo_start_date="";
        $applied_at_invoice=$data['applied_at_invoice'];
		$activated_once=$data['activated_once'];
		$is_promo_editable=$data['is_promo_editable'];
        
        
		if($data['promo_start_date']!=""){
			$promo_start_date=date("D,j M Y H:i", strtotime($data['promo_start_date']));
		}
		if($data['promo_end_date']!=""){
			$promo_end_date=date("D,j M Y H:i", strtotime($data['promo_end_date']));
		}
		
		$promo_active=$data['promo_active'];

		//echo "$promo_active=".$promo_active."||"."$promo_approval=".$promo_approval."||$approval_sent=".$approval_sent."||";
		
		if(date('Y-m-d H:i') > date($promo_end_datev)){
			$promo_active_status=" Expired ";
		}
		else{
			if($promo_active==0 && $promo_approval==0 && $approval_sent==0){
			$promo_active_status=" Draft ";
			}
			if($promo_active==0 && $promo_approval==1 && $approval_sent==1){
				$promo_active_status=" Approved ";
			}
			if($promo_active==0 && $promo_approval==0 && $approval_sent==1){
				$promo_active_status=" Approval Sent ";
			}
			// if($promo_active==0 && $promo_approval==1 && $approval_sent==0){
			// 	$promo_active_status=" Approval Sent ";
			// }
			if($promo_active==0 && $promo_approval==1 && $approval_sent==1 && $activated_once==1){
				$promo_active_status=" Deactivated ";
			}
			if($promo_active==1 && $promo_approval==1 && $approval_sent==1 && $activated_once==1){
				$promo_active_status=" Active ";
			}
		}

		$promo_to_get=$data['to_get'];
		
		$get_type=$data['get_type'];
	}
        $quote_selector_value=$controller->get_quote_selector_value($quote_selector);
        
        if (stripos($quote_selector_value,'same') !== false) {
            $qualify_for_same=1;
        }
        else{
            $qualify_for_same=0;
        }
        if (stripos($quote_selector_value,'different') !== false) {
            $qualify_for_different=1;
        }
        else{
            $qualify_for_different=0;
        }
        
        if (stripos($quote_selector_value,'tiered') !== false) {
            $qualify_for_tier=1;
        }
        else{
            $qualify_for_tier=0;
        }
        if($qualify_for_tier==1){
          $promo_quote_arr=explode('<br>',$promo_quote);
            $promo_quotes=""; 
            $i=0;
            array_pop($promo_quote_arr); 
            foreach($promo_quote_arr as $arr){
                $promo_quotes.='<li id="'.$i.'_listNUM">'.$arr.'</li>';
                $i++;
            }
            
        }

}
else{
	redirect(base_url().'admin/Promotions/promotion_not_found');
}
?>
<?php 
if($this->session->flashdata("notification")){
	
} 
?>
<html lang="en">
<head>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/jqueryui/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 
<style>
.popover{
   border:none;
   border-radius:unset;
   
   width:100%;

   overflow-wrap:break-word;
}
.btn{
	border-radius:unset;
}

.btn-round-lg{
border-radius: 22.5px;
}
.btn-round{
border-radius: 17px;
}
.btn-round-sm{
border-radius: 15px;
}
.btn-round-xs{
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
}

.popover-content {
   height: 200px;
   overflow-y: scroll;
}
.table {
	font-size:.9em;
    margin-bottom: 0px;
}


/* Tooltip styling */

[data-tooltip]:after,
[data-tooltip]:before {
    content: '';
    display: none;
    font-size: .75em;
    position: absolute;
}
[data-tooltip]:after {
    border-top: .5em solid #222;
    border-top: .5em solid hsla(0,0%,0%,.9);
    border-left: .5em solid transparent;
    border-right: .5em solid transparent;
    height: 0;
    left: 40%;
    width: 0;
    z-index: 2000;
}
[data-tooltip]:before {
    background-color: #222;
    background-color: hsla(0,0%,0%,.9);
    border-radius: .25em;
    color: #f6f6f6;
    content: attr(data-tooltip);
    font-family: sans-serif;
    left: 0;
    padding: .25em .75em;
    white-space: nowrap;
}
[data-tooltip]:hover:after,
[data-tooltip]:hover:before {
    display: block;
}
[data-tooltip]:hover:after {
    top: -.5em;
}
[data-tooltip]:hover:before {
    top: -2.5em;
}

	ul.listack > li {
    display: inline-block;
    /* You can also add some margins here to make it look prettier */
    zoom:1;
    display:inline;
    /* this fix is needed for IE7- */
   margin-right: 5%;
}

.material-button {
    position: relative;
    top: 0;
    z-index: 1;
    width: 30px;
    height: 30px;
    color: #fff;
    border: none;
    border-radius: 70%;
    box-shadow: 0 3px 6px rgba(0,0,0,.275);
    outline: none;
}
.btn-xsm{
  width: 25px;
  height: 25px;
  border-radius: 60%;  
}
.list-left li, .list-right li {
    cursor: pointer;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #000;
    }
.nav-tabs>li>a {
	color: #000;
}
.nav-tabs {
    border-bottom: 1px solid #cccccc;
}

.panel-heading .accordion-toggle:after {
    /* symbol for "opening" panels */
    font-family: 'Glyphicons Halflings';  /* essential for enabling glyphicon */
    content: "\e114";    /* adjust as needed, taken from bootstrap.css */
    float: left;        /* adjust as needed */
    color: grey;         /* adjust as needed */
}
.panel-heading .accordion-toggle.collapsed:after {
    /* symbol for "collapsed" panels */
    content: "\e080";    /* adjust as needed, taken from bootstrap.css */
}

    .nav-tabs { border-bottom: 2px solid #DDD; }
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
    .nav-tabs > li > a { border: none; color: #666; }
        .nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none; color: #4285F4 !important; background: transparent; }
        .nav-tabs > li > a::after { content: ""; background: #4285F4; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
    .nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
.tab-nav > li > a::after { background: #21527d none repeat scroll 0% 0%; color: #fff; }
.tab-pane { padding: 15px 0; }
.tab-content{padding:20px}

.card {background: #FFF none repeat scroll 0% 0%; box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3); margin-bottom: 30px; }

ul.step_process > li{
	width:33.3%;
	text-align: center;
}
.tab-content {
	padding: 0px;
}
.tab-pane{
	padding:20px;
}


#accordion-first .accordion-group {
    margin-bottom: 10px;
}

#accordion-first .accordion-heading, #accordion-first .accordion-toggle:hover, #accordion-first .accordion-heading .accordion-toggle.active {
    background: none repeat scroll 0% 0% transparent;
}
#accordion-first .accordion-heading {
    border-bottom: 0px none;
    font-size: 16px;
}

#accordion-first .accordion-heading .accordion-toggle {
    display: block;
    cursor: pointer;
    padding: 5px 0px !important;
    color: #222;
    outline: medium none !important;
    text-decoration: none;
}

#accordion-first .accordion-heading .accordion-toggle.active em{background-color: #F7C221;}

#accordion-first .accordion-heading .accordion-toggle > em {
    background-color: #337ab7;;
    border-radius: 100px;
    color: #FFF;
    font-size: 14px;
    height: 32px;
    line-height: 32px;
    margin-right: 10px;
    text-align: center;
    width: 32px;}

.margin_bottom{
	margin-bottom:2em;
}
.list-group{
	height:300px;
	overflow:auto;
}
    </style>

<script type="text/javascript">

			

    $(function () {
    	var start_dates=new Date()
    	var end_dates=new Date()

    	if(document.getElementById('start_date')!=null){
    		if(document.getElementById('start_date').value!=""){
    			start_dates=new Date(document.getElementById('start_date').value)
    		}
    	}
    	if(document.getElementById('end_date')!=null){
    		if(document.getElementById('end_date').value){
     			end_dates=new Date(document.getElementById('end_date').value)   			
    		}
    	}
    	
    	if(document.getElementById('start_date')!=null){
    		if(new Date(document.getElementById('start_date').value) > new Date()){
    			start_dates=new Date()
    		}
    	}
    	if(document.getElementById('end_date')!=null){
    		if(new Date(document.getElementById('end_date').value) < new Date()){
     			end_dates=new Date()			
    		}
    	}

        /*$('#start_date').datetimepicker({
					format: 'YYYY-MM-DD HH:mm',
                	minDate: new Date(),
                	useCurrent: false,
                	maxDate:end_dates,
                });
 
        $('#end_date').datetimepicker({
            useCurrent: false ,//Important! See issue #1075
			format: 'YYYY-MM-DD HH:mm',
            minDate:start_dates,
            
        });
        $("#start_date").on("dp.change", function (e) {
            $('#end_date').data("DateTimePicker").minDate(e.date);
        });
        $("#end_date").on("dp.change", function (e) {
            $('#start_date').data("DateTimePicker").maxDate(e.date);
        });*/
    });
	
	
	$(document).ready(function (){	
	$('#end_date').bootstrapMaterialDatePicker
			({
				time: true,
				weekStart: 0, 
				format: 'YYYY-MM-DD HH:mm', 
			}).on('change', function(e, date)
			{
				$('#start_date').bootstrapMaterialDatePicker('setMaxDate', date);
			});;
			
		$('#start_date').bootstrapMaterialDatePicker
			({
				time: true,
				weekStart: 0, 
				format: 'YYYY-MM-DD HH:mm',  
				shortTime : true
			}).on('change', function(e, date)
			{
				$('#end_date').bootstrapMaterialDatePicker('setMinDate', date);
			});
			});

</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header"><h4 class="text-center bold">Category : <?php echo $controller->get_promo_type_name($promo_type) ?> | Promo-id #<?php echo $promo_id ?></h4></div>  

<div class="row">
<div class="col-md-4">
		<div id="top_right_button_action">
		<?php if($promo_active==0){?>
			<?php if($promo_approval!=1){ ?> 
				<?php if($approval_sent==0){?>
						<button class="btn btn-info btn-xs btn-block" id="approve_promo_button1" promo_id="<?php echo $promo_uid ?>" onclick="send_approval_for_promotion(this)" >Send For Approval</button>
				<?php }else{ ?>
					<p class="lead">Sent For Approval</p>
				<?php }?>
			<?php }else{ ?> 
				<button class="btn btn-info btn-xs btn-block" id="activate_promo_button1" promo_id="<?php echo $promo_uid ?>" onclick="activate_promotion(this)" >Activate Promotion</button>
			<?php }?>
		<?php }?>
		<?php if($promo_active==1){ ?>
			<button class="btn btn-info btn-xs btn-block" id="draft_promo_button" promo_id="<?php echo $promo_uid ?>" onclick="draft_promotion(this)" >Deactivate </button>
		<?php }?>
		</div>
									
							
</div>
</div>
<div class="row">
<div class="col-md-12" id="form_feed_div">
		<div class="panel panel-success">
	      <div class="panel-heading">Promotion Details</div>
	      	<div class="panel-body">
	      		<span class="pull-right"><i><small class="text-success">Note: Popover Remains Static when click on that item</small></i> </span>
	      		<div class="table-responsive margin_bottom">
	      			<table class="table">
	      				<tr>
	      					<td>Promotion Status:</td>
	      					<td><span id="promo_active_status"><?php echo (isset($promo_active_status)) ? $promo_active_status : '';?></span></td>
	      					<td></td>
	      				</tr>
	      				<tr>
	      					<td>Deal Name:</td>
	      					<td><span id="disp_deal_name_edit_container"><?php echo $promo_name?></span>  &nbsp; </td>
	      					<td><button class="btn btn-primary btn-fab btn-fab-mini btn-round" id="button_deal_name_edit_container"  onclick="open_deal_name_edit()" <?php if($is_promo_editable=="no"){echo "disabled";}?>><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></td>
	      				</tr>
	      				<tr>
	      					<td>Deal Quote:</td>
	      					<td><span id="disp_deal_quote_edit_container"><?php if($qualify_for_tier!=1) {
	      					    echo $promo_quote;
	      					    }else{
	      					        echo $promo_quotes;
	      					    }?></span>  </td>
	      					<td><button class="btn btn-primary btn-fab btn-fab-mini btn-round" onclick="open_deal_quote_edit(<?php echo $quote_selector ?>)" id="button_deal_quote_edit_container" <?php if($is_promo_editable=="no"){echo "disabled";}?>><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></td>
	      				</tr>
	      				<tr>
	      					<td>Start Date:</td>
	      					<td><span id="disp_deal_start_date_edit_container"><?php echo $promo_start_date?></span></td>
	      					<td><button class="btn btn-primary btn-fab btn-fab-mini btn-round"  onclick="open_start_date_edit()" id="button_deal_start_date_edit_container" <?php if($is_promo_editable=="no"){echo "disabled";}?>> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></td>
	      				</tr>
	      				<tr>
	      					<td>End Date:</td>
	      					<td><span id="disp_deal_end_date_edit_container"> <?php echo $promo_end_date?></span></td>
	      					<td><button class="btn btn-primary btn-fab btn-fab-mini btn-round" id="button_deal_end_date_edit_container" onclick="open_end_date_edit()" <?php if($is_promo_editable=="no"){echo "disabled";}?>><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></td>
	      				</tr>
	      			</table>
	      		</div>

<input type="hidden" id="main_promo_id" value="<?php echo $promo_uid ?>">
			    
	<div class="container-fluid bg-info" id="deal_name_edit_container" style="display:none">
	       <div class="row">
			    <label class="control-label col-md-1 bold">Deal Name:</label>
			    <div class="col-md-6">
			      <input type="text" class="form-control" id="deal_name" name="deal_name" required placeholder="Deal Name" value="<?php echo $promo_name;?>">
			    </div>
			    <div class="col-md-5">
			    	<button class="btn btn-success btn-xs" onclick="save_deal_name_edit(this)" <?php if($is_promo_editable=="no"){echo "disabled";}?>>Submit</button> <button class="btn btn-danger btn-xs" onclick="close_deal_name_edit()" <?php if($is_promo_editable=="no"){echo "disabled";}?>>Cancel</button>
			    </div>
			</div>
	</div>

	<div class="container-fluid bg-info">
	<input type="hidden" id="SKU_PARAMS">
	<div class="row" id="display_realted_input_form" style="display:none">
			    <label class="control-label col-md-1" for="email">Deal Offer:</label>
			    <div class="col-md-11" id="single" style="display:none">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="col-md-2" align="center">
                                <label class="control-label" id="firstWord"></label>
                            </div>
                            <div class="col-md-4" id="firstInput">
                                
                            </div>
                            <div class="col-md-6" id="show_buy_select">
                                
                            </div>
                        </div>
                        <div class="col-md-1" align="center">
                            <label class="control-label" id="middleWord" ></label>
                        </div>
                        <div class="col-md-5">
                            <div class="col-md-4" id="secondInput">
                                
                            </div>
                            <div class="col-md-5" id="show_get_select">
                                
                            </div>
                            <div class="col-md-2">
                            <label class="control-label" id="endWord" ></label>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-md-11" id="multiple" style="display:none">
                    <div class="entry row">
                        <div class="col-md-5">
                            <div class="col-md-2" align="center">
                                <label class="control-label" id="firstWord_m"></label>
                            </div>
                            <div class="col-md-4" id="firstInput_m">
                                
                            </div>
                            <div class="col-md-6" id="show_buy_select_m">
                                
                            </div>
                        </div>
                        <div class="col-md-1" align="center">
                            <label class="control-label" id="middleWord_m" ></label>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-4" id="secondInput_m">
                                
                            </div>
                            <div class="col-md-5" id="show_get_select_m">
                                
                            </div>
                            <div class="col-md-2">
                            <label class="control-label" id="endWord_m" ></label>
                            </div>
                        </div>
                        <div class="col-md-2 pull-right" id="add_button_tiers">
                            
                        </div>   
                    </div>
                </div>
			    <div class="col-md-12">
                    <div class="row" id="apply_promotion_at_check_out">
                        
                    </div>
                </div>
			    <div class="col-md-12">
			    	<div class="row" align="center">
					    <label class="control-label" ><h4><ul id="final_offer_name"><?php if($qualify_for_tier!=1) {
                                echo $promo_quote;
                                }else{
                                    echo $promo_quotes;
                                }?></ul></h4></label>
					    <input type="hidden" id="final_offer_value" name="final_offer_value" value='<?php echo $promo_quote ?>'>
					</div>
					<div class="row" align="center"><div class="col-md-12"><button class="btn btn-success btn-xs" onclick="save_deal_quote_edit(this)" >Submit</button> <button class="btn btn-danger btn-xs" onclick="close_deal_quote_edit()">Cancel</button></div></div>
			    </div>
			</div>
			</div>
			
	      	<div class="container-fluid bg-info" id="deal_start_date_edit_container" style="display:none">
					<div class="row">
						<label class="control-label col-md-1" for="email">Start Date:</label>
						<div class="col-md-6">
							<input type='text' class="form-control" name="start_date" id='start_date' value="<?php echo $promo_start_datev?>" required />
						</div>
						<div class="col-md-5"><button class="btn btn-success btn-xs" onclick="save_start_date_edit(this)">Submit</button> <button class="btn btn-danger btn-xs" onclick="close_start_date_edit()">Cancel</button></div>
					</div>
			</div>
			<div class="container-fluid bg-info" id="deal_end_date_edit_container" style="display:none">
					<div class="row">
						<label class="control-label col-md-1" for="email">End Date:</label>
						<div class="col-md-6">
			                <input type='text' class="form-control" name="end_date" id='end_date' <?php echo $controller->getEndDateDepend($quote_selector); ?> value="<?php echo $promo_end_datev?>" required />
			                
						</div>
						<div class="col-md-5"><button class="btn btn-success btn-xs" onclick="save_end_date_edit(this)">Submit</button> <button class="btn btn-danger btn-xs" onclick="close_end_date_edit()">Cancel</button></div>
					</div>
			</div>
	      		
	      	</div>
	      </div>
	      </div>
	    </div>


</div>
<script>
	function open_deal_name_edit(){
		document.getElementById('deal_name_edit_container').style.display=""
		//document.getElementById('button_deal_name_edit_container').style.display="none";
		document.getElementById('button_deal_name_edit_container').style.visibility="hidden";
		
	}
	function close_deal_name_edit(){
		document.getElementById('deal_name_edit_container').style.display="none"
	//	document.getElementById('button_deal_name_edit_container').style.display="";
		document.getElementById('button_deal_name_edit_container').style.visibility="visible";
	}
	function save_deal_name_edit(obj){
		var new_Deal_Name=document.getElementById('deal_name').value
        var promo_id=document.getElementById('main_promo_id').value
        if(new_Deal_Name!='<?php echo $promo_name ?>'){
        	
        
        save_deal_name=1;
		deactivate_flag=0;
        <?php if($promo_active){ ?>
				
			var answer=confirm("Warning! You have edited an active promotion. Continuing will first deactivate this promotion and save it in `Reverted Drafts`. You can further action it there.")
			if(answer){
				save_deal_name=1;
				deactivate_flag=1;
			}
			else{
				save_deal_name=0;
				return false;
			}
				
		<?php } ?>
		var approval_flag=0
        <?php if($approval_sent==1 && $promo_active==0){?> 
    

            var answer = confirm ("WARNING! This promotion is already sent for approvals. Changing deal name will cancel the approval request.");
            if (answer){
                goAhead=1
                approval_flag=1
                }
            else{goAhead=0;return false}

        <?php } ?>
        
        
        if(new_Deal_Name!=""){
        	var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
					obj.classList.remove("btn-success");
					obj.classList.add("btn-warning");
					obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
		                	if(xhr.responseText==1){
							
							document.getElementById('disp_deal_name_edit_container').innerHTML=new_Deal_Name
							document.getElementById('promotion_summary_promo_name').innerHTML=new_Deal_Name

							if(document.getElementById('approve_promo_button1')==null){
								var top_activate_button_div=document.getElementById('top_right_button_action')
								top_activate_button_div.innerHTML=""
							var top_activate_button=document.createElement('button');
								top_activate_button.innerHTML="Send For Approval"
								top_activate_button.classList.add('btn','btn-success','btn-sm')
								top_activate_button.setAttribute('onclick',"send_approval_for_promotion(this)")
								top_activate_button.setAttribute('promo_id',promo_id)
								top_activate_button.setAttribute('id','approve_promo_button1')
								top_activate_button_div.appendChild (top_activate_button);
							}
							
							if(document.getElementById('approve_promo_button2')==null){
								var bottom_activate_button_div=document.getElementById('bottom_activate_button_div')
								bottom_activate_button_div.innerHTML=""
							var bottom_activate_button=document.createElement('button');
								bottom_activate_button.innerHTML="Send For Approval"
								bottom_activate_button.classList.add('btn','btn-success','btn-lg')
								bottom_activate_button.setAttribute('onclick',"send_approval_for_promotion(this)")
								bottom_activate_button.setAttribute('promo_id',promo_id)
								bottom_activate_button.setAttribute('id','approve_promo_button2')
								bottom_activate_button_div.appendChild (bottom_activate_button);
							}
							
							obj.classList.remove("btn-warning");
							obj.classList.add("btn-success");
							obj.innerHTML='<i class="fa fa-floppy-o" aria-hidden="true"></i>'
							document.getElementById('deal_name_edit_container').style.display="none"
							//document.getElementById('button_deal_name_edit_container').style.display="";
							document.getElementById('button_deal_name_edit_container').style.visibility="visible";
		                	}
		                	else{
		                	obj.classList.remove("btn-warning");
							obj.classList.add("btn-success");
							obj.innerHTML='<i class="fa fa-floppy-o " aria-hidden="true"></i>'
							document.getElementById('deal_name_edit_container').style.display="none"
							//document.getElementById('button_deal_name_edit_container').style.display="";
							document.getElementById('button_deal_name_edit_container').style.visibility="visible";
		                	}
		                	
		                	if(approval_flag==1){
                                alert("The approval request is cancelled")
                                window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
                            }
                        
                        
                            if(deactivate_flag==1){
                                alert("This promotion is deactivated")
                                window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
                            }
		                	
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/update_promotion_deal_name", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("promo_id="+promo_id+"&new_Deal_Name="+new_Deal_Name+"&deactivate_flag="+deactivate_flag+"&approval_flag="+approval_flag);
		        }
		    }
		    else{
        	alert('Deal Name cannot be null')
        	}
        }
        else{
        	obj.classList.remove("btn-warning");
			obj.classList.add("btn-success");
			obj.innerHTML='<i class="fa fa-floppy-o" aria-hidden="true"></i>'
			document.getElementById('deal_name_edit_container').style.display="none"
			//document.getElementById('button_deal_name_edit_container').style.display="";
			document.getElementById('button_deal_name_edit_container').style.visibility="visible";
        	}
	}
	function open_deal_quote_edit(obj){
		display_realted_input_form(obj)
		document.getElementById('display_realted_input_form').style.display=""
		//document.getElementById('button_deal_quote_edit_container').style.display="none"
		document.getElementById('button_deal_quote_edit_container').style.visibility="hidden";
		<?php if($qualify_for_tier==1) { ?>
              document.getElementById('disp_deal_quote_edit_container').innerHTML=""
         <?php  }?>
         $('.entry:not(:first)').remove()
	}
	
var getWord= "\\b"+"get"+"\\b";
var middleGetWord="\\b"+"get a surprise gift"+"\\b";
var start_word;
var middle_word;
var sku_Word="\\b"+"select sku"+"\\b";
var getSame="\\b"+"same"+"\\b";
var getDifferent="\\b"+"different"+"\\b";
  function getSkuResult(same_word){

            if(same_word.match(getSame)){
                return "(Same SKUs)"
        }
        if(same_word.match(getDifferent)){
            return "(Different SKUs)"
        }
        else{
            return ""
        }
    }	
	function getMatchedResult(start_word,getWord){
			if(start_word.match(getWord)){
				return true
			}
			else{
				return false
			}
		}
	function getSkuWord(findSku){

                if(findSku.match(sku_Word)){
                    return true
                }
                if(findSku.match(getDifferent)){
                    return false;
                }
            }
    var buy_type="" 
    var get_type=""     
    $(function() {
        $(document).on('click', '.btn-add', function(e) {

            e.preventDefault();

            var controlForm = $('#multiple');


            laste=$(this).parents('.entry:last').parent().children().last();
 
            var btnNum= parseInt($(this).attr('num'))

            if(laste.find('input').eq(1).val()!="" && laste.find('input').eq(0).val()!=""){
                
            var btnNum= parseInt($(this).attr('num'))
            var $inputs = $('#multiple .entry:last :input').not(':input[type=button], :input[type=submit], :input[type=reset]');
            var htmls=""
            $inputs.each(function() {
                    if($(this).val()==""){
                         return false;
                    }
            })
            $inputs.each(function() {
            
            
            if($(this).attr('name')=='buy_deal_value[]'){
                htmls+='Buy above '+$(this).val()+''
            }
            if($(this).attr('name')=='buy_deal_type[]'){
                htmls+=$(this).val()+""
                buy_type=$(this).val()
            }

            if($(this).attr('name')=='get_deal_value[]'){
                htmls+='And Get '+$(this).val()+'% '
            }
            if($(this).attr('name')=='get_deal_type[]'){
                htmls+=$(this).val()+"<br>"
                get_type=$(this).val()
            }
           });

                
                
                
                currentEntry = $(this).parents('.entry:first');

                newEntry = $(currentEntry.clone()).appendTo(controlForm);
newEntry.find('input').val('');
newEntry.find('select:eq(0)').replaceWith('<input type="text" value="'+buy_type+'" class="form-control" name="buy_deal_type[]" readonly>')
newEntry.find('select[name*="get_deal_type[]"]').replaceWith('<input type="text" value="'+get_type+'" class="form-control" name="get_deal_type[]" readonly>')

$( "input[name*='buy_deal_type[]']" ).val(buy_type);
$( "input[name*='get_deal_type[]']" ).val(get_type);

            controlForm.find('.entry:last .btn-apply').attr('num',btnNum+'_listNUM')
            controlForm.find('.entry:not(:first) .btn-add').removeClass('btn-add').addClass('btn-remove').removeClass('btn-success').addClass('btn-danger').html('<span class="glyphicon glyphicon-minus"></span>').attr('num',btnNum+'_listNUM');
            
         controlForm.find('.entry:last .btn-add').attr('num',btnNum+1) 
            }else{
                alert("Fill input fields First.Then add more fields")
                return false;
            }
        $(this).attr('num',btnNum+1)          
        }).on('click', '.btn-remove', function(e) {

            $(this).parents('.entry:first').remove();
            var targetlist=$(this).attr('num')

            $('#'+targetlist).remove();
            //document.getElementById('final_offer_value').value=document.getElementById('final_offer_name').innerHTML
            resetFunct()
            e.preventDefault();
            return false;
        });
    });
function resetFunct(){
    document.getElementById('final_offer_name').innerHTML=""
    document.getElementById('final_offer_value').value=""
    var btnNum= 0
    var enteryElems=document.getElementsByClassName('entry');
    if(enteryElems.length > 1){
            
            
            
            var htmls=""
            var htmlv=""
            var $inputs = $(':input').not(':input[type=button], :input[type=submit], :input[type=reset]');
            $inputs.each(function() {
                
                
                if($(this).attr('name')=='buy_deal_value[]'){
                    if($(this).val()==""){
                         return false;
                    }
                    htmls+='<li class="_listNUM" id="'+btnNum+'_listNUM'+'">'+"Buy above "+$(this).val()+" "
                    htmlv+='Buy above '+$(this).val()+''
                }
                if($(this).attr('name')=='buy_deal_type[]'){
                    htmls+=$(this).val()+" "
                    buy_type=$(this).val()
                    htmlv+=$(this).val()+" "
                }
    
                if($(this).attr('name')=='get_deal_value[]'){
                    htmls+="And Get "+$(this).val()+"% "
                    htmlv+='And Get '+$(this).val()+'% '
                }
                if($(this).attr('name')=='get_deal_type[]'){
                    htmlv+=$(this).val()+"<br>"
                    htmls+=$(this).val()+'</li>'
                    get_type=$(this).val()
                    //alert(btnNum)
                    $('.btn-remove').eq(btnNum).attr('num',btnNum+1+'_listNUM')
                    $('.btn-apply').eq(btnNum).attr('num',btnNum+'_listNUM')
                    btnNum+=1;
                    
                }
                

            });


            document.getElementById('final_offer_name').innerHTML+=htmls
            document.getElementById('final_offer_value').value+=htmlv
            $(btnNum+'_listNUM').remove()

    }
}    

function apply(obj){
    var btnApply=obj.getAttribute('num')
    //alert(btnApply)
    if(document.getElementById(btnApply)==null){

    var $inputs = $(obj).parent().parent().find(':input').not(':input[type=button], :input[type=submit], :input[type=reset]');
            var htmls=""
            $inputs.each(function() {
                    if($(this).val()==""){
                         return false;
                    }
            })
           $inputs.each(function() {
            if($(this).attr('name')=='buy_deal_value[]'){
                htmls+='Buy above '+$(this).val()+''
            }
            if($(this).attr('name')=='buy_deal_type[]'){
                htmls+=$(this).val()+" "
                buy_type=$(this).val()
            }

            if($(this).attr('name')=='get_deal_value[]'){
                htmls+='And Get '+$(this).val()+'% '
            }
            if($(this).attr('name')=='get_deal_type[]'){
                htmls+=$(this).val()+"<br>"
                get_type=$(this).val()
            }
           });
        document.getElementById('final_offer_name').innerHTML+='<li id="'+btnApply+'">'+htmls+'</li>'
            document.getElementById('final_offer_value').value+=htmls
     }
     else{
            var $inputs = $(obj).parent().parent().find(':input').not(':input[type=button], :input[type=submit], :input[type=reset]');
            var htmls=""
            $inputs.each(function() {
                    if($(this).val()==""){
                         return false;
                    }
            })
            $inputs.each(function() {
            if($(this).attr('name')=='buy_deal_value[]'){
                htmls+='Buy above '+$(this).val()+''
            }
            if($(this).attr('name')=='buy_deal_type[]'){
                htmls+=$(this).val()+" "
                buy_type=$(this).val()
            }

            if($(this).attr('name')=='get_deal_value[]'){
                htmls+='And Get '+$(this).val()+'% '
            }
            if($(this).attr('name')=='get_deal_type[]'){
                htmls+=$(this).val()+"<br>"
                get_type=$(this).val()
            }
           });
           updateValue()
        document.getElementById(btnApply).innerHTML=htmls
          
     }
}

function updateValue(){
    document.getElementById('final_offer_value').value=""
        var btnNum= 0
    var enteryElems=document.getElementsByClassName('entry');
    if(enteryElems.length > 0){
            
            var htmls=""
            
            var $inputs = $(':input').not(':input[type=button], :input[type=submit], :input[type=reset]');
           $inputs.each(function() {
            if($(this).attr('name')=='buy_deal_value[]'){
                htmls+='Buy above '+$(this).val()+' '
            }
            if($(this).attr('name')=='buy_deal_type[]'){
                htmls+=$(this).val()+" "
                buy_type=$(this).val()
            }

            if($(this).attr('name')=='get_deal_value[]'){
                htmls+='And Get '+$(this).val()+'% '
            }
            if($(this).attr('name')=='get_deal_type[]'){
                htmls+=$(this).val()+"<br>"
                get_type=$(this).val()
            }
           });
            document.getElementById('final_offer_value').value+=htmls
    }
}

        
function showBuyDealTypem(obj){
    $( "input[name*='buy_deal_type[]']" ).val(obj.value);
    document.getElementById('final_offer_name').innerHTML=""
    document.getElementById('final_offer_value').value=""
    var btnNum= 0
    var enteryElems=document.getElementsByClassName('entry');
    if(enteryElems.length > 1){
            
            var htmls=""
            var htmlv=""
            var $inputs = $(':input').not(':input[type=button], :input[type=submit], :input[type=reset]');

		$inputs.each(function() {
                
                
                if($(this).attr('name')=='buy_deal_value[]'){
                    if($(this).val()==""){
                         return false;
                    }
                    htmls+='<li class="_listNUM" id="'+btnNum+'_listNUM'+'">'+"Buy above "+$(this).val()+" "
                    htmlv+='Buy above '+$(this).val()+' '
                }
                if($(this).attr('name')=='buy_deal_type[]'){
                    htmls+=$(this).val()+" "
                    buy_type=$(this).val()
                    htmlv+=$(this).val()+" "
                }
    
                if($(this).attr('name')=='get_deal_value[]'){
                    htmls+="And Get "+$(this).val()+"% "
                    htmlv+='And Get '+$(this).val()+'% '
                }
                if($(this).attr('name')=='get_deal_type[]'){
                    htmlv+=$(this).val()+"<br>"
                    htmls+=$(this).val()+'</li>'
                    get_type=$(this).val()
                    //alert(btnNum)
                    $('.btn-remove').eq(btnNum).attr('num',btnNum+1+'_listNUM')
                    $('.btn-apply').eq(btnNum).attr('num',btnNum+'_listNUM')
                    btnNum+=1;
                    
                }
                

            });

            document.getElementById('final_offer_name').innerHTML+=htmls
            document.getElementById('final_offer_value').value+=htmlv
            $(btnNum+'_listNUM').remove()

    }
    

}
function showGetDealTypem(obj){
    $( "input[name*='get_deal_type[]']" ).val(obj.value);
    document.getElementById('final_offer_name').innerHTML=""
    document.getElementById('final_offer_value').value=""
    var btnNum= 0
    var enteryElems=document.getElementsByClassName('entry');
     if(enteryElems.length > 1){
            
            var htmls=""
            var htmlv=""
            var $inputs = $(':input').not(':input[type=button], :input[type=submit], :input[type=reset]');
            $inputs.each(function() {
                
                
                if($(this).attr('name')=='buy_deal_value[]'){
                    if($(this).val()==""){
                         return false;
                    }
                    htmls+='<li class="_listNUM" id="'+btnNum+'_listNUM'+'">'+"Buy above "+$(this).val()+" "
                    htmlv+='Buy above '+$(this).val()+' '
                }
                if($(this).attr('name')=='buy_deal_type[]'){
                    htmls+=$(this).val()+" "
                    buy_type=$(this).val()
                    htmlv+=$(this).val()+" "
                }
    
                if($(this).attr('name')=='get_deal_value[]'){
                    htmls+="And Get "+$(this).val()+"% "
                    htmlv+='And Get '+$(this).val()+'% '
                }
                if($(this).attr('name')=='get_deal_type[]'){
                    htmlv+=$(this).val()+"<br>"
                    htmls+=$(this).val()+'</li>'
                    get_type=$(this).val()
                    //alert(btnNum)
                    $('.btn-remove').eq(btnNum).attr('num',btnNum+1+'_listNUM')
                    $('.btn-apply').eq(btnNum).attr('num',btnNum+'_listNUM')
                    btnNum+=1;
                    
                }
                

            });

            document.getElementById('final_offer_name').innerHTML+=htmls
            document.getElementById('final_offer_value').value+=htmlv
            $(btnNum+'_listNUM').remove()

    }
}	
	function restrict_to_below_100Fun(obj){
	if(obj.value>=100){
		alert("Please choose below 100%");
		obj.value="";
		return false;
	}
}
	function isNumberwithdot(evt) {
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode > 31 && ((charCode < 48 || charCode > 57) && charCode!=46)) {
		return false;
	}
	return true;
}	
	function display_realted_input_form(obj){	
		var parentDiv = document.getElementById('display_realted_input_form');
		document.getElementById('apply_promotion_at_check_out').innerHTML="";	
		var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
							select_data=JSON.parse(xhr.responseText)
							var buy_input_arr=select_data[0]
							var buy_select_arr=select_data[1]
							var buy_deal_value='<?php echo $buy_deal_value?>'
							var get_deal_value="<?php echo $get_deal_value?>"
							var	get_deal_type="<?php echo $get_deal_type?>"
							var	buy_deal_type="<?php echo $buy_deal_type?>"
                            //alert(get_deal_value)
                            //alert(buy_deal_value)
							for (var i=0;i < buy_input_arr.length;i++){
							        var discount_word=buy_input_arr[i].promotion_quote.toLowerCase()
                                    var reverse_select_input
                                    if(getdiscountResult(discount_word)){
                                        reverse_select_input=1;
                                    }
                                    else{
                                        reverse_select_input=0;
                                    }
									var singleItemDiscount
									if(getDiscountForSingle(discount_word)){
										singleItemDiscount=0;
									}else{
										singleItemDiscount=1;
									}
							    var same_word=buy_input_arr[i].promotion_quote.toLowerCase() ;
                                 document.getElementById('SKU_PARAMS').value= getSkuResult(same_word)
							    
                                    <?php if($qualify_for_tier==1){?>
                                    middle_word=buy_input_arr[i].middle_word.toLowerCase() 
                                    start_word=buy_input_arr[i].promotion_quote.toLowerCase()
                                    getWord= "\\b"+"tiered discount"+"\\b";
                                    if(getMatchedResult(start_word,getWord)){
                                        
                                        <?php if($qualify_for_tier==1){
                                            $buy_deal_value_arr=explode(',',$buy_deal_value);
                                            $get_deal_value_arr=explode(',',$get_deal_value);
                                            $get_deal_type_arr=explode(',',$get_deal_type);
                                            $buy_deal_type_arr=explode(',',$buy_deal_type);
                                        }?>
                                        var buy_val=[];
                                        var get_val=[];
                                        <?php  foreach($buy_deal_value_arr as $arr){
                                            ?>
                                            buy_val.push('<?php echo $arr;?>')
                                        <?php  }?>
                                        
                                        <?php foreach($get_deal_value_arr as $arr){

                                            ?>
                                            get_val.push('<?php echo $arr;?>')
                                        <?php   }?>
                                        var repetation='<?php echo count($buy_deal_value_arr)?>'
                                        //alert(repetation)
                                        
                                    var show_get_select=buy_input_arr[i].show_get_select
                                    document.getElementById('firstWord_m').innerHTML=buy_input_arr[i].start_word
                                    document.getElementById('middleWord_m').innerHTML=buy_input_arr[i].middle_word 
                                    document.getElementById('endWord_m').innerHTML=buy_input_arr[i].end_word
                                    
                                        document.getElementById('multiple').style.display=""
                                        document.getElementById('single').style.display="none";
										document.getElementById('add_button_tiers').innerHTML="";
                                        var parentdivforTierBtn=document.getElementById('add_button_tiers');
                                        var appendBtn=document.createElement('button');
                                           appendBtn.classList.add('btn-sm','btn','btn-success','btn-apply','pull-left');
                                           appendBtn.setAttribute('num','0_listNUM');
                                           appendBtn.setAttribute('onclick','apply(this)');
                                           appendBtn.innerHTML='<span class="glyphicon glyphicon-ok"></span>';
                                           appendBtn.type="button";
                                           parentdivforTierBtn.appendChild(appendBtn)
                                        var appendBtn=document.createElement('button');
                                           appendBtn.classList.add('btn-sm','btn','btn-success','btn-add');
                                           appendBtn.setAttribute('num','1');
                                           appendBtn.innerHTML='<span class="glyphicon glyphicon-plus"></span>';
                                           appendBtn.type="button";
                                           appendBtn.setAttribute('style','margin-left:5px');
                                           parentdivforTierBtn.appendChild(appendBtn)
                                        
                                                           
                                        parDivFirstInput=document.getElementById('firstInput_m')
                                    parDivFirstInput.innerHTML=""
                                    if(buy_input_arr[i].input_place_for_buy==1){
                                        var inputElem=document.createElement('input')
                                        inputElem.type="number"
                                        inputElem.classList.add("form-control");
                                        inputElem.setAttribute('data_type',buy_input_arr[i].input_type_for_buy);
                                        inputElem.name="buy_deal_value[]";
                                        inputElem.value='<?php echo $buy_deal_value_arr[0]?>';
                                        inputElem.setAttribute('required',true);
                                        parDivFirstInput.appendChild (inputElem);
                                        if(buy_input_arr[i].input_type_for_buy=="date"){
                                            /*$('#buy_deal_value').datetimepicker({
                                                format: 'YYYY-MM-DD HH:mm',
                                                minDate: new Date(),
                                                useCurrent: false,
                                            });
                                            
                                            $("#buy_deal_value").on("dp.change", function (e) {
                                                document.getElementById('end_date').value=$("#buy_deal_value").val();
                                                $('#start_date').data("DateTimePicker").maxDate(e.date);
                                                putFinalDeal()
                                            });*/
											
											$('#buy_deal_value').bootstrapMaterialDatePicker({
												time: true,
												weekStart: 0, 
												format: 'YYYY-MM-DD HH:mm', 
												shortTime : true
											 }).on('change', function(e, date){
												document.getElementById('end_date').value=$("#buy_deal_value").val();
												//$('#start_date').data("DateTimePicker").maxDate(e.date);
												$('#start_date').bootstrapMaterialDatePicker('setMaxDate', date);
												putFinalDeal()
											});
											
                                            document.getElementById('end_date').readOnly=true
                                        }
                                        else{
                                            document.getElementById('end_date').readOnly=false
                                        }
                                    }
                                    else{
                                        parDivFirstInput.innerHTML=""
                                    }
                                    
                                    parDivsecondInput=document.getElementById('secondInput_m')
                                    parDivsecondInput.innerHTML=""
                                    if(buy_input_arr[i].input_place_for_get==1){
                                        var inputElem=document.createElement('input')
                                        inputElem.type=buy_input_arr[i].input_type_for_get
                                        inputElem.classList.add("form-control");
                                        appendBtn.setAttribute('required',true);
                                        inputElem.name="get_deal_value[]";
                                        inputElem.value='<?php echo $get_deal_value_arr[0]?>'
                                        inputElem.setAttribute('required',true);
										inputElem.setAttribute('onkeypress','return isNumberwithdot(event);');
										inputElem.setAttribute('onkeyup','return restrict_to_below_100Fun(this);');
                                        parDivsecondInput.appendChild (inputElem);
                                        
                                    }
                                    else{
                                        parDivsecondInput.innerHTML=""
                                    }
                                    
                                    parDivshow_buy_select=document.getElementById('show_buy_select_m')
                                    parDivshow_buy_select.innerHTML=""
                                    if(buy_input_arr[i].show_buy_select==1){
                                        var selectElement = document.createElement ("select");
                                        selectElement.classList.add("form-control");
                                        selectElement.setAttribute('id','buy_deal_type');
                                        selectElement.setAttribute('onchange','showBuyDealTypem(this)');
                                        selectElement.name="buy_deal_type[]";
                                        selectElement.required=true;
                                        parDivshow_buy_select.appendChild (selectElement);
                                        
                                        var buy_select_arr=select_data[1]
                                        buySelectElement=document.getElementById('buy_deal_type')
                                        buySelectElement.innerHTML=""
                                        var option = new Option ("Select Type","");
                                        buySelectElement.options[buySelectElement.options.length] = option;
                                        var prev="";
                                        for (var i=0;i < buy_select_arr.length;i++)
                                        {
                                            if (prev != buy_select_arr[i].buy_units) {
                                                var option = new Option(buy_select_arr[i].buy_units, buy_select_arr[i].buy_units);
                                                buySelectElement.options[buySelectElement.options.length] = option;
                                            }
                                            prev=buy_select_arr[i].buy_units;
                                        }
                                        document.getElementById('buy_deal_type').value='<?php echo $buy_deal_type_arr[0]?>'
                                    }
                                    else{
                                        parDivshow_buy_select.innerHTML=""
                                    }
                                    
                                    parDivshow_get_select=document.getElementById('show_get_select_m')
                                    parDivshow_get_select.innerHTML=""
                                    if(show_get_select==1){
                                        var selectElement = document.createElement ("select");
                                        selectElement.classList.add("form-control");
                                        selectElement.setAttribute('id','get_deal_type');
                                        selectElement.setAttribute('onchange','showGetDealTypem(this)');
                                        selectElement.name="get_deal_type[]";
                                        selectElement.required=true;
                                        parDivshow_get_select.appendChild (selectElement);
                                        
                                        getSelectElement=document.getElementById('get_deal_type')
                                        getSelectElement.innerHTML=""
                                        var option = new Option ("Select Type","");
                                        getSelectElement.options[getSelectElement.options.length] = option;
                                        for (var i=0;i < buy_select_arr.length;i++)
                                        {
                                            var option = new Option (buy_select_arr[i].give_units,buy_select_arr[i].give_units);
                                            getSelectElement.options[getSelectElement.options.length] = option;
                                        }
                                        document.getElementById('get_deal_type').value='<?php echo $get_deal_type_arr[0]?>';
                                    }
                                    else{
                                        parDivshow_get_select.innerHTML=""
                                    }

                                    if(repetation>1){
                                        $('.btn-add').attr('num',repetation)
                                        var repet=1
                                        var buy_type='<?php echo $buy_deal_type_arr[0]?>';
                                        var get_type='<?php echo $get_deal_type_arr[0]?>';
                                     while(repet<repetation){

                                         var controlForm = $('#multiple');
                                         currentEntry = $('.entry:first');
                                        newEntry = $(currentEntry.clone()).appendTo(controlForm);
                                        newEntry.find('input').val('');
                                        newEntry.find('select:eq(0)').replaceWith('<input type="text" value="'+buy_type+'" class="form-control" name="buy_deal_type[]" readonly>')
                                        newEntry.find('select[name*="get_deal_type[]"]').replaceWith('<input type="text" value="'+get_type+'" class="form-control" name="get_deal_type[]" readonly>')
                                        
                                        $( "input[name*='buy_deal_type[]']" ).val(buy_type);
                                        $( "input[name*='get_deal_type[]']" ).val(get_type);
                                        $( "input[name='buy_deal_value[]']" ).eq(repet).val(buy_val[repet]);
                                        $( "input[name='get_deal_value[]']" ).eq(repet).val(get_val[repet]);
                                        
                                                    controlForm.find('.entry:last .btn-apply').attr('num',repet+'_listNUM')
                                                    controlForm.find('.entry:not(:first) .btn-add').removeClass('btn-add').addClass('btn-remove').removeClass('btn-success').addClass('btn-danger').html('<span class="glyphicon glyphicon-minus"></span>').attr('num',repet+'_listNUM');
                                                    
                                                 controlForm.find('.entry:last .btn-add').attr('num',repet+1) 
                                        
                                         
                                         repet+=1;
                                     }
                                        
                                    }
                                       
                                    if(buy_select_arr==false){
                                         putFinalDeal()
                                     } 
                                       
                                      return false; 
                                      
                                       
                                    }
                        <?php } ?>
                        var show_get_select=buy_input_arr[i].show_get_select
                        document.getElementById('firstWord').innerHTML=buy_input_arr[i].start_word
                        document.getElementById('middleWord').innerHTML=buy_input_arr[i].middle_word 
                        document.getElementById('endWord').innerHTML=buy_input_arr[i].end_word
                        
                        document.getElementById('multiple').style.display="none"
                        document.getElementById('single').style.display=""
                                    
                        document.getElementById('add_button_tiers').innerHTML=""            
                                    
                        start_word=buy_input_arr[i].start_word.toLowerCase()

                        var getWord= "\\b"+"get"+"\\b"
                        middle_word=buy_input_arr[i].middle_word.toLowerCase() 
                        
                        if(getMatchedResult(start_word,getWord)){

                                    parDivFirstInput=document.getElementById('firstInput')
                                parDivFirstInput.innerHTML=""
                                if(buy_input_arr[i].input_place_for_buy==1){
                                    var inputElem=document.createElement('input')
                                    inputElem.type="text"
                                    inputElem.classList.add("form-control");
                                    inputElem.setAttribute('id','get_deal_value');
                                    inputElem.setAttribute('data_type',buy_input_arr[i].input_type_for_buy);
                                    inputElem.setAttribute('onkeyup','showGetDealValue()');
                                    inputElem.name="get_deal_value";
                                    inputElem.setAttribute('required',true);
                                    inputElem.value=get_deal_value;
                                    parDivFirstInput.appendChild (inputElem);
                                    if(buy_input_arr[i].input_type_for_buy=="date"){
                                       
/*									   $('#buy_deal_value').datetimepicker({
                                            format: 'YYYY-MM-DD HH:mm',
                                            minDate: new Date(),
                                            useCurrent: false,
                                        });
                                        
                                        $("#buy_deal_value").on("dp.change", function (e) {
                                            document.getElementById('end_date').value=$("#buy_deal_value").val();
                                            $('#start_date').data("DateTimePicker").maxDate(e.date);
                                            putFinalDeal()
                                        });
										*/
										$('#buy_deal_value').bootstrapMaterialDatePicker({
											time: true,
											weekStart: 0, 
											format: 'YYYY-MM-DD HH:mm', 
											shortTime : true
										 }).on('change', function(e, date){
											document.getElementById('end_date').value=$("#buy_deal_value").val();
											//$('#start_date').data("DateTimePicker").maxDate(e.date);
											$('#start_date').bootstrapMaterialDatePicker('setMaxDate', date);
											putFinalDeal()
										});
                                        document.getElementById('end_date').readOnly=true
                                    }
                                    else{
                                        document.getElementById('end_date').readOnly=false
                                    }
                                    document.getElementById('get_deal_value').value='<?php echo $get_deal_value ?>'
                                }
                                else{
                                    parDivFirstInput.innerHTML=""
                                }
                                
                                parDivsecondInput=document.getElementById('secondInput')
                                parDivsecondInput.innerHTML=""
                                if(buy_input_arr[i].input_place_for_get==1){
                                    var inputElem=document.createElement('input')
                                    inputElem.type=buy_input_arr[i].input_type_for_get
                                    inputElem.classList.add("form-control");
                                    inputElem.setAttribute('id','buy_deal_value');
                                    inputElem.setAttribute('onkeyup','showBuyDealValue()');
                                    inputElem.name="buy_deal_value";
                                    inputElem.value=buy_deal_value;
                                    inputElem.setAttribute('required',true);
                                    if(buy_input_arr[i].input_type_for_buy=="date"){
                                        inputElem.type="text";
                                        inputElem.setAttribute('style','display:none')
                                    }
                                    else{
                                        inputElem.setAttribute('style','display:')
                                    }
                                    parDivsecondInput.appendChild (inputElem);
                                    
                                }
                                else{
                                    parDivsecondInput.innerHTML=""
                                }
                                parDivshow_buy_select=document.getElementById('show_buy_select')
                                parDivshow_buy_select.innerHTML=""
                                if(buy_input_arr[i].show_buy_select==1){
                                    var selectElement = document.createElement ("select");
                                    selectElement.classList.add("form-control");
                                    selectElement.setAttribute('id','get_deal_type');
                                    selectElement.setAttribute('onchange','showGetDealType()');
                                    selectElement.name="get_deal_type";
                                    selectElement.required=true;
                                    parDivshow_buy_select.appendChild (selectElement);
                                    
                                    var buy_select_arr=select_data[1]
                                    buySelectElement=document.getElementById('get_deal_type')
                                    buySelectElement.innerHTML=""
                                    var option = new Option ("Select Type","");
                                    buySelectElement.options[buySelectElement.options.length] = option;
                                    for (var i=0;i < buy_select_arr.length;i++)
                                    {
                                        var option = new Option (buy_select_arr[i].buy_units,buy_select_arr[i].buy_units);
                                        buySelectElement.options[buySelectElement.options.length] = option;
                                    }
                                    document.getElementById('get_deal_type').value=get_deal_type
                                }
                                else{
                                    parDivshow_buy_select.innerHTML=""
                                }
                                
                                parDivshow_get_select=document.getElementById('show_get_select')
                                parDivshow_get_select.innerHTML=""
                                if(show_get_select==1){
                                    var selectElement = document.createElement ("select");
                                    selectElement.classList.add("form-control");
                                    selectElement.setAttribute('id','buy_deal_type');
                                    selectElement.setAttribute('onchange','showBuyDealType()');
                                    selectElement.name="buy_deal_type";
                                    selectElement.required=true;
                                    parDivshow_get_select.appendChild (selectElement);
                                    
                                    getSelectElement=document.getElementById('buy_deal_type')
                                    getSelectElement.innerHTML=""
                                    var option = new Option ("Select Type","");
                                    getSelectElement.options[getSelectElement.options.length] = option;
                                    for (var i=0;i < buy_select_arr.length;i++)
                                    {
                                        var option = new Option (buy_select_arr[i].give_units,buy_select_arr[i].give_units);
                                        getSelectElement.options[getSelectElement.options.length] = option;
                                    }
                                    document.getElementById('buy_deal_type').value=buy_deal_type
                                }
                                else{
                                    parDivshow_get_select.innerHTML=""
                                }

                            }
                            else{
                                    var same_word=buy_input_arr[i].promotion_quote.toLowerCase() ;
                                   parDivFirstInput=document.getElementById('firstInput')
                                   parDivFirstInput.innerHTML=""
                                    if(getSkuWord(same_word)){
                                        var inputElem=document.createElement('input')
                                        inputElem.type="hidden"
                                        inputElem.classList.add("form-control");

                                        inputElem.setAttribute('data_type',buy_input_arr[i].input_type_for_buy);
                                        //inputElem.setAttribute('onkeyup','showBuyDealValuem()');
                                        inputElem.name="buy_deal_value";
                                        inputElem.setAttribute('required',true);
                                        inputElem.value=1;
                                        parDivFirstInput.appendChild (inputElem);
                                        
                                    }else{

                                    if(buy_input_arr[i].input_place_for_buy==1){
                                        var inputElem=document.createElement('input')
                                        inputElem.type="text"
                                        inputElem.classList.add("form-control");
										if(singleItemDiscount==1){
											inputElem.type="number"
											inputElem.setAttribute("min","2")
										}
                                        inputElem.setAttribute('id','buy_deal_value');
                                        inputElem.setAttribute('data_type',buy_input_arr[i].input_type_for_buy);
                                        inputElem.setAttribute('onkeyup','showBuyDealValue()');
                                        inputElem.name="buy_deal_value";
                                        inputElem.value=buy_deal_value;
                                        inputElem.setAttribute('required',true);
                                        parDivFirstInput.appendChild (inputElem);
                                        if(buy_input_arr[i].input_type_for_buy=="date"){
                                            inputElem.type="text"
                                           /* $('#buy_deal_value').datetimepicker({
                                                format: 'YYYY-MM-DD HH:mm',
                                                minDate: new Date(),
                                                useCurrent: false,
                                            });
                                            
                                            $("#buy_deal_value").on("dp.change", function (e) {
                                                document.getElementById('end_date').value=$("#buy_deal_value").val();
                                                $('#start_date').data("DateTimePicker").maxDate(e.date);
                                                putFinalDeal()
                                            });
											*/
											$('#buy_deal_value').bootstrapMaterialDatePicker({
												time: true,
												weekStart: 0, 
												format: 'YYYY-MM-DD HH:mm', 
												shortTime : true
											 }).on('change', function(e, date){
												document.getElementById('end_date').value=$("#buy_deal_value").val();
												//$('#start_date').data("DateTimePicker").maxDate(e.date);
												$('#start_date').bootstrapMaterialDatePicker('setMaxDate', date);
												putFinalDeal()
											});
                                            document.getElementById('end_date').readOnly=true
                                        }
                                        else{
                                            document.getElementById('end_date').readOnly=false
                                        }
                                    }
                                    else{
                                        parDivFirstInput.innerHTML=""
                                    }
                                   }
                                    
                                    parDivsecondInput=document.getElementById('secondInput')
                                    parDivsecondInput.innerHTML=""
                                    if(buy_input_arr[i].input_place_for_get==1){
                                        var inputElem=document.createElement('input')
                                        inputElem.type=buy_input_arr[i].input_type_for_get
                                        inputElem.classList.add("form-control");
                                        inputElem.setAttribute('id','get_deal_value');
                                        inputElem.setAttribute('onkeyup','showGetDealValue()');
                                        inputElem.name="get_deal_value";
                                        inputElem.value=get_deal_value;
                                        inputElem.setAttribute('required',true);
                                        /*if(buy_input_arr[i].input_type_for_buy=="date"){
                                            inputElem.setAttribute('style','display:none')
                                        }
                                        else{
                                            inputElem.setAttribute('style','display:')
                                        }*/
                                        parDivsecondInput.appendChild (inputElem);
                                        document.getElementById('get_deal_value').value='<?php echo $get_deal_value ?>'
                                    }
                                    else{
                                        parDivsecondInput.innerHTML=""
                                    }
                                    
                                    parDivshow_buy_select=document.getElementById('show_buy_select')
                                    parDivshow_buy_select.innerHTML=""
                                    if(buy_input_arr[i].show_buy_select==1){
                                        var selectElement = document.createElement ("select");
                                        selectElement.classList.add("form-control");
                                        selectElement.setAttribute('id','buy_deal_type');
                                        selectElement.setAttribute('onchange','showBuyDealType()');
                                        selectElement.name="buy_deal_type";
                                        selectElement.required=true;
                                        parDivshow_buy_select.appendChild (selectElement);
                                        
                                        var buy_select_arr=select_data[1]
                                        buySelectElement=document.getElementById('buy_deal_type')
                                        buySelectElement.innerHTML=""
                                        var option = new Option ("Select Type","");
                                        buySelectElement.options[buySelectElement.options.length] = option;
                                        for (var i=0;i < buy_select_arr.length;i++)
                                        {
                                            var option = new Option (buy_select_arr[i].buy_units,buy_select_arr[i].buy_units);
                                            buySelectElement.options[buySelectElement.options.length] = option;
                                        }
                                        document.getElementById('buy_deal_type').value=buy_deal_type
                                    }
                                    else{
                                        parDivshow_buy_select.innerHTML=""
                                    }
                                    
                                    parDivshow_get_select=document.getElementById('show_get_select')
                                    parDivshow_get_select.innerHTML=""
                                    if(show_get_select==1){
                                        
                                        
                                        if(reverse_select_input==0){
                                            var selectElement = document.createElement ("select");
                                            selectElement.classList.add("form-control");
                                            selectElement.setAttribute('id','get_deal_type');
                                            selectElement.setAttribute('onchange','showGetDealType()');
                                            selectElement.name="get_deal_type";
                                            selectElement.required=true;
                                            parDivshow_get_select.appendChild (selectElement);
                                            
                                            getSelectElement=document.getElementById('get_deal_type')
                                            getSelectElement.innerHTML=""
                                            var option = new Option ("Select Type","");
                                            getSelectElement.options[getSelectElement.options.length] = option;
                                            for (var i=0;i < buy_select_arr.length;i++)
                                            {
                                                var option = new Option (buy_select_arr[i].give_units,buy_select_arr[i].give_units);
                                                getSelectElement.options[getSelectElement.options.length] = option;
                                            }
                                            document.getElementById('get_deal_type').value=get_deal_type
                                        }
                                        if(reverse_select_input==1){
                                            var selectElement = document.createElement ("select");
                                            selectElement.classList.add("form-control");
                                            selectElement.setAttribute('id','get_deal_type');
                                            selectElement.setAttribute('onchange','showGetDealType()');
                                            selectElement.name="get_deal_type";
                                            selectElement.required=true;
                                            parDivshow_get_select.appendChild (selectElement);
                                            var $select = $("#get_deal_type");
                                            for (i=1;i<=99;i++){
                                                $select.append($('<option></option>').val(i+'%').html(i+'%'))
                                            }
                                            document.getElementById('get_deal_type').value=get_deal_type
                                        }
                                        
                                        
                                        
                                        
                                        
                                        
                                    }
                                    else{
                                        parDivshow_get_select.innerHTML=""
                                    }
                                }       
                            }
							
							showBuyDealType()
							showGetDealType()
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_promotion_quote_variables", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("quote_selector_val="+obj);
		        }

		}
		
		

			var buydealval,buydealtype,getdealval,getdealtype
			
			function cleardata(){
			if(document.getElementById('firstWord')!=null){
				document.getElementById('firstWord').innerHTML=""
			}
			
			if(document.getElementById('firstWord')!=null){
				document.getElementById('firstInput').innerHTML=""
			}
			
			if(document.getElementById('firstWord')!=null){
				document.getElementById('show_buy_select').innerHTML=""
			}
			
			if(document.getElementById('firstWord')!=null){
				document.getElementById('middleWord').innerHTML=""
			}
			
			if(document.getElementById('firstWord')!=null){
				document.getElementById('secondInput').innerHTML=""
			}
			
			if(document.getElementById('firstWord')!=null){
				document.getElementById('show_get_select').innerHTML=""
			}
			
			if(document.getElementById('firstWord')!=null){
				document.getElementById('endWord').innerHTML=""
			}
			
			if(document.getElementById('firstWord')!=null){
				document.getElementById('final_offer_name').innerHTML=""
			}
			
			if(document.getElementById('firstWord')!=null){
				document.getElementById('final_offer_value').value=""
			}
			document.getElementById('start_date').value=""
			document.getElementById('end_date').value=""
			buydealval="";buydealtype="";getdealval="";getdealtype="";
		}
			
				function showBuyDealValue(){
					if(document.getElementById('buy_deal_value')!=null){
						if(document.getElementById('buy_deal_value').value!=""){
							if(document.getElementById('buy_deal_value').getAttribute('data_type')=="date"){
								var m = moment(document.getElementById('buy_deal_value').value);
								buydealval=m.format('ddd, D MMM YYYY H:mm');
							}
							else{
								buydealval=document.getElementById('buy_deal_value').value;
							}
							putFinalDeal()
							
						}
					}
				}
				
				var getWords= "\\b"+"shipping"+"\\b";
                
                function getShippingResult(shipping_word){
                    if(shipping_word.match(getWords)){
                        return true
                    }
                    else{
                        return false
                    }
                }
				
				var discWords= "\\b"+"x% off"+"\\b";
                function getdiscountResult(discount_word){
                    if(discount_word.match(discWords)){
                        return true
                    }
                    else{
                        return false
                    }
                }
				var singleDiscount="\\b"+"straight x% off"+"\\b";
                function getDiscountForSingle(discount_word){
                    if(discount_word.match(singleDiscount)){
                        return true
                    }
                    else{
                        return false
                    }
                }
				
				
				
                var discountWords= "\\b"+"discount"+"\\b";
                function getdiscountWords(discount_words){
                    if(discount_words.match(discountWords)){
                        return true
                    }
                    else{
                        return false
                    }
                }
                
                /*var percentages="%";
                function hasPercentage(percent_val){
                    if(percent_val.match(percentages)){
                        return true
                    }
                    else{
                        return false
                    }
                }*/
				
				function showBuyDealType(){
				    var applied_at_invoice="<?php echo $applied_at_invoice?>";
                    var parent_div_c=document.getElementById('apply_promotion_at_check_out')

                    parent_div_c.innerHTML=""
                    if(document.getElementById('buy_deal_type')!=null){
                        if(document.getElementById('buy_deal_type').value!=""){
                            if(document.getElementById('buy_deal_type').value=="<?php echo curr_code; ?>"){

                                var labelElemp=document.createElement('label')
                                labelElemp.classList.add('control-label','col-sm-2');
                                labelElemp.innerHTML="Apply At:"
                                labelElemp.setAttribute('for','apply check')
                                parent_div_c.appendChild(labelElemp);
                                
                                var divElemp=document.createElement('div')
                                divElemp.classList.add('col-sm-10');
                                divElemp.setAttribute('id','apply_at_check')
                                parent_div_c.appendChild(divElemp);
                                
                                var parent_div=document.getElementById('apply_at_check')
                                
                                buydealtype=document.getElementById('buy_deal_type').value;
                                var labelElement=document.createElement ("label")
                                labelElement.classList.add('checkbox-inline');
                                labelElement.setAttribute('style','padding-left: 0px;')
                                labelElement.setAttribute('id',"checkbox_inline_at_promo1");
                                parent_div.appendChild (labelElement);
                                var checkElem=document.createElement("input")
                                    checkElem.type = "radio";
                                    checkElem.name="promotion_at_check_out"
                                    checkElem.value="1";
                                    if(applied_at_invoice==1){
                                        checkElem.setAttribute("checked",true)
                                    }

                                    checkElem.setAttribute("onchange","changePromoApplyTo(this)");
                                    
                                document.getElementById('checkbox_inline_at_promo1').appendChild(checkElem);
                                document.getElementById('checkbox_inline_at_promo1').appendChild(document.createTextNode(" Applied To Invoices"));

                                
                                /*var labelElement=document.createElement ("label")
                                labelElement.classList.add('checkbox-inline');
                                labelElement.setAttribute('style','padding-left: 0px;')
                                labelElement.setAttribute('id',"checkbox_inline_at_promo2");
                                parent_div.appendChild (labelElement);
                                
                                var checkElem=document.createElement("input")
                                    checkElem.type = "radio";
                                    checkElem.name="promotion_at_check_out";
                                    checkElem.value="0";
                                    if(applied_at_invoice==0){
                                        checkElem.setAttribute("checked",true)
                                    }
                                    checkElem.setAttribute("onchange","changePromoApplyTo(this)");
                                    
                                   document.getElementById('checkbox_inline_at_promo2').appendChild(checkElem);
                                   document.getElementById('checkbox_inline_at_promo2').appendChild(document.createTextNode(" Applied To SKUs "));
                                    
*/
                                    var shippingText=document.getElementById("firstWord").innerHTML;
                                    var shipping_word=shippingText.toLowerCase() ;
                                    if(getShippingResult(shipping_word)){
                                        document.getElementById('apply_promotion_at_check_out').style.display='none'
                                    }
                                    else{
                                        document.getElementById('apply_promotion_at_check_out').style.display=''
                                    }
                                

                                    
                            }
                            else{
                                buydealtype=document.getElementById('buy_deal_type').value;
                            }
                            
                            //getdealtype=buydealtype
                            putFinalDeal()
                        }
                    }
                    if(document.getElementById('get_deal_type')!=null){
                        //document.getElementById('get_deal_type').value=buydealtype
                        
                    }
                    
                }
                
                function changePromoApplyTo(obj){
                    putFinalDeal()
                }
                
				function showGetDealValue(){
					if(document.getElementById('get_deal_value')!=null){
						if(document.getElementById('get_deal_value').value){
							getdealval=document.getElementById('get_deal_value').value;
						    putFinalDeal()
						}
					}
					
				}
				/*function showGetDealType(){
                    if(document.getElementById('get_deal_type')!=null){
                        if(document.getElementById('get_deal_type').value!=""){
                            getdealtype=document.getElementById('get_deal_type').value;
                            if(document.getElementById('buy_deal_value')!=null){
                                if(document.getElementById('buy_deal_value').getAttribute('data_type')=="date"){
                                var get_deal_type=document.getElementById('get_deal_type').value
                                if(document.getElementById('get_deal_type').value=="SKU Free !"){
                                document.getElementById('get_deal_value').setAttribute("style","")
                                }
                                else if(get_deal_type==curr_code || get_deal_type=="Qty" || get_deal_type=="Units") {
                                document.getElementById('get_deal_value').setAttribute("style","")
                                }
                                else{
                                    document.getElementById('get_deal_value').setAttribute("style","display:none")
                                    document.getElementById('get_deal_value').value=""
                                    getdealval=""
                                }
                              }
                            }
                            
                            putFinalDeal()  
                        }
                    }
                    
                }*/
               function showGetDealType(){
                    if(document.getElementById('get_deal_type')!=null){
                        if(document.getElementById('get_deal_type').value!=""){
                            getdealtype=document.getElementById('get_deal_type').value;
                            if(document.getElementById('buy_deal_value')!=null){
                                if(document.getElementById('buy_deal_value').getAttribute('data_type')=="date"){
                                var get_deal_type=document.getElementById('get_deal_type').value
                                if(document.getElementById('get_deal_type').value=="cash back"){
                                //document.getElementById('get_deal_value').setAttribute("style","")
                                    var secondInput=document.getElementById('secondInput')
                                    secondInput.innerHTML=""
                                    var selectElement = document.createElement ("select");
                                    selectElement.classList.add("form-control");
                                    selectElement.setAttribute('id','get_deal_value');
                                    selectElement.setAttribute('onchange','showGetDealValue()');
                                    selectElement.name="get_deal_value";
                                    selectElement.required=true;
                                    secondInput.appendChild (selectElement);
                                    var $select = $("#get_deal_value");
                                    for (i=1;i<=99;i++){
                                        $select.append($('<option></option>').val(i+'%').html(i+'%'))
                                    }
                                    for(var i = 0; i < $select.length; i++) {
                                      $select[i].selectedIndex =0;
                                    } 
                                    
                                }
                                else
                                if(get_deal_type=="<?php echo curr_code; ?>" || get_deal_type=="Qty" || get_deal_type=="Units"||get_deal_type== "SKU Free !" ) {
                                    
                                    document.getElementById('secondInput').innerHTML=""
                                parDivsecondInput=document.getElementById('secondInput')
                                    parDivsecondInput.innerHTML=""
                                        var inputElem=document.createElement('input')
                                        inputElem.type="text"
                                        inputElem.classList.add("form-control");
                                        inputElem.setAttribute('id','get_deal_value');
                                        inputElem.setAttribute('onkeyup','showGetDealValue()');
                                        inputElem.name="get_deal_value";
                                        inputElem.setAttribute('required',true);
                                        parDivsecondInput.appendChild (inputElem);
                                        //document.getElementById('get_deal_value').value='<?php echo $get_deal_value ?>'
                                //document.getElementById('get_deal_value').setAttribute("style","")
                                }
                                else{
                                    document.getElementById('get_deal_value').setAttribute("style","display:none")
                                    document.getElementById('get_deal_value').value=""
                                    getdealval=""
                                }
                              }
                            }
                            
                            putFinalDeal()  
                        }
                    }
                    
                }
				function putFinalDeal(){
                                                buydealval='';
						if(document.getElementById('buy_deal_value')!=null){
                                                    if(document.getElementById('buy_deal_value').getAttribute('data_type')=="date"){
                                                        var m = moment(document.getElementById('buy_deal_value').value);
                                                        buydealval=m.format('ddd, D MMM YYYY H:mm') +" hrs";
                                                    }
                                                    else{
                                                        buydealval=document.getElementById('buy_deal_value').value;
                                                    }
                                                }
                                                console.log("buydealval"+buydealval+"||");
                                                
                                                var htmls=""
						
						
						if(getMatchedResult(start_word,getWord)){
							if(document.getElementById('firstWord')!=null){
                                                            if(document.getElementById('buy_deal_value').getAttribute('data_type')=="date"){
                                                                var m = moment(document.getElementById('buy_deal_value').value);
                                                                buydealval=m.format('ddd, D MMM YYYY H:mm') +" hrs";
                                                            }
                                                            else{
                                                                buydealval=document.getElementById('buy_deal_value').value;
                                                            }
                                                        }
                                                        htmls+=document.getElementById('firstWord').innerHTML+" ("
                                                    
							if(document.getElementById('get_deal_value')!=null){
								if(getdealval!=""){
									htmls+=document.getElementById('get_deal_value').value+" "
								}
							}
							if(document.getElementById('get_deal_type')!=null){
								htmls+=document.getElementById('get_deal_type').value+") "
							}
							if(document.getElementById('middleWord')!=null){
								htmls+=document.getElementById('middleWord').innerHTML+" "
							}
							
							if(document.getElementById('buy_deal_value')!=null){
								htmls+=buydealval+" "
							}
							if(document.getElementById('buy_deal_type')!=null){
								htmls+=document.getElementById('buy_deal_type').value+" "
							}
							if(document.getElementById('endWord')!=null){
								htmls+=document.getElementById('endWord').innerHTML+" "
							}
						}
						else if(getMatchedResult(middle_word,middleGetWord)){
							
								if(document.getElementById('firstWord')!=null){
								htmls+=document.getElementById('firstWord').innerHTML+" "
								}
								if(document.getElementById('buy_deal_value')!=null){
									htmls+=buydealval+" "
								}
								if(document.getElementById('buy_deal_type')!=null){
									htmls+=document.getElementById('buy_deal_type').value+" "
								}
								if(document.getElementById('middleWord')!=null){
									htmls+=document.getElementById('middleWord').innerHTML+" ("
								}
								if(document.getElementById('get_deal_value')!=null){
									if(getdealval!=""){
										htmls+=document.getElementById('get_deal_value').value+" "
									}
								}
								if(document.getElementById('get_deal_type')!=null){
									htmls+=document.getElementById('get_deal_type').value+") "
								}
								if(document.getElementById('endWord')!=null){
									htmls+=document.getElementById('endWord').innerHTML+" "
								}
						}
						else{
							if(document.getElementById('firstWord')!=null){
							htmls+=document.getElementById('firstWord').innerHTML+" "
							}
							if(document.getElementById('buy_deal_value')!=null){
								htmls+=buydealval+" "
							}
							if(document.getElementById('buy_deal_type')!=null){
								htmls+=document.getElementById('buy_deal_type').value+" "
							}
							if(document.getElementById('middleWord')!=null){
								htmls+=document.getElementById('middleWord').innerHTML+" "
							}
							if(document.getElementById('get_deal_value')!=null){
								if(getdealval!=""){
									htmls+=document.getElementById('get_deal_value').value+" "
								}
							}
							if(document.getElementById('get_deal_type')!=null){
								htmls+=document.getElementById('get_deal_type').value+" "
							}
							if(document.getElementById('endWord')!=null){
								htmls+=document.getElementById('endWord').innerHTML+" "
							}
						}
						
						var x = document.getElementsByName("promotion_at_check_out");
                        var i;
                        for (i = 0; i < x.length; i++) {
                            if (x[i].type == "radio") {
                                if(x[i].checked){
                                    //alert(x[i].value)
                                    htmls+='('+x[i].nextSibling.data+')'
                                    
                                }
                            }
                        }
						
    
						
						document.getElementById('final_offer_name').innerHTML=htmls +document.getElementById('SKU_PARAMS').value
						document.getElementById('final_offer_value').value=htmls+document.getElementById('SKU_PARAMS').value
						
				}

		
	
	
	function save_deal_quote_edit(obj){
		var save_deal_quote=1;
		var deactivate_flag=0;
		var buy_deal_value=""; var get_deal_value="";var buy_deal_type=""; var get_deal_type="";
		var promotion_at_check_out="";
		if(document.getElementById('buy_deal_value')!=null){
		if(document.getElementById('buy_deal_value').hasAttribute('min')){
			if(document.getElementById('buy_deal_value').value<(document.getElementById('buy_deal_value').getAttribute('min'))){
				alert("Buying value should be greater than 1")
				return false;
			}
		}
		}
		
		
		if(document.getElementsByName("promotion_at_check_out")!=null)
		var x = document.getElementsByName("promotion_at_check_out");
        var i;
        for (i = 0; i < x.length; i++) {
            if (x[i].type == "radio") {
                if(x[i].checked){
                    promotion_at_check_out=x[i].value
                    //htmls+='('+x[i].nextSibling.data+')'
                }
            }
        }

		
		<?php if($qualify_for_tier==1){ ?>
		    promotion_at_check_out=1;
		    if(document.getElementsByName('buy_deal_value[]')!=null){
		        var buy_deal_value_elems=document.getElementsByName('buy_deal_value[]');
		        for (var i=0;i < buy_deal_value_elems.length;i++){
		            buy_deal_value+=buy_deal_value_elems[i].value+',';
		        }
                buy_deal_value = buy_deal_value.replace(/,\s*$/, "");
            }
            if(document.getElementsByName('get_deal_value[]')!=null){
                var get_deal_value_elems=document.getElementsByName('get_deal_value[]');
                for (var i=0;i < get_deal_value_elems.length;i++){
                    get_deal_value+=get_deal_value_elems[i].value+',';
                }
                get_deal_value = get_deal_value.replace(/,\s*$/, "");
            }
            
            if(document.getElementsByName('buy_deal_type[]')!=null){
                var buy_deal_type_elems=document.getElementsByName('buy_deal_type[]');
                for (var i=0;i < buy_deal_type_elems.length;i++){
                    buy_deal_type+=buy_deal_type_elems[i].value+',';
                }
                buy_deal_type = buy_deal_type.replace(/,\s*$/, "");
            }
            if(document.getElementsByName('get_deal_type[]')!=null){
                var get_deal_type_elems=document.getElementsByName('get_deal_type[]');
                for (var i=0;i < get_deal_type_elems.length;i++){
                    get_deal_type+=get_deal_type_elems[i].value+',';
                }
                get_deal_type = get_deal_type.replace(/,\s*$/, "");
            }
		<?php }else{?>
		    if(document.getElementsByName('buy_deal_value')[0]!=null){
             buy_deal_value=document.getElementsByName('buy_deal_value')[0].value
            }
            if(document.getElementById('get_deal_value')!=null){
                 get_deal_value=document.getElementById('get_deal_value').value
            }
            
            if(document.getElementById('buy_deal_type')!=null){
                 buy_deal_type=document.getElementById('buy_deal_type').value
            }
            if(document.getElementById('get_deal_type')!=null){
                 get_deal_type=document.getElementById('get_deal_type').value
            }
		<?php }?>
		
		
		var new_deal_quote=document.getElementById('final_offer_value').value
        var promo_id=document.getElementById('main_promo_id').value
        
        var endDate=document.getElementById('end_date').value
        
        if(new_deal_quote!=""){
        if('<?php echo $promo_quote ?>'!=new_deal_quote){
			<?php if($promo_active){ ?>
				
					var answer=confirm("Warning! You have edited an active promotion. Continuing will first deactivate this promotion and save it in `Reverted Drafts`. You can further action it there.")
					if(answer){
						save_deal_quote=1;
						deactivate_flag=1;
					}
					else{
						save_deal_quote=0;
						return false;
					}
				
			<?php } ?>
        	
	
        ///////
        var start_dates=new Date(document.getElementById('start_date').value)
       
		var end_dates=document.getElementById('end_date').value

        end_dates=new Date(end_dates)
        var dates_submit=1
        
        if(end_dates<start_dates){
			var answer = confirm ("Start date will be reset to Null since your End date chosed is before the start date chosen");
			if (answer){
				dates_submit=1
				document.getElementById('start_date').value=""
	        	document.getElementById('disp_deal_start_date_edit_container').innerHTML=""
				}
			else{dates_submit=0;return false}
			        	
		}      	
		var end_date=document.getElementById('end_date').value	
		var start_date=document.getElementById('start_date').value
		///////
        var approval_flag=0
        <?php if($approval_sent==1 && $promo_active==0){?> 
    

            var answer = confirm ("WARNING! This promotion is already sent for approvals. Changing Deal Quote will cancel the approval request.");
            if (answer){
                goAhead=1
                approval_flag=1
                }
            else{goAhead=0;return false}

        <?php } ?>
        if(dates_submit==1){
        	var xhr = false;
			if (window.XMLHttpRequest) {
			    xhr = new XMLHttpRequest();
			}
			else {
			    xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
		
			 var xhr = new XMLHttpRequest();
		
					if (xhr) {
						obj.classList.remove("btn-success");
						obj.classList.add("btn-warning");
						obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
			            xhr.onreadystatechange = function () {
			                if (xhr.readyState == 4 && xhr.status == 200) {
			                	if(xhr.responseText==1){
									document.getElementById('final_offer_name').value=new_deal_quote
									document.getElementById('disp_deal_quote_edit_container').innerHTML=new_deal_quote
									document.getElementById('promotion_summary_promo_quote').innerHTML=new_deal_quote
									obj.classList.remove("btn-warning");
									obj.classList.add("btn-success");
									obj.innerHTML='<i class="fa fa-floppy-o" aria-hidden="true"></i>'
									document.getElementById('display_realted_input_form').style.display="none"
									//document.getElementById('button_deal_quote_edit_container').style.display=""
									document.getElementById('button_deal_quote_edit_container').style.visibility="visible";
									
									if(document.getElementById('approve_promo_button1')==null){
										var top_activate_button_div=document.getElementById('top_right_button_action')
										top_activate_button_div.innerHTML=""
									var top_activate_button=document.createElement('button');
										top_activate_button.innerHTML="Send For Approval"
										top_activate_button.classList.add('btn','btn-success','btn-sm')
										top_activate_button.setAttribute('onclick',"send_approval_for_promotion(this)")
										top_activate_button.setAttribute('promo_id',promo_id)
										top_activate_button.setAttribute('id','approve_promo_button1')
										top_activate_button_div.appendChild (top_activate_button);
									}
									
									if(document.getElementById('approve_promo_button2')==null){
										var bottom_activate_button_div=document.getElementById('bottom_activate_button_div')
										bottom_activate_button_div.innerHTML=""
									var bottom_activate_button=document.createElement('button');
										bottom_activate_button.innerHTML="Send For Approval"
										bottom_activate_button.classList.add('btn','btn-success','btn-lg')
										bottom_activate_button.setAttribute('onclick',"send_approval_for_promotion(this)")
										bottom_activate_button.setAttribute('promo_id',promo_id)
										bottom_activate_button.setAttribute('id','approve_promo_button2')
										bottom_activate_button_div.appendChild (bottom_activate_button);
									}
									if(approval_flag==1){
                                        if(count_items_for_purchasing==1){
                                        alert("The approval request is cancelled")
                                        window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
                                        }
                                    }
									
									
									if(deactivate_flag==1){
										alert("This promotion is deactivated")
										window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
									}
									else{
										location.reload()
									}
			                	}
			                	else{
			                		obj.classList.remove("btn-warning");
									obj.classList.add("btn-success");
									obj.innerHTML='<i class="fa fa-floppy-o" aria-hidden="true"></i>'
									document.getElementById('display_realted_input_form').style.display="none"
									//document.getElementById('button_deal_quote_edit_container').style.display="";
									document.getElementById('button_deal_quote_edit_container').style.visibility="visible";
			                	}
			                	if(approval_flag==1){
                                        if(count_items_for_purchasing==1){
                                        alert("The approval request is cancelled")
                                        window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
                                        }
                                    }
                                    
                                    
                                    if(deactivate_flag==1){
                                        alert("This promotion is deactivated")
                                        window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
                                    }
                                    else{
                                        location.reload()
                                    }
			                }
			            }
			            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/update_promotion_deal_quote", true);
			            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			            xhr.send("promo_id="+promo_id+"&new_deal_quote="+new_deal_quote+"&buy_deal_value="+buy_deal_value+"&get_deal_value="+get_deal_value+"&buy_deal_type="+buy_deal_type+"&get_deal_type="+get_deal_type+"&endDate="+endDate+"&start_date="+start_date+"&deactivate_flag="+deactivate_flag+"&promotion_at_check_out="+promotion_at_check_out+"&approval_flag="+approval_flag);
			        }
		    }
		    
		   }else{
		   		obj.classList.remove("btn-warning");
				obj.classList.add("btn-success");
				obj.innerHTML='<i class="fa fa-floppy-o" aria-hidden="true"></i>'
				document.getElementById('display_realted_input_form').style.display="none"
				//document.getElementById('button_deal_quote_edit_container').style.display="";
				document.getElementById('button_deal_quote_edit_container').style.visibility="visible";
		   } 
		    
		    
        }
        else{
        	alert('Offer Quote cannot be null')
        }
	}
	function close_deal_quote_edit(){
		document.getElementById('display_realted_input_form').style.display="none"
		//document.getElementById('button_deal_quote_edit_container').style.display="";
		document.getElementById('button_deal_quote_edit_container').style.visibility="visible";
		document.getElementById('add_button_tiers').innerHTML="";
		<?php if($qualify_for_tier==1) { ?>
              document.getElementById('disp_deal_quote_edit_container').innerHTML=document.getElementById('final_offer_name').innerHTML
         <?php  }?>
	}
	function open_end_date_edit(){
		
		document.getElementById('deal_end_date_edit_container').style.display=""
		//document.getElementById('button_deal_end_date_edit_container').style.display="none";
		document.getElementById('button_deal_end_date_edit_container').style.visibility="hidden";
	}
	function close_end_date_edit(){
		document.getElementById('deal_end_date_edit_container').style.display="none"
		document.getElementById('button_deal_end_date_edit_container').style.visibility="visible";
	}
	function save_end_date_edit(obj){
		var end_date=document.getElementById('end_date').value
        var promo_id=document.getElementById('main_promo_id').value
        var deactivate_flag=0;
        
        if(end_date!='<?php echo $promo_end_datev?>'){ 
        
        <?php if($promo_active){ ?>
				
			var answer=confirm("Warning! You have edited an active promotion. Continuing will first deactivate this promotion and save it in `Reverted Drafts`. You can further action it there.")
			if(answer){

				deactivate_flag=1;
			}
			else{

				return false;
			}
				
		<?php } ?>
		var approval_flag=0
        <?php if($approval_sent==1 && $promo_active==0){?> 
    
       
            var answer = confirm ("WARNING! This promotion is already sent for approvals. Changing End Date will cancel the approval request.");
            if (answer){
                goAhead=1
                approval_flag=1
                }
            else{goAhead=0;return false}
    
        <?php } ?>
        
        
        
        if(end_date!=""){
        	var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
					obj.classList.remove("btn-success");
					obj.classList.add("btn-warning");
					obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
		                	if(document.getElementById('approve_promo_button1')==null){
								var top_activate_button_div=document.getElementById('top_right_button_action')
								top_activate_button_div.innerHTML=""
							var top_activate_button=document.createElement('button');
								top_activate_button.innerHTML="Send For Approval"
								top_activate_button.classList.add('btn','btn-success','btn-sm')
								top_activate_button.setAttribute('onclick',"send_approval_for_promotion(this)")
								top_activate_button.setAttribute('promo_id',promo_id)
								top_activate_button.setAttribute('id','approve_promo_button1')
								top_activate_button_div.appendChild (top_activate_button);
							}
							
							if(document.getElementById('approve_promo_button2')==null){
								var bottom_activate_button_div=document.getElementById('bottom_activate_button_div')
								bottom_activate_button_div.innerHTML=""
							var bottom_activate_button=document.createElement('button');
								bottom_activate_button.innerHTML="Send For Approval"
								bottom_activate_button.classList.add('btn','btn-success','btn-lg')
								bottom_activate_button.setAttribute('onclick',"send_approval_for_promotion(this)")
								bottom_activate_button.setAttribute('promo_id',promo_id)
								bottom_activate_button.setAttribute('id','approve_promo_button2')
								bottom_activate_button_div.appendChild (bottom_activate_button);
							}
		                	
		                	
		                	if(xhr.responseText==1){
							var m = moment(end_date);
							document.getElementById('disp_deal_end_date_edit_container').innerHTML=m.format('ddd, D MMM YYYY H:mm')
							document.getElementById('promotion_summary_end_date').innerHTML=m.format('ddd, D MMM YYYY H:mm')
							obj.classList.remove("btn-warning");
							obj.classList.add("btn-success");
							obj.innerHTML='<i class="fa fa-floppy-o" aria-hidden="true"></i>'
							document.getElementById('deal_end_date_edit_container').style.display="none"
							//document.getElementById('button_deal_end_date_edit_container').style.display="";
							document.getElementById('button_deal_end_date_edit_container').style.visibility="visible";
		                	}
		                	else{
		                		var m = moment(end_date);
							document.getElementById('disp_deal_end_date_edit_container').innerHTML=m.format('ddd, D MMM YYYY H:mm')
							document.getElementById('promotion_summary_end_date').innerHTML=m.format('ddd, D MMM YYYY H:mm')
							obj.classList.remove("btn-warning");
							obj.classList.add("btn-success");
							obj.innerHTML='<i class="fa fa-floppy-o" aria-hidden="true"></i>'
							document.getElementById('deal_end_date_edit_container').style.display="none"
							//document.getElementById('button_deal_end_date_edit_container').style.display="";
							document.getElementById('button_deal_end_date_edit_container').style.visibility="visible";
		                	}
		                	check_activations()
		                	
		                	if(approval_flag==1){
                                alert("The approval request is cancelled")
                                window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
                            }
                            
                            
                            if(deactivate_flag==1){
                                alert("This promotion is deactivated")
                                window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
                            }
		                		
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/update_promotion_end_date_date", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("promo_id="+promo_id+"&end_date="+end_date+"&deactivate_flag="+deactivate_flag+"&approval_flag="+approval_flag);
		        }
	        }
	        else{
	        	alert('Offer Start Date cannot be null')
	        }
       }
       else{
       		obj.classList.remove("btn-warning");
			obj.classList.add("btn-success");
			obj.innerHTML='<i class="fa fa-floppy-o" aria-hidden="true"></i>'
			document.getElementById('deal_end_date_edit_container').style.display="none"
			//document.getElementById('button_deal_end_date_edit_container').style.display="";
			document.getElementById('button_deal_end_date_edit_container').style.visibility="visible";
       }
	}
	function open_start_date_edit(){
		
		document.getElementById('deal_start_date_edit_container').style.display=""
		document.getElementById('button_deal_start_date_edit_container').style.visibility="hidden"
	}
	function close_start_date_edit(){
		document.getElementById('deal_start_date_edit_container').style.display="none"
		document.getElementById('button_deal_start_date_edit_container').style.visibility="visible"
	}
	function save_start_date_edit(obj){
		var start_date=document.getElementById('start_date').value
        var promo_id=document.getElementById('main_promo_id').value
        
        if(start_date!='<?php echo $promo_start_datev ?>'){
        	var deactivate_flag=0;
        if(start_date!=""){
        	
       <?php if($promo_active){ ?>
				
			var answer=confirm("Warning! You have edited an active promotion. Continuing will first deactivate this promotion and save it in `Reverted Drafts`. You can further action it there.")
			if(answer){

				deactivate_flag=1;
			}
			else{

				return false;
			}
				
		<?php } ?>
		
		var approval_flag=0
        <?php if($approval_sent==1 && $promo_active==0){?> 
    
            var answer = confirm ("WARNING! This promotion is already sent for approvals. Changing Start Date will cancel the approval request.");
            if (answer){
                goAhead=1
                approval_flag=1
                }
            else{goAhead=0;return false}
        <?php } ?>
        	
    	var start_dates=new Date(document.getElementById('start_date').value)
       
		var end_dates=document.getElementById('end_date').value
		
        end_dates=new Date(end_dates)
        var dates_submit=1
        
        if(end_dates<start_dates){
			var answer = confirm ("End Date will be reset to Null since your start date chosen is after the end date shown");
			if (answer){
				dates_submit=1
				document.getElementById('end_date').value=""
	        	document.getElementById('disp_deal_end_date_edit_container').innerHTML=""
	        	document.getElementById('end_date').value=""
				}
			else{dates_submit=0;return false}
			        	
		}      	
		var end_date=document.getElementById('end_date').value	
        if(dates_submit==1){
        	var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
					obj.classList.remove("btn-success");
					obj.classList.add("btn-warning");
					obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
		                	if(document.getElementById('approve_promo_button1')==null){
								var top_activate_button_div=document.getElementById('top_right_button_action')
								top_activate_button_div.innerHTML=""
							var top_activate_button=document.createElement('button');
								top_activate_button.innerHTML="Send For Approval"
								top_activate_button.classList.add('btn','btn-success','btn-sm')
								top_activate_button.setAttribute('onclick',"send_approval_for_promotion(this)")
								top_activate_button.setAttribute('promo_id',promo_id)
								top_activate_button.setAttribute('id','approve_promo_button1')
								top_activate_button_div.appendChild (top_activate_button);
							}
							
							if(document.getElementById('approve_promo_button2')==null){
								var bottom_activate_button_div=document.getElementById('bottom_activate_button_div')
								bottom_activate_button_div.innerHTML=""
							var bottom_activate_button=document.createElement('button');
								bottom_activate_button.innerHTML="Send For Approval"
								bottom_activate_button.classList.add('btn','btn-success','btn-lg')
								bottom_activate_button.setAttribute('onclick',"send_approval_for_promotion(this)")
								bottom_activate_button.setAttribute('promo_id',promo_id)
								bottom_activate_button.setAttribute('id','approve_promo_button2')
								bottom_activate_button_div.appendChild (bottom_activate_button);
							}
		                	
		                	if(xhr.responseText==1){
							var m = moment(start_date);
							
							document.getElementById('disp_deal_start_date_edit_container').innerHTML=m.format('ddd, D MMM YYYY H:mm')
							document.getElementById('promotion_summary_start_date').innerHTML=m.format('ddd, D MMM YYYY H:mm')
							obj.classList.remove("btn-warning");
							obj.classList.add("btn-success");
							obj.innerHTML='<i class="fa fa-floppy-o" aria-hidden="true"></i>'
							document.getElementById('deal_start_date_edit_container').style.display="none"
							//document.getElementById('button_deal_start_date_edit_container').style.display="";
							document.getElementById('button_deal_start_date_edit_container').style.visibility="visible";
		                	}
		                	else{
		                		var m = moment(start_date);
								document.getElementById('disp_deal_start_date_edit_container').innerHTML=m.format('ddd, D MMM YYYY H:mm')
							document.getElementById('promotion_summary_start_date').innerHTML=m.format('ddd, D MMM YYYY H:mm')
								obj.classList.remove("btn-warning");
								obj.classList.add("btn-success");
								obj.innerHTML='<i class="fa fa-floppy-o" aria-hidden="true"></i>'
								document.getElementById('deal_start_date_edit_container').style.display="none"
								//document.getElementById('button_deal_start_date_edit_container').style.display="";
								document.getElementById('button_deal_start_date_edit_container').style.visibility="visible";
		                	}
		                	check_activations()
		                	
		                	if(approval_flag==1){
                                alert("The approval request is cancelled")
                                window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
                            }
                            
                            
                            if(deactivate_flag==1){
                                alert("This promotion is deactivated")
                                window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
                            }
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/update_promotion_start_date", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("promo_id="+promo_id+"&start_date="+start_date+"&end_date="+end_date+"&deactivate_flag="+deactivate_flag+"&approval_flag="+approval_flag);
		        }
        	
        	}	
        
        }
        else{
        	alert('Offer Start Date cannot be null')
        }
     }
     else{
     	obj.classList.remove("btn-warning");
		obj.classList.add("btn-success");
		obj.innerHTML='<i class="fa fa-floppy-o" aria-hidden="true"></i>'
		document.getElementById('deal_start_date_edit_container').style.display="none"
		//document.getElementById('button_deal_start_date_edit_container').style.display=""
		document.getElementById('button_deal_start_date_edit_container').style.visibility="visible";
     }
        
        
	}
	
</script>
<div class="container-fluid">
<div class="row">
<div class="col-md-12">
	    
<?php //echo  "applied_at_invoice " . $applied_at_invoice ;?>

                                    <ul class="nav nav-pills nav-pills-info step_process" role="tablist">
 <?php if($applied_at_invoice!=1){ ?> 
                                        <li role="presentation"><a href="#product_selector_container" aria-controls="product_selector_container" role="tab" data-toggle="tab">Step 1. Add Products to Promotion <button class="btn btn-xs btn-info btn-round-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a></li>
<?php }?>



	
	<?php 
	
	if (!preg_match("/\b(Same SKUs)\b/i", $promo_quote, $match)) {
                           
                    
	
	if($get_deal_value!="" && ($buy_deal_type!='')&&($get_deal_type!=curr_code)&&($get_deal_type!='cash back') && $applied_at_invoice!=1 && $qualify_for_tier!=1){?>
	                                        <li role="presentation"
  <?php if($this->session->flashdata("notification")){
         if($this->session->flashdata("notification")=="Proceed To Step 2"){
            echo 'class="active"';
         }
    }?>
    
    
    
<?php if($this->session->flashdata("notification")){
         if($this->session->flashdata("notification")=="Update Giveaway Units"){
            echo 'class="active"';
         }
    } ?> 
	>
                                        <a href="#product_selector_container_freeItem" aria-controls="product_selector_container_freeItem" role="tab" data-toggle="tab">
   <?php
		

   if($get_deal_value!="" &&($get_deal_type!=curr_code)){
       echo 'Step 2.';
   }else{
       echo 'Step 1.';
   }?>                                      
                                            
                                             Add Giveaway Products<button class="btn btn-xs btn-info btn-round-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
											 </li>
                                        
    <?php  }
	}
	?>
	
    <?php if($get_deal_value!="" && ($buy_deal_type==curr_code)&&($get_deal_type!=curr_code)&&($applied_at_invoice!=1)){?>

                                        <li role="presentation"
    <?php if($this->session->flashdata("notification")){
         if($this->session->flashdata("notification")=="Proceed To Step 2"){
            echo 'class="active"';
         }
    } 
	?>
	<?php if($this->session->flashdata("notification")){
         if($this->session->flashdata("notification")=="Update Giveaway Units"){
            echo 'class="active"';
         }
    }
	?>  
	>
                                        <a href="#product_selector_container_freeItem" aria-controls="product_selector_container_freeItem" role="tab" data-toggle="tab">
   <?php if($get_deal_value!="" && ($buy_deal_type!=curr_code)&&($get_deal_type!=curr_code)&&($applied_at_invoice==1)){
       echo 'Step 1.';
   }else{
       echo 'Step 2.';
   }?>                                      
                                            
                                             Add Giveaway Products <button class="btn btn-xs btn-info btn-round-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
											 </li>
                                        
    <?php } ?>
	
	
	
	
    <?php if($get_deal_value!="" && ($buy_deal_type=='')&&($get_deal_type!=curr_code)&&($get_deal_type!='cash back')){?>

                                        
	<li role="presentation"
  <?php if($this->session->flashdata("notification")){
         if($this->session->flashdata("notification")=="Proceed To Step 2"){
            echo 'class="active"';
         }
    } 
	?> 
    
    
    
   <?php if($this->session->flashdata("notification")){
         if($this->session->flashdata("notification")=="Update Giveaway Units"){
            echo 'class="active"';
         }
    }
	?> 
	>
                                        <a href="#product_selector_container_freeItem" aria-controls="product_selector_container_freeItem" role="tab" data-toggle="tab">
   <?php if($get_deal_value!="" && ($buy_deal_type==curr_code)&&($get_deal_type!=curr_code)&&($applied_at_invoice==1)){
       echo 'Step 1.';
   }else{
       echo 'Step 2.';
   }?>                                      
                                            
                                             Add Giveaway Products <button class="btn btn-xs btn-info btn-round-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
											 </li>
                                        
    <?php } ?>
	
	
	   <?php if($get_deal_value!="" && ($buy_deal_type!='')&&($get_deal_type!=curr_code)&&($get_deal_type!='cash back') && $applied_at_invoice==1 && $qualify_for_tier!=1){?>

                                        
	<li role="presentation"
  <?php if($this->session->flashdata("notification")){
         if($this->session->flashdata("notification")=="Proceed To Step 2"){
            echo 'class="active"';
         }
    } 
	?> 
    
    
    
   <?php if($this->session->flashdata("notification")){
         if($this->session->flashdata("notification")=="Update Giveaway Units"){
            echo 'class="active"';
         }
    }
	?> 
	>
                                        <a href="#product_selector_container_freeItem" aria-controls="product_selector_container_freeItem" role="tab" data-toggle="tab">
   <?php if($get_deal_value!="" && ($buy_deal_type==curr_code )&&($get_deal_type!=curr_code)&&($applied_at_invoice==1)){
       echo 'Step 1.';
   }else{
       echo 'Step 2.';
   }?>                                      
                                            
                                             Add Giveaway Products <button class="btn btn-xs btn-info btn-round-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
											 </li>
                                        
    <?php } ?>
	
	
    <?php if($applied_at_invoice!=1){?>
                                        <li 
   <?php if(!($get_deal_type==curr_code || $get_deal_value==""||$qualify_for_different==1)&&($buy_deal_type=="")){
	  		 if($this->session->flashdata("notification")){
				 if($this->session->flashdata("notification")=="Proceed To Step 2"){
				 	echo 'class=""';
				 }
			 }
	}?>                                                                                    
                                         role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" onclick="check_activations()">Step <?php if(!($get_deal_type==curr_code || $get_deal_value==""||$qualify_for_different!=1)){ echo "3";}else if($get_deal_value!="" && ($buy_deal_type==curr_code)&&($get_deal_type!=curr_code)){ echo "3";}else if($buy_deal_type==''&& $get_deal_type!=curr_code && $get_deal_value!=''&& $get_deal_type!='cash back'){echo '3';}else
                                         if($get_deal_value!="" && ($buy_deal_type!='')&&($get_deal_type!=curr_code )&&($get_deal_type!='cash back') && $applied_at_invoice!=1 &&(!preg_match("/\b(Same SKUs)\b/i", $promo_quote, $match))){echo '3';}else{echo '2';}?>. Preview Promotion <button class="btn btn-xs btn-info btn-round-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a></li>
          <?php }else{

              if($get_deal_value!="" && ($buy_deal_type==curr_code)&&($get_deal_type!=curr_code)){ ?>
                   <li   role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" onclick="check_activations()">Step 2. Preview Promotion <button class="btn btn-xs btn-info btn-round-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a></li>  
             <?php  }else{
                   if(($get_deal_value=="" || $qualify_for_tier==1) && ($applied_at_invoice!=1) || ($qualify_for_tier==1 && $applied_at_invoice!=1)){?>
                         <li  role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" onclick="check_activations()">Step 2. Preview Promotion <button class="btn btn-xs btn-info btn-round-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a></li>  
             <?php }
              else{ ?>
                         <li   role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" onclick="check_activations()">Step 1. Preview Promotion <button class="btn btn-xs btn-info btn-round-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a></li>  
              <?php }
              ?>
               
    
         <?php } }?>                               
                                    </ul>
                                    


<!-- Tab panes -->
<div class="tab-content">
	<div role="tabpanel" class="tab-pane" id="product_selector_container">
		
		<div class="row bg-info" id="total_items_listed_for_purchasing">
		      	<div class="col-sm-12">
		      		Products Tagged
		      		<ul id="list_container_containing_purchasing_items" class="list-unstyled">
		      		    <?php if(!empty($bundle_promoitems_on_purchase)){
		      		        foreach($bundle_promoitems_on_purchase as $data){ ?>
		      		            <?php if($data['pcat_id']==""){
		      		                echo '<li> All Items in Store <button class="material-button btn-xs btn-danger" bundle_id="'.$data['id'].'" onclick="delete_BundleItem(this)"><i class="fa fa-trash" aria-hidden="true"></i></button></li>';
		      		            }
                                else{
                                    if($data['pcat_id']!="" && $data['cat_id']=="" && $data['subcat_id']=="" && $data['brand_id']==""){
                                        echo $controller->get_parent_Category_name($data['pcat_id'],$promo_uid,$data['id']);
                                    }
                                    else{
                                        if($data['cat_id']!="" && $data['subcat_id']=="" && $data['brand_id']==""){
                                          echo $controller->get_Category_name($data['cat_id'],$promo_uid,$data['id']);  
                                        }
                                        else{
                                            if($data['subcat_id']!="" && $data['brand_id']==""){
                                                echo $controller->get_Sub_Category_name($data['subcat_id'],$promo_uid,$data['id']);
                                            }
                                        else{
                                                if($data['brand_id']!="" && $data['product_id']=="" ){
                                                    echo $controller->get_Brand_name($data['brand_id'],$promo_uid,$data['id']);
                                                }
                                                else{
                                                    echo $controller->get_Product_name($data['product_id'],$promo_uid,$data['id']);
                                                }
                                            }
                                        }
                                    }
                                    
                                }
		      		            ?>
		      		       <?php } 
		      		    }?>
		      			<?php if(!empty($promoitems_on_purchase)){
		      				foreach($promoitems_on_purchase as $data){ 
		      				    if($data['product_id']!=""){ ?>
		      				      <li id="<?php echo $data['product_id'].'_listpurch_'.$promo_uid;?>"><?php echo $controller->get_promoitems_on_purchase_list($data['product_id'],$promo_uid) ?></li>
		      				<?php } }
		      			}
		      			else{
		      			    if(empty($promoitems_on_purchase) && empty($bundle_promoitems_on_purchase)){
		      			        echo '<li>No Items Added</li>';
		      			    }
		      			}?>
		      		</ul>
		      	</div>
	      	</div>

 	      	
<div class="panel-group accordion-3" <?php if($is_promo_editable=="no"){echo "style='display:none;'";}?>>
            <div class="panel panel-info">
              <div class="panel-heading" data-toggle="collapse" data-parent=".accordion-3" href="#a3-a01" style="cursor:pointer;">
                <h4 class="panel-title">Add Products to Promotion <i class="fa fa-plus pull-right"></i></h4>
              </div>
              <div id="a3-a01" class="panel-collapse collapse">
                <div class="panel-body">
<?php //if(!empty($parent_category)){ ?>	      	
<?php if(1){ ?>	      	
	      	<form id="add_products_for_purchasing_in_promotions" onsubmit="return purchasing_items_submit()" class="form-horizontal" action="<?php echo base_url()?>admin/Promotions/add_items_tagged_for_purchasing" method="post">


			  	<div class="form-group">
				    <div class="col-md-12">
				    	<ul class="listack" id="item_picker_del">
						<li> All products in store</li>
						
						</ul>
				    </div>
				 </div>
				  <div class="form-group">
                        <div class="col-md-5">
                              <select class="form-control" name="parent_category" id="parent_category_selector" onchange="get_category()">
                                <option value="">Choose Parent Category</option>
                                <?php foreach($parent_category as $catdata){?>
                                    <option value="<?php echo $catdata['pcat_id'] ?>"><?php echo $catdata['pcat_name'] ?></option>
                                <?php } ?>
								<option value="0">-None-</option>
                              </select>
                        </div>
                        <div id="parentCategorySubmit">
                            <button type="submit" class="btn btn-info">Add All Items In Store For Promotion</button>
                        </div>
                   </div>
				   <div class="form-group" id="CategoryContainer">
				       
				   </div>
				   <div class="form-group" id="subCategoryContainer">

				   </div>
				   <div class="form-group" id="brandContainer">

				   </div>
				   <div class="form-group" id="attributeContainer">

				   </div>
				   <div class="form-group" id="productContainer">

				   </div>
				   <div class="form-group" id="productTypeContainer">

				   </div>
				   <div class="form-group">
					 	<b class="lead pull-right" id="collection_of_all_sku_to_buy"></b>
					 </div>

				    <div class="row" id="product_filter_level_selector_" style="display:none">
				        <div class="dual-list list-right col-md-6">
				            <div class="well">
				                <div class="form-group">
				                    <div class="col-md-12">
				                        <div class="input-group">
				                            <input type="text" id="right_list_search" name="SearchDualList" class="form-control" placeholder="search"> 
				                            <span class="input-group-addon glyphicon glyphicon-unchecked selector" style="cursor: pointer; top: 0px;" title="Select All"></span>
				                            <span class="input-group-addon glyphicon glyphicon-plus move-left" style="cursor: pointer; top: 0px;" title="Add Selected"></span>
				                        </div>
				                    </div>
				                </div>
				                <ul class="list-group" id="dual-list-right">
				                	

				                </ul>
				            </div>
				        </div>
				
				
				        <div class="dual-list list-left col-md-6">
				            <div class="well text-right">
				                <div class="form-group">
				                    <div class="col-md-12">
				                        <div class="input-group">
				                            <input type="text" id="left_list_search" name="SearchDualList" class="form-control" placeholder="search"> 
				                            <span class="input-group-addon glyphicon glyphicon-unchecked selector" style="cursor: pointer; top: 0px;" title="Select All"></span>
				                            <span class="input-group-addon glyphicon glyphicon-minus move-right" style="cursor: pointer; top: 0px;" title="Remove Selected"></span>
				                        </div>
				                    </div>
				                </div>
				                
				                <ul class="list-group" id="dual-list-left"></ul>
				            </div>
				        </div>
				        
					</div>
<input type="hidden" name="products_tagged_for_purchasing" id="products_tagged_for_purchasing"/>
<input type="hidden" name="promo_uid" id="products_tagged_for_purchasing_promo_uid" value="<?php echo $promo_uid ?>"/>
					  
					</form>
					
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center" id="modal_title">Promotion Alert
			<!-- Existing Promotions Running On Selected Products -->
		</h4>
      </div>
      <div class="modal-body" id="already_promotion_display">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="continueFun()" id="continue_to_submit">Continue To Submit</button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">Reconsider</button>
      </div>
    </div>

  </div>
</div>
<script>
function continueFun(){
    document.getElementById("add_products_for_purchasing_in_promotions").submit();
}
function purchasing_items_submit() { 
    var values = {};
    var products_tagged_for_purchasing=''
    var parent_category=''
    var category =''
    var sub_category=''
    var attributes =''
    var attributes_selector=""
    var promotionsFormsAdd=document.getElementById('add_products_for_purchasing_in_promotions')
    var formselectelements=promotionsFormsAdd.getElementsByTagName('select')
    
    for(var i=0;i<formselectelements.length;i++){
        if(formselectelements[i].name=="parent_category"){
            parent_category = formselectelements[i].value;
        }
        if(formselectelements[i].name=="category"){
            category = formselectelements[i].value;
        }
        if(formselectelements[i].name=="sub_category"){
            sub_category = formselectelements[i].value;
        }
        if(formselectelements[i].name=="attributes"){
            attributes = formselectelements[i].value;
        }
        if(formselectelements[i].name=="attributes_selector"){
            attributes_selector = formselectelements[i].value;
        }
    }
products_tagged_for_purchasing=document.getElementById('products_tagged_for_purchasing').value
var promo_uid=document.getElementById('products_tagged_for_purchasing_promo_uid').value

if((parent_category=="")&&(category=="")&&(sub_category=="")&&(attributes=="")&&(attributes_selector=="")&&(products_tagged_for_purchasing=="")){

    getAllStoreRelatedProductDiscount(promo_uid)
}
if((parent_category!="")&&(category=="")&&(sub_category=="")&&(attributes=="")&&(attributes_selector=="")&&(products_tagged_for_purchasing=="")){
    getAllparent_categoryRelatedProductDiscount(promo_uid,parent_category)
}
if((parent_category!="")&&(category!="")&&(sub_category=="")&&(attributes=="")&&(attributes_selector=="")&&(products_tagged_for_purchasing=="")){
    getAllcategoryRelatedProductDiscount(promo_uid,parent_category,category)
}
if((parent_category!="")&&(category!="")&&(sub_category!="")&&(attributes=="")&&(attributes_selector=="")&&(products_tagged_for_purchasing=="")){
    getAllsub_categoryRelatedProductDiscount(promo_uid,parent_category,category,sub_category)
}
if((parent_category!="")&&(category!="")&&(sub_category!="")&&(attributes!="")&&(attributes_selector=="")&&(products_tagged_for_purchasing=="")){
    getAllattributesRelatedProductDiscount(promo_uid,parent_category,category,sub_category,attributes)
}
if((parent_category!="")&&(category!="")&&(sub_category!="")&&(attributes!="")&&(attributes_selector!="")&&(products_tagged_for_purchasing=="")){
     getAllattributes_selectorRelatedProductDiscount(promo_uid,parent_category,category,sub_category,attributes,attributes_selector)
}
if((parent_category!="")&&(category!="")&&(sub_category!="")&&(attributes!="")&&(attributes_selector!="")&&(products_tagged_for_purchasing!="")){
     getAllproducts_tagged_for_purchasingRelatedProductDiscount(promo_uid,parent_category,category,sub_category,attributes,attributes_selector,products_tagged_for_purchasing)
}

  return false;
} 


function getAllStoreRelatedProductDiscount(promo_uid){
  var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
		 //alert('here');

                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                               if(xhr.responseText!=""){
                                   document.getElementById('already_promotion_display').innerHTML=xhr.responseText
								   moq_valid=$('#moq_valid').val();
								   if(parseInt(moq_valid)>0){
										$('#continue_to_submit').hide();
								   }else{
										$('#continue_to_submit').show();
								   }
                                    $('#myModal').modal('show');
                               }
                               else{
                                   continueFun()
                               }
                            
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/getAllStoreRelatedProductDiscount", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('promo_uid='+promo_uid);
                }
}
function getAllparent_categoryRelatedProductDiscount(promo_uid,obj){
  var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {

                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                           if(xhr.responseText!=""){
                                   document.getElementById('already_promotion_display').innerHTML=xhr.responseText
								   moq_valid=$('#moq_valid').val();
								   if(parseInt(moq_valid)>0){
										$('#continue_to_submit').hide();
								   }else{
										$('#continue_to_submit').show();
								   }
                                    $('#myModal').modal('show');
                           }
                           else{
                               continueFun()
                           }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/getAllparent_categoryRelatedProductDiscount", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('promo_uid='+promo_uid+'&parent_category='+obj);
                }
}
function getAllcategoryRelatedProductDiscount(promo_uid,parent_category,category){
  var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if(xhr.responseText!=""){
                                   document.getElementById('already_promotion_display').innerHTML=xhr.responseText
								   moq_valid=$('#moq_valid').val();
								   if(parseInt(moq_valid)>0){
										$('#continue_to_submit').hide();
								   }else{
										$('#continue_to_submit').show();
								   }
                                    $('#myModal').modal('show');
                           }
                           else{
                               continueFun()
                           }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/getAllcategoryRelatedProductDiscount", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('promo_uid='+promo_uid+'&parent_category='+parent_category+'&category='+category);
                }
}
function getAllsub_categoryRelatedProductDiscount(promo_uid,parent_category,category,sub_category){
  var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                           if(xhr.responseText!=""){
                                   document.getElementById('already_promotion_display').innerHTML=xhr.responseText
								   moq_valid=$('#moq_valid').val();
								   if(parseInt(moq_valid)>0){
										$('#continue_to_submit').hide();
								   }else{
										$('#continue_to_submit').show();
								   }
                                    $('#myModal').modal('show');
                           }
                           else{
                               continueFun()
                           }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/getAllsub_categoryRelatedProductDiscount", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('promo_uid='+promo_uid+'&parent_category='+parent_category+'&category='+category+'&sub_category='+sub_category);
                }
}
function getAllattributesRelatedProductDiscount(promo_uid,parent_category,category,sub_category,attributes){
  var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if(xhr.responseText!=""){
                                   document.getElementById('already_promotion_display').innerHTML=xhr.responseText
								   moq_valid=$('#moq_valid').val();
								   if(parseInt(moq_valid)>0){
										$('#continue_to_submit').hide();
								   }else{
										$('#continue_to_submit').show();
								   }
                                    $('#myModal').modal('show');
                           }
                           else{
                               continueFun()
                           }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/getAllattributesRelatedProductDiscount", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('promo_uid='+promo_uid+'&parent_category='+parent_category+'&category='+category+'&sub_category='+sub_category+'&attributes='+attributes);
                }
}
function getAllattributes_selectorRelatedProductDiscount(promo_uid,parent_category,category,sub_category,attributes,attributes_selector){
  var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if(xhr.responseText!=""){
                                   document.getElementById('already_promotion_display').innerHTML=xhr.responseText
								   moq_valid=$('#moq_valid').val();
								   if(parseInt(moq_valid)>0){
										$('#continue_to_submit').hide();
								   }else{
										$('#continue_to_submit').show();
								   }
                                    $('#myModal').modal('show');
                           }
                           else{
                               continueFun()
                           }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/getAllattributes_selectorRelatedProductDiscount", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('promo_uid='+promo_uid+'&parent_category='+parent_category+'&category='+category+'&sub_category='+sub_category+'&attributes='+attributes+'&attributes_selector='+attributes_selector);
                }
}
function getAllproducts_tagged_for_purchasingRelatedProductDiscount(promo_uid,parent_category,category,sub_category,attributes,attributes_selector,products_tagged_for_purchasing){


	//alert('comming here');

  var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {

							//return false;
                            if(xhr.responseText!=""){
                                   document.getElementById('already_promotion_display').innerHTML=xhr.responseText
								   moq_valid=$('#moq_valid').val();
								   if(parseInt(moq_valid)>0){
										$('#continue_to_submit').hide();
								   }else{
										$('#continue_to_submit').show();
								   }
                                    $('#myModal').modal('show');
                           }
                           else{
                               continueFun()
                           }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/getAllproducts_tagged_for_purchasingRelatedProductDiscount", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('promo_uid='+promo_uid+'&parent_category='+parent_category+'&category='+category+'&sub_category='+sub_category+'&attributes='+attributes+'&attributes_selector='+attributes_selector+'&products_tagged_for_purchasing='+products_tagged_for_purchasing);
                }
}




 
</script>
					
					
					
				   <?php  }
else{
	echo "No products to show";
}
 ?> 
                </div>
              </div>
            </div>
 </div>	      	

	</div>
	
	
	
	
	   <?php 
	   if (!preg_match("/\b(Same SKUs)\b/i", $promo_quote, $match)) {
	   
	   if($get_deal_value!="" && ($buy_deal_type!='')&&($get_deal_type!=curr_code)&&($get_deal_type!='cash back') && $applied_at_invoice!=1){?>
    
    <div role="tabpanel" class="tab-pane 
    <?php 
	
	if($applied_at_invoice=='' && $qualify_for_tier==1){
		if($this->session->flashdata("notification")){
			if($this->session->flashdata("notification")=="Proceed To Step 2"){
				echo 'active';
			}
		}
		
		if($this->session->flashdata("notification")){
			if($this->session->flashdata("notification")=="Update Giveaway Units"){
				echo 'active';
			}
		}
	}
	?>

    " id="product_selector_container_freeItem">
        
        <div class="row bg-info" id="total_items_listed_for_giving">
                <div class="col-md-12">
                    Products Tagged
                    <table class="table table-hover" id="list_container_containing_purchasing_items_freeItem">
                        <thead>
                            <tr>
                                <td width="60%"></td>
                                <td width="25%"></td>
                                <td width="10%"></td>
                                <td width="5%"></td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($promoitems_on_purchase_free)){
                            foreach($promoitems_on_purchase_free as $data){ ?>
                                <?php echo $controller->get_promoitems_on_purchase_list_free($data['product_id'],$data['inv_id'],$promo_uid,$data['quantity_def']) ?>
                            <?php }
                        }
                        else{
                            echo '<tr><td>No Items Added</td></tr>';
                        }?>
                        </tbody>
                    </table>
                
                </div>
            </div><br>

<div class="panel-group accordion-3">
            <div class="panel panel-info ">
              <div class="panel-heading" data-toggle="collapse" data-parent=".accordion-3" href="#a3-a02">
                <h4 class="panel-title">Add Giveaway Products <i class="fa fa-plus pull-right"></i></h4>
              </div>
              <div id="a3-a02" class="panel-collapse collapse">
                <div class="panel-body">
 <?php //if(!empty($parent_category)){ ?>
 <?php if(1){ ?>
        <form class="form-horizontal" action="<?php echo base_url()?>admin/Promotions/add_items_tagged_for_purchasing_free" method="post">
                <div class="form-group">
                    <div class="col-md-10">
                        <ul class="listack" id="item_picker_del_freeItem">
                        <li> All products in store</li>
                        </ul>
                    </div>
                 </div>
                 
                    
                 
                   <div class="form-group">
                        <div class="col-md-10">
                              <select class="form-control" name="parent_category" id="parent_category_selector_freeItem" onchange="get_category_freeItem()">
                                <option value="">Choose Parent Category</option>
                                <?php foreach($parent_category as $catdata){?>
                                    <option value="<?php echo $catdata['pcat_id'] ?>"><?php echo $catdata['pcat_name'] ?></option>
                                <?php } ?>
								 <option value="0">-None-</option>
                              </select>
                        </div>
                   </div>
                   <div class="form-group" id="CategoryContainer_freeItem">
                       
                   </div>
                   <div class="form-group" id="subCategoryContainer_freeItem">

                   </div>
                   <div class="form-group" id="brandContainer_freeItem">

                   </div>
                   <div class="form-group" id="attributeContainer_freeItem">

                   </div>
                   <div class="form-group" id="productContainer_freeItem">

                   </div>
                   <div class="form-group" id="productTypeContainer_freeItem">

                   </div>
                   <div class="form-group">
 <b class="lead pull-right" id="collection_of_all_sku_to_buy_freeItem"></b>
                 </div>

                    <div class="row" id="product_filter_level_selector_freeItem" style="display:none">
                        <div class="dual-list list-right col-md-6">
                            <div class="well">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="input-group"> 
                                            <input type="text" id="right_list_search_freeItem" name="SearchDualList" class="form-control" placeholder="search"> 
                                            <span class="input-group-addon glyphicon glyphicon-unchecked selector" style="cursor: pointer; top: 0px;" title="Select All"></span>
                                            <span class="input-group-addon glyphicon glyphicon-plus move-left" style="cursor: pointer; top: 0px;" title="Add Selected"></span>
                                        </div>
                                    </div>
                                </div>
                                <ul class="list-group" id="dual-list-right_freeItem">
                                    

                                </ul>
                            </div>
                        </div>
                
                
                        <div class="dual-list list-left col-md-6">
                            <div class="well text-right">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="text" id="left_list_search_freeItem" name="SearchDualList" class="form-control" placeholder="search"> 
                                            <span class="input-group-addon glyphicon glyphicon-unchecked selector" style="cursor: pointer; top: 0px;" title="Select All"></span>
                                            <span class="input-group-addon glyphicon glyphicon-minus move-right" style="cursor: pointer; top: 0px;" title="Remove Selected"></span>
                                        </div>
                                    </div>
                                </div>
                                
                                <ul class="list-group" id="dual-list-left_freeItem"></ul>
                            </div>
                        </div>
                        
                    </div>
<input type="hidden" name="products_tagged_for_purchasing" id="products_tagged_for_purchasing_freeItem"/>
<input type="hidden" name="promo_uid" value="<?php echo $promo_uid ?>"/>                      
                    </form>
                    
                   <?php  }
else{
    echo "No products to show";
}
 ?>
                </div>
              </div>
            </div>
 </div>
            
            

        
    </div>
    <?php } }?>
	
	<?php if($get_deal_value!="" && ($buy_deal_type!='')&&($get_deal_type!=curr_code)&&($get_deal_type!='cash back') && $applied_at_invoice==1){?>
	
	<div role="tabpanel" class="tab-pane 
	<?php if($this->session->flashdata("notification")){
		 if($this->session->flashdata("notification")=="Proceed To Step 2"){
		 	echo 'active';
		 }
	}?>  
	<?php if($this->session->flashdata("notification")){
		 if($this->session->flashdata("notification")=="Update Giveaway Units"){
		 	echo 'active';
		 }
	} ?> 
	 " id="product_selector_container_freeItem">
		
		<div class="row bg-info" id="total_items_listed_for_giving">
		      	<div class="col-md-12">
		      		Products Tagged
		      		<table class="table table-hover" id="list_container_containing_purchasing_items_freeItem">
		      			<thead>
		      				<tr>
		      					<td width="60%"></td>
		      					<td width="25%"></td>
		      					<td width="10%"></td>
		      					<td width="5%"></td>
		      				</tr>
		      			</thead>
		      			<tbody>
		      			<?php if(!empty($promoitems_on_purchase_free)){
		      				foreach($promoitems_on_purchase_free as $data){ ?>
		      					<?php echo $controller->get_promoitems_on_purchase_list_free($data['product_id'],$data['inv_id'],$promo_uid,$data['quantity_def']) ?>
		      				<?php }
		      			}
		      			else{
		      				echo '<tr><td>No Items Added</td></tr>';
		      			}?>
		      			</tbody>
		      		</table>
		      	
		      	</div>
	      	</div><br>

<div class="panel-group accordion-3">
            <div class="panel panel-info ">
              <div class="panel-heading" data-toggle="collapse" data-parent=".accordion-3" href="#a3-a02">
                <h4 class="panel-title">Add Giveaway Products <i class="fa fa-plus pull-right"></i></h4>
              </div>
              <div id="a3-a02" class="panel-collapse collapse">
                <div class="panel-body">
 <?php //if(!empty($parent_category)){ ?>
 <?php if(1){ ?>
      	<form class="form-horizontal" action="<?php echo base_url()?>admin/Promotions/add_items_tagged_for_purchasing_free" method="post">
			  	<div class="form-group">
				    <div class="col-md-10">
				    	<ul class="listack" id="item_picker_del_freeItem">
						<li> All products in store</li>
						
						</ul>
				    </div>
				 </div>
				 
				 	
				 
				   <div class="form-group">
                        <div class="col-md-10">
                              <select class="form-control" name="parent_category" id="parent_category_selector_freeItem" onchange="get_category_freeItem()">
                                <option value="">Choose Parent Category</option>
                                <?php foreach($parent_category as $catdata){?>
                                    <option value="<?php echo $catdata['pcat_id'] ?>"><?php echo $catdata['pcat_name'] ?></option>
                                <?php } ?>
								<option value="0">-None-</option>
                              </select>
                        </div>
                   </div>
                   <div class="form-group" id="CategoryContainer_freeItem">
                       
                   </div>
				   <div class="form-group" id="subCategoryContainer_freeItem">

				   </div>
				   <div class="form-group" id="brandContainer_freeItem">

				   </div>
				   <div class="form-group" id="attributeContainer_freeItem">

				   </div>
				   <div class="form-group" id="productContainer_freeItem">

				   </div>
				   <div class="form-group" id="productTypeContainer_freeItem">

				   </div>
				   <div class="form-group">
				<b class="lead pull-right" id="collection_of_all_sku_to_buy_freeItem"></b>
				 </div>

				    <div class="row" id="product_filter_level_selector_freeItem" style="display:none">
				        <div class="dual-list list-right col-md-6">
				            <div class="well">
				                <div class="form-group">
				                    <div class="col-md-12">
				                        <div class="input-group">
				                            <input type="text" id="right_list_search_freeItem" name="SearchDualList" class="form-control" placeholder="search"> 
				                            <span class="input-group-addon glyphicon glyphicon-unchecked selector" style="cursor: pointer; top: 0px;" title="Select All"></span>
				                            <span class="input-group-addon glyphicon glyphicon-plus move-left" style="cursor: pointer; top: 0px;" title="Add Selected"></span>
				                        </div>
				                    </div>
				                </div>
				                <ul class="list-group" id="dual-list-right_freeItem">
				                	

				                </ul>
				            </div>
				        </div>
				
				
				        <div class="dual-list list-left col-md-6">
				            <div class="well text-right">
				                <div class="form-group">
				                    <div class="col-md-12">
				                        <div class="input-group">
				                            <input type="text" id="left_list_search_freeItem" name="SearchDualList" class="form-control" placeholder="search"> 
				                            <span class="input-group-addon glyphicon glyphicon-unchecked selector" style="cursor: pointer; top: 0px;" title="Select All"></span>
				                            <span class="input-group-addon glyphicon glyphicon-minus move-right" style="cursor: pointer; top: 0px;" title="Remove Selected"></span>
				                        </div>
				                    </div>
				                </div>
				                
				                <ul class="list-group" id="dual-list-left_freeItem"></ul>
				            </div>
				        </div>
				        
					</div>
<input type="hidden" name="products_tagged_for_purchasing" id="products_tagged_for_purchasing_freeItem"/>
<input type="hidden" name="promo_uid" value="<?php echo $promo_uid ?>"/>					  
					</form>
					
				   <?php  }
else{
	echo "No products to show";
}
 ?>
                </div>
              </div>
            </div>
 </div>

	</div>
	<?php } ?>
	
	<?php if($get_deal_value!="" && ($buy_deal_type=='')&&($get_deal_type!=curr_code)&&($get_deal_type!='cash back') && $applied_at_invoice!=1){?>
	
	
		<div role="tabpanel" class="tab-pane 
	<?php if($this->session->flashdata("notification")){
		 if($this->session->flashdata("notification")=="Proceed To Step 2"){
		 	echo 'active';
		 }
	}?>
	<?php if($this->session->flashdata("notification")){
		 if($this->session->flashdata("notification")=="Update Giveaway Units"){
		 	echo 'active';
		 }
	} ?> 
	" id="product_selector_container_freeItem">
		
		<div class="row bg-info" id="total_items_listed_for_giving">
		      	<div class="col-md-12">
		      	Products Tagged: 
		      		<table class="table table-hover" id="list_container_containing_purchasing_items_freeItem">
		      			<thead>
		      				<tr>
		      					<td width="60%"></td>
		      					<td width="25%"></td>
		      					<td width="10%"></td>
		      					<td width="5%"></td>
		      				</tr>
		      			</thead>
		      			<tbody>
		      			<?php if(!empty($promoitems_on_purchase_free)){
		      				foreach($promoitems_on_purchase_free as $data){ ?>
		      					<?php echo $controller->get_promoitems_on_purchase_list_free($data['product_id'],$data['inv_id'],$promo_uid,$data['quantity_def']) ?>
		      				<?php }
		      			}
		      			else{
		      				echo '<tr><td>No Items Added</td></tr>';
		      			}?>
		      			</tbody>
		      		</table>
		      	
		      	</div>
	      	</div>

<div class="panel-group accordion-3">
            <div class="panel panel-info ">
              <div class="panel-heading" data-toggle="collapse" data-parent=".accordion-3" href="#a3-a02">
                <h4 class="panel-title">Add Giveaway Products <i class="fa fa-plus pull-right"></i></h4>
              </div>
              <div id="a3-a02" class="panel-collapse collapse">
                <div class="panel-body">
 <?php //if(!empty($parent_category)){ ?>
 <?php if(1){ ?>
      	<form class="form-horizontal" action="<?php echo base_url()?>admin/Promotions/add_items_tagged_for_purchasing_free" method="post">
			  	<div class="form-group">
				    <div class="col-md-10">
				    	<ul class="listack" id="item_picker_del_freeItem">
						<li> All products in store</li>
						
						</ul>
				    </div>
				 </div>
				 
				 	
				 
				   <div class="form-group">
                        <div class="col-md-10">
                              <select class="form-control" name="parent_category" id="parent_category_selector_freeItem" onchange="get_category_freeItem()">
                                <option value="">Choose Parent Category</option>
                                <?php foreach($parent_category as $catdata){?>
                                    <option value="<?php echo $catdata['pcat_id'] ?>"><?php echo $catdata['pcat_name'] ?></option>
                                <?php } ?>
								<option value="0">-None-</option>
                              </select>
                        </div>
                   </div>
                   <div class="form-group" id="CategoryContainer_freeItem">
                       
                   </div>
				   <div class="form-group" id="subCategoryContainer_freeItem">

				   </div>
				   <div class="form-group" id="brandContainer_freeItem">

				   </div>
				   <div class="form-group" id="attributeContainer_freeItem">

				   </div>
				   <div class="form-group" id="productContainer_freeItem">

				   </div>
				   <div class="form-group" id="productTypeContainer_freeItem">

				   </div>
				   <div class="form-group">
				<b class="lead pull-right" id="collection_of_all_sku_to_buy_freeItem"></b>
				 </div>

				    <div class="row" id="product_filter_level_selector_freeItem" style="display:none">
				        <div class="dual-list list-right col-md-6">
				            <div class="well">
				                <div class="form-group">
				                    <div class="col-md-12">
				                        <div class="input-group">
				                            <input type="text" id="right_list_search_freeItem" name="SearchDualList" class="form-control" placeholder="search"> 
				                            <span class="input-group-addon glyphicon glyphicon-unchecked selector" style="cursor: pointer; top: 0px;" title="Select All"></span>
				                            <span class="input-group-addon glyphicon glyphicon-plus move-left" style="cursor: pointer; top: 0px;" title="Add Selected"></span>
				                        </div>
				                    </div>
				                </div>
				                <ul class="list-group" id="dual-list-right_freeItem">
				                	

				                </ul>
				            </div>
				        </div>
				
				
				        <div class="dual-list list-left col-md-6">
				            <div class="well text-right">
				                <div class="form-group">
				                    <div class="col-md-12">
				                        <div class="input-group">
				                            <input type="text" id="left_list_search_freeItem" name="SearchDualList" class="form-control" placeholder="search"> 
				                            <span class="input-group-addon glyphicon glyphicon-unchecked selector" style="cursor: pointer; top: 0px;" title="Select All"></span>
				                            <span class="input-group-addon glyphicon glyphicon-minus move-right" style="cursor: pointer; top: 0px;" title="Remove Selected"></span>
				                        </div>
				                    </div>
				                </div>
				                
				                <ul class="list-group" id="dual-list-left_freeItem"></ul>
				            </div>
				        </div>
				        
					</div>
<input type="hidden" name="products_tagged_for_purchasing" id="products_tagged_for_purchasing_freeItem"/>
<input type="hidden" name="promo_uid" value="<?php echo $promo_uid ?>"/>					  
					</form>
					
				   <?php  }
else{
	echo "No products to show";
}
 ?>
                </div>
              </div>
            </div>
 </div>
	      	
	      	

		
	</div>
	<?php } ?>
	
	
	
	<div role="tabpanel"  class="tab-pane 
	<?php if(!($get_deal_type==curr_code || $qualify_for_different==1 ||$get_deal_value!="")){
             if($this->session->flashdata("notification")){
                 if($this->session->flashdata("notification")=="Proceed To Step 2"){
                    echo 'active';
                 }
             }
    }?>
    <?php if($applied_at_invoice==1 && $get_deal_value==""){
        echo "active";
    }?> 
	
	" id="messages">
		<div class="row">
			<div class="col-md-12 text-center">
				<h1><span id="promotion_summary_promo_name"><?php echo $promo_name ?></span></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">
				<h1><span id="promotion_summary_promo_quote"><?php echo $promo_quote ?></span></h1>
			</div>
		</div>
		
		<div style="display:inline-block;height:5%"></div>
		
		<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-4 text-right" for="email">Start Date:</label>
						<div class="col-md-6">
							<p style="font-size:1.3em"><span id="promotion_summary_start_date"><?php echo $promo_start_date?></span></p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-4 text-right" for="email">End Date:</label>
						<div class="col-md-6 ">
			                <p style="font-size:1.3em"><span id="promotion_summary_end_date"><?php echo $promo_end_date?></span></p>
						</div>
					</div>
				</div>
			</div>
			
		<div style="display:inline-block;height:5%"></div>
		
		     <div id="accordion-first" class="clearfix">
                        <div class="accordion" id="accordion2">
                        <?php if($applied_at_invoice!=1){ ?>
                          <div class="col-md-6">
                            <div class="col-md-6 col-md-offset-2">
                                <div class="accordion-group">
                                <div class="accordion-heading">
                                  <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                    <em class="icon-fixed-width fa fa-plus"></em>Products Tagged To Promotion
                                  </a>
                                </div>
                                <div style="height: 0px;" id="collapseOne" class="accordion-body collapse">
                                  <div class="accordion-inner">
                                    <div class="col-md-12" id="summary_for_purchasing_"></div>
                                  </div>
                                </div>
                               </div>
                          </div>
                        </div>
                       <?php } ?>
                       
                       <?php /* if(!($get_deal_type==curr_code ||$qualify_for_different!=1)&& ($get_deal_value!=""||$applied_at_invoice!=1||$qualify_for_tier!=1)){ ?>
                        <div class="col-md-6">
                        	<div class="col-md-6 col-md-offset-2">
	                        	<div class="accordion-group">
		                            <div class="accordion-heading">
		                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
		                                <em class="icon-fixed-width fa fa-plus"></em>Products Tagged To Give Away Products
		                              </a>
		                            </div>
		                            <div style="height: 0px;" id="collapseFour" class="accordion-body collapse">
		                              <div class="accordion-inner">
		                                <div class="col-md-12" id="summary_for_giving_"></div>
		                              </div>
		                            </div>
		                          </div>
                          	</div>
                        </div>	
                        <?php } */ ?>	
                        <?php 
						if (!preg_match("/\b(Same SKUs)\b/i", $promo_quote, $match)) {
						if($get_deal_value!="" && ($buy_deal_type!='')&&($get_deal_type!=curr_code)&&($get_deal_type!='cash back') && $applied_at_invoice!=1 && $qualify_for_tier!=1){ ?>
                          <div class="col-md-6">
                            <div class="col-md-6 col-md-offset-2">
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                      <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                                        <em class="icon-fixed-width fa fa-plus"></em>Products Tagged To Give Away Products
                                      </a>
                                    </div>
                                    <div style="height: 0px;" id="collapseFour" class="accordion-body collapse">
                                      <div class="accordion-inner">
                                        <div class="col-md-12" id="summary_for_giving_"></div>
                                      </div>
                                    </div>
                                  </div>
                            </div>
                        </div>  
                            <?php }  
						if($get_deal_value!="" && ($buy_deal_type!='')&&($get_deal_type!=curr_code)&&($get_deal_type!='cash back') && ($get_deal_type!='discount') && $applied_at_invoice==1){ ?>
						
						<?php 
						$show_pro_tab=1;
						$price_arr=explode(',',$buy_deal_type);
						if(count($price_arr)>1){
							$show_pro_tab=0;
						}else{
							if((in_array(curr_code,$price_arr)) && $applied_at_invoice==0){
							$show_pro_tab=0;
							}
						}
						
						
								if($show_pro_tab==1){
									?>
	
						<div class="col-md-6"></div>
                          <div class="col-md-6">
                            <div class="col-md-6 col-md-offset-2">
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                      <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                                        <em class="icon-fixed-width fa fa-plus"></em>Products Tagged To Give Away Products
                                      </a>
                                    </div>
                                    <div style="height: 0px;" id="collapseFour" class="accordion-body collapse">
                                      <div class="accordion-inner">
                                        <div class="col-md-12" id="summary_for_giving_"></div>
                                      </div>
                                    </div>
                                  </div>
                            </div>
                        </div>  

                            <?php 
									}
								}	
							 }
							?>
                       <?php /* if($get_deal_value!="" && ($buy_deal_type==curr_code)&&($get_deal_type!=curr_code))
					   
					   { ?>
                        <div class="col-md-6 pull-right">
                            <div class="col-md-6 col-md-offset-2">
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                      <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                                        <em class="icon-fixed-width fa fa-plus"></em>fgdProducts Tagged To Give Away Products
                                      </a>
                                    </div>
                                    <div style="height: 0px;" id="collapseFour" class="accordion-body collapse">
                                      <div class="accordion-inner">
                                        <div class="col-md-12" id="summary_for_giving_"></div>
                                      </div>
                                    </div>
                                  </div>
                            </div>
                        </div>  
                        <?php } */ ?>   
                       <?php if($get_deal_value!="" && ($buy_deal_type=='')&&($get_deal_type!=curr_code) && ($get_deal_type!="cash back")){?>
                           <div class="col-md-6 pull-right">
                            <div class="col-md-6 col-md-offset-2">
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                      <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                                        <em class="icon-fixed-width fa fa-plus"></em>Products Tagged To Give Away Products
                                      </a>
                                    </div>
                                    <div style="height: 0px;" id="collapseFour" class="accordion-body collapse">
                                      <div class="accordion-inner">
                                        <div class="col-md-12" id="summary_for_giving_"></div>
                                      </div>
                                    </div>
                                  </div>
                            </div>
                        </div>
                           
                        <?php } ?>      
                        </div><!-- end accordion -->
                    </div>
      
         
          

           <div style="display:inline-block;height:10%"></div>
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-6 text-right">
						<div class="form-group">
							<div id="bottom_activate_button_div">
							
							<?php if($promo_active==0){?>
									<?php if($promo_approval!=1){ ?> 
										<?php if($approval_sent==0){?>
												<button class="btn btn-success btn-lg" id="approve_promo_button2" promo_id="<?php echo $promo_uid ?>" onclick="send_approval_for_promotion(this)" >Send For Approval</button>
										<?php }else{ ?>
											<p class="lead">Sent For Approval</p>
										<?php }?>
									<?php }else{ ?> 
										<button class="btn btn-success btn-lg" id="activate_promo_button2" promo_id="<?php echo $promo_uid ?>" onclick="activate_promotion(this)" >Activate Promotion</button>
									<?php }?>
								<?php }?>
								<?php if($promo_active==1){ ?>
									<button class="btn btn-warning btn-lg" id="draft_promo_button" promo_id="<?php echo $promo_uid ?>" onclick="draft_promotion(this)" >Deactivate</button>
							<?php }?> 
							
							</div>
							
						</div>
					</div>
					<div class="col-xs-6 text-left">
						<div class="form-group">
							<button class="btn btn-warning btn-lg" id="draft_promo_button" promo_id="<?php echo $promo_uid ?>" onclick="draft_promotion(this)" >Save As Draft</button>
						</div>
					</div>
				</div>
				<div class="col-xs-12" id="inActiveNotification_giving">
					
				</div>
				<div class="col-xs-12" id="inActiveNotification_enddate">
					
				</div>
				<div class="col-xs-12" id="inActiveNotification_startdate">
					
				</div>
				<div class="col-xs-12" id="inActiveNotification_purchasing">
					
				</div>
				<div class="col-xs-12" id="inActiveNotification_enddate">
					
				</div>
				<div class="col-xs-12" id="inActiveNotification_startdate">
					
				</div>
				<div class="col-xs-12" id="inActiveNotification_purchasing">
					
				</div>
				<div class="col-xs-12" id="inActiveNotification_expired">
					
				</div>
				

			</div>
                                        
</div>


<script>
function delete_BundleItem(obj){
    var promo_id='<?php echo $promo_uid?>'
var ul = obj.parentNode.parentNode

var liNodes = [];

for (var i = 0; i < ul.childNodes.length; i++) {
    if (ul.childNodes[i].nodeName == "LI") {
        liNodes.push(ul.childNodes[i]);
    }
}
count_items_for_purchasing=liNodes.length
    var goAhead=1;
    var deactivate_flag=0
    
    <?php if($promo_active==1){ ?>

    if(count_items_for_purchasing==1){
        var answer = confirm ("WARNING! This promotion is active. Deleting all items will deactivate the promotion");
        if (answer){
            goAhead=1
            deactivate_flag=1
            }
        else{goAhead=0;return false}
    }
    <?php } ?>
    var approval_flag=0
    <?php if($approval_sent==1 && $promo_active==0){?> 

    if(count_items_for_purchasing==1){
        var answer = confirm ("WARNING! This promotion is already sent for approvals. Deleting all items will cancel the approval request.");
        if (answer){
            goAhead=1
            approval_flag=1
            }
        else{goAhead=0;return false}
    }
    <?php } ?>

    var bundle_id=obj.getAttribute('bundle_id');

    if(goAhead==1){           
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                    if(xhr.responseText==1){
                                        obj.parentNode.remove();
                                        var ul = document.getElementById('list_container_containing_purchasing_items')

                                        var liNodes = [];
                                        
                                        for (var i = 0; i < ul.childNodes.length; i++) {
                                            if (ul.childNodes[i].nodeName == "LI") {
                                                liNodes.push(ul.childNodes[i]);
                                            }
                                        }
                                        if(liNodes.length==0){
                                            document.getElementById('list_container_containing_purchasing_items').innerHTML="<li>No Items Added</li>"
                                        }
                                        
                                    }
                                    if(deactivate_flag==1){
                                        if(count_items_for_purchasing==1){
                                        alert("This promotion is deactivated")
                                        window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
                                        }
                                    }
                                    if(approval_flag==1){
                                        if(count_items_for_purchasing==1){
                                        alert("The approval request is cancelled")
                                        window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
                                        }
                                    }
                                    check_activations();
                                }   
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/delete_bundle_item", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send("bundle_id="+bundle_id+"&promo_id="+promo_id+"&deactivate_flag="+deactivate_flag+"&approval_flag="+approval_flag);
      }
      else{
          return false;
      }
    
}

window.load=check_activations()
function check_activations(){
	<?php if(date('Y-m-d H:i') > date($promo_end_datev)){ ?>
		

			document.getElementById('inActiveNotification_expired').innerHTML='<div class="col-md-6 col-md-offset-3 alert alert-danger"><strong>Promotion Expired </strong> End Date is before current date</div>'
			
		<?php }
		else{ ?>
			document.getElementById('inActiveNotification_expired').innerHTML=''
		<?php } ?>
	
	get_promotion_summary()
	<?php if(!($get_deal_type==curr_code || $get_deal_value==""||$applied_at_invoice==1||$qualify_for_different!=1)){ ?>
	var quantity_def_free=0;
	
	var final_free_button_obj_arr=document.getElementsByClassName("final_free_button");
	for(i=0;i<final_free_button_obj_arr.length;i++){
		 quantity_def_free+=parseInt(final_free_button_obj_arr[i].getAttribute("quantity_def_free"));
	}
	var get_deal_value='<?php echo $get_deal_value ?>'


		//var get_deal_value=parseInt(document.getElementById("get_deal_value").value);
	
		if(get_deal_value!=quantity_def_free){

			document.getElementById('inActiveNotification_giving').innerHTML='<div class="col-md-6 col-md-offset-3 alert alert-danger"><strong><?php if($approval_sent!=1){echo "Promotion Cannot Be Sent For Approval !!";}else{echo "Promotion Cannot Be Activated !!";}?>.</strong> Giveaway products units are mismatching</div>'
			
		}
		else{
			document.getElementById('inActiveNotification_giving').innerHTML=''
		}
	
	
	<?php } ?>
	<?php if($get_deal_value!="" && ($buy_deal_type==curr_code)&&($get_deal_type!=curr_code)&&($get_deal_type!='cash back') && ($get_deal_type!='discount')){ ?>
	    var quantity_def_free=0;
    var final_free_button_obj_arr=document.getElementsByClassName("final_free_button");
    for(i=0;i<final_free_button_obj_arr.length;i++){
         quantity_def_free+=parseInt(final_free_button_obj_arr[i].getAttribute("quantity_def_free"));
    }
    var get_deal_value='<?php echo $get_deal_value ?>'


        //var get_deal_value=parseInt(document.getElementById("get_deal_value").value);
    
        if(get_deal_value!=quantity_def_free){

            document.getElementById('inActiveNotification_giving').innerHTML='<div class="col-md-6 col-md-offset-3 alert alert-danger"><strong><?php if($approval_sent!=1){echo "Promotion Cannot Be Sent For Approval !!";}else{echo "Promotion Cannot Be Activated !!";}?>.</strong> Giveaway products units are mismatching</div>'
            
        }
        else{
            document.getElementById('inActiveNotification_giving').innerHTML=''
        }
	<?php } ?>
	<?php if($get_deal_value!="" && ($buy_deal_type=='')&&($get_deal_type!=curr_code)&&($get_deal_type!='cash back') && ($get_deal_type!='discount')){?>
	    var quantity_def_free=0;
        var final_free_button_obj_arr=document.getElementsByClassName("final_free_button");
        for(i=0;i<final_free_button_obj_arr.length;i++){
             quantity_def_free+=parseInt(final_free_button_obj_arr[i].getAttribute("quantity_def_free"));
        }
        var get_deal_value='<?php echo $get_deal_value ?>'
    
    
            //var get_deal_value=parseInt(document.getElementById("get_deal_value").value);
        
            if(get_deal_value!=quantity_def_free){
    
                document.getElementById('inActiveNotification_giving').innerHTML='<div class="col-md-6 col-md-offset-3 alert alert-danger"><strong><?php if($approval_sent!=1){echo "Promotion Cannot Be Sent For Approval !!";}else{echo "Promotion Cannot Be Activated !!";}?>.</strong> Giveaway products units are mismatching</div>'
                
            }
        else{
            document.getElementById('inActiveNotification_giving').innerHTML=''
        }   
	<?php } ?>
if(document.getElementById('summary_for_purchasing_')!=null){

    if(document.getElementById('summary_for_purchasing_').innerHTML.trim()=="No Items Added<br>"){
        document.getElementById('inActiveNotification_purchasing').innerHTML='<div class="col-md-6 col-md-offset-3 alert alert-danger"><strong><?php if($approval_sent!=1){echo "Promotion Cannot Be Sent For Approval !!";}else {echo "Promotion Cannot Be Activated !! ";}?>.</strong> No Products Are Added For Promotion</div>'
            
        }
   else{
          document.getElementById('inActiveNotification_purchasing').innerHTML=''
        }
}
	
	//alert(document.getElementById('disp_deal_end_date_edit_container').innerHTML.trim())
	
	if(document.getElementById('disp_deal_end_date_edit_container').innerHTML.trim()==""){
		document.getElementById('inActiveNotification_enddate').innerHTML='<div class="col-md-6 col-md-offset-3 alert alert-danger"><strong>Promotion Cannot Be Sent For Approval !!.</strong> End Date Is Not Set</div>'
		
	}
	else{
		document.getElementById('inActiveNotification_enddate').innerHTML=''
	}
	
	if(document.getElementById('disp_deal_start_date_edit_container').innerHTML.trim()==""){
		document.getElementById('inActiveNotification_startdate').innerHTML='<div class="col-md-6 col-md-offset-3 alert alert-danger"><strong>Promotion Cannot Be Sent For Approval !!.</strong> Start Date Is Not Set</div>'
		
	}
	else{
		document.getElementById('inActiveNotification_startdate').innerHTML=''
	}
	
	var inActiveNotification_giving = document.getElementById('inActiveNotification_giving').innerHTML
	var inActiveNotification_purchasing = document.getElementById('inActiveNotification_purchasing').innerHTML
	var inActiveNotification_enddate = document.getElementById('inActiveNotification_enddate').innerHTML
	var inActiveNotification_startdate = document.getElementById('inActiveNotification_startdate').innerHTML
	var inActiveNotification_expired = document.getElementById('inActiveNotification_expired').innerHTML
	
	
	

	if(inActiveNotification_expired.trim()!="" ||inActiveNotification_giving.trim()!="" || inActiveNotification_purchasing.trim()!="" || inActiveNotification_enddate.trim()!=""|| inActiveNotification_startdate.trim()!=""){
		if(document.getElementById('approve_promo_button1')!=null){
			document.getElementById('approve_promo_button1').setAttribute('disabled','true')
		}
		if(document.getElementById('approve_promo_button2')!=null){
			document.getElementById('approve_promo_button2').setAttribute('disabled','true')
		}
		if(document.getElementById('activate_promo_button2')!=null){
			document.getElementById('activate_promo_button2').setAttribute('disabled','true')
		}
		if(document.getElementById('activate_promo_button1')!=null){
			document.getElementById('activate_promo_button1').setAttribute('disabled','true')
		}
	}else{
		if(document.getElementById('approve_promo_button1')!=null){
			document.getElementById('approve_promo_button1').removeAttribute("disabled");
		}
		if(document.getElementById('approve_promo_button2')!=null){
			document.getElementById('approve_promo_button2').removeAttribute("disabled");
		}
		if(document.getElementById('activate_promo_button2')!=null){
			document.getElementById('activate_promo_button2').removeAttribute("disabled");
		}
		if(document.getElementById('activate_promo_button1')!=null){
			document.getElementById('activate_promo_button1').removeAttribute("disabled");
		}
		
	}
}

function activate_promotion(obj){
	 var promo_id=obj.getAttribute('promo_id')
  var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
					obj.classList.remove("btn-success");
					obj.classList.add("btn-warning");
					obj.innerHTML='Sending <i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
		                	if(xhr.responseText==1){
		                	document.getElementById('promo_active_status').innerHTML="Active"

							obj.classList.remove("btn-warning");
							obj.classList.add("btn-success");
							obj.innerHTML='Activated'
							
							window.location='<?php echo base_url()?>admin/Promotions/promotion_pending_approval'
		                	}
		                	else{
		                		obj.classList.remove("btn-warning");
								obj.classList.add("btn-success");
								obj.innerHTML='Activated'
								window.location='<?php echo base_url()?>admin/Promotions/promotion_pending_approval'
		                	}
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/activate_promotion", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("promo_id="+promo_id);
		        }
}
function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function send_approval_for_promotion(obj){
  var promo_id=obj.getAttribute('promo_id')
  var su_email = prompt("Approver's Email_id: ", '<?php echo $su_admin_email?>');
    if (!validateEmail(su_email)) {
         alert("Email is incorrect form")
         return false;
    }
  var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
					obj.classList.remove("btn-success");
					obj.classList.add("btn-warning");
					obj.innerHTML='Sending <i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
		                	if(xhr.responseText==1){
		                	document.getElementById('promo_active_status').innerHTML="Active"

							obj.classList.remove("btn-warning");
							obj.classList.add("btn-success");
							obj.innerHTML='Approval Sent'
							
							window.location='<?php echo base_url()?>admin/Promotions/promotion_pending_approval'
		                	}
		                	else{
		                		obj.classList.remove("btn-warning");
								obj.classList.add("btn-success");
								obj.innerHTML='Approval Sent'
								window.location='<?php echo base_url()?>admin/Promotions/promotion_pending_approval'
		                	}
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/send_approval_for_promotion", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("promo_id="+promo_id+"&su_email="+su_email);
		        }
}

function draft_promotion(obj){
		var promo_id=obj.getAttribute('promo_id')
		var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
					obj.classList.remove("btn-success");
					obj.classList.add("btn-warning");
					obj.innerHTML='Updating <i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
		                	document.getElementById('promo_active_status').innerHTML="Draft"
		                	if(xhr.responseText==1){

							obj.classList.remove("btn-warning");
							obj.classList.add("btn-warning");
							obj.innerHTML='Draft'
							window.location='<?php echo base_url()?>admin/Promotions/draft_promotions'
		                	}
		                	else{
		                		obj.classList.remove("btn-warning");
								obj.classList.add("btn-success");
								obj.innerHTML='Draft'
								alert("Draft not saved. Refresh the page and try again")
							//window.location='<?php echo base_url()?>admin/Promotions/draft_promotions'
		                	}
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/save_as_draft_promotion", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("promo_id="+promo_id);
		        }	
}

function get_promotion_summary(){
    if(document.getElementById('summary_for_purchasing_')!=null){
        document.getElementById('summary_for_purchasing_').innerHTML=""

        var domereq=document.createElement('div');
         domereq.innerHTML=document.getElementById('list_container_containing_purchasing_items').innerHTML
        document.getElementById('summary_for_purchasing_').appendChild (domereq)
        var litems=domereq.getElementsByTagName('li')
        var strreq=""
        for(i=0;i<litems.length;i++){
            if(litems[i].childNodes[1]!=null){
                litems[i].childNodes[1].remove()
            }
            strreq+=litems[i].innerHTML+'<br>'
            
        }
        document.getElementById('summary_for_purchasing_').innerHTML=strreq
        domereq.remove()
    }
	var purchasing_dom=document.getElementById('total_items_listed_for_purchasing').innerHTML
	<?php if(!($get_deal_type==curr_code || $get_deal_value==""||$applied_at_invoice==1)){ ?>
	if(document.getElementById('summary_for_giving_')!=null){
	    var giving_dom =document.getElementById('summary_for_giving_')
        giving_dom.innerHTML=""
	}
	
	<?php } ?>
	<?php if($get_deal_value!="" && ($buy_deal_type==curr_code)&&($get_deal_type!=curr_code)){ ?>
	    if(document.getElementById('summary_for_giving_')!=null){
            var giving_dom =document.getElementById('summary_for_giving_')
            giving_dom.innerHTML=""
        }
	    
	    if(document.getElementById('summary_for_giving_')!=null){
    if(document.getElementById('giving_table')==null){
        var givingTAble=document.createElement('table')
        givingTAble.setAttribute('id','giving_table')
        givingTAble.classList.add('table','table-hover','table-bordered')
        givingTAble.setAttribute('style','font-size:.99em;border: 1px solid #ddd;')
        giving_dom.appendChild (givingTAble)
        document.getElementById('giving_table').innerHTML=""
    }
    
    
    
    var givingTable=document.getElementById('list_container_containing_purchasing_items_freeItem')
    for (var i = 0, row; row = givingTable.rows[i]; i++) {
       //iterate through rows
       //rows would be accessed using the "row" variable assigned in the for loop
       if(i!=0){
                    var trelm=document.createElement('tr')
                    trelm.setAttribute('id',i+'tr_giving')
                    trelm.classList.add('info')
                    document.getElementById('giving_table').appendChild(trelm)
            for (var j = 0, col; col = row.cells[j]; j++) {
             //iterate through columns
             //columns would be accessed using the "col" variable assigned in the for loop
var y = Math.floor(Math.random()*100)
             if(j==2){
                    if(row.cells[j].childNodes[0].getAttribute('quantity_def_free')!=0){
    
                        
                        var tdelm1=document.createElement('td')
                        tdelm1.setAttribute('id',j+'tr1_giving')
                        tdelm1.innerHTML=row.cells[j].parentNode.childNodes[0].innerHTML
                        document.getElementById(i+'tr_giving').appendChild(tdelm1)

                        var tdelm2=document.createElement('td')
                        tdelm2.setAttribute('id',j+'tr2_giving'+y)
                        tdelm2.innerHTML=row.cells[j].innerHTML
                        document.getElementById(i+'tr_giving').appendChild(tdelm2)
                        document.getElementById(j+'tr2_giving'+y).childNodes[0].classList.remove('free_button')
                        document.getElementById(j+'tr2_giving'+y).childNodes[0].classList.add('final_free_button')
                    }

                }
             
           }
       }
         
    }
    }
	<?php } ?>
<?php if($get_deal_value!="" && ($buy_deal_type=='')&&($get_deal_type!=curr_code)){?>
    if(document.getElementById('summary_for_giving_')!=null){
            var giving_dom =document.getElementById('summary_for_giving_')
            giving_dom.innerHTML=""
        }
    if(document.getElementById('summary_for_giving_')!=null){
       
    if(document.getElementById('giving_table')==null){
        var givingTAble=document.createElement('table')
        givingTAble.setAttribute('id','giving_table')
        givingTAble.classList.add('table','table-hover','table-bordered')
        givingTAble.setAttribute('style','font-size:.99em;border: 1px solid #ddd;')
        giving_dom.appendChild (givingTAble)
        document.getElementById('giving_table').innerHTML=""
    }
    
    
    
    var givingTable=document.getElementById('list_container_containing_purchasing_items_freeItem')
    for (var i = 0, row; row = givingTable.rows[i]; i++) {
       //iterate through rows
       //rows would be accessed using the "row" variable assigned in the for loop
       if(i!=0){
                    var trelm=document.createElement('tr')
                    trelm.setAttribute('id',i+'tr_giving')
                    trelm.classList.add('info')
                    document.getElementById('giving_table').appendChild(trelm)
            for (var j = 0, col; col = row.cells[j]; j++) {
             //iterate through columns
             //columns would be accessed using the "col" variable assigned in the for loop
var y = Math.floor(Math.random()*100)
             if(j==2){
                    if(row.cells[j].childNodes[0].getAttribute('quantity_def_free')!=0){
    
                        
                        var tdelm1=document.createElement('td')
                        tdelm1.setAttribute('id',j+'tr1_giving')
                        tdelm1.innerHTML=row.cells[j].parentNode.childNodes[0].innerHTML
                        document.getElementById(i+'tr_giving').appendChild(tdelm1)

                        var tdelm2=document.createElement('td')
                        tdelm2.setAttribute('id',j+'tr2_giving'+y)
                        tdelm2.innerHTML=row.cells[j].innerHTML
                        document.getElementById(i+'tr_giving').appendChild(tdelm2)
                        document.getElementById(j+'tr2_giving'+y).childNodes[0].classList.remove('free_button')
                        document.getElementById(j+'tr2_giving'+y).childNodes[0].classList.add('final_free_button')
                    }

                }
             
           }
       }
         
    }
  }
<?php }?>
<?php if($get_deal_value!="" && ($buy_deal_type!='')&&($get_deal_type!=curr_code)){?>
    
    if(document.getElementById('summary_for_giving_')!=null){
            var giving_dom =document.getElementById('summary_for_giving_')
            giving_dom.innerHTML=""
        }
    if(document.getElementById('summary_for_giving_')!=null){
       
    if(document.getElementById('giving_table')==null){
        var givingTAble=document.createElement('table')
        givingTAble.setAttribute('id','giving_table')
        givingTAble.classList.add('table','table-hover','table-bordered')
        givingTAble.setAttribute('style','font-size:.99em;border: 1px solid #ddd;')
        giving_dom.appendChild (givingTAble)
        document.getElementById('giving_table').innerHTML=""
    }
    
    
    
    var givingTable=document.getElementById('list_container_containing_purchasing_items_freeItem')
    for (var i = 0, row; row = givingTable.rows[i]; i++) {
       //iterate through rows
       //rows would be accessed using the "row" variable assigned in the for loop
       if(i!=0){
                    var trelm=document.createElement('tr')
                    trelm.setAttribute('id',i+'tr_giving')
                    trelm.classList.add('info')
                    document.getElementById('giving_table').appendChild(trelm)
            for (var j = 0, col; col = row.cells[j]; j++) {
             //iterate through columns
             //columns would be accessed using the "col" variable assigned in the for loop
var y = Math.floor(Math.random()*100)
             if(j==2){
                    if(row.cells[j].childNodes[0].getAttribute('quantity_def_free')!=0){
    
                        
                        var tdelm1=document.createElement('td')
                        tdelm1.setAttribute('id',j+'tr1_giving')
                        tdelm1.innerHTML=row.cells[j].parentNode.childNodes[0].innerHTML
                        document.getElementById(i+'tr_giving').appendChild(tdelm1)

                        var tdelm2=document.createElement('td')
                        tdelm2.setAttribute('id',j+'tr2_giving'+y)
                        tdelm2.innerHTML=row.cells[j].innerHTML
                        document.getElementById(i+'tr_giving').appendChild(tdelm2)
                        document.getElementById(j+'tr2_giving'+y).childNodes[0].classList.remove('free_button')
                        document.getElementById(j+'tr2_giving'+y).childNodes[0].classList.add('final_free_button')
                    }

                }
             
           }
       }
         
    }
  }
<?php }?>     
	//check_activations()
}
function show_quantity_span_edit(obj){
	var promo_id=obj.getAttribute('promo_id')
	var inv_id=obj.getAttribute('inv_id')
	
	document.getElementById(inv_id+'_free_edit_input_'+promo_id).style.display="inline-block"
	document.getElementById(inv_id+'_free_edit_input_button_'+promo_id).style.display="none"
}
function hide_quantity_span_edit(obj){
	var promo_id=obj.getAttribute('promo_id')
	var inv_id=obj.getAttribute('inv_id')

	document.getElementById(inv_id+'_free_edit_input_'+promo_id).style.display="none"
	document.getElementById(inv_id+'_free_edit_input_button_'+promo_id).style.display=""
}
<?php if(!($get_deal_type==curr_code || $get_deal_value=="")){ ?>
function update_free_units_number(obj){
	var res_proce=1;
	var deactivate_flag=0
	
	var promo_id=obj.getAttribute('promo_uid')
	var inv_id=obj.getAttribute('inv_id')
	var quant_val=document.getElementById(inv_id+'_free_'+promo_id).value
	var tot_free_elem=document.getElementsByClassName('free_button');
	var item_quant_now=0
	for(i=0;i<tot_free_elem.length;i++){
		if(tot_free_elem[i].getAttribute('id',inv_id+'_free_disp_button_'+promo_id)!=inv_id+'_free_disp_button_'+promo_id){
			item_quant_now+=parseInt(tot_free_elem[i].getAttribute('quantity_def_free'))
		}
		
	}

	//alert(item_quant_now)
	item_quant_now+=parseInt(quant_val)
	
	if(document.getElementById(inv_id+'_free_'+promo_id).value!=""){
		if(item_quant_now <= '<?php echo $promo_to_get?>'){
			document.getElementById(inv_id+'_free_error_'+promo_id).innerHTML=""

		}
		else{
			document.getElementById(inv_id+'_free_error_'+promo_id).innerHTML='<small style="color:red">Value Exceeds Offer Unit</small>'
			return false;
		}
	}
	else{
			document.getElementById(inv_id+'_free_error_'+promo_id).innerHTML='<small style="color:red">Value should Not Be Null</small>'
			return false;
		}
	
	<?php if($get_deal_type!='cash back'){ ?>
		
	if(item_quant_now==0){
		<?php if($promo_active==1){ ?>
		
			if(quant_val< <?php echo $promo_to_get?>){
				var result = confirm("Warning! You have edited an active promotion. The total SKU units would be less than the defined deal units. Continuing will first deactivate this promotion and save it in `Reverted Drafts`. You can further action it there.");
				if (result== true) {
				    res_proce=1
				    deactivate_flag=1
				}
				else{
					res_proce=0
					return false;
				}
			}
		<?php } ?>
		  var approval_flag=0
        <?php if($approval_sent==1 && $promo_active==0){?> 
    
        if(quant_val< <?php echo $promo_to_get?>){
            var answer = confirm ("WARNING! This promotion is already sent for approvals. The total SKU units would be less than the defined deal units.This action will cancel the approval request.");
            if (answer){
                res_proce=1
                approval_flag=1
                }
            else{res_proce=0;return false}
        }
        <?php } ?>
	}
	if(item_quant_now!=0){
		<?php if($promo_active==1){ ?>
		
			if(quant_val< <?php echo $promo_to_get?>){
				var result = confirm("Warning! You have edited an active promotion. The total SKU units would be less than the defined deal units. Continuing will first deactivate this promotion and save it in `Reverted Drafts`. You can further action it there.");
				if (result== true) {
				    res_proce=1
				    deactivate_flag=1
				}
				else{
					res_proce=0
					return false;
				}
			}
		<?php } ?>
		 var approval_flag=0
        <?php if($approval_sent==1 && $promo_active==0){?> 
    
        if(quant_val< <?php echo $promo_to_get?>){
            var answer = confirm ("WARNING! This promotion is already sent for approvals. The total SKU units would be less than the defined deal units.This action will cancel the approval request.");
            if (answer){
                res_proce=1
                approval_flag=1
                }
            else{res_proce=0;return false}
        }
        <?php } ?>
	}

	if(document.getElementsByClassName('free_button').length==1){
		<?php if($promo_active==1){ ?>
				var tot_free_elem=document.getElementsByClassName('free_button');
				var item_quant_now=0
				for(i=0;i<tot_free_elem.length;i++){
					if(tot_free_elem[i].getAttribute('id',inv_id+'_free_disp_button_'+promo_id)!=inv_id+'_free_disp_button_'+promo_id){
						item_quant_now+=parseInt(tot_free_elem[i].getAttribute('quantity_def_free'))
					}
				}
				item_quant_now+=parseInt(quant_val)
				//item_quant_now+=parseInt(quant_val)
				if(item_quant_now < <?php echo $promo_to_get?>){
					var result = confirm("Warning! You have edited an active promotion. The total SKU units would be less than the defined deal units. Continuing will first deactivate this promotion and save it in `Reverted Drafts`. You can further action it there.");
						if (result== true) {
						    res_proce=1
						    deactivate_flag=1
						}
						else{
							return false;
							res_proce=0
						}
				}
			<?php } ?>
		var approval_flag=0
        <?php if($approval_sent==1 && $promo_active==0){?>
            
        var tot_free_elem=document.getElementsByClassName('free_button');
                var item_quant_now=0
                for(i=0;i<tot_free_elem.length;i++){
                    if(tot_free_elem[i].getAttribute('id',inv_id+'_free_disp_button_'+promo_id)!=inv_id+'_free_disp_button_'+promo_id){
                        item_quant_now+=parseInt(tot_free_elem[i].getAttribute('quantity_def_free'))
                    }
                }
                item_quant_now+=parseInt(quant_val);
    
        if(item_quant_now< <?php echo $promo_to_get?>){
            var answer = confirm ("WARNING! This promotion is already sent for approvals. The total SKU units would be less than the defined deal units.This action will cancel the approval request.");
            if (answer){
                res_proce=1
                approval_flag=1
                }
            else{res_proce=0;return false}
        }
        <?php } ?>
	}
	if(quant_val!=item_quant_now){
			<?php if($promo_active==1){ ?>
				var tot_free_elem=document.getElementsByClassName('free_button');
				var item_quant_now=0
				for(i=0;i<tot_free_elem.length;i++){
					if(tot_free_elem[i].getAttribute('id',inv_id+'_free_disp_button_'+promo_id)!=inv_id+'_free_disp_button_'+promo_id){
						item_quant_now+=parseInt(tot_free_elem[i].getAttribute('quantity_def_free'))
					}
				}
				item_quant_now+=parseInt(quant_val)
				//item_quant_now+=parseInt(quant_val)
				if(item_quant_now < <?php echo $promo_to_get?>){
					var result = confirm("Warning! You have edited an active promotion. The total SKU units would be less than the defined deal units. Continuing will first deactivate this promotion and save it in `Reverted Drafts`. You can further action it there.");
						if (result== true) {
						    res_proce=1
						    deactivate_flag=1
						}
						else{
							res_proce=0;
							return false;
						}
				}
			<?php } ?>
	}
<?php }?>
	
if(res_proce==1){
	
	
	var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
					obj.classList.remove("btn-success");
					obj.classList.add("btn-warning");
					obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
		                	
		                	if(document.getElementById('approve_promo_button1')==null){
								var top_activate_button_div=document.getElementById('top_right_button_action')
								top_activate_button_div.innerHTML=""
							var top_activate_button=document.createElement('button');
								top_activate_button.innerHTML="Send For Approval"
								top_activate_button.classList.add('btn','btn-success','btn-sm')
								top_activate_button.setAttribute('onclick',"send_approval_for_promotion(this)")
								top_activate_button.setAttribute('promo_id',promo_id)
								top_activate_button.setAttribute('id','approve_promo_button1')
								top_activate_button_div.appendChild (top_activate_button);
							}
							
							if(document.getElementById('approve_promo_button2')==null){
								var bottom_activate_button_div=document.getElementById('bottom_activate_button_div')
								bottom_activate_button_div.innerHTML=""
							var bottom_activate_button=document.createElement('button');
								bottom_activate_button.innerHTML="Send For Approval"
								bottom_activate_button.classList.add('btn','btn-success','btn-lg')
								bottom_activate_button.setAttribute('onclick',"send_approval_for_promotion(this)")
								bottom_activate_button.setAttribute('promo_id',promo_id)
								bottom_activate_button.setAttribute('id','approve_promo_button2')
								bottom_activate_button_div.appendChild (bottom_activate_button);
							}
		                	
		                	
		                	
		                	if(xhr.responseText==1){
		                		var units;
		                		if(quant_val>1){
		                			units=" units"
		                		}
		                		else{
		                			units=" unit"
		                		}
								document.getElementById(inv_id+'_free_disp_button_'+promo_id).innerHTML=quant_val+units;
								document.getElementById(inv_id+'_free_disp_button_'+promo_id).setAttribute('quantity_def_free',quant_val);
		                		obj.classList.remove("btn-warning");
								obj.classList.add("btn-success");
								obj.innerHTML='<i class="fa fa-floppy-o" aria-hidden="true"></i>'
								document.getElementById(inv_id+'_free_edit_input_'+promo_id).style.display="none"
								document.getElementById(inv_id+'_free_edit_input_button_'+promo_id).style.display=""
		                	}
		                	else{
			                	obj.classList.remove("btn-warning");
								obj.classList.add("btn-success");
								obj.innerHTML='<i class="fa fa-floppy-o" aria-hidden="true"></i>'
								document.getElementById(inv_id+'_free_edit_input_'+promo_id).style.display="none"
								document.getElementById(inv_id+'_free_edit_input_button_'+promo_id).style.display=""
		                	}	
		
							check_activations();
							if(deactivate_flag==1){
								
								alert("This promotion is deactivated")
								window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
								
							}
							if(approval_flag==1){
                                        if(count_items_for_purchasing==1){
                                        alert("The approval request is cancelled")
                                        window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
                                     }
                               }
		                }
		                
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/update_promo_free_units_number", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("promo_id="+promo_id+"&inv_id="+inv_id+"&quant_val="+quant_val+"&deactivate_flag="+deactivate_flag+"&approval_flag="+approval_flag);
		        }
	}

}

function delete_this_selected_item_free(obj){
	var promo_id=obj.getAttribute('promo_id')
	var inv_id=obj.getAttribute('inv_id')
	var tot_free_elem=document.getElementsByClassName('free_button');
	var item_quant_now=0
	for(i=0;i<tot_free_elem.length;i++){
		if(tot_free_elem[i].getAttribute('id',inv_id+'_free_disp_button_'+promo_id)!=inv_id+'_free_disp_button_'+promo_id){
			item_quant_now+=parseInt(tot_free_elem[i].getAttribute('quantity_def_free'))
		}
		
	}
	//alert(item_quant_now)
	//item_quant_now+=parseInt(quant_val)
	var res_proce=1;
	<?php if($get_deal_type!="cash back"){?>
	    
	
    	if(item_quant_now < <?php echo $promo_to_get?>){
    		var result = confirm("Are You Sure ! The total SKU units would be less than the defined deal units");
    			if (result== true) {
    			    res_proce=1
    			}
    			else{
    				res_proce=0
    			}
    	}
    	
        var deactivate_flag=0
        var approval_flag=0
        <?php if($promo_active==1){ ?>
        if(item_quant_now < <?php echo $promo_to_get?>){
            var result = confirm("Warning!!, You have edited an active promotion. Continuing will first deactivate this promotion and save it in `Reverted Drafts`. You can further action it there.");
                if (result== true) {
                    res_proce=1
                    deactivate_flag=1
                }
                else{
                    res_proce=0
                }
        }
        <?php } ?>
        <?php if($approval_sent==1 && $promo_active==0){?> 
         if(item_quant_now < <?php echo $promo_to_get?>){
            var result = confirm("Warning!!, Activation request is sent.Continuing this will Cancel");
                if (result== true) {
                    res_proce=1
                    approval_flag=1
                }
                else{
                    res_proce=0
                }
        }
        <?php } ?>
	
	<?php }?>
	
	
	if(res_proce==1){
		var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
					obj.classList.remove("btn-danger");
					obj.classList.add("btn-warning");
					obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
		                	if(xhr.responseText==1){
		                		
		                		document.getElementById(inv_id+'_tr_line_'+promo_id).remove()
								if(document.getElementById('list_container_containing_purchasing_items_freeItem').innerHTML.trim() == ''){
										document.getElementById('list_container_containing_purchasing_items_freeItem').innerHTML='<tr><td>No Items Added</td></tr>';
										
									}
									
								if(deactivate_flag==1){
                                    if(count_items_for_purchasing==0){
                                    alert("This promotion is deactivated")
                                    window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
                                    }
                                }
                                if(approval_flag==1){
                                    if(count_items_for_purchasing==0){
                                    alert("This approval request is cancel")
                                    window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
                                    }
                                }
		                	}
		                	else{
		                		alert("Error in Deleting");
			                	obj.classList.remove("btn-warning");
								obj.classList.add("btn-danger");
								obj.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>'
		                	}	
		
						check_activations()	
		                }
		                
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/delete_this_promo_item_purchasing_free", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("promo_id="+promo_id+"&inv_id="+inv_id+"&deactivate_flag="+deactivate_flag+"&approval_flag="+approval_flag);
		        }
	}
	else{
		return false;
	}
	
		        
}

function update_buy_promo_list_freeItem(){
	if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
		
		var uls = document.getElementById("dual-list-left_freeItem");
			var listItem = uls.getElementsByTagName("li");
			
			var newNums ="";
			var skuIds=[];
			
			for (var i=0; i < listItem.length; i++) {
			    newNums+=(  listItem[i].getAttribute('data-value-name') )+"<br>" ;
			    skuIds.push(  listItem[i].getAttribute('sku-id') ) ;
			}
		document.getElementById('products_tagged_for_purchasing_freeItem').value=skuIds
		document.getElementById('collection_of_all_sku_to_buy_freeItem').innerHTML="";
		var buttonElement=document.createElement ("button")
			buttonElement.innerHTML="Selected "+listItem.length+" SKU"
			buttonElement.type="button"
			buttonElement.classList.add('btn','btn-success','btn-sm');
			buttonElement.setAttribute('data-popover','true')
			buttonElement.setAttribute('data-html',true)
			buttonElement.setAttribute('data-content',newNums)
			//document.getElementById('collection_of_all_sku_to_buy_freeItem').appendChild (buttonElement);
			var buttonSubmit=document.createElement ("button")
			buttonSubmit.innerHTML="Add Items Taged For Free"
			buttonSubmit.type="submit";
			buttonSubmit.setAttribute("style","margin-left:10px");
			buttonSubmit.classList.add('btn','btn-info');
			
			document.getElementById('collection_of_all_sku_to_buy_freeItem').appendChild (buttonSubmit);
	}
	else{
		document.getElementById('collection_of_all_sku_to_buy_freeItem').innerHTML="Nothing Selected"
	}
}

    function remove_category_freeItem(){
        document.getElementById('parent_category_selector_freeItem').value=""
        document.getElementById('CategoryContainer_freeItem').innerHTML=""
        document.getElementById('parentCatSel_freeItem').remove();
            if(document.getElementById('subCatSel_freeItem')!=null){
                document.getElementById('subCatSel_freeItem').remove();
            }
            if(document.getElementById('brand_sel_freeItem')!=null){
                document.getElementById('brand_sel_freeItem').remove();
            }
            if(document.getElementById('attribute_sel_freeItem')!=null){
                document.getElementById('attribute_sel_freeItem').remove();
            }
            if(document.getElementById('product_sel_freeItem')!=null){
                document.getElementById('product_sel_freeItem').remove();
            }

            var parentDiv = document.getElementById('subCategoryContainer_freeItem');
            parentDiv.innerHTML=""
            if(document.getElementById('brandContainer_freeItem').innerHTML!=null){
                document.getElementById('brandContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('attributeContainer_freeItem').innerHTML!=null){
                document.getElementById('attributeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productContainer_freeItem').innerHTML!=null){
                document.getElementById('productContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
                document.getElementById('productTypeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
                document.getElementById('dual-list-right_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
                document.getElementById('dual-list-left_freeItem').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
                document.getElementById('product_filter_level_selector_freeItem').style.display="none"
            }
            if(document.getElementById('collection_of_all_sku_to_buy_freeItem')!=null){
                document.getElementById('collection_of_all_sku_to_buy_freeItem').innerHTML="";
            }
    }

	function remove_subcategory_freeItem(){
			document.getElementById('category_selector_freeItem').value=""
			document.getElementById('subCatSel_freeItem').remove();
			if(document.getElementById('brand_sel_freeItem')!=null){
				document.getElementById('brand_sel_freeItem').remove();
			}
			if(document.getElementById('attribute_sel_freeItem')!=null){
				document.getElementById('attribute_sel_freeItem').remove();
			}
			if(document.getElementById('product_sel_freeItem')!=null){
				document.getElementById('product_sel_freeItem').remove();
			}

			var parentDiv = document.getElementById('subCategoryContainer_freeItem');
			parentDiv.innerHTML=""
			if(document.getElementById('brandContainer_freeItem').innerHTML!=null){
				document.getElementById('brandContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('attributeContainer_freeItem').innerHTML!=null){
				document.getElementById('attributeContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('productContainer_freeItem').innerHTML!=null){
				document.getElementById('productContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
				document.getElementById('productTypeContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
				document.getElementById('dual-list-right_freeItem').innerHTML=""
			}
			if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
				document.getElementById('dual-list-left_freeItem').innerHTML=""
				update_buy_promo_list()
			}
			if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
				document.getElementById('product_filter_level_selector_freeItem').style.display="none"
			}
			if(document.getElementById('collection_of_all_sku_to_buy_freeItem')!=null){
                document.getElementById('collection_of_all_sku_to_buy_freeItem').innerHTML="";
            }
			update_buy_promo_list_freeItem()
	}
	function remove_brand_freeItem(){
		document.getElementById('sub_category_selector_freeItem').value=""
			document.getElementById('brand_sel_freeItem').remove();

			if(document.getElementById('attribute_sel_freeItem')!=null){
				document.getElementById('attribute_sel_freeItem').remove();
			}
			if(document.getElementById('product_sel_freeItem')!=null){
				document.getElementById('product_sel_freeItem').remove();
			}

			if(document.getElementById('brandContainer_freeItem').innerHTML!=null){
				document.getElementById('brandContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('attributeContainer_freeItem').innerHTML!=null){
				document.getElementById('attributeContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('productContainer_freeItem').innerHTML!=null){
				document.getElementById('productContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
				document.getElementById('productTypeContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
				document.getElementById('dual-list-right_freeItem').innerHTML=""
			}
			if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
				document.getElementById('dual-list-left_freeItem').innerHTML=""
				update_buy_promo_list()
			}
			if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
				document.getElementById('product_filter_level_selector_freeItem').style.display="none"
			}
			if(document.getElementById('collection_of_all_sku_to_buy_freeItem')!=null){
                document.getElementById('collection_of_all_sku_to_buy_freeItem').innerHTML="";
            }
			update_buy_promo_list_freeItem()
		
	}
	function remove_product_freeItem(){
		document.getElementById('attribute_selector_freeItem').value=""
			document.getElementById('attribute_sel_freeItem').remove();

			if(document.getElementById('product_sel_freeItem')!=null){
				document.getElementById('product_sel_freeItem').remove;
			}

			if(document.getElementById('attributeContainer_freeItem').innerHTML!=null){
				document.getElementById('attributeContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('productContainer_freeItem').innerHTML!=null){
				document.getElementById('productContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
				document.getElementById('productTypeContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
				document.getElementById('dual-list-right_freeItem').innerHTML=""
			}
			if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
				document.getElementById('dual-list-left_freeItem').innerHTML=""
				update_buy_promo_list()
			}
			if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
				document.getElementById('product_filter_level_selector_freeItem').style.display="none"
			}
			if(document.getElementById('collection_of_all_sku_to_buy_freeItem')!=null){
                document.getElementById('collection_of_all_sku_to_buy_freeItem').innerHTML="";
            }
			update_buy_promo_list_freeItem()
	}
	function remove_product_variant_freeItem(){
		document.getElementById('product_selector_freeItem').value=""
			document.getElementById('product_sel_freeItem').remove();


			if(document.getElementById('productContainer_freeItem').innerHTML!=null){
				document.getElementById('productContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
				document.getElementById('productTypeContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
				document.getElementById('dual-list-right_freeItem').innerHTML=""
			}
			if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
				document.getElementById('dual-list-left_freeItem').innerHTML=""
				update_buy_promo_list()
			}
			if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
				document.getElementById('product_filter_level_selector_freeItem').style.display="none"
			}
			if(document.getElementById('collection_of_all_sku_to_buy_freeItem')!=null){
                document.getElementById('collection_of_all_sku_to_buy_freeItem').innerHTML="";
            }
			update_buy_promo_list_freeItem()
	}

        function get_category_freeItem(){
        var parentCatval=document.getElementById('parent_category_selector_freeItem').value
        var x = document.getElementById("parent_category_selector_freeItem");
        var parentCatText=x.options[x.selectedIndex].text
        if(document.getElementById('parentCatSel_freeItem')!=null){
            remove_category_freeItem()
        }
        document.getElementById('parent_category_selector_freeItem').value=parentCatval
        document.getElementById("parent_category_selector_freeItem").options[x.selectedIndex].text=parentCatText
            var parentDiv = document.getElementById('CategoryContainer_freeItem');
            parentDiv.innerHTML=""
            if(document.getElementById('subCategoryContainer_freeItem').innerHTML!=null){
                document.getElementById('subCategoryContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('brandContainer_freeItem').innerHTML!=null){
                document.getElementById('brandContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('attributeContainer_freeItem').innerHTML!=null){
                document.getElementById('attributeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productContainer_freeItem').innerHTML!=null){
                document.getElementById('productContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
                document.getElementById('productTypeContainer_freeItem').innerHTML=""
            }
            if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
                document.getElementById('dual-list-right_freeItem').innerHTML=""
            }
            
            if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
                document.getElementById('dual-list-left_freeItem').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
                document.getElementById('product_filter_level_selector_freeItem').style.display="none"
            }

        if(parentCatval!=""){         
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                    var select_data=JSON.parse(xhr.responseText);
                                    /*var labelElement=document.createElement ("label")
                                    labelElement.classList.add('control-label','col-md-2');
                                    labelElement.innerHTML="Category:"
                                    parentDiv.appendChild (labelElement);*/
                                    
                                    if(select_data.length>0){                                   
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-10');
                                        divElement.setAttribute('id','ParentCategorySelect_freeItem')
                                        parentDiv.appendChild (divElement);
                                        
                                        var selectDiv=document.getElementById('ParentCategorySelect_freeItem');
                                        var selectElement = document.createElement ("select");
                                        selectElement.classList.add("form-control");
                                        selectElement.setAttribute('id','category_selector_freeItem');
                                        selectElement.setAttribute('onchange','get_subcategory_freeItem()');
                                        selectElement.name="category";
                                        var option = new Option ("Choose Category","");
                                        selectElement.options[selectElement.options.length] = option;
                                        for (var i=0;i < select_data.length;i++)
                                        {
                                            
                                            var option = new Option (select_data[i].cat_name,select_data[i].cat_id);
                                            selectElement.options[selectElement.options.length] = option;
                                        }
                                        selectDiv.appendChild (selectElement);
                                        if(document.getElementById('parentCatSel_freeItem')==null){
                                            var list_stack_disp=document.getElementById('item_picker_del_freeItem')
                                            var list_level_span = document.createElement ("li");
                                            list_level_span.innerHTML=parentCatText +'&nbsp';
                                            list_level_span.setAttribute('id',"parentCatSel_freeItem");
                                            list_level_span.setAttribute("selector_li_name",parentCatText)
                                            list_stack_disp.appendChild (list_level_span);
                                            var btncon=document.getElementById("parentCatSel_freeItem")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',"parentCatSel_freeItem"+parentCatval);
                                            list_level_button.setAttribute('onclick','remove_category_freeItem()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                        }
                                        else{
                                            document.getElementById('parentCatSel').innerHTML=parentCatText
                                            document.getElementById('parentCatSel').setAttribute("selector_li_name",parentCatText)
                                            var btncon=document.getElementById("parentCatSel")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',"parentCatSel_freeItem"+parentCatval);
                                            list_level_button.setAttribute('onclick','remove_category_freeItem()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                        }       
                                      }
                                     else{
                                        if(document.getElementById('parentCatSel_freeItem')){
                                            document.getElementById('parentCatSel_freeItem').remove();
                                        }
                                        if(document.getElementById('subCatSel_freeItem')){
                                            document.getElementById('subCatSel_freeItem').remove();
                                        }
                                        if(document.getElementById('brand_sel_freeItem')){
                                            document.getElementById('brand_sel_freeItem').remove();
                                        }
                                        if(document.getElementById('brand_sel_freeItem')){
                                            document.getElementById('brand_sel_freeItem').remove();
                                        }
                                        if(document.getElementById('attribute_sel_freeItem')){
                                            document.getElementById('attribute_sel_freeItem').remove();
                                        }
                                        if(document.getElementById('product_sel_freeItem')){
                                            document.getElementById('product_sel_freeItem').remove();
                                        }
                                        var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-10');
                                        labelElement.setAttribute('style','text-align: left')
                                        labelElement.innerHTML="No products to show"
                                        parentDiv.appendChild (labelElement);
                                    } 
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_category", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send("pcat_id="+parentCatval);
                }
        }
    }
	
	function get_subcategory_freeItem(){
		
		var catval=document.getElementById('category_selector_freeItem').value
		var x = document.getElementById("category_selector_freeItem");
		var catText=x.options[x.selectedIndex].text
		if(document.getElementById('subCatSel_freeItem')!=null){
			remove_subcategory_freeItem()
		}
		document.getElementById('category_selector_freeItem').value=catval
		document.getElementById("category_selector_freeItem").options[x.selectedIndex].text=catText
			var parentDiv = document.getElementById('subCategoryContainer_freeItem');
			parentDiv.innerHTML=""
			if(document.getElementById('brandContainer_freeItem').innerHTML!=null){
				document.getElementById('brandContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('attributeContainer_freeItem').innerHTML!=null){
				document.getElementById('attributeContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('productContainer_freeItem').innerHTML!=null){
				document.getElementById('productContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
				document.getElementById('productTypeContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
				document.getElementById('dual-list-right_freeItem').innerHTML=""
			}
			
			if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
				document.getElementById('dual-list-left_freeItem').innerHTML=""
				update_buy_promo_list()
			}
			if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
				document.getElementById('product_filter_level_selector_freeItem').style.display="none"
			}

		if(catval!=""){			
		var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
									var select_data=JSON.parse(xhr.responseText);
		                  			/*var labelElement=document.createElement ("label")
									labelElement.classList.add('control-label','col-md-2');
									labelElement.innerHTML="Sub Category:"
									parentDiv.appendChild (labelElement);*/
									
									if(select_data.length>0){
    									var divElement=document.createElement ("div")
    									divElement.classList.add('col-md-10');
    									divElement.setAttribute('id','subCategorySelect_freeItem')
    									parentDiv.appendChild (divElement);
    									
    									var selectDiv=document.getElementById('subCategorySelect_freeItem');
    									var selectElement = document.createElement ("select");
    									selectElement.classList.add("form-control");
    									selectElement.setAttribute('id','sub_category_selector_freeItem');
    									selectElement.setAttribute('onchange','get_brands_freeItem()');
    									selectElement.name="sub_category";
    									var option = new Option ("Choose Sub Category","");
    									selectElement.options[selectElement.options.length] = option;
    									for (var i=0;i < select_data.length;i++)
    									{
    										
    									    var option = new Option (select_data[i].subcat_name,select_data[i].subcat_id);
    									    selectElement.options[selectElement.options.length] = option;
    									}
    									selectDiv.appendChild (selectElement);
    									if(document.getElementById('subCatSel_freeItem')==null){
    										var list_stack_disp=document.getElementById('item_picker_del_freeItem')
    										var list_level_span = document.createElement ("li");
    										list_level_span.innerHTML=catText +'&nbsp';
    										list_level_span.setAttribute('id',"subCatSel_freeItem");
    										list_level_span.setAttribute("selector_li_name",catText)
    										list_stack_disp.appendChild (list_level_span);
    										var btncon=document.getElementById("subCatSel_freeItem")
    										var list_level_button=document.createElement ("button");
    										list_level_button.type="button";
    										list_level_button.setAttribute('id',"subCatSel_freeItem"+catval);
    										list_level_button.setAttribute('onclick','remove_subcategory_freeItem()');
    										list_level_button.classList.add("material-button","btn-danger");
    										list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
    										btncon.appendChild (list_level_button);
    									}
    									else{
    										document.getElementById('subCatSel_freeItem').innerHTML=catText
    										document.getElementById('subCatSel_freeItem').setAttribute("selector_li_name",catText)
    										var btncon=document.getElementById("subCatSel_freeItem")
    										var list_level_button=document.createElement ("button");
    										list_level_button.type="button";
    										list_level_button.setAttribute('id',"subCatSel_freeItem"+catval);
    										list_level_button.setAttribute('onclick','remove_subcategory_freeItem()');
    										list_level_button.classList.add("material-button","btn-danger");
    										list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
    										btncon.appendChild (list_level_button);
    									}
    								}
    								else{
    								    if(document.getElementById('subCatSel_freeItem')){
    								        document.getElementById('subCatSel_freeItem').remove();
    								    }
    								    if(document.getElementById('brand_sel_freeItem')){
                                            document.getElementById('brand_sel_freeItem').remove();
                                        }
                                        if(document.getElementById('brand_sel_freeItem')){
                                            document.getElementById('brand_sel_freeItem').remove();
                                        }
                                        if(document.getElementById('attribute_sel_freeItem')){
                                            document.getElementById('attribute_sel_freeItem').remove();
                                        }
                                        if(document.getElementById('product_sel_freeItem')){
                                            document.getElementById('product_sel_freeItem').remove();
                                        }
                                        var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-10');
                                        labelElement.setAttribute('style','text-align: left')
                                        labelElement.innerHTML="No products to show"
                                        parentDiv.appendChild (labelElement);
                                    }		
										
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_sub_catageory", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("cat_id="+catval);
		        }
		}
	}
	
	function get_brands_freeItem(){
		var sub_cat_val=document.getElementById('sub_category_selector_freeItem').value
		var x = document.getElementById("sub_category_selector_freeItem");
		var sub_catText=x.options[x.selectedIndex].text
		var parentDiv = document.getElementById('brandContainer_freeItem');
			parentDiv.innerHTML=""

			if(document.getElementById('attributeContainer_freeItem').innerHTML!=null){
				document.getElementById('attributeContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('productContainer_freeItem').innerHTML!=null){
				document.getElementById('productContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
				document.getElementById('productTypeContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
				document.getElementById('dual-list-right_freeItem').innerHTML=""
			}
			if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
				document.getElementById('dual-list-left_freeItem').innerHTML=""
				update_buy_promo_list()
			}
			if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
				document.getElementById('product_filter_level_selector_freeItem').style.display="none"
			}				
		if(sub_cat_val!=""){			
		var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
									var select_data=JSON.parse(xhr.responseText);
									/*var labelElement=document.createElement ("label")
										labelElement.classList.add('control-label','col-md-2');
										labelElement.innerHTML="Select Brand:"
										parentDiv.appendChild (labelElement);*/
										
										
										
									if(select_data.length>0){
										var divElement=document.createElement ("div")
										divElement.classList.add('col-md-10');
										divElement.setAttribute('id','brandSelect_freeItem')
										parentDiv.appendChild (divElement);
										
										var selectDiv=document.getElementById('brandSelect_freeItem');
										var selectElement = document.createElement ("select");
										selectElement.classList.add("form-control");
										selectElement.setAttribute('id','attribute_selector_freeItem');
										selectElement.setAttribute('onchange','get_all_attributes_freeItem()');
										selectElement.name="attributes";
										var option = new Option ("Choose Brand","");
										selectElement.options[selectElement.options.length] = option;
										for (var i=0;i < select_data.length;i++)
										{
										    var option = new Option (select_data[i].brand_name,select_data[i].brand_id);
										    selectElement.options[selectElement.options.length] = option;
										}
										selectDiv.appendChild (selectElement);

										if(document.getElementById('brand_sel_freeItem')==null){
											var list_stack_disp=document.getElementById('item_picker_del_freeItem')
											var list_level_span = document.createElement ("li");
											list_level_span.innerHTML=sub_catText +'&nbsp';
											list_level_span.setAttribute('id',"brand_sel_freeItem");
											list_level_span.setAttribute('selector_li_name',sub_catText);
											list_stack_disp.appendChild (list_level_span);
											var btncon=document.getElementById("brand_sel_freeItem")
											var list_level_button=document.createElement ("button");
											list_level_button.type="button";
											list_level_button.setAttribute('id',"brand_sel_freeItem"+sub_cat_val);
											list_level_button.setAttribute('onclick','remove_brand_freeItem()');
											list_level_button.classList.add("material-button","btn-danger");
											list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
											btncon.appendChild (list_level_button);
											
											if(document.getElementById('attribute_sel_freeItem')){
											document.getElementById('attribute_sel_freeItem').remove();
											}
											if(document.getElementById('product_sel_freeItem')){
												document.getElementById('product_sel_freeItem').remove();
											}
										}
										else{
											document.getElementById('brand_sel_freeItem').innerHTML=sub_catText;
											document.getElementById('brand_sel_freeItem').setAttribute('selector_li_name',sub_catText);
											var btncon=document.getElementById("brand_sel_freeItem")
											var list_level_button=document.createElement ("button");
											list_level_button.type="button";
											list_level_button.setAttribute('id',"brand_sel_freeItem"+sub_cat_val);
											list_level_button.setAttribute('onclick','remove_brand_freeItem()');
											list_level_button.classList.add("material-button","btn-danger");
											list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
											btncon.appendChild (list_level_button);
											if(document.getElementById('attribute_sel_freeItem')){
											document.getElementById('attribute_sel_freeItem').remove();
											}
											if(document.getElementById('product_sel_freeItem')){
												document.getElementById('product_sel_freeItem').remove();
											}
										}
										
									}
									else{
										if(document.getElementById('brand_sel_freeItem')){
											document.getElementById('brand_sel_freeItem').remove();
										}
										if(document.getElementById('attribute_sel_freeItem')){
											document.getElementById('attribute_sel_freeItem').remove();
										}
										if(document.getElementById('product_sel_freeItem')){
											document.getElementById('product_sel_freeItem').remove();
										}
										var labelElement=document.createElement ("label")
										labelElement.classList.add('control-label','col-md-10');
										labelElement.setAttribute('style','text-align: left')
										labelElement.innerHTML="No products to show"
										parentDiv.appendChild (labelElement);
									}
		                  			
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_brand", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("sub_cat_val="+sub_cat_val);
		        }
		}
	}
	
function get_all_attributes_freeItem(){
	var brands_val=document.getElementById('attribute_selector_freeItem').value
	var x = document.getElementById("attribute_selector_freeItem");
		var brands_Text=x.options[x.selectedIndex].text
		var parentDiv = document.getElementById('attributeContainer_freeItem');
			parentDiv.innerHTML=""

			if(document.getElementById('productContainer_freeItem').innerHTML!=null){
				document.getElementById('productContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('productTypeContainer_freeItem').innerHTML!=null){
				document.getElementById('productTypeContainer_freeItem').innerHTML=""
			}
			if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
				document.getElementById('dual-list-right_freeItem').innerHTML=""
			}
			if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
				document.getElementById('dual-list-left_freeItem').innerHTML=""
				update_buy_promo_list()
			}
			if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
				document.getElementById('product_filter_level_selector_freeItem').style.display="none"
			}					
		if(brands_val!=""){			
		var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
									var select_data=JSON.parse(xhr.responseText);
									/*var labelElement=document.createElement ("label")
										labelElement.classList.add('control-label','col-md-2');
										labelElement.innerHTML="Select Product:"
										parentDiv.appendChild (labelElement);*/
										
										
										
									if(select_data.length>0){
										var divElement=document.createElement ("div")
										divElement.classList.add('col-md-10');
										divElement.setAttribute('id','attributeSelect_freeItem')
										parentDiv.appendChild (divElement);
										
										var selectDiv=document.getElementById('attributeSelect_freeItem');
										var selectElement = document.createElement ("select");
										selectElement.classList.add("form-control");
										selectElement.setAttribute('id','product_selector_freeItem');
										selectElement.setAttribute('onchange','get_all_items_freeItem()');
										selectElement.name="attributes_selector";
										var option = new Option ("Choose Product","");
										selectElement.options[selectElement.options.length] = option;
										for (var i=0;i < select_data.length;i++)
										{
										    var option = new Option (select_data[i].product_name,select_data[i].product_id);
										    selectElement.options[selectElement.options.length] = option;
										}
										selectDiv.appendChild (selectElement);
										
										if(document.getElementById('attribute_sel_freeItem')==null){
											var list_stack_disp=document.getElementById('item_picker_del_freeItem')
											var list_level_span = document.createElement ("li");
											list_level_span.innerHTML=brands_Text +'&nbsp';
											list_level_span.setAttribute('id',"attribute_sel_freeItem");
											list_level_span.setAttribute('selector_li_name',brands_Text);
											list_stack_disp.appendChild (list_level_span);
											var btncon=document.getElementById("attribute_sel_freeItem")
											var list_level_button=document.createElement ("button");
											list_level_button.type="button";
											list_level_button.setAttribute('id',brands_val+"attribute_sel_freeItem");
											list_level_button.setAttribute('onclick','remove_product_freeItem()');
											list_level_button.classList.add("material-button","btn-danger");
											list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
											btncon.appendChild (list_level_button);
											if(document.getElementById('product_sel_freeItem')){
												document.getElementById('product_sel_freeItem').remove();
											}
										}
										else{
											document.getElementById('attribute_sel_freeItem').innerHTML=brands_Text
											document.getElementById('attribute_sel_freeItem').setAttribute('selector_li_name',brands_Text);
											var btncon=document.getElementById("attribute_sel_freeItem")
											var list_level_button=document.createElement ("button");
											list_level_button.type="button";
											list_level_button.setAttribute('id',brands_val+"attribute_sel_freeItem");
											list_level_button.setAttribute('onclick','remove_product_freeItem()');
											list_level_button.classList.add("material-button","btn-danger");
											list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
											btncon.appendChild (list_level_button);
											if(document.getElementById('product_sel_freeItem')){
												document.getElementById('product_sel_freeItem').remove();
											}
										}
										
									}
									else{
										if(document.getElementById('attribute_sel_freeItem')){
											document.getElementById('attribute_sel_freeItem').remove();
										}
										if(document.getElementById('product_sel_freeItem')){
											document.getElementById('product_sel_freeItem').remove();
										}
										var labelElement=document.createElement ("label")
										labelElement.classList.add('control-label','col-md-10');
										labelElement.setAttribute('style','text-align: left')
										labelElement.innerHTML="No products to show"
										parentDiv.appendChild (labelElement);
									}
		                  			
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_attributes", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("brands_val="+brands_val);
		        }
		}
}	

function get_all_items_freeItem(){
		var product_val=document.getElementById('product_selector_freeItem').value
		var x = document.getElementById("product_selector_freeItem");
		var product_Text=x.options[x.selectedIndex].text
		var parentDiv = document.getElementById('productContainer_freeItem');
		parentDiv.innerHTML=""

		if(document.getElementById('dual-list-right_freeItem').innerHTML!=null){
			document.getElementById('dual-list-right_freeItem').innerHTML=""
		}
		if(document.getElementById('dual-list-left_freeItem').hasChildNodes()){
			document.getElementById('dual-list-left_freeItem').innerHTML=""
			update_buy_promo_list()
		}
		if(document.getElementById('product_filter_level_selector_freeItem').innerHTML!=null){
			document.getElementById('product_filter_level_selector_freeItem').style.display="none"
		}		
		if(product_val!=""){			
		var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
									var select_data=JSON.parse(xhr.responseText);
									document.getElementById('dual-list-left_freeItem').innerHTML=""
									document.getElementById('dual-list-right_freeItem').innerHTML=""
										
									if(select_data.length>0){
										document.getElementById('product_filter_level_selector_freeItem').style.display=""
										
										var liDiv=document.getElementById('dual-list-right_freeItem');
										document.getElementById('dual-list-left_freeItem').innerHTML=""
										document.getElementById('dual-list-right_freeItem').innerHTML=""
										for (var i=0;i < select_data.length;i++)
										{
											var val=select_data[i].attribute_1_value.split(':');
											var liElement = document.createElement ("li");
											liElement.innerHTML=select_data[i].sku_id+'('+select_data[i].attribute_1+'='+val[0]+', '+select_data[i].attribute_2+'='+select_data[i].attribute_2_value+')';
											liElement.classList.add("list-group-item");
											liElement.setAttribute('data-value-name',select_data[i].sku_id+'('+select_data[i].attribute_1+'='+select_data[i].attribute_1_value+', '+select_data[i].attribute_2+'='+select_data[i].attribute_2_value+')');
											liElement.setAttribute('sku-id',select_data[i].id);
											liElement.setAttribute('data-value',select_data[i].id);
											liElement.name="item_typs";
											liDiv.appendChild (liElement);
										}
										activate_li_freeItem();
										populate_attribute_filter_freeItem(product_val);
										
										if(document.getElementById('product_sel_freeItem')==null){
											if(product_Text.length > 10){
											product_Texts = product_Text.substring(0,10)+"...";
											}
											else{
												product_Texts=product_Text;
											}
											
											var list_stack_disp=document.getElementById('item_picker_del_freeItem')
											var list_level_span = document.createElement ("li");
											list_level_span.innerHTML=product_Texts +'&nbsp';
											list_level_span.setAttribute('id',"product_sel_freeItem");
											list_level_span.setAttribute('selector_li_name',product_Text);
											list_stack_disp.appendChild (list_level_span);
											var btncon=document.getElementById("product_sel_freeItem")
											var list_level_button=document.createElement ("button");
											list_level_button.type="button";
											list_level_button.setAttribute('id',product_val+"_product_sel_freeItem");
											list_level_button.setAttribute('onclick','remove_product_variant_freeItem()');
											list_level_button.classList.add("material-button","btn-danger");
											list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
											btncon.appendChild (list_level_button);
										}
										else{
											if(product_Text.length > 10){
											product_Texts = product_Text.substring(0,10)+"...";
											}
											document.getElementById('product_sel_freeItem').innerHTML=product_Texts
											document.getElementById('product_sel_freeItem').setAttribute('selector_li_name',product_Text)
											var btncon=document.getElementById("product_sel_freeItem")
											var list_level_button=document.createElement ("button");
											list_level_button.type="button";
											list_level_button.setAttribute('id',product_val+"_product_sel_freeItem");
											list_level_button.setAttribute('onclick','remove_product_variant_freeItem()');
											list_level_button.classList.add("material-button","btn-danger");
											list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
											btncon.appendChild (list_level_button);
										}
										
										
										
									}
									else{
										if(document.getElementById('product_sel_freeItem')){
											document.getElementById('product_sel_freeItem').remove();
										}
										document.getElementById('product_filter_level_selector_freeItem').style.display="none"
										var selectDiv=document.getElementById('attributeSelect_freeItem');
										var labelElement=document.createElement ("label")
										labelElement.classList.add('control-label','col-md-10');
										labelElement.setAttribute('style','text-align: left')
										labelElement.innerHTML="No products to show"
										selectDiv.appendChild (labelElement);
									}
		                  			
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_items_in_type", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("product_val="+product_val);
		        }
		}
}

function populate_attribute_filter_freeItem(product_val){
	var productTypeContainer =document.getElementById('productTypeContainer_freeItem')
		productTypeContainer.innerHTML=""
		
		var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
									var select_data=JSON.parse(xhr.responseText);										
									//alert(select_data.attr1.length);
									//alert(select_data.attr2.length);
									if(select_data.attr1.length>0){
										var labelElement=document.createElement ("div")
										labelElement.classList.add('col-md-12');
										labelElement.innerHTML="Filter "+select_data.attribute_1+" :";
										productTypeContainer.appendChild (labelElement);
										var divElement=document.createElement ("div")
										divElement.classList.add('col-md-12');
										divElement.setAttribute('id','attributeColorSelect_freeItem')
										productTypeContainer.appendChild (divElement);
										var checkDiv=document.getElementById('attributeColorSelect_freeItem');
									
										
										for (var i=0;i < select_data.attr1.length;i++){
											var val=select_data.attr1[i].split(':');
											
											var labelElement=document.createElement ("label")
											labelElement.classList.add('checkbox-inline');
											labelElement.setAttribute('id','id_freeItem'+select_data.attr1[i]);
											checkDiv.appendChild (labelElement);
											
											var labelDiv=document.getElementById('id_freeItem'+select_data.attr1[i]);
											var checkbox = document.createElement('input');
											checkbox.type = "checkbox";
											checkbox.name = "filter_color";
											checkbox.setAttribute("onclick","applyToFilter_freeItem("+product_val+")");
											checkbox.value = select_data.attr1[i];
											checkbox.id = "id_freeItem"+select_data.attr1[i];
											
											var label = document.createElement('label')
											label.htmlFor = "id_freeItem"+select_data.attr1[i];
											label.appendChild(document.createTextNode(val[0]));
											
											labelDiv.appendChild(checkbox);
											labelDiv.appendChild(label);
										}
										
									}
									if(select_data.attr2!=null){
									if(select_data.attr2.length>0){
										var labelElement=document.createElement ("div")
										labelElement.classList.add('col-md-12');
										labelElement.innerHTML="Filter "+select_data.attribute_2+" :";
										productTypeContainer.appendChild (labelElement);
										var divElement=document.createElement ("div")
										divElement.classList.add('col-md-12');
										divElement.setAttribute('id','attributeSizeSelect_freeItem')
										productTypeContainer.appendChild (divElement);
										var checkDiv=document.getElementById('attributeSizeSelect_freeItem');
									
										
										for (var i=0;i < select_data.attr2.length;i++){
											var labelElement=document.createElement ("label")
											labelElement.classList.add('checkbox-inline');
											labelElement.setAttribute('id','id_freeItem'+select_data.attr2[i]);
											checkDiv.appendChild (labelElement);
											
											var labelDiv=document.getElementById('id_freeItem'+select_data.attr2[i]);
											var checkbox = document.createElement('input');
											checkbox.type = "checkbox";
											checkbox.name = "filter_size";
											checkbox.setAttribute("onclick","applyToFilter_freeItem("+product_val+")");
											checkbox.value = select_data.attr2[i];
											checkbox.id = "id_"+select_data.attr2[i];
											
											var label = document.createElement('label')
											label.htmlFor = "id_"+select_data.attr2[i];
											label.appendChild(document.createTextNode(select_data.attr2[i]));
											
											labelDiv.appendChild(checkbox);
											labelDiv.appendChild(label);
										}
										countLeftlist_freeItem()
									}
									}
									
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_items_in_type_filter", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("product_val="+product_val);
		        }
}

function applyToFilter_freeItem(product_val){
	update_buy_promo_list()
	var colorelem=document.getElementsByName('filter_color')
	var colorelemarr = []
	for(i=0;i<colorelem.length;i++){
		if(colorelem[i].checked){
			colorelemarr.push(colorelem[i].value);
		}
	}
	var color=colorelemarr.join("-");
	var sizeelem=document.getElementsByName('filter_size')
	var sizeelemarr = []
	for(i=0;i<sizeelem.length;i++){
		if(sizeelem[i].checked){
			sizeelemarr.push(sizeelem[i].value);
		}
	}
	var size=sizeelemarr.join("-");
	
	var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
									var select_data=JSON.parse(xhr.responseText);
									var liDiv=document.getElementById('dual-list-right_freeItem');
									liDiv.innerHTML=""
									document.getElementById('dual-list-left_freeItem').innerHTML=""
										//alert(xhr.responseText);
									if(select_data.length>0){
										document.getElementById('product_filter_level_selector_freeItem').style.display=""
										
										var liDiv=document.getElementById('dual-list-right_freeItem');
										for (var i=0;i < select_data.length;i++)
										{
										    var val=select_data[i].attribute_1_value.split(':');
											var liElement = document.createElement ("li");
											liElement.innerHTML=select_data[i].sku_id+'('+select_data[i].attribute_1+'='+val[0]+', '+select_data[i].attribute_2+'='+select_data[i].attribute_2_value+')';
											liElement.classList.add("list-group-item");
											liElement.setAttribute('data-value-name',select_data[i].sku_id+'('+select_data[i].attribute_1+'='+select_data[i].attribute_1_value+', '+select_data[i].attribute_2+'='+select_data[i].attribute_2_value+')');
											liElement.setAttribute('sku-id',select_data[i].id);
											liElement.setAttribute('data-value',select_data[i].id);
											liElement.name="item_typs";
											liDiv.appendChild (liElement);
										}
										activate_li_freeItem();
										countLeftlist_freeItem()
										//populate_attribute_filter(product_val);
										
									}
									else{
										document.getElementById('product_filter_level_selector_freeItem').style.display="none"
										var selectDiv=document.getElementById('attributeSelect_freeItem');
										var labelElement=document.createElement ("label")
										labelElement.classList.add('control-label','col-md-10');
										labelElement.setAttribute('style','text-align: left')
										labelElement.innerHTML="No products to show"
										selectDiv.appendChild (labelElement);
									}										
									
									
									
									
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_items_after_filter", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("color_val="+color+"&size_val="+size+"&product_val="+product_val);
		        }
	
}
 function countLeftlist_freeItem(){
    	var right_list_search=document.querySelectorAll('#dual-list-right_freeItem li').length;
    	var left_list_search=document.querySelectorAll('#dual-list-left_freeItem li').length;
    	document.getElementById("right_list_search_freeItem").setAttribute('placeholder','Total SKU displayed:  '+right_list_search);
    	document.getElementById("left_list_search_freeItem").setAttribute('placeholder','Total SKU displayed:  '+left_list_search);	
    }
	<?php } ?> 
</script>




<?php if(!($get_deal_type==curr_code)){ ?>

<script type="text/javascript">
	function activate_li_freeItem() {
    var move_right = '<span class="glyphicon glyphicon-minus pull-left  dual-list-move-right" title="Remove Selected"></span>';
    var move_left  = '<span class="glyphicon glyphicon-plus  pull-right dual-list-move-left " title="Add Selected"></span>';
    
    $(".dual-list.list-left .list-group").sortable({
        stop: function( event, ui ) {
            updateSelectedOptions();
        }
    });
    
    
    $('body').on('click', '.list-group .list-group-item', function () {
        $(this).toggleClass('active');
    });
    
    
    $('body').on('click', '.dual-list-move-right', function (e) {
        e.preventDefault();

        actives = $(this).parent();
        $(this).parent().find("span").remove();
        $(move_left).clone().appendTo(actives);
        actives.clone().appendTo('.list-right ul').removeClass("active");
        actives.remove();
        
        sortUnorderedList("dual-list-right");
        
        updateSelectedOptions();
    });
    
    
    $('body').on('click', '.dual-list-move-left', function (e) {
        e.preventDefault();

        actives = $(this).parent();
        $(this).parent().find("span").remove();
        $(move_right).clone().appendTo(actives);
        actives.clone().appendTo('.list-left ul').removeClass("active");
        actives.remove();
        
        updateSelectedOptions();
    });
    
    
    $('.move-right, .move-left').click(function () {
        var $button = $(this), actives = '';
        if ($button.hasClass('move-left')) {
            actives = $('.list-right ul li.active');
            actives.find(".dual-list-move-left").remove();
            actives.append($(move_right).clone());
            actives.clone().appendTo('.list-left ul').removeClass("active");
            actives.remove();
            
            
        } else if ($button.hasClass('move-right')) {
            actives = $('.list-left ul li.active');
            actives.find(".dual-list-move-right").remove();
            actives.append($(move_left).clone());
            actives.clone().appendTo('.list-right ul').removeClass("active");
            actives.remove();

        }
        
        updateSelectedOptions();
        
    });
    
    function countLeftlist_freeItem(){
    	var right_list_search=document.querySelectorAll('#dual-list-right_freeItem li').length;
    	var left_list_search=document.querySelectorAll('#dual-list-left_freeItem li').length;
    	document.getElementById("right_list_search_freeItem").setAttribute('placeholder','Total SKU displayed:  '+right_list_search);
    	document.getElementById("left_list_search_freeItem").setAttribute('placeholder','Total SKU displayed:  '+left_list_search);	
    }

    
    function updateSelectedOptions() {
    	countLeftlist_freeItem();
    	update_buy_promo_list_freeItem();
        $('#dual-list-options_freeItem').find('option').remove();

        $('.list-left ul li').each(function(idx, opt) {
            $('#dual-list-options_freeItem').append($("<option></option>")
                .attr("value", $(opt).data("value"))
                .text( $(opt).text())
                .prop("selected", "selected")
            ); 
        });
    }
    
    
    $('.dual-list .selector').click(function () {
        var $checkBox = $(this);
        if (!$checkBox.hasClass('selected')) {
            $checkBox.addClass('selected').closest('.well').find('ul li:not(.active)').addClass('active');
            $checkBox.children('i').removeClass('glyphicon-unchecked').addClass('glyphicon-check');
        } else {
            $checkBox.removeClass('selected').closest('.well').find('ul li.active').removeClass('active');
            $checkBox.children('i').removeClass('glyphicon-check').addClass('glyphicon-unchecked');
        }
    });
    
    
    $('[name="SearchDualList"]').keyup(function (e) {
        var code = e.keyCode || e.which;
        if (code == '9') return;
        if (code == '27') $(this).val(null);
        var $rows = $(this).closest('.dual-list').find('.list-group li');
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
        $rows.show().filter(function () {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });
    
    
    $(".glyphicon-search").on("click", function() {
        $(this).next("input").focus();
    });
    
    
    function sortUnorderedList(ul, sortDescending) {
        $("#" + ul + "_freeItem li").sort(sort_li).appendTo("#" + ul +"_freeItem");
        
        function sort_li(a, b){
            return ($(b).data('value')) < ($(a).data('value')) ? 1 : -1;    
        }
    }
        
    
    $("#dual-list-left_freeItem li").append(move_right);
    $("#dual-list-right_freeItem li").append(move_left);
}


	</script>	    
	<?php } ?>     
<!--script for free item ends--->	    


<script>

function delete_this_selected_item(obj){
	var goAhead=1;
	var deactivate_flag=0
	var approval_flag=0
	<?php if($promo_active==1){ ?>
	var count_items_for_purchasing=obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.childNodes[1].getAttribute('count_items_for_purchasing')
	if(count_items_for_purchasing==1){
		var answer = confirm ("Warning! You have edited an active promotion. Continuing will first deactivate this promotion and save it in `Reverted Drafts`. You can further action it there.");
		if (answer){
			goAhead=1
			deactivate_flag=1
			}
		else{goAhead=0;return false}
	}
	<?php } ?>
    <?php if($approval_sent==1 && $promo_active==0){?> 
        var count_items_for_purchasing=obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.childNodes[1].getAttribute('count_items_for_purchasing')
    if(count_items_for_purchasing==1){
        var answer = confirm ("Warning !!.This promotion is already sent for approval.Deleting All Items will Cancel the request.");
        if (answer){
            goAhead=1
            approval_flag=1
            }
        else{goAhead=0;return false}
    }
    <?php } ?>
	
	
if(goAhead==1){
		var promo_id=obj.getAttribute('promo_id')
	var inv_id=obj.getAttribute('inv_id')
	var product_id=obj.getAttribute('product_id')
	var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
					obj.classList.remove("btn-danger");
					obj.classList.add("btn-warning");
					obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
		                	var ret=JSON.parse(xhr.responseText)
		                	if(ret[0]==1){

		                		if(ret[1]=="No Items Added"){
		                		    document.getElementById(product_id+'_listpurch_'+promo_id).remove()
		                		    var ul = document.getElementById('list_container_containing_purchasing_items')

                                    var liNodes = [];
                                    
                                    for (var i = 0; i < ul.childNodes.length; i++) {
                                        if (ul.childNodes[i].nodeName == "LI") {
                                            liNodes.push(ul.childNodes[i]);
                                        }
                                    }
                                    if(liNodes.length==0){
                                        document.getElementById('list_container_containing_purchasing_items').innerHTML="<li>No Items Added</li>"
                                    }
		                		}
		                		else{
		                		    document.getElementById(product_id+'_listpurch_'+promo_id).innerHTML=ret[1]
		                		}
		                		

								check_activations()	
								
		                	}
		                	else{
		                		alert("Error in Deleting");
			                	obj.classList.remove("btn-warning");
								obj.classList.add("btn-danger");
								obj.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>'
		                	}	
							var count_items_for_purchasing=ret[2]
							if(deactivate_flag==1){
								if(count_items_for_purchasing==0){
								alert("This promotion is deactivated")
								window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
								}
							}
							if(approval_flag==1){
                                if(count_items_for_purchasing==0){
                                alert("This approval request is cancel")
                                window.location.href='<?php echo base_url()?>admin/Promotions/manage_promo_items/'+promo_id
                                }
                            }

							if(count_items_for_purchasing>0){
								var del_promo_items_btn=document.getElementsByClassName('productToPromotion')
								for(i=0;i<del_promo_items_btn.length;i++){
									del_promo_items_btn[i].setAttribute('count_items_for_purchasing',count_items_for_purchasing)
								}
							}
		                }
		                
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/delete_this_promo_item_purchasing", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("promo_id="+promo_id+"&inv_id="+inv_id+"&product_id="+product_id+"&deactivate_flag="+deactivate_flag+"&approval_flag="+approval_flag);
		        }
}			        
}

function update_buy_promo_list(){
	if(document.getElementById('dual-list-left').hasChildNodes()){
	    document.getElementById('AttributeSubmit').innerHTML=""
		
		var uls = document.getElementById("dual-list-left");
			var listItem = uls.getElementsByTagName("li");
			
			var newNums ="";
			var skuIds=[];
			
			for (var i=0; i < listItem.length; i++) {
			    newNums+=(  listItem[i].getAttribute('data-value-name') )+"<br>" ;
			    skuIds.push(  listItem[i].getAttribute('sku-id') ) ;
			}
		document.getElementById('products_tagged_for_purchasing').value=skuIds
		document.getElementById('collection_of_all_sku_to_buy').innerHTML="";
		
			//document.getElementById('collection_of_all_sku_to_buy').appendChild (buttonElement);
			var buttonSubmit=document.createElement ("button")
			buttonSubmit.innerHTML="Add Items Taged Under Purchasing"
			buttonSubmit.type="submit";
			buttonSubmit.setAttribute("style","margin-left:10px");
			buttonSubmit.classList.add('btn','btn-info');
			
			document.getElementById('collection_of_all_sku_to_buy').appendChild (buttonSubmit);
	}
	else{
		document.getElementById('collection_of_all_sku_to_buy').innerHTML="Nothing Selected"
		document.getElementById('products_tagged_for_purchasing').value=""
		if(document.getElementById('AttributeSubmit')!=null){
		    if(document.getElementById('AttributeSubmit').innerHTML.trim()==""){
            var submitButton=document.createElement('button')
                submitButton.type="submit";
                submitButton.classList.add("btn","btn-info");
                submitButton.innerHTML="Add All Items Under this for Promotion";
                document.getElementById('AttributeSubmit').appendChild(submitButton)
            }
		}
	}
}

    function remove_category(){
        document.getElementById('parent_category_selector').value=""
        document.getElementById('CategoryContainer').innerHTML=""
        document.getElementById('parentCatSel').remove();
            if(document.getElementById('subCatSel')!=null){
                document.getElementById('subCatSel').remove();
            }
            if(document.getElementById('brand_sel')!=null){
                document.getElementById('brand_sel').remove();
            }
            if(document.getElementById('attribute_sel')!=null){
                document.getElementById('attribute_sel').remove();
            }
            if(document.getElementById('product_sel')!=null){
                document.getElementById('product_sel').remove();
            }

            var parentDiv = document.getElementById('subCategoryContainer');
            parentDiv.innerHTML=""
            
            if(document.getElementById('AttributeSubmit')!=null){
                document.getElementById('AttributeSubmit').innerHTML=""
            }
            if(document.getElementById('BrandSubmit')!=null){
                document.getElementById('BrandSubmit').innerHTML=""
            }
            if(document.getElementById('CategorySubmit')!=null){
                document.getElementById('CategorySubmit').innerHTML=""
            }
            if(document.getElementById('SubCategorySubmit')!=null){
                document.getElementById('SubCategorySubmit').innerHTML=""
            }
            if(document.getElementById('parentCategorySubmit')!=null){
                document.getElementById('parentCategorySubmit').innerHTML=""
            }
            
            if(document.getElementById('brandContainer').innerHTML!=null){
                document.getElementById('brandContainer').innerHTML=""
            }
            if(document.getElementById('attributeContainer').innerHTML!=null){
                document.getElementById('attributeContainer').innerHTML=""
            }
            if(document.getElementById('productContainer').innerHTML!=null){
                document.getElementById('productContainer').innerHTML=""
            }
            if(document.getElementById('productTypeContainer').innerHTML!=null){
                document.getElementById('productTypeContainer').innerHTML=""
            }
            if(document.getElementById('dual-list-right').innerHTML!=null){
                document.getElementById('dual-list-right').innerHTML=""
            }
            if(document.getElementById('dual-list-left').hasChildNodes()){
                document.getElementById('dual-list-left').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
                document.getElementById('product_filter_level_selector_').style.display="none"
            }
            if(document.getElementById('collection_of_all_sku_to_buy')!=null){
                document.getElementById('collection_of_all_sku_to_buy').innerHTML="";
            }
            document.getElementById('parentCategorySubmit').innerHTML=""
            var submitButton=document.createElement('button')
                submitButton.type="submit";
                submitButton.classList.add("btn","btn-info");
                submitButton.innerHTML="Add All Items In Store for Promotion";
             document.getElementById('parentCategorySubmit').appendChild(submitButton)
    }

	function remove_subcategory(){
			document.getElementById('category_selector').value=""
			document.getElementById('subCatSel').remove();
			if(document.getElementById('brand_sel')!=null){
				document.getElementById('brand_sel').remove();
			}
			if(document.getElementById('attribute_sel')!=null){
				document.getElementById('attribute_sel').remove();
			}
			if(document.getElementById('product_sel')!=null){
				document.getElementById('product_sel').remove();
			}

			var parentDiv = document.getElementById('subCategoryContainer');
			parentDiv.innerHTML=""
			
			if(document.getElementById('AttributeSubmit')!=null){
                document.getElementById('AttributeSubmit').innerHTML=""
            }
            if(document.getElementById('BrandSubmit')!=null){
                document.getElementById('BrandSubmit').innerHTML=""
            }
            if(document.getElementById('CategorySubmit')!=null){
                document.getElementById('CategorySubmit').innerHTML=""
            }
            if(document.getElementById('SubCategorySubmit')!=null){
                document.getElementById('SubCategorySubmit').innerHTML=""
            }
            if(document.getElementById('parentCategorySubmit')!=null){
                document.getElementById('parentCategorySubmit').innerHTML=""
            }
            var x = document.getElementById("parent_category_selector");
            var parentCatText=x.options[x.selectedIndex].text
            var submitButton=document.createElement('button')
                submitButton.type="submit";
                submitButton.classList.add("btn","btn-info");
                submitButton.innerHTML="Add All Items Under "+parentCatText+" for Promotion";
            document.getElementById('parentCategorySubmit').appendChild(submitButton)
			
			if(document.getElementById('brandContainer').innerHTML!=null){
				document.getElementById('brandContainer').innerHTML=""
			}
			if(document.getElementById('attributeContainer').innerHTML!=null){
				document.getElementById('attributeContainer').innerHTML=""
			}
			if(document.getElementById('productContainer').innerHTML!=null){
				document.getElementById('productContainer').innerHTML=""
			}
			if(document.getElementById('productTypeContainer').innerHTML!=null){
				document.getElementById('productTypeContainer').innerHTML=""
			}
			if(document.getElementById('dual-list-right').innerHTML!=null){
				document.getElementById('dual-list-right').innerHTML=""
			}
			if(document.getElementById('dual-list-left').hasChildNodes()){
				document.getElementById('dual-list-left').innerHTML=""
				update_buy_promo_list()
			}
			if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
				document.getElementById('product_filter_level_selector_').style.display="none"
			}
			if(document.getElementById('collection_of_all_sku_to_buy')!=null){
                document.getElementById('collection_of_all_sku_to_buy').innerHTML="";
            }
	}
	function remove_brand(){
		document.getElementById('sub_category_selector').value=""
			document.getElementById('brand_sel').remove();

			if(document.getElementById('attribute_sel')!=null){
				document.getElementById('attribute_sel').remove();
			}
			if(document.getElementById('product_sel')!=null){
				document.getElementById('product_sel').remove();
			}
			
            if(document.getElementById('AttributeSubmit')!=null){
                document.getElementById('AttributeSubmit').innerHTML=""
            }
            if(document.getElementById('BrandSubmit')!=null){
                document.getElementById('BrandSubmit').innerHTML=""
            }
            if(document.getElementById('CategorySubmit')!=null){
                document.getElementById('CategorySubmit').innerHTML=""
            }
            if(document.getElementById('SubCategorySubmit')!=null){
                document.getElementById('SubCategorySubmit').innerHTML=""
            }
            if(document.getElementById('parentCategorySubmit')!=null){
                document.getElementById('parentCategorySubmit').innerHTML=""
            }
            var x = document.getElementById("category_selector");
            var catText=x.options[x.selectedIndex].text
            var submitButton=document.createElement('button')
                submitButton.type="submit";
                submitButton.classList.add("btn","btn-info");
                submitButton.innerHTML="Add All Items Under "+catText+" for Promotion";
            document.getElementById('CategorySubmit').appendChild(submitButton)
            
			if(document.getElementById('brandContainer').innerHTML!=null){
				document.getElementById('brandContainer').innerHTML=""
			}
			if(document.getElementById('attributeContainer').innerHTML!=null){
				document.getElementById('attributeContainer').innerHTML=""
			}
			if(document.getElementById('productContainer').innerHTML!=null){
				document.getElementById('productContainer').innerHTML=""
			}
			if(document.getElementById('productTypeContainer').innerHTML!=null){
				document.getElementById('productTypeContainer').innerHTML=""
			}
			if(document.getElementById('dual-list-right').innerHTML!=null){
				document.getElementById('dual-list-right').innerHTML=""
			}
			if(document.getElementById('dual-list-left').hasChildNodes()){
				document.getElementById('dual-list-left').innerHTML=""
				update_buy_promo_list()
			}
			if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
				document.getElementById('product_filter_level_selector_').style.display="none"
			}
			if(document.getElementById('collection_of_all_sku_to_buy')!=null){
                document.getElementById('collection_of_all_sku_to_buy').innerHTML="";
            }
	}
	function remove_product(){
		document.getElementById('attribute_selector').value=""
			document.getElementById('attribute_sel').remove();

			if(document.getElementById('product_sel')!=null){
				document.getElementById('product_sel').remove;
			}
			
            if(document.getElementById('AttributeSubmit')!=null){
                document.getElementById('AttributeSubmit').innerHTML=""
            }
            if(document.getElementById('BrandSubmit')!=null){
                document.getElementById('BrandSubmit').innerHTML=""
            }
            if(document.getElementById('CategorySubmit')!=null){
                document.getElementById('CategorySubmit').innerHTML=""
            }
            if(document.getElementById('SubCategorySubmit')!=null){
                document.getElementById('SubCategorySubmit').innerHTML=""
            }
            if(document.getElementById('parentCategorySubmit')!=null){
                document.getElementById('parentCategorySubmit').innerHTML=""
            }
            var x = document.getElementById("sub_category_selector");
            var sub_catText=x.options[x.selectedIndex].text
            var submitButton=document.createElement('button')
                submitButton.type="submit";
                submitButton.classList.add("btn","btn-info");
                submitButton.innerHTML="Add All Items Under "+sub_catText+" for Promotion";
             document.getElementById('SubCategorySubmit').appendChild(submitButton)
            
			if(document.getElementById('attributeContainer').innerHTML!=null){
				document.getElementById('attributeContainer').innerHTML=""
			}
			if(document.getElementById('productContainer').innerHTML!=null){
				document.getElementById('productContainer').innerHTML=""
			}
			if(document.getElementById('productTypeContainer').innerHTML!=null){
				document.getElementById('productTypeContainer').innerHTML=""
			}
			if(document.getElementById('dual-list-right').innerHTML!=null){
				document.getElementById('dual-list-right').innerHTML=""
			}
			if(document.getElementById('dual-list-left').hasChildNodes()){
				document.getElementById('dual-list-left').innerHTML=""
				update_buy_promo_list()
			}
			if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
				document.getElementById('product_filter_level_selector_').style.display="none"
			}
			if(document.getElementById('collection_of_all_sku_to_buy')!=null){
                document.getElementById('collection_of_all_sku_to_buy').innerHTML="";
            }
	}
	function remove_product_variant(){
		document.getElementById('product_selector').value=""
			document.getElementById('product_sel').remove();


			if(document.getElementById('productContainer').innerHTML!=null){
				document.getElementById('productContainer').innerHTML=""
			}
			if(document.getElementById('productTypeContainer').innerHTML!=null){
				document.getElementById('productTypeContainer').innerHTML=""
			}
			if(document.getElementById('dual-list-right').innerHTML!=null){
				document.getElementById('dual-list-right').innerHTML=""
			}
			if(document.getElementById('dual-list-left').hasChildNodes()){
				document.getElementById('dual-list-left').innerHTML=""
				update_buy_promo_list()
			}
			
			if(document.getElementById('AttributeSubmit')!=null){
                document.getElementById('AttributeSubmit').innerHTML=""
            }
            if(document.getElementById('BrandSubmit')!=null){
                document.getElementById('BrandSubmit').innerHTML=""
            }
            if(document.getElementById('CategorySubmit')!=null){
                document.getElementById('CategorySubmit').innerHTML=""
            }
            if(document.getElementById('SubCategorySubmit')!=null){
                document.getElementById('SubCategorySubmit').innerHTML=""
            }
            if(document.getElementById('parentCategorySubmit')!=null){
                document.getElementById('parentCategorySubmit').innerHTML=""
            }
            var x = document.getElementById("attribute_selector");
            var brands_Text=x.options[x.selectedIndex].text
            var submitButton=document.createElement('button')
                submitButton.type="submit";
                submitButton.classList.add("btn","btn-info");
                submitButton.innerHTML="Add All Items Under "+brands_Text+" for Promotion";
             document.getElementById('BrandSubmit').appendChild(submitButton)
			
			if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
				document.getElementById('product_filter_level_selector_').style.display="none"
			}
			if(document.getElementById('collection_of_all_sku_to_buy')!=null){
                document.getElementById('collection_of_all_sku_to_buy').innerHTML="";
            }
	}
	
	function get_category(){
	    var parentCatval=document.getElementById('parent_category_selector').value
        var x = document.getElementById("parent_category_selector");
        var parentCatText=x.options[x.selectedIndex].text
        if(document.getElementById('parentCatSel')!=null){
            remove_category()
        }
        document.getElementById('parent_category_selector').value=parentCatval
        document.getElementById("parent_category_selector").options[x.selectedIndex].text=parentCatText
            var parentDiv = document.getElementById('CategoryContainer');
            parentDiv.innerHTML=""
            if(document.getElementById('subCategoryContainer').innerHTML!=null){
                document.getElementById('subCategoryContainer').innerHTML=""
            }
            if(document.getElementById('brandContainer').innerHTML!=null){
                document.getElementById('brandContainer').innerHTML=""
            }
            if(document.getElementById('attributeContainer').innerHTML!=null){
                document.getElementById('attributeContainer').innerHTML=""
            }
            if(document.getElementById('productContainer').innerHTML!=null){
                document.getElementById('productContainer').innerHTML=""
            }
            if(document.getElementById('productTypeContainer').innerHTML!=null){
                document.getElementById('productTypeContainer').innerHTML=""
            }
            if(document.getElementById('dual-list-right').innerHTML!=null){
                document.getElementById('dual-list-right').innerHTML=""
            }
            
            if(document.getElementById('dual-list-left').hasChildNodes()){
                document.getElementById('dual-list-left').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
                document.getElementById('product_filter_level_selector_').style.display="none"
            }

        if(parentCatval!=""){
           document.getElementById('parentCategorySubmit').innerHTML=""          
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                    var select_data=JSON.parse(xhr.responseText);
                                    /*var labelElement=document.createElement ("label")
                                    labelElement.classList.add('control-label','col-md-2');
                                    labelElement.innerHTML="Category:"
                                    parentDiv.appendChild (labelElement);*/
                                    if(select_data.length>0){
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-5');
                                        divElement.setAttribute('id','ParentCategorySelect')
                                        parentDiv.appendChild (divElement);
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-5');
                                        divElement.setAttribute('id','CategorySubmit')
                                        parentDiv.appendChild (divElement);
                                        
                                        var selectDiv=document.getElementById('ParentCategorySelect');
                                        var selectElement = document.createElement ("select");
                                        selectElement.classList.add("form-control");
                                        selectElement.setAttribute('id','category_selector');
                                        selectElement.setAttribute('onchange','get_subcategory()');
                                        selectElement.name="category";
                                        var option = new Option ("Choose Category","");
                                        selectElement.options[selectElement.options.length] = option;
                                        for (var i=0;i < select_data.length;i++)
                                        {
                                            
                                            var option = new Option (select_data[i].cat_name,select_data[i].cat_id);
                                            selectElement.options[selectElement.options.length] = option;
                                        }
                                        selectDiv.appendChild (selectElement);
                                        var submitButton=document.createElement('button')
                                            submitButton.type="submit";
                                            submitButton.classList.add("btn","btn-info");
                                            submitButton.innerHTML="Add All Items Under "+parentCatText+" for Promotion";
                                         document.getElementById('parentCategorySubmit').appendChild(submitButton)   
                                        if(document.getElementById('parentCatSel')==null){
                                            var list_stack_disp=document.getElementById('item_picker_del')
                                            var list_level_span = document.createElement ("li");
                                            list_level_span.innerHTML=parentCatText +'&nbsp';
                                            list_level_span.setAttribute('id',"parentCatSel");
                                            list_level_span.setAttribute("selector_li_name",parentCatText)
                                            list_stack_disp.appendChild (list_level_span);
                                            var btncon=document.getElementById("parentCatSel")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',"parentCatSel_"+parentCatval);
                                            list_level_button.setAttribute('onclick','remove_category()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                        }
                                        else{
                                            document.getElementById('parentCatSel').innerHTML= parentCatText+'&nbsp';
                                            document.getElementById('parentCatSel').setAttribute("selector_li_name",parentCatText)
                                            var btncon=document.getElementById("parentCatSel")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',"parentCatSel_"+parentCatval);
                                            list_level_button.setAttribute('onclick','remove_category()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                        }
                                   }
                                     else{
                                        if(document.getElementById('parentCatSel')){
                                            document.getElementById('parentCatSel').remove();
                                        }
                                        if(document.getElementById('subCatSel')){
                                            document.getElementById('subCatSel').remove();
                                        }
                                        if(document.getElementById('brand_sel')){
                                            document.getElementById('brand_sel').remove();
                                        }
                                        if(document.getElementById('brand_sel')){
                                            document.getElementById('brand_sel').remove();
                                        }
                                        if(document.getElementById('attribute_sel')){
                                            document.getElementById('attribute_sel').remove();
                                        }
                                        if(document.getElementById('product_sel')){
                                            document.getElementById('product_sel').remove();
                                        }
                                        var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-10');
                                        labelElement.setAttribute('style','text-align: left')
                                        labelElement.innerHTML="No products to show"
                                        parentDiv.appendChild (labelElement);
                                        document.getElementById('parentCategorySubmit').innerHTML=""
                                    }       
                                        
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_category", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send("pcat_id="+parentCatval);
                }
        }
        else{
            document.getElementById('parentCategorySubmit').innerHTML=""
            var submitButton=document.createElement('button')
                submitButton.type="submit";
                submitButton.classList.add("btn","btn-info");
                submitButton.innerHTML="Add All Items In Store for Promotion";
             document.getElementById('parentCategorySubmit').appendChild(submitButton)
        }
	}

	
	function get_subcategory(){
			
		var catval=document.getElementById('category_selector').value
		var x = document.getElementById("category_selector");
		var catText=x.options[x.selectedIndex].text
		if(document.getElementById('subCatSel')!=null){
			remove_subcategory()
		}
		document.getElementById('category_selector').value=catval
		document.getElementById("category_selector").options[x.selectedIndex].text=catText
			var parentDiv = document.getElementById('subCategoryContainer');
			parentDiv.innerHTML=""
			if(document.getElementById('brandContainer').innerHTML!=null){
				document.getElementById('brandContainer').innerHTML=""
			}
			if(document.getElementById('attributeContainer').innerHTML!=null){
				document.getElementById('attributeContainer').innerHTML=""
			}
			if(document.getElementById('productContainer').innerHTML!=null){
				document.getElementById('productContainer').innerHTML=""
			}
			if(document.getElementById('productTypeContainer').innerHTML!=null){
				document.getElementById('productTypeContainer').innerHTML=""
			}
			if(document.getElementById('dual-list-right').innerHTML!=null){
				document.getElementById('dual-list-right').innerHTML=""
			}
			
			if(document.getElementById('dual-list-left').hasChildNodes()){
				document.getElementById('dual-list-left').innerHTML=""
				update_buy_promo_list()
			}
			if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
				document.getElementById('product_filter_level_selector_').style.display="none"
			}

		if(catval!=""){			
		var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
									var select_data=JSON.parse(xhr.responseText);
		                  			/*var labelElement=document.createElement ("label")
									labelElement.classList.add('control-label','col-md-2');
									labelElement.innerHTML="Sub Category:"
									parentDiv.appendChild (labelElement);*/
									   
									if(select_data.length>0){
    									var divElement=document.createElement ("div")
    									divElement.classList.add('col-md-5');
    									divElement.setAttribute('id','subCategorySelect')
    									parentDiv.appendChild (divElement);
    									var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-5');
                                        divElement.setAttribute('id','SubCategorySubmit')
                                        parentDiv.appendChild (divElement);
    									
                                        document.getElementById('parentCategorySubmit').innerHTML=""
                                        document.getElementById('CategorySubmit').innerHTML=""
    									var submitButton=document.createElement('button')
                                            submitButton.type="submit";
                                            submitButton.classList.add("btn","btn-info");
                                            submitButton.innerHTML="Add All Items Under "+catText+" for Promotion";
                                         document.getElementById('CategorySubmit').appendChild(submitButton)
    									
    									var selectDiv=document.getElementById('subCategorySelect');
    									var selectElement = document.createElement ("select");
    									selectElement.classList.add("form-control");
    									selectElement.setAttribute('id','sub_category_selector');
    									selectElement.setAttribute('onchange','get_brands()');
    									selectElement.name="sub_category";
    									var option = new Option ("Choose Sub Category","");
    									selectElement.options[selectElement.options.length] = option;
    									for (var i=0;i < select_data.length;i++)
    									{
    										
    									    var option = new Option (select_data[i].subcat_name,select_data[i].subcat_id);
    									    selectElement.options[selectElement.options.length] = option;
    									}
    									selectDiv.appendChild (selectElement);
    									if(document.getElementById('subCatSel')==null){
    										var list_stack_disp=document.getElementById('item_picker_del')
    										var list_level_span = document.createElement ("li");
    										list_level_span.innerHTML=catText +'&nbsp';
    										list_level_span.setAttribute('id',"subCatSel");
    										list_level_span.setAttribute("selector_li_name",catText)
    										list_stack_disp.appendChild (list_level_span);
    										var btncon=document.getElementById("subCatSel")
    										var list_level_button=document.createElement ("button");
    										list_level_button.type="button";
    										list_level_button.setAttribute('id',"subCatSel_"+catval);
    										list_level_button.setAttribute('onclick','remove_subcategory()');
    										list_level_button.classList.add("material-button","btn-danger");
    										list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
    										btncon.appendChild (list_level_button);
    									}
    									else{
    										document.getElementById('subCatSel').innerHTML=catText+'&nbsp';
    										document.getElementById('subCatSel').setAttribute("selector_li_name",catText)
    										var btncon=document.getElementById("subCatSel")
    										var list_level_button=document.createElement ("button");
    										list_level_button.type="button";
    										list_level_button.setAttribute('id',"subCatSel_"+catval);
    										list_level_button.setAttribute('onclick','remove_subcategory()');
    										list_level_button.classList.add("material-button","btn-danger");
    										list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
    										btncon.appendChild (list_level_button);
    									}
    								}
                                     else{
                                        if(document.getElementById('subCatSel')){
                                            document.getElementById('subCatSel').remove();
                                        }
                                        if(document.getElementById('brand_sel')){
                                            document.getElementById('brand_sel').remove();
                                        }
                                        if(document.getElementById('brand_sel')){
                                            document.getElementById('brand_sel').remove();
                                        }
                                        if(document.getElementById('attribute_sel')){
                                            document.getElementById('attribute_sel').remove();
                                        }
                                        if(document.getElementById('product_sel')){
                                            document.getElementById('product_sel').remove();
                                        }
                                        var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-10');
                                        labelElement.setAttribute('style','text-align: left')
                                        labelElement.innerHTML="No products to show"
                                        parentDiv.appendChild (labelElement);
                                        document.getElementById('CategorySubmit').innerHTML=""
                                    }		
										
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_sub_catageory", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("cat_id="+catval);
		        }
		}
	}
	
	function get_brands(){
		var sub_cat_val=document.getElementById('sub_category_selector').value
		var x = document.getElementById("sub_category_selector");
		var sub_catText=x.options[x.selectedIndex].text
		var parentDiv = document.getElementById('brandContainer');
			parentDiv.innerHTML=""

			if(document.getElementById('attributeContainer').innerHTML!=null){
				document.getElementById('attributeContainer').innerHTML=""
			}
			if(document.getElementById('productContainer').innerHTML!=null){
				document.getElementById('productContainer').innerHTML=""
			}
			if(document.getElementById('productTypeContainer').innerHTML!=null){
				document.getElementById('productTypeContainer').innerHTML=""
			}
			if(document.getElementById('dual-list-right').innerHTML!=null){
				document.getElementById('dual-list-right').innerHTML=""
			}
			if(document.getElementById('dual-list-left').hasChildNodes()){
				document.getElementById('dual-list-left').innerHTML=""
				update_buy_promo_list()
			}
			if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
				document.getElementById('product_filter_level_selector_').style.display="none"
			}				
		if(sub_cat_val!=""){			
		var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
									var select_data=JSON.parse(xhr.responseText);
									/*var labelElement=document.createElement ("label")
										labelElement.classList.add('control-label','col-md-2');
										labelElement.innerHTML="Select Brand:"
										parentDiv.appendChild (labelElement);*/
									
										
										
									if(select_data.length>0){
										var divElement=document.createElement ("div")
										divElement.classList.add('col-md-5');
										divElement.setAttribute('id','brandSelect')
										parentDiv.appendChild (divElement);
										var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-5');
                                        divElement.setAttribute('id','BrandSubmit')
                                        parentDiv.appendChild (divElement);
                                        
                                        document.getElementById('CategorySubmit').innerHTML=""
                                        document.getElementById('SubCategorySubmit').innerHTML=""
                                        var submitButton=document.createElement('button')
                                            submitButton.type="submit";
                                            submitButton.classList.add("btn","btn-info");
                                            submitButton.innerHTML="Add All Items Under "+sub_catText+" for Promotion";
                                         document.getElementById('SubCategorySubmit').appendChild(submitButton)
										
										var selectDiv=document.getElementById('brandSelect');
										var selectElement = document.createElement ("select");
										selectElement.classList.add("form-control");
										selectElement.setAttribute('id','attribute_selector');
										selectElement.setAttribute('onchange','get_all_attributes()');
										selectElement.name="attributes";
										var option = new Option ("Choose Brand","");
										selectElement.options[selectElement.options.length] = option;
										for (var i=0;i < select_data.length;i++)
										{
										    var option = new Option (select_data[i].brand_name,select_data[i].brand_id);
										    selectElement.options[selectElement.options.length] = option;
										}
										selectDiv.appendChild (selectElement);

										if(document.getElementById('brand_sel')==null){
											var list_stack_disp=document.getElementById('item_picker_del')
											var list_level_span = document.createElement ("li");
											list_level_span.innerHTML=sub_catText +'&nbsp';
											list_level_span.setAttribute('id',"brand_sel");
											list_level_span.setAttribute('selector_li_name',sub_catText);
											list_stack_disp.appendChild (list_level_span);
											var btncon=document.getElementById("brand_sel")
											var list_level_button=document.createElement ("button");
											list_level_button.type="button";
											list_level_button.setAttribute('id',"brand_sel_"+sub_cat_val);
											list_level_button.setAttribute('onclick','remove_brand()');
											list_level_button.classList.add("material-button","btn-danger");
											list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
											btncon.appendChild (list_level_button);
											
											if(document.getElementById('attribute_sel')){
											document.getElementById('attribute_sel').remove();
											}
											if(document.getElementById('product_sel')){
												document.getElementById('product_sel').remove();
											}
										}
										else{
											document.getElementById('brand_sel').innerHTML=sub_catText+'&nbsp';;
											document.getElementById('brand_sel').setAttribute('selector_li_name',sub_catText);
											var btncon=document.getElementById("brand_sel")
											var list_level_button=document.createElement ("button");
											list_level_button.type="button";
											list_level_button.setAttribute('id',"brand_sel_"+sub_cat_val);
											list_level_button.setAttribute('onclick','remove_brand()');
											list_level_button.classList.add("material-button","btn-danger");
											list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
											btncon.appendChild (list_level_button);
											if(document.getElementById('attribute_sel')){
											document.getElementById('attribute_sel').remove();
											}
											if(document.getElementById('product_sel')){
												document.getElementById('product_sel').remove();
											}
										}
										
									}
									else{
										if(document.getElementById('brand_sel')){
											document.getElementById('brand_sel').remove();
										}
										if(document.getElementById('attribute_sel')){
											document.getElementById('attribute_sel').remove();
										}
										if(document.getElementById('product_sel')){
											document.getElementById('product_sel').remove();
										}
										var labelElement=document.createElement ("label")
										labelElement.classList.add('control-label','col-md-10');
										labelElement.setAttribute('style','text-align: left')
										labelElement.innerHTML="No products to show"
										parentDiv.appendChild (labelElement);
										document.getElementById('SubCategorySubmit').innerHTML=""
									}
		                  			
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_brand", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("sub_cat_val="+sub_cat_val);
		        }
		}
	}
	
function get_all_attributes(){
	var brands_val=document.getElementById('attribute_selector').value
	var x = document.getElementById("attribute_selector");
		var brands_Text=x.options[x.selectedIndex].text
		var parentDiv = document.getElementById('attributeContainer');
			parentDiv.innerHTML=""

			if(document.getElementById('productContainer').innerHTML!=null){
				document.getElementById('productContainer').innerHTML=""
			}
			if(document.getElementById('productTypeContainer').innerHTML!=null){
				document.getElementById('productTypeContainer').innerHTML=""
			}
			if(document.getElementById('dual-list-right').innerHTML!=null){
				document.getElementById('dual-list-right').innerHTML=""
			}
			if(document.getElementById('dual-list-left').hasChildNodes()){
				document.getElementById('dual-list-left').innerHTML=""
				update_buy_promo_list()
			}
			if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
				document.getElementById('product_filter_level_selector_').style.display="none"
			}					
		if(brands_val!=""){			
		var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
									var select_data=JSON.parse(xhr.responseText);
									/*var labelElement=document.createElement ("label")
										labelElement.classList.add('control-label','col-md-2');
										labelElement.innerHTML="Select Product:"
										parentDiv.appendChild (labelElement);*/
										
										
									if(select_data.length>0){
										var divElement=document.createElement ("div")
										divElement.classList.add('col-md-5');
										divElement.setAttribute('id','attributeSelect')
										parentDiv.appendChild (divElement);
										var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-5');
                                        divElement.setAttribute('id','AttributeSubmit')
                                        parentDiv.appendChild (divElement);
                                        
                                        document.getElementById('SubCategorySubmit').innerHTML=""
                                        document.getElementById('BrandSubmit').innerHTML=""
                                        var submitButton=document.createElement('button')
                                            submitButton.type="submit";
                                            submitButton.classList.add("btn","btn-info");
                                            submitButton.innerHTML="Add All Items Under "+brands_Text+" for Promotion";
                                         document.getElementById('BrandSubmit').appendChild(submitButton)
										
										var selectDiv=document.getElementById('attributeSelect');
										var selectElement = document.createElement ("select");
										selectElement.classList.add("form-control");
										selectElement.setAttribute('id','product_selector');
										selectElement.setAttribute('onchange','get_all_items()');
										selectElement.name="attributes_selector";
										var option = new Option ("Choose Product","");
										selectElement.options[selectElement.options.length] = option;
										for (var i=0;i < select_data.length;i++)
										{
										    var option = new Option (select_data[i].product_name,select_data[i].product_id);
										    selectElement.options[selectElement.options.length] = option;
										}
										selectDiv.appendChild (selectElement);
										
										if(document.getElementById('attribute_sel')==null){
											var list_stack_disp=document.getElementById('item_picker_del')
											var list_level_span = document.createElement ("li");
											list_level_span.innerHTML=brands_Text +'&nbsp';
											list_level_span.setAttribute('id',"attribute_sel");
											list_level_span.setAttribute('selector_li_name',brands_Text);
											list_stack_disp.appendChild (list_level_span);
											var btncon=document.getElementById("attribute_sel")
											var list_level_button=document.createElement ("button");
											list_level_button.type="button";
											list_level_button.setAttribute('id',brands_val+"attribute_sel_");
											list_level_button.setAttribute('onclick','remove_product()');
											list_level_button.classList.add("material-button","btn-danger");
											list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
											btncon.appendChild (list_level_button);
											if(document.getElementById('product_sel')){
												document.getElementById('product_sel').remove();
											}
										}
										else{
											document.getElementById('attribute_sel').innerHTML=brands_Text+'&nbsp';
											document.getElementById('attribute_sel').setAttribute('selector_li_name',brands_Text);
											var btncon=document.getElementById("attribute_sel")
											var list_level_button=document.createElement ("button");
											list_level_button.type="button";
											list_level_button.setAttribute('id',brands_val+"attribute_sel_");
											list_level_button.setAttribute('onclick','remove_product()');
											list_level_button.classList.add("material-button","btn-danger");
											list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
											btncon.appendChild (list_level_button);
											if(document.getElementById('product_sel')){
												document.getElementById('product_sel').remove();
											}
										}
										
									}
									else{
										if(document.getElementById('attribute_sel')){
											document.getElementById('attribute_sel').remove();
										}
										if(document.getElementById('product_sel')){
											document.getElementById('product_sel').remove();
										}
										var labelElement=document.createElement ("label")
										labelElement.classList.add('control-label','col-md-10');
										labelElement.setAttribute('style','text-align: left')
										labelElement.innerHTML="No products to show"
										parentDiv.appendChild (labelElement);
										document.getElementById('BrandSubmit').innerHTML=""
									}
		                  			
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_attributes", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("brands_val="+brands_val);
		        }
		}
}	

function get_all_items(){
		var product_val=document.getElementById('product_selector').value
		var x = document.getElementById("product_selector");
		var product_Text=x.options[x.selectedIndex].text
		var parentDiv = document.getElementById('productContainer');
		parentDiv.innerHTML=""

		if(document.getElementById('dual-list-right').innerHTML!=null){
			document.getElementById('dual-list-right').innerHTML=""
		}
		if(document.getElementById('dual-list-left').hasChildNodes()){
			document.getElementById('dual-list-left').innerHTML=""
			update_buy_promo_list()
		}
		if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
			document.getElementById('product_filter_level_selector_').style.display="none"
		}		
		if(product_val!=""){			
		var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
									var select_data=JSON.parse(xhr.responseText);
									document.getElementById('dual-list-left').innerHTML=""
									document.getElementById('dual-list-right').innerHTML=""
										
									if(select_data.length>0){                                      
                                        document.getElementById('BrandSubmit').innerHTML=""
                                        if(document.getElementById('AttributeSubmit').innerHTML.trim()==""){
                                            var submitButton=document.createElement('button')
                                            submitButton.type="submit";
                                            submitButton.classList.add("btn","btn-info");
                                            submitButton.innerHTML="Add All Items Under this for Promotion";
                                            document.getElementById('AttributeSubmit').appendChild(submitButton)
                                        }
										document.getElementById('product_filter_level_selector_').style.display=""
										
										var liDiv=document.getElementById('dual-list-right');
										document.getElementById('dual-list-left').innerHTML=""
										document.getElementById('dual-list-right').innerHTML=""
										for (var i=0;i < select_data.length;i++)
										{
											var val= select_data[i].attribute_1_value.split(":")
											var liElement = document.createElement ("li");
											liElement.innerHTML=select_data[i].sku_id+'('+select_data[i].attribute_1+'='+val[0]+', '+select_data[i].attribute_2+'='+select_data[i].attribute_2_value+')';
											liElement.classList.add("list-group-item");
											liElement.setAttribute('data-value-name',select_data[i].sku_id+'('+select_data[i].attribute_1+'='+select_data[i].attribute_1_value+', '+select_data[i].attribute_2+'='+select_data[i].attribute_2_value+')');
											liElement.setAttribute('sku-id',select_data[i].id);
											liElement.setAttribute('data-value',select_data[i].id);
											liElement.name="item_typs";
											liDiv.appendChild (liElement);
										}
										activate_li();
										populate_attribute_filter(product_val);
										
										if(document.getElementById('product_sel')==null){
											if(product_Text.length > 10){
												product_Texts = product_Text.substring(0,10)+"...";
											}
											else{
												product_Texts=product_Text;
											}
											
											var list_stack_disp=document.getElementById('item_picker_del')
											var list_level_span = document.createElement ("li");
											list_level_span.innerHTML=product_Texts +'&nbsp';
											list_level_span.setAttribute('id',"product_sel");
											list_level_span.setAttribute('selector_li_name',product_Text);
											list_stack_disp.appendChild (list_level_span);
											var btncon=document.getElementById("product_sel")
											var list_level_button=document.createElement ("button");
											list_level_button.type="button";
											list_level_button.setAttribute('id',product_val+"_product_sel");
											list_level_button.setAttribute('onclick','remove_product_variant()');
											list_level_button.classList.add("material-button","btn-danger");
											list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
											btncon.appendChild (list_level_button);
										}
										else{
											if(product_Text.length > 10){
												product_Texts = product_Text.substring(0,10)+"...";
											}
											document.getElementById('product_sel').innerHTML=product_Texts+'&nbsp';
											document.getElementById('product_sel').setAttribute('selector_li_name',product_Text)		
											var btncon=document.getElementById("product_sel")
											var list_level_button=document.createElement ("button");
											list_level_button.type="button";
											list_level_button.setAttribute('id',product_val+"_product_sel");
											list_level_button.setAttribute('onclick','remove_product_variant()');
											list_level_button.classList.add("material-button","btn-danger");
											list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
											btncon.appendChild (list_level_button);
										}
										
										
										
									}
									else{
										if(document.getElementById('product_sel')){
											document.getElementById('product_sel').remove();
										}
										document.getElementById('product_filter_level_selector_').style.display="none"
										var selectDiv=document.getElementById('attributeSelect');
										var labelElement=document.createElement ("label")
										labelElement.classList.add('control-label','col-md-10');
										labelElement.setAttribute('style','text-align: left')
										labelElement.innerHTML="No products to show"
										selectDiv.appendChild (labelElement);
										document.getElementById('AttributeSubmit').innerHTML=""
									}
		                  			
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_items_in_type", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("product_val="+product_val);
		        }
		}
}

function populate_attribute_filter(product_val){
	var productTypeContainer =document.getElementById('productTypeContainer')
		productTypeContainer.innerHTML=""
		
		var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
									var select_data=JSON.parse(xhr.responseText);										
									//alert(select_data.attr1.length);
									//alert(select_data.attr2.length);
									if(select_data.attr1.length>0){
										var labelElement=document.createElement ("div")
										labelElement.classList.add('col-md-12');
										labelElement.innerHTML="Filter "+select_data.attribute_1+" :";
										productTypeContainer.appendChild (labelElement);
										var divElement=document.createElement ("div")
										divElement.classList.add('col-md-12');
										divElement.setAttribute('id','attributeColorSelect')
										productTypeContainer.appendChild (divElement);
										var checkDiv=document.getElementById('attributeColorSelect');
									
										
										for (var i=0;i < select_data.attr1.length;i++){
											var val=select_data.attr1[i].split(':');
											var labelElement=document.createElement ("label")
											labelElement.classList.add('checkbox-inline');
											labelElement.setAttribute('id','id_'+select_data.attr1[i]);
											checkDiv.appendChild (labelElement);
											
											var labelDiv=document.getElementById('id_'+select_data.attr1[i]);
											var checkbox = document.createElement('input');
											checkbox.type = "checkbox";
											checkbox.name = "filter_color";
											checkbox.setAttribute("onclick","applyToFilter("+product_val+")");
											checkbox.value = select_data.attr1[i];
											checkbox.id = "id_"+select_data.attr1[i];
											
											var label = document.createElement('label')
											label.htmlFor = "id_"+select_data.attr1[i];
											label.appendChild(document.createTextNode(val[0]));
											
											labelDiv.appendChild(checkbox);
											labelDiv.appendChild(label);
										}
										
									}
									if(select_data.attr2!=null){
									if(select_data.attr2.length>0){
										var labelElement=document.createElement ("div")
										labelElement.classList.add('col-md-12');
										labelElement.innerHTML="Filter "+select_data.attribute_2+" :";
										productTypeContainer.appendChild (labelElement);
										var divElement=document.createElement ("div")
										divElement.classList.add('col-md-12');
										divElement.setAttribute('id','attributeSizeSelect')
										productTypeContainer.appendChild (divElement);
										var checkDiv=document.getElementById('attributeSizeSelect');
									
										
										for (var i=0;i < select_data.attr2.length;i++){
											var labelElement=document.createElement ("label")
											labelElement.classList.add('checkbox-inline');
											labelElement.setAttribute('id','id_'+select_data.attr2[i]);
											checkDiv.appendChild (labelElement);
											
											var labelDiv=document.getElementById('id_'+select_data.attr2[i]);
											var checkbox = document.createElement('input');
											checkbox.type = "checkbox";
											checkbox.name = "filter_size";
											checkbox.setAttribute("onclick","applyToFilter("+product_val+")");
											checkbox.value = select_data.attr2[i];
											checkbox.id = "id_"+select_data.attr2[i];
											
											var label = document.createElement('label')
											label.htmlFor = "id_"+select_data.attr2[i];
											label.appendChild(document.createTextNode(select_data.attr2[i]));
											
											labelDiv.appendChild(checkbox);
											labelDiv.appendChild(label);
										}
										countLeftlist()
									}
									}
									
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_items_in_type_filter", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("product_val="+product_val);
		        }
}

function applyToFilter(product_val){
	update_buy_promo_list()
	var colorelem=document.getElementsByName('filter_color')
	var colorelemarr = []
	for(i=0;i<colorelem.length;i++){
		if(colorelem[i].checked){
			colorelemarr.push(colorelem[i].value);
		}
	}
	var color=colorelemarr.join("-");
	var sizeelem=document.getElementsByName('filter_size')
	var sizeelemarr = []
	for(i=0;i<sizeelem.length;i++){
		if(sizeelem[i].checked){
			sizeelemarr.push(sizeelem[i].value);
		}
	}
	var size=sizeelemarr.join("-");
	
	var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
									var select_data=JSON.parse(xhr.responseText);
									var liDiv=document.getElementById('dual-list-right');
									liDiv.innerHTML=""
									document.getElementById('dual-list-left').innerHTML=""
										//alert(xhr.responseText);
									if(select_data.length>0){
										document.getElementById('product_filter_level_selector_').style.display=""
										
										var liDiv=document.getElementById('dual-list-right');
										for (var i=0;i < select_data.length;i++)
										{
										    var val=select_data[i].attribute_1_value.split(':');
											var liElement = document.createElement ("li");
											liElement.innerHTML=select_data[i].sku_id+'('+select_data[i].attribute_1+'='+val[0]+', '+select_data[i].attribute_2+'='+select_data[i].attribute_2_value+')';
											liElement.classList.add("list-group-item");
											liElement.setAttribute('data-value-name',select_data[i].sku_id+'('+select_data[i].attribute_1+'='+select_data[i].attribute_1_value+', '+select_data[i].attribute_2+'='+select_data[i].attribute_2_value+')');
											liElement.setAttribute('sku-id',select_data[i].id);
											liElement.setAttribute('data-value',select_data[i].id);
											liElement.name="item_typs";
											liDiv.appendChild (liElement);
										}
										activate_li();
										countLeftlist()
										//populate_attribute_filter(product_val);
										
									}
									else{
										document.getElementById('product_filter_level_selector_').style.display="none"
										var selectDiv=document.getElementById('attributeSelect');
										var labelElement=document.createElement ("label")
										labelElement.classList.add('control-label','col-md-10');
										labelElement.setAttribute('style','text-align: left')
										labelElement.innerHTML="No products to show"
										selectDiv.appendChild (labelElement);
									}										
									
									
									
									
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_items_after_filter", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("color_val="+color+"&size_val="+size+"&product_val="+product_val);
		        }
	
}
 function countLeftlist(){
    	var right_list_search=document.querySelectorAll('#dual-list-right li').length;
    	var left_list_search=document.querySelectorAll('#dual-list-left li').length;
    	document.getElementById("right_list_search").setAttribute('placeholder','Total SKU displayed:  '+right_list_search);
    	document.getElementById("left_list_search").setAttribute('placeholder','Total SKU displayed:  '+left_list_search);	
    }
	
</script>






<script type="text/javascript">
	function activate_li() {
    var move_right = '<span class="glyphicon glyphicon-minus pull-left  dual-list-move-right" title="Remove Selected"></span>';
    var move_left  = '<span class="glyphicon glyphicon-plus  pull-right dual-list-move-left " title="Add Selected"></span>';
    
    $(".dual-list.list-left .list-group").sortable({
        stop: function( event, ui ) {
            updateSelectedOptions();
        }
    });
    
    
    $('body').on('click', '.list-group .list-group-item', function () {
        $(this).toggleClass('active');
    });
    
    
    $('body').on('click', '.dual-list-move-right', function (e) {
        e.preventDefault();

        actives = $(this).parent();
        $(this).parent().find("span").remove();
        $(move_left).clone().appendTo(actives);
        actives.clone().appendTo('.list-right ul').removeClass("active");
        actives.remove();
        
        sortUnorderedList("dual-list-right");
        
        updateSelectedOptions();
    });
    
    
    $('body').on('click', '.dual-list-move-left', function (e) {
        e.preventDefault();

        actives = $(this).parent();
        $(this).parent().find("span").remove();
        $(move_right).clone().appendTo(actives);
        actives.clone().appendTo('.list-left ul').removeClass("active");
        actives.remove();
        
        updateSelectedOptions();
    });
    
    
    $('.move-right, .move-left').click(function () {
        var $button = $(this), actives = '';
        if ($button.hasClass('move-left')) {
            actives = $('.list-right ul li.active');
            actives.find(".dual-list-move-left").remove();
            actives.append($(move_right).clone());
            actives.clone().appendTo('.list-left ul').removeClass("active");
            actives.remove();
            
            
        } else if ($button.hasClass('move-right')) {
            actives = $('.list-left ul li.active');
            actives.find(".dual-list-move-right").remove();
            actives.append($(move_left).clone());
            actives.clone().appendTo('.list-right ul').removeClass("active");
            actives.remove();

        }
        
        updateSelectedOptions();
        
    });
    
    function countLeftlist(){
    	var right_list_search=document.querySelectorAll('#dual-list-right li').length;
    	var left_list_search=document.querySelectorAll('#dual-list-left li').length;
    	document.getElementById("right_list_search").setAttribute('placeholder','Total SKU displayed:  '+right_list_search);
    	document.getElementById("left_list_search").setAttribute('placeholder','Total SKU displayed:  '+left_list_search);	
    }

    
    function updateSelectedOptions() {
    	countLeftlist();
    	update_buy_promo_list();
        $('#dual-list-options').find('option').remove();

        $('.list-left ul li').each(function(idx, opt) {
            $('#dual-list-options').append($("<option></option>")
                .attr("value", $(opt).data("value"))
                .text( $(opt).text())
                .prop("selected", "selected")
            ); 
        });
    }
    
    
    $('.dual-list .selector').click(function () {
        var $checkBox = $(this);
        if (!$checkBox.hasClass('selected')) {
            $checkBox.addClass('selected').closest('.well').find('ul li:not(.active)').addClass('active');
            $checkBox.removeClass('glyphicon-unchecked').addClass('glyphicon-check');
        } else {
            $checkBox.removeClass('selected').closest('.well').find('ul li.active').removeClass('active');
            $checkBox.removeClass('glyphicon-check').addClass('glyphicon-unchecked');
        }
    });
    
    
    $('[name="SearchDualList"]').keyup(function (e) {
        var code = e.keyCode || e.which;
        if (code == '9') return;
        if (code == '27') $(this).val(null);
        var $rows = $(this).closest('.dual-list').find('.list-group li');
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
        $rows.show().filter(function () {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });
    
    
    $(".glyphicon-search").on("click", function() {
        $(this).next("input").focus();
    });
    
    
    function sortUnorderedList(ul, sortDescending) {
        $("#" + ul + " li").sort(sort_li).appendTo("#" + ul);
        
        function sort_li(a, b){
            return ($(b).data('value')) < ($(a).data('value')) ? 1 : -1;    
        }
    }
        
    
    $("#dual-list-left li").append(move_right);
    $("#dual-list-right li").append(move_left);
}


	</script>
	
	
<script type='text/javascript'>//<![CDATA[
$(window).load(function(){
var originalLeave = $.fn.popover.Constructor.prototype.leave;
$.fn.popover.Constructor.prototype.leave = function(obj){
  var self = obj instanceof this.constructor ?
    obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
  var container, timeout;

  originalLeave.call(this, obj);

  if(obj.currentTarget) {
    container = $(obj.currentTarget).siblings('.popover')
    timeout = self.timeout;
    container.one('mouseenter', function(){
      //We entered the actual popover – call off the dogs
      clearTimeout(timeout);
      //Let's monitor popover content instead
      container.one('mouseleave', function(){
        $.fn.popover.Constructor.prototype.leave.call(self, self);
      });
    })
  }
};


$('body').popover({ selector: '[data-popover]', trigger: 'click hover',title : '&nbsp <a href="#" class="close" data-dismiss="alert"><i class="fa fa-times" aria-hidden="true"></i></a>',template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div><div class="popover-footer">&nbsp;</div>', placement: 'auto', delay: {show: 0, hide: 400}});

$(document).on("click", ".popover .close" , function(){
        $(this).parents(".popover").popover('hide');
    });
    
    
/*$(function () {
  // Smooth Scroll
  $('a[href*=#]').bind('click', function(e){
    var anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: $(anchor.attr('href')).offset().top
    }, 1000);
    e.preventDefault();
  });
});*/

$('.accordion-3').on('show.bs.collapse', function(n){
  $(n.target).siblings('.panel-heading').find('.panel-title i').toggleClass('fa-minus fa-plus');
});
$('.accordion-3').on('hide.bs.collapse', function(n){
  $(n.target).siblings('.panel-heading').find('.panel-title i').toggleClass('fa-plus fa-minus');
});

});//]]> 

</script>
</div>
</div>
</div>
</div>
</div>
</body>
</html>