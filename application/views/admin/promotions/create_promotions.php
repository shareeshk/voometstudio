<html lang="en">
<head>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/jqueryui/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 
<style>
.bootstrap-datetimepicker-widget{
	z-index:1050
}

</style>

<script type="text/javascript">
    /*$(function () {
        var start_dates=document.getElementById('start_date').value
        start_dates=new Date()
    	var end_dates=document.getElementById('end_date').value
        $('#start_date').datetimepicker({
					format: 'YYYY-MM-DD HH:mm',
                	minDate: new Date(),
                	useCurrent: false,
                });
        $('#end_date').datetimepicker({
            useCurrent: false ,//Important! See issue #1075
			format: 'YYYY-MM-DD HH:mm',
            minDate:new Date(start_dates),

            
        });
        $("#start_date").on("dp.change", function (e) {
            $('#end_date').data("DateTimePicker").minDate(e.date);
        });
        $("#end_date").on("dp.change", function (e) {
            $('#start_date').data("DateTimePicker").maxDate(e.date);
        });
    });*/
	$(document).ready(function (){	
	$('#end_date').bootstrapMaterialDatePicker
			({
				time: true,
				weekStart: 0, 
				format: 'YYYY-MM-DD HH:mm', 
			}).on('change', function(e, date)
			{
				$('#start_date').bootstrapMaterialDatePicker('setMaxDate', date);
			});;
			
		$('#start_date').bootstrapMaterialDatePicker
			({
				time: true,
				weekStart: 0, 
				format: 'YYYY-MM-DD HH:mm',  
				shortTime : true
			}).on('change', function(e, date)
			{
				$('#end_date').bootstrapMaterialDatePicker('setMinDate', date);
			});
			});
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">

<div class="page-header"><h4 class="text-center bold">Create Promotions</h4></div>  
       
<div class="row">
<div class="col-md-12" id="form_feed_div">
	<?php if(!empty($promotion_type)){ ?> 
	<div class="panel panel-success">
	      <div class="panel-heading">Deal Name</div>
	      <div class="panel-body">
	<form method="post" action="<?php echo base_url()?>admin/Promotions/save_promotions" class="form-horizontal" autocomplete="off">
		
	      	<div class="form-group">
				  <label class="control-label col-md-2" for="deal_type">Promotion Type:</label>
				  <div class="col-md-10">
					  <select class="form-control" name="deal_type" id="deal_type" onchange="get_all_promotion_quotes()" required>
					    <option value="" >Choose an option</option>
					    <?php 
					    foreach($promotion_type as $data){ ?>
					    	<option value="<?php echo $data['id']?>" ><?php echo $data['promotion_name']?></option>
					    <?php }?>
					  </select>
				  </div>
			</div>
			<div class="form-group" id="deal_sample_quote_container">
				  <!--<label class="control-label col-sm-2" for="deal_sample_quote">Sample Promotion Quote:</label>
				  <div class="col-sm-10">
					  <select class="form-control" name="deal_sample_quote" id="deal_sample_quote" onchange="" required>
					    <option value="" >Choose</option>
					    
					  </select>
				  </div>-->
			</div>
	      	<div class="form-group">
			    <label class="control-label col-md-2" for="deal_name">Deal Name:</label>
			    <div class="col-md-10">
			      <input type="text" class="form-control" id="deal_name" name="deal_name" required placeholder="Deal Name">
			    </div>
			</div>
			<input type="hidden" id="SKU_PARAMS">
			<div class="form-group" id="display_realted_input_form">
			    <label class="control-label col-md-2" for="email">Deal Offer:</label>
			    <div class="col-md-10" id="single" style="display:none">
			    	<div class="row">
			    		
						<div id="firstWord"></div>

						<div id="firstInput"></div>

						<div id="show_buy_select"></div>
						
			    		<div id="middleWord"></div>
						
	
						<div id="secondInput"></div>
						
						<div id="show_get_select"></div>
						
						<div id="endWord"></div>
			    		
			    		
			    	</div>
			    </div>
			    <div class="col-md-10" id="multiple" style="display:none">
                    <div class="entry row">
                        
						<div class="col-md-1" align="center">
							<label class="control-label" id="firstWord_m"></label>
						</div>
						<div class="col-md-2" id="firstInput_m">
							
						</div>
						<div class="col-md-2" id="show_buy_select_m">
							
						</div>
                        
                        <div class="col-md-1" align="center">
                            <label class="control-label" id="middleWord_m" ></label>
                        </div>
                       
						<div class="col-md-1" id="secondInput_m">
							
						</div>
						<div class="col-md-2" id="show_get_select_m">
							
						</div>
						<div class="col-md-1">
							<label class="control-label" id="endWord_m" ></label>
						</div>
                        
                        <div class="col-md-2 pull-right" id="add_button_tiers">
                            
                        </div>   
                    </div>
                </div>
			    
			</div>
			<div class="row">
				<div class="form-group" id="apply_promotion_at_check_out">
					
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label class="control-label col-sm-4" for="email">Start Date:</label>
						<div class="col-sm-6">
							<input type='text' class="form-control" name="start_date" id='start_date' required />
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label class="control-label col-sm-4" for="email">End Date:</label>
						<div class="col-sm-6">
			                <input type='text' class="form-control" name="end_date" id='end_date' required />
						</div>
					</div>
				</div>
			</div>
			
			<div class="form-group" align="center">
			    <label class="control-label" ><h2><ul id="final_offer_name" class="text-left"></ul></h2></label>
			    <input type="hidden" id="final_offer_value" name="final_offer_value" required>
			    <input type="hidden" id="promotion_dependency" name="promotion_dependency" required>
			</div>
			
			<div class="form-group">
				<div class="col-sm-8 col-sm-offset-4">
			    <button type="submit" class="btn btn-xs btn-success">Save and Proceed</button> <button type="reset" class="btn btn-xs btn-info">Reset</button>
			    </div>
				</div>
				 </form>
			</div>

	      </div>
	    </div>
	
	 
	 <script>
	 
	 var getWord= "\\b"+"get"+"\\b";
	 var middleGetWord="\\b"+"get a surprise gift"+"\\b";
	 var start_word;
	 var middle_word;
	 var sku_Word="\\b"+"select sku"+"\\b";
	 var getSame="\\b"+"same"+"\\b";
     var getDifferent="\\b"+"different"+"\\b";
	  function getSkuResult(same_word){

                if(same_word.match(getSame)){
                    return "(Same SKUs)"
                }
                if(same_word.match(getDifferent)){
                    return "(Different SKUs)"
                }
                else{
                    return ""
                }
            }
	    function getSkuWord(findSku){

                if(findSku.match(sku_Word)){
                    return true
                }
                if(findSku.match(getDifferent)){
                    return false;
                }
            }
	 	function get_all_promotion_quotes(){
	 		cleardata()
	 	var deal_val=document.getElementById('deal_type').value
	    var x = document.getElementById("deal_type");
		var deal_Text=x.options[x.selectedIndex].text
		var parentDiv = document.getElementById('deal_sample_quote_container');
			parentDiv.innerHTML=""
				
		if(deal_val!=""){			
		var xhr = false;
		if (window.XMLHttpRequest) {
		    xhr = new XMLHttpRequest();
		}
		else {
		    xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
									var select_data=JSON.parse(xhr.responseText);
									var labelElement=document.createElement ("label")
										labelElement.classList.add('control-label','col-sm-2');
										labelElement.innerHTML="Select Promo Option"
										parentDiv.appendChild (labelElement);
										
										
										
									if(select_data.length>0){
										var divElement=document.createElement ("div")
										divElement.classList.add('col-sm-10');
										divElement.setAttribute('id','dealQuoteSelect')
										parentDiv.appendChild (divElement);
										
										var selectDiv=document.getElementById('dealQuoteSelect');
										var selectElement = document.createElement ("select");
										selectElement.classList.add("form-control");
										selectElement.setAttribute('id','quote_selector');
										selectElement.setAttribute('onchange','display_realted_input_form()');
										selectElement.name="quote_selector";
										var option = new Option ("Select Option","");
										selectElement.options[selectElement.options.length] = option;
										for (var i=0;i < select_data.length;i++)
										{
										    var option = new Option (select_data[i].promotion_quote,select_data[i].id);
										    selectElement.options[selectElement.options.length] = option;
											if(selectElement.options[i+1].text=="Get A Surprise Gift Above X (Qty or Amnt)" || selectElement.options[i+1].text=="Buy Before X (Date) Get Y Free" || selectElement.options[i+1].text.trim()=="Buy Before X (Date) Get A Surprise Gift"){
												
													//selectElement.options[i+1].disabled=true;
											}
										}
										selectDiv.appendChild (selectElement);
										
										
										}
										
									
									else{

										var labelElement=document.createElement ("label")
										labelElement.classList.add('control-label','col-sm-10');
										labelElement.setAttribute('style','text-align: left')
										labelElement.innerHTML="No products to show1"
										parentDiv.appendChild (labelElement);
									}
								}	
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_promotion_quote", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("deal_val="+deal_val);
		        }
		}
		
		

            
        function getMatchedResult(start_word,getWord){

                if(start_word.match(getWord)){
                    return true
                }
                else{
                    return false
                }
            }
	var buy_type=""	
	var get_type=""    	
    $(function() {
        $(document).on('click', '.btn-add', function(e) {

            e.preventDefault();

            var controlForm = $('#multiple');


            laste=$(this).parents('.entry:last').parent().children().last();
 
            var btnNum= parseInt($(this).attr('num'))

            if(laste.find('input').eq(1).val()!="" && laste.find('input').eq(0).val()!=""){
                
            var btnNum= parseInt($(this).attr('num'))
            var $inputs = $('#multiple .entry:last :input').not(':input[type=button], :input[type=submit], :input[type=reset]');
            var htmls=""
            $inputs.each(function() {
                    if($(this).val()==""){
                         return false;
                    }
            })
            $inputs.each(function() {
            
            
            if($(this).attr('name')=='buy_deal_value[]'){
                htmls+="Buy above "+$(this).val()+" "
            }
            if($(this).attr('name')=='buy_deal_type[]'){
                htmls+=$(this).val()+" "
                buy_type=$(this).val()
            }

            if($(this).attr('name')=='get_deal_value[]'){
                htmls+="And Get "+$(this).val()+"% "
            }
            if($(this).attr('name')=='get_deal_type[]'){
                htmls+=$(this).val()+"<br>"
                get_type=$(this).val()
            }


        });

                
                
                
                currentEntry = $(this).parents('.entry:first');

                newEntry = $(currentEntry.clone()).appendTo(controlForm);
newEntry.find('input').val('');
newEntry.find('select:eq(0)').replaceWith('<input type="text" value="'+buy_type+'" class="form-control" name="buy_deal_type[]" readonly>')
newEntry.find('select[name*="get_deal_type[]"]').replaceWith('<input type="text" value="'+get_type+'" class="form-control" name="get_deal_type[]" readonly>')

$( "input[name*='buy_deal_type[]']" ).val(buy_type);
$( "input[name*='get_deal_type[]']" ).val(get_type);

            controlForm.find('.entry:last .btn-apply').attr('num',btnNum+'_listNUM')
            controlForm.find('.entry:not(:first) .btn-add').removeClass('btn-add').addClass('btn-remove').removeClass('btn-success').addClass('btn-danger').html('<span class="glyphicon glyphicon-minus"></span>').attr('num',btnNum+'_listNUM');
            
         controlForm.find('.entry:last .btn-add').attr('num',btnNum+1) 
            }else{
                alert("Fill input fields First.Then add more fields")
                return false;
            }
        $(this).attr('num',btnNum+1)          
        }).on('click', '.btn-remove', function(e) {

            $(this).parents('.entry:first').remove();
            var targetlist=$(this).attr('num')

            $('#'+targetlist).remove();
            resetFunct()
            //document.getElementById('final_offer_value').value=document.getElementById('final_offer_name').innerHTML
            e.preventDefault();
            return false;
        });
    });
function resetFunct(){
    document.getElementById('final_offer_name').innerHTML=""
    document.getElementById('final_offer_value').value=""
    var btnNum= 0
    var enteryElems=document.getElementsByClassName('entry');
    if(enteryElems.length > 1){
            
            var htmls=""
            var htmlv=""
            var $inputs = $(':input').not(':input[type=button], :input[type=submit], :input[type=reset]');
            $inputs.each(function() {
                
                
                if($(this).attr('name')=='buy_deal_value[]'){
                    if($(this).val()==""){
                         return false;
                    }
                    htmls+='<li class="_listNUM" id="'+btnNum+'_listNUM'+'">'+"Buy above "+$(this).val()+" "
                    htmlv+='Buy above '+$(this).val()+' '
                }
                if($(this).attr('name')=='buy_deal_type[]'){
                    htmls+=$(this).val()+" "
                    buy_type=$(this).val()
                    htmlv+=$(this).val()+"</span> "
                }
    
                if($(this).attr('name')=='get_deal_value[]'){
                    htmls+="And Get "+$(this).val()+"% "
                    htmlv+='And Get '+$(this).val()+'% '
                }
                if($(this).attr('name')=='get_deal_type[]'){
                    htmlv+=$(this).val()+"<br>"
                    htmls+=$(this).val()+'</li>'
                    get_type=$(this).val()
                    //alert(btnNum)
                    $('.btn-remove').eq(btnNum).attr('num',btnNum+1+'_listNUM')
                    $('.btn-apply').eq(btnNum).attr('num',btnNum+'_listNUM')
                    btnNum+=1;
                    
                }
                

            });

            document.getElementById('final_offer_name').innerHTML+=htmls
            document.getElementById('final_offer_value').value+=htmlv
            $(btnNum+'_listNUM').remove()

    }
}    

function apply(obj){
    var btnApply=obj.getAttribute('num')
    //alert(btnApply)
    if(document.getElementById(btnApply)==null){

    var $inputs = $(obj).parent().parent().find(':input').not(':input[type=button], :input[type=submit], :input[type=reset]');
            var htmls=""
            $inputs.each(function() {
                    if($(this).val()==""){
                         return false;
                    }
            })
            $inputs.each(function() {
            
            
            if($(this).attr('name')=='buy_deal_value[]'){
                htmls+='Buy above '+$(this).val()+' '
            }
            if($(this).attr('name')=='buy_deal_type[]'){
                htmls+=$(this).val()+" "
                buy_type=$(this).val()
            }

            if($(this).attr('name')=='get_deal_value[]'){
                htmls+='And Get '+$(this).val()+'% '
            }
            if($(this).attr('name')=='get_deal_type[]'){
                htmls+=$(this).val()+"<br>"
                get_type=$(this).val()
            }
           });
        document.getElementById('final_offer_name').innerHTML+='<li id="'+btnApply+'">'+htmls+'</li>'
            document.getElementById('final_offer_value').value+=htmls
     }
     else{
            var $inputs = $(obj).parent().parent().find(':input').not(':input[type=button], :input[type=submit], :input[type=reset]');
            var htmls=""
            $inputs.each(function() {
                    if($(this).val()==""){
                         return false;
                    }
            })
             $inputs.each(function() {
            
            
            if($(this).attr('name')=='buy_deal_value[]'){
                htmls+="Buy above "+$(this).val()+" "
            }
            if($(this).attr('name')=='buy_deal_type[]'){
                htmls+=$(this).val()+" "
                buy_type=$(this).val()
            }

            if($(this).attr('name')=='get_deal_value[]'){
                htmls+="And Get "+$(this).val()+"% "
            }
            if($(this).attr('name')=='get_deal_type[]'){
                htmls+=$(this).val()+"<br>"
                get_type=$(this).val()
            }
           });
        document.getElementById(btnApply).innerHTML=htmls
        updateValue()
          
     }
}
function updateValue(){
    document.getElementById('final_offer_value').value=""
        var btnNum= 0
    var enteryElems=document.getElementsByClassName('entry');
    if(enteryElems.length > 0){
            
            var htmls=""
            
            var $inputs = $(':input').not(':input[type=button], :input[type=submit], :input[type=reset]');
            $inputs.each(function() {
            
            
            if($(this).attr('name')=='buy_deal_value[]'){
                htmls+='Buy above '+$(this).val()+' '
            }
            if($(this).attr('name')=='buy_deal_type[]'){
                htmls+=$(this).val()+" "
                buy_type=$(this).val()
            }

            if($(this).attr('name')=='get_deal_value[]'){
                htmls+='And Get '+$(this).val()+'% '
            }
            if($(this).attr('name')=='get_deal_type[]'){
                htmls+=$(this).val()+"<br>"
                get_type=$(this).val()
            }
           });
            document.getElementById('final_offer_value').value+=htmls
    }
}
		
function showBuyDealTypem(obj){
    $( "input[name*='buy_deal_type[]']" ).val(obj.value);
    document.getElementById('final_offer_name').innerHTML=""
    document.getElementById('final_offer_value').value=""
    var btnNum= 0
    var enteryElems=document.getElementsByClassName('entry');
    if(enteryElems.length > 1){
            
            var htmls=""
            var htmlv=""
            var $inputs = $(':input').not(':input[type=button], :input[type=submit], :input[type=reset]');
            $inputs.each(function() {
                
                
                if($(this).attr('name')=='buy_deal_value[]'){
                    if($(this).val()==""){
                         return false;
                    }
                    htmls+='<li class="_listNUM" id="'+btnNum+'_listNUM'+'">'+"Buy above "+$(this).val()+" "
                    htmlv+='Buy above '+$(this).val()+' '
                }
                if($(this).attr('name')=='buy_deal_type[]'){
                    htmls+=$(this).val()+" "
                    buy_type=$(this).val()
                    htmlv+=$(this).val()+" "
                }
    
                if($(this).attr('name')=='get_deal_value[]'){
                    htmls+="And Get "+$(this).val()+"% "
                    htmlv+='And Get '+$(this).val()+'% '
                }
                if($(this).attr('name')=='get_deal_type[]'){
                    htmlv+=$(this).val()+"<br>"
                    htmls+=$(this).val()+'</li>'
                    get_type=$(this).val()
                    //alert(btnNum)
                    $('.btn-remove').eq(btnNum).attr('num',btnNum+1+'_listNUM')
                    $('.btn-apply').eq(btnNum).attr('num',btnNum+'_listNUM')
                    btnNum+=1;
                    
                }
                

            });

            document.getElementById('final_offer_name').innerHTML+=htmls
            document.getElementById('final_offer_value').value+=htmlv
            $(btnNum+'_listNUM').remove()

    }
    

}
function showGetDealTypem(obj){
    $( "input[name*='get_deal_type[]']" ).val(obj.value);
    document.getElementById('final_offer_name').innerHTML=""
    document.getElementById('final_offer_value').value=""
    var btnNum= 0
    var enteryElems=document.getElementsByClassName('entry');
     if(enteryElems.length > 1){
            
            var htmls=""
            var htmlv=""
            var $inputs = $(':input').not(':input[type=button], :input[type=submit], :input[type=reset]');
            $inputs.each(function() {
                
                
                if($(this).attr('name')=='buy_deal_value[]'){
                    if($(this).val()==""){
                         return false;
                    }
                    htmls+='<li class="_listNUM" id="'+btnNum+'_listNUM'+'">'+"Buy above "+$(this).val()+" "
                    htmlv+='Buy above '+$(this).val()+''
                }
                if($(this).attr('name')=='buy_deal_type[]'){
                    htmls+=$(this).val()+" "
                    buy_type=$(this).val()
                    htmlv+=$(this).val()+" "
                }
    
                if($(this).attr('name')=='get_deal_value[]'){
                    htmls+="And Get "+$(this).val()+"% "
                    htmlv+='And Get '+$(this).val()+'% '
                }
                if($(this).attr('name')=='get_deal_type[]'){
                    htmlv+=$(this).val()+"<br>"
                    htmls+=$(this).val()+'</li>'
                    get_type=$(this).val()
                    //alert(btnNum)
                    $('.btn-remove').eq(btnNum).attr('num',btnNum+1+'_listNUM')
                    $('.btn-apply').eq(btnNum).attr('num',btnNum+'_listNUM')
                    btnNum+=1;
                    
                }
                

            });

            document.getElementById('final_offer_name').innerHTML+=htmls
            document.getElementById('final_offer_value').value+=htmlv
            $(btnNum+'_listNUM').remove()

    }
}
function restrict_to_below_100Fun(obj){
	if(obj.value>=100){
		alert("Please choose below 100%");
		obj.value="";
		return false;
	}
}
	function isNumberwithdot(evt) {
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode > 31 && ((charCode < 48 || charCode > 57) && charCode!=46)) {
		return false;
	}
	return true;
}	
		function display_realted_input_form(){
		    $('#multiple').find('.entry:not(:first)').remove();
			cleardata()
			document.getElementById('apply_promotion_at_check_out').innerHTML=""
			if(document.getElementById('buy_deal_type')!=null){
				document.getElementById('buy_deal_type').value=""
			}
			if(document.getElementById('get_deal_type')!=null){
				document.getElementById('get_deal_type').value=""
			}

			//document.getElementById('multiple').innerHTML=""
			
		 	var quote_selector_val=document.getElementById('quote_selector').value
		    var x = document.getElementById("quote_selector");
			var quote_selector_Text=x.options[x.selectedIndex].text
			var parentDiv = document.getElementById('display_realted_input_form');
			//parentDiv.innerHTML=""
					
			if(quote_selector_val!=""){			
			var xhr = false;
			if (window.XMLHttpRequest) {
			    xhr = new XMLHttpRequest();
			}
			else {
			    xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			 var xhr = new XMLHttpRequest();
					
					if (xhr) {
			            xhr.onreadystatechange = function () {
			                if (xhr.readyState == 4 && xhr.status == 200) {
								select_data=JSON.parse(xhr.responseText)
								
								var buy_input_arr=select_data[0]
								var buy_select_arr=select_data[1]

								for (var i=0;i < buy_input_arr.length;i++){
								    var discount_word=buy_input_arr[i].promotion_quote.toLowerCase()
								    document.getElementById('promotion_dependency').value=buy_input_arr[i].promotion_dependency
								    var reverse_select_input
                                    if(getdiscountResult(discount_word)){
                                        reverse_select_input=1;
                                    }
                                    else{
                                        reverse_select_input=0;
                                    }
									
									var singleItemDiscount
									//alert(discount_word);
									if(getDiscountForSingle(discount_word)){
										singleItemDiscount=0;
									}else{
										singleItemDiscount=1;
									}
									var same_word=buy_input_arr[i].promotion_quote.toLowerCase() ;

									document.getElementById('SKU_PARAMS').value= getSkuResult(same_word)
									   
									middle_word=buy_input_arr[i].middle_word.toLowerCase() 
									start_word=buy_input_arr[i].promotion_quote.toLowerCase()
									getWord= "\\b"+"tiered discount"+"\\b";
									if(getMatchedResult(start_word,getWord)){
									var show_get_select=buy_input_arr[i].show_get_select
                                    document.getElementById('firstWord_m').innerHTML=buy_input_arr[i].start_word
                                    document.getElementById('middleWord_m').innerHTML=buy_input_arr[i].middle_word 
                                    document.getElementById('endWord_m').innerHTML=buy_input_arr[i].end_word
									
									    document.getElementById('multiple').style.display=""
									    document.getElementById('single').style.display="none"
										document.getElementById('add_button_tiers').innerHTML="";
									    var parentdivforTierBtn=document.getElementById('add_button_tiers');
									    var appendBtn=document.createElement('button');
                                           appendBtn.classList.add('btn-sm','btn','btn-success','btn-apply','pull-left');
                                           appendBtn.setAttribute('num','0_listNUM');
                                           
                                           appendBtn.setAttribute('onclick','apply(this)');
                                           appendBtn.innerHTML='<span class="glyphicon glyphicon-ok"></span>';
                                           appendBtn.type="button";
                                           parentdivforTierBtn.appendChild(appendBtn)
									    var appendBtn=document.createElement('button');
									       appendBtn.classList.add('btn-sm','btn','btn-success','btn-add');
									       appendBtn.setAttribute('num','1');
									       appendBtn.innerHTML='<span class="glyphicon glyphicon-plus"></span>';
									       appendBtn.type="button";
									       appendBtn.setAttribute('style','margin-left:5px');
									       parentdivforTierBtn.appendChild(appendBtn)
									    
									                       
									    parDivFirstInput=document.getElementById('firstInput_m')
                                    parDivFirstInput.innerHTML=""
                                    if(buy_input_arr[i].input_place_for_buy==1){
                                        var inputElem=document.createElement('input')
                                        inputElem.type="number"
                                        inputElem.classList.add("form-control");
                                        /*inputElem.setAttribute('id','buy_deal_value');*/
                                        inputElem.setAttribute('data_type',buy_input_arr[i].input_type_for_buy);
                                        //inputElem.setAttribute('onkeyup','showBuyDealValuem()');
                                        inputElem.name="buy_deal_value[]";
                                        inputElem.setAttribute('required',true);
                                        parDivFirstInput.appendChild (inputElem);
                                        if(buy_input_arr[i].input_type_for_buy=="date"){
                                           /* $('#buy_deal_value').datetimepicker({
                                                format: 'YYYY-MM-DD HH:mm',
                                                minDate: new Date(),
                                                useCurrent: false,
                                            });
                                            
                                            $("#buy_deal_value").on("dp.change", function (e) {
                                                document.getElementById('end_date').value=$("#buy_deal_value").val();
                                                $('#start_date').data("DateTimePicker").maxDate(e.date);
                                                putFinalDeal()
                                            });
											*/
											$('#buy_deal_value').bootstrapMaterialDatePicker({
												time: true,
												weekStart: 0, 
												format: 'YYYY-MM-DD HH:mm', 
												shortTime : true
											 }).on('change', function(e, date){
												document.getElementById('end_date').value=$("#buy_deal_value").val();
												//$('#start_date').data("DateTimePicker").maxDate(e.date);
												$('#start_date').bootstrapMaterialDatePicker('setMaxDate', date);
												putFinalDeal()
											});
											
                                            document.getElementById('end_date').readOnly=true
                                        }
                                        else{
                                            document.getElementById('end_date').readOnly=false
                                        }
                                    }
                                    else{
                                        parDivFirstInput.innerHTML=""
                                    }
                                    
                                    parDivsecondInput=document.getElementById('secondInput_m')
                                    parDivsecondInput.innerHTML=""
                                    if(buy_input_arr[i].input_place_for_get==1){
                                        var inputElem=document.createElement('input')
                                        inputElem.type=buy_input_arr[i].input_type_for_get
                                        inputElem.classList.add("form-control");
                                        appendBtn.setAttribute('required',true);
                                        /*inputElem.setAttribute('id','get_deal_value');*/
                                        //inputElem.setAttribute('onkeyup','showGetDealValuem()');
                                        inputElem.name="get_deal_value[]";
                                        inputElem.setAttribute('required',true);
										inputElem.setAttribute('onkeypress','return isNumberwithdot(event);');
										inputElem.setAttribute('onkeyup','return restrict_to_below_100Fun(this);');
										
                                        parDivsecondInput.appendChild (inputElem);
                                        
                                    }
                                    else{
                                        parDivsecondInput.innerHTML=""
                                    }
                                    
                                    parDivshow_buy_select=document.getElementById('show_buy_select_m')
                                    parDivshow_buy_select.innerHTML=""
                                    if(buy_input_arr[i].show_buy_select==1){
                                        var selectElement = document.createElement ("select");
                                        selectElement.classList.add("form-control");
                                        selectElement.setAttribute('id','buy_deal_type');
                                        selectElement.setAttribute('onchange','showBuyDealTypem(this)');
                                        selectElement.name="buy_deal_type[]";
                                        selectElement.required=true;
                                        parDivshow_buy_select.appendChild (selectElement);

                                        var buy_select_arr=select_data[1]
                                        buySelectElement=document.getElementById('buy_deal_type')
                                        buySelectElement.innerHTML=""
                                        var option = new Option ("Select Type","");
                                        buySelectElement.options[buySelectElement.options.length] = option;
                                        var prev="";
                                        for (var i=0;i < buy_select_arr.length;i++)
                                        {
                                            if (prev != buy_select_arr[i].buy_units) {
                                                var option = new Option(buy_select_arr[i].buy_units, buy_select_arr[i].buy_units);
                                                buySelectElement.options[buySelectElement.options.length] = option;
                                            }
                                            prev=buy_select_arr[i].buy_units;
                                        }
                                    }
                                    else{
                                        parDivshow_buy_select.innerHTML=""
                                    }
                                    
                                    parDivshow_get_select=document.getElementById('show_get_select_m')
                                    parDivshow_get_select.innerHTML=""
                                    if(show_get_select==1){
                                        var selectElement = document.createElement ("select");
                                        selectElement.classList.add("form-control");
                                        selectElement.setAttribute('id','get_deal_type');
                                        selectElement.setAttribute('onchange','showGetDealTypem(this)');
                                        selectElement.name="get_deal_type[]";
                                        selectElement.required=true;
                                        parDivshow_get_select.appendChild (selectElement);
                                        
                                        getSelectElement=document.getElementById('get_deal_type')
                                        getSelectElement.innerHTML=""
                                        var option = new Option ("Select Type","");
                                        getSelectElement.options[getSelectElement.options.length] = option;
                                        for (var i=0;i < buy_select_arr.length;i++)
                                        {
                                            var option = new Option (buy_select_arr[i].give_units,buy_select_arr[i].give_units);
                                            getSelectElement.options[getSelectElement.options.length] = option;
                                        }
                                    }
                                    else{
                                        parDivshow_get_select.innerHTML=""
                                    }
									   
							        if(buy_select_arr==false){
                                         putFinalDeal()
                                     } 
									   
									  return false;  
									}
						
						var show_get_select=buy_input_arr[i].show_get_select
                        document.getElementById('firstWord').innerHTML='<div class="col-md-1" align="center"><label class="control-label" >'+buy_input_arr[i].start_word+'</label></div>';
						
                        document.getElementById('middleWord').innerHTML='<div class="col-md-1" align="center"><label class="control-label">'+buy_input_arr[i].middle_word+'</label></div>'; 
						
                        document.getElementById('endWord').innerHTML='<div class="col-md-1"><label class="control-label">'+buy_input_arr[i].end_word+'</div></div>';
						
						document.getElementById('multiple').style.display="none"
                        document.getElementById('single').style.display=""
                        			
						document.getElementById('add_button_tiers').innerHTML=""			
									
						start_word=buy_input_arr[i].start_word.toLowerCase()

						var getWord= "\\b"+"get"+"\\b"
						middle_word=buy_input_arr[i].middle_word.toLowerCase() 
						
						if(getMatchedResult(start_word,getWord)){

							parDivFirstInput=document.getElementById('firstInput')
								parDivFirstInput.innerHTML=""
								if(buy_input_arr[i].input_place_for_buy==1){
									
									var inputElem=document.createElement('input')
									inputElem.type="text"
									inputElem.classList.add("form-control");
									inputElem.setAttribute('id','get_deal_value');
									inputElem.setAttribute('data_type',buy_input_arr[i].input_type_for_buy);
									inputElem.setAttribute('onkeyup','showGetDealValue()');
									inputElem.name="get_deal_value";
									inputElem.setAttribute('required',true);
									//parDivFirstInput.appendChild (inputElem);
									
									firstInputHtml='<div class="col-md-2"><input class="form-control" id="get_deal_value" data_type="'+buy_input_arr[i].input_type_for_buy+'" onkeyup="showGetDealValue()" name="get_deal_value" required="true" type="text"></div>';
									parDivFirstInput.innerHTML=firstInputHtml;
									
									if(buy_input_arr[i].input_type_for_buy=="date"){
										/*$('#buy_deal_value').datetimepicker({
											format: 'YYYY-MM-DD HH:mm',
						                	minDate: new Date(),
						                	useCurrent: false,
						                });
						               
						                $("#buy_deal_value").on("dp.change", function (e) {
								            document.getElementById('end_date').value=$("#buy_deal_value").val();
								            $('#start_date').data("DateTimePicker").maxDate(e.date);
								            putFinalDeal()
								        });
										*/
										
										$('#buy_deal_value').bootstrapMaterialDatePicker({
												time: true,
												weekStart: 0, 
												format: 'YYYY-MM-DD HH:mm', 
												shortTime : true
											 }).on('change', function(e, date){
												document.getElementById('end_date').value=$("#buy_deal_value").val();
												//$('#start_date').data("DateTimePicker").maxDate(e.date);
												$('#start_date').bootstrapMaterialDatePicker('setMaxDate', date);
												putFinalDeal()
											});
										
						                document.getElementById('end_date').readOnly=true
									}
									else{
										document.getElementById('end_date').readOnly=false
									}
								}
								else{
									parDivFirstInput.innerHTML=""
								}
								
								parDivsecondInput=document.getElementById('secondInput')
								parDivsecondInput.innerHTML=""
								if(buy_input_arr[i].input_place_for_get==1){
									var inputElem=document.createElement('input')
									inputElem.type=buy_input_arr[i].input_type_for_get
									inputElem.classList.add("form-control");
									inputElem.setAttribute('id','buy_deal_value');
									inputElem.setAttribute('onkeyup','showBuyDealValue()');
									inputElem.name="buy_deal_value";
									inputElem.setAttribute('required',true);
									if(buy_input_arr[i].input_type_for_buy=="date"){
										inputElem.setAttribute('style','display:none')
										
										secondInputHtml='<input class="form-control" style="display:none;" id="buy_deal_value" onkeyup="showBuyDealValue()" name="buy_deal_value" required="true" type="'+buy_input_arr[i].input_type_for_get+'">';
										parDivsecondInput.innerHTML=secondInputHtml;
									}
									else{
										inputElem.setAttribute('style','display:')
										secondInputHtml='<div class="col-md-2"><input class="form-control" id="buy_deal_value" onkeyup="showBuyDealValue()" name="buy_deal_value" required="true" type="'+buy_input_arr[i].input_type_for_get+'"></div>';
										parDivsecondInput.innerHTML=secondInputHtml;
									}
									//parDivsecondInput.appendChild (inputElem);
									
									
									
								}
								else{
									parDivsecondInput.innerHTML=""
								}
								
								parDivshow_buy_select=document.getElementById('show_buy_select')
								parDivshow_buy_select.innerHTML=""
								if(buy_input_arr[i].show_buy_select==1){
									var selectElement = document.createElement ("select");
									selectElement.classList.add("form-control");
									selectElement.setAttribute('id','get_deal_type');
									selectElement.setAttribute('onchange','showGetDealType()');
									selectElement.name="get_deal_type";
									selectElement.required=true;
									//parDivshow_buy_select.appendChild (selectElement);
									
									
									ShowBySelectHtml='<div class="col-md-3"><select class="form-control" id="get_deal_type"  onchange="showGetDealType()" name="get_deal_type" required="true" ></select></div>';
									parDivshow_buy_select.innerHTML=ShowBySelectHtml;
									
									
									var buy_select_arr=select_data[1]
									buySelectElement=document.getElementById('get_deal_type')
									buySelectElement.innerHTML=""
									var option = new Option ("Select Type","");
									buySelectElement.options[buySelectElement.options.length] = option;
									for (var i=0;i < buy_select_arr.length;i++)
									{
									    var option = new Option (buy_select_arr[i].buy_units,buy_select_arr[i].buy_units);
									    buySelectElement.options[buySelectElement.options.length] = option;
									}
								}
								else{
									parDivshow_buy_select.innerHTML=""
								}
								
								parDivshow_get_select=document.getElementById('show_get_select')
								parDivshow_get_select.innerHTML=""
								if(show_get_select==1){
									var selectElement = document.createElement ("select");
									selectElement.classList.add("form-control");
									selectElement.setAttribute('id','buy_deal_type');
									selectElement.setAttribute('onchange','showBuyDealType()');
									selectElement.name="buy_deal_type";
									selectElement.required=true;
									//parDivshow_get_select.appendChild (selectElement);
									
									ShowBySelectHtml='<div class="col-md-2"><select class="form-control" id="buy_deal_type"  onchange="showBuyDealType()" name="buy_deal_type" required="true" ></select></div>';
									parDivshow_get_select.innerHTML=ShowBySelectHtml;
									
									
									getSelectElement=document.getElementById('buy_deal_type')
									getSelectElement.innerHTML=""
									var option = new Option ("Select Type","");
									getSelectElement.options[getSelectElement.options.length] = option;
									for (var i=0;i < buy_select_arr.length;i++)
									{
									    var option = new Option (buy_select_arr[i].give_units,buy_select_arr[i].give_units);
									    getSelectElement.options[getSelectElement.options.length] = option;
									}
								}
								else{
									parDivshow_get_select.innerHTML=""
								}

							}
							else{
							    
							    var same_word=buy_input_arr[i].promotion_quote.toLowerCase() ;
                                   parDivFirstInput=document.getElementById('firstInput')
                                   parDivFirstInput.innerHTML=""
                                    if(getSkuWord(same_word)){          
                                        var inputElem=document.createElement('input')
                                        inputElem.type="hidden"
                                        inputElem.classList.add("form-control");
                                        inputElem.required='true';
                                        inputElem.name="buy_deal_value";
                                        inputElem.setAttribute('required',true);
                                        inputElem.value=1;
                                       // parDivFirstInput.appendChild (inputElem);
                                       
									   firstInputHtml='<input class="form-control" name="buy_deal_value" required="true" type="hidden" value="1">';
										parDivFirstInput.innerHTML=firstInputHtml;
										
                                    }else{

                                    if(buy_input_arr[i].input_place_for_buy==1){
										
                                        var inputElem=document.createElement('input')
                                        inputElem.type="text"
                                        inputElem.classList.add("form-control");
                                        inputElem.setAttribute('id','buy_deal_value');
                                        inputElem.setAttribute('data_type',buy_input_arr[i].input_type_for_buy);
                                        inputElem.setAttribute('onkeyup','showBuyDealValue()');
                                        inputElem.name="buy_deal_value";
										if(singleItemDiscount==1){
											
											inputElem.type="number"
											inputElem.setAttribute("min","1");
											
											firstInputHtml='<div class="col-md-2"><input class="form-control" id="buy_deal_value" data_type="'+buy_input_arr[i].input_type_for_buy+'" onkeyup="showBuyDealValue()" name="buy_deal_value" required="true" type="number" min="1"></div>';
											parDivFirstInput.innerHTML=firstInputHtml;
										}else{
											firstInputHtml='<div class="col-md-2"><input class="form-control" id="buy_deal_value" data_type="'+buy_input_arr[i].input_type_for_buy+'" onkeyup="showBuyDealValue()" name="buy_deal_value" required="true" type="text"></div>';
											parDivFirstInput.innerHTML=firstInputHtml;
										}
                                        inputElem.setAttribute('required',true);
                                        //parDivFirstInput.appendChild (inputElem);

										
                                        if(buy_input_arr[i].input_type_for_buy=="date"){
											
											firstInputHtml='<div class="col-md-2"><input class="form-control" id="buy_deal_value" data_type="'+buy_input_arr[i].input_type_for_buy+'" onkeyup="showBuyDealValue()" name="buy_deal_value" required="true" type="text"></div>';
											parDivFirstInput.innerHTML=firstInputHtml;
											
											inputElem.type="text"
											inputElem.removeAttribute("min")
											
                                            /*$('#buy_deal_value').datetimepicker({
                                                format: 'YYYY-MM-DD HH:mm',
                                                minDate: new Date(),
                                                useCurrent: false,
                                            });
                                            
                                            $("#buy_deal_value").on("dp.change", function (e) {
                                                document.getElementById('end_date').value=$("#buy_deal_value").val();
                                                $('#start_date').data("DateTimePicker").maxDate(e.date);
                                                putFinalDeal()
                                            });
											*/
											
											$('#buy_deal_value').bootstrapMaterialDatePicker({
												time: true,
												weekStart: 0, 
												format: 'YYYY-MM-DD HH:mm', 
												shortTime : true
											 }).on('change', function(e, date){
												document.getElementById('end_date').value=$("#buy_deal_value").val();
												//$('#start_date').data("DateTimePicker").maxDate(e.date);
												$('#start_date').bootstrapMaterialDatePicker('setMaxDate', date);
												putFinalDeal()
											});
                                            document.getElementById('end_date').readOnly=true
                                        }
                                        else{
                                            document.getElementById('end_date').readOnly=false
                                        }
                                    }
                                    else{
                                        parDivFirstInput.innerHTML=""
                                    }
                                   }
									
									parDivsecondInput=document.getElementById('secondInput')
									parDivsecondInput.innerHTML=""
									if(buy_input_arr[i].input_place_for_get==1){
										var inputElem=document.createElement('input')
										inputElem.type=buy_input_arr[i].input_type_for_get
										inputElem.classList.add("form-control");
										inputElem.setAttribute('id','get_deal_value');
										inputElem.setAttribute('onkeyup','showGetDealValue()');
										inputElem.name="get_deal_value";
										inputElem.setAttribute('required',true);
										/*if(buy_input_arr[i].input_type_for_buy=="date"){
											inputElem.setAttribute('style','display:none')
										}
										else{
											inputElem.setAttribute('style','display:')
										}*/
										//parDivsecondInput.appendChild (inputElem);
										
										secondInputHtml='<div class="col-md-2"><input class="form-control" id="get_deal_value" onkeyup="showGetDealValue()" name="get_deal_value" required="true" type="'+buy_input_arr[i].input_type_for_get+'"></div>';
										parDivsecondInput.innerHTML=secondInputHtml;
										
									}
									else{
										parDivsecondInput.innerHTML=""
									}
									
									parDivshow_buy_select=document.getElementById('show_buy_select')
									parDivshow_buy_select.innerHTML=""
									if(buy_input_arr[i].show_buy_select==1){
										var selectElement = document.createElement ("select");
										selectElement.classList.add("form-control");
										selectElement.setAttribute('id','buy_deal_type');
										selectElement.setAttribute('onchange','showBuyDealType()');
										selectElement.name="buy_deal_type";
										selectElement.required=true;
										parDivshow_buy_select.appendChild (selectElement);
										
										ShowBySelectHtml='<div class="col-md-3"><select class="form-control" id="buy_deal_type"  onchange="showBuyDealType()" name="buy_deal_type" required="true" ></select></div>';
										parDivshow_buy_select.innerHTML=ShowBySelectHtml;
										
										var buy_select_arr=select_data[1]
										buySelectElement=document.getElementById('buy_deal_type')
										buySelectElement.innerHTML=""
										var option = new Option ("Select Type","");
										buySelectElement.options[buySelectElement.options.length] = option;
										for (var i=0;i < buy_select_arr.length;i++)
										{
										    var option = new Option (buy_select_arr[i].buy_units,buy_select_arr[i].buy_units);
										    buySelectElement.options[buySelectElement.options.length] = option;
										}
									}
									else{
										parDivshow_buy_select.innerHTML=""
									}
									
									parDivshow_get_select=document.getElementById('show_get_select')
									parDivshow_get_select.innerHTML=""
									if(show_get_select==1){
									    if(reverse_select_input==0){
									        var selectElement = document.createElement ("select");
                                            selectElement.classList.add("form-control");
                                            selectElement.setAttribute('id','get_deal_type');
                                            selectElement.setAttribute('onchange','showGetDealType()');
                                            selectElement.name="get_deal_type";
                                            selectElement.required=true;
                                            //parDivshow_get_select.appendChild (selectElement);
											
											ShowBySelectHtml='<div class="col-md-2"><select class="form-control" id="get_deal_type"  onchange="showGetDealType()" name="get_deal_type" required="true" ></select></div>';
											parDivshow_get_select.innerHTML=ShowBySelectHtml;
                                            
                                            getSelectElement=document.getElementById('get_deal_type')
                                            getSelectElement.innerHTML=""
                                            var option = new Option ("Select Type","");
                                            getSelectElement.options[getSelectElement.options.length] = option;
                                            for (var i=0;i < buy_select_arr.length;i++)
                                            {
                                                var option = new Option (buy_select_arr[i].give_units,buy_select_arr[i].give_units);
                                                getSelectElement.options[getSelectElement.options.length] = option;
                                            }
									    }
									    if(reverse_select_input==1){
									        var selectElement = document.createElement ("select");
                                            selectElement.classList.add("form-control");
                                            selectElement.setAttribute('id','get_deal_type');
                                            selectElement.setAttribute('onchange','showGetDealType()');
                                            selectElement.name="get_deal_type";
                                            selectElement.required=true;
                                           // parDivshow_get_select.appendChild (selectElement);
											
											ShowBySelectHtml='<div class="col-md-2"><select class="form-control" id="get_deal_type"  onchange="showGetDealType()" name="get_deal_type" required="true" ></select></div>';
											parDivshow_get_select.innerHTML=ShowBySelectHtml;
											
                                            var $select = $("#get_deal_type");
                                            for (i=1;i<=99;i++){
                                                $select.append($('<option></option>').val(i+'%').html(i+'%'))
                                            }
									    }
										
									}
									else{
										parDivshow_get_select.innerHTML=""
									}
									
									if(getSkuWord(same_word)){
                                        parDivshow_get_select=document.getElementById('show_buy_select')
                                        parDivshow_get_select.innerHTML=""
                                        var inputElem=document.createElement('input')
                                        inputElem.type="hidden"
                                        inputElem.classList.add("form-control");
                                        inputElem.required='true';
                                        inputElem.name="buy_deal_type";
                                        inputElem.value="Qty";
                                        parDivshow_get_select.appendChild (inputElem);
                                        }
								}		
							}
							
							if(buy_select_arr==false){
                                    putFinalDeal()
                                }
							
			              }
			            }
			            xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_promotion_quote_variables", true);
			            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			            xhr.send("quote_selector_val="+quote_selector_val);
			        }
			}
		}
		
/* zsdcfsd zdcfsrf asefwsf*/

			var buydealval,buydealtype,getdealval,getdealtype
			
			function cleardata(){
			if(document.getElementById('firstWord')!=null){
				document.getElementById('firstWord').innerHTML=""
			}
			
			if(document.getElementById('firstWord')!=null){
				document.getElementById('firstInput').innerHTML=""
			}
			
			if(document.getElementById('firstWord')!=null){
				document.getElementById('show_buy_select').innerHTML=""
			}
			
			if(document.getElementById('firstWord')!=null){
				document.getElementById('middleWord').innerHTML=""
			}
			
			if(document.getElementById('firstWord')!=null){
				document.getElementById('secondInput').innerHTML=""
			}
			
			if(document.getElementById('firstWord')!=null){
				document.getElementById('show_get_select').innerHTML=""
			}
			
			if(document.getElementById('firstWord')!=null){
				document.getElementById('endWord').innerHTML=""
			}
			
			if(document.getElementById('firstWord')!=null){
				document.getElementById('final_offer_name').innerHTML=""
			}
			
			if(document.getElementById('firstWord')!=null){
				document.getElementById('final_offer_value').value=""
			}
			document.getElementById('start_date').value=""
			document.getElementById('end_date').value=""
			buydealval="";buydealtype="";getdealval="";getdealtype="";
		}
		/*function showBuyDealValuem(){}
		function showBuyDealTypem(){}
		function showGetDealValuem(){}
		function putFinalDealm(){}*/	
				function showBuyDealValue(){
					if(document.getElementById('buy_deal_value')!=null){
						if(document.getElementById('buy_deal_value').value!=""){
							if(document.getElementById('buy_deal_value').getAttribute('data_type')=="date"){
								var m = moment(document.getElementById('buy_deal_value').value);
								buydealval=m.format('ddd, D MMM YYYY H:mm');
							}
							else{
								buydealval=document.getElementById('buy_deal_value').value;
							}
							putFinalDeal()
						}
					}
				}
				var getWords= "\\b"+"shipping"+"\\b";
				
				function getShippingResult(shipping_word){
                    if(shipping_word.match(getWords)){
                        return true
                    }
                    else{
                        return false
                    }
                }
                var discWords= "\\b"+"x% off"+"\\b";
                function getdiscountResult(discount_word){
                    if(discount_word.match(discWords)){
                        return true
                    }
                    else{
                        return false
                    }
                }
				
				var singleDiscount="\\b"+"straight x% off"+"\\b";
                function getDiscountForSingle(discount_word){
                    if(discount_word.match(singleDiscount)){
                        return true
                    }
                    else{
                        return false
                    }
                }
               /* var percentages="%";
				function hasPercentage(percent_val){
                    if(percent_val.match(percentages)){
                        return true
                    }
                    else{
                        return false
                    }
                }*/
				function showBuyDealType(){
					var parent_div_c=document.getElementById('apply_promotion_at_check_out')

					parent_div_c.innerHTML=""
					if(document.getElementById('buy_deal_type')!=null){
						if(document.getElementById('buy_deal_type').value!=""){
							if(document.getElementById('buy_deal_type').value=="<?php echo curr_code; ?>"){
                                
								var labelElemp=document.createElement('label')
								labelElemp.classList.add('control-label','col-sm-2');
								labelElemp.innerHTML="Apply At:"
								labelElemp.setAttribute('for','apply check')
								parent_div_c.appendChild(labelElemp);
								
								var divElemp=document.createElement('div')
								divElemp.classList.add('col-sm-10');
								divElemp.setAttribute('id','apply_at_check')
								parent_div_c.appendChild(divElemp);
								
								var parent_div=document.getElementById('apply_at_check')
								
								buydealtype=document.getElementById('buy_deal_type').value;
								var labelElement=document.createElement ("label")
								labelElement.classList.add('checkbox-inline');
								labelElement.setAttribute('style','padding-left: 0px;')
								labelElement.setAttribute('id',"checkbox_inline_at_promo1");
								parent_div.appendChild (labelElement);
								var checkElem=document.createElement("input")
									checkElem.type = "radio";
									checkElem.name="promotion_at_check_out"
									checkElem.value="1";
									checkElem.setAttribute("checked",true)
									checkElem.setAttribute("onchange","changePromoApplyTo(this)");
								document.getElementById('checkbox_inline_at_promo1').appendChild(checkElem);
								document.getElementById('checkbox_inline_at_promo1').appendChild(document.createTextNode(" Applied To Invoices"));

								
								/*var labelElement=document.createElement ("label")
								labelElement.classList.add('checkbox-inline');
								labelElement.setAttribute('style','padding-left: 0px;')
								labelElement.setAttribute('id',"checkbox_inline_at_promo2");
								parent_div.appendChild (labelElement);
								
								var checkElem=document.createElement("input")
									checkElem.type = "radio";
									checkElem.name="promotion_at_check_out";

									checkElem.value="0";
									checkElem.setAttribute("onchange","changePromoApplyTo(this)");
									document.getElementById('checkbox_inline_at_promo2').appendChild(checkElem);
									document.getElementById('checkbox_inline_at_promo2').appendChild(document.createTextNode(" Applied To SKUs "));
									*/
								var x = document.getElementById("quote_selector");
                                var shippingText=x.options[x.selectedIndex].text
                                var shipping_word=shippingText.toLowerCase() ;

                                if(getShippingResult(shipping_word)){
                                    document.getElementById('apply_promotion_at_check_out').style.display='none'
                                }
                                else{
                                    document.getElementById('apply_promotion_at_check_out').style.display=''
                                }

									
							}
							else{
								buydealtype=document.getElementById('buy_deal_type').value;
							}
							
							//getdealtype=buydealtype
							putFinalDeal()
						}
					}
					if(document.getElementById('get_deal_type')!=null){
						//document.getElementById('get_deal_type').value=buydealtype
						
					}
					
				}
				function changePromoApplyTo(obj){
                    putFinalDeal()
                }
				function showGetDealValue(){
					if(document.getElementById('get_deal_value')!=null){
						if(document.getElementById('get_deal_value').value){
							getdealval=document.getElementById('get_deal_value').value;
							
							    /*var x = document.getElementById("quote_selector");
                                var discountText=x.options[x.selectedIndex].text
                                var discount_word=discountText.toLowerCase() ;

                                if(getdiscountResult(discount_word)){
                                    percent_val=document.getElementById('get_deal_value').value

                                    if(hasPercentage(percent_val)){
                                        getdealval=document.getElementById('get_deal_value').value;
                                        document.getElementById('get_deal_value').value=getdealval
                                    }else{
                                        getdealval=document.getElementById('get_deal_value').value+"%";
                                        document.getElementById('get_deal_value').value=getdealval
                                    }

                                }
                                else{
                                    getdealval=document.getElementById('get_deal_value').value;
                                }*/
							
							
						    putFinalDeal()
						}
					}
				}
				function showGetDealType(){
					if(document.getElementById('get_deal_type')!=null){
						if(document.getElementById('get_deal_type').value!=""){
							getdealtype=document.getElementById('get_deal_type').value;
							if(document.getElementById('buy_deal_value')!=null){
								if(document.getElementById('buy_deal_value').getAttribute('data_type')=="date"){
								var get_deal_type=document.getElementById('get_deal_type').value
								if(document.getElementById('get_deal_type').value=="cash back"){
                                                                   
								 //document.getElementById('get_deal_value').setAttribute("style","")
    								var secondInput=document.getElementById('secondInput')
    								secondInput.innerHTML=""
    								var selectElement = document.createElement ("select");
                                    selectElement.classList.add("form-control");
                                    selectElement.setAttribute('id','get_deal_value');
                                    selectElement.setAttribute('onchange','showGetDealValue()');
                                    selectElement.name="get_deal_value";
                                    selectElement.required=true;
                                    secondInput.appendChild (selectElement);
									
									secondInputHtml='<div class="col-md-2"><select id="get_deal_value" onchange="showGetDealValue()" class="form-control" name="get_deal_value" required="true" required="true" ></select></div>';
									secondInput.innerHTML=secondInputHtml;
										
                                    var $select = $("#get_deal_value");
                                    for (i=1;i<=99;i++){
                                        $select.append($('<option></option>').val(i+'%').html(i+'%'))
                                    }
                                    for(var i = 0; i < $select.length; i++) {
                                      $select[i].selectedIndex =0;
                                    } 								
								}
								else
								if(get_deal_type=="<?php echo curr_code; ?>" || get_deal_type=="Qty" || get_deal_type=="Units"||get_deal_type== "SKU Free !" ) {
								    
								    document.getElementById('secondInput').innerHTML=""
                                parDivsecondInput=document.getElementById('secondInput')
                                    parDivsecondInput.innerHTML=""
                                        var inputElem=document.createElement('input')
                                        inputElem.type="text"
                                        inputElem.classList.add("form-control");
                                        inputElem.setAttribute('id','get_deal_value');
                                        inputElem.setAttribute('onkeyup','showGetDealValue()');
                                        inputElem.name="get_deal_value";
                                        inputElem.setAttribute('required',true);
                                       // parDivsecondInput.appendChild (inputElem);
										
										secondInputHtml='<div class="col-md-2"><input class="form-control" id="get_deal_value" onkeyup="showGetDealValue()" name="get_deal_value" required="true" type="text"></div>';
										parDivsecondInput.innerHTML=secondInputHtml;
								    
								//document.getElementById('get_deal_value').setAttribute("style","")
								}else{
                                                                    console.log(get_deal_type+"get_deal_type");
									document.getElementById('get_deal_value').setAttribute("style","display:none")
									document.getElementById('get_deal_value').removeAttribute('required');
									document.getElementById('get_deal_value').value=""
									getdealval=""
								}
							  }
							}
							
							putFinalDeal()	
						}
					}
					
				}
				function putFinalDeal(){
				    
				    if(document.getElementById('buy_deal_value')!=null){
				        if(document.getElementById('buy_deal_value').getAttribute('data_type')=="date"){
                            var m = moment(document.getElementById('buy_deal_value').value);
                            buydealval=m.format('ddd, D MMM YYYY H:mm') +" hrs";
                        }
                        else{
                            buydealval=document.getElementById('buy_deal_value').value;
                        }
				    }
						
						var htmls=""
						
						
						if(getMatchedResult(start_word,getWord)){
							
							if(document.getElementById('firstWord')!=null){
								htmls+=document.getElementById('firstWord').innerText+" ("
							}
							if(document.getElementById('get_deal_value')!=null){
								if(getdealval!=""){
									htmls+=getdealval+" "
								}
							}
							if(document.getElementById('get_deal_type')!=null){
								htmls+=getdealtype+") "
							}
							if(document.getElementById('middleWord')!=null){
								htmls+=document.getElementById('middleWord').innerText+" "
							}
							
							if(document.getElementById('buy_deal_value')!=null){
								htmls+=buydealval+" "
							}
							if(document.getElementById('buy_deal_type')!=null){
								htmls+=buydealtype+" "
							}
							if(document.getElementById('endWord')!=null){
								htmls+=document.getElementById('endWord').innerText+" "
							}
						}
						else if(getMatchedResult(middle_word,middleGetWord)){
							
								if(document.getElementById('firstWord')!=null){
								htmls+=document.getElementById('firstWord').innerText+" "
								}
								if(document.getElementById('buy_deal_value')!=null){
									htmls+=buydealval+" "
								}
								if(document.getElementById('buy_deal_type')!=null){
									htmls+=buydealtype+" "
								}
								if(document.getElementById('middleWord')!=null){
									htmls+=document.getElementById('middleWord').innerText+" ("
								}
								if(document.getElementById('get_deal_value')!=null){
									if(getdealval!=""){
										htmls+=getdealval+" "
									}
								}
								if(document.getElementById('get_deal_type')!=null){
									htmls+=getdealtype+") "
								}
								if(document.getElementById('endWord')!=null){
									htmls+=document.getElementById('endWord').innerText+" "
								}
						}
						else{
							if(document.getElementById('firstWord')!=null){
							htmls+=document.getElementById('firstWord').innerText+" "
							}
							if(document.getElementById('buy_deal_value')!=null){
								htmls+=buydealval+" "
							}
							if(document.getElementById('buy_deal_type')!=null){
								htmls+=buydealtype+" "
							}
							if(document.getElementById('middleWord')!=null){
								htmls+=document.getElementById('middleWord').innerText+" "
							}
							if(document.getElementById('get_deal_value')!=null){
								if(getdealval!=""){
									htmls+=getdealval+" "
								}
							}
							if(document.getElementById('get_deal_type')!=null){
								htmls+=getdealtype+" "
							}
							if(document.getElementById('endWord')!=null){
								htmls+=document.getElementById('endWord').innerText+" "
							}
						}
						
                        var x = document.getElementsByName("promotion_at_check_out");
                        var i;
                        for (i = 0; i < x.length; i++) {
                            if (x[i].type == "radio") {
                                if(x[i].checked){
                                    //alert(x[i].value)
                                    htmls+='('+x[i].nextSibling.data+')'
                                }
                            }
                        }
						
						document.getElementById('final_offer_name').innerHTML=htmls +document.getElementById('SKU_PARAMS').value
						document.getElementById('final_offer_value').value=htmls +document.getElementById('SKU_PARAMS').value
						
				}
			</script>
	 
	 
	 
	 <?php }else{echo "Create Promotion cannot be added. No Promotion Type Are Found";}?>
</div>
</div>    
</div>	    
</body>
</html>