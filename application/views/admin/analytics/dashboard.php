<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Highcharts Example</title> 
<style type="text/css">
body{
overflow-x: hidden;
}
.navbar{
min-height: 0px;
margin-bottom: 0px; 
}
.slidebar-form {
padding: 7px 15px 13px 15px;
}
.slidebar-nav {
left: 250px;
list-style: none;
height: 100%;
margin: 0;
margin-left: -250px;  
overflow-y: auto;
padding: 0;
position: fixed; 
width: 0;
z-index: 1000;
-webkit-transition: all 0.5s ease;
-moz-transition: all 0.5s ease;
-o-transition: all 0.5s ease;
transition: all 0.5s ease;
} 
#wrapper_analytics.toggled .slidebar-nav {
width: 250px;
}
.slidebar-nav .navbar-collapse {
padding: 0;
max-height: none;
}
.slidebar-nav ul {
float: none;
width: 100%;
}
.slidebar-nav ul:not {
display: block;
}
.slidebar-nav li {
float: none;
display: block;
}
.slidebar-nav li a {
padding-top: 12px;
padding-bottom: 12px;
}
.slidebar-nav .open .dropdown-menu {
position: static;
float: none;
width: auto;
margin: 0;
padding: 5px 0;
background-color: transparent;
border: 0;
-webkit-box-shadow: none;
box-shadow: none;
}
.slidebar-nav .open .dropdown-menu > li > a {
padding: 5px 15px 5px 25px;
}
.slidebar-nav .navbar-brand {
width: 100%;
}
.slidebar-nav .open > a > b.caret {
border-top: none;
border-bottom: 4px solid;
}
.slidebar-nav .navbar-nav {
margin: 0;
}

#wrapper_analytics {
padding-left: 0;
-webkit-transition: all 0.5s ease;
-moz-transition: all 0.5s ease;
-o-transition: all 0.5s ease;
transition: all 0.5s ease;
}
#wrapper_analytics.toggled {
padding-left: 250px;
}
#page-wrapper6 {
width: 100%;
position: absolute;
padding: 15px;
top: 50px;
}
#wrapper_analytics.toggled #page-wrapper6 {
position: absolute;
margin-right: -250px;
}
@media(min-width:768px) {
#wrapper_analytics {
/*padding-left: 250px;*/
}
#wrapper_analytics.toggled {
padding-left: 0;
}
#page-wrapper6 {
padding: 20px;
position: relative;
}
#wrapper_analytics.toggled #page-wrapper6 {
position: relative;
margin-right: 0;
}
.slidebar-nav {
width: 250px;;
}
#wrapper_analytics.toggled .slidebar-nav {
width: 0;
}
}

.slidebar-toggle {
position: relative;
float: left;
padding: 9px 10px;
background-color: transparent;
background-image: none;
border: 1px solid transparent;
border-radius: 4px;
}
.slidebar-toggle:focus {
outline: 0;
}
.slidebar-toggle .icon-bar {
display: block;
width: 22px;
height: 2px;
border-radius: 1px;
}
.slidebar-toggle .icon-bar + .icon-bar {
margin-top: 4px;
}
.navbar-default .slidebar-toggle {
border-color: #ddd;
}
.navbar-default .slidebar-toggle:hover,
.navbar-default .slidebar-toggle:focus {
background-color: #ddd;
}
.navbar-default .slidebar-toggle .icon-bar {
background-color: #888;
}

/*
* White navbar style
*/

#navbar-white.navbar-default { /* #efffff - #ffffff */
font-size: 14px;

border: 0px;
border-radius: 0;
}

#navbar-white.navbar-default .navbar-toggle,
#navbar-white.navbar-default .slidebar-toggle {
border-color: #ccffff;
}

#navbar-white.navbar-default .navbar-nav>li>a,
#navbar-white.navbar-default .navbar-nav>li>ul>li>a, 
#navbar-white.navbar-default .navbar-brand {
color: #999999; 
}
</style>

<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<link href="<?php echo base_url();?>assets/css/highslide.css" rel="stylesheet" />  
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>

<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/data.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/exporting.js"></script>
 
<script src="<?php echo base_url()?>assets/analytic_js/highslide-full.min.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/highslide.config.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/drilldown.js"></script> 
        
<script src="<?php echo base_url()?>assets/jqueryui/jquery-ui.min.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 

<script type="text/javascript">
            $(function () {
                /*$('#from_date').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
                $('#to_date').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
                $('#particular_year').datetimepicker({
                    format: 'YYYY'
                });*/
            });
            
            function emptyStartEndDate(){
              //  document.getElementById("from_date").value="";
               // document.getElementById("to_date").value=""
            }
            function emptyYear(){
               // document.getElementById("particular_year").value="";
            }
			$(document).ready(function (){	
           $('#to_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'YYYY-MM-DD'
			});
			
		$('#from_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'YYYY-MM-DD', 
				shortTime : true
			}).on('change', function(e, date)
			{
				$('#to_date').bootstrapMaterialDatePicker('setMinDate', date);
			});
			$('#particular_year').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		weekStart: 0, 
		format: 'YYYY', 
		shortTime : true
	}).on('change', function(e, date){
		$('#particular_year').bootstrapMaterialDatePicker('setMinDate', date);
	});
});			
        </script>  
                
        <style>
        .card-body{
            padding: 1.5rem;
            box-shadow:  0 2px 6px 0 rgba(67, 89, 113, 0.12);
            border-radius: 0.5rem;
            min-height: 178px;
        }
        .dyn-height {
            max-height:450px;
            overflow-y:auto;
        }
        .card-body .img_icon{
            width: 25%;
        }
        .card-body p{
            font-size: 16px;
        }

        /* three dots */
    .dashboard .menu-nav {
        display: none;/*uncommment to view*/
       float:right;
       /* display: flex; */
       justify-content: space-between;
     }
     
     .dashboard  .menu-item {
       color: #FCC;
       padding: 3px;
     }
     
     .three-dots:after {
       cursor: pointer;
       color: #444;
       content: '\2807';
       font-size: 20px;
       padding: 0 5px;
     }
     
     .dashboard .dropdown {
       position: absolute;
       right: 10px;
       background-color: #fff;
       color: #333;
       font-weight: bold;
       padding:10px;
       outline: none;
       opacity: 0;
       z-index: -1;
       max-height: 0;
       transition: opacity 0.1s, z-index 0.1s, max-height: 5s;
       box-shadow: 0 0.25rem 1rem rgba(161,172,184,.45);
     }
     
     .dashboard .dropdown-container:focus {
       outline: none;
     }
     
     .dashboard .dropdown-container:focus .dropdown {
       opacity: 1;
       z-index: 100;
       max-height: 100vh;
       transition: opacity 0.2s, z-index 0.2s, max-height: 0.2s;
     }
     .dashboard .dropdown a{
        line-height: 30px;
        color:#333;
        text-decoration: none;
        font-weight: 500;
     }
     .dashboard .dropdown a:hover{
        color:red;
        text-decoration: none;
     }
        /* three dots */

        /* new high chart */
        #container {
    height: 420px;
}
#container1 {
    height: 200px;
}
#container2 {
    height: 250px;
}
#container3 {
    height: 200px;
}
#container4 {
    height: 150px;
}
.colr_TT{
    margin-top: 40px;
}
.colr_TT .txt{
    color: #566a7f;
    font-weight: bold;
    font-size: 14px;
}
.icon_inr{
    font-size: 30px;
    text-align: center;
    background-color: #cddde7;
    padding: 5px;
    border-radius: 5px;
    color: blue;
}

        /* new high chart */

        .user-progress{
            margin-top: 20px;
        }
        .text-muted {
            color: #a1acb8 !important;
        }
        .heading_lineh
        {
            font-weight: bold;
            line-height: 30px;
        }
        .text-cap{
            text-transform: capitalize;
        }
        .right_value{
            color: #566a7f;
            font-size: 16px;
            font-weight: 600;
        }
        .flex-shrink-0{
            margin-bottom: 10px;
        }
        .small_title{
            font-size: 18px;
            color: #566a7f;
        }
        </style>
</head>
  <body>
  
 <div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>

  <div id="wrapper_analytics">    
    <main id="page-wrapper6">
        <div class="container-fluid"> 
         <div class="row" style="display:none" id="">
          
        </div>  
            
        </div>
    </main>
  </div><!-- /#wrapper -->

<!--- new section--->
  <div class="container dashboard">

  <!---filter section --->

  <div class="row">
  <h4 class="text-center">Filter for Total Profit and Sales </h4>
  </div>

  <div class="row filter_section">
    
            <div id="dateRangePicker">
            <div class='col-md-3 col-md-offset-2'>
                <div class="form-group">
                   <input type='text' class="form-control" id='from_date' placeholder="From date" value="<?php echo date("Y-m-d", strtotime("-30 days"));?>"/>
                </div>
            </div>  
            <div class='col-md-3'>
                <div class="form-group">
                    <input type='text' class="form-control" id='to_date' placeholder="To date" value="<?php echo date("Y-m-d");?>"/>
                </div>
            </div>


            <div class='col-md-2' style="display:none;">
                <div class="form-group">
                    <input type='text' class="form-control" id='particular_year' onclick="emptyStartEndDate()" placeholder="Particular Year" value="<?php echo date("Y");?>"/>
                </div>
            </div>

            <div class='col-md-2 pull-right'>
                <div class="form-group">
                    <button class="btn btn-warning" id="filter_button" onclick="profits()"><i class="fa fa-filter"></i> Filter</button>
                </div>
            </div>
            </div>

  </div><!---row--->
            
  <!---filter section --->

    <div class="row">

        <div class="col-md-8">

            <div class="card-body ">
                <div class="col-sm-7">
                <div class="">

                <?php if($profit_status=='high'){ ?>

                    <h5 class="card-title text-primary">Congratulations Admin! 🎉</h5>
                    <p class="mb-4">You have done <span class="fw-medium">
                    
                    <b><?php echo $profit; ?>%</b></span> more sales today.</p>

                    <!---<a href="javascript:;" class="btn btn-sm btn-outline-primary">View Badges</a>--->

                    <?php

                     }else{
                        
                        ?>
                            <h5 class="card-title text-primary">Sorry Admin! 🎉</h5>
                            <p class="mb-4">You have done <span class="fw-medium"> <b><?php echo $profit; ?>%</b></span> less sales today.</p>
                    <?php

                    } 
                    
                    ?>
                </div>
                </div>
                <div class="col-sm-5 text-center text-sm-left">
                <div class="pb-0 px-0 px-md-4">
                    <img src="<?php echo base_url(); ?>assets/images/analytics/man-with-laptop-light.png" height="140" alt="View Badge User">
                </div>
                </div>
            </div><!--card body--->
        </div>

        <div class="col-md-2">
            <div class="card-body pb-0 px-0 px-md-4">
                <div class="menu-nav">
                    <div class="menu-item"></div>
                    <div class="dropdown-container" tabindex="-1">
                    <div class="three-dots"></div>
                    <div class="dropdown">
                        <a href="#"><div>View More</div></a>
                        <a href="#"><div>Delete</div></a>
                    </div>
                    </div>
                </div>

            <div class="avatar flex-shrink-0">
                <img src="<?php echo base_url(); ?>assets/images/analytics/chart-success.png" alt="chart success" class="img_icon rounded">
              </div>
                <span class="fw-medium d-block mb-1 small_title">Total Profit</span>
                <br><small class="text-info">(WSP-Cost Price)</small>
               <h3><?php echo curr_sym; ?> <span id="total_profit"><?php echo $profits_today;?></span></h3>
                <span style="display:none;">
               <?php if($profit_status=='high'){ ?>
                    <small class="text-success fw-medium"><i class="bx bx-up-arrow-alt"></i> +<span id="profit_percentage"><?php echo $profit; ?></span>%</small>
               <?php }else{
                ?>
                 <small class="text-danger fw-medium"><i class="bx bx-down-arrow-alt"></i> +<span id="profit_percentage"><?php echo $profit; ?></span>%</small>
                <?php
               } ?>
               <span>
            </div>
        </div>
        <div class="col-md-2">
            <div class="card-body pb-0 px-0 px-md-4">
                <div class="menu-nav">
                    <div class="menu-item"></div>
                    <div class="dropdown-container" tabindex="-1">
                    <div class="three-dots"></div>
                    <div class="dropdown">
                        <a href="#"><div>View More</div></a>
                        <a href="#"><div>Delete</div></a>
                    </div>
                    </div>
                </div>

            <div class="avatar flex-shrink-0">
                <img src="<?php echo base_url(); ?>assets/images/analytics/wallet-info.png" alt="Credit Card" class="img_icon rounded">
              </div>
                <span class="fw-medium d-block mb-1 small_title">Total Sales</span>
                <br><small class="text-info">(Total Cart Value)</small>
               <h3><?php echo curr_sym; ?> <span id="total_sales"><?php echo $total_sales_today;?></span></h3>
               <span style="display:none;">
               <?php if($sales_status=='high'){ ?>

               <small class="text-success fw-medium"><i class="bx bx-up-arrow-alt"></i> +<?php echo $sales; ?>%</small>
               <?php }else{
                ?>
                <small class="text-danger fw-medium"><i class="bx bx-down-arrow-alt"></i> -<?php echo $sales; ?>%</small>
                <?php 
               } ?>
               </span>
            </div>
        </div>

    </div><!---row--->

    <div class="row colr_TT">

    <div class="second">

     <div class="col-md-8">

        <div class="card-body">
        <div class="row">

        <div class="col-md-8">


        <figure class="highcharts-figure">
            <div id="container"></div>
            <p class="highcharts-description">
               
            </p>
        </figure>
        </div>
        <div class="col-md-4">
            
                <div class="row">
                    <div id="container1"></div>
                </div>
            

            
            <div class="row colr_TT text-center" >
                <h4><?php echo $growth; ?>% Company Growth</h4>
            </div>

            <div class="row colr_TT" >
                <div class="col-md-6">
                    <div class="row txt">
                        <div class="col-md-4 icon_inr">
                            <i class="fa fa-inr" aria-hidden="true"></i>
                        </div>
                        <div class="col-md-8">
                        <span style="font-size:16px;"><small style="font-size:12px;">2023</small> <br><i class="fa fa-inr" aria-hidden="true"></i><?php echo $previous_yr_total;?></span>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">

                    <div class="row txt">
                        <div class="col-md-4 icon_inr">
                            <i class="fa fa-inr" aria-hidden="true"></i>
                        </div>
                        <div class="col-md-8">
                            <span style="font-size:16px;"><small style="font-size:12px;">2024</small> <br><i class="fa fa-inr" aria-hidden="true"></i><?php echo $current_yr_total;?></span>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        </div><!--row-->
        
        </div>

     </div>

     <div class="col-md-4">

     <div class="row">
     <div class="col-md-6">
            <div class="card-body pb-0 px-0 px-md-4">
                <div class="menu-nav">
                    <div class="menu-item"></div>
                    <div class="dropdown-container" tabindex="-1">
                    <div class="three-dots"></div>
                    <div class="dropdown">
                        <a href="#"><div>View More</div></a>
                        <a href="#"><div>Delete</div></a>
                    </div>
                    </div>
                </div>

            <div class="avatar flex-shrink-0">
                <img src="<?php echo base_url(); ?>assets/images/analytics/chart-success.png" alt="chart success" class="img_icon rounded">
              </div>
                <span class="fw-medium d-block mb-1 small_title">Total Payments</span>
               <h3><?php echo curr_sym.$total_payments;?></h3>
               <!--<small class="text-success fw-medium"><i class="bx bx-up-arrow-alt"></i> +72.80%</small>--->
            </div>
        </div>
        <div class="col-md-6">
            <div class="card-body pb-0 px-0 px-md-4">
                <div class="menu-nav">
                    <div class="menu-item"></div>
                    <div class="dropdown-container" tabindex="-1">
                    <div class="three-dots"></div>
                    <div class="dropdown">
                        <a href="#"><div>View More</div></a>
                        <a href="#"><div>Delete</div></a>
                    </div>
                    </div>
                </div>

            <div class="avatar flex-shrink-0">
                <img src="<?php echo base_url(); ?>assets/images/analytics/wallet-info.png" alt="Credit Card" class="img_icon rounded">
              </div>
                <span class="fw-medium d-block mb-1 small_title">Total Refund</span>
               <h3><?php echo curr_sym.$total_refund;?></h3>
               <!---<small class="text-success fw-medium"><i class="bx bx-up-arrow-alt"></i> +28.42%</small>-->
            </div>
        </div>

        <div class="row colr_TT">
            <div class="col-md-12">
                <div class="col-md-12">
                    <div class="card-body">
                    <div id="container2"></div>
                    </div>
                </div>
            </div>
        </div><!---row--->

     </div>
     </div>

     </div><!--row-->

     <div class="row ">

        <div class="col-md-12 colr_TT">

        <div class="col-md-6">
            <div class="card-body dyn-height" style="min-height: 450px;">
                    <div class="card-header d-flex align-items-center justify-content-between pb-0">
                        <div class="card-title mb-0">
                            <div class="menu-nav">
                                <div class="menu-item"></div>
                                <div class="dropdown-container" tabindex="-1">
                                <div class="three-dots"></div>
                                <div class="dropdown">
                                    <a href="#"><div>View More</div></a>
                                    <a href="#"><div>Delete</div></a>
                                </div>
                                </div>
                            </div>
                        <h5 class="m-0 me-2">Order Statistics</h5>
                        <!-- <small class="text-muted"><?php echo $total_sales; ?> Total Sales</small> -->
                        </div>

                        <div class="col-md-12 text-center">
                            <div class="col-md-6">
                                <h2 class="mb-2"><?php echo $total_order; ?> <br><small>Total Orders</small></h2>
                            </div>

                            <div class="col-md-6">
                            <div id="container4" class=""></div>
                            </div>
                        </div>


                        <div class="dropdown">
                        <button class="btn p-0" type="button" id="orederStatistics" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="bx bx-dots-vertical-rounded"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="orederStatistics" style="">
                            <a class="dropdown-item" href="javascript:void(0);">Select All</a>
                            <a class="dropdown-item" href="javascript:void(0);">Refresh</a>
                            <a class="dropdown-item" href="javascript:void(0);">Share</a>
                        </div>
                        </div>
                    </div>

               <?php if(!empty($total_order_cat)){

                foreach( $total_order_cat as $key => $value ){

?>

            <div class="row">
                <div class="col-md-2">
                    <img src="<?php echo base_url(); ?>assets/images/analytics/chart-success.png" alt="" class="rounded" >
                </div>
                <div class="col-md-6">
                    <h6 class="mb-0 heading_lineh text-cap" ><?php echo $value->cat_name; ?><br><small class="text-muted text-cap"><?php echo $value->pcat_name; ?></small></h6>
                    
                </div>
                <div class="col-md-4 text-center">
                    <div class="user-progress">
                        <h6 class="mb-0 right_value"><?php echo $value->total_orders; ?></h6>
                    </div>
                </div>
            </div><!--row-->


<?php
                }
               }?>
            
            </div>
        </div>
        <div class="col-md-6">
            <div class="card-body dyn-height">
<style>

/* Tabs */
.tabs {
  width: 100%;
  background-color: #fff;
  border-radius: 5px 5px 5px 5px;
}
ul#tabs-nav {
  list-style: none;
  margin: 0;
  padding: 5px;
  overflow: auto;
}
ul#tabs-nav li {
  float: left;
  font-weight: bold;
  margin-right: 2px;
  padding: 8px 10px;
  border-radius: 5px 5px 5px 5px;
  /*border: 1px solid #d5d5de;
  border-bottom: none;*/
  cursor: pointer;
  font-size: 16px;
}
ul#tabs-nav li:hover,
ul#tabs-nav li.active {
  background-color: #08E;
  color:#fff;
}
#tabs-nav li a {
text-decoration: none;
/*color: #FFF;*/
}

ul#tabs-nav .active a{
    color: #FFF;
}
#tabs-nav li a:active {
text-decoration: none;
    color: #FFF;
}
.tab-content {
  padding: 10px;
  /*border: 5px solid #09F;*/
  background-color: #FFF;
}
.content_inner{
    padding: 15px;
    line-height: 30px;
    margin-top: -21px;
}
.font_big{
    font-size: 25px;
}
</style>

                <!-- tab--->
                <div class="tabs">
  <ul id="tabs-nav">
    <li><a href="#tab1">Processing</a></li>
    <li><a href="#tab2">Delivered</a></li>
    <li><a href="#tab3">Cancel</a></li>
    <li><a href="#tab4">Return</a></li>
    <li><a href="#tab5">Failed</a></li>
    
  </ul> <!-- END tabs-nav -->
  <div id="tabs-content">
    <div id="tab1" class="tab-content">
        <!---tt--->
        
        <div class="" style="margin-bottom: 20px;">

              <div class="avatar flex-shrink-0 me-3" style="width: 30%;float: left;">
                <img src="<?php echo base_url() ?>assets/images/analytics/wallet.png" alt="User" style="margin-top:22px;">
              </div>

              
                    <div class="content_inner">
                        <span class="text-muted d-block">Total Amount</span><br>
                        <span class="mb-0 me-1 font_big"><?php echo curr_sym.$orders_data["processing_orders_total_amount"]; ?></span><br>
                        <span class="text-success fw-medium">
                            <i class="fa fa-dot"></i>
                            Orders : <?php echo $orders_data["processing_orders"]; ?>
                        </span>
                        
                    </div>
               
            </div>
            
    
        <!---tt--->
        
    </div>
    <div id="tab2" class="tab-content">
      <!---tt--->
        
      <div class="" style="margin-bottom: 20px;">

        <div class="avatar flex-shrink-0 me-3" style="width: 30%;float: left;">
        <img src="<?php echo base_url() ?>assets/images/analytics/wallet.png" alt="User" style="margin-top:22px;">
        </div>


            <div class="content_inner">
                <span class="text-muted d-block">Total Amount</span><br>
                <span class="mb-0 me-1 font_big"><?php echo curr_sym.$orders_data["delivered_orders_total_amount"]; ?></span><br>
                <span class="text-success fw-medium">
                    <i class="fa fa-dot"></i>
                    Orders : <?php echo $orders_data["delivered_orders"]; ?>
                </span>
                
            </div>        
        </div>

        <!---tt--->
    </div>
    <div id="tab3" class="tab-content">
      <!---tt--->
        
      <div class="" style="margin-bottom: 20px;">

        <div class="avatar flex-shrink-0 me-3" style="width: 30%;float: left;">
        <img src="<?php echo base_url() ?>assets/images/analytics/wallet.png" alt="User" style="margin-top:22px;">
        </div>

            <div class="content_inner">
                <span class="text-muted d-block">Total Amount</span><br>
                <span class="mb-0 me-1 font_big"><?php echo curr_sym.$orders_data["cancelled_orders_total_amount"]; ?></span><br>
                <span class="text-success fw-medium">
                    <i class="fa fa-dot"></i>
                    Orders : <?php echo $orders_data["cancelled_orders"]; ?>
                </span>
                
            </div>
            
        </div>

        <!---tt--->
    </div>

    <div id="tab4" class="tab-content">
      <!---tt--->
        
      <div class="" style="margin-bottom: 20px;">

        <div class="avatar flex-shrink-0 me-3" style="width: 30%;float: left;">
        <img src="<?php echo base_url() ?>assets/images/analytics/wallet.png" alt="User" style="margin-top:22px;">
        </div>


            <div class="content_inner">
                <span class="text-muted d-block">Total Amount</span><br>
                <span class="mb-0 me-1 font_big"><?php echo curr_sym.$orders_data["return_orders_total_amount"]; ?></span><br>
                <span class="text-success fw-medium">
                    <i class="fa fa-dot"></i>
                    Orders : <?php echo $orders_data["return_orders"]; ?>
                </span>
                
            </div>
            
        
        </div>
        
        <!---tt--->
    </div>

    <div id="tab5" class="tab-content">
      <!---tt--->
        
      <div class="" style="margin-bottom: 20px;">

        <div class="avatar flex-shrink-0 me-3" style="width: 30%;float: left;">
        <img src="<?php echo base_url() ?>assets/images/analytics/wallet.png" alt="User" style="margin-top:22px;">
        </div>

            <div class="content_inner">
                <span class="text-muted d-block">Total Amount</span><br>
                <span class="mb-0 me-1 font_big"><?php echo curr_sym.$orders_data["failed_orders_total_amount"]; ?></span><br>
                <span class="text-success fw-medium">
                    <i class="fa fa-dot"></i>
                    Orders : <?php echo $orders_data["failed_orders"]; ?>
                </span>
                
            </div>
        
        </div>
        
        <!---tt--->
    </div>
    
  </div> <!-- END tabs-content -->
</div> <!-- END tabs -->
                

                <!-- tab--->
        <div id="container3" class="colr_TT"></div>
        
    </div>
</div>

<div class="col-md-6" style="display:none;">
        <div class="card-body dyn-height">
                <div class="card-title mb-0">
                        <div class="menu-nav">
                            <div class="menu-item"></div>
                            <div class="dropdown-container" tabindex="-1">
                            <div class="three-dots"></div>
                            <div class="dropdown">
                                <a href="#"><div>View More</div></a>
                                <a href="#"><div>Delete</div></a>
                            </div>
                            </div>
                        </div>
                    <h5 class="m-0 me-2">Wallet</h5>
                    
                </div>


                <?php if(!empty($wallet_data)){
                    foreach($wallet_data as $w_data){

                        if($w_data->email!=''){
                        ?>

                        <div class="row">
                            <div class="col-md-2">
                                <img src="<?php echo base_url(); ?>assets/images/analytics/wallet.png" alt="" class=" rounded" style="width:100%;" >
                            </div>
                            <div class="col-md-6">
                                <h6 class="mb-0 heading_lineh text-cap" ><small class="text-muted text-cap"><?php echo $w_data->email; ?></small><br><?php echo $w_data->name; ?></h6>
                                
                            </div>
                            <div class="col-md-4 text-center">
                                <div class="user-progress">
                                    <h6 class="mb-0 right_value"><?php echo curr_sym.$w_data->wallet_amount; ?></h6>
                                </div>
                            </div>
                        </div><!--row-->

                        <?php 
                        }
                    }
                } ?>
        </div><!---card body --->
</div>

        </div>

     </div><!--row-->
     


    <!----- customer intelligence ---->
     
    <?php include('vistor_analytics_dashboard.php'); ?>
    <!----- customer intelligence ---->


    </div>

  </div>

<!--- new section--->

</div>
        
<script type='text/javascript'>

var base_url='<?php echo base_url(); ?>';

$(document).ready(function() {

/* circular progress bar */

// Create the chart
Highcharts.chart('container1', {
  chart: {
    type: 'pie'
  },
  title: {
    text: 'Growth',
    style: {
        color: '#DF8500',
        fontSize:'14px'
    }
  },
  subtitle: {
    text: `<div><?php echo $growth; ?>%</div> of Total`,
    align: "center",
    verticalAlign: "middle",
    style: {
      "fontSize": "14px",
      "textAlign": "center"
    },
    x: 0,
    y: -2,
    useHTML: true
  },
  plotOptions: {
    pie: {
      shadow: false,
      center: ["50%", "50%"],
      dataLabels: {
        enabled: true
      },
      states: {
        hover: {
          enabled: false
        }
      },
      size: "95%",
      innerSize: "95%",
      borderColor: null,
      borderWidth: 8
    }

  },
  tooltip: {
    valueSuffix: '%',
    style: {
		  fontSize: '14px'
		}
  },
  series: [{
    innerSize: '80%',
    data: [{
      name: 'Growth',
      y: <?php echo intval($growth); ?>,
      color: '#e7eaeb'
    }, {
      name: 'Non Prioritised',
      y: <?php echo (100-intval($growth)); ?>,
    }]
  }],
  exporting: {
        enabled: false
    },
    credits: {
        enabled: false
    },
});


/* circular progress bar */

/* line chart */


/* line chart */


// Show the first tab and hide the rest
$('#tabs-nav li:first-child').addClass('active');
$('.tab-content').hide();
$('.tab-content:first').show();

// Click function
$('#tabs-nav li').click(function(){
  $('#tabs-nav li').removeClass('active');
  $(this).addClass('active');
  $('.tab-content').hide();
  
  var activeTab = $(this).find('a').attr('href');
  $(activeTab).fadeIn();
  return false;
});

/* Line chart */

Highcharts.chart('container3', {

title: {
    text: '',
    align: 'left'
},

subtitle: {
    text: '',
    align: 'left'
},

yAxis: {
    title: {
        text: 'count',
        style: {
                fontSize:'14px'
            }
    },
    labels: {
            style: {
                fontSize:'14px'
            }
        }
},

xAxis: {
    accessibility: {
        rangeDescription: 'Range: 2010 to 2020'
    }
},

legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'middle'
},

plotOptions: {
    series: {
        label: {
            connectorAllowed: false
        },
        pointStart: 2010
    }
},

series: [{
    name: 'Income',
    data: [100, 120, 130, 150, 160, 180,
        190, 200, 210, 220, 230]
}],

responsive: {
    rules: [{
        condition: {
            maxWidth: 500
        },
        chartOptions: {
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
            }
        }
    }]
},
exporting: {
        enabled: false
    },
    credits: {
        enabled: false
    }

});




/* Line chart */

});

function order_total_revenue (datae) {
         var datae=JSON.parse(datae)
         Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
	legend: {
	  itemStyle: {
		 fontSize:'16px'
	  }
	},
    tooltip: {
        valueSuffix: '',
        style: {
		  fontSize: '14px'
		}
    },
    title: {
        text: 'Total Revenue',
        style: {
            color: '#DF8500',
            fontSize:'14px'
        }
    },
    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
        labels: {
            style: {
                fontSize:'14px'
            }
        }
    },
    yAxis:{
        title: {
                text: 'Revenue (Rs)',
                style: {
                    fontSize:'14px'
                }
            },
        labels: {
            style: {
                fontSize:'14px'
            }
        }
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        column: {
            borderRadius: '2%'
        }
    },
    series:datae,
    exporting: {
        enabled: false
    }
});
    
}

function profile_registration (datae) {
         var datae=JSON.parse(datae)
         Highcharts.chart('container2', {
    chart: {
        type: 'line'
    },
	legend: {
	  itemStyle: {
		 fontSize:'16px'
	  }
	},
    title: {
        text: 'Number of Customers',
        style: {
                    color: '#DF8500',
                    fontSize:'14px'
                }
    },
    subtitle: {
        text: ' '
    },
    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
        labels: {
            autoRotation: [-45, -90],
                style: {
                    fontSize:'14px'
                }
            }
    },
    yAxis: {
        title: {
                text: 'Count',
                style: {
                    fontSize:'14px'
                }
            },
        labels: {
            style: {
                fontSize:'14px'
            }
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true,
                style: {
                fontSize: '12px',
                fontFamily: 'Verdana, sans-serif'
            }
            },
            enableMouseTracking: false
        }
    },
    series: [{
        name: 'Registration - <?php echo date('Y'); ?>',
        data: datae
    }],
    exporting: {
        enabled: false
    },
    credits: {
        enabled: false
    }

});

    
}

function orders_status_pie_chart(){
	
    Highcharts.chart('container3', {
    /*chart: {
        type: 'pie'
    },
	*/
	
	 chart: {
	  type: 'pie',
      events: {
        load: function() {
          this.series[0].points.forEach(point => {
            point.update({
              dataLabels: {
                style: {
                  fontSize: '14px'
                }
              }
            })
          })
        }
      }
    },
	
	
    title: {
        text: 'Orders Graph',
        style: {
            color: '#DF8500',
            fontSize:'14px'
        }
    },
    tooltip: {
        valueSuffix: '',
        style: {
		  fontSize: '14px'
		}
    },
    subtitle: {
        text:
        ''
    },
    plotOptions: {
        series: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: [{
                enabled: true,
                // format: '<b>{point.name}</b><br>'+Math.round(this.point.percentage)+'%',
                formatter: function() {
                    return '<b>'+ this.point.name +'</b>: '+ Math.round(this.percentage) +' %';
                },
                distance: 20
            }, {
                enabled: false,
                distance: -40,
                format: '{point.percentage:.1f}%',
                style: {
                    fontSize: '14px',
                    textOutline: 'none',
                    opacity: 0.7
                },
                filter: {
                    operator: '>',
                    property: 'percentage',
                    value: 10
                }
            }]
        }
    },
    series: [
        {
            name: 'Total',
            colorByPoint: true,
            data: [
                {
                    name: 'Processing',
                    y: <?php echo intval($orders_data["processing_orders"]); ?>
                },
                {
                    name: 'Delivered',
                    sliced: true,
                    selected: true,
                    y: <?php echo intval($orders_data["delivered_orders"]); ?>
                },
                {
                    name: 'Cancel',
                    y: <?php echo intval($orders_data["cancelled_orders"]); ?>
                },
                {
                    name: 'Return',
                    y: <?php echo intval($orders_data["return_orders"]); ?>
                },
                {
                    name: 'Failed',
                    y:<?php echo intval($orders_data["failed_orders"]); ?>
                }
            ]
        }
    ],
    exporting: {
        enabled: false
    },
    credits: {
        enabled: false
    }
});

}

function order_pie_chart_cat (datae) {
         var datae=JSON.parse(datae)
         data=[]; hits=[];
       
         Highcharts.chart('container4', {
  chart: {
    type: 'pie'
  },
  title: {
    text: ''
  },
  plotOptions: {
    pie: {
      shadow: false,
      center: ["50%", "50%"],
      dataLabels: {
        enabled: false
      },
      states: {
        hover: {
          enabled: false
        }
      },
      size: "100%",
      innerSize: "100%",
      borderColor: null,
      borderWidth: 5
    }

  },
  tooltip: {
    valueSuffix: '%',
    style: {
		  fontSize: '14px'
		}
  },
  series: [{
    name: 'Total',
    colorByPoint: true,
    innerSize: '80%',
    data: datae
  }],
    exporting: {
        enabled: false
    },
    credits: {
        enabled: false
    }
});

}

function getCatList(){
        
		var from_date='';
        var to_date='';
        
        var particular_year=document.getElementById("particular_year").value
        
        
        var xhr = false;

        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        var xhr = new XMLHttpRequest();
		 
		/* This block I modified for samrick 6/7/2021 ends */
                if (xhr) {
                                           
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                
                            order_pie_chart_cat(xhr.responseText);// dynamic
                             
                            if(xhr.responseText==1){

                           }
                           else{
                               
                           }
                        }
                    }
                    xhr.open('POST', base_url+"admin/Analytics/getCatOrder", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('from_date='+from_date+'&to_date='+to_date+'&particular_year='+particular_year);
                }
    }
    
    function getTotalRevenue(){
        
		var from_date='';
        var to_date='';
        
        var particular_year=document.getElementById("particular_year").value
        
        var xhr = false;

        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        var xhr = new XMLHttpRequest();
		 
		/* This block I modified for samrick 6/7/2021 ends */
                if (xhr) {
                                           
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                
                            order_total_revenue(xhr.responseText);// dynamic
                             
                            if(xhr.responseText==1){

                           }
                           else{
                               
                           }
                        }
                    }
                    xhr.open('POST', base_url+"admin/Analytics/getRevenueOnYearBasis", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('from_date='+from_date+'&to_date='+to_date+'&particular_year='+particular_year);
                }
    }
    function profileRegisterCount(){
        
		var from_date='';
        var to_date='';
        
        var particular_year=document.getElementById("particular_year").value
        
        var xhr = false;

        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        var xhr = new XMLHttpRequest();
		 
		/* This block I modified for samrick 6/7/2021 ends */
                if (xhr) {
                                           
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                
                            profile_registration(xhr.responseText);// dynamic
                             
                            if(xhr.responseText==1){

                           }
                           else{
                               
                           }
                        }
                    }
                    xhr.open('POST', base_url+"admin/Analytics/profileRegisterCount", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('from_date='+from_date+'&to_date='+to_date+'&particular_year='+particular_year);
                }
    }
    
   

    function show_profit_and_sales(data){
        //console.log(data);

        var datae=JSON.parse(data)

        //alert(datae.total_cost_price);
        //alert(datae.total_profit);
        //alert(datae.total_sales);

        $("#total_profit").html(datae.total_profit);
        $("#total_sales").html(datae.total_sales);

    }

    function profits(){

        var from_date=document.getElementById('from_date').value;
        var to_date=document.getElementById('to_date').value;
        
        var particular_year=document.getElementById("particular_year").value;
        
        var xhr = false;

        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        var xhr = new XMLHttpRequest();
		 
        if (xhr) {
                                    
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                        
                    show_profit_and_sales(xhr.responseText);// dynamic
                        
                    if(xhr.responseText==1){

                    }
                    else{
                        
                    }
                }
            }
            xhr.open('POST', base_url+"admin/Analytics/getProfitAndSales", true);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.send('from_date='+from_date+'&to_date='+to_date+'&particular_year='+particular_year);
        }
    }

    $(document).ready(function(){
        getCatList();
        getTotalRevenue();
        profileRegisterCount();
        orders_status_pie_chart();
        profits();
    });

</script>
        
    </body>
       
</html>
