<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Business Score</title> 
<style type="text/css">
body{
overflow-x: hidden;
}
.analyzing_stuff_div{
	margin-top:1rem;
	display:none;
}
#analyzing_stuff{
	color:#0000ff;
	font-weight:bold;
}
#result_businessscore_div{
	display:none;
}
.result_businessscore_extra_last4weeks_div{
	margin-top:1rem;
	display:none;
}
.leads{
	font-weight:bold;
	font-size:40px;
	color:#337ab7;
}
.result_businessscore_extra_last4weeks_div .lead{
	color:#337ab7;
}
</style>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<link href="<?php echo base_url();?>assets/css/highslide.css" rel="stylesheet" /> 
<script> var base_url="<?php echo base_url()?>"</script>  
        
<script src="<?php echo base_url()?>assets/jqueryui/jquery-ui.min.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 
   
</head>
  <body>
  
 <div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>

  <div id="wrapper_analytics">    
    
        <div class="container-fluid"> 
		<div class="row">
          <div class="col-md-4 col-md-offset-4">
				<h3 class="text-center">Your Business Score</h3>
		  </div>
		</div>
         <div class="row">
          <div class="col-md-4 col-md-offset-4">
				 <div class="form-group">
					 <select id="businessscore_duration" name="businessscore_duration" class="form-control" onchange="getBusinessScoreInitiateFun('change_dropdown')">
						 <option value="last week">last week</option>
						 <option value="last 2 weeks">last 2 weeks</option>
						 <option value="last 4 weeks" selected>last 4 weeks</option>
						 <option value="last month">last month</option>
						 <option value="last 3 months">last 3 months</option>
						 <option value="last 6 months">last 6 months</option>
						 <option value="last year">last year</option>
					 </select>
                </div>
				
         </div>
       
        
        </div>  
             
        <div class="row">
		  <div class='col-md-4 col-md-offset-4 text-center'>
                   <button class="btn btn-warning" onclick="getBusinessScoreInitiateFun()"> Start</button>
				   <button class="btn btn-warning" onclick="location.reload()"> Reset</button>
            </div>
		 </div>	
		 
		  <div class="row analyzing_stuff_div">
		  <div class='col-md-4 col-md-offset-4 text-center' id="analyzing_div">
                  Analyzing <span id="analyzing_stuff"></span>
				  
				
					  
  
            </div>
		 </div>	
		 
		  <div class="row analyzing_stuff_div">
		  <div class='col-md-8 col-md-offset-2'>
                 <div class="progress" style="height: 20px;">
  <div
    class="progress-bar"
    role="progressbar"
    style="width: 0%;"
    aria-valuenow="0"
    aria-valuemin="0"
    aria-valuemax="100"
	id="businessscore_duration_div"
  >
    0%
  </div>
</div>
  
            </div>
		 </div>	
		 
		 
		 
		  <div class="row" id="result_businessscore_div">
		  <div class='col-md-4 col-md-offset-4'>
		 <div class="card card-default">
  <div class="card-body text-center">
      <p class="lead">Your Business Score is</p>
	  <p class="leads" id="result_businessscore_number"></p>
	  <p class="lead" id="result_businessscore_text"></p>
  </div>
</div>
		 </div>
		 </div> 
		 
		 
		  <div class="row result_businessscore_extra_last4weeks_div">
		  <div class='col-md-3 col-md-offset-3'>
		 <div class="card card-default">
  <div class="card-body text-center">
      <p class="lead">Market Share</p>
	  <p>
		You have a <b>stagnation</b> in gaining market share. Keep implementing customer retention strategy and optimizing your product mix.
	  </p>
  </div>
</div>
		 </div>
		  <div class='col-md-3'>
		 <div class="card card-default">
  <div class="card-body text-center">
      <p class="lead">Long Term Value</p>
	  <p>
		You are <b>failing to create</b> Long Term Value for your customers. You need to implement customer experience management strategies in your business.
	  </p>
  </div>
</div>
		 </div>
		 </div> 
		 
		 
		 
		   <div class="row result_businessscore_extra_last4weeks_div">
		  <div class='col-md-3 col-md-offset-3'>
		 <div class="card card-default">
  <div class="card-body text-center">
      <p class="lead">CrossSell & UpSell</p>
	  <p>
		This is working OK for you. Your strategy is working!
	  </p>
  </div>
</div>
		 </div>
		  <div class='col-md-3'>
		 <div class="card card-default">
  <div class="card-body text-center">
      <p class="lead">Product Mix</p>
	  <p>
		Your Product Mix is skewed. Please look into spreading out Value across your product Mix.
	  </p>
  </div>
</div>
		 </div>
		 </div> 
		 
		 
		 
		   <div class="row result_businessscore_extra_last4weeks_div">
		  <div class='col-md-3 col-md-offset-3'>
		 <div class="card card-default">
  <div class="card-body text-center">
      <p class="lead">Reach & Site Time</p>
	  <p>
		This is working OK for you. Your strategy is working!
	  </p>
  </div>
</div>
		 </div>
		  <div class='col-md-3'>
		 <div class="card card-default">
  <div class="card-body text-center">
      <p class="lead">Grievances</p>
	  <p>
		Your business is taking longer than your prescribed SLA to resolve the grieavances. Most cusotmers are not happy!
	  </p>
  </div>
</div>
		 </div>
		 </div> 
		 
		 
			
		
        </div>
    
  </div><!-- /#wrapper -->

</div>
        
		




        <script type='text/javascript'>
		    var analyzing_arr=["Products...",
							   "Guest Users...",
							   "Registered Users...",
							   "Cart...",
							   "Clicks...",
							   "Sections...",
							   "Unique orders...",
							   "Abandoned Carts...",
							   "Purchase Value...",
							   "Wishlist...",
							   "Offers Applied...",
							   "Offers Availed...",
							   "Cross Sells...",
							   "Upsells...",
							   "Grievances & Resolutions...",
							   "Total Site Time...",
							   "IP Addresses...",
							   "Geo Locations..."];
			var i=0;
			var timeVar;
			var filling_color=0;
			
			function getBusinessScoreInitiateFun(flag=""){
				// in this function by default I am setting all values to 0 and divs as display none
				i=0;
				filling_color=-5.55;
				$(".analyzing_stuff_div").css({"display":"none"});
				$("#result_businessscore_div").css({"display":"none"});
				$(".result_businessscore_extra_last4weeks_div").css({"display":"none"});
				$("#businessscore_duration_div").html(filling_color+"%");
				$("#businessscore_duration_div").attr("aria-valuenow",filling_color);
				$("#businessscore_duration_div").css({"width":filling_color+"%"});
				$("#analyzing_div").html("Analyzing <span id='analyzing_stuff'></span>");
				
				if(flag!="change_dropdown"){
					getBusinessScoreFun();
				}
				if(flag=="change_dropdown"){
					if(timeVar!==undefined){
						clearTimeout(timeVar);
					}
					return false;
				}
			}
			function getBusinessScoreFun(){
				$(".analyzing_stuff_div").css({"display":"block"});
				$("#analyzing_stuff").html(analyzing_arr[i]);
				
				filling_color=Math.round(filling_color*100)/100
				filling_color=filling_color+5.55;
				filling_color=filling_color.toFixed(2);
				if(filling_color>=98.5){
					filling_color=100;
				}
				$("#businessscore_duration_div").html(filling_color+"%");
				$("#businessscore_duration_div").attr("aria-valuenow",filling_color);
				$("#businessscore_duration_div").css({"width":filling_color+"%"});
				i++;
				
				if(filling_color==100){
					$("#analyzing_div").html("Analysis <span style='color:#337ab7' id='analyzing_stuff'>Completed</span>");
					clearTimeout(timeVar);
					setOutputForBusinessScore();
				}
				
				
				timeVar=setTimeout(function(){
							getBusinessScoreFun();
							
							
				
				
						},3333)
						
			}
			
			function setOutputForBusinessScore(){
				  /* Result Score card starts */
					$("#result_businessscore_div").css({"display":"block"});
					if($("#businessscore_duration").val()=="last week"){ //available
						$("#result_businessscore_number").html("8.6/10");
						$("#result_businessscore_text").html("Bravo! You are rocking!");
					}
					if($("#businessscore_duration").val()=="last 2 weeks"){
						$("#result_businessscore_number").html("NA");
						$("#result_businessscore_text").html("Not Generated");
					}
					if($("#businessscore_duration").val()=="last 4 weeks"){ //available
						$("#result_businessscore_number").html("5.6/10");
						$("#result_businessscore_text").html("You are doing OK.<br>Need is to push your strategies harder!");
						$(".result_businessscore_extra_last4weeks_div").css({"display":"block"});
					}
					if($("#businessscore_duration").val()=="last month"){
						$("#result_businessscore_number").html("NA");
						$("#result_businessscore_text").html("Not Generated");
					}
					if($("#businessscore_duration").val()=="last 3 months"){ //available
						$("#result_businessscore_number").html("1.2/10");
						$("#result_businessscore_text").html("You must exit this business and save your money & time both!");
					}
					if($("#businessscore_duration").val()=="last 6 months"){
						$("#result_businessscore_number").html("NA");
						$("#result_businessscore_text").html("Not Generated");
					}
					if($("#businessscore_duration").val()=="last year"){
						$("#result_businessscore_number").html("NA");
						$("#result_businessscore_text").html("Not Generated");
					}
					/* Result Score card ends */
					return false;
			}
        </script>
       

        
    </body>   
</html>