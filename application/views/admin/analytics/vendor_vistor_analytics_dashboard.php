<br>
<br>
<hr>
<br>
<br>

<!--- new section--->
  <div class="container dashboard">


  <!---filter section --->

  <div class="row">
            <div id="dateRangePicker">
            <div class='col-md-3'>
                <div class="form-group">
                   <input type='text' class="form-control" id='from_date' onclick="emptyYear()" placeholder="From date" value="<?php echo date("Y-m-d", strtotime("-30 days"));?>"/>
                </div>
            </div>  
            <div class='col-md-3'>
                <div class="form-group">
                    <input type='text' class="form-control" id='to_date' onclick="emptyYear()" placeholder="To date" value="<?php echo date("Y-m-d");?>"/>
                </div>
            </div>

            <div class='col-md-2'>
                <div class="form-group">
                    <input type='text' class="form-control" id='particular_year' onclick="emptyStartEndDate()" placeholder="Particular Year" value="<?php echo date("Y");?>"/>
                </div>
            </div>

            <div class='col-md-2 pull-right'>
                <div class="form-group">
                    <button class="btn btn-warning" id="filter_button" onclick="VisitorAnalytics()"><i class="fa fa-filter"></i> Filter</button>
                </div>
            </div>

            </div>

         </div>
         </div>
         
        
        </div>  
  <!---filter section --->

  <div class="row">
        <div class="col-md-12">
            <div class="card-body">
            <figure class="highcharts-figure">
                <div id="container_vistor_time_spent"></div>
                <p class="highcharts-description">
                    <!-- Chart showing data loaded dynamically. The individual data points can
                    be clicked to display more information. -->
                </p>
            </figure>
            </div>
        </div>
  </div>
    
    <div class="row">

        <div class="col-md-6">

            <div class="card-body">
                
                <figure class="highcharts-figure">
                    <div id="container_device_based"></div>
                </figure>
                
            </div><!--card body--->
        </div>

        <div class="col-md-6" style="display:none;">
            <div class="card-body pb-0 px-0 px-md-4">
                
                <figure class="highcharts-figure">
                    <div id="container_city"></div>
                </figure>
             </div>
        </div>
        
        <div class="col-md-12">
            <div class="card-body pb-0 px-0 px-md-4">
                <figure class="highcharts-figure">
                    <div id="container_session_based"></div>
                </figure>
             </div>
        </div>


        
        <div class="col-md-6">
            <div class="card-body pb-0 px-0 px-md-4">
                <figure class="highcharts-figure">
                    <div id="container_products_sale"></div>
                </figure>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="card-body pb-0 px-0 px-md-4">
                <figure class="highcharts-figure">
                    <div id="container_products_sale_amount"></div>
                </figure>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card-body pb-0 px-0 px-md-4">
                <figure class="highcharts-figure">
                    <div id="container_product_sale_profit"></div>
                </figure>
            </div>
        </div>
        
    

    </div><!---row--->



</div>
<!--- new section--->
        
<script type='text/javascript'>

var base_url='<?php echo base_url(); ?>';

$(document).ready(function() {

});


/* new  */

// https://www.highcharts.com/docs/chart-and-series-types/error-bar-series

function VisitorTimeSpent(data){

    data=JSON.parse(data);
    //alert(data)
    console.log(data);

    var category=data.category;
    var vistors=data.visitors;
    var surf_time_all=data.surf_time_all;

    //alert(surf_time_all);

    Highcharts.chart('container_vistor_time_spent', {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: 'Total Visitors vs Time spent (minutes)'
    },
    xAxis: [
        {
            categories: category
        }
    ],
    yAxis: [
        {
            // Primary yAxis
            labels: {
                format: '{value}',
                style: {
                    color: '#DF8500'
                }
            },
            title: {
                text: 'Visitors',
                style: {
                    color: '#DF8500'
                }
            }
        },
        {
            // Secondary yAxis
            title: {
                text: 'Time Spent',
                style: {
                    color: '#6b8abc'
                }
            },
            labels: {
                format: '{value} m',
                style: {
                    color: '#6b8abc'
                }
            },
            opposite: true
        }
    ],

    tooltip: {
        shared: true
    },

    series: [
        {
            name: 'Time Spent in minutes',
            type: 'column',
            color: '#6b8abc',
            yAxis: 1,
            data: surf_time_all,
            tooltip: {
                pointFormat:
                    '<span style="font-weight: bold; color: {series.color}">{series.name}</span>: <b>{point.y} m</b> ',
                    style: {
                        fontSize: '14px',
                    }
            }
        },
        
        {
            name: 'Visitors',
            type: 'spline',
            color: '#DF8500',
            lineWidth: 2,
            zones: [
                {
                    value: 0,
                    color: '#00ffff'
                },
                {
                    value: 10,
                    color: '#90EE90'
                },
                {
                    value: 15,
                    color: '#FFD700'
                },
                {
                    value: 20,
                    color: '#FFA500'
                },
                {
                    value: 25,
                    color: '#FF8C00'
                },
                {
                    value: 30,
                    color: '#FF5733'
                }
            ],

            data: vistors,
            tooltip: {
                pointFormat:
                    '<br><span style="font-weight: bold; color: {series.color}">{series.name}</span>: <b>{point.y}</b> ',
                style: {
                    fontSize: '14px',
                }
            }
        }
    ],
    exporting: {
        enabled: false
    },
    credits: {
        enabled: false
    }
});

/* device based pie chart */

Highcharts.chart('container_device_based', {
    chart: {
        type: 'pie'
    },
    title: {
        text: 'Visitors Devices'
    },
    // tooltip: {
    //     valueSuffix: '%'
    // },
    tooltip: {
        // formatter: function () {
        //     return '</b>' + this.y + '</b>';
        // }
    },
    subtitle: {
        text:''
    },
    plotOptions: {
        series: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: [{
                enabled: true,
                distance: 20
            }, {
                enabled: true,
                distance: -40,
                format: '{point.percentage}%',
                style: {
                    fontSize: '1.2em',
                    textOutline: 'none',
                    opacity: 0.7
                },
                filter: {
                    operator: '>',
                    property: 'percentage',
                    value: 10
                }
            }]
        }
    },
    series: [
        {
            name: 'Count ',
            colorByPoint: true,
            data: data.devices
        }
    ]
});

/* device based pie chart */

/* City based column chart */
/*Highcharts.chart('container_city', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Visitors vs City',
        align: 'left'
    },
    subtitle: {
        text:
            '',
        align: 'left'
    },
    xAxis: {
        categories: data.cities,
        crosshair: true,
        accessibility: {
            description: 'Cities'
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Number of Visitors'
        }
    },
    tooltip: {
        valueSuffix: ' '
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [
        {
            name: 'Visitors',
            data: data.city_visitors
         },
        // {
        //     name: 'User',
        //     data: [51086, 136000, 5500, 141000, 107180, 77000]
        // }
    ]
});*/

/* City based column chart */

/* in and out session */

Highcharts.chart('container_session_based', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Guest vs User',
        align: 'left'
    },
    subtitle: {
        text:'',
        align: 'left'
    },
    xAxis: {
        categories: category,
        crosshair: true,
        accessibility: {
            description: ''
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Visitors Count'
        }
    },
    tooltip: {
        valueSuffix: ' '
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [
        {
            name: 'In Session',
            data: data.user
        },
        {
            name: 'Out Session',
            data: data.guest
        }
    ]
});



/* in and out session */

/* Product Sales */

obj=data.product_sale_count;
sale = Object.keys(obj).map(function (key) {
 return [key, obj[key]];
});

Highcharts.chart('container_products_sale', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Products Purchase List (Delivered)'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            autoRotation: [-45, -90],
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total Sale Count (Qty)'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: '<b>{point.y}</b>'
    },
    series: [{
        name: 'Population',
        colors: [
            '#9b20d9', '#9215ac', '#861ec9', '#7a17e6', '#7010f9', '#691af3',
            '#6225ed', '#5b30e7', '#533be1', '#4c46db', '#4551d5', '#3e5ccf',
            '#3667c9', '#2f72c3', '#277dbd', '#1f88b7', '#1693b1', '#0a9eaa',
            '#03c69b',  '#00f194'
        ],
        colorByPoint: true,
        groupPadding: 0,
        data:sale,
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '12px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});


/* Product Sales Amount */

obj_1=data.product_sale_amount;
sale_amount = Object.keys(obj_1).map(function (key) {
 return [key, obj_1[key]];
});

Highcharts.chart('container_products_sale_amount', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Products Purchase Value in Rs. (Delivered)'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            autoRotation: [-45, -90],
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Order Amount (Rs)'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: '<b>Rs.{point.y}</b>'
    },
    series: [{
        name: '',
        colors: [
            '#9b20d9', '#9215ac', '#861ec9', '#7a17e6', '#7010f9', '#691af3',
            '#6225ed', '#5b30e7', '#533be1', '#4c46db', '#4551d5', '#3e5ccf',
            '#3667c9', '#2f72c3', '#277dbd', '#1f88b7', '#1693b1', '#0a9eaa',
            '#03c69b',  '#00f194'
        ],
        colorByPoint: true,
        groupPadding: 0,
        data:sale_amount,
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '12px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});


/* Product Sales Amount */

/* Product Count vs Product Sale */

Highcharts.chart('container_product_sale_profit', {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: 'Product Count vs Product Purchase Value (Rs.)'
    },
    xAxis: [
        {
            categories: data.comb_sku_list
        }
    ],
    yAxis: [
        {
            // Primary yAxis
            labels: {
                format: '{value}',
                style: {
                    color: '#DF8500'
                }
            },
            title: {
                text: 'Purchase Count',
                style: {
                    color: '#DF8500'
                }
            }
        },
        {
            // Secondary yAxis
            title: {
                text: 'Purchase Amount',
                style: {
                    color: '#6b8abc'
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: '#6b8abc'
                }
            },
            opposite: true
        }
    ],

    tooltip: {
        shared: true
    },

    series: [
        {
            name: 'Purchase Amount in Rs.',
            type: 'column',
            color: '#6b8abc',
            yAxis: 1,
            data: data.comb_sku_amount,
            tooltip: {
                pointFormat:
                    '<span style="font-weight: bold; color: {series.color}">{series.name}</span>: <b>{point.y}</b> '
            }
        },
        
        {
            name: 'Purchase Count',
            type: 'spline',
            color: '#DF8500',
            lineWidth: 2,
            zones: [
                {
                    value: 0,
                    color: '#00ffff'
                },
                {
                    value: 10,
                    color: '#90EE90'
                },
                {
                    value: 15,
                    color: '#FFD700'
                },
                {
                    value: 20,
                    color: '#FFA500'
                },
                {
                    value: 25,
                    color: '#FF8C00'
                },
                {
                    value: 30,
                    color: '#FF5733'
                }
            ],

            data: data.comb_sku_count,
            tooltip: {
                pointFormat:
                    '<br><span style="font-weight: bold; color: {series.color}">{series.name}</span>: <b>{point.y}</b> '
            }
        }
    ],
    exporting: {
        enabled: false
    },
    credits: {
        enabled: false
    },
    dataLabels: {
            enabled: true,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y}', // one decimal
            style: {
                fontSize: '12px',
                // fontFamily: 'Verdana, sans-serif'
            }
        }
});

/* Product Count vs Product Sale */


}

function VisitorAnalytics(){

    //alert();
//var report_type=$(obj).attr('report');
var report_type="total_time_spent";
var from_date=document.getElementById('from_date').value
var to_date=document.getElementById('to_date').value


//alert(report_type);

var xhr = false;
if (window.XMLHttpRequest) {
    xhr = new XMLHttpRequest();
}else {
    xhr = new ActiveXObject("Microsoft.XMLHTTP");
}
var xhr = new XMLHttpRequest();
        if (xhr) {
                         
            //obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
            xhr.onreadystatechange = function () {
                //alert(2222)
                if (xhr.readyState == 4 && xhr.status == 200) {
                    
                    if(report_type=="total_time_spent"){
                        //alert(4444444);
                        VisitorTimeSpent(xhr.responseText);
                        //document.getElementById('filter_button').setAttribute('report','time_spent');
                        // document.getElementById('filter_button').setAttribute('onclick','VendorGetCustomer(this)');
                        //document.getElementById('from_date').value=""
                        //document.getElementById('to_date').value=""
                    }
                    
                    /* newly added */
                    
                    if(xhr.responseText==1){

                    }else{
                       //obj.innerHTML='<i class="fa fa-trash-o" aria-hidden="true"></i>'
                    }
                }
            }
            xhr.open('POST', base_url+"admin/Analytics/getVisitorTimeSpent", true);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.send('report_type='+report_type+'&from_date='+from_date+'&to_date='+to_date);
        }
}
/* new  */
$(document).ready(function(){
    VisitorAnalytics();
    //GetProductSales();
});



</script>

