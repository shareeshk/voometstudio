<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Analytics</title> 
<style type="text/css">
body{
overflow-x: hidden;
}
.navbar{
min-height: 0px;
margin-bottom: 0px; 
}
.slidebar-form {
padding: 7px 15px 13px 15px;
}
.slidebar-nav {
left: 250px;
list-style: none;
height: 100%;
margin: 0;
margin-left: -250px;  
overflow-y: auto;
padding: 0;
position: fixed; 
width: 0;
z-index: 1000;
-webkit-transition: all 0.5s ease;
-moz-transition: all 0.5s ease;
-o-transition: all 0.5s ease;
transition: all 0.5s ease;
} 
#wrapper_analytics.toggled .slidebar-nav {
width: 250px;
}
.slidebar-nav .navbar-collapse {
padding: 0;
max-height: none;
}
.slidebar-nav ul {
float: none;
width: 100%;
}
.slidebar-nav ul:not {
display: block;
}
.slidebar-nav li {
float: none;
display: block;
}
.slidebar-nav li a {
padding-top: 12px;
padding-bottom: 12px;
}
.slidebar-nav .open .dropdown-menu {
position: static;
float: none;
width: auto;
margin: 0;
padding: 5px 0;
background-color: transparent;
border: 0;
-webkit-box-shadow: none;
box-shadow: none;
}
.slidebar-nav .open .dropdown-menu > li > a {
padding: 5px 15px 5px 25px;
}
.slidebar-nav .navbar-brand {
width: 100%;
}
.slidebar-nav .open > a > b.caret {
border-top: none;
border-bottom: 4px solid;
}
.slidebar-nav .navbar-nav {
margin: 0;
}

#wrapper_analytics {
padding-left: 0;
-webkit-transition: all 0.5s ease;
-moz-transition: all 0.5s ease;
-o-transition: all 0.5s ease;
transition: all 0.5s ease;
}
#wrapper_analytics.toggled {
padding-left: 250px;
}
#page-wrapper6 {
width: 100%;
position: absolute;
padding: 15px;
top: 50px;
}
#wrapper_analytics.toggled #page-wrapper6 {
position: absolute;
margin-right: -250px;
}
@media(min-width:768px) {
#wrapper_analytics {
/*padding-left: 250px;*/
}
#wrapper_analytics.toggled {
padding-left: 0;
}
#page-wrapper6 {
padding: 20px;
position: relative;
}
#wrapper_analytics.toggled #page-wrapper6 {
position: relative;
margin-right: 0;
}
.slidebar-nav {
width: 250px;;
}
#wrapper_analytics.toggled .slidebar-nav {
width: 0;
}
}

.slidebar-toggle {
position: relative;
float: left;
padding: 9px 10px;
background-color: transparent;
background-image: none;
border: 1px solid transparent;
border-radius: 4px;
}
.slidebar-toggle:focus {
outline: 0;
}
.slidebar-toggle .icon-bar {
display: block;
width: 22px;
height: 2px;
border-radius: 1px;
}
.slidebar-toggle .icon-bar + .icon-bar {
margin-top: 4px;
}
.navbar-default .slidebar-toggle {
border-color: #ddd;
}
.navbar-default .slidebar-toggle:hover,
.navbar-default .slidebar-toggle:focus {
background-color: #ddd;
}
.navbar-default .slidebar-toggle .icon-bar {
background-color: #888;
}

/*
* White navbar style
*/

#navbar-white.navbar-default { /* #efffff - #ffffff */
font-size: 14px;

border: 0px;
border-radius: 0;
}

#navbar-white.navbar-default .navbar-toggle,
#navbar-white.navbar-default .slidebar-toggle {
border-color: #ccffff;
}

#navbar-white.navbar-default .navbar-nav>li>a,
#navbar-white.navbar-default .navbar-nav>li>ul>li>a, 
#navbar-white.navbar-default .navbar-brand {
color: #999999; 
}
</style>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<link href="<?php echo base_url();?>assets/css/highslide.css" rel="stylesheet" /> 
<script> var base_url="<?php echo base_url(); ?>";</script>  
<script src="<?php echo base_url()?>assets/analytic_js/highcharts.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/data.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/exporting.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/highslide-full.min.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/highslide.config.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/drilldown.js"></script> 
<script src="<?php echo base_url()?>assets/analytic_js/vendor_customer.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/page.js"></script>  
        
<script src="<?php echo base_url()?>assets/jqueryui/jquery-ui.min.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 
<script type="text/javascript">
            
            
            function emptyStartEndDate(){
             //   document.getElementById("from_date").value="";
              //  document.getElementById("to_date").value=""
            }
            function emptyYear(){
               // document.getElementById("particular_year").value="";
            }
			$(document).ready(function (){	
           $('#to_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'YYYY-MM-DD'
			});
			
		$('#from_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'YYYY-MM-DD', 
				shortTime : true
			}).on('change', function(e, date)
			{
				$('#to_date').bootstrapMaterialDatePicker('setMinDate', date);
			});
			$('#particular_year').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		weekStart: 0, 
		format: 'YYYY', 
		shortTime : true
	}).on('change', function(e, date){
		$('#particular_year').bootstrapMaterialDatePicker('setMinDate', date);
	});
});			
        </script>    
</head>
  <body>
  
 <div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>

  <div id="wrapper_analytics">    
    <main id="page-wrapper6">
        <div class="container-fluid"> 
         <div class="row" style="display:none;" id="filter_container">
          <div class="col-md-8">
         <div class="row">
            <div id="dateRangePicker">
            <div class='col-md-3'>
                <div class="form-group">
                   <input type='text' class="form-control" id='from_date' onclick="emptyYear()" placeholder="From date" value="<?php echo date("Y-m-d", strtotime("-30 days"));?>"/>
                </div>
            </div>  
            <div class='col-md-3'>
                <div class="form-group">
                    <input type='text' class="form-control" id='to_date' onclick="emptyYear()" placeholder="To date" value="<?php echo date("Y-m-d");?>"/>
                </div>
            </div>
            </div>
            <?php 
            
            if($menu_flag=='vendor_inventory_view'){ 
              
              ?>
            <div class='col-md-2' id="limit_result">
                <div class="form-group">
                    <input type='number' min="10" class="form-control" id="limit_result_val"  placeholder="Limit Result" value="10" />
                </div>
            </div>

            <?php }else{
?>

            <div class='col-md-2' id="limit_result" style="display:none;">
                <div class="form-group">
                    <input type='number' min="10" class="form-control" id="limit_result_val"  placeholder="Limit Result" value="10" />
                </div>
            </div>
<?php

            } ?>

            <?php
            
            //echo $menu_flag;
            if($menu_flag=='vendor_analytics_customers'){ ?>
                <div class='col-md-2' id="limit_result_life_cycle" style="display: none;">
                <div class="form-group">
                    <select class="form-control" name="life_cycle_years_val" id="life_cycle_years_val" required>
                        <option value="1">1 yr</option>
                        <option value="2">2 yrs</option>
                        <option value="3">3 yrs</option>
                        <option value="4">4 yrs</option>
                        <option value="5">5 yrs</option>
                        <option value="6">6 yrs</option>
                        <option value="7">7 yrs</option>
                        <option value="8">8 yrs</option>
                        <option value="9">9 yrs</option>
                        <option value="10" selected>10 yrs</option>

                    </select>
                </div>
                </div>
            <?php }?>
            
            <?php
            
            //echo $menu_flag;
            if($menu_flag=='vendor_visitors_traffic'){ ?>
                <div class='col-md-2' id="page_val_div" style="display: none;">
                <div class="form-group">
                    <select class="form-control" name="page" id="page_val" required>
                        <option value="" selected>Home page</option>
                        <option value="catalog/null/boxes-and-bags/corrugated-boxes">boxes-and-bags/corrugated-boxes</option>
                        <option value="cart">Cart</option>
                        <option value="checkout">Checkout</option>

                    </select>
                </div>
                </div>


            <?php }else{
                ?>
                <input type="hidden" value="" name="page" id="page_val">
                <?php
            } ?>

            
         </div>
         </div>
         <div class='col-md-2 pull-right'>
                <div class="form-group">
                    <button class="btn btn-warning" id="filter_button"><i class="fa fa-filter"></i> Filter</button>
                </div>
            </div>
        
        </div>  
             <div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
				  <li><a href="#tab_graph"  data-toggle="tab">Graph</a></li>
				</ul>
				<div class="tab-content">
				  <div class="tab-pane active" id="tab_tableformat">
					<div id="container" class="chart_display" style="width:100%;margin: 0 auto"></div>

          <?php if($menu_flag=='vendor_inventory_view'){     
              ?>
          <div id="table_format" style="width: 70%;text-align: center;" class="col-md-offset-2"></div>

          <?php } ?>

				  </div>
				  
				
				 </div>
						
					
			 </div>
        
		
		
        </div>
    </main>
  </div><!-- /#wrapper -->

</div>
        
<script type='text/javascript'>

$(document).ready(function() {
    $("#menu-toggle").click(function(e){e.preventDefault();
    $("#wrapper_analytics").toggleClass("toggled");});
    $('#menu-toggle').click(function () {
        setInterval(function(){ $('#container').highcharts().reflow(); }, 100);
    });
});
function getCustomerRegistrations(){
    setInterval(function(){ $('#container').highcharts().reflow(); }, 100);
    $("#wrapper_analytics").addClass("toggled")
}
</script>
        
    </body>   
</html>