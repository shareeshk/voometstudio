<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Highcharts Example</title> 
<style type="text/css">
body{
overflow-x: hidden;
}
.navbar{
min-height: 0px;
margin-bottom: 0px; 
}
.slidebar-form {
padding: 7px 15px 13px 15px;
}
.slidebar-nav {
left: 250px;
list-style: none;
height: 100%;
margin: 0;
margin-left: -250px;  
overflow-y: auto;
padding: 0;
position: fixed; 
width: 0;
z-index: 1000;
-webkit-transition: all 0.5s ease;
-moz-transition: all 0.5s ease;
-o-transition: all 0.5s ease;
transition: all 0.5s ease;
} 
#wrapper_analytics.toggled .slidebar-nav {
width: 250px;
}
.slidebar-nav .navbar-collapse {
padding: 0;
max-height: none;
}
.slidebar-nav ul {
float: none;
width: 100%;
}
.slidebar-nav ul:not {
display: block;
}
.slidebar-nav li {
float: none;
display: block;
}
.slidebar-nav li a {
padding-top: 12px;
padding-bottom: 12px;
}
.slidebar-nav .open .dropdown-menu {
position: static;
float: none;
width: auto;
margin: 0;
padding: 5px 0;
background-color: transparent;
border: 0;
-webkit-box-shadow: none;
box-shadow: none;
}
.slidebar-nav .open .dropdown-menu > li > a {
padding: 5px 15px 5px 25px;
}
.slidebar-nav .navbar-brand {
width: 100%;
}
.slidebar-nav .open > a > b.caret {
border-top: none;
border-bottom: 4px solid;
}
.slidebar-nav .navbar-nav {
margin: 0;
}

#wrapper_analytics {
padding-left: 0;
-webkit-transition: all 0.5s ease;
-moz-transition: all 0.5s ease;
-o-transition: all 0.5s ease;
transition: all 0.5s ease;
}
#wrapper_analytics.toggled {
padding-left: 250px;
}
#page-wrapper6 {
width: 100%;
position: absolute;
padding: 15px;
top: 50px;
}
#wrapper_analytics.toggled #page-wrapper6 {
position: absolute;
margin-right: -250px;
}
@media(min-width:768px) {
#wrapper_analytics {
/*padding-left: 250px;*/
}
#wrapper_analytics.toggled {
padding-left: 0;
}
#page-wrapper6 {
padding: 20px;
position: relative;
}
#wrapper_analytics.toggled #page-wrapper6 {
position: relative;
margin-right: 0;
}
.slidebar-nav {
width: 250px;;
}
#wrapper_analytics.toggled .slidebar-nav {
width: 0;
}
}

.slidebar-toggle {
position: relative;
float: left;
padding: 9px 10px;
background-color: transparent;
background-image: none;
border: 1px solid transparent;
border-radius: 4px;
}
.slidebar-toggle:focus {
outline: 0;
}
.slidebar-toggle .icon-bar {
display: block;
width: 22px;
height: 2px;
border-radius: 1px;
}
.slidebar-toggle .icon-bar + .icon-bar {
margin-top: 4px;
}
.navbar-default .slidebar-toggle {
border-color: #ddd;
}
.navbar-default .slidebar-toggle:hover,
.navbar-default .slidebar-toggle:focus {
background-color: #ddd;
}
.navbar-default .slidebar-toggle .icon-bar {
background-color: #888;
}

/*
* White navbar style
*/

#navbar-white.navbar-default { /* #efffff - #ffffff */
font-size: 14px;

border: 0px;
border-radius: 0;
}

#navbar-white.navbar-default .navbar-toggle,
#navbar-white.navbar-default .slidebar-toggle {
border-color: #ccffff;
}

#navbar-white.navbar-default .navbar-nav>li>a,
#navbar-white.navbar-default .navbar-nav>li>ul>li>a, 
#navbar-white.navbar-default .navbar-brand {
color: #999999; 
}
</style>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<link href="<?php echo base_url();?>assets/css/highslide.css" rel="stylesheet" /> 
<script> var base_url="<?php echo base_url()?>"</script>  
<script src="<?php echo base_url()?>assets/analytic_js/highcharts.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/data.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/exporting.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/highslide-full.min.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/highslide.config.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/drilldown.js"></script> 
<script src="<?php echo base_url()?>assets/analytic_js/customer.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/product.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/promotions.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/orders.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/returns.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/customer_age.js"></script>  
<script src="<?php echo base_url()?>assets/analytic_js/customer_gender.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/customer_registrations.js"></script>  
<script src="<?php echo base_url()?>assets/analytic_js/customer_price_segmentation.js"></script>  
<script src="<?php echo base_url()?>assets/analytic_js/product_searched_keywords.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/products_drill.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/products_purchased.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/products_rated.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/numberOfPromotions.js"></script> 
<script src="<?php echo base_url()?>assets/analytic_js/popularPromotions.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/promotions_per_month.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/replacementCases.js"></script> 
<script src="<?php echo base_url()?>assets/analytic_js/refund_cases.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/dailyPurchases.js"></script> 
<script src="<?php echo base_url()?>assets/analytic_js/dailyPurchasesComplete.js"></script>     
<script src="<?php echo base_url()?>assets/analytic_js/cancelledOrders.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/purchaseTrends.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/paymentmethods.js"></script>
<script src="<?php echo base_url()?>assets/analytic_js/orders_stages.js"></script>  
        
<script src="<?php echo base_url()?>assets/analytic_js/page.js"></script>  
        
<script src="<?php echo base_url()?>assets/jqueryui/jquery-ui.min.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 
<script type="text/javascript">
            $(function () {
                /*$('#from_date').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
                $('#to_date').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
                $('#particular_year').datetimepicker({
                    format: 'YYYY'
                });*/
            });
            
            function emptyStartEndDate(){
               // document.getElementById("from_date").value="";
               // document.getElementById("to_date").value=""
            }
            function emptyYear(){
                //document.getElementById("particular_year").value="";
            }
			$(document).ready(function (){	
           $('#to_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'YYYY-MM-DD'
			});
			
		$('#from_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'YYYY-MM-DD', 
				shortTime : true
			}).on('change', function(e, date)
			{
				$('#to_date').bootstrapMaterialDatePicker('setMinDate', date);
			});
			$('#particular_year').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		weekStart: 0, 
		format: 'YYYY', 
		shortTime : true
	}).on('change', function(e, date){
		$('#particular_year').bootstrapMaterialDatePicker('setMinDate', date);
	});
});			
        </script>    
</head>
  <body>
  
 <div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>

  <div id="wrapper_analytics">    
    <main id="page-wrapper6">
        <div class="container-fluid"> 
         <div class="row" style="display:none" id="filter_container">
          <div class="col-md-8">
         <div class="row">
            <div id="dateRangePicker">
            <div class='col-md-3'>
                <div class="form-group">
                   <input type='text' class="form-control" id='from_date' onclick="emptyYear()" placeholder="From date" value="<?php echo date("Y-m-d", strtotime("-30 days"));?>"/>
                </div>
            </div>  
            <div class='col-md-3'>
                <div class="form-group">
                    <input type='text' class="form-control" id='to_date' onclick="emptyYear()" placeholder="To date" value="<?php echo date("Y-m-d");?>"/>
                </div>
            </div>
            </div>
            <div class='col-md-2'>
                <div class="form-group">
                    <input type='text' class="form-control" id='particular_year' onclick="emptyStartEndDate()" placeholder="Particular Year" value="<?php echo date("Y");?>"/>
                </div>
            </div>
            <div class='col-md-2' id="limit_result">
                <div class="form-group">
                    <input type='text' class="form-control" id="limit_result_val"  placeholder="Limit Result"/>
                </div>
            </div>
            <div class='col-md-2' id="limit_result_order">
                <div class="form-group">
                  <select id="limit_result_order_val" class="form-control" name="limit_result_order_val">
                      <option value="desc">Top To Bottom</option>
                      <option value="asc">Bottom To Top</option>
                  </select>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="form-group">
                 <select id="select_level" class="form-control" name="select_level">
                     
                 </select>
                </div>
             </div>
         </div>
         </div>
         <div class='col-md-2 pull-right'>
                <div class="form-group">
                    <button class="btn btn-warning" id="filter_button"><i class="fa fa-filter"></i> Filter</button>
                </div>
            </div>
        
        </div>  
             <div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
				  <li class="active"><a href="#tab_tableformat"  data-toggle="tab">Table Format</a></li>
				  <li><a href="#tab_graph"  data-toggle="tab">Graph</a></li>
				</ul>
				<div class="tab-content">
				  <div class="tab-pane active" id="tab_tableformat">
					<div id="container" style="width:100%;margin: 0 auto"></div>
				  </div>
				  
				  <div class="tab-pane" id="tab_graph">
				  <script>
				  function graph_home(){						
						$.ajax({
							url:base_url+"admin/Analytics/get_graph_home",
							data:"graph_format_json="+document.getElementById("graph_format_json").value,
							type:"POST",
							dataType:"JSON",
							success:function(data){
								////////////////////////////////
								if(data.users_graph!=""){
									from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Visitor  Analytics (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Visitor Analytics (For All Time)";
									}
								Highcharts.chart('container_graph_users_types', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: titletext
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y}',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            },
			 showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: data.users_graph
    }]
});
							}
///////////////////////////
if(data.total_sections_graphs!=""){
	from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Section  Analytics (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Section Analytics (For All Time)";
									}
Highcharts.chart('container_graph_total', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: titletext
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y} s</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y} s',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            },
			 showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: data.total_sections_graphs
    }]
});
							}
//////////////////////
if(data.guest_sections_graphs!=""){
	from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Section  Analytics - Guests (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Section Analytics - Guests (For All Time)";
									}
Highcharts.chart('container_graph_guest', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: titletext
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y} s</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y} s',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            },
			 showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: data.guest_sections_graphs
    }]
});
							}
///////////////////////
if(data.users_sections_graphs!=""){
	from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Section  Analytics - Users (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Section Analytics - Users (For All Time)";
									}
										Highcharts.chart('container_graph_users', {
											chart: {
												plotBackgroundColor: null,
												plotBorderWidth: null,
												plotShadow: false,
												type: 'pie'
											},
											title: {
												text: titletext
											},
											tooltip: {
												pointFormat: '{series.name}: <b>{point.y} s</b>'
											},
											plotOptions: {
												pie: {
													allowPointSelect: true,
													cursor: 'pointer',
													dataLabels: {
														enabled: true,
														format: '<b>{point.name}</b>: {point.y} s',
														style: {
															color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
														}
													},
													showInLegend: true
												}
											},
											series: [{
												name: 'Brands',
												colorByPoint: true,
												data: data.users_sections_graphs
											}]
										});
}
								}
							});
						
					}
					function graphBySKU_product(obj){
						type_menu=document.getElementById("type_of_menu").value;
						skuId=obj.value;
						$.ajax({
							url:base_url+"admin/Analytics/get_graph_by_sku_product",
							data:"graph_format_json="+document.getElementById("graph_format_json").value+"&skuId="+skuId+"&type_menu="+type_menu,
							type:"POST",
							dataType:"JSON",
							success:function(data){
								////////////////////////////////
								if(data.users_graph!=""){
									from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Visitor  Analytics (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Visitor Analytics (For All Time)";
									}
								Highcharts.chart('container_graph_users_types', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: titletext
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y}',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            },
			 showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: data.users_graph
    }]
});
							}
///////////////////////////
if(data.total_sections_graphs!=""){
	from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Section  Analytics (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Section Analytics (For All Time)";
									}
Highcharts.chart('container_graph_total', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: titletext
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y} s</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y} s',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            },
			 showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: data.total_sections_graphs
    }]
});
							}
//////////////////////
if(data.guest_sections_graphs!=""){
	from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Section  Analytics - Guests (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Section Analytics - Guests (For All Time)";
									}
Highcharts.chart('container_graph_guest', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: titletext
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y} s</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y} s',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            },
			 showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: data.guest_sections_graphs
    }]
});
							}
///////////////////////
if(data.users_sections_graphs!=""){
	from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Section  Analytics - Users (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Section Analytics - Users (For All Time)";
									}
										Highcharts.chart('container_graph_users', {
											chart: {
												plotBackgroundColor: null,
												plotBorderWidth: null,
												plotShadow: false,
												type: 'pie'
											},
											title: {
												text: titletext
											},
											tooltip: {
												pointFormat: '{series.name}: <b>{point.y} s</b>'
											},
											plotOptions: {
												pie: {
													allowPointSelect: true,
													cursor: 'pointer',
													dataLabels: {
														enabled: true,
														format: '<b>{point.name}</b>: {point.y} s',
														style: {
															color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
														}
													},
													showInLegend: true
												}
											},
											series: [{
												name: 'Brands',
												colorByPoint: true,
												data: data.users_sections_graphs
											}]
										});
}
								}
							});
						
					}
					
					function graph_categoryFun(obj){
						type_menu=document.getElementById("type_of_menu").value;
						skuId=obj.value;
						$.ajax({
							url:base_url+"admin/Analytics/get_graph_categories",
							data:"graph_format_json="+document.getElementById("graph_format_json").value+"&skuId="+skuId+"&type_menu="+type_menu,
							type:"POST",
							dataType:"JSON",
							success:function(data){
								////////////////////////////////
								if(data.users_graph!=""){
									from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Visitor  Analytics (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Visitor Analytics (For All Time)";
									}
								Highcharts.chart('container_graph_users_types', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: titletext
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y}',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            },
			 showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: data.users_graph
    }]
});
							}
///////////////////////////
if(data.total_sections_graphs!=""){
	from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Section  Analytics (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Section Analytics (For All Time)";
									}
Highcharts.chart('container_graph_total', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: titletext
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y} s</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y} s',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            },
			 showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: data.total_sections_graphs
    }]
});
							}
//////////////////////
if(data.guest_sections_graphs!=""){
	from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Section  Analytics - Guests (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Section Analytics - Guests (For All Time)";
									}
Highcharts.chart('container_graph_guest', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: titletext
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y} s</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y} s',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            },
			 showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: data.guest_sections_graphs
    }]
});
							}
///////////////////////
if(data.users_sections_graphs!=""){
	from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Section  Analytics - Users (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Section Analytics - Users (For All Time)";
									}
										Highcharts.chart('container_graph_users', {
											chart: {
												plotBackgroundColor: null,
												plotBorderWidth: null,
												plotShadow: false,
												type: 'pie'
											},
											title: {
												text: titletext
											},
											tooltip: {
												pointFormat: '{series.name}: <b>{point.y} s</b>'
											},
											plotOptions: {
												pie: {
													allowPointSelect: true,
													cursor: 'pointer',
													dataLabels: {
														enabled: true,
														format: '<b>{point.name}</b>: {point.y} s',
														style: {
															color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
														}
													},
													showInLegend: true
												}
											},
											series: [{
												name: 'Brands',
												colorByPoint: true,
												data: data.users_sections_graphs
											}]
										});
}
								}
							});
						
					}
					///
					
					function graphBySKU_button(obj){
						type_menu=document.getElementById("type_of_menu").value;
						skuId=obj.value;
						$.ajax({
							url:base_url+"admin/Analytics/get_graph_by_sku_button",
							data:"graph_format_json="+document.getElementById("graph_format_json").value+"&skuId="+skuId+"&type_menu="+type_menu,
							type:"POST",
							dataType:"JSON",
							success:function(data){
								////////////////////////////////
								if(data.users_graph!=""){
									from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Visitor  Analytics (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Visitor Analytics (For All Time)";
									}
								Highcharts.chart('container_graph_users_types', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: titletext
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y}',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            },
			 showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: data.users_graph
    }]
});
							}
///////////////////////////
if(data.total_sections_graphs!=""){
	from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Section  Analytics (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Section Analytics (For All Time)";
									}
Highcharts.chart('container_graph_total', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: titletext
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y} s</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y} s',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            },
			 showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: data.total_sections_graphs
    }]
});
							}
//////////////////////
if(data.guest_sections_graphs!=""){
	from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Section  Analytics - Guests (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Section Analytics - Guests (For All Time)";
									}
Highcharts.chart('container_graph_guest', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: titletext
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y} s</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y} s',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            },
			 showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: data.guest_sections_graphs
    }]
});
							}
///////////////////////
if(data.users_sections_graphs!=""){
	from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Section  Analytics - Users (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Section Analytics - Users (For All Time)";
									}
										Highcharts.chart('container_graph_users', {
											chart: {
												plotBackgroundColor: null,
												plotBorderWidth: null,
												plotShadow: false,
												type: 'pie'
											},
											title: {
												text: titletext
											},
											tooltip: {
												pointFormat: '{series.name}: <b>{point.y} s</b>'
											},
											plotOptions: {
												pie: {
													allowPointSelect: true,
													cursor: 'pointer',
													dataLabels: {
														enabled: true,
														format: '<b>{point.name}</b>: {point.y} s',
														style: {
															color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
														}
													},
													showInLegend: true
												}
											},
											series: [{
												name: 'Brands',
												colorByPoint: true,
												data: data.users_sections_graphs
											}]
										});
}
								}
							});
						
					}
					/////////
					
					
					function graphBySKU_reviews(obj){
						type_menu=document.getElementById("type_of_menu").value;
						skuId=obj.value;
						$.ajax({
							url:base_url+"admin/Analytics/get_graph_by_sku_reviews",
							data:"graph_format_json="+document.getElementById("graph_format_json").value+"&skuId="+skuId+"&type_menu="+type_menu,
							type:"POST",
							dataType:"JSON",
							success:function(data){
								////////////////////////////////
								if(data.users_graph!=""){
									from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Visitor  Analytics (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Visitor Analytics (For All Time)";
									}
								Highcharts.chart('container_graph_users_types', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: titletext
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y}',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            },
			 showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: data.users_graph
    }]
});
							}
///////////////////////////
if(data.total_sections_graphs!=""){
	from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Section  Analytics (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Section Analytics (For All Time)";
									}
Highcharts.chart('container_graph_total', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: titletext
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y} s</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y} s',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            },
			 showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: data.total_sections_graphs
    }]
});
							}
//////////////////////
if(data.guest_sections_graphs!=""){
	from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Section  Analytics - Guests (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Section Analytics - Guests (For All Time)";
									}
Highcharts.chart('container_graph_guest', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: titletext
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y} s</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y} s',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            },
			 showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: data.guest_sections_graphs
    }]
});
							}
///////////////////////
if(data.users_sections_graphs!=""){
	from_date_for_graph=$("#from_date_for_graph").val();
									to_date_for_graph=$("#to_date_for_graph").val();
									if(from_date_for_graph!=""){
										titletext="Section  Analytics - Users (From:"+from_date_for_graph+" - To:"+to_date_for_graph+")";
									}
									else{
										titletext="Section Analytics - Users (For All Time)";
									}
										Highcharts.chart('container_graph_users', {
											chart: {
												plotBackgroundColor: null,
												plotBorderWidth: null,
												plotShadow: false,
												type: 'pie'
											},
											title: {
												text: titletext
											},
											tooltip: {
												pointFormat: '{series.name}: <b>{point.y} s</b>'
											},
											plotOptions: {
												pie: {
													allowPointSelect: true,
													cursor: 'pointer',
													dataLabels: {
														enabled: true,
														format: '<b>{point.name}</b>: {point.y} s',
														style: {
															color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
														}
													},
													showInLegend: true
												}
											},
											series: [{
												name: 'Brands',
												colorByPoint: true,
												data: data.users_sections_graphs
											}]
										});
}
								}
							});
						
					}
					
				  </script>
				  <textarea id="graph_format_json" style="display:none;"></textarea>
				  <input type="hidden" id="from_date_for_graph">
				  <input type="hidden" id="to_date_for_graph">
				  <input type="hidden" id="type_of_menu">
					<div class="row" id="product_inventories_div">
						<div class="col-md-12">
							<select id="sku_list" class="form-control" style="width:30%;" onchange="graphBySKU_product(this)">
								<option value="">-SKU List-</option>
							</select>
						</div>
					</div>
					<div class="row" id="categories_div">
						<div class="col-md-12">
							<select id="category_list" class="form-control" style="width:30%;" onchange="graph_categoryFun(this)">
								<option value="">-Category List-</option>
							</select>
						</div>
					</div>
					
					<div class="row" id="buttons_graph_div">
						<div class="col-md-3">
							<select id="sku_list_button" class="form-control" onchange="graphBySKU_button(this)">
								
							</select>
						</div>
					</div>
					
					<div class="row" id="reviews_graph_div">
						<div class="col-md-3">
							<select id="sku_list_reviews" class="form-control" onchange="graphBySKU_reviews(this)">
								
							</select>
						</div>
					</div>
					
					
					<div id="container_graph_users_types" style="width:100%;margin: 0 auto;border-bottom:1px solid #e1e1e1"></div>
					<div id="container_graph_total" style="width:100%;margin: 0 auto;border-bottom:1px solid #e1e1e1"></div>
					<div id="container_graph_guest" style="width:100%;margin: 0 auto;border-bottom:1px solid #e1e1e1"></div>
					<div id="container_graph_users" style="width:100%;margin: 0 auto;border-bottom:1px solid #e1e1e1"></div>
				  </div>
				  
				 </div>
						
					
			 </div>
        
		
		
        </div>
    </main>
  </div><!-- /#wrapper -->

</div>
        
        <script type='text/javascript'>
        
        $(document).ready(function() {
            $("#menu-toggle").click(function(e){e.preventDefault();$("#wrapper_analytics").toggleClass("toggled");});
            $('#menu-toggle').click(function () {
                setInterval(function(){ $('#container').highcharts().reflow(); }, 100);
            });
        });
        function getCustomerRegistrations(){
            setInterval(function(){ $('#container').highcharts().reflow(); }, 100);
            $("#wrapper_analytics").addClass("toggled")
        }
        </script>
        <script>
		
		</script>

        
    </body>   
</html>