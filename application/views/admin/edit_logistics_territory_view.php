<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	

<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script type="text/javascript">

function showDivPrevious(){
	
	var form = document.getElementById('search_for_logistics_territory');		
	
	form.action='<?php echo base_url()."admin/Catalogue/logistics_territory"; ?>';
	form.submit();
			
	 //window.location.href = '<?php echo base_url(); ?>admin/Catalogue/category';
}
</script>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row-fluid" id="editDiv3">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="edit_logistics_territory" id="edit_logistics_territory" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_vendors: {
					required: true,
				},
				edit_logistics: {
					required: true,
				},
				edit_logistics_service: {
					required: true,
				},
				edit_territory_name: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Logistics Territory</h3>
		</div>	
		</div>
		<div class="tab-content">				
			<input type="hidden" name="edit_logistics_territory_id" id="edit_logistics_territory_id" value="<?php echo $get_logistics_territory_data["logistics_territory_id"];?>">
				
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Vendor-</label>
					<select name="edit_vendors" id="edit_vendors" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($vendors as $vendors_value) {  ?>
						<option value="<?php echo $vendors_value->vendor_id; ?>" <?php echo ($vendors_value->vendor_id==$get_logistics_territory_data['vendor_id'])? "selected":''; ?>><?php echo $vendors_value->name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Logistics Name-</label>
					<select name="edit_logistics" id="edit_logistics" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($logistics as $logistics_value) {  ?>
						<option value="<?php echo $logistics_value->logistics_id; ?>" <?php echo ($logistics_value->logistics_id==$get_logistics_territory_data['logistics_id'])? "selected":''; ?>><?php echo $logistics_value->logistics_name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Logistics Service City-</label>
					<select name="edit_logistics_service" id="edit_logistics_service" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($logistics_service as $logistics_service_value) {  ?>
						<option value="<?php echo $logistics_service_value->logistics_service_id; ?>" <?php echo ($logistics_service_value->logistics_service_id==$get_logistics_territory_data['logistics_service_id'])? "selected":''; ?>><?php echo $logistics_service_value->city; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Territory Name-</label>
					<select name="edit_territory_name" id="edit_territory_name" class="form-control">
						<option value=""></option>
						<?php foreach ($logistics_all_territory_name as $logistics_all_territory_name_value) {  ?>
						<option value="<?php echo $logistics_all_territory_name_value->territory_name; ?>" <?php echo ($logistics_all_territory_name_value->territory_name==$get_logistics_territory_data['territory_name'])? "selected":''; ?>><?php echo $logistics_all_territory_name_value->territory_name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>			
			<div class="row">
				<div class="col-md-10 col-md-offset-3"> 
					<button class="btn btn-info btn-sm" type="submit" id="submit-data" disabled="">Submit</button>
		
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
				</div>
			</div>		
		</div>
	</form>
	<form name="search_for_logistics_territory" id="search_for_logistics_territory" method="post">
		<input value="<?php echo $get_logistics_territory_data["vendor_id"]; ?>" type="hidden" id="vendor_id" name="vendor_id"/>
		<input value="<?php echo $get_logistics_territory_data["logistics_id"]; ?>" type="hidden" id="logistics_id" name="logistics_id"/>
		<input value="view" type="hidden" name="create_editview">
	</form>
</div>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>
</body>
</html>
<script type="text/javascript">

function showAvailableLogisticsEdit(obj){
	
	vendor_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_logistics",
				type:"post",
				data:"vendor_id="+vendor_id,
				success:function(data){
					
					if(data!=0){

						$("#edit_logistics").html(data);		
					}
					else{
						$("#edit_logistics").html('<option value="">--No Logistics--</option>');	
						
					}
				}
			});
}

$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_logistics_territory'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_logistics_territory"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_logistics_territory'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});
});

function form_validation_edit()
{

	var vendors = $('select[name="edit_vendors"]').val().trim();
	var logistics_service = $('select[name="edit_logistics_service"]').val().trim();
	var territory_name = $('select[name="edit_territory_name"]').val().trim();	
	var logistics = $('select[name="edit_logistics"]').val().trim();
	   var err = 0;
		if((vendors=='') || (logistics_service=='') || (territory_name=='') || (logistics=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
				document.getElementById("edit_vendors").disabled="";
				document.getElementById("edit_logistics").disabled="";
				document.getElementById("edit_logistics_service").disabled="";
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#edit_logistics_territory');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_logistics_territory/"?>',
				type: 'POST',
				data: $('#edit_logistics_territory').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {			
				if(data)
				{
					swal({
						title:"Success", 
						text:"Logistics territory is successfully updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				else
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)	
				}
				
			});
}
}]);			
			}
	
	return false;
}


</script>
