<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Cancelled Orders</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<style>
.popover{
	min-width:80rem;
}
.fa-question-circle{
	color:#e46c0a;
}
</style>
<script type="text/javascript">
var table;

$(document).ready(function(){
$('#cancelled_orders_table').on('click', function(e){
	$('.orderid_details_popover').each(function () {
        // hide any open popovers when the anywhere else in the body is clicked
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
           // if($('.orderid_details_popover').length>1)
           // $('.orderid_details_popover').popover('hide');
            $(e.target).popover('toggle');
           
            });
});
function fetchData(){
	var fetch_data = '';  
               orderid_orderitemid=$(this).attr("id");
			   orderid_to_get_history=orderid_orderitemid.split("_")[1]; 
			   orderitemid_to_get_history=orderid_orderitemid.split("_")[2];
                $.ajax({  
                    url:"<?php echo base_url()?>admin/Orders/get_order_summary_mail_data_from_history_table",
					type:"POST",
					data:"order_id="+orderid_to_get_history+"&order_item_id="+orderitemid_to_get_history, 
                     async:false,  
                     success:function(data){  
                          fetch_data = data;  
                     }  
                });  
                return fetch_data;  
}
$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   table = $('#cancelled_orders_table').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Orders/cancelled_orders_processing", // json datasource
			data: function (d) { d.status_of_order = $('#status_of_order').val(); },
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#cancelled_orders_table_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("cancelled_count").innerHTML=json.recordsFiltered;
				return json.data;
            }
          },
		  drawCallback: function () {
				$('.orderid_details_popover').popover({  
                content:fetchData,  
                html:true,  
                placement:'right'  
           });
		   
		   
		   
                },
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'5%',
         'className': 'dt-body-center'
      },{
         'targets': 1,
         'width':'35%',
      },{
         'targets': 2,
         'width':'25%',
      },{
         'targets': 3,
         'width':'15%',
      },{
         'targets': 4,
         'width':'10%',
      },{
         'targets': 5,
         'width':'10%',
      }],
      'order': [0, 'desc']
   });
});	

function draw_table(){
	table.draw();
}

function open_cancel_refund_issue_fun(order_item_id){
	location.href="<?php echo base_url()?>admin/Orders/cancels_issue_refund/"+order_item_id;
}

</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header"><h4 class="text-center">Cancelled Orders <span class="badge" id="cancelled_count"></span></h4></div>

	<table id="cancelled_orders_table" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
		<tr>
				<th colspan="6">
				<div class="row">
				<div class="col-md-3 col-md-offset-4">
				<select class="form-control" name="status_of_order" id="status_of_order" onchange="draw_table()">
				<option value="is_on_wait" selected>Incoming</option>
				<option value="in_process">Refund in process</option>
				<option value="failure">Refund failure</option>
				<option value="all">All orders</option>
				</select>
				</div>
				</div>
				</th>
		</tr>
		<tr>
				<th class="text-primary small bold">Image</th>
				<th class="text-primary small bold">Order Summary</th>
				<th class="text-primary small bold">Cancellation Details</th>
				<th class="text-primary small bold">Quantity and Price</th>
				<th class="text-primary small bold">Buyer Details</th>
				<th class="text-primary small bold">Refund Status</th>
		</tr>	
		</thead>
	</table>
</div>

  <div class="modal" id="order_summary">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Order Summary</h4>
           </div>
           <div  class="modal-body" id="model_content">
		   <div class="container">
				
			</div>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
  </div>
  
  
  <div class="modal" id="open_refund_type_modal">
     <div class="modal-dialog modal-sm">
        <div class="modal-content">
			<div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Update Refund Type</h4>
			</div>
			<div  class="modal-body" id="content">		
		<form name="update_refund_method_as_wallet" id="update_refund_method_as_wallet" class="form-horizontal">
				<input type="hidden" value="" name="order_item_id" id="order_item_id">
				<div class="form-group">
				<div class="col-md-12">
				<h4 class="title bold">Refund method: Wallet</h4> 
				</div>	
				</div>	
				<div class="form-group">
				<div class="col-md-12">
				<textarea name="refund_type_comments" class="form-control" rows="3" placeholder="Type your comments"></textarea>
				</div>
				</div>
				<div class="form-group">
				<div class="col-md-12">
				<button type="submit" class="btn btn-success btn-sm btn-block"> Submit </button>
				</div>
				</div>
		</form>
			</div>
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
  </div>
  
  <script type="text/javascript">
  $("#order_summary").modal("hide");
  $("#open_refund_type_modal").modal("hide");
  
  function view_order_summary_(order_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Orders/get_data_from_invoice_offers",
		type:"POST",
		data:"order_id="+order_id+"&cancelled_order=1",
		success:function(data){
			
			$("#description").val("");	
			$("#model_content").html(data);
			$("#order_summary").modal("show");
			
		}
	});
	
}

function open_refund_type_modal(order_item_id){
	$("#open_refund_type_modal").modal("show");
	$("#order_item_id").val(order_item_id);
}

$("#update_refund_method_as_wallet").on('submit',(function(e) {
		e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>admin/Orders/update_cancel_table_refund_type",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					
					if(data==true){
					$('#open_refund_type_modal').modal('hide');
						swal({
							title:"success!", 
							text:"Successfully sent", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#open_refund_type_modal").trigger("reset");
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
					
				}	        
		   });
	}));

function update_refund_method_as_wallet(){

	if(order_item_id!=null && order_item_id!=''){
		$.ajax({
			url:"<?php echo base_url()?>admin/Orders/update_cancel_table_refund_type",
			type:"POST",
			data:"order_item_id="+order_item_id,
			success:function(data){
				
				if(data==true){
					alert('successfully updated');
					location.reload();
				}else{
					alert('Something went wrong...! Please check');
				}
				
			}
		});
	}
	
}
function resend_cancel_refund_fun(order_item_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Orders/resend_cancel_refund",
		type:"POST",
		data:"order_item_id="+order_item_id,
		success:function(data){
			if(data){
				location.reload();
				//location.href="<?php echo base_url()?>admin/Orders/cancels_refund_request_orders_accounts_incoming";
			}
		}
	})
}
  </script>
  </div>
</body> 
</html>  