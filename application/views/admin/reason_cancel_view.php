<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style type="text/css">

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.wizard-card .tab-content {
	 min-height: 400px;
	 padding:0px;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
	
<script type="text/javascript">

function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_delete_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
swal({
			title: 'Are you sure?',
			text: "Delete Reason cancel",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {	
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/delete_reason_cancel_selected",
		type:"post",
		data:"selected_list="+selected_list,
		
		success:function(data){
			
			//$("#multiple_delete_button1").html('Delete Selected Items');
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Given "+table+"(s) has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}

function show_sort_order(){
	var reason_name=document.getElementById("reason_name").value;
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/show_reason_cancel_sort_order",
		type:"post",
		data:"reason_name="+reason_name,
		
		success:function(data){
			$("#menu_sort_order").html(data);
		}
	});
}

</script>
<script>
function showDivCreate(){
	
	document.getElementById('viewDiv1').style.display = "none";
	document.getElementById('createDiv2').style.display = "block";
	show_sort_order();
}
function showDivEdit(){
	
	document.getElementById('viewDiv1').style.display = "block";
	document.getElementById('createDiv2').style.display = "none";
	
}
</script>
<?php //print_r($parent_catagories);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">

<div class="row" id="viewDiv1">
	<div class="wizard-header text-center">
		<h5>Customer Cancel Reason 
		</h5>                        		
		                       	
	</div>
<table id="myTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
			<tr>
				<th colspan="6">
					<button id="multiple_delete_button1" class="btn btn-danger btn-xs" onclick="multilpe_delete_fun('reason_cancel')">Delete </button>
					<button id="multiple_delete_button4" class="btn btn-info btn-xs" onclick="showDivCreate()">Go to Create</button>
					
				</th>
			</tr>
			<tr>
				<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
				<th>S. No</th>
				<th>Reason Name </th>
				<th>Sort order</th>
				<th>Last Updated</th>
				<th>Action</th>
			</tr>
</thead>
<tbody>
<?php
$i=0;
foreach ($reason_cancel as $reason_cancel_value) { 
$i++; ?>
<tr>
	<td><input type="checkbox" name="selected_list" value="<?php echo $reason_cancel_value->cancel_reason_id; ?>"></td>
	<td><?php echo $i; ?></td>
	<td><?php echo $reason_cancel_value -> reason_name; ?></td>
	<td>
	<?php 
	echo  $reason_cancel_value -> sort_order;
	
	 ?>
	</td>
	<td><?php echo $reason_cancel_value -> timestamp?></td>

	<td>
	
	<form action="<?php echo base_url()?>admin/Catalogue/edit_reason_cancel_form" method="post"><input type="hidden" value="<?php echo $reason_cancel_value -> cancel_reason_id;?>" name="cancel_reason_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>
	</td>

</tr>
<?php } ?>
</tbody>
</table>
</div>

<div class="row-fluid" id="createDiv2" style="display:none;">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">

	<form name="create_Reason_cancel" id="create_Reason_cancel" method="post" action="#" onsubmit="return form_validation();">
	<input type="hidden" name="sort_order_flag" id="sort_order_flag">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				reason_name: {
					required: true,
				},
				menu_sort_order: {
					required: true,
				}
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Create Reason Cancel</h3>
		</div>	
		</div> 
		
		<div class="tab-content">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Reason cancel</label>
					<input id="reason_name" name="reason_name" type="text" class="form-control"  />
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter the Sort Order</label>
					
					<select name="menu_sort_order" id="menu_sort_order" class="form-control">
						
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit_create_parent_category">Submit</button>
					<button class="btn btn-warning btn-sm" id="reset_create_parent_category" type="reset">Reset</button>
					<button class="btn btn-info btn-sm" type="button" onclick="showDivEdit()">Go to View</button>
					</div>
				</div>
			</div>

		</div>		
	</form>

</div>
</div>
</div>
</div>
</div>
<div class="footer">

</div>
</div>
</body>
</html>

<script type="text/javascript">

$(document).ready(function(){
    $('#myTable').dataTable({ stateSave: true });
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
});

function editReasonCancelFun(cancel_reason_id){
	location.href="<?php echo base_url()?>admin/Catalogue/edit_reason_cancel_form/"+cancel_reason_id;

}
$('#result').hide();
function form_validation(){
	var reason_name = $('input[name="reason_name"]').val().trim();
	var menu_sort_order = $('select[name="menu_sort_order"]').val().trim();
	
	   var err = 0;
		if((reason_name=='') || (menu_sort_order=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}

		if(err==0){	
			var form_status = $('<div class="form_status"></div>');		
			var form = $('#create_Reason_cancel');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();			
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_reason_cancel/"?>',
				type: 'POST',
				data: $('#create_Reason_cancel').serialize(),
				dataType: 'html',
				
			}).done(function(data){
				form_status.html('')
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					document.getElementById("reason_name").value="";
					document.getElementById("reason_name").focus();
				});
			}
			if(data=="yes")
				{
					swal({
					title:"Success", 
					text:"Reason cancel is successfully added!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});  					  
					
					
				}if(data=="no"){
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
				
			});
}
}]);			
			}
	
	return false;
}

</script>
