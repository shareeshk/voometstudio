<html>
<head>
<meta charset="utf-8">
<title>Reviews and Ratings</title>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<style>
#back_to_filter{
	margin-bottom:0.5em;
	margin-top:0.8em;
}
.content{
	margin-top:20px;
}
.text-info{
	margin-left:1em;
}
.form-inline .form-control{
	width:100%;
}
</style>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<script>
var table1;
var table2;
var table3;
$(document).ready(function (){

	$("#reset_form_button").click(function() {
		$(this).closest('form').find("input[type=number]").val("");
	});
	inventory_id='<?php echo (isset($inventory_id))? $inventory_id :'';?>';
	//alert(inventory_id);
    var rows_selected = [];
     table1 = $('#table_rated_inventory_pending').DataTable({
	   
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Ratemanaging/rated_inventory_processing",
			data: function (d) { d.status = "pending"; d.rating = $('#rating').val();d.inventory_id=inventory_id;},
            type: "post",
            error: function(){
              $("#table_rated_inventory_pending_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				
				document.getElementById("ratings_count").innerHTML=json.recordsFiltered;
				
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
   
     table2 = $('#table_rated_inventory_allowed').DataTable({
	   
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Ratemanaging/rated_inventory_processing",
			data: function (d) { d.status = "allowed";d.rating = $('#rating').val();d.inventory_id=inventory_id;},
            type: "post",
            error: function(){
              $("#table_rated_inventory_allowed_processing").css("display","none");
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
   
    table3 = $('#table_rated_inventory_blocked').DataTable({
	   
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Ratemanaging/rated_inventory_processing",
            type: "post",
			data: function (d) { d.status = "blocked";d.rating = $('#rating').val();d.inventory_id=inventory_id;},
            error: function(){
              $("#table_rated_inventory_blocked_processing").css("display","none");
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
  
    $( "#rating" ).on("keyup", function(){
		table1.draw();
		table2.draw();
		table3.draw();
	});
	
	$('#reset_form_button').on('click',function(){
		table1.draw();
		table2.draw();
		table3.draw();
	});
	$("#submit_button").click(function() {
		rating=$('#rating').val();
		if(rating!='' && rating!=null){
			table1.draw();
			table2.draw();
			table3.draw();
		}else{
			alert('Please Enter Rating Value');
		}
	});
	
	$('#back').on('click',function(){
		
		var form = document.getElementById('back_to_filter');		
		form.action='<?php echo base_url(); ?>admin/Ratemanaging/rated_inventory_filter/';
		form.submit();
		
	});
	
});	

function allow(id){

	$.ajax({
		url:"<?php echo base_url()?>admin/Ratemanaging/allow_rated_inventory/"+id,
		type:"post",
		data:"1=2",
		beforeSend: function(){
			$("#allow_btn").html('<i class="fa fa-refresh fa-spin"></i> Processing');
		},
		success:function(data){
			
			$("#allow_btn").html('Allow');
			if(data==true){
			//	alert("Allowed Successfully");
				table1.draw();
				table2.draw();
				table3.draw();
			}
			else{
				alert("Error");
			}
		}
	});

}
function block(id){
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Ratemanaging/notallow_rated_inventory/"+id,
		type:"post",
		data:"1=2",
		beforeSend: function(){
			$("#block_btn").html('<i class="fa fa-refresh fa-spin"></i> Processing');
		},
		success:function(data){
			$("#block_btn").html('Block');
			if(data==true){
				//alert("Blocked Successfully");
				table1.draw();
				table2.draw();
				table3.draw();
			}
			else{
				alert("Error");
			}
		}
	});
}
 
</script>
<div class="container" id="get">
<div class="page-header">
<h4 class="text-center bold">Ratings count <span class="badge"><div id="ratings_count"></div></span></h4>
</div>
<div class="row">
							<ul class="nav nav-pills" data-tabs="tabs">
								<li class="active"><a href="#home" data-toggle="tab">Incoming ratings</a></li>
								<li><a href="#updates" data-toggle="tab">Allowed ratings</a></li>
								<li><a href="#history" data-toggle="tab">Blocked ratings</a></li>
								<li></li>
								<li><form name="back_to_filter" id="back_to_filter" method="post">
<input type="hidden" value="<?php echo $pcat_id; ?>" name="pcat_id">
<input type="hidden" value="<?php echo $cat_id; ?>" name="cat_id">
<input type="hidden" value="<?php echo $subcat_id; ?>" name="subcat_id">
<input type="hidden" value="<?php echo $brand_id; ?>" name="brand_id">
<input type="hidden" value="<?php echo $product_id; ?>" name="product_id">
<input type="hidden" value="<?php echo $inventory_id; ?>" name="inventory_id">
<a id="back" class="text-info bold" style="cursor:pointer">Go back</a>
</form></li>
							</ul>
							<div class="content">
								<div class="tab-content text-center">
									<div class="tab-pane active" id="home">
										<table id="table_rated_inventory_pending" class="table table-striped table-bordered" cellspacing="0" width="100%">
		
		<thead>
			<tr>
				<th colspan="6">
				<div class="row">
					<div class="col-md-4"><button id="multiple_delete_button" class="btn btn-success btn-xs" onclick="multilpe_allow_fun('pending ratings',2)">Allow Selected Ratings</button> <button id="multiple_delete_button" class="btn btn-danger btn-xs" onclick="multilpe_block_fun('pending ratings',2)">Block Selected Ratings</button></div>
					<div class="col-md-8">
					<form name="search_for_rating">
					<div class="col-md-4">
					<input type="number" min="1" max="5" class="form-control" name="rating" value="" id="rating" placeholder="Search by ratings">
					</div>
					<div class="col-md-8">
					<button type="button" class="btn btn-info btn-xs" id="submit_button">Submit</button>
					<button class="btn btn-info btn-xs" type="reset" id="reset_form_button">Reset</button>
					</div>
					</form>
					</div>
					</div>
				</th>
			</tr>
			<tr>
			<th class="text-primary small bold"><input type="checkbox" id="common_checkbox_pending" name="common_checkbox_pending" onclick="selectAllPendingRatingsFun(this)"></th>
            <th class="text-primary small bold">Customer</th>
            <th class="text-primary small bold">Inventory Name</th>
            <th class="text-primary small bold">Rating</th>
            <th class="text-primary small bold">Review</th>
            <th class="text-primary small bold">Date Posted</th>
       		</tr>
        </thead>
		
  </table>
									</div>
									<div class="tab-pane" id="updates">
										<table id="table_rated_inventory_allowed" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th colspan="6">
					<div class="col-md-4"><button id="multiple_delete_button" class="btn btn-danger btn-xs" onclick="multilpe_block_fun('allowed ratings',1)">Block Selected Ratings</button></div>
					<div class="col-md-8">
					<form name="search_for_rating">
<div class="col-md-6">
<input type="number" min="1" max="5" class="form-control" name="rating" value="" id="rating" placeholder="Search by ratings">
</div>
<div class="col-md-6">
<button type="button" class="btn btn-info btn-xs" id="submit_button">Submit</button>
<button class="btn btn-info btn-xs" type="reset" id="reset_form_button">Reset</button>
</div>
</form>
					</div>
				</th>
			</tr>
			<tr>
			<th class="text-primary small bold"><input type="checkbox" id="common_checkbox_allowed" name="common_checkbox_allowed" onclick="selectAllAllowedRatingsFun(this)"></th>
            <th class="text-primary small bold">Customer</th>
            <th class="text-primary small bold">Inventory Name</th>
            <th class="text-primary small bold">Rating</th>
            <th class="text-primary small bold">Review</th>
            <th class="text-primary small bold">Date Posted</th>
       		</tr>
        </thead>
		
  </table>
									</div>
									<div class="tab-pane" id="history">
										<table id="table_rated_inventory_blocked" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
			<th colspan="6">
				<div class="col-md-4"><button id="multiple_delete_button" class="btn btn-success btn-xs" onclick="multilpe_allow_fun('blocked ratings',0)">Allow Selected Ratings</button></div>
				<div class="col-md-8">
				<form name="search_for_rating">
<div class="col-md-6">
<input type="number" min="1" max="5" class="form-control" name="rating" value="" id="rating" placeholder="Search by ratings">
</div>
<div class="col-md-6">
<button type="button" class="btn btn-info btn-xs" id="submit_button">Submit</button>
<button class="btn btn-info btn-xs" type="reset" id="reset_form_button">Reset</button>
</div>
</form>
				</div>
			</th>
			</tr>
			<tr>
			<th class="text-primary small bold"><input type="checkbox" id="common_checkbox_blocked" name="common_checkbox_blocked" onclick="selectAllBlockedRatingsFun(this)"></th>
            <th class="text-primary small bold">Customer</th>
            <th class="text-primary small bold">Inventory Name</th>
            <th class="text-primary small bold">Rating</th>
            <th class="text-primary small bold">Review</th>
            <th class="text-primary small bold">Date Posted</th>
       		</tr>
        </thead>
		
  </table>
									</div>
								</div>
							</div>

</div>
</div>
</div>
</body>
</html>
<script type="text/javascript">

function selectAllPendingRatingsFun(obj){
	var obj_arr=document.getElementsByName("selected_list_2");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function selectAllAllowedRatingsFun(obj){
	var obj_arr=document.getElementsByName("selected_list_1");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function selectAllBlockedRatingsFun(obj){
	var obj_arr=document.getElementsByName("selected_list_0");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_allow_fun(table,status){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list_"+status);
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Ratemanaging/allow_rated_inventory",
		type:"post",
		data:"selected_list="+selected_list,
		beforeSend: function(){
			$("#multiple_delete_button").html('<i class="fa fa-refresh fa-spin"></i> Processing');
		},
		success:function(data){
			
			$("#multiple_delete_button").html('Delete Selected Items');
			if(data==true){
				alert("Given "+table+"(s) has been allowed successfully");
				table1.draw();
				table2.draw();
				table3.draw();
			}
			else{
				alert("Error");
			}
		}
	});
}
function multilpe_block_fun(table,status){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list_"+status);
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Ratemanaging/notallow_rated_inventory",
		type:"post",
		data:"selected_list="+selected_list,
		beforeSend: function(){
			$("#multiple_delete_button").html('<i class="fa fa-refresh fa-spin"></i> Processing');
		},
		success:function(data){
			
			$("#multiple_delete_button").html('Delete Selected Items');
			if(data==true){
				alert("Given "+table+"(s) has been blocked successfully");
				table1.draw();
				table2.draw();
				table3.draw();
			}
			else{
				alert("Error");
			}
		}
	});
}
$(document).ready(function() {

	var obj_pending=document.getElementsByName("common_checkbox_pending");	
	var obj_allowed=document.getElementsByName("common_checkbox_allowed");	
	var obj_blocked=document.getElementsByName("common_checkbox_blocked");	
	var obj_arr_blocked=document.getElementsByName("selected_list_0");
	var obj_arr_allowed=document.getElementsByName("selected_list_1");
	var obj_arr_pending=document.getElementsByName("selected_list_2");
	
	for(i=0;i<obj_arr_pending.length;i++){
		obj_arr_pending[i].checked=false;
	}
	for(i=0;i<obj_pending.length;i++){
		obj_pending[i].checked=false;
	}
	for(i=0;i<obj_arr_blocked.length;i++){
		obj_arr_blocked[i].checked=false;
	}
	for(i=0;i<obj_blocked.length;i++){
		obj_blocked[i].checked=false;
	}
	for(i=0;i<obj_arr_allowed.length;i++){
		obj_arr_allowed[i].checked=false;
	}
	for(i=0;i<obj_allowed.length;i++){
		obj_allowed[i].checked=false;
	}
	
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
		var obj_pending=document.getElementsByName("common_checkbox_pending");	
		var obj_allowed=document.getElementsByName("common_checkbox_allowed");	
		var obj_blocked=document.getElementsByName("common_checkbox_blocked");	
		var obj_arr_blocked=document.getElementsByName("selected_list_0");
		var obj_arr_allowed=document.getElementsByName("selected_list_1");
		var obj_arr_pending=document.getElementsByName("selected_list_2");
		
		for(i=0;i<obj_arr_pending.length;i++){
			obj_arr_pending[i].checked=false;
		}
		for(i=0;i<obj_pending.length;i++){
			obj_pending[i].checked=false;
		}
		for(i=0;i<obj_arr_blocked.length;i++){
			obj_arr_blocked[i].checked=false;
		}
		for(i=0;i<obj_blocked.length;i++){
			obj_blocked[i].checked=false;
		}
		for(i=0;i<obj_arr_allowed.length;i++){
			obj_arr_allowed[i].checked=false;
		}
		for(i=0;i<obj_allowed.length;i++){
			obj_allowed[i].checked=false;
		}
    });
	
	
});

</script>