<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Returned Orders</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   var table = $('#returned_orders_table').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Orders/returned_orders_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#returned_orders_table_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("returned_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'5%',
         'className': 'dt-body-center'
      },{
         'targets': 1,
         'width':'40%',
      },{
         'targets': 2,
         'width':'20%',
      },{
         'targets': 3,
         'width':'15%',
      },{
         'targets': 4,
         'width':'10%',
      },{
         'targets': 5,
         'width':'10%',
      }],
      'order': [0, 'desc'],
	  "autoWidth": false
   });
});	

function sendInvoiceFun(order_item_id,order_id){
	swal({
			title: 'Are you sure?',
			text: "want to send mail?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Send it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
			$.ajax({
				url:"<?php echo base_url('admin/Orders/generate_invoice_pdf')?>",
				type:'POST',
				data:"order_item_id="+order_item_id+"&order_id="+order_id+"&sendInvoice=yes&type_of_copy=revised",
				beforeSend:function(){
					$('.invoice_sent_'+order_item_id).html('<i class="fa fa-spinner"></i> Processing');
				}
		}).success( function(data){
			if(data){
				$('.invoice_sent_'+order_item_id).html('Send Invoice');
				swal({
						title:"success!", 
						text:"Invoice has been generated successfully", 
						type: "success",
						allowOutsideClick: false	
					}).then(function () {
						window.open("<?php echo base_url()?>assets/pictures/invoices/Invoice_"+order_id+""+order_item_id+"_revised.pdf","_blank");

					});
				
				
			}
			else{
				swal("Error", "not sent", "error");
			}
		});
		})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
		$.ajax({
				url:"<?php echo base_url('admin/Orders/generate_invoice_pdf')?>",
				type:'POST',
				data:"order_item_id="+order_item_id+"&order_id="+order_id+"&sendInvoice=no",
				beforeSend:function(){
					$('.invoice_sent_'+order_item_id).html('<i class="fa fa-spinner"></i> Processing');
				}
		}).success( function(data){
			if(data){
				$('.invoice_sent_'+order_item_id).html('Send Invoice');
				swal({
						title:"success!", 
						text:"Invoice has been generated successfully", 
						type: "success",
						allowOutsideClick: false
						
					}).then(function () {
						window.open("<?php echo base_url()?>assets/pictures/invoices/Invoice_"+order_id+""+order_item_id+"_revised.pdf","_blank");
					});
				
			}
			else{
				swal("Error", "not sent", "error");
			}
		});
				
			  }
			});
	

 }
 function generate_invoice_for_order_ID_offers(order_id,type_of_order){
	if(type_of_order=="cashback"){
		var str="CBK";
	}else{
		var str="SG";
	}
	
	window.open("<?php echo base_url()?>assets/pictures/invoices/Orders/Invoice_"+str+"_"+order_id+".pdf","_blank");
 }
</script>
  
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header"><h4 class="text-center">Returned Orders <span class="badge" id="returned_count"></span></h4></div>
	<table id="returned_orders_table" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr >
				
				<th class="text-primary small bold">Image</th>
				<th class="text-primary small bold">Order Summary</th>
				<th class="text-primary small bold">Order Status</th>
				<th class="text-primary small bold">Quantity and Price</th>
				<th class="text-primary small bold">Buyer Details</th>
				<th class="text-primary small bold">Actions</th>
			</tr>
		</thead>
	</table>
</div>


			
				<div class="modal" id="cancel_order_modal">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title"> Cancel Order</h4>
           </div>
           <div  class="modal-body">
				<form id="cancel_order_form">
				<div class="form-group">
					<select class="form-control" name="cancel_reason" id="cancel_reason" required>
							  		<option value="">Please Choose</option>
							  	<?php foreach($cancel_reason as $reasons){?>
							  		<option value="<?php echo $reasons['cancel_reason_id'] ?>"><?php echo ucfirst($reasons['cancel_reason']); ?> </option>
							  	<?php }?>
					</select>
					</div>
           			<div class="form-group">
						<label>Reason For Cancellation:</label>
           				<textarea class="form-control" rows="5" name="reason_for_cancel_order" id="reason_for_cancel_order" required></textarea>
           			</div>
           			<div class="form-group">
           				<button type="button" class="btn btn-md btn-success" onclick="set_as_cancelled_fun()"> Cancel Order</button>
           			</div>
				</form>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close"class="btn">Close</a>
        </div>
     </div>
    </div>
  </div>
  
  <div class="modal" id="order_summary">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Order Summary</h4>
           </div>
           <div  class="modal-body" id="model_content">
		  
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close"class="btn">Close</a>
        </div>
     </div>
    </div>
  </div>
  
  <script type="text/javascript">
  $("#order_summary").modal("hide");
  function view_order_summary_(order_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Orders/get_data_from_invoice_offers",
		type:"POST",
		data:"order_id="+order_id,
		success:function(data){
			
			$("#description").val("");	
			$("#model_content").html(data);
			$("#order_summary").modal("show");
			
		}
	})
	
}
function go_to_return_details(order_item_id){	
	 $('#return_details_'+order_item_id).attr('action', '<?php echo base_url();?>admin/Orders/return_details_of_order_item').submit();
  
}
  </script>
  </div>
</body> 
</html>  