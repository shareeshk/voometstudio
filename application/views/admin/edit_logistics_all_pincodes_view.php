<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

.remove_range,.remove_range:hover,.remove_field,.remove_field:hover{
	color:red;
}
.edit_remove_range,.edit_remove_range:hover,.edit_remove_field,.edit_remove_field:hover{
	color:red;
}
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>
<script type="text/javascript">
       $(document).ready(function () {
          
		   $.ajax({
			   url:"<?php echo base_url()?>admin/Catalogue/logistics_pincode_count",
			   type:"POST",
			   success:function(data){
				   
				   $("#logistics_pincodes_count").text(data);
			   }
		   })
			

       });    
</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<script>

function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function showDivPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/logistics_all_pincodes';
}
</script>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row-fluid edit_common" id="editDiv3">
<div class="col-md-8 col-md-offset-2">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">

	<form name="edit_logistics_all_pincodes" id="edit_logistics_all_pincodes" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_state: {
					required: true,
				},
				edit_city: {
					required: true,
				},
				edit_pin: {
					required: true,
				},
				'edit_pincode_from[]': {
					required: true,
				},
				'edit_pincode_to[]': {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Logistics Pincodes</h3>
		</div>	
		</div> 
		
		<div class="tab-content">			
			<input type="hidden" name="edit_pincode_id" id="edit_pincode_id" value="<?php echo $get_logistics_all_pincodes_data["pincode_id"];?>">
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Choose State</label>
					<select name="edit_state" id="edit_state" class="form-control">
						<option value=""></option>
						<?php foreach ($logistics_all_states as $logistics_all_states_value) {  ?>
						<option value="<?php echo $logistics_all_states_value->state_id; ?>" <?php echo ($logistics_all_states_value->state_id==$get_logistics_all_pincodes_data['state_id'])? "selected":''; ?>><?php echo $logistics_all_states_value->state_name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Enter City</label>
					<input id="edit_city" name="edit_city" type="text" class="form-control" value="<?php echo $get_logistics_all_pincodes_data["city"];?>"/>
					<input id="edit_city_default" name="edit_city_default" type="hidden" class="form-control" value="<?php echo $get_logistics_all_pincodes_data["city"];?>"/>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Enter PIN (First three digits)</label>
					<input id="edit_pin" name="edit_pin" type="text" class="form-control" value="<?php echo $get_logistics_all_pincodes_data["pin"];?>"/>
				</div>
				</div>
			</div>	
			<div class="row edit-multi-fields">
			<div class="form-group edit_add_range">
				<div class="col-md-4 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Enter Pincodes Starting Number</label>
					<input name="edit_pincode_from[]" id="edit_pincode_from" type="text" class="form-control" />
				</div>
				</div>
				<div class="col-md-4">  
				<div class="form-group">
					<label class="control-label">Enter Pincodes Ending Number</label>
					<input name="edit_pincode_to[]" id="edit_pincode_to" type="text" class="form-control" />
				</div>
				</div>
				<div class="col-md-1">  
				
					<a href="#" class="btn btn-link edit_remove_range">[ X ]</a>
				
				</div>
				</div>
			</div>	
				
					
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<button id="edit_add_one_more" name="edit_add_one_more" class="btn btn-warning edit_add_one_more pull-right btn-sm">Add One More</button>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h4>Distinct Pincodes</h4>
				</div>
			</div>

			<div class="row edit_distinct_values_common">
				<div class="col-md-10 col-md-offset-1">
				<div class="edit_distinct_values" id="edit_distinct_values">
	
				</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group add_range">
				<div class="col-md-6 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Pincode Counts which are distinct</label>
					<input id="edit_pincode_distinct_count" name="edit_pincode_distinct_count" type="text" class="form-control" />
				</div>
				</div>
				<div class="col-md-4">  
				<div class="form-group label-floating">
					<button id="edit_generate_input1" class="btn btn-warning pull-right edit_generate_input_fields btn-sm">Generate pincodes</button>
				</div>
				</div>
				
				</div>
			</div>		
			<div class="row">
				<div class="col-md-8 col-md-offset-3">
				<div class="form-group">
					<button class="btn btn-info btn-sm" id="submit-data" disabled="" type="submit">Submit</button>
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
				</div>	
				</div>
			</div>		

		</div>		
	</form>

</div>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>
</body>
</html>
<script type="text/javascript">

$(function(){
	
$('.common').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add_one_more", $(this)).click(function(e) {
        $('.add_range:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('');
		$('.remove_range').show();		
		return false;
    });
	$($wrapper).on("click",".remove_range", function(e){ 
	elements_length=$('.add_range > div', $wrapper).length;
	e.preventDefault(); 
		if(elements_length==1){
			//$('.remove_range').hide();
		}else{
		$('.remove_range').show();
		$(this).parent('div').parent('div').remove();	
		
		}
        
    })
	//remove of pincode range
});
var x = 1;
$(".generate_input_fields").click(function(e) {
	count = $('input[name="pincode_distinct_count"]').val().trim();

	for(i=0;i<count;i++){
		 x++; 
        $('.distinct_values').append('<div><div class="col-md-3"><div class="form-group label-floating"><input type="text" name="edit_distinct_pincodes[]" class="form-control" placeholder="pincode"/></div></div><div class="col-md-2"><div class="form-group label-floating"><a href="#" class="btn btn-link edit_remove_field">Close</a></div></div></div>');
	}
	$('input[name="pincode_distinct_count"]').val('');	
	return false;
    });
	
	$('.distinct_values_common').on("click",".remove_field", function(e){ 
	e.preventDefault();
	$(this).parent('div').parent('div').remove(); 
	x--;
	});
	
	$('.edit_common').each(function() {
    var $edit_wrapper = $('.edit-multi-fields', this);
    $(".edit_add_one_more", $(this)).click(function(e) {
        $('.edit_add_range:first-child', $edit_wrapper).clone(true).appendTo($edit_wrapper).find('input').val('');
		$('.edit_remove_range').show();		
		return false;
    });
	$($edit_wrapper).on("click",".edit_remove_range", function(e){ 
	elements_length=$('.edit_add_range > div', $edit_wrapper).length;
	e.preventDefault(); 
		if(elements_length==1){
			//$('.edit_remove_range').hide();
		}else{
		$('.edit_remove_range').show();
		$(this).parent('div').parent('div').parent('div').remove();	
		
		}
        
    })
	//remove of pincode range
});
var y = 1;
$(".edit_generate_input_fields").click(function(e) {
	count = $('input[name="edit_pincode_distinct_count"]').val().trim();
	 
	for(i=0;i<count;i++){
		 y++; 
        $('.edit_distinct_values').append('<div class="row"><div class="col-md-3 align_top"><input type="text" name="edit_distinct_pincodes[]" class="form-control" placeholder="pincode"></div><div class="col-md-1 align_top"><a href="#" class="btn btn-link edit_remove_field">X</a></div></div>');
	}
	$('input[name="edit_pincode_distinct_count"]').val('');	
	return false;
    });
	
	$('.edit_distinct_values_common').on("click",".edit_remove_field", function(e){ 
	e.preventDefault();
	$(this).parent('div').parent('div').remove(); y--;
	});
	
});


 editLogisticsAllPincodesFun();
 
function editLogisticsAllPincodesFun(){
	pincode_id='<?php echo $pincode_id; ?>';
	
			
					pincode_id='<?php echo $get_logistics_all_pincodes_data["pincode_id"]?>';
					city='<?php echo $get_logistics_all_pincodes_data["city"]?>';
					pin='<?php echo $get_logistics_all_pincodes_data["pin"]?>';
					pincodes='<?php echo $get_logistics_all_pincodes_data["pincodes"]?>';
					pincodes_range='<?php echo $get_logistics_all_pincodes_data["pincodes_range"]?>';
					distinct_pincodes='<?php echo $get_logistics_all_pincodes_data["distinct_pincodes"]?>';
				
					pincodes_range_arr=pincodes_range.split("|");
					edit_multi_fields="";
					
					for(x in pincodes_range_arr){
						pincodes_range_val=pincodes_range_arr[x];
						pincodes_range_val_arr=pincodes_range_val.split("-");
						edit_multi_fields+='<div class="form-group edit_add_range col-md-offset-1"><div class="row"><div class="col-md-12 col-md-offset-1">Enter Pincode Range</div><div class="col-md-4 col-md-offset-1"><input name="edit_pincode_from[]" type="text" id="edit_pincode_from_'+x+'" class="form-control" placeholder="Enter Pincodes Starting Number" value='+pincodes_range_val_arr[0]+'></div><div class="col-md-4"><input name="edit_pincode_to[]" type="text" id="edit_pincode_to_'+x+'" class="form-control" placeholder="Enter Pincodes Ending Number" value='+pincodes_range_val_arr[1]+'></div><div class="col-md-1"><a href="#" class="btn btn-link edit_remove_range">[ X ]</a></div></div></div>';
					}
					distinct_pincodes_arr=distinct_pincodes.split("|");
					edit_distinct_values='';
					for(x in distinct_pincodes_arr){
						distinct_pincodes_val=distinct_pincodes_arr[x];
					
						edit_distinct_values+='<div class="row"><div class="col-md-3"><div class="form-group label-floating"><input type="text" name="edit_distinct_pincodes[]" class="form-control" placeholder="pincode" value='+distinct_pincodes_val+' ></div></div><div class="col-md-2"><a href="#" class="btn btn-link edit_remove_field">X</a></div></div>';
					}	
					
					$(".edit_distinct_values").html(edit_distinct_values);
					$(".edit-multi-fields").html(edit_multi_fields);

///////////////////////
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_logistics_all_pincodes'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_logistics_all_pincodes"]').bind('change keyup click', function () {

    var disable = true;
    $("form[name='edit_logistics_all_pincodes'] :input").each(function () {
		
			var type = $(this).getType();
			var id = $(this).attr('id');
			if (type == 'text' || type == 'select') {
				//alert(orig[id].value);
				//alert($(this).val());
				disable = (orig[id].value == $(this).val());
				
			} else if (type == 'radio') {
				disable = (orig[id].checked == $(this).is(':checked'));
			}
			if (!disable) {
				return false; // break out of loop
			}
		
    });
    button.prop('disabled', disable);
});

/////////////////////////
					
				

}
$('#result').hide();


function form_validation_edit()
{
	var err = 0;
	var city = $('input[name="edit_city"]').val().trim();
	var pin = $('input[name="edit_pin"]').val().trim();	
	var pincode_from=document.getElementsByName("edit_pincode_from[]");
	var pincode_to=document.getElementsByName("edit_pincode_to[]");

	for(var x=0;x<pincode_from.length;x++)
	{
	
		if(pincode_from[x].value.trim() == '' || (parseInt(pincode_from[x].value.trim()) >= parseInt(pincode_to[x].value.trim())))
		{	
			//document.getElementsByName('edit_pincode_from[]')[x].setAttribute("style", "border: 1px solid red;");
			//document.getElementsByName('edit_pincode_to[]')[x].setAttribute("style", "border: 1px solid red;");				
			err = 1;
		}
		else
		{	
			//document.getElementsByName('edit_pincode_from[]')[x].setAttribute("style", "border: 1px solid #ccc;");
			//document.getElementsByName('edit_pincode_to[]')[x].setAttribute("style", "border: 1px solid #ccc;");				
		}
		
	}
		if(city=='')
		{
		   //$('input[name="edit_city"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('input[name="edit_city"]').css({"border": "1px solid #ccc"});
		}
		if(pin=='')
		{
		   //$('input[name="edit_pin"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('input[name="edit_pin"]').css({"border": "1px solid #ccc"});
		}
		if(err==1)
		{
			   $('#error_result_id').show();
			   $('#result').hide();
		}
		else
			{
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#edit_logistics_all_pincodes');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_logistics_all_pincodes/"?>',
				type: 'POST',
				data: $('#edit_logistics_all_pincodes').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {	
			  
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					
					edit_city_default=document.getElementById("edit_city_default").value;
					document.getElementById("edit_city").value=edit_city_default;
					document.getElementById("edit_city").focus();
					
				});
				  }
				if(data==1)
				{		
					  					 
					swal({
						title:"Success", 
						text:"Logistics all pincode is successfully Updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				if(data==0)
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)	
				}
				
			});
}
}]);			
			}
	
	return false;
}

</script>
