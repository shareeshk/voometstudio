<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Refund Contact Buyer</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<style>
.panel-height {
  min-height: 40vh;
}
</style>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<h4 class="text-center">Offline Chat - <?php echo $return_order_id; ?></h4>

		<?php

			if(!empty($returns_refund_desired_obj)){
				$customer_id=$returns_refund_desired_obj->customer_id;
			}else{
				$customer_id='';
			}
		?>

<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<form id="send_sms_mail_to_customer" name="send_sms_mail_to_customer" method="post" class="form-inline text-center">
				<input type="hidden" name="order_item_id" value="<?php echo $order_item_id; ?>">
				<input type="hidden" name="return_order_id" value="<?php echo $return_order_id; ?>">
				<input type="hidden" name="customer_id" value="<?php echo $customer_id; ?>">
				<input type="hidden" name="request_type" value="refund">	
				<div class="form-group" style="margin-top:0;">
				<div class="col-md-12">
				<input type="checkbox" name="sms" value="1" >
				<label>Send SMS</label>
				</div>
				</div>
				<div class="form-group" style="margin-top:0;">
				<div class="col-md-12">
				<input type="checkbox" name="email" value="1">
				<label for="email">Send Email</label>
				</div>
				</div>
				<div class="form-group" style="margin-top:0;">
				<div class="col-md-12">
				<input type="submit" name="submit" value="Submit" class="btn btn-success btn-sm">
				</div>
				</div>
		</form>
	</div>
	</div>
	<div class="row">
	<div class="col-md-8 col-md-offset-2">
 <form id="send_form_admin_contact_buyer_form" method="post" enctype="multipart/form-data" class="form-horizontal">
	
		<input type="hidden" name="order_id" value="<?php echo $returns_refund_desired_obj->order_id; ?>"> 
		<input type="hidden" name="order_item_id" value="<?php echo $order_item_id; ?>">
		 <div class="panel panel-success">
               <div id="accordion" class="panel-heading text-center lead" style="cursor:pointer;">
                  Conversation with Customer
               </div>
                  <div class="panel-body panel-height">
				   <?php
							if(!empty($returns_refund_desired_obj)){
								
							
							foreach($returns_conservation_obj_arr as $returns_conservation_obj){
								?>
				  <div class="media">
				<div class="media-body">
				<h4 class="media-heading"><?php
									if($returns_conservation_obj->user_type=="admin"){
								 ?>
								 <span class="text-primary">Admin</span>
								 <?php
									}
								 ?>
								 <?php
									if($returns_conservation_obj->user_type=="customer"){
								 ?>
								 <span class="text-info"><?php echo $controller->get_customer_name($returns_conservation_obj->customer_id);?></span>
								 <?php
									}
								 ?> 
								 <small><i><span class="glyphicon glyphicon-time"></span> <?php echo date("D j M,Y h:i A",strtotime($returns_conservation_obj->timestamp));?></i></small>
								 </h4>
				<p><?php

									if(strlen($returns_conservation_obj->description)>200){
										echo ' <span style="color:#777777;" class="mes"><div class="f-row margin-top more">'.str_split($returns_conservation_obj->description,200)[0].'<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' .substr($returns_conservation_obj->description,201). '</span>&nbsp;&nbsp;<a href="#" class="morelink" onclick="morelinkfun(this)">more</a></span></div></span>';
									}else{
										echo '<span style="color:#777777;" class="mes"><div class="f-row margin-top more">'.$returns_conservation_obj->description.'</div></span>';
									}
								?></p>
				</div>
				<div class="media-right">
				
				 <?php
								if($returns_conservation_obj->image!=""){
							  ?>
							  <a download href="<?php echo base_url('admin/Returns/returns_file_download/'.$returns_conservation_obj->id)?>"><input type="button" class="btn-xs btn-success btn common" value="Download"/> </a>
							  <?php
								}
							  ?>
				</div>
				</div>
                       
						
								<?php
							}
							}else{
								$return_order_id=0;
								echo "<p class='lead text-primary text-center'>No Conversation available</p>";
							}
							?>
                  </div>
                  <div class="panel-footer" style="background-color:#fff;">
						
						<input type="hidden" name="return_order_id" value="<?php echo $return_order_id; ?>">
						<input type="hidden" name="customer_id" value="<?php echo $customer_id; ?>">
	
						<input type="hidden" name="user_type" value="admin">
						<input type="hidden" name="status" value="pending">
						
						<input type="hidden" name="admin_action" value="pending">
						<input type="hidden" name="customer_action" value="pending">
                        <div class="input-group">
                           <textarea placeholder="Reply Message" name="description" id="description" class="form-control"></textarea>
						   
                           <span class="input-group-btn">
                           <button class="btn btn-primary btn-lg" id="btn-chat" type="submit">Send</button>
                           </span>
                        </div>
                    
                  </div>
            </div>
</form>
</div>
</div>

</div>

</body> 
</html>  


<script>

$(document).ready(function(){
	$("#send_form_admin_contact_buyer_form").on('submit',(function(e) {
		e.preventDefault();
		var description=$("#description").val();
		if(description!=""){
			$.ajax({
				url:"<?php echo base_url();?>admin/Returns/contact_buyer",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					if(data){
						
						swal({
							title:"success!", 
							text:"Successfully sent", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#send_form_admin_contact_buyer_form").trigger("reset");	
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
					
				}	        
		   });
		}
		else{
			alert("Please enter description!");
		}
	}));

	$("#send_sms_mail_to_customer").on('submit',(function(e) {
		var sms=$("input[name=sms]").is(':checked');
		var email=$("input[name=email]").is(':checked');

		if(sms==false && email==false){
			alert('Please select any one');
			return false;
		}
		if(sms==true || email==true){		
			swal({
				html: '<h4>Processing...</h4>',
			});	
			swal.showLoading();
		}
		e.preventDefault();
		
			$.ajax({
				url:"<?php echo base_url();?>admin/Returns/send_sms_or_mail_to_customer",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data) 	// A function to be called if request succeeds
				{
		
					if(data){
					
				swal({
					title:"success!", 
					text:"Successfully sent", 
					type: "success",
					allowOutsideClick: false
					
				}).then(function () {
					$("#send_sms_mail_to_customer").trigger("reset");
					//location.reload();

				});
			}else{
				swal("Error", "not sent", "error");
			}
					
				}	        
		   });	
	}));
	
});
</script>

