<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
.remove_range{
	margin-top:36px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script type="text/javascript">

var table;

$(document).ready(function (){
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    var rows_selected = [];
     table = $('#table_logistics_weight').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Catalogue/logistics_weight_processing",
			data: function (d) { d.vendor_id = $('#vendor_id').val();d.logistics_id = $('#logistics_id').val();d.logistics_territory_id = $('#logistics_territory_id').val();d.logistics_delivery_mode_id = $('#logistics_delivery_mode_id').val();d.logistics_parcel_category_id = $('#logistics_parcel_category_id').val();},
            type: "post",
            error: function(){
              $("#table_logistics_weight_processing").css("display","none");
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
   
	$('#reset_form_button').on('click',function(){
		table.draw();
	});
	
	$("#submit_button").click(function() {
		pcat_id=$('#pcat_id').val();
		table.draw();
	});

});	

function drawtable(obj){
	table.draw();
}

function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_delete_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
swal({
			title: 'Are you sure?',
			text: "Delete Logistic weight",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/delete_logistics_weight_selected",
		type:"post",
		data:"selected_list="+selected_list,
		
		success:function(data){
			
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Given "+table+"(s) has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
</script>
<script>

$(document).ready(function(){
	<?php
		if($create_editview=="create"){
			?>
			$("#viewDiv1").css({"display":"none"});
			$("#createDiv2").css({"display":""});
			<?php
		}
		else if($create_editview=="view"){
			?>
			$("#createDiv2").css({"display":"none"});
			$("#viewDiv1").css({"display":""});
			<?php
		}
	?>
});
</script>
<script>

function changeViewPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/logistics_weight_filter';
}
</script>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="wizard-header text-center">
		<h5> Logistics weight 
		</h5>                 		          	
	</div>
<div class="row" id="viewDiv1" style="display:none;">
	<input value="<?php echo $vendor_id; ?>" type="hidden" id="vendor_id">
	<input value="<?php echo $logistics_id; ?>" type="hidden" id="logistics_id">
	<input value="<?php echo $logistics_territory_id; ?>" type="hidden" id="logistics_territory_id">
	<input value="<?php echo $logistics_delivery_mode_id; ?>" type="hidden" id="logistics_delivery_mode_id">
	<input value="<?php echo $logistics_parcel_category_id; ?>" type="hidden" id="logistics_parcel_category_id">

<table id="table_logistics_weight" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<th colspan="8">
			<button id="multiple_delete_button" class="btn btn-danger btn-xs" onclick="multilpe_delete_fun('logistics weight')">Delete</button>
			<button class="btn btn-primary btn-xs" type="button" onclick="changeViewPrevious()">Change View</button>
		</th>
	</tr>
	<tr>
		<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
		<th>Logistics Name</th>
		<th>Territory Name</th>
		<th>Weight (kg)</th>
		<th>Details</th>
		<th>Last Updated</th>
		<th>Action</th>
	</tr>
</thead>

</table>
</div>

<div class="row common" id="createDiv2" style="display:none;">
<div class="col-md-8 col-md-offset-2">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="create_logistics_weight" id="create_logistics_weight" method="post" action="#" onsubmit="return form_validation();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				vendors: {
					required: true,
				},
				logistics: {
					required: true,
				},
				logistics_territory: {
					required: true,
				},
				delivery_mode: {
					required: true,
				},
				parcel_type: {
					required: true,
				},
				weight_in_kg: {
					required: true,
				},
				/*volume_in_kg: {
					required: true,
				},
				logistics_price: {
					required: true,
				}
				lesser_v: {
					required: true,
				},*/
				lesser_m: {
					required: true,
				},
				/*greater_v: {
					required: true,
				},
				greater_m: {
					required: true,
				},
				'v_m[]': {
					required: true,
				},
				'v_from[]': {
					required: true,
				},
				'v_to[]': {
					required: true,
				},*/
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Create Logistics Weight</h3>
		</div>	
		</div>
		<div class="tab-content">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Vendor-</label>
					<select name="vendors" id="vendors" class="form-control" onchange="showAvailableLogistics(this)">
						<option value=""></option>
						<?php foreach ($vendors as $vendors_value) {  ?>
						<option value="<?php echo $vendors_value->vendor_id; ?>"><?php echo $vendors_value->name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Logistics Name-</label>
					<select name="logistics" id="logistics" class="form-control"  onchange="showAvailableLogisticsTerritories(this)">
						<option value=""></option>
						<?php foreach ($logistics as $logistics_value) {  ?>
						<option value="<?php echo $logistics_value->logistics_id; ?>"><?php echo $logistics_value->logistics_name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Logistics Territory-</label>
					<select name="logistics_territory" id="logistics_territory" class="form-control">
						<option value=""></option>
							
					</select>
				</div>
				</div>
				
			</div>	
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Choose Delivery Mode</label>
					<select name="delivery_mode" id="delivery_mode" class="form-control">
						<option value=""></option>
							
					</select>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Parcel Type</label>
					<select name="parcel_type" id="parcel_type" class="form-control" onchange="check_break_point_is_there_or_not_logistics_weightFun(this)">
						<option value=""></option>
							
					</select>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">Enter Weight in grams Eg:0-0.5</label>
					<input id="weight_in_kg" name="weight_in_kg" type="text" class="form-control" />
				</div>
				</div>
			</div>	

			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">For Every Additional Stuff</label>
					<select id="additional_weight_in_kg" name="additional_weight_in_kg" class="form-control" onchange="additional_weight_in_kgFun(this)"/>
						<option value="no">No</option>
						<option value="yes">Yes</option>
					</select>
				</div>
				</div>
			</div>	
			
			<div class="row" id="additional_weight_value_in_kg_div" style="display:none;">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">Enter Additional KG</label>
					<input id="additional_weight_value_in_kg" name="additional_weight_value_in_kg" type="text" class="form-control" />
				</div>
				</div>
			</div>	
			
			<div class="row" id="do_u_want_to_roundup_div" style="display:none;">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">Do you want to round up</label>
					<select id="do_u_want_to_roundup" name="do_u_want_to_roundup" class="form-control"  onchange="do_u_want_to_roundupFun(this)"/>
						<option value=""></option>
						<option value="no">No</option>
						<option value="yes">Yes</option>
					</select>
				</div>
				</div>
			</div>	
			
			<div class="row" id="do_u_want_to_include_breakupweight_div" style="display:none;">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">Do you want to include breakup point</label>
					<select id="do_u_want_to_include_breakupweight" class="form-control"  name="do_u_want_to_include_breakupweight"/>
						<option value=""></option>
						<option value="no">No</option>
						<option value="yes">Yes</option>
					</select>
				</div>
				</div>
			</div>	
			
			
<!----new structure for volumetric weight---->
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
					<h4>Define Various Volumetric Weight with Charge</h4>					
				</div>
				
			</div>		

			<div class="row">
					<div class="col-md-4 col-md-offset-1">  
					<div class="form-group label-floating">
						<label class="control-label">Less than</label>
						<input name="lesser_v" type="text" class="form-control" />
					</div>
					</div>
					<div class="col-md-4">  
					<div class="form-group label-floating">
						<label class="control-label">Charge</label>
						<input name="lesser_m" type="text" class="form-control" />
					</div>
					</div>				
			</div>
			
			<!--<div class="row multi-fields">
				<div class="row form-group add_range">
					<div class="col-md-2 col-md-offset-1">  
					<div class="form-group label-floating">
						<label class="control-label">From</label>
						<input name="v_from[]" type="text" class="form-control" />
					</div>
					</div>
					
					<div class="col-md-2">  
					<div class="form-group label-floating">
						<label class="control-label">To</label>
						<input name="v_to[]" type="text" class="form-control" />
					</div>
					</div>
					<div class="col-md-4">  
					<div class="form-group label-floating">
						<label class="control-label">Charge</label>
						<input name="v_m[]" type="text" class="form-control" />
					</div>
					</div>
					<div class="col-md-3 text-center">  	
						<button type="button" class="btn btn-danger pull-right btn-sm remove_range">X</button>			
					</div>
				</div>
			</div>	

			<div class="row">
				<div class="col-md-12 text-right">
				<button type="button" id="add_one_more" name="add_one_more" class="btn btn-warning add_one_more pull-right btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Add New range</button>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-4 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Greater than</label>
					<input name="greater_v" type="text" class="form-control"/>
				</div>
				</div>
				<div class="col-md-4">  
				<div class="form-group label-floating">
					<label class="control-label">Charge</label>
					<input name="greater_m" type="text" class="form-control"/>
				</div>
				</div>				
			</div>
			--->
<!----new structure for volumetric weight---->


			
			<div class="row">
				<div class="col-md-8 col-md-offset-2"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit">Submit</button>
					<button class="btn btn-warning btn-sm" type="reset">Reset</button>
					<button class="btn btn-primary btn-sm" type="button" onclick="changeViewPrevious()">Change View</button>
					</div>
				</div>
			</div>		
				
		</div>		
	</form>

</div>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>
</body>
</html>
<script type="text/javascript">

$(function(){
	
$('.common').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add_one_more", $(this)).click(function(e) {
        $('.add_range:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('');
		$('.remove_range').show();	
		return false;
    });
	$($wrapper).on("click",".remove_range", function(e){ 
	elements_length=$('.add_range > div', $wrapper).length;
	e.preventDefault(); 
		if(elements_length==1){
			$('.remove_range').hide();
		}else{
			$('.remove_range').show();
			$(this).parent('div').parent('div').remove();	
		}
     
    })
	//remove of pincode range
});

});
function do_u_want_to_roundupFun(obj){
	if(obj.value=="yes"){
		$("#do_u_want_to_include_breakupweight_div").css({"display":"block"});
	}
	else{
		$("#do_u_want_to_include_breakupweight_div").css({"display":"none"});
		$("#do_u_want_to_include_breakupweight").val("");
	}
}
function additional_weight_in_kgFun(obj){
	if(obj.value=="yes"){
		$("#do_u_want_to_roundup_div").css({"display":"block"});
		$("#additional_weight_value_in_kg_div").css({"display":"block"});
	}
	else{
		$("#do_u_want_to_include_breakupweight_div").css({"display":"none"});
		$("#do_u_want_to_include_breakupweight").val("");
		$("#do_u_want_to_roundup_div").css({"display":"none"});
		$("#do_u_want_to_roundup").val("");
		$("#additional_weight_value_in_kg_div").css({"display":"none"});
		$("#additional_weight_value_in_kg").val("");
	}
}
function showAvailableLogisticsDeliveryModes(logistics_id){
	$.ajax({
			url:"<?php echo base_url(); ?>admin/Catalogue/show_available_logistics_delivery_modes",
			type:"post",
			data:"logistics_id="+logistics_id,
			success:function(data){
				
				if(data!=0){

					$("#delivery_mode").html(data);	
				}
				else{
					$("#delivery_mode").html('<option value="">--No Logistics Delivery Modes--</option>');	
					
				}
			}
		});
}
function showAvailableLogisticsParcelCategories(logistics_id){
	$.ajax({
			url:"<?php echo base_url(); ?>admin/Catalogue/show_available_logistics_parcel_categories",
			type:"post",
			data:"logistics_id="+logistics_id,
			success:function(data){
				
				if(data!=0){

					$("#parcel_type").html(data);	
				}
				else{
					$("#parcel_type").html('<option value="">--No Logistics Parcel Categories--</option>');	
					
				}
			}
		});
}
function showAvailableLogisticsTerritories(obj){
	
		logistics_id=obj.value;
		$.ajax({
				//url:"<?php echo base_url(); ?>admin/Catalogue/show_available_logistics_territories_for_weight",
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_logistics_territories_all",
				type:"post",
				data:"logistics_id="+logistics_id,
				success:function(data){
					
					if(data!=0){

						$("#logistics_territory").html(data);	
						showAvailableLogisticsDeliveryModes(logistics_id);
						showAvailableLogisticsParcelCategories(logistics_id);
					}
					else{
						$("#logistics_territory").html('<option value="">--No Logistics Territorys--</option>');	
						
					}
				}
			});
	
}
function showAvailableLogistics(obj){
	
		vendor_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_logistics",
				type:"post",
				data:"vendor_id="+vendor_id,
				success:function(data){
					
					if(data!=0){

						$("#logistics").html(data);		
					}
					else{
						$("#logistics").html('<option value="">--No Logistics--</option>');	
						
					}
				}
			});
	
}
function check_break_point_is_there_or_not_logistics_weightFun(obj){
	var vendor_id = $('select[name="vendors"]').val().trim();
	var logistics_id = $('select[name="logistics"]').val().trim();
	var logistics_territory_id = $('select[name="logistics_territory"]').val().trim();
	var logistics_delivery_mode_id = $('select[name="delivery_mode"]').val().trim();
	var logistics_parcel_category_id=obj.value;
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/check_break_point_is_there_or_not_logistics_weight",
		type:"post",
		data:"vendor_id="+vendor_id+"&logistics_id="+logistics_id+"&logistics_territory_id="+logistics_territory_id+"&logistics_delivery_mode_id="+logistics_delivery_mode_id+"&logistics_parcel_category_id="+logistics_parcel_category_id,
		
		success:function(data){
			if(data=="yes"){
				$("#additional_weight_in_kg").val("yes");
				$("#additional_weight_in_kg option").each(function(){
					if($(this).val()=="no"){
						$(this).attr({"disabled":true});
					}
				});
			}
			else{
				$("#additional_weight_in_kg").val("no");
				$("#additional_weight_in_kg option").each(function(){
					if($(this).val()=="no"){
						$(this).attr({"disabled":false});
					}
				});
			}
		}
	});
	
}
function form_validation()
{
	var vendors = $('select[name="vendors"]').val().trim();
	var logistics = $('select[name="logistics"]').val().trim();
	var logistics_territory = $('select[name="logistics_territory"]').val().trim();
	var weight_in_kg = $('input[name="weight_in_kg"]').val().trim();
	//var volume_in_kg = $('input[name="volume_in_kg"]').val().trim();	
	//var logistics_price = $('input[name="logistics_price"]').val().trim();	

	   var err = 0;
	   if((vendors=='') || (logistics=='') || (logistics_territory=='') || (weight_in_kg=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
	   
		if(err==0)
			{
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#create_logistics_weight');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_logistics_weight/"?>',
				type: 'POST',
				data: $('#create_logistics_weight').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {
 
				if(data)
				{
					swal({
						title:"Success", 
						text:"Logistics Weight is successfully added!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						//location.reload();
						changeViewPrevious();

					});
				}
				else
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
			});
}
}]);			
			}
	
	return false;
}

</script>
