<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Customer Bank Transfer</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 

<link href="<?php echo base_url();?>assets/css/jquery.timepicker.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/js/jquery.timepicker.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js" ></script>
<link href="<?php echo base_url();?>assets/css/datepicker.css" rel="stylesheet">

<style>
.datepicker{z-index:1151 !important;}
</style>
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   var table = $('#customer_bank_tranfer_accounts_incoming_table').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Manage_cust/customer_bank_tranfer_accounts_incoming_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#customer_bank_tranfer_accounts_incoming_table_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("wallet_transfer_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'5%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});	

function accept_fun(wallet_transaction_bank_id){
	$.ajax({
			url:"<?php echo base_url()?>admin/Manage_cust/wallet_transaction_bank_accept",
			type:"POST",
			data:"wallet_transaction_bank_id="+wallet_transaction_bank_id,
			success:function(data){
				swal("Processed");
				location.reload();
			}
	});
}

function reject_fun(wallet_transaction_bank_id){
	$.ajax({
			url:"<?php echo base_url()?>admin/Manage_cust/wallet_transaction_bank_reject",
			type:"POST",
			data:"wallet_transaction_bank_id="+wallet_transaction_bank_id,
			success:function(data){
				swal("Rejected");
				location.reload();
			}
	});
}

function wallet_bank_success_fun(wallet_transaction_bank_id){
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Manage_cust/customer_wallet_bank_transfer_accounts_incoming_walletsuccess",
		type:"POST",
		data:"wallet_transaction_bank_id="+wallet_transaction_bank_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			//alert(data);
			$("#wallet_bank_success_form").html(data);
			$("#wallet_bank_success_modal").modal("show");
		}
	})
	
	
}
function wallet_bank_failure_fun(wallet_transaction_bank_id){
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Manage_cust/customer_wallet_bank_transfer_accounts_incoming_walletfailure",
		type:"POST",
		data:"wallet_transaction_bank_id="+wallet_transaction_bank_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			//alert(data);
			$("#wallet_bank_failure_form").html(data);
			$("#wallet_bank_failure_modal").modal("show");
		}
	})
}

</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header"><h2>Customer Bank Transfer - Incoming Requests <span class="badge" id="wallet_transfer_count"></span></h2></div>
	<table id="customer_bank_tranfer_accounts_incoming_table" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr >
				<th>Customer</th>
				<th>Wallet Transactions</th>
				<th>Bank Details</th>
				<th></th>
			</tr>
		</thead>
	</table>
</div>

<!---------------------->


<!------------------------------------------->
<!-- wallet success modal things starts ---->
<script>
$(document).ready(function(){
	
	$('#transaction_date').datepicker({
		format: "yyyy-mm-dd",
		language: 'pt-BR'
	});  
            

	$('#transaction_time').timepicker();
	$('#transaction_btn').on('click', function (){
		$('#transaction_time').timepicker('setTime', new Date());
	});
	
	
	
	$("#wallet_bank_success_form").on('submit',(function(e) {
		e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>admin/Manage_cust/wallet_transaction_bank_success",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					if(data){
						$('#wallet_bank_success_modal').modal('hide');
						swal({
							title:"success!", 
							text:"Successfully sent", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#wallet_bank_success_form").trigger("reset");
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
					
				}	        
		   });
	}));

});
function divFunction_account_success(){
    $("#wallet_bank_success_form").trigger("reset");	
		location.reload();
}
</script>


<!--- modal success starts----------------->
<div class="modal" id="wallet_bank_success_modal" data-backdrop="static" role="dialog" tabindex="-1" aria-hidden="true">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		 <?php //echo $returns_refund_request_orders_obj->return_reason_comments; ?>
		 <form id="wallet_bank_success_form" method="post" enctype="multipart/form-data">
		 
		 
		</form>
         </div>
      </div>
   </div>
</div>

<!--- modal success ends----------------->

<!-- wallet success modal things ends ---->

<!-- wallet failure modal things starts ---->

<script>

$(document).ready(function(){
	
	$("#wallet_bank_failure_form").on('submit',(function(e) {
		e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>admin/Manage_cust/wallet_transaction_bank_failure",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					if(data){
						$('#wallet_bank_failure_modal').modal('hide');
						swal({
							title:"success!", 
							text:"Successfully sent", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#wallet_bank_failure_form").trigger("reset");
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
					
				}	        
		   });
	}));
	

});
function divFunction_account_failure(){
    $("#wallet_bank_failure_form").trigger("reset");	
		location.reload();
}
</script>


<!--- modal failure starts----------------->
<div class="modal" id="wallet_bank_failure_modal" data-backdrop="static" role="dialog" tabindex="-1" aria-hidden="true">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		 <?php //echo $returns_refund_request_orders_obj->return_reason_comments; ?>
		 <form id="wallet_bank_failure_form" method="post" enctype="multipart/form-data">
		 
		
			</form>
         </div>
      </div>
   </div>
</div>

<!--- modal failure ends----------------->
<!-- wallet failure modal things ends ---->
</div>

</body> 
</html>  