<html lang="en">
<head>
<style>
.nav > li > a {
    position: relative;
    display: block;
    padding: 2px 15px;
}
.top_align
{
	margin-bottom: 10px;
}
a:hover {
    color: #ccc;
    text-decoration: none;
    transition: all 0.25s;
}
</style>

<style>
.btn-file {
	position: relative;
	overflow: hidden;
}
.btn-file input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	min-width: 100%;
	min-height: 100%;
	font-size: 100px;
	text-align: right;
	filter: alpha(opacity=0);
	opacity: 0;
	outline: none;
	background: white;
	cursor: inherit;
	display: block;
}
 </style>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js"></script>
<body>

<div class="columns-container">
    <div class="container" id="columns">

        <div class="row">

            <div class="center_column col-xs-12 col-sm-12" id="center_column">
				<div class="col-md-12">
				<div class="well">
				<div class="panel panel-default">
				  <div class="panel-heading">
					<?php echo $total_records_in_tracking_temp;?> Records - Tracking of Orders (Details page and Checkout Page)
				  </div>
				  <div class="panel-body">
				      <div class="col-sm-12">
				          <table id="trackingaddtocartcheckout_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Sl.No</th>
                            <th>SKU</th>
                            <th>Quantity</th>
                            <th>Customer</th>
                            <th>Page Name</th>
							<th>Timestamp</th>
                        </tr>
                    </thead>
                    <tbody>
  
                    </tbody>
                </table>
				      </div>
				  </div>
				</div>
				</div>
				</div>
               
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   
   var table = $('#trackingaddtocartcheckout_table').DataTable({
       
        "bProcessing": true,
         "serverSide": true,
		 "ajax":{
            url :"<?php echo base_url();?>admin/Trackingaddtocartcheckout/trackingaddtocartcheckout_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#trackingaddtocartcheckout_table_processing").css("display","none");
            },
			//success: function(data){
				//alert(data.recordsTotal)
			//}
          },
          
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'1%',
         'className': 'dt-body-center'
      }],
      'order': [0, 'desc']
   });
}); 
</script> 
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>



</body>
</html>