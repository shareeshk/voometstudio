<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Refund Request Orders - Refund Failure</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<style>
.form-group{
	margin-bottom:10px;
	margin-top:5px;
	padding-bottom:0;
}
</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   var table = $('#returns_refund_request_orders_table_refund_failure').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Returns/returns_refund_request_orders_refund_failure_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#returns_refund_request_orders_table_refund_failure_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("refund_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'30%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});	
$(document).ready(function(){
	var returns_refund_orders_list_checked=0;
	$("input[name='returns_refund_orders_list']").each(function(){
		if($(this).is(":checked")){
			returns_refund_orders_list_checked++;
		}
	})
	/*if(returns_refund_orders_list_checked==0){
		document.getElementById("refund_info_div").style.display="none";
	}*/
})

function open_return_refund_issue_fun(order_item_id){
	location.href="<?php echo base_url()?>admin/Returns/returns_issue_refund/"+order_item_id;
}






function resend_return_refund_fun(order_item_id){
	swal({
			title: 'Are you sure?',
			text: "Resend the return refund",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Resend it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/resend_return_refund",
		type:"POST",
		data:"order_item_id="+order_item_id,
		success:function(data){
			if(data){
				swal({
					title:"Success!", 
					text:"Return Refund resended", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
			
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"Not sent:)", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}

function transfer_to_wallet_fun(order_item_id){
	swal({
			title: 'Are you sure?',
			text: "Transfer to Wallet",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Transfer it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/transfer_to_wallet",
		type:"POST",
		data:"order_item_id="+order_item_id,
		success:function(data){
			if(data){
				swal({
					title:"Success!", 
					text:"Transfered to Wallet", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"Not sent:)", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}

</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="page-header"><h4 class="text-center">Refund Request Orders - Refund Failure <span class="badge" id="refund_count"></span></h4></div>
	<table id="returns_refund_request_orders_table_refund_failure" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr >
				<th class="text-primary small bold">Return Summary</th>
				<th class="text-primary small bold">Return Reason</th>
				<th class="text-primary small bold">Refund Details</th>
				<th class="text-primary small bold">Actions</th>
			</tr>
		</thead>
	</table>
</div>

<!---------------------->
</div>
</body> 
</html>  