<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Admin</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 

<style type="text/css">
.padding_right{
	float:left;
	margin-right:15px;
	
}
.accordion-toggle:hover {
  text-decoration: none;
}
.more-less {
        color: #212121;
    }
	.free_items_style{
		padding: 10px;
		border-bottom: 1px solid #ccc;
		margin-bottom: 13px;
	}
.form-inline{
	margin-bottom:0;
}
.popover{
	min-width:80rem;
}
.fa-question-circle{
	color:#e46c0a;
}
</style>

<script>
$(document).ready(function(){
	
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
});
</script>

<script type="text/javascript">

var table;

$(document).ready(function(){
$('#become_a_reseller_table').on('click', function(e){
	
	
            //if($('.orderid_details_popover').length>1)
          //  $('.orderid_details_popover').popover('hide');
            $(e.target).popover('toggle');
           
            });
});

$(document).ready(function (){	
	
		$('#to_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY'
			});
			
		$('#from_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY', 
				shortTime : true
			}).on('change', function(e, date)
			{
				$('#to_date').bootstrapMaterialDatePicker('setMinDate', date);
			});
			
	
			
   // Array holding selected row IDs
   var rows_selected = [];
    table = $('#become_a_reseller_table').DataTable({
	   
         "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Admin_manage/become_a_reseller_processing", // json datasource
            data: function (d) { d.from_date = $('#from_date').val(); d.to_date = $('#to_date').val();d.vendor_id = $('#vendor_id').val(); },
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#become_a_reseller_table_processing").css("display","none");
            },
            "dataSrc": function ( json ) {
                    document.getElementById("become_a_reseller_count").innerHTML=json.recordsFiltered;
                    return json.data;
            }
          },
		  
        drawCallback: function () {
        	   
                },
	
	
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'5%',
         'className': 'dt-body-center'
      },{
         'targets': 1,
         'width':'15%',
      },{
         'targets': 2,
         'width':'25%',
      },{
         'targets': 3,
         'width':'10%',
      },{
         'targets': 4,
         'width':'10%',
      }
  ],
      'order': [0, 'desc']
   });
 //  $('#from_date, #to_date').keyup( function() {
//        table.draw();
//    } );
	
////////////added for accordition///////////

	$('.collapse').on('shown.bs.collapse', function(){
	$(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
	}).on('hidden.bs.collapse', function(){
	$(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
	});

	////////////added for accordition///////////
	
$( "#from_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
table.draw();
} );

$( "#to_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
table.draw();
} );

$('#reset_form_button').on('click',function(){
	table.draw();
});

$('#submit_form_button').on('click',function(){
	table.draw();
});


});	

</script>
  
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header title"><h4 class="text-center">Become A Reseller <span class="badge" id="become_a_reseller_count"></span></h4></div>
	<table id="become_a_reseller_table" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th colspan="4">
				<div class="row">
					<div class="col-md-3">
					
					</div>
					<div class="col-md-9 text-right">
					<form id="date_range" class="form-inline">
					
					<input type="text" name="from_date" id="from_date" placeholder="From Date" class="form-control">
					<input type="text" name="to_date" id="to_date" placeholder="To Date" class="form-control">
					<button type="button" class="btn btn-sm btn-primary" id="submit_form_button">Submit</button>
					<button type="reset" class="btn btn-sm btn-info" id="reset_form_button">Reset</button>
					</form>
					
					<!--<button type="submit" class="btn btn-primary">Search</button>-->
					
					</div>
					</div>
				</th>
			</tr>
			<tr>
         <th>
            S.No
         </th>
         <th class="text-primary small bold">Request ID</th>
         <th class="text-primary small bold">Details</th>
         <th class="text-primary small bold">City</th>
         <th class="text-primary small bold">Created On</th>
			</tr>
		</thead>
	</table>
</div>

  

 </div> 
</body> 
</html>  