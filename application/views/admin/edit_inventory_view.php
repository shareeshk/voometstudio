<!doctype html>
<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />	
<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<style type="text/css">
 .wizard-navigation {
        -webkit-transform: translateZ(0);
		-webkit-backface-visibility: hidden;
    }
.small_color_box{
	width: 30px;
	height: 30px;
	border: 1px solid #ccc;
	margin-top: 1px;
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}

#edit_wizardPictureSmall_picname{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname2{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname2{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname3{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname3{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname4{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname4{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname5{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname5{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname6{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname6{
	word-wrap: break-word;
}
textarea.form-control {
    height:38px;
}
.CamListDiv {
    height: 450px;
    float: left;
    overflow: scroll;
    overflow-x:hidden;

}
.well{
	width:100%;

}

</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 

<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<?php 
	if(isset($id)){
		
		$inventory_id=$id;
		$cat_id=$cat_id;
		$subcat_id=$subcat_id;
		$brand_id=$brand_id;
		$product_id=$product_id;
	}else{
		header("Location:".base_url()."admin/Catalogue/inventory/");	
	}

?>
<script type="text/javascript">
$(document).ready(function (){
$('#back').on('click',function(){	
		var form = document.getElementById('back_to_inventory');
		form.action='<?php echo base_url(); ?>admin/Catalogue/inventory';
		form.submit();
	});
});		
</script>
<script type="text/javascript">
function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

function edit_filter_details_form_valid()
{
	var err = 0;	
	var filter=document.getElementsByName("edit_filter[]");

	if(err==1)
	{
		//$('#edit_validation_error').show();
		return false;
		
	}else{
				//$('#edit_validation_error').hide();
				var data = $("#highlight").Editor("getText").trim();
				$("#highlight").val(data);
				
				var data1 = $("#highlight_faq").Editor("getText").trim();
				$("#highlight_faq").val(data1);
				
				var data2 = $("#highlight_whatsinpack").Editor("getText").trim();
				$("#highlight_whatsinpack").val(data2);
				
				var data3 = $("#highlight_aboutthebrand").Editor("getText").trim();
				$("#highlight_aboutthebrand").val(data3);
		
				var form_status = $('<div class="form_status"></div>');	
				var form_edit = document.forms.namedItem("edit_inventory");
				var ajaxData = new FormData(form_edit);	
		
				var form = $('#edit_inventory');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_inventory/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false,  
				
			}).done(function(data)
			  {
				  
				if(data==true)
				{
					swal({
						title:"Success", 
						text:"Inventory is successfully Updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();
					});
				}
				else
				{
					swal({
						title:"Error", 
						text:"Inventory not yet Updated!", 
						type: "error",
						allowOutsideClick: false
					}).then(function () {
						location.reload();
					});	
				}
		return true;
	});
	}
}]);
		return true;
	}
}
var edit_val;


</script>
<script type="text/javascript">
		function spec_fun_edit(obj,specification_id){
			//document.getElementsByName("edit_specification_textarea_value["+specification_id+"]")[0].setAttribute("style", "border: 1px solid #ccc;");
			if(obj.value=="Others"){
				document.getElementsByClassName("edit_specification_textarea_value["+specification_id+"]")[0].style.display="block";
			}
			else{
				document.getElementsByClassName("edit_specification_textarea_value["+specification_id+"]")[0].style.display="none";
			}
		}

	function edit_display_color(color_value){
		colors_arr=color_value.split(":");
		color_code=colors_arr[1];
		color_name=colors_arr[0];
		if(color_value!=""){
		$('#edit_small_color_box').css({"background-color": color_code});
		}else{
			$('#edit_small_color_box').css({"background-color": ""});
		}
		$('#edit_small_color_box').attr('title',color_name);
	}

	function edit_filter_display_color(color_value){
		colors_arr=color_value.split(":");
		color_code=colors_arr[1];
		color_name=colors_arr[0];
		$('#edit_filter_small_color_box').css({"background-color": color_code});
		$('#edit_filter_small_color_box').attr('title',color_name);
	}
</script>	

<div class="container">
<div class="row">			
<div class="col-md-12">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
<form action="" name="edit_inventory" id="edit_inventory" method="post" enctype="multipart/form-data">
	<?php
		
		foreach($get_all_specification_group as $get_all_specification_group_value)
		{
			$sp=$controller->get_all_specification($get_all_specification_group_value->specification_group_id);
			?>
			<script type="text/javascript">
                                /*
                                 * 
                                 * @type type
                                                edit_base_price: {
							required: true,
		     
						},
                                 */
				var $validator = $('.wizard-card form').validate({
					rules: {
						
				
						edit_attribute_1_value: {
							required: true,
						},
			
						edit_vendors: {
							required: true,
						},
						edit_selling_price: {
							required: true,
		     
						},
						edit_cost_price: {
							required: true,
		     
						},
//						edit_purchased_price: {
//							required: true,
//						},
						edit_tax: {
							required: true,
		      
						},
						edit_moq: {
							required: true,
		     
						},
						edit_max_oq: {
							required: true,
		      
						},
						edit_low_stock: {
							required: true,
		     
						},
						edit_cutoff_stock: {
							required: true,
		      
						},
                                                edit_sku_id: {
							required: true,
							minlength: 3
						},
						/*'edit_logistics_parcel_category_id[]': {
							required: true,
		      
						},*/
						edit_flat_shipping_charge: {
							required: true,
		      
						},
						
						edit_inventory_weight_in_kg: {
							required: true,
		      
						},
						edit_inventory_volume_in_kg: {
							required: true,
		       
						},
						edit_inventory_length: {
							required: true,
					   
						},
						edit_inventory_breadth: {
							required: true,
					   
						},
						edit_inventory_height: {
							required: true,
					   
						},
						edit_stock: {
							required: true,
		      
						},
						edit_product_status: {
							required: true,
		      
						},
						edit_product_status_view: {
							required: true,
		      
						},

			<?php
			
			foreach($filterbox as $filterbox_value)
			{
				//$filter=$controller->get_all_filter($filterbox_value->filterbox_id);
				//if(strtolower($filterbox_value->filterbox_name)!="color"){
					
				//foreach($filter as $filter_value)
				//{
					echo "'".'edit_filter['.$filterbox_value->filterbox_id.']'."'".':{';
					echo 'required: true },';
				
				//}
				//}
			}
			?>
			
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
			</script>
		<?php } ?>
		<div class="wizard-header">
			
		<h4 class="text-left col-md-12">
			<small>
		<?php 
		
		//print_r( $inventory_data_arr);

		echo $cat_structure["pcat_name"].' > '.$cat_structure["cat_name"].' > '.$cat_structure["subcat_name"].' > '.$cat_structure["brand_name"];

		$inventory_type_txt='';
		
		if($inventory_type=='1'){
			$inventory_type_txt='Only as Main Inventory';
		}elseif($inventory_type=='2'){
			$inventory_type_txt='Only as Addon Inventory';

		}elseif($inventory_type=='3'){
			$inventory_type_txt='Both as Main & as Addon';
		}
		
		?>
		</small>
		</h4>

		<h3 class="wizard-title"> Edit SKU ID : <?php echo $sku_id; ?>
	
		<?php if($inventory_type!=''){
			echo "  <span><small class='text-info'>".$inventory_type_txt.'</small></span>';
		}
		?>
		</h3></div>  

		
		<div class="wizard-navigation">
			<ul>
				<li><a href="#edit_step-11" data-toggle="tab">General</a></li>
				<li><a href="#edit_step-71" data-toggle="tab">Highlights</a></li>
				<li><a href="#edit_step-21" data-toggle="tab">Logistics</a></li>
				<li><a href="#edit_step-31" data-toggle="tab">Attributes</a></li>
				<li><a href="#edit_step-41" data-toggle="tab">Specifications</a></li>
				<li><a href="#edit_step-51" data-toggle="tab">Filters</a></li>
				<li><a href="#edit_step-61" data-toggle="tab">Images</a></li>
			</ul>
		</div>
		
			<input name="edit_product_id" type="hidden" id="edit_product_id" value="<?php echo $product_id;?>">
			<input name="edit_inventory_id" id="edit_inventory_id" type="hidden"  value="<?php echo $inventory_id;?>"/>
			<input name="previous_largeimage1" id="previous_largeimage1" type="hidden" value="<?php echo $inventory_data_arr["largeimage"];?>"/>
			<input name="previous_image" id="previous_image" type="hidden" value="<?php echo $inventory_data_arr["common_image"];?>"/>
			<input name="previous_image1" id="previous_image1" type="hidden" value="<?php echo $inventory_data_arr["image"];?>"/>
			<input name="previous_thumbnail1" id="previous_thumbnail1" type="hidden" value="<?php echo $inventory_data_arr["thumbnail"];?>"/>
			
			<input name="previous_largeimage2" id="previous_largeimage2" type="hidden" value="<?php echo $inventory_data_arr["largeimage2"];?>"/>
			<input name="previous_image2" id="previous_image2" type="hidden" value="<?php echo $inventory_data_arr["image2"];?>"/>
			<input name="previous_thumbnail2" id="previous_thumbnail2" type="hidden" value="<?php echo $inventory_data_arr["thumbnail2"];?>"/>
			
			<input name="previous_largeimage3" id="previous_largeimage3" type="hidden" value="<?php echo $inventory_data_arr["largeimage3"];?>"/>
			<input name="previous_image3" id="previous_image3" type="hidden" value="<?php echo $inventory_data_arr["image3"];?>"/>
			<input name="previous_thumbnail3" id="previous_thumbnail3" type="hidden" value="<?php echo $inventory_data_arr["thumbnail3"];?>"/>
			
			<input name="previous_largeimage4" id="previous_largeimage4" type="hidden" value="<?php echo $inventory_data_arr["largeimage4"];?>"/>
			<input name="previous_image4" id="previous_image4" type="hidden" value="<?php echo $inventory_data_arr["image4"];?>"/>
			<input name="previous_thumbnail4" id="previous_thumbnail4" type="hidden" value="<?php echo $inventory_data_arr["thumbnail4"];?>"/>
			
			<input name="previous_largeimage5" id="previous_largeimage5" type="hidden" value="<?php echo $inventory_data_arr["largeimage5"];?>"/>
			<input name="previous_image5" id="previous_image5" type="hidden" value="<?php echo $inventory_data_arr["image5"];?>"/>
			<input name="previous_thumbnail5" id="previous_thumbnail5" type="hidden" value="<?php echo $inventory_data_arr["thumbnail5"];?>"/>
			
			
			<input name="previous_largeimage6" id="previous_largeimage6" type="hidden" value="<?php echo $inventory_data_arr["largeimage6"];?>"/>
			<input name="previous_image6" id="previous_image6" type="hidden" value="<?php echo $inventory_data_arr["image6"];?>"/>
			<input name="previous_thumbnail6" id="previous_thumbnail6" type="hidden" value="<?php echo $inventory_data_arr["thumbnail6"];?>"/>
			
			
		
			<input name="previous_largeimage1_unlink" id="previous_largeimage1_unlink" type="hidden" value="<?php echo $inventory_data_arr["largeimage"];?>"/>
			<input name="previous_image1_unlink" id="previous_image1_unlink" type="hidden" value="<?php echo $inventory_data_arr["image"];?>"/>
			<input name="previous_thumbnail1_unlink" id="previous_thumbnail1_unlink" type="hidden" value="<?php echo $inventory_data_arr["thumbnail"];?>"/>
			
			<input name="previous_largeimage2_unlink" id="previous_largeimage2_unlink" type="hidden" value="<?php echo $inventory_data_arr["largeimage2"];?>"/>
			<input name="previous_image2_unlink" id="previous_image2_unlink" type="hidden" value="<?php echo $inventory_data_arr["image2"];?>"/>
			<input name="previous_thumbnail2_unlink" id="previous_thumbnail2_unlink" type="hidden" value="<?php echo $inventory_data_arr["thumbnail2"];?>"/>
			
			<input name="previous_largeimage3_unlink" id="previous_largeimage3_unlink" type="hidden" value="<?php echo $inventory_data_arr["largeimage3"];?>"/>
			<input name="previous_image3_unlink" id="previous_image3_unlink" type="hidden" value="<?php echo $inventory_data_arr["image3"];?>"/>
			<input name="previous_thumbnail3_unlink" id="previous_thumbnail3_unlink" type="hidden" value="<?php echo $inventory_data_arr["thumbnail3"];?>"/>
			
			<input name="previous_largeimage4_unlink" id="previous_largeimage4_unlink" type="hidden" value="<?php echo $inventory_data_arr["largeimage4"];?>"/>
			<input name="previous_image4_unlink" id="previous_image4_unlink" type="hidden" value="<?php echo $inventory_data_arr["image4"];?>"/>
			<input name="previous_thumbnail4_unlink" id="previous_thumbnail4_unlink" type="hidden" value="<?php echo $inventory_data_arr["thumbnail4"];?>"/>
			
			<input name="previous_largeimage5_unlink" id="previous_largeimage5_unlink" type="hidden" value="<?php echo $inventory_data_arr["largeimage5"];?>"/>
			<input name="previous_image5_unlink" id="previous_image5_unlink" type="hidden" value="<?php echo $inventory_data_arr["image5"];?>"/>
			<input name="previous_thumbnail5_unlink" id="previous_thumbnail5_unlink" type="hidden" value="<?php echo $inventory_data_arr["thumbnail5"];?>"/>
			
			<input name="previous_largeimage6_unlink" id="previous_largeimage6_unlink" type="hidden" value="<?php echo $inventory_data_arr["largeimage6"];?>"/>
			<input name="previous_image6_unlink" id="previous_image6_unlink" type="hidden" value="<?php echo $inventory_data_arr["image6"];?>"/>
			<input name="previous_thumbnail6_unlink" id="previous_thumbnail6_unlink" type="hidden" value="<?php echo $inventory_data_arr["thumbnail6"];?>"/>
			
			
			
	<div class="tab-content">
	<div class="tab-pane" id="edit_step-11">
		<div class="row">
			<div class="col-md-12">
			    <h4 class="info-text"> Product and Stock Information</h4>
			</div>
		</div>
              
		

		<div class="row">
				<div class="col-md-5 col-md-offset-1"> 
					<div class="form-group label-floating">
						<label class="control-label">Enter Display Name on Button 1</label>
						<input name="sku_display_name_on_button_1" type="text" class="form-control"  value="<?php echo $inventory_data_arr["sku_display_name_on_button_1"];?>"/>
					</div>
				</div>
				<div class="col-md-5"> 	
				
					<div class="form-group label-floating">
						<label class="control-label">Enter Display Name on Button 2</label>
						<input name="sku_display_name_on_button_2" type="text" class="form-control"   value="<?php echo $inventory_data_arr["sku_display_name_on_button_2"];?>"/>
					</div>
				</div>
			</div>
			


		<div class="row">
			
			<div class="col-md-10 col-md-offset-1">
				<div class="form-group">
					<label class="control-label">
					SKU ID</label>
					<input name="edit_sku_id" id="edit_sku_id" type="text" class="form-control" value="<?php echo $inventory_data_arr["sku_id"];?>"/>
				</div>
			</div>
		</div>
                <div class="row">
			<div class="col-md-10 col-md-offset-1"> 
                            <div class="form-group label-floating">
                                    <label class="control-label">SKU Name </label>
                                    <input name="sku_name" id="sku_name" type="text" class="form-control" value="<?php echo $inventory_data_arr["sku_name"];?>" onkeypress="return blockSpecialChar(event)"/>
                            </div>
			</div>
		</div>
               
		
		
		
		
		
		

		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="form-group">
					<label class="control-label">-Product Status-
					</label>
					<select name="edit_product_status_view" id="edit_product_status_view" class="form-control search_select">
						<option value="" selected></option>
						<option value="1" <?php if($inventory_data_arr["product_status_view"]=="1"){echo "selected";}?>>Active</option>
						<option value="0" <?php if($inventory_data_arr["product_status_view"]=="0"){echo "selected";}?>>Inactive</option>
			
					</select>
				</div>
			</div>
			
		</div>
		<div class="row" style="display:none;">
		
                    
                        <div class="col-md-5 col-md-offset-1"> 
				<div class="form-group">
					<label class="control-label">Company Cost Price (<?php echo curr_sym;?>)
					</label>
					<input name="edit_purchased_price" id="edit_purchased_price" type="text" class="form-control"   value="<?php echo $inventory_data_arr["purchased_price"];?>" onKeyPress="return isNumber(event)"/>
				</div>
				<script>
				$(document).ready(function(){
					assign_filter_values('<?php echo  $inventory_data_arr["purchased_price"];?>','price');
				});
				</script>
			</div>
			
			
			<div class="col-md-5"> 
				<div class="form-group">
					<label class="control-label">Company Base Price (<?php echo curr_sym;?>)
					</label>
					<input name="edit_base_price" id="edit_base_price" type="text" class="form-control"   value="<?php echo $inventory_data_arr["base_price"];?>" onKeyPress="return isNumber(event)"/>
				</div>
				<script>
				//$(document).ready(function(){
					//assign_filter_values('<?php echo  $inventory_data_arr["selling_price"];?>','price');
				//});
				</script>
			</div>
			
			
		</div>
		
		
		<div class="row">
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group">
					<label class="control-label" id="edit_max_selling_price_div">MRP (<?php echo curr_sym;?>) {MRP - Base Price + Tax Price}
					</label>
                                    <input type="hidden" id="db_edit_max_selling_price" value="<?php echo $inventory_data_arr["max_selling_price"];?>"> 
					<input name="edit_max_selling_price" id="edit_max_selling_price" type="text" class="form-control"   value="<?php echo $inventory_data_arr["max_selling_price"];?>" onKeyPress="return isNumber(event)" onblur="calculateSelling_priceDiscountFun();calculateTax_priceFun();"/>
				</div>
				<script>
				$(document).ready(function(){
					assign_filter_values('<?php echo  $inventory_data_arr["max_selling_price"];?>','price');
				});
				</script>
			</div>
			<div class="col-md-5"> 
				<div class="form-group">
					<label class="control-label" id="edit_selling_price_div">Web Selling Price (<?php echo curr_sym;?>) {WSP - with any discount applied}
					</label>
                                        <input type="hidden" id="db_edit_selling_price" value="<?php echo $inventory_data_arr["selling_price"];?>">
					<input name="edit_selling_price" id="edit_selling_price" type="text" class="form-control"   value="<?php echo $inventory_data_arr["selling_price"];?>" onKeyPress="return isNumber(event)" onkeyup="calculateSelling_priceDiscountFun();calculateTax_priceFun();"/>
				</div>
			</div>
            		</div>

            <!--added newly---->
            <div class="row">
			
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group">
					<label class="control-label" id="edit_cost_price_div">Cost Price (<?php echo curr_sym;?>) {the cost of the product to the company}
					</label>
                                        
					<input name="edit_cost_price" id="edit_cost_price" type="text" class="form-control"   value="<?php echo $inventory_data_arr["cost_price"];?>" onKeyPress="return isNumber(event)"/>
				</div>
				<script>
				$(document).ready(function(){
					assign_filter_values('<?php echo  $inventory_data_arr["selling_price"];?>','price');
				});
				</script>
			</div>
                
                        <div class="col-md-5"> 
				<div class="form-group">
					<label class="control-label">
					Discount Percent {% of WSP from MRP}</label>
                                        <input type="hidden" id="db_edit_selling_discount" value="<?php echo $inventory_data_arr["selling_discount"];?>">
					<input name="edit_selling_discount" id="edit_selling_discount" type="text" class="form-control"   value="<?php echo $inventory_data_arr["selling_discount"];?>" readOnly  />
				</div>
			</div>
		</div>

            <div class="row">
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group" id="tax_percent_price_div">
					<label class="control-label">Tax % Value</label>
					<input name="tax_percent_price" id="tax_percent_price" type="text" class="form-control" value="<?php echo $inventory_data_arr["tax_percent_price"];?>" readOnly />
				</div>
			</div>
			
			<div class="col-md-5"> 
				<div class="form-group" id="taxable_price_div">
					<label class="control-label">Taxable Value </label>
					<input name="taxable_price" id="taxable_price" type="text" class="form-control" value="<?php echo $inventory_data_arr["taxable_price"];?>" readonly/>
				</div>
			</div>
		</div>
                
                <div class="row">
			<div class="col-md-2 col-md-offset-1"> 
				<div class="form-group">
					<label class="control-label">CGST (%)</label>
					<input name="CGST" id="CGST" type="text" onKeyPress="return isNumber(event)" class="form-control" value="<?php echo $inventory_data_arr["CGST"];?>"  onkeyup="calculateTaxPercentFun();"/>
				</div>
			</div>
			
			<div class="col-md-3"> 
				<div class="form-group" id="IGST_div">
					<label class="control-label">IGST (%)</label>
					<input name="IGST" id="IGST" onKeyPress="return isNumber(event)" type="text" class="form-control" value="<?php echo $inventory_data_arr["IGST"];?>" onkeyup="calculateTaxPercentFun();"/>
				</div>
			</div>
                        <div class="col-md-3"> 
				<div class="form-group" id="SGST_div">
					<label class="control-label">SGST (%)</label>
					<input name="SGST" id="SGST" onKeyPress="return isNumber(event)" type="text" class="form-control" value="<?php echo $inventory_data_arr["SGST"];?>" onkeyup="calculateTaxPercentFun();"/>
				</div>
			</div>
			
			
			<div class="col-md-2"> 
				<div class="form-group">
					<label class="control-label">
					Tax (%)</label>
					<input name="edit_tax" id="edit_tax" type="text" class="form-control"   value="<?php echo $inventory_data_arr["tax_percent"];?>" onKeyPress="return isNumber(event)" readonly/>
				</div>
			</div>
			
			
			
			
		</div>
                
                
            <!--added newly---->
		
		<?php
		/*
		?>
		<div class="row">
			
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group">
					<label class="control-label">Quantity
					</label>
					<input name="edit_mrp_quantity" id="edit_mrp_quantity" type="text" class="form-control"   value="<?php echo $inventory_data_arr["mrp_quantity"];?>"/>
				</div>
				<script>
				$(document).ready(function(){
					assign_filter_values('<?php echo  $inventory_data_arr["selling_price"];?>','price');
				});
				</script>
			</div>
		</div>
		<?php
		*/
		?>

		<div class="row">
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">Price validity</label>
					<input name="price_validity" id="price_validity" value="<?php echo ($inventory_data_arr["price_validity"]!='0000-00-00') ? $inventory_data_arr["price_validity"] : '';?>" type="text" class="form-control"/>
				</div>
			</div>
			<div class="col-md-5"> 
				<div class="form-group label-floating">
					<label class="control-label">Material Type of SKU (Wood, Steel, etc., )</label>
					<input name="material" id="material" value="<?php echo $inventory_data_arr["material"];?>" type="text" class="form-control"/>
				</div>
			</div>
			
		</div>
		
		
		<div class="row">
			<div class="col-md-5 col-md-offset-1"> 
			
				<div class="form-group">
					<label class="control-label">Enter Minimum Order Quantity
					</label>
					<input name="edit_moq" id="edit_moq" type="number" class="form-control" min="<?php echo $promo_qty_min; ?>"   value="<?php echo $inventory_data_arr["moq"];?>" <?php echo ($active_qty_promo_exist>0) ? '' : '' ; ?> required/>
				</div>
				<?php if($active_qty_promo_exist>0){ ?>
					<div class="text-danger"><small><b><i>Note: There is a promotion Running for MOQ <?php echo $promo_qty_min; ?></i></b></small></div>
				<?php } ?>
			</div>		  
			<div class="col-md-5"> 	
				<div class="form-group">
				<label class="control-label">Enter Maximum Order Quantity
				</label>
				<input name="edit_max_oq" id="edit_max_oq" type="number" class="form-control" min="<?php echo ($active_qty_promo_exist>0) ? ($promo_qty_min+1) : ($inventory_data_arr["moq"]+1) ; ?>"   value="<?php echo $inventory_data_arr["max_oq"];?>" required/>
				</div>
			</div>
		</div>	
		
			<div class="row">
			
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">-Select A Relavent Stock Status-</label>
					<select name="edit_product_status" id="product_status" class="form-control search_select">
						<option value=""></option>
						<option value="In Stock" <?php if($inventory_data_arr["product_status"]=="In Stock"){echo "selected";}?>>Ready In Stock</option>
						
						<option value="Made To Order" <?php if($inventory_data_arr["product_status"]=="Made To Order"){echo "selected";}?>>Made To Order</option>

						<option value="Out Of Stock" <?php if($inventory_data_arr["product_status"]=="Out Of Stock"){echo "selected";}?>>Out Of Stock</option>
						<option value="Discontinued" <?php if($inventory_data_arr["product_status"]=="Discontinued"){echo "selected";}?>>Discontinued</option>
					</select>
				</div>
			</div>
				<div class="col-md-5"> 
					<div class="form-group">	
						<label class="control-label">Stock count
						</label>
						<input class="form-control" name="edit_stock" id="edit_stock" type="number"  value="<?php echo $inventory_data_arr["stock"];?>"   onkeyup="assign_filter_values_stock()"/>
						
						<script>
						$(document).ready(function(){
							assign_filter_values_stock();
						});
						</script>
					</div>
				</div>	
				
			</div>	
			<div class="row">
				<div class="col-md-5 col-md-offset-1"> 
					<div class="form-group">
						<label class="control-label">Low Stock Count (Warning Qty)
						</label>    
						<input class="form-control" name="edit_low_stock" id="edit_low_stock" type="number"  value="<?php echo $inventory_data_arr["low_stock"];?>"  onkeyup="stock_count_validation();"/>
					</div>
				</div>
				<div class="col-md-5"> 
					<div class="form-group">
						<label class="control-label">Cutoff Stock Count
						</label>     
						<input class="form-control" name="edit_cutoff_stock" id="edit_cutoff_stock" type="number"   value="<?php echo $inventory_data_arr["cutoff_stock"];?>"   onkeyup="assign_filter_values_stock();stock_count_validation();"/>
					</div>
				</div>
			</div>
			<div class="row">
			<div class="col-md-5 col-md-offset-1">
				<input type="hidden" name="edit_add_to_cart" value="no"/>
				<input name="edit_add_to_cart" id="edit_add_to_cart"  type="checkbox" value="yes" <?php if($inventory_data_arr["add_to_cart"]=="yes"){echo "checked";}?> />
				<label class="control-label">Add to Cart</label>
				<p id="buttonDisplayErrorMessage" class="text-danger"></p>
			</div>
			<div class="col-md-5"> 
				<input type="hidden" name="edit_request_for_quotation" value="no"/>
				<input name="edit_request_for_quotation" id="edit_request_for_quotation"  type="checkbox" value="yes" <?php if($inventory_data_arr["request_for_quotation"]=="yes"){echo "checked";}?> />
				<label class="control-label">Request For Quotation</label>
			</div>
		</div>
			
			<script>
			$(document).ready(function(){
				assign_filter_values_stock();
			});
		function stock_count_validation(){
			low_stock=parseInt($('#edit_low_stock').val());
			cutoff_stock=parseInt($('#edit_cutoff_stock').val());
			if(low_stock!='' && cutoff_stock!=''){
				if(low_stock<cutoff_stock){
					alert('Low stock count should be greater than cutoff stock');
					$('#edit_cutoff_stock').val('<?php echo $inventory_data_arr["cutoff_stock"];?>');
				}
			}	
		}
		</script>
			
		
	</div>
	<div class="tab-pane" id="edit_step-71">
	
	
	<!---positioning ------->
	
	<div class="panel-group" id="positioning" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="positionheadingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#positioning_div" aria-expanded="true" aria-controls="positioning_div">
                        
                        Postioning
                    </a>
                </h4>
            </div>
            <div id="positioning_div" class="panel-collapse collapse" role="tabpanel" aria-labelledby="positionheadingOne">
                <div class="panel-body">
                     
				<div class="row">
					<div class="col-md-8 col-md-offset-2"> 	
						<div class='item'>
							<div class="form-group label-floating">
								<label class="control-label">Positioning title</label>   
								<input class="form-control"  type="text" name="positioning_uhave_title" value="<?php echo $inventory_data_arr["positioning_uhave_title"];?>">
							</div>

						</div>
						
					</div>
					
				</div>

				<div class="row">
					<div class="col-md-8 col-md-offset-2"> 	
						<div class='item'>
							<div class="form-group label-floating">
								<label class="control-label">Positioning title 2</label>   
								<input class="form-control"  type="text" name="positioning_others_have_title" value="<?php echo $inventory_data_arr["positioning_uhave_title"];?>">
							</div>

						</div>
						
					</div>
					
				</div>

				<?php 
				
				if(!empty($inventory_positioning)){
					foreach ($inventory_positioning as $key=>$inventory_positioning_obj){
						$len=($key+1);
						?>

						<div class="row">
							<div class="col-md-8 col-md-offset-2"> 	
								<div class='item'>
									<div class="form-group label-floating">
										<label class="control-label">Content</label>   
										<input class="form-control"  type="text" name="position_text[<?php echo $len; ?>]" value="<?php echo $inventory_positioning_obj->position_text; ?>">
									</div>
		
								</div>								
							</div>
							
						</div>


						<div class="row">
							<div class="col-md-8 col-md-offset-2"> 
									<div class="form-group">
									<div class="col-md-5 col-md-offset-1">Do you have ?</div>
									<div class="col-md-3">
										<label class="radio-inline"><input name="position_uhave[<?php echo $len; ?>]" <?php echo ($inventory_positioning_obj->position_uhave=='yes') ? 'checked': ''; ?> value="yes" type="radio" >Yes</label>
									</div>
									<div class="col-md-3">
										<label class="radio-inline"><input name="position_uhave[<?php echo $len; ?>]" <?php echo ($inventory_positioning_obj->position_uhave=='no') ? 'checked': ''; ?> value="no" type="radio">No</label>
									</div>
							
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-8 col-md-offset-2"> 
								<div class="form-group">
									<div class="col-md-5 col-md-offset-1">Do others have ?</div>
									<div class="col-md-3">
										<label class="radio-inline"><input name="position_others_have[<?php echo $len; ?>]" <?php echo ($inventory_positioning_obj->position_others_have=='yes') ? 'checked': ''; ?> value="yes" type="radio" >Yes</label>
									</div>
									<div class="col-md-3">
										<label class="radio-inline"><input name="position_others_have[<?php echo $len; ?>]" <?php echo ($inventory_positioning_obj->position_others_have=='no') ? 'checked': ''; ?> value="no" type="radio">No</label>
									</div>
							
								</div>
							</div>

						</div>

						<input type="hidden" name="position_id[<?php echo $len; ?>]" value="<?php echo $inventory_positioning_obj->position_id; ?>">

						<?php if($len==1){ ?>

						<div class="row">
							<div class="col-md-8 col-md-offset-2"> 
								<div class="form-group">
									<button type="button" id="add" class="btn btn-sm btn-success pull-right" onclick="add_items()">Add +</button>
								</div>
							</div>
						</div>

						
						
						<?php }else{
							?>

								<div class="col-md-8 col-md-offset-1"><button class="btn btn-sm btn-danger pull-right" class="remove" >Remove</a>
								</div>

							<?php 
						} ?>
						<?php
					}
				}else{
					?>

<div class="row">
							<div class="col-md-8 col-md-offset-2"> 	
								<div class='item'>
									<div class="form-group label-floating">
										<label class="control-label">Content</label>   
										<input class="form-control"  type="text" name="position_text[1]">
									</div>

		
								</div>
								
							</div>
							
						</div>

						<!---tt-->

	<div class="row">
		<div class="col-md-8 col-md-offset-2"> 
				<div class="form-group">
				<div class="col-md-5 col-md-offset-1">Do you have ?</div>
				<div class="col-md-3">
					<label class="radio-inline"><input name="position_uhave[1]" value="yes" type="radio" >Yes</label>
				</div>
				<div class="col-md-3">
					<label class="radio-inline"><input name="position_uhave[1]" value="no" type="radio">No</label>
				</div>
		
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 col-md-offset-2"> 
			<div class="form-group">
				<div class="col-md-5 col-md-offset-1">Do others have ?</div>
				<div class="col-md-3">
					<label class="radio-inline"><input name="position_others_have[1]" value="yes" type="radio" >Yes</label>
				</div>
				<div class="col-md-3">
					<label class="radio-inline"><input name="position_others_have[1]" value="no" type="radio">No</label>
				</div>
		
			</div>
		</div>

	</div>
	<div class="row">
		<div class="col-md-8 col-md-offset-2"> 
			<div class="form-group">
	
	
				<button type="button" id="add" class="btn btn-sm btn-success pull-right" onclick="add_items()">Add +</button>

			</div>
		</div>
	</div>


					<?php
				}
				
				?>




						<div class="row">
							<div class="col-md-12 col-md-offset-1"> 	
								<div id="items"></div>
							</div>
						</div>


                </div>
            </div>

        </div>
        </div>
		
	
	<!---positioning ------->
	
	<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        
                        Product Details
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                      
					  <div class="row">
							<div class="col-md-12"> 	
									<textarea class="form-control" name="highlight" id="highlight"></textarea>
							</div>
						</div>
						
						
                </div>
            </div>
        </div>
        </div>
		
		
		
		<div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                        
                        FAQ
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                      
					  
					<div class="row">
						<div class="col-md-12"> 	
								<textarea class="form-control" name="highlight_faq" id="highlight_faq"></textarea>
						</div>
					</div>
						
						
                </div>
            </div>
        </div>
        </div>
	
	
	 		<div class="panel-group" id="accordion4" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                        
                        What's in the pack?
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                      
					  
					<div class="row">
						<div class="col-md-12"> 	
								<textarea class="form-control" name="highlight_whatsinpack" id="highlight_whatsinpack"></textarea>
						</div>
					</div>
						
						
                </div>
            </div>
        </div>
        </div>







<div class="panel-group" id="accordion5" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingDownloads">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDownloads" aria-expanded="true" aria-controls="collapseDownloads">
                        Downloads
                    </a>
                </h4>
            </div>
            <div id="collapseDownloads" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingDownloads">
                <div class="panel-body">
                      
					  <input type="hidden" name="delete_file_1_upload" id="delete_file_1_upload" value="">
					  <input type="hidden" name="delete_file_2_upload" id="delete_file_2_upload" value="">
					  <input type="hidden" name="delete_file_3_upload" id="delete_file_3_upload" value="">
					  <input type="hidden" name="delete_file_4_upload" id="delete_file_4_upload" value="">
					  <input type="hidden" name="delete_file_5_upload" id="delete_file_5_upload" value="">
					  <input type="hidden" name="delete_file_6_upload" id="delete_file_6_upload" value="">
					  
					<div class="row">
						<div class="col-md-6">
							<div class="col-md-12"> 	
								<input type="text" name="file_1_name" id="file_1_name" class="form-control" placeholder="Name of file 1" value="<?php echo $inventory_data_arr["file_1_name"];?>">
							</div>
							<div class="col-md-12"> 	
								<input type="file" name="file_1_upload" id="file_1_upload" onchange="deleteDownloadsLinkFun('delete_file_1_upload')"> 
								<span id="delete_file_1_upload_span">
									<?php
										if($inventory_data_arr["file_1_upload"]!=""){
									?>
										<a href="<?php echo  base_url().$inventory_data_arr["file_1_upload"];?>" download>Download</a> <i class="fa fa-times fa-1x" style="cursor:pointer" onclick="deleteDownloadsLinkFun('delete_file_1_upload')"></i>
									<?php
										}
									?>
								</span>
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="col-md-12"> 	
								<input type="text" name="file_2_name" id="file_2_name" class="form-control" placeholder="Name of file 2" value="<?php echo $inventory_data_arr["file_2_name"];?>">
							</div>
							<div class="col-md-12"> 	
								<input type="file" name="file_2_upload" id="file_2_upload" onchange="deleteDownloadsLinkFun('delete_file_2_upload')"> 
								<span id="delete_file_2_upload_span">
								<?php
									if($inventory_data_arr["file_2_upload"]!=""){
								?>
								<a href="<?php echo  base_url().$inventory_data_arr["file_2_upload"];?>" download>Download</a> <i class="fa fa-times fa-1x" style="cursor:pointer" onclick="deleteDownloadsLinkFun('delete_file_2_upload')"></i>
								<?php
									}
								?>
								</span>
							</div>
						</div>
					</div>
					
					
					<div class="row">
						<div class="col-md-6">
							<div class="col-md-12"> 	
								<input type="text" name="file_3_name" id="file_3_name" class="form-control" placeholder="Name of file 3" value="<?php echo $inventory_data_arr["file_3_name"];?>">
							</div>
							<div class="col-md-12"> 	
								<input type="file" name="file_3_upload" id="file_3_upload" onchange="deleteDownloadsLinkFun('delete_file_3_upload')"> 
								<span id="delete_file_3_upload_span">
								<?php
									if($inventory_data_arr["file_3_upload"]!=""){
								?>
								<a href="<?php echo  base_url().$inventory_data_arr["file_3_upload"];?>" download>Download</a> <i class="fa fa-times fa-1x" style="cursor:pointer" onclick="deleteDownloadsLinkFun('delete_file_3_upload')"></i>
								<?php
									}
								?>
								</span>
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="col-md-12"> 	
								<input type="text" name="file_4_name" id="file_4_name" class="form-control" placeholder="Name of file 4" value="<?php echo $inventory_data_arr["file_4_name"];?>">
							</div>
							<div class="col-md-12"> 	
								<input type="file" name="file_4_upload" id="file_4_upload" onchange="deleteDownloadsLinkFun('delete_file_4_upload')"> 
								<span id="delete_file_4_upload_span">
								<?php
									if($inventory_data_arr["file_4_upload"]!=""){
								?>
								<a href="<?php echo  base_url().$inventory_data_arr["file_4_upload"];?>" download>Download</a> <i class="fa fa-times fa-1x" style="cursor:pointer" onclick="deleteDownloadsLinkFun('delete_file_4_upload')"></i>
								<?php
									}
								?>
								</span>
							</div>
						</div>
					</div>
					
					
					<div class="row">
						<div class="col-md-6">
							<div class="col-md-12"> 	
								<input type="text" name="file_5_name" id="file_5_name" class="form-control" placeholder="Name of file 5" value="<?php echo $inventory_data_arr["file_5_name"];?>">
							</div>
							<div class="col-md-12"> 	
								<input type="file" name="file_5_upload" id="file_5_upload" onchange="deleteDownloadsLinkFun('delete_file_5_upload')"> 
								<span id="delete_file_5_upload_span">
								<?php
									if($inventory_data_arr["file_5_upload"]!=""){
								?>
								<a href="<?php echo  base_url().$inventory_data_arr["file_5_upload"];?>" download>Download</a> <i class="fa fa-times fa-1x" style="cursor:pointer" onclick="deleteDownloadsLinkFun('delete_file_5_upload')"></i>
								<?php
									}
								?>
								</span>
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="col-md-12"> 	
								<input type="text" name="file_6_name" id="file_6_name" class="form-control" placeholder="Name of file 6" value="<?php echo $inventory_data_arr["file_6_name"];?>">
							</div>
							<div class="col-md-12"> 	
								<input type="file" name="file_6_upload" id="file_6_upload" onchange="deleteDownloadsLinkFun('delete_file_6_upload')"> 
								<span id="delete_file_6_upload_span">
								<?php
									if($inventory_data_arr["file_6_upload"]!=""){
								?>
								<a href="<?php echo  base_url().$inventory_data_arr["file_6_upload"];?>" download>Download</a> <i class="fa fa-times fa-1x" style="cursor:pointer" onclick="deleteDownloadsLinkFun('delete_file_6_upload')"></i>
								<?php
									}
								?>
								</span>
							</div>
						</div>
					</div>
					
						
						
                </div>
            </div>
        </div>
        </div>





		
		<!--- product technical specs starts -->
		
		<div class="panel-group" id="accordion6" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingProductTechnicalSpecs">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseProductTechnicalSpecs" aria-expanded="true" aria-controls="collapseProductTechnicalSpecs">
                        Product Technical Specs
                    </a>
                </h4>
            </div>
            <div id="collapseProductTechnicalSpecs" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingProductTechnicalSpecs">
                <div class="panel-body">
                      
					  <?php
						for($spec_i=1;$spec_i<=10;$spec_i++){
					  ?>
					  
					<div class="row">
						<div class="col-md-3">
							<input type="text" name="prod_tech_spec_name_<?php echo $spec_i;?>" id="prod_tech_spec_name_<?php echo $spec_i;?>" class="form-control" placeholder="Name <?php echo $spec_i;?>" value="<?php echo $inventory_data_arr["prod_tech_spec_name_".$spec_i];?>">
						</div>
						
						<div class="col-md-8">
							<textarea name="prod_tech_spec_desc_<?php echo $spec_i;?>" id="prod_tech_spec_desc_<?php echo $spec_i;?>" class="form-control" placeholder="Description <?php echo $spec_i;?>" rows="5"><?php echo $inventory_data_arr["prod_tech_spec_desc_".$spec_i];?></textarea>
						</div>
					</div>
					
					
					 <?php
						}
					  ?>
						
						
                </div>
            </div>
        </div>
        </div>
		
		
		<!--- product technical specs ends -->
		
		<!-- About the brand starts --->
		
	<div class="panel-group" id="accordion7" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingAboutthebrand">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseAboutthebrand" aria-expanded="true" aria-controls="collapseAboutthebrand">
                        
                        About the Brand
                    </a>
                </h4>
            </div>
            <div id="collapseAboutthebrand" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingAboutthebrand">
                <div class="panel-body">
                      
					  
					<div class="row">
						<div class="col-md-12"> 	
								<textarea class="form-control" name="highlight_aboutthebrand" id="highlight_aboutthebrand"></textarea>
						</div>
					</div>
						
						
                </div>
            </div>
        </div>
        </div>


		<!-- About the brand ends --->
			
			
			
		
		<!--- testimonial starts -->
		
		<div class="panel-group" id="accordion7" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTestimonial">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTestimonial" aria-expanded="true" aria-controls="collapseTestimonial">
                        What customers opinion about this product
                    </a>
                </h4>
            </div>
            <div id="collapseTestimonial" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTestimonial">
                <div class="panel-body">
                      
					
					 <h6>First Entry</h6> 
					<div class="row">
						<div class="col-md-12">
							<input type="text" name="testimonial_name_1" id="testimonial_name_1" class="form-control" placeholder="Name" value="<?php echo $inventory_data_arr["testimonial_name_1"];?>">
						</div>
						
						<div class="col-md-12">
							<textarea name="testimonial_desc_1" id="testimonial_desc_1" class="form-control" placeholder="Description" rows="5"><?php echo $inventory_data_arr["testimonial_desc_1"];?></textarea>
						</div>
					</div>
					<h6>Second Entry</h6> 
					<div class="row">
						<div class="col-md-12">
							<input type="text" name="testimonial_name_2" id="testimonial_name_2" class="form-control" placeholder="Name" value="<?php echo $inventory_data_arr["testimonial_name_2"];?>">
						</div>
						
						<div class="col-md-12">
							<textarea name="testimonial_desc_2" id="testimonial_desc_2" class="form-control" placeholder="Description" rows="5"><?php echo $inventory_data_arr["testimonial_desc_2"];?></textarea>
						</div>
					</div>
					<h6>Third Entry</h6> 
					<div class="row">
						<div class="col-md-12">
							<input type="text" name="testimonial_name_3" id="testimonial_name_3" class="form-control" placeholder="Name" value="<?php echo $inventory_data_arr["testimonial_name_3"];?>">
						</div>
						
						<div class="col-md-12">
							<textarea name="testimonial_desc_3" id="testimonial_desc_3" class="form-control" placeholder="Description" rows="5"><?php echo $inventory_data_arr["testimonial_desc_3"];?></textarea>
						</div>
					</div>
					
					
					
						
                </div>
            </div>
        </div>
        </div>
		
		
		<!--- testimonial ends -->
		
		
		
		
		<!--- Warranty starts -->
		
		<div class="panel-group" id="accordion8" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingWarranty">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseWarranty" aria-expanded="true" aria-controls="collapseWarranty">
                        Warranty
                    </a>
                </h4>
            </div>
            <div id="collapseWarranty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingWarranty">
                <div class="panel-body">
                      
					<div class="row">
						<div class="col-md-12">
							<input type="text" name="warranty" id="warranty" class="form-control" placeholder="Warranty" value="<?php echo $inventory_data_arr["warranty"];?>">
						</div>
					</div>
				
					
					
					
						
                </div>
            </div>
        </div>
        </div>
		
		
		<!--- Warranty ends -->
		
		
		<!--- emi starts -->
		
		<div class="panel-group" id="accordion_emi" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingemi">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsemi" aria-expanded="true" aria-controls="collapsemi">
                        EMI options
                    </a>
                </h4>
            </div>
            <div id="collapsemi" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingemi">
                <div class="panel-body">
                      
				<div class="row">
				<div class="col-md-10"> 
					<div class="form-group">
					<div class="col-md-5 col-md-offset-1">Do you have EMI options ?</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="emi_options" id="emi1" value="yes" type="radio" <?php echo ($inventory_data_arr["emi_options"]=='yes') ? 'checked': ''; ?>>Yes</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="emi_options" id="emi2" value="no" type="radio" <?php echo ($inventory_data_arr["emi_options"]=='no') ? 'checked': ''; ?>>No</label>
					</div>
				
					</div>
				</div>
			</div>	

						
                </div>
            </div>
        </div>
        </div>
		
		
		<!--- emi ends -->

		<!--- emi starts -->
		
		<div class="panel-group" id="accordion_brouchure" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading_brouchure">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collaps_brouchure" aria-expanded="true" aria-controls="collaps_brouchure">
                       Product Brouchure

                    </a>
                </h4>
            </div>
            <div id="collaps_brouchure" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_brouchure">
                <div class="panel-body">
                      
				<div class="row">
				<div class="col-md-10"> 
					<div class="form-group">
					<div class="col-md-5 col-md-offset-1"><label class="form-label" for="brouchure">Upload Product Brouchure </label>
					<div>
	<u class="text-info"><a href="<?php echo base_url()."".$inventory_data_arr['brouchure'] ?>" target="_blank"><i class="fa fa-paperclip" aria-hidden="true"></i> Uploaded Attachment</a></u>
					</div>
				
				</div>
					<div class="col-md-5">
						<input name="brouchure" id="brouchure" type="file" class="form-control" style="opacity: inherit;
    height: 50px;">
	<input name="old_brouchure" id="old_brouchure" type="hidden" value="<?php echo $inventory_data_arr["brouchure"] ?>" class="form-control" >
	

					</div>
				
					<br>
					

					</div>
				</div>
			</div>	

						
                </div>
            </div>
        </div>
        </div>
		
		
		<!--- brouchure ends -->
		
		
		
		<!--- Shipswithin starts -->
		
		<div class="panel-group" id="accordion8" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingShipswithin">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseShipswithin" aria-expanded="true" aria-controls="collapseShipswithin">
					Shipping Timeline
                    </a>
                </h4>
            </div>
            <div id="collapseShipswithin" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingShipswithin">
                <div class="panel-body">
                      
					<div class="row">
						<div class="col-md-12">
							
							<select type="text" name="shipswithin" id="shipswithin" class="form-control" >
								<option value="">Shipped In XX Number Of Days</option>
								<?php
								/*for($i=0;$i<=60;$i++){
									?>
									<option value="<?php echo $i; ?>" <?php echo ($inventory_data_arr["shipswithin"]==$i) ? 'selected' : ''; ?>><?php echo $i ?> </option>
									<?php
								}*/
								?>
								<option value=""></option>
								<option value="1" <?php echo ($inventory_data_arr["shipswithin"]=='1') ? 'selected' : ''; ?>>1</option>
								<option value="2" <?php echo ($inventory_data_arr["shipswithin"]=='2') ? 'selected' : ''; ?>>2</option>
								<option value="3" <?php echo ($inventory_data_arr["shipswithin"]=='3') ? 'selected' : ''; ?>>3</option>
								<option value="4" <?php echo ($inventory_data_arr["shipswithin"]=='4') ? 'selected' : ''; ?>>4</option>
								<option value="5" <?php echo ($inventory_data_arr["shipswithin"]=='5') ? 'selected' : ''; ?>>5</option>
								<option value="6" <?php echo ($inventory_data_arr["shipswithin"]=='6') ? 'selected' : ''; ?>>6</option>
								<option value="7" <?php echo ($inventory_data_arr["shipswithin"]=='7') ? 'selected' : ''; ?>>7</option>
								<option value="10" <?php echo ($inventory_data_arr["shipswithin"]=='10') ? 'selected' : ''; ?>>10</option>
								<option value="15" <?php echo ($inventory_data_arr["shipswithin"]=='15') ? 'selected' : ''; ?>>15</option>
								<option value="20" <?php echo ($inventory_data_arr["shipswithin"]=='20') ? 'selected' : ''; ?>>20</option>
								<option value="30" <?php echo ($inventory_data_arr["shipswithin"]=='30') ? 'selected' : ''; ?>>30</option>
								<option value="45" <?php echo ($inventory_data_arr["shipswithin"]=='45') ? 'selected' : ''; ?>>45</option>
								<option value="60" <?php echo ($inventory_data_arr["shipswithin"]=='60') ? 'selected' : ''; ?>>60</option>

							</select>
						</div>
					</div>
				
					
					
					
						
                </div>
            </div>
        </div>
        </div>
		
		
		<!--- Shipswithin ends -->
		
		
		<div class="panel-group" id="accordion_tag" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading_tag">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_tag" aria-expanded="true" aria-controls="collapse_tag">
                       Tag to Frontend
                    </a>
                </h4>
            </div>
            <div id="collapse_tag" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_tag">
                <div class="panel-body">
                      
					<div class="row">
					<div class="col-md-4 col-md-offset-1"><label class="form-label" for="brouchure">Frontend Display of Product Under  </label></div>
						<div class="col-md-6">
							
								<?php
								$under_pcat_frontend=$inventory_data_arr["under_pcat_frontend"];
								echo $under_pcat_frontend;
								$under_pcat_frontend_arr=($under_pcat_frontend!='') ? explode(',',$under_pcat_frontend) : [];
								//print_r($under_pcat_frontend_arr);

								foreach($parent_category as $parent){

									?>
									<input type="checkbox" name="under_pcat_frontend[]" id="typ_<?php echo $parent->pcat_id; ?>" value="<?php echo $parent->pcat_id; ?>" <?php echo (in_array($parent->pcat_id,$under_pcat_frontend_arr)) ? 'checked' : '';?>> <label class="control-label" for="typ_<?php echo $parent->pcat_id; ?>"><?php echo $parent->pcat_name; ?> </label>
									<?php
								}
								?>
							
						</div>
					</div>
				
					
					
					
						
                </div>
            </div>
        </div>
        </div>
		
			
			
			
	</div>
	<div class="tab-pane" id="edit_step-21">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
			<div class="form-group"> 
				<label class="control-label">-Select Vendor-
				</label>


				<?php 
				//print_r($vendors);
				//echo $inventory_data_arr["inv_vendor_id"];
				?>
				
				<select class="form-control" name="edit_vendors" id="edit_vendors">
					<option value=""></option>
					<?php foreach ($vendors as $vendors_value) {  ?>
					<?php
					  if($inventory_data_arr["inv_vendor_id"]==$vendors_value->vendor_id){
					?>
					<option value="<?php echo $vendors_value->vendor_id; ?>" selected><?php echo $vendors_value->name; ?></option>
					<?php
					  }
					  else{
							
						  ?>
						  
						  <option value="<?php echo $vendors_value->vendor_id; ?>"><?php echo $vendors_value->name; ?></option>
						  <?php
					  }
					?>
					<?php } ?>
				</select>
			</div>
			</div>
		</div>	
		<?php
			//print_r($inventory_data_arr);
		?>
		
	<div class="row">
		<div class="col-md-5 col-md-offset-1"> 
			<div class="form-group">
			
				<label class="control-label">Select Parcel Category
				</label>
				<select class="form-control" name="edit_logistics_parcel_category_id[]" multiple id="edit_logistics_parcel_category">
				<?php
				foreach($logistics_parcel_category as $logistics_parcel_category_value)
				{
					 $logistics_parcel_category_id_in=$inventory_data_arr["logistics_parcel_category_id"];
					 $logistics_parcel_category_id_arr=explode(",",$logistics_parcel_category_id_in);
					 if(in_array($logistics_parcel_category_value->logistics_parcel_category_id,$logistics_parcel_category_id_arr)){
						echo '<option value="'.$logistics_parcel_category_value->logistics_parcel_category_id.'" selected>'.$logistics_parcel_category_value->logistics_name.'-'.$logistics_parcel_category_value->parcel_type.'</option>';
					 }
					 else{
						 echo '<option value="'.$logistics_parcel_category_value->logistics_parcel_category_id.'" >'.$logistics_parcel_category_value->logistics_name.'-'.$logistics_parcel_category_value->parcel_type.'</option>';
					 }
				}
				?>
				</select>
			</div>
		</div>
		<div class="col-md-5"> 
			<div class="form-group">
				<label class="control-label">Select Free shipping Territories
				</label>
				<?php
					//print_r($logistics_all_territory_name);
				?>
				<select name="edit_logistics_territory_id[]" id="edit_logistics_territory" class="form-control" multiple>
				
				<?php
				foreach($logistics_all_territory_name as $logistics_all_territory_name_value)
				{
					 $free_shipping_territories_in=$inventory_data_arr["free_shipping_territories"];
					$free_shipping_territories_arr=explode(",",$free_shipping_territories_in);
					 if(in_array($logistics_all_territory_name_value->territory_name_id,$free_shipping_territories_arr)){
				echo '<option value="'.$logistics_all_territory_name_value->territory_name_id.'" selected>'.$logistics_all_territory_name_value->territory_name.'</option>';
					 }
					 else{
						echo '<option value="'.$logistics_all_territory_name_value->territory_name_id.'" >'.$logistics_all_territory_name_value->territory_name.'</option>'; 
					 }
				}
				?>
				</select>
			</div>
		</div>
	</div>	
	
	<div class="row">
		<div class="col-md-5 col-md-offset-1"> 
			<div class="form-group">
			<script>
				function edit_shipping_charge_not_applicableFun(obj){
						$("#edit_flat_shipping_charge").val("");
						$("#edit_inventory_weight_in_kg").val("");
						$("#edit_inventory_volume_in_kg").val("");
						$("#edit_inventory_length").val("");
						$("#edit_inventory_breadth").val("");
						$("#edit_inventory_height").val("");
						
					if(obj.checked==true){
						$("#edit_flat_shipping_charge_div").css({"display":""});
						if(!$("#edit_flat_shipping_charge").attr("required")){
							$("#edit_flat_shipping_charge").attr("required",true);
						}
						
						$("#edit_inventory_weight_in_kg_edit_inventory_volume_in_kg_div").css({"display":"none"});
						if($("#edit_inventory_weight_in_kg").attr("required")){
							$("#edit_inventory_weight_in_kg").attr("required",false);
						}
						if($("#edit_inventory_volume_in_kg").attr("required")){
							$("#edit_inventory_volume_in_kg").attr("required",false);
						}
						
						$("#edit_inventory_length_div").css({"display":"none"});
						if($("#edit_inventory_length").attr("required")){
							$("#edit_inventory_length").attr("required",false);
						}
						$("#edit_inventory_breadth_div").css({"display":"none"});
						if($("#edit_inventory_breadth").attr("required")){
							$("#edit_inventory_breadth").attr("required",false);
						}
						$("#edit_inventory_height_div").css({"display":"none"});
						if($("#edit_inventory_height").attr("required")){
							$("#edit_inventory_height").attr("required",false);
						}
						
						
					}
					else{
						$("#edit_flat_shipping_charge_div").css({"display":"none"});
						if($("#edit_flat_shipping_charge").attr("required")){
							$("#edit_flat_shipping_charge").attr("required",false);
						}
						
						$("#edit_inventory_weight_in_kg_edit_inventory_volume_in_kg_div").css({"display":""});
						if(!$("#edit_inventory_weight_in_kg").attr("required")){
							$("#edit_inventory_weight_in_kg").attr("required");
						}
						if(!$("#edit_inventory_volume_in_kg").attr("required")){
							$("#edit_inventory_volume_in_kg").attr("required");
						}
						
						$("#edit_inventory_length_div").css({"display":""});
						if(!$("#edit_inventory_length").attr("required")){
							$("#edit_inventory_length").attr("required");
						}
						$("#edit_inventory_breadth_div").css({"display":""});
						if(!$("#edit_inventory_breadth").attr("required")){
							$("#edit_inventory_breadth").attr("required");
						}
						$("#edit_inventory_height_div").css({"display":""});
						if(!$("#edit_inventory_height").attr("required")){
							$("#edit_inventory_height").attr("required");
						}
					}
				}
			</script>
				<input name="edit_shipping_charge_not_applicable" id="edit_shipping_charge_not_applicable"  type="checkbox" value="yes" <?php if($inventory_data_arr["shipping_charge_not_applicable"]=="yes"){echo "checked";}?> onchange="edit_shipping_charge_not_applicableFun(this)"/>
				<label class="control-label">Shipping Charge Not Applicable
				</label>
			</div>
		</div>
		<div class="col-md-5" <?php if($inventory_data_arr["shipping_charge_not_applicable"]!="yes"){echo "style='display:none;'";}?> id="edit_flat_shipping_charge_div"> 
			<div class="form-group">
				<label class="control-label">
				</label>
				 <?php 
					if($inventory_data_arr["shipping_charge_not_applicable"]=="yes"){
						$edit_flat_shipping_charge=$inventory_data_arr["flat_shipping_charge"];
					}
					else{
						$edit_flat_shipping_charge=0;
					}
				 ?>
				<input class="form-control" name="edit_flat_shipping_charge" type="text" id="edit_flat_shipping_charge"   value="<?php echo $edit_flat_shipping_charge;?>" onKeyPress="return isNumber(event)" <?php if($inventory_data_arr["shipping_charge_not_applicable"]=="yes"){echo "required'";}?>/>
			</div>
		</div>			 
	</div>	
	
	
	<div class="row"  <?php if($inventory_data_arr["shipping_charge_not_applicable"]=="yes"){echo "style='display:none;'";}?>  id="edit_inventory_weight_in_kg_edit_inventory_volume_in_kg_div">
		<div class="col-md-5 col-md-offset-1"> 
			<div class="form-group">
				<label class="control-label">Enter Consignment Actual Weight in kg
				</label>
				<input class="form-control" name="edit_inventory_weight_in_kg" id="edit_inventory_weight_in_kg"  type="number" value="<?php echo $inventory_data_arr["inventory_weight_in_kg"];?>" step="any"  <?php if($inventory_data_arr["shipping_charge_not_applicable"]=="yes"){}else{echo "required";}?>/>
			</div>
		</div>
		<div class="col-md-5"> 
			<div class="form-group">
				<label class="control-label">Enter Volumetric Weight in cms (L x B x H in cms)
				</label>
				<input class="form-control" name="edit_inventory_volume_in_kg" type="number" id="edit_inventory_volume_in_kg"   value="<?php echo $inventory_data_arr["inventory_volume_in_kg"];?>" step="any"  <?php if($inventory_data_arr["shipping_charge_not_applicable"]=="yes"){}else{echo "required";}?>/>
			</div>
		</div>			 
	</div>	

	<div class="row">

	<div class="" id="inventory_unit_div">
				<div class="col-md-2 col-md-offset-1"> 
					<div class="form-group">
						<label class="control-label">Select Unit
						</label>
						<select class="form-control" name="inventory_unit" id="inventory_unit" >
							<option value=""></option>
							<option value="mm" <?php echo ($inventory_data_arr["inventory_unit"]=='mm') ? 'selected' : '';?> >mm</option>
							<option value="cm" <?php echo ($inventory_data_arr["inventory_unit"]=='cm') ? 'selected' : '';?>>cm</option>
							<option value="m" <?php echo ($inventory_data_arr["inventory_unit"]=='m') ? 'selected' : '';?>>m</option>
						</select>
					</div>
				</div>
			</div>

<div class=""  <?php if($inventory_data_arr["shipping_charge_not_applicable"]=="yes"){echo "style='display:none;'";}?>   id="edit_inventory_length_div">
				<div class="col-md-3 "> 
					<div class="form-group">
						<label class="control-label">Inventory Length
						</label>
						<input class="form-control" name="edit_inventory_length" id="edit_inventory_length"  type="number" value="<?php echo $inventory_data_arr["inventory_length"];?>" step="any"  <?php if($inventory_data_arr["shipping_charge_not_applicable"]=="yes"){}else{echo "required";}?>/>
					</div>
				</div>
			</div>	
			<div class=""  <?php if($inventory_data_arr["shipping_charge_not_applicable"]=="yes"){echo "style='display:none;'";}?>   id="edit_inventory_breadth_div">
				<div class="col-md-3"> 
					<div class="form-group">
						<label class="control-label">Inventory Breadth
						</label>
						<input class="form-control" name="edit_inventory_breadth" id="edit_inventory_breadth"  type="number"  value="<?php echo $inventory_data_arr["inventory_breadth"];?>"  step="any"  <?php if($inventory_data_arr["shipping_charge_not_applicable"]=="yes"){}else{echo "required";}?>/>
					</div>
				</div>
			</div>	
			<div class=""  <?php if($inventory_data_arr["shipping_charge_not_applicable"]=="yes"){echo "style='display:none;'";}?>  id="edit_inventory_height_div">
				<div class="col-md-2"> 
					<div class="form-group">
						<label class="control-label">Inventory Height
						</label>
						<input class="form-control" name="edit_inventory_height" id="edit_inventory_height"  type="number"  value="<?php echo $inventory_data_arr["inventory_height"];?>"   step="any"  <?php if($inventory_data_arr["shipping_charge_not_applicable"]=="yes"){}else{echo "required";}?>/>
					</div>
				</div>
			</div>

				</div> <!---row--->		

<?php
/*
?>
	<div class="row">
		<div class="col-md-5 col-md-offset-1"> 
			<div class="form-group">
				<label class="control-label">Enter Volumetric Breakup Point
				</label>
				<input class="form-control" name="edit_volumetric_breakup_point" id="edit_volumetric_breakup_point"  type="number" value="<?php echo $inventory_data_arr["volumetric_breakup_point"];?>"/>
			</div>
		</div>
	</div>	
	<?php
*/
?>
		  
		
	</div>
	<?php $selected_val=''; ?>
	<div class="tab-pane" id="edit_step-31">
		

			<?php
			
			foreach($attributes as $attributes_value)
			{
			?>
			<input type="hidden" name="edit_attribute_1" id="edit_attribute_1"  value="<?php echo $attributes_value->attribute1_name;?>"/>
			<?php
			
			if(strtolower($attributes_value->attribute1_name)=="color"){
				?>
			<div class="row">
				<div class="col-md-5 col-md-offset-1"> 
				
					<div class="form-group">
					<?php $attribute_values=explode(',',$attributes_value->attribute1_option);?>
					<label class="control-label">-Select <?php echo $attributes_value->attribute1_name;?>-
					</label>
					<select onchange="edit_display_color(this.value);assign_filter_values(this.value,'<?php echo trim(strtolower($attributes_value->attribute1_name));?>','color')" name="edit_attribute_1_value" class="form-control" id="edit_attribute_1_value">
						<option value=""></option>
						<?php
					
							foreach($attribute_values as $val){
								if($val!=""){
									$color_arr=explode(':',$val);
									//echo '<option value="'.$val.'">'.$color_arr[0].'</option>';
									
									if($inventory_data_arr["attribute_1_value"]==$val){
										$selected_val=$val;
										echo '<option value="'.$val.'" selected>'.$color_arr[0].'</option>';
									}
									else{
										echo '<option value="'.$val.'">'.$color_arr[0].'</option>';
									}
								}
							}
						?>
					</select>
					<script>
					$(document).ready(function(){
						assign_filter_values('<?php echo $selected_val;?>','<?php echo trim(strtolower($attributes_value->attribute1_name));?>','color')
					});
					</script>
					</div>
				</div>
			</div>
				<?php
			
			}else{
			
			?>
			<div class="row">
				<div class="col-md-5 col-md-offset-1"> 
					<div class="form-group">
					<label class="control-label">-Select <?php echo $attributes_value->attribute1_name;?>-
					</label>
					<?php $attribute_values=explode(',',$attributes_value->attribute1_option); ?>
					<select name="edit_attribute_1_value" id="edit_attribute_1_value" class="form-control" onchange="assign_filter_values(this.value,'<?php echo trim(strtolower($attributes_value->attribute1_name));?>')">
						<option value=""></option>
						<?php
							foreach($attribute_values as $val)
							{
								if($val!=""){
									if($inventory_data_arr["attribute_1_value"]==$val){
										$selected_val=$val;
										echo '<option value="'.$val.'" selected>'.$val.'</option>';
									}
									else{
										echo '<option value="'.$val.'">'.$val.'</option>';
									}
								}
							}
						?>
					</select>
					<script>
					$(document).ready(function(){
						assign_filter_values('<?php echo $selected_val;?>','<?php echo trim(strtolower($attributes_value->attribute1_name));?>')
					});
					</script>
					</div>
				</div>
			</div>
				<?php } ?>
	
			<?php if(!empty($attributes_value->attribute2_name)){ ?>

				<input type="hidden" name="edit_attribute_2" id="edit_attribute_2"   value="<?php echo $attributes_value->attribute2_name;?>"/>

			<div class="row">
				<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group">
				<?php $attribute_values=explode(',',$attributes_value->attribute2_option); ?>
				<label class="control-label">-Select <?php echo $attributes_value->attribute2_name;?>-
				</label>
				<select name="edit_attribute_2_value" id="edit_attribute_2_value" class="form-control" onchange="assign_filter_values(this.value,'<?php echo trim(strtolower($attributes_value->attribute2_name));?>')">
					<option value=""></option>
					<?php
					foreach($attribute_values as $val)
					{  
						if($val!=""){
							if($inventory_data_arr["attribute_2_value"]==$val){
								$selected_val=$val;
								echo '<option value="'.$val.'" selected>'.$val.'</option>';
							}
							else{
								echo '<option value="'.$val.'" >'.$val.'</option>';
							}
						}
					}
					?>
				</select>
				
				<script>
				$(document).ready(function(){
					assign_filter_values('<?php echo $selected_val;?>','<?php echo trim(strtolower($attributes_value->attribute2_name));?>')
				});
				</script>
					
				</div>
				</div>
			</div>
			<?php }else{ ?>	 
		  
				<input type="hidden" name="edit_attribute_2" id="edit_attribute_2"   value="<?php echo $attributes_value->attribute2_name;?>"/>
				<input type="hidden" name="edit_attribute_2_value"    value="<?php echo $inventory_data_arr["attribute_4_value"];?>"/>
			   <?php } ?>
		  
			<?php if(!empty($attributes_value->attribute3_name)){ ?>

					<input type="hidden" name="edit_attribute_3" id="edit_attribute_3"   value="<?php echo $attributes_value->attribute3_name;?>" />

			<div class="row">
				<div class="col-md-5 col-md-offset-1">
					<div class="form-group">
					<?php $attribute_values=explode(',',$attributes_value->attribute3_option); ?>
					<label class="control-label">-Select <?php echo $attributes_value->attribute3_name;?>-
					</label>
					<select name="edit_attribute_3_value" id="edit_attribute_3_value" class="form-control" onchange="assign_filter_values(this.value,'<?php echo trim(strtolower($attributes_value->attribute3_name));?>')">
						<option value=""></option>
						<?php
						foreach($attribute_values as $val)
						{
							if($val!=""){
								if($inventory_data_arr["attribute_3_value"]==$val){
									$selected_val=$val;
									echo '<option value="'.$val.'" selected>'.$val.'</option>';
								}
								else{
									echo '<option value="'.$val.'" >'.$val.'</option>';
								}
							}
						}
						?>
					</select>
					<script>
					$(document).ready(function(){
						assign_filter_values('<?php echo $selected_val;?>','<?php echo trim(strtolower($attributes_value->attribute3_name));?>')
					});
					</script>
					</div>
				</div>
			</div>
			<?php }else{ ?>	 
		  
				<input type="hidden" name="edit_attribute_3" id="edit_attribute_3"   value="<?php echo $attributes_value->attribute3_name;?>" />
				<input type="hidden" name="edit_attribute_3_value" id="edit_attribute_3_value"   value="<?php echo $inventory_data_arr["attribute_3_value"];?>" />
			   <?php
			} ?>
		  
			<?php if(!empty($attributes_value->attribute4_name)){ ?>

				<input type="hidden" name="edit_attribute_4" id="edit_attribute_4"   value="<?php echo $attributes_value->attribute4_name;?>"/>

			<div class="row">
				<div class="col-md-5 col-md-offset-1">
				<div class="form-group">
				<?php $attribute_values=explode(',',$attributes_value->attribute4_option); ?>
				<label class="control-label">-Select <?php echo $attributes_value->attribute4_name;?>-
				</label>
				<select name="edit_attribute_4_value" id="edit_attribute_4_value" class="form-control" onchange="assign_filter_values(this.value,'<?phpecho trim(strtolower($attributes_value->attribute4_name));?>')">
					<option value=""></option>
					<?php
					foreach($attribute_values as $val)
					{
						if($val!=""){
							if($inventory_data_arr["attribute_4_value"]==$val){
								$selected_val=$val;
								echo '<option value="'.$val.'" selected>'.$val.'</option>';
							}
							else{
								echo '<option value="'.$val.'" >'.$val.'</option>';
							}
						}
					}
					?>
				</select>
				<script>
				$(document).ready(function(){
					assign_filter_values('<?php echo $selected_val;?>','<?php echo trim(strtolower($attributes_value->attribute4_name));?>')
				});
				</script>
				</div>
				</div>
			</div>
		  <?php }else{ ?>	 
		  
				<input type="hidden" name="edit_attribute_4" id="edit_attribute_4" class="form-control" value="<?php echo $attributes_value->attribute4_name;?>"/>
				<input type="hidden" name="edit_attribute_4_value" id="edit_attribute_4_value" class="form-control" value="<?php echo $inventory_data_arr["attribute_4_value"];?>"/>
			   <?php
			} 
		  
			} ?>
		
	</div>
  
	<div class="tab-pane well CamListDiv" id="edit_step-41">
		
		<?php

		foreach($get_all_specification_group as $get_all_specification_group_value)
		{
			?>
		<div class="panel-group">
		<div class="panel panel-default">
			<div class="panel-heading" data-toggle="collapse" href="#collapse<?php echo $get_all_specification_group_value->specification_group_id; ?>">
					<h4 class="panel-title">
						<a><?php echo $get_all_specification_group_value->specification_group_name; ?></a>
					</h4>
			</div> 
			 <div id="collapse<?php echo $get_all_specification_group_value->specification_group_id; ?>" class="panel-collapse collapse">
			 <div class="panel-body">	
			<?php
			$sp=$controller->get_all_specification($get_all_specification_group_value->specification_group_id);

			$i=0;
			//print_r($get_all_specification_by_inventory_id_arr);
			
			foreach($sp as $k =>$sp_value)
			{
				//print_r($get_all_specification_by_inventory_id_arr);
				$spe=array();
				foreach($get_all_specification_by_inventory_id_arr as $spec_arr){
					
					$spe[]=$spec_arr["specification_id"];

					/*if($spec_arr["specification_id"]==$sp_value->specification_id){
						$spec_result_arr=$spec_arr;
						break;
					}*/
				}
				
				
				if(in_array($sp_value->specification_id, $spe)){
					$key=array_search($sp_value->specification_id, $spe);
					$spec_result_arr=$get_all_specification_by_inventory_id_arr[$key];
				}else{
					//none selected // not available in db
					$spec_result_arr["inventory_specification_id"]="";
					$spec_result_arr["value_texted"]='';
					foreach($spec_result_arr as $x => $y){
						$spec_result_arr[$x]="";
					}
				}
					
				
				$i++;
				if($i%2==1 || $i==1){
					echo '</div><div class="row">'; 
				}
			?>

			
			<div class="col-md-5 <?php if($k%2==0){?>col-md-offset-1<?php } ?>">
				<div class="form-group">
				
				<label class="control-label">-Select <?php echo $sp_value->specification_name; ?>-
				</label>
				<select name="edit_specification_select_value[<?php echo $sp_value->specification_id;?>]" class="form-control" onchange="spec_fun_edit(this,<?php echo $sp_value->specification_id;?>)">

					<option value=""></option>
					<?php
					$flag_selected=0;
					$values=explode(',',$sp_value->specification_value);
					foreach($values as $val)
					{
						if($val!=""){
							if($spec_result_arr["specification_value"]==$val){
								$flag_selected=1;
								echo '<option value="'.$val.'" selected>'.$val.'</option>';
							}
							else{
								echo '<option value="'.$val.'">'.$val.'</option>';
							}
						}
					}
					?>
					<option value="Others" <?php if($spec_result_arr["value_texted"]=="1"){$flag_selected=1; echo "selected";}?>>Others</option>
					<option value="0" <?php if($flag_selected==0){echo "selected";}?>>-None Selected-</option>
			
				</select>			
				</div>
			
			<input type="hidden" name="edit_inventory_specification_id[<?php echo $sp_value->specification_id;?>]" value="<?php echo $spec_result_arr["inventory_specification_id"];?>">

			<div class="edit_specification_textarea_value[<?php echo $sp_value->specification_id;?>]"  <?php if($spec_result_arr["value_texted"]=="1"){ echo 'style="display:block;"';}else{ echo 'style="display:none;"';}?> >
				<div class="form-group">
				<textarea name="edit_specification_textarea_value[<?php echo $sp_value->specification_id;?>]" id="textarea_<?php echo $get_all_specification_group_value->specification_group_id."_".$i; ?>" class="form-control" placeholder="Enter your own <?php echo $sp_value->specification_name; ?> value"><?php if($spec_result_arr["value_texted"]=="1"){echo $spec_result_arr["specification_value"];} ?></textarea>
				
				</div>
			</div>
				
			</div>
			 
			<?php
			
			}
			?>
			</div>
			</div>
			</div>  
			</div>     
			<?php
		}

		?>
   
		
	</div>
	
	<div class="tab-pane well CamListDiv" id="edit_step-51">
		<div class="row">
		<?php
		$count=0;
		//echo "<pre>";print_r($filterbox);echo "</pre>";
		$filtered_result_arr=[];
		foreach($filterbox as $k =>$filterbox_value)
		{
			foreach($get_all_filter_by_inventory_id_arr as $filtered_arr){
					if($filtered_arr["filterbox_id"]==$filterbox_value->filterbox_id){
						$filtered_result_arr=$filtered_arr;
						break;
					}
					else{
						if(!empty($filtered_result_arr)){
							foreach($filtered_result_arr as $k => $v){
								$filtered_result_arr[$k]="";
							}
						}
					}
				}
		?>
			<input type="hidden" name="edit_product_filter_id[<?php echo $filterbox_value->filterbox_id; ?>]" value="<?php echo isset($filtered_result_arr["product_filter_id"]) ? $filtered_result_arr["product_filter_id"] : '';?>"/>
			  
			<?php
				$sp=$controller->get_all_filter($filterbox_value->filterbox_id);
				if(strtolower($filterbox_value->filterbox_name)=="color"){
					
			?>

			
				<div class="col-md-5 <?php if($count%2==0){?>col-md-offset-1<?php } ?>">
				<div class="form-group">
				
				<?php
				if(isset($filtered_result_arr["filter_options"])){
					foreach($sp as $sp_value){
						if($filtered_result_arr["filter_options"]==$sp_value->filter_options){
				?>
				<input type="hidden" class="dynamic_input" name="edit_filter[<?php echo $filterbox_value->filterbox_id; ?>]" value="<?php echo $sp_value->filter_id;?>">
				<?php
						}
					}
				}
					?>
				
				<label class="control-label">-Select <?php echo $filterbox_value->filterbox_name; ?>-</label> 
					<select  id="<?php echo trim(strtolower($filterbox_value->filterbox_name)); ?>" filterbox_name="<?php echo trim(strtolower($filterbox_value->filterbox_name)); ?>" onchange="edit_filter_display_color(this.options[this.selectedIndex].getAttribute('filtervalues'))" name="edit_filter[<?php echo $filterbox_value->filterbox_id; ?>]" class="form-control filterbox" disabled>
						<option value=""></option>
					
						<?php
						$flag_selected=0;
							foreach($sp as $sp_value){
								
								$color_arr=explode(':',$sp_value->filter_options);
								
								if($filtered_result_arr["filter_options"]==$sp_value->filter_options){
									$flag_selected=1;
							echo '<option filtervalues="'.$sp_value->filter_options.'" value="'.$sp_value->filter_id.'" selected>'.$color_arr[0].'</option>';
						}
						else{
							echo '<option filtervalues="'.$sp_value->filter_options.'" value="'.$sp_value->filter_id.'">'.$color_arr[0].'</option>';
						}

							}
						?>

						<option value="0" <?php if($flag_selected==0){echo "selected";}?>>-None Selected-</option>
						
				
					</select>
				</div>	
				</div>
			
					<?php
					
				}else{
					  
					  ?>
			
			 <?php
						//if(strtolower($filterbox_value->filterbox_name)=="availability" || strtolower($filterbox_value->filterbox_name)=="price" || strtolower($filterbox_value->filterbox_name)=="brand"){
						if(isset($filtered_result_arr["filter_options"])){
							$flag_selected=0;
							foreach($sp as $sp_value){
								
								if($filtered_result_arr["filter_options"]==$sp_value->filter_options){
									$flag_selected=1;
					  ?>
					  <input type="hidden" class="dynamic_input" name="edit_filter[<?php echo $filterbox_value->filterbox_id; ?>]" value="<?php echo $sp_value->filter_id;?>">
					  <?php
								}
							}
							if($flag_selected==0){
								?>
								<input type="hidden" class="dynamic_input" name="edit_filter[<?php echo $filterbox_value->filterbox_id; ?>]" value="0">
								<?php
							}
						}
						?>
				<div class="col-md-5 <?php if($count%2==0){?>col-md-offset-1<?php } ?>"> 	  
					<div class="form-group">
					<label class="control-label">-Select <?php echo $filterbox_value->filterbox_name; ?>-</label> 
					<select id="<?php echo trim(strtolower($filterbox_value->filterbox_name)); ?>" filterbox_name="<?php echo trim(strtolower($filterbox_value->filterbox_name)); ?>" name="edit_filter[<?php echo $filterbox_value->filterbox_id; ?>]" class="form-control filterbox">
						<option value=""></option>
						
						<?php
						$flag_selected=0;
						foreach($sp as $sp_value)
						{
							
							if($filtered_result_arr["filter_options"]==$sp_value->filter_options){
								$flag_selected=1;
								echo '<option value="'.$sp_value->filter_id.'" selected>'.$sp_value->filter_options.'</option>';
							}
							else{
								echo '<option value="'.$sp_value->filter_id.'">'.$sp_value->filter_options.'</option>';
							}
						}
						?>
						<option value="0" <?php if($flag_selected==0){echo "selected";}?>>-None Selected-</option>
					</select>			
					</div>
				</div>	
			
					  <?php
				}
				$flag_selected=0;
				$count++;
		}
		?> 
		</div>	
	</div>

	
	<script>
	
	var typingTimer;                //timer identifier
	var doneTypingInterval = 5000;  //time in ms, 5 second for example
	var $input = $('#edit_selling_price');

	//on keyup, start the countdown
	$input.on('keyup', function () {
	  clearTimeout(typingTimer);
	  val=this.value;
	  typingTimer = setTimeout(doneTyping(val), doneTypingInterval);
	});

	//on keydown, clear the countdown 
	$input.on('keydown', function () {
	  clearTimeout(typingTimer);
	});

	//user is "finished typing," do something
	function doneTyping (val) {
	  if(val!=''){
		  assign_filter_values(val,'price')
	  }
	}
	
	
	function assign_filter_values_stock(){
		
		$("#availability > option").each(function() {
			var attr = $(this).attr('selected');
			if (typeof attr !== typeof undefined && attr !== false) {
				$(this).removeAttr("selected");
			}
	   })
			  
			  
		elem="#availability";
		stock=parseInt($("#edit_stock").val().trim());
		cutoff_stock=parseInt($("#edit_cutoff_stock").val().trim());
		if(stock!="" && cutoff_stock!=""){
			if(stock>cutoff_stock){
				stock_availability="yes";
			}
			else{
				stock_availability="no";
			}
			
			if(stock_availability=="yes"){
				$("#availability > option").each(function(){
					if($(this).val()=="0"){
						//$(this).attr({"selected":true});
						$("#availability").val($(this).val());
						$("#availability").attr({"disabled":true});
						$("#availability").parent('div').removeAttr("class");
						$("#availability").parent('div').addClass("label-floating form-group");
						
						$("input[name='"+$(elem).attr("name")+"'].dynamic_input").val(0);
						
						
					}
				});
			}
			if(stock_availability=="no"){
				$("#availability > option").each(function(){
					
					if($(this).text().toLowerCase()=="include out of stock"){
						$(this).attr({"selected":true});
						$("#availability").val($(this).val());
						
						$("#availability").attr({"disabled":true});
						$("#availability").parent('div').removeAttr("class");
						$("#availability").parent('div').addClass("label-floating form-group");
						
						$("input[name='"+$(elem).attr("name")+"'].dynamic_input").val($(this).val());
						
								
					}
				});
			}
			
		}
		
		
	}
	
	
	function assign_filter_values(data,name,color=''){

		$(".filterbox").each(function(index, elem) {
		   valu = $(elem).val();
		   filterbox_name = $(elem).attr("filterbox_name");
		   id=$(elem).attr('id');
		  
		  //id is elem
		  // alert(filterbox_name);
		   if(name==filterbox_name){
			  //alert(filterbox_name);
			  
			  $("#"+id+" > option").each(function() {
				  if($(this).attr("selected")==true){
					  $(this).removeAttr("selected");
				  }
			  })
				$("#"+id+" > option").each(function() {
					
					fil_text_org=this.value;
					//alert(fil_text_org);
					fil_text=this.text.toLowerCase();
					//alert(fil_text);
					data=data.toLowerCase();
					
					if(color=="color"){
						colors_arr=data.split(":");
						code=colors_arr[1];
						name=colors_arr[0];
						data=name;
					}
					
					//alert(fil_text + ' ' + data);
					
					if(name=="price"){
						if(fil_text!=''){
							fil_text_arr=fil_text.split("-");
							lprice=parseInt(fil_text_arr[0].trim());
							hprice=parseInt(fil_text_arr[1].trim());
	
							if(data>=lprice && data<=hprice){	
								$(this).attr({"selected":true});
								$(elem).val(fil_text_org);
								
								$(elem).attr({"disabled":true});
								$(elem).parent('div').removeAttr("class");
								$(elem).parent('div').addClass("label-floating form-group");
								
								$("input[name='"+$(elem).attr("name")+"'].dynamic_input").val(fil_text_org);
								
							}
						}
					}
					if(name!="price"){
						if(fil_text==data && fil_text_org!=''){
							//alert(fil_text);
							//$(this).attr('selected',"true");
							$(this).attr({"selected":true});
							$(elem).val(fil_text_org);
							
							//$(elem).attr({"disabled":true});
							$(elem).parent('div').removeAttr("class");
							$(elem).parent('div').addClass("label-floating form-group");
							
							$("input[name='"+$(elem).attr("name")+"'].dynamic_input").val(fil_text_org);
								
							

							
							
						}
					}
					
				});
		   }
		});

	}
	</script>
	
	<div class="tab-pane" id="edit_step-61">
		
		<!---common--image--->
		<div class="row">
			<div class="col-md-5 col-md-offset-3" id="inv_images_div">
			<div class="col-md-12 text-center"><b>Common (300x366)</b></div>
				<div class="col-md-12">
				<div class="picture-container">
					<div class="picture" id="edit_commonPic">
						<img src="<?php echo base_url().$inventory_data_arr["common_image"]?>" class="picture-src" id="edit_wizardPictureCommon" title=""/>
				
						<input type="file" name="edit_common_image" id="edit_wizard-picture-Common"/>
					</div>
				<h6 id="edit_wizardPictureCommon_picname">Common Image</h6>
				</div>
				</div>	
			</div>
		</div>
		<!---common--image--->
		
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
			
			<button name="add_image_inventory" id="add_image_inventory_btn" type="button" class="btn btn-success" disabled onclick="add_image_inventory_fun()">Add</button>
			
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 col-md-offset-2"  id="inv_images_div_1">	
			<div class="col-md-10 col-md-offset-1"><b>FIRST PAIR</b></div>
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic1">
					<img class="picture-src" id="edit_wizardPictureSmall1" title=""/>
		            <input type="file" name="edit_thumbnail1" id="edit_wizard-picture-small1" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["thumbnail"]?>" alt="Image Not Found" id="previous_thumbnail_view1" name="previous_thumbnail_view1" width="100"/>
					
					
					
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname1">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic1">
					<img class="picture-src" id="edit_wizardPictureBig1" title=""/>
		            <input type="file" name="edit_image1" id="edit_wizard-picture-big1" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["image"]?>" alt="Image Not Found" id="previous_image_view1" name="previous_image_view1" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname1">Big Image (420x512)</h6>
				</div>
				

				
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic1">
					<img class="picture-src" id="edit_wizardPictureLarge1" title=""/>
		            <input type="file" name="edit_largeimage1" id="edit_wizard-picture-large1" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["largeimage"]?>" alt="Image Not Found" id="previous_largeimage_view1" name="previous_largeimage_view1" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname1">Large Image (850x1036)</h6>
				</div>
				

				
			</div>
			
			</div>
			<div class="col-md-5"  id="inv_images_div_2">	
			<div class="col-md-10 col-md-offset-1"><b>SECOND PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(2)"></i></b></div>
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic2">
					<img class="picture-src" id="edit_wizardPictureSmall2" title=""/>
		            <input type="file" name="edit_thumbnail2" id="edit_wizard-picture-small2" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["thumbnail2"]?>" alt="Image Not Found" id="previous_thumbnail_view2" name="previous_thumbnail_view2" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname2">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic2">
					<img class="picture-src" id="edit_wizardPictureBig2" title=""/>
		            <input type="file" name="edit_image2" id="edit_wizard-picture-big2" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["image2"]?>" alt="Image Not Found" id="previous_image_view2" name="previous_image_view2" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname2">Big Image (420x512)</h6>
				</div>
			</div>
			
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic2">
					<img class="picture-src" id="edit_wizardPictureLarge2" title=""/>
		            <input type="file" name="edit_largeimage2" id="edit_wizard-picture-large2" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["largeimage2"]?>" alt="Image Not Found" id="previous_largeimage_view2" name="previous_largeimage_view2" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname2">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 col-md-offset-2" id="inv_images_div_3">
<div class="col-md-10 col-md-offset-1"><b>THIRD PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(3)"></i></b></div>			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic3">
					<img class="picture-src" id="edit_wizardPictureSmall3" title=""/>
		            <input type="file" name="edit_thumbnail3" id="edit_wizard-picture-small3" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["thumbnail3"]?>" alt="Image Not Found" id="previous_thumbnail_view3" name="previous_thumbnail_view3" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname3">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic3">
					<img class="picture-src" id="edit_wizardPictureBig3" title=""/>
		            <input type="file" name="edit_image3" id="edit_wizard-picture-big3" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["image3"]?>" alt="Image Not Found" id="previous_image_view3" name="previous_image_view3" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname3">Big Image (420x512)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic3">
					<img class="picture-src" id="edit_wizardPictureLarge3" title=""/>
		            <input type="file" name="edit_largeimage3" id="edit_wizard-picture-large3" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["largeimage3"]?>" alt="Image Not Found" id="previous_largeimage_view3" name="previous_largeimage_view3" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname3">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
			<div class="col-md-5" id="inv_images_div_4">	
			<div class="col-md-10 col-md-offset-1"><b>FOURTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(4)"></i></b></div>
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic4">
					<img class="picture-src" id="edit_wizardPictureSmall4" title=""/>
		            <input type="file" name="edit_thumbnail4" id="edit_wizard-picture-small4" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["thumbnail4"]?>" alt="Image Not Found" id="previous_thumbnail_view4" name="previous_thumbnail_view4" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname4">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic4">
					<img class="picture-src" id="edit_wizardPictureBig4" title=""/>
		            <input type="file" name="edit_image4" id="edit_wizard-picture-big4" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["image4"]?>" alt="Image Not Found" id="previous_image_view4" name="previous_image_view4" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname4">Big Image (420x512)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic4">
					<img class="picture-src" id="edit_wizardPictureLarge4" title=""/>
		            <input type="file" name="edit_largeimage4" id="edit_wizard-picture-large4" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["largeimage4"]?>" alt="Image Not Found" id="previous_largeimage_view4" name="previous_largeimage_view4" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname4">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 col-md-offset-2" id="inv_images_div_5">
<div class="col-md-10 col-md-offset-1"><b>FIFTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(5)"></i></b></div>			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic5">
					<img class="picture-src" id="edit_wizardPictureSmall5" title=""/>
		            <input type="file" name="edit_thumbnail5" id="edit_wizard-picture-small5" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["thumbnail5"]?>" alt="Image Not Found" id="previous_thumbnail_view5" name="previous_thumbnail_view5" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname5">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic5">
					<img class="picture-src" id="edit_wizardPictureBig5" title=""/>
		            <input type="file" name="edit_image5" id="edit_wizard-picture-big5" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["image5"]?>" alt="Image Not Found" id="previous_image_view5" name="previous_image_view5" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname5">Big Image (420x512)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic5">
					<img class="picture-src" id="edit_wizardPictureLarge5" title=""/>
		            <input type="file" name="edit_largeimage5" id="edit_wizard-picture-large5" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["largeimage5"]?>" alt="Image Not Found" id="previous_largeimage_view5" name="previous_largeimage_view5" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname5">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
			
			
			
			
			<div class="col-md-5" id="inv_images_div_6">
<div class="col-md-10 col-md-offset-1"><b>SIXTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(6)"></i></b></div>			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic6">
					<img class="picture-src" id="edit_wizardPictureSmall6" title=""/>
		            <input type="file" name="edit_thumbnail6" id="edit_wizard-picture-small6" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["thumbnail6"]?>" alt="Image Not Found" id="previous_thumbnail_view6" name="previous_thumbnail_view6" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname6">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic6">
					<img class="picture-src" id="edit_wizardPictureBig6" title=""/>
		            <input type="file" name="edit_image6" id="edit_wizard-picture-big6" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["image6"]?>" alt="Image Not Found" id="previous_image_view6" name="previous_image_view6" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname6">Big Image (420x512)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic6">
					<img class="picture-src" id="edit_wizardPictureLarge6" title=""/>
		            <input type="file" name="edit_largeimage6" id="edit_wizard-picture-large6" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventory_data_arr["largeimage6"]?>" alt="Image Not Found" id="previous_largeimage_view6" name="previous_largeimage_view6" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname6">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
			
			
			
			
		</div>
	</div>
	
	</div>
	<div class="wizard-footer">
		<div class="pull-right">
			<input type='button' class='btn btn-next btn-fill btn-success btn-wd' name='next' value='Next'  onclick="enterCurrentTabFun()"/>
			<input type='button' class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='Finish' id="edit_inventory_finish_btn" onClick="edit_filter_details_form_valid()"/>
		</div>
		<div class="pull-left">
			<input type='button' class='btn btn-previous btn-fill btn-default btn-wd' id="previous_button" name='previous' value='Previous' />
			<input type='button' class='btn btn-previouspage btn-fill btn-default btn-wd' id="back" value='Previous' />
		</div>
		<div class="clearfix">
		</div>
	</div>	
</form>

<?php
		if($inventory_data_arr["image"]==""){ $total_no_of_images=0;}
		else if($inventory_data_arr["image2"]==""){ $total_no_of_images=1;}
		else if($inventory_data_arr["image3"]==""){ $total_no_of_images=2;}
		else if($inventory_data_arr["image4"]==""){ $total_no_of_images=3;}
		else if($inventory_data_arr["image5"]==""){ $total_no_of_images=4;}
		else if($inventory_data_arr["image6"]==""){ $total_no_of_images=5;}
		else{$total_no_of_images=6;}
	?>	
	<script>
		function hide_inv_imagesFun(div_index_id){
			
			document.getElementById("previous_largeimage"+div_index_id).value="";
			document.getElementById("previous_image"+div_index_id).value="";
			document.getElementById("previous_thumbnail"+div_index_id).value="";
			
			document.getElementById("edit_wizard-picture-small"+div_index_id).value="";
			document.getElementById("edit_wizard-picture-big"+div_index_id).value="";
			document.getElementById("edit_wizard-picture-large"+div_index_id).value="";
			
			
			document.getElementById("edit_wizardPictureSmall"+div_index_id).removeAttribute("src");
			document.getElementById("edit_wizardPictureBig"+div_index_id).removeAttribute("src");
			document.getElementById("edit_wizardPictureLarge"+div_index_id).removeAttribute("src");
			
			document.getElementById("edit_wizardPictureSmall_picname"+div_index_id).innerHTML="Small Image (100x122)";
			document.getElementById("edit_wizardPictureBig_picname"+div_index_id).innerHTML="Big Image (420x512)";
			document.getElementById("edit_wizardPictureLarge_picname"+div_index_id).innerHTML="Large Image (850x1036)";
			
			document.getElementById("inv_images_div_"+div_index_id).style.display="none";
			enableDisabledAddInventoryButtonFun();
		}
		function enableDisabledAddInventoryButtonFun(){
			small_image_count=0;
			big_image_count=0;
			large_image_count=0;
			for(i=1;i<=5;i++){
				if(document.getElementById("inv_images_div_"+i).style.display==""){
					if(document.getElementById("edit_wizard-picture-small"+i).value!="" || document.getElementById("previous_thumbnail_view"+i).value!=""){
						small_image_count++;
					}
					if(document.getElementById("edit_wizard-picture-big"+i).value!="" || document.getElementById("previous_image_view"+i).value!=""){
						big_image_count++;
					}
					if(document.getElementById("edit_wizard-picture-large"+i).value!="" || document.getElementById("previous_largeimage_view"+i).value!=""){
						large_image_count++;
					}
				}
			}
			
			if(small_image_count==0 && big_image_count==0 && large_image_count==0){
				document.getElementById("add_image_inventory_btn").disabled=true;
			}
			else{
				if(small_image_count==big_image_count && big_image_count==large_image_count){
					document.getElementById("add_image_inventory_btn").disabled=false;
					document.getElementById("edit_inventory_finish_btn").disabled=false;
				}
				else{
					document.getElementById("add_image_inventory_btn").disabled=true;
					document.getElementById("edit_inventory_finish_btn").disabled=true;
					
				}
			}
		}
		function initializeInventoryImageDivFun(){
			for(i=<?php echo ($total_no_of_images+1);?>;i<=6;i++){
				document.getElementById("inv_images_div_"+i).style.display="none";
			}
		}
		function add_image_inventory_fun(){
			for(i=2;i<=6;i++){
				if(document.getElementById("inv_images_div_"+i).style.display=="none"){
					document.getElementById("inv_images_div_"+i).style.display="";
					document.getElementById("add_image_inventory_btn").disabled=true;
					break;
				}
			}
		}
		initializeInventoryImageDivFun();
		enableDisabledAddInventoryButtonFun();
		
	</script>
	
</div>
</div> <!-- wizard container -->
</div>
</div>
 
</div>
<div class="footer">
<form name="back_to_inventory" id="back_to_inventory" method="post">
		<input type="hidden" value="<?php echo $pcat_id; ?>" name="parent_category">
		<input type="hidden" value="<?php echo $cat_id; ?>" name="categories">
		<input type="hidden" value="<?php echo $subcat_id; ?>" name="subcategories">
		<input type="hidden" value="<?php echo $brand_id; ?>" name="brands">
		<input type="hidden" value="<?php echo $product_id; ?>" name="products">
		<input value="view" type="hidden" name="create_editview">
</form>
</div>
</div>
<?php		

//$highlight=str_replace("\n", '', addslashes($inventory_data_arr["highlight"]));

$highlight=$inventory_data_arr["highlight"];
$highlight1=str_replace("\n", '', $highlight);

$string = trim(preg_replace('/\s+/', ' ', $highlight1));

$highlight_faq=$inventory_data_arr["highlight_faq"];
$highlight_faq1=str_replace("\n", '', $highlight_faq);

$string1 = trim(preg_replace('/\s+/', ' ', $highlight_faq1));



$highlight_whatsinpack=$inventory_data_arr["highlight_whatsinpack"];
$highlight_whatsinpack1=str_replace("\n", '', $highlight_whatsinpack);

$string2 = trim(preg_replace('/\s+/', ' ', $highlight_whatsinpack1));


$highlight_aboutthebrand=$inventory_data_arr["highlight_aboutthebrand"];
$highlight_aboutthebrand1=str_replace("\n", '', $highlight_aboutthebrand);

$highlight_aboutthebrand_str = trim(preg_replace('/\s+/', ' ', $highlight_aboutthebrand1));

?>

 
<script  type="text/javascript" src="<?php echo base_url();?>assets/richte/editor.js" ></script>
<script>
function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}
$(document).ready(function() {
	
	var textarea = $("#highlight");
	textarea.Editor();
	str='<?php echo $string; ?>';
	data=decodeHtml(str);
	$('#highlight').siblings("div").children("div.Editor-editor").html(data);
	
	var textarea1 = $("#highlight_faq");
	textarea1.Editor();
	str='<?php echo $string1; ?>';
	data1=decodeHtml(str);
	$('#highlight_faq').siblings("div").children("div.Editor-editor").html(data1);
	
	
	var textarea2 = $("#highlight_whatsinpack");
	textarea2.Editor();
	str='<?php echo $string2; ?>';
	data2=decodeHtml(str);
	$('#highlight_whatsinpack').siblings("div").children("div.Editor-editor").html(data2);
	
	
	var textarea_aboutthebrand = $("#highlight_aboutthebrand");
	textarea_aboutthebrand.Editor();
	highlight_aboutthebrand_str='<?php echo $highlight_aboutthebrand_str; ?>';
	highlight_aboutthebrand_data=decodeHtml(highlight_aboutthebrand_str);
	$('#highlight_aboutthebrand').siblings("div").children("div.Editor-editor").html(highlight_aboutthebrand_data);
	
	


 });

 function enterCurrentTabFun(){
	$(".tab-content .tab-pane.active").each(function(){
		if($(this).attr("id")=="edit_step-21"){
			vendor_id=$("#edit_vendors").val();
			if($("#edit_logistics_parcel_category").val()!=null){
				logistics_parcel_category_in=$("#edit_logistics_parcel_category").val().join("-");
				
				$.ajax({
					url:"<?php echo base_url()?>admin/Catalogue/check_selection_in_all_parcel_categories",
					type:"post",
					data:"vendor_id="+vendor_id+"&logistics_parcel_category_in="+logistics_parcel_category_in,
					success:function(data){
						if(data=="no"){
							swal({
								title:"Info", 
								text:"Please select atleast one parcel category from logisitcs", 
								type: "info"
						}).then(function () {
							
						});
							$("#previous_button").click();
						}
						else{
							
						}
					}
				});
			}
	
		}
		if($(this).attr("id")=="edit_step-11"){
			add_to_cart=$('#edit_add_to_cart').is(':checked');
			request_for_quotation=$('#edit_request_for_quotation').is(':checked');
			if(add_to_cart===false && request_for_quotation===false){
				$("#buttonDisplayErrorMessage").html("Please select any one of an option");
				swal({
						title:"Error", 
						text:"Please select Either 'Add to Cart' or 'Request for Quotation' option", 
						type: "error",
						allowOutsideClick: false
					}).then(function () {
						$("#previous_button").click();
					});
			}else{
				$("#buttonDisplayErrorMessage").html("");
			}
		}
	});
 }
 
 function calculateSelling_priceDiscountFun(){
 
        edit_selling_price=$("#edit_selling_price").val();
        edit_max_selling_price=$("#edit_max_selling_price").val();
        
        if(edit_selling_price=="" || edit_max_selling_price==""){
            $("#edit_selling_discount").val("");
	}
        if(edit_selling_price!="" && edit_max_selling_price!=""){
            if(parseFloat(edit_max_selling_price)>=parseFloat(edit_selling_price)){
                wsp_dis=Math.round(((parseFloat(edit_max_selling_price)-parseFloat(edit_selling_price))/parseFloat(edit_max_selling_price))*100,2);
                $("#edit_selling_discount").val(wsp_dis);
            }else{
                swal({
                        title:"Error", 
                        text:"MRP should be greater or equal to WSP", 
                        type: "error",
                        allowOutsideClick: false
                }).then(function () {
                        db_val_dis=$("#db_edit_selling_discount").val(); 
                        db_val_price=$("#db_edit_selling_price").val(); 
                        db_val=$("#db_edit_max_selling_price").val();
                        
                        $("#edit_max_selling_price").val(db_val);
                        $("#edit_selling_price").val(db_val_price);
                        $("#edit_selling_discount").val(db_val_dis);
                        
                });
               
            }
            //alert(wsp_dis);
	}
 }
 function calculateTax_priceFun(){
	 
        tax=$("#edit_tax").val();
        msp=$("#edit_max_selling_price").val();
        selling_price=$("#edit_selling_price").val();
        
        if(selling_price!="" && tax!=""){
            
            //taxable_price=(parseFloat(msp)-parseFloat(tax_percent_price));
            taxable_price=(parseFloat(selling_price)/(1+parseFloat(tax)/100)).toFixed(2);
            tax_percent_price=(parseFloat(selling_price)-parseFloat(taxable_price)).toFixed(2);
            //alert(taxable_price);
            $("#tax_percent_price").val(tax_percent_price);
            $("#taxable_price").val(taxable_price);
            
            $("#tax_percent_price_div").removeAttr("class");
            $("#tax_percent_price_div").attr("class","form-group label-floating is-empty is-focused");
            $("#taxable_price_div").removeAttr("class");
            $("#taxable_price_div").attr("class","form-group label-floating is-empty is-focused");
        }
         /* calculate tax price and taxable vale */
 }
 function calculateSelling_priceFun(){
	 edit_base_price=$("#edit_base_price").val();
	 edit_tax=$("#edit_tax").val();
	 if(edit_base_price!="" && edit_tax!=""){
		edit_selling_price=Math.round(parseFloat(edit_base_price)+((edit_tax/100)*parseFloat(edit_base_price)));
		$("#edit_max_selling_price").val(edit_selling_price);
	 }
	 if(edit_base_price=="" || edit_tax==""){
		 $("#edit_max_selling_price").val("");
	 }
	 /*
	 if(edit_base_price=="" || edit_tax==""){
		 $("#edit_selling_price").val("");
	 }
	 if($("#edit_selling_price").val()!=""){
		 $("#edit_selling_price_div").removeAttr("class");
		 $("#edit_selling_price_div").attr("class","form-group label-floating is-empty is-focused");
	 }
	 else{
		 $("#edit_selling_price_div").removeAttr("class");
		 $("#edit_selling_price_div").attr("class","form-group label-floating is-empty");
	 }
	 */
	 
 }
 /*
 $(document).ready(function(){
 $(".tab-content .tab-pane").each(function(){
		if($(this).attr("id")=="edit_step-21"){
			$(this).addClass("active");
		}
		else{
			$(this).removeClass("active");
		}
 });
 
 $(".wizard-navigation ul li.edit_step").each(function(){
	 if($(this).attr("id")=="edit_step-21_tab"){
		 $(this).addClass("active");
	 }
	 else{
		  $(this).removeClass("active");
	 }
 });
 });
 */
 /*function toggle_highlight(){
	 $('#highlight_div').toggle();
 }*/

 </script>
 
 <script>
				function calculateTaxPercentFun(){
					var CGST_value=$("#CGST").val().trim();
					var IGST_value=$("#IGST").val().trim();
					var SGST_value=$("#SGST").val().trim();
					
					if(CGST_value==""){CGST_value=0;}
					if(IGST_value==""){IGST_value=0;}
					if(SGST_value==""){SGST_value=0;}
					
					$("#edit_tax").val(parseFloat(CGST_value)+parseFloat(IGST_value)+parseFloat(SGST_value));
					calculateSelling_priceDiscountFun();
					calculateTax_priceFun();
				}
			</script>
			<script>
				function deleteDownloadsLinkFun(delete_file_n_upload){
					
					$("#"+delete_file_n_upload).val(delete_file_n_upload);
					$("#"+delete_file_n_upload+"_span").css({"display":"none"});
				}
			</script>
			
<script>
$(document).ready(function (){
	$('#price_validity').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		weekStart: 0, 
		format: 'YYYY-MM-DD', 
		shortTime : true
	}).on('change', function(e, date){
		$('#price_validity').bootstrapMaterialDatePicker('setMinDate', date);
	});
});


function add_items(){
	var len = $('.item').length; 

let template = `<div class="item">
<div class="row">
<div class=' col-md-8 col-md-offset-1'>
	<label class="control-label">Content</label>
	<input type="text" class="form-control" name="position_text[`+(parseInt(len)+1)+`]" placeholder="" />
	</div>
</div>
<div class="row">
	<div class="col-md-8 col-md-offset-1"> 
		<div class="form-group">
		<div class="col-md-5 col-md-offset-1">Do you have ?</div>
		<div class="col-md-3">
			<label class="radio-inline"><input name="position_uhave[`+(parseInt(len)+1)+`]" value="yes" type="radio" >Yes</label>
		</div>
		<div class="col-md-3">
			<label class="radio-inline"><input name="position_uhave[`+(parseInt(len)+1)+`]" value="no" type="radio">No</label>
		</div>

	</div>
</div>

<div class="row">
	<div class="col-md-8 col-md-offset-1"> 
		<div class="form-group">
		<div class="col-md-5 col-md-offset-1">Do others have ?</div>
		<div class="col-md-3">
			<label class="radio-inline"><input name="position_others_have[`+(parseInt(len)+1)+`]" value="yes" type="radio" >Yes</label>
		</div>
		<div class="col-md-3">
			<label class="radio-inline"><input name="position_others_have[`+(parseInt(len)+1)+`]" value="no" type="radio">No</label>
		</div>

	</div>
</div>
<div class="col-md-8 col-md-offset-1"><button class="btn btn-sm btn-danger pull-right" class="remove" >Remove</a>
</div>
</div>`; 

$("#items").append(template);

}
$(document).ready(()=>{	
    $("body").on("click", ".remove", (e)=>{
        $(e.target).parents(".item").remove();
    })
});

function blockSpecialChar(e){
	var k;
	document.all ? k = e.keyCode : k = e.which;
	return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
} 

</script>		
</body>
</html>

