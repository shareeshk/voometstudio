<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.wizard-card .tab-content {
	 min-height: 400px;
	 padding:0px;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<script>

function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_delete_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
swal({
			title: 'Are you sure?',
			text: "Delete Logistic delivery mode",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {	
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/delete_logistics_delivery_mode_selected",
		type:"post",
		data:"selected_list="+selected_list,
		
		success:function(data){
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Given "+table+"(s) has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
</script>
<script>

$(document).ready(function(){
	<?php
		if($create_editview=="create"){
			?>
			$("#viewDiv1").css({"display":"none"});
			$("#createDiv2").css({"display":""});
			<?php
		}
		else if($create_editview=="view"){
			?>
			$("#createDiv2").css({"display":"none"});
			$("#viewDiv1").css({"display":""});
			<?php
		}
	?>
});
</script>
<script>

function changeViewPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/logistics_delivery_mode_filter';
}
</script>
<?php //print_r($shipping_charge);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="wizard-header text-center">
		<h5> Logistics Delivery mode 
		</h5>                 		          	
	</div>

<div class="row" id="viewDiv1">
<table id="myTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<th colspan="8">
			<button id="multiple_delete_button" class="btn btn-danger btn-xs" onclick="multilpe_delete_fun('Logistics Delivery Mode')">Delete</button>
			
			<button class="btn btn-primary btn-xs" type="button" onclick="changeViewPrevious()">Change View</button>
		</th>
	</tr>
	<tr>
		<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
		<th>Sl.No.</th>
		<th>Vendor</th>
		<th>Logistics</th>
		<th>Delivery Name</th>
		<th>Speed Duration (Days)</th>
		<th>Last Updated</th>
		<th>Action</th>
	</tr>
</thead>
<tbody>
<?php
$i=0;
foreach ($logistics_delivery_mode as $logistics_delivery_mode_value) { 
$i++; ?>

<tr>
<td><input type="checkbox" name="selected_list" value="<?php echo $logistics_delivery_mode_value->logistics_delivery_mode_id; ?>"></td>
<td><?php echo $i; ?></td>
<td>
	<?php 
		$get_vendors_data_arr=$this->Model_catalogue->get_vendors_data($logistics_delivery_mode_value -> vendor_id);
		echo $get_vendors_data_arr["name"];
	?>
</td>
<td><?php echo $logistics_delivery_mode_value -> logistics_name?></td>
<td><?php echo $logistics_delivery_mode_value -> delivery_mode?></td>
<td>
	<?php 
		$speed_territory_duration_arr=json_decode($logistics_delivery_mode_value -> speed_duration,true);
		foreach($speed_territory_duration_arr as $territory_name_id => $speed_noof_days){
			$get_logistics_all_territory_name_data_arr=$controller->get_logistics_all_territory_name_data($territory_name_id);
			if(trim($speed_noof_days)==""){
				$speed_noof_days="None";
			}
			echo $get_logistics_all_territory_name_data_arr["territory_name"]." = ".$speed_noof_days;
			echo "<br>";
		}
	?>
</td>

<td><?php echo $logistics_delivery_mode_value -> timestamp?></td>

<td>
	
	<form action="<?php echo base_url()?>admin/Catalogue/edit_logistics_delivery_mode_form" method="post">	<input type="hidden" value="<?php echo $logistics_delivery_mode_value -> logistics_delivery_mode_id;?>" name="logistics_delivery_mode_id"><input type="hidden" value="<?php echo $logistics_delivery_mode_value -> vendor_id;?>" name="vendor_id"><input type="hidden" value="<?php echo $logistics_delivery_mode_value -> logistics_id;?>" name="logistics_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>
</td>

</tr>
<?php } ?>
</tbody>
</table>
</div>

<div class="row" id="createDiv2" style="display:none;">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
			
	<form name="create_logistics_delivery_mode" id="create_logistics_delivery_mode" method="post" action="#" onsubmit="return form_validation();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				vendors: {
					required: true,
				},
				logistics: {
					required: true,
				},
				delivery_mode: {
					required: true,
				}
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Create Logistics Delivery Mode</h3>
		</div>	
		</div>	
			<div class="tab-content">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Vendor-</label>
					<select name="vendors" id="vendors" class="form-control" onchange="showAvailableLogistics(this)">
						<option value=""></option>
						<?php foreach ($vendors as $vendors_value) {  ?>
						<option value="<?php echo $vendors_value->vendor_id; ?>"><?php echo $vendors_value->name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Logistics Name-</label>
					<select name="logistics" id="logistics" class="form-control" onchange="showAvailableLogisticsTerritories(this)">
						<option value=""></option>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating" id="logistics_territories_div">
				</div>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Delivery Mode</label>
					<input id="delivery_mode" name="delivery_mode" type="text" class="form-control" />
				</div>
				</div>
			</div>
				
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group">
					<div class="col-md-6">Make Default Logistics Delivery Mode</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="default_logistics" id="default_logistics1" value="1" type="radio" checked="checked">View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="default_logistics" id="default_logistics2" value="0" type="radio" checked="checked">Hide</label>
					</div>
				
					</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-8 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit">Submit</button>
					<button class="btn btn-warning btn-sm" id="reset_create_parent_category" type="reset">Reset</button>
					<button class="btn btn-primary btn-xs" type="button" onclick="changeViewPrevious()">Change View</button>
					</div>
				</div>
			</div>

		</div>		
	</form>

</div>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>
</body>
</html>
<script>
$(document).ready(function(){
    $('#myTable').dataTable({ stateSave: true });
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
});
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
        return false;
    }
    return true;
}
function showAvailableLogisticsTerritories(obj){
	
		logistics_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_logistics_territories_for_delivery_modes",
				type:"post",
				data:"logistics_id="+logistics_id,
				success:function(data){
					
					if(data!=0){

						$("#logistics_territories_div").html(data);		
					}
					else{
						$("#logistics_territories_div").html('No Territories Found');	
						
					}
				}
			});
	
}
function showAvailableLogistics(obj){
	
		vendor_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_logistics",
				type:"post",
				data:"vendor_id="+vendor_id,
				success:function(data){
					
					if(data!=0){

						$("#logistics").html(data);		
					}
					else{
						$("#logistics").html('<option value="">--No Logistics--</option>');	
						
					}
				}
			});
	
}
function deleteLogisticsDeliveryModeFun(logistics_delivery_mode_id){
	if(confirm("Are you sure want to delete?")){
		location.href="<?php echo base_url(); ?>admin/Catalogue/delete_logistics_delivery_mode/"+logistics_delivery_mode_id;
	}
}
$('#result').hide();

function form_validation()
{
	var vendors = $('input[name="vendors"]').val();
	var logistics = $('input[name="logistics"]').val();
	var delivery_mode = $('input[name="delivery_mode"]').val();
		
	   var err = 0;
		if((vendors=='') || (logistics=='') || (delivery_mode=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#create_logistics_delivery_mode');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_logistics_delivery_mode/"?>',
				type: 'POST',
				data: $('#create_logistics_delivery_mode').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					document.getElementById("delivery_mode").value="";
					document.getElementById("delivery_mode").focus();
				});
			}  
				if(data=="yes")
				{
					 					  
					swal({
						title:"Success", 
						text:"Logistics delivery mode is successfully added!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				if(data=="no")
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
			});
}
}]);			
			}
	
	return false;
}

</script>
