<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	

<script>

function showDivPrevious(){
	
	var form = document.getElementById('search_for_subcategory');		
	
	form.action='<?php echo base_url()."admin/Catalogue/subcategory"; ?>';
	form.submit();

}
</script>
<?php
if($get_subcategory_data['menu_sort_order']=="0"){?>
	<script>
	$(document).ready(function () {
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	document.getElementById('show_available_common_sub').disabled = true;
	
	});
	</script>
	<?php
}
?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">

<div class="row" id="editDiv3">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="edit_subcategories" id="edit_subcategories" method="post" action="#" onsubmit="return form_validation_edit();" enctype="multipart/form-data">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_parent_category: {
					required: true,
				},
				edit_categories: {
					required: true,
				},
				edit_subcat_name: {
					required: true,
				},
				edit_subcat_comparision:{
					required: true,
				},
				edit_menu_sort_order: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Subcategory</h3>
		</div>	
		</div>
		<div class="tab-content">
		<input type="hidden" name="edit_subcat_id" id="edit_subcat_id" value="<?php echo $get_subcategory_data["subcat_id"];?>">

			<div class="row">
			
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Parent Category-</label>
					<select name="edit_parent_category" id="edit_parent_category" class="form-control" disabled>
						<option value=""></option>
						
						<?php foreach ($parent_catagories as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>"<?php echo ($parent_category_value->pcat_id==$pcat_id)? "selected":''; ?>><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<option value="0" <?php echo ($pcat_id==0)? "selected":''; ?>>--None--</option>
					</select>
				</div>
				</div>
			</div>
		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Category-</label>
					<select name="edit_categories" id="edit_categories" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($categories as $category_value) {  ?>
						<option value="<?php echo $category_value->cat_id; ?>"<?php echo ($category_value->cat_id==$cat_id)? "selected":''; ?>><?php echo $category_value->cat_name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Enter Sub Category Name</label>
					<input id="edit_subcat_name" name="edit_subcat_name" type="text" class="form-control" value="<?php echo $get_subcategory_data["subcat_name"];?>"/>
					<input id="edit_subcat_name_default" name="edit_subcat_name_default" type="hidden" class="form-control" value="<?php echo $get_subcategory_data["subcat_name"];?>"/>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Comparision</label>
					<select id="edit_subcat_comparision" name="edit_subcat_comparision" class="form-control">
						<option value="" <?php if($get_subcategory_data["subcat_comparision"]==""){echo "selected";}?>>-Select-</option>
						<option value="yes" <?php if($get_subcategory_data["subcat_comparision"]=="yes"){echo "selected";}?>>Yes</option>
						<option value="no" <?php if($get_subcategory_data["subcat_comparision"]=="no"){echo "selected";}?>>No</option>
					</select>
				</div>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Enter the Sort Order</label>
					
					<select name="edit_menu_sort_order" id="edit_menu_sort_order" class="form-control" onchange="show_view_validatn()">
					<option value="<?php echo $get_subcategory_data["menu_sort_order"];?>" selected><?php echo $get_subcategory_data["menu_sort_order"];?></option>
						<?php
							if($get_sort_order_options_for_sub_cat["key"]==""){
								if($get_subcategory_data["menu_sort_order"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php } ?>
								<option value="1">1</option>
								<?php
							}
							else{
								if($get_subcategory_data["menu_sort_order"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php
								}
								$value_arr=explode(",",$get_sort_order_options_for_sub_cat["value"]);
								foreach($value_arr as $k => $v){
									?>
										<option value="<?php echo $v;?>"><?php echo $v;?></option>
									<?php
									}
							}
						?>
					</select>
					<input id="edit_menu_sort_order_default" name="edit_menu_sort_order_default" type="hidden" value="<?php echo $get_subcategory_data["menu_sort_order"];?>" />	
				</div>
				</div>
				
			</div>			
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group label-floating">
			<label class="control-label">-Swap Options-</label>
			<select class="form-control" id="show_available_common_sub" onchange="changeEventHandler(event)">
			<option selected value=""></option>
			<?php
			
			foreach($subcategory_sort_order_arr as $subcategory_order_arr){
				
				?>
				
					<option value="<?php echo $subcategory_order_arr["subcat_id"];?>"><?php echo $subcategory_order_arr["subcat_name"]."(".$subcategory_order_arr["menu_sort_order"].")";?></option>
					<?php
			}
			?>
			
			</select>
			</div>
				</div>
			</div>		
					
			<div class="row">
				<div class="col-md-10"> 
					<div class="form-group">
					<div class="col-md-5 col-md-offset-1">Show Sub Category</div>
					<div class="col-md-3">
						<label class="radio-inline"><input id="edit_view1" name="edit_view" value="1" type="radio" <?php if($get_subcategory_data["view"]=="1"){echo "checked";}?>>View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input id="edit_view2" name="edit_view" value="0" type="radio" <?php if($get_subcategory_data["view"]=="0"){echo "checked";}?>>Hide</label>
					</div>
				
					</div>
				</div>
			</div>			
													
			<div class="row">
				<div class="col-md-10 col-md-offset-3"> 
					<button class="btn btn-info btn-sm" type="submit" id="submit-data" disabled="">Submit</button>
		
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
				</div>
			</div>	
		</div>		
	</form>
	<form name="search_for_subcategory" id="search_for_subcategory" method="post">
		<input value="<?php echo $get_subcategory_data["pcat_id"]; ?>" type="hidden" id="pcat_id" name="pcat_id"/>
		<input value="<?php echo $get_subcategory_data["cat_id"]; ?>" type="hidden" id="cat_id" name="cat_id"/>
		<input value="view" type="hidden" name="create_editview">
	</form>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>

<script>
function changeEventHandler(event){
        parent_id=event.target.value;
		if(parent_id!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		}
		if(parent_id==""){
	edit_menu_sort_order=document.getElementById("edit_menu_sort_order").value;
	if(edit_menu_sort_order==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;					
	}
	if(edit_menu_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
		}
}

function show_view_validatn(){
	edit_menu_sort_order=document.getElementById("edit_menu_sort_order").value;
	show_available_common_sub=document.getElementById("show_available_common_sub").value;
	edit_menu_sort_order_default=document.getElementById("edit_menu_sort_order_default").value;
	if(show_available_common_sub==""){
	if(edit_menu_sort_order==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	if(edit_menu_sort_order_default==0){
	document.getElementById('show_available_common_sub').disabled = true;
	}
	if(edit_menu_sort_order_default!=0){
	document.getElementById('show_available_common_sub').disabled = false;
	}
	}
	if(edit_menu_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		//document.getElementById('show_available_common_sub').disabled = false;
	}
	}
	if(show_available_common_sub!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
	
}
</script>
<script>

function showAvailableCategoriesEdit(obj)
{
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#edit_categories").html(data);
					}
					else{
						$("#edit_categories").html('<option value=""></option>')
						$("#brands").html('<option value=""></option>')

					}
				}
			});
	}
}

$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_subcategories'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_subcategories"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_subcategories'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});

});
function form_validation_edit()
{
	var subcat_id = $('input[name="edit_subcat_id"]').val().trim();
	var subcat_name = $('input[name="edit_subcat_name"]').val().trim();
	var parent_category = $('select[name="edit_parent_category"]').val().trim();
	var categories = $('select[name="edit_categories"]').val().trim();
	var menu_sort_order = $('select[name="edit_menu_sort_order"]').val().trim();
	var view = document.querySelector('input[name="edit_view"]:checked').value;
	var menu_sort_order_default = $('input[name="edit_menu_sort_order_default"]').val().trim();
	
	var common_subcat=document.getElementById("show_available_common_sub").value;
			if(common_subcat!=""){
				
				
			}
		var err = 0;
		if(!(subcat_name.length>=3) || (parent_category=='') || (categories=='') || (menu_sort_order=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		
		if(err==0)
			{
				document.getElementById("edit_parent_category").disabled="";
				document.getElementById("edit_categories").disabled="";
				
		$("#pcat_id_reload").val(parent_category);
		$("#cat_id_reload").val(categories);
		$("#subcat_id_reload").val(subcat_id);		
		var form_status = $('<div class="form_status"></div>');		
		var form = $('#edit_subcategories');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();	
				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_subcategory/"?>',
				type: 'POST',
				data: $('#edit_subcategories').serialize()+"&common_subcat="+common_subcat,
				dataType: 'html',
				
			}).done(function(data)
			  {
				form_status.html('') 
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					
					edit_subcat_name_default=document.getElementById("edit_subcat_name_default").value;
					document.getElementById("edit_subcat_name").value=edit_subcat_name_default;
					document.getElementById("edit_subcat_name").focus();
					document.getElementById("edit_parent_category").disabled=true;
					document.getElementById("edit_categories").disabled=true;
				});
				  }
				if(data==1){
					swal({
						title:"Success", 
						text:"Subcategory is successfully updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						//location.reload();
						$("#reload_form")[0].submit();

					});
				}if(data==0){
					
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
			});
			}
}]);
			}
	
	return false;
}

</script>
<form action="<?php echo base_url()?>admin/Catalogue/edit_subcategory_form" method="post" id="reload_form">
	<input type="hidden" name="pcat_id" id="pcat_id_reload" value="<?php echo $get_subcategory_data["pcat_id"];?>">
	<input type="hidden" name="cat_id" id="cat_id_reload" value="<?php echo $get_subcategory_data["cat_id"];?>">
	<input type="hidden" name="subcat_id" id="subcat_id_reload" value="<?php echo $get_subcategory_data["subcat_id"];?>">
</form>
</div>
</body>
</html>
