<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/bootstrap-colorpicker.min.css" rel="stylesheet">


<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.input-group .input-group-addon {
	background-color:#ccc;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo base_url();?>assets/js/color_classifier.js"></script>
<script>
function show_view_validatn_create(){
	menu_sort_order=document.getElementById("sort_order").value;
	if(menu_sort_order==0){
	document.getElementById("view1").disabled = true;
	document.getElementById("view2").checked = true;					
	}
	if(menu_sort_order!=0){
		document.getElementById("view1").disabled = false;
		document.getElementById("view1").checked = true;					
	}
}
</script>

<script type="text/javascript">
var table;

$(document).ready(function (){
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    var rows_selected = [];
    table = $('#table_filter').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Catalogue/filter_processing",
			data: function (d) { d.pcat_id = $('#pcat_id').val();d.cat_id = $('#cat_id').val();d.subcat_id = $('#subcat_id').val();d.filterbox_id = $('#filterbox_id').val();d.active=1},
            type: "post",
            error: function(){
              $("#table_filter_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				
				document.getElementById("filter_count").innerHTML=json.recordsFiltered;
				
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
   
	$('#reset_form_button').on('click',function(){
		table.draw();
	});
	
	$("#submit_button").click(function() {
		table.draw();
	});
	$('#search_toggle').on('click',function(){	
		$('#search_block').toggle();
	});

});	


function drawtable(obj){
	table.draw();
	
}
function showSortOrderFilter(obj){
	if(obj.value!=""){
	filterbox_id=obj.value;
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/show_filter_sort_order",
		type:"post",
		data:"filterbox_id="+filterbox_id,
		
		success:function(data){
			$("#sort_order").html(data);
		}
	});
	}else{
		$("#sort_order").html("");
	}
}

function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_delete_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
swal({
			title: 'Are you sure?',
			text: "Archived Filter(entire tree)",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Archive it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/update_filter_selected",
		type:"post",
		data:"selected_list="+selected_list+"&active=0",
		
		success:function(data){
			if(data==true){
				swal({
						title:"Archived!", 
						text:"Given "+table+"(s) has been archived successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
function multilpe_delete_when_no_data(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
swal({
			title: 'Are you sure?',
			text: "Delete Filter",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/delete_filter_when_no_data",
		type:"post",
		data:"selected_list="+selected_list,
		
		success:function(data){
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Given "+table+"(s) has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else if(data==false){
				swal("Error", "Error in deleting this file", "error");
			}else{
				swal("Error", "Oops ..! You can't delete it.", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
</script>
<script>

$(document).ready(function(){
	<?php
		if($create_editview=="create"){
			?>
			$("#viewDiv1").css({"display":"none"});
			$("#createDiv2").css({"display":""});
			<?php
		}
		else if($create_editview=="view"){
			?>
			$("#createDiv2").css({"display":"none"});
			$("#viewDiv1").css({"display":""});
			<?php
		}
	?>
});
</script>
<script>

function changeViewPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/filter_filter';
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="row" id="viewDiv1" style="display:none;">
	<div class="wizard-header text-center">
		<h5> Number of Filter <span class="badge"><div id="filter_count"></div></span> 
		</h5>                 		          	
	</div>
	<input value="<?php echo $pcat_id; ?>" type="hidden" id="pcat_id">
	<input value="<?php echo $cat_id; ?>" type="hidden" id="cat_id">
	<input value="<?php echo $subcat_id; ?>" type="hidden" id="subcat_id">
	<input value="<?php echo $filterbox_id; ?>" type="hidden" id="filterbox_id">
	
<table id="table_filter" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<th colspan="8">
			<button id="multiple_delete_button1" class="btn btn-warning btn-xs" onclick="multilpe_delete_fun('filter')">Archive all</button>
			<button id="multiple_delete_button2" class="btn btn-danger btn-xs" onclick="multilpe_delete_when_no_data('filter')">Delete</button>
			<button class="btn btn-primary btn-xs" type="button" onclick="changeViewPrevious()">Change View</button>
		</th>
	</tr>
	<tr>
		<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
		<th>Filter Name</th>
		<th>Filter Box Name</th>	
		<th>Filter to Catalog</th>
		<th>Sort Order</th>
		<th>View</th>
		<th>Last Updated</th>
		<th>Action</th>		
	</tr>
</thead>

</table>
</div>

<div class="row-fluid" id="createDiv2" style="display:none;">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">

	<form name="create_filter" id="create_filter" method="post" onsubmit="return form_validation();">
	<input type="hidden" name="sort_order_flag" id="sort_order_flag">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				parent_category: {
					required: true,
				},
				
				sort_order: {
					required: true,
				},
				filterbox: {
					required: true,
				},
				
				filter_options: {
					required: true,
				},
				filterbox_type: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Create Filter</h3>
		</div>	
		</div>
		<div class="tab-content">
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Parent category-</label>
					<select name="parent_category" id="parent_category" class="form-control align_top" onchange="showAvailableCategories(this)">
						<option value=""></option>
						<?php foreach ($parent_category as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>"><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<option value="0">--None--</option>
						
					</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group label-floating">
					<label class="control-label">-Select Category-</label>
					<select name="categories" id="categories" class="form-control align_top" onchange="showAvailableSubCategories(this)">
						<option value=""></option>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Sub Category-</label>
					<select name="subcategories" id="subcategories" class="form-control align_top" onchange="showAvailableFilterbox(this)">
						<option value=""></option>
					</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group label-floating">
					<label class="control-label">-Select Filterbox -</label>
					<select name="filterbox" id="filterbox" class="form-control align_top" onchange="check_color_attribute(this.options[this.selectedIndex].getAttribute('filterboxname'));showSortOrderFilter(this);">
						<option value=""></option>
						
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">Enter Filter Name</label>
					<div class="add_color_picker"></div>
						
					<input id="filter_options" name="filter_options" type="text" class="form-control" />
				</div>
				</div>
				
				
			</div>	
			<div class="row">
				
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Sort Order Value</label>
					
					<select name="sort_order" id="sort_order" class="form-control" onchange="show_view_validatn_create()">
						
					</select>
				</div>
				</div>
			</div>
			
					<input type="hidden" id="selected_filterbox" name="selected_filterbox" value="">
<script type="text/javascript">

function check_color_attribute(val){
	$('#selected_filterbox').val(val);
	if(val.toLowerCase()=="color"){
		$('.add_color_picker').html('');
		$('.add_color_picker').append('<div class="row"><div class="col-md-4">Pick color: <div class="input-group colorpicker-component" id="cp"><input id="color_value" name="color_value" value="" class="form-control" type="text"><span class="input-group-addon"><i></i></span></div></div><div class="col-md-4">Type color name: <input id="color_name" name="color_name" value="" class="form-control" type="text"></div><div class="col-md-4">Sample color name : <input name="color_name_sample" id="color_name_sample" value="" class="form-control sample_name" type="text"></div></div>');
	
		$('#filter_options').hide();
		
		callll();

	}else{
		$('#filter_options').show();
		$('.add_color_picker').html('');
	}
}

function callll(){
	
	$('#cp').colorpicker({
			format: "hex",
			colorSelectors: {
                'black': '#000000',
                'white': '#ffffff',
                'red': '#FF0000',
                'default': '#777777',
                'primary': '#337ab7',
                'success': '#5cb85c',
                'info': '#5bc0de',
                'warning': '#f0ad4e',
                'danger': '#d9534f'
            }
		});
		
	url='<?php echo base_url() ?>assets/js/dataset.js';
	
	$('#cp').colorpicker().on('changeColor', function (e) {
				window.classifier = new ColorClassifier();
				get_dataset(url, function (data){
					window.classifier.learn(data);			
					color_value=$('#color_value').val();	
					var result_name = window.classifier.classify(color_value);
					$('#color_name_sample').val(result_name);

				});
	});
	
}
function edit_callll(){
	
	$('#edit_cp').colorpicker({
			format: "hex",
			colorSelectors: {
                'black': '#000000',
                'white': '#ffffff',
                'red': '#FF0000',
                'default': '#777777',
                'primary': '#337ab7',
                'success': '#5cb85c',
                'info': '#5bc0de',
                'warning': '#f0ad4e',
                'danger': '#d9534f'
            }
		});
		
	url='<?php echo base_url() ?>assets/js/dataset.js';
	
	$('#edit_cp').colorpicker().on('changeColor', function (e) {
				window.classifier = new ColorClassifier();
				get_dataset(url, function (data){
					window.classifier.learn(data);			
					color_value=$('#edit_color_value').val();	
					var result_name = window.classifier.classify(color_value);
					$('#edit_color_name_sample').val(result_name);

				});
	});
	window.classifier = new ColorClassifier();
				get_dataset(url, function (data){
					window.classifier.learn(data);			
					color_value=$('#edit_color_value').val();	
					var result_name = window.classifier.classify(color_value);
					$('#edit_color_name_sample').val(result_name);

				});
}
</script>
			<div class="row">
				<div class="col-md-6 col-md-offset-3"> 
					<div class="form-group">
					<div class="col-md-6">Show Filter</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="view" id="view1" value="1" type="radio" checked="checked">View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="view" id="view2" value="0" type="radio">Hide</label>
					</div>
				
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-4"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit_filter">Submit</button>
					<button class="btn btn-warning btn-sm" type="reset">Reset</button>
					<button class="btn btn-primary btn-sm" type="button" onclick="changeViewPrevious()">Change View</button>
					</div>
				</div>
			</div>
								

		</div>		
	</form>
</div>				
</div>
</div>				
</div>
</div>
<div class="footer">
</div>

<script type="text/javascript">

function showAvailableFilterbox(obj){
	pcat_id=$("#parent_category").val();
	cat_id=$("#categories").val();
	//if(obj.value!="" && obj.value!="None"){
		subcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_filterbox",
				type:"post",
				data:"pcat_id="+pcat_id+"&cat_id="+cat_id+"&subcat_id="+subcat_id+"&active=1"+"&flagtype=3",
				success:function(data){
					if(data!=0){
						$("#filterbox").html(data);
						$("#sort_order").html("");
						$("#edit_filterbox").html(data);
					}
					else{
						$("#filterbox").html('<option value="0"></option>');
						$("#sort_order").html("");
						$("#edit_filterbox").html('<option value="0"></option>');

					}
					
				}
			});
	//}
	
}

function showAvailableSubCategories(obj){
	pcat_id=$("#parent_category").val();
	//if(obj.value!="" && obj.value!="None"){
		cat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#subcategories").html(data);
						$("#sort_order").html("");
					}
					else{
						$("#subcategories").html('<option value="0"></option>')
						$("#sort_order").html("");
						$("#brands").html('<option value="0"></option>')

					}
				}
		});
		
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_filterbox",
				type:"post",
				data:"pcat_id="+pcat_id+"&cat_id="+cat_id+"&active=1"+"&flagtype=2",
				success:function(data){
					if(data!=0){
						$("#filterbox").html(data);
						table.draw();
					}else{
						$("#filterbox").html('<option value=""></option>');
					}
				}
		});
	//}
	
}

function showAvailableCategories(obj){
	//if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#categories").html(data);
						$("#sort_order").html("");
					}
					else{
						$("#categories").html('<option value="0"></option>')
						$("#sort_order").html("");
						$("#brands").html('<option value="0"></option>')

					}
				}
			});
			$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_filterbox",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1"+"&flagtype=1",
				success:function(data){
					if(data!=0){
						$("#filterbox").html(data);
						table.draw();
					}else{
						$("#filterbox").html('<option value=""></option>');
					}
				}
			});
	//}
	
}

function form_validation(){
	
	str='';

	filterboxname=$('#selected_filterbox').val();
	
	if(filterboxname.toLowerCase()=="color"){
		color_name=$('#color_name').val();
		color_value=$('#color_value').val();
						
		if(color_value!='' && color_name!=''){
			str=color_name+':'+color_value;	
		}else{
			alert('Please fill all fields');
			str='';
		}
		$('#filter_options').val(str);
	}

	var filter_options = $('input[name="filter_options"]').val().trim();
	var filterbox = $('select[name="filterbox"]').val().trim();
	var parent_category = $('select[name="parent_category"]').val().trim();
	var categories = $('select[name="categories"]').val().trim();
	var subcategories = $('select[name="subcategories"]').val().trim();
	var sort_order = $('select[name="sort_order"]').val().trim();
	var view = document.querySelector('input[name="view"]:checked').value;

	
		var err = 0;
		
		if(filter_options=="")
		{
		   //$('input[name="filter_options"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('input[name="filter_options"]').css({"border": "1px solid #ccc"});
		}
		if(filterbox=="")
		{
		  //$('select[name="filterbox"]').css("border", "1px solid red");
		   err = 1;
		}else{
			//$('select[name="filterbox"]').css({"border": "1px solid #ccc"});
		}
		
		if(parent_category=='')
		{
		   //$('select[name="parent_category"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('select[name="parent_category"]').css({"border": "1px solid #ccc"});
		}
		
		if(filterbox=='' || filterbox==0){
			
			if(categories==''){
			   //$('select[name="categories"]').css("border", "1px solid red");	   
			   err = 1;
			}
			else{			
				//$('select[name="categories"]').css({"border": "1px solid #ccc"});
			}
			if(subcategories==''){
			   //$('select[name="subcategories"]').css("border", "1px solid red");	   
			   err = 1;
			}else{			
				//$('select[name="subcategories"]').css({"border": "1px solid #ccc"});
			}
		}else{
			//$('select[name="categories"]').css({"border": "1px solid #ccc"});
			//$('select[name="subcategories"]').css({"border": "1px solid #ccc"});
		}
		if(sort_order==''){
		  // $('input[name="sort_order"]').css("border", "1px solid red");	   
		   err = 1;
		}
		
		else{			
			//$('input[name="sort_order"]').css({"border": "1px solid #ccc"});
		}
		
		if(err==1)
		{
			   $('#error_result_id').show();
			   $('#result').hide();
		}
		else
			{	
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#create_filter');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();				
			
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_filter/"?>',
				type: 'POST',
				data: $('#create_filter').serialize(),
				dataType: 'html',
				success:function(data){
					form_status.html('')
				if(data=="exist"){
					swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
					}).then(function(){
						document.getElementById("filter_options").value="";
						document.getElementById("filter_options").focus();
					});
				}
					if(data=="yes")
					{
						
						swal({
							title:"Success", 
							text:"Filter is successfully added!", 
							type: "success",
							allowOutsideClick: false
						}).then(function () {
							location.reload();

						});
					}
					if(data=="no")
					{
						swal(
							'Oops...',
							'Error in form',
							'error'
						)
					}
					
				}
				});
}
}]);
			}
	return false;
}


</script>
</div>
</body>

</html>