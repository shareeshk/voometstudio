<html lang="en">
<head>

<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/bootstrap-colorpicker.min.css" rel="stylesheet">
	
<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.input-group .input-group-addon {
	background-color:#ccc;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo base_url();?>assets/js/color_classifier.js"></script>

<script>
	function showPicker_edit(elem){
		if(elem.value ==0){
      document.getElementById('colorpicker_edit').style.display = "none";
	  document.getElementById('otherattribute_edit').style.display = "none";
   }
	if(elem.value == 1){
      document.getElementById('colorpicker_edit').style.display = "block";
	  document.getElementById('otherattribute_edit').style.display = "none";
   }	
   if(elem.value == 2){
      document.getElementById('otherattribute_edit').style.display = "block";
	  document.getElementById('colorpicker_edit').style.display = "none";
   }
}
</script>
<script>

function showDivPrevious(){
	
	var form = document.getElementById('search_for_brand');		
	
	form.action='<?php echo base_url()."admin/Catalogue/attribute"; ?>';
	form.submit();
			
	 //window.location.href = '<?php echo base_url(); ?>admin/Catalogue/category';
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<?php 
	//print_r($attributes);
?>
<div class="container-fluid">

<div class="row" id="editDiv3">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
		
	<span id="error_edit" style="display:none;color:red;"> Please check the category. This might be already added</span>
				
	<form name="edit_attribute" id="edit_attribute" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_parent_category: {
					required: true,
				},
				edit_categories: {
					required: true,
				},
				edit_subcategories: {
					required: true,
				},
				edit_select_color: {
					required: true,
				},
				edit_attribute1_name: {
					required: true,
				},
				'edit_color_value[]': {
					required: true,
				},
				'edit_color_name[]': {
					required: true,
				},
				'edit_attribute1_name1': {
					required: true,
				},
				'edit_attribute1_option1': {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Attribute</h3>
		</div>	
		</div>			
		<input type="hidden" name="edit_attribute_id" id="edit_attribute_id" value="<?php echo $attr_arr["attribute_id"];?>">
		<div class="tab-content">
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Parent category-</label>
					<select name="edit_parent_category" id="edit_parent_category" class="form-control" onchange="showAvailableCategoriesEdit(this)" disabled>
						<option value=""></option>
						<?php foreach ($parent_catagories as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>" <?php echo ($parent_category_value->pcat_id==$pcat_id)? "selected":''; ?>><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } if(empty($parent_catagories)){
							?>
							<option value="0" selected>--None--</option>
							<?php 
						}
						 if($pcat_id==0){
							?>
							<option value="0" selected>--None--</option>
							<?php 
						}
						else{?>
						<option value="0">--None--</option>
						<?php }
						?>
					</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">-Select Category-</label>
					<select name="edit_categories" id="edit_categories" class="form-control" onchange="showAvailableSubCategoriesEdit(this)" disabled >
						<option value=""></option>
						<?php foreach ($categories as $category_value) {  ?>
						<option value="<?php echo $category_value->cat_id; ?>"<?php echo ($category_value->cat_id==$cat_id)? "selected":''; ?>><?php echo $category_value->cat_name; ?></option>
						<?php } ?>
						<option value="0">--None--</option>
					</select>
				</div>
				</div>
			</div>	
			<div class="row">
			
			<?php 
			//print_r($attr_arr);
			
			?>
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Sub Category-</label>
					<select name="edit_subcategories" id="edit_subcategories" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($subcategories as $subcategory_value) {  ?>
						<option value="<?php echo $subcategory_value->subcat_id; ?>" <?php echo ($subcategory_value->subcat_id==$subcat_id)? "selected":''; ?>><?php echo $subcategory_value->subcat_name; ?></option>
						<?php } ?>
						<option value="0">--None--</option>
					</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">-Select option for an Attribute1-</label>
					<select name="edit_select_color" id="edit_select_color" class="form-control" onchange="showPicker_edit(this)">
						<option value="0"></option>
						<option value="1" <?php echo ($attr_arr['attribute1_name']=="Color") ? "selected" :'vvvv' ;?>>Color Attribute</option>
						<option value="2">others Attribute</option>
					</select>
				</div>
				</div>
			</div>

				
					
					
					
					
					
	<div class="row" style="display:none;" id="colorpicker_edit">
		
		<div class="col-md-8 col-md-offset-1">                               
			<div class="form-group">
			<label class="control-label">Enter Attribute Name (1)</label>
				<input id="edit_attribute1_name" name="edit_attribute1_name" type="text" class="form-control" placeholder="Enter Attribute Name (1)" value="Color">
			</div>
		</div>
		<div class="col-md-2">                               
				<div class="form-group">
				<button id="edit_add_one_more" type="button" name="edit_add_one_more" class="btn btn-info edit_add_one_more pull-right"><i class="fa fa-plus" aria-hidden="true"></i></button>
		</div>
		</div>
	
		<div class="edit-multi-fields col-md-offset-1">
					
			<div class="add_range col-md-11">
				
				<div class="col-md-4">
				Pick color: 
					<div class="edit-color-picker input-group colorpicker-component">
						<input name="edit_color_value[]" id="edit_color_value[]" type="text" value=""  class="form-control" />
						<span class="input-group-addon"><i></i></span>
						
					</div>
				</div>

				<div class="col-md-3">
					Type color name: 
					<input type="text" id="edit_color_name[]" name="edit_color_name[]" value=""  class="form-control" />
				</div>
				
				<div class="col-md-4">
					Sample color name : <input type="text" id="edit_color_name_sample[]" name="edit_color_name_sample[]"  value=""  class="form-control edit_sample_name" />
				</div>
				
			
			</div>
			
			
		
		</div>
	
		<textarea id="edit_attribute1_option" name="edit_attribute1_option" style="display:none" class="form-control" placeholder="Enter Attribute Options (1) Seperated by Comma(,)" ></textarea>
		
	</div>
	
<script type="text/javascript">

function edit_call_this(add_btn=''){
	$('.edit-color-picker').each(function(i) {
			
		$(this).removeAttr('id');
		
		$(this).attr('id', 'edit_cp' + i);
		$('#edit_cp'+i).colorpicker({
			format: "hex",
			colorSelectors: {
                'black': '#000000',
                'white': '#ffffff',
                'red': '#FF0000',
                'default': '#777777',
                'primary': '#337ab7',
                'success': '#5cb85c',
                'info': '#5bc0de',
                'warning': '#f0ad4e',
                'danger': '#d9534f'
            }
		});
		if(add_btn==''){
			edit_call_function(i);//onload
		}
		$('#edit_cp'+i).colorpicker().on('changeColor', function (e) {
			edit_call_function(i);
			
		});
	});
	
	$('.edit_sample_name').each(function(i) {
		$(this).removeAttr('id');
		$(this).attr('id', 'edit_sample_name_' + i);
		
	});
}


$(function() {
	var $wrapper_edit = $('.edit-multi-fields', this);
	
    $(".edit_add_one_more", $(this)).click(function(e) {
		//alert('ddd');
		//return false;
        //$('.add_range:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('');
		var edit_color_value = $('input[name="edit_color_value[]"]').val();
		var edit_color_name = $('input[name="edit_color_name[]"]').val();

		if(edit_color_value==""){
		   //$('input[name="edit_color_value[]"]').css("border", "1px solid red");	   
		  	
		}else if(edit_color_name==""){
			//$('input[name="edit_color_value[]"]').css({"border": "1px solid #ccc"});
		  //$('input[name="edit_color_name[]"]').css("border", "1px solid red");	
		}else{			
		
			//$('input[name="edit_color_name[]"]').css({"border": "1px solid #ccc"});
			
		$wrapper_edit.append('<div class="edit_add_range col-md-11"><div class="col-md-4">Pick color: <div class="edit-color-picker input-group colorpicker-component"><div class="form-group label-floating"><input name="edit_color_value[]" id="edit_color_value[]" value="" class="form-control" type="text"></div><span class="input-group-addon"></span></div></div><div class="col-md-3">Type color name: <div class="form-group label-floating"><input id="edit_color_name[]" name="edit_color_name[]" value="" class="form-control" type="text"/></div></div><div class="col-md-3">Sample color name :<div class="form-group label-floating"> <input name="edit_color_name_sample[]" id="edit_color_name_sample[]"  class="form-control edit_sample_name" type="text" value=""/></div></div><div class="col-md-1 align_top"><button class="btn btn-danger btn-sm edit_remove_range"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>');
		
		$('.edit_remove_range').show();	
		edit_call_this('add_btn');
		return false;
		}
    });
	
	$($wrapper_edit).on("click",".edit_remove_range", function(e){ 
	elements_length=$('.edit_add_range', $wrapper_edit).length;
	e.preventDefault(); 
		if(elements_length==1){
			$('.edit_remove_range').hide();
			$(this).parent('div').parent('div').remove();	
		}else{
		$('.edit_remove_range').show();
		$(this).parent('div').parent('div').remove();	
		
		}
        
    })
	
 });

//var button = $('#submit-data');
//button.prop('', disable);
		
function edit_call_function(j){

window.classifier = new ColorClassifier();
url='<?php echo base_url() ?>assets/js/dataset.js';
get_dataset(url, function (data){
    window.classifier.learn(data);
	edit_color_value=document.getElementsByName('edit_color_value[]')[j].value;
	//alert(edit_color_value);
	var edit_result_name = window.classifier.classify(edit_color_value);
	document.getElementsByName('edit_color_name_sample[]')[j].value=edit_result_name;
});

}
</script>
				<div class="row" style="display:none;" id="otherattribute_edit">
					<div class="col-md-5 col-md-offset-1">   
					<div class="form-group">
						<label class="control-label">Enter Attribute Name (1)</label>
						<input id="edit_attribute1_name1" name="edit_attribute1_name1" type="text" class="form-control" />
					</div>
					</div>
					
					<div class="col-md-5">   
					<div class="form-group">
						<label class="control-label">Enter Attribute Options (1) Seperated by Comma(,)</label>
						<textarea id="edit_attribute1_option1" name="edit_attribute1_option1" type="text" class="form-control" ></textarea>
					</div>
					</div>
					
				</div>	
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Enter Attribute Name (2)</label>
					<input id="edit_attribute2_name" name="edit_attribute2_name" type="text" class="form-control" />
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">Enter Attribute Options (2) Seperated by Comma(,)</label>
					<textarea id="edit_attribute2_option" name="edit_attribute2_option" type="text" class="form-control" ></textarea>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Enter Attribute Name (3)</label>
					<input id="edit_attribute3_name" name="edit_attribute3_name" type="text" class="form-control" />
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">Enter Attribute Options (3) Seperated by Comma(,) </label>
					<textarea id="edit_attribute3_option" name="edit_attribute3_option" type="text" class="form-control" ></textarea>
				</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Enter Attribute Name (4)</label>
					<input id="edit_attribute4_name" name="edit_attribute4_name" type="text" class="form-control" />
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">Enter Attribute Options (4) Seperated by Comma(,)</label>
					<textarea id="edit_attribute4_option" name="edit_attribute4_option" type="text" class="form-control" ></textarea>
				</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10 col-md-offset-3"> 
					<div class="form-group">
					<div class="col-md-3">Show Attribute</div>
					<div class="col-md-2">
						<label class="radio-inline"><input name="edit_view" id="edit_view1" value="1" type="radio" <?php if($attr_arr["view"]=="1"){echo "checked";}?>>View</label>
					</div>
					<div class="col-md-2">
						<label class="radio-inline"><input name="edit_view" id="edit_view2" value="0" type="radio"<?php if($attr_arr["view"]=="0"){echo "checked";}?>>Hide</label>
					</div>
				
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-5"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit-data" disabled="">Submit</button>
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
					
					</div>
				</div>
			</div>	
				
		</div>		
				
	</form>
	<form name="search_for_brand" id="search_for_brand" method="post">
		<input value="<?php echo $pcat_id; ?>" type="hidden" id="pcat_id" name="pcat_id"/>
		<input value="<?php echo $cat_id; ?>" type="hidden" id="cat_id" name="cat_id"/>
		<input value="<?php echo $subcat_id; ?>" type="hidden" id="subcat_id" name="subcat_id"/>
		<input value="view" type="hidden" name="create_editview">
	</form>			
</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>

<script>

function showAvailableCategoriesEdit(obj)
{
	//if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#edit_categories").html(data);
						
						
					}
					else{
						$("#edit_categories").html('<option value=""></option>')
						$("#brands").html('<option value=""></option>')

					}
				}
			});
	//}
}
function showAvailableSubCategoriesEdit(obj)
{
	cat_id=obj.value;
	//if(obj.value!="" && obj.value!="None"){
		
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#edit_subcategories").html(data);
							
					}
					else{
						$("#edit_subcategories").html('<option value=""></option>');

					}
				}
			});
	//}
}

editAttributeFun('<?php echo $catalog;?>','<?php echo $catalog_id;?>','<?php echo $attribute_id;?>');

function editAttributeFun(catalog,catalog_id,attribute_id){

$("#edit_attribute_id").val(attribute_id);
		
			if(catalog =="pcat_id")
			{
				$("#edit_parent_category").val(catalog_id);
			}
			$('#edit_attribute')[0].reset();
  
					
					edit_attribute_id='<?php echo $attr_arr['attribute_id'];?>';
					attribute1_name='<?php echo $attr_arr['attribute1_name']?>';
					attribute2_name='<?php echo $attr_arr['attribute2_name']?>';
					attribute3_name='<?php echo $attr_arr['attribute3_name']?>';
					attribute4_name='<?php echo $attr_arr['attribute4_name']?>';
					attribute1_option='<?php echo $attr_arr['attribute1_option']?>';
					attribute2_option='<?php echo $attr_arr['attribute2_option']?>';
					attribute3_option='<?php echo $attr_arr['attribute3_option']?>';
					attribute4_option='<?php echo $attr_arr['attribute4_option']?>';
					
				
					
					edit_multi_fields='';
					
					if(attribute1_name.toLowerCase()=="color"){
						$("#edit_select_color").val(1);	
						 $('#colorpicker_edit').show();
						  $('#otherattribute_edit').hide();
						colors_value_arr=attribute1_option.split(",");
						for(x in colors_value_arr){
							colors_value=colors_value_arr[x];
							colors_arr=colors_value.split(":");
							//0 is color //1 color value
							//alert(colors_arr[0]);
							
							edit_multi_fields+='<div class="edit_add_range col-md-11"><div class="col-md-4">Pick color:<div class="edit-color-picker input-group colorpicker-component colorpicker-element"><input name="edit_color_value[]" id="edit_color_value[]" value="'+colors_arr[1]+'" class="form-control" type="text"><span class="input-group-addon"><i></i></span></div></div><div class="col-md-3">Type color name:<input name="edit_color_name[]" id="edit_color_name[]" value="'+colors_arr[0]+'" class="form-control" type="text"></div><div class="col-md-3">Sample color name : <input id="edit_color_name_sample[]" name="edit_color_name_sample[]" value="" class="form-control edit_sample_name" type="text" readonly></div><div class="col-md-1 align_top"><button id="color_create" class="btn btn-danger btn-sm edit_remove_range"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>';
			
						}
						$("#edit_attribute1_name").val(attribute1_name);
						$("#edit_attribute1_option").val(attribute1_option);
						//edit_multi_fields+='<div class="col-md-1 align_top"><button id="edit_add_one_more" name="edit_add_one_more" class="btn btn-info edit_add_one_more pull-right"><i class="fa fa-plus" aria-hidden="true"></i></button></div>';
					}if(attribute1_name.toLowerCase()!="color"){
						$("#edit_select_color").val(2);
						 $('#colorpicker_edit').hide();
						 $('#otherattribute_edit').show();
						 $("#edit_attribute1_name1").val(attribute1_name);	
						 $("#edit_attribute1_option1").val(attribute1_option);
					}

					//edit-multi-fields
					$(".edit-multi-fields").html(edit_multi_fields);
					$("#edit_attribute2_name").val(attribute2_name);
					$("#edit_attribute3_name").val(attribute3_name);	
					$("#edit_attribute4_name").val(attribute4_name);	
					$("#edit_attribute2_option").val(attribute2_option);	
					$("#edit_attribute3_option").val(attribute3_option);	
					$("#edit_attribute4_option").val(attribute4_option);	
					
					edit_call_this();

}
$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
	
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
	
}

$("form[name='edit_attribute'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
	if($(this).attr('name')!="edit_color_name_sample[]"){
		orig[$(this).attr('id')] = tmp;
	}
});

$('form[name="edit_attribute"]').bind('change keyup click', function () {

    var disable = true;
    $("form[name='edit_attribute'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
		var name = $(this).attr('name');

		if($(this).attr('name')!="edit_color_name_sample[]"){
			if (type == 'text' || type == 'select' || type == 'textarea') {
				disable = (orig[id].value == $(this).val());

			} else if (type == 'radio') {
				disable = (orig[id].checked == $(this).is(':checked'));
			}

			if (!disable) {
				return false; // break out of loop
			}
		}
    });

    button.prop('disabled', disable);
});


});

function form_validation_edit()
{
	var err = 0;
	var value=document.getElementById("edit_select_color").value;
	if(value==1){
	var str='';
	var names=[];
	var values=[];
	$('.edit-color-picker').each(function(k) {
		color_name=document.getElementsByName('edit_color_name[]')[k].value.trim();
		color_value=document.getElementsByName('edit_color_value[]')[k].value.trim();
		if($.inArray(color_name,names) == -1){
			// the element is not in the array
			names.push(color_name);
		}else{
			alert('duplication of name');
			//return false;
			 err = 1;
		}
		if($.inArray(color_value,values) == -1){
			// the element is not in the array
			values.push(color_value);
		}else{
			alert('duplication of value');
			//return false;
			 err = 1;
		}
		str+=color_name+':'+color_value;
		str+=',';
		
	});
	
	$('textarea[name="edit_attribute1_option"]').val(str);
	var attribute1_name = $('input[name="edit_attribute1_name"]').val().trim();
	var attribute1_option = $('textarea[name="edit_attribute1_option"]').val().trim();
	var color_value_edit = $('input[name="edit_color_name[]"]').val().trim();
	var color_name_edit = $('input[name="edit_color_value[]"]').val().trim();
	
	if(color_value_edit=="")
		{
		  
		   err = 1;
		}else{
			
		}
		if(color_name_edit=="")
		{
		  
		   err = 1;
		}else{
			
		}
	}
	if(value==2){
	var attribute11_name = $('input[name="edit_attribute1_name1"]').val().trim();
	var attribute11_option = $('textarea[name="edit_attribute1_option1"]').val().trim();
	if(attribute11_name=="")
		{
		    
		   err = 1;
		}
		else{			
			
		}
		if(attribute11_option=="")
		{
		   
		   err = 1;
		}else{
			
		}
}
	var attribute_id=$("#edit_attribute_id").val();
	//var attribute1_name = $('input[name="edit_attribute1_name"]').val().trim();
	//var attribute1_option = $('textarea[name="edit_attribute1_option"]').val().trim();
	var parent_category = $('select[name="edit_parent_category"]').val().trim();
	var categories = $('select[name="edit_categories"]').val().trim();
	var subcategories = $('select[name="edit_subcategories"]').val().trim();
	var select_color = $('select[name="edit_select_color"]').val().trim();
	var view = document.querySelector('input[name="edit_view"]:checked').value;
	
		
		if(parent_category=='')
		{
		   //$('select[name="edit_parent_category"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('select[name="edit_parent_category"]').css({"border": "1px solid #ccc"});
		}
		if(categories=='')
		{
		     
		   err = 1;
		}
		else{			
			
		}
		
		if(subcategories=='')
		{
		     
		   err = 1;
		}
		else{			
			
		}
		if(select_color==0)
		{
		     
		   err = 1;
		}
		else{			
			
		}
		if(err==1)
		{
			   $('#error_result_id').show();
			   $('#result').hide();
		}
		else
			{
			$("#error_edit").hide();
			document.getElementById("edit_parent_category").disabled="";
			document.getElementById("edit_categories").disabled="";
			document.getElementById("edit_subcategories").disabled="";	 
			var form_status = $('<div class="form_status"></div>');		
			var form = $('#edit_attribute');		
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();
					 $.ajax({
							url: '<?php echo base_url()."admin/Catalogue/edit_attribute/"?>',
							type: 'POST',
							data: $('#edit_attribute').serialize()+"&value="+value,
							dataType: 'html',
							
						}).done(function(data){
														
								form_status.html('') 
								if(data){
									
									swal({
										title:"Success", 
										text:"Attribute is successfully updated!", 
										type: "success",
										allowOutsideClick: false
									}).then(function () {
										location.reload();

									});
								}else{
									swal(
										'Oops...',
										'Error in form',
										'error'
									)
								}
							}); 
}
}]);
				  
			}
	return false;
}

</script>
</div>
</body>

</html>