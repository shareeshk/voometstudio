<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>

<style>

</style>
<script type="text/javascript">
       $(document).ready(function () {
          
		   $.ajax({
			   url:"<?php echo base_url()?>admin/Catalogue/archived_vendor_count",
			   type:"POST",
			   success:function(data){
				   
				   $("#archived_vendors_count").text(data);
			   }
		   })
			

       });    
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#myTable').dataTable({ stateSave: true });
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
});
function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_restore_fun(table_name){
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table_name+"!");
		return false;
	}
	selected_list=selected_list_arr.join(",");
	swal({
			title: 'Are you sure?',
			text: "Restore Archived Vendors",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, restore it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
		$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/update_vendors_selected",
		type:"post",
		data:"selected_list="+selected_list+"&active=1",
		
		success:function(data){
			if(data==true){
				swal({
						title:"Restored!", 
						text:"Given "+table_name+"(s) has been restored successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
</script>

<?php //print_r($vendor);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
	<div class="wizard-header text-center">
		<h5> Number of Archived Vendors <span class="badge"><div id="archived_vendors_count"></div></span> 
		</h5>                        		               	
	</div>

<div class="row" id="viewDiv1">
<table id="myTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
		<tr>
		<th colspan="5">
			<button id="multiple_delete_button" class="btn btn-success btn-xs" onclick="multilpe_restore_fun('vendor')">Restore</button>
		</th>
		</tr>
	<tr>
		<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
		<th>S. No</th>
		<th>Name </th>
		<th>Details</th>
		<th>Last Updated</th>
	</tr>
</thead>
<tbody>
<?php
$i=0;
foreach ($vendors as $vendors_value) { 
$i++; ?>

<tr>
<td><input type="checkbox" name="selected_list" value="<?php echo $vendors_value->vendor_id; ?>"></td>
<td><?php echo $i; ?></td>
<td><?php echo $vendors_value -> name?></td>
<td>
	<?php 
	echo "Email :". $vendors_value -> email.'<br>';
	echo "Mobile :". $vendors_value -> mobile.'<br>';
	echo "Land Line :".  $vendors_value -> landline.'<br>';
	echo "View :".  $vendors_value -> address1.' ,'; 
	echo   $vendors_value -> address2.'<br>'; 
	echo "Pincode :".  $vendors_value -> pincode.'<br>'; 
	 ?>
</td>
<td><?php echo $vendors_value -> timestamp?></td>

</tr>
<?php } ?>
</tbody>
</table>
</div>

</div>
</div>
</body>
</html>