<?php
	if(!$this->session->userdata("user_id")){
		header("location:".base_url()."Administrator");
		exit;
	}
?>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon/VS_favicon.ico">
<title>Flamingo Admin Panel</title>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.1.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/fonts_material.css" />
<?php /* ?><link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/fonts_roboto.css" /> <?php */ ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome.min.css" />
<link href="<?php echo base_url();?>assets/css/animsition.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-kit.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/sweetalert2.min.css" rel="stylesheet" /> 
<script src="<?php echo base_url();?>assets/js/sweetalert2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/animsition.js"></script>
<style>
body.swal2-in,html.swal2-in{overflow-y:scroll;}
body{overflow-y:scroll;padding-right: 0 !important;}
.cursor_pointer{
	cursor: pointer;	
}
.change_to_log{
    
    font-size:0.8em;
    font-style:italic;
}
.change_head{
    color:ff0000;
}
.change_data{
    color:#8080ff;
    font-style: oblique;
}

#log_data  td:nth-child(3) { font-size:0.8em;
    font-style:italic; }
#log_data  td:nth-child(4) { font-size:0.8em;
    font-style:italic; }
    
#log_data  td:nth-child(5) { font-size:0.8em;
    font-style:italic; }
   
.log_dates{
     background-color:#e6ffe6;    
     
 }   
    
.badge-yellow {
    background-color: #f2994b !important;
} 

#Notification_symbol{
	color:red;
}

#wrapper {
    padding-left: 0;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;	
}

#wrapper.toggled {
    padding-left: 250px;
}

#sidebar-wrapper {
	top:70px;
    position: fixed;
    left: 250px;
    z-index: 1000;
    overflow-y: auto;
    margin-left: -250px;
    width: 0;
    height: 100%;
    background: #fff;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}

#wrapper.toggled #sidebar-wrapper {
    width: 250px;
}

#page-content-wrapper {
    padding: 15px;
    width: 100%;
}

#wrapper.toggled #page-content-wrapper {
    position: absolute;
    margin-right: -250px;
}

/* main menu */
.navbar-nav>li .active2{
    background-color: blue;
    border: 1px solid;
    border-radius: 10px;
}
.navbar .navbar-nav > .active > a, .navbar .navbar-nav > .active > a:hover, .navbar .navbar-nav > .active > a:focus {
    font-weight: bold;
}
.active_drop {
    background-color: #03a9f4;
    color: #FFFFFF !important;
}
.active_left{
    font-weight: bold !important;
    background-color: #03a9f4;
    color: #FFFFFF !important;
}

/* Sidebar Styles */

.sidebar-nav {
    position: absolute;
    top: 16px;
	left:16px;
    margin: 0;
    padding: 0 32px 0 0;
    width: 250px;
    list-style: none;
}

.sidebar-nav li {
	text-indent: 5px;
    line-height: 40px;
}

.sidebar-nav li a {
    display: block;
    color: #333;
    text-decoration: none;
}

.sidebar-nav li a:hover {
    /*background: #03a9f4;*/
    color: #333;
    text-decoration: none;
}

.sidebar-nav li a:active,
.sidebar-nav li a:focus {
    text-decoration: none;
	/*background: #03a9f4;*/
    font-weight: bold;
    /*color: #fff;*/
}
.sidebar-nav li .active {
    text-decoration: none;
    background: #03a9f4;
    font-weight: bold;
    color: #fff;
}

.sidebar-nav > .sidebar-brand {
    height: 65px;
    font-size: 18px;
    line-height: 60px;
}

.sidebar-nav > .sidebar-brand a {
    color: #777;
}

.sidebar-nav > .sidebar-brand a:hover {
    background: none;
    color: #777;
}

@media (min-width: 768px) {
    

    #wrapper.toggled {
        padding-left: 0;
    }

    #sidebar-wrapper {
        width: 250px;
    }

    #wrapper.toggled #sidebar-wrapper {
        width: 0;
    }

    #page-content-wrapper {
        padding: 20px;
    }

    #wrapper.toggled #page-content-wrapper {
        position: relative;
        margin-right: 0;
    }
}

 .navbar .nav>li>ul>li>a>.label {
    top: 9px;
	text-align: center;
    font-size: 9px;
    padding: 2px 3px;
    line-height: .9;
 }
 .sidebar-nav>li>a>.label{
    text-align: center;
    font-size: 10px;
    padding: 2px 3px;
    line-height: .9;
 
 }
.fa-bell{
	color:red;
}
.page-header{
	margin-top:20px;
	margin-bottom:10px;
}
.wizard-container{
	margin-top:20px;
}
a.morelink {
	text-decoration:none;
	outline: none;
}
.morecontent span {
	display: none;
}
.modal-open .modal .modal-body::-webkit-scrollbar {display:none;}
</style>


<script>
$(document).ready(function(){
	$(".modal").on('shown.bs.modal', function(){
    if($("body").hasClass("modal-open")){
			$("body").css({"padding-right": ""});
			var modalObj = $(this).find('.modal-content');
			if ($(modalObj).height() > $(window).height()) {
			$(".modal-open .modal").css({"overflow-y": "hidden"});
			$(".modal-open .modal .modal-body").css({"overflow-y": "auto","max-height": "calc(100vh - 100px)"});
			$("body").css({"overflow-y": "scroll"});
		}else{
			$(".modal-open .modal").css({"overflow-y": "hidden"});
			$(".modal-open .modal .modal-body").css({"overflow-y": "auto","max-height": "calc(100vh - 150px)"});
			$("body").css({"overflow-y": "scroll"});
		}
	}
});
});
function morelinkfun(obj){
	var moretext = "more";
	var lesstext = "less";
	if($(obj).hasClass("less")) {
		$(obj).removeClass("less");
		$(obj).html(moretext);
	} else {
		$(obj).addClass("less");
		$(obj).html(lesstext);
	}
	$(obj).parent().prev().toggle();
	$(obj).prev().toggle();
	return false;	
}
function order_details(id){
	document.getElementById("order_item_details_"+id).submit();
}

$(document).ready(function(){
	var onResize = function() {
  $("body").css("padding-top", $(".navbar-fixed-top").height()+20);
};
$(window).resize(onResize);
$(function() {
  onResize();
});
	
	get_total_count_of_replacement_and_return();
	get_total_count_of_notification();
	get_notify_active_fun();
	get_notify_active_invoice_fun();
	get_notify_cancel_order_fun();
	
	get_notify_return_replacement_fun();
	get_notify_return_refund_fun();
	
	get_notify_replacement_reject_fun();
	get_notify_replacement_accept_fun();
	get_notify_replacement_ready_to_replace_fun();
	get_notify_replacement_cancel_fun();
	get_notify_replacement_stock_fun();

	get_notify_refund_account_fun();
	get_notify_cancel_account_fun();
	get_notify_replacement_account_fun();
	get_notify_bank_transfer_account_fun();
	
	get_notify_refund_reject_fun();
	get_notify_refund_accept_fun();
	get_notify_refund_again_fun();
	get_notify_refund_failure_fun();
	get_notify_refund_account_incoming_again_fun();
});

function get_notify_refund_account_incoming_again_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_refund_account_incoming_again",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#total_refund_incoming_again_full_refund").removeClass("fa fa-bell-o");
					$("#total_refund_incoming_again_full_refund").addClass("fa fa-bell");
					//$("#notify_account_refund_menu").removeClass("fa fa-bell-o");
					$("#notify_account_refund_menu").addClass("fa fa-bell");
					notify_accounts_menu_fun();
					
				}

			}
	});
}

function get_notify_refund_failure_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_refund_failure",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#total_unread_return_msg_refund_failure").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_refund_failure").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun();
					
				}

			}
	});
}
//////////accounts-start///////////
function get_notify_refund_account_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_refund_account",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#notify_account_refund_menu").removeClass("fa fa-bell-o");
					$("#notify_account_refund_menu").addClass("fa fa-bell");
					notify_accounts_menu_fun();
					
				}

			}
	});
}
function get_notify_cancel_account_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_cancel_account",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#notify_account_cancel_menu").removeClass("fa fa-bell-o");
					$("#notify_account_cancel_menu").addClass("fa fa-bell");
					notify_accounts_menu_fun();
					
				}

			}
	});
}
function get_notify_replacement_account_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_replacement_account",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#notify_account_replacement_menu").removeClass("fa fa-bell-o");
					$("#notify_account_replacement_menu").addClass("fa fa-bell");
					notify_accounts_menu_fun();
					
				}

			}
	});
}
function get_notify_bank_transfer_account_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_bank_transfer_account",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#notify_account_bank_transfer_menu").removeClass("fa fa-bell-o");
					$("#notify_account_bank_transfer_menu").addClass("fa fa-bell");
					notify_accounts_menu_fun();
					
				}

			}
	});
}
get_notify_promotion_account_fun();
get_notify_promotion_account_invoice_fun();

function get_notify_promotion_account_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_promotion_account",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#notify_account_promotion_menu").removeClass("fa fa-bell-o");
					$("#notify_account_promotion_menu").addClass("fa fa-bell");
					//$("#total_promotion_order_item_refund").removeClass("fa fa-bell-o");
					$("#total_promotion_order_item_refund").addClass("fa fa-bell");
					notify_accounts_menu_fun();
					
				}

			}
	});
}

function get_notify_promotion_account_invoice_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_promotion_account_invoice",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#notify_account_promotion_menu").removeClass("fa fa-bell-o");
					$("#notify_account_promotion_menu").addClass("fa fa-bell");
					
					notify_accounts_menu_fun();
					
				}

			}
	});
}
function notify_accounts_menu_fun(){
		//if($("#notify_account_refund_menu").attr("class")=="fa fa-bell" || $("#notify_account_cancel_menu").attr("class")=="fa fa-bell" || $("#notify_account_replacement_menu").attr("class")=="fa fa-bell" || $("#notify_account_bank_transfer_menu").attr("class")=="fa fa-bell" || $("#notify_account_promotion_menu").attr("class")=="fa fa-bell"){		
			$("#notify_accounts_menu").removeClass("fa fa-bell-o");
			$("#notify_accounts_menu").addClass("fa fa-bell");
		//}
	}
//////////accounts-end/////////////	
function get_total_count_of_replacement_and_return(){
	
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_total_count_of_replacement_and_return",
			type:"POST",
			dataType:"JSON",
			data:"1=2",
			success:function(data){

				return_msg=data.return_msg;
				msg_awaiting=data.msg_awaiting;
				msg_customer_rejected=data.msg_customer_rejected;
				msg_customer_accepted=data.msg_customer_accepted;
				msg_admin_rejected=data.msg_admin_rejected;
				msg_refund_failure=data.msg_refund_failure;
				success_partial_refund=data.success_partial_refund;
				
				need_to_be_refund=data.need_to_be_refund;
				refund_in_process=data.refund_in_process;
				success_full_refund=data.success_full_refund;
					
				return_msg_replacement=data.return_msg_replacement;
				msg_awaiting_replacement=data.msg_awaiting_replacement;
				msg_customer_accepted_replacement=data.msg_customer_accepted_replacement;
				msg_customer_rejected_replacement=data.msg_customer_rejected_replacement;
				msg_balance_clear_replacement=data.msg_balance_clear_replacement;
				
				msg_admin_rejected_replacement=data.msg_admin_rejected_replacement;
				success_partial_refund_replacement=data.success_partial_refund_replacement;
				return_stock_requests_replacement=data.return_stock_requests_replacement;
				ready_to_replace=data.ready_to_replace;
				success_refund_replacement=data.success_refund_replacement;
				return_stock_notification=data.return_stock_notification;
				replacement_return_cancel=data.replacement_return_cancel;
				
				
				if(return_msg!=""){
					//$("#total_unread_return_msg").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun();
				}
				else{
					$("#total_unread_return_msg").removeClass("fa fa-bell");
					//$("#total_unread_return_msg").addClass("fa fa-bell-o");
				}
				
				if(msg_awaiting!=""){
					//$("#total_unread_return_msg_awaiting").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_awaiting").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun();
				}
				else{
					$("#total_unread_return_msg_awaiting").removeClass("fa fa-bell");
					//$("#total_unread_return_msg_awaiting").addClass("fa fa-bell-o");
					
				}
				
				if(msg_customer_rejected!=""){
					//$("#total_unread_return_msg_customer_rejected").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_customer_rejected").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun();
				}
				else{
					$("#total_unread_return_msg_customer_rejected").removeClass("fa fa-bell");
					//$("#total_unread_return_msg_customer_rejected").addClass("fa fa-bell-o");
					
				}
				
				if(msg_customer_accepted!=""){
					//$("#total_unread_return_msg_customer_accepted").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_customer_accepted").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun();
				}
				else{
					$("#total_unread_return_msg_customer_accepted").removeClass("fa fa-bell");
					//$("#total_unread_return_msg_customer_accepted").addClass("fa fa-bell-o");
				}
				
				if(msg_admin_rejected!=""){
					//$("#total_unread_return_msg_admin_rejected").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_admin_rejected").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun();
				}
				else{
					$("#total_unread_return_msg_admin_rejected").removeClass("fa fa-bell");
					//$("#total_unread_return_msg_admin_rejected").addClass("fa fa-bell-o");
				}
				
				if(msg_refund_failure!=""){
					//$("#total_unread_return_msg_refund_failure").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_refund_failure").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun();
				}
				else{
					$("#total_unread_return_msg_refund_failure").removeClass("fa fa-bell");
					//$("#total_unread_return_msg_refund_failure").addClass("fa fa-bell-o");
				}
				

				if(success_partial_refund!=""){
					//$("#total_unread_return_msg_refund_success_partial_refund").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_refund_success_partial_refund").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun();
				}
				else{
					$("#total_unread_return_msg_refund_success_partial_refund").removeClass("fa fa-bell");
					//$("#total_unread_return_msg_refund_success_partial_refund").addClass("fa fa-bell-o");
				}
				
				if(need_to_be_refund!=""){
					//$("#total_need_to_be_refund").removeClass("fa fa-bell-o");
					$("#total_need_to_be_refund").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun();
				}
				else{
					$("#total_need_to_be_refund").removeClass("fa fa-bell");
					//$("#total_need_to_be_refund").addClass("fa fa-bell-o");
				}
				
				if(refund_in_process!=""){
					//$("#total_refund_in_process").removeClass("fa fa-bell-o");
					$("#total_refund_in_process").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun();
				}
				else{
					$("#total_refund_in_process").removeClass("fa fa-bell");
					//$("#total_refund_in_process").addClass("fa fa-bell-o");
				}
				
				if(success_full_refund!=""){
					//$("#total_success_full_refund").removeClass("fa fa-bell-o");
					$("#total_success_full_refund").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun();
				}
				else{
					$("#total_success_full_refund").removeClass("fa fa-bell");
					//$("#total_success_full_refund").addClass("fa fa-bell-o");
				}
				
		
				
				///////////replacement//////////
				if(return_msg_replacement!=""){
					//$("#total_unread_return_msg_replacement").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_replacement").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun_rep();
				}
				else{
					$("#total_unread_return_msg_replacement").removeClass("fa fa-bell");
					//$("#total_unread_return_msg_replacement").addClass("fa fa-bell-o");
				}
				
				if(msg_awaiting_replacement!=""){
					//$("#total_unread_return_msg_awaiting_replacement").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_awaiting_replacement").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun_rep();
				}
				else{
					$("#total_unread_return_msg_awaiting_replacement").removeClass("fa fa-bell");
					//$("#total_unread_return_msg_awaiting_replacement").addClass("fa fa-bell-o");
				}
				
				if(msg_customer_rejected_replacement!=""){
					//$("#total_unread_return_msg_customer_rejected_replacement").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_customer_rejected_replacement").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun_rep();
				}
				else{
					$("#total_unread_return_msg_customer_rejected_replacement").removeClass("fa fa-bell");
					//$("#total_unread_return_msg_customer_rejected_replacement").addClass("fa fa-bell-o");
				}
				
				if(msg_customer_accepted_replacement!=""){
					//$("#total_unread_return_msg_customer_accepted_replacement").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_customer_accepted_replacement").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun_rep();
				}
				else{
					$("#total_unread_return_msg_customer_accepted_replacement").removeClass("fa fa-bell");
					//$("#total_unread_return_msg_customer_accepted_replacement").addClass("fa fa-bell-o");
				}
				
				if(msg_balance_clear_replacement!=""){
					//$("#total_unread_return_msg_balance_clear_replacement").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_balance_clear_replacement").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun_rep();
				}
				else{
					$("#total_unread_return_msg_balance_clear_replacement").removeClass("fa fa-bell");
					//$("#total_unread_return_msg_balance_clear_replacement").addClass("fa fa-bell-o");
				}
				
				if(msg_admin_rejected_replacement!=""){
					//$("#total_unread_return_msg_admin_rejected_replacement").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_admin_rejected_replacement").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun_rep();
				}
				else{
					$("#total_unread_return_msg_admin_rejected_replacement").removeClass("fa fa-bell");
					//$("#total_unread_return_msg_admin_rejected_replacement").addClass("fa fa-bell-o");
				}
								
				if(ready_to_replace!=""){
					//$("#total_return_stock_ready_to_replace").removeClass("fa fa-bell-o");
					$("#total_return_stock_ready_to_replace").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun_rep();
				}
				else{
					$("#total_return_stock_ready_to_replace").removeClass("fa fa-bell");
					//$("#total_return_stock_ready_to_replace").addClass("fa fa-bell-o");
				}						
				if(success_partial_refund_replacement!=""){
					//$("#total_unread_return_msg_refund_success_partial_refund_replacement").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_refund_success_partial_refund_replacement").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun_rep();
				}
				else{
					$("#total_unread_return_msg_refund_success_partial_refund_replacement").removeClass("fa fa-bell");
					//$("#total_unread_return_msg_refund_success_partial_refund_replacement").addClass("fa fa-bell-o");
				}
				
				if(success_refund_replacement!=""){
					//$("#total_unread_return_msg_refund_success_refund_replacement").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_refund_success_refund_replacement").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun_rep();
				}
				else{
					$("#total_unread_return_msg_refund_success_refund_replacement").removeClass("fa fa-bell");
					//$("#total_unread_return_msg_refund_success_refund_replacement").addClass("fa fa-bell-o");
				}
				
				if(replacement_return_cancel!=""){
					//$("#total_unread_return_msg_replacement_cancel").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_replacement_cancel").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun_rep();
				}
				else{
					$("#total_unread_return_msg_replacement_cancel").removeClass("fa fa-bell");
					//$("#total_unread_return_msg_replacement_cancel").addClass("fa fa-bell-o");
				}
				
				if(return_stock_notification!=""){
					//$("#total_unread_return_msg_stock").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_stock").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun_rep();
				}
				else{
					$("#total_unread_return_msg_stock").removeClass("fa fa-bell");
					//$("#total_unread_return_msg_stock").addClass("fa fa-bell-o");
				}
			}
			
	});
	
}

///////////refund-start////////////
function get_notify_refund_reject_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_return_reject",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#total_unread_return_msg_customer_rejected").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_customer_rejected").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun();
					
				}

			}
	});
}
function get_notify_refund_accept_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_refund_accept",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					$("#total_unread_return_msg_customer_accepted").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_customer_accepted").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun();
					
				}

			}
	});
}
function get_notify_refund_again_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_refund_again",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#total_unread_return_msg_refund_success_partial_refund").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_refund_success_partial_refund").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun();
					
				}

			}
	});
}
///////////refund-end///////////

///////////replacement-start///////////
function get_notify_replacement_reject_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_replacement_reject",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#total_unread_return_msg_customer_rejected_replacement").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_customer_rejected_replacement").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun_rep();
					
				}

			}
	});
}
function get_notify_replacement_stock_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_replacement_stock",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#total_unread_return_msg_stock").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_stock").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun_rep();
					
				}

			}
	});
}
function get_notify_replacement_cancel_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_replacement_cancel",
			type:"POST",
			data:"1=2",
			success:function(data){	
		
				if(data=="yes"){
					//$("#total_unread_return_msg_replacement_cancel").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_replacement_cancel").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun_rep();
					
				}

			}
	});
}
get_notify_replacement_stock_yes_no_decision_fun();
function get_notify_replacement_stock_yes_no_decision_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_replacement_stock_yes_no_decision",
			type:"POST",
			data:"1=2",
			success:function(data){
			//alert(data);				
				if(data=="yes"){
					//$("#total_unread_return_msg_stock").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_stock").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun_rep();
					
				}

			}
	});
}
function get_notify_replacement_accept_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_replacement_accept",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#total_unread_return_msg_customer_accepted_replacement").removeClass("fa fa-bell-o");
					$("#total_unread_return_msg_customer_accepted_replacement").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun_rep();
				}

			}
	});
}
function get_notify_replacement_ready_to_replace_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_replacement_ready_to_replace",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#total_return_stock_ready_to_replace").removeClass("fa fa-bell-o");
					$("#total_return_stock_ready_to_replace").addClass("fa fa-bell");
					notify_returns_msg_alert_menu_fun_rep();
				}

			}
	});
}
///////////replacement-end///////////
///////////orders-start/////////////////////

function get_notify_active_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_active",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#notify_active_menu").removeClass("fa fa-bell-o");
					$("#notify_active_menu").addClass("fa fa-bell");
					notify_orders_menu_fun();
					
				}

			}
	});
}
function get_notify_active_invoice_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_invoice",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#notify_freeitem_menu").removeClass("fa fa-bell-o");
					$("#notify_freeitem_menu").addClass("fa fa-bell");
					notify_orders_menu_fun();
					
				}

			}
	});
}
function get_notify_cancel_order_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_cancel_order",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#notify_refundcancels_menu").removeClass("fa fa-bell-o");
					$("#notify_refundcancels_menu").addClass("fa fa-bell");
					notify_orders_menu_fun();
					
				}

			}
	});
}

	function notify_orders_menu_fun(){
		//if($("#notify_active_menu").attr("class")=="fa fa-bell" || $("#notify_freeitem_menu").attr("class")=="fa fa-bell" || $("#notify_refundcancels_menu").attr("class")=="fa fa-bell"){		
			$("#notify_orders_menu").removeClass("fa fa-bell-o");
			$("#notify_orders_menu").addClass("fa fa-bell");
		//}
	}
////////////orders-end//////////////
/////////return-rep-cancel-start///////////	

function get_notify_return_replacement_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_return_replacement",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#notify_return_replacement_menu").removeClass("fa fa-bell-o");
					$("#notify_return_replacement_menu").addClass("fa fa-bell");
					notify_reurns_menu_fun();
					
				}

			}
	});
}
function get_notify_return_refund_fun(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_return_refund",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#notify_return_refund_menu").removeClass("fa fa-bell-o");
					$("#notify_return_refund_menu").addClass("fa fa-bell");
					notify_reurns_menu_fun();
					
				}

			}
	});
}
get_notify_return_cancel_for_repl_and_refund_stock();

function get_notify_return_cancel_for_repl_and_refund_stock(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_notify_return_cancel_for_repl_and_refund_stock",
			type:"POST",
			data:"1=2",
			success:function(data){				
				if(data=="yes"){
					//$("#notify_return_cancel_menu").removeClass("fa fa-bell-o");
					$("#notify_return_cancel_menu").addClass("fa fa-bell");
					notify_reurns_menu_fun();	
				}
			}
	});
}

	function notify_reurns_menu_fun(){
		//if($("#notify_return_replacement_menu").attr("class")=="fa fa-bell" || $("#notify_return_refund_menu").attr("class")=="fa fa-bell" || $("#notify_return_cancel_menu").attr("class")=="fa fa-bell"){		
			$("#notify_returns_menu").removeClass("fa fa-bell-o");
			$("#notify_returns_menu").addClass("fa fa-bell");
		//}
	}
//////////////////return-rep-cancel-end///////////////	
	
	//////////refund//////////
	function notify_returns_msg_alert_menu_fun(){
		//if($("#total_unread_return_msg_customer_accepted").attr("class")=="fa fa-bell" || $("#total_unread_return_msg").attr("class")=="fa fa-bell" || $("#total_unread_return_msg_awaiting").attr("class")=="fa fa-bell" || $("#total_unread_return_msg_customer_rejected").attr("class")=="fa fa-bell" || $("#total_unread_return_msg_admin_rejected").attr("class")=="fa fa-bell" || $("#total_unread_return_msg_refund_failure").attr("class")=="fa fa-bell" || $("#total_unread_return_msg_refund_success_partial_refund").attr("class")=="fa fa-bell" || $("#total_need_to_be_refund").attr("class")=="fa fa-bell" || $("#total_refund_in_process").attr("class")=="fa fa-bell" || $("#total_success_full_refund").attr("class")=="fa fa-bell"){		
			//$("#notify_return_refund_menu").removeClass("fa fa-bell-o");
			$("#notify_return_refund_menu").addClass("fa fa-bell");
			notify_reurns_menu_fun();
		//}
	}
	////////////replacement///////////
	function notify_returns_msg_alert_menu_fun_rep(){
		//if($("#total_unread_return_msg_replacement").attr("class")=="fa fa-bell" || $("#total_unread_return_msg_awaiting_replacement").attr("class")=="fa fa-bell" || $("#total_unread_return_msg_customer_rejected_replacement").attr("class")=="fa fa-bell" || $("#total_unread_return_msg_customer_accepted_replacement").attr("class")=="fa fa-bell" || $("#total_unread_return_msg_balance_clear_replacement").attr("class")=="fa fa-bell" || $("#total_unread_return_msg_admin_rejected_replacement").attr("class")=="fa fa-bell" || $("#total_return_stock_ready_to_replace").attr("class")=="fa fa-bell" || $("#total_unread_return_msg_refund_success_partial_refund_replacement").attr("class")=="fa fa-bell" || $("#total_unread_return_msg_refund_success_refund_replacement").attr("class")=="fa fa-bell" || $("#total_unread_return_msg_replacement_cancel").attr("class")=="fa fa-bell" || $("#total_unread_return_msg_stock").attr("class")=="fa fa-bell"){		
			//$("#notify_return_replacement_menu").removeClass("fa fa-bell-o");
			$("#notify_return_replacement_menu").addClass("fa fa-bell");
			notify_reurns_menu_fun();
		//}
	}

function get_total_count_of_notification(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_total_count_of_notification",
			type:"POST",
			dataType:"JSON",
			data:"1=2",
			success:function(data){				
				low_stock_notification_count=data.low_stock_notification;
				request_out_of_stock_count=data.request_out_of_stock;
				new_vendor_sku_notification=data.new_vendor_sku_notification;
				
				if(low_stock_notification_count!=''){
					//$("#total_count_of_notification").html(low_stock_notification_count);
					//$("#Notification_symbol").html(' <i class="fa fa-bell" aria-hidden="true"></i> ');
				if(low_stock_notification_count!=""){
					//$("#total_count_of_notification").removeClass("fa fa-bell-o");
					$("#total_count_of_notification").addClass("fa fa-bell");
				}else{
					$("#total_count_of_notification").removeClass("fa fa-bell");
					//$("#total_count_of_notification").addClass("fa fa-bell-o");
				}

				if(new_vendor_sku_notification!=""){

					$("#catalog_menu").removeClass("fa fa-bell-o");
					$("#catalog_menu").addClass("fa fa-bell");
					
					$("#vendor_catalog_menu").removeClass("fa fa-bell-o");
					$("#vendor_catalog_menu").addClass("fa fa-bell");
					//alert()

				}else{
					$("#catalog_menu").removeClass("fa fa-bell-o");
					$("#catalog_menu").removeClass("fa fa-bell");

					$("#vendor_catalog_menu").removeClass("fa fa-bell-o");
					$("#vendor_catalog_menu").removeClass("fa fa-bell");
					
				}
				
				notify_notification_symbol_menu_fun();	
				}
				
			}
	});
}
	function notify_notification_symbol_menu_fun(){
		//if($("#total_count_of_notification").attr("class")=="fa fa-bell"){		
			$("#Notification_symbol").removeClass("fa fa-bell-o");
			$("#Notification_symbol").addClass("fa fa-bell");
		//}
	}
function get_all_num_data_of_all_promotion(){
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {
                                           
                    var listElems=document.getElementsByClassName('badge-yellow');
                    for(var i=0;i<listElems.length;i++){
                        listElems[i].innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
                    }
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            var all_nums=JSON.parse(xhr.responseText)
                            document.getElementById('revertdraft_promotion_num').innerHTML=all_nums['revertdraft_promotion_num'];
                            document.getElementById('expired_promotion_num').innerHTML=all_nums['expired_promotion_num'];
                            document.getElementById('active_promotion_num').innerHTML=all_nums['active_promotion_num'];
                            document.getElementById('draft_promotion_num').innerHTML=all_nums['draft_promotion_num'];
                            document.getElementById('deactivated_promotion_num').innerHTML=all_nums['deactivated_promotion_num'];
                            document.getElementById('pending_promotion_num').innerHTML=all_nums['pending_promotion_num'];
                            document.getElementById('approved_promotion_num').innerHTML=all_nums['approved_promotion_num'];
                            document.getElementById('rejected_promotions_num').innerHTML=all_nums['rejected_promotions_num'];
                            
                        }else{
                            var listElems=document.getElementsByClassName('badge-yellow');
                            for(var i=0;i<listElems.length;i++){
                                listElems[i].innerHTML='<i class="fa fa-exclamation-circle" aria-hidden="true"></i>'
                            }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_num_data_of_all_promotion", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send();
                }   
}

</script>
<script type="text/javascript">
       $(document).ready(function () {
		   hide_notifi();
      });  
function hide_notifi(){
	$.ajax({
			   url:"<?php echo base_url()?>admin/Manage_cust/requests_count",
			   type:"POST",
			   success:function(data){
				   if(data=='yes'){
						
						//$("#bank_transfer_request_menu").removeClass("fa fa-bell-o");
						$("#bank_transfer_request_menu").addClass("fa fa-bell");
						
						notify_customer_bank_transfer_menu_fun();
				   }
				   
			   }
		   })
}
   function notify_customer_bank_transfer_menu_fun(){
		//if($("#bank_transfer_request_menu").attr("class")=="fa fa-bell"){		
			$("#customers_menu").removeClass("fa fa-bell-o");
			$("#customers_menu").addClass("fa fa-bell");
		//}
	}
</script>
</head>

<body>

<?php if(!isset($menu_flag)){
	$menu_flag='';
}


//echo "//".$menu_flag."//";
$c=$this->router->fetch_class();
$m=$this->router->fetch_method();
//echo $m;exit;
$admin_user_type=$this->session->userdata("user_type");
$admin_module_type=$this->session->userdata("module_type");
?>
<div id="wrapper" class="toggled">
   
        <!-- Sidebar -->
<div id="sidebar-wrapper" style="border:1px solid black;">
    <ul class="sidebar-nav menu-content" id="side_menu_content">
		<?php
		
			if($menu_flag=="customer_view" && isset($customer_id)){
			
		?>
			
		<li class="active"><a data-toggle="tab" href="<?php echo base_url()?>admin/Manage_cust/view/<?php echo $customer_id;?>#tab-1" >Personal Profile</a></li>
		<li><a data-toggle="tab" href="<?php echo base_url()?>admin/Manage_cust/view/<?php echo $customer_id;?>#tab-2" >All Addresses</a></li>
		<li><a data-toggle="tab" href="<?php echo base_url()?>admin/Manage_cust/view/<?php echo $customer_id;?>#tab-3"  >Wishlists</a></li>
		<li><a data-toggle="tab" href="<?php echo base_url()?>admin/Manage_cust/view/<?php echo $customer_id;?>#tab-4" >Ratings & Reviews</a></li>
		<li><a data-toggle="tab" href="<?php echo base_url()?>admin/Manage_cust/view/<?php echo $customer_id;?>#tab-5" >Bank Details</a></li>
		<li><a data-toggle="tab" href="<?php echo base_url()?>admin/Manage_cust/view/<?php echo $customer_id;?>#tab-6" >Saved Cards</a></li>
		<li><a data-toggle="tab" href="<?php echo base_url()?>admin/Manage_cust/view/<?php echo $customer_id;?>#tab-7" >Wallet Details</a></li>
		<li><a data-toggle="tab" href="<?php echo base_url()?>admin/Manage_cust/view/<?php echo $customer_id;?>#tab-8" >Placed Orders</a></li>
		<li><a data-toggle="tab" href="<?php echo base_url()?>admin/Manage_cust/view/<?php echo $customer_id;?>#tab-9" >Rated Service</a></li>
			
		<?php
			}
		?>
			
		<?php
			
			if($menu_flag==""){
				
		?>
              	
		<?php
			}
		?>

		<?php
			if($menu_flag=='front_end'){
		?>
			<li><a class="animsition-link <?php echo ($m=='banner_and_footer') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Ui_content/banner_and_footer">Banner & Footer</a></li>
			<li><a class="animsition-link <?php echo ($m=='banner_category') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Ui_content/banner_category">Banner for category</a></li>
			<li><a class="animsition-link <?php echo ($m=='testimonial') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Ui_content/testimonial">testimonial</a></li>
			
			<li><a class="animsition-link <?php echo ($m=='site_description') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Ui_content/site_description">Why?</a></li>
			
			<li><a class="animsition-link <?php echo ($m=='assign_inventory') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Ui_content/assign_inventory">Assign Products</a></li>
		<?php 
		
			}
		?>
		<?php
            if($menu_flag=='blog'){
        ?>
			<li><a class="animsition-link <?php echo ($m=='write_a_blog') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Ui_content/write_a_blog">Write A Blog</a></li>
            <li><a class="animsition-link <?php echo ($m=='all_blogs_unpublished') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Ui_content/all_blogs_unpublished">All Unpublished</a></li>
           
            <li><a class="animsition-link <?php echo ($m=='all_blogs_published') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Ui_content/all_blogs_published">Published</a></li>
            
            <li><a class="animsition-link <?php echo ($m=='unpublished_comments') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Ui_content/unpublished_comments">New Comments</a></li>
            
            <li><a class="animsition-link <?php echo ($m=='published_comments') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Ui_content/published_comments">Published Comments</a></li>
        <?php 
        
            }
        ?>
			<?php
            if($menu_flag=='newsletter'){
        ?>
            <li><a class="animsition-link <?php echo ($m=='all_site_newsletter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Ui_content/all_site_newsletter">All News Letters</a></li>
            
        <?php 
        
            }
        ?>
		
		<?php
			if($menu_flag=='catalog_active_links'){
                            /*&& $admin_user_type=="Su Master Country" &&*/
				if(($admin_module_type=="catalog_manage" && $admin_user_type=="Su Master Country") || $admin_user_type=="Master Country" ){
		?>
			
		<li><a class="animsition-link <?php echo ($m=="parent_category") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/parent_category">Manage Parent Categories</a></li>

		<li><a class="<?php echo ($m=="category_filter" || $m=="category" || $m=="edit_category_form" ) ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/category_filter">Manage Categories</a></li>
		<li><a class="<?php echo ($m=="subcategory_filter" || $m=="subcategory" || $m=="edit_subcategory_form" ) ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/subcategory_filter">Manage Sub Categories</a></li>
		<li><a class="<?php echo ($m=="brands_filter" || $m=="brands" || $m=="edit_brand_form") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/brands_filter">Manage Brands</a></li>
		<li><a class="<?php echo ($m=="products_filter" || $m=="products" || $m=="edit_product_form") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/products_filter">Manage Products</a></li>
		<li><a class="<?php echo ($m=="attribute_filter"|| $m=="attribute" || $m=="edit_attribute_form" || $m=="attrib_expansions" || $m=="attrib_expansion_form1") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/attribute_filter">Manage Attributes</a></li>
		<li><a class="<?php echo ($m=="filterbox_filter" || $m=="filterbox" || $m=="edit_filterbox_form") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/filterbox_filter">Manage Filter box</a></li>
		<li><a class="<?php echo ($m=="filter_filter" || $m=="filter" || $m=="edit_filter_form") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/filter_filter">Manage Filter</a></li>
		<li><a class="<?php echo ($m=="specification_group_filter" || $m=="specification_group" || $m=="edit_specification_group_form") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/specification_group_filter">Manage Specification Group</a></li>
		<li><a class="<?php echo ($m=="specification_filter" || $m=="specification" || $m=="edit_specification_form") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/specification_filter">Manage Specification</a></li>
		<li><a class="<?php echo ($m=="inventory_filter" || $m=="inventory" || $m=="edit_inventory_form") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/inventory_filter">Manage Inventory</a></li>
                <li><a class="<?php echo ($m=="inventory_import_export") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/inventory_import_export">Inventory Export</a></li>

				

		<?php
				}else if($admin_user_type!="finance" && ($admin_user_type=="Su Master Country" && $admin_module_type=="catalog") || $admin_user_type=="Master Country" ){
					?>
					<li><a class="<?php echo ($m=="inventory_filter" || $m=="inventory" || $m=="edit_inventory_form") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/inventory_filter">Manage Inventory</a></li>

					<li><a class="<?php echo ($m=="inventory_all") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/inventory_all">All Inventory</a></li>

					<?php
				}else if($admin_user_type=="vendor"){
					?>
						<li><a class="<?php echo ($m=="brands_filter" || $m=="brands" || $m=="edit_brand_form") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/brands_filter">Manage Brands</a></li>
						<li><a class="<?php echo ($m=="products_filter" || $m=="products" || $m=="edit_product_form") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/products_filter">Manage Products</a></li>
						<li><a class="<?php echo ($m=="inventory_filter" || $m=="inventory" || $m=="edit_inventory_form") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/inventory_filter">Manage Inventory</a></li>
					<?php
					}
			}
		?>
		
		<?php
			if($menu_flag=='catalog_active_links_archived'){

				if($admin_user_type=='vendor'){
					?>
					
		<li><a class="<?php echo ($m=='archived_brands_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/archived_brands_filter">Brands</a></li>
		<li><a class="<?php echo ($m=='archived_products_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/archived_products_filter">Products</a></li>
		<li><a class="<?php echo ($m=='archived_inventory_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/archived_inventory_filter">Inventory</a></li>
					<?php

					
				}else{
?>

			

		<li><a class="<?php echo ($m=='archived_parent_category') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/archived_parent_category">Parent Categories</a></li>
		<li><a class="<?php echo ($m=='archived_category_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/archived_category_filter">Categories</a></li>
		<li><a class="<?php echo ($m=='archived_subcategory_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/archived_subcategory_filter">Sub Categories</a></li>
		<li><a class="<?php echo ($m=='archived_brands_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/archived_brands_filter">Brands</a></li>
		<li><a class="<?php echo ($m=='archived_products_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/archived_products_filter">Products</a></li>
		<li><a class="<?php echo ($m=='archived_inventory_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/archived_inventory_filter">Inventory</a></li>
		<li><a class="<?php echo ($m=='archived_attribute_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/archived_attribute_filter">Attributes</a></li>
		<li><a class="<?php echo ($m=='archived_filterbox_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/archived_filterbox_filter">Filter box</a></li>
		<li><a class="<?php echo ($m=='archived_filter_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/archived_filter_filter">Filter</a></li>
		<li><a class="<?php echo ($m=='archived_specification_group_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/archived_specification_group_filter">Specification Group</a></li>
		<li><a class="<?php echo ($m=='archived_specification_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/archived_specification_filter">Specification</a></li>
		<li><a class="<?php echo ($m=='archived_vendors') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/archived_vendors">vendors</a></li> 

		<?php
				}
		?>
		<?php

			}
		?>
		
		<?php
			if($menu_flag=='shipping_active_links'){
		?>
			
		 				
		<li><a class="<?php echo ($m=='vendors') ? 'active_left' : ''; ?>"  href="<?php echo base_url();?>admin/Catalogue/vendors">Vendors</a></li> 
		<li><a class="<?php echo ($m=='logistics_filter') ? 'active_left' : ''; ?>"  href="<?php echo base_url();?>admin/Catalogue/logistics_filter"> Logistics</a></li> 
		<li><a class="<?php echo ($m=='logistics_all_pincodes') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/logistics_all_pincodes"> All Pincodes</a></li> 
		<li><a class="<?php echo ($m=='logistics_all_territory_name_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/logistics_all_territory_name_filter"> All Territory Names</a></li> 
		<li><a class="<?php echo ($m=='logistics_territory_states' || $m=='logistics_territory_states_filter' || $m=='edit_logistics_territory_states_form') ? 'active_left' : ''; ?>"  href="<?php echo base_url();?>admin/Catalogue/logistics_territory_states_filter"> Logistics States</a></li>
		<li><a class="<?php echo ($m=='logistics_territory_states_cities') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/logistics_territory_states_cities_filter"> Logistics Cities</a></li>
		
		<li><a  class="<?php echo ($m=='logistics_territory_states_cities_pincodes_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/logistics_territory_states_cities_pincodes_filter"> Logistics Pincodes</a></li>
		
                <li><a class="<?php echo ($m=='logistics_delivery_mode_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/logistics_delivery_mode_filter"> Logistics Delivery Mode</a></li>
		
		<li><a class="<?php echo ($m=='logistics_weight_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/logistics_weight_filter">Logistics Weight</a></li>
		<li><a class="<?php echo ($m=='logistics_parcel_category_filter') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/logistics_parcel_category_filter"> Logistics Parcel Category</a></li>
		<?php
			}
		?>
		<?php
			if($menu_flag=='reasons_active_links'){
		?>
			
		<li><a class="<?php echo ($m=='reasons_filter' || $m=='reasons' || $m=='edit_reasons_form') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/reasons_filter">Customer Return Reasons</a></li>
		<li><a class="<?php echo ($m=='subreasons_filter' || $m=='subreasons' || $m=='edit_subreasons_form' ) ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/subreasons_filter">Customer Return Subreasons</a></li>
		<li><a class="<?php echo ($m=='payment_options' || $m=='edit_payment_options_form') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/payment_options">Customer Payment options</a></li>
		<li><a class="<?php echo ($m=='reason_cancel') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/reason_cancel">Customer Cancel Reason </a></li>
		<li><a class="<?php echo ($m=='admin_cancel_reason' || $m=='cancel_reason' || $m=='edit_reason_cancel_form') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/admin_cancel_reason">Admin Cancel Reason</a></li>
		<li><a class="<?php echo ($m=='admin_replacement_reason' || $m=='edit_admin_replacement_reason_form') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/admin_replacement_reason">Admin Replacement Reason</a></li>
		<li><a class="<?php echo ($m=='admin_return_reason' || $m=='edit_admin_return_reason_form') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/admin_return_reason">Admin Refund Reason</a></li>
		<li><a class="<?php echo ($m=='customer_deactivate_reason' || $m=='edit_customer_deactivate_reason_form') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/customer_deactivate_reason">Customer Deactivate Reason</a></li>
		<?php
			}
		?>
		
		<?php
			if($menu_flag=='notification_active_links'){
		?>
			
		<li><a class="<?php echo ($m=='low_stock_notification') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Orders/low_stock_notification">Low stock Notification<i class="" id="total_count_of_notification" aria-hidden="true"></i></a></li>
		<li><a class="<?php echo ($m=='request_for_out_of_stock') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Orders/request_for_out_of_stock">Notify Request</a></li>
		<li><a class="<?php echo ($m=='completed_request_for_out_of_stock') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Orders/completed_request_for_out_of_stock">Notified Request</a></li>
		<?php
			}
		?>
			
		<?php
			if($menu_flag=='active_order_active_links'){
		?> 	
		<li><a class="<?php echo ($m=='onhold_orders') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Orders/onhold_orders">Onhold Orders <span class="badge" id="total_onhold_orders"></span></a></li>
		<li><a class="<?php echo ($m=='approved_orders') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Orders/approved_orders">Approved Orders <span class="badge" id="total_approved_orders"></span></b></a></li>
		<li><a class="<?php echo ($m=='confirmed_orders') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Orders/confirmed_orders">Confirmed Orders <span class="badge" id="total_confirmed_orders"></span></a></li>
		<li><a class="<?php echo ($m=='packed_orders') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Orders/packed_orders">Packed Orders <span class="badge" id="total_packed_orders"></span></a></li>
		<li><a class="<?php echo ($m=='shipped_orders') ? 'active_left' : ''; ?>"  href="<?php echo base_url();?>admin/Orders/shipped_orders">Shipped Orders <span class="badge" id="total_shipped_orders"></span></a></li>
		<li><a class="<?php echo ($m=='cancelled_orders') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Orders/cancelled_orders">Cancelled orders <i class="" id="notify_refundcancels_menu" aria-hidden="true"></i></a></li>
		<?php
			}
		?>
		
		<?php
			if($menu_flag=='inactive_order_active_links'){
		?>
                <li><a class="<?php echo ($m=="delivered_orders" || $m=="delivered_orders_inventory_wise") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Orders/delivered_orders">Delivered Orders&nbsp;<b><span class="badge" id="total_delivered_orders"></span></b></a></li>
		<li><a class="<?php echo ($m=='returned_orders') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Orders/returned_orders">Returned Orders&nbsp;<b><span class="badge" id="total_returned_orders"></span></b></a></li>
		<li><a class="<?php echo ($m=='cancelled_orders_inactive') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Orders/cancelled_orders_inactive">Cancelled Orders&nbsp;<b><span class="badge" id="total_cancelled_inactive_orders"></span></b></a></li>
		<li><a class="<?php echo ($m=='shipped_charges') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Orders/shipped_charges">Shipping Charges</a></li>
		<?php
			}
		?>
			
		<?php
			if($menu_flag=='replacement_active_links'){
		?>
			
		<li><a class="<?php echo ($m=='returns_replacement_request_orders') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_replacement_request_orders">Incoming<i class="" id="total_unread_return_msg_replacement" aria-hidden="true"></i></a></li>
		
		<li><a class="<?php echo ($m=='returns_replacement_request_orders_awaiting') ? 'active_left' : ''; ?>"  href="<?php echo base_url();?>admin/Returns/returns_replacement_request_orders_awaiting">Awaiting <b><i class="" id="total_unread_return_msg_awaiting_replacement" aria-hidden="true"></i></b></a></li>
		
		<li><a class="<?php echo ($m=='returns_replacement_request_orders_customer_accepted') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_replacement_request_orders_customer_accepted">Customer accepted<i class="" id="total_unread_return_msg_customer_accepted_replacement" aria-hidden="true"></i></a></li>
		
		<li><a class="<?php echo ($m=='returns_replacement_request_orders_balance_clear') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_replacement_request_orders_balance_clear">Balance Clear<i class="" id="total_unread_return_msg_balance_clear_replacement" aria-hidden="true"></i></a></li>
		
		<li><a class="<?php echo ($m=='returns_replacement_request_orders_customer_rejected') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_replacement_request_orders_customer_rejected">Customer rejected<b><i class="" id="total_unread_return_msg_customer_rejected_replacement" aria-hidden="true"></i></b></a></li>
		
		<li><a class="<?php echo ($m=='returns_replacement_request_orders_admin_rejected') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_replacement_request_orders_admin_rejected">Admin rejected<i class="" id="total_unread_return_msg_admin_rejected_replacement" aria-hidden="true"></i></a></li>
		
		<li><a class="<?php echo ($m=='returns_replacement_request_orders_ready_to_replace') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_replacement_request_orders_ready_to_replace">Ready to replace<i class="" id="total_return_stock_ready_to_replace" aria-hidden="true"></i></a></li>
		
		<li><a class="<?php echo ($m=='return_stock_notification') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/return_stock_notification">Return Stock Notification<i class="" id="total_unread_return_msg_stock" aria-hidden="true"></i></a></li>
		
		<li><a class="<?php echo ($m=='returns_replacement_request_orders_refund_success') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_replacement_request_orders_refund_success">Partial Success<i class="" id="total_unread_return_msg_refund_success_partial_refund_replacement" aria-hidden="true"></i></a></li>
		
		<li><a class="<?php echo ($m=='returns_replacement_request_orders_refund_success_full_refund') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_replacement_request_orders_refund_success_full_refund">Full Success<i class="" id="total_unread_return_msg_refund_success_refund_replacement" aria-hidden="true"></i></a></li>
		
		<li><a class="<?php echo ($m=='returns_replacement_request_orders_replacement_cancels') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_replacement_request_orders_replacement_cancels">Replacement Cancels<i class="" id="total_unread_return_msg_replacement_cancel" aria-hidden="true"></i></a></li>
		
		<?php
			}
		?>
			
		<?php
			if($menu_flag=='refund_active_links'){
		?>
			
		<li><a class="<?php echo ($m=='returns_refund_request_orders') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_refund_request_orders">Incoming<i class="" id="total_unread_return_msg" aria-hidden="true"></i></a></li>
		
		<li><a class="<?php echo ($m=='returns_refund_request_orders_awaiting') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_refund_request_orders_awaiting">Awaiting<i class="" id="total_unread_return_msg_awaiting" aria-hidden="true"></i></a></li>
		
		<li><a class="<?php echo ($m=='returns_refund_request_orders_admin_rejected') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_refund_request_orders_admin_rejected">Admin rejected<i class="" id="total_unread_return_msg_admin_rejected" aria-hidden="true"></i></a></li>
		
		<li><a class="<?php echo ($m=='returns_refund_request_orders_customer_rejected') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_refund_request_orders_customer_rejected">Customer rejected<i class="" id="total_unread_return_msg_customer_rejected" aria-hidden="true"></i></a></li>
		
		<li><a class="<?php echo ($m=='returns_refund_request_orders_customer_accepted') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_refund_request_orders_customer_accepted">Customer accepted<i class="" id="total_unread_return_msg_customer_accepted" aria-hidden="true"></i></a></li>
		
		<li><a class="<?php echo ($m=='returns_refund_request_orders_refund') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_refund_request_orders_refund">Issue refund<i class="" id="total_need_to_be_refund" aria-hidden="true"></i></a></li>
		
		<li><a class="<?php echo ($m=='returns_refund_request_orders_refund_inprocess') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_refund_request_orders_refund_inprocess">Refund Processing<i class="" id="total_refund_in_process" aria-hidden="true"></i></a></li>
		
		<li><a class="<?php echo ($m=='returns_refund_request_orders_refund_success') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_refund_request_orders_refund_success">Partial Success<i class="" id="total_unread_return_msg_refund_success_partial_refund" aria-hidden="true"></i></a></li>
		
		<li><a class="<?php echo ($m=='returns_refund_request_orders_refund_success_full_refund') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_refund_request_orders_refund_success_full_refund">Full Success<i class="" id="total_success_full_refund" aria-hidden="true"></i></a></li>
		
		<li><a class="<?php echo ($m=='returns_refund_request_orders_refund_again') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_refund_request_orders_refund_again">Issue refund Again</a></li>
		
		<li><a class="<?php echo ($m=='returns_refund_request_orders_refund_failure') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_refund_request_orders_refund_failure">Refund Failure<i class="" id="total_unread_return_msg_refund_failure" aria-hidden="true"></i></a></li>
		<?php
			}
		?>
			
		<?php
			if($menu_flag=='accounts_refund_active_links'){
		?>
			
		<li><a class="<?php echo ($m=='returns_refund_request_orders_accounts_incoming') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_refund_request_orders_accounts_incoming">Refunds Incoming</a></li>
		<li><a class="<?php echo ($m=='returns_refund_request_orders_accounts_success') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_refund_request_orders_accounts_success">Refunds Success<b></b></a></li>
		<li><a class="<?php echo ($m=='returns_refund_request_orders_accounts_failure') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_refund_request_orders_accounts_failure">Refunds Failure<b></b></a></li>
		<li><a class="<?php echo ($m=='returns_refund_request_orders_accounts_incoming_again') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_refund_request_orders_accounts_incoming_again">Refunds Incoming again <i class="" id="total_refund_incoming_again_full_refund" aria-hidden="true"></i></a></li>
		
		<?php
			}
		?>
			
		<?php
			if($menu_flag=='accounts_cancel_active_links'){
		?>
		<li><a class="<?php echo ($m=='cancels_refund_request_orders_accounts_incoming') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Orders/cancels_refund_request_orders_accounts_incoming">Cancel Incoming<b></b></a></li>
		<li><a class="<?php echo ($m=='cancels_refund_request_orders_accounts_success') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Orders/cancels_refund_request_orders_accounts_success">Cancel Success<b></b></a></li>
		<li><a class="<?php echo ($m=='cancels_refund_request_orders_accounts_failure') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Orders/cancels_refund_request_orders_accounts_failure">Cancel Failure<b></b></a></li>
	
		<?php
			}
		?>
			
		<?php
			if($menu_flag=='accounts_replacement_active_links'){
		?>
			
		<li><a class="<?php echo ($m=='returns_replacement_request_orders_accounts_incoming') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_replacement_request_orders_accounts_incoming">Replacement Incoming <b></b></a></li>
		<li><a class="<?php echo ($m=='returns_replacement_request_orders_accounts_success') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/returns_replacement_request_orders_accounts_success">Replacement Success <b></b></a></li>
		<?php
			}
		?>
			
		<?php
			if($menu_flag=='accounts_banktransfer_active_links'){
		?>
		<li><a class="<?php echo ($m=='customer_wallet_bank_transfer_accounts_incoming') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Manage_cust/customer_wallet_bank_transfer_accounts_incoming">Bank Transfer(Incoming)<b></b></a></li>
		
		<li><a class="<?php echo ($m=='customer_wallet_bank_transfer_accounts_success') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Manage_cust/customer_wallet_bank_transfer_accounts_success">Bank Transfer(Success)<b></b></a></li>
		
		<li><a class="<?php echo ($m=='customer_wallet_bank_transfer_accounts_failure') ? 'active_left' : ''; ?>"  href="<?php echo base_url();?>admin/Manage_cust/customer_wallet_bank_transfer_accounts_failure">Bank Transfer(Failure) <b></b></a></li>
		
		<?php
			}
		?>
			
		<?php
			if($menu_flag=='accounts_invoice_active_links'){
		?>
		
		<li><a class="<?php echo ($m=='invoice_offers_request_for_refund') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/invoice_offers_request_for_refund"> Promotion Invoice refund <b></b></a></li>
		<li><a class="<?php echo ($m=='order_item_offers_request_for_refund') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Returns/order_item_offers_request_for_refund">Promotion Order Item refund <i class="" id="total_promotion_order_item_refund" aria-hidden="true"></i><b></b></a></li>
		<?php
			}
		?>
		
		<?php if($menu_flag=='coupon'){ ?>
                	<li><a class="<?php echo ($m=='manage_coupon') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Coupon/manage_coupon"> Manage Coupon </a></li>
                	<li><a class="<?php echo ($m=='add_coupon') ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Coupon/add_coupon"> Create Coupon </a></li>
                
                <?php } ?>
                
		<?php
			if($menu_flag=='analytics_orders'){
		?>
			
		<li><a onclick="getOrders(this)" report="dailyPurchases" style="cursor:pointer;" id="active_orders_link">Active Orders</a></li>
		<li><a onclick="getOrders(this)" report="dailyPurchasesComplete" style="cursor:pointer;"> Delivered Orders</a></li>
		<li><a onclick="getOrders(this)" report="cancelledOrdersNew" style="cursor:pointer;">Cancelled Orders</a></li>
		<li><a onclick="getOrders(this)" report="purchaseTrend" style="cursor:pointer;">Purchase Trends</a></li>
		<li><a onclick="getOrders(this)" report="paymentMethods" style="cursor:pointer;">Payment Methods</a></li>
		<li><a onclick="getOrders(this)" report="ordersStages" style="cursor:pointer;">Orders Snapshort</a></li>
		            
<script>
    $(window).load(function() {
       // $('#wrapper').removeClass('toggled');
       // $('#orders li :eq(0)').click()
	   $("#active_orders_link").click();
    })
</script>
		<?php
			}
		?>
		
		<?php
			if($menu_flag=='analytics_returns'){
		?>
			
		<li><a onclick="getReturns(this)" report="refund_cases" style="cursor:pointer;" id="refund_cases_link">Refund Cases</a></li>
                    <li><a onclick="getReturns(this)" report="replacement_cases" style="cursor:pointer;">Replacement Cases</a></li>
		<script>
    $(window).load(function() {
       // $('#wrapper').removeClass('toggled');
       // $('#orders li :eq(0)').click()
	   $("#refund_cases_link").click();
    })
</script>
		<?php
			}
		?>
		
		<?php
			if($menu_flag=='analytics_promotions'){
		?>
			
		<li><a onclick="getPromotions(this)" report="number_of_promotions" style="cursor:pointer;"  id="running_promotions_link">Running Promotions</a></li>
                    <li><a onclick="getPromotions(this)" report="promotions_per_month" style="cursor:pointer;">Promotions Per Month</a></li>
                    <li><a onclick="getPromotions(this)" report="popular_promotions" style="cursor:pointer;">Popular promotions</a></li>
		<script>
    $(window).load(function() {
       // $('#wrapper').removeClass('toggled');
       // $('#orders li :eq(0)').click()
	   $("#running_promotions_link").click();
    })
</script>
		<?php
			}
		?>
		
		<?php
			if($menu_flag=='analytics_customers'){
		?>
			
		<li><a onclick="getCustomer(this)" report="customer_reg" style="cursor:pointer;"  id="customer_reg_link">Customer Registrations</a></li>
                    <li><a onclick="getCustomer(this)" report="customer_gen" style="cursor:pointer;">Customer Gender</a></li>
                   <!-- <li><a onclick="getCustomer(this)" report="customer_age" style="cursor:pointer;">Customer Age</a></li>-->
                    <li><a onclick="getCustomer(this)" report="customer_price_segmentation" style="cursor:pointer;">Customer Price Segmentation</a></li>
		<script>
    $(window).load(function() {
       // $('#wrapper').removeClass('toggled');
       // $('#orders li :eq(0)').click()
	   $("#customer_reg_link").click();
    })
</script>
		<?php
			}
		?>
		
		<!-------------------- oct 9 2022 newly added starts------------>
		<?php
		if($menu_flag=="vendor_analytics_customers"){
		?>
		<!-- <li><a onclick="VendorGetCustomer(this)" report="vendor_customer_reg" style="cursor:pointer;"  id="vendor_customer_reg_link">Customer Registrations</a></li>
		<li><a onclick="VendorGetCustomer(this)" report="vendor_customer_gender" style="cursor:pointer;"  id="vendor_customer_gender">Customer Gender</a></li> -->
		<li><a onclick="VendorGetCustomer(this)" report="vendor_customer_age" id="vendor_customer_age_link" style="cursor:pointer;">Customer Age</a></li>
		<li><a onclick="VendorGetCustomer(this)" report="vendor_customer_revenue" style="cursor:pointer;">Customer Revenue</a></li>
		<li><a onclick="VendorGetCustomer(this)" report="vendor_customer_revenue_gender" style="cursor:pointer;">Customer Revenue Gender</a></li>
		<li><a onclick="VendorGetCustomer(this)" report="vendor_customer_subscribed" style="cursor:pointer;">Customer Subscribed</a></li>
		<li><a onclick="VendorGetCustomer(this)" report="vendor_customer_sustainability" style="cursor:pointer;">Customer Sustainability</a></li>
		<li><a onclick="VendorGetCustomer(this)" report="vendor_customer_life_cycle" style="cursor:pointer;">Customer Life Cycle</a></li>
		
		<script>
			$(window).load(function() {
				$("#vendor_customer_age_link").click();
			})
		</script>

		<?php
			}
		if($menu_flag=="vendor_abandoned_cart_analytics"){
				?>
			
			<li><a onclick="VendorGetCustomer(this)" report="vendor_abandoned_cart_weekly" id="vendor_abandoned_cart_link" style="cursor:pointer;">Cart Weekly</a></li>
			<li><a onclick="VendorGetCustomer(this)" report="vendor_abandoned_cart_clearance" style="cursor:pointer;">Cart Clearance View</a></li>
			
			<script>
				$(window).load(function() {
					$("#vendor_abandoned_cart_link").click();
				});
			</script>
	
			<?php
				}
			?>

			<?php 
			if($menu_flag=="vendor_visitors_traffic"){
				?>
<!-- 			
			<li><a onclick="VendorGetCustomer(this)" report="visitor_traffic" id="visitor_traffic_link" style="cursor:pointer;">Total Vistors</a></li>
			<li><a onclick="VendorGetCustomer(this)" report="vistors_request_pages" style="cursor:pointer;">Traffic Request Pages</a></li> -->
			<li><a onclick="VendorGetCustomer(this)" report="total_visitor" id="total_visitor_link" style="cursor:pointer;">Total Visitors Traffic</a></li>
			<li><a onclick="VendorGetCustomer(this)" report="total_visitor_pagewise" style="cursor:pointer;">Total Visitors Traffic Pagewise </a></li>
			<li><a onclick="VendorGetCustomer(this)" report="total_visitor_pagewise_top" style="cursor:pointer;">Top Visited Pages </a></li>
			<li><a onclick="VendorGetCustomer(this)" report="total_visitor_pagewise_bottom" style="cursor:pointer;">Bottom Visited Pages </a></li>
			
			<script>
				$(window).load(function() {
					$("#total_visitor_link").click();
				});
			</script>
	
			<?php
				}
			?>

<?php 
			if($menu_flag=="vendor_inventory_view"){
				?>
			
			<li><a onclick="VendorGetCustomer(this)" report="visitor_top_viewed" id="visitor_top_viewed_link" style="cursor:pointer;">Top viewed Inventory</a></li>
			<li><a onclick="VendorGetCustomer(this)" report="vistors_bottom_viewed" style="cursor:pointer;">Bottom Viewed</a></li>
			
			<script>
				$(window).load(function() {
					$("#visitor_top_viewed_link").click();
				});
			</script>
	
			<?php
				}
			?>
			
			
			
			<!-------------------- oct 9 2022 newly added ends------------>
			
		
		<?php
			if($menu_flag=='analytics_inventory'){
		?>
			
		 <li><a onclick="getProducts(this,'searchedkeyword_inventory')" report="searched_keyword" style="cursor:pointer;"  id="searched_keyword_link">Searched Keyword</a></li>
		 <!--------------This block I modified for samrick 6/7/2021-->
                    <!--<li><a onclick="getProducts(this)" report="parent_category" style="cursor:pointer;">Parent Category</a></li>-->
                    <li><a onclick="getProducts(this,'category_inventory')" report="category" style="cursor:pointer;"> Category</a></li>
                    <li><a onclick="getProducts(this,'subcategory_inventory')" report="sub_category" style="cursor:pointer;">Sub Category</a></li>
                    <li><a onclick="getProducts(this,'brand_inventory')" report="brand" style="cursor:pointer;">Brands</a></li>
                    <li><a onclick="getProducts(this,'products_inventory')" report="products" style="cursor:pointer;">Products</a></li>

                    <li><a onclick="getProducts(this,'mostpurchased_inventory')" report="most_purchased" style="cursor:pointer;">Most Purchased</a></li>

                    <li><a onclick="getProducts(this,'ratings_inventory')" report="ratings" style="cursor:pointer;">Ratings</a></li>
		<script>
    $(window).load(function() {
       // $('#wrapper').removeClass('toggled');
       // $('#orders li :eq(0)').click()
	   $("#searched_keyword_link").click();
    })
</script>
		<?php
			}
		?>
		
		<?php
			if($menu_flag=='analytics_visitor_clicks'){
		?>
			
		 <li><a onclick="getPagesViewed(this)" report="home" style="cursor:pointer;"  id="analytics_visitor_clicks_link">Home</a></li>
                    <!--<li><a onclick="getPagesViewed(this)" report="category" style="cursor:pointer;">Category</a></li> -->
                    <li><a onclick="getPagesViewed(this)" report="product" style="cursor:pointer;">Products</a></li> 
                    <li><a onclick="getPagesViewed(this)" report="button" style="cursor:pointer;">Button Activity</a></li> 
                    <li><a onclick="getPagesViewed(this)" report="reviews" style="cursor:pointer;">Reviews read</a></li> 
		<script>
    $(window).load(function() {
       // $('#wrapper').removeClass('toggled');
       // $('#orders li :eq(0)').click()
	   $("#analytics_visitor_clicks_link").click();
    })
</script>
		<?php
			}
		?>
		
		
		
    </ul>
</div>
		
		
<nav class="navbar navbar-info navbar-fixed-top">
<div class="container-fluid">
<?php

if($this->session->flashdata("notification"))
{
	echo "<script type='text/javascript'> alert(\"".$this->session->flashdata("notification")."\"); </script>";	
}
?>

    <?php
	  /* && $admin_user_type=="Master Country" */
	if($admin_user_type!="finance"){
        ?>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#menu-toggle" id="menu-toggle"><span class="glyphicon glyphicon-list" aria-hidden="true"></span></a>
        </div>
        <div class="overlay"></div>

        <?php
        }
	
        if($admin_user_type!="finance" && $admin_user_type=="Master Country" && $admin_module_type==""){
        ?>
	<!--- main menu ---->
        <ul class="nav navbar-nav">

		
		<li class="dropdown"><a  class="<?php echo ($menu_flag=='catalog_active_links' || $menu_flag=='catalog_active_links_archived' || $menu_flag=='shipping_active_links' || $menu_flag=='reasons_active_links' || $menu_flag=='notification_active_links') ? 'active2' : ''; ?>"  href="#" data-toggle="dropdown" class="dropdown-toggle">Catalog <i  class="" id="catalog_menu" aria-hidden="true"></i> <b class="caret"></b></a>
			<ul class="dropdown-menu">
				

				<li><a class="animsition-link <?php echo ($m=='vendor_inventory') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Catalogue/vendor_inventory">Vendor Catalog <i class="fa fa-bell-o" id="vendor_catalog_menu" aria-hidden="true"></i></a></li>


			<li><a class="<?php echo ($m=="admin_settings") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/admin_settings">Admin Settings </a></li>
			<li><a class="<?php echo ($m=="inventory_all") ? 'active_left' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/inventory_all">All Inventory / Tag Inventory</a></li>
			
				<li><a class="animsition-link <?php echo ($m=='parent_category') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Catalogue/parent_category">Active</a></li>
				<li><a class="<?php echo ($m=='archived_parent_category') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Catalogue/archived_parent_category">Archived</a></li>
				<li><a class="<?php echo ($m=='vendors') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Catalogue/vendors"> Shipping</a></li>
				<li><a class="<?php echo ($m=='reasons_filter') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Catalogue/reasons_filter">Reasons</a></li>
				<li class="dropdown"><a class="<?php echo ($m=='low_stock_notification') ? 'active_drop' : '' ?>"  href="<?php echo base_url();?>admin/Orders/low_stock_notification">Stock Notification<span id="Notification_symbol"></span></a></li>     
			</ul>
        </li>
		
		<li class="dropdown"><a class="<?php echo ($menu_flag=="customer_view" || $menu_flag=="product_vendors_view" || $menu_flag=="customer_wallet_bank_transfer" || $menu_flag=="customer_profile" || $menu_flag=="customer_profile_values" || $menu_flag=="franchise" || $menu_flag=="design_request") ? 'active2' : ''; ?>" href="#" data-toggle="dropdown" class="dropdown-toggle">Customers <i class="fa fa-bell-o" id="customers_menu" aria-hidden="true"></i><b class="caret"></b></a>
			<ul class="dropdown-menu">
                <li><a class="<?php echo ($menu_flag=='customer_view') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Manage_cust">Customer Profile</a></li>
				
				<li><a class="<?php echo ($menu_flag=='product_vendors_view') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Catalogue/product_vendors">Product Vendors</a></li>

                <li><a class="<?php echo ($m=='customer_wallet_bank_transfer' || $menu_flag=="customer_wallet_bank_transfer" ) ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Manage_cust/customer_wallet_bank_transfer">Bank Transfer Request <i class="" id="bank_transfer_request_menu" aria-hidden="true"></i></a></li>
				<li><a class="<?php echo ($m=='customer_profile' || $menu_flag=="customer_profile") ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Manage_cust/customer_profile">Create Profile Name</a></li>
				<li><a class="<?php echo ($m=='customer_profile_values' || $menu_flag=="customer_profile_values") ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Manage_cust/customer_profile_values">Create Profile Values</a></li>
				<li><a class="<?php echo ($menu_flag=='request_quote') ? 'active_drop' : ''; ?>" href="<?php echo base_url();?>admin/Admin_manage/request_quote">Requests for Quote<i class="" id="" aria-hidden="true"></i></a></li>

				<li><a class="<?php echo ($menu_flag=='become_a_reseller') ? 'active_drop' : ''; ?>" href="<?php echo base_url();?>admin/Admin_manage/become_a_reseller">Become A Seller<i class="" id="" aria-hidden="true"></i></a></li>
				
				<li><a class="<?php echo ($menu_flag=='design_request') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Manage_cust/design_request">Design Request</a></li>
				<li><a class="<?php echo ($menu_flag=='franchise') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Franchise/franchises">Franchise</a></li>

				
			</ul>
		</li>
				
		<li class="dropdown"><a class="<?php echo ($menu_flag=="active_order_active_links" || $menu_flag=="inactive_order_active_links" || $menu_flag=="invoices_free_items_status") ? 'active2' : ''; ?>" href="#" data-toggle="dropdown" class="dropdown-toggle">Orders <i class="fa fa-bell-o" id="notify_orders_menu" aria-hidden="true"></i> <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li><a class="<?php echo ($menu_flag=='active_order_active_links') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Orders/onhold_orders">Active Orders <i class="" id="notify_active_menu" aria-hidden="true"></i></a></li>
				<li><a class="<?php echo ($menu_flag=='inactive_order_active_links') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Orders/delivered_orders">In-active Orders</a></li>
				
				<li><a class="<?php echo ($menu_flag=='failed_order_active_links') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Orders/failed_orders">Failed Orders</a></li>

				<li><a class="<?php echo ($menu_flag=='invoices_free_items_status') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Orders/invoices_free_items_status">Free items on invoices <i class="" id="notify_freeitem_menu" aria-hidden="true"></i></a></li>
			</ul>
        </li>
				
		<li class="dropdown"><a class="<?php echo ($menu_flag=="replacement_active_links" || $menu_flag=="refund_active_links" || $menu_flag=="returns_cancelled_orders") ? 'active2' : ''; ?>" href="#" data-toggle="dropdown" class="dropdown-toggle">Returns <i class="fa fa-bell-o" id="notify_returns_menu" aria-hidden="true"></i><b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li><a class="<?php echo ($menu_flag=="replacement_active_links") ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Returns/returns_replacement_request_orders">Replacements <i class="" id="notify_return_replacement_menu" aria-hidden="true"></i></a></li>
				<li><a class="<?php echo ($menu_flag=="refund_active_links") ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Returns/returns_refund_request_orders">Refunds <i class="" id="notify_return_refund_menu" aria-hidden="true"></i></a></li>
				<li><a class="<?php echo ($menu_flag=="returns_cancelled_orders") ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Returns/returns_cancelled_orders">Cancelled Returns<i class="" id="notify_return_cancel_menu" aria-hidden="true"></i></a></li>
			</ul>
        </li>
		
		<li class="dropdown"><a class="<?php echo ($menu_flag=="accounts_refund_active_links" || $menu_flag=="accounts_cancel_active_links" || $menu_flag=="accounts_replacement_active_links" || $menu_flag=="accounts_banktransfer_active_links" || $menu_flag=="accounts_invoice_active_links" || $menu_flag=="emioptions_list") ? 'active2' : ''; ?>" href="#" data-toggle="dropdown" class="dropdown-toggle">Accounts <i class="fa fa-bell-o" id="notify_accounts_menu" aria-hidden="true"></i> <b class="caret"></b></a>
		<ul class="dropdown-menu">
			<li><a class="<?php echo ($m=='returns_refund_request_orders_accounts_incoming' || $menu_flag=="accounts_refund_active_links") ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Returns/returns_refund_request_orders_accounts_incoming">Refunds <i class="" id="notify_account_refund_menu" aria-hidden="true"></i></a></li>
			<li><a class="<?php echo ($m=='cancels_refund_request_orders_accounts_incoming' || $menu_flag=="accounts_cancel_active_links") ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Orders/cancels_refund_request_orders_accounts_incoming">Cancel <i class="" id="notify_account_cancel_menu" aria-hidden="true"></i></a></li>
			<li><a class="<?php echo ($m=='returns_replacement_request_orders_accounts_incoming' || $menu_flag=="accounts_replacement_active_links") ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Returns/returns_replacement_request_orders_accounts_incoming">Replacement <i class="" id="notify_account_replacement_menu" aria-hidden="true"></i></a></li>
			<li><a class="<?php echo ($m=='customer_wallet_bank_transfer_accounts_incoming' || $menu_flag=="accounts_banktransfer_active_links") ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Manage_cust/customer_wallet_bank_transfer_accounts_incoming">Bank Transfer <i class="" id="notify_account_bank_transfer_menu" aria-hidden="true"></i></a></li>
			<li><a class="<?php echo ($m=='invoice_offers_request_for_refund'|| $menu_flag=="accounts_invoice_active_links") ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Returns/invoice_offers_request_for_refund">Promotion <i class="" id="notify_account_promotion_menu" aria-hidden="true"></i></a></li>		
			<li><a class="<?php echo ($m=='emioptions_list' || $menu_flag=="emioptions_list") ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/EMIoptions/emioptions_list">EMI Options</a></li>					
				
        </ul>
        </li>
		
		<li class="dropdown"><a class="<?php echo ($menu_flag=="rated_inventory" || $menu_flag=="ratings_for_service" ) ? 'active2' : ''; ?>" href="#" data-toggle="dropdown" class="dropdown-toggle">Ratings<b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li><a class="<?php echo ($m=='rated_inventory' || $menu_flag=="rated_inventory") ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Ratemanaging/rated_inventory">Product Rating</a></li>
				<li><a class="<?php echo ($m=='ratings_for_service' || $menu_flag=="ratings_for_service") ? 'active_drop' : '' ?>"  href="<?php echo base_url();?>admin/Ratemanaging/ratings_for_service">Service Rating</a></li>
			</ul>
		</li>
      
               
		<li class="dropdown" onclick="get_all_num_data_of_all_promotion()"><a class="<?php echo ($c=="Promotions") ? 'active2' : ''; ?>" href="#" data-toggle="dropdown" class="dropdown-toggle">Promotions <b class="caret"></b></a>
	        <ul class="dropdown-menu">
			<li><a class="<?php echo ($m=='create_promotions') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/create_promotions"> Create Promotions</a></li>
			<li><a class="<?php echo ($m=='draft_promotions') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/draft_promotions"> Draft Promotions <!--<span  id="draft_promotion_num" class="badge badge-yellow"></span>--></a></li>
			<li><a class="<?php echo ($m=='promotion_pending_approval') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/promotion_pending_approval"> Pending Approval <!--<span  id="pending_promotion_num" class="badge badge-yellow" ></span>--></a></li>
			<li><a class="<?php echo ($m=='approved_promotions') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/approved_promotions"> Approved Promotions <!--<span  id="approved_promotion_num" class="badge badge-yellow" ></span>--></a></li>
			<li><a class="<?php echo ($m=='active_promotions') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/active_promotions"> Active Promotions <!--<span  id="active_promotion_num" class="badge badge-yellow" ></span>--></a></li>
			<li><a class="<?php echo ($m=='deactive_promotions') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/deactive_promotions"> Deactivated Promotions <!--<span  id="deactivated_promotion_num" class="badge badge-yellow" ></span>--></a></li>
			<li><a class="<?php echo ($m=='revertedDraft_promotions') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/revertedDraft_promotions"> Reverted Draft Promotions <!--<span  id="revertdraft_promotion_num" class="badge badge-yellow" ></span>--></a></li>
			<li><a class="<?php echo ($m=='rejected_promotions') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/rejected_promotions"> Rejected Promotions <!--<span  id="rejected_promotions_num" class="badge badge-yellow" ></span>--></a></li>
			<li><a class="<?php echo ($m=='expired_promotions') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/expired_promotions"> Expired Promotions <!--<span  id="expired_promotion_num" class="badge badge-yellow" ></span>--></a></li>
			
	        </ul>
	    </li>
      
		

		<li class="dropdown"><a class="<?php echo ($c=="Analytics") ? 'active2' : ''; ?>" href="#" data-toggle="dropdown" class="dropdown-toggle">Analytics <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li><a class="<?php echo ($m=='dashboard') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Analytics/dashboard">Dashboard </a></li>
				<li><a class="<?php echo ($m=='orders') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Analytics/orders">Orders </a></li>
				<li><a class="<?php echo ($m=='customers') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Analytics/customers">Customers </a></li>
				<li><a class="<?php echo ($m=='inventory') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Analytics/inventory">Inventory </a></li>
				<!-- <li><a class="<?php echo ($m=='visitor_clicks') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Analytics/visitor_clicks">Visitor Clicks </a></li> -->
				<li><a class="<?php echo ($m=='get_businessscore') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Analytics/get_businessscore">Get Business Score </a></li>
				
				
				<!-- newly added starts --->
				
				<li><a class="<?php echo ($menu_flag=='vendor_analytics_customers') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Analytics/vendor_customers">Customers </a></li>
				<li><a class="<?php echo ($menu_flag=='vendor_abandoned_cart_analytics') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Analytics/vendor_abandoned_cart">Abandoned Cart </a></li>
				<li><a class="<?php echo ($menu_flag=='vendor_visitors_traffic') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Analytics/vendor_visitors_traffic">Traffic</a></li>
				<li><a class="<?php echo ($menu_flag=='vendor_inventory_view') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Analytics/vendor_inventory_view">SKU Views</a></li>
				
				
				<!-- newly added ends --->
				
				
				<?php
				/*
				?>
				<li><a href="<?php echo base_url();?>admin/Analytics/returns">Returns </a></li>
				<li><a href="<?php echo base_url();?>admin/Analytics/promotions">Promotions </a></li>
				<?php
				*/
				?>
				<li><a href="#" style="color:#e1e1e1">Returns </a></li>
				<li><a href="#" style="color:#e1e1e1">Promotions </a></li>
			</ul>
        </li>
		
		
		
		<li class="dropdown"><a class="<?php echo ($c=="Complaint_manage" || $c=="Policys" || $c=="Recomendations" || $c=="Ui_content" || $c=="Pricetobeshown" || $c=="Trackingaddtocartcheckout" || $c=="Coupon") ? 'active2' : ''; ?>" href="#" data-toggle="dropdown" class="dropdown-toggle">Others <b class="caret"></b></a>
			<ul class="dropdown-menu">
						<!-- complaints starts --->
						<li><a class="<?php echo ($c=='Complaint_manage') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Complaint_manage">All Complaints <span  id="show1" class="badge" style="background-color:red"></span></a></li>
						<!-- complaints ends --->
			
                                <!---coupon-->
                                <li><a class="<?php echo ($c=="Coupon") ? 'active_drop' : ''; ?>" href="<?php echo base_url();?>admin/Coupon/manage_coupon" >Coupon</a></li>
                        
                                <!---coupon-->       
				<li><a class="<?php echo ($c=='Policys') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Policys">Policys</a></li>
				<li><a class="<?php echo ($c=='Recomendations') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Recomendations">Recomendations </a></li>
				<li><a class="animsition-link <?php echo ($m=='common_page') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Ui_content/common_page">Index Page</a></li>
				<li><a class="animsition-link <?php echo ($m=='blogs') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Ui_content/blogs">Blogs</a></li>
				<li><a class="animsition-link <?php echo ($m=='all_site_newsletter') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Ui_content/all_site_newsletter">News Letters</a></li>
				<li><a class="<?php echo ($c=='Pricetobeshown') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Pricetobeshown">Price To be Shown</a></li>
                                <li><a class="<?php echo ($c=='Trackingaddtocartcheckout') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Trackingaddtocartcheckout">Tracking Details/Checkout Pages</a></li>
								
								
								
								
								
			</ul>
        </li>
		
	
		
		
        <li class="dropdown"><a href="" data-toggle="dropdown" class="dropdown-toggle">Logout

            <?php
            if($this->session->userdata("logged_in")){
                     echo $this->session->userdata("user");
            }
                    ?> <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">

                <li id="logout"><a href='<?php echo base_url();?>admin/Adminout'>Logout</a></li>
            </ul>
        </li>
        </ul>
	<!--- main menu ---->
 <?php
        }else if($admin_user_type!="finance" && $admin_user_type=="Su Master Country" && $admin_module_type=="catalog"){
			?>
		 <ul class="nav navbar-nav">
		<li class="dropdown"><a  class="<?php echo ($menu_flag=='catalog_active_links' || $menu_flag=='catalog_active_links_archived' || $menu_flag=='shipping_active_links' || $menu_flag=='reasons_active_links' || $menu_flag=='notification_active_links') ? 'active2' : ''; ?>"  href="#" data-toggle="dropdown" class="dropdown-toggle">Catalog <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li><a class="animsition-link <?php echo ($m=='inventory_filter') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Catalogue/inventory_filter">Active</a></li>
			</ul>
        </li>
		
		<li class="dropdown"><a class="<?php echo ($menu_flag=="customer_view" || $menu_flag=="customer_wallet_bank_transfer" || $menu_flag=="customer_profile" || $menu_flag=="customer_profile_values" || $menu_flag=="request_quote" ) ? 'active2' : ''; ?>" href="#" data-toggle="dropdown" class="dropdown-toggle">Customers <i class="fa fa-bell-o" id="customers_menu" aria-hidden="true"></i><b class="caret"></b></a>
			<ul class="dropdown-menu">
                <li><a class="<?php echo ($menu_flag=='customer_view') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Manage_cust">Customer Profile</a></li>
                <li><a class="<?php echo ($m=='customer_wallet_bank_transfer' || $menu_flag=="customer_wallet_bank_transfer" ) ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Manage_cust/customer_wallet_bank_transfer">Bank Transfer Request <i class="" id="bank_transfer_request_menu" aria-hidden="true"></i></a></li>
				<li><a class="<?php echo ($m=='customer_profile' || $menu_flag=="customer_profile") ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Manage_cust/customer_profile">Create Profile Name</a></li>
				<li><a class="<?php echo ($m=='customer_profile_values' || $menu_flag=="customer_profile_values") ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Manage_cust/customer_profile_values">Create Profile Values</a></li>
				<li><a class="<?php echo ($menu_flag=='request_quote') ? 'active_drop' : ''; ?>" href="<?php echo base_url();?>admin/Admin_manage/request_quote">Requests for Quote<i class="" id="" aria-hidden="true"></i></a></li>
			</ul>
		</li>
				
		<li class="dropdown"><a class="<?php echo ($menu_flag=="active_order_active_links" || $menu_flag=="inactive_order_active_links" || $menu_flag=="invoices_free_items_status") ? 'active2' : ''; ?>" href="#" data-toggle="dropdown" class="dropdown-toggle">Orders <i class="fa fa-bell-o" id="notify_orders_menu" aria-hidden="true"></i> <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li><a class="<?php echo ($menu_flag=='active_order_active_links') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Orders/onhold_orders">Active Orders <i class="" id="notify_active_menu" aria-hidden="true"></i></a></li>
				<li><a class="<?php echo ($menu_flag=='inactive_order_active_links') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Orders/delivered_orders">Delivered/In-active Orders</a></li>
				
				<li><a class="<?php echo ($menu_flag=='invoices_free_items_status') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Orders/invoices_free_items_status">Free items on invoices <i class="" id="notify_freeitem_menu" aria-hidden="true"></i></a></li>
			</ul>
        </li>
		
		
		
        <li class="dropdown"><a href="" data-toggle="dropdown" class="dropdown-toggle">Logout

            <?php
            if($this->session->userdata("logged_in")){
                     echo $this->session->userdata("user");
            }
                    ?> <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">

                <li id="logout"><a href='<?php echo base_url();?>admin/Adminout'>Logout</a></li>
            </ul>
        </li>
        </ul>
		
		
		
			<?php
		}
		elseif($admin_user_type=="Su Master Country" && $admin_module_type=='catalog_manage'){
                    /* view and edit */
            ?>
        <ul class="nav navbar-nav">
		<li class="dropdown"><a  class="<?php echo ($menu_flag=='catalog_active_links' || $menu_flag=='catalog_active_links_archived' || $menu_flag=='shipping_active_links' || $menu_flag=='reasons_active_links' || $menu_flag=='notification_active_links') ? 'active2' : ''; ?>"  href="#" data-toggle="dropdown" class="dropdown-toggle">Catalog <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li><a class="animsition-link <?php echo ($m=='parent_category') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Catalogue/parent_category">Active</a></li>
			</ul>
        </li>
		
		<li class="dropdown"><a class="<?php echo ($menu_flag=="customer_view" || $menu_flag=="customer_wallet_bank_transfer" || $menu_flag=="customer_profile" || $menu_flag=="customer_profile_values") ? 'active2' : ''; ?>" href="#" data-toggle="dropdown" class="dropdown-toggle">Customers <i class="fa fa-bell-o" id="customers_menu" aria-hidden="true"></i><b class="caret"></b></a>
			<ul class="dropdown-menu">
                <li><a class="<?php echo ($menu_flag=='customer_view') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Manage_cust">Customer Profile</a></li>
                <li><a class="<?php echo ($m=='customer_wallet_bank_transfer' || $menu_flag=="customer_wallet_bank_transfer" ) ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Manage_cust/customer_wallet_bank_transfer">Bank Transfer Request <i class="" id="bank_transfer_request_menu" aria-hidden="true"></i></a></li>
				<li><a class="<?php echo ($m=='customer_profile' || $menu_flag=="customer_profile") ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Manage_cust/customer_profile">Create Profile Name</a></li>
				<li><a class="<?php echo ($m=='customer_profile_values' || $menu_flag=="customer_profile_values") ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Manage_cust/customer_profile_values">Create Profile Values</a></li>
			</ul>
		</li>
				
		<li class="dropdown"><a class="<?php echo ($menu_flag=="active_order_active_links" || $menu_flag=="inactive_order_active_links" || $menu_flag=="invoices_free_items_status") ? 'active2' : ''; ?>" href="#" data-toggle="dropdown" class="dropdown-toggle">Orders <i class="fa fa-bell-o" id="notify_orders_menu" aria-hidden="true"></i> <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li><a class="<?php echo ($menu_flag=='active_order_active_links') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Orders/onhold_orders">Active Orders <i class="" id="notify_active_menu" aria-hidden="true"></i></a></li>
				<li><a class="<?php echo ($menu_flag=='inactive_order_active_links') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Orders/delivered_orders">Delivered Orders</a></li>
				<li><a class="<?php echo ($menu_flag=='failed_order_active_links') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Orders/failed_orders">Failed Orders</a></li>
				<li><a class="<?php echo ($menu_flag=='invoices_free_items_status') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Orders/invoices_free_items_status">Free items on invoices <i class="" id="notify_freeitem_menu" aria-hidden="true"></i></a></li>
			</ul>
        </li>
		
		
		
        <li class="dropdown"><a href="" data-toggle="dropdown" class="dropdown-toggle">Logout

            <?php
            if($this->session->userdata("logged_in")){
                     echo $this->session->userdata("user");
            }
                    ?> <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">

                <li id="logout"><a href='<?php echo base_url();?>admin/Adminout'>Logout</a></li>
            </ul>
        </li>
        </ul>
		
		
        <?php
		}elseif($admin_user_type=="vendor"){
			?>
<ul class="nav navbar-nav">
			<li class="dropdown"><a  class="<?php echo ($menu_flag=='catalog_active_links' || $menu_flag=='catalog_active_links_archived' || $menu_flag=='shipping_active_links' || $menu_flag=='reasons_active_links' || $menu_flag=='notification_active_links') ? 'active2' : ''; ?>"  href="#" data-toggle="dropdown" class="dropdown-toggle">Catalog <b class="caret"></b></a>
				<ul class="dropdown-menu">
				
				<li><a class="animsition-link <?php echo ($m=='create_vendor_catalog') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Catalogue/create_vendor_catalog_inventory">Build My Catalog</a></li>

				<li><a class="animsition-link <?php echo ($m=='vendor_inventory') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Catalogue/vendor_inventory">View My Catalog</a></li>

					<li><a class="animsition-link <?php echo ($m=='brands_filter') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Catalogue/brands_filter">Active</a></li>
					<li><a class="<?php echo ($m=='archived_brands_filter') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Catalogue/archived_brands_filter">Archived</a></li>
					<li><a class="<?php echo ($m=="inventory_all") ? 'active_drop' : ''; ?>" href="<?php echo base_url();?>admin/Catalogue/inventory_all">All Inventory</a></li>
				</ul>

			</li>
			<!-- <li class="dropdown"><a class="<?php echo ($menu_flag=="customer_view" || $menu_flag=="customer_wallet_bank_transfer" || $menu_flag=="customer_profile" || $menu_flag=="customer_profile_values") ? 'active2' : ''; ?>" href="#" data-toggle="dropdown" class="dropdown-toggle">Customers <i class="fa fa-bell-o" id="customers_menu" aria-hidden="true"></i><b class="caret"></b></a>
			<ul class="dropdown-menu">
                <li><a class="<?php echo ($menu_flag=='customer_view') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Manage_cust">Customer Profile</a></li>
			</ul>

		</li> -->
				
		<li class="dropdown"><a class="<?php echo ($menu_flag=="active_order_active_links" || $menu_flag=="inactive_order_active_links" || $menu_flag=="invoices_free_items_status") ? 'active2' : ''; ?>" href="#" data-toggle="dropdown" class="dropdown-toggle">Orders <i class="fa fa-bell-o" id="notify_orders_menu" aria-hidden="true"></i> <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li><a class="<?php echo ($menu_flag=='active_order_active_links') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Orders/onhold_orders">Active Orders <i class="" id="notify_active_menu" aria-hidden="true"></i></a></li>
				<li><a class="<?php echo ($menu_flag=='inactive_order_active_links') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Orders/delivered_orders">In-active Orders</a></li>
				
				<li><a class="<?php echo ($menu_flag=='invoices_free_items_status') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Orders/invoices_free_items_status">Free items on invoices <i class="" id="notify_freeitem_menu" aria-hidden="true"></i></a></li>
			</ul>
        </li>
		<li class="dropdown" onclick="get_all_num_data_of_all_promotion()"><a class="<?php echo ($c=="Promotions") ? 'active2' : ''; ?>" href="#" data-toggle="dropdown" class="dropdown-toggle">Promotions <b class="caret"></b></a>
	        <ul class="dropdown-menu">
			<li><a class="<?php echo ($m=='create_promotions') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/create_promotions"> Create Promotions</a></li>
			<li><a class="<?php echo ($m=='draft_promotions') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/draft_promotions"> Draft Promotions <!--<span  id="draft_promotion_num" class="badge badge-yellow"></span>--></a></li>
			<li><a class="<?php echo ($m=='promotion_pending_approval') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/promotion_pending_approval"> Pending Approval <!--<span  id="pending_promotion_num" class="badge badge-yellow" ></span>--></a></li>
			<li><a class="<?php echo ($m=='approved_promotions') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/approved_promotions"> Approved Promotions <!--<span  id="approved_promotion_num" class="badge badge-yellow" ></span>--></a></li>
			<li><a class="<?php echo ($m=='active_promotions') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/active_promotions"> Active Promotions <!--<span  id="active_promotion_num" class="badge badge-yellow" ></span>--></a></li>
			<li><a class="<?php echo ($m=='deactive_promotions') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/deactive_promotions"> Deactivated Promotions <!--<span  id="deactivated_promotion_num" class="badge badge-yellow" ></span>--></a></li>
			<li><a class="<?php echo ($m=='revertedDraft_promotions') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/revertedDraft_promotions"> Reverted Draft Promotions <!--<span  id="revertdraft_promotion_num" class="badge badge-yellow" ></span>--></a></li>
			<li><a class="<?php echo ($m=='rejected_promotions') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/rejected_promotions"> Rejected Promotions <!--<span  id="rejected_promotions_num" class="badge badge-yellow" ></span>--></a></li>
			<li><a class="<?php echo ($m=='expired_promotions') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Promotions/expired_promotions"> Expired Promotions <!--<span  id="expired_promotion_num" class="badge badge-yellow" ></span>--></a></li>
			
	        </ul>
	    </li>

		<li class="dropdown"><a class="<?php echo ($menu_flag=="customer_analytics" || $menu_flag=="abandoned_cart_analytics" || $menu_flag=="vendor_dashboard") ? 'active2' : ''; ?>" href="#" data-toggle="dropdown" class="dropdown-toggle">Analytics <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li><a class="<?php echo ($m=='vendor_dashboard') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Analytics/vendor_dashboard">Dashboard </a></li>
				<li><a class="<?php echo ($menu_flag=='vendor_analytics_customers') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Analytics/vendor_customers">Customers </a></li>
				<li><a class="<?php echo ($menu_flag=='vendor_abandoned_cart_analytics') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Analytics/vendor_abandoned_cart">Abandoned Cart </a></li>
				<li><a class="<?php echo ($menu_flag=='vendor_visitors_traffic') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Analytics/vendor_visitors_traffic">Traffic</a></li>
				<li><a class="<?php echo ($menu_flag=='vendor_inventory_view') ? 'active_drop' : '' ?>" href="<?php echo base_url();?>admin/Analytics/vendor_inventory_view">Inventory</a></li>
			</ul>
        </li>
		
		</ul>
		<ul class="nav navbar-nav pull-right">
		<?php
			$name="Logout";
            if($this->session->userdata("logged_in")){
				$name=$this->session->userdata("user_name");
            }
			?>
        <li class="dropdown pull-right">
			<a href="" data-toggle="dropdown" class="dropdown-toggle"><?php echo $name; ?>

            <?php
            if($this->session->userdata("logged_in")){
				echo $this->session->userdata("user");
            }
			?> <b class="caret"></b>
            </a>
            <ul class="dropdown-menu pull-right">

                <li id="" ><a href='<?php echo base_url();?>admin/Catalogue/edit_vendors_form/<?php echo $this->session->userdata("user_id"); ?>'>Edit Profile</a></li>
				<li id="" ><a href='<?php echo base_url();?>admin/Catalogue/vendors_change_passowrd/<?php echo $this->session->userdata("user_id"); ?>'>Change Password</a></li>

                <li id="logout" ><a href='<?php echo base_url();?>admin/Adminout'>Logout</a></li>
            </ul>
        </li>
        
		</ul>
			<?php

        }else{
		?>
        <ul class="nav navbar-nav">
            <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Customer Orders <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url();?>Bmanage/delivered_order">Completed Orders</a></li>
                    </ul>
            </li>
            <li class="dropdown">
                <a href="" data-toggle="dropdown" class="dropdown-toggle">

                <?php
                if($this->session->userdata("logged_in")){
                         echo $this->session->userdata("user");
                }
                ?> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">

                        <li id="logout"><a href='<?php echo base_url();?>admin/Adminout'>Logout</a></li>
                </ul>
            </li>
        </ul>
		<?php
	}
?>
</div>
</nav>
</div>
<script>
$(".nav a").on("click", function(){
   $(".nav").find(".active").removeClass("active");
   $(this).parent().addClass("active");
});
</script>

<script>
	$("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
	
	$(document).ready(function() {
  $(".animsition").animsition();
});


</script>	
</body>
</html>