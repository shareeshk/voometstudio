<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Total Shipping Charges</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" /> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 
<style type="text/css">
.padding_right{
	float:left;
	margin-right:15px;	
}
.accordion-toggle:hover {
  text-decoration: none;
}
.more-less {
        color: #212121;
    }
	.free_items_style{
		padding: 10px;
		border-bottom: 1px solid #ccc;
		margin-bottom: 13px;
	}
.form-inline{
	margin-bottom:0;
}
.form-group{
	margin-bottom:0;
	margin-top:0;
	padding-bottom:0;
	vertical-align:bottom;
}
</style>

<script type="text/javascript">
$(document).ready(function (){
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("input[type=text]").val("");
	});

		$('#to_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY'
			});
			
		$('#from_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY', 
				shortTime : true
			}).on('change', function(e, date)
			{
				$('#to_date').bootstrapMaterialDatePicker('setMinDate', date);
			});
   // Array holding selected row IDs
   var rows_selected = [];
   var table = $('#shipping_charges_table').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Orders/shipping_charges_processing", // json datasource
			data: function (d) { d.from_date = $('#from_date').val(); d.to_date = $('#to_date').val(); },
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#shipping_charges_table_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("delivered_count").innerHTML=json.recordsFiltered;
				document.getElementById("total_shipping_charges").innerHTML=json.total_shipping_charges;
				
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'5%',
         'className': 'dt-body-center'
      },{
         'targets': 1,
         'width':'40%',
      },{
         'targets': 2,
         'width':'20%',
      },{
         'targets': 3,
         'width':'15%',
      },{
         'targets': 4,
         'width':'10%',
      },{
         'targets': 5,
         'width':'10%',
      }],
      'order': [0, 'desc']
   });
	$( "#from_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
	table.draw();
	} );

	$( "#to_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
	table.draw();
	} );

	$('#reset_form_button').on('click',function(){
		table.draw();
	});

	$('#submit_form_button').on('click',function(){
		table.draw();
	});
});	

function sendInvoiceFun(order_item_id,order_id){
	
		swal({
			title: 'Are you sure?',
			text: "want to send mail?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Send it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
		$.ajax({
				url:"<?php echo base_url('admin/Orders/generate_invoice_pdf')?>",
				type:'POST',
				data:"order_item_id="+order_item_id+"&order_id="+order_id+"&sendInvoice=yes",
				beforeSend:function(){
					$('.invoice_sent_'+order_item_id).html('<i class="fa fa-spinner"></i> Processing');
				}
		}).success( function(data){
			$('.invoice_sent_'+order_item_id).html('Send Invoice');
				if(data){
					swal({
						title:"success!", 
						text:"Invoice has been generated successfully", 
						type: "success",
						allowOutsideClick: false
						
					}).then(function () {
						window.open("<?php echo base_url()?>assets/pictures/invoices/Invoice_"+order_id+""+order_item_id+".pdf","_blank");

					});
				}else{
					swal("Error", "not sent", "error");
				}
			
		});
		})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
		$.ajax({
				url:"<?php echo base_url('admin/Orders/generate_invoice_pdf')?>",
				type:'POST',
				data:"order_item_id="+order_item_id+"&order_id="+order_id+"&sendInvoice=no",
				beforeSend:function(){
					$('.invoice_sent_'+order_item_id).html('<i class="fa fa-spinner"></i> Processing');
				}
		}).success( function(data){
				if(data){
					$('.invoice_sent_'+order_item_id).html('Send Invoice');
					swal({
						title:"success!", 
						text:"Invoice has been generated successfully", 
						type: "success",
						allowOutsideClick: false
						
					}).then(function () {
						window.open("<?php echo base_url()?>assets/pictures/invoices/Invoice_"+order_id+""+order_item_id+".pdf","_blank");

					});
				}else{
					swal("Error", "not sent", "error");
				}
			
		});
				
			  }
			});	
	
 }
 function generate_invoice_for_order_ID_offers(order_id,type_of_order){
	if(type_of_order=="cashback"){
		var str="CBK";
	}else{
		var str="SG";
	}
	
	window.open("<?php echo base_url()?>assets/pictures/invoices/Orders/Invoice_"+str+"_"+order_id+".pdf","_blank");
 }
</script>
  
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header"><h4 class="text-center">Shipping Charges <span class="badge" id="delivered_count"></span></h4></div>
	<table id="shipping_charges_table" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th colspan="6">
					<div class="col-md-12 text-center">
						<form id="date_range" class="form-inline">
						<input type="text" name="from_date" id="from_date" placeholder="From Date" class="form-control">
						<input type="text" name="to_date" id="to_date" placeholder="To Date" class="form-control">
						<button type="button" class="btn btn-sm btn-primary" id="submit_form_button">Submit</button>
						<button type="reset" class="btn btn-sm btn-info" id="reset_form_button">Reset</button>
						<span class="text-right" id="total_shipping_charges_div">
							Total Shipping Charges : <?php echo curr_sym; ?> <span id="total_shipping_charges">0</span>
						</span>
						</form>
					</div>
				</th>
			</tr>
			<tr>
				<th class="text-primary small bold">Image</th>
				<th class="text-primary small bold">Order Summary</th>
				<th class="text-primary small bold">Delivered Details</th>
				<th class="text-primary small bold">Quantity and Price</th>
				<th class="text-primary small bold">Buyer Details</th>
				<th class="text-primary small bold">Actions</th>
			</tr>
		</thead>
	</table>
</div>


  <div class="modal" id="order_summary">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Order Summary</h4>
           </div>
           <div  class="modal-body" id="model_content">
		  
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
  </div>
  
<script type="text/javascript">
  $("#order_summary").modal("hide");
  function view_order_summary_(order_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Orders/get_data_from_invoice_offers",
		type:"POST",
		data:"order_id="+order_id,
		success:function(data){
			
			$("#description").val("");	
			$("#model_content").html(data);
			$("#order_summary").modal("show");
			
		}
	});
	
}
function go_to_return_details(order_item_id){	
	 $('#return_details_'+order_item_id).attr('action', '<?php echo base_url();?>admin/Orders/return_details_of_order_item').submit();
}
</script>
</div>
</body> 
</html>  