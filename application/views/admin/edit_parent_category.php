<html lang="en">
<head>

<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style type="text/css">
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}

.wizard-card .tab-content {
	 min-height: 400px;
	 padding:0px;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	

<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">
function showDivPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/parent_category';
}

</script>
<?php
if($get_parent_category_data['menu_sort_order']=="0"){?>
	<script>
	$(document).ready(function () {
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	document.getElementById("show_available_common_cat").disabled = true;
	});
	</script>
	<?php
}
?>

<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid" id="entireresult">

<div class="row" id="editDiv3">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">            
				
	<form name="edit_parent_category" id="edit_parent_category" method="post" action="#" onsubmit="return form_validation_edit();">
		<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_pcat_name: {
					required: true,
				},
				edit_menu_sort_order: {
					required: true,
				},
				edit_show_sub_menu: {
					required: true,
				}
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Parent category</h3>
		</div>	
		</div> 
		
		<div class="tab-content">
			<input type="hidden" name="pcat_id" id="pcat_id" value="<?php echo $pcat_id;?>">	
				
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Enter Parent Category Name</label>
					<input id="edit_pcat_name" name="edit_pcat_name" type="text" class="form-control" value="<?php echo $get_parent_category_data["pcat_name"];?>"/>
					<input id="default_edit_pcat_name" name="default_edit_pcat_name" type="hidden" class="form-control" value="<?php echo $get_parent_category_data["pcat_name"];?>"/>
				</div>
				</div>
			</div>
	
			<input type="hidden" name="edit_menu_level"  value="1">
					
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Show Sub Menu-</label>
					<select name="edit_show_sub_menu" id="edit_show_sub_menu" class="form-control search_select">
						<option value="" selected></option>
						<option value="1" <?php if($get_parent_category_data["show_sub_menu"]=="1"){echo "selected";}?>>Show</option>
						<option value="0" <?php if($get_parent_category_data["show_sub_menu"]=="0"){echo "selected";}?>>Hide</option>
			
					</select>
				</div>
				</div>
			</div>	
				
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Enter the Sort Order</label>
					<select name="edit_menu_sort_order" id="edit_menu_sort_order" class="form-control" onchange="show_view_validatn()">
					<option value="<?php echo $get_parent_category_data["menu_sort_order"];?>" selected><?php echo $get_parent_category_data["menu_sort_order"];?></option>
						<?php
							if($get_sort_order_options_for_p_cat["key"]==""){
								if($get_parent_category_data["menu_sort_order"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php } ?>
								<option value="1">1</option>
								<?php
							}
							else{
								if($get_parent_category_data["menu_sort_order"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php
								}
								$value_arr=explode(",",$get_sort_order_options_for_p_cat["value"]);
								foreach($value_arr as $k => $v){
									?>
										<option value="<?php echo $v;?>"><?php echo $v;?></option>
									<?php
									}
							}
						?>
					</select>
					<input id="edit_menu_sort_order_default_value" name="edit_menu_sort_order_default_value" type="hidden" value="<?php echo $get_parent_category_data["menu_sort_order"];?>" >
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
			<div class="form-group label-floating">
			<label class="control-label">-Swap Options-</label>
			<select class="form-control" id="show_available_common_cat" onchange="changeEventHandler(event)">
			<option selected value=""></option>
			<?php
			foreach($show_sort_order_arr as $sort_order_arr){
				
				?>
				
					<option value="<?php echo $sort_order_arr["type"]."_".$sort_order_arr["common_cat_id"];?>"><?php echo $sort_order_arr["common_cat_name"]."-".$sort_order_arr["type"]."(".$sort_order_arr["menu_sort_order"].")";?></option>
					<?php
			}
			?>
			
			</select>
			</div>
			</div>
			</div>
								
			<div class="row">
				<div class="col-md-10"> 
					<div class="form-group">
					<div class="col-md-5 col-md-offset-1">Show Parent Category</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="edit_view" id="edit_view1" value="1" type="radio" <?php if($get_parent_category_data["view"]=="1"){echo "checked";}?>>View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="edit_view" id="edit_view2" value="0" type="radio" <?php if($get_parent_category_data["view"]=="0"){echo "checked";}?>>Hide</label>
					</div>
				
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10"> 
					<div class="form-group">
					<div class="col-md-5 col-md-offset-1">Show In Combo Pack</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="combo_pack_status" id="combo_pack_status1" value="yes" type="radio" <?php if($get_parent_category_data["combo_pack_status"]=="yes"){echo "checked";}?>>Yes</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="combo_pack_status" id="combo_pack_status2" value="no" type="radio" <?php if($get_parent_category_data["combo_pack_status"]=="no"){echo "checked";}?>>No</label>
					</div>
				
					</div>
				</div>
			</div>

						
			<div class="row">
				<div class="col-md-8 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" id="submit-data" disabled="" type="submit">Submit</button>
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>				
</div>
</div>
<div class="footer">

</div>
</div>
</div>
</body>

</html>

<script>
function changeEventHandler(event){
        parent_id=event.target.value;
		if(parent_id!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		}
		if(parent_id==""){
	edit_menu_sort_order=document.getElementById("edit_menu_sort_order").value;
	if(edit_menu_sort_order==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;					
	}
	if(edit_menu_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
		}
}
function show_view_validatn(){
	edit_menu_sort_order=document.getElementById("edit_menu_sort_order").value;
	show_available_common_cat=document.getElementById("show_available_common_cat").value;
	edit_menu_sort_order_default=document.getElementById("edit_menu_sort_order_default").value;
	if(show_available_common_cat==""){
	if(edit_menu_sort_order==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	if(edit_menu_sort_order_default==0){
	document.getElementById('show_available_common_cat').disabled = true;
	}
	if(edit_menu_sort_order_default!=0){
	document.getElementById('show_available_common_cat').disabled = false;
	}
	}
	if(edit_menu_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		//document.getElementById("show_available_common_cat").disabled = false;
	}
	}
	if(show_available_common_cat!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
	
}

var flag=1;
function check_duplication_name(obj){
	var pcat_name = obj.value; 
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/check_duplication_name",
		type:"post",
		data:"pcat_name="+pcat_name,
		success:function(data){
			if(data=="yes"){
					flag=0;
			}
			if(data=="no"){
				flag=1;
			}
		}
	});
				
 }
</script>
<script type="text/javascript">
$(document).ready(function(){

var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_parent_category'] :input").each(function () {

    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_parent_category"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_parent_category'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (orig[id].value == $(this).val());

        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});
});
$('#result').hide();

function form_validation_edit(){
var pcat_name = $('input[name="edit_pcat_name"]').val().trim();
var menu_level = $('input[name="edit_menu_level"]').val().trim();
var show_sub_menu = $('select[name="edit_show_sub_menu"]').val().trim();
var menu_sort_order = $('select[name="edit_menu_sort_order"]').val().trim();
var view = document.querySelector('input[name="edit_view"]:checked').value;
var menu_sort_order_default = $('input[name="edit_menu_sort_order_default_value"]').val().trim();

var type="pcat";
var common_cat_id="";
var show_sort_order="";
var show_available_common_cat_arr=[];

var show_available_common_cat=document.getElementById("show_available_common_cat").value;

if(show_available_common_cat!=""){
	show_available_common_cat_arr=show_available_common_cat.split("_");

	if(show_available_common_cat_arr.length==2){
		type=show_available_common_cat_arr[0];
		common_cat_id=show_available_common_cat_arr[1];
	}
	if(show_available_common_cat_arr.length==1){
		show_sort_order=show_available_common_cat;
	}
}
		
		
	   var err = 0;
	   
		if(!(pcat_name.length>=3) || (show_sub_menu=='') || (menu_sort_order=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}

		if(err==0)
			{
var form_status = $('<div class="form_status"></div>');		
var form = $('#edit_parent_category');	
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();

				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_parent_category/"?>',
				type: 'POST',
				data: $('#edit_parent_category').serialize()+"&type="+type+"&show_sort_order="+show_sort_order+"&common_cat_id="+common_cat_id,
				dataType: 'html',
				
			}).done(function(data)
			  {
				form_status.html('')
				if(data=="exist"){
					swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					default_edit_pcat_name=document.getElementById("default_edit_pcat_name").value;
					document.getElementById("edit_pcat_name").value=default_edit_pcat_name;
					document.getElementById("edit_pcat_name").focus();
					
				});
				  }
				if(data==1){
					
					swal({
						title:"Success", 
						text:"Parent Category is successfully updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}if(data==0){
					
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
				
				
			});
}
}]);
			}
	
	return false;
		
}


</script>

