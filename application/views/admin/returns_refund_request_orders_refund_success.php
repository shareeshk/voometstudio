<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Refund Request Orders - Refund Success</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<style>
.form-group{
	margin-bottom:10px;
	margin-top:5px;
	padding-bottom:0;
}
.form-inline .form-control{
	width:100%;
}
</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript">
var table;
$(document).ready(function (){
	
	$("#return_pickup_modal").on('hidden.bs.modal', function () {
		$("#return_pickup_form").trigger("reset");	
		draw_table();
    });
	$("#return_refund_request_again_modal").on('hidden.bs.modal', function () {
		$("#return_refund_request_again_form").trigger("reset");	
		draw_table();
    });$("#return_refund_request_again_accepted_modal").on('hidden.bs.modal', function () {
		$("#return_refund_request_again_accepted_form").trigger("reset");	
		draw_table();
    });
	$("#return_refund_request_again_rejected_modal").on('hidden.bs.modal', function () {
		$("#return_refund_request_again_rejected_form").trigger("reset");	
		draw_table();
    });
	
   // Array holding selected row IDs
   var rows_selected = [];
   table = $('#returns_refund_request_orders_table_refund_success').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Returns/returns_refund_request_orders_refund_success_processing", // json datasource
			data: function (d) { d.status_of_refund = $('#status_of_refund').val(); },
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#returns_refund_request_orders_table_refund_success_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("refund_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'40%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});	

function draw_table(){
	table.draw();
}

$(document).ready(function(){
	var returns_refund_orders_list_checked=0;
	$("input[name='returns_refund_orders_list']").each(function(){
		if($(this).is(":checked")){
			returns_refund_orders_list_checked++;
		}
	})
	/*if(returns_refund_orders_list_checked==0){
		document.getElementById("refund_info_div").style.display="none";
	}*/
});

function open_return_refund_issue_success_fun(order_item_id){
	location.href="<?php echo base_url()?>admin/Returns/returns_issue_refund_success/"+order_item_id;
}

function open_return_refund_request_again_fun(order_item_id,return_order_id){

	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/returns_refund_request_orders_refund_success_request_again",
		type:"POST",
		data:"return_order_id="+return_order_id+"&order_item_id="+order_item_id,
		success:function(data){
			
			$("#return_refund_request_again_form").html(data);
			$("#return_refund_request_again_modal").modal("show");
			
		}
	})
	
}


function initiate_order_return_pickupFun(obj,orders_status_id,return_order_id,order_item_id){
	
	/*if(confirm("Do you want Customer to return the Item?! (second)")==true){
		initiate_return_pickup_again=1;
	}else{
		initiate_return_pickup_again=0;
	}*/
	swal({
			title: 'Do you want?',
			text: "Customer to return the Item",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Return Item!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
				initiate_return_pickup_again=1;
				$.ajax({
		url:"<?php echo base_url()?>admin/Returns/initiate_order_return_again_pickupFun",
		type:"POST",
		data:"orders_status_id="+orders_status_id+"&return_order_id="+return_order_id+"&order_item_id="+order_item_id+"&initiate_return_pickup_again="+initiate_return_pickup_again,
		success:function(data){
			if(data){
				swal({
								title:"success!", 
								text:"Pickup of an item has been Initiated successfully", 
								type: "success",
								allowOutsideClick: false
								
							}).then(function () {
								location.reload();

							});
			}
			else{
				swal("error");
			}
			
		}
		})
				})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				  
				initiate_return_pickup_again=0;
				$.ajax({
		url:"<?php echo base_url()?>admin/Returns/initiate_order_return_again_pickupFun",
		type:"POST",
		data:"orders_status_id="+orders_status_id+"&return_order_id="+return_order_id+"&order_item_id="+order_item_id+"&initiate_return_pickup_again="+initiate_return_pickup_again,
		success:function(data){
			if(data){
				swal({
								title:"success!", 
								text:"Pickup of an item has been Initiated successfully", 
								type: "success",
								allowOutsideClick: false
								
							}).then(function () {
								location.reload();

							});
				
			}
			else{
				swal("error");
			}
			
		}
		})
			  }
			});	
	
	
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header"><h4 class="text-center">Refund Request Orders - Refund Success (Partial Refunds) <span class="badge" id="refund_count"></span></h4></div>
	<table id="returns_refund_request_orders_table_refund_success" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
			<th colspan="4">
		<div class="col-md-4 col-md-offset-4">
			<select class="form-control" name="status_of_refund" id="status_of_refund" onchange="draw_table()">
				<option value="success">Success</option>
				<option value="pending">Pending</option>
				<option value="all">All</option>
			</select>
		</div>
			</th>
			</tr>
			<tr>
				<th class="text-primary small bold">Return Summary</th>
				<th class="text-primary small bold">Return Reason</th>
				<th class="text-primary small bold">Refund Details</th>
				<th class="text-primary small bold">Actions</th>
			</tr>
		</thead>
	</table>
</div>

<div class="modal" id="return_pickup_modal_second" data-backdrop="static" role="dialog">
<div class="modal-dialog modal-sm">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
				<div class="panel panel-primary">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                  Update Item received status
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right" onclick="divFunction_order_rep_pickup()"></span></span>
               </div>
               <div aria-expanded="true" class="">
					<div class="panel-body">
		<form id="return_pickup_form_second" method="post" enctype="multipart/form-data" class="form-horizontal">
			<input type="hidden" name="orders_status_id" id="orders_status_id_s">
			<input type="hidden" name="return_order_id" id="return_order_id_s">
			
			<input type="hidden" name="order_item_id" id="order_item_id_s">
			<input type="hidden" name="inventory_id" id="inventory_id_s">
			<input type="hidden" name="quantity_refund" id="quantity_refund_s">
						<div class="form-group">
							<div class="col-md-12">
									<input name="update_pickup" checked type="checkbox">
									 Items are pickedup
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12">
									<input name="update_stock" type="radio" value="item_has_reached_warehouse" checked>
									 Item has reached warehouse
									 </div>
									 <div class="col-md-12">
									 <input name="update_stock" type="radio" value="item_has_not_reached_warehouse">
									 Item has not reached warehouse
							</div>
						</div>
						<div class="form-group">
						<div class="col-md-12">
							<input class="btn btn-success btn-xs btn-block" value="Submit" type="submit">
						</div>
					</div>
						</form>
					</div>

               </div>
            </div>
		
         </div>
      </div>
   </div>
</div>
<!------------pickup modal------->

<script type="text/javascript">

function open_order_return_pickup_again(obj,orders_status_id,return_order_id,order_item_id,inventory_id,quantity){

	$("#orders_status_id_s").val(orders_status_id);
	$("#return_order_id_s").val(return_order_id);
	$("#order_item_id_s").val(order_item_id);
	$("#inventory_id_s").val(inventory_id);
	$("#quantity_refund_s").val(quantity);
	$("#return_pickup_modal_second").modal("show");
}

$(document).ready(function(){
	
	$("#return_pickup_form_second").on('submit',(function(e) {
		
		var update_stock=$("input[name=update_stock]").is(':checked');
		var update_pickup=$("input[name=update_pickup]").is(':checked');
		
		if(update_stock==false){
			swal('Pls fill all fields');
			return false;
		}
		
		if(update_pickup==false){
			swal('Items should be picked up.');
			return false;
		}
		e.preventDefault();
		
		$.ajax({
				url:"<?php echo base_url();?>admin/Returns/update_order_return_pickup_again",
				type: "POST",    
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){	
				
				
					if(data){
						$('#return_pickup_modal_second').modal('hide');
						swal({
							title:"success!", 
							text:"Successfully Updated", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#return_pickup_form_second").trigger("reset");
							$("#return_pickup_modal_second").modal("hide");
							draw_table();

						});
					}else{
						swal("Error", "not sent", "error");
					}
					
				}	        
		   });
		
		
	}));

});
function divFunction_order_rep_pickup(){
		$("#return_pickup_form_second").trigger("reset");	
		$("#return_pickup_modal_second").modal("hide");
		draw_table();
}
</script>
<!------------pickup modal------->

<div class="modal" id="return_pickup_modal" role="dialog">
<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
					<div class="panel panel-primary">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                  <span class="glyphicon glyphicon-comment"></span> Update Item received
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right"></span></span>
               </div>
               <div aria-expanded="true" class="">
					<div class="panel-body">
		<form id="return_pickup_form" method="post" enctype="multipart/form-data" class="form-horizontal">
			<input type="hidden" name="orders_status_id" id="orders_status_id">
			<input type="hidden" name="return_order_id" id="return_order_id">
			
			<input type="hidden" name="order_item_id" id="order_item_id">
			<input type="hidden" name="inventory_id" id="inventory_id">
			<input type="hidden" name="quantity_refund" id="quantity_refund">

						<div class="form-group">
							<div class="col-md-12">
									<input name="update_pickup" checked type="checkbox">
									 Items are pickedup
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12">
									<input name="update_stock" type="radio" value="item_has_reached_warehouse">
									 Item has reached warehouse
									 <input name="update_stock" type="radio" value="item_has_not_reached_warehouse">
									 Item has not reached warehouse
							</div>
						</div>
						<div class="form-group">
						<div class="col-md-12">
							<input class="btn btn-success btn-xs btn-block" value="Submit" type="submit">
						</div>
					</div>
						</form>
					</div>

               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!------------pickup modal------->

<script type="text/javascript">

function open_order_return_pickup(obj,orders_status_id,return_order_id,order_item_id,inventory_id,quantity){
	
	$("#orders_status_id").val(orders_status_id);
	$("#return_order_id").val(return_order_id);
	$("#order_item_id").val(order_item_id);
	$("#inventory_id").val(inventory_id);
	$("#quantity_refund").val(quantity);
	$("#return_pickup_modal").modal("show");
}

$(document).ready(function(){
	
	$("#return_pickup_form").on('submit',(function(e) {
		
		var update_stock=$("input[name=update_stock]").is(':checked');
		var update_pickup=$("input[name=update_pickup]").is(':checked');
		
		if(update_stock==false){
			swal('Pls fill all fields');
			return false;
		}
		
		if(update_pickup==false){
			swal('Items should be picked up.');
			return false;
		}
		e.preventDefault();
		
		$.ajax({
				url:"<?php echo base_url();?>admin/Returns/update_order_return_pickup",
				type: "POST",    
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){	
				
					if(data){
						swal({
								title:"success!", 
								text:"Pickup of an item has been Initiated successfully", 
								type: "success",
								allowOutsideClick: false
								
							}).then(function () {
								$("#return_pickup_form").trigger("reset");
								$("#return_pickup_modal").modal("hide");
								location.reload();

							});
						
					}else{
						swal("Something went wrong!");
					}
				}	        
		   });
		
		
	}));
	
	
	
});
</script>
<!------------pickup modal------->

<!------------------------------------------->

<!-- contact buyer model things starts --->
<script>
$(document).ready(function(){
	$("#send_form_admin_contact_buyer_form").on('submit',(function(e) {
		e.preventDefault();
		var description=$("#description").val();
		if(description!=""){
			$.ajax({
				url:"<?php echo base_url();?>admin/Returns/contact_buyer",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					if(data){
						
						swal({
							title:"success!", 
							text:"Successfully sent", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#send_form_admin_contact_buyer_form").trigger("reset");	
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
					
				}	        
		   });
		}
		else{
			alert("Please enter description!");
		}
	}));

});
function divFunction_order_contact(){
    $("#send_form_admin_contact_buyer_form").trigger("reset");	
		location.reload();
}
</script>



<!-- contact buyer model things ends --->




<!--- modal request again starts ----------------->

<div class="modal" id="return_refund_request_again_modal" data-backdrop="static" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		  <div class="panel panel-info">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                   Accept refund request
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right"></span></span>
               </div>
              
			  <div aria-expanded="true" class="">
			  
                  <div class="panel-body">
		 <form id="return_refund_request_again_form" method="post" enctype="multipart/form-data" class="form-horizontal">
		
		
		</form>
		</div>
               </div>
            </div>
		
         </div>
      </div>
   </div>
</div>

 <script>
		 
function update_order_return_pickupFun(obj,orders_status_id,return_order_id,order_item_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/update_order_return_pickup",
		type:"POST",
		data:"orders_status_id="+orders_status_id+"&return_order_id="+return_order_id+"&order_item_id="+order_item_id,
		success:function(data){
			if(data){
				swal({
								title:"success!", 
								text:"Pickup of an item has been Updated successfully", 
								type: "success",
								allowOutsideClick: false
								
							}).then(function () {
								location.reload();

							});
			}
			else{
				swal("error");
			}
			
		}
	})
}

function invoice_returns_refund_partialFun(order_item_id,order_id,quantity_refunded){
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/generate_invoice_for_partial_refunds_pdf",
		type:"POST",
		data:"order_item_id="+order_item_id+"&order_id="+order_id+"&quantity_refunded="+quantity_refunded,
		beforeSend:function(){
				$('.invoice_sent_'+order_item_id).html('<i class="fa fa-spinner"></i> Processing');
			}
	}).success( function(data){
		if(data){
			$('.invoice_sent_'+order_item_id).html('Send Invoice');
			swal({
				title:"success!", 
				text:"Invoice has been sent successfully!", 
				type: "success",
				allowOutsideClick: false
				
			}).then(function () {
				window.open("<?php echo base_url()?>assets/pictures/invoices/Invoice_partialref_"+order_id+""+order_item_id+".pdf","_target");

			});
		}else{
			swal("Error", "not sent", "error");
		}
		
	});
	
}

function request_again_admin_actionFun(admin_action,order_item_id){
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/request_again_admin_action",
		type:"POST",
		data:$("#return_refund_request_again_form").serialize()+"&admin_action="+admin_action,
		success:function(data){
				if(data){
						$("#return_refund_request_again_modal").modal("hide");	
						swal({
							title:"success!", 
							text:"Submitted successfully", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							//$("#return_refund_request_again_modal").modal("hide");	
							draw_table();

						});
					}else{
						swal("Error", "not sent", "error");
					}
			
		}
	})
}
		 </script>
		
		
		
		
		<script type="text/javascript">
		
			$(document).ready(function(){
	
				$("#return_refund_request_again_modal").on('shown.bs.modal', function () {
					
					order_item_id=$("#order_item_id_req").val();	
					
					var remain_quant_checkbox=$("#refund_remaining_quantities_"+order_item_id).is(':checked');
					
					if(remain_quant_checkbox==false){
						$('#quantity_requested_'+order_item_id).attr('readonly',true);
					
					}else{
						$('#quantity_requested_'+order_item_id).attr('readonly',false);
					}
			
					desired_quantity=$("#quantity_refund_req").val();
					promotion_available=$("#promotion_available_req").val();
					
					if(promotion_available==1){
						refund_promo_calculation_for_second_request(desired_quantity,order_item_id);
					}else{
						//alert('promotion not available');
					}
						
				});
			});
			
			function check_it_is_checked(order_item_id){

				var remain_quant_checkbox=$("#refund_remaining_quantities_"+order_item_id).is(':checked');

				if(remain_quant_checkbox==false){
					$('#quantity_requested_'+order_item_id).attr('readonly',true);
				
				}else{
					$('#quantity_requested_'+order_item_id).attr('readonly',false);
				}
				
			}
			function check_promo_details_for_second_request(obj,order_item_id){

				desired_quantity=obj.value;
				promotion_available=$("#promotion_available_req").val();
				
				if(promotion_available==1){
					refund_promo_calculation_for_second_request(desired_quantity,order_item_id);
				}
			}
			
			function refund_promo_calculation_for_second_request(desired_quantity,order_item_id){

            var curr_sym='<?php echo curr_sym; ?>';

			var desired_quantity_refund=desired_quantity;
			//get the onchange quantity as a desired quantity
			var requested_unitsmarker=parseInt(document.getElementById('quantity_refund_req').value);
			var ordered_quantity=parseInt(document.getElementById('ordered_quantity_req').value);
			var first_request_quantity=parseInt(document.getElementById('first_request_quantity_req').value);

			var order_item_invoice_discount_value=parseFloat(document.getElementById('order_item_invoice_discount_value_'+order_item_id).value) || 0;//"falsey" value to 0
			var order_item_invoice_discount_value_each=parseFloat(document.getElementById('order_item_invoice_discount_value_each_'+order_item_id).value) || 0;//"falsey" value to 0
				
			var promotion_available=parseInt(document.getElementById('promotion_available_req').value);
			var promotion_minimum_quantity=parseInt(document.getElementById('promotion_minimum_quantity_req').value);
			var promotion_quote=document.getElementById('promotion_quote_req').value
			var promotion_default_discount_promo=document.getElementById('promotion_default_discount_promo_req').value
			
			var promotion_item=document.getElementById('promotion_item_req').value
			var promotion_item_num=document.getElementById('promotion_item_num_req').value
			var promotion_cashback=parseInt(document.getElementById('promotion_cashback_req').value);
			var promotion_item_multiplier=parseInt(document.getElementById('promotion_item_multiplier_req').value);
			var default_discount=parseInt(document.getElementById('default_discount_req').value);
			var promotion_discount=parseInt(document.getElementById('promotion_discount_req').value);

			var cash_back_value=parseInt(document.getElementById('cash_back_value_req').value);
			var status_of_refund_for_cashback_on_sku=$("#status_of_refund_for_cashback_on_sku_"+order_item_id).val();
			
			var individual_price_of_product_with_promotion=parseFloat(document.getElementById('individual_price_of_product_with_promotion_req').value);
			var individual_price_of_product_without_promotion=parseFloat(document.getElementById('individual_price_of_product_without_promotion_req').value);
			var total_price_of_product_with_promotion=parseFloat(document.getElementById('total_price_of_product_with_promotion_req').value) || 0;
			var total_price_of_product_without_promotion=parseFloat(document.getElementById('total_price_of_product_without_promotion_req').value) || 0;
			
			if(promotion_available==1){

				$("#decided_individual_price_of_product_without_promotion_req").val('');
				$("#decided_total_price_of_product_without_promotion_req").val('');
				$("#decided_quantity_without_promotion_req").val('');
				$("#decided_individual_price_of_product_with_promotion_req").val('');
				$("#decided_total_price_of_product_with_promotion_req").val('');
				$("#decided_quantity_with_promotion_req").val('');
				$("#decided_cash_back_value_req").val('');
			}
			
			var quotient=0;
			var modulo=0;
			var promo_str="";
			var total_amount=0;

		if(promotion_available==1){

			if((promotion_discount!=default_discount) && (promotion_discount>0)){
				
				promo_discount_available=1;
			}else{
				promo_discount_available=0;
			}
			
			modulo=parseInt(desired_quantity_refund%promotion_minimum_quantity);//remainder
			quotient=Math.floor(parseInt(desired_quantity_refund/promotion_minimum_quantity));//answer
			
			modulo_purchased=parseInt(ordered_quantity%promotion_minimum_quantity);//remainder
			quotient_purchased=Math.floor(parseInt(ordered_quantity/promotion_minimum_quantity));//answer

			var number_of_free_items=0;
			if(promotion_item_num!=''){
				
				var promo_item_arr = promotion_item_num.split(',');
				var free_item_count=0;
				number_of_free_items=promo_item_arr.length;
				for(h=0;h<number_of_free_items;h++){
					free_item_count+=parseInt(promo_item_arr[h]);
				}
			}
			
			var promotion_surprise_gift_type=document.getElementById('promotion_surprise_gift_type_req').value;
			
			if(promotion_surprise_gift_type=='Qty'){
				quantity_filled=parseInt(desired_quantity_refund)+parseInt(first_request_quantity);
				remain=parseInt(ordered_quantity-quantity_filled);
				
				if(remain<promotion_minimum_quantity){
					return_surprise_gift=1;
				}else{
					return_surprise_gift=0;
				}
			}
			var promotion_surprise_gift=document.getElementById('promotion_surprise_gift_req').value;

			if(promotion_surprise_gift_type=='<?php echo curr_code; ?>'){
				quantity_filled=parseInt(desired_quantity_refund)+parseInt(first_request_quantity);
				if(ordered_quantity==quantity_filled){
					return_surprise_gift_amount=1;
				}else{
					return_surprise_gift_amount=0;
				}
			}

			
		if(ordered_quantity >= promotion_minimum_quantity){
			
			if(desired_quantity_refund<promotion_minimum_quantity && (modulo_purchased!=modulo) && desired_quantity_refund!=quotient && (free_item_count>0 || promo_discount_available==1) && promo_discount_available != 1){
				if(modulo_purchased>0){
					alert("You cannot accept for this quantity.You can accept for multiples of "+promotion_minimum_quantity+ " or " +modulo_purchased+" quantity");
				}else{
					alert("You cannot accept for this quantity.You can accept for multiples of "+promotion_minimum_quantity+" quantity");
				}
	
				$("#details_of_refund_request_"+order_item_id).hide();
				$('#submit_button_'+order_item_id).prop('disabled', true);
				return false;
				
			}else if(desired_quantity_refund>promotion_minimum_quantity && modulo_purchased==0 && modulo>0 && promo_discount_available != 1){
				
				alert("You cannot accept for this quantity.You can accept for multiples of "+promotion_minimum_quantity);
				$("#details_of_refund_request_"+order_item_id).hide();
				//document.getElementById("quantity_accept_"+order_item_id).value=requested_unitsmarker;
				$('#submit_button_'+order_item_id).prop('disabled', true);
				return false;
				
			}else if(desired_quantity_refund>promotion_minimum_quantity && modulo_purchased>0 && modulo_purchased!=modulo && modulo!=0 && promo_discount_available != 1){
				
				alert("You cannot accept for this quantity.You can accept for multiples of "+promotion_minimum_quantity+ " or " +modulo_purchased);
				$("#details_of_refund_request_"+order_item_id).hide();
				//document.getElementById("quantity_accept_"+order_item_id).value=requested_unitsmarker;
				$('#submit_button_'+order_item_id).prop('disabled', true);
				return false;
			}
			
			$("#details_of_refund_request_"+order_item_id).show();
			$('#submit_button_'+order_item_id).prop('disabled', false);
			
			if(desired_quantity_refund >= promotion_minimum_quantity){
				
				if(promo_discount_available != 1){
					
					if(quotient>0){
						
						indi_price_with_promo=parseInt(individual_price_of_product_with_promotion/quotient);
						
						if(number_of_free_items>0){
							count_quotient=(quotient)*promotion_minimum_quantity;
						}else{
							count_quotient=quotient*promotion_minimum_quantity;
						}
						
						//alert(quotient);
						//alert(promotion_minimum_quantity);

						total_price_with_promo=count_quotient*individual_price_of_product_with_promotion;
						
						promo_str+='<tr><td>'+count_quotient+' * '+individual_price_of_product_with_promotion+'<br><small style="color:green;"><b><i>'+promotion_quote+' for quantity ('+count_quotient+')</i></b></small></td><td> '+curr_sym+total_price_with_promo+'</td></tr>';
						total_amount+=parseFloat(total_price_with_promo);
						
						
						$("#decided_individual_price_of_product_with_promotion_req").val(individual_price_of_product_with_promotion);
						$("#decided_total_price_of_product_with_promotion_req").val(total_price_with_promo);
						$("#decided_quantity_with_promotion_req").val(count_quotient);
					
						if(promotion_cashback>0){
							if(status_of_refund_for_cashback_on_sku=="refunded"){
								cash_back_value=parseFloat(individual_price_of_product_with_promotion*(promotion_cashback/100)).toFixed(2);
								
								cash_back_value_with_quotient=parseFloat(quotient*cash_back_value).toFixed(2);
								promo_str+='<tr><td><span style="color:red;">Cashback value for quantity ('+quotient+')</span></td><td> <span style="color:red;"> -'+curr_sym+cash_back_value_with_quotient+'</span></td></tr>';

								total_amount-=parseFloat(cash_back_value_with_quotient);
							}else{
								cash_back_value_with_quotient=0;
							}
							$("#decided_cash_back_value_req").val(cash_back_value_with_quotient);
						}
					}
					
					if(number_of_free_items>0){
						
						free_skus_from_inventory=$.parseJSON($('#free_inventory_available_req').val());
						
						free_str="";
						
						//alert(free_skus_from_inventory);
						if(free_skus_from_inventory!=null){
							if(free_skus_from_inventory.length>0){
								
								for(y=0;y<free_skus_from_inventory.length;y++){
									
									free_str+=free_skus_from_inventory[y].str+" <small>("+quotient*parseInt(free_skus_from_inventory[y].count)+")</small> <br>";
								}
							}
							
						}

						promo_str+='<tr><td colspan="2"><b><h6 style="color:blue;"></b> Free Items to be returned with the requested Quantity of product</h6>'+free_str+'</td><tr>';
					}
					if(promotion_surprise_gift_type=='Qty'){
						if(return_surprise_gift==1){
						
							surprise_free_skus_from_inventory=$.parseJSON($('#surprise_free_inventory_available_req').val());
							
							surprise_free_str="";
							
							if(surprise_free_skus_from_inventory!=null){
								if(surprise_free_skus_from_inventory.length>0){
									
									for(z=0;z<surprise_free_skus_from_inventory.length;z++){
										
										surprise_free_str+=surprise_free_skus_from_inventory[z].str+" <small>("+parseInt(surprise_free_skus_from_inventory[z].count)+")</small> <br>";
									}
								}
								
							}

							promo_str+='<tr><td colspan="2"><b><h6 style="color:blue;"></b>Surprise Free Items to be returned with the requested Quantity of product</h6>'+surprise_free_str+'</td><tr>';
						}
					}
					if(promotion_surprise_gift_type=='<?php echo curr_code; ?>'){
						if(return_surprise_gift_amount==1){
							promo_str+='<tr><td><h6 style="color:blue;">'+curr_sym+promotion_surprise_gift+' was credited to customer wallet. So this amount will be reduced from the total item price.</h6> </td><td style="color:red"> - '+curr_sym+promotion_surprise_gift+'</td></tr>';
							total_amount-=parseFloat(promotion_surprise_gift);
						}
					}
					if(modulo>0){
						//breakup 2
						indi_price_without_promo=parseInt(individual_price_of_product_without_promotion/modulo);
						
						total_price_without_promo=parseInt(modulo*individual_price_of_product_without_promotion);
						
						promo_str+='<tr><td>'+modulo+' * '+individual_price_of_product_without_promotion+'<br><small style="color:green;"><b><i>'+promotion_default_discount_promo+' for quantity ('+modulo+')</i></b></small></td><td> '+curr_sym+total_price_without_promo+'</td></tr>';
						total_amount+=parseFloat(total_price_without_promo);
						$("#decided_individual_price_of_product_without_promotion_req").val(individual_price_of_product_without_promotion);
						$("#decided_total_price_of_product_without_promotion_req").val(total_price_without_promo);
						$("#decided_quantity_without_promotion_req").val(modulo);
					}
					
				}
				if(promo_discount_available == 1){
					
					//calculation has been done while dumping for individual_price
					
					/*total_price_of_product=parseFloat(total_price_of_product_with_promotion+total_price_of_product_without_promotion).toFixed(2);
					indi_price_with_promo=parseFloat(total_price_of_product/ordered_quantity).toFixed(2);*/
					
					indi_price_with_promo=individual_price_of_product_with_promotion;
					
					total=parseFloat(indi_price_with_promo*desired_quantity_refund).toFixed(2);
					promo_str+='<tr><td>'+desired_quantity_refund+' * '+indi_price_with_promo+'</td><td>'+curr_sym+total+'</td></tr>';
					total_amount+=parseFloat(total);
					$("#decided_individual_price_of_product_with_promotion_req").val(indi_price_with_promo);
					$("#decided_total_price_of_product_with_promotion_req").val(total);
					$("#decided_quantity_with_promotion_req").val(desired_quantity_refund);
				}
			
				
			}else{
				if(promo_discount_available != 1){
					if(modulo>0){
						//breakup 2
						indi_price_without_promo=parseInt(individual_price_of_product_without_promotion/modulo);
						
						total_price_without_promo=parseInt(modulo*individual_price_of_product_without_promotion);
						
						promo_str+='<tr><td>'+modulo+' * '+individual_price_of_product_without_promotion+'<br><small style="color:green;"><b><i>'+promotion_default_discount_promo+' for quantity ('+modulo+')</i></b></small></td><td> '+curr_sym+total_price_without_promo+'</td></tr>';
						
						total_amount+=total_price_without_promo;
						$("#decided_individual_price_of_product_without_promotion_req").val(individual_price_of_product_without_promotion);
						$("#decided_total_price_of_product_without_promotion_req").val(total_price_without_promo);
						$("#decided_quantity_without_promotion_req").val(modulo);
						
						
					}
				}
				
				if(promo_discount_available == 1){
					//calculation has been done while dumping for individual_price
					indi_price_with_promo=individual_price_of_product_with_promotion;
					total=parseFloat(indi_price_with_promo*desired_quantity_refund);
					promo_str+='<tr><td>'+desired_quantity_refund+' * '+indi_price_with_promo+'</td><td>'+curr_sym+total+'</td></tr>';
					total_amount+=parseFloat(total);
			
					$("#decided_individual_price_of_product_with_promotion_req").val(indi_price_with_promo);
					$("#decided_total_price_of_product_with_promotion_req").val(total);
					$("#decided_quantity_with_promotion_req").val(desired_quantity_refund);
				}
			}
			
		}	

		}//promo_available

			str_invoice_discount="";		

			if(order_item_invoice_discount_value>0){
				
				inv_discount=parseFloat((order_item_invoice_discount_value_each)*desired_quantity_refund).toFixed(2);
				
				total_product_price=parseFloat(total_amount-inv_discount).toFixed(2);
				
				str_invoice_discount='<tr><td> Invoice discount price which has to be deducted ('+desired_quantity_refund+' * '+order_item_invoice_discount_value_each+')</td><td style="color:red;">- '+curr_sym+inv_discount+'</td></tr>';
				
				total_amount=parseFloat(parseFloat(total_product_price)).toFixed(2);
			
			}else{
				total_amount=parseFloat(total_amount);
			}	
			
			str='<small><table class="table table-bordered table-hover"><thead><tr><th>Net Amount Calculations</th><th>Value</th></tr></thead><tbody>';
			
			str+=promo_str;
			str+=str_invoice_discount;
			//str+=shipping_price_str;
			
			str+='<tr><td>Total Product Price </td><td>'+curr_sym+total_amount+'</td></tr></tbody></table></small>'
			document.getElementById('details_of_refund_request_'+order_item_id).innerHTML=str;
			
			$("#total_product_price_requested_with_promo_admin_"+order_item_id).val(total_amount);
			
			return total_amount;
			
			}
</script>

<!--- modal request again ends----------------->

<!--- modal request again starts ----------------->

<script type="text/javascript">

			
function open_return_refund_request_again_fun_accepted(order_item_id,return_order_id){

	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/returns_refund_request_orders_refund_success_request_accepted_again",
		type:"POST",
		data:"return_order_id="+return_order_id+"&order_item_id="+order_item_id+"&status=accepted",
		success:function(data){
			
			$("#return_refund_request_again_accepted_form").html(data);
			$("#return_refund_request_again_accepted_modal").modal("show");
			
		}
	})
	
}
function open_return_refund_request_again_fun_rejected(order_item_id,return_order_id){

	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/returns_refund_request_orders_refund_success_request_rejected_again",
		type:"POST",
		data:"return_order_id="+return_order_id+"&order_item_id="+order_item_id+"&status=rejected",
		success:function(data){
			
			$("#return_refund_request_again_rejected_form").html(data);
			$("#return_refund_request_again_rejected_modal").modal("show");
			
		}
	})
	
}
</script>
<div class="modal" id="return_refund_request_again_accepted_modal" data-backdrop="static" role="dialog">
   <div class="modal-dialog modal-sm">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		  <div class="panel panel-info">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                   Accept refund request
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right"></span></span>
               </div>
              
			  <div aria-expanded="true" class="">
			  
                  <div class="panel-body">
		<form id="return_refund_request_again_accepted_form" method="post" enctype="multipart/form-data" class="form-horizontal">
		</form>
		 </div>
      </div>
   </div>
		 </div>
      </div>
   </div>
</div>



<script>
$(document).ready(function(){
	
				$("#return_refund_request_again_accepted_modal").on('shown.bs.modal', function () {
					
					order_item_id=$("#order_item_id_accept").val();
					var remain_quant_checkbox=$("#refund_remaining_quantities_"+order_item_id).is(':checked');

					if(remain_quant_checkbox==false){
						$('#quantity_requested_'+order_item_id).attr('readonly',true);
					
					}else{
						$('#quantity_requested_'+order_item_id).attr('readonly',false);
					}
			
					desired_quantity=$("#quantity_refund_acc").val();
					promotion_available=$("#promotion_available_acc").val();
					//alert(order_item_id);
					if(promotion_available==1){
						refund_promo_calculation_for_second_request_accept(desired_quantity,order_item_id);
					}else{
						//alert('promotion not available');
					}
						
				});
			});
			
function refund_promo_calculation_for_second_request_accept(desired_quantity,order_item_id){

            var curr_sym='<?php echo curr_sym; ?>';
			var desired_quantity_refund=desired_quantity;
			//get the onchange quantity as a desired quantity
			var requested_unitsmarker=parseInt(document.getElementById('quantity_refund_acc').value);
			var ordered_quantity=parseInt(document.getElementById('ordered_quantity_acc').value);
			var first_request_quantity=parseInt(document.getElementById('first_request_quantity_acc').value);
			
			var order_item_invoice_discount_value=parseFloat(document.getElementById('order_item_invoice_discount_value_acc_'+order_item_id).value) || 0;//"falsey" value to 0
			var order_item_invoice_discount_value_each=parseFloat(document.getElementById('order_item_invoice_discount_value_each_acc_'+order_item_id).value) || 0;//"falsey" value to 0
				
			var promotion_available=parseInt(document.getElementById('promotion_available_acc').value);
			var promotion_minimum_quantity=parseInt(document.getElementById('promotion_minimum_quantity_acc').value);
			var promotion_quote=document.getElementById('promotion_quote_acc').value
			var promotion_default_discount_promo=document.getElementById('promotion_default_discount_promo_acc').value
			
			var promotion_item=document.getElementById('promotion_item_acc').value
			var promotion_item_num=document.getElementById('promotion_item_num_acc').value
			var promotion_cashback=parseInt(document.getElementById('promotion_cashback_acc').value);
			var promotion_item_multiplier=parseInt(document.getElementById('promotion_item_multiplier_acc').value);
			var default_discount=parseInt(document.getElementById('default_discount_acc').value);
			var promotion_discount=parseInt(document.getElementById('promotion_discount_acc').value);

			var cash_back_value=parseInt(document.getElementById('cash_back_value_acc').value);
			var status_of_refund_for_cashback_on_sku=$("#status_of_refund_for_cashback_on_sku_"+order_item_id).val();
			var individual_price_of_product_with_promotion=parseFloat(document.getElementById('individual_price_of_product_with_promotion_acc').value);
			var individual_price_of_product_without_promotion=parseFloat(document.getElementById('individual_price_of_product_without_promotion_acc').value);
			var total_price_of_product_with_promotion=parseFloat(document.getElementById('total_price_of_product_with_promotion_acc').value) || 0;
			var total_price_of_product_without_promotion=parseFloat(document.getElementById('total_price_of_product_without_promotion_acc').value) || 0;
			
			if(promotion_available==1){

				$("#decided_individual_price_of_product_without_promotion_acc").val('');
				$("#decided_total_price_of_product_without_promotion_acc").val('');
				$("#decided_quantity_without_promotion_acc").val('');
				$("#decided_individual_price_of_product_with_promotion_acc").val('');
				$("#decided_total_price_of_product_with_promotion_acc").val('');
				$("#decided_quantity_with_promotion_acc").val('');
				$("#decided_cash_back_value_acc").val('');
			}
			
			var quotient=0;
			var modulo=0;
			var promo_str="";
			var total_amount=0;
		
		
		if(promotion_available==1){

			if((promotion_discount!=default_discount) && (promotion_discount>0)){
				promo_discount_available=1;
			}else{
				promo_discount_available=0;
			}
			
			modulo=parseInt(desired_quantity_refund%promotion_minimum_quantity);//remainder
			quotient=Math.floor(parseInt(desired_quantity_refund/promotion_minimum_quantity));//answer
			
			modulo_purchased=parseInt(ordered_quantity%promotion_minimum_quantity);//remainder
			quotient_purchased=Math.floor(parseInt(ordered_quantity/promotion_minimum_quantity));//answer

			var number_of_free_items=0;
			if(promotion_item_num!=''){
				
				var promo_item_arr = promotion_item_num.split(',');
				var free_item_count=0;
				number_of_free_items=promo_item_arr.length;
				for(h=0;h<number_of_free_items;h++){
					free_item_count+=parseInt(promo_item_arr[h]);
				}
			}
			var promotion_surprise_gift_type=document.getElementById('promotion_surprise_gift_type_acc').value;
			
			if(promotion_surprise_gift_type=='Qty'){
				quantity_filled=parseInt(desired_quantity_refund)+parseInt(first_request_quantity);
				remain=parseInt(ordered_quantity-quantity_filled);
				
				if(remain<promotion_minimum_quantity){
					return_surprise_gift=1;
				}else{
					return_surprise_gift=0;
				}
			}
			var promotion_surprise_gift=document.getElementById('promotion_surprise_gift_acc').value;

			if(promotion_surprise_gift_type=='<?php echo curr_code; ?>'){
				quantity_filled=parseInt(desired_quantity_refund)+parseInt(first_request_quantity);
				if(ordered_quantity==quantity_filled){
					return_surprise_gift_amount=1;
				}else{
					return_surprise_gift_amount=0;
				}
			}
			
		if(ordered_quantity >= promotion_minimum_quantity){
			
			if(desired_quantity_refund<promotion_minimum_quantity && (modulo_purchased!=modulo) && desired_quantity_refund!=quotient && (free_item_count>0 || promo_discount_available==1) && promo_discount_available != 1){
				if(modulo_purchased>0){
					alert("You cannot accept for this quantity.You can accept for multiples of "+promotion_minimum_quantity+ " or " +modulo_purchased+" quantity");
				}else{
					alert("You cannot accept for this quantity.You can accept for multiples of "+promotion_minimum_quantity+" quantity");
				}
	
				$("#details_of_refund_request_"+order_item_id).hide();
				$('#submit_button_'+order_item_id).prop('disabled', true);
				return false;
				
			}else if(desired_quantity_refund>promotion_minimum_quantity && modulo_purchased==0 && modulo>0 && promo_discount_available != 1){
				
				alert("You cannot accept for this quantity.You can accept for multiples of "+promotion_minimum_quantity);
				$("#details_of_refund_request_"+order_item_id).hide();
				//document.getElementById("quantity_accept_"+order_item_id).value=requested_unitsmarker;
				$('#submit_button_'+order_item_id).prop('disabled', true);
				return false;
				
			}else if(desired_quantity_refund>promotion_minimum_quantity && modulo_purchased>0 && modulo_purchased!=modulo && modulo!=0 && promo_discount_available != 1){
				
				alert("You cannot accept for this quantity.You can accept for multiples of "+promotion_minimum_quantity+ " or " +modulo_purchased);
				$("#details_of_refund_request_"+order_item_id).hide();
				//document.getElementById("quantity_accept_"+order_item_id).value=requested_unitsmarker;
				$('#submit_button_'+order_item_id).prop('disabled', true);
				return false;
			}
			
			$("#details_of_refund_request_"+order_item_id).show();
			$('#submit_button_'+order_item_id).prop('disabled', false);
			
			if(desired_quantity_refund >= promotion_minimum_quantity){
				
				if(promo_discount_available != 1){
					
					if(quotient>0){
						
						indi_price_with_promo=parseInt(individual_price_of_product_with_promotion/quotient);
						
						if(number_of_free_items>0){
							count_quotient=(quotient)*promotion_minimum_quantity;
						}else{
							count_quotient=quotient*promotion_minimum_quantity;
						}
						
						//alert(quotient);
						//alert(promotion_minimum_quantity);

						total_price_with_promo=count_quotient*individual_price_of_product_with_promotion;
						
						promo_str+='<tr><td>'+count_quotient+' * '+individual_price_of_product_with_promotion+'<br><small style="color:green;"><b><i>'+promotion_quote+' for quantity ('+count_quotient+')</i></b></small></td><td> '+curr_sym+total_price_with_promo+'</td></tr>';
						total_amount+=parseFloat(total_price_with_promo);
						
						
						$("#decided_individual_price_of_product_with_promotion_acc").val(individual_price_of_product_with_promotion);
						$("#decided_total_price_of_product_with_promotion_acc").val(total_price_with_promo);
						$("#decided_quantity_with_promotion_acc").val(count_quotient);
					
						if(promotion_cashback>0){
							if(status_of_refund_for_cashback_on_sku=="refunded"){
								cash_back_value=parseFloat(individual_price_of_product_with_promotion*(promotion_cashback/100)).toFixed(2);
								cash_back_value_with_quotient=parseFloat(quotient*cash_back_value).toFixed(2);
								promo_str+='<tr><td><span style="color:red;">Cashback value for quantity ('+quotient+')</span></td><td> <span style="color:red;"> -'+curr_sym+cash_back_value_with_quotient+'</span></td></tr>';
								total_amount-=parseFloat(cash_back_value_with_quotient);
							}else{
								cash_back_value_with_quotient=0;
							}
							$("#decided_cash_back_value_acc").val(cash_back_value_with_quotient);
						}
					}
					
					if(number_of_free_items>0){
						
						free_skus_from_inventory=$.parseJSON($('#free_inventory_available_acc').val());
						
						free_str="";
						
						//alert(free_skus_from_inventory);
						if(free_skus_from_inventory!=null){
							if(free_skus_from_inventory.length>0){
								
								for(y=0;y<free_skus_from_inventory.length;y++){
									
									free_str+=free_skus_from_inventory[y].str+" <small>("+quotient*parseInt(free_skus_from_inventory[y].count)+")</small> <br>";
								}
							}
							
						}

						promo_str+='<tr><td colspan="2"><b><h6 style="color:blue;"></b> Free Items to be returned with the requested Quantity of product</h6>'+free_str+'</td><tr>';
					}
					if(promotion_surprise_gift_type=='Qty'){
						if(return_surprise_gift==1){
						
							surprise_free_skus_from_inventory=$.parseJSON($('#surprise_free_inventory_available_acc').val());
							
							surprise_free_str="";
							
							if(surprise_free_skus_from_inventory!=null){
								if(surprise_free_skus_from_inventory.length>0){
									
									for(z=0;z<surprise_free_skus_from_inventory.length;z++){
										
										surprise_free_str+=surprise_free_skus_from_inventory[z].str+" <small>("+parseInt(surprise_free_skus_from_inventory[z].count)+")</small> <br>";
									}
								}
								
							}

							promo_str+='<tr><td colspan="2"><b><h6 style="color:blue;"></b>Surprise Free Items to be returned with the requested Quantity of product</h6>'+surprise_free_str+'</td><tr>';
						}
					}
					if(promotion_surprise_gift_type=='<?php echo curr_code; ?>'){
						if(return_surprise_gift_amount==1){
							promo_str+='<tr><td><h6 style="color:blue;">'+curr_sym+promotion_surprise_gift+' was credited to customer wallet. So this amount will be reduced from the total item price.</h6> </td><td style="color:red"> - '+curr_sym+promotion_surprise_gift+'</td></tr>';
							total_amount-=parseFloat(promotion_surprise_gift);
						}
					}
					if(modulo>0){
						//breakup 2
						indi_price_without_promo=parseInt(individual_price_of_product_without_promotion/modulo);
						
						total_price_without_promo=parseInt(modulo*individual_price_of_product_without_promotion);
						
						promo_str+='<tr><td>'+modulo+' * '+individual_price_of_product_without_promotion+'<br><small style="color:green;"><b><i>'+promotion_default_discount_promo+' for quantity ('+modulo+')</i></b></small></td><td> '+curr_sym
                            +total_price_without_promo+'</td></tr>';
						total_amount+=parseFloat(total_price_without_promo);
						$("#decided_individual_price_of_product_without_promotion_acc").val(individual_price_of_product_without_promotion);
						$("#decided_total_price_of_product_without_promotion_acc").val(total_price_without_promo);
						$("#decided_quantity_without_promotion_acc").val(modulo);
					}
					
				}
				if(promo_discount_available == 1){
					//calculation has been done while dumping for individual_price
					indi_price_with_promo=individual_price_of_product_with_promotion;
					total=parseFloat(indi_price_with_promo*desired_quantity_refund).toFixed(2);
					promo_str+='<tr><td>'+desired_quantity_refund+' * '+indi_price_with_promo+'</td><td>'+curr_sym+total+'</td></tr>';
					total_amount+=parseFloat(total);
					$("#decided_individual_price_of_product_with_promotion_acc").val(indi_price_with_promo);
					$("#decided_total_price_of_product_with_promotion_acc").val(total);
					$("#decided_quantity_with_promotion_acc").val(desired_quantity_refund);
				}
			
				
			}else{
				if(promo_discount_available != 1){
					if(modulo>0){
						//breakup 2
						indi_price_without_promo=parseInt(individual_price_of_product_without_promotion/modulo);
						
						total_price_without_promo=parseInt(modulo*individual_price_of_product_without_promotion);
						
						promo_str+='<tr><td>'+modulo+' * '+individual_price_of_product_without_promotion+'<br><small style="color:green;"><b><i>'+promotion_default_discount_promo+' for quantity ('+modulo+')</i></b></small></td><td> '+curr_sym+total_price_without_promo+'</td></tr>';
						
						total_amount+=parseFloat(total_price_without_promo);
						$("#decided_individual_price_of_product_without_promotion_acc").val(individual_price_of_product_without_promotion);
						$("#decided_total_price_of_product_without_promotion_acc").val(total_price_without_promo);
						$("#decided_quantity_without_promotion_acc").val(modulo);
						
						
					}
				}
				
				if(promo_discount_available == 1){
					//calculation has been done while dumping for individual_price
					indi_price_with_promo=individual_price_of_product_with_promotion;
					total=parseFloat(indi_price_with_promo*desired_quantity_refund).toFixed(2);
					promo_str+='<tr><td>'+desired_quantity_refund+' * '+indi_price_with_promo+'</td><td>'+curr_sym+total+'</td></tr>';
					total_amount+=parseFloat(total);
			
					$("#decided_individual_price_of_product_with_promotion_acc").val(indi_price_with_promo);
					$("#decided_total_price_of_product_with_promotion_acc").val(total);
					$("#decided_quantity_with_promotion_acc").val(desired_quantity_refund);
				}
			}
			
		}	

		}//promo_available

			str_invoice_discount="";		

			if(order_item_invoice_discount_value>0){
				
				inv_discount=parseFloat((order_item_invoice_discount_value_each)*desired_quantity_refund).toFixed(2);
				
				total_product_price=parseFloat(total_amount-inv_discount).toFixed(2);
				
				str_invoice_discount='<tr><td> Invoice discount price which has to be deducted ('+desired_quantity_refund+' * '+order_item_invoice_discount_value_each+')</td><td style="color:red;">- '+curr_sym+inv_discount+'</td></tr>';
				
				total_amount=parseFloat(parseFloat(total_product_price)).toFixed(2);
			
			}else{
				total_amount=parseFloat(total_amount);
			}	
			
			
			str='<small><table class="table table-bordered table-hover"><thead><tr><th>Net Amount Calculations</th><th>Value</th></tr></thead><tbody>';
			
			str+=promo_str;
			str+=str_invoice_discount;
			//str+=shipping_price_str;
			
			str+='<tr><td>Total Product Price </td><td>'+curr_sym+total_amount+'</td></tr></tbody></table></small>'
			document.getElementById('details_of_refund_request_'+order_item_id).innerHTML=str;
			
			$("#total_product_price_requested_with_promo_admin_"+order_item_id).val(total_amount);
			
			return total_amount;
			
			}

</script>

<div class="modal" id="return_refund_request_again_rejected_modal" data-backdrop="static" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		  <div class="panel panel-danger">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                   Rejected details
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right"></span></span>
               </div>
              
			  <div aria-expanded="true" class="">
			  
                  <div class="panel-body">
		<form id="return_refund_request_again_rejected_form" method="post" enctype="multipart/form-data" class="form-horizontal">
		</form>
		</div>
      </div>
   </div>
		 </div>
      </div>
   </div>
</div>



<script>
$(document).ready(function(){
	
				$("#return_refund_request_again_rejected_modal").on('shown.bs.modal', function () {
					
					order_item_id=$("#order_item_id_reject").val();
					var remain_quant_checkbox=$("#refund_remaining_quantities_"+order_item_id).is(':checked');

					if(remain_quant_checkbox==false){
						$('#quantity_requested_'+order_item_id).attr('readonly',true);
					
					}else{
						$('#quantity_requested_'+order_item_id).attr('readonly',false);
					}
			
					desired_quantity=$("#quantity_refund_rej").val();
					promotion_available=$("#promotion_available_rej").val();
					//alert(order_item_id);
					if(promotion_available==1){
						refund_promo_calculation_for_second_request_reject(desired_quantity,order_item_id);
					}else{
						//alert('promotion not available');
					}
						
				});
			});
function refund_promo_calculation_for_second_request_reject(desired_quantity,order_item_id){

            var curr_sym='<?php echo curr_sym; ?>';
			var desired_quantity_refund=desired_quantity;
			//get the onchange quantity as a desired quantity
			var requested_unitsmarker=parseInt(document.getElementById('quantity_refund_rej').value);
			var ordered_quantity=parseInt(document.getElementById('ordered_quantity_rej').value);
			var first_request_quantity=parseInt(document.getElementById('first_request_quantity_rej').value);
			
			var order_item_invoice_discount_value=parseFloat(document.getElementById('order_item_invoice_discount_value_rej_'+order_item_id).value) || 0;//"falsey" value to 0
			var order_item_invoice_discount_value_each=parseFloat(document.getElementById('order_item_invoice_discount_value_each_rej_'+order_item_id).value) || 0;//"falsey" value to 0
			
			var promotion_available=parseInt(document.getElementById('promotion_available_rej').value);
			var promotion_minimum_quantity=parseInt(document.getElementById('promotion_minimum_quantity_rej').value);
			var promotion_quote=document.getElementById('promotion_quote_rej').value
			var promotion_default_discount_promo=document.getElementById('promotion_default_discount_promo_rej').value
			
			var promotion_item=document.getElementById('promotion_item_rej').value
			var promotion_item_num=document.getElementById('promotion_item_num_rej').value
			var promotion_cashback=parseInt(document.getElementById('promotion_cashback_rej').value);
			var promotion_item_multiplier=parseInt(document.getElementById('promotion_item_multiplier_rej').value);
			var default_discount=parseInt(document.getElementById('default_discount_rej').value);
			var promotion_discount=parseInt(document.getElementById('promotion_discount_rej').value);

			var cash_back_value=parseInt(document.getElementById('cash_back_value_rej').value);
			var status_of_refund_for_cashback_on_sku=$("#status_of_refund_for_cashback_on_sku_"+order_item_id).val();
			var individual_price_of_product_with_promotion=parseFloat(document.getElementById('individual_price_of_product_with_promotion_rej').value);
			var individual_price_of_product_without_promotion=parseFloat(document.getElementById('individual_price_of_product_without_promotion_rej').value);
			var total_price_of_product_with_promotion=parseFloat(document.getElementById('total_price_of_product_with_promotion_rej').value) || 0;
			var total_price_of_product_without_promotion=parseFloat(document.getElementById('total_price_of_product_without_promotion_rej').value) || 0;
			
			if(promotion_available==1){

				$("#decided_individual_price_of_product_without_promotion_rej").val('');
				$("#decided_total_price_of_product_without_promotion_rej").val('');
				$("#decided_quantity_without_promotion_rej").val('');
				$("#decided_individual_price_of_product_with_promotion_rej").val('');
				$("#decided_total_price_of_product_with_promotion_rej").val('');
				$("#decided_quantity_with_promotion_rej").val('');
				$("#decided_cash_back_value_rej").val('');
			}
			
			var quotient=0;
			var modulo=0;
			var promo_str="";
			var total_amount=0;
		
		
		if(promotion_available==1){

			if((promotion_discount!=default_discount) && (promotion_discount>0)){
				promo_discount_available=1;
			}else{
				promo_discount_available=0;
			}
			
			modulo=parseInt(desired_quantity_refund%promotion_minimum_quantity);//remainder
			quotient=Math.floor(parseInt(desired_quantity_refund/promotion_minimum_quantity));//answer
			
			modulo_purchased=parseInt(ordered_quantity%promotion_minimum_quantity);//remainder
			quotient_purchased=Math.floor(parseInt(ordered_quantity/promotion_minimum_quantity));//answer

			var number_of_free_items=0;
			if(promotion_item_num!=''){
				
				var promo_item_arr = promotion_item_num.split(',');
				var free_item_count=0;
				number_of_free_items=promo_item_arr.length;
				for(h=0;h<number_of_free_items;h++){
					free_item_count+=parseInt(promo_item_arr[h]);
				}
			}
			
			var promotion_surprise_gift_type=document.getElementById('promotion_surprise_gift_type_rej').value;
			
			if(promotion_surprise_gift_type=='Qty'){
				quantity_filled=parseInt(desired_quantity_refund)+parseInt(first_request_quantity);
				remain=parseInt(ordered_quantity-quantity_filled);
				
				if(remain<promotion_minimum_quantity){
					return_surprise_gift=1;
				}else{
					return_surprise_gift=0;
				}
			}
			var promotion_surprise_gift=document.getElementById('promotion_surprise_gift_rej').value;

			if(promotion_surprise_gift_type=='<?php echo curr_code; ?>'){
				if(ordered_quantity==desired_quantity_refund){
					return_surprise_gift_amount=1;
				}else{
					return_surprise_gift_amount=0;
				}
			}
			
		if(ordered_quantity >= promotion_minimum_quantity){
			
			if(desired_quantity_refund<promotion_minimum_quantity && (modulo_purchased!=modulo) && desired_quantity_refund!=quotient && (free_item_count>0 || promo_discount_available==1) && promo_discount_available != 1){
				if(modulo_purchased>0){
					alert("You cannot accept for this quantity.You can accept for multiples of "+promotion_minimum_quantity+ " or " +modulo_purchased+" quantity");
				}else{
					alert("You cannot accept for this quantity.You can accept for multiples of "+promotion_minimum_quantity+" quantity");
				}
	
				$("#details_of_refund_request_"+order_item_id).hide();
				$('#submit_button_'+order_item_id).prop('disabled', true);
				return false;
				
			}else if(desired_quantity_refund>promotion_minimum_quantity && modulo_purchased==0 && modulo>0 && promo_discount_available != 1){
				
				alert("You cannot accept for this quantity.You can accept for multiples of "+promotion_minimum_quantity);
				$("#details_of_refund_request_"+order_item_id).hide();
				//document.getElementById("quantity_accept_"+order_item_id).value=requested_unitsmarker;
				$('#submit_button_'+order_item_id).prop('disabled', true);
				return false;
				
			}else if(desired_quantity_refund>promotion_minimum_quantity && modulo_purchased>0 && modulo_purchased!=modulo && modulo!=0 && promo_discount_available != 1){
				
				alert("You cannot accept for this quantity.You can accept for multiples of "+promotion_minimum_quantity+ " or " +modulo_purchased);
				$("#details_of_refund_request_"+order_item_id).hide();
				//document.getElementById("quantity_accept_"+order_item_id).value=requested_unitsmarker;
				$('#submit_button_'+order_item_id).prop('disabled', true);
				return false;
			}
			
			$("#details_of_refund_request_"+order_item_id).show();
			$('#submit_button_'+order_item_id).prop('disabled', false);
			
			if(desired_quantity_refund >= promotion_minimum_quantity){
				
				if(promo_discount_available != 1){
					
					if(quotient>0){
						
						indi_price_with_promo=parseInt(individual_price_of_product_with_promotion/quotient);
						
						if(number_of_free_items>0){
							count_quotient=(quotient)*promotion_minimum_quantity;
						}else{
							count_quotient=quotient*promotion_minimum_quantity;
						}
						
						//alert(quotient);
						//alert(promotion_minimum_quantity);

						total_price_with_promo=count_quotient*individual_price_of_product_with_promotion;
						
						promo_str+='<tr><td>'+count_quotient+' * '+individual_price_of_product_with_promotion+'<br><small style="color:green;"><b><i>'+promotion_quote+' for quantity ('+count_quotient+')</i></b></small></td><td> '+curr_sym+total_price_with_promo+'</td></tr>';
						total_amount+=parseFloat(total_price_with_promo);
						
						
						$("#decided_individual_price_of_product_with_promotion_rej").val(individual_price_of_product_with_promotion);
						$("#decided_total_price_of_product_with_promotion_rej").val(total_price_with_promo);
						$("#decided_quantity_with_promotion_rej").val(count_quotient);
					
						if(promotion_cashback>0){
							if(status_of_refund_for_cashback_on_sku=="refunded"){
								cash_back_value=parseFloat(individual_price_of_product_with_promotion*(promotion_cashback/100)).toFixed(2);
								
								cash_back_value_with_quotient=parseFloat(quotient*cash_back_value).toFixed(2);
								promo_str+='<tr><td><span style="color:red;">Cashback value for quantity ('+quotient+')</span></td><td> <span style="color:red;"> -'+curr_sym+cash_back_value_with_quotient+'</span></td></tr>';

								total_amount-=parseFloat(cash_back_value_with_quotient);
							}else{
								cash_back_value_with_quotient=0;
							}
								$("#decided_cash_back_value_rej").val(cash_back_value_with_quotient);
						}
					}
					
					if(number_of_free_items>0){
						
						free_skus_from_inventory=$.parseJSON($('#free_inventory_available_rej').val());
						
						free_str="";
						
						//alert(free_skus_from_inventory);
						if(free_skus_from_inventory!=null){
							if(free_skus_from_inventory.length>0){
								
								for(y=0;y<free_skus_from_inventory.length;y++){
									
									free_str+=free_skus_from_inventory[y].str+" <small>("+quotient*parseInt(free_skus_from_inventory[y].count)+")</small> <br>";
								}
							}
							
						}

						promo_str+='<tr><td colspan="2"><b><h6 style="color:blue;"></b> Free Items to be returned with the requested Quantity of product</h6>'+free_str+'</td><tr>';
					}
					if(promotion_surprise_gift_type=='Qty'){
						if(return_surprise_gift==1){
						
							surprise_free_skus_from_inventory=$.parseJSON($('#surprise_free_inventory_available_rej').val());
							
							surprise_free_str="";
							
							if(surprise_free_skus_from_inventory!=null){
								if(surprise_free_skus_from_inventory.length>0){
									
									for(z=0;z<surprise_free_skus_from_inventory.length;z++){
										
										surprise_free_str+=surprise_free_skus_from_inventory[z].str+" <small>("+parseInt(surprise_free_skus_from_inventory[z].count)+")</small> <br>";
									}
								}
								
							}

							promo_str+='<tr><td colspan="2"><b><h6 style="color:blue;"></b>Surprise Free Items to be returned with the requested Quantity of product</h6>'+surprise_free_str+'</td><tr>';
						}
					}
					if(promotion_surprise_gift_type=='<?php echo curr_code; ?>'){
						if(return_surprise_gift_amount==1){
							promo_str+='<tr><td><h6 style="color:blue;">'+curr_sym+promotion_surprise_gift+' was credited to customer wallet. So this amount will be reduced from the total item price.</h6> </td><td style="color:red"> - '+curr_sym+promotion_surprise_gift+'</td></tr>';
							total_amount-=parseFloat(promotion_surprise_gift);
						}
					}
					if(modulo>0){
						//breakup 2
						indi_price_without_promo=parseInt(individual_price_of_product_without_promotion/modulo);
						
						total_price_without_promo=parseInt(modulo*individual_price_of_product_without_promotion);
						
						promo_str+='<tr><td>'+modulo+' * '+individual_price_of_product_without_promotion+'<br><small style="color:green;"><b><i>'+promotion_default_discount_promo+' for quantity ('+modulo+')</i></b></small></td><td> '+curr_sym+total_price_without_promo+'</td></tr>';
						total_amount+=parseFloat(total_price_without_promo);
						$("#decided_individual_price_of_product_without_promotion_rej").val(individual_price_of_product_without_promotion);
						$("#decided_total_price_of_product_without_promotion_rej").val(total_price_without_promo);
						$("#decided_quantity_without_promotion_rej").val(modulo);
					}
					
				}
				if(promo_discount_available == 1){
					//calculation has been done while dumping for individual_price
					indi_price_with_promo=individual_price_of_product_with_promotion;
					total=parseFloat(indi_price_with_promo*desired_quantity_refund).toFixed(2);
					promo_str+='<tr><td>'+desired_quantity_refund+' * '+indi_price_with_promo+'</td><td>'+curr_sym+total+'</td></tr>';
					total_amount+=parseFloat(total);
					$("#decided_individual_price_of_product_with_promotion_rej").val(indi_price_with_promo);
					$("#decided_total_price_of_product_with_promotion_rej").val(total);
					$("#decided_quantity_with_promotion_rej").val(desired_quantity_refund);
				}
			
				
			}else{
				if(promo_discount_available != 1){
					if(modulo>0){
						//breakup 2
						indi_price_without_promo=parseInt(individual_price_of_product_without_promotion/modulo);
						
						total_price_without_promo=parseInt(modulo*individual_price_of_product_without_promotion);
						
						promo_str+='<tr><td>'+modulo+' * '+individual_price_of_product_without_promotion+'<br><small style="color:green;"><b><i>'+promotion_default_discount_promo+' for quantity ('+modulo+')</i></b></small></td><td> '+curr_sym+total_price_without_promo+'</td></tr>';
						
						total_amount+=parseFloat(total_price_without_promo);
						$("#decided_individual_price_of_product_without_promotion_rej").val(individual_price_of_product_without_promotion);
						$("#decided_total_price_of_product_without_promotion_rej").val(total_price_without_promo);
						$("#decided_quantity_without_promotion_rej").val(modulo);
						
						
					}
				}
				
				if(promo_discount_available == 1){
					//calculation has been done while dumping for individual_price
					indi_price_with_promo=individual_price_of_product_with_promotion;
					total=parseFloat(indi_price_with_promo*desired_quantity_refund).toFixed(2);
					promo_str+='<tr><td>'+desired_quantity_refund+' * '+indi_price_with_promo+'</td><td>'+curr_sym+total+'</td></tr>';
					total_amount+=parseFloat(total);
			
					$("#decided_individual_price_of_product_with_promotion_rej").val(indi_price_with_promo);
					$("#decided_total_price_of_product_with_promotion_rej").val(total);
					$("#decided_quantity_with_promotion_rej").val(desired_quantity_refund);
				}
			}
			
		}	

		}//promo_available

			str_invoice_discount="";		

			if(order_item_invoice_discount_value>0){
				
				inv_discount=parseFloat((order_item_invoice_discount_value_each)*desired_quantity_refund).toFixed(2);
				
				total_product_price=parseFloat(total_amount-inv_discount).toFixed(2);
				
				str_invoice_discount='<tr><td> Invoice discount price which has to be deducted ('+desired_quantity_refund+' * '+order_item_invoice_discount_value_each+')</td><td style="color:red;">- '+curr_sym+inv_discount+'</td></tr>';
				
				total_amount=parseFloat(parseFloat(total_product_price)).toFixed(2);
			
			}else{
				total_amount=parseFloat(total_amount);
			}	
			str='<small><table class="table table-bordered table-hover"><thead><tr><th>Net Amount Calculations</th><th>Value</th></tr></thead><tbody>';
			
			str+=promo_str;
			str+=str_invoice_discount;
			//str+=shipping_price_str;
			
			str+='<tr><td>Total Product Price </td><td>'+curr_sym+total_amount+'</td></tr></tbody></table></small>'
			document.getElementById('details_of_refund_request_'+order_item_id).innerHTML=str;
			
			$("#total_product_price_requested_with_promo_admin_"+order_item_id).val(total_amount);
			
			return total_amount;
			
			}

</script>
	
<!---------------------->
<div class="modal" id="order_summary">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Order Summary</h4>
           </div>
           <div  class="modal-body" id="model_content">
		   <div class="container">
				
			</div>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close"class="btn">Close</a>
        </div>
     </div>
    </div>
  </div>
  
  
  
  
  
  <script type="text/javascript">
  $("#order_summary").modal("hide");
  function view_order_summary_(order_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/get_data_from_invoice_offers",
		type:"POST",
		data:"order_id="+order_id,
		success:function(data){
			
			$("#description").val("");	
			$("#model_content").html(data);
			$("#order_summary").modal("show");
			
		}
	})
	
}

function go_to_ref_details(order_item_id){	
	 $('#ref_details_'+order_item_id).attr('action', '<?php echo base_url();?>admin/Returns/ref_details_of_order_item').submit();
}
  </script>
  
  
  <!--------------free Items-return modal------>

<div class="modal" id="return_free_items_modal" data-backdrop="static" role="dialog">
		<div class="modal-dialog">
		  <!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body"> 
			 
					<div class="panel panel-info">
					   <div id="accordion" class="panel-heading" style="cursor:pointer;">
						  Details of Promotion
						  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right"></span></span>
					   </div>
					   <div aria-expanded="true">
							<div class="panel-body" id="return_free_items_data">
								
							</div>
					   </div>
					</div>
					<div class="modal-footer">
						<a class="text-info bold" data-dismiss="modal" style="cursor:pointer;">Close</a>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	
	<script>
	function show_freeitems_return(obj){		
		$('#return_free_items_modal').modal();
		html_str=obj.getElementsByTagName("span")[0].innerHTML;
		$('#return_free_items_data').html(html_str);
	}
	</script>
  
  </div>
</body> 
</html>  
