<html lang="en">
<head>

<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<style>
#current_password_message,#new_password_message,#confirm_new_password_message{
	color:#ff0000;
}
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script type="text/javascript">

function isNumber(evt) {

    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
	
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
	
    return true;
}	   
</script>
<script>

function showDivPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/vendors';
}
</script>

<?php //print_r($vendor);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row-fluid" id="editDiv3">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="change_password" id="change_password" method="post" action="<?php echo base_url()?>admin/Catalogue/vendor_update_password" >
					
		<input type="hidden" name="vendor_id" id="vendor_id" value="<?php echo $get_vendors_data["vendor_id"];?>">
		
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Change Password</h3>
		<div>	
		</div>
		<div class="tab-content">
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Current Password</label>
					<input type="password" class="form-control" id="current_password" name="current_password" onblur="checkCurrentPassword()" value="">
					<span id="current_password_message"></span>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">New Password:</label>
					<input type="password" class="form-control" onblur="check_password_strength()"  id="new_password" name="new_password" value="">
					<span id="new_password_message"></span>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Confirm Password:</label>
					<input type="password" class="form-control" onkeyup="check_password_match()" disabled id="confirm_new_password" name="confirm_new_password" value="">
						<span id="confirm_new_password_message"></span>
				</div>
				</div>
			</div>


			<!---new fields--->
			

			<div class="row">
				<div class="col-md-10 col-md-offset-1">
				<div class="form-group">
					
					<button type="submit" disabled id="submit_button" class="btn btn-success preventDflt"> Update </button>
				</div>	
				</div>
			</div>		
	
	</div>
</form>

</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>
</div>
</body>
</html>

<script>
	function checkCurrentPassword() {
		var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
		 var current_password = document.getElementById('current_password').value; 
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
					
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
		                   if(xhr.responseText==1){
		                   	document.getElementById('current_password_message').innerHTML=""
		                   	document.getElementById('confirm_new_password').disabled = false;
		                   	document.getElementById('new_password').disabled = false;
		                   	document.getElementById('current_password_container').remove();
		                   }
		                   else{
		                   	document.getElementById('current_password_message').innerHTML="Please enter your valid password";
		                   	document.getElementById('confirm_new_password').disabled = true;
		                   	document.getElementById('new_password').disabled = true;

		                   }
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>admin/Catalogue/vendor_check_current_password", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("current_password="+current_password);
		        }
		}
	 function check_password_strength(){
			var new_pass=document.getElementById('new_password').value;
			var pass_ex=/(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z]).{6,10}/;
			
			if(pass_ex.test(new_pass)){
				if(document.getElementById('confirm_new_password').value!=""){
					check_password_match();
					document.getElementById('confirm_new_password').disabled = false;
				}
				else{
					document.getElementById('confirm_new_password').disabled = false;
					document.getElementById('new_password_message').innerHTML=""
				}
				
			}
			else{
				document.getElementById('confirm_new_password').disabled = true;
				document.getElementById('new_password_message').innerHTML="Must contain one lower & uppercase letter, and one non-alpha character (a number or a symbol.), min six characters to max ten characters"
				document.getElementById('submit_button').disabled = true;
			}
		}
	function check_password_match(){
		var new_pass=document.getElementById('new_password').value;
		var confirm_new_password=document.getElementById('confirm_new_password').value;
		if(new_pass==confirm_new_password){
			document.getElementById('submit_button').disabled = false;
			document.getElementById('confirm_new_password_message').innerHTML="";
			document.getElementById('new_password_message').innerHTML=""
		}
		if(new_pass!=confirm_new_password){
			document.getElementById('submit_button').disabled = true;
			document.getElementById('confirm_new_password_message').innerHTML="New password and confirm password do not match"
		}
	}

    $('form#change_password').submit(function(){
        $(this).find(':button[type=submit]').prop('disabled', true);
    });
</script>