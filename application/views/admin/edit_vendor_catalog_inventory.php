<html lang="en">
<head>


<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />

<style type="text/css">
.form-group{
	text-align:left;
}
.Editor-container{
	text-align:left !important;
}
 .wizard-navigation {
        -webkit-transform: translateZ(0);
		-webkit-backface-visibility: hidden;
    }
.small_color_box{
	width: 30px;
	height: 30px;
	border: 1px solid #ccc;
	margin-top: 1px;
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}

#edit_wizardPictureSmall_picname{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname2{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname2{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname3{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname3{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname4{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname4{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname5{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname5{
	word-wrap: break-word;
}
#edit_wizardPictureSmall_picname6{
	word-wrap: break-word;
}
#edit_wizardPictureBig_picname6{
	word-wrap: break-word;
}
textarea.form-control {
    height:38px;
}
.CamListDiv {
    height: 450px;
    float: left;
    overflow: scroll;
    overflow-x:hidden;

}
.well{
	width:100%;

}

</style>

<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 


<script type="text/javascript">

function isNumber(evt) {

    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
	
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
	
    return true;
}	   
</script>
<script>

function showDivPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/vendors';
}
</script>

<?php //print_r($vendor);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row-fluid" id="editDiv3">
<div class="col-md-8 col-md-offset-2">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
			
<?php 
if(!empty($inventories)){
	
	$highlight=$inventories->description;
$highlight1=str_replace("\n", '', $highlight);

$string = trim(preg_replace('/\s+/', ' ', $highlight1));

		?>

<form name="create_inventory" id="create_inventory" enctype="multipart/form-data">

<input type="hidden" name="action" value="edit">
<input type="hidden" name="vendor_catalog_id" value="<?php echo $inventories->vendor_catalog_id; ?>">

	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				// sku_id: {
				// 	required: true,
				// 	minlength: 3
				//  },
				//  moq: {
				// 	required: true,
		     
				// },
				// max_oq: {
				// 	required: true,
		      
				// },
				// stock: {
				// 	required: true,
		      
				// }
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>				
		
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Build My Catalog</h3>
		<div>	
		</div>
		<input name="previous_largeimage1" id="previous_largeimage1" type="hidden" value="<?php echo $inventories->largeimage;?>"/>
			<input name="previous_image" id="previous_image" type="hidden" value="<?php echo $inventories->common_image;?>"/>
			<input name="previous_image1" id="previous_image1" type="hidden" value="<?php echo $inventories->image;?>"/>
			<input name="previous_thumbnail1" id="previous_thumbnail1" type="hidden" value="<?php echo $inventories->thumbnail;?>"/>
			
			<input name="previous_largeimage2" id="previous_largeimage2" type="hidden" value="<?php echo $inventories->largeimage2;?>"/>
			<input name="previous_image2" id="previous_image2" type="hidden" value="<?php echo $inventories->image2;?>"/>
			<input name="previous_thumbnail2" id="previous_thumbnail2" type="hidden" value="<?php echo $inventories->thumbnail2;?>"/>
			
			<input name="previous_largeimage3" id="previous_largeimage3" type="hidden" value="<?php echo $inventories->largeimage3;?>"/>
			<input name="previous_image3" id="previous_image3" type="hidden" value="<?php echo $inventories->image3;?>"/>
			<input name="previous_thumbnail3" id="previous_thumbnail3" type="hidden" value="<?php echo $inventories->thumbnail3;?>"/>
			
			<input name="previous_largeimage4" id="previous_largeimage4" type="hidden" value="<?php echo $inventories->largeimage4;?>"/>
			<input name="previous_image4" id="previous_image4" type="hidden" value="<?php echo $inventories->image4;?>"/>
			<input name="previous_thumbnail4" id="previous_thumbnail4" type="hidden" value="<?php echo $inventories->thumbnail4;?>"/>
			
			<input name="previous_largeimage5" id="previous_largeimage5" type="hidden" value="<?php echo $inventories->largeimage5;?>"/>
			<input name="previous_image5" id="previous_image5" type="hidden" value="<?php echo $inventories->image5;?>"/>
			<input name="previous_thumbnail5" id="previous_thumbnail5" type="hidden" value="<?php echo $inventories->thumbnail5;?>"/>
			
			
			<input name="previous_largeimage6" id="previous_largeimage6" type="hidden" value="<?php echo $inventories->largeimage6;?>"/>
			<input name="previous_image6" id="previous_image6" type="hidden" value="<?php echo $inventories->image6;?>"/>
			<input name="previous_thumbnail6" id="previous_thumbnail6" type="hidden" value="<?php echo $inventories->thumbnail6;?>"/>
			
			
		
			<input name="previous_largeimage1_unlink" id="previous_largeimage1_unlink" type="hidden" value="<?php echo $inventories->largeimage;?>"/>
			<input name="previous_image1_unlink" id="previous_image1_unlink" type="hidden" value="<?php echo $inventories->image;?>"/>
			<input name="previous_thumbnail1_unlink" id="previous_thumbnail1_unlink" type="hidden" value="<?php echo $inventories->thumbnail;?>"/>
			
			<input name="previous_largeimage2_unlink" id="previous_largeimage2_unlink" type="hidden" value="<?php echo $inventories->largeimage2;?>"/>
			<input name="previous_image2_unlink" id="previous_image2_unlink" type="hidden" value="<?php echo $inventories->image2;?>"/>
			<input name="previous_thumbnail2_unlink" id="previous_thumbnail2_unlink" type="hidden" value="<?php echo $inventories->thumbnail2;?>"/>
			
			<input name="previous_largeimage3_unlink" id="previous_largeimage3_unlink" type="hidden" value="<?php echo $inventories->largeimage3;?>"/>
			<input name="previous_image3_unlink" id="previous_image3_unlink" type="hidden" value="<?php echo $inventories->image3;?>"/>
			<input name="previous_thumbnail3_unlink" id="previous_thumbnail3_unlink" type="hidden" value="<?php echo $inventories->thumbnail3;?>"/>
			
			<input name="previous_largeimage4_unlink" id="previous_largeimage4_unlink" type="hidden" value="<?php echo $inventories->largeimage4;?>"/>
			<input name="previous_image4_unlink" id="previous_image4_unlink" type="hidden" value="<?php echo $inventories->image4;?>"/>
			<input name="previous_thumbnail4_unlink" id="previous_thumbnail4_unlink" type="hidden" value="<?php echo $inventories->thumbnail4;?>"/>
			
			<input name="previous_largeimage5_unlink" id="previous_largeimage5_unlink" type="hidden" value="<?php echo $inventories->largeimage5;?>"/>
			<input name="previous_image5_unlink" id="previous_image5_unlink" type="hidden" value="<?php echo $inventories->image5;?>"/>
			<input name="previous_thumbnail5_unlink" id="previous_thumbnail5_unlink" type="hidden" value="<?php echo $inventories->thumbnail5;?>"/>
			
			<input name="previous_largeimage6_unlink" id="previous_largeimage6_unlink" type="hidden" value="<?php echo $inventories->largeimage6;?>"/>
			<input name="previous_image6_unlink" id="previous_image6_unlink" type="hidden" value="<?php echo $inventories->image6;?>"/>
			<input name="previous_thumbnail6_unlink" id="previous_thumbnail6_unlink" type="hidden" value="<?php echo $inventories->thumbnail6;?>"/>
			
			
		
		<div class="tab-content">
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">What Furniture Are You Uploading?</label>
					<select id="category_id" name="category_id" class="form-control"/>
					
					<option value=""></option>


					<?php
					foreach($parent_category as $parent){
						?>
						<option value="<?php echo $parent->pcat_id; ?>" <?php echo ($inventories->category_id==$parent->pcat_id) ? 'selected' : ''; ?>><?php echo $parent->pcat_name; ?></option>
						<?php
					}
					?>

					</select>
				</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter SKU Name</label>
					<input id="sku_name" name="sku_name" type="text" class="form-control" value="<?php echo $inventories->sku_name;  ?>"/>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter SKU ID</label>
					<input id="sku_id" name="sku_id" type="text" class="form-control" value="<?php echo $inventories->sku_id;  ?>"/>
				</div>
				</div>
			</div>

			<!-- <div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Material</label>
					<input id="material" name="material" type="text" class="form-control" value=""/>
				</div>
				</div>
			</div> -->

			<!-- <div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">description</label>
					<textarea id="description" name="description" type="text" class="form-control"></textarea>
				</div>
				</div>
			</div> -->

			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">End Customer Selling Price (Incl. All Taxes) (Rs.)</label>
					<input id="offer_price" name="offer_price" type="text" class="form-control" value="<?php echo $inventories->offer_price;  ?>" onkeyup="calculateTax_priceFun();" />
				</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label"><?php echo SITE_NAME; ?> Margin Percentage(%)</label>
					<input id="admin_margin_percentage" maxlength="2" onKeyPress="return isNumber(event)" name="admin_margin_percentage" type="text" class="form-control" value="<?php echo $inventories->admin_margin_percentage;  ?>" onkeyup="calculateTax_priceFun();"/>
				</div>
				</div>
			</div>
			<div class="row">
			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating" id="taxable_price_div" >
					<label class="control-label">Actual <?php echo SITE_NAME; ?> Margin (Rs.)</label>
					<input name="admin_margin_value" id="taxable_price" type="text" class="form-control"  value="<?php echo $inventories->admin_margin_value;  ?>" readonly/>
				</div>
			</div>

			</div>

			<div class="row">
			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">Price Validity</label>
					<input name="price_validity" id="price_validity" type="text" class="form-control" value="<?php echo $inventories->price_validity;  ?>"/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">Material Type of SKU (Wood, Steel, etc., )</label>
					<input name="material" id="material" type="text" class="form-control" value="<?php echo $inventories->material;  ?>" />
				</div>
			</div>
			
		</div>

		<div class="row">
			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">Enter Minimum Order Quantity Per Order</label>
					<input name="moq" type="number" class="form-control" min="1" value="<?php echo $inventories->moq;  ?>" />
				</div>
			</div>
			</div>

		<div class="row">
			<div class="col-md-10 col-md-offset-1"> 	
			
				<div class="form-group label-floating">
					<label class="control-label">Enter Maximum Order Quantity  Per Order</label>
					<input name="max_oq" type="number" class="form-control" min="1" value="<?php echo $inventories->max_oq;  ?>"/>
				</div>
			</div>
		</div>

		<div class="row">
			
			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">- Select A Relavent Stock Status -</label>
					<input type="hidden" value="<?php echo $inventories->product_status; ?>" name="old_product_status">
					<select name="product_status" id="product_status" class="form-control search_select" onchange="show_input()">
						<option value="" selected></option>
						<option value="In Stock"  <?php echo ($inventories->product_status=='In Stock') ? 'selected' : ''; ?>>Ready In Stock</option>
						<option value="Made To Order"  <?php echo ($inventories->product_status=='Made To Order') ? 'selected' : ''; ?>>Made To Order</option>
						<option value="Out Of Stock" <?php echo ($inventories->product_status=='Out Of Stock') ? 'selected' : ''; ?>>Out Of Stock</option>
						<option value="Discontinued" <?php echo ($inventories->product_status=='Discontinued') ? 'selected' : ''; ?>>Discontinued</option>
					</select>
				</div>
			</div>
		</div>


		<!--- new fields--->

		<div class="row div_hide" id="stock_div_count" style="display:none;">

			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">	
					<label class="control-label">Currently How Many Units Are Available In Your Warehouse?</label>
					<input class="form-control" name="stock" id="stock" type="number" min="1" value="<?php echo $inventories->stock; ?>">
				</div>
			</div>

		</div>

		<!--- new fields--->

		<div class="row div_hide" id="made_to_order_div" style="display:none">

			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">	
					<label class="control-label">How Many Days It takes To Manufacture One Piece?</label>
					<input class="form-control" name="manufacture_days_per_piece" id="manufacture_days_per_piece" type="number" min="1" value="<?php echo $inventories->manufacture_days_per_piece; ?>">
				</div>
			</div>

		</div>

		
		<div class="row div_hide" id="out_of_stock_div" style="display:none">

			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">	
					<label class="control-label">By When This SKU Will Be Available?</label>
					<input class="form-control" name="sku_available_date" id="sku_available_date" type="text"  value="<?php echo ($inventories->sku_available_date=='0000-00-00' || $inventories->sku_available_date=='0000-00-00 00:00:00') ? '': $inventories->sku_available_date ; ?>" >
				</div>
			</div>

		</div>

		<div class="row div_hide" id="discontinuation_div" style="display:none">

			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">	
					<label class="control-label">Discontinued Date</label>
					<input type="text" class="form-control" value="<?php echo ($inventories->discontinued_date!='0000-00-00') ? $inventories->discontinued_date : ''; ?>" name="discontinued_date" id="discontinued_date">
				</div>
			</div>
			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">	
					<label class="control-label">Please Provide the Discontinuation Comments</label>
					<textarea class="form-control" name="discontinuation_comments" id="discontinuation_comments"><?php echo  $inventories->discontinuation_comments; ?></textarea> 
				</div>
			</div>

			

		</div>


		<div class="row">
			<div class="col-md-10 col-md-offset-1"> 	
			<h4>Product Description</h4>
					<textarea class="form-control" name="highlight" id="highlight"></textarea>
			</div>
		</div>

		<div class="row">
				<div class="col-md-10"> 
					<div class="form-group label-floating">
					<div class="col-md-5 col-md-offset-1">Do You Offer EMI Options ?</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="emi_options" id="emi1" value="yes" <?php echo ($inventories->emi_options=='yes') ? 'checked' : ''; ?> type="radio" >Yes</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input <?php echo ($inventories->emi_options=='no') ? 'checked' : ''; ?> name="emi_options" id="emi2" value="no" type="radio">No</label>
					</div>
				
					</div>
				</div>
			</div>	

			<!---- new fields---->
			<div class="row">
			<div class="" id="inventory_unit_div">
				<div class="col-md-2 col-md-offset-1"> 
					<div class="form-group">
						<label class="control-label">Select Unit
						</label>
						<select class="form-control" name="inventory_unit" id="inventory_unit" >
							<option value=""></option>
							<option value="mm" <?php echo ($inventories->inventory_unit=='mm') ? 'selected': '' ?>>mm</option>
							<option value="cm"  <?php echo ($inventories->inventory_unit=='cm') ? 'selected': '' ?>>cm</option>
							<option value="m"  <?php echo ($inventories->inventory_unit=='m') ? 'selected': '' ?>>m</option>
						</select>
					</div>
				</div>
			</div>	
			<div class="" id="inventory_length_div">
				<div class="col-md-3"> 
					<div class="form-group">
						<label class="control-label">Length
						</label>
						<input class="form-control" name="inventory_length" id="inventory_length" min="1" type="number" step="any" value="<?php echo $inventories->inventory_length; ?>"/>
					</div>
				</div>
			</div>	
			<div class="" id="inventory_breadth_div">
				<div class="col-md-3 "> 
					<div class="form-group">
						<label class="control-label">Breadth
						</label>
						<input class="form-control" name="inventory_breadth" id="inventory_breadth" min="1" type="number" step="any" value="<?php echo $inventories->inventory_breadth; ?>"/>
					</div>
				</div>
			</div>	
			<div class="" id="inventory_height_div">
				<div class="col-md-3"> 
					<div class="form-group">
						<label class="control-label">Height
						</label>
						<input class="form-control" name="inventory_height" id="inventory_height" min="1" type="number" step="any" value="<?php echo $inventories->inventory_height; ?>"/>
					</div>
				</div>
			</div>

			</div>

			<!---new fields--->

			<div class="row">

				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group label-floating">	
						<label class="control-label">Weight Per Unit in Kg</label>
						<input class="form-control" name="weight_per_unit" id="weight_per_unit" type="text" value="<?php echo $inventories->weight_per_unit; ?>">
					</div>
				</div>

			</div>
			
			<div class="row" style="display:none;">

				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group label-floating">	
						<label class="control-label">Dimension Of Furniture (L X W X H)</label>
						<input class="form-control" name="dimension_of_furniture" id="dimension_of_furniture" type="text" value="<?php echo $inventories->dimension_of_furniture; ?>">
					</div>
				</div>

			</div>
			<div class="row">
						<div class="col-md-10 col-md-offset-1">
						<div class="form-group label-floating">	
						<label class="control-label">Shipped In XX Number Of Days</label>
							<select type="text" name="shipswithin" id="shipswithin" class="form-control" >
								
								<option value=""></option>
								<option value="1" <?php echo ($inventories->shipswithin=='1') ? 'selected' : ''; ?>>1</option>
								<option value="2" <?php echo ($inventories->shipswithin=='2') ? 'selected' : ''; ?>>2</option>
								<option value="3" <?php echo ($inventories->shipswithin=='3') ? 'selected' : ''; ?>>3</option>
								<option value="4" <?php echo ($inventories->shipswithin=='4') ? 'selected' : ''; ?>>4</option>
								<option value="5" <?php echo ($inventories->shipswithin=='5') ? 'selected' : ''; ?>>5</option>
								<option value="6" <?php echo ($inventories->shipswithin=='6') ? 'selected' : ''; ?>>6</option>
								<option value="7" <?php echo ($inventories->shipswithin=='7') ? 'selected' : ''; ?>>7</option>
								<option value="10" <?php echo ($inventories->shipswithin=='10') ? 'selected' : ''; ?>>10</option>
								<option value="15" <?php echo ($inventories->shipswithin=='15') ? 'selected' : ''; ?>>15</option>
								<option value="20" <?php echo ($inventories->shipswithin=='20') ? 'selected' : ''; ?>>20</option>
								<option value="30" <?php echo ($inventories->shipswithin=='30') ? 'selected' : ''; ?>>30</option>
								<option value="45" <?php echo ($inventories->shipswithin=='45') ? 'selected' : ''; ?>>45</option>
								<option value="60" <?php echo ($inventories->shipswithin=='60') ? 'selected' : ''; ?>>60</option>

							</select>
						</div>
							</div>
					</div>
					
					
				<div class="row">

				
				
				<div class="col-md-offset-1"> 


					<div class="form-group">

					<div class="col-md-7">
						<!-- <input name="brouchure" id="brouchure" type="file" class="form-control" style="opacity: inherit;
    height: 50px;"> -->

						<!----new --->

						<div class="row files" id="files1">
							<span class="btn btn-default btn-file">
							Upload New Product Brouchure  <input type="file" name="brouchure" />
							</span>
							<br />
							<ul class="fileList" style="width:100%;"></ul>
						</div>


						<!----new --->
					</div>


					<div class="col-md-5">

					
					<?php
					if($inventories->brouchure!=''){
						?>
					
					<div class="text-left">
	<u class="text-info"><a href="<?php echo base_url()."".$inventories->brouchure; ?>" target="_blank"><i class="fa fa-paperclip" aria-hidden="true"></i> Uploaded Product Brouchure </a></u>
					</div>
					<?php } ?>

					</div>
					
					
					</div>
				</div>
			</div>	

			<div class="row">

			<div class="col-md-10 col-md-offset-1">
				<div class="form-group">	
					<div class="col-md-6">
						<label class="" for="brouchure">Frontend Display of Product Under  </label>
					</div>
						<div class="col-md-6">
							
						<?php
						$under_pcat_frontend_arr=[];

						if($inventories->under_pcat_frontend!=''){

						
								$under_pcat_frontend=$inventories->under_pcat_frontend;
								$under_pcat_frontend_arr=($under_pcat_frontend!='') ? explode(',',$under_pcat_frontend) : '';
						}
								foreach($parent_category as $parent){

									?>
									<input type="checkbox" name="under_pcat_frontend[]" id="typ_<?php echo $parent->pcat_id; ?>" value="<?php echo $parent->pcat_id; ?>" <?php echo (in_array($parent->pcat_id,$under_pcat_frontend_arr)) ? 'checked' : '';?>> <label class="control-label" for="typ_<?php echo $parent->pcat_id; ?>"><?php echo $parent->pcat_name; ?> </label>
									<?php
								}
								?>

							
						</div>
					</div>
				</div>
			</div>
			<!---new fields--->

		<!---common--image--->
		<br>
		<hr>
		<div class="row">
			<div class="col-md-5 col-md-offset-3" id="inv_images_div">
			<div class="col-md-12 text-center"><b>Common (300x366)</b></div>
				<div class="col-md-12">
				<div class="picture-container">
					<div class="picture" id="edit_commonPic">
						<img src="<?php echo base_url().$inventories->common_image;?>" class="picture-src" id="edit_wizardPictureCommon" title=""/>
				
						<input type="file" name="common_image" id="edit_wizard-picture-Common"/>
					</div>
				<h6 id="edit_wizardPictureCommon_picname">Common Image</h6>
				</div>
				</div>	
			</div>
		</div>
		<!---common--image--->
		
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
			
			<button name="add_image_inventory" id="add_image_inventory_btn" type="button" class="btn btn-success" disabled onclick="add_image_inventory_fun()">Add</button>
			
			</div>
		</div>
		<div class="row">
			<div class="col-md-11 col-md-offset-1"  id="inv_images_div_1">	
			<div class="col-md-10 col-md-offset-1"><b>FIRST PAIR</b></div>
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic1">
					<img class="picture-src" id="edit_wizardPictureSmall1" title=""/>
		            <input type="file" name="thumbnail1" id="edit_wizard-picture-small1" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->thumbnail;?>" alt="Image Not Found" id="previous_thumbnail_view1" name="previous_thumbnail_view1" width="100"/>
					
					
					
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname1">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic1">
					<img class="picture-src" id="edit_wizardPictureBig1" title=""/>
		            <input type="file" name="image1" id="edit_wizard-picture-big1" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->image;?>" alt="Image Not Found" id="previous_image_view1" name="previous_image_view1" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname1">Big Image (420x512)</h6>
				</div>
				

				
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic1">
					<img class="picture-src" id="edit_wizardPictureLarge1" title=""/>
		            <input type="file" name="largeimage1" id="edit_wizard-picture-large1" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->largeimage;?>" alt="Image Not Found" id="previous_largeimage_view1" name="previous_largeimage_view1" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname1">Large Image (850x1036)</h6>
				</div>
				

				
			</div>
			
			</div>
			<div class="col-md-11 col-md-offset-1"  id="inv_images_div_2">	
			<div class="col-md-10 col-md-offset-1"><b>SECOND PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(2)"></i></b></div>
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic2">
					<img class="picture-src" id="edit_wizardPictureSmall2" title=""/>
		            <input type="file" name="thumbnail2" id="edit_wizard-picture-small2" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->thumbnail2;?>" alt="Image Not Found" id="previous_thumbnail_view2" name="previous_thumbnail_view2" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname2">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic2">
					<img class="picture-src" id="edit_wizardPictureBig2" title=""/>
		            <input type="file" name="image2" id="edit_wizard-picture-big2" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->image2;?>" alt="Image Not Found" id="previous_image_view2" name="previous_image_view2" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname2">Big Image (420x512)</h6>
				</div>
			</div>
			
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic2">
					<img class="picture-src" id="edit_wizardPictureLarge2" title=""/>
		            <input type="file" name="largeimage2" id="edit_wizard-picture-large2" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->largeimage2;?>" alt="Image Not Found" id="previous_largeimage_view2" name="previous_largeimage_view2" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname2">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
		</div>
		<div class="row">
			<div class="col-md-11 col-md-offset-1" id="inv_images_div_3">
<div class="col-md-10 col-md-offset-1"><b>THIRD PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(3)"></i></b></div>			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic3">
					<img class="picture-src" id="edit_wizardPictureSmall3" title=""/>
		            <input type="file" name="thumbnail3" id="edit_wizard-picture-small3" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->thumbnail3;?>" alt="Image Not Found" id="previous_thumbnail_view3" name="previous_thumbnail_view3" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname3">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic3">
					<img class="picture-src" id="edit_wizardPictureBig3" title=""/>
		            <input type="file" name="image3" id="edit_wizard-picture-big3" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->image3;?>" alt="Image Not Found" id="previous_image_view3" name="previous_image_view3" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname3">Big Image (420x512)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic3">
					<img class="picture-src" id="edit_wizardPictureLarge3" title=""/>
		            <input type="file" name="largeimage3" id="edit_wizard-picture-large3" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->largeimage3;?>" alt="Image Not Found" id="previous_largeimage_view3" name="previous_largeimage_view3" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname3">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
			<div class="col-md-11 col-md-offset-1" id="inv_images_div_4">	
			<div class="col-md-10 col-md-offset-1"><b>FOURTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(4)"></i></b></div>
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic4">
					<img class="picture-src" id="edit_wizardPictureSmall4" title=""/>
		            <input type="file" name="thumbnail4" id="edit_wizard-picture-small4" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->thumbnail4;?>" alt="Image Not Found" id="previous_thumbnail_view4" name="previous_thumbnail_view4" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname4">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic4">
					<img class="picture-src" id="edit_wizardPictureBig4" title=""/>
		            <input type="file" name="image4" id="edit_wizard-picture-big4" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->image4;?>" alt="Image Not Found" id="previous_image_view4" name="previous_image_view4" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname4">Big Image (420x512)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic4">
					<img class="picture-src" id="edit_wizardPictureLarge4" title=""/>
		            <input type="file" name="largeimage4" id="edit_wizard-picture-large4" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->largeimage4;?>" alt="Image Not Found" id="previous_largeimage_view4" name="previous_largeimage_view4" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname4">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
		</div>
		<div class="row">
			<div class="col-md-11 col-md-offset-1" id="inv_images_div_5">
<div class="col-md-10 col-md-offset-1"><b>FIFTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(5)"></i></b></div>			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic5">
					<img class="picture-src" id="edit_wizardPictureSmall5" title=""/>
		            <input type="file" name="thumbnail5" id="edit_wizard-picture-small5" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->thumbnail5;?>" alt="Image Not Found" id="previous_thumbnail_view5" name="previous_thumbnail_view5" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname5">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic5">
					<img class="picture-src" id="edit_wizardPictureBig5" title=""/>
		            <input type="file" name="image5" id="edit_wizard-picture-big5" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->image5;?>" alt="Image Not Found" id="previous_image_view5" name="previous_image_view5" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname5">Big Image (420x512)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic5">
					<img class="picture-src" id="edit_wizardPictureLarge5" title=""/>
		            <input type="file" name="largeimage5" id="edit_wizard-picture-large5" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->largeimage5;?>" alt="Image Not Found" id="previous_largeimage_view5" name="previous_largeimage_view5" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname5">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
			
			
			
			
			<div class="col-md-11 col-md-offset-1" id="inv_images_div_6">
<div class="col-md-10 col-md-offset-1"><b>SIXTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(6)"></i></b></div>			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_smallpic6">
					<img class="picture-src" id="edit_wizardPictureSmall6" title=""/>
		            <input type="file" name="thumbnail6" id="edit_wizard-picture-small6" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->thumbnail6;?>" alt="Image Not Found" id="previous_thumbnail_view6" name="previous_thumbnail_view6" width="100"/>
				</div>
			
				<h6 id="edit_wizardPictureSmall_picname6">Small Image (100x122)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_bigpic6">
					<img class="picture-src" id="edit_wizardPictureBig6" title=""/>
		            <input type="file" name="image6" id="edit_wizard-picture-big6" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->image6;?>" alt="Image Not Found" id="previous_image_view6" name="previous_image_view6" width="100"/>
				</div>
				<h6 id="edit_wizardPictureBig_picname6">Big Image (420x512)</h6>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="picture-container">
				<div class="picture" id="edit_largepic6">
					<img class="picture-src" id="edit_wizardPictureLarge6" title=""/>
		            <input type="file" name="largeimage6" id="edit_wizard-picture-large6" onchange="enableDisabledAddInventoryButtonFun()">
					<img src="<?php echo base_url().$inventories->largeimage6;?>" alt="Image Not Found" id="previous_largeimage_view6" name="previous_largeimage_view6" width="100"/>
				</div>
				<h6 id="edit_wizardPictureLarge_picname6">Large Image (850x1036)</h6>
				</div>
			</div>
			
			</div>
			
			
			
			
		</div>
		</div>
	
	</div>
	
	
	
			<!---new fields--->


			<?php
		if($inventories->image==""){ $total_no_of_images=0;}
		else if($inventories->image2==""){ $total_no_of_images=1;}
		else if($inventories->image3==""){ $total_no_of_images=2;}
		else if($inventories->image4==""){ $total_no_of_images=3;}
		else if($inventories->image5==""){ $total_no_of_images=4;}
		else if($inventories->image6==""){ $total_no_of_images=5;}
		else{$total_no_of_images=6;}
	?>	
			<script>

function hide_inv_imagesFun(div_index_id){
			
			document.getElementById("previous_largeimage"+div_index_id).value="";
			document.getElementById("previous_image"+div_index_id).value="";
			document.getElementById("previous_thumbnail"+div_index_id).value="";
			
			document.getElementById("edit_wizard-picture-small"+div_index_id).value="";
			document.getElementById("edit_wizard-picture-big"+div_index_id).value="";
			document.getElementById("edit_wizard-picture-large"+div_index_id).value="";
			
			
			document.getElementById("edit_wizardPictureSmall"+div_index_id).removeAttribute("src");
			document.getElementById("edit_wizardPictureBig"+div_index_id).removeAttribute("src");
			document.getElementById("edit_wizardPictureLarge"+div_index_id).removeAttribute("src");
			
			document.getElementById("edit_wizardPictureSmall_picname"+div_index_id).innerHTML="Small Image (100x122)";
			document.getElementById("edit_wizardPictureBig_picname"+div_index_id).innerHTML="Big Image (420x512)";
			document.getElementById("edit_wizardPictureLarge_picname"+div_index_id).innerHTML="Large Image (850x1036)";
			
			document.getElementById("inv_images_div_"+div_index_id).style.display="none";
			enableDisabledAddInventoryButtonFun();
		}
		function enableDisabledAddInventoryButtonFun(){
			small_image_count=0;
			big_image_count=0;
			large_image_count=0;
			for(i=1;i<=5;i++){
				if(document.getElementById("inv_images_div_"+i).style.display==""){
					if(document.getElementById("edit_wizard-picture-small"+i).value!="" || document.getElementById("previous_thumbnail_view"+i).value!=""){
						small_image_count++;
					}
					if(document.getElementById("edit_wizard-picture-big"+i).value!="" || document.getElementById("previous_image_view"+i).value!=""){
						big_image_count++;
					}
					if(document.getElementById("edit_wizard-picture-large"+i).value!="" || document.getElementById("previous_largeimage_view"+i).value!=""){
						large_image_count++;
					}
				}
			}
			
			if(small_image_count==0 && big_image_count==0 && large_image_count==0){
				document.getElementById("add_image_inventory_btn").disabled=true;
			}
			else{
				if(small_image_count==big_image_count && big_image_count==large_image_count){
					document.getElementById("add_image_inventory_btn").disabled=false;
					//document.getElementById("edit_inventory_finish_btn").disabled=false;
				}
				else{
					document.getElementById("add_image_inventory_btn").disabled=true;
					//document.getElementById("edit_inventory_finish_btn").disabled=true;
					
				}
			}
		}
		function initializeInventoryImageDivFun(){
			for(i=<?php echo ($total_no_of_images+1);?>;i<=6;i++){
				document.getElementById("inv_images_div_"+i).style.display="none";
			}
		}
		function add_image_inventory_fun(){
			for(i=2;i<=6;i++){
				if(document.getElementById("inv_images_div_"+i).style.display=="none"){
					document.getElementById("inv_images_div_"+i).style.display="";
					document.getElementById("add_image_inventory_btn").disabled=true;
					break;
				}
			}
		}
		initializeInventoryImageDivFun();
		enableDisabledAddInventoryButtonFun();
	</script>
	                        
	<div class="wizard-footer" style="display:none;">
		<div class="pull-right">
			
			<input type='button' class='btn btn-finish btn-fill btn-success btn-wd' name='finish' id="add_inventory_finish_btn" value='Finish' onClick="filter_details_form_valid()" />
		</div>

		
		<div class="clearfix"></div>
	</div>
	
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
				<div class="text-center">
					<button class="btn btn-finish btn-fill btn-success btn-wd" type="button" id="submit-data" onClick="filter_details_form_valid()"></i>Submit</button>
					<?php
					$admin_user_type=$this->session->userdata("user_type");
					 ?>
					
				</div>	
				</div>
			</div>		
	
	</div>
</form>

<?php } ?>


</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>
</div>
</body>
</html>
<script>

function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

$(document).ready(function(){

	var textarea = $("#highlight");
	textarea.Editor();
	str='<?php echo $string; ?>';
	data=decodeHtml(str);
	$('#highlight').siblings("div").children("div.Editor-editor").html(data);
	

var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_vendors'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_vendors"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_vendors'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});

});



function get_city_state_details_by_pincodeFun(){

		pin=$("#edit_pincode").val();
		
		if(pin===undefined){
		}
		else{
		$.ajax({
				method : "POST",
				url : "<?php echo base_url();?>Account/get_city_state_details_by_pincode",
				data:{'pin' : pin},
				dataType:"JSON",
				}).success(function mySucces(result) {
					$("#city").val(result.city);
					$("#state").val(result.state);
					$("#country").val("India");
					
					if(parseInt(result.count)==0){
                        
                        $('.delivery_address_pin_error').show();
                        $('#pin_code_check').addClass('has-error');
                    }else{
                        //alert(result.count+"no error");
                        
                        $('.delivery_address_pin_error').hide();
                        $('#pin_code_check').removeClass('has-error');
                    }

				}, function myError(response) {
					
				});
		}	
	 }

function filter_details_form_valid()
{
	
	var form_status = $('<div class="form_status"></div>');		
	var form = $('#create_inventory');	
	var err = 0;
		
		
	if(err==1)
	{
		//$('#validation_error').show();
		return false;
		
	}else{
			var data = $("#highlight").Editor("getText");
			$("#highlight").val(data);
			
			
			var form = $('#create_inventory');	
			var form_create = document.forms.namedItem("create_inventory");
			var ajaxData = new FormData(form_create);
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_vendor_catalog/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false, 		
				
			}).done(function(data)
			  {	
				if(data==true)
				{
					  swal({
						title:"Success", 
						text:"Inventory is successfully Updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				else
				{
					swal(
						'Oops...',
						data,
						'error'
					)	
				}
		return true;
	});
	}
}]);
				
		return true;
	}
}
</script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/richte/editor.js" ></script>
<script>
$(document).ready(function() {
	var textarea = $("#highlight");
	//textarea.Editor();
	
});
$(document).ready(function (){
	$('#price_validity').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		weekStart: 0, 
		format: 'YYYY-MM-DD', 
		shortTime : true
	}).on('change', function(e, date){
		$('#price_validity').bootstrapMaterialDatePicker('setMinDate', date);
	});
	
	$('#discontinued_date').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		weekStart: 0, 
		format: 'YYYY-MM-DD', 
		shortTime : true
	}).on('change', function(e, date){
		$('#discontinued_date').bootstrapMaterialDatePicker('setMinDate', date);
	});

	$('#sku_available_date').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		weekStart: 0, 
		format: 'YYYY-MM-DD', 
		shortTime : true
	}).on('change', function(e, date){
		$('#sku_available_date').bootstrapMaterialDatePicker('setMinDate', date);
	});

});

function calculateTax_priceFun(){
	 
	 tax=$("#admin_margin_percentage").val();
	 msp=$("#max_selling_price").val();
	 offer_price=$("#offer_price").val();
	 
	 if(offer_price!="" && tax!=""){

		taxable_price=(parseInt(offer_price)-Math.round(parseFloat(offer_price)*parseFloat(tax)/100));//.toFixed(2)

		 //$("#tax_percent_price").val(tax_percent_price);
		 $("#taxable_price").val(taxable_price);
		
		 $("#taxable_price_div").removeAttr("class");
		 $("#taxable_price_div").attr("class","form-group label-floating is-empty is-focused");
	 }else{
		//alert('Please Enter Valid Selling Price for End Customer and Vommet Studio Margin Percentage')
	 }
	  /* calculate tax price and taxable vale */
}



$.fn.fileUploader = function (filesToUpload) {
    this.closest(".files").change(function (evt) {

        for (var i = 0; i < evt.target.files.length; i++) {
            filesToUpload.push(evt.target.files[i]);
        };
        var output = [];

        for (var i = 0, f; f = evt.target.files[i]; i++) {
            var removeLink = "<a style=\"color:red !important;padding: 10px;\" class=\"removeFile\" href=\"#\" data-fileid=\"" + i + "\">Remove</a>";

            output.push("<li style='text-align: center;width: 100%;'><strong>", escape(f.name), "</strong> - ",
                f.size, " bytes. &nbsp; &nbsp; ", removeLink, "</li> ");
        }

        $(this).children(".fileList")
            .html(output.join(""));
    });
};

var filesToUpload = [];

$(document).on("click",".removeFile", function(e){
    e.preventDefault();
    var fileName = $(this).parent().children("strong").text();
     // loop through the files array and check if the name of that file matches FileName
    // and get the index of the match
    for(i = 0; i < filesToUpload.length; ++ i){
        if(filesToUpload[i].name == fileName){
            //console.log("match at: " + i);
            // remove the one element at the index where we get a match
            filesToUpload.splice(i, 1);
        }	
	}
    //console.log(filesToUpload);
    // remove the <li> element of the removed file from the page DOM
    $(this).parent().remove();
});

$("#files1").fileUploader(filesToUpload);
function show_input(){

	val=$("#product_status").val();

	$('.div_hide').hide();
	
	if(val!=''){
		if(val=='In Stock'){
			$('#stock_div_count').show();
		}
		if(val=='Made To Order'){
			$('#made_to_order_div').show();
		}
		if(val=='Out Of Stock'){
			$('#out_of_stock_div').show();
		}
		if(val=='Discontinued'){
			$('#discontinuation_div').show();
		}
	}else{
		$('#div_hide').hide();
	}
}

$(document).ready(function(){
	show_input();
})
</script>