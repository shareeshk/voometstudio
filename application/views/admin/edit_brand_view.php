<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script type="text/javascript">

function showDivPrevious(){
	
	var form = document.getElementById('search_for_brand');		
	
	form.action='<?php echo base_url()."admin/Catalogue/brands"; ?>';
	form.submit();

}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row" id="editDiv3">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="edit_brand" id="edit_brand" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_parent_category: {
					required: true,
				},
				edit_categories: {
					required: true,
				},
				edit_subcategories: {
					required: true,
				},
				edit_brand_name: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Brand</h3>
		</div>
		</div>
		<div class="tab-content">
		<input type="hidden" name="edit_brand_id" id="edit_brand_id" value="<?php echo $get_brand_data["brand_id"];?>">
		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Parent Category-</label>
					<select name="edit_parent_category" id="edit_parent_category" class="form-control" disabled>
						<?php foreach ($parent_catagories as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>" <?php echo ($parent_category_value->pcat_id==$pcat_id)? "selected":''; ?>><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<option value="0"<?php echo ($pcat_id==0)? "selected":''; ?>>--None--</option>
							
					</select>
				</div>
				</div>
			</div>
		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Category-</label>
					<select name="edit_categories" id="edit_categories" class="form-control " disabled>
						<?php foreach ($categories as $category_value) {  ?>
						<option value="<?php echo $category_value->cat_id; ?>" <?php echo ($category_value->cat_id==$cat_id)? "selected":''; ?>><?php echo $category_value->cat_name; ?></option>
						<?php } ?>
		
					</select>
				</div>
				</div>
			</div>
					
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Subcategory-</label>
					<select name="edit_subcategories" id="edit_subcategories" class="form-control" disabled>
						<?php foreach ($subcategories as $subcategory_value) {  ?>
						<option value="<?php echo $subcategory_value->subcat_id; ?>" <?php echo ($subcategory_value->subcat_id==$subcat_id)? "selected":''; ?>><?php echo $subcategory_value->subcat_name; ?></option>
						<?php } ?>
		
					</select>
				</div>
				</div>
			</div>		
					
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Enter Brand Name</label>
					<input id="edit_brand_name" name="edit_brand_name" type="text" class="form-control" value="<?php echo $get_brand_data["brand_name"];?>"/>
					<input id="edit_brand_name_default" name="edit_brand_name_default" type="hidden" class="form-control" value="<?php echo $get_brand_data["brand_name"];?>"/>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10"> 
					<div class="form-group">
					<div class="col-md-5 col-md-offset-1">Show Brand</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="edit_view" id="edit_view1" value="1" type="radio" <?php if($get_brand_data["view"]=="1"){echo "checked";}?>>View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="edit_view" id="edit_view2" value="0" type="radio" <?php if($get_brand_data["view"]=="0"){echo "checked";}?>>Hide</label>
					</div>
				
					</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-3">
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit-data" disabled="">Submit</button>
		
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
					</div>
				</div>
			</div>				
													
			
		</div>		
	</form>
	<form name="search_for_brand" id="search_for_brand" method="post">
		<input value="<?php echo $get_brand_data["pcat_id"]; ?>" type="hidden" id="pcat_id" name="pcat_id"/>
		<input value="<?php echo $get_brand_data["cat_id"]; ?>" type="hidden" id="cat_id" name="cat_id"/>
		<input value="<?php echo $get_brand_data["subcat_id"]; ?>" type="hidden" id="subcat_id" name="subcat_id"/>
		<input value="view" type="hidden" name="create_editview">
	</form>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>

<script type="text/javascript">

function showAvailableCategoriesEdit(obj)
{
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					alert(data);
					if(data!=0){
						$("#edit_categories").html(data);
					}
					else{
						$("#edit_categories").html('<option value=""></option>')
						$("#brands").html('<option value=""></option>')

					}
				}
			});
	}
}

$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_brand'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_brand"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_brand'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (orig[id].value == $(this).val());

        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});

});
function form_validation_edit()
{
	
	var brand_name = $('input[name="edit_brand_name"]').val().trim();
	var parent_category = $('select[name="edit_parent_category"]').val().trim();
	var categories = $('select[name="edit_categories"]').val().trim();
	var subcategories = $('select[name="edit_subcategories"]').val().trim();
	var view = document.querySelector('input[name="edit_view"]:checked').value;

	   var err = 0;
	   if((brand_name=='') || (parent_category=='') || (categories=='') || (subcategories=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
				document.getElementById("edit_parent_category").disabled="";
				document.getElementById("edit_categories").disabled="";
				document.getElementById("edit_subcategories").disabled="";
		var form_status = $('<div class="form_status"></div>');		
		var form = $('#edit_brand');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();		
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_brand/"?>',
				type: 'POST',
				data: $('#edit_brand').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {
				form_status.html('') 
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					edit_brand_name_default=document.getElementById("edit_brand_name_default").value;
					document.getElementById("edit_brand_name").value=edit_brand_name_default;
					document.getElementById("edit_brand_name").focus();
					document.getElementById("edit_parent_category").disabled=true;
					document.getElementById("edit_categories").disabled=true;
					document.getElementById("edit_subcategories").disabled=true;
				});
				  } 
				if(data==1)
				{

					  swal({
						title:"Success", 
						text:"Brand is successfully updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				if(data==0)
				{
					
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
			});
}
}]);			
			}
	
	return false;
}

</script>
</div>
</body>

</html>