<html lang="en">
<head>	
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />


<style type="text/css">
.cursor-pointer {
    cursor: pointer !important;
}
.small_color_box{
	width: 30px;
	height: 30px;
	border: 1px solid #ccc;
	margin-top: 1px;
}

.loading {
	text-align: center; 
	margin:0;product
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
#wizardPictureSmall_picname{
	word-wrap: break-word;
}
#wizardPictureBig_picname{
	word-wrap: break-word;
}
#wizardPictureSmall_picname2{
	word-wrap: break-word;
}
#wizardPictureBig_picname2{
	word-wrap: break-word;
}
#wizardPictureSmall_picname3{
	word-wrap: break-word;
}
#wizardPictureBig_picname3{
	word-wrap: break-word;
}
#wizardPictureSmall_picname4{
	word-wrap: break-word;
}
#wizardPictureBig_picname4{
	word-wrap: break-word;
}
#wizardPictureSmall_picname5{
	word-wrap: break-word;
}
#wizardPictureBig_picname5{
	word-wrap: break-word;
}
#wizardPictureSmall_picname6{
	word-wrap: break-word;
}
#wizardPictureBig_picname6{
	word-wrap: break-word;
}
textarea.form-control {
    height:36px;
}
.CamListDiv {
    height: 450px;
    float: left;
    overflow: scroll;
    overflow-x:hidden;
}
.well{
	width:100%;
}
</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 

<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
<script type="text/javascript">
var table;

$(document).ready(function (){
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    var rows_selected = [];
    table = $('#table_inventory').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Catalogue/vendor_inventory_processing",
			data: function (d) { d.vendor_id = $('#vendor_id').val();d.vendor_city = $('#vendor_city').val();d.from_date = $('#from_date').val(); d.to_date = $('#to_date').val(); },
            type: "post",
            error: function(){
              $("#table_inventory_processing").css("display","none");
            },
			"dataSrc": function ( json ) {				
				document.getElementById("inventory_count").innerHTML=json.recordsFiltered;	
						
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });  
	   
   $( "#from_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
	table.draw();
	});

	$( "#to_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
	table.draw();
	});

	$('#reset_form_button').on('click',function(){
		table.draw();
	});

	$('#submit_form_button').on('click',function(){
		table.draw();
	});	
	
});	


function drawtable(obj=""){
	table.draw();
}

function btn_hover(){
	$('.notification_btn').hover(
					function() {
						var $this = $(this); // caching $(this)
						$this.data('defaultText', $this.text());
						$this.text("I'm replaced!");
						
					},
					function() {
						var $this = $(this); // caching $(this)
						$this.text($this.data('defaultText'));
					}
				);	
}
function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_delete_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
	swal({
			title: 'Are you sure?',
			text: "Archived Inventory",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Archive it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
		$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/update_inventory_selected",
		type:"post",
		data:"selected_list="+selected_list+"&active=0",
		
		success:function(data){
			if(data==true){
				
				swal({
						title:"Archived!", 
						text:"Given "+table+"(s) has been Moved to Trash successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}
			else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
		});
	})
		    },
	allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
function multilpe_delete_when_no_info(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
		swal({
			title: 'Are you sure?',
			text: "Delete Subcategory",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/delete_inventory_when_no_info",
		type:"post",
		data:"selected_list="+selected_list,
		success:function(data){
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Given "+table+"(s) has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else if(data==false){
				swal("Error", "Error in deleting this file", "error");
			}else{
				swal("Error", "Oops ..! You can't delete it.", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}

function draw_table(){

	table.draw();
	btn_hover()
	
}

</script>
<script type="text/javascript">
	function display_color(color_value){
	var attribute_1_value=document.getElementById("attribute_1_value").value;
		//alert(color_value);
		colors_arr=color_value.split(":");
		color_code=colors_arr[1];
		color_name=colors_arr[0];
		if(attribute_1_value!=""){
		$('#small_color_box').css({"background-color": color_code});
		$('#small_color_box').attr('title',color_name);
		}if(attribute_1_value==""){
			$('#small_color_box').css({"background-color": ""});
		}
	}
</script>
<script type="text/javascript">
	function filter_display_color(color_value){
		//alert(color_value);
		colors_arr=color_value.split(":");
		color_code=colors_arr[1];
		color_name=colors_arr[0];
		$('#filter_small_color_box').css({"background-color": color_code});
		$('#filter_small_color_box').attr('title',color_name);
	}
</script>	
<script>
		function spec_fun(obj,specification_id){
			//document.getElementsByName("specification_textarea_value["+specification_id+"]")[0].setAttribute("style", "border: 1px solid #ccc;");
			if(obj.value=="Others"){
				document.getElementsByClassName("specification_textarea_value["+specification_id+"]")[0].style.display="block";
			}
			else{
				document.getElementsByClassName("specification_textarea_value["+specification_id+"]")[0].style.display="none";
			}
		}
</script>
<script type="text/javascript">
$(document).ready(function (){
$('#back').on('click',function(){	
		var form = document.getElementById('back_to_inventory');
		form.action='<?php echo base_url(); ?>admin/Catalogue/inventory_filter';
		form.submit();
	});
});		
</script>
<script>

function changeViewPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/inventory_filter';
}
</script>

</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">


<div class="row">
<div class="wizard-header text-center">
	<h5>
		 Number of SKUs <span class="badge"><div id="inventory_count"></div></span> 
	</h5>
	
</div>
<table id="table_inventory" class="table table-bordered table-striped" cellspacing="0" width="100%" style="font-size:14px;">
<thead>
	<tr style="display:none;">
		<th colspan="5">
            <div class="float-right">
               

            </div>
                    <?php 
                                        
                    $admin_user_type=$this->session->userdata("user_type");
                    $admin_module_type=$this->session->userdata("module_type");
                    if($admin_user_type="Su Master Country" && $admin_module_type=='catalog' ){
                        /*only edit - no delete*/
                        ?>
                    
                    <?php 
                    
                    }else{
                        ?>
                        <button id="multiple_delete_button1" class="btn btn-warning btn-xs" onclick="multilpe_delete_fun('inventory')">Archive</button>
			<button id="multiple_delete_button2" class="btn btn-danger btn-xs" onclick="multilpe_delete_when_no_info('inventory')">Delete</button>
                    <?php                    
                    }
                    ?>
            
            <!---import uantity ---->

            <!-- Import link -->
            <div class="col-md-12 head">

            </div>


		</th>

		<tr><th colspan="6">
		<form id="date_range" class="form-inline">

		<?php if($admin_user_type!='vendor'){ ?>
			<div class="col-md-3">
			<select class="form-control" name="vendor_id" id="vendor_id" onchange="draw_table()">

					<option value="">-Select Vendor</option>
				<?php
				
				if(!empty($vendors)){
					foreach($vendors as $ven){
						?>
						<option value="<?php echo $ven->vendor_id; ?>"><?php echo $ven->name; ?> - <?php echo $ven->email; ?>  </option>
						<?php 
					}
				}
				?>
			</select>
		</div>

		<div class="col-md-3">
			<select class="form-control" name="vendor_city" id="vendor_city" onchange="draw_table()">

					<option value="">-Select City-</option>
				<?php
				
				if(!empty($vendors_city_based)){
					foreach($vendors_city_based as $ven_c){
						?>
						<option value="<?php echo $ven_c->city; ?>"><?php echo $ven_c->city; ?> - <?php echo $ven_c->pincode;?> </option>
						<?php 
					}
				}
				?>
			</select>
		</div>

		<?php }else{
			?>
			<input type="hidden" name="vendor_id" id="vendor_id" value="<?php echo $this->session->userdata("user_id"); ?>">
			<input type="hidden" name="vendor_city" id="vendor_city" value="">

			<?php 
		} ?>

			<div id="datetimediv" class="col-md-6 text-right">
				
				<input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date">
				<input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date">
				<button type="button" class="btn btn-success btn-xs" id="submit_form_button">Submit</button>
				<button type="reset" class="btn btn-warning btn-xs" id="reset_form_button">Reset</button>
				
	
			</div>
		</th>

		</form>
		</tr>
	</tr>
	<tr>
		<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
		<th>Image</th>
		
		<?php if($admin_user_type!='vendor'){
			?>
			<th>
			Vendor Details
			</th>
			<?php
		} ?>
		
		<th>SKU Details</th>
		<th>Details</th>
		<th>Action</th>		
	</tr>
</thead>

</table>
</div>



	<script>
	
	var typingTimer;                //timer identifier
	var doneTypingInterval = 5000;  //time in ms, 5 second for example
	var $input = $('#tax');

	//on keyup, start the countdown
	$input.on('keyup', function () {
	  clearTimeout(typingTimer);
	  val=$('#selling_price').val();
	  typingTimer = setTimeout(doneTyping(val), doneTypingInterval);
	});

	//on keydown, clear the countdown 
	$input.on('keydown', function () {
	  clearTimeout(typingTimer);
	});

	//user is "finished typing," do something
	function doneTyping (val) {
	  if(val!=''){
		  assign_filter_values(val,'price')
	  }
	}
	function assign_filter_values_stock(){
		elem="#availability";
		stock=parseInt($("#stock").val().trim());
		cutoff_stock=parseInt($("#cutoff_stock").val().trim());
		if(stock!="" && cutoff_stock!=""){
			if(stock>cutoff_stock){
				stock_availability="yes";
			}
			else{
				stock_availability="no";
			}
			//alert(stock_availability);
			if(stock_availability=="yes"){
				$("#availability > option").each(function(){
					if($(this).val()=="0"){
						//$(this).attr({"selected":true});
						$("#availability").val($(this).val());
						$("#availability").attr({"disabled":true});
						$("#availability").parent('div').removeAttr("class");
						$("#availability").parent('div').addClass("label-floating form-group");
						$("#filter_select").append("<input type='hidden' name='"+$(elem).attr("name")+"' class='dynamic_input' value='0'>");
						//$("input[name='"+$(elem).attr("name")+"'].dynamic_input").val(0);
						
						
					}
				});
			}
			if(stock_availability=="no"){
				$("#availability > option").each(function(){
					if($(this).text()=="include out of stock"){
						//$(this).attr({"selected":true});
						$("#availability").val($(this).val());
						$("#availability").attr({"disabled":true});
						$("#availability").parent('div').removeAttr("class");
						$("#availability").parent('div').addClass("label-floating form-group");
						$("#filter_select").append("<input type='hidden' name='"+$(elem).attr("name")+"' class='dynamic_input' value='"+$(this).val()+"'>");
						//$("input[name='"+$(elem).attr("name")+"'].dynamic_input").val($(this).val());
						
								
					}
				});
			}
			
		}
		
		
	}
	function assign_filter_values(data,name,color=''){

		$(".filterbox").each(function(index, elem) {
		   filterbox_name = $(elem).attr("filterbox_name");
		   id=$(elem).attr('id');
		  
		  //id is elem
		  // alert(filterbox_name);
		   if(name==filterbox_name){
			  
				$("#"+id+" > option").each(function() {
					
					fil_text_org=this.value;
					//alert(fil_text_org);
					fil_text=this.text.toLowerCase();
					//alert(fil_text);
					data=data.toLowerCase();
					
					if(color=="color"){
						colors_arr=data.split(":");
						code=colors_arr[1];
						name=colors_arr[0];
						data=name;
					}
					
					//alert(fil_text + ' ' + data);
					
					if(name=="price"){
						//alert(fil_text);
						if(fil_text!=''){
							fil_text_arr=fil_text.split("-");
							lprice=parseInt(fil_text_arr[0].trim());
							hprice=parseInt(fil_text_arr[1].trim());
	
							if(data>=lprice && data<=hprice){	
								//$(elem).find("option[value='"+fil_text_org+"']").attr('selected',"true");
								$(elem).val(fil_text_org);
								$(elem).attr({"disabled":true});
								$(elem).parent('div').removeAttr("class");
								$(elem).parent('div').addClass("label-floating form-group");
								
								$("#filter_select").append("<input type='hidden' name='"+$(elem).attr("name")+"' class='dynamic_input' value='"+fil_text_org+"'>");
								//$("input[name='"+$(elem).attr("name")+"'].dynamic_input").val(fil_text_org);
								
								
							
								//$(elem).parent('div').addClass('is-focused');
								
							}
						}
					}
					if(name!="price"){
						if(fil_text==data && fil_text_org!=''){
							//alert(fil_text);
							//$(elem).find("option[value='"+fil_text_org+"']").attr('selected',"true");
							$(elem).val(fil_text_org);
							$(elem).attr({"disabled":true});
							$(elem).parent('div').removeAttr("class");
							$(elem).parent('div').addClass("label-floating form-group");
							
							$("#filter_select").append("<input type='hidden' name='"+$(elem).attr("name")+"' class='dynamic_input' value='"+fil_text_org+"'>");
							//$("input[name='"+$(elem).attr("name")+"'].dynamic_input").val(fil_text_org);
								
							

							
							//$(elem).parent('div').addClass('is-focused');
							
						}
						
					}
					
				});
		   }
		});

	}
	</script>
	
	
	

<div class="modal" id="status_modal">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Update Inventory Status</h4>
           </div>
           <div  class="modal-body" id="model_content">
				<form method="post" id="request_form" name="request_form">
		  			<input type="hidden" name="app_id" id="app_id" value="">
		  			<input type="hidden" name="inventory_id" id="inventory_id" value="">
		  			

					<div class="row">

					<div class="col-md-11 col-md-offset-1"> 
							<div class="form-group">
								<label class="control-label">Status
								</label>
								<select name="status" id="status" type="text" class="form-control" required onchange="show_rejected_comments()"/>
								<option value=""> - Select status - </option>
								<option value="1">Approved</option>
								<option value="2">Onhold</option>
								<option value="3">Rejected</option>
								</select>
							</div>
						</div>
  
						<div class="col-md-11 col-md-offset-1" id="show_rejected_comments_div" style="display:none;" > 	
							<div class="form-group">
							<label class="control-label">Rejections Comments if any 
							</label>
							<textarea name="comments" id="comments" class="form-control"></textarea>
							</div>
						</div>

						<div class="col-md-11 col-md-offset-1"> 	
							<div class="form-group text-center">
							
							<button class="btn btn-info btn-sm" type="button" onclick="change_status()" id="submit-data"></i>Submit</button>
							</div>
						</div>
					</div>



				</form>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
</div>

<div class="modal" id="cat_modal">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Tag to Vommet Inventory - <b>SKU ID :<span id="sku_id"></span></b></h4>
           </div>
           <div  class="modal-body" id="model_content">
				<form method="post" id="tag_request_form" name="tag_request_form">

		  			<input type="hidden" name="vendor_id" id="cat_vendor_id" value="">
		  			<input type="hidden" name="vendor_catalog_id" id="vendor_catalog_id" value="">
		  			
					
			<div class="row">
				<div class="col-md-10">
					<div class="form-group label-floating is-focused">
					<label class="control-label">-Select Parent Category-</label>
					<select name="parent_category" id="parent_category" class="form-control" onchange="showAvailableCategories(this)" required>
						<option value=""></option>
						<?php foreach ($parent_category as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>" ><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						
						
					</select>
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10">
					<div class="form-group label-floating">
					<label class="control-label">-Select Category-</label>
					<select name="categories" id="categories" class="form-control" onchange="showAvailableSubCategories(this)" required>
						<option value=""></option>
					</select>
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10">		
					<div class="form-group label-floating">
					<label class="control-label">-Select Sub Category-</label>
					<select name="subcategories" id="subcategories" class="form-control" onchange="showAvailableBrands(this)" required>
						<option value=""></option>
					</select>
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10">		
					<div class="form-group label-floating">
					<label class="control-label">-Select Brands-</label>
					<select name="brands" id="brands" class="form-control" onchange="showAvailableProducts(this)" required>
						<option value=""></option>
					</select>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10">		
					<div class="form-group label-floating">
					<label class="control-label">-Select Products-</label>
					<select name="products" id="products" class="form-control" required>
						<option value=""></option>
					</select>
					</div>
				</div>
			</div>	

					<div class="row">

						<div class="col-md-11 col-md-offset-1"> 	
							<div class="form-group text-center">
							
							<button class="btn btn-info btn-sm" type="button" onclick="tag_vendor_inventory()" id="submit-data"></i>Submit</button>
							</div>
						</div>
					</div>



				</form>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
</div>

<script  type="text/javascript" src="<?php echo base_url();?>assets/richte/editor.js" ></script>
<script>
$(document).ready(function() {
	var textarea = $("#highlight");
	textarea.Editor();
	
	var textarea1 = $("#highlight_faq");
	textarea1.Editor();
	
	var textarea2 = $("#highlight_whatsinpack");
	textarea2.Editor();
	
	var textarea3 = $("#highlight_aboutthebrand");
	textarea3.Editor();
 });
 
 /*function toggle_highlight(){
	 $('#highlight_div').toggle();
 }*/
 function enterCurrentTabFun(){
	$(".tab-content .tab-pane.active").each(function(){
		if($(this).attr("id")=="step-21"){
			vendor_id=$("#vendors").val();
			if($("#logistics_parcel_category").val()!=null){
				logistics_parcel_category_in=$("#logistics_parcel_category").val().join("-");
				
				$.ajax({
					url:"<?php echo base_url()?>admin/Catalogue/check_selection_in_all_parcel_categories",
					type:"post",
					data:"vendor_id="+vendor_id+"&logistics_parcel_category_in="+logistics_parcel_category_in,
					success:function(data){
						if(data=="no"){
							swal({
								title:"Info", 
								text:"Please select atleast one parcel category from logisitcs", 
								type: "info"
						}).then(function () {
							
						});
						$("#previous_button").click();
						}
						else{
							
						}
					}
				});
			}
	
		}		
	});
 }
 
 function calculateSelling_priceDiscountFun(){
 
        selling_price=$("#selling_price").val();
        max_selling_price=$("#max_selling_price").val();
        
        if(selling_price=="" || max_selling_price==""){
            $("#selling_discount").val("");
	}
        if(selling_price!="" && max_selling_price!=""){
            if(parseFloat(max_selling_price)>=parseFloat(selling_price)){
                wsp_dis=Math.round(((parseFloat(max_selling_price)-parseFloat(selling_price))/parseFloat(max_selling_price))*100).toFixed(2);
                $("#selling_discount").val(wsp_dis);
                $("#selling_discount_div").removeAttr("class");
		$("#selling_discount_div").attr("class","form-group label-floating is-focused");
                
            }else{
                swal({
                        title:"Error", 
                        text:"MRP should be greater or equal to WSP", 
                        type: "error",
                        allowOutsideClick: false
                }).then(function () {
                        $("#selling_discount").val(""); 
                });
               
            }
            //alert(wsp_dis);
	}
 }
  function calculateTax_priceFun(){
	 
        tax=$("#tax").val();
        msp=$("#max_selling_price").val();
        selling_price=$("#selling_price").val();
        
        if(selling_price!="" && tax!=""){
            
            //taxable_price=(parseFloat(msp)-parseFloat(tax_percent_price));
            taxable_price=(parseFloat(selling_price)/(1+parseFloat(tax)/100)).toFixed(2);
            tax_percent_price=(parseFloat(selling_price)-parseFloat(taxable_price)).toFixed(2);
            //alert(taxable_price);
            $("#tax_percent_price").val(tax_percent_price);
            $("#taxable_price").val(taxable_price);
            
            $("#tax_percent_price_div").removeAttr("class");
            $("#tax_percent_price_div").attr("class","form-group label-floating is-empty is-focused");
            $("#taxable_price_div").removeAttr("class");
            $("#taxable_price_div").attr("class","form-group label-floating is-empty is-focused");
        }
         /* calculate tax price and taxable vale */
 }

 /* Export inventory */

function export_inventory(table){

    $.ajax({
        url:"<?php echo base_url()?>admin/Catalogue/export_inventory",
        type:"post",
        data:"table="+table+"&active=0",
        success:function(data){
            if(data==true){

            } else{
                swal("Error", "Error in exporting inventories", "error");
            }
        }
    });
}
 /* Export inventory */
 /* Import inventory */
function formToggle(ID){
    var element = document.getElementById(ID);
    if(element.style.display === "none"){
        element.style.display = "block";
    }else{
        element.style.display = "none";
    }
}


 </script>
 
 
 <script>
	function calculateTaxPercentFun(){
		var CGST_value=$("#CGST").val().trim();
		var IGST_value=$("#IGST").val().trim();
		var SGST_value=$("#SGST").val().trim();
		
		if(CGST_value==""){CGST_value=0;}
		if(IGST_value==""){IGST_value=0;}
		if(SGST_value==""){SGST_value=0;}
		
		$("#tax").val(parseFloat(CGST_value)+parseFloat(IGST_value)+parseFloat(SGST_value));
		calculateTax_priceFun();
	}

	function change_status(app_id,inventory_id,status){
    
    if(app_id!='' && status!=''){
        
        swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
        $.ajax({
            url:"<?php echo base_url()?>admin/Catalogue/vendor_inventory_change_status",
            type:"POST",
            data:"vendor_id="+app_id+"&inventory_id="+inventory_id+"&status="+status,
            dataType: "JSON",                
            asyc:false,
            success:function(data){
                if (parseInt(data.flag) == 1) {
                    swal(
                        data.message,
                        '',
                        'info'
                    ) 
                    table.draw();
                } 
            }
        });
        
         }
}]);
    }
    
}

$(document).ready(function (){
	$('#price_validity').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		weekStart: 0, 
		format: 'YYYY-MM-DD', 
		shortTime : true
	}).on('change', function(e, date){
		$('#price_validity').bootstrapMaterialDatePicker('setMinDate', date);
	});
});

function view_cat_modal(app_id,vendor_catalog_id,pcat_id,sku_id){
	$("#cat_vendor_id").val(app_id);
	$("#vendor_catalog_id").val(vendor_catalog_id);
	$("#parent_category").val(pcat_id);
	showAvailableCategories(pcat_id);
	$("#sku_id").html(sku_id);
	$("#cat_modal").modal("show");
}

function view_status_modal(app_id,inventory_id,status){
	$("#app_id").val(app_id);
	$("#inventory_id").val(inventory_id);
	$("#status").val(status);
	$("#status_modal").modal("show");
}

function delete_notification(id){

	if(id!=''){
        
        swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
        $.ajax({
            url:"<?php echo base_url()?>admin/Catalogue/delete_catalog_notification",
            type:"POST",
            data:"id="+id,
            dataType: "JSON",                
            asyc:false,
            success:function(data){
                if (parseInt(data.flag) == 1) {
                    swal(
                        data.message,
                        '',
                        'info'
                    ) 
                    drawtable();
					//$("#status_modal").modal("hide");
					get_total_count_of_notification()

                } 
            }
        });
        
         }
}]);
    }

	
   
}
function change_status(){

	app_id=$("#app_id").val();
	inventory_id=$("#inventory_id").val();
	status=$("#status").val();
	comments=$("#comments").val();

    if(app_id!='' && status!=''){
        
        swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
        $.ajax({
            url:"<?php echo base_url()?>admin/Catalogue/vendor_inventory_change_status",
            type:"POST",
            data:"vendor_id="+app_id+"&inventory_id="+inventory_id+"&status="+status+"&comments="+comments,
            dataType: "JSON",                
            asyc:false,
            success:function(data){
                if (parseInt(data.flag) == 1) {
                    swal(
                        data.message,
                        '',
                        'info'
                    ) 
                    drawtable();
					$("#status_modal").modal("hide");

                } 
            }
        });
        
         }
}]);
    }
    
}

function show_rejected_comments(){
	status=$("#status").val();
	if(status=='3'){
		$('#show_rejected_comments_div').show();
	}else{
		$('#show_rejected_comments_div').hide();
	}
}
function editInventoryFun(vendor_catalog_id){
	location.href="<?php echo base_url()?>admin/Catalogue/edit_vendor_catalog_inventory/"+vendor_catalog_id;
}

function showAvailableProducts(obj){
	
	if(obj.value!="" && obj.value!="None"){
		brand_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_products",
				type:"post",
				data:"brand_id="+brand_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#products").html(data);
					}
					else{
						$("#products").html('<option value=""></option>');
					}
				}
			});
	}
	
}

function showAvailableBrands(obj){
	
	if(obj.value!="" && obj.value!="None"){
		subcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_brands",
				type:"post",
				data:"subcat_id="+subcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#brands").html(data);
					}
					else{
						$("#brands").html('<option value=""></option>');
					}
				}
			});
	}
	
}
function showAvailableSubCategories(obj){
	
	if(obj.value!="" && obj.value!="None"){
		cat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#subcategories").html(data);
					}
					else{
						$("#subcategories").html('<option value=""></option>')
						$("#brands").html('<option value=""></option>')

					}
				}
			});
	}
	
}

function showAvailableCategories(pcat_id){
	//obj.value!="" && obj.value!="None"
	if(1){
		pcat_id;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#categories").html(data);
					}
					else{
						$("#categories").html('<option value=""></option>')
						$("#brands").html('<option value=""></option>')

					}
				}
			});
	}
	
}

function tag_vendor_inventory(){


	if(1){
        
        swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
        $.ajax({
            url:"<?php echo base_url()?>admin/Catalogue/tag_vendor_inventory",
            type:"POST",
            data:$('#tag_request_form').serialize(),
            dataType: "JSON",                
            asyc:false,
            success:function(data){
                if (parseInt(data.flag) == 1) {
                    
					swal({
						title:"", 
						html:data.message, 
						type: "info",
						allowOutsideClick: false
				}).then(function () {
					inv_id=parseInt(data.inv_id);
					p_id=parseInt(data.p_id);

					if(inv_id!='' && p_id!=''){
						window.location.href = '<?php echo base_url(); ?>admin/Catalogue/edit_inventory_form/'+inv_id+'/'+p_id;
					}else{
						alert('Error');
					}
				});
				
				

					
                    //table.draw();
                } 
            }
        });
        
         }
}]);
    }
    
}

function get_total_count_of_notification(){
	$.ajax({
			url:"<?php echo base_url()?>admin/Admin_manage/get_total_count_of_notification",
			type:"POST",
			dataType:"JSON",
			data:"1=2",
			success:function(data){				
				new_vendor_sku_notification=data.new_vendor_sku_notification;
				
				if(new_vendor_sku_notification!=""){

					$("#catalog_menu").removeClass("fa fa-bell-o");
					$("#catalog_menu").addClass("fa fa-bell");
					
					$("#vendor_catalog_menu").removeClass("fa fa-bell-o");
					$("#vendor_catalog_menu").addClass("fa fa-bell");
					//alert()

				}else{
					$("#catalog_menu").removeClass("fa fa-bell-o");
					$("#catalog_menu").removeClass("fa fa-bell");

					$("#vendor_catalog_menu").removeClass("fa fa-bell-o");
					$("#vendor_catalog_menu").removeClass("fa fa-bell");
					
				}
				
			}
	});
}

</script>


</body>
</html>
