<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Cancelled Orders</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript">
var table;
$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   table = $('#cancelled_orders_table').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Orders/cancelled_orders_inactive_processing", // json datasource
			data: function (d) { d.status_of_order = $('#status_of_order').val(); },
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#cancelled_orders_table_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("cancelled_count").innerHTML=json.recordsFiltered;
				return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'5%',
         'className': 'dt-body-center'
      },{
         'targets': 1,
         'width':'5%',
      },{
         'targets': 2,
         'width':'35%',
      },{
         'targets': 3,
         'width':'25%',
      },{
         'targets': 4,
         'width':'15%',
      },{
         'targets': 5,
         'width':'15%',
      }],
      'order': [0, 'desc']
   });
});	

function draw_table(){
	table.draw();
}

function open_cancel_refund_issue_fun(order_item_id){
	location.href="<?php echo base_url()?>admin/Orders/cancels_issue_refund/"+order_item_id;
}

</script>
  
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="page-header"><h4 class="text-center">Cancelled Orders<span class="badge" id="cancelled_count"></span></h4></div>

	<table id="cancelled_orders_table" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
		<tr>
				<th colspan="6">
				<div class="row">
				<div class="col-md-3 col-md-offset-4">
				<select class="form-control" name="status_of_order" id="status_of_order" onchange="draw_table()">
					<option value="all" selected>All</option>	
					<option value="inactive">No Refund</option>
					<option value="success">Refund Success</option>
					<!--<option value="failure">Refund failure</option>			-->
				</select>
				</div>
				</div>
				</th>
		</tr>
		<tr>
				<th class="text-primary small bold">Image</th>
				<th class="text-primary small bold">Order Summary</th>
				<th class="text-primary small bold">Cancellation Details</th>
				<th class="text-primary small bold">Quantity and Price</th>
				<th class="text-primary small bold">Buyer Details</th>
				<th class="text-primary small bold">Refund Status</th>
		</tr>
		</thead>
	</table>
</div>

  <div class="modal" id="order_summary">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Order Summary</h4>
           </div>
           <div  class="modal-body" id="model_content">
		   <div class="container">
				
			</div>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close"class="btn">Close</a>
        </div>
     </div>
    </div>
  </div>
  
  <script type="text/javascript">
  $("#order_summary").modal("hide");
  function view_order_summary_(order_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Orders/get_data_from_invoice_offers",
		type:"POST",
		data:"order_id="+order_id+"&cancelled_order=1",
		success:function(data){			
			$("#description").val("");	
			$("#model_content").html(data);
			$("#order_summary").modal("show");	
		}
	})
	
}
  </script>
  </div>
</body> 
</html>  