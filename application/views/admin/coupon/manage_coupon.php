<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Manage Coupon</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 

<style type="text/css">
.padding_right{
	float:left;
	margin-right:15px;
	
}
.accordion-toggle:hover {
  text-decoration: none;
}
.more-less {
        color: #212121;
    }
	.free_items_style{
		padding: 10px;
		border-bottom: 1px solid #ccc;
		margin-bottom: 13px;
	}
.form-inline{
	margin-bottom:0;
}
.popover{
	min-width:80rem;
}
.fa-question-circle{
	color:#e46c0a;
}
</style>

<script>
$(document).ready(function(){
	
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
});
</script>

<script type="text/javascript">

var table;

function fetchData(){
	var fetch_data = '';  
               orderid_orderitemid=$(this).attr("id");
			   orderid_to_get_history=orderid_orderitemid.split("_")[1]; 
			    orderitemid_to_get_history=orderid_orderitemid.split("_")[2]; 
                $.ajax({  
                    url:"<?php echo base_url()?>admin/Orders/get_order_summary_mail_data_from_history_table",
					type:"POST",
					data:"order_id="+orderid_to_get_history+"&order_item_id="+orderitemid_to_get_history, 
                     async:false,  
                     success:function(data){  
                          fetch_data = data;  
                     }  
                });  
                return fetch_data;  
}


$(document).ready(function (){	
	
	$('#exact_delivery_date').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		weekStart: 0, 
		format: 'YYYY-MM-DD', 
		shortTime : true
	}).on('change', function(e, date){
		$('#exact_delivery_date').bootstrapMaterialDatePicker('setMinDate', date);
	});
	
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("input[type=text]").val("");
	});

	/*var from_date_obj=$('#from_date').datepicker({
	format: "yyyy/mm/dd"
	}).on('changeDate', function(ev) {
		from_date_obj.hide();
	}).data('datepicker');

	var to_date_obj=$('#to_date').datepicker({
	format: "yyyy/mm/dd"
	}).on('changeDate', function(ev) {
		to_date_obj.hide();
	}).data('datepicker');  */

		$('#to_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY'
			});
			
		$('#from_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY', 
				shortTime : true
			}).on('change', function(e, date)
			{
				$('#to_date').bootstrapMaterialDatePicker('setMinDate', date);
			});
			
	
			
   // Array holding selected row IDs
   var rows_selected = [];
    table = $('#manage_coupon_table').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Coupon/manage_coupon_processing", // json datasource
            data: function (d) { d.from_date = $('#from_date').val(); d.to_date = $('#to_date').val(); },
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#manage_coupon_table_processing").css("display","none");
            },
            "dataSrc": function ( json ) {
                document.getElementById("coupon_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
        drawCallback: function () {
            
		   
        },
	
	
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'5%',
         'className': 'dt-body-center'
      },{
         'targets': 1,
         'width':'5%',
      },{
         'targets': 2,
         'width':'15%',
      },{
         'targets': 3,
         'width':'15%',
      },{
         'targets': 4,
         'width':'15%',
      },{
         'targets': 5,
         'width':'15%',
      },{
         'targets': 6,
         'width':'15%',
      }],
      'order': [0, 'desc']
   });
 //  $('#from_date, #to_date').keyup( function() {
//        table.draw();
//    } );
	
////////////added for accordition///////////

	$('.collapse').on('shown.bs.collapse', function(){
	$(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
	}).on('hidden.bs.collapse', function(){
	$(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
	});

	////////////added for accordition///////////
	
$( "#from_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
table.draw();
} );

$( "#to_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
table.draw();
} );

$('#reset_form_button').on('click',function(){
	table.draw();
});

$('#submit_form_button').on('click',function(){
	table.draw();
});


});	

			
function selectAllFun(obj){
	var obj_arr=document.getElementsByName("onhold_orders_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}

</script>
  
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header title"><h4 class="text-center">Coupons <span class="badge" id="coupon_count"></span></h4></div>


	<table id="manage_coupon_table" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th colspan="7">
                                    <div class="row">

                                    <div class="col-md-3">
                                    <a class="btn btn-sm btn-success" id="add_coupon_btn" href="<?php echo base_url();?>admin/Coupon/add_coupon">Create Coupon
                                    </a>  
                                    </div>
					<!--<div class="col-md-3">
					<button class="btn btn-sm btn-success" id="set_as_approved_btn" onclick="set_as_approved_fun()">Approve</button>                                            <button class="btn btn-sm btn-danger" onclick="cancel_order_fun()">Cancel</button>
					</div>-->
					<div class="col-md-9 text-right">
					<form id="date_range" class="form-inline">
					<input type="text" name="from_date" id="from_date" placeholder="From Date" class="form-control">
					<input type="text" name="to_date" id="to_date" placeholder="To Date" class="form-control">
					<button type="button" class="btn btn-sm btn-primary" id="submit_form_button">Submit</button>
					<button type="reset" class="btn btn-sm btn-info" id="reset_form_button">Reset</button>
					</form>
					
					<!--<button type="submit" class="btn btn-primary">Search</button>-->
					
					</div>
                                    </div>
				</th>
			</tr>
			<tr>
				<th>S.No<!---<input type='checkbox' onclick="selectAllFun(this)">--></th>
				<th class="text-primary small bold">Coupon Code</th>
				<th class="text-primary small bold">Coupon Name</th>
				<th class="text-primary small bold">Details</th>
				<th class="text-primary small bold">Coupon Value</th>
				<th class="text-primary small bold">Valid Time Period</th>
				<th class="text-primary small bold">Action</th>
			</tr>
		</thead>
	</table>
</div>


  
    
  <div class="modal" id="coupon_summary">
     <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title small">Coupon Summary</h4>
            </div>
            <div  class="modal-body small" id="model_content">
                <div class="container">

                </div>
            </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
  </div>
  
<script type="text/javascript">

$("#coupon_summary").modal("hide");

function view_order_summary_(coupon_id){
    //  alert(order_id);
    $.ajax({
        url:"<?php echo base_url()?>admin/Coupon/get_data_from_invoice_offers",
        type:"POST",
        data:"coupon_id="+coupon_id,
        success:function(data){
            //alert(data);
            $("#description").val("");	
            $("#model_content").html(data);
            $("#order_summary").modal("show");
        }
    });
} 

function change_status(current_status,coupon_id){
//alert(current_status);
if(parseInt(current_status)==0 || parseInt(current_status)==1) {
    $.ajax({
        url:"<?php echo base_url()?>admin/Coupon/change_status",
        type:"POST",
        data:"coupon_id="+coupon_id+"&status="+current_status,
        dataType: "JSON",                
        asyc:false,
        success:function(data){
            if (parseInt(data.flag) == 1) {
                swal(
                    data.message,
                    '',
                    'info'
                ) 
                table.draw();
            } 
        }
    });
}

}

</script>
 </div> 
</body> 
</html>  