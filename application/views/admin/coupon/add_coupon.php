<html lang="en">
<head>

<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/multiselect.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 
<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>


<script type="text/javascript">

function isNumber(evt) {

    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
	
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
	
    return true;
}	   
</script>


<?php //print_r($vendor);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row-fluid" id="editDiv3">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="coupun" id="coupon_form" method="post"  enctype="multipart/form-data">
	<script type="text/javascript">
                        $.validator.addMethod('lessthan', function(value, element, param) {
                            var value_type = $('#type').val();

                            if(value_type == 2 && parseInt($(param).val()) != '')
                            { 
                                if(parseInt(value) > parseInt($(param).val())){
                                    $('#lesser_msg').html('');
                                }else{
                                    $('#lesser_msg').html('Total cart value should be greater than flat value.');
                                }
                                console.log(parseInt(value) + " || "+  parseInt($(param).val()));
                                return parseInt(value) > parseInt($(param).val());
                            }
                            else{ 
                                $('#lesser_msg').html('');
                                console.log('else');
                                return true;
                            }
                              //return (value_type == 1 && $(param).val() != '') ? this.optional(element) || value > $(param).val() : false; 

                        }, 'Invalid value');
                        $.validator.addMethod('greaterthan', function(value, element, param) {
                            var value_type = $('#type').val();
                            console.log(parseInt(value));
                           if(value_type == 1){
                               if(parseInt(value) <= 99 && parseInt(value)>0){
                                   $('#greaterthan_msg').html('');
                               }else{
                                   $('#greaterthan_msg').html('Percentage value should be 1 to 99 ');
                               }
                               return parseInt(value) <= 99 && parseInt(value)>0
                           }else{
                               $('#greaterthan_msg').html('');
                               return true;
                           }
                           
                               
                        }, 'Invalid value');
        
			var $validator = $('.wizard-card form').validate({
			rules: {
                            "type_of_coupon" : "required",
                            "coupon_name" :"required",
                            "coupon_code" :"required",
                            "type" :"required",
                            "coupon_per_user" :"required",
                            "value1" :{
                                required: {
                                    depends: function(element) {
                                        if(parseInt($('#type').val())==2){ return true; } else { return false; } 
                                    }
                                }
                            },
                            "value2" :{
                                required: {
                                    depends: function(element) {
                                        if(parseInt($('#type').val())==1){ return true; } else { return false; } 
                                    }
                                },
                                greaterthan:"99",
                            },
                            "start_date" :"required",
                            "end_date" :"required",
                            "status" :"required",
                            "to_select_list[]": {
                                required: {
                                    depends: function(element) {
                                        //console.log(parseInt($('#type_of_coupon').val()));
                                        if(parseInt($('#type_of_coupon').val())==1){ return true; } else { return false; } 
                                    }
                                }
                            },
                            "tot_cart_val":{
                                required: {
                                    depends: function(element) {
                                        //console.log(parseInt($('#type_of_coupon').val()));
                                        if(parseInt($('#type_of_coupon').val())==2){ return true; } else { return false; } 
                                    }
                                },
                                lessthan : '#value1'
                            },
			},
                        messages: {
                    
                                "tot_cart_val":
                                    {
                                        required: "Enter valid amount",
                                        lessthan: "Amount should be greater than flat amount."
                                    }

                        },
			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>				
		
                <input type="hidden" name="coupon_id" id="coupon_id" value="<?php echo $coupon_id;?>">
		
                <?php 
                    $coupon_name='';
                    $coupon_code='';
                    $sku_pcat_id='';
                    $sku_cat_id='';
                    $sku_subcat_id='';
                    $sku_brand_id='';
                    $sku_product_id='';
                    $sku_ids='';
                    $quantity='';
                    $purchase_quantity='';
                    $type='';
                    $value='';
                    $start_date='';
                    $end_date='';
                    $status='';
                    $type_of_coupon='';
                    $terms='';
                    $tot_cart_val='';
                    $coupon_per_user='';
                    $text="Create Coupon";
                    
                    if(!empty($coupon_data)){
                        $sku_pcat_id=$coupon_data->sku_pcat_id;
                        $sku_cat_id=$coupon_data->sku_cat_id;
                        $sku_subcat_id=$coupon_data->sku_subcat_id;
                        $sku_brand_id=$coupon_data->sku_brand_id;
                        $sku_product_id=$coupon_data->sku_product_id;
                        $sku_ids=$coupon_data->sku_ids;
                        /**/
                        $ids=explode('|',$sku_ids);
                        $c_txt='';
                        if(!empty($ids) && $sku_ids!=''){
                            for($k=0;$k<count($ids);$k++){
                                $rand_1=sample_code();
                                $str=explode('-',$ids[$k]);
                                //print_r($str);
                                if(!empty($str)){
                                    $c_txt.='<option value="'.$str[0].'-'.$str[1].'" selected >'.$str[0].' </option>';
                                }
                            }
                        }else{
                            $c_txt.='';
                        }
                        /**/
                        $coupon_name=$coupon_data->coupon_name;
                        $coupon_code=$coupon_data->coupon_code;
                        
                        $type=$coupon_data->type;
                        $value=$coupon_data->value;
                        $start_date=$coupon_data->start_date;
                        $end_date=$coupon_data->end_date;
                        $status=$coupon_data->status;
                        $type_of_coupon=$coupon_data->type_of_coupon;
                        
                        $purchase_quantity=$coupon_data->purchase_quantity;
                        $terms=$coupon_data->terms;
                        $tot_cart_val=$coupon_data->tot_cart_val;
                        $coupon_per_user=$coupon_data->coupon_per_user;
                        $text="Edit Coupon";
                        
                    }
                ?>
                <input type="hidden" id="sku_pcat_id_hidden" value="<?php echo $sku_pcat_id; ?>">
                <input type="hidden" id="sku_cat_id_hidden" value="<?php echo $sku_cat_id; ?>">
                <input type="hidden" id="sku_subcat_id_hidden" value="<?php echo $sku_subcat_id; ?>">
                <input type="hidden" id="sku_brand_id_hidden" value="<?php echo $sku_brand_id; ?>">
                <input type="hidden" id="sku_product_id_hidden" value="<?php echo $sku_product_id; ?>">
                <input type="hidden" id="sku_ids_hidden" value="<?php echo $sku_ids; ?>">
		<div class="wizard-header">
                    
		<div align="center">
			<h3 class="wizard-title formheader"><?php echo $text; ?></h3>
		<div>	
		</div>
		<div class="tab-content">
		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group  label-floating">
					<label class="control-label">Coupon Name</label>
					<input id="coupon_name" name="coupon_name" type="text" class="form-control" value="<?php echo $coupon_name;?>"/>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Coupon Code</label>
					<input id="coupon_code" name="coupon_code" type="text" class="form-control" value="<?php echo $coupon_code;?>"/>
				</div>
				</div>
                            
			</div>
			<div class="row" style="display:none;">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Type of value</label>
					<select id="type" name="type" type="text" class="form-control" onchange="myselect()">
                                            <option value=""></option>
                                            <!-- <option value="1" <?php echo ($type=='1') ? "selected": "" ?> > Percentage</option> -->
                                            <option value="2" <?php echo ($type=='2') ? "selected": "" ?> selected> Flat</option>
                                        </select>
				</div>
				</div>
			</div>
            <!-- <?php echo ($type=='2') ? 'style=""' : 'style="display:none;"' ; ?>  -->
			<div class="row" id="flat_div" >
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Flat Value</label>
					<input id="value1" name="value1" onKeyPress="return isNumber(event)"  type="text" class="form-control" value="<?php echo $value;?>"/>
				</div>
				</div>
                            
			</div>
                    
			<div class="row" id="percentage_div" <?php echo ($type=='1') ? 'style=""' : 'style="display:none;"' ; ?>>
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Value</label>
                                        <input id="value2" maxlength="3" onblur="handleChange(this)" name="value2" onKeyPress="return isNumber(event)" onchange="handleChange(this)"  type="text" class="form-control" value="<?php echo $value;?>"/>
				</div>
                                    <span style="color:red;" id="greaterthan_msg"></span>
				</div>
                            
			</div>
                        
                    
                        
            <div class="row" style="display:none;">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Type of Coupon</label>
                                        <select id="type_of_coupon" name="type_of_coupon" type="text" class="form-control" onchange="show_divs(this.value)">
                                            <option value=""></option>
<!--                                            <option value="1" <?php echo ($type_of_coupon=='1') ? "selected": "" ?> > Product SKUs</option>-->
                                            <option value="2" <?php echo ($type_of_coupon=='2') ? "selected": "" ?> selected> Invoice</option>
                                        </select>
				</div>
				</div>
			</div>
                    
                    <!---user invoice section ---->

                    <!-- <?php echo ($type_of_coupon=='2' ) ? '' : 'style="display:none;"'; ?> -->
                    <div id="user_invoice_section"  >
                        <div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Total cart value (greater than)</label>
                                        <input id="tot_cart_val" name="tot_cart_val" onkeypress="hide_err_text();" onKeyPress="return isNumber(event)" type="text" class="form-control" value="<?php echo $tot_cart_val;?>"/>
				</div>
                                    <span style="color:red;" id="lesser_msg"></span>
				</div>
			</div>
                    </div>
                    <!---user invoice section ---->
                    
                    <!---sku section --->
                    <div id="sku_section" <?php echo ($type_of_coupon=='1' ) ? '' : 'style="display:none;"'; ?>>
                                    <div class="form-group">
                                        <h4 class="control-label col-sm-10">Select SKUs </h4>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="form-group label-floating">
                                            <label class="control-label">-Select Parent Category-</label>
                                                <select id="pcat_id" class="form-control" name="sku_pcat_id" onchange="showAvailableCategories(this)">
                                                    <option value=""></option>

                                                    <?php foreach ($parent_category as $parent_category_value) {  ?>
                                                    <option value="<?php echo $parent_category_value->pcat_id; ?>" <?php echo ($parent_category_value->pcat_id==$sku_pcat_id)? "selected":''; ?> ><?php echo $parent_category_value->pcat_name; ?></option>
                                                    <?php } ?>
                                                    <option value="0" <?php echo (intval($sku_pcat_id)==0) ? 'selected' : ''; ?>>--None--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="form-group label-floating">
                                            <label class="control-label">-Select Category-</label>
                                            <select  class="form-control" id="categories" name="sku_cat_id"  onchange="showAvailableSubCategories(this)">
                                                <option value=""></option>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    </div>
                        
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="form-group label-floating">
                                            <label class="control-label">-Select Sub Category-</label>
                                            <select  class="form-control" id="subcategories" name="sku_subcat_id"  onchange="showAvailableBrands(this)">
                                                <option value=""></option>
                                                
                                            </select>
                                            </div>
                                        </div>
                                    </div>
            
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="form-group label-floating">
                                            <label class="control-label">-Select Brand-</label>
                                            <select  class="form-control" id="brands" name="sku_brand_id"  onchange="showAvailableProducts(this)" onchange="showAvailableSkus(this)">
                                                <option value=""></option>
                                                
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="form-group label-floating">
                                            <label class="control-label">-Select Products-</label>
                                            <select  class="form-control" id="products" name="sku_product_id" onchange="showAvailableSkus(this)">
                                                <option value=""></option>
                                                
                                            </select>
                                            </div>
                                        </div>
                                    </div>
            
                                    <div class="form-group">
                                        <div class="row">
                                        <div class="col-md-10 col-md-offset-1">  
                                            <div class="col-sm-5">
                                                <select name="from_select_list[]" id="from_select_list" class="form-control" size="8" multiple="multiple"> 
                                                </select>
                                            </div>

                                            <div class="col-sm-2 margin_top">
                                                <button type="button" id="create_rightAll" class="btn btn-block btn-xs btn-info"><i class="glyphicon glyphicon-forward"></i></button>
                                                <button type="button" id="create_rightSelected" class="btn btn-block btn-xs btn-info"><i class="glyphicon glyphicon-chevron-right"></i></button>
                                                <button type="button" id="create_leftSelected" class="btn btn-block btn-xs btn-info"><i class="glyphicon glyphicon-chevron-left"></i></button>
                                                <button type="button" id="create_leftAll" class="btn btn-block btn-xs btn-info"><i class="glyphicon glyphicon-backward"></i></button>
                                            </div>

                                            <div class="col-sm-5">
                                                <select name="to_select_list[]" id="to_select_list" class="form-control" size="8" multiple="multiple">
                                                    
                                                   <?php echo $c_txt; ?>
                                                </select>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
            
                    </div>
                    <!---sku section --->
                     
                    <div class="row">
                            <div class="col-md-10 col-md-offset-1">  
                            <div class="form-group  label-floating">
                                    <label class="control-label">Terms</label>
                                    <textarea id="terms" name="terms" class="form-control" ><?php echo $terms;?></textarea>
                            </div>
                            </div>

                    </div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">  
                        <div class="form-group label-floating">
                            <label class="control-label">From date</label>
                            <input type="text" name="start_date" id="start_date" class="form-control" value="<?php echo ($start_date!='') ? date('d/m/Y H:i',strtotime($start_date)) : '' ; ?>" >
                        </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">  
                        <div class="form-group label-floating">
                            <label class="control-label">End date</label>
                            <input type="text" name="end_date" id="end_date" class="form-control" value="<?php  echo ($start_date!='') ? date('d/m/Y H:i',strtotime($end_date)) : ''; ?>"><!-- comment -->
                        </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">                               
                        <div class="form-group label-floating">
                                <label class="control-label">-Coupon Status-</label>
                                <select name="status" id="status" class="form-control">
                                        <option value=""></option>
                                        <option value="1" <?php echo ($status=='1') ? "selected" : "" ?> >Active</option>
                                        <option value="0" <?php echo ($status=='0') ? "selected" : "" ?> >Inactive</option>
                                </select>
                        </div>
                        </div>
				
                    </div>

                    <div class="row">
                            <div class="col-md-10 col-md-offset-1">  
                            <div class="form-group  label-floating">
                                    <label class="control-label">Coupon per User</label>
                                    <input id="coupon_per_user" name="coupon_per_user" class="form-control" value="<?php echo ($coupon_per_user !='') ? $coupon_per_user : ''; ?>" maxlength="10" onKeyPress="return isNumber(event)" >
                            </div>
                            </div>

                    </div>
                        
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
				<div class="form-group">
					<button class="btn btn-info btn-sm" type="button" id="submit-data" onclick="submit_form();"></i>Submit</button>
					<button class="btn btn-Warning btn-sm" type="reset" >Reset</button>
				</div>	
				</div>
			</div>		
	
	</div>
                </div>
                </div>
                
</form>

</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>
</div>
</body>
</html>
<script type="text/javascript">

$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='coupon'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="coupon"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_vendors'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});

});

function show_divs(val_type){
    if(parseInt(val_type)==1){
        $('#sku_section').show();
        $('#user_invoice_section').hide();
    }
    if(parseInt(val_type)==2){
        $('#sku_section').hide();
        $('#user_invoice_section').show();
    }
    if(val_type==''){
        $('#sku_section').hide();
        $('#user_invoice_section').hide();
    }
}

function submit_form() {
    
   
                if($('#coupon_form').valid())
                {
                    var type_of_coupon = 2;
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url()."admin/Coupon/check_coupon_exists"; ?>',
                        dataType: "JSON",
                        async:false,
                        data: {
                            coupon_name: $('#coupon_name').val(),
                            coupon_code: $('#coupon_code').val(),
                            coupon_id: $('#coupon_id').val()
                        },
                        success: function (response) {
                            //alert(response);
                            //alert(parseInt(response.flag));
                            //return false;
                            if (1) {
                                
                                //alert(parseInt(response.flag));
                                
                                if (parseInt(response.flag) == 1) {
                                    /*swal(
                                        'Coupon code or coupon name exists already',
                                        'Please change it',
                                        'info'
                                    ) */
									
									swal({
						title:"Info", 
						text:"Coupon code or coupon name exists already", 
						type: "info",
						allowOutsideClick: false
					}).then(function () {
						return false;

					});
					
					
                                    return false;
                                } else if(parseInt(response.flag)==0){
									document.getElementById("coupon_form").action="<?php echo base_url() ?>admin/Coupon/add_coupon_submit";
									document.getElementById("coupon_form").submit();
                                    //return true;
                                   /* not exist */
                                   /*alert('q');
                                   $.ajax({
                                            url: '<?php echo base_url()."admin/Coupon/add_coupon_submit"?>',
                                            type: 'POST',
                                            data: $('#coupon_form').serialize(),
                                            dataType: 'html',

                                            }).done(function(data)
                                              {
                                                  alert(data);

                                                    if(data==true)
                                                    {

                                                            swal({
                                                                    title:"Success", 
                                                                    text:"Coupon is created Successfully!", 
                                                                    type: "success",
                                                                    allowOutsideClick: false
                                                            }).then(function () {
                                                                    location.reload();

                                                            });
                                                    }
                                                    if(data==0)
                                                    {
                                                            swal(
                                                                    'Oops...',
                                                                    'Error in form',
                                                                    'error'
                                                            )
                                                    }

                                            });
                            */
                                   /* not exist */
                                   
                                   
                                }else {
                                    return false;
                                /*
                                    swal(
                                        'Oops...',
                                        'Error in form',
                                        'error'
                                    )*/
                                }
                            }
                        }
                    });
                }
                else
                {
                    return false;
                }
                }
function form_validation_edit()
{
	var name = $('input[name="edit_name"]').val().trim();
	var email = $('input[name="edit_email"]').val().trim();
	var mobile = $('input[name="edit_mobile"]').val().trim();
	var pincode = $('input[name="pincode"]').val().trim();
	var edit_address1 = $('input[name="edit_address1"]').val().trim();
	var edit_address2 = $('input[name="edit_address2"]').val().trim();
	var edit_landline = $('input[name="edit_landline"]').val().trim();

	   var err = 0;
	   if((name=='') || (email=='') || (mobile=='') || (pincode=='') || (edit_address1=='') || (edit_address2=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
var form_status = $('<div class="form_status"></div>');		
var form = $('#edit_vendors');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();		
				$.ajax({
				url: '<?php echo base_url()."admin/Coupon/add_coupon/"?>',
				type: 'POST',
				data: $('#edit_vendors').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {
                              //alert(data);
                                
				if(data==true)
				{
										  
					swal({
						title:"Success", 
						text:"Your profile is successfully updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				if(data==0)
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
			});
}
}]);			
			} 
	
	return false;
}

    <!---<!-- sku selection -->
function showAvailableProducts(obj){
	
	if(obj.value!="" && obj.value!="None"){
		brand_id=obj.value;
                selected_product_id=$('#sku_product_id_hidden').val();
		$.ajax({
                        url:"<?php echo base_url(); ?>admin/Coupon/show_available_products",
                        type:"post",
                        data:"brand_id="+brand_id+"&active=1&product_id="+selected_product_id,
                        success:function(data){
                                if(data!=0){
                                        $("#products").html(data);
                                        if(selected_product_id!='' &&selected_product_id!=null){
                                            $('#products').trigger("change");
                                        }
                                }
                                else{
                                        $("#products").html('<option value=""></option>');
                                }
                        }
                });
	}
	
}

 
     
function showAvailableBrands(obj){
	
	if(obj.value!="" && obj.value!="None"){
		subcat_id=obj.value;
                selected_brand_id=$('#sku_brand_id_hidden').val();
		$.ajax({
                        url:"<?php echo base_url(); ?>admin/Coupon/show_available_brands",
                        type:"post",
                        data:"subcat_id="+subcat_id+"&active=1"+"&brand_id="+selected_brand_id,
                        success:function(data){
                                if(data!=0){
                                        $("#brands").html(data);
                                        $('#brands').trigger("change");
                                }
                                else{
                                        $("#brands").html('<option value=""></option>');
                                }
                        }
                });
	}
	
}
function showAvailableSubCategories(obj){
	
	if(obj.value!="" && obj.value!="None"){
		cat_id=obj.value;
                selected_subcat=$('#sku_subcat_id_hidden').val();
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Coupon/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id+"&active=1"+"&subcat_id="+selected_subcat,
				success:function(data){
					if(data!=0){
						$("#subcategories").html(data);
                                                $('#subcategories').trigger("change");
					}
					else{
						$("#subcategories").html('<option value=""></option>')
						$("#brands").html('<option value=""></option>')

					}
				}
			});
	}
	
}

function showAvailableCategories(obj){
        //alert();
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
                
                selected_cat=$('#sku_cat_id_hidden').val();
                //alert(selected_cat);
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Coupon/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1"+"&cat_id="+selected_cat,
				success:function(data){
					if(data!=0){
                                            $("#categories").html(data);
                                            if(selected_cat!='' && selected_cat!=null){
                                                
                                                $('#categories').trigger("change");
                                                
                                            }       
					}else{
                                            $("#categories").html('<option value=""></option>')
                                            $("#brands").html('<option value=""></option>')

					}
				}
			});
	}
	
}
function showAvailableSkus(obj){
            if(obj.value!="" && obj.value!="None"){
		product_id=obj.value;
                sku_ids=$('#sku_ids_hidden').val();
                $.ajax({
                            url:"<?php echo base_url(); ?>admin/Coupon/show_available_skus",
                            type:"post",
                            data:"product_id="+product_id+"&active=1&sku_ids="+sku_ids,
                            success:function(data){
                                /*if(data!=0){
                                        $("#skus").html(data);
                                }else{
                                        $("#skus").html('<option value=""></option>');
                                }*/
                                
                                if(data!=0){
                                    $("#from_select_list").html(data);	
                                    
                                }else{
                                    if(sku_ids==''){
                                        $("#to_select_list").html("");	
                                    }
                                }
                            }
                    });
            }
}

function hide_err_text(){
    $('#lesser_msg').html('');
}

function myselect() {
    var type = document.getElementById("type").value;
    //alert(type);
    if (parseInt(type) == 2) {
        $('#percentage_div').hide();
        $('#flat_div').show();
        
    }else if (parseInt(type) == 1) {
        $('#flat_div').hide();
        $('#percentage_div').show();
        
    }
    if(parseInt(type) == ''){
        $('#flat_div').hide();
        $('#percentage_div').hide();  
    }
}
function handleChange(input) {
    if (input.value < 0) input.value = 0;
    if (input.value > 99) input.value = 99;
}

$('#from_select_list').multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
        }
    });

    $('#end_date').bootstrapMaterialDatePicker
            ({
                    time: true,
                    clearButton: true,
                    weekStart: 0, 
                    format: 'DD/MM/YYYY hh:mm',
                    shortTime : true,
                    use24hours: true,
                    clearButton: true,
                    
            });

    $('#start_date').bootstrapMaterialDatePicker
    ({
            time: true,
            clearButton: true,
            weekStart: 0, 
            format: 'DD/MM/YYYY hh:mm', 
            shortTime : true,
            use24hours: true,
            clearButton: true,
            
    }).on('change', function(e, date)
    {
            $('#end_date').bootstrapMaterialDatePicker('setMinDate', date);
    });

    $(document).ready(function(){
       coupon_id=$('#coupon_id').val();
       type_of_coupon=$('#type_of_coupon').val();
       
       if(coupon_id!='' && parseInt(type_of_coupon)==1){
           //alert("coupon_id="+coupon_id+" type_of_coupon="+type_of_coupon);
           $('#pcat_id').trigger("change");
       }
});
		
</script>
