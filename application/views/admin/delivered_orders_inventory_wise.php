<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Delivered Orders</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" /> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 
<style type="text/css">
.padding_right{
	float:left;
	margin-right:15px;	
}
.accordion-toggle:hover {
  text-decoration: none;
}
.more-less {
        color: #212121;
    }
	.free_items_style{
		padding: 10px;
		border-bottom: 1px solid #ccc;
		margin-bottom: 13px;
	}
.form-inline{
	margin-bottom:0;
}
.form-group{
	margin-bottom:0;
	margin-top:0;
	padding-bottom:0;
	vertical-align:bottom;
}
</style>

<script type="text/javascript">
$(document).ready(function (){
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("input[type=text]").val("");
	});

		$('#to_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY'
			});
			
		$('#from_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY', 
				shortTime : true
			}).on('change', function(e, date)
			{
				$('#to_date').bootstrapMaterialDatePicker('setMinDate', date);
			});
   // Array holding selected row IDs
   var rows_selected = [];
   var table = $('#delivered_orders_inventory_wise_table').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Orders/delivered_orders_inventory_wise_processing", // json datasource
			data: function (d) { d.from_date = $('#from_date').val(); d.to_date = $('#to_date').val();d.filter_inventory_id = "<?php echo $filter_inventory_id;?>"; },
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#delivered_orders_inventory_wise_table_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("delivered_inventory_wise").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'5%',
         'className': 'dt-body-center'
      },{
         'targets': 1,
         'width':'40%',
      },{
         'targets': 2,
         'width':'20%',
      },{
         'targets': 3,
         'width':'15%',
      },{
         'targets': 4,
         'width':'10%',
      },{
         'targets': 5,
         'width':'10%',
      }],
      'order': [0, 'desc']
   });
	$( "#from_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
	table.draw();
	} );

	$( "#to_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
	table.draw();
	} );

	$('#reset_form_button').on('click',function(){
		table.draw();
	});

	$('#submit_form_button').on('click',function(){
		table.draw();
	});
});	


 function generate_invoice_for_order_ID_offers(order_id,type_of_order){
	if(type_of_order=="cashback"){
		var str="CBK";
	}else{
		var str="SG";
	}
	
	window.open("<?php echo base_url()?>assets/pictures/invoices/Orders/Invoice_"+str+"_"+order_id+".pdf","_blank");
 }
</script>
  
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header"><h4 class="text-center">Delivered Orders - <?php echo $get_inventory_info_by_inventory_id_obj->sku_id?> <span class="badge" id="delivered_inventory_wise"></span></h4></div>
	<table id="delivered_orders_inventory_wise_table" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th colspan="6">
					<div class="col-md-12 text-center">
						<form id="date_range" class="form-inline">
						<input type="text" name="from_date" id="from_date" placeholder="From Date" class="form-control">
						<input type="text" name="to_date" id="to_date" placeholder="To Date" class="form-control">
						<button type="button" class="btn btn-sm btn-primary" id="submit_form_button">Submit</button>
						<button type="reset" class="btn btn-sm btn-info" id="reset_form_button">Reset</button>
						<input type='button' class='btn btn-sm btn-info' id="back" value='Previous' />
						</form>
					</div>
				</th>
			</tr>
			<tr>
				<th class="text-primary small bold">Image</th>
				<th class="text-primary small bold">Order Summary</th>
				<th class="text-primary small bold">Delivered Details</th>
				<th class="text-primary small bold">Quantity and Price</th>
				<th class="text-primary small bold">Buyer Details</th>
				<th class="text-primary small bold">Status</th>
			</tr>
		</thead>
	</table>
</div>


  <div class="modal" id="order_summary">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Order Summary</h4>
           </div>
           <div  class="modal-body" id="model_content">
		  
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
  </div>
  <form name="back_to_inventory" id="back_to_inventory" method="post">
		<input type="hidden" value="<?php echo $get_inventory_info_by_inventory_id_obj->pcat_id; ?>" name="parent_category">
		<input type="hidden" value="<?php echo $get_inventory_info_by_inventory_id_obj->cat_id; ?>" name="categories">
		<input type="hidden" value="<?php echo $get_inventory_info_by_inventory_id_obj->subcat_id; ?>" name="subcategories">
		<input type="hidden" value="<?php echo $get_inventory_info_by_inventory_id_obj->brand_id; ?>" name="brands">
		<input type="hidden" value="<?php echo $get_inventory_info_by_inventory_id_obj->product_id; ?>" name="products">
		<input value="view" type="hidden" name="create_editview">
</form>
<script type="text/javascript">
  $("#order_summary").modal("hide");
  function view_order_summary_(order_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Orders/get_data_from_invoice_offers",
		type:"POST",
		data:"order_id="+order_id,
		success:function(data){
			
			$("#description").val("");	
			$("#model_content").html(data);
			$("#order_summary").modal("show");
			
		}
	});
	
}
function go_to_return_details(order_item_id){	
	 $('#return_details_'+order_item_id).attr('action', '<?php echo base_url();?>admin/Orders/return_details_of_order_item').submit();
}
</script>
<script type="text/javascript">
$(document).ready(function (){
$('#back').on('click',function(){	
		var form = document.getElementById('back_to_inventory');
		form.action='<?php echo base_url(); ?>admin/Catalogue/inventory';
		form.submit();
	});
});		
</script>
</div>
</body> 
</html>  