<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<script>
function show_view_validatn_create(){
	sort_order=document.getElementById("sort_order").value;
	if(sort_order==0){
	document.getElementById("view1").disabled = true;
	document.getElementById("view2").checked = true;					
	}
	if(sort_order!=0){
		document.getElementById("view1").disabled = false;
		document.getElementById("view1").checked = true;					
	}
}
</script>

<script type="text/javascript">
var table;

$(document).ready(function (){
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    var rows_selected = [];
    table = $('#table_specification').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Catalogue/specification_processing",
			data: function (d) { d.pcat_id = $('#pcat_id').val();d.cat_id = $('#cat_id').val();d.subcat_id = $('#subcat_id').val();d.specification_group_id = $('#specification_group_id').val();d.active =1;},
            type: "post",
            error: function(){
              $("#table_specification_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				
				document.getElementById("specification_count").innerHTML=json.recordsFiltered;
				
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
   
	$('#reset_form_button').on('click',function(){
		table.draw();
	});
	
	$("#submit_button").click(function() {
		table.draw();
	});
	$('#search_toggle').on('click',function(){	
		$('#search_block').toggle();
	});

});	

function drawtable(obj){
	table.draw();
}

function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function showSortOrderSpecification(obj){
	
	
	if(obj.value!=""){
	specification_group_id=obj.value;
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/show_specifi_sort_order",
		type:"post",
		data:"specification_group_id="+specification_group_id,
		
		success:function(data){
			$("#sort_order").html(data);
		}
	});
	}else{
		
		$("#sort_order").html("");
	}
}

function multilpe_delete_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
swal({
			title: 'Are you sure?',
			text: "Archived Specification(entire tree)",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Archive it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/update_specification_selected",
		type:"post",
		data:"selected_list="+selected_list+"&active=0",
		
		success:function(data){
			if(data==true){
				swal({
						title:"Archived!", 
						text:"Given "+table+"(s) has been archived successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
function multilpe_delete_when_no_data(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
swal({
			title: 'Are you sure?',
			text: "Delete Specification",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/delete_specification_selected_when_no_data",
		type:"post",
		data:"selected_list="+selected_list,
		
		success:function(data){
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Given "+table+"(s) has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else if(data==false){
				swal("Error", "Error in deleting this file", "error");
			}else{
				swal("Error", "Oops ..! You can't delete it.", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
</script>
<script>

$(document).ready(function(){
	<?php
		if($create_editview=="create"){
			?>
			$("#viewDiv1").css({"display":"none"});
			$("#createDiv2").css({"display":""});
			<?php
		}
		else if($create_editview=="view"){
			?>
			$("#createDiv2").css({"display":"none"});
			$("#viewDiv1").css({"display":""});
			<?php
		}
	?>
});
</script>
<script>

function changeViewPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/specification_filter';
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<?php 
//print_r($filter);
?>

<div class="container">

<div class="row" id="viewDiv1" style="display:none;">
	<div class="wizard-header text-center">
		<h5> Number of Specification <span class="badge"><div id="specification_count"></div></span> 
		</h5>                 		          	
	</div>
	<input value="<?php echo $pcat_id; ?>" type="hidden" id="pcat_id">
	<input value="<?php echo $cat_id; ?>" type="hidden" id="cat_id">
	<input value="<?php echo $subcat_id; ?>" type="hidden" id="subcat_id">
	<input value="<?php echo $specification_group_id; ?>" type="hidden" id="specification_group_id">
	
<table id="table_specification" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<th colspan="9">
			<button id="multiple_delete_button1" class="btn btn-warning btn-xs" onclick="multilpe_delete_fun('specification')">Archive all</button>	
			<button id="multiple_delete_button2" class="btn btn-danger btn-xs" onclick="multilpe_delete_when_no_data('specification')">Delete</button>
			<button class="btn btn-primary btn-xs" type="button" onclick="changeViewPrevious()">Change View</button>
		</th>
	</tr>
	<tr>
		<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
		<th>Specification Name</th>
		<th>Specification Values</th>
		<th>Specification Group Name</th>	
		<th>Specification to Catalog</th>
		<th>Sort Order</th>
		<th>View</th>
		<th>Last Updated</th>
		<th>Action</th>		
	</tr>
</thead>

</table>
</div>

<div class="row-fluid" id="createDiv2" style="display:none;">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">

	<form name="create_specification" id="create_specification" method="post" action="#" onsubmit="return form_validation();">
	
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				parent_category: {
					required: true,
				},
				
				sort_order: {
					required: true,
				},
				specification_group: {
					required: true,
				},
				specification_name: {
					required: true,
				},
				specification_value: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Create Specification</h3>
		</div>	
		</div>
		<div class="tab-content">
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Parent category-</label>
					<select name="parent_category" id="parent_category" class="form-control" onchange="showAvailableCategories(this)">
						<option value=""></option>
						<?php foreach ($parent_category as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>"><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<option value="0">--None--</option>
						
					</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group label-floating">
					<label class="control-label">-Select Category-</label>
					<select name="categories" id="categories" class="form-control" onchange="showAvailableSubCategories(this)">
						<option value=""></option>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Sub Category-</label>
					<select name="subcategories" id="subcategories" class="form-control" onchange="showAvailableSpecificationGroup(this)">
						<option value=""></option>
					</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group label-floating">
					<label class="control-label">-Select Specification_group -</label>
					<select name="specification_group" id="specification_group" class="form-control" onchange="showSortOrderSpecification(this)">
						<option value=""></option>
						
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">Enter Specification Name</label>
					<input id="specification_name" name="specification_name" type="text" class="form-control" />
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Specification Values separated by comma(,) </label>
					<textarea id="specification_value" name="specification_value" type="text" class="form-control" ></textarea>
				</div>
				</div>
			</div>
			<div class="row">
				
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Sort Order Value</label>
					
					<select name="sort_order" id="sort_order" class="form-control" onchange="show_view_validatn_create()">
						
					</select>
				</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-6 col-md-offset-3"> 
					<div class="form-group">
					<div class="col-md-6">Show Specification</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="view" id="view1" value="1" type="radio" checked="checked">View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="view" id="view2" value="0" type="radio">Hide</label>
					</div>
				
					</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-6 col-md-offset-4"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit_specification">Submit</button>
					<button class="btn btn-warning btn-sm" type="reset">Reset</button>
					<button class="btn btn-primary btn-sm" type="button" onclick="changeViewPrevious()">Change View</button>
					</div>
				</div>
			</div>
		</div>						

	</form>
				
</div>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
<script>

function showAvailableSpecificationGroup(obj){
	pcat_id=$("#parent_category").val();
	cat_id=$("#categories").val();
	//if(obj.value!="" && obj.value!="None"){
		subcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_specification_group",
				type:"post",
				data:"pcat_id="+pcat_id+"&cat_id="+cat_id+"&subcat_id="+subcat_id+"&active=1"+"&flagtype=3",
				success:function(data){
					
					if(data!=0){
						$("#specification_group").html(data);
						$("#edit_specification_group").html(data);
						$("#sort_order").html("");
					}
					else{
						$("#specification_group").html('<option value="0"></option>');
						$("#edit_specification_group").html('<option value="0"></option>');
						$("#sort_order").html("");
					}
				}
			});
	//}
	
}

function showAvailableSubCategories(obj){
	pcat_id=$("#parent_category").val();
	//if(obj.value!="" && obj.value!="None"){
		cat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#subcategories").html(data);
						$("#sort_order").html("");
					}
					else{
						$("#subcategories").html('<option value="0"></option>')
						$("#sort_order").html("");
						$("#brands").html('<option value="0"></option>')

					}
				}
			});
			
			$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_specification_group",
				type:"post",
				data:"pcat_id="+pcat_id+"&cat_id="+cat_id+"&active=1"+"&flagtype=2",
				success:function(data){
					
					if(data!=0){
						$("#specification_group").html(data);
						table.draw();
					}else{
						$("#specification_group").html('<option value=""></option>');
					}
				}
		});
	//}
	
}

function showAvailableCategories(obj){
	//if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#categories").html(data);
						$("#sort_order").html("");
					}
					else{
						$("#categories").html('<option value="0"></option>')
						$("#sort_order").html("");
						$("#brands").html('<option value="0"></option>')

					}
				}
			});
			
			$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_specification_group",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1"+"&flagtype=1",
				success:function(data){
					if(data!=0){
						$("#specification_group").html(data);
						table.draw();
					}else{
						$("#specification_group").html('<option value=""></option>');
					}
				}
		});
	//}
	
}

function form_validation()
{
	var specification_name = $('input[name="specification_name"]').val().trim();
	var specification_group = $('select[name="specification_group"]').val().trim();
	var parent_category = $('select[name="parent_category"]').val().trim();
	var categories = $('select[name="categories"]').val().trim();
	var subcategories = $('select[name="subcategories"]').val().trim();
	var specification_value = $('textarea[name="specification_value"]').val().trim();
	var sort_order = $('select[name="sort_order"]').val().trim();
	var view = document.querySelector('input[name="view"]:checked').value;
	
	
		var err = 0;
		
		if(specification_name=="")
		{
		   //$('input[name="specification_name"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('input[name="specification_name"]').css({"border": "1px solid #ccc"});
		}
		if(specification_group=="")
		{
		   //$('select[name="specification_group"]').css("border", "1px solid red");
		   err = 1;
		}else{
			//$('select[name="specification_group"]').css({"border": "1px solid #ccc"});
		}
		
		if(parent_category=='')
		{
		   //$('select[name="parent_category"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('select[name="parent_category"]').css({"border": "1px solid #ccc"});
		}
		
		if(specification_group=='' || specification_group==0){
			
			if(categories==''){
			   //$('select[name="categories"]').css("border", "1px solid red");	   
			   err = 1;
			}else{			
				//$('select[name="categories"]').css({"border": "1px solid #ccc"});
			}
			if(subcategories==''){
			   //$('select[name="subcategories"]').css("border", "1px solid red");	   
			   err = 1;
			}else{			
				//$('select[name="subcategories"]').css({"border": "1px solid #ccc"});
			}
		}else{
			//$('select[name="categories"]').css({"border": "1px solid #ccc"});
			//$('select[name="subcategories"]').css({"border": "1px solid #ccc"});
		}
		
		if(specification_value=="")
		{
		   //$('textarea[name="specification_value"]').css("border", "1px solid red");
		   //err = 1;
		}else{
			//$('textarea[name="specification_value"]').css({"border": "1px solid #ccc"});
		}
		if(sort_order=='')
		{
		   //$('input[name="sort_order"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('input[name="sort_order"]').css({"border": "1px solid #ccc"});
		}
		
		if(err==1)
		{
			   $('#error_result_id').show();
			   $('#result').hide();
		}
		else
			{	
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#create_specification');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();			
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_specification/"?>',
				type: 'POST',
				data: $('#create_specification').serialize(),
				dataType: 'html',
				
				}).done(function(data)
				  {
					form_status.html('') 
				if(data=="exist"){
					swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
					}).then(function(){
					document.getElementById("specification_name").value="";
					document.getElementById("specification_name").focus();
					});
				}
					if(data=="yes")
					{					  
						swal({
							title:"Success", 
							text:"Specification is successfully added!", 
							type: "success",
							allowOutsideClick: false
						}).then(function () {
							location.reload();

						});
					}
					if(data=="no")
					{
						swal(
							'Oops...',
							'Error in form',
							'error'
						)
					}
					
					
				});
}
}]);
			}
	return false;
}


</script>
</div>
</body>

</html>