<style type="text/css">
.form-group .help-block {
display: none;
}

.form-group.has-error .help-block {
display: block;
}
#columns{
max-width: 400px;
width: 100%;
margin: 0 auto;
}
</style>
<script src="<?php echo base_url();?>assets/js/angular.js" data-semver="1.2.5" data-require="angular.js@1.2.5"></script>
<script type="text/javascript">
module = angular.module('app', []);
module.run(function ($rootScope) {
$rootScope.$on('scope.stored', function (event, data) {
});
});

module.factory('Scopes', function ($rootScope) {
var mem = {};
return {
store: function (key, value) {
$rootScope.$emit('scope.stored', key);
mem[key] = value;
},
get: function (key) {
return mem[key];
}
};
});

module.directive('userExistC', function($http) {
return {
restrict: 'A',
require: 'ngModel',
link: function(scope, element, attr, ctrl) {
function customValidator(ngModelValue)
{
ctrl.$setValidity('userexistValidator', true);
var FormData = {'val' : ngModelValue}; 

$http({
method : "POST",
url : "admin/Adminlog/admin_check_user",
data:FormData,
dataType: 'json',
}).success(function mySucces(res) {

if(res.valid!=null)
{
if(res.valid==true)
{
ctrl.$setValidity('userexistValidator',true );

}
if(res.valid==false){
ctrl.$setValidity('userexistValidator', false);

}
}
else{
ctrl.$setValidity('userexistValidator', true);

}

}, function myError(response) {
alert('error');
});

return ngModelValue;
}
ctrl.$parsers.push(customValidator);     
}
};
});


module.directive('showErrors', function($timeout) {
return {
restrict: 'A',
require: '^form',
link: function (scope, el, attrs, formCtrl) {
var inputEl   = el[0].querySelector("[name]");
var inputNgEl = angular.element(inputEl);
var inputName = inputNgEl.attr('name');
var blurred = false;
inputNgEl.bind('blur', function() {
blurred = true;
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$watch(function() {
return formCtrl[inputName].$invalid
}, function(invalid) {
if (!blurred && invalid) { return }
el.toggleClass('has-error', invalid);
});

scope.$on('show-errors-check-validity', function() {
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$on('show-errors-reset', function() {
$timeout(function() {
el.removeClass('has-error');
}, 0, false);
});
}
}
});


function LoginController($scope,$http)
{

$scope.login_auth = function() {

$scope.$broadcast('show-errors-check-validity');
if ($scope.loginForm.$valid) {
username=$scope.username;
password=$scope.password;

var FormData = {'password' : password,'username' :username}; 			 
$http({
method : "POST",
url : "<?php echo base_url(); ?>admin/Adminlog/admin_login",
data:FormData,
dataType: 'json',
}).success(function mySucces(res) {

if(res.valid!=null && res.valid==true){		
location.reload();
window.location="Admin_type_manage";
}else{
alert("Please check your password");
//	swal("", "Please check your password", "error");
return false;
}

}, function myError(response) {
alert('error');
});

}
};

}

</script>
<div class="columns-container">
<div class="container">
<div id="columns">
<!-- page heading-->
<h2 class="page-heading text-center">
<span class="page-heading-title2">Admin Login</span>
</h2>
<!-- ../page heading-->
<div class="page-content" ng-app="app">
<div class="row">

<div class="col-md-12" id="login_form">

<div class="box-authentication" ng-controller="LoginController">


<form name="loginForm" novalidate id="loginForm">

<div class="form-group" show-errors>
<input id="" name="username" type="text" class="form-control" placeholder="Email/Mobile" required ng-model="username" user-exist-c="username" mobile-exist-c="username">
<p ng-if="loginForm.username.$error.required" class="help-block">Please enter Email/Mobile</p>

<p class="help-block" ng-if="loginForm.username.$error.userexistValidator">Admin is Not Yet Registered</p>
</div>

<div class="form-group" show-errors style="margin-bottom:3em;">
<input name="password" id="password" type="password" class="form-control" placeholder="Enter Password" required ng-model="password" password-check-c="password">


<p ng-if="loginForm.password.$error.required" class="help-block">Please enter password</p>
</div>

<button class="button btn-block" ng-click="login_auth()"><i class="fa fa-lock"></i> Sign in</button>
</form>
</div>

</div>
</div>
</div>
</div>
</div>
</div>