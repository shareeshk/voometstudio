<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Order Item Info</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
	<div class="container-fluid">
		
		<div class="col-md-8 col-md-offset-2">
			<h4>Order Item Details</h4>
			<table class="table table-bordered small" cellspacing="0" width="100%">
				<thead><th colspan="2"><b> Return Order Id :</b><?php echo $return_order_id; ?></th> </thead>
				
				<tbody>
				
				<tr><td>SKU ID: </td><td><?php echo $refund_info_obj->sku_id; ?> </td></tr>
				<tr><td>SKU Name</td><td><font color='blue'><?php echo $refund_info_obj->ord_sku_name; ?></b></font></td></tr>
				<tr><td>Product Name</td><td><font color='blue'><?php echo $refund_info_obj->product_name; ?></b></font></td></tr>
				<tr><td>Order Item ID</td><td><?php echo $refund_info_obj->order_item_id ; ?> </td></tr>
				<tr><td>Order ID</td><td><?php echo $refund_info_obj->order_id; ?></td></tr>
				
				
				<tr><td>Qty</td><td><?php echo $refund_info_obj->quantity ?></td></tr>
				<tr><td>Product Price</td><td> <?php echo curr_sym; ?><?php echo $refund_info_obj->product_price; ?> each</td></tr>
				<tr><td>Shipping Charge</td><td> <?php echo curr_sym; ?><?php echo $refund_info_obj->shipping_charge;?> each</td></tr>
				<tr><td>Total</td><td> <?php echo curr_sym; ?><?php echo $refund_info_obj->grandtotal; ?></td></tr>
				
				
				
				<tr><td>Delivered On</td><td><?php echo date("D j M,Y",strtotime($refund_info_obj->order_delivered_timestamp)); ?></td></tr>
				<tr><td>Courier Name</td><td><?php echo $refund_info_obj->logistics_name?></td></tr>
				<tr><td>Tracking Number</td><td><?php echo $refund_info_obj->tracking_number; ?></td></tr>
				
				<tr>
				<td>
				<b><u>Delivered to</u></b>
				</td>
				<td>
				<?php echo $refund_info_obj->customer_name; ?><br>
				<?php echo $refund_info_obj->address1; ?><br>
				<?php echo $refund_info_obj->address2." ".$refund_info_obj->city; ?><br>
				<?php echo $refund_info_obj->state." ".$refund_info_obj->country; ?><br>
				<b>Pincode:</b><?php echo $refund_info_obj->pincode; ?><br>
				<b>Mobile:</b><?php echo $refund_info_obj->mobile; ?>
				</td>
				</tr>
				</tbody>
			</table>
		</div>

	</div>

</div>
</body> 
</html>  