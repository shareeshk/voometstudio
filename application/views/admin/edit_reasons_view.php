<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	

<script type="text/javascript">

function showDivPrevious(){
	
	var form = document.getElementById('search_for_reasons');		
	
	form.action='<?php echo base_url()."admin/Catalogue/reasons"; ?>';
	form.submit();
			
	 //window.location.href = '<?php echo base_url(); ?>admin/Catalogue/category';
}
</script>
<?php
if($get_reason_data['sort_order']=="0"){?>
	<script>
	$(document).ready(function () {
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	document.getElementById('show_available_common_reason_return').disabled = true;
	
	});
	</script>
	<?php
}
?>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row-fluid" id="editDiv3">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="edit_reason" id="edit_reason" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_parent_category: {
					required: true,
				},
				edit_categories: {
					required: true,
				},
				edit_subcategories: {
					required: true,
				},
				edit_menu_sort_order: {
					required: true,
				},
				edit_reason_name: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Reasons</h3>
		</div>	
		</div>
		<div class="tab-content">				
		<input type="hidden" name="edit_reason_id" id="edit_reason_id" value="<?php echo $get_reason_data["reason_id"];?>">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Parent Category-</label>
					<select name="edit_parent_category" id="edit_parent_category" class="form-control" onchange="showAvailableCategoriesEdit(this)" disabled>
						<option value=""></option>
						<?php foreach ($parent_catagories as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>" <?php echo ($parent_category_value->pcat_id==$pcat_id)? "selected":''; ?>><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<?php if(empty($parent_catagories)){
							?>
							<option value="0" selected>--None--</option>
							<?php 
						}else{?>
						<option value="0">--None--</option>
						<?php }
						?>
							
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Category-</label>
					<select name="edit_categories" id="edit_categories" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($categories as $categories_value) {  ?>
						<option value="<?php echo $categories_value->cat_id; ?>" <?php echo ($categories_value->cat_id==$cat_id)? "selected":''; ?>><?php echo $categories_value->cat_name; ?></option>
						<?php } ?>
		
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Sub Category-</label>
					<select name="edit_subcategories" id="edit_subcategories" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($subcategories as $subcategories_value) {  ?>
						<option value="<?php echo $subcategories_value->subcat_id; ?>" <?php echo ($subcategories_value->subcat_id==$subcat_id)? "selected":''; ?>><?php echo $subcategories_value->subcat_name; ?></option>
						<?php } ?>
		
					</select>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Enter Reasons Name</label>
					<input id="edit_reason_name" name="edit_reason_name" type="text" class="form-control" value="<?php echo $get_reason_data["reason_name"];?>"/>
					<input id="edit_reason_name_default" name="edit_reason_name_default" type="hidden" class="form-control" value="<?php echo $get_reason_data["reason_name"];?>"/>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Enter the Sort Order</label>
					
					<select name="edit_menu_sort_order" id="edit_menu_sort_order" class="form-control" onchange="show_view_validatn()">
					<option value="<?php echo $get_reason_data["sort_order"];?>" selected><?php echo $get_reason_data["sort_order"];?></option>
						<?php
							if($get_sort_order_options_for_reason_return["key"]==""){
								if($get_reason_data["sort_order"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php } ?>
								<option value="1">1</option>
								<?php
							}
							else{
								if($get_reason_data["sort_order"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php
								}
								$value_arr=explode(",",$get_sort_order_options_for_reason_return["value"]);
								foreach($value_arr as $k => $v){
									?>
										<option value="<?php echo $v;?>"><?php echo $v;?></option>
									<?php
									}
							}
						?>
					</select>
					<input id="edit_menu_sort_order_default" name="edit_menu_sort_order_default" type="hidden" value="<?php echo $get_reason_data["sort_order"];?>"/>	
				</div>
				</div>
			</div>		
						
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group label-floating">
			<label class="control-label">-Swap Options-</label>
			<select class="form-control" id="show_available_common_reason_return" onchange="changeEventHandler(event)">
			<option selected value=""></option>
			<?php
			
			foreach($subcategory_sort_order_arr as $subcategory_order_arr){
				
				?>
				
					<option value="<?php echo $subcategory_order_arr["reason_id"];?>"><?php echo $subcategory_order_arr["reason_name"];?></option>
					<?php
			}
			?>
			
			</select>
			</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10"> 
					<div class="form-group">
					<div class="col-md-5 col-md-offset-1">Show Reasons</div>
					<div class="col-md-3">
						<label class="radio-inline"><input id="edit_view1" name="edit_view" value="1" type="radio" <?php if($get_reason_data["view"]=="1"){echo "checked";}?>>View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input id="edit_view2" name="edit_view" value="0" type="radio" <?php if($get_reason_data["view"]=="0"){echo "checked";}?>>Hide</label>
					</div>
				
					</div>
				</div>
			</div>		
			<!--<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<select class="form-control" id="show_available_common_reason_return" style="visibility:hidden;">
						<option selected value="">-Select Options-</option>
					</select>
				</div>
			</div>	-->
			<div class="row">
				<div class="col-md-10 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit_edit_reason" disabled="">Submit</button>
		
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
					</div>
				</div>
			</div>										

		</div>		
	</form>
	<form name="search_for_reasons" id="search_for_reasons" method="post">
		<input value="<?php echo $get_reason_data["pcat_id"]; ?>" type="hidden" id="pcat_id" name="pcat_id"/>
		<input value="<?php echo $get_reason_data["cat_id"]; ?>" type="hidden" id="cat_id" name="cat_id"/>
		<input value="<?php echo $get_reason_data["subcat_id"]; ?>" type="hidden" id="subcat_id" name="subcat_id"/>
		<input value="view" type="hidden" name="create_editview">
	</form>
</div>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
<script>
function changeEventHandler(event){
        parent_id=event.target.value;
		if(parent_id!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		}
		if(parent_id==""){
	edit_menu_sort_order=document.getElementById("edit_menu_sort_order").value;
	if(edit_menu_sort_order==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;					
	}
	if(edit_menu_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
		}
}
function show_view_validatn(){
	edit_menu_sort_order=document.getElementById("edit_menu_sort_order").value;
	show_available_common_reason_return=document.getElementById("show_available_common_reason_return").value;
	if(show_available_common_reason_return==""){
	if(edit_menu_sort_order==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	document.getElementById('show_available_common_reason_return').disabled = false;
	}
	if(edit_menu_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		//document.getElementById('show_available_common_reason_return').disabled = false;
	}
	}
	if(show_available_common_reason_return!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
	
}
</script>
<script type="text/javascript">

$(document).ready(function (){
	pcat_id=$('#f_pcat_id').val();
	cat_id=$('#f_cat_id').val();
	subcat_id=$('#f_subcat_id').val();

	if(pcat_id!='' && pcat_id!=null){
		
		$("#pcat_id").val(pcat_id);
		
		$.ajax({
			url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
			type:"post",
			async:false,
			data:"cat_id="+cat_id+"&active=1",
			success:function(data){
				if(data!=0){
					$("#subcat_id").html(data);
					$("#subcat_id").val(subcat_id);
				}
				else{
					$("#subcat_id").html('<option value="">-None-</option>')
					$("#brand_id").html('<option value="">-None-</option>')

				}
			}
		});
		$.ajax({
			url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
			type:"post",
			async:false,
			data:"pcat_id="+pcat_id+"&active=1",
			success:function(data){
				
				if(data!=0){
					$("#cat_id").html(data);
					$("#cat_id").val(cat_id);
				}
				else{
					$("#cat_id").html('<option value="">-None-</option>')
					$("#brand_id").html('<option value="">-None-</option>')

				}
			}
		});
		table.draw();
	}
});


function showAvailableCategoriesEdit(obj)
{
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#edit_categories").html(data);
					}
					else{
						$("#edit_categories").html('<option value="">-None-</option>')
						$("#reasons").html('<option value="">-None-</option>')

					}
				}
			});
	}
}

$(document).ready(function(){
var button = $('#submit_edit_reason');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_reason'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_reason"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_reason'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});


});

function form_validation_edit()
{

	var reason_name = $('input[name="edit_reason_name"]').val().trim();
	var parent_category = $('select[name="edit_parent_category"]').val().trim();
	var categories = $('select[name="edit_categories"]').val().trim();
	var subcategories = $('select[name="edit_subcategories"]').val().trim();
	var menu_sort_order = $('select[name="edit_menu_sort_order"]').val().trim();
	var view = $('input[name="edit_view"]').val();
	var common_reason_return="";
			if($("#show_available_common_reason_return").length!=0){
				var common_reason_return=document.getElementById("show_available_common_reason_return").value;
			}
	   var err = 0;
		if((reason_name=='') || (parent_category=='') || (categories=='') || (subcategories=='') || (menu_sort_order=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
				document.getElementById("edit_parent_category").disabled="";
				document.getElementById("edit_categories").disabled="";
				document.getElementById("edit_subcategories").disabled="";
		var form_status = $('<div class="form_status"></div>');		
		var form = $('#edit_reason');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();		
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_reason/"?>',
				type: 'POST',
				data: $('#edit_reason').serialize()+"&common_reason_return="+common_reason_return,
				dataType: 'html',
				
			}).done(function(data)
			  {
				form_status.html('') 
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					edit_reason_name_default=document.getElementById("edit_reason_name_default").value;
					document.getElementById("edit_reason_name").value=edit_reason_name_default;
					document.getElementById("edit_reason_name").focus();
					
					document.getElementById("edit_parent_category").disabled=true;
					document.getElementById("edit_categories").disabled=true;
					document.getElementById("edit_subcategories").disabled=true;
					
				});
				  }
				if(data==1)
				{
					swal({
						title:"Success", 
						text:"Reason Return is successfully updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
					
				}
				if(data==0)
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)	
				}
				
			});
}
}]);			
			}
	
	return false;
}

</script>
</div>
</body>

</html>