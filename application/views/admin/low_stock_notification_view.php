<html lang="en">
<head>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>

<style type="text/css">
.align_top{
	margin-top: 20px;
}
.align_bottom{
    margin-bottom: 20px;
}
</style>
<script type="text/javascript">
var table;
$(document).ready(function () {
          
	table = $('#low_stock_notification').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Orders/low_stock_notification_processing",
			data: function (d) {d.status = 1;},
            type: "post",
            error: function(){
              $("#table_low_stock_notification_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				
				document.getElementById("low_notification_count").innerHTML=json.recordsFiltered;
				
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
	});

});
	   

	
</script>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
	<div id="parent_button" class="page-header">
	<h4 class="text-center">Total count <span class="badge"><div id="low_notification_count"></div></span>
	</h4>
	</div>


	<div class="row" id="viewDiv1">
	<table id="low_stock_notification" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>

				<tr>
					<th class="text-primary small bold">Sl.No.</th>
					<th class="text-primary small bold">SKU details</th>
					<th class="text-primary small bold">Parent level details</th>
					<th class="text-primary small bold">Available Stock</th>
					<th class="text-primary small bold">low Stock</th>
					<th class="text-primary small bold">Cut OFF Stock</th>
					<th class="text-primary small bold">Action</th>
					
				</tr>
		</thead>

	</table>
	</div>

</div>

<div class="modal" id="update_inventory_stock_modal">
     <div class="modal-dialog modal-sm">
        <div class="modal-content">
           <div class="modal-header">
              <h4 class="modal-title">Update Inventory stock ( SKU ID - <span id="SKUID"></span>)</h4>
           </div>
           <div  class="modal-body" id="model_content">

			<form name="update_inventory_stock" id="update_inventory_stock" class="form-horizontal">
					<input type="hidden" name="inventory_id" id="inventory_id">
					<input type="hidden" name="SKUID_in_update_stock" id="SKUID_in_update_stock">
					
					<div class="form-group">
						<div class="col-sm-12">
							<label for="stock" >
								
							Enter stock to be updated
							</label>
							<input type="text" name="stock" id="stock" class="form-control">
						</div>
					</div>
					<br>
					<div class="form-group">
						<div class="col-sm-12">
							<button class="btn btn-success btn-xs btn-block" type="submit" id="submit_update_stock">Submit</button>
						</div>
					</div>
						
			</form>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close"class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
  </div>
</div>
</body>
</html>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#update_inventory_stock_modal").on('hidden.bs.modal', function () {
		$("#update_inventory_stock").trigger("reset");	
		table.draw();
    });
	
	$("#update_inventory_stock").on('submit',(function(e) {
	e.preventDefault();	
	
	var stock=$("input[name=stock]").val();	
	var check_numeric=$.isNumeric(stock);	
	
		if(check_numeric==false){
			return false;
		}
	$("#submit_update_stock").html("Processing <i class='fa fa-refresh fa-spin'></i>");
		$.ajax({
				url:"<?php echo base_url();?>admin/Orders/update_inventory_stock",
				type: "POST",    
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){	
				$("#submit_update_stock").html("Submit");
					if(data){
						//alert("Successfully Updated");	
						
						swal({
							title:"Updated!", 
							text:"Updated", 
							type: "success",
							allowOutsideClick: false
						}).then(function () {
							//table.draw();
							location.reload();
						});
						
						
						$("#update_inventory_stock_modal").modal("hide");
						table.draw();
					}else{
						alert("not updated");
					}
				}	        
		   });
		
		
	}));
});
function update_stock_of_inventory(id,sku_id,stock){
	$("#SKUID").html(sku_id);
	$("#SKUID_in_update_stock").val(sku_id);
	$("#inventory_id").val(id);
	$("#stock").val(stock);
	$("#update_inventory_stock_modal").modal("show");
}


</script>
