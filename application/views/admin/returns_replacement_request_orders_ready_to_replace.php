<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Replacement Request Orders - Ready To Replace</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<style>
.form-group{
	margin-bottom:5px;
	margin-top:0;
	padding-bottom:0;
}
</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript">
var table;
$(document).ready(function (){
   // Array holding selected row IDs
    var rows_selected = [];
    table = $('#returns_replacement_request_orders_table_ready_to_replace').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Returns/returns_replacement_request_orders_ready_to_replace_processing", // json datasource
			data: function (d) { d.status_of_replaced_order = $('#status_of_replaced_order').val(); },
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#returns_replacement_request_orders_table_ready_to_replace_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("replacement_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'40%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});	
$(document).ready(function(){
	var returns_refund_orders_list_checked=0;
	$("input[name='returns_refund_orders_list']").each(function(){
		if($(this).is(":checked")){
			returns_refund_orders_list_checked++;
		}
	})
	/*if(returns_refund_orders_list_checked==0){
		document.getElementById("refund_info_div").style.display="none";
	}*/
});

function draw_table(){
	table.draw();
}


function sendReplacementOrderFun(order_item_id){
	document.getElementById("type_of_order_div_"+order_item_id).style.display="block";
}
function sendForReplacementFun(order_item_id){
	var type_of_order=document.getElementById("type_of_order_"+order_item_id).value;
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/send_order_for_replacement",
		type:"POST",
		data:"type_of_order="+type_of_order+"&order_item_id="+order_item_id,
		success:function(data){

		//alert(data);
		//return false;
					if(data){
						
						swal({
							title:"success!", 
							text:"Successfully done", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
			
		}
	})
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="page-header"><h4 class="text-center">Replacement Request Orders - Ready To Replace <span class="badge" id="replacement_count"></span></h4></div>
	<table id="returns_replacement_request_orders_table_ready_to_replace" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
				<tr>
				<th colspan="3">
				<div class="row">
				<div class="col-md-4 col-md-offset-4">
			<select class="form-control" name="status_of_replaced_order" id="status_of_replaced_order" onchange="draw_table()">
				<option value="ready_to_replace">Ready to replace</option>
				<option value="processed_orders">Processed orders</option>
			</select>
				</div>
				</div>
				</th>
			</tr>
			<tr>
				<th class="text-primary small bold">Return Summary</th>
				<th class="text-primary small bold">Return Reason</th>
				<th class="text-primary small bold">Actions</th>
			</tr>
		</thead>
	</table>
</div>

<div class="modal" id="order_summary">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Order Summary</h4>
           </div>
           <div  class="modal-body" id="model_content">
		   <div class="container">
				
			</div>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close"class="btn">Close</a>
        </div>
     </div>
    </div>
  </div>
  
 



<!------------------------------------->
  
  
  <script type="text/javascript">
  $("#order_summary").modal("hide");
  function view_order_summary_(order_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/get_data_from_invoice_offers",
		type:"POST",
		data:"order_id="+order_id,
		success:function(data){
			
			$("#description").val("");	
			$("#model_content").html(data);
			$("#order_summary").modal("show");
			
		}
	})
	
}

  </script>
  </div>
</body> 
</html>  