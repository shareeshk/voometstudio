<html lang="en">
<head>	
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />


<style type="text/css">
	.nav-pills > li.active > a{
		width: fit-content;
	}
.cursor-pointer {
    cursor: pointer !important;
}
.small_color_box{
	width: 30px;
	height: 30px;
	border: 1px solid #ccc;
	margin-top: 1px;
}

.loading {
	text-align: center; 
	margin:0;product
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
#wizardPictureSmall_picname{
	word-wrap: break-word;
}
#wizardPictureBig_picname{
	word-wrap: break-word;
}
#wizardPictureSmall_picname2{
	word-wrap: break-word;
}
#wizardPictureBig_picname2{
	word-wrap: break-word;
}
#wizardPictureSmall_picname3{
	word-wrap: break-word;
}
#wizardPictureBig_picname3{
	word-wrap: break-word;
}
#wizardPictureSmall_picname4{
	word-wrap: break-word;
}
#wizardPictureBig_picname4{
	word-wrap: break-word;
}
#wizardPictureSmall_picname5{
	word-wrap: break-word;
}
#wizardPictureBig_picname5{
	word-wrap: break-word;
}
#wizardPictureSmall_picname6{
	word-wrap: break-word;
}
#wizardPictureBig_picname6{
	word-wrap: break-word;
}
textarea.form-control {
    height:36px;
}
.CamListDiv {
    height: 450px;
    float: left;
    overflow: scroll;
    overflow-x:hidden;
}
.well{
	width:100%;
}
</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 

<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
<script type="text/javascript">
var table;

$(document).ready(function (){
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    

$('#back').on('click',function(){	
		var form = document.getElementById('back_to_inventory');
		form.action='<?php echo base_url(); ?>admin/Catalogue/inventory_filter';
		form.submit();
	});
});		
</script>
<script>

function changeViewPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/inventory_filter';
}
</script>
<script type="text/javascript">

function filter_details_form_valid()
{
	
	
	var form_status = $('<div class="form_status"></div>');		
	var form = $('#create_inventory');	
	var err = 0;
		var image = $('input[name="image1"]').val().trim();
		var thumbnail = $('input[name="thumbnail1"]').val().trim();
		if(image=='')
		{
		   $('div[class="picture"]').css("border", "2px solid #f44336");	   
		   err = 1;
		}
		else{
			$('div[class="picture"]').css({"border": "2px solid #ccc"});
		}
		if(thumbnail=='')
		{
		   $('div[class="picture"]').css("border", "2px solid #f44336");	   
		   err = 1;
		}
		else{
			$('div[class="picture"]').css({"border": "2px solid #ccc"});
		}
		subcat_id=<?php echo $subcat_id;?>;
		

	//form1=additional_details_form_valid();
	//form2=specification_details_form_valid();
	//form3=additional_details_form_valid();
	
	if(err==1)
	{
		//$('#validation_error').show();
		return false;
		
	}else{
			
			var data = $("#highlight").Editor("getText");
			$("#highlight").val(data);
			
			
			var form = $('#create_inventory');	
			var form_create = document.forms.namedItem("create_inventory");
			var ajaxData = new FormData(form_create);
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {

				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_inventory/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false, 		
				
			}).done(function(data)
			  {	
				if(data==true)
				{
					  swal({
						title:"Success", 
						text:"Inventory is successfully Created!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				else
				{
					swal(
						'Oops...',
						data,
						'error'
					)	
				}
		return true;
	});
	}
}]);
				
		return true;
	}
}

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}
function editInventoryFun(inventory_id,product_id){
	location.href="<?php echo base_url()?>admin/Catalogue/edit_inventory_form/"+inventory_id+"/"+product_id;

}
function view_delivered_orders_by_inventory_id(inventory_id){
	location.href="<?php echo base_url()?>admin/Orders/delivered_orders_inventory_wise/"+inventory_id;
}
$(document).ready(function(){
	<?php
		if($create_editview=="create"){
			?>
			
			$("#createDiv2").css({"display":""});
			<?php
		}
		
	?>
});
</script>		
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">

<div class="row" id="createDiv2" style="">
<div class="col-md-12">
	<div class="wizard-container">
	<div class="card wizard-card" data-color="green" id="wizardProfile">
	<form name="create_inventory" id="create_inventory" enctype="multipart/form-data">

		<div class="wizard-header">

		<h4 class="text-left col-md-12">
			<small>
		<?php 
		
		//print_r( $inventory_data_arr);

		echo $cat_structure["pcat_name"].' > '.$cat_structure["cat_name"].' > '.$cat_structure["subcat_name"].' > '.$cat_structure["brand_name"];

		$inventory_type_txt='';
		
		if($inventory_type=='1'){
			$inventory_type_txt='Only as Main Inventory';
		}elseif($inventory_type=='2'){
			$inventory_type_txt='Only as Addon Inventory';

		}elseif($inventory_type=='3'){
			$inventory_type_txt='Both as Main & as Addon';
		}
		
		?>
		</small>
		</h4>

		<h3 class="wizard-title">Create Inventory of  <?php echo $product_details["product_name"]; ?>
	
		<?php if($inventory_type!=''){
			echo " <br> <span><small class='text-info'>".$inventory_type_txt.'</small></span>';
		}
		?>
	</h3>
		</div>    
		

	<div class="wizard-navigation">
		<ul>
			<li><a href="#step-11" data-toggle="tab">General</a></li>
			<li><a href="#step-71" data-toggle="tab">Highlights</a></li>
			<li><a href="#step-61" data-toggle="tab">Images</a></li>
		</ul>
	</div>

	<input name="product_id" type="hidden" id="product_id" value="<?php echo $product_id;?>">
	<input name="inventory_type" type="hidden" id="inventory_type" value="<?php echo $inventory_type;?>">

	<div class="tab-content">
	
	<div class="tab-pane" id="step-11">
		<div class="row">
			<div class="col-md-12">
			    <h4 class="info-text"> Product and Stock Information</h4>
			</div>
		</div>
		
		
			<div class="row">
				<div class="col-md-5 col-md-offset-1"> 
					<div class="form-group label-floating">
						<label class="control-label">Enter Display Name on Button 1</label>
						<input name="sku_display_name_on_button_1" type="text" class="form-control" />
					</div>
				</div>
				<div class="col-md-5"> 	
				
					<div class="form-group label-floating">
						<label class="control-label">Enter Display Name on Button 2</label>
						<input name="sku_display_name_on_button_2" type="text" class="form-control" value="Pick Me!"/>
					</div>
				</div>
			</div>
		
		
		
                <div class="row">

                        <div class="col-md-10 col-md-offset-1">  
                        <div class="form-group label-floating">
                                <label class="control-label">Seller SKU</label>
                                <input name="sku_id" type="text" class="form-control" />
                        </div>
                        </div>
                </div>
		<div class="row">
			<div class="col-md-10 col-md-offset-1"> 
                            <div class="form-group label-floating">
                                    <label class="control-label">SKU Name </label>
                                    <input name="sku_name" id="sku_name" type="text" class="form-control" />
                            </div>
			</div>
		</div>

		
  
		<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Product Status-</label>
					<select name="product_status_view" id="product_status_view" class="form-control search_select">
						<option value=""></option>
						<option value="1">Active</option>
						<option value="0">Inactive</option>
					</select>
				</div>
				</div>
				
		</div>
            <div class="row" style="display:none;">
		
                    
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">Company Cost Price (<?php echo curr_sym; ?>) </label>
					<input name="purchased_price" id="purchased_price" onKeyPress="return isNumber(event)" type="text" class="form-control" />
				</div>
			</div>
			
			
			<div class="col-md-5"> 
				<div class="form-group label-floating">
					<label class="control-label">Company Base Price (<?php echo curr_sym; ?>) </label>
					<input name="base_price" id="base_price" onKeyPress="return isNumber(event)" type="text" class="form-control"  />
				</div>
			</div>
			
			
		</div>
		
		<div class="row">
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group label-floating" id="max_selling_price_div">
					<label class="control-label">MRP (<?php echo curr_sym;?>) {Including tax}</label>
					<input name="max_selling_price" id="max_selling_price" onKeyPress="return isNumber(event)" type="text" class="form-control" onkeyup="calculateTax_priceFun()"/>
				</div>
			</div>
			<div class="col-md-5"> 
			<div class="form-group label-floating">
                                        <label class="control-label" id="selling_price_div">Web Selling Price (<?php echo curr_sym;?>) {WSP - with any discount applied}
                                        </label>
                                        <input name="selling_price" id="selling_price" type="text" class="form-control" onKeyPress="return isNumber(event)" onkeyup="calculateSelling_priceDiscountFun();calculateTax_priceFun();"/>
                                </div>
			</div>
		</div>
           
                <!--added newly---->
                <div class="row">

                        <div class="col-md-5 col-md-offset-1"> 
                                <div class="form-group label-floating">
                                        <label class="control-label" id="cost_price_div">Cost Price (<?php echo curr_sym;?>) {the cost of the product to the company}
                                        </label>
                                        <input name="cost_price" id="cost_price" type="text" class="form-control" onKeyPress="return isNumber(event)"/>
                                </div>

                        </div>

                        <div class="col-md-5"> 
                                <div class="form-group label-floating" id="selling_discount_div"> 
                                        <label class="control-label">
                                        Discount Percent {% of WSP from MRP}</label>
                                        <input name="selling_discount" id="selling_discount" type="text" class="form-control" readOnly  />
                                </div>
                        </div>
                </div>
                
                
                

                <!--added newly---->


		


		<div class="row">
			
			
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group">
					<label class="control-label">Quantity
					</label>
					<input name="mrp_quantity" id="mrp_quantity" type="text" class="form-control"   value=""/>
				</div>
				
			</div>
		</div>

	
		<div class="row">
			
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">-Select Stock Status-</label>
					<select name="product_status" id="product_status" class="form-control search_select">
						<option value="" selected></option>
						<option value="In Stock" selected>Ready In Stock</option>
						<option value="Made To Order">Made To Order</option>
						<option value="Out Of Stock">Out Of Stock</option>
						<option value="Discontinued">Discontinued</option>
					</select>
				</div>
			</div>

			<div class="col-md-5 "> 
				<div class="form-group label-floating">	
					<label class="control-label">Stock count</label>
					<input class="form-control" name="stock" id="stock" type="number" >
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-md-5 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">Low Stock Count (Warning Qty)</label>    
                                        <input class="form-control" name="low_stock" type="number" onkeyup="stock_count_validation();" id="low_stock" value="100">
				</div>
			</div>
			<div class="col-md-5"> 
				<div class="form-group label-floating">
					<label class="control-label">Cutoff Stock Count</label>     
                                        <input class="form-control" name="cutoff_stock" id="cutoff_stock" type="number"  value="25">
				</div>
			</div>
		</div>
		
		<script>
		function stock_count_validation(){
			low_stock=parseInt($('#low_stock').val());
			cutoff_stock=parseInt($('#cutoff_stock').val());
			if(low_stock!='' && cutoff_stock!=''){
				if(low_stock<cutoff_stock){
					alert('Low stock count should be greater than cutoff stock');
					$('#cutoff_stock').val('');
				}
			}	
		}
		</script>
		
		
	</div>
	
	<div class="tab-pane" id="step-71">
	
		<div class="row">

		<div class="col-md-6 col-md-offset-3">

		<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        
                        Product Details
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                      
					  <div class="row">
							<div class="col-md-12"> 	
									<textarea class="form-control" name="highlight" id="highlight"></textarea>
							</div>
						</div>
						
						
                </div>
            </div>
        </div>
        </div>

		</div><!---col-md-3--->
		</div><!--row-->
		
		
		
		
		<div class="row">
				<div class="col-md-6 col-md-offset-3"> 
				<div class="form-group label-floating">
					<label class="control-label">Enter Consignment Actual Weight in kg</label>
					<input class="form-control" name="inventory_addon_weight_in_kg" id="inventory_addon_weight_in_kg" type="number" step="any" required>
				</div>
				</div>
			</div>
		
		
		<!-- About the brand starts --->
		
		
	</div>


	
	<script>
	
	var typingTimer;                //timer identifier
	var doneTypingInterval = 5000;  //time in ms, 5 second for example
	var $input = $('#tax');

	//on keyup, start the countdown
	

	//on keydown, clear the countdown 
	$input.on('keydown', function () {
	  clearTimeout(typingTimer);
	});

	//user is "finished typing," do something
	
	
	
	</script>
	
	
	<div class="tab-pane" id="step-61">
		
		<!---common--image--->
		<div class="row">
			<div class="col-md-5 col-md-offset-3" id="inv_images_div">
			<div class="col-md-12 text-center"><b>Common (300x366)</b></div>
				<div class="col-md-12">
				<div class="picture-container">
					<div class="picture" id="commonPic">
						<img class="picture-src" id="wizardPictureCommon" title=""/>
				
						<input type="file" name="common_image" id="wizard-picture-Common"/>
					</div>
				<h6 id="wizardPictureCommon_picname">Common Image</h6>
				</div>
				</div>	
			</div>
		</div>
		<!---common--image--->
		
		<div class="row">
			<div class="col-md-2 col-md-offset-1">
			<button name="add_image_inventory" id="add_image_inventory_btn" type="button" class="btn btn-success" disabled onclick="add_image_inventory_fun()">Add Pair</button>
			</div>
	
		</div>
		<div class="row">
		
		
			<!---- first pair starts --->
			<div class="col-md-5 col-md-offset-2" id="inv_images_div_1">
			<div class="col-md-10 col-md-offset-1"><b>FIRST PAIR</b></div>
			
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic1">
						<img class="picture-src" id="wizardPictureSmall1" title=""/>
				
						<input type="file" name="thumbnail1" id="wizard-picture-small1"onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname1">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic1">
						<img class="picture-src" id="wizardPictureBig1" title=""/>
				
						<input type="file" name="image1" id="wizard-picture-big1" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname1">Medium Image (420x512)</h6>
				</div>
				</div>
				
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic1">
						<img class="picture-src" id="wizardPictureLarge1" title=""/>
				
						<input type="file" name="largeimage1" id="wizard-picture-large1" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname1">Big Image (850x1036)</h6>
				</div>
				</div>
				
				
			</div>
			<!---- first pair ends --->
			<!---- second pair starts --->
			<div class="col-md-5"  id="inv_images_div_2">
			<div class="col-md-10 col-md-offset-1"><b>SECOND PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(2)"></i></b></div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic2">
						<img class="picture-src" id="wizardPictureSmall2" title=""/>
				
						<input type="file" name="thumbnail2" id="wizard-picture-small2" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname2">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic2">
						<img class="picture-src" id="wizardPictureBig2" title=""/>
				
						<input type="file" name="image2" id="wizard-picture-big2" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname2">Medium Image (420x512)</h6>
				</div>
				</div>
				
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic2">
						<img class="picture-src" id="wizardPictureLarge2" title=""/>
				
						<input type="file" name="largeimage2" id="wizard-picture-large2" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname2">Big Image (850x1036)</h6>
				</div>
				</div>
				
				
			</div>
			<!---- second pair ends --->
		</div>
		<div class="row">
			<!---- third pair starts --->
			<div class="col-md-5 col-md-offset-2" id="inv_images_div_3">
			<div class="col-md-10 col-md-offset-1"><b>THIRD PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(3)"></i></b></div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic3">
						<img class="picture-src" id="wizardPictureSmall3" title=""/>
				
						<input type="file" name="thumbnail3" id="wizard-picture-small3" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname3">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic3">
						<img class="picture-src" id="wizardPictureBig3" title=""/>
				
						<input type="file" name="image3" id="wizard-picture-big3" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname3">Medium Image (420x512)</h6>
				</div>
				</div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic3">
						<img class="picture-src" id="wizardPictureLarge3" title=""/>
				
						<input type="file" name="largeimage3" id="wizard-picture-large3" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname3">Big Image (850x1036)</h6>
				</div>
				</div>
			</div>
			<!---- third pair ends --->
			<!---- fourth pair starts --->
			<div class="col-md-5" id="inv_images_div_4">
			<div class="col-md-10 col-md-offset-1"><b>FOURTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;"  onclick="hide_inv_imagesFun(4)"></i></b></div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic4">
						<img class="picture-src" id="wizardPictureSmall4" title=""/>
				
						<input type="file" name="thumbnail4" id="wizard-picture-small4" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname4">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic4">
						<img class="picture-src" id="wizardPictureBig4" title=""/>
				
						<input type="file" name="image4" id="wizard-picture-big4" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname4">Medium Image (420x512)</h6>
				</div>
				</div>
				
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic4">
						<img class="picture-src" id="wizardPictureLarge4" title=""/>
				
						<input type="file" name="largeimage4" id="wizard-picture-large4" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname4">Big Image (850x1036)</h6>
				</div>
				</div>
				
			</div>
			<!---- fourth pair ends --->
			<div class="row">
			<!---- fifth pair starts --->
			<div class="col-md-5 col-md-offset-2" id="inv_images_div_5">
			<div class="col-md-10 col-md-offset-1"><b>FIFTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(5)"></i></b></div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic5">
						<img class="picture-src" id="wizardPictureSmall5" title=""/>
				
						<input type="file" name="thumbnail5" id="wizard-picture-small5" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname5">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic5">
						<img class="picture-src" id="wizardPictureBig5" title=""/>
				
						<input type="file" name="image5" id="wizard-picture-big5" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname5">Medium Image (420x512)</h6>
				</div>
				</div>
				
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic5">
						<img class="picture-src" id="wizardPictureLarge5" title=""/>
				
						<input type="file" name="largeimage5" id="wizard-picture-large5" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname5">Big Image (850x1036)</h6>
				</div>
				</div>
				
			</div>
			<!---- fifth pair ends --->
			
			
			
			<!---- sixth pair starts --->
			<div class="col-md-5" id="inv_images_div_6">
			<div class="col-md-10 col-md-offset-1"><b>SIXTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(6)"></i></b></div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic6">
						<img class="picture-src" id="wizardPictureSmall6" title=""/>
				
						<input type="file" name="thumbnail6" id="wizard-picture-small6" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname6">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic6">
						<img class="picture-src" id="wizardPictureBig6" title=""/>
				
						<input type="file" name="image6" id="wizard-picture-big6" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname6">Medium Image (420x512)</h6>
				</div>
				</div>
				
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic6">
						<img class="picture-src" id="wizardPictureLarge6" title=""/>
				
						<input type="file" name="largeimage6" id="wizard-picture-large6" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname6">Big Image (850x1036)</h6>
				</div>
				</div>
				
			</div>
			<!---- sixth pair ends --->
			
			
			
			</div>
		</div>
	</div>	 
	
	<script>
		function hide_inv_imagesFun(div_index_id){
			
			document.getElementById("wizard-picture-small"+div_index_id).value="";
			document.getElementById("wizard-picture-big"+div_index_id).value="";
			document.getElementById("wizard-picture-large"+div_index_id).value="";
			
			document.getElementById("wizardPictureSmall"+div_index_id).removeAttribute("src");
			document.getElementById("wizardPictureBig"+div_index_id).removeAttribute("src");
			document.getElementById("wizardPictureLarge"+div_index_id).removeAttribute("src");
			
			document.getElementById("wizardPictureSmall_picname"+div_index_id).innerHTML="SMALL IMAGE (100x122)";
			document.getElementById("wizardPictureBig_picname"+div_index_id).innerHTML="Medium IMAGE";
			document.getElementById("wizardPictureLarge_picname"+div_index_id).innerHTML="Big IMAGE";
			
			document.getElementById("inv_images_div_"+div_index_id).style.display="none";
			enableDisabledAddInventoryButtonFun();
		}
		function enableDisabledAddInventoryButtonFun(){
			small_image_count=0;
			big_image_count=0;
			large_image_count=0;
			for(i=1;i<=6;i++){
				if(document.getElementById("inv_images_div_"+i).style.display==""){
					if(document.getElementById("wizard-picture-small"+i).value!=""){
						small_image_count++;
					}
					if(document.getElementById("wizard-picture-big"+i).value!=""){
						big_image_count++;
					}
					if(document.getElementById("wizard-picture-large"+i).value!=""){
						large_image_count++;
					}
				}
			}
			if(small_image_count==0 && big_image_count==0 && large_image_count==0){
				document.getElementById("add_image_inventory_btn").disabled=true;
			}
			else{
				if((small_image_count==big_image_count) && (big_image_count==large_image_count)){
					document.getElementById("add_image_inventory_btn").disabled=false;
					document.getElementById("add_inventory_finish_btn").disabled=false;
				}
				else{
					document.getElementById("add_image_inventory_btn").disabled=true;
					document.getElementById("add_inventory_finish_btn").disabled=true;
					
				}
			}
		}
		function initializeInventoryImageDivFun(){
			for(i=2;i<=6;i++){
				document.getElementById("inv_images_div_"+i).style.display="none";
			}
		}
		function add_image_inventory_fun(){
			for(i=2;i<=6;i++){
				if(document.getElementById("inv_images_div_"+i).style.display=="none"){
					document.getElementById("inv_images_div_"+i).style.display="";
					document.getElementById("add_image_inventory_btn").disabled=true;
					break;
				}
			}
		}
		initializeInventoryImageDivFun();
		enableDisabledAddInventoryButtonFun();
	</script>
	
	</div>		                        
	<div class="wizard-footer">
		<div class="pull-right">
			<input type='button' class='btn btn-next btn-fill btn-success btn-wd' name='next' value='Next' onclick="enterCurrentTabFun()"/>
			<input type='button' class='btn btn-finish btn-fill btn-success btn-wd' name='finish' id="add_inventory_finish_btn" value='Finish' onClick="filter_details_form_valid()" />
		</div>

		<div class="pull-left">
			<input type='button' class='btn btn-previous btn-fill btn-default btn-wd' id="previous_button" name='previous' value='Previous' />
			<input type='button' class='btn btn-previouspage btn-fill btn-default btn-wd' id="back" value='Previous' />
		</div>
		<div class="clearfix"></div>
	</div>
	</form>
		
		
	</div>
	</div> <!-- wizard container -->
</div>
</div>
<div class="footer">

</div>
</div>

<form name="back_to_inventory" id="back_to_inventory" method="post">
		<input type="hidden" value="<?php echo $pcat_id; ?>" name="parent_category">
		<input type="hidden" value="<?php echo $cat_id; ?>" name="categories">
		<input type="hidden" value="<?php echo $subcat_id; ?>" name="subcategories">
		<input type="hidden" value="<?php echo $brand_id; ?>" name="brands">
		<input type="hidden" value="<?php echo $product_id; ?>" name="products">
		
</form>
</div>

<div class="modal" id="status_modal">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Update Inventory Status</h4>
           </div>
           <div  class="modal-body" id="model_content">
				<form method="post" id="request_form" name="request_form">
		  			<input type="hidden" name="app_id" id="app_id" value="">
		  			<input type="hidden" name="inventory_id" id="inventory_id" value="">
		  			

					<div class="row">

					<div class="col-md-11 col-md-offset-1"> 
							<div class="form-group">
								<label class="control-label">Status
								</label>
								<select name="status" id="status" type="text" class="form-control" required onchange="show_rejected_comments()"/>
								<option value=""> - Select status - </option>
								<option value="1">Publish</option>
								<option value="2">Unpublish</option>
								<option value="3">Reject</option>
								</select>
							</div>
						</div>
  
						<div class="col-md-11 col-md-offset-1" id="show_rejected_comments_div" style="display:none;" > 	
							<div class="form-group">
							<label class="control-label">Rejections Comments if any 
							</label>
							<textarea name="comments" id="comments" class="form-control"></textarea>
							</div>
						</div>

						<div class="col-md-11 col-md-offset-1"> 	
							<div class="form-group text-center">
							
							<button class="btn btn-info btn-sm" type="button" onclick="change_status()" id="submit-data"></i>Submit</button>
							</div>
						</div>
					</div>



				</form>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
</div>

<script  type="text/javascript" src="<?php echo base_url();?>assets/richte/editor.js" ></script>
<script>
$(document).ready(function() {
	
	var textarea = $("#highlight");
	textarea.Editor();
	
 });
 
 /*function toggle_highlight(){
	 $('#highlight_div').toggle();
 }*/
 function enterCurrentTabFun(){
	$(".tab-content .tab-pane.active").each(function(){
		if($(this).attr("id")=="step-21"){
			vendor_id=$("#vendors").val();
			if($("#logistics_parcel_category").val()!=null){
				logistics_parcel_category_in=$("#logistics_parcel_category").val().join("-");
				
				$.ajax({
					url:"<?php echo base_url()?>admin/Catalogue/check_selection_in_all_parcel_categories",
					type:"post",
					data:"vendor_id="+vendor_id+"&logistics_parcel_category_in="+logistics_parcel_category_in,
					success:function(data){
						if(data=="no"){
							swal({
								title:"Info", 
								text:"Please select atleast one parcel category from logisitcs", 
								type: "info"
						}).then(function () {
							
						});
						$("#previous_button").click();
						}
						else{
							
						}
					}
				});
			}
	
		}	
		
		
		
	});
 }
 
 function calculateSelling_priceDiscountFun(){
 
        selling_price=$("#selling_price").val();
        max_selling_price=$("#max_selling_price").val();
        
        if(selling_price=="" || max_selling_price==""){
            $("#selling_discount").val("");
	}
        if(selling_price!="" && max_selling_price!=""){
            if(parseFloat(max_selling_price)>=parseFloat(selling_price)){
                wsp_dis=Math.round(((parseFloat(max_selling_price)-parseFloat(selling_price))/parseFloat(max_selling_price))*100).toFixed(2);
                $("#selling_discount").val(wsp_dis);
                $("#selling_discount_div").removeAttr("class");
		$("#selling_discount_div").attr("class","form-group label-floating is-focused");
                
            }else{
                swal({
                        title:"Error", 
                        text:"MRP should be greater or equal to WSP", 
                        type: "error",
                        allowOutsideClick: false
                }).then(function () {
                        $("#selling_discount").val(""); 
                });
               
            }
            //alert(wsp_dis);
	}
 }
  function calculateTax_priceFun(){
	 
        tax=$("#tax").val();
        msp=$("#max_selling_price").val();
        selling_price=$("#selling_price").val();
        
        if(selling_price!="" && tax!=""){
            
            //taxable_price=(parseFloat(msp)-parseFloat(tax_percent_price));
            taxable_price=(parseFloat(selling_price)/(1+parseFloat(tax)/100)).toFixed(2);
            tax_percent_price=(parseFloat(selling_price)-parseFloat(taxable_price)).toFixed(2);
            //alert(taxable_price);
            $("#tax_percent_price").val(tax_percent_price);
            $("#taxable_price").val(taxable_price);
            
            $("#tax_percent_price_div").removeAttr("class");
            $("#tax_percent_price_div").attr("class","form-group label-floating is-empty is-focused");
            $("#taxable_price_div").removeAttr("class");
            $("#taxable_price_div").attr("class","form-group label-floating is-empty is-focused");
        }
         /* calculate tax price and taxable vale */
 }

 /* Export inventory */

function export_inventory(table){

    $.ajax({
        url:"<?php echo base_url()?>admin/Catalogue/export_inventory",
        type:"post",
        data:"table="+table+"&active=0",
        success:function(data){
            if(data==true){

            } else{
                swal("Error", "Error in exporting inventories", "error");
            }
        }
    });
}
 /* Export inventory */
 /* Import inventory */
function formToggle(ID){
    var element = document.getElementById(ID);
    if(element.style.display === "none"){
        element.style.display = "block";
    }else{
        element.style.display = "none";
    }
}


 </script>
 
 
 <script>
	function calculateTaxPercentFun(){
		var CGST_value=$("#CGST").val().trim();
		var IGST_value=$("#IGST").val().trim();
		var SGST_value=$("#SGST").val().trim();
		
		if(CGST_value==""){CGST_value=0;}
		if(IGST_value==""){IGST_value=0;}
		if(SGST_value==""){SGST_value=0;}
		
		$("#tax").val(parseFloat(CGST_value)+parseFloat(IGST_value)+parseFloat(SGST_value));
		calculateTax_priceFun();
	}

	function change_status(app_id,inventory_id,status){
    
    if(app_id!='' && status!=''){
        
        swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
        $.ajax({
            url:"<?php echo base_url()?>admin/Catalogue/inventory_change_status",
            type:"POST",
            data:"vendor_id="+app_id+"&inventory_id="+inventory_id+"&status="+status,
            dataType: "JSON",                
            asyc:false,
            success:function(data){
                if (parseInt(data.flag) == 1) {
                    swal(
                        data.message,
                        '',
                        'info'
                    ) 
                    table.draw();
                } 
            }
        });
        
         }
}]);
    }
    
}

$(document).ready(function (){
	$('#price_validity').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		weekStart: 0, 
		format: 'YYYY-MM-DD', 
		shortTime : true
	}).on('change', function(e, date){
		$('#price_validity').bootstrapMaterialDatePicker('setMinDate', date);
	});
});

function view_status_modal(app_id,inventory_id,status){
	$("#app_id").val(app_id);
	$("#inventory_id").val(inventory_id);
	$("#status").val(status);
	$("#status_modal").modal("show");
}

function change_status(){

	app_id=$("#app_id").val();
	inventory_id=$("#inventory_id").val();
	status=$("#status").val();
	comments=$("#comments").val();

    if(app_id!='' && status!=''){
        
        swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
        $.ajax({
            url:"<?php echo base_url()?>admin/Catalogue/inventory_change_status",
            type:"POST",
            data:"vendor_id="+app_id+"&inventory_id="+inventory_id+"&status="+status+"&comments="+comments,
            dataType: "JSON",                
            asyc:false,
            success:function(data){
                if (parseInt(data.flag) == 1) {
                    swal(
                        data.message,
                        '',
                        'info'
                    ) 
                    drawtable();
                } 
            }
        });
        
         }
}]);
    }
    
}

function show_rejected_comments(){
	status=$("#status").val();
	if(status=='3'){
		$('#show_rejected_comments_div').show();
	}else{
		$('#show_rejected_comments_div').hide();
	}
}

function add_items(){
	var len = $('.item').length; 

let template = `<div class="item">
<div class="row">
<div class=' col-md-8 col-md-offset-1'>
	<label class="control-label">Content</label>
	<input type="text" class="form-control" name="position_text[`+(parseInt(len)+1)+`]" placeholder="" />
	</div>
</div>
<div class="row">
	<div class="col-md-8 col-md-offset-1"> 
		<div class="form-group">
		<div class="col-md-5 col-md-offset-1">Do you have ?</div>
		<div class="col-md-3">
			<label class="radio-inline"><input name="position_uhave[`+(parseInt(len)+1)+`]" value="yes" type="radio" >Yes</label>
		</div>
		<div class="col-md-3">
			<label class="radio-inline"><input name="position_uhave[`+(parseInt(len)+1)+`]" value="no" type="radio">No</label>
		</div>

	</div>
</div>

<div class="row">
	<div class="col-md-8 col-md-offset-1"> 
		<div class="form-group">
		<div class="col-md-5 col-md-offset-1">Do others have ?</div>
		<div class="col-md-3">
			<label class="radio-inline"><input name="position_others_have[`+(parseInt(len)+1)+`]" value="yes" type="radio" >Yes</label>
		</div>
		<div class="col-md-3">
			<label class="radio-inline"><input name="position_others_have[`+(parseInt(len)+1)+`]" value="no" type="radio">No</label>
		</div>

	</div>
</div>
<div class="col-md-8 col-md-offset-1"><button class="btn btn-sm btn-danger pull-right" class="remove" >Remove</a>
</div>
</div>`; 

$("#items").append(template);

}
$(document).ready(()=>{	
    $("body").on("click", ".remove", (e)=>{
        $(e.target).parents(".item").remove();
    })
});

</script>


</body>
</html>
