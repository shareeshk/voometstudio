<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

.remove_range,.remove_range:hover,.remove_field,.remove_field:hover{
	color:red;
}
.edit_remove_range,.edit_remove_range:hover,.edit_remove_field,.edit_remove_field:hover{
	color:red;
}
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>
<script type="text/javascript">
       $(document).ready(function () {
          
		   $.ajax({
			   url:"<?php echo base_url()?>admin/Catalogue/logistics_pincode_count",
			   type:"POST",
			   success:function(data){
				   
				   $("#logistics_pincodes_count").text(data);
			   }
		   })
			

       });    
</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<script>

function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_delete_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
swal({
			title: 'Are you sure?',
			text: "Delete Logistics all pincode",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {	
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/delete_logistics_all_pincodes_selected",
		type:"post",
		data:"selected_list="+selected_list,
		
		success:function(data){
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Given "+table+"(s) has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
</script>
<script>
function showDivCreate(){
	
	document.getElementById('viewDiv1').style.display = "none";
	document.getElementById('createDiv2').style.display = "block";
	
}
function showDivEdit(){
	
	document.getElementById('viewDiv1').style.display = "block";
	document.getElementById('createDiv2').style.display = "none";
	
}
</script>
<body>
<?php 
$state_names_arr=array();
foreach ($logistics_all_states as $logistics_all_states_value) {
	$state_names_arr[$logistics_all_states_value->state_id]=$logistics_all_states_value->state_name;
} 
?>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row" id="viewDiv1">
<div class="col-md-12">
	<div class="wizard-header text-center">
		<h5> Number of Logistics pincodes <span class="badge"><div id="logistics_pincodes_count"></div></span> 
		</h5>                        		
		                       	
	</div>
<table id="myTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
			<tr>
				<th colspan="5">
					<div class="col-md-12"><button id="multiple_delete_button" class="btn btn-danger btn-xs" onclick="multilpe_delete_fun('pincode')">Delete</button> <button id="multiple_delete_button4" class="btn btn-info btn-xs" onclick="showDivCreate()">Go to Create</button></div>
				</th>
			</tr>
	<tr>
		<th width="5%"><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
		<th width="8%">S. No</th>
		<th width="25%">Region Details</th>
		<th width="50%">Pincodes</th>
		<th width="12%">Action</th>
	</tr>
</thead>
<tbody>
<?php
$i=0;
foreach ($logistics_all_pincodes as $logistics_all_pincodes_value) { 
$i++; ?>

<tr>
<td><input type="checkbox" name="selected_list" value="<?php echo $logistics_all_pincodes_value->pincode_id; ?>"></td>
<td><?php echo $i; ?></td>

<td>
	<b>State :</b> <?php echo $state_names_arr[$logistics_all_pincodes_value -> state_id];?><br>
	<b>City :</b> <?php echo $logistics_all_pincodes_value -> city;?><br>
	<b>PIN :</b> <?php echo $logistics_all_pincodes_value -> pin;?>
</td>
<td style="word-break: break-all;"><?php echo $logistics_all_pincodes_value -> pincodes?></td>
<td>
	
	<form action="<?php echo base_url()?>admin/Catalogue/edit_logistics_all_pincodes_form" method="post">	<input type="hidden" value="<?php echo $logistics_all_pincodes_value -> pincode_id;?>" name="pincode_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>
</td>

</tr>
<?php } ?>
</tbody>
</table>
</div>
</div>
<div class="row-fluid common" id="createDiv2" style="display:none;">
<div class="col-md-8 col-md-offset-2">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">

	<form name="create_logistics_all_pincodes" id="create_logistics_all_pincodes" method="post" action="#" onsubmit="return form_validation();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				state: {
					required: true,
				},
				city: {
					required: true,
				},
				pin: {
					required: true,
				},
				'pincode_from[]': {
					required: true,
				},
				'pincode_to[]': {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Create Logistics Pincodes</h3>
		</div>	
		</div> 
		
		<div class="tab-content">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Choose State</label>
					<select name="state" id="state" class="form-control">
						<option value=""></option>
						<?php foreach ($logistics_all_states as $logistics_all_states_value) {  ?>
						<option value="<?php echo $logistics_all_states_value->state_id; ?>"><?php echo $logistics_all_states_value->state_name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter City</label>
					<input id="city" name="city" type="text" class="form-control" />
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter PIN (First three digits)</label>
					<input id="pin" name="pin" type="text" class="form-control" />
				</div>
				</div>
			</div>
			<div class="row multi-fields">
			<div class="form-group add_range">
				<div class="col-md-4 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Pincodes Starting Number</label>
					<input name="pincode_from[]" type="text" class="form-control" />
				</div>
				</div>
				<div class="col-md-4">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Pincodes Ending Number</label>
					<input name="pincode_to[]" type="text" class="form-control" />
				</div>
				</div>
				<div class="col-md-3">  
					<div class="form-group label-floating">
						<a href="#" class="text-info bold remove_range"><i class="fa fa-times-circle text-danger fa-2x" aria-hidden="true"></i></a>
					</div>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				
					<button id="add_one_more" name="add_one_more" class="btn btn-warning add_one_more pull-right btn-sm">Add One More</button>
				
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
				<div class="form-group distinct_values_common">
					<div class="distinct_values" id="distinct_values">
	
					</div>
				</div>
				</div>
			</div>	
			<div class="row">
				<div class="form-group">
				<div class="col-md-6 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Pincode Counts which are distinct</label>
					<input id="pincode_distinct_count" name="pincode_distinct_count" type="text" class="form-control" />
				</div>
				</div>
				<div class="col-md-4">  
				<div class="form-group label-floating">
					<button class="btn btn-warning pull-right generate_input_fields btn-sm">Generate pincodes</button>
				</div>
				</div>
				
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit_create_parent_category">Submit</button>
					<button class="btn btn-warning btn-sm" id="reset_create_parent_category" type="reset">Reset</button>
					<button class="btn btn-info btn-sm" type="button" onclick="showDivEdit()">Go to View</button>
					</div>
				</div>
			</div>
			
				

		</div>		
	</form>

</div>
</div>
</div>
</div>

</div>
<div class="footer">
</div>
</div>
</body>
</html>
<script type="text/javascript">

$(function(){
	
$('.common').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add_one_more", $(this)).click(function(e) {
        $('.add_range:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('');
		$('.remove_range').show();		
		return false;
    });
	$($wrapper).on("click",".remove_range", function(e){
		elements_length=$('.add_range > div', $wrapper).length;
		e.preventDefault(); 
			if(elements_length==3){
				//$('.remove_range').hide();
			}else{
			$('.remove_range').show();
			$(this).parent('div').parent('div').parent('div').remove();	
			}
    })
	//remove of pincode range
});
var x = 1;
$(".generate_input_fields").click(function(e) {
	count = $('input[name="pincode_distinct_count"]').val().trim();
	//$('.distinct_values').append('<div class="row">');
	
	for(i=0;i<count;i++){
		 x++; 
        $('.distinct_values').append('<div class="row"><div class="col-md-3"><div class="form-group label-floating"><input type="text" name="distinct_pincodes[]" class="form-control" placeholder="pincode"/></div></div><div class="col-md-2"><div class="form-group"><a href="#" class="btn btn-link remove_field"><i class="fa fa-times-circle text-danger fa-2x"></i></a></div></div></div>');
		if(i%2==1){
			$("<div class='row'></div>").html("<b>love</b>");
		}
	}
	//$('.distinct_values').append('</div>');
	$('input[name="pincode_distinct_count"]').val('');	
	return false;
});
	
	$('.distinct_values_common').on("click",".remove_field", function(e){ 
	e.preventDefault();
	$(this).parent('div').parent('div').parent('div').remove(); x--;
	});
	
$('.edit_common').each(function() {
    var $edit_wrapper = $('.edit-multi-fields', this);
    $(".edit_add_one_more", $(this)).click(function(e) {
        $('.edit_add_range:first-child', $edit_wrapper).clone(true).appendTo($edit_wrapper).find('input').val('');
		$('.edit_remove_range').show();		
		return false;
    });
	$($edit_wrapper).on("click",".edit_remove_range", function(e){ 
	elements_length=$('.edit_add_range > div', $edit_wrapper).length;
	e.preventDefault(); 
		if(elements_length==1){
			$('.edit_remove_range').hide();
		}else{
		$('.edit_remove_range').show();
		$(this).parent('div').parent('div').remove();	
		
		}
        
    })
	//remove of pincode range
});
var y = 1;
$(".edit_generate_input_fields").click(function(e) {
	count = $('input[name="edit_pincode_distinct_count"]').val().trim();
	 
	for(i=0;i<count;i++){
		 y++; 
        $('.edit_distinct_values').append('<div><div class="col-md-3 align_top"><input type="text" name="edit_distinct_pincodes[]" class="form-control" placeholder="pincode"></div><div class="col-md-1 align_top"><a href="#" class="btn btn-link edit_remove_field">X</a></div></div>');
	}
	$('input[name="edit_pincode_distinct_count"]').val('');	
	return false;
    });
	
	$('.edit_distinct_values_common').on("click",".edit_remove_field", function(e){ 
	e.preventDefault();
	$(this).parent('div').parent('div').remove(); y--;
	});
	
});

$(document).ready(function(){
    $('#myTable').dataTable({ stateSave: true });
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
});

function form_validation()
{
	var err = 0;
	var city = $('input[name="city"]').val().trim();
	var pin = $('input[name="pin"]').val().trim();	
	var pincode_from=document.getElementsByName("pincode_from[]");
	var pincode_to=document.getElementsByName("pincode_to[]");
	
	for(var x=0;x<pincode_from.length;x++)
	{
		if(pincode_from[x].value.trim() == '' || (parseInt(pincode_from[x].value.trim()) >= parseInt(pincode_to[x].value.trim())))
		{	
			//document.getElementsByName('pincode_from[]')[x].setAttribute("style", "border: 1px solid red;");
			//document.getElementsByName('pincode_to[]')[x].setAttribute("style", "border: 1px solid red;");				
			err = 1;
		}
		else
		{	
			//document.getElementsByName('pincode_from[]')[x].setAttribute("style", "border: 1px solid #ccc;");
			//document.getElementsByName('pincode_to[]')[x].setAttribute("style", "border: 1px solid #ccc;");				
		}
		
	}
	
		if(city=='')
		{
		   //$('input[name="city"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('input[name="city"]').css({"border": "1px solid #ccc"});
		}
		if(pin=='')
		{
		   //$('input[name="pin"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('input[name="pin"]').css({"border": "1px solid #ccc"});
		}
		
		if(err==1)
		{
			   $('#error_result_id').show();
			   $('#result').hide();
		}
		else
			{
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#create_logistics_all_pincodes');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_logistics_all_pincodes/"?>',
				type: 'POST',
				data: $('#create_logistics_all_pincodes').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {	
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					document.getElementById("city").value="";
					document.getElementById("city").focus();
				});
			}
				if(data=="yes")
				{
					  					  
					swal({
						title:"Success", 
						text:"Logistics all pincode is successfully added!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				if(data=="no")
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
			});
}
}]);			
			}
	
	return false;
}

</script>
