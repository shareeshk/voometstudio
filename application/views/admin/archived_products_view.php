<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script type="text/javascript">
var table;
$(document).ready(function (){
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    var rows_selected = [];
    table = $('#table_products').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Catalogue/products_processing",
			data: function (d) { d.pcat_id = $('#pcat_id').val();d.cat_id = $('#cat_id').val();d.subcat_id = $('#subcat_id').val();arr=$('#brand_id').val().split('-');d.brand_id = arr[0];d.active=$('#active_pr').val();d.page="archi"},
            type: "post",
            error: function(){
              $("#table_products_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				
				document.getElementById("archived_products_count").innerHTML=json.recordsFiltered;
				
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
   
	$('#reset_form_button').on('click',function(){
		table.draw();
	});
	$("#submit_button").click(function() {
		table.draw();
	});
	
});	
function showAvailableCategories_drawtable(obj){
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=2",
				success:function(data){
					table.draw();
					if(data!=0){
						$("#cat_id").html(data);
					}else{
						$("#cat_id").html('<option value="0">No Category</option>');
					}
				}
			});
	}
	
}
function showAvailableSubCategories_drawtable(obj){
	if(obj.value!="" && obj.value!="None"){
		cat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id+"&active=2",
				success:function(data){
					table.draw();
					if(data!=0){
						$("#subcat_id").html(data);
					}else{
						$("#subcat_id").html('<option value="0">No SubCategory</option>');
					}
				}
			});
	}
	
}
function showAvailableBrands_drawtable(obj){	
	if(obj.value!="" && obj.value!="None"){
		subcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_brands",
				type:"post",
				data:"subcat_id="+subcat_id+"&active=2&with_status=1",
				success:function(data){
					table.draw();
					if(data!=0){
						$("#brand_id").html(data);	
					}
					else{
						$("#brand_id").html('<option value="0">-None-</option>');
					}
				}
			});
	}
}
function drawtable(obj){
	arr = obj.value.split('-');
	status=arr[1];
	$('#active_brands').val(status);
	table.draw();
}
function drawtable_1(obj){
	table.draw();
}
function multilpe_restore_fun(table_name){
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table_name+"!");
		return false;
	}
	selected_list=selected_list_arr.join(",");
	swal({
			title: 'Are you sure?',
			text: "Restore Archived Products",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, restore it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	active=$('#active_brands').val();
	if(active==1){
		$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/update_products_selected",
		type:"post",
		data:"selected_list="+selected_list+"&active=1",
		
		success:function(data){
			if(data==true){
				swal({
						title:"Restored!", 
						text:"Given "+table_name+"(s) has been restored successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	}else{
		swal(
			'Sorry',
			'You cant restore it. Try to restore the corresponding (Main) Product.',
			'error'
		)
		var obj=document.getElementsByName("common_checkbox");	
		var obj_arr=document.getElementsByName("selected_list");
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
		for(i=0;i<obj.length;i++){
			obj[i].checked=false;
		}
	}
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<?php 
//print_r($products);
?>

<div class="container">

<div class="row" id="viewDiv1">
	<div class="wizard-header text-center">
		<h5> Number of Product <span class="badge"><div id="archived_products_count"></div></span> 
		</h5>                 		          	
	</div>
	<input value="<?php echo $pcat_id; ?>" type="hidden" id="pcat_id">
	<input value="<?php echo $cat_id; ?>" type="hidden" id="cat_id">
	<input value="<?php echo $subcat_id; ?>" type="hidden" id="subcat_id">
	<input value="<?php echo $brand_id; ?>" type="hidden" id="brand_id">
	
	<input value="<?php echo $brand_active; ?>" type="hidden" name="active_brands" id="active_brands">

	<div class="col-md-12" id="search_block">
	<form name="search_for_brands" class="col-md-12">
		
		<div class="row"> 
			
			<div class="col-md-4">
				<select name="active_pr" id="active_pr" class="form-control" onchange="drawtable_1(this)">
					<option value=0>Inactive</option>		
					<option value=1>active</option>		
				</select>
			</div>
			
		</div>
		
	</form>
	</div>
	
<table id="table_products" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<th colspan="5">
			<div class="padding_right"><button id="multiple_delete_button" class="btn btn-success btn-xs" onclick="multilpe_restore_fun('products')">Restore</button></div>
		</th>
	</tr>
	<tr>
		<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
		<th>Product Name</th>
		<th>Product to Catalog</th>
		<th>Last Updated</th>
			
	</tr>
</thead>

</table>
</div>

</div>

<script type="text/javascript">

$(document).ready(function (){
	pcat_id=$('#f_pcat_id').val();
	cat_id=$('#f_cat_id').val();
	subcat_id=$('#f_subcat_id').val();
	brand_id=$('#f_brand_id').val();
	product_id=$('#f_product_id').val();
	inventory_id=$('#f_inventory_id').val();
	brand_active=$('#brand_active').val();
	product_active=$('#product_active').val();
	if(pcat_id!='' && pcat_id!=null){		
		$("#pcat_id").val(pcat_id);	
		$("#active_pr").val(product_active);	
		
		$.ajax({
			url:"<?php echo base_url(); ?>admin/Catalogue/show_available_brands",
			type:"post",
			async:false,
			data:"subcat_id="+subcat_id+"&active=2&with_status=1",
			success:function(data){
				if(data!=0){					
					$("#brand_id").html(data);
					$("#brand_id").val(brand_id+'-'+brand_active);
				}else{
					$("#brand_id").html('<option value="">-None-</option>');
				}
			}
		});
		$.ajax({
			url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
			type:"post",
			async:false,
			data:"cat_id="+cat_id+"&active=1",
			success:function(data){
				if(data!=0){
					$("#subcat_id").html(data);
					$("#subcat_id").val(subcat_id);
				}else{
					$("#subcat_id").html('<option value="">-None-</option>');
					$("#brand_id").html('<option value="">-None-</option>');
				}
			}
		});
		$.ajax({
			url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
			type:"post",
			async:false,
			data:"pcat_id="+pcat_id+"&active=1",
			success:function(data){	
				if(data!=0){
					$("#cat_id").html(data);
					$("#cat_id").val(cat_id);
				}else{
					$("#cat_id").html('<option value="">-None-</option>');
					$("#brand_id").html('<option value="">-None-</option>');
				}
			}
		});
		table.draw();
	}
});

function manageInventoryFun(product_id){
		$.ajax({
			url: '<?php echo base_url(); ?>admin/Catalogue/archived_inventory/',
			type: 'POST',
			data: "product_id="+product_id,
			dataType: 'JSON',	
		}).done(function(data){
			alert(data);
		});
}
</script>
</div>
</body>

</html>

