<html>
<head>
<meta charset="utf-8">
<title>Reviews and Ratings</title>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container" id="get">
	<form name="back_to_filter" id="back_to_filter" method="post">
		<input type="hidden" value="<?php echo $pcat_id; ?>" name="pcat_id" id="pcat_id">
		<input type="hidden" value="<?php echo $cat_id; ?>" name="cat_id" id="cat_id">
		<input type="hidden" value="<?php echo $subcat_id; ?>" name="subcat_id" id="subcat_id">
		<input type="hidden" value="<?php echo $brand_id; ?>" name="brand_id" id="brand_id">
		<input type="hidden" value="<?php echo $product_id; ?>" name="product_id" id="product_id">
		<input type="hidden" value="<?php echo $inventory_id; ?>" name="inventory_id" id="inventory_id">
	</form>

	<div class="row">
		<h4 class="text-center"> Select Inventory To view Ratings and Reviews</h4>
		<form name="search_for_rating" id="search_for_rating" method="post" action="#" onsubmit="return form_validation();" class="form-horizontal">
			
					<div class="form-group">
					<div class="col-md-6 col-md-offset-3">
					<select name="parent_category" id="parent_category" class="form-control align_top" onchange="showAvailableCategories(this)">
						<option value="">-Select Parent Category-</option>
						<?php foreach ($parent_category as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>" <?php echo ($parent_category_value->pcat_id==$pcat_id)? "selected":''; ?> ><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<option value="0">--None--</option>
						
					</select>
					</div>
					</div>
					
					<div class="form-group">
					<div class="col-md-6 col-md-offset-3">
					<select name="categories" id="categories" class="form-control align_top" onchange="showAvailableSubCategories(this)">
						<option value="">-Select Category-</option>
					</select>
					</div>
					</div>
					
					<div class="form-group">
					<div class="col-md-6 col-md-offset-3">
					<select name="subcategories" id="subcategories" class="form-control align_top" onchange="showAvailableBrands(this)">
						<option value="">-Select Sub Category-</option>
					</select>
					</div>
					</div>
					<div class="form-group">
					<div class="col-md-6 col-md-offset-3">
					<select name="brands" id="brands" class="form-control align_top" onchange="showAvailableProducts(this)">
						<option value="">-Select Brands-</option>
					</select>
					</div>
					</div>
					<div class="form-group">
					<div class="col-md-6 col-md-offset-3">
					<select name="products" id="products" class="form-control align_top" onchange="showAvailableInventory(this)">
						<option value="">-Select Products-</option>
					</select>
					</div>
					</div>
					<div class="form-group">
					<div class="col-md-6 col-md-offset-3">
					<select name="inventory" id="inventory" class="form-control align_top">
						<option value="">-Select Inventory-</option>
					</select>
					</div>
					</div>
					<div class="form-group">
					<div class="col-md-6 col-md-offset-5">
						<button type="submit" class="btn btn-info btn-xs" id="submit_button">Submit</button>
						<button class="btn btn-warning btn-xs" type="reset" id="reset_form_button">Reset</button>
					</div>
					</div>
				
		</form>
	</div>
</div>
</body>
</html>
<script type="text/javascript">
$(document).ready(function (){
	pcat_id=$('#pcat_id').val();
	cat_id=$('#cat_id').val();
	subcat_id=$('#subcat_id').val();
	brand_id=$('#brand_id').val();
	product_id=$('#product_id').val();
	inventory_id=$('#inventory_id').val();

	
	if(pcat_id!='' && pcat_id!=null){
		$("#parent_category").val(pcat_id);
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_inventory",
				type:"post",
				data:"product_id="+product_id,
				success:function(data){
					if(data!=0){
						$("#inventory").html(data);
						$("#inventory").val(inventory_id);
					}
					else{
						$("#inventory").html('<option value="">-None-</option>');
					}
				}
			});
			$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_products",
				type:"post",
				data:"brand_id="+brand_id,
				success:function(data){
					if(data!=0){
						$("#products").html(data);
						$("#products").val(product_id);
					}
					else{
						$("#products").html('<option value="">-None-</option>');
					}
				}
			});
			$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_brands",
				type:"post",
				data:"subcat_id="+subcat_id,
				success:function(data){
					if(data!=0){
						$("#brands").html(data);
						$("#brands").val(brand_id);
					}
					else{
						$("#brands").html('<option value="">-None-</option>');
					}
				}
			});
			$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id,
				success:function(data){
					if(data!=0){
						$("#subcategories").html(data);
						$("#subcategories").val(subcat_id);
					}
					else{
						$("#subcategories").html('<option value="">-None-</option>')
						$("#brands").html('<option value="">-None-</option>')

					}
				}
			});
			$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id,
				success:function(data){
					if(data!=0){
						$("#categories").html(data);
						$("#categories").val(cat_id);
					}
					else{
						$("#categories").html('<option value="">-None-</option>')
						$("#brands").html('<option value="">-None-</option>')

					}
				}
			});
	}
});

function showAvailableInventory(obj){
	
	if(obj.value!="" && obj.value!="None"){
		product_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_inventory",
				type:"post",
				data:"product_id="+product_id,
				success:function(data){
					if(data!=0){
						$("#inventory").html(data);
					}
					else{
						$("#inventory").html('<option value="">-None-</option>');
					}
				}
			});
	}
	
}
function showAvailableProducts(obj){
	
	if(obj.value!="" && obj.value!="None"){
		brand_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_products",
				type:"post",
				data:"brand_id="+brand_id,
				success:function(data){
					if(data!=0){
						$("#products").html(data);
					}
					else{
						$("#products").html('<option value="">-None-</option>');
					}
				}
			});
	}
	
}
function showAvailableBrands(obj){
	
	if(obj.value!="" && obj.value!="None"){
		subcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_brands",
				type:"post",
				data:"subcat_id="+subcat_id,
				success:function(data){
					if(data!=0){
						$("#brands").html(data);
					}
					else{
						$("#brands").html('<option value="">-None-</option>');
					}
				}
			});
	}
	
}
function showAvailableSubCategories(obj){
	
	if(obj.value!="" && obj.value!="None"){
		cat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id,
				success:function(data){
					if(data!=0){
						$("#subcategories").html(data);
					}
					else{
						$("#subcategories").html('<option value="">-None-</option>')
						$("#brands").html('<option value="">-None-</option>')

					}
				}
			});
	}
	
}

function showAvailableCategories(obj){
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id,
				success:function(data){
					if(data!=0){
						$("#categories").html(data);
					}
					else{
						$("#categories").html('<option value="">-None-</option>')
						$("#brands").html('<option value="">-None-</option>')

					}
				}
			});
	}
	
}
function form_validation()
{
	var parent_category = $('select[name="parent_category"]').val().trim();
	var categories = $('select[name="categories"]').val().trim();
	var subcategories = $('select[name="subcategories"]').val().trim();
	var brands = $('select[name="brands"]').val().trim();
	var products = $('select[name="products"]').val().trim();
	var inventory = $('select[name="inventory"]').val().trim();

	    var err = 0;
		if(categories=='')
		{
		   $('select[name="categories"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('select[name="categories"]').css({"border": "1px solid #ccc"});
		}
		
		if(subcategories=='')
		{
		   $('select[name="subcategories"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{
			$('select[name="subcategories"]').css({"border": "1px solid #ccc"});
		}
		if(brands=='')
		{
		   $('select[name="brands"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('select[name="brands"]').css({"border": "1px solid #ccc"});
		}
	
		if(products=='')
		{
		   $('select[name="products"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('select[name="products"]').css({"border": "1px solid #ccc"});
		}
		if(inventory==''){
		   $('select[name="inventory"]').css("border", "1px solid red");	   
		   err = 1;
		}else{			
			$('select[name="inventory"]').css({"border": "1px solid #ccc"});
		}
	
		if(err==1){
			
		    $('#error_result_id').show();
		    $('#result').hide();
			
		}else{

			var form_status = $('<div class="form_status"></div>');		
			var form = document.getElementById('search_for_rating');		
			form.action='<?php echo base_url()."admin/Ratemanaging/rated_inventory"; ?>';
			form.submit();
		}
	
	return false;
}
</script>
