<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Customer Bank Transfer</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 


<style type="text/css">
#text-editor .btn-group, .btn-group-vertical {
    position: relative;
    margin: 0px 1px;
}
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}

.wizard-card .tab-content {
	 min-height: 400px;
	 padding:0px;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
.wizard-card input[type="file"]
{
	opacity: 0 !important;
	cursor: pointer;
	display: block;
	height: 100%;
	left: 0;
	opacity: 0 !important;
	position: absolute;
	top: 0;
	width: 100%;
}
#c_head{
	width:300px !important;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js"></script>

<style>
.datepicker{z-index:1151 !important;}
</style>
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   var table = $('#customer_bank_tranfer_accounts_incoming_table').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Manage_cust/customer_bank_tranfer_accounts_incoming_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#customer_bank_tranfer_accounts_incoming_table_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("wallet_transfer_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'40%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});	

function accept_fun(wallet_transaction_bank_id){
	$.ajax({
			url:"<?php echo base_url()?>admin/Manage_cust/wallet_transaction_bank_accept",
			type:"POST",
			data:"wallet_transaction_bank_id="+wallet_transaction_bank_id,
			success:function(data){
				swal("Processed");
				location.reload();
			}
	});
}

function reject_fun(wallet_transaction_bank_id){
	$.ajax({
			url:"<?php echo base_url()?>admin/Manage_cust/wallet_transaction_bank_reject",
			type:"POST",
			data:"wallet_transaction_bank_id="+wallet_transaction_bank_id,
			success:function(data){
				swal("Rejected");
				location.reload();
			}
	});
}

function wallet_bank_success_fun(wallet_transaction_bank_id){
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Manage_cust/customer_wallet_bank_transfer_accounts_incoming_walletsuccess",
		type:"POST",
		data:"wallet_transaction_bank_id="+wallet_transaction_bank_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			//alert(data);
			$("#wallet_bank_success_form").html(data);
			$("#wallet_bank_success_modal").modal("show");
		}
	})
	
	
}
function wallet_bank_failure_fun(wallet_transaction_bank_id){
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Manage_cust/customer_wallet_bank_transfer_accounts_incoming_walletfailure",
		type:"POST",
		data:"wallet_transaction_bank_id="+wallet_transaction_bank_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			//alert(data);
			$("#wallet_bank_failure_form").html(data);
			$("#wallet_bank_failure_modal").modal("show");
		}
	})
}
function showDivCreate_emioptions(){
	document.getElementById('viewDiv1').style.display = "none";
	document.getElementById('createDiv2_emioptions').style.display = "block";
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="row" id="viewDiv1">
<div class="page-header"><h4 class="text-center">EMI Options <span class="badge" id="count"></span></h4></div>
<table id="emioptions_table" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th colspan="6">
					<div class="row">				
						<div class="col-md-2">				
							<button class="btn btn-info btn-xs" onclick="showDivCreate_emioptions()">Add</button>
							<button id="multiple_delete_button3" class="btn btn-danger btn-xs" onclick="multilpe_delete_no_data_fun()">Delete</button>
						</div>
					</div>
				</th>
			</tr>
			<tr>
				<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
				<th class="text-primary small bold">Image</th>
				<th class="text-primary small bold">Name</th>
				<th class="text-primary small bold">Plan Name</th>
				<th class="text-primary small bold">Plan</th>
				<th class="text-primary small bold">EMI start value(%)</th>
			</tr>
		</thead>
	</table>
	</div>
	
	
	
	
	
	

<!--- emioptions div starts ------->
<div class="row" id="createDiv2_emioptions" style="display:none;">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
 	
		<form name="add_emioptions_form" id="add_emioptions_form" method="post" action="#" onsubmit="return form_validation_emioptions();" enctype="multipart/form-data">
		
		<script type="text/javascript">
			var $validator = $('.wizard-card #add_emioptions_form').validate({
			rules: {
				name_emioptions: {
					required: true,
				},
				planname_emioptions: {
					required: true,
				},
				emistartvalue_emioptions: {
					required: true,
				},
				emiplan_emioptions_0: {
					required: true,
				},
				interest_emioptions_0: {
					required: true,
				},
				totalcost_emioptions_0: {
					required: true,
				}
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
		
		<div class="row">
			
			<div class="col-md-offset-3 col-md-6">
			
			
				
			<!---common--image--->
		
			<div class="col-md-5 col-md-offset-3">
			<div class="col-md-12 text-center"><b>Image (200*200)</b></div>
				<div class="col-md-12">
				<div class="picture-container">
					<div class="picture">
						<img class="picture-src" id="wizardPictureCommon200" title=""/>
				
						<input type="file" name="image_emioptions" id="wizard-picture-Common200" required/>
					</div>
				
				</div>
				</div>	
			</div>
		
		<!---common--image--->
		
		
			<div class="col-md-12">
				<div class="form-group">
					<input type="text" class="form-control" name="name_emioptions" required placeholder="Name">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<input type="text" class="form-control"  name="planname_emioptions" required placeholder="Plan Name">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<button class="btn btn-success" type="button" onclick="addFurthurEMIPlanDetailsFun()">Add Furthur EMI Plan Details</button>
				</div>
			</div>
			<div id="setof_emiplan_emioptions_div">
				<div class="col-md-12 emiplan_emioptions_div"  id="emiplan_emioptions_0_div">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" class="form-control"  name="emiplan_emioptions_0" required placeholder="EMI Plan">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" class="form-control"  name="interest_emioptions_0" required placeholder="Interest">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<input type="text" class="form-control"  name="totalcost_emioptions_0" required placeholder="Total Cost" onkeypress="return isNumberwithdot(event);">
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<textarea id="emiplan_emioptions_details_json" name="emiplan_emioptions_details_json" style="display:none;">
				
			</textarea>
			
			<div class="col-md-12">
				<div class="form-group">
					<input type="text" class="form-control"  name="emistartvalue_emioptions" required placeholder="EMI Start Value (%)" onkeypress="return isNumberwithdot(event);">
				</div>
			</div>
			
			
			
			
			
			
			
		
		
		
			
		
		
		
			
			
		
		<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-info btn-xs" id="submit_emioptions_button">Submit</button>
			<button class="btn btn-warning btn-xs" type="reset" id="reset_form_emioptions_button">Reset</button>
		</div>
			
			</div>
		
		</div>
		
		</form>
				
</div>
</div> <!-- wizard container -->
</div>
</div>
<!--- emioptions div ends ----->




</div>

</div>
</body> 
</html>  



<script type="text/javascript">


function multilpe_delete_no_data_fun(){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
	swal({
			title: 'Are you sure?',
			text: "Delete EMI Option(s)",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
			
	$.ajax({
		url:"<?php echo base_url()?>admin/EMIoptions/delete_emioptions",
		type:"post",
		data:"selected_list="+selected_list,
		
		success:function(data){
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					draw_table();
				});
			}else {
				swal("Error", "Oops ..! You can't delete it.", "error");
			}
			//location.reload();
			
		}
	});
	
			})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
	
}


var table;
$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   table = $('#emioptions_table').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url: '<?php echo base_url()."admin/EMIoptions/EMIoptions_processing"?>', // json datasource
			data: function (d) { d.page = $('#s_page').val();d.section_name_in_page = $('#s_section_name_in_page').val();d.pcat_id = $('#pcat_id').val();d.cat_id = $('#categories1').val(); },
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#assign_inv_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("count").innerHTML=json.recordsFiltered;
				return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'5%',
         'className': 'dt-body-center'
      },{
         'targets': 1,
         'width':'25%',
      },{
         'targets': 2,
         'width':'25%',
      },{
         'targets': 3,
         'width':'15%',
      },{
         'targets': 4,
         'width':'30%',
      },{
         'targets': 5,
         'width':'10%',
      }],
      'order': [0, 'desc']
   });
});	

function draw_table(){
	table.draw();
}
$('#reset_form_emioptions_button').on('click',function(){
	table.draw();
});

$('#submit_emioptions_button').on('click',function(){
	table.draw();
});

function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
</script>
<script>
			function form_validation_emioptions(){

	var name_emioptions = $('input[name="name_emioptions"]').val().trim();
	var planname_emioptions = $('input[name="planname_emioptions"]').val().trim();
	var emistartvalue_emioptions = $('input[name="emistartvalue_emioptions"]').val();
	
	emiplan_emioptions_details_arr=[];
	$(".emiplan_emioptions_div").each(function(){
		emiplan_emioptions_id=$(this).attr("id");
		emiplan_emioptions_index=emiplan_emioptions_id.split("_")[2];
		emiplan_emioptions=$('input[name="emiplan_emioptions_'+emiplan_emioptions_index+'"]').val().trim();
		interest_emioptions=$('input[name="interest_emioptions_'+emiplan_emioptions_index+'"]').val().trim();
		totalcost_emioptions=$('input[name="totalcost_emioptions_'+emiplan_emioptions_index+'"]').val().trim();
		if(emiplan_emioptions.trim()=="" && interest_emioptions.trim()=="" && totalcost_emioptions.trim()==""){
			
		}
		else{
			emiplan_emioptions_details_arr.push(emiplan_emioptions+"|||||"+interest_emioptions+"|||||"+totalcost_emioptions);
		}
	});
	emiplan_emioptions_details_json=JSON.stringify(emiplan_emioptions_details_arr);
	$("#emiplan_emioptions_details_json").html(emiplan_emioptions_details_json)
	
	//var inventory = $('select[name="inventory"]').val().trim();

	    var err = 0;
		if(name_emioptions=='')
		{
		   $('input[name="name_emioptions"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('input[name="name_emioptions"]').css({"border": "1px solid #ccc"});
		}
		if(planname_emioptions=='')
		{
		   $('input[name="planname_emioptions"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('input[name="planname_emioptions"]').css({"border": "1px solid #ccc"});
		}
		if(emistartvalue_emioptions=='')
		{
		   $('input[name="emistartvalue_emioptions"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			$('input[name="emistartvalue_emioptions"]').css({"border": "1px solid #ccc"});
		}

		
		if(err==0){
		//alert();return false;
			var form_status = $('<div class="form_status"></div>');		

			/////////////////////////

			var form = $('#add_emioptions_form');	
			var form_create = document.forms.namedItem("add_emioptions_form");
			var ajaxData = new FormData(form_create);
			
			swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();
    $.ajax({
				url: '<?php echo base_url()."admin/EMIoptions/add_EMIoptions"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false,

			}).done(function(data){	
				form_status.html('');
				
				
			if(data){
				swal({
					title:"Success", 
					text:"Added!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}
			if(data=="no")
			{
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
				
			});
  }
}]);
			
			////////////////////////
			
			}
	
	return false;
}
			    function deleteFurthurEMIPlanDetailsFun(emiplan_index_delete){
					$("#emiplan_emioptions_"+emiplan_index_delete+"_div").remove();
				}
				function isNumberwithdot(evt) {
					evt = (evt) ? evt : window.event;
					var charCode = (evt.which) ? evt.which : evt.keyCode;
					if (charCode > 31 && ((charCode < 48 || charCode > 57) && charCode!=46)) {
						return false;
					}
					return true;
				}
			    var emiplan_index=1;
				function addFurthurEMIPlanDetailsFun(){
					emiplan_index++;
					var emiplan_str="";
					emiplan_str+='<div class="col-md-12 emiplan_emioptions_div" id="emiplan_emioptions_'+emiplan_index+'_div">';
						emiplan_str+='<div class="row">';
							emiplan_str+='<div class="col-md-4">';
								emiplan_str+='<div class="form-group">';
									emiplan_str+='<input type="text" class="form-control"  name="emiplan_emioptions_'+emiplan_index+'"  placeholder="EMI Plan">';
								emiplan_str+='</div>';
							emiplan_str+='</div>';
							emiplan_str+='<div class="col-md-4">';
								emiplan_str+='<div class="form-group">';
									emiplan_str+='<input type="text" class="form-control"  name="interest_emioptions_'+emiplan_index+'"  placeholder="Interest">';
								emiplan_str+='</div>';
							emiplan_str+='</div>';
							emiplan_str+='<div class="col-md-3">';
								emiplan_str+='<div class="form-group">';
									emiplan_str+='<input type="text" class="form-control"  name="totalcost_emioptions_'+emiplan_index+'" onkeypress="return isNumberwithdot(event);" placeholder="Total Cost">';
								emiplan_str+='</div>';
							emiplan_str+='</div>';
							emiplan_str+='<div class="col-md-1">';
								emiplan_str+='<div class="form-group">';
									emiplan_str+='<i class="fa fa-trash" style="cursor:pointer" onclick="deleteFurthurEMIPlanDetailsFun('+emiplan_index+')"></i>';
								emiplan_str+='</div>';
							emiplan_str+='</div>';
						emiplan_str+='</div>';
					emiplan_str+='</div>';
					$("#setof_emiplan_emioptions_div").append(emiplan_str);
				}
			</script>