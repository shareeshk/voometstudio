<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Cancel Refund Request Orders - Refund Failure</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   var table = $('#cancels_refund_request_orders_table_accounts_failure').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Orders/cancels_refund_request_orders_accounts_failure_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#cancels_refund_request_orders_table_accounts_failure_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("cancel_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'5%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});	



</script>
  
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header"><h4 class="text-center">Cancels Refund Request Orders - Refund Failure <span class="badge" id="cancel_count"></span></h4></div>
	<table id="cancels_refund_request_orders_table_accounts_failure" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="text-primary small bold">Image</th>
				<th class="text-primary small bold">Order Summary</th>
				<th class="text-primary small bold">Cancellation Details</th>
				<th class="text-primary small bold">Refund Details</th>
				<th class="text-primary small bold">Buyer Details</th>
				<th class="text-primary small bold">Failure Comments</th>
			</tr>
		</thead>
	</table>
</div>
 </div> 
</body> 
</html>  