<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">

function showDivPrevious(){
	
	var form = document.getElementById('search_for_specification');		
	
	form.action='<?php echo base_url()."admin/Catalogue/specification"; ?>';
	form.submit();
			
	 //window.location.href = '<?php echo base_url(); ?>admin/Catalogue/category';
}

</script>
<?php

if($get_specification_data['specification_sort_order']=="0"){?>
	<script>
	$(document).ready(function () {
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	document.getElementById('show_available_common_specification').disabled = true;
	});
	</script>
	<?php
}
?>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<?php 
//print_r($filter);
?>

<div class="container-fluid">

<div class="row-fluid" id="editDiv3">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
			
	<form name="edit_specification" id="edit_specification" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_parent_category: {
					required: true,
				},
				
				edit_sort_order: {
					required: true,
				},
				edit_specification_group: {
					required: true,
				},
				edit_specification_name: {
					required: true,
				},
				edit_specification_value: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Specification</h3>
		</div>	
		</div>
		<div class="tab-content">
					
		<input type="hidden" name="edit_specification_id" id="edit_specification_id" value="<?php echo $get_specification_data["specification_id"];?>">
		
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Parent category-</label>
					<select name="edit_parent_category" id="edit_parent_category" class="form-control" onchange="showAvailableCategoriesEdit(this)" disabled>
						<option value=""></option>
						<?php foreach ($parent_catagories as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>" <?php echo ($parent_category_value->pcat_id==$pcat_id)? "selected":''; ?>><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<?php if(empty($parent_catagories)){
							?>
								<option value="0" selected>--None--</option>
								<?php 
							}else{?>
							<option value="0">--None--</option>
							<?php }
							?>
					</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">-Select Category-</label>
					<select name="edit_categories" id="edit_categories" class="form-control" onchange="showAvailableSubCategoriesEdit(this)" disabled>
						<option value=""></option>
						<?php foreach ($categories as $category_value) {  ?>
						<option value="<?php echo $category_value->cat_id; ?>" <?php echo ($category_value->cat_id==$cat_id)? "selected":''; ?>><?php echo $category_value->cat_name; ?></option>
						<?php } ?>
		
					</select>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Sub Category-</label>
					<select name="edit_subcategories" id="edit_subcategories" class="form-control" onchange="showAvailableSpecificationGroup(this)" disabled>
						<option value=""></option>
						<?php foreach ($subcategories as $subcategory_value) {  ?>
						<option value="<?php echo $subcategory_value->subcat_id; ?>" <?php echo ($subcategory_value->subcat_id==$subcat_id)? "selected":''; ?>><?php echo $subcategory_value->subcat_name; ?></option>
						<?php } ?>
		
					</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">-Select Specification Group-</label>
					<select name="edit_specification_group" id="edit_specification_group" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($specification_group as $specification_group_value) {  ?>
						<option value="<?php echo $specification_group_value->specification_group_id; ?>" <?php echo ($specification_group_value->specification_group_id==$specification_group_id)? "selected":''; ?>><?php echo $specification_group_value->specification_group_name; ?></option>
						<?php } ?>
							
					</select>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Enter Specification Name</label>
					<input id="edit_specification_name" name="edit_specification_name" type="text" class="form-control" value="<?php echo $get_specification_data["specification_name"];?>"/>
					<input id="edit_specification_name_default" name="edit_specification_name_default" type="hidden" class="form-control" value="<?php echo $get_specification_data["specification_name"];?>"/>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">Enter Specification Values separated by comma(,) </label>
					<textarea id="edit_specification_value" name="edit_specification_value" type="text" class="form-control"><?php echo $get_specification_data["specification_value"];?></textarea>
				</div>
				</div>
			</div>		
			<div class="row">
				
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Enter Sort Order Value</label>
					
					<select name="edit_sort_order" id="edit_sort_order" class="form-control" onchange="show_view_validatn()">
					<option value="<?php echo $get_specification_data["specification_sort_order"];?>" selected><?php echo $get_specification_data["specification_sort_order"];?></option>
						<?php
							if($get_sort_order_options_for_specification["key"]==""){
								if($get_specification_data["specification_sort_order"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php } ?>
								<option value="1">1</option>
								<?php
							}
							else{
								if($get_specification_data["specification_sort_order"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php
								}
								$value_arr=explode(",",$get_sort_order_options_for_specification["value"]);
								foreach($value_arr as $k => $v){
									?>
										<option value="<?php echo $v;?>"><?php echo $v;?></option>
									<?php
									}
							}
						?>
					</select>
					<input id="edit_sort_order_default" name="edit_sort_order_default" type="hidden" value="<?php echo $get_specification_data["specification_sort_order"];?>"/>
				</div>
				</div>
				
			</div>		
					
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group label-floating">
			<label class="control-label">-Swap Options-</label>
			<select class="form-control" id="show_available_common_specification" onchange="changeEventHandler(event)">
			<option selected value=""></option>
			<?php
			
			foreach($specification_sort_order_arr as $subcategory_order_arr){
				if($subcategory_order_arr["specification_id"]!=$edit_specification_id){
				?>
				
					<option value="<?php echo $subcategory_order_arr["specification_id"];?>"><?php echo $subcategory_order_arr["specification_name"];?></option>
					<?php
				}
			}
			?>
			
			</select>
			</div>
				</div>
			</div>		
				
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group">
					<div class="col-md-6">Show Specification</div>
					<div class="col-md-3">
						<label class="radio-inline"><input id="edit_view1" name="edit_view" value="1" type="radio" <?php if($get_specification_data["view_specification"]=="1"){echo "checked";}?>>View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input id="edit_view2" name="edit_view" value="0" type="radio" <?php if($get_specification_data["view_specification"]=="0"){echo "checked";}?>>Hide</label>
					</div>
				
					</div>
				</div>
			</div>		
			
			<div class="row">
				<div class="col-md-6 col-md-offset-4">
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit-data" disabled="">Submit</button>
		
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
					</div>
				</div>
			</div>				
		</div>			
				
	</form>
	<form name="search_for_specification" id="search_for_specification" method="post">
		<input value="<?php echo $get_specification_data["pcat_id"]; ?>" type="hidden" id="pcat_id" name="pcat_id"/>
		<input value="<?php echo $get_specification_data["cat_id"]; ?>" type="hidden" id="cat_id" name="cat_id"/>
		<input value="<?php echo $get_specification_data["subcat_id"]; ?>" type="hidden" id="subcat_id" name="subcat_id"/>
		<input value="<?php echo $get_specification_data["specification_group_id"]; ?>" type="hidden" id="specification_group_id" name="specification_group_id"/>
		<input value="view" type="hidden" name="create_editview">
	</form>
</div>
</div>
</div>
</div>

</div>
<div class="footer">
</div>
<script>
function changeEventHandler(event){
        parent_id=event.target.value;
		if(parent_id!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		}
		if(parent_id==""){
	edit_sort_order=document.getElementById("edit_sort_order").value;
	if(edit_sort_order==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;					
	}
	if(edit_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
		}
}
function show_view_validatn(){
	edit_sort_order=document.getElementById("edit_sort_order").value;
	show_available_common_specification=document.getElementById("show_available_common_specification").value;
	if(show_available_common_specification==""){
	if(edit_sort_order==0){
		document.getElementById("edit_view1").disabled = true;
		document.getElementById("edit_view2").checked = true;
		
	}
	if(edit_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		
	}
	}
	if(show_available_common_specification!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
	
}
</script>
<script>

function showAvailableSpecificationGroup(obj){
	pcat_id=$("#parent_category").val();
	cat_id=$("#categories").val();
	//if(obj.value!="" && obj.value!="None"){
		subcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_specification_group",
				type:"post",
				data:"pcat_id="+pcat_id+"&cat_id="+cat_id+"&subcat_id="+subcat_id+"&active=1"+"&flagtype=3",
				success:function(data){
					
					if(data!=0){
						$("#specification_group").html(data);
						$("#edit_specification_group").html(data);
					}
					else{
						$("#specification_group").html('<option value="0"></option>');
						$("#edit_specification_group").html('<option value="0"></option>');
					}
				}
			});
	//}
	
}

function showAvailableCategoriesEdit(obj)
{
	//if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#edit_categories").html(data);
					}
					else{
						$("#edit_categories").html('<option value="0"></option>')
					}
				}
			});
	//}
}
function showAvailableSubCategoriesEdit(obj)
{
	//if(obj.value!="" && obj.value!="None"){
		cat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#edit_subcategories").html(data);
					}
					else{
						$("#edit_subcategories").html('<option value="0"></option>');

					}
				}
			});
	//}
}

$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_specification'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_specification"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_specification'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select' || type == 'textarea') {
            disable = (orig[id].value == $(this).val());

        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});

});

function form_validation_edit()
{

	var specification_id=$("#edit_specification_id").val();
	var specification_name = $('input[name="edit_specification_name"]').val().trim();
	var specification_group = $('select[name="edit_specification_group"]').val().trim();
	var parent_category = $('select[name="edit_parent_category"]').val().trim();
	var categories = $('select[name="edit_categories"]').val().trim();
	var subcategories = $('select[name="edit_subcategories"]').val().trim();
	var edit_specification_value = $('textarea[name="edit_specification_value"]').val().trim();
	var edit_sort_order = $('select[name="edit_sort_order"]').val().trim();
	var view = document.querySelector('input[name="edit_view"]:checked').value;
	var sort_order_default = $('input[name="edit_sort_order_default"]').val().trim();
	
	var common_specification="";
			if($("#show_available_common_specification").length!=0){
				var common_specification=document.getElementById("show_available_common_specification").value;
			}

	   var err = 0;
	   if(!(specification_name.length>=3) || (edit_specification_value=='') || (edit_sort_order=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
				document.getElementById("edit_parent_category").disabled="";
				document.getElementById("edit_categories").disabled="";
				document.getElementById("edit_subcategories").disabled="";
				document.getElementById("edit_specification_group").disabled="";
			var form_status = $('<div class="form_status"></div>');		
			var form = $('#edit_specification');		
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();			
					 $.ajax({
							url: '<?php echo base_url()."admin/Catalogue/edit_specification/"?>',
							type: 'POST',
							data: $('#edit_specification').serialize()+"&common_specification="+common_specification,
							dataType: 'html',
							
						}).done(function(data)
						  {
							  form_status.html('') 
							if(data=="exist"){
								  swal({
								title:"Error", 
								text:"That name is already created. Try another.", 
								type: "error",
								allowOutsideClick: false
							}).then(function(){
					edit_specification_name_default=document.getElementById("edit_specification_name_default").value;
					document.getElementById("edit_specification_name").value=edit_specification_name_default;
								document.getElementById("edit_specification_name").focus();
								document.getElementById("edit_parent_category").disabled=true;
								document.getElementById("edit_categories").disabled=true;
								document.getElementById("edit_subcategories").disabled=true;
								document.getElementById("edit_specification_group").disabled=true;
								
							});
							  } 
								if(data==1)
								{	  									  
									swal({
										title:"Success", 
										text:"Specification is successfully updated!", 
										type: "success",
										allowOutsideClick: false
									}).then(function () {
										location.reload();

									});
								}
								if(data==0){								
									swal(
										'Oops...',
										'Error in form',
										'error'
									)	
								}
								
								
				}); 
}
}]);				
			}
	
	return false;
}

</script>
</div>
</body>

</html>