<html lang="en">
<head>


<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />

<style>
	.form-group{
		text-align:left;
	}
.btn-file {
	position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
.Editor-container{
 text-align:left;
}
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>
<style type="text/css">
.cursor-pointer {
    cursor: pointer !important;
}
.small_color_box{
	width: 30px;
	height: 30px;
	border: 1px solid #ccc;
	margin-top: 1px;
}

.loading {
	text-align: center; 
	margin:0;product
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
#wizardPictureSmall_picname{
	word-wrap: break-word;
}
#wizardPictureBig_picname{
	word-wrap: break-word;
}
#wizardPictureSmall_picname2{
	word-wrap: break-word;
}
#wizardPictureBig_picname2{
	word-wrap: break-word;
}
#wizardPictureSmall_picname3{
	word-wrap: break-word;
}
#wizardPictureBig_picname3{
	word-wrap: break-word;
}
#wizardPictureSmall_picname4{
	word-wrap: break-word;
}
#wizardPictureBig_picname4{
	word-wrap: break-word;
}
#wizardPictureSmall_picname5{
	word-wrap: break-word;
}
#wizardPictureBig_picname5{
	word-wrap: break-word;
}
#wizardPictureSmall_picname6{
	word-wrap: break-word;
}
#wizardPictureBig_picname6{
	word-wrap: break-word;
}
textarea.form-control {
    height:36px;
}
.CamListDiv {
    height: 450px;
    float: left;
    overflow: scroll;
    overflow-x:hidden;
}
.well{
	width:100%;
}
</style>

<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 


<script type="text/javascript">

function isNumber(evt) {

    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
	
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
	
    return true;
}	   
</script>
<script>

function showDivPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/vendors';
}
</script>

<?php //print_r($vendor);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row-fluid" id="editDiv3">
<div class="col-md-8 col-md-offset-2">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
<form name="create_inventory" id="create_inventory" enctype="multipart/form-data">
<input type="hidden" name="action" value="create">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				/*sku_id: {
					required: true,
					minlength: 3
				 },
				 moq: {
					required: true,
		     
				},
				max_oq: {
					required: true,
		      
				},
				stock: {
					required: true,
		      
				}*/
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>				
		
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Build My Catalog</h3>
		<div>	
		</div>
		<div class="tab-content">
			
		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">What Furniture Are You Uploading?</label>
					<select id="category_id" name="category_id" class="form-control" required/>
					<option value=""></option>
					<?php
					foreach($parent_category as $parent){
						?>
						<option value="<?php echo $parent->pcat_id; ?>"><?php echo $parent->pcat_name; ?></option>
						<?php
					}
					?>
					
					</select>
				</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter SKU Name</label>
					<input id="sku_name" name="sku_name" type="text" class="form-control" value=""/>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter SKU ID</label>
					<input id="sku_id" name="sku_id" type="text" class="form-control" value=""/>
				</div>
				</div>
			</div>

			<!-- <div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Material</label>
					<input id="material" name="material" type="text" class="form-control" value=""/>
				</div>
				</div>
			</div> -->

			<!-- <div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">description</label>
					<textarea id="description" name="description" type="text" class="form-control"></textarea>
				</div>
				</div>
			</div> -->

			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">End Customer Selling Price (Incl. All Taxes) (Rs.)</label>
					<input id="offer_price" onKeyPress="return isNumber(event)" name="offer_price" type="text" class="form-control" value=""  onkeyup="calculateTax_priceFun();"/>
				</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label"><?php echo SITE_NAME; ?> Margin Percentage(%)</label>
					<input id="admin_margin_percentage" maxlength="2" onKeyPress="return isNumber(event)" name="admin_margin_percentage" type="text" class="form-control" value="" onkeyup="calculateTax_priceFun();"/>
				</div>
				</div>
			</div>
			<div class="row">
			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating" id="taxable_price_div" >
					<label class="control-label">Actual <?php echo SITE_NAME; ?> Margin (Rs.)</label>
					<input name="admin_margin_value" id="taxable_price" type="text" class="form-control" readonly/>
				</div>
			</div>

			</div>

			<div class="row">
			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">Price Validity</label>
					<input name="price_validity" id="price_validity" type="text" class="form-control"/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">Material Type of SKU (Wood, Steel, etc., )</label>
					<input name="material" id="material" type="text" class="form-control"/>
				</div>
			</div>
			
		</div>

		<div class="row">
			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">Enter Minimum Order Quantity Per Order</label>
					<input name="moq" type="number" class="form-control" min="1" value="1" />
				</div>
			</div>
			</div>

		<div class="row">
			<div class="col-md-10 col-md-offset-1"> 	
			
				<div class="form-group label-floating">
					<label class="control-label">Enter Maximum Order Quantity Per Order</label>
					<input name="max_oq" type="number" class="form-control" min="1" value="1000"/>
				</div>
			</div>
		</div>

		<div class="row">
			
			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">
					<label class="control-label">- Select A Relavent Stock Status -</label>
					<select name="product_status" id="product_status" class="form-control search_select" onchange="show_input(this.value)">
						<option value="" selected></option>
						<option value="In Stock" selected>Ready In Stock </option>
						<option value="Made To Order"> Made to Order</option>
						<option value="Out Of Stock">Out Of Stock</option>
						<!-- <option value="Discontinued">Discontinued</option> -->
					</select>
				</div>
			</div>

		</div>

		<!--- new fields--->

		<div class="row div_hide" id="stock_div_count" >

			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">	
					<label class="control-label">Currently How Many Units Are Available In Your Warehouse?</label>
					<input class="form-control" name="stock" id="stock" type="number" min="1">
				</div>
			</div>

		</div>

		<!--- new fields--->

		<div class="row div_hide" id="made_to_order_div" style="display:none">

			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">	
					<label class="control-label">How Many Days It takes To Manufacture One Piece?</label>
					<input class="form-control" name="manufacture_days_per_piece" id="manufacture_days_per_piece" type="number" min="1">
				</div>
			</div>

		</div>

		
		<div class="row div_hide" id="out_of_stock_div" style="display:none">

			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">	
					<label class="control-label">By When This SKU Will Be Available?</label>
					<input class="form-control" name="sku_available_date" id="sku_available_date" type="text" >
				</div>
			</div>

		</div>


		<div class="row div_hide" id="discontinuation_div" style="display:none">

			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">	
					<label class="control-label">Discontinued Date</label>
					<input type="text" class="form-control" name="discontinued_date" id="discontinued_date">
				</div>
			</div>

			<div class="col-md-10 col-md-offset-1"> 
				<div class="form-group label-floating">	
					<label class="control-label">Please Provide the Discontinuation Comments</label>
					<textarea class="form-control" name="discontinuation_comments" id="discontinuation_comments"></textarea> 
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-10 col-md-offset-1"> 	
			<h4>Product Description</h4>
					<textarea class="form-control" name="highlight" id="highlight"></textarea>
			</div>
		</div>

		<div class="row">
				<div class="col-md-10"> 
					<div class="form-group label-floating">
					<div class="col-md-5 col-md-offset-1">Do You Offer EMI Options ?</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="emi_options" id="emi1" value="yes" type="radio" >Yes</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="emi_options" id="emi2" value="no" type="radio">No</label>
					</div>
				
					</div>
				</div>
			</div>	

			<!---- new fields---->
			<div class="row">
			<div class="" id="inventory_unit_div">
			<div class="form-group">
				<div class="col-md-12 col-md-offset-1">Dimension Of Furniture (L X W X H)</div>
			</div>
				<div class="col-md-2 col-md-offset-1"> 
					<div class="form-group">
						<label class="control-label">Select Unit
						</label>
						<select class="form-control" name="inventory_unit" id="inventory_unit" >
							<option value=""></option>
							<option value="mm">mm</option>
							<option value="cm">cm</option>
							<option value="m">m</option>
						</select>
					</div>
				</div>
			</div>	
			<div class="" id="inventory_length_div">
				<div class="col-md-3"> 
					<div class="form-group">
						<label class="control-label">Length
						</label>
						<input class="form-control" name="inventory_length" id="inventory_length" min="1" type="number" step="any"/>
					</div>
				</div>
			</div>	
			<div class="" id="inventory_breadth_div">
				<div class="col-md-3 "> 
					<div class="form-group">
						<label class="control-label">Breadth
						</label>
						<input class="form-control" name="inventory_breadth" id="inventory_breadth" min="1" type="number" step="any"/>
					</div>
				</div>
			</div>	
			<div class="" id="inventory_height_div">
				<div class="col-md-3"> 
					<div class="form-group">
						<label class="control-label">Height
						</label>
						<input class="form-control" name="inventory_height" id="inventory_height" min="1" type="number" step="any"/>
					</div>
				</div>
			</div>

			</div>

			<!---new fields--->

				
			<div class="row">

				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group label-floating">	
						<label class="control-label">Weight Per Unit in Kg</label>
						<input class="form-control" name="weight_per_unit" id="weight_per_unit" type="text" >
					</div>
				</div>

			</div>
			
			<div class="row" style="display:none;">

				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group label-floating">	
						<label class="control-label">Dimension Of Furniture (L X W X H)</label>
						<input class="form-control" name="dimension_of_furniture" id="dimension_of_furniture" type="text" >
					</div>
				</div>

			</div>

			<div class="row">
						<div class="col-md-10 col-md-offset-1">
						<div class="form-group label-floating">	
						<label class="control-label">Shipped In XX Number Of Days</label>
							<select type="text" name="shipswithin" id="shipswithin" class="form-control" >
								<option value=""></option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="10">10</option>
								<option value="15">15</option>
								<option value="20">20</option>
								<option value="30">30</option>
								<option value="45">45</option>
								<option value="60">60</option>
							
							</select>
				</div>
						</div>
					</div>
						
				<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
										
					<div class="form-group">

					<label class="">Upload Product Brouchure</label>

						<div class="files" id="files1">
							<span class="btn btn-default btn-file">
								Browse  <input type="file" name="brouchure" />
							</span>
							<br />
							<ul class="fileList" style="width:100%;"></ul>
						</div>

<!----new --->
					</div>
				
					</div>
				</div>

			

			</div>	

			
			<div class="row">

			<div class="col-md-10 col-md-offset-1">
				<div class="form-group">	
					<div class="col-md-6">
						<label class="" for="brouchure">Frontend Display of Product Under </label>
					</div>
						<div class="col-md-6">
							
								<?php
								foreach($parent_category as $parent){
									?>
									<input type="checkbox" name="under_pcat_frontend[]" id="typ_<?php echo $parent->pcat_id; ?>" value="<?php echo $parent->pcat_id; ?>"> <label class="" for="typ_<?php echo $parent->pcat_id; ?>"><?php echo $parent->pcat_name; ?> </label>
									<?php
								}
								?>
							
							</div>
							</div>
							</div>
							</div>
			<!---new fields--->

			<br>
			<br>
			<br>
			<!---common--image--->
		<div class="row">
			<div class="col-md-5 col-md-offset-3" id="inv_images_div">
			<div class="col-md-12 text-center"><b>Common (300x366)</b></div>
				<div class="col-md-12">
				<div class="picture-container">
					<div class="picture" id="commonPic">
						<img class="picture-src" id="wizardPictureCommon" title=""/>
								
						<input type="file" name="common_image" id="wizard-picture-Common"/>
					</div>
				<h6 id="wizardPictureCommon_picname">Common Image</h6>
				</div>
				</div>	
			</div>
		</div>
		<!---common--image--->
		
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-left">
			<button name="add_image_inventory" id="add_image_inventory_btn" type="button" class="btn btn-success pull-left" disabled onclick="add_image_inventory_fun()">Add Pair</button>
			</div>
	
		</div>
		<div class="row">
		
		
			<!---- first pair starts --->
			<div class="col-md-10 col-md-offset-1" id="inv_images_div_1">
			<div class="col-md-10 col-md-offset-1"><b>FIRST PAIR</b></div>
			
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic1">
						<img class="picture-src" id="wizardPictureSmall1" title=""/>
				
						<input type="file" name="thumbnail1" id="wizard-picture-small1"onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname1">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic1">
						<img class="picture-src" id="wizardPictureBig1" title=""/>
				
						<input type="file" name="image1" id="wizard-picture-big1" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname1">Medium Image (420x512)</h6>
				</div>
				</div>
				
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic1">
						<img class="picture-src" id="wizardPictureLarge1" title=""/>
				
						<input type="file" name="largeimage1" id="wizard-picture-large1" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname1">Big Image (850x1036)</h6>
				</div>
				</div>
				
				
			</div>
			<!---- first pair ends --->
			<!---- second pair starts --->
			<div class="row"  id="inv_images_div_2">
			<div class="col-md-10 col-md-offset-1"><b>SECOND PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(2)"></i></b></div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic2">
						<img class="picture-src" id="wizardPictureSmall2" title=""/>
				
						<input type="file" name="thumbnail2" id="wizard-picture-small2" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname2">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic2">
						<img class="picture-src" id="wizardPictureBig2" title=""/>
				
						<input type="file" name="image2" id="wizard-picture-big2" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname2">Medium Image (420x512)</h6>
				</div>
				</div>
				
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic2">
						<img class="picture-src" id="wizardPictureLarge2" title=""/>
				
						<input type="file" name="largeimage2" id="wizard-picture-large2" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname2">Big Image (850x1036)</h6>
				</div>
				</div>
				
				
			</div>
			<!---- second pair ends --->
		</div>
		<div class="row">
			<!---- third pair starts --->
			<div class="col-md-10 col-md-offset-1" id="inv_images_div_3">
			<div class="col-md-10 col-md-offset-1"><b>THIRD PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(3)"></i></b></div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic3">
						<img class="picture-src" id="wizardPictureSmall3" title=""/>
				
						<input type="file" name="thumbnail3" id="wizard-picture-small3" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname3">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic3">
						<img class="picture-src" id="wizardPictureBig3" title=""/>
				
						<input type="file" name="image3" id="wizard-picture-big3" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname3">Medium Image (420x512)</h6>
				</div>
				</div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic3">
						<img class="picture-src" id="wizardPictureLarge3" title=""/>
				
						<input type="file" name="largeimage3" id="wizard-picture-large3" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname3">Big Image (850x1036)</h6>
				</div>
				</div>
			</div>
			<!---- third pair ends --->
			<!---- fourth pair starts --->
			<div class="row" id="inv_images_div_4">
			<div class="col-md-10 col-md-offset-1"><b>FOURTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;"  onclick="hide_inv_imagesFun(4)"></i></b></div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic4">
						<img class="picture-src" id="wizardPictureSmall4" title=""/>
				
						<input type="file" name="thumbnail4" id="wizard-picture-small4" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname4">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic4">
						<img class="picture-src" id="wizardPictureBig4" title=""/>
				
						<input type="file" name="image4" id="wizard-picture-big4" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname4">Medium Image (420x512)</h6>
				</div>
				</div>
				
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic4">
						<img class="picture-src" id="wizardPictureLarge4" title=""/>
				
						<input type="file" name="largeimage4" id="wizard-picture-large4" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname4">Big Image (850x1036)</h6>
				</div>
				</div>
				
			</div>
			<!---- fourth pair ends --->
			<div class="row">
			<!---- fifth pair starts --->
			<div class="col-md-10 col-md-offset-1" id="inv_images_div_5">
			<div class="col-md-10 col-md-offset-1"><b>FIFTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(5)"></i></b></div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic5">
						<img class="picture-src" id="wizardPictureSmall5" title=""/>
				
						<input type="file" name="thumbnail5" id="wizard-picture-small5" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname5">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic5">
						<img class="picture-src" id="wizardPictureBig5" title=""/>
				
						<input type="file" name="image5" id="wizard-picture-big5" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname5">Medium Image (420x512)</h6>
				</div>
				</div>
				
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic5">
						<img class="picture-src" id="wizardPictureLarge5" title=""/>
				
						<input type="file" name="largeimage5" id="wizard-picture-large5" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname5">Big Image (850x1036)</h6>
				</div>
				</div>
				
			</div>
			<!---- fifth pair ends --->
			
			
			
			<!---- sixth pair starts --->
			<div class="row" id="inv_images_div_6">
			<div class="col-md-10 col-md-offset-1"><b>SIXTH PAIR <i class="fa fa-times" aria-hidden="true" style="cursor:pointer;" onclick="hide_inv_imagesFun(6)"></i></b></div>
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="smallpic6">
						<img class="picture-src" id="wizardPictureSmall6" title=""/>
				
						<input type="file" name="thumbnail6" id="wizard-picture-small6" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
				<h6 id="wizardPictureSmall_picname6">Small Image (100x122)</h6>
				</div>
				</div>	
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="bigpic6">
						<img class="picture-src" id="wizardPictureBig6" title=""/>
				
						<input type="file" name="image6" id="wizard-picture-big6" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureBig_picname6">Medium Image (420x512)</h6>
				</div>
				</div>
				
				<div class="col-md-4">
				<div class="picture-container">
					<div class="picture" id="largepic6">
						<img class="picture-src" id="wizardPictureLarge6" title=""/>
				
						<input type="file" name="largeimage6" id="wizard-picture-large6" onchange="enableDisabledAddInventoryButtonFun()"/>
					</div>
					<h6 id="wizardPictureLarge_picname6">Big Image (850x1036)</h6>
				</div>
				</div>
				
			</div>
			<!---- sixth pair ends --->
			
			
			
			</div>
		</div>
	</div>
			<!---new fields--->
			<script>
		function hide_inv_imagesFun(div_index_id){
			
			document.getElementById("wizard-picture-small"+div_index_id).value="";
			document.getElementById("wizard-picture-big"+div_index_id).value="";
			document.getElementById("wizard-picture-large"+div_index_id).value="";
			
			document.getElementById("wizardPictureSmall"+div_index_id).removeAttribute("src");
			document.getElementById("wizardPictureBig"+div_index_id).removeAttribute("src");
			document.getElementById("wizardPictureLarge"+div_index_id).removeAttribute("src");
			
			document.getElementById("wizardPictureSmall_picname"+div_index_id).innerHTML="SMALL IMAGE (100x122)";
			document.getElementById("wizardPictureBig_picname"+div_index_id).innerHTML="Medium IMAGE";
			document.getElementById("wizardPictureLarge_picname"+div_index_id).innerHTML="Big IMAGE";
			
			document.getElementById("inv_images_div_"+div_index_id).style.display="none";
			enableDisabledAddInventoryButtonFun();
		}
		function enableDisabledAddInventoryButtonFun(){
			small_image_count=0;
			big_image_count=0;
			large_image_count=0;
			for(i=1;i<=6;i++){
				if(document.getElementById("inv_images_div_"+i).style.display==""){
					if(document.getElementById("wizard-picture-small"+i).value!=""){
						small_image_count++;
					}
					if(document.getElementById("wizard-picture-big"+i).value!=""){
						big_image_count++;
					}
					if(document.getElementById("wizard-picture-large"+i).value!=""){
						large_image_count++;
					}
				}
			}
			
			if(small_image_count==0 && big_image_count==0 && large_image_count==0){
				document.getElementById("add_image_inventory_btn").disabled=true;
			}
			else{
				if((small_image_count==big_image_count) && (big_image_count==large_image_count)){
					document.getElementById("add_image_inventory_btn").disabled=false;
					document.getElementById("add_inventory_finish_btn").disabled=false;
				}
				else{
					document.getElementById("add_image_inventory_btn").disabled=true;
					document.getElementById("add_inventory_finish_btn").disabled=true;
					
				}
			}
		}
		function initializeInventoryImageDivFun(){
			for(i=2;i<=6;i++){
				document.getElementById("inv_images_div_"+i).style.display="none";
			}
		}
		function add_image_inventory_fun(){
			for(i=2;i<=6;i++){
				if(document.getElementById("inv_images_div_"+i).style.display=="none"){
					document.getElementById("inv_images_div_"+i).style.display="";
					document.getElementById("add_image_inventory_btn").disabled=true;
					break;
				}
			}
		}
		initializeInventoryImageDivFun();
		enableDisabledAddInventoryButtonFun();
	</script>
	
	</div>		                        
	<div class="wizard-footer">
		<!-- <div class="pull-right">
			
			<input type='button' class='btn btn-finish btn-fill btn-success btn-wd' name='finish' id="add_inventory_finish_btn" value='Finish' onClick="filter_details_form_valid()" />
		</div> -->

		
		<div class="clearfix"></div>
	</div>
	
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
				<div class="text-center">
					<button class="btn btn-finish btn-fill btn-success btn-wd" type="button" id="submit-data" onClick="filter_details_form_valid()"></i>Submit</button>
					<?php
					$admin_user_type=$this->session->userdata("user_type");
					 ?>
					
				</div>	
				</div>
			</div>		
	
	</div>
</form>

</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>
</div>
</body>
</html>
<script>

$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_vendors'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_vendors"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_vendors'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});

});

function form_validation_edit()
{
	var name = $('input[name="edit_name"]').val().trim();
	var email = $('input[name="edit_email"]').val().trim();
	var mobile = $('input[name="edit_mobile"]').val().trim();
	var pincode = $('input[name="edit_pincode"]').val().trim();
	var edit_address1 = $('input[name="edit_address1"]').val().trim();
	var edit_address2 = $('input[name="edit_address2"]').val().trim();
	var edit_landline = $('input[name="edit_landline"]').val().trim();

	   var err = 0;
	   if((name=='') || (email=='') || (mobile=='') || (pincode=='') || (edit_address1=='') || (edit_address2=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
var form_status = $('<div class="form_status"></div>');		
var form = $('#edit_vendors');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();		
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_vendors/"?>',
				type: 'POST',
				data: $('#edit_vendors').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				});
				  }  
				if(data==1)
				{
										  
					swal({
						title:"Success", 
						text:"Vendors is successfully updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				if(data==0)
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
			});
}
}]);			
			}
	
	return false;
}

function get_city_state_details_by_pincodeFun(){

		pin=$("#edit_pincode").val();
		
		if(pin===undefined){
		}
		else{
		$.ajax({
				method : "POST",
				url : "<?php echo base_url();?>Account/get_city_state_details_by_pincode",
				data:{'pin' : pin},
				dataType:"JSON",
				}).success(function mySucces(result) {
					$("#city").val(result.city);
					$("#state").val(result.state);
					$("#country").val("India");
					
					if(parseInt(result.count)==0){
                        
                        $('.delivery_address_pin_error').show();
                        $('#pin_code_check').addClass('has-error');
                    }else{
                        //alert(result.count+"no error");
                        
                        $('.delivery_address_pin_error').hide();
                        $('#pin_code_check').removeClass('has-error');
                    }

				}, function myError(response) {
					
				});
		}	
	 }

function filter_details_form_valid()
{
	
	var form_status = $('<div class="form_status"></div>');		
	var form = $('#create_inventory');	
	var err = 0;
		var image = $('input[name="image1"]').val().trim();
		var thumbnail = $('input[name="thumbnail1"]').val().trim();
		if(image=='')
		{
		   $('div[class="picture"]').css("border", "2px solid #f44336");	   
		   err = 1;
		}
		else{
			$('div[class="picture"]').css({"border": "2px solid #ccc"});
		}
		if(thumbnail=='')
		{
		   $('div[class="picture"]').css("border", "2px solid #f44336");	   
		   err = 1;
		}
		else{
			$('div[class="picture"]').css({"border": "2px solid #ccc"});
		}
		
	if(err==1)
	{
		//$('#validation_error').show();
		return false;
		
	}else{
			var data = $("#highlight").Editor("getText");
			$("#highlight").val(data);
			
			
			var form = $('#create_inventory');	
			var form_create = document.forms.namedItem("create_inventory");
			var ajaxData = new FormData(form_create);
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_vendor_catalog/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false, 		
				
			}).done(function(data)
			  {	
				if(data==true)
				{
					  swal({
						title:"Success", 
						text:"Inventory is successfully Created!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				else
				{
					swal(
						'Oops...',
						data,
						'error'
					)	
				}
		return true;
	});
	}
}]);
				
		return true;
	}
}
</script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/richte/editor.js" ></script>
<script>
$(document).ready(function() {
	var textarea = $("#highlight");
	textarea.Editor();
	
});
$(document).ready(function (){
	$('#price_validity').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		weekStart: 0, 
		format: 'YYYY-MM-DD', 
		shortTime : true
	}).on('change', function(e, date){
		$('#price_validity').bootstrapMaterialDatePicker('setMinDate', date);
	});
	$('#discontinued_date').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		weekStart: 0, 
		format: 'YYYY-MM-DD', 
		shortTime : true
	}).on('change', function(e, date){
		$('#discontinued_date').bootstrapMaterialDatePicker('setMinDate', date);
	});
	
	$('#sku_available_date').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		weekStart: 0, 
		format: 'YYYY-MM-DD', 
		shortTime : true
	}).on('change', function(e, date){
		$('#sku_available_date').bootstrapMaterialDatePicker('setMinDate', date);
	});
});

function calculateTax_priceFun(){
	 
	 tax=$("#admin_margin_percentage").val();
	 msp=$("#max_selling_price").val();
	 offer_price=$("#offer_price").val();
	 
	 if(offer_price!="" && tax!=""){

		//parseInt(offer_price)-
		 taxable_price=(Math.round(parseFloat(offer_price)*parseFloat(tax)/100));//.toFixed(2)

		 //$("#tax_percent_price").val(tax_percent_price);
		 $("#taxable_price").val(taxable_price);
		
		 $("#taxable_price_div").removeAttr("class");
		 $("#taxable_price_div").attr("class","form-group label-floating is-empty is-focused");
	 }else{
		//alert('Please Enter Valid Selling Price for End Customer and Vommet Studio Margin Percentage')
	 }
	  /* calculate tax price and taxable vale */
}

$.fn.fileUploader = function (filesToUpload) {
    this.closest(".files").change(function (evt) {

        for (var i = 0; i < evt.target.files.length; i++) {
            filesToUpload.push(evt.target.files[i]);
        };
        var output = [];

        for (var i = 0, f; f = evt.target.files[i]; i++) {
            var removeLink = "<a style=\"color:red !important;padding: 10px;\" class=\"removeFile\" href=\"#\" data-fileid=\"" + i + "\">Remove</a>";

            output.push("<li style='text-align: center;width: 100%;'><strong>", escape(f.name), "</strong> - ",
                f.size, " bytes. &nbsp; &nbsp; ", removeLink, "</li> ");
        }

        $(this).children(".fileList")
            .html(output.join(""));
    });
};

var filesToUpload = [];

$(document).on("click",".removeFile", function(e){
    e.preventDefault();
    var fileName = $(this).parent().children("strong").text();
     // loop through the files array and check if the name of that file matches FileName
    // and get the index of the match
    for(i = 0; i < filesToUpload.length; ++ i){
        if(filesToUpload[i].name == fileName){
            //console.log("match at: " + i);
            // remove the one element at the index where we get a match
            filesToUpload.splice(i, 1);
        }	
	}
    //console.log(filesToUpload);
    // remove the <li> element of the removed file from the page DOM
    $(this).parent().remove();
});

$("#files1").fileUploader(filesToUpload);

$("#uploadBtn").click(function (e) {
	e.preventDefault();
});

function show_input(val){
	$('.div_hide').hide();
	
	if(val!=''){
		if(val=='In Stock'){
			$('#stock_div_count').show();
		}
		if(val=='Made To Order'){
			$('#made_to_order_div').show();
		}
		if(val=='Out Of Stock'){
			$('#out_of_stock_div').show();
		}
	}else{
		$('#div_hide').hide();
	}
}

</script>