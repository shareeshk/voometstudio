<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Refund Request Orders - Customer Accepted</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<style>
.form-group{
	margin-bottom:10px;
	margin-top:5px;
	padding-bottom:0;
}
</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript">
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header"><h4 class="text-center">Issue refund</h4></div>
<div class="row">
	<div class="col-md-12">
		 <ul class="nav nav-pills nav-pills-info page-header" role="tablist">
			<li class="active"><a href="#fullrefund-tab" role="tab" data-toggle="tab">Full Refund</a></li>
			<li><a href="#partialrefund-tab" role="tab" data-toggle="tab">Partial Refund</a></li>
		</ul>
			<div class="tab-content">
				<!---------------- landline tab start --------------------->
				<div class="tab-pane active" id="fullrefund-tab">
					<form id="full_refund_form" class="form-horizontal">
						<input type="hidden" name="return_order_id" value="<?php echo $order_return_decision_data_obj->return_order_id;?>">
						<input type="hidden" name="quantity_refunded" value="<?php echo $order_return_decision_data_obj->quantity;?>">
						<input type="hidden" name="order_item_id" value="<?php echo $order_return_decision_data_obj->order_item_id;?>">
						<input type="hidden" name="customer_id" value="<?php echo $order_return_decision_data_obj->customer_id;?>">
						
						<?php
						//print_r($returns_desired_data_obj);
						$promotion_available=$returns_desired_data_obj->promotion_available;
						//echo $order_return_decision_data_obj->cash_back_value;
						if($promotion_available==1){
							$product_price=abs(floatval($order_return_decision_data_obj->total_price_of_product_with_promotion)+floatval($order_return_decision_data_obj->total_price_of_product_without_promotion));
							
							if($order_return_decision_data_obj->cash_back_value>0){
								$product_price-=floatval($order_return_decision_data_obj->cash_back_value);
							}
							if($order_return_decision_data_obj->promotion_surprise_gift_type==curr_code){
								if($returns_desired_data_obj->purchased_qty==$order_return_decision_data_obj->quantity){
									$product_price-=floatval($order_return_decision_data_obj->promotion_surprise_gift);
								}
							}
							
							if($returns_desired_data_obj->promotion_invoice_discount>0){
								
								$order_item_invoice_discount_value_each=$returns_desired_data_obj->order_item_invoice_discount_value_each;
								
								$inv_discount=round($order_item_invoice_discount_value_each*$order_return_decision_data_obj->quantity,2);
								$product_price-=floatval($inv_discount);
							}
							
							$shipping_charge=round($order_return_decision_data_obj->quantity*$order_return_decision_data_obj->shipping_charge);
							
						}else{
							
							$product_price=$order_return_decision_data_obj->quantity*$order_return_decision_data_obj->product_price;
							if($returns_desired_data_obj->promotion_invoice_discount>0){
								
								$order_item_invoice_discount_value_each=$returns_desired_data_obj->order_item_invoice_discount_value_each;
								
								$inv_discount=round($order_item_invoice_discount_value_each*$order_return_decision_data_obj->quantity,2);
								$product_price-=floatval($inv_discount);
							}
							$shipping_charge=round($order_return_decision_data_obj->quantity*$order_return_decision_data_obj->shipping_charge);
						}
						?>
						<input type="hidden" name="item_price_without_concession" value="<?php echo $product_price;?>">
						
						<input type="hidden" name="item_deduction_percentage" value="0">
						<input type="hidden" name="shipping_price_without_concession" value="<?php echo round($order_return_decision_data_obj->quantity*$order_return_decision_data_obj->shipping_charge);?>">
						
						<input type="hidden" name="shipping_deduction_percentage" value="0">
						
						<input type="hidden" name="item_with_deductions_concession" value="<?php echo $product_price;?>">
						
						<input type="hidden" name="shipping_with_deductions_concession" value="<?php echo round($order_return_decision_data_obj->quantity*$order_return_decision_data_obj->shipping_charge);?>">
						
						<div class="form-group">
							<div class="col-md-3">Reason Refund:</div>
							<div class="col-md-9">
							
								<?php
									
									if(strlen($returns_desired_data_obj->return_reason_comments)>200){
										echo '<div class="f-row margin-top more">'.str_split($returns_desired_data_obj->return_reason_comments,200)[0].'<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' .substr($returns_desired_data_obj->return_reason_comments,201). '</span>&nbsp;&nbsp;<a href="#" class="morelink" onclick="morelinkfun(this)">more</a></span></div>';
									}else{
										echo '<div class="f-row margin-top more">'.$returns_desired_data_obj->return_reason_comments.'</div>';
									}
									
								?>
								
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3">Quantity Requested:</div>
							<div class="col-md-9">
								<?php
									echo $order_return_decision_data_obj->quantity;
								?>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-3">Total Product Price:</div>
							<div class="col-md-9">
								<?php
									echo curr_sym.$product_price;
								?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3">Shipping Price for (<?php echo $order_return_decision_data_obj->quantity; ?> quantity ) :</div>
							<div class="col-md-9">
								<?php
									echo curr_sym.$shipping_charge;
								?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3">Total Refund:</div>
							<div class="col-md-9">
							<?php
							
							if($promotion_available==1){
								//$total_refund=($order_return_decision_data_obj->product_price_with_offer+($order_return_decision_data_obj->shipping_charge*$order_return_decision_data_obj->quantity));
								$total_refund=($product_price)+round($order_return_decision_data_obj->shipping_charge*$order_return_decision_data_obj->quantity);
							
							}else{
								$total_refund=(($product_price)+round($order_return_decision_data_obj->shipping_charge*$order_return_decision_data_obj->quantity));
							}
							
							echo curr_sym.$total_refund;
								
							?>
							<input type="hidden" id="total_refund_without_concession_full_refund" name="total_refund_without_concession" value="<?php echo $total_refund;?>">
							
							</div>
						</div>
						<div class="form-group">
						<div class="col-md-3">Return Shipping Concession:</div>
							<div class="col-md-4">
								<input type="text" id="return_shipping_concession_full_refund" name="return_shipping_concession" value="0" size="5" onblur="calculate_total_refund_with_concession()" onkeypress="return isNumber(event);" class="form-control" placeholder="Return Shipping Concession"> 
								</div>
								<div class="col-md-5">
								Maximum suggestion is <?php echo curr_sym; ?><?php echo 2*round($order_return_decision_data_obj->shipping_charge*$order_return_decision_data_obj->quantity);?>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3">Other Concession:</div>
							<div class="col-md-4">
								<input type="text" id="return_other_concession_full_refund" name="return_other_concession" value="0" size="5" max="20" onblur="calculate_total_refund_with_concession()" onkeypress="return isNumber(event);" class="form-control">
								</div>
								<div class="col-md-5">
								Maximum suggestion is <?php
								echo curr_sym.round($order_return_decision_data_obj->shipping_charge*$order_return_decision_data_obj->quantity)/2
								?>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3">Total Refund (with concession):</div>
							<div class="col-md-9" id="total_refund_with_concession_full_refund_div"></div>
							
							<input type="hidden" id="total_refund_with_concession_full_refund" name="total_refund_with_concession" value="">
							
						</div>
						<div class="form-group">
							<div class="col-md-3">Memo to buyer:</div>
							<div class="col-md-4">
								<textarea class="form-control" name="memo_to_buyer" required></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3">Seller Memo:</div>
							<div class="col-md-4">
								<textarea class="form-control" name="memo_to_seller" required></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3">Refund Method:</div>
							<div class="col-md-4">
							<input type="hidden" name="refund_method" value="<?php echo $returns_desired_data_obj->refund_method;?>">
							<input type="hidden" name="refund_bank_id" value="<?php echo $returns_desired_data_obj->refund_bank_id;?>">
								<?php
									if($returns_desired_data_obj->refund_method=="Wallet" || $returns_desired_data_obj->refund_method=="Razorpay" ){
										echo $returns_desired_data_obj->refund_method;
									}
									if($returns_desired_data_obj->refund_method=="Bank Transfer"){
										echo $returns_desired_data_obj->refund_method;
										echo $controller->get_bank_details($returns_desired_data_obj->refund_bank_id);
									}
								?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-4 col-md-offset-3">
								<input class="btn btn-success btn-xs btn-block" type="button" name="full_refund_btn" value="Issue Full Refund" onclick="full_refund_fun()">
							</div>
						</div>
					</form>
				</div>
				<!---- partial refund tab starts--------------->
				<div class="tab-pane" id="partialrefund-tab">
				<form id="partial_refund_form" class="form-horizontal">
						<input type="hidden" name="return_order_id" value="<?php echo $order_return_decision_data_obj->return_order_id;?>">
						<input type="hidden" name="order_item_id" value="<?php echo $order_return_decision_data_obj->order_item_id;?>">
						<input type="hidden" name="quantity_refunded" value="<?php echo $order_return_decision_data_obj->quantity;?>">
						<input type="hidden" name="customer_id" value="<?php echo $order_return_decision_data_obj->customer_id;?>">
						
						<div class="form-group">
							<div class="col-md-3">Quantity Requested:</div>
							<div class="col-md-9">
								<?php
									echo $order_return_decision_data_obj->quantity;
								?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3">SKU:</div>
							<div class="col-md-9">
								<?php
									echo $controller->get_inventory_info_by_inventory_id($order_return_decision_data_obj->inventory_id)->sku_id;
								?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3">Order Item ID:</div>
							<div class="col-md-9">
								<?php
									echo $order_return_decision_data_obj->order_item_id;
								?>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-3">Refund Method:</div>
							<div class="col-md-9">
							<input type="hidden" name="refund_method" value="<?php echo $returns_desired_data_obj->refund_method;?>">
							<input type="hidden" name="refund_bank_id" value="<?php echo $returns_desired_data_obj->refund_bank_id;?>">
								<?php
									if($returns_desired_data_obj->refund_method=="Wallet" || $returns_desired_data_obj->refund_method=="Razorpay"){
										echo $returns_desired_data_obj->refund_method;
									}
									if($returns_desired_data_obj->refund_method=="Bank Transfer"){
										echo $returns_desired_data_obj->refund_method;
										echo $controller->get_bank_details($returns_desired_data_obj->refund_bank_id);
									}
								?>
							</div>
						</div>
						
						
						<div class="form-group">
							<div class="col-md-3"><b>Reason For Refund:</b></div>
							<div class="col-md-9">

								<?php									
									if(strlen($returns_desired_data_obj->return_reason_comments)>200){
										echo '<div class="f-row margin-top more">'.str_split($returns_desired_data_obj->return_reason_comments,200)[0].'<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' .substr($returns_desired_data_obj->return_reason_comments,201). '</span>&nbsp;&nbsp;<a href="#" class="morelink" onclick="morelinkfun(this)">more</a></span></div>';
									}else{
										echo '<div class="f-row margin-top more">'.$returns_desired_data_obj->return_reason_comments.'</div>';
									}		
								?>
							</div>
						</div>
						<?php
							$total_product_price_with_shipping=0;
							$total_max_amount=0;
							//print_r($order_return_decision_data_obj);
							$promotion_available=$returns_desired_data_obj->promotion_available;
						?>
						<div class="form-group">
						<div class="col-md-12">
						<table class="table table-striped table-bordered" cellspacing="0" width="100%">
							<tr>
								<th>Details</th>
								<th>Order amount</th>
								<th>Deductions in percentage</th>
								<th>Amount to refund</th>
								<th>Max amount Suggestion</th>
							</tr>
							<tr>
								<td>
									Product:
								</td>
								<td>
									<?php
										//check this calculation//
										
										if($promotion_available==1){
		
											
											$tot_pro_price=(floatval($order_return_decision_data_obj->total_price_of_product_with_promotion)+floatval($order_return_decision_data_obj->total_price_of_product_without_promotion));
											
											if($order_return_decision_data_obj->cash_back_value>0){
												$tot_pro_price-=floatval($order_return_decision_data_obj->cash_back_value);
											}
											
											if($order_return_decision_data_obj->promotion_surprise_gift_type==curr_code){
												if($returns_desired_data_obj->purchased_qty==$order_return_decision_data_obj->quantity){
													$tot_pro_price-=floatval($order_return_decision_data_obj->promotion_surprise_gift);
												}
											}
											if($returns_desired_data_obj->promotion_invoice_discount>0){
												$order_item_invoice_discount_value_each=$returns_desired_data_obj->order_item_invoice_discount_value_each;
												
												$inv_discount=round($order_item_invoice_discount_value_each*$order_return_decision_data_obj->quantity,2);
												$tot_pro_price-=floatval($inv_discount);
											}

											$total_product_price_with_shipping+=$tot_pro_price;
											
											echo curr_sym.$tot_pro_price;
											
										}else{
											$tot_pro_price=$order_return_decision_data_obj->product_price*$order_return_decision_data_obj->quantity;
											if($returns_desired_data_obj->promotion_invoice_discount>0){
												$order_item_invoice_discount_value_each=$returns_desired_data_obj->order_item_invoice_discount_value_each;
												
												$inv_discount=round($order_item_invoice_discount_value_each*$order_return_decision_data_obj->quantity,2);
												$tot_pro_price-=floatval($inv_discount);
											}
											$total_product_price_with_shipping+=$tot_pro_price;
											echo curr_sym.$tot_pro_price;
										}
									
										
									?>
									<input type="hidden" name="order_amount_product" id="order_amount_product" value="<?php echo $tot_pro_price;?>">
								</td>
								<td>
									<input type="text" name="deductions_product" id="deductions_product" value="<?php echo $order_return_decision_data_obj->product_price_with_offer_percentage;?>" onkeypress="return isNumber(event);" onkeyup="cal_amount_to_refund_productFun(this.value);" readonly class="form-control">
								</td>
								<td>
									<input type="text" id="amount_to_refund_product" name="amount_to_refund_product" value="<?php echo $order_return_decision_data_obj->product_price_with_offer;?>" readonly class="form-control">
								</td>
								
								<td>
									<?php if($promotion_available==1){
											
										$total_pro_price=(floatval($order_return_decision_data_obj->total_price_of_product_with_promotion)+floatval($order_return_decision_data_obj->total_price_of_product_without_promotion));
										
										$total_max_amount+=$total_pro_price;
										
										echo curr_sym.$total_pro_price;
											
									}else{
										$total_pro_price=$order_return_decision_data_obj->product_price*$order_return_decision_data_obj->quantity;
										
										$total_max_amount+=$total_pro_price;
										
										echo curr_sym.$total_pro_price;
									}
										
									?>
								</td>
							</tr>
							<tr>
								<td>
									Shipping:
								</td>
								<td>
									<?php
										$total_product_price_with_shipping+=round($order_return_decision_data_obj->shipping_charge*$order_return_decision_data_obj->quantity);
										
										echo curr_sym.round($order_return_decision_data_obj->shipping_charge*$order_return_decision_data_obj->quantity);
									?>
									<input type="hidden" name="order_amount_shipping" id="order_amount_shipping" value="<?php echo round($order_return_decision_data_obj->shipping_charge*$order_return_decision_data_obj->quantity);?>">
								</td>
								<td>
									<input type="text" name="deductions_shipping" id="deductions_shipping" value="<?php echo $order_return_decision_data_obj->shipping_charge_with_offer_percentage;?>" onkeypress="return isNumber(event);"   onkeyup="cal_amount_to_refund_shippingFun(this.value);" readonly class="form-control">
								</td>
								<td>
									<input type="text" id="amount_to_refund_shipping" name="amount_to_refund_shipping" value="<?php echo round($order_return_decision_data_obj->shipping_charge*$order_return_decision_data_obj->quantity);?>" size="4" readonly class="form-control">
								</td>
								<td>
									<?php
										$total_max_amount+=round($order_return_decision_data_obj->shipping_charge*$order_return_decision_data_obj->quantity);
										
										echo curr_sym.round($order_return_decision_data_obj->shipping_charge*$order_return_decision_data_obj->quantity);
									?>
								</td>
							</tr>
							<tr>
								<td>
									Return shipping concession:
								</td>
								<td>
									----
								</td>
								<td>
									----
								</td>
								<td>
									<input type="text" id="amount_to_refund_return_shipping_concession" name="amount_to_refund_return_shipping_concession" value="0.00" onkeypress="return isNumber(event);" onkeyup="cal_total_amount_to_refundFun()" class="form-control">
								</td>
								<td>
									<?php
										$total_max_amount+=round($order_return_decision_data_obj->shipping_charge*$order_return_decision_data_obj->quantity)*2;
										
										echo curr_sym.round($order_return_decision_data_obj->shipping_charge*$order_return_decision_data_obj->quantity)*2;
									?>
								</td>
							</tr>
							<tr>
								<td>
									Other concession:
								</td>
								<td>
									----
								</td>
								<td>
									----
								</td>
								<td>
									<input type="text" id="amount_to_refund_other_concession" name="amount_to_refund_other_concession" value="0.00" onkeypress="return isNumber(event);" onkeyup="cal_total_amount_to_refundFun()" class="form-control">
								</td>
								<td>
									<?php
										$total_max_amount+=round($order_return_decision_data_obj->shipping_charge*$order_return_decision_data_obj->quantity)/2;
										
										echo curr_sym.round($order_return_decision_data_obj->shipping_charge*$order_return_decision_data_obj->quantity)/2;
									?>
								</td>
							</tr>
							<tr>
								<td>
									Total:
								</td>
								<td>
									<?php
										echo curr_sym.$total_product_price_with_shipping;
									?>
									<input type="hidden" name="total_product_price_with_shipping" id="total_product_price_with_shipping" value="<?php echo $total_product_price_with_shipping;?>">
								</td>
								<td>
									----
								</td>
								<td>
									<input type="hidden" name="total_amount_to_refund" id="total_amount_to_refund" value="0">
									<div id="total_amount_to_refund_div"></div>
								</td>
								<td>
									<?php
										echo curr_sym.$total_max_amount;
									?>
								</td>
							</tr>
						</table>
						</div>
						</div>
						<div class="form-group">
							<div class="col-md-3"><b>Memo to buyer:</b></div>
							<div class="col-md-4">
								<textarea class="form-control" name="memo_to_buyer_partial_refund"></textarea>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-3"><b>Seller memo:</b></div>
							<div class="col-md-4">
								<textarea class="form-control" name="memo_to_seller_partial_refund"></textarea>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-4 col-md-offset-3">
								<input class="btn btn-success btn-xs btn-block" type="button" name="partial_refund_btn" value="Issue Partial Refund" onclick="partial_refund_fun()">
							</div>
						</div>
					</form>
				</div>
				<!---- partial refund tab starts--------------->
			</div>
	</div>
</div>
</div>
<!------------------------------------->	
<!---------------------->
<script>
function calculate_total_refund_with_concession(){
	if(document.getElementById("return_shipping_concession_full_refund").value==""){
		document.getElementById("return_shipping_concession_full_refund").value=0;
	}
	var return_shipping_concession_full_refund=parseFloat(document.getElementById("return_shipping_concession_full_refund").value);
	/////////////////////////////////
	if(document.getElementById("return_other_concession_full_refund").value==""){
		document.getElementById("return_other_concession_full_refund").value=0;
	}
	var return_other_concession_full_refund=parseFloat(document.getElementById("return_other_concession_full_refund").value);
	//////////////////////////////////////////
	
	var total_refund_without_concession=parseFloat(document.getElementById("total_refund_without_concession_full_refund").value);
	total_refund_with_concession=return_other_concession_full_refund+return_shipping_concession_full_refund+total_refund_without_concession;
	document.getElementById("total_refund_with_concession_full_refund_div").innerHTML="<?php echo curr_sym;?>"+total_refund_with_concession;
	document.getElementById("total_refund_with_concession_full_refund").value=total_refund_with_concession;
}
calculate_total_refund_with_concession();
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
        return false;
    }
    return true;
}
function full_refund_fun(){
	/*var memo_to_buyer = $('textarea[name="memo_to_buyer"]').val().trim();
	var memo_to_seller = $('textarea[name="memo_to_seller"]').val().trim();
		if(memo_to_buyer==''){
			alert("pls fill all fields");
			return false;
		}
		if(memo_to_seller==''){
			alert("pls fill all fields");
			return false;
		}*/
	var return_shipping_concession_full_refund=document.getElementById("return_shipping_concession_full_refund").value;
	
	var return_other_concession_full_refund=document.getElementById("return_other_concession_full_refund").value;
	$.ajax({
		url:"<?php echo base_url();?>admin/Returns/returns_issue_full_refund",
		type:"POST",
		data:$("#full_refund_form").serialize(),
		success:function(data){
			if(data){
				
				swal({
					title:"success!", 
					text:"Refund successfully done", 
					type: "success",
					allowOutsideClick: false
					
				}).then(function () {
					location.href="<?php echo base_url()?>admin/Returns/returns_refund_request_orders_refund_inprocess";

				});
			}else{
				swal("Error", "not sent", "error");
			}
			
		}
	})
	
	
}

function cal_amount_to_refund_productFun(v){
	if(v==""){
		v=0;
	}
	order_amount_product=parseFloat(document.getElementById("order_amount_product").value);
//document.getElementById("amount_to_refund_product").value=Math.round(order_amount_product-(order_amount_product*(v/100)));
	//cal_total_amount_to_refundFun();
	
}
cal_amount_to_refund_productFun(<?php echo $order_return_decision_data_obj->product_price_with_offer_percentage;?>);
function cal_amount_to_refund_shippingFun(v){
	if(v==""){
		v=0;
	}
	order_amount_shipping=parseFloat(document.getElementById("order_amount_shipping").value);
	document.getElementById("amount_to_refund_shipping").value=Math.round(order_amount_shipping-order_amount_shipping*(v/100));
	//cal_total_amount_to_refundFun();
}
cal_amount_to_refund_shippingFun(<?php echo $order_return_decision_data_obj->shipping_charge_with_offer_percentage;?>);
function cal_total_amount_to_refundFun(){
	
	var amount_to_refund_return_shipping_concession=parseFloat(document.getElementById("amount_to_refund_return_shipping_concession").value);
	var amount_to_refund_other_concession=parseFloat(document.getElementById("amount_to_refund_other_concession").value);
	
	var amount_to_refund_product=document.getElementById("amount_to_refund_product").value;
	var amount_to_refund_shipping=document.getElementById("amount_to_refund_shipping").value;
	document.getElementById("total_amount_to_refund_div").innerHTML="<?php echo curr_sym;?>"+(parseFloat(amount_to_refund_product)+parseFloat(amount_to_refund_shipping)+parseFloat(amount_to_refund_return_shipping_concession)+parseFloat(amount_to_refund_other_concession));
	
	document.getElementById("total_amount_to_refund").value=(parseFloat(amount_to_refund_product)+parseFloat(amount_to_refund_shipping)+parseFloat(amount_to_refund_return_shipping_concession)+parseFloat(amount_to_refund_other_concession));
}
cal_total_amount_to_refundFun();

function partial_refund_fun(){

	var order_amount_product=document.getElementById("order_amount_product").value;
	var deductions_product=document.getElementById("deductions_product").value;
	var order_amount_shipping=document.getElementById("order_amount_shipping").value;
	var deductions_shipping=document.getElementById("deductions_shipping").value;
	var total_product_price_with_shipping=document.getElementById("total_product_price_with_shipping").value;
	
	var amount_to_refund_product=document.getElementById("amount_to_refund_product").value;
	var amount_to_refund_shipping=document.getElementById("amount_to_refund_shipping").value;
	var amount_to_refund_return_shipping_concession=document.getElementById("amount_to_refund_return_shipping_concession").value;
	var amount_to_refund_other_concession=document.getElementById("amount_to_refund_other_concession").value;
	var total_amount_to_refund=document.getElementById("total_amount_to_refund").value;
	
	
	$.ajax({
		url:"<?php echo base_url();?>admin/Returns/returns_issue_partial_refund",
		type:"POST",
		data:$("#partial_refund_form").serialize(),
		success:function(data){
				if(data){
						
						swal({
							title:"success!", 
							text:"Refund successfully done", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							location.href="<?php echo base_url()?>admin/Returns/returns_refund_request_orders_refund_inprocess";

						});
					}else{
						swal("Error", "not sent", "error");
					}
			
		}
	})
	
	
}
</script>


<script>
<?php
	if($type_of_refund_flag==0){
		?>
		$('a[href="#partialrefund-tab"]').trigger("click");
		<?php
	}
?>
</script>
</div>
</body> 
</html>  

 