<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Order Item Info</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
	<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
		<h4 class="text-center alert alert-success">Order Item Details</h4>
		</div>
	</div>
		<div class="row">
		<div class="col-md-8 col-md-offset-2">
		<div class="well">
		<div class="row">
		<div class="col-md-8 col-md-offset-2">
			
			<?php if(!empty($refund_info_obj)){ ?>
			
			<table class="table table-bordered" cellspacing="0" width="100%">
				<thead class="bg-info"><th colspan="2"><b> Return Order Id :</b><?php echo $return_order_id; ?></th> </thead>
				
				<tbody>
				
				<tr><td>Order Item ID</td><td><?php echo $refund_info_obj->order_item_id ; ?> </td></tr>
				<tr><td>Order ID</td><td><?php echo $refund_info_obj->order_id; ?></td></tr>
				
				
				<?php if($refund_info_obj->ord_addon_products_status=='1'){

						$ord_addon_products=$refund_info_obj->ord_addon_products;
						$arr=json_decode($ord_addon_products);
						$count=count($arr);
						$ord_addon_products_count=$count;
						$i=1;
						foreach($arr as $val){
												
												
							$name=$val->inv_name;
							$image=$val->inv_image;
							$sku_id=$val->inv_sku_id;
							$price=$val->inv_price;
					?>

<tr><td>SKU <?php echo $i; ?></td><td><?php echo  $sku_id; ?><br>Price: <?php echo  curr_sym.$price; ?></td></tr>

					<?php
				$i++;
				} ?>

				<tr><td>Qty</td><td><?php echo $count; ?> (Combo products)</td></tr>

				<tr><td>Total Product Price</td><td> <?php echo curr_sym; ?><?php echo $refund_info_obj->ord_addon_total_price; ?> </td></tr>
				
				

				<tr><td>Combo Discount(%)</td><td> <?php echo round($refund_info_obj->ord_selling_discount); ?> % OFF</td></tr>

					<?php 
				}else{
?>

				<tr><td>SKU ID </td><td><?php echo $refund_info_obj->sku_id; ?> </td></tr>
				<tr><td>SKU Name</td><td><font class="text-primary"><?php echo $refund_info_obj->ord_sku_name; ?></b></font></td></tr>
				<tr><td>Product Name</td><td><font class="text-primary"><?php echo $refund_info_obj->product_name; ?></b></font></td></tr>

				<tr><td>Qty</td><td><?php echo $refund_info_obj->quantity ?></td></tr>


				<tr><td>Product Price</td><td> <?php echo curr_sym; ?><?php echo $refund_info_obj->product_price; ?> each</td></tr>
				<tr><td>MRP</td><td> <?php echo curr_sym; ?><?php echo round($refund_info_obj->ord_max_selling_price); ?> each</td></tr>
				<tr><td>Default Discount(%)</td><td> <?php echo round($refund_info_obj->ord_selling_discount); ?> % OFF</td></tr>

<?php 

				} ?>
				
				<tr><td>Shipping Charge</td><td> <?php echo curr_sym; ?><?php echo $refund_info_obj->shipping_charge;?> each</td></tr>
				<tr><td>Total</td><td> <?php echo curr_sym; ?><?php echo $refund_info_obj->grandtotal; ?></td></tr>
				
				
				
				<tr><td>Delivered On</td><td><?php echo date("D j M,Y",strtotime($refund_info_obj->order_delivered_timestamp)); ?></td></tr>
				<tr><td>Courier Name</td><td><?php echo $refund_info_obj->logistics_name?></td></tr>
				<tr><td>Tracking Number</td><td><?php echo $refund_info_obj->tracking_number; ?></td></tr>
				
				<tr>
				<td>
				Delivered to
				</td>
				<td>
				<ul class="list-unstyled">
				<li><?php echo $refund_info_obj->customer_name; ?></li>
				<li><?php echo $refund_info_obj->address1; ?></li>
				<li><?php echo $refund_info_obj->address2." ".$refund_info_obj->city; ?></li>
				<li><?php echo $refund_info_obj->state." ".$refund_info_obj->country; ?></li>
				<li><?php echo $refund_info_obj->pincode; ?></li>
				<li><?php echo $refund_info_obj->mobile; ?></li>
				</ul>
				</td>
				</tr>
				</tbody>
			</table>
			<?php }else{
				?>
				No Details
				<?php
			} ?>
		</div>

	</div>
	</div>
</div>
</div>
</div>
</div>
</body> 
</html>  