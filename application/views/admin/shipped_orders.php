<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Shipped Orders</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script>
<style type="text/css">
.padding_right{
	float:left;
	margin-right:15px;	
}
.accordion-toggle:hover {
  text-decoration: none;
}
.more-less {
        color: #212121;
    }
	.free_items_style{
		padding: 10px;
		border-bottom: 1px solid #ccc;
		margin-bottom: 13px;
	}
.form-inline{
	margin-bottom:0;
}
.form-group{
	margin-bottom:0;
	margin-top:0;
	padding-bottom:0;
	vertical-align:bottom;
}
.datepicker{
	z-index:1151 !important;
}
.popover{
	min-width:80rem;
}
.fa-question-circle{
	color:#e46c0a;
}
</style>


<script type="text/javascript">
var table;


$(document).ready(function(){
$('#shipped_orders_table').on('click', function(e){
	$('.orderid_details_popover').each(function () {
        // hide any open popovers when the anywhere else in the body is clicked
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
           // if($('.orderid_details_popover').length>1)
          //  $('.orderid_details_popover').popover('hide');
            $(e.target).popover('toggle');
           
            });
});
function fetchData(){
	var fetch_data = '';  
               orderid_orderitemid=$(this).attr("id");
			   orderid_to_get_history=orderid_orderitemid.split("_")[1]; 
			   orderitemid_to_get_history=orderid_orderitemid.split("_")[2]; 
                $.ajax({  
                    url:"<?php echo base_url()?>admin/Orders/get_order_summary_mail_data_from_history_table",
					type:"POST",
					data:"order_id="+orderid_to_get_history+"&order_item_id="+orderitemid_to_get_history,  
                     async:false,  
                     success:function(data){  
                          fetch_data = data;  
                     }  
                });  
                return fetch_data;  
}

$(document).ready(function (){
	$('#exact_delivery_date').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		weekStart: 0, 
		format: 'YYYY-MM-DD', 
		shortTime : true
	}).on('change', function(e, date){
		$('#exact_delivery_date').bootstrapMaterialDatePicker('setMinDate', date);
	});
	
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("input[type=text]").val("");
	});

	
		$('#to_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY'
			});
			
		$('#from_date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				weekStart: 0, 
				format: 'DD/MM/YYYY', 
				shortTime : true
			}).on('change', function(e, date)
			{
				$('#to_date').bootstrapMaterialDatePicker('setMinDate', date);
			});	
	
   // Array holding selected row IDs
   var rows_selected = [];
    table = $('#shipped_orders_table').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Orders/shipped_orders_processing", // json datasource
			data: function (d) { d.from_date = $('#from_date').val(); d.to_date = $('#to_date').val(); },
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#shipped_orders_table_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("shipped_count").innerHTML=json.recordsFiltered;
				
                return json.data;
            }
          },
		  
		  
		   drawCallback: function () {
				$('.orderid_details_popover').popover({  
                content:fetchData,  
                html:true,  
                placement:'right'  
           });
		   
		   
		   
                },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'5%',
         'className': 'dt-body-center'
      },{
         'targets': 1,
         'width':'5%',
      },{
         'targets': 2,
         'width':'35%',
      },{
         'targets': 3,
         'width':'25%',
      },{
         'targets': 4,
         'width':'15%',
      },{
         'targets': 5,
         'width':'15%',
      }],
      'order': [0, 'desc']
   });
	
	$( "#from_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
	table.draw();
	} );

	$( "#to_date" ).bootstrapMaterialDatePicker().on('change', function(e, date) {
	table.draw();
	} );

	$('#reset_form_button').on('click',function(){
		table.draw();
	});

	$('#submit_form_button').on('click',function(){
		table.draw();
	});
});	
function selectAllFun(obj){
	var obj_arr=document.getElementsByName("shipped_orders_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}

function update_delivered_date_and_time(){
	var list_to_be_delivered="";
	var list_to_be_delivered_arr=new Array();
	var obj_arr=document.getElementsByName("shipped_orders_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			list_to_be_delivered_arr.push(obj_arr[i].value);
		}
	}
	
	if(list_to_be_delivered_arr.length==0){
		swal("Choose an order to be updated as Delivered!");
		return false;
	}
	if(list_to_be_delivered_arr.length>1){
		swal("Choose only one order to be updated as Delivered!");
		return false;
	}
	
	if(list_to_be_delivered_arr.length==0){
		swal("Choose atleast one order!");
		return false;
	}
	document.getElementById("delivered_order_date_and_time_form").reset();
	$('#delivered_orders_date_and_time').modal('show');
}

function set_as_delivered_fun(){
	
	var list_to_be_delivered="";
	var list_to_be_delivered_arr=new Array();
	var obj_arr=document.getElementsByName("shipped_orders_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			list_to_be_delivered_arr.push(obj_arr[i].value);
		}
	}
	
	if(list_to_be_delivered_arr.length==0){
		swal("Choose an order to be updated as Delivered!");
		return false;
	}
	if(list_to_be_delivered_arr.length>1){
		swal("Choose only one order to be updated as Delivered!");
		return false;
	}
	
	if(list_to_be_delivered_arr.length==0){
		swal("Choose atleast one order!");
		return false;
	}
	list_to_be_delivered=list_to_be_delivered_arr.join("-");
	
	if(document.getElementById("order_delivered_date").value.trim()==""){
		swal("Enter Deliverd Date!");
		return false;
	}
	if(document.getElementById("order_delivered_time").value.trim()==""){
		swal("Enter delivered time!");
		return false;
	}
	order_delivered_date=document.getElementById("order_delivered_date").value;
	order_delivered_time=document.getElementById("order_delivered_time").value;
	swal({
			title: 'Are you sure?',
			html:
    '<label for="sms"><input type="checkbox" name="sms" value="1" id="sms"><small><span>&nbsp;send SMS</span></small></label>&nbsp;&nbsp;'+
	'<label for="email"><input type="checkbox" name="email" value="1" id="email"><small><span>&nbsp;send Email</span></small></label>',
			text: "Deliver this order",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Deliver it!',
			showLoaderOnConfirm: true,
			input: 'checkbox',
			inputValue: 0,
			inputPlaceholder:
				'&nbsp; send notification to customer',
			inputValidator: function(result){
				
			return new Promise(function (resolve, reject) {
				
				sms=0;
				email=0;
				var sms_res=$("input[name=sms]").is(':checked');
				var email_res=$("input[name=email]").is(':checked');
				
				if(sms_res==true){
					sms=1;
				}
				if(email_res==true){
					email=1;
				}
				if(sms==1 || email==1){
					swal.showLoading();
				}
				$.ajax({
					url:"<?php echo base_url()?>admin/Orders/set_as_delivered",
					type:"post",
					data:"list_to_be_delivered="+list_to_be_delivered+"&order_delivered_date="+order_delivered_date+"&order_delivered_time="+order_delivered_time+"&send_notification="+result+"&sms="+sms+"&email="+email,
					beforeSend: function(){
						$("#set_as_delivered_btn").html('<i class="fa fa-refresh fa-spin"></i> Processing');
					},
					success:function(data){
						$("#set_as_delivered_btn").html('Delivered');
						
						if(data){
							swal({
								title:"Delivered!", 
								text:"Given order(s) has been Delivered successfully", 
								type: "success",
								allowOutsideClick: false
							}).then(function () {
								location.reload();

							});
						}else{
							swal("Error", "Error", "error");
						}
						
					}
				});
			
			})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});	
}
function cancel_order_fun(){
	
	var list_to_be_cancelled_arr=new Array();
	var obj_arr=document.getElementsByName("shipped_orders_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			list_to_be_cancelled_arr.push(obj_arr[i].value);
		}
	}
	if(list_to_be_cancelled_arr.length==0){
		swal("Choose an order to be cancelled!");
		return false;
	}
	if(list_to_be_cancelled_arr.length>1){
		swal("Choose only one order to be cancelled!");
		return false;
	}
	document.getElementById("cancel_order_form").reset();
	$('#cancel_order_modal').modal('show');

}

function set_as_cancelled_fun(){
	var list_to_be_cancelled="";
	var list_to_be_cancelled_arr=new Array();
	var obj_arr=document.getElementsByName("shipped_orders_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			list_to_be_cancelled_arr.push(obj_arr[i].value);
		}
	}
	if(list_to_be_cancelled_arr.length==0){
		swal("Choose an order to be cancelled!");
		return false;
	}
	if(list_to_be_cancelled_arr.length>1){
		swal("Choose only one order to be cancelled!");
		return false;
	}
	list_to_be_cancelled=list_to_be_cancelled_arr.join("-");
	
	if(document.getElementById("cancel_reason").value.trim()==""){
		swal("Enter reason for cancellation!");
		return false;
	}
	if(document.getElementById("reason_for_cancel_order").value.trim()==""){
		swal("Enter reason for cancellation!");
		return false;
	}
	cancel_reason=document.getElementById("cancel_reason").value;
	reason_for_cancel_order=document.getElementById("reason_for_cancel_order").value;
	
	var update_stock=$("#update_stock").is(':checked');
	
	if(update_stock==true){
		update_inventory_stock="yes";
	}else{
		update_inventory_stock="no";
	}
	//alert("list_to_be_cancelled="+list_to_be_cancelled+"&reason_for_cancel_order="+reason_for_cancel_order);
	//return false;
	swal({
			title: 'Are you sure?',
			html:
    '<label for="sms"><input type="checkbox" name="sms" value="1" id="sms"><small><span>&nbsp;send SMS</span></small></label>&nbsp;&nbsp;'+
	'<label for="email"><input type="checkbox" name="email" value="1" id="email" checked=true><small><span>&nbsp;send Email</span></small></label>',
			text: "Cancel this order",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Cancel it!',
			showLoaderOnConfirm: true,
			input: 'checkbox',
			inputValue: 1,
			inputPlaceholder:
				'&nbsp; send notification to customer',
			inputValidator: function(result){
			return new Promise(function (resolve, reject) {
				
				sms=0;
				email=0;
				var sms_res=$("input[name=sms]").is(':checked');
				var email_res=$("input[name=email]").is(':checked');
				
				if(sms_res==true){
					sms=1;
				}
				if(email_res==true){
					email=1;
				}
				if(sms==1 || email==1){
					swal.showLoading();
				}	
				$.ajax({
					url:"<?php echo base_url()?>admin/Orders/set_as_cancelled_for_shipped",
					type:"post",
					data:"list_to_be_cancelled="+list_to_be_cancelled+"&reason_for_cancel_order="+reason_for_cancel_order+"&cancel_reason="+cancel_reason+"&update_inventory_stock="+update_inventory_stock+"&send_notification="+result+"&sms="+sms+"&email="+email,
					success:function(data){		
						if(data){
							swal({
								title:"Cancelled!", 
								text:"Given order(s) has been cancelled successfully", 
								type: "success",
								allowOutsideClick: false
							}).then(function () {
								location.reload();

							});
						}else{
							swal("Error", "Error", "error");
						}
					}
				});
			
			})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
</script>
  
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header"><h4 class="text-center">Shipped Orders <span class="badge" id="shipped_count"></span></h4></div>
	<table id="shipped_orders_table" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th colspan="6">
					<div class="row">
					<div class="col-md-4">
					<button class="btn btn-sm btn-success" id="set_as_delivered_btn" onclick="update_delivered_date_and_time()">Delivered</button> <button class="btn btn-sm btn-danger" onclick="cancel_order_fun()">Not Delivered</button>
					</div>
					<div class="col-md-8 text-right">
						<form id="date_range" class="form-inline">
						<input type="text" name="from_date" id="from_date" placeholder="From Date" class="form-control">
						<input type="text" name="to_date" id="to_date" placeholder="To Date" class="form-control">
						<button type="button" class="btn btn-sm btn-primary" id="submit_form_button">Submit</button>
						<button type="reset" class="btn btn-sm btn-info" id="reset_form_button">Reset</button>
						</form>
					</div>
					</div>
				</th>
			</tr>
			<tr>
				<th><input type='checkbox' onclick="selectAllFun(this)"></th>
				<th class="text-primary small bold">Image</th>
				<th class="text-primary small bold">Order Summary</th>
				<th class="text-primary small bold">Logistics Details</th>
				<th class="text-primary small bold">Quantity and Price</th>
				<th class="text-primary small bold">Buyer Details</th>
			</tr>
		</thead>
	</table>
</div>


<div class="modal" id="cancel_order_modal">
     <div class="modal-dialog modal-sm">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title"> Cancel Order</h4>
           </div>
           <div class="modal-body">
				<form id="cancel_order_form" class="form-horizontal">
				<div class="form-group">
				<div class="col-md-12">
					<select class="form-control" name="cancel_reason" id="cancel_reason" required>
							  		<option value="">-Select reason for cancellation-</option>
							  	<?php foreach($cancel_reason as $reasons){?>
							  		<option value="<?php echo $reasons['cancel_reason_id'] ?>"><?php echo ucfirst($reasons['cancel_reason']); ?> </option>
							  	<?php }?>
					</select>
					</div>
					</div>
           			<div class="form-group">
						<div class="col-md-12">
           				<textarea class="form-control" rows="3" name="reason_for_cancel_order" id="reason_for_cancel_order" required placeholder="Reason For Cancellation"></textarea>
						</div>
           			</div>
					<div class="form-group">
					<div class="col-sm-12">
						<label>
						  <input type="checkbox" name="update_stock" id="update_stock"> Do you want to Update Stock?
						</label>
					</div>
				  </div>
				  <div class="form-group">
					<div class="col-sm-12">
					  <button type="button" class="btn btn-sm btn-success btn-block" onclick="set_as_cancelled_fun()"> Cancel Order</button>
					</div>
				  </div>
				</form>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
  </div>
  
  <div class="modal" id="delivered_orders_date_and_time">
     <div class="modal-dialog modal-sm">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Update Date and Time</h4>
           </div>
           <div class="modal-body">
				<form id="delivered_order_date_and_time_form" class="form-horizontal">
					<div class="form-group">
						<div class="col-md-12">
						<input class="form-control" type="text" name="order_delivered_date" id="order_delivered_date" placeholder="Delivered Date" required />
						</div>
					</div>
           			<div class="form-group">
						<div class="col-md-12">
							<input class="form-control" type="text" name="order_delivered_time" id="order_delivered_time" placeholder="Delivered Time" required />
							
						</div>
					</div>
           			<div class="form-group">
						<div class="col-md-12">
           				<button type="button" class="btn btn-sm btn-block btn-success" onclick="set_as_delivered_fun()">Submit</button>
						</div>
           			</div>
				</form>
           </div>
				<script>
				  $(document).ready(function(){
						
						$('#order_delivered_date').bootstrapMaterialDatePicker
						({
							time: false,
							clearButton: true
						});

						$('#order_delivered_time').bootstrapMaterialDatePicker
						({
							date: false,
							shortTime: true,
							use24hours: false,
							clearButton: true,
							format: 'hh:mm A'
						});

						$.material.init()
					});
				  </script>
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
  </div>
  
  <div class="modal" id="order_summary">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Order Summary</h4>
           </div>
           <div  class="modal-body" id="model_content">
		  
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
  </div>
  
  <div class="modal" id="exact_delivery_date_modal">
     <div class="modal-dialog modal-sm">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Update Exact delivery date</h4>
           </div>
           <div  class="modal-body" id="exact_delivery_date_content">
		  
		   <form name="exact_delivery_date_form" id="exact_delivery_date_form"> 
				<input type="hidden" name="exact_buyer_mobilenumber" id="exact_buyer_mobilenumber" value="">
				<input type="hidden" name="order_item_id" id="exact_order_item_id" value="">
				<div class="form-group">
				<input class="form-control" name="exact_delivery_date" id="exact_delivery_date" method="post" placeholder="Exact delivery date">
				</div>
				<div class="form-group">
				<button class="btn btn-sm btn-success btn-block" type="submit"> Submit</button>
				</div>
		   </form>
           </div>
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
  </div>
  
  <script type="text/javascript">
  $("#order_summary").modal("hide");
  function view_order_summary_(order_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Orders/get_data_from_invoice_offers",
		type:"POST",
		data:"order_id="+order_id,
		success:function(data){
			
			$("#description").val("");	
			$("#model_content").html(data);
			$("#order_summary").modal("show");
			
		}
	})
	
}
function open_update_expected_delivery_date_modal(order_item_id,mobile){
	$('#exact_order_item_id').val(order_item_id);
	$('#exact_buyer_mobilenumber').val(mobile);
	$('#exact_delivery_date_modal').modal('show');
	
}
$(document).ready(function(){
	$('#exact_delivery_date_modal').on('hidden.bs.modal', function () {
		$("#exact_delivery_date_form")[0].reset();
	});
	$('#exact_delivery_date_modal').on('shown.bs.modal', function () {
		//$("#exact_delivery_date_form")[0].reset();
	});
});

$("#exact_delivery_date_form").on('submit',(function(e) {
	e.preventDefault();
	exact_delivery_date=$('#exact_delivery_date').val();
	if(exact_delivery_date==""){
		swal('Info','Please select date','info');
		return false;
	}
	
	$.ajax({
		url:"<?php echo base_url(); ?>admin/Orders/update_exact_date",
		type:'POST',
		data:new FormData(this),
		contentType: false,
		cache: false,	
		processData:false,
		success:function(data){
			if(data){
				swal({
					title:"Updated!", 
					text:"Updated successfully", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					table.draw();
					$('#exact_delivery_date_modal').modal('hide');

				});
			}else{
				swal("Error", "Error", "error");
			}
		}
	});
}));
  </script>
 </div> 
</body> 
</html>  