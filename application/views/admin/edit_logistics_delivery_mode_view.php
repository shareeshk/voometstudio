<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.wizard-card .tab-content {
	 min-height: 400px;
	 padding:0px;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>


<script>

function showDivPrevious(){
	 var form = document.getElementById('search_for_logistics_delivery_mode');		
	
	form.action='<?php echo base_url()."admin/Catalogue/logistics_delivery_mode"; ?>';
	form.submit();
}
</script>
<form name="search_for_logistics_delivery_mode" id="search_for_logistics_delivery_mode" method="post">
		<input value="<?php echo $get_logistics_delivery_mode_data["vendor_id"]; ?>" type="hidden" name="vendor_id"/>
		<input value="<?php echo $get_logistics_delivery_mode_data["logistics_id"]; ?>" type="hidden" name="logistics_id"/>
		<input value="view" type="hidden" name="create_editview">
	</form>
<?php //print_r($shipping_charge);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row-fluid" id="editDiv3">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="edit_logistics_delivery_mode" id="edit_logistics_delivery_mode" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_vendors: {
					required: true,
				},
				edit_logistics: {
					required: true,
				},
				edit_delivery_mode: {
					required: true,
				},
				edit_speed_territory_duration: {
					required: true,
				}
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Logistics Delivery Mode</h3>
		</div>	
		</div>	
		<div class="tab-content">
		<input type="hidden" name="edit_logistics_delivery_mode_id" id="edit_logistics_delivery_mode_id" value="<?php echo $get_logistics_delivery_mode_data["logistics_delivery_mode_id"];?>">
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Vendor-</label>
					<select name="edit_vendors" id="edit_vendors" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($vendors as $vendors_value) {  ?>
						<option value="<?php echo $vendors_value->vendor_id; ?>" <?php if($get_logistics_delivery_mode_data["vendor_id"]==$vendors_value->vendor_id){echo "selected";} ?>><?php echo $vendors_value->name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Logistics Name-</label>
					<select name="edit_logistics" id="edit_logistics" class="form-control" disabled>
						<option value="<?php echo $get_logistics_delivery_mode_data["logistics_id"];?>"><?php echo $get_logistics_delivery_mode_data["logistics_name"];?></option>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Delivery Mode</label>
					<input id="edit_delivery_mode" name="edit_delivery_mode" type="text" class="form-control" value="<?php echo $get_logistics_delivery_mode_data["delivery_mode"];?>"/>
					<input id="edit_delivery_mode_default" name="edit_delivery_mode_default" type="hidden" class="form-control" value="<?php echo $get_logistics_delivery_mode_data["delivery_mode"];?>"/>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					
					<?php
					echo $speed_territory_duration_table;
					?>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group">
					<div class="col-md-6">Make Default Logistics delivery Mode</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="edit_default_logistics" id="edit_default_logistics1" value="1" type="radio" <?php if($get_logistics_delivery_mode_data["default_delivery_mode"]=="1"){echo "checked";}?> >View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="edit_default_logistics" id="edit_default_logistics2" value="0" type="radio" <?php if($get_logistics_delivery_mode_data["default_delivery_mode"]=="0"){echo "checked";}?>>Hide</label>
					</div>
				
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-3">
					<div class="form-group">
					<button class="btn btn-info btn-sm" id="submit-data" disabled="" type="submit">Submit</button>
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
					</div>
				</div>
			</div>
				
		</div>		
	</form>

</div>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>
</body>
</html>
<script>

$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_logistics_delivery_mode'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_logistics_delivery_mode"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_logistics_delivery_mode'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});

});
//$('#result').hide();
function form_validation_edit()
{
	
	var vendors = $('input[name="edit_vendors"]').val();
	var logistics = $('input[name="edit_logistics"]').val();
	var delivery_mode = $('input[name="edit_delivery_mode"]').val().trim();
	

	   var err = 0;
		if((vendors=='') || (logistics=='') || (delivery_mode=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#edit_logistics_delivery_mode');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_logistics_delivery_mode/"?>',
				type: 'POST',
				data: $('#edit_logistics_delivery_mode').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {	
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					edit_delivery_mode_default=document.getElementById("edit_delivery_mode_default").value;
					document.getElementById("edit_delivery_mode").value=edit_delivery_mode_default;
					document.getElementById("edit_delivery_mode").focus();
					
				});
				  }
				if(data==1)
				{
					swal({
						title:"Success", 
						text:"Logistics delivery mode is successfully updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				if(data==0)
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)		
				}
				
			});
}
}]);			
			}
	
	return false;
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
        return false;
    }
    return true;
}

</script>
