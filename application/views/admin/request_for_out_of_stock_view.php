<html lang="en">
<head>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>

<style type="text/css">
.align_top{
	margin-top: 20px;
}
.align_bottom{
    margin-bottom: 20px;
}
#low_stock_notification tr td:nth-child(3){
    background-color:#eee;
	color:red;
}
</style>
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #398439;
}

input:focus + .slider {
  box-shadow: 0 0 1px #398439;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<script type="text/javascript">
var table;
$(document).ready(function () {
          
	table = $('#request_for_out_of_stock').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Orders/request_for_out_of_stock_processing",
			
            type: "post",
            error: function(){
              $("#table_request_for_out_of_stock_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				
				document.getElementById("request_for_out_of_stock_count").innerHTML=json.recordsFiltered;
				
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      },{
        'targets': 1,
        'width':'10%'
      },{
        'targets': 2,
        'width':'15%'
      },{
        'targets': 3,
        'width':'20%'
      },{
        'targets': 4,
        'width':'25%'
      },{
        'targets': 5,
        'width':'20%'
      }],
      'order': [0, 'desc']
	});

});
	   
function drawtable(obj){
	//if(obj.value!="" && obj.value!="None"){
	//	table.draw();
	//}
	table.draw();
}
	
</script>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div id="parent_button" class="page-header">
	<h4 class="text-center">Requests for out of stock <span class="badge"><div id="request_for_out_of_stock_count"></div></span>
	</h4>
	</div>
	
	
	<!------------------------------------------->
		
	<div class="row">
	<div class="col-md-12" align="right">
<label class="switch align-top">
			  <input type="checkbox" id="switchbuttonAutoassign_btn"  onchange="switchbuttonAutoassignFun(this)" <?php if($controller->get_out_of_stock_switch()=="yes"){echo "checked";}?>>
			  <span class="slider round"></span>
			</label>
	</div>
</div>	
	<!---------------------------------------------->
	

	<div class="row">
	<div class="col-md-12" id="viewDiv1">
	<table id="request_for_out_of_stock" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
				<tr>
					<th class="text-primary small bold">Sl.No.</th>
					<th class="text-primary small bold">SKU</th>
					<th class="text-primary small bold">SKU Details</th>
					<th class="text-primary small bold">Requests and Stock Information</th>
					<th class="text-primary small bold">List of emails</th>
					<th class="text-primary small bold">List of Mobile Numbers</th>
				</tr>
		</thead>
	</table>
	</div>
	</div>

</div>
</div>
</body>
</html>

<script type="text/javascript">

function send_mail_to_notify(id,sku_id){
	swal({
			title: 'Are you sure?',
			text: "Do you want to mail to all these E-mail addresses?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes',
			showLoaderOnConfirm: true,
			preConfirm: function(){	
			return new Promise(function (resolve, reject) {	
			$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>admin/Orders/send_mail_to_notify',
			data:"inventory_id="+id+"&sku_id="+sku_id,
			success:function(res){
				if(res){
					swal({
					title:"Mailed!", 
					text:"Mail has been sent successfully", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					//table.draw();
					location.reload();
				});
			}
					
				}
		});
							
				})
			
		    },
		
		allowOutsideClick: false
		
		}).then(function() {
			},function(dismiss){
			  if (dismiss === 'cancel'){
					swal({
							title:"Cancelled", 
							text:"No action taken", 
							type: "error"
					}).then(function () {
						location.reload();
					});
			  }
			});	
}
function send_message_to_notify(id,sku_id){
	swal({
			title: 'Are you sure?',
			text: "Do you want to send messages to all these mobile numbers?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes',
			showLoaderOnConfirm: true,
			preConfirm: function(){	
			return new Promise(function (resolve, reject) {	
			$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>admin/Orders/send_message_to_notify',
			data:"inventory_id="+id+"&sku_id="+sku_id,
			success:function(res){
				if(res){
					swal({
					title:"Messaged!", 
					text:"Message has been sent successfully", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					//table.draw();
					location.reload();
				});
				}
			}
		});
							
				})
			
		    },
		
		allowOutsideClick: false
		
		}).then(function() {
			},function(dismiss){
			  if (dismiss === 'cancel'){
					swal({
							title:"Cancelled", 
							text:"No action taken", 
							type: "error"
					}).then(function () {
						location.reload();
					});
			  }
			});

}

function switchbuttonAutoassignFun(obj){
	if(obj.checked==true){
		out_of_stock_switch_status="yes";
	}
	else{
		out_of_stock_switch_status="no";
	}
	$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>admin/Orders/out_of_stock_switch_fun',
			data:"out_of_stock_switch_status="+out_of_stock_switch_status,
			success:function(res){
				if(res==true){
					if(out_of_stock_switch_status=="yes"){
						swal({
							title:"Success", 
							html:"Auto Email <b>ON</b>", 
							type: "success"
						}).then(function () {
							
						});
					}
					if(out_of_stock_switch_status=="no"){
						swal({
							title:"Success", 
							html:"Auto Email <b>OFF</b>", 
							type: "success"
						}).then(function () {
							
						});
					}
				}
				else{
					swal({
							title:"", 
							text:"Oops Error", 
							type: "error"
					}).then(function () {
						
					});
				}
			}
	});
}


</script>
