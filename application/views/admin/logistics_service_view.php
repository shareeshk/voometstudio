<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<style>
.margin_top{
	margin-top:5em;
}
select {
	width:230px;
}
.entities{
	width:500px;
	height:100px;
}
.select_list_entities,.select_list_entities{
	width:180px;
}
.move_btns{
	width:50px;
}
#moveright,#moveleft,#moverightall,#moveleftall{
	margin:14px 0px 5px 0px;
	width:40px;
}
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
select.form-control[multiple], .form-group.is-focused select.form-control[multiple] {
    height: 140px;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/multiselect.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#create_rightAll,#create_rightSelected,#create_leftAll,#create_leftSelected").click(function(){
		setTimeout(function(){ 
			obj=document.getElementById("to_select_list");
			for(i=0;i<obj.length;i++){
				obj.options[i].selected=true;
			} 
		}, 1);
	});
	
});

var table;

$(document).ready(function (){
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    var rows_selected = [];
     table = $('#table_logistics_service').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Catalogue/logistics_service_processing",
			data: function (d) { d.vendor_id = $('#vendor_id').val();d.logistics_id = $('#logistics_id').val();},
            type: "post",
            error: function(){
              $("#table_logistics_service_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				
				document.getElementById("logistics_service_count").innerHTML=json.recordsFiltered;
				
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'10%',
        'className': 'dt-body-left'
      },{
        'targets': 1,
        'width':'10%',
      },{
        'targets': 2,
        'width':'10%',
      },{
        'targets': 3,
        'width':'50%',
      },{
        'targets': 4,
        'width':'10%',
      },{
        'targets': 5,
        'width':'10%',
      }],
      'order': [0, 'desc']
   });
   
	$('#reset_form_button').on('click',function(){
		table.draw();
	});
	
	$("#submit_button").click(function() {
		pcat_id=$('#pcat_id').val();
		table.draw();
	});

});	

function drawtable(obj){
	table.draw();
}

function test(){

var button = $('#submit-data');
var orig = [];
var orig1 = [];
$.fn.getType = function () {
	//alert(this[0].tagName +"|"+ $(this[0]).attr("multiple"));
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : ((this[0].tagName.toLowerCase()=="select" && $(this[0]).attr("multiple")=="multiple") ? (this[0].tagName.toLowerCase()+"-"+$(this[0]).attr("multiple")) : this[0].tagName.toLowerCase());
}
search_arr=[];
$("form[name='edit_logistics_service'] :input").each(function () {
		
		var type = $(this).getType(); 
		
		var tmp = {
			'type': type,
			'value': $(this).val()
		};
		
		if (type == 'radio' || type == 'text' || type == 'select') {
			tmp.checked = $(this).is(':checked');
			orig[$(this).attr('id')] = tmp;
		}
		if (type == 'select-multiple') {
			 search_arr=[];
			 $(this).find("option").each(function(){
				 search_arr.push($(this).attr("value"));
			 });
			orig1[$(this).attr('id')] = search_arr;
		}
});

$('form[name="edit_logistics_service"]').bind('change keyup click', function () {
    var disable = true;
    $("form[name='edit_logistics_service'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'select-multiple') {
			 search_cur_arr=[];
			$(this).find("option").each(function(){
				 search_cur_arr.push($(this).attr("value"));
			 });
            disable = (orig1[id].sort().join("-") == search_cur_arr.sort().join("-"));
			
        }else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false;
        }
    });

    button.prop('disabled', disable);
});

/////////////////////////
}
function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_delete_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
swal({
			title: 'Are you sure?',
			text: "Delete Logistic service",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/delete_logistics_service_selected",
		type:"post",
		data:"selected_list="+selected_list,
		
		success:function(data){
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Given "+table+"(s) has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
</script>
<script>

$(document).ready(function(){
	<?php
		if($create_editview=="create"){
			?>
			$("#viewDiv1").css({"display":"none"});
			$("#createDiv2").css({"display":""});
			<?php
		}
		else if($create_editview=="view"){
			?>
			$("#createDiv2").css({"display":"none"});
			$("#viewDiv1").css({"display":""});
			<?php
		}
	?>
});
</script>
<script>

function changeViewPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/logistics_service_filter';
}
</script>
<?php //print_r($logistics_service);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="wizard-header text-center">
		<h5> Number of Logistics Services <span class="badge"><div id="logistics_service_count"></div></span> 
		</h5>                 		          	
	</div>
<div class="row" id="viewDiv1" style="display:none;">
	
	<input value="<?php echo $vendor_id; ?>" type="hidden" id="vendor_id">
	<input value="<?php echo $logistics_id; ?>" type="hidden" id="logistics_id">

<table id="table_logistics_service" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<th colspan="6">
		<div class="col-md-12">
			<button id="multiple_delete_button" class="btn btn-danger btn-xs" onclick="multilpe_delete_fun('Logistic service')">Delete</button> <button class="btn btn-primary btn-xs" type="button" onclick="changeViewPrevious()">Change View</button>
			</div>
		</th>
	</tr>
	<tr>
		<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
		<th>Logistics Name </th>
		<th>City & PIN</th>
		<th>Pincodes</th>
		<th>Last Updated</th>
		<th>Action</th>
	</tr>
</thead>
</table>
</div>

<div class="row" id="createDiv2" style="display:none;">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">

	<form name="create_logistics_service" id="create_logistics_service" method="post" action="#" onsubmit="return form_validation();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				vendors: {
					required: true,
				},
				logistics: {
					required: true,
				},
				pin: {
					required: true,
				},
				city: {
					required: true,
				},
				'to_select_list[]': {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Create Logistics Service</h3>
		</div>	
		</div>
		<div class="tab-content">
			<div class="row">
				<div class="col-md-12">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Vendor-</label>
					<select name="vendors" id="vendors" class="form-control" onchange="showAvailableLogistics(this)">
						<option value=""></option>
						<?php foreach ($vendors as $vendors_value) {  ?>
						<option value="<?php echo $vendors_value->vendor_id; ?>"><?php echo $vendors_value->name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Logistics Name-</label>
					<select name="logistics" id="logistics" class="form-control">
						<option value=""></option>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select City Pin-</label>
					<select name="pin" id="pin" class="form-control" onchange="show_available_logistics_pincodes('create',this)">
						<option value=""></option>
						<?php foreach ($logistics_all_pincodes as $logistics_all_pincodes_value) {  ?>
						<option value="<?php echo $logistics_all_pincodes_value->pin."_".strtolower($logistics_all_pincodes_value->city); ?>"><?php echo $logistics_all_pincodes_value->city." - ".$logistics_all_pincodes_value->pin; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>			
			<div class="row">
				<div class="col-md-12">                               
				<div class="form-group label-floating">
					<label class="control-label"></label>
					<input id="city" name="city" type="text" class="form-control" readOnly />
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-12">                               
				<h5>Select Pincodes</h5>
				</div>
			</div>	

			<div class="row">
				<div class="col-md-12">  
                <div class="col-sm-5">
                    <select name="from_select_list[]" id="from_select_list" class="form-control" size="8" multiple="multiple"> 
                    </select>
                </div>
                
                <div class="col-sm-2 margin_top">
                    <button type="button" id="create_rightAll" class="btn btn-block btn-xs"><i class="glyphicon glyphicon-forward"></i></button>
                    <button type="button" id="create_rightSelected" class="btn btn-block btn-xs"><i class="glyphicon glyphicon-chevron-right"></i></button>
                    <button type="button" id="create_leftSelected" class="btn btn-block btn-xs"><i class="glyphicon glyphicon-chevron-left"></i></button>
                    <button type="button" id="create_leftAll" class="btn btn-block btn-xs"><i class="glyphicon glyphicon-backward"></i></button>
                </div>
                
                <div class="col-sm-5">
                    <select name="to_select_list[]" id="to_select_list" class="form-control" size="8" multiple="multiple"></select>
                </div>
				</div>
            </div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-xs" type="submit">Submit</button>
					<button class="btn btn-warning btn-xs" type="reset">Reset</button>
					<button class="btn btn-primary btn-xs" type="button" onclick="changeViewPrevious()">Change View</button>
					</div>
				</div>
			</div>

		</div>		
	</form>
		
</div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    // make code pretty
    $('#from_select_list').multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
        }
    });
});
</script>
<script>
	function go(obj){
		//if(obj.options[obj.selectedIndex].value){
			test();
		//}
	}
</script>
<script type="text/javascript">
$(document).ready(function() {
    // make code pretty
    $('#search').multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
        }
    });
});
</script>

<script type="text/javascript">
function move_list_items(sourceid, destinationid){
	$("#"+sourceid+"  option:selected").appendTo("#"+destinationid);
}

	//this will move all selected items from source list to destination list
function move_list_items_all(sourceid, destinationid){
	$("#"+sourceid+" option").appendTo("#"+destinationid);
}

function showAvailableLogistics(obj){
	
		vendor_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_logistics",
				type:"post",
				data:"vendor_id="+vendor_id,
				success:function(data){
					
					if(data!=0){

						$("#logistics").html(data);		
					}
					else{
						$("#logistics").html('<option value="">--No Logistics--</option>');	
						
					}
				}
			});
	
}


function show_available_logistics_pincodes(action,obj){

	values=obj.value;
	val_arr=values.split("_");
	pin=val_arr[0];
	city=val_arr[1];
	if(action=="create"){
		$("#city").val(city);
	}else{
		$("#edit_city").val(city);
	}
	
	if(pin!=""){
		$.ajax({
			url:'<?php echo base_url()."admin/Catalogue/get_available_logistics_pincodes/"?>',
			type:"post",
			data:"pin="+pin,
			dataType: 'JSON',
			traditional: true,
			success:function(data){
				if(data!=""){
				
					data_arr_from=data.pincodes.split("|");//from all pincodes
					if(data.pincode!=null){
					data_arr_to=data.pincode.split("|");
					//from all logistics services
					diff_from_arr = data_arr_from.filter(function(x) { return data_arr_to.indexOf(x) < 0 });
					html_options_to='';				
					for(l=0;l<data_arr_to.length;l++){
						html_options_to+='<option value="'+data_arr_to[l]+'">'+data_arr_to[l]+'</option>';
					}
					if(action=="create"){
						$("#to_select_list").html(html_options_to);
					}else{
						$("#search_to").html(html_options_to);
						$.each(data_arr_to.split("|"), function(i,e){
						$("#search_to option[value='" + e + "']").prop("selected", true);
						});
					}
					
					}else{
						diff_from_arr=data_arr_from;
					}
					
					html_options_from='';
					
					for(k=0;k<diff_from_arr.length;k++){
						html_options_from+='<option value="'+diff_from_arr[k]+'">'+diff_from_arr[k]+'</option>';
					}

					if(action=="create"){
						$("#from_select_list").html(html_options_from);
						
					}else{
						$("#search").html(html_options_from);
					}

				}
			}
		});
	}
}

function form_validation()
{
	var city = $('input[name="city"]').val().trim();
	var pin = $('select[name="pin"]').val().trim();
	var to_select_list=document.getElementsByName("to_select_list[]");	
	var logistics = $('select[name="logistics"]').val().trim();
	var vendors = $('select[name="vendors"]').val().trim();
	   var err = 0;var select_err=0;
		
		$("#to_select_list option").each(function() {
				if($(this).is(':selected')){
					select_err++;
				}	
		});	
		if(select_err==0)
		{
			err=1;
			//$('select[name="to_select_list[]"]').css({"border": "1px solid red"});
		}else{
			//$('select[name="to_select_list[]"]').css({"border": "1px solid #ccc"});
		}
	   
		
		if(city=='')
		{
		   //$('input[name="city"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('input[name="city"]').css({"border": "1px solid #ccc"});
		}
		if(pin=='')
		{
		   //$('select[name="pin"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('select[name="pin"]').css({"border": "1px solid #ccc"});
		}

		if(logistics=='')
		{
		  //$('select[name="logistics"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('select[name="logistics"]').css({"border": "1px solid #ccc"});
		}
		if(vendors=='')
		{
		   //$('select[name="vendors"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('select[name="vendors"]').css({"border": "1px solid #ccc"});
		}
		
		if(err==1)
		{
			   $('#error_result_id').show();
			   $('#result').hide();
		}
		else
			{
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#create_logistics_service');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_logistics_service/"?>',
				type: 'POST',
				data: $('#create_logistics_service').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {
				if(data)
				{
					swal({
						title:"Success", 
						text:"Logistics Service is successfully Added!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					}); 					
					
					  
				}
				else
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
			});
}
}]);			
			}
	
	return false;
}

</script>
</body>
</html>