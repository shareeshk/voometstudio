<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
.remove_range{
	margin-top:36px;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script type="text/javascript">

function showDivPrevious(){
	
	var form = document.getElementById('search_for_logistics_weight');		
	
	form.action='<?php echo base_url()."admin/Catalogue/logistics_weight"; ?>';
	form.submit();
			
	 //window.location.href = '<?php echo base_url(); ?>admin/Catalogue/category';
}
</script>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row-fluid common" id="editDiv3">
<div class="col-md-8 col-md-offset-2">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="edit_logistics_weight" id="edit_logistics_weight" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_vendors: {
					required: true,
				},
				edit_logistics: {
					required: true,
				},
				edit_logistics_territory: {
					required: true,
				},
				edit_weight_in_kg: {
					required: true,
				},
				/*edit_volume_in_kg: {
					required: true,
				},
				edit_logistics_price: {
					required: true,
				}*/
				lesser_v: {
					required: true,
				},
				lesser_m: {
					required: true,
				},
				/*greater_v: {
					required: true,
				},
				greater_m: {
					required: true,
				},
				'v_m[]': {
					required: true,
				},
				'v_from[]': {
					required: true,
				},
				'v_to[]': {
					required: true,
				},*/
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>

		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Logistics Weight</h3>
		</div>	
		</div>
		
		<?php
		$input_arr=$volumetric_data;
		$temp_iphan_arr=[];
		$temp_greater_arr=[];
		foreach($input_arr as $k => $arr){
			if(preg_match("/-/",$arr["volume_in_kg"])){
				$temp_iphan_arr[$k]=explode("-",$arr["volume_in_kg"])[0];
			}
			if(preg_match("/>/",$arr["volume_in_kg"])){
				$temp_greater_arr[$k]=explode(">",$arr["volume_in_kg"])[0];
			}
		}
		asort($temp_iphan_arr);
		$output_arr=[];
		if(!empty($temp_iphan_arr)){	
			foreach($temp_iphan_arr as $k => $v){
				$output_arr[]=$input_arr[$k];
			}
		}
		if(!empty($temp_greater_arr)){
			foreach($temp_greater_arr as $k => $v){
				$output_arr[]=$input_arr[$k];
			}
		}
		
		$volumetric_data=$output_arr;

		$lesser = reset($volumetric_data);
		if(count($volumetric_data)>1){
			$greater = end($volumetric_data);
		}
		else{
			$greater=array();
		}
//print_r($lesser);
		$range = array_slice($volumetric_data, 1, -1);//first and last
		
		//print_r($get_logistics_weight_data);
		//echo $weight_in_kg;
		
		?>
		<div class="tab-content">				
			<input type="hidden" name="edit_logistics_weight_id" id="edit_logistics_weight_id" value="<?php echo $get_logistics_weight_data["logistics_weight_id"];?>">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group ">
					<label class="control-label">-Select Vendor-</label>
					<select name="edit_vendors" id="edit_vendors" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($vendors as $vendors_value) {  ?>
						<option value="<?php echo $vendors_value->vendor_id; ?>"<?php echo ($vendors_value->vendor_id==$get_logistics_weight_data['vendor_id'])? "selected":''; ?>><?php echo $vendors_value->name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group ">
					<label class="control-label">-Select Logistics Name-</label>
					<select name="edit_logistics" id="edit_logistics" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($logistics as $logistics_value) {  ?>
						<option value="<?php echo $logistics_value->logistics_id; ?>" <?php echo ($logistics_value->logistics_id==$get_logistics_weight_data['logistics_id'])? "selected":''; ?>><?php echo $logistics_value->logistics_name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Logistics Territory-</label>
					<select name="edit_logistics_territory" id="edit_logistics_territory" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($logistics_territory as $logistics_territory_name) {  ?>
						<option value="<?php echo $logistics_territory_name->territory_name_id; ?>"<?php echo ($logistics_territory_name->territory_name_id==$get_logistics_weight_data['territory_name_id'])? "selected":''; ?>><?php echo $logistics_territory_name->territory_name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
				
			</div>	
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Choose Delivery Mode</label>
					<select name="delivery_mode" id="delivery_mode" class="form-control" disabled>
						<option value="<?php echo $logistics_delivery_mode_id;?>"><?php echo $delivery_mode;?></option>
							
					</select>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Parcel Type</label>
					<select name="parcel_type" id="parcel_type" class="form-control" disabled>
						<option value="<?php echo $logistics_parcel_category_id;?>"><?php echo $parcel_type;?></option>
							
					</select>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Enter Weight in grams Eg.0-0.5</label>
					<input id="edit_weight_in_kg" name="edit_weight_in_kg" type="text" class="form-control" value="<?php echo $get_logistics_weight_data["weight_in_kg"];?>"/>
				</div>
				</div>
			</div>	
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">For Every Additional Stuff</label>
					<select id="additional_weight_in_kg" name="additional_weight_in_kg" class="form-control"  onchange="additional_weight_in_kgFun(this)"/>
						<option value="no" <?php if($get_logistics_weight_data["additional_weight_in_kg"]=="no"){echo "selected";}?> <?php if($check_break_point_is_there_or_not_logistics_weight_edit_status=="yes"){echo "disabled";}?>>No</option>
						<option value="yes" <?php if($get_logistics_weight_data["additional_weight_in_kg"]=="yes"){echo "selected";}?> >Yes</option>
					</select>
				</div>
				</div>
			</div>	
			
			<div class="row" id="additional_weight_value_in_kg_div" <?php if($get_logistics_weight_data["additional_weight_in_kg"]==""){echo "style='display:none;'";}else{echo "style='display:block;'";}?>>
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">Enter Additional KG</label>
					<input id="additional_weight_value_in_kg" name="additional_weight_value_in_kg" type="text" class="form-control"  value="<?php echo $get_logistics_weight_data["additional_weight_value_in_kg"];?>"/>
				</div>
				</div>
			</div>	
			
			
			<div class="row" id="do_u_want_to_roundup_div" <?php if($get_logistics_weight_data["do_u_want_to_roundup"]==""){echo "style='display:none;'";}else{echo "style='display:block;'";}?>>
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">Do you want to round up</label>
					<select id="do_u_want_to_roundup" name="do_u_want_to_roundup" class="form-control"  onchange="do_u_want_to_roundupFun(this)"/>
						<option value=""  <?php if($get_logistics_weight_data["do_u_want_to_roundup"]==""){echo "selected";}?>></option>
						<option value="no" <?php if($get_logistics_weight_data["do_u_want_to_roundup"]=="no"){echo "selected";}?>>No</option>
						<option value="yes"  <?php if($get_logistics_weight_data["do_u_want_to_roundup"]=="yes"){echo "selected";}?>>Yes</option>
					</select>
				</div>
				</div>
			</div>	
			
			<div class="row" id="do_u_want_to_include_breakupweight_div" <?php if($get_logistics_weight_data["do_u_want_to_include_breakupweight"]==""){echo "style='display:none;'";}else{echo "style='display:block;'";}?>>
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">Do you want to include breakup point</label>
					<select id="do_u_want_to_include_breakupweight" class="form-control"  name="do_u_want_to_include_breakupweight"/>
						<option value="" <?php if($get_logistics_weight_data["do_u_want_to_include_breakupweight"]==""){echo "selected";}?>></option>
						<option value="no" <?php if($get_logistics_weight_data["do_u_want_to_include_breakupweight"]=="no"){echo "selected";}?>>No</option>
						<option value="yes"  <?php if($get_logistics_weight_data["do_u_want_to_include_breakupweight"]=="yes"){echo "selected";}?>>Yes</option>
					</select>
				</div>
				</div>
			</div>	
			
			
			

			<?php /*
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Enter Volumetric Weight</label>
					<input id="edit_volume_in_kg" name="edit_volume_in_kg" type="text" class="form-control" value="<?php echo $get_logistics_weight_data["volume_in_kg"];?>"/>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Enter Charge</label>
					<input id="edit_logistics_price" name="edit_logistics_price" type="text" class="form-control" value="<?php echo $get_logistics_weight_data["logistics_price"];?>"/>
				</div>
				</div>
			</div>
			
			*/ ?>
			
			<!----new structure for volumetric weight---->
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
					<h4>Define Various Volumetric Weight with Charge</h4>					
				</div>
				
			</div>		

			<div class="row">
					<div class="col-md-4 col-md-offset-1">  
					<div class="form-group label-floating">
						<label class="control-label">Less than</label>

						
						<input name="lesser_v" value='<?php echo isset($lesser["volume_in_kg"]) ? $lesser["volume_in_kg"] : '';?>' type="text" class="form-control" />
						<input name="lesser_id" value='<?php echo isset($lesser["logistics_weight_id"]) ? $lesser["logistics_weight_id"] : '';?>' type="hidden" class="form-control" />
					</div>
					</div>
					<div class="col-md-4">  
					<div class="form-group label-floating">
						<label class="control-label">Charge</label>
						<input name="lesser_m" value='<?php echo isset($lesser["logistics_price"]) ? $lesser["logistics_price"] : '';?>' type="text" class="form-control" />
					</div>
					</div>				
			</div>
			
			<?php
			/*
			?>
			<div class="row multi-fields">
				
				<?php 
				$original_ids=array();
				
				foreach($range as $ran){
					
						$volume=$ran["volume_in_kg"];
						$volume_arr=explode('-',$volume);
						$v_from=$volume_arr[0];
						$v_to=$volume_arr[1];
						$original_ids[]=$ran["logistics_weight_id"];
						?>
	
						<div class="row form-group add_range">
							<div class="col-md-2 col-md-offset-1">  
							<div class="form-group label-floating">
								<label class="control-label">From</label>
								<input name="v_from[]" value='<?php echo $v_from; ?>' type="text" class="form-control" />
							</div>
							</div>
							
							<div class="col-md-2">  
							<div class="form-group label-floating">
								<label class="control-label">To</label>
								<input name="v_to[]" value='<?php echo $v_to; ?>' type="text" class="form-control" />
							</div>
							</div>
							<div class="col-md-4">  
							<div class="form-group label-floating">
								<label class="control-label">Charge</label>
								<input name="v_m[]" value='<?php echo $ran["logistics_price"]; ?>' type="text" class="form-control" />
							</div>
							</div>
								<input name="v_ids[]" value='<?php echo $ran["logistics_weight_id"]; ?>' type="hidden" class="form-control" />
							<div class="col-md-3 text-center">  	
								<button type="button" class="btn btn-danger pull-right btn-sm remove_range">X</button>			
							</div>
						</div>
						
						<?php
				} 
				
				?>
				
			</div>	
			
			
			
			<div class="row">
				<div class="col-md-12 text-right">
				<button type="button" id="add_one_more" name="add_one_more" class="btn btn-warning add_one_more pull-right btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Add New range</button>
				</div>
				<?php 
					foreach($original_ids as $id){
						?>
						<input name="original_ids[]" value='<?php echo $id; ?>' type="hidden"/>
						<?php
					}
				?>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Greater than</label>
					<?php
					if(isset($greater["volume_in_kg"])){
						$volume_in_kg=$greater["volume_in_kg"];
						if(strpos($volume_in_kg, '>') !== false) {
							$volume_in_kg=str_replace('>', '', $volume_in_kg);
						}
					}
					else{
						$volume_in_kg="";
					}
					?>
					<input name="greater_v" type="text" value='<?php echo $volume_in_kg;?>' class="form-control"/>
					<input name="greater_id" value='<?php if(isset($greater["logistics_weight_id"])){echo $greater["logistics_weight_id"];}else{echo "";}?>' type="hidden" class="form-control" />
				</div>
				</div>
				<div class="col-md-4">  
				<div class="form-group label-floating">
					<label class="control-label">Charge</label>
					<input name="greater_m" type="text" value='<?php if(isset($greater["logistics_price"])){echo $greater["logistics_price"];}else{echo "";}?>' class="form-control"/>
				</div>
				</div>				
			</div>
			
			<?php
			*/
			?>
<!----new structure for volumetric weight---->

			<div class="row">
				<div class="col-md-8 col-md-offset-3"> 
					<button class="btn btn-info btn-sm" type="submit" id="submit-data" disabled="">Submit</button>
		
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
				</div>
			</div>

		</div>		
	</form>
	<form name="search_for_logistics_weight" id="search_for_logistics_weight" method="post">
		<input value="<?php echo $get_logistics_weight_data["vendor_id"]; ?>" type="hidden" id="vendor_id" name="vendor_id"/>
		<input value="<?php echo $get_logistics_weight_data["logistics_id"]; ?>" type="hidden" id="logistics_id" name="logistics_id"/>
		<input value="<?php echo $get_logistics_weight_data["territory_name_id"]; ?>" type="hidden" id="logistics_territory_id" name="logistics_territory_id"/>
		
		<input value="<?php echo $get_logistics_weight_data["logistics_parcel_category_id"]; ?>" type="hidden"  name="parcel_type"/>
		
		<input value="<?php echo $get_logistics_weight_data["logistics_delivery_mode_id"]; ?>" type="hidden"  name="delivery_mode"/>
		
		<input value="view" type="hidden" name="create_editview">
	</form>
</div>
</div>
</div>
</div>

</div>
<div class="footer">
</div>
</div>
</body>
</html>
<script type="text/javascript">

$(function(){
	
$('.common').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add_one_more", $(this)).click(function(e) {
        $('.add_range:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('');
		$('.remove_range').show();
		return false;
    });
	$($wrapper).on("click",".remove_range", function(e){ 
	elements_length=$('.add_range > div', $wrapper).length;
	e.preventDefault();
		if(elements_length==1){
			$('.remove_range').hide();
		}else{
			$('.remove_range').show();
			$(this).parent('div').parent('div').remove();	
		}
     
    })
	//remove of pincode range
});

var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_logistics_weight'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_logistics_weight"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_logistics_weight'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});
});

function do_u_want_to_roundupFun(obj){
	if(obj.value=="yes"){
		$("#do_u_want_to_include_breakupweight_div").css({"display":"block"});
	}
	else{
		$("#do_u_want_to_include_breakupweight_div").css({"display":"none"});
		$("#do_u_want_to_include_breakupweight").val("");
	}
}
function additional_weight_in_kgFun(obj){
	if(obj.value=="yes"){
		$("#additional_weight_value_in_kg_div").css({"display":"block"});
		$("#do_u_want_to_roundup_div").css({"display":"block"});
	}
	else{
		$("#do_u_want_to_include_breakupweight_div").css({"display":"none"});
		$("#do_u_want_to_include_breakupweight").val("");
		$("#do_u_want_to_roundup_div").css({"display":"none"});
		$("#do_u_want_to_roundup").val("");
		$("#additional_weight_value_in_kg_div").css({"display":"none"});
		$("#additional_weight_value_in_kg").val("");
	}
}

function form_validation_edit()
{

	var edit_vendors = $('select[name="edit_vendors"]').val().trim();
	var logistics = $('select[name="edit_logistics"]').val().trim();
	var logistics_territory = $('select[name="edit_logistics_territory"]').val().trim();
	
	var weight_in_kg = $('input[name="edit_weight_in_kg"]').val().trim();
	//var volume_in_kg = $('input[name="edit_volume_in_kg"]').val().trim();	
	//var logistics_price = $('input[name="edit_logistics_price"]').val().trim();	

	   var err = 0;
		if((edit_vendors=='') || (logistics=='') || (logistics_territory=='') || (weight_in_kg=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
				document.getElementById("edit_vendors").disabled="";
				document.getElementById("edit_logistics").disabled="";
				document.getElementById("edit_logistics_territory").disabled="";
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#edit_logistics_weight');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_logistics_weight/"?>',
				type: 'POST',
				data: $('#edit_logistics_weight').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {		
				if(data)
				{		
					  					  
					swal({
						title:"Success", 
						text:"Logistics Weight is successfully updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						showDivPrevious();

					});
				}
				else
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)	
				}
				
			});
}
}]);			
			}
	
	return false;
}


</script>
