<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/multiselect.js"></script>
<style>
.margin_top{
	margin-top:5em;
}
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
select.form-control[multiple], .form-group.is-focused select.form-control[multiple] {
    height: 140px;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script type="text/javascript">



function test(){

var button = $('#submit-data');
var orig = [];
var orig1 = [];
$.fn.getType = function () {
	//alert(this[0].tagName +"|"+ $(this[0]).attr("multiple"));
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : ((this[0].tagName.toLowerCase()=="select" && $(this[0]).attr("multiple")=="multiple") ? (this[0].tagName.toLowerCase()+"-"+$(this[0]).attr("multiple")) : this[0].tagName.toLowerCase());
}
search_arr=[];
$("form[name='edit_logistics_territory_states_cities'] :input").each(function () {
		
		var type = $(this).getType(); 
		
		var tmp = {
			'type': type,
			'value': $(this).val()
		};
		
		if (type == 'radio' || type == 'text' || type == 'select') {
			tmp.checked = $(this).is(':checked');
			orig[$(this).attr('id')] = tmp;
		}
		if (type == 'select-multiple') {
			 search_arr=[];
			 $(this).find("option").each(function(){
				 search_arr.push($(this).attr("value"));
			 });
			orig1[$(this).attr('id')] = search_arr;
		}
});

$('form[name="edit_logistics_territory_states_cities"]').bind('change keyup click', function () {
    var disable = true;
    $("form[name='edit_logistics_territory_states_cities'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'select-multiple') {
			 search_cur_arr=[];
			$(this).find("option").each(function(){
				 search_cur_arr.push($(this).attr("value"));
			 });
            disable = (orig1[id].sort().join("-") == search_cur_arr.sort().join("-"));
			
        }else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false;
        }
    });

    button.prop('disabled', disable);
});

/////////////////////////
}

</script>

<script>
function showDivPrevious(){
	var form = document.getElementById('search_for_logistics_territory_states_cities');		
	
	form.action='<?php echo base_url()."admin/Catalogue/logistics_territory_states_cities"; ?>';
	form.submit();
}
</script>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">

	<form name="edit_logistics_territory_states_cities" id="edit_logistics_territory_states_cities" method="post" action="#" onsubmit="return form_validation_edit();">
	<input type="hidden" name="edit_logistics_territory_state_city_id" value="<?php echo $logistics_territory_state_city_id;?>">
	
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_vendors: {
					required: true,
				},
				edit_logistics: {
					required: true,
				},
				edit_logistics_territories: {
					required: true,
				 },
				// 'to_select_list[]': {
				// 	required: true,
				// }
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Logistics Territory States Cities</h3>
		</div>	
		</div>
		<div class="tab-content">
			<div class="row">
				<div class="col-md-12">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Vendor-</label>
					<select name="edit_vendors" id="edit_vendors" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($vendors as $vendors_value) {  ?>
						<option value="<?php echo $vendors_value->vendor_id; ?>" <?php if($get_logistics_territory_states_cities["vendor_id"]==$vendors_value->vendor_id){echo "selected";} ?>><?php echo $vendors_value->name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Logistics Name-</label>
					<select name="edit_logistics" id="edit_logistics" class="form-control" disabled>
						<option value="<?php echo $get_logistics_territory_states_cities["logistics_id"];?>"><?php echo $get_logistics_territory_states_cities["logistics_name"];?></option>
					</select>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Logistics Territory-</label>
					<select name="edit_logistics_territories" id="edit_logistics_territories" class="form-control" disabled>
						<option value="<?php echo $get_logistics_territory_states_cities["territory_name_id"];?>"><?php echo $territory_name;?></option>
					</select>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Logistics State-</label>
					<select name="edit_logistics_states" id="edit_logistics_states" class="form-control" disabled>
						<option value="<?php echo $get_logistics_territory_states_cities["state_id"];?>"><?php echo $state_name;?></option>
					</select>
				</div>
				</div>
			</div>
			
			<!----- division ------>

			<div class="row">
				<div class="col-md-12">                               
				<h5>Select Divisions</h5>
				</div>
			</div>	

			<div class="row">
				<div class="col-md-12">  
                
                    <select name="select_list_division[]" id="from_select_list_division" class="form-control" size="8" multiple="multiple" required> 
						<?php

							$selected_divisions=$get_logistics_territory_states_cities['division_id_list'];
							$selected_divisions_arr=array();
							if($selected_divisions!=''){
								$selected_divisions_arr=explode('|',$selected_divisions);
							}
							foreach($all_divisions as $div){

								$selected='';
								if(!empty($selected_divisions_arr)){
									if(in_array($div["Division_ID"],$selected_divisions_arr)){
										$selected='selected';
									}
								}

								?>
								<option value="<?php echo $div["Division_ID"];?>" <?php echo $selected; ?>><?php echo $div["Division_Name"]." - ".$div["Division_ID"];?></option>
								<?php
							}
						?>
                    </select>
                </div>
			</div>
                

			<!----- division ------>

			<div class="row" style="display:none;">
				<div class="col-md-12">                               
				<h5>Select Cities</h5>
				</div>
			</div>	

			<div class="row" style="display:none;">
				<div class="col-md-12">  
                <div class="col-sm-5">
                    <select name="from_select_list[]" id="from_select_list" class="form-control" size="8" multiple="multiple"> 
						<?php
							foreach($logistics_cities_by_statesObj as $vObj){
								?>
								<option value="<?php echo $vObj->pincode_id;?>"><?php echo $vObj->city." - ".$vObj->pin;?></option>
								<?php
							}
						?>
                    </select>
                </div>
                
                <div class="col-sm-2 margin_top">
                    <button type="button" id="create_rightAll" class="btn btn-block btn-xs"><i class="glyphicon glyphicon-forward"></i></button>
                    <button type="button" id="create_rightSelected" class="btn btn-block btn-xs"><i class="glyphicon glyphicon-chevron-right"></i></button>
                    <button type="button" id="create_leftSelected" class="btn btn-block btn-xs"><i class="glyphicon glyphicon-chevron-left"></i></button>
                    <button type="button" id="create_leftAll" class="btn btn-block btn-xs"><i class="glyphicon glyphicon-backward"></i></button>
                </div>
                
                <div class="col-sm-5">
                    <select name="to_select_list[]" id="to_select_list" class="form-control" size="8" multiple="multiple">
						<?php
							foreach($get_citylistObj as $vObj){
								?>
								<option value="<?php echo $vObj->pincode_id;?>"><?php echo $vObj->city." - ".$vObj->pin;?></option>
								<?php
							}
						?>
					</select>
                </div>
				</div>
            </div>
			
			
				
				
		
			
			<div class="row">
				<div class="col-md-9 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-xs" type="submit">Submit</button>
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
					</div>
				</div>
			</div>

		</div>		
	</form>
	<form name="search_for_logistics_territory_states_cities" id="search_for_logistics_territory_states_cities" method="post">
		<input value="<?php echo $get_logistics_territory_states_cities["vendor_id"]; ?>" type="hidden" id="vendor_id" name="vendor_id"/>
		<input value="<?php echo $get_logistics_territory_states_cities["logistics_id"]; ?>" type="hidden" id="logistics_id" name="logistics_id"/>
		<input value="<?php echo $get_logistics_territory_states_cities["territory_name_id"]; ?>" type="hidden" id="territory_name_id" name="territory_name_id"/>
		<input value="<?php echo $get_logistics_territory_states_cities["state_id"]; ?>" type="hidden" id="state_id" name="state_id"/>
		<input value="view" type="hidden" name="create_editview">
	</form>
		
</div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    // make code pretty
    $('#from_select_list').multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
        }
    });
	$('#from_select_list_division').multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
        }
    });
});
</script>
<script>
	function go(obj){
		//if(obj.options[obj.selectedIndex].value){
			test();
		//}
	}
</script>

<script type="text/javascript">
$(document).ready(function(){
	$("#create_rightAll,#create_rightSelected,#create_leftAll,#create_leftSelected").click(function(){
		setTimeout(function(){ 
			obj=document.getElementById("to_select_list");
			for(i=0;i<obj.length;i++){
				obj.options[i].selected=true;
			} 
		}, 1);
	});

});


function form_validation_edit()
{
	var obj=document.getElementById("to_select_list");
			
	for(i=0;i<obj.length;i++){
		obj.options[i].selected=true;
	}
			
	var err = 0;
	var select_err=0;
		
		$("#to_select_list option").each(function() {
				if($(this).is(':selected')){
					select_err++;
				}	
		});	
		
		if(select_err==0)
		{
			//err=1;
		}	   
		
		
		if(err==1)
		{
			   $('#error_result_id').show();
			   $('#result').hide();
		}
		else
			{
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#edit_logistics_territory_states_cities');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_logistics_territory_states_cities/"?>',
				type: 'POST',
				data: $('#edit_logistics_territory_states_cities').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {
				 if(data)
				{
					swal({
						title:"Success", 
						text:"Logistics Territory Cities is successfully Updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					}); 					
					
					  
				}
				else
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
			});
}
}]);			
			}
	
	return false;
}

</script>
</body>
</html>