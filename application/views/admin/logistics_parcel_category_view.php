<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:60%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>

<script>

$(document).ready(function(){
	<?php
		if($create_editview=="create"){
			?>
			$("#viewDiv1").css({"display":"none"});
			$("#createDiv2").css({"display":""});
			<?php
		}
		else if($create_editview=="view"){
			?>
			$("#createDiv2").css({"display":"none"});
			$("#viewDiv1").css({"display":""});
			<?php
		}
	?>
});
</script>

<script>

function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_delete_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
swal({
			title: 'Are you sure?',
			text: "Delete Logistic parcel category",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {	
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/delete_logistics_parcel_category_selected",
		type:"post",
		data:"selected_list="+selected_list,
		
		success:function(data){
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Given "+table+"(s) has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
</script>
<script>

function changeViewPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/logistics_parcel_category_filter';
}
</script>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="wizard-header text-center">
		<h5> Logistics Parcel Category 
		</h5>                 		          	
	</div>
<div class="row" id="viewDiv1">
<table id="myTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<th colspan="9">
			<button id="multiple_delete_button" class="btn btn-danger btn-xs" onclick="multilpe_delete_fun('parcel category')">Delete</button>
			<button class="btn btn-primary btn-xs" type="button" onclick="changeViewPrevious()">Change View</button>
		</th>
	</tr>
	<tr>
		<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
		<th>Sl.No</th>
		<th>Vendor Name</th>
		<th>Logists Name</th>
		<th>Parcel Type</th>
		<th>Parcel Type Description</th>
		<th>Default</th>
		<th>Last Updated</th>
		<th>Action</th>
	</tr>
</thead>
<tbody>
<?php
$i=0;
foreach ($logistics_parcel_category as $logistics_parcel_category_value) { 
$i++; ?>

<tr>
<td><input type="checkbox" name="selected_list" value="<?php echo $logistics_parcel_category_value->logistics_parcel_category_id; ?>"></td>
<td><?php echo $i; ?></td>
<td>
	<?php 
		$get_vendors_data_arr=$this->Model_catalogue->get_vendors_data($logistics_parcel_category_value -> vendor_id);
		echo $get_vendors_data_arr["name"];
	?>
</td>
<td><?php echo $logistics_parcel_category_value -> logistics_name?></td>
<td><?php echo ucwords($logistics_parcel_category_value -> parcel_type); ?></td>
<td><?php echo $logistics_parcel_category_value -> parcel_type_description?></td>
<td><?php  if($logistics_parcel_category_value -> default_parcel_category==1){echo "True";}else{echo "False";}?></td>
<td><?php echo $logistics_parcel_category_value -> timestamp?></td>

<td>
	
	<form action="<?php echo base_url()?>admin/Catalogue/edit_logistics_parcel_category_form" method="post">	<input type="hidden" value="<?php echo $logistics_parcel_category_value -> logistics_parcel_category_id;?>" name="logistics_parcel_category_id"><input type="hidden" value="<?php echo $logistics_parcel_category_value -> vendor_id;?>" name="vendor_id"><input type="hidden" value="<?php echo $logistics_parcel_category_value -> logistics_id;?>" name="logistics_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>
</td>

</tr>
<?php } ?>
</tbody>
</table>
</div>

<div class="row-fluid" id="createDiv2" style="display:none;">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">

	<form name="create_logistics_parcel_category" id="create_logistics_parcel_category" method="post" action="#" onsubmit="return form_validation();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				vendors: {
					required: true,
				},
				logistics: {
					required: true,
				},
				parcel_type: {
					required: true,
				},
				parcel_type_description: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Create Logistics Parcel Category</h3>
		</div>	
		</div> 
		
		<div class="tab-content">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Vendor-</label>
					<select name="vendors" id="vendors" class="form-control" onchange="showAvailableLogistics(this)">
						<option value=""></option>
						<?php foreach ($vendors as $vendors_value) {  ?>
						<option value="<?php echo $vendors_value->vendor_id; ?>"><?php echo $vendors_value->name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Logistics Name-</label>
					<select name="logistics" id="logistics" class="form-control">
						<option value=""></option>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Parcel Type</label>
					<input id="parcel_type" name="parcel_type" type="text" class="form-control" />
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Description of Parcel Type</label>
					<textarea id="parcel_type_description" name="parcel_type_description" class="form-control" ></textarea>
				</div>
				</div>
			</div>
			
				<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group">
					<div class="col-md-6">Make Default Logistics Parcel Category</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="default_logistics" id="default_logistics1" value="1" type="radio" checked="checked">View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="default_logistics" id="default_logistics2" value="0" type="radio" checked="checked">Hide</label>
					</div>
				
					</div>
				</div>
			</div>			
			<div class="row">
				<div class="col-md-8 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit">Submit</button>
					<button class="btn btn-warning btn-sm" type="reset">Reset</button>
					<button class="btn btn-primary btn-xs" type="button" onclick="changeViewPrevious()">Change View</button>
					</div>
				</div>
			</div>		
			
		</div>		
	</form>

</div>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>
</body>
</html>
<script>
$(document).ready(function(){
    $('#myTable').dataTable({ stateSave: true });
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
});

function showAvailableLogistics(obj){
	
		vendor_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_logistics",
				type:"post",
				data:"vendor_id="+vendor_id,
				success:function(data){
					
					if(data!=0){

						$("#logistics").html(data);		
					}
					else{
						$("#logistics").html('<option value="">--No Logistics--</option>');	
						
					}
				}
			});
	
}
function form_validation()
{
	var vendors = $('input[name="vendors"]').val();
	var logistics = $('input[name="logistics"]').val();
	var parcel_type = $('input[name="parcel_type"]').val().trim();
	var parcel_type_description = $('textarea[name="parcel_type_description"]').val().trim();	
	

	   var err = 0;
		if((vendors=='') || (logistics=='') || (parcel_type=='') || (parcel_type_description=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#create_logistics_parcel_category');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_logistics_parcel_category/"?>',
				type: 'POST',
				data: $('#create_logistics_parcel_category').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					document.getElementById("parcel_type").value="";
					document.getElementById("parcel_type").focus();
				});
			}
				if(data=="yes")
				{
					swal({
						title:"Success", 
						text:"Logistics Parcel Category is successfully added!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});  					  
					
				}
				if(data=="no")
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
			});
}
}]);			
			}
	
	return false;
}

</script>
