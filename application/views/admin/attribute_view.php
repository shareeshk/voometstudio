<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/bootstrap-colorpicker.min.css" rel="stylesheet">
<style>
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}
.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.input-group .input-group-addon {
	background-color:#ccc;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo base_url();?>assets/js/color_classifier.js"></script>
<script>
	function showPicker_1(elem){
		if(elem.value ==0){
      document.getElementById('colorpicker').style.display = "none";
	  document.getElementById('otherattribute').style.display = "none";
   }
	if(elem.value == 1){
      document.getElementById('colorpicker').style.display = "block";
	  document.getElementById('otherattribute').style.display = "none";
   }	
   if(elem.value == 2){
      document.getElementById('otherattribute').style.display = "block";
	  document.getElementById('colorpicker').style.display = "none";
   }
}
</script>
<script>
	function showPicker_edit(elem){
		if(elem.value ==0){
      document.getElementById('colorpicker_edit').style.display = "none";
	  document.getElementById('otherattribute_edit').style.display = "none";
   }
	if(elem.value == 1){
      document.getElementById('colorpicker_edit').style.display = "block";
	  document.getElementById('otherattribute_edit').style.display = "none";
   }	
   if(elem.value == 2){
      document.getElementById('otherattribute_edit').style.display = "block";
	  document.getElementById('colorpicker_edit').style.display = "none";
   }
}
</script>
<script>
var table;

$(document).ready(function (){
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    var rows_selected = [];
    table = $('#table_attribute').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Catalogue/attribute_processing",
			data: function (d) { d.pcat_id = $('#pcat_id').val();d.cat_id = $('#cat_id').val();d.subcat_id = $('#subcat_id').val();d.active=1},
            type: "post",
            error: function(){
              $("#table_attribute_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				
				document.getElementById("attributes_count").innerHTML=json.recordsFiltered;
				
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
   
	$('#reset_form_button').on('click',function(){
		table.draw();
	});
	
	$("#submit_button").click(function() {
		table.draw();
	});
	

});	


function drawtable(){
	table.draw();
}

function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_delete_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
	swal({
			title: 'Are you sure?',
			text: "Archived Attribute",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Archive it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/update_attribute_selected",
		type:"post",
		data:"selected_list="+selected_list+"&active=0",
		success:function(data){
			if(data==true){
				swal({
						title:"Archived!", 
						text:"Given "+table+"(s) has been archived successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	})
		    },
	allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});

}
function multilpe_delete_when_no_data(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
		swal({
			title: 'Are you sure?',
			text: "Delete Attribute",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/delete_attribute_when_no_data",
		type:"post",
		data:"selected_list="+selected_list,
		
		success:function(data){
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Given "+table+"(s) has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else if(data==false){
				swal("Error", "Error in deleting this file", "error");
			}else{
				swal("Error", "Oops ..! You can't delete it.", "error");
			}
		}
	});
	})
		    },
	allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
</script>
<script>

$(document).ready(function(){
	<?php
		if($create_editview=="create"){
			?>
			$("#viewDiv1").css({"display":"none"});
			$("#createDiv2").css({"display":""});
			<?php
		}
		else if($create_editview=="view"){
			?>
			$("#createDiv2").css({"display":"none"});
			$("#viewDiv1").css({"display":""});
			<?php
		}
	?>
});
</script>
<script>

function changeViewPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/attribute_filter';
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<?php 
	//print_r($attributes);
?>
<div class="container">

<div class="row" id="viewDiv1" style="display:none;">
	<div class="wizard-header text-center">
		<h5> Number of Attribute <span class="badge"><div id="attributes_count"></div></span> 
		</h5>                 		          	
	</div>
	<input value="<?php echo $pcat_id; ?>" type="hidden" id="pcat_id">
	<input value="<?php echo $cat_id; ?>" type="hidden" id="cat_id">
	<input value="<?php echo $subcat_id; ?>" type="hidden" id="subcat_id">
	
<table id="table_attribute" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<th colspan="6">
			<button id="multiple_delete_button" class="btn btn-warning btn-xs" onclick="multilpe_delete_fun('attribute')">Archive all</button>
			<button id="multiple_delete_button2" class="btn btn-danger btn-xs" onclick="multilpe_delete_when_no_data('attribute')">Delete</button>
			<button class="btn btn-primary btn-xs" type="button" onclick="changeViewPrevious()">Change View</button>
		</th>
	</tr>
	<tr>
		<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
		<th>Attributes</th>
		<th>Attribute to catalog</th>
		<th>View</th>
		<th>Last Updated</th>
		<th>Action</th>		
	</tr>
</thead>
</table>
</div>

<div class="row" id="createDiv2" style="display:none;">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
             
	<form name="create_attribute" id="create_attribute" method="post" action="#" onsubmit="return form_validation();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				parent_category: {
					required: true,
				},
				categories: {
					required: true,
				},
				subcategories: {
					required: true,
				},
				select_color: {
					required: true,
				},
				attribute1_name: {
					required: true,
				},
				'color_value[]': {
					required: true,
				},
				'color_name[]': {
					required: true,
				},
				'attribute11_name': {
					required: true,
				},
				'attribute11_option': {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Create Attribute</h3>
		</div>	
		</div>
		<div class="tab-content">
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Parent category-</label>
					<select name="parent_category" id="parent_category" class="form-control" onchange="showAvailableCategories(this)">
						<option value=""></option>
						<?php foreach ($parent_category as $parent_category_value) {  	
						if(in_array($parent_category_value->pcat_id,$parent_cat_check)){
						?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>" disabled><?php echo $parent_category_value->pcat_name; ?></option>
						<?php 
						}
						else{ 
						?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>"><?php echo $parent_category_value->pcat_name; ?></option>
						<?php 
							} 
						} 
						?>
						<option value="0">--None--</option>
						
					</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group label-floating">
					<label class="control-label">-Select Category-</label>
					<select name="categories" id="categories" class="form-control" onchange="showAvailableSubCategories(this)">
						<option value=""></option>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Sub Category-</label>
					<select name="subcategories" id="subcategories" class="form-control" onchange="showAvailableBrands(this)">
						<option value=""></option>
						
					</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group label-floating">
					<label class="control-label">-Select option for an Attribute1-</label>
					<select name="select_color" id="select_color" class="form-control" onchange="showPicker_1(this)">
						<option value="" selected></option>
						<option value="1">Color Attribute</option>
						<option value="2">Others Attribute</option>
						
					</select>
				</div>
				</div>
			</div>

	<!---color_picker--------------->
	
	<div class="row" style="display:none;" id="colorpicker">
		
		
		<div class="col-md-7 col-md-offset-1">                               
			<div class="form-group label-floating">
			<label class="control-label">Enter Attribute Name (1)</label>
				<input id="attribute1_name" name="attribute1_name" type="text" class="form-control" value="Color">
			</div>
		</div>
		<div class="col-md-3">                               
				<div class="form-group label-floating">
				<button id="add_one_more" type="button" name="add_one_more" class="btn btn-info add_one_more pull-right">Insert options</button>
			</div>
		</div>				
		<div class="multi-fields">

		</div>
	
		<textarea id="attribute1_option" name="attribute1_option" style="display:none" class="form-control" placeholder="Enter Attribute Options (1) Seperated by Comma(,)" ></textarea>
		
	</div>
	
	
<script type="text/javascript">
 call_this();

function call_this(){
	$('.create-color-picker').each(function(i) {
			
		$(this).removeAttr('id');
		
		$(this).attr('id', 'cp' + i);
		$('#cp'+i).colorpicker({
			format: "hex",
			colorSelectors: {
                'black': '#000000',
                'white': '#ffffff',
                'red': '#FF0000',
                'default': '#777777',
                'primary': '#337ab7',
                'success': '#5cb85c',
                'info': '#5bc0de',
                'warning': '#f0ad4e',
                'danger': '#d9534f'
            }
		});
		
		$('#cp'+i).colorpicker().on('changeColor', function (e) {
			call_function(i);
		});
	});
	
	$('.sample_name').each(function(i) {
		$(this).removeAttr('id');
		$(this).attr('id', 'sample_name_' + i);
		
	});
}


$(function() {
	var $wrapper = $('.multi-fields', this);
    $(".add_one_more", $(this)).click(function(e) {
		
		var color_value = $('input[name="color_value[]"]').val();
		var color_name = $('input[name="color_name[]"]').val();

		if(color_value==""){
		   $('input[name="color_value[]"]').css("border", "1px solid red");	   
		  	
		}else if(color_name==""){
			$('input[name="color_value[]"]').css({"border": "1px solid #fff"});
		  $('input[name="color_name[]"]').css("border", "1px solid red");	
		}else{			
		
			$('input[name="color_name[]"]').css({"border": "1px solid #fff"});
		
        //$('.add_range:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('');
		$wrapper.append('<div class="add_range col-md-12 col-md-offset-1"><div class="col-md-3 "><div class="create-color-picker input-group colorpicker-component"><div class="form-group label-floating"><label class="control-label">Pick color</label><input name="color_value[]" value="" class="form-control" type="text"/></div><span class="input-group-addon"></span></div></div><div class="col-md-3 "><div class="form-group label-floating"><label class="control-label">Type color name</label><input id="color_name" name="color_name[]" value="" class="form-control" type="text" /></div></div><div class="col-md-3"><div class="form-group label-floating"><input name="color_name_sample[]" value="" class="form-control sample_name" type="text" placeholder="Sample color name" readonly/></div></div><div class="col-md-1"><button class="btn btn-danger remove_range"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>');
		
		$('.remove_range').show();	
		call_this();
		return false;
		}
    });
	
	$($wrapper).on("click",".remove_range", function(e){	
	elements_length=$('.add_range', $wrapper).length;
	e.preventDefault(); 
		if(elements_length==1){
			$('.remove_range').hide();
			$(this).parent('div').parent('div').remove();
				
		}else{
		$('.remove_range').show();
		$(this).parent('div').parent('div').remove();	
		
		}
        
    })
	
 });

//$('#cp3').colorpicker().on('changeColor', function (e) {
//	call_function();
//});
		
function call_function(j){

window.classifier = new ColorClassifier();
url='<?php echo base_url() ?>assets/js/dataset.js';
get_dataset(url, function (data){
    window.classifier.learn(data);
	color_value=document.getElementsByName('color_value[]')[j].value;
	
	var result_name = window.classifier.classify(color_value);
	//color_val=$('#sample_name_'+j).val(result_name);
	document.getElementsByName('color_name_sample[]')[j].value=result_name;
});

}
</script>

	<!---color_picker--------------->
				<div class="row" style="display:none;" id="otherattribute">
					<div class="col-md-5 col-md-offset-1">   
					<div class="form-group label-floating">
						<label class="control-label">Enter Attribute Name (1)</label>
						<input id="attribute11_name" name="attribute11_name" type="text" class="form-control" />
					</div>
					</div>
					
					<div class="col-md-5">   
					<div class="form-group label-floating">
						<label class="control-label">Enter Attribute Options (1) Seperated by Comma(,)</label>
						<textarea id="attribute11_option" name="attribute11_option" type="text" class="form-control" ></textarea>
					</div>
					</div>
					
				</div>
				
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">Enter Attribute Name (2)</label>
					<input id="attribute2_name" name="attribute2_name" type="text" class="form-control" />
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Attribute Options (2) Seperated by Comma(,)</label>
					<textarea id="attribute2_option" name="attribute2_option" type="text" class="form-control" ></textarea>
				</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">Enter Attribute Name (3)</label>
					<input id="attribute3_name" name="attribute3_name" type="text" class="form-control"  />
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Attribute Options (3) Seperated by Comma(,) </label>
					<textarea id="attribute3_option" name="attribute3_option" type="text" class="form-control" ></textarea>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">Enter Attribute Name (4)</label>
					<input id="attribute4_name" name="attribute4_name" type="text" class="form-control" />
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Attribute Options (4) Seperated by Comma(,)</label>
					<textarea id="attribute4_option" name="attribute4_option" type="text" class="form-control" ></textarea>
				</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10 col-md-offset-3"> 
					<div class="form-group">
					<div class="col-md-3">Show Attribute</div>
					<div class="col-md-2">
						<label class="radio-inline"><input name="view" id="view1" value="1" type="radio" checked="checked">View</label>
					</div>
					<div class="col-md-2">
						<label class="radio-inline"><input name="view" id="view2" value="0" type="radio">Hide</label>
					</div>
				
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-8 col-md-offset-4"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit">Submit</button>
					<button class="btn btn-warning btn-sm" type="reset">Reset</button>
					<button class="btn btn-primary btn-sm" type="button" onclick="changeViewPrevious()">Change View</button>
					</div>
				</div>
			</div>	
		</div>	
		
	</form>

</div>
</div>
</div>
</div>
<div class="footer">
</div>

</div>

<script>
function showAvailableBrands(obj){
	
	//if(obj.value!="" && obj.value!="None"){
		subcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_brands",
				type:"post",
				data:"subcat_id="+subcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#brands").html(data);
					}
					else{
						$("#brands").html('<option value=""></option>');

					}
				}
			});
	//}
	
}
function showAvailableSubCategories(obj){
		cat_id=obj.value;
	//if(obj.value!="" && obj.value!="None"){
	
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#subcategories").html(data);
				
			$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_attribute_subcheck/"?>',
				type: 'POST',
				dataType:"JSON",
				data:"cat_id="+cat_id,
				success:function(res){
					//alert(res);
					for(x in res){
						$("#subcategories").find("option[value='"+res[x]+"']").attr({"disabled":true});
					}
					
					}
			});
					}
					else{
						$("#subcategories").html('<option value=""></option>')
						$("#brands").html('<option value=""></option>')

					}
				}
			});
	//}
	
}


function showAvailableCategories(obj){
	
	pcat_id=obj.value;
	//if(obj.value!="" && obj.value!="None"){
		
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){		
			$("#categories").html(data);
			$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_attribute_check/"?>',
				type: 'POST',
				dataType:"JSON",
				data:"pcat_id="+pcat_id,
				success:function(res){
					for(x in res){
						$("#categories").find("option[value='"+res[x]+"']").attr({"disabled":true});
					}
					
					}
			});
					}
					else{
						$("#categories").html('<option value=""></option>')
						$("#brands").html('<option value=""></option>')

					}
				}
			});
	//}
	
}


function deleteAttributeFun(attribute_id){
	
	if(confirm("Are you sure want to delete?")){

		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/delete_attribute/"+attribute_id,
				type: 'POST',
				data: "1=2",
				dataType: 'html',
			}).done(function(data){
				if(data==true){	 
					alert('successfully Deleted');
					showDiv1();
				}else{
					alert('error');
				}	
			});
	}
}


function form_validation(){	

	var err = 0;
	var value=document.getElementById("select_color").value;

if(value==1){
	var str='';
	var names=[];
	var values=[];
	$('.create-color-picker').each(function(k) {
		color_name=document.getElementsByName('color_name[]')[k].value.trim();
		color_value=document.getElementsByName('color_value[]')[k].value.trim();
		if($.inArray(color_name,names) == -1){
			// the element is not in the array
			names.push(color_name);
		}else{
			alert('duplication of name');
			return false;
		}
		if($.inArray(color_value,values) == -1){
			// the element is not in the array
			values.push(color_value);
		}else{
			alert('duplication of value');
			return false;
		}
		str+=color_name+':'+color_value;
		str+=',';
		
	});
	
	$('textarea[name="attribute1_option"]').val(str);
	var attribute1_name = $('input[name="attribute1_name"]').val().trim();
	var attribute1_option = $('textarea[name="attribute1_option"]').val().trim();
	var color_value_create = $('input[name="color_value[]"]').val().trim();
	var color_name_create = $('input[name="color_name[]"]').val().trim();
	
	if(color_value_create=="")
		{
		   
		   err = 1;
		}else{
			
		}
		if(color_name_create=="")
		{
		 
		   err = 1;
		}else{
		
		}
}
if(value==2){
	var attribute11_name = $('input[name="attribute11_name"]').val().trim();
	var attribute11_option = $('textarea[name="attribute11_option"]').val().trim();
	if(attribute11_name=="")
		{
		     
		   err = 1;
		}
		else{			
			
		}
		if(attribute11_option=="")
		{
		  
		   err = 1;
		}else{
		
		}
}
	
	var parent_category = $('select[name="parent_category"]').val().trim();
	var categories = $('select[name="categories"]').val().trim();
	var subcategories = $('select[name="subcategories"]').val().trim();
	var select_color = $('select[name="select_color"]').val().trim();
	var view = document.querySelector('input[name="view"]:checked').value;

		if(parent_category=='')
		{
		    
		   err = 1;
		}
		else{			
			
		}
		if(categories=='')
		{
		  
		   err = 1;
		}
		else{			
			
		}
		if(subcategories=='')
		{
		 
		   err = 1;
		}
		else{			
		
		}
		if(select_color==0)
		{
		     
		   err = 1;
		}
		else{			
			
		}
		if(err==1)
		{
			   $('#error_result_id').show();
			   $('#result').hide();
		}
		else
			{
		
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#create_attribute');	
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();					
					  	$.ajax({
						url: '<?php echo base_url()."admin/Catalogue/add_attribute/"?>',
						type: 'POST',
						data: $('#create_attribute').serialize()+"&value="+value,
						dataType: 'html',
						
						}).done(function(data)
						  {
							  form_status.html('') 
							if(data)
							{
								
								swal({
										title:"Success", 
										text:"Attribute is successfully added!", 
										type: "success",
										allowOutsideClick: false
									}).then(function () {
										location.reload();

									});
							}
							else
							{
								swal(
										'Oops...',
										'Error in form',
										'error'
									)
							}
							
						});
}
}]);		
			}
	return false;
}

</script>
</div>
</body>

</html>