<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="robots" content="noindex">
<title>Policies</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
    <div class="container">
	<h3>Price to be shown</h3>
<form name="create_inventory" id="create_inventory" enctype="multipart/form-data">
<div class="row">
				<div class="col-md-5"> 
				<div class="form-group label-floating">
					<label class="control-label">-Select-</label>
					<select name="price_to_be_shown" id="price_to_be_shown" class="form-control">
						<option value="base_price" <?php if($get_pricetobeshown=='base_price'){echo "selected";}?>>Show Base Price + Tax Price</option>
						<option value="selling_price" <?php if($get_pricetobeshown=='selling_price'){echo "selected";}?>>Show Price Inclusive of Taxes</option>
					</select>
				</div>
				</div>
			 </div>
			<div class="row">
				<div class="col-md-5"> 
				 <input type='button' class='btn btn-finish btn-fill btn-success btn-wd' value='Submit' onClick="price_to_be_shownFun()" />
				</div>
			 </div> 
			
</form>
    
    
</div><!-- container -->
   
   
	</div>
	
	<script>
	function price_to_be_shownFun(){
		price_to_be_shown=$("#price_to_be_shown").val();
		
		$.ajax({
									url:"<?php echo base_url();?>admin/Pricetobeshown/add_pricetobeshown",
									type: "POST",      				// Type of request to be send, called as method
									data: "price_to_be_shown="+price_to_be_shown,	// Data sent to server, a set of key/value pairs representing form fields and values 
									beforeSend:function(){
										$("#aigrid_data_div").html("<i class='fa fa-cog fa-spin fa-3x fa-fw'></i>");
									},
									success: function(data)  		// A function to be called if request succeeds
									{
										
										if(data==true){
					  swal({
						title:"Success", 
						text:"Success", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				else{
					swal(
						'Oops...',
						data,
						'error'
					)	
				}
									}	        
							   });
							   

				
			
	}
	</script>
</body>
</html>
