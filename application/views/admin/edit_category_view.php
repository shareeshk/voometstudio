<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<style type="text/css">

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:0px;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	

<script type="text/javascript">

function myeditCategory(obj,type){
	
	pcat_id="";
	if(type=="pcat"){
		if($("#edit_menu_level").val()=="2"){
				pcat_id=obj.value;
		}
	}
	
	if($("#edit_menu_level").val()!=""){
		menu_level=$("#edit_menu_level").val();
		menu_level_default_value=$("#edit_menu_level_default_value").val();
		edit_parent_category_default_value=$("#edit_parent_category_default_value").val();
		menu_level_edit_menu_sort_order=$("#menu_level_edit_menu_sort_order").val();
		menu_level_edit_menu_sort_order_arr=menu_level_edit_menu_sort_order.split("-");
		edit_menu_sort_order_arr_in_default=menu_level_edit_menu_sort_order_arr[1];
		$.ajax({
			url:"<?php echo base_url()?>admin/Catalogue/get_edit_parent_category_ajax",
			type:"post",
			data:"menu_level="+menu_level+"&menu_level_default_value="+menu_level_default_value+"&edit_parent_category_default_value="+edit_parent_category_default_value+"&pcat_id="+pcat_id+"&edit_menu_sort_order_arr_in_default="+edit_menu_sort_order_arr_in_default,
			dataType:"JSON",
			success:function(data){
			data_arr=data;
			$("#edit_parent_category").html(data_arr[0]);
			$("#edit_menu_sort_order").html(data_arr[1]);
			$("#edit_menu_sort_order").append("");
			document.getElementById("show_cat_available_common_cat").disabled = true;
			
			
			if(menu_level_edit_menu_sort_order_arr[0]!=""){
			if(menu_level_edit_menu_sort_order_arr[0]==$("#edit_menu_level").val()){
				//$("#edit_menu_sort_order").val(menu_level_edit_menu_sort_order_arr[1]);
				$("#edit_menu_sort_order").html(data_arr[1]);
				parent_cat_default_id=$("#edit_parent_category_default_value").val();
				
				if(parent_cat_default_id==$("#edit_parent_category").val()){
				$("#edit_menu_sort_order").append("<option value='"+menu_level_edit_menu_sort_order_arr[1]+"' selected>"+menu_level_edit_menu_sort_order_arr[1]+"</option>");
				if(menu_level_edit_menu_sort_order_arr[0]!=$("#edit_menu_level").val()){
				document.getElementById("show_cat_available_common_cat").disabled = true;
				}
				if(menu_level_edit_menu_sort_order_arr[0]==$("#edit_menu_level").val() && menu_level_edit_menu_sort_order_arr[1]!=0){
				document.getElementById("show_cat_available_common_cat").disabled = false;
				}
				}
				if(parent_cat_default_id!=$("#edit_parent_category").val()){
				$("#edit_menu_sort_order").append("");
				document.getElementById("show_cat_available_common_cat").disabled = true;
				}
			}
			
		}
			$('form[name="edit_category"]').trigger('change');
			}
			});

	}
}

</script>
<?php
if($get_category_data['menu_sort_order']=="0"){?>
	<script>
	$(document).ready(function () {
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	document.getElementById('show_cat_available_common_cat').disabled = true;
	});
	</script>
	<?php
}
?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="row" id="editDiv3">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">    

	<form name="edit_category" id="edit_category" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_menu_level: {
					required: true,
				},
				edit_parent_category: {
					required: true,
				},
				edit_cat_name: {
					required: true,
				},
				edit_menu_sort_order: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>	
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Category</h3>
		</div>
		</div>
		<div class="tab-content">
		
		<input type="hidden" name="edit_cat_id" id="edit_cat_id" value="<?php echo $get_category_data["cat_id"];?>">
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Menu Level-</label>
					<select name="edit_menu_level" id="edit_menu_level" class="form-control" onchange="myeditCategory(this,'menu_level')">
						<option value=""></option>
						<option value="1" <?php if($get_category_data["menu_level"]=="1"){echo "selected";}?>>Assign as Parent Category</option>
						<option value="2" <?php if($get_category_data["menu_level"]=="2"){echo "selected";}?>>Assign under Parent Category</option>
						
					</select>
					<input id="edit_menu_level_default_value" name="edit_menu_level_default_value" type="hidden" value="<?php echo $get_category_data["menu_level"];?>" >
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Parent Category-</label>		
					<select name="edit_parent_category" id="edit_parent_category" class="form-control" onchange="myeditCategory(this,'pcat')">
					<?php
						if($get_category_data["menu_level"]=='1'){
							echo '<option value="0">--None--</option>';
						}
						if($get_category_data["menu_level"]=='2'){
							echo $controller->get_edit_parent_category($get_category_data["menu_level"],$get_category_data["pcat_id"],$get_category_data["menu_level"],$get_category_data["pcat_id"]);
					
						}
						?>
					</select>
					<input id="edit_parent_category_default_value" name="edit_parent_category_default_value" type="hidden" value="<?php echo $get_category_data["pcat_id"];?>" />
				</div>
				</div>
			</div>		
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Enter Category Name</label>
					<input id="edit_cat_name" name="edit_cat_name" type="text" class="form-control" value="<?php echo $get_category_data["cat_name"];?>"/>
					<input id="edit_cat_name_default_value" name="edit_cat_name_default_value" type="hidden" value="<?php echo $get_category_data["cat_name"];?>" >
				</div>
				</div>
			</div>
					
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Enter the Sort Order</label>
					<select name="edit_menu_sort_order" id="edit_menu_sort_order" class="form-control" onchange="show_view_validatn()">
					<option value="<?php echo $get_category_data["menu_sort_order"];?>" selected><?php echo $get_category_data["menu_sort_order"];?></option>
						<?php
							if($get_sort_order_options_for_cat["key"]==""){
								if($get_category_data["menu_sort_order"]!="0"){
								?>
								<option value="0">0</option>
								<?php } ?>
								<option value="1">1</option>
								<?php
							}
							else{
								if($get_category_data["menu_sort_order"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php
								}
								$value_arr=explode(",",$get_sort_order_options_for_cat["value"]);
								foreach($value_arr as $k => $v){
									?>
										<option value="<?php echo $v;?>"><?php echo $v;?></option>
									<?php
									}
							}
						?>
					</select>
					<input id="menu_level_edit_menu_sort_order" name="menu_level_edit_menu_sort_order" type="hidden" value="<?php echo $get_category_data["menu_level"].'-'.$get_category_data["menu_sort_order"];?>" />	
				</div>
				</div>
				
			</div>		
						
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group label-floating">
			<label class="control-label">-Swap Options-</label>
			<select class="form-control" id="show_cat_available_common_cat" onchange="changeEventHandler(event)">
			<option selected value=""></option>
			<?php foreach($show_sort_order_arr as $sort_order_arr){ ?>
			<?php
				if($sort_order_arr["menu_sort_order"]!=$edit_menu_sort_order){
			?>
					<option value="<?php echo $sort_order_arr["type"]."_".$sort_order_arr["common_cat_id"];?>"><?php echo $sort_order_arr["common_cat_name"]."_".$sort_order_arr["type"]."(".$sort_order_arr["menu_sort_order"].")";?></option>
					<?php } ?>
			<?php } ?>
			</select>
			</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group">
					<div class="col-md-6">Show Category</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="edit_view" id="edit_view1" value="1" type="radio" <?php if($get_category_data["view"]=="1"){echo "checked";}?>>View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="edit_view" id="edit_view2" value="0" type="radio" <?php if($get_category_data["view"]=="0"){echo "checked";}?>>Hide</label>
					</div>
				
					</div>
				</div>
			</div>	
													
			<div class="row">
				<div class="col-md-8 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit-data" disabled="">Submit</button>
		
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
					</div>
				</div>
			</div>	
			
		</div>		
	</form>
	<form name="search_for_category" id="search_for_category" method="post">
		<input value="<?php echo $get_category_data["pcat_id"]; ?>" type="hidden" id="pcat_id" name="pcat_id">
		<input value="view" type="hidden" name="create_editview">
	</form>

</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>

<script>
function changeEventHandler(event){
        parent_id=event.target.value;
		if(parent_id!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		}
		if(parent_id==""){
	edit_menu_sort_order=document.getElementById("edit_menu_sort_order").value;
	if(edit_menu_sort_order==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;					
	}
	if(edit_menu_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
		}
}
function show_view_validatn(){
	edit_parent_category_default_value=document.getElementById("edit_parent_category_default_value").value;
	edit_parent_category=document.getElementById("edit_parent_category").value;
	edit_menu_level_default_value=document.getElementById("edit_menu_level_default_value").value;
	edit_menu_level=document.getElementById("edit_menu_level").value;
	edit_menu_sort_order=document.getElementById("edit_menu_sort_order").value;
	show_cat_available_common_cat=document.getElementById("show_cat_available_common_cat").value;
	menu_level_edit_menu_sort_order=document.getElementById("menu_level_edit_menu_sort_order").value;
	menu_level_edit_menu_sort_order_arr=menu_level_edit_menu_sort_order.split("-");
	edit_menu_sort_order_arr_in_default=menu_level_edit_menu_sort_order_arr[1];
	if(show_cat_available_common_cat==""){
	if(edit_menu_sort_order==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	if(edit_menu_level_default_value==edit_menu_level && edit_parent_category_default_value==edit_parent_category){
		if(edit_menu_sort_order_arr_in_default!=0){
	document.getElementById('show_cat_available_common_cat').disabled = false;
			}
		}
	}
	if(edit_menu_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		//document.getElementById('show_cat_available_common_cat').disabled = false;
	}
	}
	if(show_cat_available_common_cat!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
	
}
</script>
<script type="text/javascript">

$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_category'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_category"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_category'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {

            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});

});
$('#result').hide();

function form_validation_edit(){
	
	var cat_id = $('input[name="edit_cat_id"]').val().trim();
var cat_name = $('input[name="edit_cat_name"]').val().trim();
var parent_category = $('select[name="edit_parent_category"]').val().trim();
var menu_level = $('select[name="edit_menu_level"]').val().trim();
var menu_sort_order = $('select[name="edit_menu_sort_order"]').val().trim();
var view = document.querySelector('input[name="edit_view"]:checked').value;
var menu_level_default = $('input[name="edit_menu_level_default_value"]').val().trim();
var parent_category_default = $('input[name="edit_parent_category_default_value"]').val().trim();
menu_level_edit_menu_sort_order=$("#menu_level_edit_menu_sort_order").val();
menu_level_edit_menu_sort_order_arr=menu_level_edit_menu_sort_order.split("-");
var edit_menu_sort_order_default=menu_level_edit_menu_sort_order_arr[1];
	
var type="";
var common_cat_id="";
var show_sort_order="";
var show_available_common_cat_arr=[];
var show_available_common_cat=document.getElementById("show_cat_available_common_cat").value;
if(show_available_common_cat!=""){
	show_available_common_cat_arr=show_available_common_cat.split("_");

	if(show_available_common_cat_arr.length==2){
		type=show_available_common_cat_arr[0];
		common_cat_id=show_available_common_cat_arr[1];
	}
}

	   var err = 0;
	   if((cat_name=='') || (parent_category=='') || (menu_level=='') || (menu_sort_order=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		if(err==0){
				$("#pcat_id_reload").val(parent_category);
				$("#cat_id_reload").val(cat_id);
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#edit_category');	
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();
	
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_category/"?>',
				type: 'POST',
				data: $('#edit_category').serialize()+"&type="+type+"&common_cat_id="+common_cat_id+"&edit_menu_sort_order_default="+edit_menu_sort_order_default,
				dataType: 'html',
				
			}).done(function(data){
				//document.write(data)
				form_status.html('') 
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					edit_cat_name_default_value=document.getElementById("edit_cat_name_default_value").value;
					document.getElementById("edit_cat_name").value=edit_cat_name_default_value;
					document.getElementById("edit_cat_name").focus();
					
				});
				  }
				if(data==1){
					
					  swal({
						title:"Success", 
						text:"Category is successfully updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						//location.reload();
						$("#reload_form")[0].submit();
					});
				}if(data==0){
					
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
			});
}
}]);
		}
		return false;
}
</script>
<script>
	$(document).ready(function(){
		if(localStorage.getItem("dropdown_pcat_id")){
			$('#pcat_id').val(localStorage.getItem("dropdown_pcat_id")).trigger('change');
			localStorage.removeItem("dropdown_pcat_id")
		}
	});

function showDivPrevious(){
	
	var form = document.getElementById('search_for_category');		
	
	form.action='<?php echo base_url()."admin/Catalogue/category"; ?>';
	form.submit();
			
	 //window.location.href = '<?php echo base_url(); ?>admin/Catalogue/category';
}
</script>
<form action="<?php echo base_url()?>admin/Catalogue/edit_category_form" method="post" id="reload_form">
	<input type="hidden" name="pcat_id" id="pcat_id_reload" value="<?php echo $get_category_data["pcat_id"];?>">
	<input type="hidden" name="cat_id" id="cat_id_reload" value="<?php echo $get_category_data["cat_id"];?>">
</form>
</div>
</body>
</html>