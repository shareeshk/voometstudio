<html>
<head>
<title></title>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>

</head>
<body>
<?php
	//print_r($view_delivery_staff);
?>
<div class="container-fluid">
<div class="page-header"><h3>View Sub Admins</h3></div>
<div class="row-fluid" style="margin-top:1%;">
<table id="myTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<th>Profile Pic</th>
		<th>Name </th>
		<th>Email </th>
		<th>Mobile</th>
		<th>Gender</th>
		<th>DOB</th>
		<th>User Type</th>
		<th>Actions</th>
	</tr>
</thead>
<tbody>
<?php
foreach ($view_delivery_staff as $delivery_staff) { ?>
<tr>
<td><img src="<?php echo base_url().$delivery_staff -> profile_pic?>" width="30" height="30"></td>
<td><?php echo $delivery_staff -> name?></td>
<td><?php echo $delivery_staff -> email?></td>
<td><?php echo $delivery_staff -> mobile?></td>
<td><?php echo $delivery_staff -> gender?></td>
<td><?php echo date("D j M, Y",strtotime($delivery_staff -> dob));?></td>
<td><?php echo $delivery_staff -> user_type?></td>
<td>
	<input type="button" class="btn btn-warning" value="Edit" onClick="editSubAdminFun('<?php echo $delivery_staff -> user_id;?>')">
	<input type="button" class="btn btn-danger" value="Delete" onClick="deleteSubAdminFun('<?php echo $delivery_staff -> user_id;?>')">
</td>
</tr>
<?php } ?>
</tbody>
</table>
</div>
</div>
</body>
<script>
$(document).ready(function(){
    $('#myTable').dataTable({ stateSave: true });
});
function editSubAdminFun(subadmin_user_id){
	location.href="<?php echo base_url(); ?>admin/Admin_manage/update_subadmins/"+subadmin_user_id;
}
function deleteSubAdminFun(subadmin_user_id){
	if(confirm("Are you sure want to delete?")){
		location.href="<?php echo base_url(); ?>admin/Admin_manage/delete_subadmins/"+subadmin_user_id;
	}
}
</script>
</html>