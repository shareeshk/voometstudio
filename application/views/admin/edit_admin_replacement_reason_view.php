<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style type="text/css">

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.wizard-card .tab-content {
	 min-height: 400px;
	 padding:0px;
}
.formheader {
  width:60%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">

function showDivPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/admin_replacement_reason';
}

</script>
<?php

if($get_admin_replacement_reason_data['decision_return']=="accept"){?>
	<script>
	$(document).ready(function () {
	$("#edit_pickup_identifier_div").css({"display":""});
	});
	</script>
	<?php
}if($get_admin_replacement_reason_data['decision_return']=="reject"){?>
	<script>
	$(document).ready(function () {
	$("#edit_pickup_identifier_div").css({"display":"none"});
	});
	</script>
	<?php
}
?>
<?php //print_r($parent_catagories);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row-fluid" id="editDiv3" >
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="edit_admin_replacement_reason" id="edit_admin_replacement_reason" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_return_options: {
					required: true,
				},
				edit_decision_return: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Admin Replacement Reason</h3>
		</div>	
		</div> 
		
		<div class="tab-content">					
			<input type="hidden" name="id" id="id" value="<?php echo $get_admin_replacement_reason_data["id"];?>">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Admin Replacement Reason</label>
					<input id="edit_return_options" name="edit_return_options" type="text" class="form-control" value="<?php echo $get_admin_replacement_reason_data["return_options"];?>"/>
					<input id="edit_return_options_default" name="edit_return_options_default" type="hidden" class="form-control" value="<?php echo $get_admin_replacement_reason_data["return_options"];?>"/>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Decision return-</label>
					<input id="edit_decision_return" name="edit_decision_return" type="text" class="form-control" readonly="readonly" value="<?php echo $get_admin_replacement_reason_data["decision_return"];?>" />
				</div>
				</div>
			</div>
			<div class="row" id="edit_pickup_identifier_div"  style="display:none;">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Pickup Identifier</label>
					<input id="edit_pickup_identifier" name="edit_pickup_identifier" type="text" class="form-control" value="<?php echo $get_admin_replacement_reason_data["pickup_identifier"];?>"/>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-8 col-md-offset-3"> 
				<div class="form-group">
					<button class="btn btn-info btn-sm" id="submit-data" disabled="" type="submit">Submit</button>
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
				</div>
				</div>
			</div>		

		</div>		
	</form>

</div>
</div>
</div>
</div>

</div>
<div class="footer">
</div>
</div>
</body>
</html>

<script type="text/javascript">
$('#result').hide();
$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_admin_replacement_reason'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_admin_replacement_reason"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_admin_replacement_reason'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});
});
function form_validation_edit(){
	var return_options = $('input[name="edit_return_options"]').val().trim();
	var decision_return = $('input[name="edit_decision_return"]').val().trim();	

	   var err = 0;
	   if((return_options=='') || (decision_return=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
var form_status = $('<div class="form_status"></div>');		
var form = $('#edit_admin_replacement_reason');	
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();	
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_admin_replacement_reason/"?>',
				type: 'POST',
				data: $('#edit_admin_replacement_reason').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {
				  if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					edit_return_options_default=document.getElementById("edit_return_options_default").value;
					document.getElementById("edit_return_options").value=edit_return_options_default;
					document.getElementById("edit_return_options").focus();
					
				});
				  }
				if(data==1)
				{
										
					swal({
						title:"Success", 
						text:"Admin Replacement Reason is successfully updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				if(data==0)
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)		
				}
				
			});
}
}]);			
			}
	
	return false;
}


</script>
