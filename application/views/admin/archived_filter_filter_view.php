<html>
<head>
<meta charset="utf-8">
<title>Reviews and Ratings</title>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<style>
.wizard-card{
	box-shadow:none;
}
.form-group{
	margin-top:15px;
}

</style>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="row">
<div class="col-md-12">
<div class="wizard-card" data-color="green" id="wizardProfile">	
<form name="search_for_rating" id="search_for_rating" method="post" action="#" onsubmit="return form_validation();">
<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				pcat_id: {
					required: true,
					
				},
				filterbox_id: {
					required: true,
					
				},
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
			</script>

	<div class="col-md-6 col-md-offset-3">
		
		
		<div class="row">
			<div class="col-md-10 text-center">		
			<h4>Select Parent category to access Filter</h4>
		</div>
		</div>
		<input value="" type="hidden" name="active" id="active">
			<div class="row">
				<div class="col-md-10">
					<div class="form-group label-floating">
					<label class="control-label">-Select Parent Category-</label>
					<select name="pcat_id" id="pcat_id" class="form-control search_select" onchange="showAvailableCategories_drawtable(this)">
						<option value="" selected></option>
						<?php foreach ($parent_category as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>"><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<option value="0">--None--</option>
					</select>
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10">
					<div class="form-group label-floating">
					<label class="control-label">-Select Category-</label>
					<select name="cat_id" id="cat_id" class="form-control search_select" onchange="showAvailableSubCategories_drawtable(this)">
						<option value="" selected></option>
						
					</select>
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10">		
					<div class="form-group label-floating">
					<label class="control-label">-Select Sub Category-</label>
					
					<select name="subcat_id" id="subcat_id" class="form-control search_select" onchange="showAvailableFilterbox_drawtable(this)">
						<option value="" selected></option>

					</select>
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10">		
					<div class="form-group label-floating">
					<label class="control-label">-Select Filter box-</label>
					
					<select name="filterbox_id" id="filterbox_id" class="form-control search_select" onchange="drawtable(this)">
						<option value="" selected></option>
						
					</select>
					</div>
				</div>
			</div>
			
			
				<div class="row">	
					<div class="col-md-10 text-center">
						<button type="submit" class="btn btn-info" id="submit_button">Submit</button>
						<button class="btn btn-warning" type="reset" id="reset_form_button">Reset</button>
					</div>
				</div>
		
	
	</div>

</form>
</div>
</div>

</div>
</div>
</div>
</body>
</html>
<script type="text/javascript">
$(document).ready(function (){
	pcat_id=$('#pcat_id').val();
	cat_id=$('#cat_id').val();
	subcat_id=$('#subcat_id').val();
	brand_id=$('#brand_id').val();
	product_id=$('#product_id').val();
	//inventory_id=$('#inventory_id').val();

	
});

function showAvailableCategories_drawtable(obj){
	//if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=2",
				success:function(data){
					
					if(data!=0){
						val=data+'<option value="">No Category</option>';
						$("#cat_id").html(val);
					}
					else{
						$("#cat_id").html('<option value="0"></option>');
					}
				}
			});
			$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_filterbox",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=2"+"&flagtype=1&with_status=1",
				success:function(data){
					if(data!=0){
						$("#filterbox_id").html(data);
						
					}else{
						$("#filterbox_id").html('<option value=""></option>');
						
					}
				}
		});
			
	//}
	
}
function showAvailableSubCategories_drawtable(obj){
	var pcat_id=document.getElementById("pcat_id").value;
	//if(obj.value!="" && obj.value!="None"){
		cat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id+"&active=2",
				success:function(data){
					
					if(data!=0){
						val=data+'<option value="">No SubCategory</option>';
						$("#subcat_id").html(val);
					}
					else{
						$("#subcat_id").html('<option value="0"></option>');
					}
				}
			});
			$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_filterbox",
				type:"post",
				data:"pcat_id="+pcat_id+"&cat_id="+cat_id+"&active=2"+"&flagtype=2&with_status=1",
				success:function(data){
					if(data!=0){
						$("#filterbox_id").html(data);
						
					}else{
						$("#filterbox_id").html('<option value=""></option>');
					}
				}
			});
	//}
	
}
function showAvailableFilterbox_drawtable(obj){
	var pcat_id=$('#pcat_id').val();
	var cat_id=$('#cat_id').val();

	//if(obj.value!="" && obj.value!="None"){
		subcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_filterbox",
				type:"post",
				data:"pcat_id="+pcat_id+"&cat_id="+cat_id+"&subcat_id="+subcat_id+"&active=2&with_status=1"+"&flagtype=3",
				success:function(data){
					if(data!=0){
						$("#filterbox_id").html(data);
						
					}else{
						$("#filterbox_id").html('<option value=""></option>');
					}
				}
			});
	//}
	
}

function drawtable(obj){
	arr = obj.value.split('-');
	status=arr[1];
	$('#active').val(status);
	
}
function form_validation()
{
	var parent_category = $('select[name="pcat_id"]').val().trim();
	//var categories = $('select[name="cat_id"]').val().trim();
	var filterbox_id = $('select[name="filterbox_id"]').val().trim();

	    var err = 0;
		if((parent_category=='') || (filterbox_id=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
			
		if(err==0){

			var form_status = $('<div class="form_status"></div>');		
			var form = document.getElementById('search_for_rating');		
			
			form.action='<?php echo base_url()."admin/Catalogue/archived_filter"; ?>';
			form.submit();
		}
	
	return false;
}
</script>
