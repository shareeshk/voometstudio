<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/bootstrap-colorpicker.min.css" rel="stylesheet">
	
<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.input-group .input-group-addon {
	background-color:#ccc;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	

<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo base_url();?>assets/js/color_classifier.js"></script>

</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<?php 
	//print_r($attributes);
?>
<div class="container-fluid">

<div class="row" id="editDiv3">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
	
<form name="attrib_expansion_form1" id="attrib_expansion_form1" method="post">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				attribute_name: {
					required: true,
				},
				attribute_value: {
					required: true,
				},
				attribute_expansion_name: {
					required: true,
				},
				attribute_expansion_value: {
					required: true,
				},
				attribute_sub_options:{
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Create Attribute Expansion</h3>
		</div>	
		</div>			
		<input type="hidden" name="attribute_id"  value="<?php echo $attribute_id;?>">
		<div class="tab-content">
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Attribute Name</label>
					<select name="attribute_name"  class="form-control" onchange="get_attribute_valueFun(<?php echo $attribute_id?>,this.value)">
						<option value="">-Select-</option>
						<option value="attribute1_name">Attribute1 Name</option>
						<option value="attribute2_name">Attribute2 Name</option>
						<option value="attribute3_name">Attribute3 Name</option>
						<option value="attribute4_name">Attribute4 Name</option>
					</select>
				</div>
				</div>
				<div class="col-md-5">                               
				<div class="form-group">
					<label class="control-label">Attribute Value</label>
					<input type="text" name="attribute_value" id="attribute_value" value="" class="form-control" readonly>
				</div>
				</div>
			</div>	
			
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Attribute Expansion Name</label>
					<input type="text" name="attribute_expansion_name" value="" class="form-control">
				</div>
				</div>
				<div class="col-md-5">                               
				<div class="form-group">
					<label class="control-label">Attribute Expansion Value</label>
					<input type="text" name="attribute_expansion_value" value="" class="form-control">
				</div>
				</div>
			</div>	
			
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Attribute Sub Options Seperated by Comma(,)</label>
						<textarea id="attribute_sub_options" name="attribute_sub_options" type="text" class="form-control" ></textarea>
				</div>
				</div>
			</div>	
			
			
			<div class="row">
				<div class="col-md-10 col-md-offset-5"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="button" id="submit-data" onclick="form_validation_attrib()">Next</button>
					</div>
				</div>
			</div>	
				
		</div>		
				
	</form>
	<form name="attrib_expansion_form2_proceed" id="attrib_expansion_form2_proceed" method="post"  action="<?php echo base_url()?>admin/Catalogue/attrib_expansion_form2">
	<input type="hidden" name="attribute_id" value="<?php echo $attribute_id?>">
	</form>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
</div>

<script>

function showAvailableCategoriesEdit(obj)
{
	//if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#edit_categories").html(data);
						
						
					}
					else{
						$("#edit_categories").html('<option value=""></option>')
						$("#brands").html('<option value=""></option>')

					}
				}
			});
	//}
}
function showAvailableSubCategoriesEdit(obj)
{
	cat_id=obj.value;
	//if(obj.value!="" && obj.value!="None"){
		
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#edit_subcategories").html(data);
							
					}
					else{
						$("#edit_subcategories").html('<option value=""></option>');

					}
				}
			});
	//}
}



$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
	
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
	
}

$("form[name='edit_attribute'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
	if($(this).attr('name')!="edit_color_name_sample[]"){
		orig[$(this).attr('id')] = tmp;
	}
});

$('form[name="edit_attribute"]').bind('change keyup click', function () {

    var disable = true;
    $("form[name='edit_attribute'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
		var name = $(this).attr('name');

		if($(this).attr('name')!="edit_color_name_sample[]"){
			if (type == 'text' || type == 'select' || type == 'textarea') {
				disable = (orig[id].value == $(this).val());

			} else if (type == 'radio') {
				disable = (orig[id].checked == $(this).is(':checked'));
			}

			if (!disable) {
				return false; // break out of loop
			}
		}
    });

    button.prop('disabled', disable);
});


});

function form_validation_attrib()
{
	var err=0;

	var attribute_name = $('select[name="attribute_name"]').val().trim();
	var attribute_value = $('input[name="attribute_value"]').val().trim();
	var attribute_expansion_name = $('input[name="attribute_expansion_name"]').val().trim();
	var attribute_expansion_value = $('input[name="attribute_expansion_value"]').val().trim();
	var attribute_sub_options = $('textarea[name="attribute_sub_options"]').val().trim();
 
		if((attribute_name=='') || (attribute_value=='') || (attribute_expansion_name=='') || (attribute_expansion_value=='') || (attribute_sub_options=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}

		if(err == 0){		
			
    //swal.showLoading();
					 $.ajax({
							url: '<?php echo base_url()."admin/Catalogue/attrib_expansion_form1_action"?>',
							type: 'POST',
							data: $('#attrib_expansion_form1').serialize(),
						}).done(function(data){
									
								if(data==true){
									document.getElementById("attrib_expansion_form2_proceed").submit()
								}else{
									swal(
										'Oops...',
										'Error in form',
										'error'
									)
								}
							}); 

		}else{
			alert('Please fill all fieids')	;
			return false;   
		}		  
			
}
function get_attribute_valueFun(attribute_id,attribute_name){
	if(attribute_name!=''){
		$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/get_attribute_value"?>',
				type: 'POST',
				data: "attribute_id="+attribute_id+"&attribute_name="+attribute_name,
				success:function(data){
					if(attribute_name!=""){
						$("#attribute_value").val(data);
					}else{
						$("#attribute_value").val("");	
					}
				}
		});
	}else{
		$("#attribute_value").val("");
	}
}
</script>
</div>
</body>

</html>