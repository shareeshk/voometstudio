<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<style type="text/css">

.stepwizard-step p {
    margin-top: 10px;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 50%;
    position: relative;
	margin-bottom: 3%;
}
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {

    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 2px;
}
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	

<script type="text/javascript">
var table;

$(document).ready(function (){

	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    var rows_selected = [];
    table = $('#table_inventory').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Catalogue/subreasons_processing",
			data: function (d) { d.reason_id = $('#reason_id').val();},
            type: "post",
            error: function(){
              $("#table_inventory_processing").css("display","none");
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
   
	$('#reset_form_button').on('click',function(){
		table.draw();
	});
	
	$("#submit_button").click(function() {
		table.draw();
	});
	
	$('#search_toggle').on('click',function(){	
		$('#search_block').toggle();
	});

});	

function drawtable(obj){
	table.draw();
}

function show_sort_order(obj){
	var reason_id=obj.value;
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/show_subreason_sort_order",
		type:"post",
		data:"reason_id="+reason_id,
		
		success:function(data){
			$("#menu_sort_order").html(data);
		}
	});
}

</script>
<script>
function show_view_validatn_create(){
	menu_sort_order=document.getElementById("menu_sort_order").value;
	if(menu_sort_order==0){
	document.getElementById("view1").disabled = true;
	document.getElementById("view2").checked = true;					
	}
	if(menu_sort_order!=0){
		document.getElementById("view1").disabled = false;
		document.getElementById("view1").checked = true;					
	}
}
</script>
<script>

$(document).ready(function(){
	<?php
		if($create_editview=="create"){
			?>
			$("#viewDiv1").css({"display":"none"});
			$("#createDiv2").css({"display":""});
			//show_sort_order();
			<?php
		}
		else if($create_editview=="view"){
			?>
			$("#createDiv2").css({"display":"none"});
			$("#viewDiv1").css({"display":""});
			<?php
		}
	?>
});
</script>
<script>

function changeViewPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/subreasons_filter';
}
</script>
<?php 

	if(isset($_REQUEST["reason_id"])){
		$reason_id=$_REQUEST["reason_id"];	
	}else{
		header("Location:".base_url()."admin/Catalogue/subreason/");	
	}

?>

</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">

<div class="row" id="viewDiv1" style="display:none;">
	<div class="wizard-header text-center">
		<h5> Customer Return Subreasons 
		</h5>                 		          	
	</div>
		<input value="<?php echo $reason_id; ?>" type="hidden" id="reason_id">
		
<table id="table_inventory" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<th>Id</th>
		<th>Subreasons name</th>
		<th>Sort order</th>
		<th>View</th>
		<th>Last Updated</th>
		<th>Action</th>		
	</tr>
</thead>

</table>
</div>

<div class="row" id="createDiv2" style="display:none;">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="create_subreason" id="create_subreason" method="post" action="#" onsubmit="return form_validation();">
	<input type="hidden" name="sort_order_flag" id="sort_order_flag">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				reason_id: {
					required: true,
				},
				sub_issue_name: {
					required: true,
				},
				
				menu_sort_order: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Create Subreasons</h3>
		</div>	
		</div>
		<div class="tab-content">				
					
		<!--<input name="reason_id" type="hidden" value="<?php //echo $reason_id; ?>" id="reason_id">-->

			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Select Reason Return-</label>
					<select name="reason_id" id="reason_id" class="form-control" onchange="show_sort_order(this)">
						<option value=""></option>
						<?php foreach ($reason_return as $reason_return_value) {  ?>
						<option value="<?php echo $reason_return_value->reason_id; ?>"><?php echo $reason_return_value->reason_name; ?></option>
						<?php } ?>
						<option value="0">--None--</option>
							
					</select>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">Enter Subreasons Name</label>
					<input id="sub_issue_name" name="sub_issue_name" type="text" class="form-control" />
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">Enter the Sort Order</label>
					
					<select name="menu_sort_order" id="menu_sort_order" class="form-control" onchange="show_view_validatn_create()">
						
					</select>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group">
					<div class="col-md-6">Show Subreasons</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="view" id="view1" value="1" type="radio" checked="checked">View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input name="view" id="view2" value="0" type="radio">Hide</label>
					</div>
				
					</div>
				</div>
			</div>			
			<div class="row">
				<div class="col-md-10 col-md-offset-2"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" id="submit_subreason" type="submit">Submit</button>
					<button class="btn btn-warning btn-sm" type="reset">Reset</button>
					<button class="btn btn-primary btn-sm" type="button" onclick="changeViewPrevious()">Change View</button>
					</div>
				</div>
			</div>	
		</div>		
	</form>

</div>
</div>
</div>
</div>
</div>
<div class="footer">
</div>


<script type="text/javascript">

function showAvailableSubCategories(obj){
	
	if(obj.value!="" && obj.value!="None"){
		cat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id,
				success:function(data){
					if(data!=0){
						$("#subcategories").html(data);
					}
					else{
						$("#subcategories").html('<option value="">-None-</option>')
						$("#reasons").html('<option value="">-None-</option>')

					}
				}
			});
	}
	
}
function showAvailableReasons(obj){
	if(obj.value!="" && obj.value!="None"){
		subcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_reasons",
				type:"post",
				data:"subcat_id="+subcat_id,
				success:function(data){
					if(data!=0){
						$("#reason_id").html(data);
					}
					else{
						$("#reason_id").html('<option value="">-None-</option>');
					}
				}
			});
	}
	
}
function showAvailableCategories(obj){
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id,
				success:function(data){
					if(data!=0){
						$("#categories").html(data);
					}
					else{
						$("#categories").html('<option value="">-None-</option>')
						$("#reasons").html('<option value="">-None-</option>')

					}
				}
			});
	}
	
}
function showAvailableCategoriesEdit(obj)
{
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id,
				success:function(data){
					if(data!=0){
						$("#edit_categories").html(data);
					}
					else{
						$("#edit_categories").html('<option value="">-None-</option>')
						$("#reasons").html('<option value="">-None-</option>')

					}
				}
			});
	}
}

function editsubReasonFun(sub_issue_id,reason_id){
	location.href="<?php echo base_url()?>admin/Catalogue/edit_subreasons_form/"+sub_issue_id+"/"+reason_id;
}
function deletesubReasonFun(sub_issue_id){
swal({
			title: 'Are you sure?',
			text: "Delete Subreason",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {	


		$.ajax({
				url: "<?php echo base_url(); ?>admin/Catalogue/delete_subreason/"+sub_issue_id,
				type: 'POST',
				data: "1=2",
				dataType: 'html',
			}).done(function(data){
				if(data==true){
				swal({
						title:"Deleted!", 
						text:"SubReason has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
				}else{
					swal("Error", "Error in deleting this file", "error");
				}	
			});
			})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});

}
$('#result').hide();

function form_validation()
{
	var sub_issue_name = $('input[name="sub_issue_name"]').val().trim();
	var reason_id = $('select[name="reason_id"]').val().trim();
	var menu_sort_order = $('select[name="menu_sort_order"]').val().trim();
	var view = $('input[name="view"]').val();

	   var err = 0;
	   if((sub_issue_name=='') || (reason_id=='') || (menu_sort_order=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#create_subreason');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_subreason/"?>',
				type: 'POST',
				data: $('#create_subreason').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {
				form_status.html('')
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					document.getElementById("sub_issue_name").value="";
					document.getElementById("sub_issue_name").focus();
				});
			}
				if(data=="yes")
				{
					  
					swal({
						title:"Success", 
						text:"SubReason is successfully added!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				if(data=="no")
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)
				}
				
			});
}
}]);			
			}
	
	return false;
}

</script>
</div>
</body>

</html>