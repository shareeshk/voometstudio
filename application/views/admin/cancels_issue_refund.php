<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Cancelled Orders - Issue Refund</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<style>
.form-group{
	margin-bottom:10px;
	margin-top:5px;
	padding-bottom:0;
}
</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="page-header"><h4 class="text-center">Issue refund</h4></div>
<div class="row">
	<div class="col-md-12">
		 <ul class="nav nav-pills nav-pills-info page-header">
			<li class="active"><a href="#fullrefund-tab" data-toggle="tab">Full Refund</a></li>
			<li><a href="#partialrefund-tab" data-toggle="tab">Partial Refund</a></li>
		</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="fullrefund-tab">
					<form id="full_refund_form" class="form-horizontal">
						<input type="hidden" name="cancel_id" value="<?php echo $cancels_table_data->cancel_id;?>">
						<input type="hidden" name="order_item_id" value="<?php echo $cancelled_orders_table_data->order_item_id;?>">
						<input type="hidden" name="customer_id" value="<?php echo $cancelled_orders_table_data->customer_id;?>">
						
						<!---promotion----------------->
						
						<?php
						$promotion_available=$cancelled_orders_table_data->promotion_available;
						
						if($promotion_available==1){
							$product_price=abs(floatval($cancelled_orders_table_data->total_price_of_product_with_promotion)+floatval($cancelled_orders_table_data->total_price_of_product_without_promotion));
							
						
							if($cancelled_orders_table_data->promotion_invoice_discount>0){
								
								$order_item_invoice_discount_value=$cancelled_orders_table_data->order_item_invoice_discount_value;
								
								$inv_discount=$order_item_invoice_discount_value;
								$product_price-=floatval($inv_discount);
							}
							
							if($cancelled_orders_table_data->promotion_invoice_free_shipping>0){
								$shipping_charge=0;
							}else{
								$shipping_charge=round($cancelled_orders_table_data->shipping_charge);
							}
							
							
						}else{
							
							//print_r($cancelled_orders_table_data);
							
							$product_price=$cancelled_orders_table_data->quantity*$cancelled_orders_table_data->product_price;
							if($cancelled_orders_table_data->promotion_invoice_discount>0){
								
								$order_item_invoice_discount_value=$cancelled_orders_table_data->order_item_invoice_discount_value;
								
								$inv_discount=$order_item_invoice_discount_value;
								$product_price-=floatval($inv_discount);
							}
							if($cancelled_orders_table_data->promotion_invoice_free_shipping>0){
								$shipping_charge=0;
							}else{
								$shipping_charge=round($cancelled_orders_table_data->shipping_charge);
							}
						}
						?>
						
						<?php 
						$quantity=$cancelled_orders_table_data->quantity;
						if($cancelled_orders_table_data->ord_addon_products_status=='1'){
							$shipping_charge=round($cancelled_orders_table_data->shipping_charge);
							if($cancelled_orders_table_data->ord_addon_total_price>0){
								$product_price=($cancelled_orders_table_data->ord_addon_total_price-$shipping_charge);
							}

							$quantity=count(json_decode($cancelled_orders_table_data->ord_addon_products));

							
						}
						
						?>

						<!---promotion----------------->


						<input type="hidden" name="quantity_refunded" value="<?php echo $quantity;?>">
						
						<input type="hidden" name="item_price_without_concession" value="<?php echo $product_price;?>">
						
						<input type="hidden" name="item_deduction_percentage" value="0">
						<input type="hidden" name="shipping_price_without_concession" value="<?php echo $shipping_charge;?>">
						
						<input type="hidden" name="shipping_deduction_percentage" value="0">
						
						<input type="hidden" name="item_with_deductions_concession" value="<?php echo $product_price?>">
						
						<input type="hidden" name="shipping_with_deductions_concession" value="<?php echo $shipping_charge;?>">
						
						<div class="form-group">
							<div class="col-md-3">Reason Refund:</div>
							<div class="col-md-9">
								
								<?php
		
									if(strlen($cancels_table_data->cancel_reason_comments)>200){
										echo '<div class="f-row margin-top more">'.str_split($cancels_table_data->cancel_reason_comments,200)[0].'<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' .substr($cancels_table_data->cancel_reason_comments,201). '</span>&nbsp;&nbsp;<a href="#" class="morelink" onclick="morelinkfun(this)">more</a></span></div>';
									}else{
										echo '<div class="f-row margin-top more">'.$cancels_table_data->cancel_reason_comments.'</div>';
									}
									
								?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3">Quantity:</div>
							<div class="col-md-9">
								<?php
									echo $quantity;
								?>
							</div>
						</div>
						

						<?php
						
						if($cancelled_orders_table_data->ord_addon_products_status=='1'){
							?>
						<div class="form-group">
							<div class="col-md-3">Combo Total Product Price </div>
							<div class="col-md-9">
								<?php
									echo curr_sym.$product_price;
									//$product_price=round($cancelled_orders_table_data->ord_addon_total_price);
								?>
							</div>
						</div>
							<?php
						}else{
							?>
						<div class="form-group">
							<div class="col-md-3">Total Product Price</div>
							<div class="col-md-9">
								<?php
									echo curr_sym.$product_price;
								?>
							</div>
						</div>
							<?php

						}

						?>

						<div class="form-group">
						<?php 
						if($cancelled_orders_table_data->promotion_invoice_free_shipping>0){
							$waived_off="(<span style='color:red;'>waived off)</span>";
						}else{
							$waived_off='';
						}
						?>
							<div class="col-md-3">Shipping Price <?php echo $waived_off;?>:</div>
							<div class="col-md-9">
								<?php
									echo curr_sym.$shipping_charge;
								?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3">Total Refund:</div>
							<div class="col-md-9">
							<?php
							
							if($promotion_available==1){
								$total_refund=($product_price)+round($shipping_charge);
							
							}else{
								$total_refund=(($product_price)+round($shipping_charge));
							}
							
							echo curr_sym.$total_refund;

							?>
							
							<input type="hidden" id="total_refund_without_concession_full_refund" name="total_refund_without_concession" value="<?php echo $total_refund;?>">
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-3">Memo to buyer:</div>
							<div class="col-md-4">
								<textarea class="form-control" name="memo_to_buyer"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3">Seller Memo:</div>
							<div class="col-md-4">
								<textarea class="form-control" name="memo_to_seller"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3">Refund Method:</div>
							<div class="col-md-4">
							<input type="hidden"  id="refund_method" name="refund_method" value="<?php echo $refund_table_data->refund_type;?>">
							<input type="hidden" name="refund_bank_id" value="<?php echo $refund_table_data->refund_bank_id;?>">
								<?php
									if($refund_table_data->refund_type=="Wallet" || $refund_table_data->refund_type=="Razorpay" ){
										echo $refund_table_data->refund_type;
									}
									if($refund_table_data->refund_type=="Bank Transfer"){
										echo $refund_table_data->refund_type;
										echo $controller->get_bank_details($refund_table_data->refund_bank_id);
									}
								?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-4 col-md-offset-3">
                                <input type="hidden" id="razorpay_refund_id" name="razorpay_refund_id" value="">
                                <input type="hidden" id="razorpay_refund_status" name="razorpay_refund_status" value="">
                                <input type="hidden" id="paymentId" name="paymentId" value="<?php echo $razorpay_paymentID; ?>">

								<?php
								
								//echo $refund_table_data->status;

								if($refund_table_data->status=='refund'){
									?>
									<span style="color:green;"><b>Refund request sent to Accounts team</b></span>

									<?php

								}else{
									?>

								<input class="btn btn-success btn-xs btn-block" type="button" name="full_refund_btn" value="Issue Full Refund" onclick="full_refund_fun()">

								<?php } ?>
								
							</div>
						</div>
					</form>
				</div>
				<!---- partial refund tab starts--------------->
				<div class="tab-pane" id="partialrefund-tab">
				<form id="partial_refund_form" class="form-horizontal">
						<input type="hidden" name="cancel_id" value="<?php echo $cancels_table_data->cancel_id;?>">
						<input type="hidden" name="order_item_id" value="<?php echo $cancelled_orders_table_data->order_item_id;?>">
						<input type="hidden" name="customer_id" value="<?php echo $cancelled_orders_table_data->customer_id;?>">
						<input type="hidden" name="quantity_refunded" value="<?php echo $quantity;?>">
						<div class="form-group">
							<div class="col-md-3">Quantity:</div>
							<div class="col-md-9">
								<?php
									echo $quantity;
								?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3">SKU:</div>
							<div class="col-md-9">
								<?php
									echo $controller->get_inventory_info_by_inventory_id($cancelled_orders_table_data->inventory_id)->sku_id;
								?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3">Order Item ID:</div>
							<div class="col-md-9">
								<?php
									echo $cancelled_orders_table_data->order_item_id;
								?>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-3">Refund Method:</div>
							<div class="col-md-4">
							<input type="hidden" id="refund_method" name="refund_method" value="<?php echo $refund_table_data->refund_type;?>">
							<input type="hidden" name="refund_bank_id" value="<?php echo $refund_table_data->refund_bank_id;?>">
								<?php
									if($refund_table_data->refund_type=="Wallet" || $refund_table_data->refund_type=="Razorpay"){
										echo $refund_table_data->refund_type;
									}
									if($refund_table_data->refund_type=="Bank Transfer"){
										echo $refund_table_data->refund_type;
										echo $controller->get_bank_details($refund_table_data->refund_bank_id);
									}
								?>
							</div>
						</div>
						
						
						<div class="form-group">
							<div class="col-md-3">Reason For Refund:</div>
							<div class="col-md-9">
								
								<?php
		
									if(strlen($cancels_table_data->cancel_reason_comments)>200){
										echo '<div class="f-row margin-top more">'.str_split($cancels_table_data->cancel_reason_comments,200)[0].'<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' .substr($cancels_table_data->cancel_reason_comments,201). '</span>&nbsp;&nbsp;<a href="#" class="morelink" onclick="morelinkfun(this)">more</a></span></div>';
									}else{
										echo '<div class="f-row margin-top more">'.$cancels_table_data->cancel_reason_comments.'</div>';
									}
									
								?>
							</div>
						</div>
						<?php
							$total_product_price_with_shipping=0;
							$total_max_amount=0;
						?>
						<table class="table table-striped table-bordered" cellspacing="0" width="100%">
							<tr>
								<th>Label details</th>
								<th>Order amount</th>
								<th>Deductions</th>
								<th>Amount to refund</th>
								<th>Max amount Suggestion</th>
							</tr>
							<tr>
								<th>
									Product:
									<?php 
									
									if($cancelled_orders_table_data->ord_addon_products_status=='1'){
										echo '<br><small> Combo Products</small>';
									}
									?>
								</th>
								<td>
									
									<?php
										//check this calculation//
										
										if($promotion_available==1){
		
											
											$tot_pro_price=(floatval($cancelled_orders_table_data->total_price_of_product_with_promotion)+floatval($cancelled_orders_table_data->total_price_of_product_without_promotion));

											if($cancelled_orders_table_data->promotion_invoice_discount>0){
												$order_item_invoice_discount_value=$cancelled_orders_table_data->order_item_invoice_discount_value;
												
												$inv_discount=$order_item_invoice_discount_value;
												$tot_pro_price-=floatval($inv_discount);
											}

											$total_product_price_with_shipping+=$tot_pro_price;
											
											//echo curr_sym.$tot_pro_price;
											
										}else{
											$tot_pro_price=$cancelled_orders_table_data->product_price*$cancelled_orders_table_data->quantity;
											if($cancelled_orders_table_data->promotion_invoice_discount>0){
												$order_item_invoice_discount_value=$cancelled_orders_table_data->order_item_invoice_discount_value;
												
												$inv_discount=$order_item_invoice_discount_value;
												$tot_pro_price-=floatval($inv_discount);
											}
											$total_product_price_with_shipping+=$tot_pro_price;
											//echo curr_sym.$tot_pro_price;
										}


										$quantity=$cancelled_orders_table_data->quantity;
										if($cancelled_orders_table_data->ord_addon_products_status=='1'){
											$shipping_charge=round($cancelled_orders_table_data->shipping_charge);
											if($cancelled_orders_table_data->ord_addon_total_price>0){
												$tot_pro_price=($cancelled_orders_table_data->ord_addon_total_price-$shipping_charge);
											}
											$quantity=count(json_decode($cancelled_orders_table_data->ord_addon_products));

										}
										

										echo curr_sym.$tot_pro_price;
									?>
	
									<input type="hidden" name="order_amount_product" id="order_amount_product" value="<?php echo $tot_pro_price;?>">
								</td>
								<td>
									<input type="text" name="deductions_product" id="deductions_product" value="0"  onkeypress="return isNumber(event);" onkeyup="cal_amount_to_refund_productFun(this.value);" class="form-control">
								</td>
								<td>
									<input type="text" id="amount_to_refund_product" name="amount_to_refund_product" value="<?php echo $tot_pro_price;?>" readonly class="form-control">
								</td>
								<td>
									<?php
									
									if($promotion_available==1){
											
										$total_pro_price=(floatval($cancelled_orders_table_data->total_price_of_product_with_promotion)+floatval($cancelled_orders_table_data->total_price_of_product_without_promotion));
										
										$total_max_amount+=$total_pro_price;
										
										//echo curr_sym.$total_pro_price;
											
									}else{
										$total_pro_price=$cancelled_orders_table_data->product_price*$cancelled_orders_table_data->quantity;
										
										$total_max_amount+=$total_pro_price;
										
										//echo curr_sym.$total_pro_price;
									}

									if($cancelled_orders_table_data->ord_addon_products_status=='1'){

										if($cancelled_orders_table_data->ord_addon_total_price>0){
											$total_pro_price=($cancelled_orders_table_data->ord_addon_total_price-$shipping_charge);
											$total_max_amount=round($cancelled_orders_table_data->ord_addon_total_price);
											$total_product_price_with_shipping=round($cancelled_orders_table_data->ord_addon_total_price);
										}
			
									}else{
										$total_max_amount+=$shipping_charge;
										$total_product_price_with_shipping+=$shipping_charge;
										
									}
										echo curr_sym.$total_pro_price;
									?>

									
								</td>
							</tr>
							<tr>
								<th>
								<?php 
						if($cancelled_orders_table_data->promotion_invoice_free_shipping>0){
							$waived_off="(<span style='color:red;'>waived off)</span>";
						}else{
							$waived_off='';
						}
						?>
									Shipping charge <?php echo $waived_off; ?>:
								</th>
								<td>
									<?php
										
										echo curr_sym.$shipping_charge;
									?>
									<input type="hidden" name="order_amount_shipping" id="order_amount_shipping" value="<?php echo $shipping_charge;?>">
								</td>
								<td>
									<input type="text" name="deductions_shipping" id="deductions_shipping" value="0" onkeypress="return isNumber(event);"   onkeyup="cal_amount_to_refund_shippingFun(this.value);" class="form-control">
								</td>
								<td>
									<input type="text" id="amount_to_refund_shipping" name="amount_to_refund_shipping" value="<?php echo $shipping_charge?>" readonly class="form-control">
								</td>
								<td>
									<?php
										
										echo curr_sym.$shipping_charge;
									?>
								</td>
							</tr>
							<tr>
								<th>
									Total:
								</th>
								<td>
									<?php
										echo curr_sym.$total_product_price_with_shipping;
									?>
									<input type="hidden" name="total_product_price_with_shipping" id="total_product_price_with_shipping" value="<?php echo $total_product_price_with_shipping;?>">
								</td>
								<td>
									----
								</td>
								<td>
									<input type="hidden" name="total_amount_to_refund" id="total_amount_to_refund" value="0">
									<div id="total_amount_to_refund_div"></div>
								</td>
								<td>
									<?php
										echo curr_sym.$total_max_amount;
									?>
								</td>
							</tr>
						</table>
						
						<div class="form-group">
							<div class="col-md-3">Memo to buyer:</div>
							<div class="col-md-4">
								<textarea class="form-control" name="memo_to_buyer_partial_refund"></textarea>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-3">Seller memo:</div>
							<div class="col-md-4">
								<textarea class="form-control" name="memo_to_seller_partial_refund"></textarea>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-4 col-md-offset-3">

                                <input type="hidden" id="razorpay_refund_id" name="razorpay_refund_id" value="">
                                <input type="hidden" id="razorpay_refund_status" name="razorpay_refund_status" value="">
                                <input type="hidden" id="paymentId" name="paymentId" value="<?php echo $razorpay_paymentID; ?>">

								<?php
								
								//echo $refund_table_data->status;

								if($refund_table_data->status=='refund'){
									?>

									<span style="color:green;"><b>Refund request sent to Accounts team</b></span>

									<?php

								}else{
									?>
<input class="btn btn-success btn-xs btn-block" type="button" name="partial_refund_btn" value="Issue Partial Refund" onclick="partial_refund_fun()">
									<?php
								}
								?>
                                
							</div>
						</div>
					</form>
				</div>
				<!---- partial refund tab starts--------------->
			</div>
	</div>
</div>
</div>
<!------------------------------------->	
<!---------------------->
<script>

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
        return false;
    }
    return true;
}
function full_refund_fun(){


    pay_type=$('#refund_method').val();

    //if(pay_type=='Razorpay') {
    if(0) {

        return false;
        /* razorpay refund */

        $.ajax({
            url: "<?php echo base_url();?>razorpay/razorpay_refund",
            type: "POST",
            data: $("#full_refund_form").serialize(),
            success: function (data) {

                data=JSON.parse(data);

                //alert(data.status);
                //alert(data.response);
                console.log(data);

                dat=data;
                if(parseInt(dat.status)==1) {
                    $('#razorpay_refund_id').val(dat.razorpay_refund_id);
                    $('#razorpay_refund_status').val(dat.razorpay_refund_status);

                    $.ajax({
                        url: "<?php echo base_url();?>admin/Orders/cancels_issue_full_refund",
                        type: "POST",
                        data: $("#full_refund_form").serialize(),
                        success: function (data) {
                            if (data) {
                                alert("Refund has been initiated successfully.");
                                location.reload();
                            } else {
                                alert("Error");
                            }
                        }
                    });

                }else{
                    $('#razorpay_refund_status_'+order_item_id).val(dat.response);
                }
            }
        });

        /* razorpay refund */

    }else {

        $.ajax({
            url: "<?php echo base_url();?>admin/Orders/cancels_issue_full_refund",
            type: "POST",
            data: $("#full_refund_form").serialize(),
            success: function (data) {
                if (data) {
                    alert("Refund successfully done");
                    location.reload();
                } else {
                    alert("Error");
                }
            }
        });

    }
}

function cal_amount_to_refund_productFun(v){
	if(v==""){
		v=0;
	}
	order_amount_product=parseFloat(document.getElementById("order_amount_product").value);
	
	document.getElementById("amount_to_refund_product").value=Math.round(order_amount_product-(order_amount_product*(v/100)));
	cal_total_amount_to_refundFun();
	
}
function cal_amount_to_refund_shippingFun(v){
	if(v==""){
		v=0;
	}
	order_amount_shipping=parseFloat(document.getElementById("order_amount_shipping").value);
	document.getElementById("amount_to_refund_shipping").value=Math.round(order_amount_shipping-order_amount_shipping*(v/100));
	cal_total_amount_to_refundFun();
}
function cal_total_amount_to_refundFun(){
	
	
	
	var amount_to_refund_product=document.getElementById("amount_to_refund_product").value;
	var amount_to_refund_shipping=document.getElementById("amount_to_refund_shipping").value;
	document.getElementById("total_amount_to_refund_div").innerHTML="<?php echo curr_sym; ?>"+(parseFloat(amount_to_refund_product)+parseFloat(amount_to_refund_shipping));
	
	document.getElementById("total_amount_to_refund").value=(parseFloat(amount_to_refund_product)+parseFloat(amount_to_refund_shipping));
}

function partial_refund_fun(){
	var order_amount_product=document.getElementById("order_amount_product").value;
	var deductions_product=document.getElementById("deductions_product").value;
	var order_amount_shipping=document.getElementById("order_amount_shipping").value;
	var deductions_shipping=document.getElementById("deductions_shipping").value;
	var total_product_price_with_shipping=document.getElementById("total_product_price_with_shipping").value;
	
	var amount_to_refund_product=document.getElementById("amount_to_refund_product").value;
	var amount_to_refund_shipping=document.getElementById("amount_to_refund_shipping").value;
	
	var total_amount_to_refund=document.getElementById("total_amount_to_refund").value;
	
	
	$.ajax({
		url:"<?php echo base_url();?>admin/Orders/cancels_issue_partial_refund",
		type:"POST",
		data:$("#partial_refund_form").serialize(),
		success:function(data){
			if(data){
				alert("Refund successfully done");
				location.reload();
			}
			else{
				alert("Error");
			}
		}
	})
	
	
}
</script>
</div>
</body> 
</html>  