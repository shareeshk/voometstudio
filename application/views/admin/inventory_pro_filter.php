<html>
<head>
<meta charset="utf-8">
<title>Reviews and Ratings</title>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<style>
.wizard-card{
	box-shadow:none;
}
.form-group{
	margin-top:15px;
}

</style>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">
<div class="row">
<div class="col-md-12">
<div class="wizard-card" data-color="green" id="wizardProfile">	
<form name="search_for_rating" id="search_for_rating" method="post" action="#" onsubmit="return form_validation();">
<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				categories: {
					required: true,
					
				},
				subcategories: {
					required: true,
				},
			
				brands: {
					required: true,
				},
				products: {
					required: true,
				},
				inventory_type: {
					required: true,
				},
				create_editview: {
					required: true,
		      
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
			</script>

	<div class="col-md-6">
		
		
		<div class="row">
			<div class="col-md-10 text-center">		
			<h4>Select Product To view Inventory</h4>
		</div>
		</div>
		
			<div class="row">
				<div class="col-md-10">
					<div class="form-group label-floating">
					<label class="control-label">-Select Parent Category-</label>
					<select name="parent_category" id="parent_category" class="form-control" onchange="showAvailableCategories(this)">
						<option value=""></option>
						<?php foreach ($parent_category as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>" <?php echo ($parent_category_value->pcat_id==$pcat_id)? "selected":''; ?> ><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } ?>
						<option value="0">--None--</option>
						
					</select>
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10">
					<div class="form-group label-floating">
					<label class="control-label">-Select Category-</label>
					<select name="categories" id="categories" class="form-control" onchange="showAvailableSubCategories(this)">
						<option value=""></option>
					</select>
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10">		
					<div class="form-group label-floating">
					<label class="control-label">-Select Sub Category-</label>
					<select name="subcategories" id="subcategories" class="form-control" onchange="showAvailableBrands(this)">
						<option value=""></option>
					</select>
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10">		
					<div class="form-group label-floating">
					<label class="control-label">-Select Brands-</label>
					<select name="brands" id="brands" class="form-control" onchange="showAvailableProducts(this)">
						<option value=""></option>
					</select>
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10">	
					<div class="form-group label-floating">
					<label class="control-label">-Select Products-</label>
                                        <?php 
                                        
                                        $admin_user_type=$this->session->userdata("user_type");
                                        $admin_module_type=$this->session->userdata("module_type");
                                        if($admin_user_type="Su Master Country" && $admin_module_type=='catalog' ){
                                            /*only edit*/
                                            $str='<option value=""></option><option value="view">-Go to View-</option>';
                                        }else{
                                            $str='<option value=""></option><option value="create">-Go to Create-</option><option value="view">-Go to View-</option>';
                                        }
                                        ?>
                                        
					<script>
                                            
                                            str='<?php echo $str; ?>';
                                            
                                            function viewFun(obj){
                                                    if(obj.value!=""){
                                                            $("#create_editview").html(str);
                                                    }
                                                    else{
                                                            $("#create_editview").html('<option value="">-Select an View-</option>');
                                                    }
                                            }
					</script>
					<select name="products" id="products" class="form-control" onchange="viewFun(this)">
						<option value=""></option>
					</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-10">	
					<div class="form-group label-floating">
					<label class="control-label">-Select Type of Inventory-</label>
					<select name="inventory_type" id="inventory_type" class="form-control" required>
						<option value=""></option>
						<option value="1">Only Main</option>
						<option value="2">Only Addon</option>
						<option value="3">Both Main and Addon</option>
						
					</select>
					</div>
				</div>
			</div>	

			<div class="row">
				<div class="col-md-10">	
					<div class="form-group label-floating">
					<label class="control-label">-Select an View-</label>
					<select name="create_editview" id="create_editview" class="form-control" onchange="showFilterByAttributes(this)">
						<option value=""></option>
						
					</select>
					</div>
				</div>
			</div>	
				<div class="row">	
					<div class="col-md-10 text-center">
						<button type="submit" class="btn btn-info" id="submit_button">Submit</button>
						<button class="btn btn-warning" type="reset" id="reset_form_button">Reset</button>
					</div>
				</div>
		
	
	</div>
	
	<div class="col-md-6" id="search_block" style="display:none;">
	
	</div>
	

</form>
</div>
</div>

</div>
</div>
</div>
</body>
</html>
<script type="text/javascript">
$(document).ready(function (){
	pcat_id=$('#pcat_id').val();
	cat_id=$('#cat_id').val();
	subcat_id=$('#subcat_id').val();
	brand_id=$('#brand_id').val();
	product_id=$('#product_id').val();
	//inventory_id=$('#inventory_id').val();

	
});

/*function showAvailableInventory(obj){
	
	if(obj.value!="" && obj.value!="None"){
		product_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Ratemanaging/show_available_inventory",
				type:"post",
				data:"product_id="+product_id,
				success:function(data){
					if(data!=0){
						$("#inventory").html(data);
					}
					else{
						$("#inventory").html('<option value="">-None-</option>');
					}
				}
			});
	}
	
}*/
function showAvailableProducts(obj){
	
	if(obj.value!="" && obj.value!="None"){
		brand_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_products",
				type:"post",
				data:"brand_id="+brand_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#products").html(data);
					}
					else{
						$("#products").html('<option value=""></option>');
					}
				}
			});
	}
	
}

function showAvailableBrands(obj){
	
	if(obj.value!="" && obj.value!="None"){
		subcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_brands",
				type:"post",
				data:"subcat_id="+subcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#brands").html(data);
					}
					else{
						$("#brands").html('<option value=""></option>');
					}
				}
			});
	}
	
}
function showAvailableSubCategories(obj){
	
	if(obj.value!="" && obj.value!="None"){
		cat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#subcategories").html(data);
					}
					else{
						$("#subcategories").html('<option value=""></option>')
						$("#brands").html('<option value=""></option>')

					}
				}
			});
	}
	
}

function showAvailableCategories(obj){
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#categories").html(data);
					}
					else{
						$("#categories").html('<option value=""></option>')
						$("#brands").html('<option value=""></option>')

					}
				}
			});
	}
	
}
function showFilterByAttributes(obj){
	if(obj.value=="view"){
			subcat_id=$("#subcategories").val();
			
			$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/get_filters_by_subcat",
				type:"post",
				data:"subcat_id="+subcat_id,
				success:function(data){
					$("#search_block").html(data);
				}
			});
		document.getElementById('search_block').style.display = "block";
	}else{
		document.getElementById('search_block').style.display = "none";
	}
}
function form_validation()
{
	var parent_category = $('select[name="parent_category"]').val().trim();
	var categories = $('select[name="categories"]').val().trim();
	var subcategories = $('select[name="subcategories"]').val().trim();
	var brands = $('select[name="brands"]').val().trim();
	var products = $('select[name="products"]').val().trim();
	var create_editview = $('select[name="create_editview"]').val().trim();
	

	    var err = 0;
		if(categories=='')
		{
		   //$('select[name="categories"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('select[name="categories"]').css({"border": "1px solid #ccc"});
		}
		
		if(subcategories=='')
		{
		   //$('select[name="subcategories"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{
			//$('select[name="subcategories"]').css({"border": "1px solid #ccc"});
		}
		if(brands=='')
		{
		   //$('select[name="brands"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('select[name="brands"]').css({"border": "1px solid #ccc"});
		}
	
		if(products=='')
		{
		   //$('select[name="products"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('select[name="products"]').css({"border": "1px solid #ccc"});
		}
		if(create_editview=='')
		{
		   //$('select[name="create_editview"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('select[name="create_editview"]').css({"border": "1px solid #ccc"});
		}
		
	
		if(err==1){
			
		    $('#error_result_id').show();
		    $('#result').hide();
			
		}else{

			var form_status = $('<div class="form_status"></div>');		
			var form = document.getElementById('search_for_rating');		
			
			form.action='<?php echo base_url()."admin/Catalogue/inventory"; ?>';
			form.submit();
		}
	
	return false;
}
</script>
