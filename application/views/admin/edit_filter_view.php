<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/bootstrap-colorpicker.min.css" rel="stylesheet">

<style>

@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.input-group .input-group-addon {
	background-color:#ccc;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	

<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo base_url();?>assets/js/color_classifier.js"></script>

<script type="text/javascript">

function showDivPrevious(){
	
	var form = document.getElementById('search_for_filter');		
	
	form.action='<?php echo base_url()."admin/Catalogue/filter"; ?>';
	form.submit();
			
	 //window.location.href = '<?php echo base_url(); ?>admin/Catalogue/category';
}
</script>
<?php

if($get_filter_data['filter_sort_order']=="0"){?>
	<script>
	$(document).ready(function () {
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	document.getElementById('show_available_common_filter').disabled = true;
	});
	</script>
	<?php
}
?>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row-fluid" id="editDiv3" >
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
			
	<form name="edit_filter" id="edit_filter" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_parent_category: {
					required: true,
				},
				
				edit_sort_order: {
					required: true,
				},
				edit_filterbox: {
					required: true,
				},
				edit_filter_options: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>				
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Filter</h3>
		</div>	
		</div>	
		<div class="tab-content">
			<input type="hidden" name="edit_filter_id" id="edit_filter_id" value="<?php echo $get_filter_data["filter_id"];?>">
				<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Parent category-</label>
					<select name="edit_parent_category" id="edit_parent_category" class="form-control" onchange="showAvailableCategoriesEdit(this)" disabled>
						<option value=""></option>
						<?php foreach ($parent_catagories as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>" <?php echo ($parent_category_value->pcat_id==$pcat_id)? "selected":''; ?>><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } if(empty($parent_catagories)){
							?>
							<option value="0" selected>--None--</option>
							<?php 
						}
						 if($pcat_id==0){
							?>
							<option value="0" selected>--None--</option>
							<?php 
						}else{?>
						<option value="0">--None--</option>
						<?php }
						?>
					</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">-Select Category-</label>
					<select name="edit_categories" id="edit_categories" class="form-control" onchange="showAvailableSubCategoriesEdit(this)" disabled>
						<option value=""></option>
						<?php foreach ($categories as $category_value) {  ?>
						<option value="<?php echo $category_value->cat_id; ?>" <?php echo ($category_value->cat_id==$cat_id)? "selected":''; ?>><?php echo $category_value->cat_name; ?></option>
						<?php } ?>
		
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Sub Category-</label>
					<select name="edit_subcategories" id="edit_subcategories" class="form-control" onchange="showAvailableFilterbox(this)" disabled>
						<option value=""></option>
						<?php foreach ($subcategories as $subcategory_value) {  ?>
						<option value="<?php echo $subcategory_value->subcat_id; ?>" <?php echo ($subcategory_value->subcat_id==$subcat_id)? "selected":''; ?>><?php echo $subcategory_value->subcat_name; ?></option>
						<?php } ?>
		
					</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">-Select Filterbox-</label>
					<select name="edit_filterbox" id="edit_filterbox" class="form-control align_top" disabled>
						<option value=""></option>
						<?php foreach ($filterbox as $filterbox_value) {  ?>
						<option filterboxname="<?php echo $filterbox_value->filterbox_name; ?>" value="<?php echo $filterbox_value->filterbox_id; ?>" <?php echo ($filterbox_value->filterbox_id==$filterbox_id)? "selected":''; ?>><?php echo $filterbox_value->filterbox_name; ?></option>
						<?php } ?>
							
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Enter Filter Name</label>
					<div class="edit_add_color_picker"></div>
						
					<input id="edit_filter_options" name="edit_filter_options" type="text" class="form-control" value="<?php echo $get_filter_data["filter_options"];?>"/>
					<input id="edit_filter_options_default" name="edit_filter_options_default" type="hidden" class="form-control" value="<?php echo $get_filter_data["filter_options"];?>"/>
				</div>
				</div>
				
				
			</div>	
			<div class="row">
				
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Enter Sort Order Value</label>
					
					<select name="edit_sort_order" id="edit_sort_order" class="form-control" onchange="show_view_validatn()">
					<option value="<?php echo $get_filter_data["filter_sort_order"];?>" selected><?php echo $get_filter_data["filter_sort_order"];?></option>
						<?php
							if($get_sort_order_options_for_filter["key"]==""){
								if($get_filter_data["filter_sort_order"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php } ?>
								<option value="1">1</option>
								<?php
							}
							else{
								if($get_filter_data["filter_sort_order"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php
								}
								$value_arr=explode(",",$get_sort_order_options_for_filter["value"]);
								foreach($value_arr as $k => $v){
									?>
										<option value="<?php echo $v;?>"><?php echo $v;?></option>
									<?php
									}
							}
						?>
					</select>
					<input id="edit_sort_order_default" name="edit_sort_order_default" type="hidden" value="<?php echo $get_filter_data["filter_sort_order"];?>">
				</div>
				</div>
				
			</div>	
					
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group label-floating">
			<label class="control-label">-Swap Options-</label>
			<select class="form-control" id="show_available_common_filter" onchange="changeEventHandler(event)">
			<option selected value=""></option>
			<?php
			
			foreach($subcategory_sort_order_arr as $subcategory_order_arr){
				
				?>
				
					<option value="<?php echo $subcategory_order_arr["filter_id"];?>"><?php echo $subcategory_order_arr["filter_options"];?></option>
					<?php
			}
			?>
			
			</select>
			</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-3"> 
					<div class="form-group">
					<div class="col-md-6">Show Filter</div>
					<div class="col-md-3">
						<label class="radio-inline"><input id="edit_view1" name="edit_view" value="1" type="radio" <?php if($get_filter_data["view_filter"]=="1"){echo "checked";}?>>View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input id="edit_view2" name="edit_view" value="0" type="radio" <?php if($get_filter_data["view_filter"]=="0"){echo "checked";}?>>Hide</label>
					</div>
				
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<select class="form-control" id="show_available_common_filter" style="visibility:hidden;">
						<option selected value="">-Select Options-</option>
					</select>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-4"> 
					<button class="btn btn-info btn-sm" type="submit" id="submit-data" disabled="">Submit</button>
		
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
				</div>
			</div>		

		</div>		
				
	</form>
	<form name="search_for_filter" id="search_for_filter" method="post">
		<input value="<?php echo $get_filter_data["pcat_id"]; ?>" type="hidden" id="pcat_id" name="pcat_id"/>
		<input value="<?php echo $get_filter_data["cat_id"]; ?>" type="hidden" id="cat_id" name="cat_id"/>
		<input value="<?php echo $get_filter_data["subcat_id"]; ?>" type="hidden" id="subcat_id" name="subcat_id"/>
		<input value="<?php echo $get_filter_data["filterbox_id"]; ?>" type="hidden" id="filterbox_id" name="filterbox_id"/>
		<input value="view" type="hidden" name="create_editview">
	</form>
</div>
</div>
</div>
</div>
</div>
<div class="footer">
</div>
<script>
function changeEventHandler(event){
        parent_id=event.target.value;
		if(parent_id!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		}
		if(parent_id==""){
	edit_sort_order=document.getElementById("edit_sort_order").value;
	if(edit_sort_order==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;					
	}
	if(edit_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
		}
}
function show_view_validatn(){
	edit_sort_order=document.getElementById("edit_sort_order").value;
	show_available_common_filter=document.getElementById("show_available_common_filter").value;
	if(show_available_common_filter==""){
	if(edit_sort_order==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;	
		
	}
	if(edit_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		
	}
	}
	if(show_available_common_filter!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
	
}
</script>
<script type="text/javascript">

function showAvailableFilterbox(obj){
	pcat_id=$("#parent_category").val();
	cat_id=$("#categories").val();
	//if(obj.value!="" && obj.value!="None"){
		subcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_filterbox",
				type:"post",
				data:"pcat_id="+pcat_id+"&cat_id="+cat_id+"&subcat_id="+subcat_id+"&active=1"+"&flagtype=3",
				success:function(data){
					if(data!=0){
						$("#filterbox").html(data);
						$("#menu_sort_order_filter_status").html("");
						$("#edit_filterbox").html(data);
					}
					else{
						$("#filterbox").html('<option value="0">-None-</option>');
						$("#menu_sort_order_filter_status").html("");
						$("#edit_filterbox").html('<option value="0">-None-</option>');

					}
					
				}
			});
	//}
	
}

function showAvailableCategoriesEdit(obj)
{
	//if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#edit_categories").html(data);
					}
					else{
						$("#edit_categories").html('<option value="0">-None-</option>')
						//$("#brands").html('<option value="">-None-</option>')

					}
				}
			});
	//}
}
function showAvailableSubCategoriesEdit(obj)
{
	//if(obj.value!="" && obj.value!="None"){
		cat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
				type:"post",
				data:"cat_id="+cat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#edit_subcategories").html(data);
					}
					else{
						$("#edit_subcategories").html('<option value="0">-None-</option>');

					}
				}
			});
	//}
}

function edit_callll(){
	
	$('#edit_cp').colorpicker({
			format: "hex",
			colorSelectors: {
                'black': '#000000',
                'white': '#ffffff',
                'red': '#FF0000',
                'default': '#777777',
                'primary': '#337ab7',
                'success': '#5cb85c',
                'info': '#5bc0de',
                'warning': '#f0ad4e',
                'danger': '#d9534f'
            }
		});
		
	url='<?php echo base_url() ?>assets/js/dataset.js';
	
	$('#edit_cp').colorpicker().on('changeColor', function (e) {
				window.classifier = new ColorClassifier();
				get_dataset(url, function (data){
					window.classifier.learn(data);			
					color_value=$('#edit_color_value').val();	
					var result_name = window.classifier.classify(color_value);
					$('#edit_color_name_sample').val(result_name);

				});
	});
	window.classifier = new ColorClassifier();
				get_dataset(url, function (data){
					window.classifier.learn(data);			
					color_value=$('#edit_color_value').val();	
					var result_name = window.classifier.classify(color_value);
					$('#edit_color_name_sample').val(result_name);

				});
}
editFilterFun();

function editFilterFun(){
	
	pcat_id='<?php echo $pcat_id; ?>';
	cat_id='<?php echo $cat_id; ?>';
	subcat_id='<?php echo $subcat_id; ?>';
	filter_id='<?php echo $filter_id; ?>';
	filterbox_id='<?php echo $filterbox_id; ?>';
	filter_options='<?php echo $get_filter_data["filter_options"];?>';
					
					filterboxname=$('#edit_filterbox').find('option:selected').attr('filterboxname');
					if(filterboxname.toLowerCase()=="color"){
						
						colors_arr=filter_options.split(":");
						
						$('.edit_add_color_picker').html('');
						$('.edit_add_color_picker').append('<div class="row"><div class="col-md-4">Pick color: <div class="input-group colorpicker-component" id="edit_cp"><input id="edit_color_value" name="edit_color_value" value="'+colors_arr[1]+'" class="form-control" type="text"><span class="input-group-addon"><i></i></span></div></div><div class="col-md-4">Type color name: <input id="edit_color_name" name="edit_color_name" value="'+colors_arr[0]+'" class="form-control" type="text"></div><div class="col-md-4">Sample color name : <input name="edit_color_name_sample" id="edit_color_name_sample" value="" class="form-control edit_sample_name" type="text"></div></div>');
					
						$('.filter_names').hide();
						edit_callll();
					}else{
						$('.edit_add_color_picker').html('');
						$('.filter_names').show();	
					}
					
					//$("#menu_edit_sort_order_status").html(sugg_key+":"+sugg_value);
					
				

}

$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_filter'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_filter"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_filter'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (orig[id].value == $(this).val());

        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});
});
function form_validation_edit()
{
	
	str='';

	filterboxname=$('#edit_filterbox').find('option:selected').attr('filterboxname');

	if(filterboxname.toLowerCase()=="color"){
		color_name=$('#edit_color_name').val();
		color_value=$('#edit_color_value').val();
						
		if(color_value!='' && color_name!=''){
			str=color_name+':'+color_value;	
		}else{
			alert('Please fill all fields');
			str='';
		}
		$('#edit_filter_options').val(str);
	}

	
	var filter_id=$("#edit_filter_id").val();
	var filter_options = $('input[name="edit_filter_options"]').val().trim();
	var filterbox = $('select[name="edit_filterbox"]').val().trim();
	var parent_category = $('select[name="edit_parent_category"]').val().trim();
	var categories = $('select[name="edit_categories"]').val().trim();
	var subcategories = $('select[name="edit_subcategories"]').val().trim();
	var edit_sort_order = $('select[name="edit_sort_order"]').val().trim();
	var view = document.querySelector('input[name="edit_view"]:checked').value;
	var sort_order_default = $('input[name="edit_sort_order_default"]').val().trim();
	
	var common_filter="";
			if($("#show_available_common_filter").length!=0){
				var common_filter=document.getElementById("show_available_common_filter").value;
			}

	   var err = 0;
		
		if(filter_options.length=="")
		{ 
		   err = 1;
		}
		else{			
			 err = 0;
		}
		
		if(err==0){
				document.getElementById("edit_parent_category").disabled="";
				document.getElementById("edit_categories").disabled="";
				document.getElementById("edit_subcategories").disabled="";
				document.getElementById("edit_filterbox").disabled="";
			var form_status = $('<div class="form_status"></div>');		
			var form = $('#edit_filter');		
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();				
					 $.ajax({
							url: '<?php echo base_url()."admin/Catalogue/edit_filter/"?>',
							type: 'POST',
							data: $('#edit_filter').serialize()+"&common_filter="+common_filter,
							dataType: 'html',
							
						}).done(function(data){
							form_status.html('') 
							if(data=="exist"){
								swal({
								title:"Error", 
								text:"That name is already created. Try another.", 
								type: "error",
								allowOutsideClick: false
							}).then(function(){
								
					edit_filter_options_default=document.getElementById("edit_filter_options_default").value;
					document.getElementById("edit_filter_options").value=edit_filter_options_default;
								document.getElementById("edit_filter_options").focus();
								document.getElementById("edit_parent_category").disabled=true;
								document.getElementById("edit_categories").disabled=true;
								document.getElementById("edit_subcategories").disabled=true;
								document.getElementById("edit_filterbox").disabled=true;
								
							});
							  }
								if(data==1){
									 
									swal({
										title:"Success", 
										text:"Filter is successfully updated!", 
										type: "success",
										allowOutsideClick: false
									}).then(function () {
										location.reload();

									});
								}if(data==0){
									swal(
										'Oops...',
										'Error in form',
										'error'
									)	
								}
								
								
			}); 
}
}]);			
			}
	
	return false;
}

</script>
</div>
</body>

</html>