<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<style>
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}
.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}
.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}
.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>

<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();   
});
</script>

<script>
$(document).ready(function(){
$('#edit_pcat_view').on('change', function() {
	if(this.checked){
		$('#edit_pcat_view').not(this).prop('checked', true); 
		$('#edit_cat_view').not(this).prop('checked', true);  
		$('#edit_subcat_view').not(this).prop('checked', true); 
	}else{
		$('#edit_cat_view').not(this).prop('checked', false);  
		$('#edit_subcat_view').not(this).prop('checked', false);
	}
});
$('#edit_cat_view').on('change', function() {
	if(this.checked){
    $('#edit_pcat_view').not(this).prop('checked', false);
	$('#edit_cat_view').not(this).prop('checked', true); 	
	$('#edit_subcat_view').not(this).prop('checked', true); 
	}else{
		$('#edit_pcat_view').not(this).prop('checked', false);
		$('#edit_subcat_view').not(this).prop('checked', false); 
	}
});
$('#edit_subcat_view').on('change', function() {
	if(this.checked){
    $('#edit_pcat_view').not(this).prop('checked', false);  
	$('#edit_cat_view').not(this).prop('checked', false);
	$('#edit_subcat_view').not(this).prop('checked', true);
}else{
	$('#edit_pcat_view').not(this).prop('checked', false);  
	$('#edit_cat_view').not(this).prop('checked', false);
}	
});

});
function showDivPrevious(){
	
	var form = document.getElementById('search_for_filterbox');		
	
	form.action='<?php echo base_url()."admin/Catalogue/filterbox"; ?>';
	form.submit();
			
	 //window.location.href = '<?php echo base_url(); ?>admin/Catalogue/category';
}
</script>
<?php
if($get_filterbox_data['sort_order']=="0"){?>
	<script>
	$(document).ready(function () {
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	document.getElementById('show_available_common_filterbox').disabled = true;
	
	});
	</script>
	<?php
}
?>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<?php 
//print_r($filterbox);
?>

<div class="container-fluid">

<div class="row-fluid" id="editDiv3">
<div class="col-md-10 col-md-offset-1">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
			
	<form name="edit_filterbox" id="edit_filterbox" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_parent_category: {
					required: true,
				},
				/*edit_categories: {
					required: true,
				},*/
				edit_sort_order: {
					required: true,
				},
				edit_filterbox_name: {
					required: true,
				},
				
				edit_filterbox_type: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Filterbox</h3>
		</div>	
		</div>
		<div class="tab-content">
		<input type="hidden" name="edit_filterbox_id" id="edit_filterbox_id"  value="<?php echo $get_filterbox_data["filterbox_id"];?>">
		<input type="hidden" id="result_type" value="">
		<input type="hidden" id="default_cat_id" value="<?php echo $get_filterbox_data["cat_id"];?>">
		<input type="hidden" id="default_subcat_id" value="<?php echo $get_filterbox_data["subcat_id"];?>">
		<input type="hidden" id="default_pcat_id" value="<?php echo $get_filterbox_data["pcat_id"];?>">
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Parent category-</label>
					<select name="edit_parent_category" id="edit_parent_category" class="form-control" onchange="showAvailableCategoriesEdit(this)" disabled>
						<option value=""></option>
						<?php foreach ($parent_catagories as $parent_category_value) {  ?>
						<option value="<?php echo $parent_category_value->pcat_id; ?>" <?php echo ($parent_category_value->pcat_id==$pcat_id)? "selected":''; ?>><?php echo $parent_category_value->pcat_name; ?></option>
						<?php } if(empty($parent_catagories)){
							?>
							<option value="0" selected>--None--</option>
							<?php 
						}
						 if($pcat_id==0){
							?>
							<option value="0" selected>--None--</option>
							<?php 
						}else{?>
						<option value="0">--None--</option>
						<?php }
						?>
					</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">-Select Category-</label>
					<select name="edit_categories" id="edit_categories" class="form-control " onchange="showAvailableSubCategoriesEdit(this)">
						<option value=""></option>
						<?php foreach ($categories as $category_value) {  ?>
						<option value="<?php echo $category_value->cat_id; ?>" <?php echo ($category_value->cat_id==$cat_id)? "selected":''; ?>><?php echo $category_value->cat_name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Sub Category-</label>
					<select name="edit_subcategories" id="edit_subcategories" class="form-control" onchange="showAvailableBrandsEdit(this)">
						<option value=""></option>
						<?php foreach ($subcategories as $subcategory_value) {  ?>
						<option value="<?php echo $subcategory_value->subcat_id; ?>"<?php echo ($subcategory_value->subcat_id==$subcat_id)? "selected":''; ?>><?php echo $subcategory_value->subcat_name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
				
				<div class="col-md-3">  
				<div class="form-group">
					<label class="control-label">Enter Filter Box Name </label>
					<input id="edit_filterbox_name" name="edit_filterbox_name" type="text" class="form-control" value="<?php echo $get_filterbox_data["filterbox_name"];?>"/>
					<input id="edit_filterbox_name_default" name="edit_filterbox_name_default" type="hidden" class="form-control" value="<?php echo $get_filterbox_data["filterbox_name"];?>"/>
				</div>
				</div>
				
				
				<div class="col-md-2">  
				<div class="form-group">
					<label class="control-label">Enter Filter Units </label>
					<input id="edit_filterbox_units" name="edit_filterbox_units" type="text" class="form-control" value="<?php echo $get_filterbox_data["filterbox_units"];?>"/>
					<input id="edit_filterbox_units_default" name="edit_filterbox_units_default" type="hidden" class="form-control" value="<?php echo $get_filterbox_data["filterbox_units"];?>"/>
				</div>
				</div>
				
				
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<div class="checkbox">
					<label><input id="edit_pcat_view" name="edit_pcat_view" value="1" type="checkbox" <?php if($get_filterbox_data["pcat_view"]=="1"){echo "checked";}?> disabled><a href="#" data-toggle="tooltip" title="This filterbox will be shown when customer clicks on Parent Category, in order to see all the inventories under that Parent Category. In this case, this filterbox is shown under filters section of frontend.">Is this filterbox common to Parent Category?</a></label>
					</div>
				</div>
				</div>
			</div>			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<div class="checkbox">
					<label><input id="edit_cat_view" name="edit_cat_view" value="1" type="checkbox" <?php if($get_filterbox_data["cat_view"]=="1"){echo "checked";}?> disabled><a href="#" data-toggle="tooltip" title="This filterbox will be shown when customer clicks on Category, in order to see all the inventories under that Category. In this case, this filterbox is shown under filters section of frontend.">Is this filterbox common to Category?</a></label>
					</div>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<div class="checkbox">
					<label><input id="edit_subcat_view" name="edit_subcat_view" value="1" type="checkbox" <?php if($get_filterbox_data["subcat_view"]=="1"){echo "checked";}?> disabled><a href="#" data-toggle="tooltip" title="This filterbox will be shown when customer clicks on Sub Category, in order to see all the inventories under that Sub Category. In this case, this filterbox is shown under filters section of frontend.">Is this filterbox common to Sub Category?</a></label>
					</div>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-5 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Filterbox Type-</label>
					<select name="edit_filterbox_type" id="edit_filterbox_type" class="form-control">
							<option value=""></option>
							<option value="checkbox" <?php if($get_filterbox_data["type"]=="checkbox"){echo "selected";}?>>Check box</option>
							<option value="select" <?php if($get_filterbox_data["type"]=="select"){echo "selected";}?>>Select</option>
							<option value="size" <?php if($get_filterbox_data["type"]=="size"){echo "selected";}?>>Size</option>
							<option value="slider" <?php if($get_filterbox_data["type"]=="slider"){echo "selected";}?>>Slider</option>
							<option value="singleslider" <?php if($get_filterbox_data["type"]=="singleslider"){echo "selected";}?>>Single Slider</option>
							<option value="doubleslider" <?php if($get_filterbox_data["type"]=="doubleslider"){echo "selected";}?>>Double Slider</option>
						</select>
				</div>
				</div>
				
				<div class="col-md-5">  
				<div class="form-group">
					<label class="control-label">Enter Sort Order Value</label>
					
					<select name="edit_sort_order" id="edit_sort_order" class="form-control" onchange="show_view_validatn()">
					<option value="<?php echo $get_filterbox_data["sort_order"];?>" selected><?php echo $get_filterbox_data["sort_order"];?></option>
						<?php
							if($get_sort_order_options_for_filterbox["key"]==""){
								if($get_filterbox_data["sort_order"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php } ?>
								<option value="1">1</option>
								<?php
							}
							else{
								if($get_filterbox_data["sort_order"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php
								}
								$value_arr=explode(",",$get_sort_order_options_for_filterbox["value"]);
								foreach($value_arr as $k => $v){
									?>
										<option value="<?php echo $v;?>"><?php echo $v;?></option>
									<?php
									}
							}
						?>
					</select>
					<input id="edit_sort_order_default" name="edit_sort_order_default" type="hidden" value="<?php echo $get_filterbox_data["sort_order"];?>"/>
				</div>
				</div>
				
			</div>		
				
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group label-floating">
			<label class="control-label">-Swap Options-</label>
			<select class="form-control" id="show_available_common_filterbox" onchange="changeEventHandler(event)">
			<option selected value=""></option>
			<?php foreach($filterbox_sort_order_arr as $filterbox_order_arr){ ?>
				<option value="<?php echo $filterbox_order_arr["filterbox_id"];?>"><?php echo $filterbox_order_arr["filterbox_name"];?></option>
			<?php } ?>
			
			</select>
			</div>
				</div>
			</div>
					
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group">
					<div class="col-md-6">Show Filterbox</div>
					<div class="col-md-3">
						<label class="radio-inline"><input id="edit_view1" name="edit_view" value="1" type="radio" <?php if($get_filterbox_data["view_filterbox"]=="1"){echo "checked";}?>>View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input id="edit_view2" name="edit_view" value="0" type="radio" <?php if($get_filterbox_data["view_filterbox"]=="0"){echo "checked";}?>>Hide</label>
					</div>
				
					</div>
				</div>
			</div>		
			
			<div class="row">
				<div class="col-md-10 col-md-offset-4"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit-data" disabled="">Submit</button>
		
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
					</div>
				</div>
			</div>	

		</div>		
	</form>
	
	<form name="search_for_filterbox" id="search_for_filterbox" method="post">
		<input value="<?php echo $get_filterbox_data["pcat_id"]; ?>" type="hidden" id="pcat_id" name="pcat_id"/>
		<input value="<?php if($get_filterbox_data["cat_id"]=='0'){echo '';}else{echo $get_filterbox_data["cat_id"];}; ?>" type="hidden" id="cat_id" name="cat_id"/>
		<input value="<?php if($get_filterbox_data["subcat_id"]=='0'){echo '';}else{echo $get_filterbox_data["subcat_id"];}; ?>" type="hidden" id="subcat_id" name="subcat_id"/>
		<input value="view" type="hidden" name="create_editview">
		
	</form>
</div>
</div>
</div>
</div>

</div>
<div class="footer">
</div>
<script>
function changeEventHandler(event){
	
        parent_id=event.target.value;
		if(parent_id!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		}
		if(parent_id==""){
	edit_sort_order=document.getElementById("edit_sort_order").value;
	if(edit_sort_order==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;					
	}
	if(edit_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
		}
}
function show_view_validatn(){
	edit_sort_order=document.getElementById("edit_sort_order").value;
	show_available_common_filterbox=document.getElementById("show_available_common_filterbox").value;
	if(show_available_common_filterbox==""){
	if(edit_sort_order==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	//document.getElementById('show_available_common_filterbox').disabled = false;	
	}
	if(edit_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		//document.getElementById('show_available_common_filterbox').disabled = true;
	}
	}
	if(show_available_common_filterbox!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
	
}
</script>
<script>

function showAvailableCategoriesEdit(obj)
{

	//if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#edit_categories").html(data);
					}
					else{
						$("#edit_categories").html('<option value="">-Select category-</option>')
						$("#brands").html('<option value="0"></option>')

					}
				}
			});
	//}
}
function showAvailableSubCategoriesEdit(obj){
	var default_pcat_id=document.getElementById("default_pcat_id").value;	
	var default_cat_id=document.getElementById("default_cat_id").value;
	
	cat_id=obj.value;
	//alert(cat_id);
	//alert(default_cat_id);
	if(default_cat_id==cat_id){
	document.getElementById("show_available_common_filterbox").disabled = false;
	
	}
	if(default_cat_id!=cat_id){
	document.getElementById("show_available_common_filterbox").disabled = true;
	
	}

		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories_for_filterbox",
				type:"post",
				data:"cat_id="+cat_id+"&active=1",
				success:function(data){
					if(data!=0){
						$("#edit_subcategories").html(data);
						document.getElementById("edit_pcat_view").checked=false;
						document.getElementById("edit_cat_view").checked=true;
						document.getElementById("edit_subcat_view").checked=true;
						document.getElementById("edit_pcat_view").disabled=true;
						document.getElementById("edit_cat_view").disabled=false;
						document.getElementById("edit_subcat_view").disabled=true;
					}
					if(data==0){
						
						$("#edit_subcategories").html('<option value=""></option>');
						document.getElementById("edit_pcat_view").checked=true;
						document.getElementById("edit_cat_view").checked=true;
						document.getElementById("edit_subcat_view").checked=true;
						document.getElementById("edit_pcat_view").disabled=false;
						document.getElementById("edit_cat_view").disabled=true;
						document.getElementById("edit_subcat_view").disabled=true;
					}

				}
			});
	
}
function showAvailableBrandsEdit(obj)
{
	
	var default_subcat_id=document.getElementById("default_subcat_id").value;
	subcat_id=obj.value;
	if(default_subcat_id==subcat_id){
	document.getElementById("show_available_common_filterbox").disabled = false;
	}
	if(default_subcat_id!=subcat_id){
	document.getElementById("show_available_common_filterbox").disabled = true;
	}
	
if(obj.value!="" && obj.value!="None"){
	document.getElementById("show_available_common_filterbox").disabled = true;
	document.getElementById("edit_pcat_view").checked=false;
	document.getElementById("edit_cat_view").checked=false;
	document.getElementById("edit_subcat_view").checked=true;
	document.getElementById("edit_pcat_view").disabled=true;
	document.getElementById("edit_cat_view").disabled=true;
	document.getElementById("edit_subcat_view").disabled=false;
}else{
		document.getElementById("show_available_common_filterbox").disabled = false;
		document.getElementById("edit_pcat_view").checked=false;
		document.getElementById("edit_cat_view").checked=true;
		document.getElementById("edit_subcat_view").checked=true;
		document.getElementById("edit_pcat_view").disabled=true;
		document.getElementById("edit_cat_view").disabled=false;
		document.getElementById("edit_subcat_view").disabled=true;
	}
}


$(document).ready(function(){
var button = $('#submit-data');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_filterbox'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio' || type == 'checkbox') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_filterbox"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_filterbox'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select' ) {
            disable = (orig[id].value == $(this).val());

	} else if (type == 'radio') {
			disable = (orig[id].checked == $(this).is(':checked'));
        }
		else if (type == 'checkbox') {
			disable = (orig[id].checked == $(this).is(':checked'));
			//document.getElementById("show_available_common_filterbox").disabled = false;
        }

        if (!disable) {
           
			if (type == 'checkbox') {
			//document.getElementById("show_available_common_filterbox").style.visibility = 'hidden';
			//document.getElementById("show_available_common_filterbox").disabled = true;
			 return false; // break out of loop
			}else{
				 return false; // break out of loop
			}
        }
    });

    button.prop('disabled', disable);
});
});
function form_validation_edit()
{
	var filterbox_id=$("#edit_filterbox_id").val();
	var filterbox_name = $('input[name="edit_filterbox_name"]').val().trim();
	var filterbox_units = $('input[name="edit_filterbox_units"]').val().trim();
	var filterbox_type = $('select[name="edit_filterbox_type"]').val().trim();
	var parent_category = $('select[name="edit_parent_category"]').val().trim();
	var categories = $('select[name="edit_categories"]').val().trim();
	var subcategories = $('select[name="edit_subcategories"]').val().trim();
	var edit_sort_order = $('select[name="edit_sort_order"]').val().trim();
	var view = document.querySelector('input[name="edit_view"]:checked').value;
	var sort_order_default = $('input[name="edit_sort_order_default"]').val().trim();
	var edit_subcat_view=document.getElementById('edit_subcat_view').checked;
	
	var common_filterbox="";
	
		if($("#show_available_common_filterbox").length!=0){
			var common_filterbox=document.getElementById("show_available_common_filterbox").value;
		}

	   var err = 0;
	   if((filterbox_name=='') || (filterbox_type=='') || (parent_category=='') || (edit_sort_order=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
				$("#pcat_id_reload").val(parent_category);
				$("#cat_id_reload").val(categories);
				$("#subcat_id_reload").val(subcategories);
				
				document.getElementById("edit_parent_category").disabled="";
				document.getElementById("edit_pcat_view").disabled="";
				document.getElementById("edit_cat_view").disabled="";
				document.getElementById("edit_subcat_view").disabled="";

			var form_status = $('<div class="form_status"></div>');		
			var form = $('#edit_filterbox');		
	swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();		
					 $.ajax({
							url: '<?php echo base_url()."admin/Catalogue/edit_filterbox/"?>',
							type: 'POST',
							data: $('#edit_filterbox').serialize()+"&common_filterbox="+common_filterbox,
							dataType: 'html',
							
						}).done(function(data)
						  {	
					  
						  form_status.html('') 
						  
								if(data==1)
								{
									  									  
									swal({
										title:"Success", 
										text:"Filterbox is successfully updated!", 
										type: "success",
										allowOutsideClick: false
									}).then(function () {
										//location.reload();
										$("#reload_form")[0].submit();
									});
								}
								if(data==0)
								{
									swal(
										'Oops...',
										'Error in form',
										'error'
									)	
								}
				
								
			}); 
}
}]);			
			}
	
	return false;
}

</script>
<form action="<?php echo base_url()?>admin/Catalogue/edit_filterbox_form" method="post" id="reload_form">
	<input type="hidden" name="pcat_id" id="pcat_id_reload" value="<?php echo $get_filterbox_data["pcat_id"];?>">
	<input type="hidden" name="cat_id" id="cat_id_reload" value="<?php echo $get_filterbox_data["pcat_id"];?>">
	<input type="hidden" name="subcat_id" id="subcat_id_reload" value="<?php echo $get_filterbox_data["pcat_id"];?>">
	<input type="hidden" name="filterbox_id" id="filterbox_id_reload" value="<?php echo $filterbox_id;?>">
	
</form>
</div>
</body>

</html>