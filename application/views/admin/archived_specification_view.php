<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
var table;

$(document).ready(function (){
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    var rows_selected = [];
    table = $('#table_specification').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Catalogue/specification_processing",
			data: function (d) { d.pcat_id = $('#pcat_id').val();d.cat_id = $('#cat_id').val();d.subcat_id = $('#subcat_id').val();arr=$('#specification_group_id').val().split('-');d.specification_group_id = arr[0];d.active =0;},
            type: "post",
            error: function(){
              $("#table_specification_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				
				document.getElementById("archived_specification_count").innerHTML=json.recordsFiltered;
				
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
    });
	$('#reset_form_button').on('click',function(){
		table.draw();
	});
	$("#submit_button").click(function() {
		table.draw();
	});
	
});	

function showAvailableCategories_drawtable(obj){
	//if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=2",
				success:function(data){
					table.draw();
					if(data!=0){
						val=data+'<option value="">No Category</option>';
						$("#cat_id").html(val);
					}else{
						$("#cat_id").html('<option value="0">No Category</option>');
					}
				}
		});
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_specification_group",
				type:"post",
				data:"pcat_id="+pcat_id+"&active=2"+"&flagtype=1&with_status=1",
				success:function(data){
					if(data!=0){
						$("#specification_group_id").html(data);
						table.draw();
					}else{
						$("#specification_group_id").html('<option value="">-None-</option>');
					}
				}
		});
	//}
}
function showAvailableSubCategories_drawtable(obj){
	var pcat_id=document.getElementById("pcat_id").value;
	//if(obj.value!="" && obj.value!="None"){
		cat_id=obj.value;
		$.ajax({
			url:"<?php echo base_url(); ?>admin/Catalogue/show_available_subcategories",
			type:"post",
			data:"cat_id="+cat_id+"&active=2",
			success:function(data){
				table.draw();
				if(data!=0){
					val=data+'<option value="">No SubCategory</option>';
					$("#subcat_id").html(val);
				}else{
					$("#subcat_id").html('<option value="0">No SubCategory</option>');
				}
			}
		});
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_specification_group",
				type:"post",
				data:"pcat_id="+pcat_id+"&cat_id="+cat_id+"&active=2"+"&flagtype=2&with_status=1",
				success:function(data){
					if(data!=0){
						$("#specification_group_id").html(data);
						table.draw();
					}else{
						$("#specification_group_id").html('<option value="">-None-</option>');
					}
				}
			});
	//}
}
function showAllSpecificationGroup_drawtable(obj){
	var pcat_id=$('#pcat_id').val();
	var cat_id=$('#cat_id').val();
	//if(obj.value!="" && obj.value!="None"){
		subcat_id=obj.value;
		$.ajax({
			url:"<?php echo base_url(); ?>admin/Catalogue/show_available_specification_group",
			type:"post",
			data:"pcat_id="+pcat_id+"&cat_id="+cat_id+"&subcat_id="+subcat_id+"&active=2&with_status=1"+"&flagtype=3",
			success:function(data){	
				if(data!=0){
					$("#specification_group_id").html(data);
					table.draw();
				}else{
					$("#specification_group_id").html('<option value="">-None-</option>');
				}
			}
		});
	//}
}
function drawtable(obj){
	arr = obj.value.split('-');
	status=arr[1];
	$('#active').val(status);
	table.draw();
}
function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_restore_fun(table_name){
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table_name+"!");
		return false;
	}
	selected_list=selected_list_arr.join(",");
	swal({
			title: 'Are you sure?',
			text: "Restore Archived Specication",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, restore it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	active=$('#active').val();

	if(active==1){
		$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/update_specification_selected",
		type:"post",
		data:"selected_list="+selected_list+"&active=1",
		
		success:function(data){
			if(data==true){
				swal({
						title:"Restored!", 
						text:"Given "+table_name+"(s) has been restored successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	}else{
		swal(
			'Sorry',
			'You cant restore it. Try to restore the corresponding (Main) Specification.',
			'error'
		)
		var obj=document.getElementsByName("common_checkbox");	
		var obj_arr=document.getElementsByName("selected_list");
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
		for(i=0;i<obj.length;i++){
			obj[i].checked=false;
		}
	}
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
	
	<div class="row" id="viewDiv1">
	<div class="wizard-header text-center">
		<h5> Number of Archived Specification <span class="badge"><div id="archived_specification_count"></div></span> 
		</h5>                 		          	
	</div>
	<input value="<?php echo $pcat_id; ?>" type="hidden" id="pcat_id">
	<input value="<?php echo $cat_id; ?>" type="hidden" id="cat_id">
	<input value="<?php echo $subcat_id; ?>" type="hidden" id="subcat_id">
	<input value="<?php echo $specification_group_id; ?>" type="hidden" id="specification_group_id">
	<input value="<?php echo $active; ?>" type="hidden" name="active" id="active">
		
	<table id="table_specification" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th colspan="8">
					<div class="padding_right"><button id="multiple_delete_button" class="btn btn-success btn-xs" onclick="multilpe_restore_fun('specification')">Restore</button></div>
				</th>
			</tr>
			<tr>
				<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
				<th>Specification Name</th>
				<th>Specification Values</th>
				<th>Specification Group Name</th>	
				<th>Specification to Catalog</th>
				<th>Sort Order</th>
				<th>View</th>
				<th>Last Updated</th>	
			</tr>
		</thead>
	</table>
	</div>

</div>
</div>
</body>

</html>