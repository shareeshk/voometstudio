<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style>
.margin_top{
	margin-top:5em;
}
select {
	width:230px;
}
.entities{
	width:500px;
	height:100px;
}
.select_list_entities,.select_list_entities{
	width:180px;
}
.move_btns{
	width:50px;
}
#moveright,#moveleft,#moverightall,#moveleftall{
	margin:14px 0px 5px 0px;
	width:40px;
}
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
select.form-control[multiple], .form-group.is-focused select.form-control[multiple] {
    height: 140px;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/multiselect.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#search_rightAll,#search_rightSelected,#search_leftAll,#search_leftSelected").click(function(){
		setTimeout(function(){ 
			obj=document.getElementById("search_to");
			for(i=0;i<obj.length;i++){
				obj.options[i].selected=true;
			} 
		}, 1);
	});
	
});
function test(){

var button = $('#submit-data');
var orig = [];
var orig1 = [];

$.fn.getType = function () {
	//alert(this[0].tagName +"|"+ $(this[0]).attr("multiple"));
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : ((this[0].tagName.toLowerCase()=="select" && $(this[0]).attr("multiple")=="multiple") ? (this[0].tagName.toLowerCase()+"-"+$(this[0]).attr("multiple")) : this[0].tagName.toLowerCase());
}
search_arr=[];
$("form[name='edit_logistics_service'] :input").each(function () {
		
		var type = $(this).getType(); 
		
		var tmp = {
			'type': type,
			'value': $(this).val()
		};
		
		if (type == 'radio' || type == 'text' || type == 'select') {
			tmp.checked = $(this).is(':checked');
			orig[$(this).attr('id')] = tmp;
		}
		if (type == 'select-multiple') {
			 search_arr=[];
			 $(this).find("option").each(function(){
				 search_arr.push($(this).attr("value"));
			 });
			orig1[$(this).attr('id')] = search_arr;
		}
});

$('form[name="edit_logistics_service"]').bind('change keyup dblclick click', function () {
    var disable = true;
	
    $("form[name='edit_logistics_service'] :input").each(function () {
        var type = $(this).getType();

        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
			if(orig[id] !== undefined){
				disable = (orig[id].value == $(this).val());
			}
			
        } else if (type == 'select-multiple') {
			 search_cur_arr=[];
			$(this).find("option").each(function(){
				 search_cur_arr.push($(this).attr("value"));
			 });
            disable = (orig1[id].sort().join("-") == search_cur_arr.sort().join("-"));
			
        }else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false;
        }
    });

    button.prop('disabled', disable);
});

/////////////////////////
}

function showDivPrevious(){
	
	var form = document.getElementById('search_for_logistics_service');		
	
	form.action='<?php echo base_url()."admin/Catalogue/logistics_service"; ?>';
	form.submit();
			
	 //window.location.href = '<?php echo base_url(); ?>admin/Catalogue/category';
}
</script>

<?php //print_r($logistics_service);?>
 <body>
 <div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">

<div class="row" id="editDiv3">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="edit_logistics_service" id="edit_logistics_service" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_vendors: {
					required: true,
				},
				edit_logistics: {
					required: true,
				},
				edit_pin: {
					required: true,
				},
				edit_city: {
					required: true,
				},
				'edit_to_select_list[]': {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Logistics Service</h3>
		</div>	
		</div>
		<div class="tab-content">				
		<input type="hidden" name="edit_logistics_service_id" id="edit_logistics_service_id" value="<?php echo $get_logistics_service_data["logistics_service_id"];?>">
			<div class="row">
				<div class="col-md-12">                               
				<div class="form-group">
					<label class="control-label">-Select Vendor-</label>
					<select name="edit_vendors" id="edit_vendors" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($vendors as $vendors_value) {  ?>
						<option value="<?php echo $vendors_value->vendor_id; ?>" <?php echo ($vendors_value->vendor_id==$get_logistics_service_data['vendor_id'])? "selected":''; ?>><?php echo $vendors_value->name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">                               
				<div class="form-group">
					<label class="control-label">-Select Logistics Name-</label>
					<select name="edit_logistics" id="edit_logistics" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($logistics as $logistics_value) {  ?>
						<option value="<?php echo $logistics_value->logistics_id; ?>" <?php echo ($logistics_value->logistics_id==$get_logistics_service_data['logistics_id'])? "selected":''; ?>><?php echo $logistics_value->logistics_name; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">                               
				<div class="form-group">
					<label class="control-label">-Select City Pin(first 3 characters)-</label>
					<select name="edit_pin" id="edit_pin" class="form-control" disabled>
						<option value=""></option>
						<?php foreach ($logistics_all_pincodes as $logistics_all_pincodes_value) {  ?>
						<option value="<?php echo $logistics_all_pincodes_value->pin."_".strtolower($logistics_all_pincodes_value->city); ?>"><?php echo $logistics_all_pincodes_value->city." - ".$logistics_all_pincodes_value->pin; ?></option>
						<?php } ?>
					</select>
				</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-12">                               
				<div class="form-group">
					<label class="control-label">Enter City Name</label>
					<input id="edit_city" name="edit_city" type="text" class="form-control"  value="<?php echo $get_logistics_service_data["city"];?>"/>
				</div>
				</div>
			</div>		

			<script>
			function go(obj){
				//if(obj.options[obj.selectedIndex].value){
					test();
				//}
			}
			</script>
			
			<div class="row">
				<div class="col-md-12">  
                <div class="col-md-5">
                    <select name="edit_from_select_list[]" id="search" class="form-control" size="8" multiple="multiple">
                      
                    </select>
                </div>
                
                <div class="col-md-2 margin_top">
                    <button type="button" id="search_rightAll" class="btn btn-block btn-xs"><i class="glyphicon glyphicon-forward"></i></button>
                    <button type="button" id="search_rightSelected" class="btn btn-block btn-xs"><i class="glyphicon glyphicon-chevron-right"></i></button>
                    <button type="button" id="search_leftSelected" class="btn btn-block btn-xs"><i class="glyphicon glyphicon-chevron-left"></i></button>
                    <button type="button" id="search_leftAll" class="btn btn-block btn-xs"><i class="glyphicon glyphicon-backward"></i></button>
                </div>
                
                <div class="col-md-5">
                    <select name="edit_to_select_list[]" id="search_to" class="form-control" size="8" multiple="multiple"></select>
                </div>
				</div>
            </div>
			<div class="row">
				<div class="col-md-9 col-md-offset-3"> 
					<button class="btn btn-info btn-sm" type="submit" id="submit-data" disabled="">Submit</button>
		
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
				</div>
			</div>

		</div>		
	</form>
	<form name="search_for_logistics_service" id="search_for_logistics_service" method="post">
		<input value="<?php echo $get_logistics_service_data["vendor_id"]; ?>" type="hidden" id="vendor_id" name="vendor_id"/>
		<input value="<?php echo $get_logistics_service_data["logistics_id"]; ?>" type="hidden" id="logistics_id" name="logistics_id"/>
		<input value="view" type="hidden" name="create_editview">
	</form>
</div>
</div>
</div>
</div>

</div>
<div class="footer">
</div>
</div>
</body>
</html>

<script type="text/javascript">
$(document).ready(function() {
    // make code pretty
    $('#search').multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
        }
    });
});
</script>

<script type="text/javascript">
function move_list_items(sourceid, destinationid){
	$("#"+sourceid+"  option:selected").appendTo("#"+destinationid);
}


	//this will move all selected items from source list to destination list
function move_list_items_all(sourceid, destinationid){
	$("#"+sourceid+" option").appendTo("#"+destinationid);
}

editLogisticsServiceFun();

function editLogisticsServiceFun(){

					logistics_service_id='<?php echo $get_logistics_service_data["logistics_service_id"]; ?>';
					city='<?php echo $get_logistics_service_data["city"]; ?>';
					pin='<?php echo $get_logistics_service_data["pin"]; ?>';
					pincode='<?php echo $get_logistics_service_data["pincode"]; ?>';
					pincodes='<?php echo $get_logistics_service_data["pincodes"]; ?>';
	
					data_arr_from=pincodes.split("|");//from all pincodes
					data_arr_to=pincode.split("|");//from all logistics services
					diff_from_arr = data_arr_from.filter(function(x) { return data_arr_to.indexOf(x) < 0 })
					html_options_from='';
					for(l=0;l<diff_from_arr.length;l++){
						j=l+1;
						html_options_from+='<option value="'+diff_from_arr[l]+'" data-position="'+j+'">'+diff_from_arr[l]+'</option>';
					}
					html_options_to='';				
					for(l=0;l<data_arr_to.length;l++){
						html_options_to+='<option value="'+data_arr_to[l]+'">'+data_arr_to[l]+'</option>';
					}

					$("#search").html(html_options_from);
					editsearch=$("#search_to").html(html_options_to);
					$.each(pincode.split("|"), function(i,e){
						$("#search_to option[value='" + e + "']").prop("selected", true);
					});	
					
					
					pin_value=pin.trim()+"_"+city.trim().toLowerCase();

					$("#edit_pin").val(pin_value);
				
				test();
			
				
}
$('#result').hide();

function form_validation_edit()
{
	
	var city = $('input[name="edit_city"]').val().trim();
	var pin = $('select[name="edit_pin"]').val().trim();
	var to_select_list=document.getElementsByName("edit_to_select_list[]");
	var logistics = $('select[name="edit_logistics"]').val().trim();

	   var err = 0;var select_err=0;
		
		$("#search_to option").each(function() {
				if($(this).is(':selected')){
					select_err++;
				}	
		});	
		if(select_err==0)
		{
			$('select[name="edit_to_select_list[]"]').css({"border": "1px solid red"});
			err=1;
		}else{
			$('select[name="edit_to_select_list[]"]').css({"border": "1px solid #ccc"});
		}
		
		if(city=='')
		{
		  // $('input[name="edit_city"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('input[name="edit_city"]').css({"border": "1px solid #ccc"});
		}
		if(pin=='')
		{
		  // $('input[name="edit_pin"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('input[name="edit_pin"]').css({"border": "1px solid #ccc"});
		}
		
		if(logistics=='')
		{
		  // $('select[name="edit_logistics"]').css("border", "1px solid red");	   
		   err = 1;
		}
		else{			
			//$('select[name="edit_logistics"]').css({"border": "1px solid #ccc"});
		}
		
		if(err==1)
		{
			   $('#error_result_id').show();
			   $('#result').hide();
		}
		else
			{
				document.getElementById("edit_vendors").disabled="";
				document.getElementById("edit_logistics").disabled="";
				document.getElementById("edit_pin").disabled="";
				var form_status = $('<div class="form_status"></div>');		
				var form = $('#edit_logistics_service');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();				
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_logistics_service/"?>',
				type: 'POST',
				data: $('#edit_logistics_service').serialize(),
				dataType: 'html',
				
			}).done(function(data)
			  {	
				if(data)
				{
					 				  
					swal({
						title:"Success", 
						text:"Logistics Service is successfully Updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				else
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)	
				}
				
			});
}
}]);			
			}
	
	return false;
}


</script>
