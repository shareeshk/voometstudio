<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
var table;
$(document).ready(function (){
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    var rows_selected = [];
    table = $('#table_inventory').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Catalogue/inventory_processing",
			data: function (d) { d.search_attribute_1_value = '<?php echo $search_attribute_1_value;?>'; d.search_attribute_2_value = '<?php echo $search_attribute_2_value;?>';d.search_attribute_3_value = '<?php echo $search_attribute_3_value;?>';d.search_attribute_4_value = '<?php echo $search_attribute_4_value;?>';d.search_product_status = '<?php echo $search_product_status;?>';d.search_view_product_status = '<?php echo $search_view_product_status;?>';d.product_id = $('#product_id').val();d.active=0},
            type: "post",
            error: function(){
              $("#table_inventory_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				
				document.getElementById("archived_inventory_count").innerHTML=json.recordsFiltered;
				
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });  
	$('#reset_form_button').on('click',function(){
		table.draw();
	});	
	$("#submit_button").click(function(){
		table.draw();
	});	
	
});	
function drawtable(obj){
	table.draw();
}
function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_restore_fun(table_name){
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table_name+"!");
		return false;
	}
	selected_list=selected_list_arr.join(",");
	swal({
			title: 'Are you sure?',
			text: "Restore Archived Products",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, restore it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	active=$('#product_active').val();
	if(active==1){
		$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/update_inventory_selected",
		type:"post",
		data:"selected_list="+selected_list+"&active=1",
		
		success:function(data){
			if(data==true){
				swal({
						title:"Restored!", 
						text:"Given "+table_name+"(s) has been restored successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	}else{
		swal(
			'Sorry',
			'You cant restore it. Try to restore the corresponding (Main) Product.',
			'error'
		)
		var obj=document.getElementsByName("common_checkbox");	
		var obj_arr=document.getElementsByName("selected_list");
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
		for(i=0;i<obj.length;i++){
			obj[i].checked=false;
		}
	}
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<!--<div><h4>Manage Inventory of - <?php //echo $product_details["product_name"]; ?></h4></div>-->
<div class="row">
<div class="wizard-header text-center">
	<h5>
		 Number of Inventory <span class="badge"><div id="archived_inventory_count"></div></span> 
	</h5>
	
</div>

</div>
<div class="row" id="viewDiv1">
	
		<input type="hidden" value="<?php echo $pcat_id; ?>" name="pcat_id" id="pcat_id">
		<input type="hidden" value="<?php echo $cat_id; ?>" name="cat_id" id="cat_id">
		<input type="hidden" value="<?php echo $subcat_id; ?>" name="subcat_id" id="subcat_id">
		<input type="hidden" value="<?php echo $brand_id; ?>" name="brand_id" id="brand_id">
		<input name="product_id" type="hidden" id="product_id" value="<?php echo $product_id;?>">
		<input name="product_active" type="hidden" id="product_active" value="<?php echo $product_active;?>">
		<input name="brand_active" type="hidden" id="brand_active" value="<?php echo $brand_active;?>">
	
<table id="table_inventory" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<th colspan="4">
			<div class="padding_right"><button id="multiple_delete_button" class="btn btn-success btn-xs" onclick="multilpe_restore_fun('inventory')">Restore</button></div>	
		</th>
	</tr>
	<tr>
		<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
		<th>Image</th>
		<th>SKU Details</th>
		<th>Details</th>	
	</tr>
</thead>

</table>
</div>

</div>
</div>
</body>

</html>