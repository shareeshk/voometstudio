<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Refund Request Orders - Incoming Requests</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script>

<script type="text/javascript">
               
$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   var table = $('#returns_refund_request_orders_table_accounts_incoming').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Returns/returns_refund_request_orders_accounts_incoming_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#returns_refund_request_orders_table_accounts_incoming_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("refund_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'40%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});	
$(document).ready(function(){
	var returns_refund_orders_list_checked=0;
	$("input[name='returns_refund_orders_list']").each(function(){
		if($(this).is(":checked")){
			returns_refund_orders_list_checked++;
		}
	})
	/*if(returns_refund_orders_list_checked==0){
		document.getElementById("refund_info_div").style.display="none";
	}*/
})

function printcontent(order_item_id){
	//Popup($("#print_"+elem).html());
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/returns_refund_request_orders_accounts_incoming_printcontent",
		type:"POST",
		data:"order_item_id="+order_item_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			Popup($("#print").html(data))
		}
	})
}

function Popup(printdiv) {
	var mywindow = window.open('', 'Refund Details', 'height=400,width=600');
	mywindow.document.write('<html><head><title>Refund Details</title>');
	mywindow.document.write('</head><body >');
	mywindow.document.write(printdiv.html());
	mywindow.document.write('</body></html>');

	mywindow.document.close(); // necessary for IE >= 10
	mywindow.focus(); // necessary for IE >= 10

	mywindow.print();
	mywindow.close();

	return true;
}	
</script>

<script>


function open_return_refund_success_fun_razorpay(order_item_id){

    /*added*/
        /* razorpay refund */

        data=$('#razorpay_form_'+order_item_id).html();
        //alert(data);
        $("#return_refund_success_form_razorpay").html(data);
        $("#return_refund_success_modal_razorpay").modal("show");
}

function open_return_refund_success_fun(order_item_id){
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/returns_refund_request_orders_accounts_incoming_success",
		type:"POST",
		data:"order_item_id="+order_item_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			//alert(data);
			$("#return_refund_success_form").html(data);
			$("#return_refund_success_modal").modal("show");
		}
	})
	
}

function open_return_refund_failure_fun(order_item_id){
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/returns_refund_request_orders_accounts_incoming_failure",
		type:"POST",
		data:"order_item_id="+order_item_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			//alert(data);
			$("#return_refund_failure_form").html(data);
			$("#return_refund_failure_modal").modal("show");
		}
	})
	
	
}
function open_return_refund_wallet_fun(order_item_id){
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/returns_refund_request_orders_accounts_incoming_wallet",
		type:"POST",
		data:"order_item_id="+order_item_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			//alert(data);
			$("#return_refund_wallet_form").html(data);
			$("#return_refund_wallet_modal").modal("show");
		}
	})
	
}

</script>
<style>
.datepicker{z-index:1151 !important;}
.materialDate{z-index:1151 !important;}
</style>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="page-header"><h4 class="text-center">Refund Request Orders - Incoming Requests <span class="badge" id="refund_count"></span></h4></div>
	<table id="returns_refund_request_orders_table_accounts_incoming" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="text-primary small bold">Return Summary</th>
				<th class="text-primary small bold">Refund Details</th>
				<th class="text-primary small bold">Actions</th>
			</tr>
		</thead>
	</table>
</div>

<script>
$(document).ready(function(){
	
	$("#return_refund_success_form").on('submit',(function(e) {
		e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>admin/Returns/return_refund_success",
				type: "POST",      				
				data:  new FormData(this),	
				contentType: false,       		
				cache: false,					
				processData:false,  			
				success: function(data){
					if(data){
						$('#return_refund_success_modal').modal('hide');
						swal({
							title:"success!", 
							text:"Successfully sent", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#return_refund_success_form").trigger("reset");
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
					
				}	        
		   });
	}));

	$("#return_refund_success_form_razorpay").on('submit',(function(e) {
		e.preventDefault();
        fd=new FormData(this);
        order_item_id= fd.get("order_item_id");
        paymentId= fd.get("paymentId");

        if(paymentId=='' || paymentId==null){
            swal("Error", "Razorpay payment Id is required", "error");
            return false;
        }

        $(".req_btn_"+order_item_id).val('Processing...');
        $(".req_btn_"+order_item_id).prop('disabled',true);

        $.ajax({
            url: "<?php echo base_url();?>razorpay/razorpay_refund",
            type: "POST",
            data:  fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function (data) {

                data=JSON.parse(data);

                //alert(data.status);
                //alert(data.response);
                console.log(data);

                //return false;

                dat=data;
                if(parseInt(dat.status)==1) {

                    //alert(dat.razorpay_refund_id+"||"+dat.razorpay_refund_status);
                    console.log(dat.razorpay_refund_id+"||"+dat.razorpay_refund_status);


                    $('.razorpay_refund_id_'+order_item_id).val(dat.razorpay_refund_id);
                    $('.razorpay_refund_status_'+order_item_id).val(dat.razorpay_refund_status);


                    /* response */
                   // var formData = new FormData($(this)[0]);
                    console.log(new FormData(document.getElementById("return_refund_success_form_razorpay")));
                    $.ajax({
                        url:"<?php echo base_url();?>admin/Returns/return_refund_success_razorpay",
                        type: "POST",
                        data:  new FormData(document.getElementById("return_refund_success_form_razorpay")),
                        contentType: false,
                        cache: false,
                        processData:false,
                        success: function(data){
                            if(data){
                                $('#return_refund_success_modal_razorpay').modal('hide');
                                swal({
                                    title:"success!",
                                    text:"Successfully sent",
                                    type: "success",
                                    allowOutsideClick: false

                                }).then(function () {
                                    $("#return_refund_success_form_razorpay").trigger("reset");
                                    location.reload();

                                });
                            }else{
                                swal("Error", "not sent", "error");
                                $('#return_refund_success_modal_razorpay').modal('hide');
                                $(".req_btn_"+order_item_id).val('Submit');
                                $(".req_btn_"+order_item_id).prop('disabled',false);
                                location.reload();
                            }

                        }
                    });

                    /* response */
                }else{
                    $('#razorpay_refund_status_'+order_item_id).val(dat.response);
                    swal("Error", dat.response, "error");
                    $('#return_refund_success_modal_razorpay').modal('hide');
                    $(".req_btn_"+order_item_id).val('Submit');
                    $(".req_btn_"+order_item_id).prop('disabled',false);
                    //location.reload();
                    open_return_refund_failure_fun(order_item_id);
                }

            }
        });

        /* razorpay refund */

        /*added*/

	}));
	
});
function divFunction_account_success(){
    $("#return_refund_success_form").trigger("reset");	
		location.reload();
}
function divFunction_account_success_razorpay(){
    $("#return_refund_success_form_razorpay").trigger("reset");
		location.reload();
}
</script>



<!--- modal success starts----------------->
<div class="modal" id="return_refund_success_modal" data-backdrop="static" role="dialog" tabindex="-1" aria-hidden="true">
   <div class="modal-dialog modal-sm">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		<div class="panel panel-success">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                   Refund Successful Transaction Update
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right" onclick="divFunction_account_success()"></span></span>
               </div>
               <div aria-expanded="true" class="">
                  <div class="panel-body">
		 <form id="return_refund_success_form" method="post" enctype="multipart/form-data" class="form-horizontal">
		 
		
			</form>
			</div>
      </div>
   </div>
         </div>
      </div>
   </div>
</div>

<!--- modal success starts----------------->
<div class="modal" id="return_refund_success_modal_razorpay" data-backdrop="static" role="dialog" tabindex="-1" aria-hidden="true">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		<div class="panel panel-success">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                  Refund request to Razorpay
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right" onclick="divFunction_account_success_razorpay()"></span></span>
               </div>
               <div aria-expanded="true" class="">
                <div class="panel-body">
                <form id="return_refund_success_form_razorpay" method="post" enctype="multipart/form-data" class="form-horizontal">


                </form>
                </div>
      </div>
   </div>
         </div>
      </div>
   </div>
</div>

<script>
$(document).ready(function(){
	$("#return_refund_failure_form").on('submit',(function(e) {
		e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>admin/Returns/return_refund_failure",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					if(data){
						$('#return_refund_failure_modal').modal('hide');
						swal({
							title:"success!", 
							text:"Successfully sent", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#return_refund_failure_form").trigger("reset");
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
					
				}	        
		   });
	}));

});
function divFunction_account_failure(){
    $("#return_refund_failure_form").trigger("reset");	
		location.reload();
}
</script>

<!--- modal failure starts----------------->
<div class="modal" id="return_refund_failure_modal" data-backdrop="static" role="dialog" tabindex="-1" aria-hidden="true">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		<div class="panel panel-danger">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                   Refund Failed Transaction
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right" onClick="divFunction_account_failure()"></span></span>
               </div>
               <div aria-expanded="true" class="">
                  <div class="panel-body">
		 <form id="return_refund_failure_form" method="post" enctype="multipart/form-data" class="form-horizontal">
		 
		
			</form>
			</div>
      </div>
   </div>
         </div>
      </div>
   </div>
</div>

<!--- modal failure ends----------------->

<!--- return refund failure things ends --->

<!--- return wallet things starts --->

<script>

$(document).ready(function(){
	
	$("#return_refund_wallet_form").on('submit',(function(e) {
		e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>admin/Returns/return_refund_wallet",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					if(data){
						$('#return_refund_wallet_modal').modal('hide');
						swal({
							title:"success!", 
							text:"Amount has been transfered to wallet", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#return_refund_wallet_form").trigger("reset");
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
					
				}	        
		   });
	}));
	
});
function divFunction_account_wallet(){
    $("#return_refund_wallet_form").trigger("reset");	
		location.reload();
}
</script>


<!--- modal wallet starts----------------->
<div class="modal" id="return_refund_wallet_modal" data-backdrop="static" role="dialog" tabindex="-1" aria-hidden="true">
   <div class="modal-dialog modal-sm">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		  <div class="panel panel-info">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                  Transfer to Customer Wallet
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right" onClick="divFunction_account_wallet()"></span></span>
               </div>
               <div aria-expanded="true" class="">
                  <div class="panel-body">
		 <form id="return_refund_wallet_form" method="post" enctype="multipart/form-data" class="form-horizontal">
		 
		 
			</form>
			</div>
      </div>
   </div>
         </div>
      </div>
   </div>
</div>




<div class="container" id="print" style="display:none;">
	
</div>

</div>
</body>

</html>  