<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style type="text/css">

.stepwizard-step p {
    margin-top: 10px;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 50%;
    position: relative;
	margin-bottom: 3%;
}
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {

    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 2px;
}
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>	

<script type="text/javascript">

function showDivPrevious(){
	
	var form = document.getElementById('search_for_subreasons');		
	
	form.action='<?php echo base_url()."admin/Catalogue/subreason"; ?>';
	form.submit();
}
</script>
<?php
if($get_subreason_data['sort_order']=="0"){?>
	<script>
	$(document).ready(function () {
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	document.getElementById('show_available_common_subreason').disabled = true;
	
	});
	</script>
	<?php
}
?>
<?php 

	/*if(isset($_REQUEST["reason_id"])){
		$reason_id=$_REQUEST["reason_id"];	
	}else{
		header("Location:".base_url()."admin/Catalogue/subreason/");	
	}*/

?>

</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container-fluid">

<div class="row-fluid" id="editDiv3">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
				
	<form name="edit_subreason" id="edit_subreason" method="post" action="#" onsubmit="return form_validation_edit();">
	<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				edit_reason_id: {
					required: true,
				},
				edit_subreason_name: {
					required: true,
				},
				
				edit_menu_sort_order: {
					required: true,
				}
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
	</script>
		<div class="wizard-header">
		<div align="center">
			<h3 class="wizard-title formheader">Edit Subreasons</h3>
		</div>	
		</div>
		<div class="tab-content">				
		
		<input type="hidden" name="edit_sub_issue_id" id="edit_sub_issue_id" value="<?php echo $get_subreason_data["sub_issue_id"];?>">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">-Select Reason Return-</label>
					<select name="edit_reason_id" id="edit_reason_id" class="form-control" onchange="show_sort_order(this)">
						<option value=""></option>
						<?php foreach ($reason_return as $reason_return_value) {  ?>
						<option value="<?php echo $reason_return_value->reason_id; ?>"<?php echo ($reason_return_value->reason_id==$reason_id)? "selected":''; ?>><?php echo $reason_return_value->reason_name; ?></option>
						<?php } ?>
						<option value="0">--None--</option>
							
					</select>
				</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group">
					<label class="control-label">Enter Subreasons Name</label>
					<input id="edit_subreason_name" name="edit_subreason_name" type="text" class="form-control" value="<?php echo $get_subreason_data["sub_issue_name"];?>"/>
					<input id="edit_subreason_name_default" name="edit_subreason_name_default" type="hidden" class="form-control" value="<?php echo $get_subreason_data["sub_issue_name"];?>"/>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group">
					<label class="control-label">Enter the Sort Order</label>
					
					<select name="edit_menu_sort_order" id="edit_menu_sort_order" class="form-control" onchange="show_view_validatn()">
					<option value="<?php echo $get_subreason_data["sort_order"];?>" selected><?php echo $get_subreason_data["sort_order"];?></option>
						<?php
							if($get_sort_order_options_for_subreason["key"]==""){
								if($get_subreason_data["sort_order"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php } ?>
								<option value="1">1</option>
								<?php
							}
							else{
								if($get_subreason_data["sort_order"]!="0"){
								?>
								
								<option value="0">0</option>
								<?php
								}
								$value_arr=explode(",",$get_sort_order_options_for_subreason["value"]);
								foreach($value_arr as $k => $v){
									?>
										<option value="<?php echo $v;?>"><?php echo $v;?></option>
									<?php
									}
							}
						?>
					</select>
					<input id="edit_menu_sort_order_default" name="edit_menu_sort_order_default" type="hidden" value="<?php echo $get_subreason_data["sort_order"];?>"/>	
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group label-floating">
			<label class="control-label">-Swap Options-</label>
			<select class="form-control" id="show_available_common_subreason" onchange="changeEventHandler(event)">
			<option selected value=""></option>
			<?php
			
			foreach($subcategory_sort_order_arr as $subcategory_order_arr){
				
				?>
				
					<option value="<?php echo $subcategory_order_arr["sub_issue_id"];?>"><?php echo $subcategory_order_arr["sub_issue_name"];?></option>
					<?php
			}
			?>
			
			</select>
			</div>
				</div>
			</div>
			
							
			<div class="row">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="form-group">
					<div class="col-md-6">Show Subreasons</div>
					<div class="col-md-3">
						<label class="radio-inline"><input id="edit_view1" name="edit_view" value="1" type="radio" <?php if($get_subreason_data["view"]=="1"){echo "checked";}?>>View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input id="edit_view2" name="edit_view" value="0" type="radio" <?php if($get_subreason_data["view"]=="0"){echo "checked";}?>>Hide</label>
					</div>
				
					</div>
				</div>
			</div>	
			
			<div class="row">
				<div class="col-md-10 col-md-offset-3"> 
					<button class="btn btn-info btn-sm" type="submit" id="submit_edit_subreason" disabled="">Submit</button>
		
					<button class="btn btn-Warning btn-sm" type="button" onclick="showDivPrevious()">Go to Previous page</button>
				</div>
			</div>	
				
		</div>			
	</form>
	<form name="search_for_subreasons" id="search_for_subreasons" method="post">
		
		<input value="<?php echo $get_subreason_data["sub_issue_id"]; ?>" type="hidden" id="sub_issue_id" name="sub_issue_id"/>
		<input value="<?php echo $get_subreason_data["reason_id"]; ?>" type="hidden" id="reason_id" name="reason_id"/>
		<input value="view" type="hidden" name="create_editview">
	</form>
</div>
</div>	
</div>	
</div>	

</div>
<div class="footer">
</div>
<script>
function changeEventHandler(event){
        parent_id=event.target.value;
		if(parent_id!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		}
		if(parent_id==""){
	edit_menu_sort_order=document.getElementById("edit_menu_sort_order").value;
	if(edit_menu_sort_order==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;					
	}
	if(edit_menu_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
		}
}
function show_view_validatn(){
	edit_menu_sort_order=document.getElementById("edit_menu_sort_order").value;
	show_available_common_subreason=document.getElementById("show_available_common_subreason").value;
	if(show_available_common_subreason==""){
	if(edit_menu_sort_order==0){
	document.getElementById("edit_view1").disabled = true;
	document.getElementById("edit_view2").checked = true;
	document.getElementById('show_available_common_subreason').disabled = false;
	}
	if(edit_menu_sort_order!=0){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
		//document.getElementById('show_available_common_subreason').disabled = false;
	}
	}
	if(show_available_common_subreason!=""){
		document.getElementById("edit_view1").disabled = false;
		document.getElementById("edit_view1").checked = true;
	}
	
}
</script>

<script type="text/javascript">

function showAvailableCategoriesEdit(obj)
{
	if(obj.value!="" && obj.value!="None"){
		pcat_id=obj.value;
		$.ajax({
				url:"<?php echo base_url(); ?>admin/Catalogue/show_available_categories",
				type:"post",
				data:"pcat_id="+pcat_id,
				success:function(data){
					if(data!=0){
						$("#edit_categories").html(data);
					}
					else{
						$("#edit_categories").html('<option value="">-None-</option>')
						$("#reasons").html('<option value="">-None-</option>')

					}
				}
			});
	}
}

$('#result').hide();
$(document).ready(function(){
var button = $('#submit_edit_subreason');
var orig = [];

$.fn.getType = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("form[name='edit_subreason'] :input").each(function () {
	
    var type = $(this).getType();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
	//alert(JSON.stringify(tmp));
    orig[$(this).attr('id')] = tmp;
});

$('form[name="edit_subreason"]').bind('change keyup', function () {

    var disable = true;
    $("form[name='edit_subreason'] :input").each(function () {
        var type = $(this).getType();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (orig[id].value == $(this).val());
			
        } else if (type == 'radio') {
            disable = (orig[id].checked == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    button.prop('disabled', disable);
});
});
function form_validation_edit()
{
	var edit_subreason_name = $('input[name="edit_subreason_name"]').val().trim();
	var edit_reason_id = $('select[name="edit_reason_id"]').val().trim();
	var menu_sort_order = $('select[name="edit_menu_sort_order"]').val().trim();
	var view = $('input[name="edit_view"]').val();
	var common_subreason="";
			if($("#show_available_common_subreason").length!=0){
				var common_subreason=document.getElementById("show_available_common_subreason").value;
			}
	   var err = 0;
		if((edit_subreason_name=='') || (edit_reason_id=='') || (menu_sort_order=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		
		if(err==0)
			{
				$("#reason_id_reload").val(edit_reason_id);
		var form_status = $('<div class="form_status"></div>');		
		var form = $('#edit_subreason');
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();		
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/edit_subreason/"?>',
				type: 'POST',
				data: $('#edit_subreason').serialize()+"&common_subreason="+common_subreason,
				dataType: 'html',
				
			}).done(function(data)
			  {
				 // alert(data);
				form_status.html('')
				if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					edit_subreason_name_default=document.getElementById("edit_subreason_name_default").value;
					document.getElementById("edit_subreason_name").value=edit_subreason_name_default;
					document.getElementById("edit_subreason_name").focus();
					
				});
				  }
				if(data==1)
				{
					  
					swal({
						title:"Success", 
						text:"Subreason is successfully updated!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						//location.reload();
						$("#reload_form")[0].submit();

					});
				}
				if(data==0)
				{
					swal(
						'Oops...',
						'Error in form',
						'error'
					)	
				}
				
				
			});
}
}]);			
			}
	
	return false;
}

</script>
<form action="<?php echo base_url()?>admin/Catalogue/edit_subreasons_form" method="post" id="reload_form">
	<input type="hidden" name="reason_id" id="reason_id_reload" value="<?php echo $get_subreason_data["reason_id"];?>">
	<input type="hidden" name="sub_issue_id" id="sub_issue_id_reload" value="<?php echo $get_subreason_data["sub_issue_id"];?>">
</form>
</div>
</body>

</html>