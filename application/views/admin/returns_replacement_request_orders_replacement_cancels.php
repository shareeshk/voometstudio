<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Replacement Request Orders - Replacement Cancels</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<style>
.form-group{
	margin-bottom:5px;
	margin-top:0;
	padding-bottom:0;
}
</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   var table = $('#returns_replacement_request_orders_table_replacement_cancels').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Returns/returns_replacement_request_orders_replacement_cancels_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#returns_replacement_request_orders_table_replacement_cancels_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("replacement_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'40%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});	
$(document).ready(function(){
	var returns_refund_orders_list_checked=0;
	$("input[name='returns_refund_orders_list']").each(function(){
		if($(this).is(":checked")){
			returns_refund_orders_list_checked++;
		}
	})
	/*if(returns_refund_orders_list_checked==0){
		document.getElementById("refund_info_div").style.display="none";
	}*/
})


function sendReplacementOrderFun(order_item_id){
	document.getElementById("type_of_order_div_"+order_item_id).style.display="block";
}


function open_return_replacement_balance_clear_for_refund_fun(order_item_id){
	shipping_charge_applied_cancels_chk="no";
	if(document.getElementById("shipping_charge_applied_cancels_chk_"+order_item_id)){
		shipping_charge_applied_cancels_chk=document.getElementById("shipping_charge_applied_cancels_chk_"+order_item_id).checked;
		if(shipping_charge_applied_cancels_chk){
			shipping_charge_applied_cancels_chk="yes";
		}
		else{
			shipping_charge_applied_cancels_chk="no";
		}
	}
	return_shipping_concession_cancels_chk="no";
	if(document.getElementById("return_shipping_concession_cancels_chk_"+order_item_id)){
		return_shipping_concession_cancels_chk=document.getElementById("return_shipping_concession_cancels_chk_"+order_item_id).checked;
		if(return_shipping_concession_cancels_chk){
			return_shipping_concession_cancels_chk="yes";
		}
		else{
			return_shipping_concession_cancels_chk="no";
		}
	}
	return_shipping_concession=0;
	if(document.getElementById("return_shipping_concession_text_"+order_item_id) && return_shipping_concession_cancels_chk=="yes"){
		return_shipping_concession=document.getElementById("return_shipping_concession_text_"+order_item_id).value;
	}
		$.ajax({
			url:"<?php echo base_url()?>admin/Returns/return_replacement_balance_clear_for_refund",
			type:"POST",
			data:"order_item_id="+order_item_id+"&shipping_charge_applied_cancels_chk="+shipping_charge_applied_cancels_chk+"&return_shipping_concession_cancels_chk="+return_shipping_concession_cancels_chk+"&return_shipping_concession="+return_shipping_concession,
			success:function(data){
				if(data){
				swal({
					title:"success!", 
					text:"Refund Balance amount is initiated", 
					type: "success",
					allowOutsideClick: false
					
				}).then(function () {
					location.reload();

				});
			}else{
				swal("Error", "error", "error");
			}
				
			}
		})
}
function to_cal_total_balance_amount_fun(order_item_id){
	total_balance_amount_hidden_val=document.getElementById("total_balance_amount_hidden_"+order_item_id).value;
	if(document.getElementById("return_shipping_concession_cancels_chk_"+order_item_id).checked){
		if(document.getElementById("return_shipping_concession_cancels_chk_"+order_item_id).value==0){
			document.getElementById("return_shipping_concession_span_"+order_item_id).style.display="none";
			document.getElementById("return_shipping_concession_text_"+order_item_id).style.display="block";
		}
		
	}
	if(document.getElementById("shipping_charge_applied_cancels_chk_"+order_item_id).checked){
		shipping_charge_applied=document.getElementById("shipping_charge_applied_cancels_chk_"+order_item_id).value;
	}
	else{
		shipping_charge_applied=0;
	}
	if(document.getElementById("return_shipping_concession_cancels_chk_"+order_item_id).checked){
		return_shipping_concession=document.getElementById("return_shipping_concession_cancels_chk_"+order_item_id).value;
	}
	else{
		return_shipping_concession=0;
	}
	
	document.getElementById("total_balance_amount_"+order_item_id).innerHTML=parseFloat(total_balance_amount_hidden_val)+parseFloat(shipping_charge_applied)+parseFloat(return_shipping_concession);
}

</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="page-header"><h4 class="text-center">Replacement Request Orders - Replacement Cancels <span class="badge" id="replacement_count"></span></h4></div>
	<table id="returns_replacement_request_orders_table_replacement_cancels" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="text-primary small bold">Return Summary</th>
				<th class="text-primary small bold">Return Reason</th>
				<th class="text-primary small bold">Actions</th>
			</tr>
		</thead>
	</table>
</div>

<!---------------------->
<div class="modal" id="order_summary">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Order Summary</h4>
           </div>
           <div  class="modal-body" id="model_content">
		   <div class="container">
				
			</div>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close"class="btn">Close</a>
        </div>
     </div>
    </div>
  </div>
  
  <script type="text/javascript">
  $("#order_summary").modal("hide");
  function view_order_summary_(order_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/get_data_from_invoice_offers",
		type:"POST",
		data:"order_id="+order_id,
		success:function(data){
			
			$("#description").val("");	
			$("#model_content").html(data);
			$("#order_summary").modal("show");
			
		}
	})
	
}
function update_refund_method_as_wallet(order_item_id){

	if(order_item_id!=null && order_item_id!=''){
		$.ajax({
			url:"<?php echo base_url()?>admin/Returns/update_replacement_refund_type",
			type:"POST",
			data:"order_item_id="+order_item_id,
			success:function(data){
				
				if(data==true){
					alert('successfully updated');
					location.reload();
				}else{
					alert('Something went wrong...! Please check');
				}
				
			}
		});
	}
}
  </script>
  </div>
</body> 
</html>  