<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<style type="text/css">
@-webkit-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}
@-moz-keyframes opacity {
	0% { opacity: 1; }
	100% { opacity: 0; }
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}

.wizard-card .tab-content {
	 min-height: 400px;
	 padding:0px;
}
.formheader {
  width:40%;
  text-align: center;
  padding: 12px;
  font-size: 12px;
  text-transform: uppercase;
  -webkit-font-smoothing: subpixel-antialiased;
  background-color: #4caf50;
  border-radius: 4px;
  color: #FFFFFF;
  font-weight: 500;
  box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
}
.wizard-card .wizard-header{
	padding-bottom:15px;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js"></script>

<script>

function show_view_validatn_create(){
	menu_sort_order=document.getElementById("menu_sort_order").value;
	if(menu_sort_order==0){
	document.getElementById("view1").disabled = true;
	document.getElementById("view2").checked = true;					
	}
	if(menu_sort_order!=0){
		document.getElementById("view1").disabled = false;
		document.getElementById("view1").checked = true;				
	}
}
</script>
<script type="text/javascript">
       $(document).ready(function () {
          
		   $.ajax({
			   url:"<?php echo base_url()?>admin/Catalogue/parent_category_count",
			   type:"POST",
			   success:function(data){
				   $("#parent_category_count").text(data);
				   
			   }
		   })
       });

</script>


<script type="text/javascript">
function show_sort_order(obj){
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/show_sort_order",
		type:"post",
		success:function(data){
			$("#menu_sort_order").html(data);
		}
	});
}

function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}


function multilpe_delete_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
	
	swal({
			title: 'Are you sure?',
			text: "Archived Parentcategory(not entire tree)",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Archive it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
			
		$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/delete_parent_category_selected",
		type:"post",
		data:"selected_list="+selected_list,
		
		success:function(data){
			
			if(data==true){
				swal({
					title:"Archived!", 
					text:"Given "+table+"(s) has been archived successfully", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
		});
			
			})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});	
}


function multilpe_delete_entire_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
   swal({
			title: 'Are you sure?',
			text: "Archived Parentcategory(entire tree)",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Archive it!',
			showLoaderOnConfirm: true,
			allowOutsideClick:false,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
			
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/update_parent_category_selected",
		type:"post",
		data:"selected_list="+selected_list+"&active=0",
		
		success:function(data){
			if(data==true){
				swal({
						title:"Archived!", 
						text:"Given "+table+"(s) has been archived successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	
			})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
	
}
function multilpe_delete_no_data_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
	swal({
			title: 'Are you sure?',
			text: "Delete Parentcategory",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
			
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/delete_parent_category_no_data_selected",
		type:"post",
		data:"selected_list="+selected_list,
		
		success:function(data){
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Given "+table+"(s) has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else if(data==false){
				swal("Error", "Error in deleting this file", "error");
			}else if(data==2){
				swal("Error", "Oops ..! You can't delete it.", "error");
			}
			//location.reload();
			
		}
	});
	
			})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
	
}
</script>
<script>
function showDivCreate(){
	
	document.getElementById('viewDiv1').style.display = "none";
	document.getElementById('createDiv2').style.display = "block";
	show_sort_order();
}
function showDivEdit(){
	
	document.getElementById('viewDiv1').style.display = "block";
	document.getElementById('createDiv2').style.display = "none";
	
}
</script>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container containerpage">		
<div class="row" id="viewDiv1">
	<div class="wizard-header text-center">
		<h5> Number of Parent category <span class="badge"><div id="parent_category_count"></div></span> 
		</h5>                        		
		                       	
	</div>
<table id="myTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
			<tr>
				<th colspan="6">
					<button id="multiple_delete_button1" class="btn btn-primary btn-xs" onclick="multilpe_delete_fun('parent_category')">Archive</button>
					<button id="multiple_delete_button2" class="btn btn-warning btn-xs" onclick="multilpe_delete_entire_fun('parent_category')">Archive all</button>
					<button id="multiple_delete_button3" class="btn btn-danger btn-xs" onclick="multilpe_delete_no_data_fun('parent_category')">Delete</button>
					<button id="multiple_delete_button4" class="btn btn-info btn-xs" onclick="showDivCreate()">Go to Create</button>
				</th>
			</tr>
			<tr>
				<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
				<th>S. No</th>
				<th>Name </th>
				<th>Details</th>
				<th>Last Updated</th>
				<th>Action</th>
			</tr>
</thead>
<tbody>
<?php
$i=0;
foreach ($parent_catagories as $parent_catagories_value) { 
$i++; ?>
<tr>
	<td><input type="checkbox" name="selected_list" value="<?php echo $parent_catagories_value->pcat_id; ?>" /></td>
	<td><?php echo $i; ?></td>
	<td><?php echo $parent_catagories_value -> pcat_name; ?></td>
	<td>
	<?php 
	echo "Menu Level :". $parent_catagories_value -> menu_level.'<br>';
	echo "Show Sub Menu :". $parent_catagories_value -> show_sub_menu.'<br>';
	echo "Sort Order :".  $parent_catagories_value -> menu_sort_order.'<br>';
	echo "View :".  $parent_catagories_value -> view.'<br>'; 
	echo "Show in Combo Pack :".  ucwords($parent_catagories_value ->combo_pack_status); 
	 ?>
	</td>
	<td><?php echo $parent_catagories_value -> timestamp?></td>

	<td>
	
	<form action="<?php echo base_url()?>admin/Catalogue/edit_parent_category_form" method="post"><input type="hidden" value="<?php echo $parent_catagories_value -> pcat_id;?>" name="pcat_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit"></form>
	</td>

</tr>
<?php } ?>
</tbody>
</table>
</div>

<div class="row" id="createDiv2" style="display:none;">
<div class="col-md-6 col-md-offset-3">
<div class="wizard-container">
<div class="card wizard-card" data-color="green" id="wizardProfile">
 	
		<form name="create_parent_category" id="create_parent_category" method="post" action="#" onsubmit="return form_validation();" enctype="multipart/form-data">
		
		<script type="text/javascript">
			var $validator = $('.wizard-card form').validate({
			rules: {
				pcat_name: {
					required: true,
				},
				menu_sort_order: {
					required: true,
				},
				show_sub_menu: {
					required: true,
				}
				
			},

			errorPlacement: function(error, element) {
				$(element).parent('div').addClass('has-error');
			}
			});
		</script>
			<div class="wizard-header">
			<div align="center">
				<h3 class="wizard-title formheader">Create Parent category</h3>
			</div>
			</div>
		
		<div class="tab-content">
				
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter Parent Category Name</label>
					<input id="pcat_name" name="pcat_name" type="text" class="form-control"/>
				</div>
				</div>
			</div>
			
					<input type="hidden" name="order_item_id" value="1">
					<input type="hidden" name="menu_level"  value="1">
					
			<div class="row">
				<div class="col-md-10 col-md-offset-1">                               
				<div class="form-group label-floating">
					<label class="control-label">-Show Sub Menu-</label>
					<select name="show_sub_menu" id="show_sub_menu" class="form-control search_select">
						<option value="" selected></option>
						<option value="1">Show</option>
						<option value="0">Hide</option>
			
					</select>
				</div>
				</div>
			</div>			
					
			<div class="row">
				<div class="col-md-10 col-md-offset-1">  
				<div class="form-group label-floating">
					<label class="control-label">Enter the Sort Order</label>
					<select name="menu_sort_order" id="menu_sort_order" class="form-control" onchange="show_view_validatn_create()">
						
					</select>
						
				</div>
				</div>
			</div>
									
			<div class="row">
				<div class="col-md-10"> 
				
					<div class="form-group">
					
					<div class="col-md-5 col-md-offset-1">Show Parent Category</div>
					<div class="col-md-3">
						<label class="radio-inline"><input type="radio" name="view" id="view1" value="1" checked="checked"/>View</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline"><input type="radio" name="view" id="view2" value="0"/>Hide</label>
					</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10"> 
				
					<div class="form-group">
					
					<div class="col-md-5 col-md-offset-1">Show In Combo Pack</div>
					<div class="col-md-3">
						<label class="radio-inline" for="combo_pack_status1"><input type="radio" name="combo_pack_status" id="combo_pack_status1" value="yes" checked="checked"/>Yes</label>
					</div>
					<div class="col-md-3">
						<label class="radio-inline" for="combo_pack_status2"><input type="radio" name="combo_pack_status" id="combo_pack_status2" value="no"/>No</label>
					</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-8 col-md-offset-3"> 
					<div class="form-group">
					<button class="btn btn-info btn-sm" type="submit" id="submit_create_parent_category">Submit</button>
					<button class="btn btn-danger btn-sm" id="reset_create_parent_category" type="reset">Reset</button>
					<button class="btn btn-primary btn-sm" type="button" onclick="showDivEdit()">Go to View</button>
					</div>
				</div>
			</div>
		</div>
		
		</form>
				
</div>
</div> <!-- wizard container -->
</div>
</div>				
<div class="footer">
</div>
</div>
</div>
</body>
</html>

<script type="text/javascript">

$(document).ready(function(){
    $('#myTable').dataTable({ stateSave: true });
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
});

var flag=1;
function check_duplication_name(obj){
	var pcat_name = obj.value; 
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/check_duplication_name",
		type:"post",
		data:"pcat_name="+pcat_name,
		success:function(data){
			if(data=="yes"){
					flag=0;
			}
			if(data=="no"){
				flag=1;
			}
		}
	});
				
 }

function form_validation(){
	var pcat_name = $('input[name="pcat_name"]').val().trim();
	var menu_level = $('input[name="menu_level"]').val().trim();
	var show_sub_menu = $('select[name="show_sub_menu"]').val().trim();
	var menu_sort_order = $('select[name="menu_sort_order"]').val().trim();
	   var err = 0;
		if((pcat_name=='') || (show_sub_menu=='') || (menu_sort_order=='')){	   
		    err = 1;
		}else{			
			err = 0;
		}
		if(err==0){
		
			var form_status = $('<div class="form_status"></div>');		
			var form = $('#create_parent_category');	

				
			/////////////////////////
			
			swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
    //swal.showLoading();
    $.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_parent_category/"?>',
				type: 'POST',
				data: $('#create_parent_category').serialize(),
				dataType: 'html',

			}).done(function(data){	
				  form_status.html('')
				  if(data=="exist"){
					  swal({
					title:"Error", 
					text:"That name is already created. Try another.", 
					type: "error",
					allowOutsideClick: false
				}).then(function(){
					document.getElementById("pcat_name").value="";
					document.getElementById("pcat_name").focus();
				});
				  }
			if(data=="yes")
			{
				swal({
					title:"Success", 
					text:"Parent Category is successfully added!", 
					type: "success",
					allowOutsideClick: false
				}).then(function () {
					location.reload();

				});
			}
			if(data=="no")
			{
				swal(
					'Oops...',
					'Error in form',
					'error'
				)
			}
				
			});
  }
}]);
			
			////////////////////////
			
			}
	
	return false;
}

</script>

