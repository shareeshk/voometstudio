<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>Replacement Request Orders - Admin Rejected</title>   
<meta name="description" content="Orders">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" /> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<style>
.form-group{
	margin-bottom:5px;
	margin-top:0;
	padding-bottom:0;
}
</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   var table = $('#returns_replacement_request_orders_table_admin_rejected').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Returns/returns_replacement_request_orders_admin_rejected_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#returns_replacement_request_orders_table_admin_rejected_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				document.getElementById("replacement_count").innerHTML=json.recordsFiltered;
                return json.data;
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'40%',
         'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
});	
$(document).ready(function(){
	var returns_refund_orders_list_checked=0;
	$("input[name='returns_refund_orders_list']").each(function(){
		if($(this).is(":checked")){
			returns_refund_orders_list_checked++;
		}
	})
	/*if(returns_refund_orders_list_checked==0){
		document.getElementById("refund_info_div").style.display="none";
	}*/
});


function open_return_refund_accept_fun(order_item_id,return_order_id){
	
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/returns_replacement_request_orders_admin_rejected_accept",
		type:"POST",
		data:"order_item_id="+order_item_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			//alert(data);
			$("#return_replacement_accept_form_rep").html(data);
			$("#return_replacement_accept_modal_rep").modal("show");
		}
	})
	
	
}
function open_return_refund_reject_fun(order_item_id,return_order_id){
	
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/returns_replacement_request_orders_admin_rejected_reject",
		type:"POST",
		data:"order_item_id="+order_item_id,
		beforeSend:function(){
			//$("#loader").html("<b>Processing.....</b>");
		},
		success:function(data){
			//alert(data);
			$("#return_replacement_reject_form_rep").html(data);
			$("#return_replacement_reject_modal_rep").modal("show");
		}
	})
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
        return false;
    }
    return true;
}
</script>
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="page-header"><h4 class="text-center">Replacement Request Orders - Admin Rejected <span class="badge" id="replacement_count"></span></h4></div>
	<table id="returns_replacement_request_orders_table_admin_rejected" class="table table-bordered table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="text-primary small bold">Return Summary</th>
				<th class="text-primary small bold">Return Reason</th>
				<th class="text-primary small bold">Actions</th>
			</tr>
		</thead>
	</table>
</div>

<!------------------------------------------->
<!-- accept model things starts ----------->
<script>
$(document).ready(function(){
	$("#return_replacement_accept_form_rep").on('submit',(function(e) {
		var update_stock=$("input[name=update_stock]").is(':checked');
		if(update_stock==false){
			conf=confirm("Don't you want to update the stock of Replaced inventory ?");
			if(conf==false){
				return false;
			}
		}
		var sms=$("input[name=sms]").is(':checked');
		var email=$("input[name=email]").is(':checked');
		
		if(sms==true || email==true){
			swal.showLoading();
		}
		e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>admin/Returns/update_return_replacement_accept",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					if(data){
							$('#return_replacement_accept_modal_rep').modal('hide');
						swal({
							title:"success!", 
							text:"Successfully Updated", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#return_replacement_accept_form_rep").trigger("reset");
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
					
				}	        
		   });
	}));
	
	
	/*$("#return_replacement_accept_modal_rep").on('hidden.bs.modal', function () {
		$("#return_replacement_accept_form_rep").trigger("reset");	
		location.reload();
    });*/
});
function divFunction_order_rep(){
    $("#return_replacement_accept_form_rep").trigger("reset");	
		location.reload();
}
</script>



<!--- modal accept starts----------------->

<div class="modal" id="return_replacement_accept_modal_rep" data-backdrop="static" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		 <script>
			function update_balanceFun(order_item_id,obj){
				quantity_need_to_be_replace=obj.value;
				balance_amount_each=document.getElementById("balance_amount_each_"+order_item_id).value;
				document.getElementById("balance_amount_div_"+order_item_id).innerHTML=quantity_need_to_be_replace*balance_amount_each;
				document.getElementById("balance_amount_"+order_item_id).value=quantity_need_to_be_replace*balance_amount_each;
			}	
		 </script>
		 <div class="panel panel-success">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                  Accept Replacement Request
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right" onClick="divFunction_order_rep()"></span></span>
               </div>
               <div aria-expanded="true">
                  <div class="panel-body">
		 <form id="return_replacement_accept_form_rep" method="post" enctype="multipart/form-data">
            
			</form>
			</div>

               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!--- modal accept ends----------------->
<!-- accept model things ends ----------->

<!--- reject model things starts --------------->
<script>

$(document).ready(function(){
	
	
	$("#return_replacement_reject_form_rep").on('submit',(function(e) {
		var update_stock=$("input[name=update_stock]").is(':checked');
		if(update_stock==false){
			conf=confirm("Don't you want to update the stock of Replaced inventory ?");
			if(conf==false){
				return false;
			}
		}
		var sms=$("input[name=sms]").is(':checked');
		var email=$("input[name=email]").is(':checked');
		
		if(sms==true || email==true){		
			swal({
				html: '<h4>Processing...</h4>',
			});	
			swal.showLoading();
		}
		e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>admin/Returns/return_replacement_reject",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					if(data){
						$('#return_replacement_reject_modal_rep').modal('hide');
						swal({
							title:"success!", 
							text:"Successfully Updated", 
							type: "success",
							allowOutsideClick: false
							
						}).then(function () {
							$("#return_replacement_reject_form_rep").trigger("reset");
							location.reload();

						});
					}else{
						swal("Error", "not sent", "error");
					}
					
				}	        
		   });
	}));
	
	
	
	/*$("#return_replacement_reject_modal_rep").on('hidden.bs.modal', function () {
		$("#return_replacement_reject_form_rep").trigger("reset");	
		location.reload();
    });*/
	
});
function divFunction_order_rep_reject(){
    $("#return_replacement_reject_form_rep").trigger("reset");	
		location.reload();
}
</script>



<!--- modal reject starts----------------->

<div class="modal" id="return_replacement_reject_modal_rep" data-backdrop="static" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		<div class="panel panel-danger">
               <div id="accordion" class="panel-heading" style="cursor:pointer;">
                   Reject Replacement Request
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right" onClick="divFunction_order_rep_reject()"></span></span>
               </div>
               <div aria-expanded="true" class="">
                  <div class="panel-body">
		 <form id="return_replacement_reject_form_rep" method="post" enctype="multipart/form-data">
           
			</form>
			</div>

               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!--- modal reject ends----------------->

<!--- reject model things ends =------------------------>
	
<!---------------------->
<div class="modal" id="order_summary">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Order Summary</h4>
           </div>
           <div  class="modal-body" id="model_content">
		   <div class="container">
				
			</div>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close"class="btn">Close</a>
        </div>
     </div>
    </div>
  </div>
  
  <script type="text/javascript">
  $("#order_summary").modal("hide");
  function view_order_summary_(order_id){
	$.ajax({
		url:"<?php echo base_url()?>admin/Returns/get_data_from_invoice_offers",
		type:"POST",
		data:"order_id="+order_id,
		success:function(data){
			
			$("#description").val("");	
			$("#model_content").html(data);
			$("#order_summary").modal("show");
			
		}
	})
	
}
  </script>
  </div>
</body> 
</html>  