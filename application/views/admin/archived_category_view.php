<html lang="en">
<head>
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />

<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">
var table;
$(document).ready(function (){

	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
	
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    var rows_selected = [];
     table = $('#table_category').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Catalogue/category_processing",
			data: function (d) { d.pcat_id = $('#pcat_id').val();d.active=0},
            type: "post",
            error: function(){
              $("#table_category_processing").css("display","none");
            },
			"dataSrc": function ( json ) {
				
				document.getElementById("archived_category_count").innerHTML=json.recordsFiltered;
				
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });
   
	$('#reset_form_button').on('click',function(){
		table.draw();
	});
	
	$("#submit_button").click(function() {
		pcat_id=$('#pcat_id').val();
		table.draw();
	});

});	
function drawtable(obj){
	arr = obj.value.split('-');
	status=arr[1];
	$('#active').val(status);
	table.draw();
}
function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function multilpe_restore_fun(table_name){
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table_name+"!");
		return false;
	}
	selected_list=selected_list_arr.join(",");
	swal({
			title: 'Are you sure?',
			text: "Restore Archived category",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, restore it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	active=$('#active').val();
	if(active==1){
		$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/update_category_selected",
		type:"post",
		data:"selected_list="+selected_list+"&active=1",
		
		success:function(data){
			if(data==true){
				swal({
						title:"Restored!", 
						text:"Given "+table_name+"(s) has been restored successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
	});
	}else{
		swal(
			'Sorry',
			'You cant restore it. Try to restore the corresponding (Main) Category.',
			'error'
		)
		//alert('Sorry, You cant restore it. Try to restore the corresponding (Main) Category');
		var obj=document.getElementsByName("common_checkbox");	
		var obj_arr=document.getElementsByName("selected_list");
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
		for(i=0;i<obj.length;i++){
			obj[i].checked=false;
		}
	}
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
</script>

<?php //print_r($categories);?>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">

	<div class="wizard-header text-center">
		<h5> Number of Category <span class="badge"><div id="archived_category_count"></div></span> 
		</h5>                        		
		                       	
	</div>
	<input value="<?php echo $pcat_id; ?>" type="hidden" id="pcat_id">
	<input value="<?php echo $active; ?>" type="hidden" name="active" id="active">
	<div class="row" id="viewDiv1">
		<?php /*
		<form name="search_for_category" class="col-md-12">
		
			<div class="col-md-4 text-right">
			Search By Parent Category:
			</div>
			<div class="col-md-4">
				<select name="pcat_id" id="pcat_id" class="form-control search_select" onchange="drawtable(this)">
							<option value="" selected>-Select Parent Category-</option>
							<?php foreach ($parent_category as $parent_category_value) { 
							$str=($parent_category_value->active==1)?" - active":" - Inactive";
							?>
							<option value="<?php echo $parent_category_value->pcat_id.'-'.$parent_category_value->active; ?>"><?php echo $parent_category_value->pcat_name.$str ?></option>
							<?php } ?>
							<option value="0-1">--No Parent Category--</option>
				</select>
			</div>
			<div class="col-md-4">
				<button type="button" class="btn btn-info" id="submit_button">Submit</button>
				<button class="btn btn-warning" type="reset" id="reset_form_button">Reset</button>
			</div>
		</form>
		*/ ?>
	<table id="table_category" class="table table-bordered table-striped" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th colspan="5">
				<div class="padding_right"><button id="multiple_delete_button" class="btn btn-success btn-xs" onclick="multilpe_restore_fun('category')">Restore</button></div>
			</th>
		</tr>
		<tr>
			<th><input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)"></th>
			<th>Category Name </th>
			<th>Parent Category</th>
			<th>Details</th>
			<th>Last Updated</th>
		</tr>
	</thead>

	</table>
	</div>
</div>
</div>
</body>
</html>