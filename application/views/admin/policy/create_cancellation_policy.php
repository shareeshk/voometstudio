<!DOCTYPE html>
<html lang="en">
<head>
  <title>Cancellation Policy</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
  <script  type="text/javascript" src="<?php echo base_url();?>assets/js/material.min.js" ></script>
  <script  type="text/javascript" src="<?php echo base_url();?>assets/js/material-kit.js" ></script>  
  <script src="<?php echo base_url()?>assets/jqueryui/jquery-ui.min.js"></script>
  <style>
.popover{
   border:none;
   border-radius:unset;
   
   width:100%;

   overflow-wrap:break-word;
}
.btn{
    border-radius:unset;
}

.btn-round-lg{
border-radius: 22.5px;
}
.btn-round{
border-radius: 17px;
}
.btn-round-sm{
border-radius: 15px;
}
.btn-round-xs{
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
}

.popover-content {
   height: 200px;
   overflow-y: scroll;
}
.table {
    font-size:.9em;
    margin-bottom: 0px;
}


/* Tooltip styling */

[data-tooltip]:after,
[data-tooltip]:before {
    content: '';
    display: none;
    font-size: .75em;
    position: absolute;
}
[data-tooltip]:after {
    border-top: .5em solid #222;
    border-top: .5em solid hsla(0,0%,0%,.9);
    border-left: .5em solid transparent;
    border-right: .5em solid transparent;
    height: 0;
    left: 40%;
    width: 0;
    z-index: 2000;
}
[data-tooltip]:before {
    background-color: #222;
    background-color: hsla(0,0%,0%,.9);
    border-radius: .25em;
    color: #f6f6f6;
    content: attr(data-tooltip);
    font-family: sans-serif;
    left: 0;
    padding: .25em .75em;
    white-space: nowrap;
}
[data-tooltip]:hover:after,
[data-tooltip]:hover:before {
    display: block;
}
[data-tooltip]:hover:after {
    top: -.5em;
}
[data-tooltip]:hover:before {
    top: -2.5em;
}


h2{
    margin-top: 0px;
    margin-bottom: 0px;
}
.page-header{
    padding-bottom: 10px; 
    margin: 0px; 
    border-bottom: 0px solid #eee; 
}
    ul.listack > li {
    display: inline-block;
    /* You can also add some margins here to make it look prettier */
    zoom:1;
    display:inline;
    /* this fix is needed for IE7- */
   margin-right: 5%;
}

.material-button {
    position: relative;
    top: 0;
    z-index: 1;
    width: 30px;
    height: 30px;
    color: #fff;
    border: none;
    border-radius: 70%;
    box-shadow: 0 3px 6px rgba(0,0,0,.275);
    outline: none;
}
.btn-xsm{
  width: 25px;
  height: 25px;
  border-radius: 60%;  
}
.list-left li, .list-right li {
    cursor: pointer;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #000;
    }
.nav-tabs>li>a {
    color: #000;
}
.nav-tabs {
    border-bottom: 1px solid #cccccc;
}

.panel-heading .accordion-toggle:after {
    /* symbol for "opening" panels */
    font-family: 'Glyphicons Halflings';  /* essential for enabling glyphicon */
    content: "\e114";    /* adjust as needed, taken from bootstrap.css */
    float: left;        /* adjust as needed */
    color: grey;         /* adjust as needed */
}
.panel-heading .accordion-toggle.collapsed:after {
    /* symbol for "collapsed" panels */
    content: "\e080";    /* adjust as needed, taken from bootstrap.css */
}
.inline_input{
 display: inline-block;
 width:50%!important   
}
.list-group{
	height:300px;
	overflow:auto;
}
</style> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">
<div class="page-header">
  <h4 class="text-danger text-center">Add and Remove Cancellation Policy</h4>
  </div>
  <form id="form_for_policy_add"  class="form-horizontal" onsubmit="return check_before_submit()" action="<?php echo base_url()?>admin/Policys/add_cancellation_restrictions" method="post">
	<h4> Step 1 : Choose an option</h4> 
      <div class="form-group">
	  <div class="col-md-12">
        <div class="radio">
          <label><input type="radio" required name="cancellation_restrict" onchange="update_display()" value="new_day_limit">Define Policy</label>
		  </div>
		  </div>
		  </div>
		<div class="form-group"> 
		
		<div class="col-md-3">
			<select id="select_order_status_type" name="select_order_status_type"  class="form-control" style="display: none">
					<option value="">Select</option>
					<option value="approved">Approved</option>
					<option value="confirmed">Confirmed</option>
					<option value="packed">Packed</option>
			</select>
		</div> 
		
	
		<div class="col-md-3">
        <select id="select_hour_day_type"  class="form-control" onchange="show_related_input_fields()" style="display: none">
                <option selected value="">Select</option>
                <option value="Hours">Hours</option>
                <option value="Days">Days</option>
 
        </select>
    </div> 
    <div class="col-md-3"> <input type="number"  min="1" max="23" class="form-control" onkeyup="validation_for_hr(this)" name="new_hour_limit" id="new_hours_for_cancellation" placeholder=" Number of hours" style="display: none"/>
        <span class="help-block" id="error_for_hr"></span>
    </div> 
    <div class="col-md-3"> <input type="number"  min="1" max="60" class="form-control" onkeyup="validation_for_day(this)" name="new_days_for_cancellation" id="new_days_for_cancellation" placeholder=" Number of days" style="display: none"/>
        <span class="help-block" id="error_for_days"></span>
    </div>        
              
</div> 
    
        <div class="form-group"> 
		<div class="col-md-12">
        <div class="radio">
          <label><input type="radio" required name="cancellation_restrict" onchange="update_display()" value="remove_cancellation">Remove 'Cancellation Policy'</label>
        </div>
      </div>
	  </div>
	   <div class="form-group" id="message_container" style="display: none;margin-bottom: 0px">
	   <div class="col-md-12">
          <label for="message">Message at UI</label>
          <textarea class="form-control" onkeyup="textCounter(this,'counter',150);" id="message" rows="3" name="message" maxlength="150"></textarea>
          <span class="help-block" id="counter">150 character(s) left</span>
        </div>
		</div>
      <script>
        function validation_for_hr(obj){
            var hr_value=obj.value;
            if(hr_value<1 || hr_value>23){
                obj.setAttribute('style','border: 1px solid #FF0000;')
                document.getElementById('error_for_hr').innerHTML='<small style="color:red"> Hour Values Must Be Between 1 to 23 </small>'
            }
            else{
                obj.setAttribute('style','border: 1px solid #009900;')
                document.getElementById('error_for_hr').innerHTML=""
            }
        }
        function validation_for_day(obj){
            var hr_value=obj.value;
            if(hr_value<1 || hr_value>90){
                obj.setAttribute('style','border: 1px solid #FF0000;')
                document.getElementById('error_for_days').innerHTML='<small style="color:red"> Days Values Must Be Between 1 to 90 </small>'
            }
            else{
                obj.setAttribute('style','border: 1px solid #009900;')
                document.getElementById('error_for_days').innerHTML=""
            }
        }
          function show_related_input_fields(){
              var selected_values=document.getElementById('select_hour_day_type').value
			  if(selected_values==""){
				  document.getElementById('error_for_hr').style.display="none"
					document.getElementById('error_for_hr').innerHTML=""
					document.getElementById('error_for_days').style.display="none"
					document.getElementById('new_days_for_cancellation').style.display="none"
					document.getElementById('new_days_for_cancellation').required=false;
					document.getElementById('new_days_for_cancellation').value=""
					document.getElementById('new_hours_for_cancellation').value=""
					document.getElementById('new_hours_for_cancellation').style.display="none"
					document.getElementById('new_hours_for_cancellation').required=false;
			  }
              if(selected_values=="Hours"){
                document.getElementById('error_for_hr').style.display=""
                document.getElementById('error_for_days').style.display="none"
                document.getElementById('error_for_days').innerHTML=""
                document.getElementById('new_hours_for_cancellation').style.display=""
                document.getElementById('new_hours_for_cancellation').required=true;
				document.getElementById('new_hours_for_cancellation').value=""
                document.getElementById('new_days_for_cancellation').value=""
                document.getElementById('new_days_for_cancellation').style.display="none"
                document.getElementById('new_days_for_cancellation').required=false;
              }
              if(selected_values=="Days"){
                document.getElementById('error_for_hr').style.display="none"
                document.getElementById('error_for_hr').innerHTML=""
                document.getElementById('error_for_days').style.display=""
                document.getElementById('new_days_for_cancellation').style.display=""
                document.getElementById('new_days_for_cancellation').required=true;
				document.getElementById('new_days_for_cancellation').value=""
                document.getElementById('new_hours_for_cancellation').value=""
                document.getElementById('new_hours_for_cancellation').style.display="none"
                document.getElementById('new_hours_for_cancellation').required=false;
              }
          }
          function update_display(){
              var radio_elems=document.getElementsByName('cancellation_restrict')
              for(var i=0;i<radio_elems.length;i++){
                    if(radio_elems[i].checked&&radio_elems[i].value=="new_day_limit"){
						document.getElementById('select_order_status_type').style.display=""
                        document.getElementById('select_hour_day_type').style.display=""
                        document.getElementById('message_container').style.display="none"
                       // document.getElementById('select_hour_day_type').required=true;
                        document.getElementById('message').required=false;
                        document.getElementById('message').value="";
                        document.getElementById('counter').innerHTML="150 character(s) left"
                    }
                    if(radio_elems[i].checked&&radio_elems[i].value=="remove_cancellation"){
						document.getElementById('select_order_status_type').style.display="none"
                        document.getElementById('select_hour_day_type').style.display="none"
                        document.getElementById('new_hours_for_cancellation').style.display="none"
                        document.getElementById('new_days_for_cancellation').style.display="none"
                        document.getElementById('new_days_for_cancellation').value=""
                        document.getElementById('new_hours_for_cancellation').value=""
                        document.getElementById('new_hours_for_cancellation').required=false;
                        document.getElementById('new_hours_for_cancellation').required=false;
                       // document.getElementById('select_hour_day_type').required=false;
                        document.getElementById('message_container').style.display=""
                        document.getElementById('message').required=true;
                    }
              }
          }
      </script>
      
		<h4> Step 2 : Select the SKUs</h4>
         <div class="form-group">
            <div class="col-md-12">
                <ul class="listack" id="item_picker_del">
                <li style="float: left"> All SKU's In Store <button type="button" class="material-button btn-info"><i class="fa fa-check" aria-hidden="true"></i></button></li>
                </ul>
            </div>
         </div>
          <div class="form-group">
                <div class="col-md-5">
                      <select class="form-control" name="parent_category" id="parent_category_selector" onchange="get_category()">
                        <option value="">Choose Parent Category</option>
                        <?php foreach($parent_category as $catdata){?>
                            <option value="<?php echo $catdata['pcat_id'] ?>"><?php echo $catdata['pcat_name'] ?></option>
                        <?php } ?>
						<option value="0">-None-</option>
                      </select>
                </div>
                <div class="col-md-5" id="parentCategorySubmit">
                    <button type="submit" class="btn btn-info">Apply to  All SKU's In Store</button>
                </div>
           </div>
           <div class="form-group">
               <div id="CategoryContainer">
                   
               </div>
           </div>
           <div class="form-group">
               <div id="subCategoryContainer">
    
               </div>
           </div>
           <div class="form-group">
              <div id="brandContainer">

              </div>
           </div>
           <div class="form-group">
                <div id="attributeContainer">

                </div>
           </div>
           <div class="form-group">
               <div id="productContainer">
    
               </div>
           </div>
           <div class="form-group">
               <div id="productTypeContainer">
    
               </div>
           </div>
           <div class="form-group">
                <b class="lead pull-right" id="collection_of_all_sku_to_buy"></b>
             </div>

            <div class="row" id="product_filter_level_selector_" style="display:none">
                <div class="dual-list list-right col-md-6">
                    <div class="well">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="text" id="right_list_search" name="SearchDualList" class="form-control" placeholder="search"> 
                                    <span class="input-group-addon glyphicon glyphicon-unchecked selector" style="cursor: pointer; top: 0px;" title="Select All"></span>
                                    <span class="input-group-addon glyphicon glyphicon-plus move-left" style="cursor: pointer; top: 0px;" title="Add Selected"></span>
                                </div>
                            </div>
                        </div>
                        <ul class="list-group" id="dual-list-right">
                            

                        </ul>
                    </div>
                </div>
        
        
                <div class="dual-list list-left col-md-6">
                    <div class="well text-right">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="text" id="left_list_search" name="SearchDualList" class="form-control" placeholder="search"> 
                                    <span class="input-group-addon glyphicon glyphicon-unchecked selector" style="cursor: pointer; top: 0px;" title="Select All"></span>
                                    <span class="input-group-addon glyphicon glyphicon-minus move-right" style="cursor: pointer; top: 0px;" title="Remove Selected"></span>
                                </div>
                            </div>
                        </div>
                        
                        <ul class="list-group" id="dual-list-left"></ul>
                    </div>
                </div>
                
            </div>
    <!--<input type="hidden" name="products_tagged_for_purchasing" id="products_tagged_for_purchasing"/>
    <input type="hidden" name="name_of_products_tagged_for_purchasing" id="name_of_products_tagged_for_purchasing"/>
    <input type="hidden" name="policy_type_id" value="<?php echo $policy_type ?>"/>-->

    <input type="hidden" name="products_tagged_for_purchasing" id="products_tagged_for_purchasing"/>
    <input type="hidden" name="override_previous_policy" id="override_previous_policy" value="0"/>
    <input type="hidden" name="policy_type_name" id="policy_type_name" value="Cancellation"/>
    <input type="hidden" name="name_of_products_tagged_for_purchasing" id="name_of_products_tagged_for_purchasing"/>
    <input type="hidden" name="policy_type_id" id="policy_type_id" value="<?php echo $policy_type ?>"/>
    
  </form>
  
  <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Message</h4>
      </div>
      <div class="modal-body" id="already_policy_display">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="continueFun()">Continue With Overide</button>
        <button type="button" class="btn btn-warning" data-dismiss="modal" onclick="resonsiderFun()">Reconsider</button>
      </div>
    </div>

  </div>
</div>
</div>
<script>

$(document).ready(function(){
    $("#myModal").on("hidden.bs.modal", function () {
        resonsiderFun()
    });
})

function resonsiderFun(){
    document.getElementById('override_previous_policy').value="0";
}
function continueFun(){
    document.getElementById("form_for_policy_add").submit();
}

document.getElementById('name_of_products_tagged_for_purchasing').value="Apply to  All SKU's In Store";
function textCounter(field,field2,maxlimit)
{
 var countfield = document.getElementById(field2);
 if ( field.value.length > maxlimit ) {
  field.value = field.value.substring( 0, maxlimit );
  return false;
 } else {
  countfield.innerHTML = maxlimit - field.value.length +' character(s) left';
 }
}

    function check_before_submit(){
        var policy_type_id=document.getElementById('policy_type_id').value
        var policy_type_name=document.getElementById('policy_type_name').value
        var allselectElements=document.getElementsByTagName('select');
        var i;var selected_attr,selected_level;
        for(i=allselectElements.length-1;i;i--){
            if(allselectElements[i].options[allselectElements[i].selectedIndex].value){
                selected_attr=allselectElements[i].options[allselectElements[i].selectedIndex].value
                selected_level=allselectElements[i].getAttribute('name');
                break;
            }
            else{
                selected_attr=0;
                selected_level='store_level';
            } 
        }
        
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {
                                           
                    //obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                           if(xhr.responseText!=0){
                               document.getElementById('already_policy_display').innerHTML="<h4>Policy Already Exists at selected hirarchy of store level</h4>If You Continue Privious Policy Gets Over Written."
                               $('#myModal').modal('show');
                               document.getElementById('override_previous_policy').value=xhr.responseText;
                           }
                           else{
                               continueFun()
                           }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Policys/getExisting_policy", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('selected_attr='+selected_attr+"&selected_level="+selected_level+"&policy_type_id="+policy_type_id+"&policy_type_name="+policy_type_name);
                }
          return false;      
    }

</script>
<script>
document.getElementById('name_of_products_tagged_for_purchasing').value="Apply to  All SKU's In Store";
function textCounter(field,field2,maxlimit)
{
 var countfield = document.getElementById(field2);
 if ( field.value.length > maxlimit ) {
  field.value = field.value.substring( 0, maxlimit );
  return false;
 } else {
  countfield.innerHTML = maxlimit - field.value.length +' character(s) left';
 }
}
</script>
<script>
    function update_buy_promo_list(){
    if(document.getElementById('dual-list-left').hasChildNodes()){
        document.getElementById('AttributeSubmit').innerHTML=""
        
        var uls = document.getElementById("dual-list-left");
            var listItem = uls.getElementsByTagName("li");
            
            var newNums ="";
            var skuIds=[];
            
            for (var i=0; i < listItem.length; i++) {
                newNums+=(  listItem[i].getAttribute('data-value-name') )+"<br>" ;
                skuIds.push(  listItem[i].getAttribute('sku-id') ) ;
            }
        document.getElementById('products_tagged_for_purchasing').value=skuIds
        document.getElementById('collection_of_all_sku_to_buy').innerHTML="";
        var x = document.getElementById("product_selector");
        var productSelectorText=x.options[x.selectedIndex].text
            //document.getElementById('collection_of_all_sku_to_buy').appendChild (buttonElement);
            var buttonSubmit=document.createElement ("button")
            buttonSubmit.innerHTML="Apply to  Selected SKU's Taged Under "+productSelectorText +" "
            buttonSubmit.type="submit";
            buttonSubmit.setAttribute("style","margin-left:10px");
            buttonSubmit.classList.add('btn','btn-info');
            
            document.getElementById('collection_of_all_sku_to_buy').appendChild (buttonSubmit);
            document.getElementById('name_of_products_tagged_for_purchasing').value="Apply to  Selected SKU's Taged Under "+productSelectorText +" "
    }
    else{
        var productSelectorText="";
        document.getElementById('collection_of_all_sku_to_buy').innerHTML="Nothing Selected"
        document.getElementById('products_tagged_for_purchasing').value=""
        if(document.getElementById("product_selector")!=null){
            var x = document.getElementById("product_selector");
            productSelectorText=x.options[x.selectedIndex].text
        }
        if(document.getElementById('AttributeSubmit')!=null){
            if(document.getElementById('AttributeSubmit').innerHTML.trim()==""){
            var submitButton=document.createElement('button')
                submitButton.type="submit";
                submitButton.classList.add("btn","btn-info");
                submitButton.innerHTML="Apply to  All SKU's Under "+ productSelectorText+" " ;
                document.getElementById('AttributeSubmit').appendChild(submitButton)
            }
        }
        document.getElementById('name_of_products_tagged_for_purchasing').value="Apply to  All SKU's Under "+productSelectorText +" "
    }
}

    function remove_category(){
        document.getElementById('parent_category_selector').value=""
        document.getElementById('CategoryContainer').innerHTML=""
        document.getElementById('parentCatSel').remove();
            if(document.getElementById('subCatSel')!=null){
                document.getElementById('subCatSel').remove();
            }
            if(document.getElementById('brand_sel')!=null){
                document.getElementById('brand_sel').remove();
            }
            if(document.getElementById('attribute_sel')!=null){
                document.getElementById('attribute_sel').remove();
            }
            if(document.getElementById('product_sel')!=null){
                document.getElementById('product_sel').remove();
            }

            var parentDiv = document.getElementById('subCategoryContainer');
            parentDiv.innerHTML=""
            
            if(document.getElementById('AttributeSubmit')!=null){
                document.getElementById('AttributeSubmit').innerHTML=""
            }
            if(document.getElementById('BrandSubmit')!=null){
                document.getElementById('BrandSubmit').innerHTML=""
            }
            if(document.getElementById('CategorySubmit')!=null){
                document.getElementById('CategorySubmit').innerHTML=""
            }
            if(document.getElementById('SubCategorySubmit')!=null){
                document.getElementById('SubCategorySubmit').innerHTML=""
            }
            if(document.getElementById('parentCategorySubmit')!=null){
                document.getElementById('parentCategorySubmit').innerHTML=""
            }
            
            if(document.getElementById('brandContainer').innerHTML!=null){
                document.getElementById('brandContainer').innerHTML=""
            }
            if(document.getElementById('attributeContainer').innerHTML!=null){
                document.getElementById('attributeContainer').innerHTML=""
            }
            if(document.getElementById('productContainer').innerHTML!=null){
                document.getElementById('productContainer').innerHTML=""
            }
            if(document.getElementById('productTypeContainer').innerHTML!=null){
                document.getElementById('productTypeContainer').innerHTML=""
            }
            if(document.getElementById('dual-list-right').innerHTML!=null){
                document.getElementById('dual-list-right').innerHTML=""
            }
            if(document.getElementById('dual-list-left').hasChildNodes()){
                document.getElementById('dual-list-left').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
                document.getElementById('product_filter_level_selector_').style.display="none"
            }
            if(document.getElementById('collection_of_all_sku_to_buy')!=null){
                document.getElementById('collection_of_all_sku_to_buy').innerHTML="";
            }
            document.getElementById('parentCategorySubmit').innerHTML=""
            var submitButton=document.createElement('button')
                submitButton.type="submit";
                submitButton.classList.add("btn","btn-info");
                submitButton.innerHTML="Apply to  All SKU's In Store ";
             document.getElementById('parentCategorySubmit').appendChild(submitButton)
             document.getElementById('name_of_products_tagged_for_purchasing').value="Apply to  All SKU's In Store ";
    }

    function remove_subcategory(){
            document.getElementById('category_selector').value=""
            document.getElementById('subCatSel').remove();
            if(document.getElementById('brand_sel')!=null){
                document.getElementById('brand_sel').remove();
            }
            if(document.getElementById('attribute_sel')!=null){
                document.getElementById('attribute_sel').remove();
            }
            if(document.getElementById('product_sel')!=null){
                document.getElementById('product_sel').remove();
            }

            var parentDiv = document.getElementById('subCategoryContainer');
            parentDiv.innerHTML=""
            
            if(document.getElementById('AttributeSubmit')!=null){
                document.getElementById('AttributeSubmit').innerHTML=""
            }
            if(document.getElementById('BrandSubmit')!=null){
                document.getElementById('BrandSubmit').innerHTML=""
            }
            if(document.getElementById('CategorySubmit')!=null){
                document.getElementById('CategorySubmit').innerHTML=""
            }
            if(document.getElementById('SubCategorySubmit')!=null){
                document.getElementById('SubCategorySubmit').innerHTML=""
            }
            if(document.getElementById('parentCategorySubmit')!=null){
                document.getElementById('parentCategorySubmit').innerHTML=""
            }
            var x = document.getElementById("parent_category_selector");
            var parentCatText=x.options[x.selectedIndex].text
            var submitButton=document.createElement('button')
                submitButton.type="submit";
                submitButton.classList.add("btn","btn-info");
                submitButton.innerHTML="Apply to  All SKU's Under "+parentCatText+" ";
            document.getElementById('parentCategorySubmit').appendChild(submitButton)
            document.getElementById('name_of_products_tagged_for_purchasing').value="Apply to  All SKU's Under "+parentCatText+" ";
            
            if(document.getElementById('brandContainer').innerHTML!=null){
                document.getElementById('brandContainer').innerHTML=""
            }
            if(document.getElementById('attributeContainer').innerHTML!=null){
                document.getElementById('attributeContainer').innerHTML=""
            }
            if(document.getElementById('productContainer').innerHTML!=null){
                document.getElementById('productContainer').innerHTML=""
            }
            if(document.getElementById('productTypeContainer').innerHTML!=null){
                document.getElementById('productTypeContainer').innerHTML=""
            }
            if(document.getElementById('dual-list-right').innerHTML!=null){
                document.getElementById('dual-list-right').innerHTML=""
            }
            if(document.getElementById('dual-list-left').hasChildNodes()){
                document.getElementById('dual-list-left').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
                document.getElementById('product_filter_level_selector_').style.display="none"
            }
            if(document.getElementById('collection_of_all_sku_to_buy')!=null){
                document.getElementById('collection_of_all_sku_to_buy').innerHTML="";
            }
    }
    function remove_brand(){
        document.getElementById('sub_category_selector').value=""
            document.getElementById('brand_sel').remove();

            if(document.getElementById('attribute_sel')!=null){
                document.getElementById('attribute_sel').remove();
            }
            if(document.getElementById('product_sel')!=null){
                document.getElementById('product_sel').remove();
            }
            
            if(document.getElementById('AttributeSubmit')!=null){
                document.getElementById('AttributeSubmit').innerHTML=""
            }
            if(document.getElementById('BrandSubmit')!=null){
                document.getElementById('BrandSubmit').innerHTML=""
            }
            if(document.getElementById('CategorySubmit')!=null){
                document.getElementById('CategorySubmit').innerHTML=""
            }
            if(document.getElementById('SubCategorySubmit')!=null){
                document.getElementById('SubCategorySubmit').innerHTML=""
            }
            if(document.getElementById('parentCategorySubmit')!=null){
                document.getElementById('parentCategorySubmit').innerHTML=""
            }
            var x = document.getElementById("category_selector");
            var catText=x.options[x.selectedIndex].text
            var submitButton=document.createElement('button')
                submitButton.type="submit";
                submitButton.classList.add("btn","btn-info");
                submitButton.innerHTML="Apply to  All SKU's Under "+catText+" ";
            document.getElementById('CategorySubmit').appendChild(submitButton)
            document.getElementById('name_of_products_tagged_for_purchasing').value="Apply to  All SKU's Under "+catText+" ";
            
            if(document.getElementById('brandContainer').innerHTML!=null){
                document.getElementById('brandContainer').innerHTML=""
            }
            if(document.getElementById('attributeContainer').innerHTML!=null){
                document.getElementById('attributeContainer').innerHTML=""
            }
            if(document.getElementById('productContainer').innerHTML!=null){
                document.getElementById('productContainer').innerHTML=""
            }
            if(document.getElementById('productTypeContainer').innerHTML!=null){
                document.getElementById('productTypeContainer').innerHTML=""
            }
            if(document.getElementById('dual-list-right').innerHTML!=null){
                document.getElementById('dual-list-right').innerHTML=""
            }
            if(document.getElementById('dual-list-left').hasChildNodes()){
                document.getElementById('dual-list-left').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
                document.getElementById('product_filter_level_selector_').style.display="none"
            }
            if(document.getElementById('collection_of_all_sku_to_buy')!=null){
                document.getElementById('collection_of_all_sku_to_buy').innerHTML="";
            }
    }
    function remove_product(){
        document.getElementById('attribute_selector').value=""
            document.getElementById('attribute_sel').remove();

            if(document.getElementById('product_sel')!=null){
                document.getElementById('product_sel').remove;
            }
            
            if(document.getElementById('AttributeSubmit')!=null){
                document.getElementById('AttributeSubmit').innerHTML=""
            }
            if(document.getElementById('BrandSubmit')!=null){
                document.getElementById('BrandSubmit').innerHTML=""
            }
            if(document.getElementById('CategorySubmit')!=null){
                document.getElementById('CategorySubmit').innerHTML=""
            }
            if(document.getElementById('SubCategorySubmit')!=null){
                document.getElementById('SubCategorySubmit').innerHTML=""
            }
            if(document.getElementById('parentCategorySubmit')!=null){
                document.getElementById('parentCategorySubmit').innerHTML=""
            }
            var x = document.getElementById("sub_category_selector");
            var sub_catText=x.options[x.selectedIndex].text
            var submitButton=document.createElement('button')
                submitButton.type="submit";
                submitButton.classList.add("btn","btn-info");
                submitButton.innerHTML="Apply to  All SKU's Under "+sub_catText+" ";
             document.getElementById('SubCategorySubmit').appendChild(submitButton)
            document.getElementById('name_of_products_tagged_for_purchasing').value="Apply to  All SKU's Under "+sub_catText+" ";
            if(document.getElementById('attributeContainer').innerHTML!=null){
                document.getElementById('attributeContainer').innerHTML=""
            }
            if(document.getElementById('productContainer').innerHTML!=null){
                document.getElementById('productContainer').innerHTML=""
            }
            if(document.getElementById('productTypeContainer').innerHTML!=null){
                document.getElementById('productTypeContainer').innerHTML=""
            }
            if(document.getElementById('dual-list-right').innerHTML!=null){
                document.getElementById('dual-list-right').innerHTML=""
            }
            if(document.getElementById('dual-list-left').hasChildNodes()){
                document.getElementById('dual-list-left').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
                document.getElementById('product_filter_level_selector_').style.display="none"
            }
            if(document.getElementById('collection_of_all_sku_to_buy')!=null){
                document.getElementById('collection_of_all_sku_to_buy').innerHTML="";
            }
    }
    function remove_product_variant(){
        document.getElementById('product_selector').value=""
            document.getElementById('product_sel').remove();


            if(document.getElementById('productContainer').innerHTML!=null){
                document.getElementById('productContainer').innerHTML=""
            }
            if(document.getElementById('productTypeContainer').innerHTML!=null){
                document.getElementById('productTypeContainer').innerHTML=""
            }
            if(document.getElementById('dual-list-right').innerHTML!=null){
                document.getElementById('dual-list-right').innerHTML=""
            }
            if(document.getElementById('dual-list-left').hasChildNodes()){
                document.getElementById('dual-list-left').innerHTML=""
                update_buy_promo_list()
            }
            
            if(document.getElementById('AttributeSubmit')!=null){
                document.getElementById('AttributeSubmit').innerHTML=""
            }
            if(document.getElementById('BrandSubmit')!=null){
                document.getElementById('BrandSubmit').innerHTML=""
            }
            if(document.getElementById('CategorySubmit')!=null){
                document.getElementById('CategorySubmit').innerHTML=""
            }
            if(document.getElementById('SubCategorySubmit')!=null){
                document.getElementById('SubCategorySubmit').innerHTML=""
            }
            if(document.getElementById('parentCategorySubmit')!=null){
                document.getElementById('parentCategorySubmit').innerHTML=""
            }
            var x = document.getElementById("attribute_selector");
            var brands_Text=x.options[x.selectedIndex].text
            var submitButton=document.createElement('button')
                submitButton.type="submit";
                submitButton.classList.add("btn","btn-info");
                submitButton.innerHTML="Apply to  All SKU's Under "+brands_Text+" ";
             document.getElementById('BrandSubmit').appendChild(submitButton)
             document.getElementById('name_of_products_tagged_for_purchasing').value="Apply to  All SKU's Under "+brands_Text+" ";
            
            if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
                document.getElementById('product_filter_level_selector_').style.display="none"
            }
            if(document.getElementById('collection_of_all_sku_to_buy')!=null){
                document.getElementById('collection_of_all_sku_to_buy').innerHTML="";
            }
    }
    
    function get_category(){
        var parentCatval=document.getElementById('parent_category_selector').value
        var x = document.getElementById("parent_category_selector");
        var parentCatText=x.options[x.selectedIndex].text
        if(document.getElementById('parentCatSel')!=null){
            remove_category()
        }
        document.getElementById('parent_category_selector').value=parentCatval
        document.getElementById("parent_category_selector").options[x.selectedIndex].text=parentCatText
            var parentDiv = document.getElementById('CategoryContainer');
            parentDiv.innerHTML=""
            if(document.getElementById('subCategoryContainer').innerHTML!=null){
                document.getElementById('subCategoryContainer').innerHTML=""
            }
            if(document.getElementById('brandContainer').innerHTML!=null){
                document.getElementById('brandContainer').innerHTML=""
            }
            if(document.getElementById('attributeContainer').innerHTML!=null){
                document.getElementById('attributeContainer').innerHTML=""
            }
            if(document.getElementById('productContainer').innerHTML!=null){
                document.getElementById('productContainer').innerHTML=""
            }
            if(document.getElementById('productTypeContainer').innerHTML!=null){
                document.getElementById('productTypeContainer').innerHTML=""
            }
            if(document.getElementById('dual-list-right').innerHTML!=null){
                document.getElementById('dual-list-right').innerHTML=""
            }
            
            if(document.getElementById('dual-list-left').hasChildNodes()){
                document.getElementById('dual-list-left').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
                document.getElementById('product_filter_level_selector_').style.display="none"
            }

        if(parentCatval!=""){
           document.getElementById('parentCategorySubmit').innerHTML=""          
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                    var select_data=JSON.parse(xhr.responseText);
                                    /*var labelElement=document.createElement ("label")
                                    labelElement.classList.add('control-label','col-md-2');
                                    labelElement.innerHTML="Category:"
                                    parentDiv.appendChild (labelElement);*/
                                    if(select_data.length>0){
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-5');
                                        divElement.setAttribute('id','ParentCategorySelect')
                                        parentDiv.appendChild (divElement);
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-5');
                                        divElement.setAttribute('id','CategorySubmit')
                                        parentDiv.appendChild (divElement);
                                        
                                        var selectDiv=document.getElementById('ParentCategorySelect');
                                        var selectElement = document.createElement ("select");
                                        selectElement.classList.add("form-control");
                                        selectElement.setAttribute('id','category_selector');
                                        selectElement.setAttribute('onchange','get_subcategory()');
                                        selectElement.name="category";
                                        var option = new Option ("Choose Category","");
                                        selectElement.options[selectElement.options.length] = option;
                                        for (var i=0;i < select_data.length;i++)
                                        {
                                            
                                            var option = new Option (select_data[i].cat_name,select_data[i].cat_id);
                                            selectElement.options[selectElement.options.length] = option;
                                        }
                                        selectDiv.appendChild (selectElement);
                                        var submitButton=document.createElement('button')
                                            submitButton.type="submit";
                                            submitButton.classList.add("btn","btn-info");
                                            submitButton.innerHTML="Apply to  All SKU's Under "+parentCatText+" ";
                                         document.getElementById('parentCategorySubmit').appendChild(submitButton)
                                         document.getElementById('name_of_products_tagged_for_purchasing').value="Apply to  All SKU's Under "+parentCatText+" ";   
                                        if(document.getElementById('parentCatSel')==null){
                                            var list_stack_disp=document.getElementById('item_picker_del')
                                            var list_level_span = document.createElement ("li");
                                            list_level_span.innerHTML=parentCatText +'&nbsp';
                                            list_level_span.setAttribute('id',"parentCatSel");
                                            list_level_span.setAttribute("selector_li_name",parentCatText)
                                            list_stack_disp.appendChild (list_level_span);
                                            var btncon=document.getElementById("parentCatSel")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',"parentCatSel_"+parentCatval);
                                            list_level_button.setAttribute('onclick','remove_category()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                        }
                                        else{
                                            document.getElementById('parentCatSel').innerHTML= parentCatText+'&nbsp';
                                            document.getElementById('parentCatSel').setAttribute("selector_li_name",parentCatText)
                                            var btncon=document.getElementById("parentCatSel")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',"parentCatSel_"+parentCatval);
                                            list_level_button.setAttribute('onclick','remove_category()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                        }
                                   }
                                     else{
                                        if(document.getElementById('parentCatSel')){
                                            document.getElementById('parentCatSel').remove();
                                        }
                                        if(document.getElementById('subCatSel')){
                                            document.getElementById('subCatSel').remove();
                                        }
                                        if(document.getElementById('brand_sel')){
                                            document.getElementById('brand_sel').remove();
                                        }
                                        if(document.getElementById('brand_sel')){
                                            document.getElementById('brand_sel').remove();
                                        }
                                        if(document.getElementById('attribute_sel')){
                                            document.getElementById('attribute_sel').remove();
                                        }
                                        if(document.getElementById('product_sel')){
                                            document.getElementById('product_sel').remove();
                                        }
                                        var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-10');
                                        labelElement.setAttribute('style','text-align: left')
                                        labelElement.innerHTML="No products to show"
                                        parentDiv.appendChild (labelElement);
                                        document.getElementById('parentCategorySubmit').innerHTML=""
                                    }       
                                        
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_category", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send("pcat_id="+parentCatval);
                }
        }
        else{
            document.getElementById('parentCategorySubmit').innerHTML=""
            var submitButton=document.createElement('button')
                submitButton.type="submit";
                submitButton.classList.add("btn","btn-info");
                submitButton.innerHTML="Apply to  All SKU's In Store ";
             document.getElementById('parentCategorySubmit').appendChild(submitButton)
             document.getElementById('name_of_products_tagged_for_purchasing').value="Apply to  All SKU's In Store ";
        }
    }

    
    function get_subcategory(){
            
        var catval=document.getElementById('category_selector').value
        var x = document.getElementById("category_selector");
        var catText=x.options[x.selectedIndex].text
        if(document.getElementById('subCatSel')!=null){
            remove_subcategory()
        }
        document.getElementById('category_selector').value=catval
        document.getElementById("category_selector").options[x.selectedIndex].text=catText
            var parentDiv = document.getElementById('subCategoryContainer');
            parentDiv.innerHTML=""
            if(document.getElementById('brandContainer').innerHTML!=null){
                document.getElementById('brandContainer').innerHTML=""
            }
            if(document.getElementById('attributeContainer').innerHTML!=null){
                document.getElementById('attributeContainer').innerHTML=""
            }
            if(document.getElementById('productContainer').innerHTML!=null){
                document.getElementById('productContainer').innerHTML=""
            }
            if(document.getElementById('productTypeContainer').innerHTML!=null){
                document.getElementById('productTypeContainer').innerHTML=""
            }
            if(document.getElementById('dual-list-right').innerHTML!=null){
                document.getElementById('dual-list-right').innerHTML=""
            }
            
            if(document.getElementById('dual-list-left').hasChildNodes()){
                document.getElementById('dual-list-left').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
                document.getElementById('product_filter_level_selector_').style.display="none"
            }

        if(catval!=""){         
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                    var select_data=JSON.parse(xhr.responseText);
                                    /*var labelElement=document.createElement ("label")
                                    labelElement.classList.add('control-label','col-md-2');
                                    labelElement.innerHTML="Sub Category:"
                                    parentDiv.appendChild (labelElement);*/
                                       
                                    if(select_data.length>0){
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-5');
                                        divElement.setAttribute('id','subCategorySelect')
                                        parentDiv.appendChild (divElement);
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-5');
                                        divElement.setAttribute('id','SubCategorySubmit')
                                        parentDiv.appendChild (divElement);
                                        
                                        document.getElementById('parentCategorySubmit').innerHTML=""
                                        document.getElementById('CategorySubmit').innerHTML=""
                                        var submitButton=document.createElement('button')
                                            submitButton.type="submit";
                                            submitButton.classList.add("btn","btn-info");
                                            submitButton.innerHTML="Apply to  All SKU's Under "+catText+" ";
                                         document.getElementById('CategorySubmit').appendChild(submitButton)
                                         document.getElementById('name_of_products_tagged_for_purchasing').value="Apply to  All SKU's Under "+catText+" ";
                                        
                                        var selectDiv=document.getElementById('subCategorySelect');
                                        var selectElement = document.createElement ("select");
                                        selectElement.classList.add("form-control");
                                        selectElement.setAttribute('id','sub_category_selector');
                                        selectElement.setAttribute('onchange','get_brands()');
                                        selectElement.name="sub_category";
                                        var option = new Option ("Choose Sub Category","");
                                        selectElement.options[selectElement.options.length] = option;
                                        for (var i=0;i < select_data.length;i++)
                                        {
                                            
                                            var option = new Option (select_data[i].subcat_name,select_data[i].subcat_id);
                                            selectElement.options[selectElement.options.length] = option;
                                        }
                                        selectDiv.appendChild (selectElement);
                                        if(document.getElementById('subCatSel')==null){
                                            var list_stack_disp=document.getElementById('item_picker_del')
                                            var list_level_span = document.createElement ("li");
                                            list_level_span.innerHTML=catText +'&nbsp';
                                            list_level_span.setAttribute('id',"subCatSel");
                                            list_level_span.setAttribute("selector_li_name",catText)
                                            list_stack_disp.appendChild (list_level_span);
                                            var btncon=document.getElementById("subCatSel")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',"subCatSel_"+catval);
                                            list_level_button.setAttribute('onclick','remove_subcategory()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                        }
                                        else{
                                            document.getElementById('subCatSel').innerHTML=catText+'&nbsp';
                                            document.getElementById('subCatSel').setAttribute("selector_li_name",catText)
                                            var btncon=document.getElementById("subCatSel")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',"subCatSel_"+catval);
                                            list_level_button.setAttribute('onclick','remove_subcategory()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                        }
                                    }
                                     else{
                                        if(document.getElementById('subCatSel')){
                                            document.getElementById('subCatSel').remove();
                                        }
                                        if(document.getElementById('brand_sel')){
                                            document.getElementById('brand_sel').remove();
                                        }
                                        if(document.getElementById('brand_sel')){
                                            document.getElementById('brand_sel').remove();
                                        }
                                        if(document.getElementById('attribute_sel')){
                                            document.getElementById('attribute_sel').remove();
                                        }
                                        if(document.getElementById('product_sel')){
                                            document.getElementById('product_sel').remove();
                                        }
                                        var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-10');
                                        labelElement.setAttribute('style','text-align: left')
                                        labelElement.innerHTML="No products to show"
                                        parentDiv.appendChild (labelElement);
                                        document.getElementById('CategorySubmit').innerHTML=""
                                    }       
                                        
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_sub_catageory", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send("cat_id="+catval);
                }
        }
    }
    
    function get_brands(){
        var sub_cat_val=document.getElementById('sub_category_selector').value
        var x = document.getElementById("sub_category_selector");
        var sub_catText=x.options[x.selectedIndex].text
        var parentDiv = document.getElementById('brandContainer');
            parentDiv.innerHTML=""

            if(document.getElementById('attributeContainer').innerHTML!=null){
                document.getElementById('attributeContainer').innerHTML=""
            }
            if(document.getElementById('productContainer').innerHTML!=null){
                document.getElementById('productContainer').innerHTML=""
            }
            if(document.getElementById('productTypeContainer').innerHTML!=null){
                document.getElementById('productTypeContainer').innerHTML=""
            }
            if(document.getElementById('dual-list-right').innerHTML!=null){
                document.getElementById('dual-list-right').innerHTML=""
            }
            if(document.getElementById('dual-list-left').hasChildNodes()){
                document.getElementById('dual-list-left').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
                document.getElementById('product_filter_level_selector_').style.display="none"
            }               
        if(sub_cat_val!=""){            
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                    var select_data=JSON.parse(xhr.responseText);
                                    /*var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-2');
                                        labelElement.innerHTML="Select Brand:"
                                        parentDiv.appendChild (labelElement);*/
                                    
                                        
                                        
                                    if(select_data.length>0){
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-5');
                                        divElement.setAttribute('id','brandSelect')
                                        parentDiv.appendChild (divElement);
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-5');
                                        divElement.setAttribute('id','BrandSubmit')
                                        parentDiv.appendChild (divElement);
                                        
                                        document.getElementById('CategorySubmit').innerHTML=""
                                        document.getElementById('SubCategorySubmit').innerHTML=""
                                        var submitButton=document.createElement('button')
                                            submitButton.type="submit";
                                            submitButton.classList.add("btn","btn-info");
                                            submitButton.innerHTML="Apply to  All SKU's Under "+sub_catText+" ";
                                         document.getElementById('SubCategorySubmit').appendChild(submitButton)
                                         document.getElementById('name_of_products_tagged_for_purchasing').value="Apply to  All SKU's Under "+sub_catText+" ";
                                        
                                        var selectDiv=document.getElementById('brandSelect');
                                        var selectElement = document.createElement ("select");
                                        selectElement.classList.add("form-control");
                                        selectElement.setAttribute('id','attribute_selector');
                                        selectElement.setAttribute('onchange','get_all_attributes()');
                                        selectElement.name="attributes";
                                        var option = new Option ("Choose Brand","");
                                        selectElement.options[selectElement.options.length] = option;
                                        for (var i=0;i < select_data.length;i++)
                                        {
                                            var option = new Option (select_data[i].brand_name,select_data[i].brand_id);
                                            selectElement.options[selectElement.options.length] = option;
                                        }
                                        selectDiv.appendChild (selectElement);

                                        if(document.getElementById('brand_sel')==null){
                                            var list_stack_disp=document.getElementById('item_picker_del')
                                            var list_level_span = document.createElement ("li");
                                            list_level_span.innerHTML=sub_catText +'&nbsp';
                                            list_level_span.setAttribute('id',"brand_sel");
                                            list_level_span.setAttribute('selector_li_name',sub_catText);
                                            list_stack_disp.appendChild (list_level_span);
                                            var btncon=document.getElementById("brand_sel")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',"brand_sel_"+sub_cat_val);
                                            list_level_button.setAttribute('onclick','remove_brand()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                            
                                            if(document.getElementById('attribute_sel')){
                                            document.getElementById('attribute_sel').remove();
                                            }
                                            if(document.getElementById('product_sel')){
                                                document.getElementById('product_sel').remove();
                                            }
                                        }
                                        else{
                                            document.getElementById('brand_sel').innerHTML=sub_catText+'&nbsp';;
                                            document.getElementById('brand_sel').setAttribute('selector_li_name',sub_catText);
                                            var btncon=document.getElementById("brand_sel")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',"brand_sel_"+sub_cat_val);
                                            list_level_button.setAttribute('onclick','remove_brand()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                            if(document.getElementById('attribute_sel')){
                                            document.getElementById('attribute_sel').remove();
                                            }
                                            if(document.getElementById('product_sel')){
                                                document.getElementById('product_sel').remove();
                                            }
                                        }
                                        
                                    }
                                    else{
                                        if(document.getElementById('brand_sel')){
                                            document.getElementById('brand_sel').remove();
                                        }
                                        if(document.getElementById('attribute_sel')){
                                            document.getElementById('attribute_sel').remove();
                                        }
                                        if(document.getElementById('product_sel')){
                                            document.getElementById('product_sel').remove();
                                        }
                                        var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-10');
                                        labelElement.setAttribute('style','text-align: left')
                                        labelElement.innerHTML="No products to show"
                                        parentDiv.appendChild (labelElement);
                                        document.getElementById('SubCategorySubmit').innerHTML=""
                                    }
                                    
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_brand", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send("sub_cat_val="+sub_cat_val);
                }
        }
    }
    
function get_all_attributes(){
    var brands_val=document.getElementById('attribute_selector').value
    var x = document.getElementById("attribute_selector");
        var brands_Text=x.options[x.selectedIndex].text
        var parentDiv = document.getElementById('attributeContainer');
            parentDiv.innerHTML=""

            if(document.getElementById('productContainer').innerHTML!=null){
                document.getElementById('productContainer').innerHTML=""
            }
            if(document.getElementById('productTypeContainer').innerHTML!=null){
                document.getElementById('productTypeContainer').innerHTML=""
            }
            if(document.getElementById('dual-list-right').innerHTML!=null){
                document.getElementById('dual-list-right').innerHTML=""
            }
            if(document.getElementById('dual-list-left').hasChildNodes()){
                document.getElementById('dual-list-left').innerHTML=""
                update_buy_promo_list()
            }
            if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
                document.getElementById('product_filter_level_selector_').style.display="none"
            }                   
        if(brands_val!=""){         
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                    var select_data=JSON.parse(xhr.responseText);
                                    /*var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-2');
                                        labelElement.innerHTML="Select Product:"
                                        parentDiv.appendChild (labelElement);*/
                                        
                                        
                                    if(select_data.length>0){
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-5');
                                        divElement.setAttribute('id','attributeSelect')
                                        parentDiv.appendChild (divElement);
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-5');
                                        divElement.setAttribute('id','AttributeSubmit')
                                        parentDiv.appendChild (divElement);
                                        
                                        document.getElementById('SubCategorySubmit').innerHTML=""
                                        document.getElementById('BrandSubmit').innerHTML=""
                                        var submitButton=document.createElement('button')
                                            submitButton.type="submit";
                                            submitButton.classList.add("btn","btn-info");
                                            submitButton.innerHTML="Apply to  All SKU's Under "+brands_Text+" ";
                                         document.getElementById('BrandSubmit').appendChild(submitButton)
                                         document.getElementById('name_of_products_tagged_for_purchasing').value="Apply to  All SKU's Under "+brands_Text+" ";
                                        
                                        var selectDiv=document.getElementById('attributeSelect');
                                        var selectElement = document.createElement ("select");
                                        selectElement.classList.add("form-control");
                                        selectElement.setAttribute('id','product_selector');
                                        selectElement.setAttribute('onchange','get_all_items()');
                                        selectElement.name="attributes_selector";
                                        var option = new Option ("Choose Product","");
                                        selectElement.options[selectElement.options.length] = option;
                                        for (var i=0;i < select_data.length;i++)
                                        {
                                            var option = new Option (select_data[i].product_name,select_data[i].product_id);
                                            selectElement.options[selectElement.options.length] = option;
                                        }
                                        selectDiv.appendChild (selectElement);
                                        
                                        if(document.getElementById('attribute_sel')==null){
                                            var list_stack_disp=document.getElementById('item_picker_del')
                                            var list_level_span = document.createElement ("li");
                                            list_level_span.innerHTML=brands_Text +'&nbsp';
                                            list_level_span.setAttribute('id',"attribute_sel");
                                            list_level_span.setAttribute('selector_li_name',brands_Text);
                                            list_stack_disp.appendChild (list_level_span);
                                            var btncon=document.getElementById("attribute_sel")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',brands_val+"attribute_sel_");
                                            list_level_button.setAttribute('onclick','remove_product()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                            if(document.getElementById('product_sel')){
                                                document.getElementById('product_sel').remove();
                                            }
                                        }
                                        else{
                                            document.getElementById('attribute_sel').innerHTML=brands_Text+'&nbsp';
                                            document.getElementById('attribute_sel').setAttribute('selector_li_name',brands_Text);
                                            var btncon=document.getElementById("attribute_sel")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',brands_val+"attribute_sel_");
                                            list_level_button.setAttribute('onclick','remove_product()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                            if(document.getElementById('product_sel')){
                                                document.getElementById('product_sel').remove();
                                            }
                                        }
                                        
                                    }
                                    else{
                                        if(document.getElementById('attribute_sel')){
                                            document.getElementById('attribute_sel').remove();
                                        }
                                        if(document.getElementById('product_sel')){
                                            document.getElementById('product_sel').remove();
                                        }
                                        var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-10');
                                        labelElement.setAttribute('style','text-align: left')
                                        labelElement.innerHTML="No products to show"
                                        parentDiv.appendChild (labelElement);
                                        document.getElementById('BrandSubmit').innerHTML=""
                                    }
                                    
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_attributes", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send("brands_val="+brands_val);
                }
        }
}   

function get_all_items(){
        var product_val=document.getElementById('product_selector').value
        var x = document.getElementById("product_selector");
        var product_Text=x.options[x.selectedIndex].text
        var parentDiv = document.getElementById('productContainer');
        parentDiv.innerHTML=""

        if(document.getElementById('dual-list-right').innerHTML!=null){
            document.getElementById('dual-list-right').innerHTML=""
        }
        if(document.getElementById('dual-list-left').hasChildNodes()){
            document.getElementById('dual-list-left').innerHTML=""
            update_buy_promo_list()
        }
        if(document.getElementById('product_filter_level_selector_').innerHTML!=null){
            document.getElementById('product_filter_level_selector_').style.display="none"
        }       
        if(product_val!=""){            
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                    var select_data=JSON.parse(xhr.responseText);
                                    document.getElementById('dual-list-left').innerHTML=""
                                    document.getElementById('dual-list-right').innerHTML=""
                                        
                                    if(select_data.length>0){                                      
                                        document.getElementById('BrandSubmit').innerHTML=""
                                        if(document.getElementById('AttributeSubmit').innerHTML.trim()==""){
                                            var submitButton=document.createElement('button')
                                            submitButton.type="submit";
                                            submitButton.classList.add("btn","btn-info");
                                            submitButton.innerHTML="Apply to  All SKU's Under "+ product_Text+" ";
                                            document.getElementById('AttributeSubmit').appendChild(submitButton)
                                            document.getElementById('name_of_products_tagged_for_purchasing').value="Apply to  All SKU's Under "+ product_Text+" ";
                                        }
                                        document.getElementById('product_filter_level_selector_').style.display=""
                                        
                                        var liDiv=document.getElementById('dual-list-right');
                                        document.getElementById('dual-list-left').innerHTML=""
                                        document.getElementById('dual-list-right').innerHTML=""
                                        for (var i=0;i < select_data.length;i++)
                                        {
											var val=select_data[i].attribute_1_value.split(':');
                                            var liElement = document.createElement ("li");
                                            liElement.innerHTML=select_data[i].sku_id+'('+select_data[i].attribute_1+'='+val[0]+', '+select_data[i].attribute_2+'='+select_data[i].attribute_2_value+')';
                                            liElement.classList.add("list-group-item");
                                            liElement.setAttribute('data-value-name',select_data[i].sku_id+'('+select_data[i].attribute_1+'='+select_data[i].attribute_1_value+', '+select_data[i].attribute_2+'='+select_data[i].attribute_2_value+')');
                                            liElement.setAttribute('sku-id',select_data[i].id);
                                            liElement.setAttribute('data-value',select_data[i].id);
                                            liElement.name="item_typs";
                                            liDiv.appendChild (liElement);
                                        }
                                        activate_li();
                                        populate_attribute_filter(product_val);
                                        
                                        if(document.getElementById('product_sel')==null){
                                            if(product_Text.length > 10){
                                                product_Texts = product_Text.substring(0,10)+"...";
                                            }
                                            
                                            var list_stack_disp=document.getElementById('item_picker_del')
                                            var list_level_span = document.createElement ("li");
                                            list_level_span.innerHTML=product_Texts +'&nbsp';
                                            list_level_span.setAttribute('id',"product_sel");
                                            list_level_span.setAttribute('selector_li_name',product_Text);
                                            list_stack_disp.appendChild (list_level_span);
                                            var btncon=document.getElementById("product_sel")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',product_val+"_product_sel");
                                            list_level_button.setAttribute('onclick','remove_product_variant()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                        }
                                        else{
                                            if(product_Text.length > 10){
                                                product_Texts = product_Text.substring(0,10)+"...";
                                            }
                                            document.getElementById('product_sel').innerHTML=product_Texts+'&nbsp';
                                            document.getElementById('product_sel').setAttribute('selector_li_name',product_Text)        
                                            var btncon=document.getElementById("product_sel")
                                            var list_level_button=document.createElement ("button");
                                            list_level_button.type="button";
                                            list_level_button.setAttribute('id',product_val+"_product_sel");
                                            list_level_button.setAttribute('onclick','remove_product_variant()');
                                            list_level_button.classList.add("material-button","btn-danger");
                                            list_level_button.innerHTML='<i class="fa fa-trash" aria-hidden="true"></i>';
                                            btncon.appendChild (list_level_button);
                                        }
                                        
                                        
                                        
                                    }
                                    else{
                                        if(document.getElementById('product_sel')){
                                            document.getElementById('product_sel').remove();
                                        }
                                        document.getElementById('product_filter_level_selector_').style.display="none"
                                        var selectDiv=document.getElementById('attributeSelect');
                                        var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-10');
                                        labelElement.setAttribute('style','text-align: left')
                                        labelElement.innerHTML="No products to show"
                                        selectDiv.appendChild (labelElement);
                                        document.getElementById('AttributeSubmit').innerHTML=""
                                    }
                                    
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_items_in_type", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send("product_val="+product_val);
                }
        }
}

function populate_attribute_filter(product_val){
    var productTypeContainer =document.getElementById('productTypeContainer')
        productTypeContainer.innerHTML=""
        
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200){
                                    var select_data=JSON.parse(xhr.responseText);                                       
                                    //alert(select_data.attr1.length);
                                    //alert(select_data.attr2.length);
                                    if(select_data.attr1.length>0){
                                        var labelElement=document.createElement ("div")
                                        labelElement.classList.add('col-md-12');
                                        labelElement.innerHTML="Filter "+select_data.attribute_1+" :";
                                        productTypeContainer.appendChild (labelElement);
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-12');
                                        divElement.setAttribute('id','attributeColorSelect')
                                        productTypeContainer.appendChild (divElement);
                                        var checkDiv=document.getElementById('attributeColorSelect');
                                    
                                        
                                        for (var i=0;i < select_data.attr1.length;i++){
                                            var labelElement=document.createElement ("label")
                                            labelElement.classList.add('checkbox-inline');
                                            labelElement.setAttribute('id','id_'+select_data.attr1[i]);
                                            checkDiv.appendChild (labelElement);
                                            
                                            var labelDiv=document.getElementById('id_'+select_data.attr1[i]);
                                            var checkbox = document.createElement('input');
                                            checkbox.type = "checkbox";
                                            checkbox.name = "filter_color";
                                            checkbox.setAttribute("onclick","applyToFilter("+product_val+")");
                                            checkbox.value = select_data.attr1[i];
                                            checkbox.id = "id_"+select_data.attr1[i];
                                            
                                            var label = document.createElement('label')
                                            label.htmlFor = "id_"+select_data.attr1[i];
											var val=select_data.attr1[i].split(':');
                                            label.appendChild(document.createTextNode(val[0]));
                                            
                                            labelDiv.appendChild(checkbox);
                                            labelDiv.appendChild(label);
                                        }
                                        
                                    }
									if(select_data.attr2!=null){
										
                                    if(select_data.attr2.length>0){
                                        var labelElement=document.createElement ("div")
                                        labelElement.classList.add('col-md-12');
                                        labelElement.innerHTML="Filter "+select_data.attribute_2+" :";
                                        productTypeContainer.appendChild (labelElement);
                                        var divElement=document.createElement ("div")
                                        divElement.classList.add('col-md-12');
                                        divElement.setAttribute('id','attributeSizeSelect')
                                        productTypeContainer.appendChild (divElement);
                                        var checkDiv=document.getElementById('attributeSizeSelect');
                                    
                                        
                                        for(var i=0;i < select_data.attr2.length;i++){
                                            var labelElement=document.createElement ("label")
                                            labelElement.classList.add('checkbox-inline');
                                            labelElement.setAttribute('id','id_'+select_data.attr2[i]);
                                            checkDiv.appendChild (labelElement);
                                            
                                            var labelDiv=document.getElementById('id_'+select_data.attr2[i]);
                                            var checkbox = document.createElement('input');
                                            checkbox.type = "checkbox";
                                            checkbox.name = "filter_size";
                                            checkbox.setAttribute("onclick","applyToFilter("+product_val+")");
                                            checkbox.value = select_data.attr2[i];
                                            checkbox.id = "id_"+select_data.attr2[i];
                                            
                                            var label = document.createElement('label')
                                            label.htmlFor = "id_"+select_data.attr2[i];
                                            label.appendChild(document.createTextNode(select_data.attr2[i]));
                                            
                                            labelDiv.appendChild(checkbox);
                                            labelDiv.appendChild(label);
                                        }
                                        countLeftlist()
                                    }
									}
                                    
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_items_in_type_filter", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send("product_val="+product_val);
                }
}

function applyToFilter(product_val){
    update_buy_promo_list()
    var colorelem=document.getElementsByName('filter_color')
    var colorelemarr = []
    for(i=0;i<colorelem.length;i++){
        if(colorelem[i].checked){
            colorelemarr.push(colorelem[i].value);
        }
    }
    var color=colorelemarr.join("-");
    var sizeelem=document.getElementsByName('filter_size')
    var sizeelemarr = []
    for(i=0;i<sizeelem.length;i++){
        if(sizeelem[i].checked){
            sizeelemarr.push(sizeelem[i].value);
        }
    }
    var size=sizeelemarr.join("-");
    
    var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
         var xhr = new XMLHttpRequest();
                
                if (xhr) {
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                                    var select_data=JSON.parse(xhr.responseText);
                                    var liDiv=document.getElementById('dual-list-right');
                                    liDiv.innerHTML=""
                                    document.getElementById('dual-list-left').innerHTML=""
                                        //alert(xhr.responseText);
                                    if(select_data.length>0){
                                        document.getElementById('product_filter_level_selector_').style.display=""
                                        
                                        var liDiv=document.getElementById('dual-list-right');
                                        for (var i=0;i < select_data.length;i++)
                                        {
                                            
                                            var liElement = document.createElement ("li");
                                            liElement.innerHTML=select_data[i].sku_id+'('+select_data[i].attribute_1+'='+select_data[i].attribute_1_value+', '+select_data[i].attribute_2+'='+select_data[i].attribute_2_value+')';
                                            liElement.classList.add("list-group-item");
                                            liElement.setAttribute('data-value-name',select_data[i].sku_id+'('+select_data[i].attribute_1+'='+select_data[i].attribute_1_value+', '+select_data[i].attribute_2+'='+select_data[i].attribute_2_value+')');
                                            liElement.setAttribute('sku-id',select_data[i].id);
                                            liElement.setAttribute('data-value',select_data[i].id);
                                            liElement.name="item_typs";
                                            liDiv.appendChild (liElement);
                                        }
                                        activate_li();
                                        countLeftlist()
                                        //populate_attribute_filter(product_val);
                                        
                                    }
                                    else{
                                        document.getElementById('product_filter_level_selector_').style.display="none"
                                        var selectDiv=document.getElementById('attributeSelect');
                                        var labelElement=document.createElement ("label")
                                        labelElement.classList.add('control-label','col-md-10');
                                        labelElement.setAttribute('style','text-align: left')
                                        labelElement.innerHTML="No products to show"
                                        selectDiv.appendChild (labelElement);
                                    }                                       
                                    
                                    
                                    
                                    
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Promotions/get_all_items_after_filter", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send("color_val="+color+"&size_val="+size+"&product_val="+product_val);
                }
    
}
 function countLeftlist(){
        var right_list_search=document.querySelectorAll('#dual-list-right li').length;
        var left_list_search=document.querySelectorAll('#dual-list-left li').length;
        document.getElementById("right_list_search").setAttribute('placeholder','Total SKU displayed:  '+right_list_search);
        document.getElementById("left_list_search").setAttribute('placeholder','Total SKU displayed:  '+left_list_search);  
    }
    
</script>






<script type="text/javascript">
    function activate_li() {
    var move_right = '<span class="glyphicon glyphicon-minus pull-left  dual-list-move-right" title="Remove Selected"></span>';
    var move_left  = '<span class="glyphicon glyphicon-plus  pull-right dual-list-move-left " title="Add Selected"></span>';
    
    $(".dual-list.list-left .list-group").sortable({
        stop: function( event, ui ) {
            updateSelectedOptions();
        }
    });
    
    
    $('body').on('click', '.list-group .list-group-item', function () {
        $(this).toggleClass('active');
    });
    
    
    $('body').on('click', '.dual-list-move-right', function (e) {
        e.preventDefault();

        actives = $(this).parent();
        $(this).parent().find("span").remove();
        $(move_left).clone().appendTo(actives);
        actives.clone().appendTo('.list-right ul').removeClass("active");
        actives.remove();
        
        sortUnorderedList("dual-list-right");
        
        updateSelectedOptions();
    });
    
    
    $('body').on('click', '.dual-list-move-left', function (e) {
        e.preventDefault();

        actives = $(this).parent();
        $(this).parent().find("span").remove();
        $(move_right).clone().appendTo(actives);
        actives.clone().appendTo('.list-left ul').removeClass("active");
        actives.remove();
        
        updateSelectedOptions();
    });
    
    
    $('.move-right, .move-left').click(function () {
        var $button = $(this), actives = '';
        if ($button.hasClass('move-left')) {
            actives = $('.list-right ul li.active');
            actives.find(".dual-list-move-left").remove();
            actives.append($(move_right).clone());
            actives.clone().appendTo('.list-left ul').removeClass("active");
            actives.remove();
            
            
        } else if ($button.hasClass('move-right')) {
            actives = $('.list-left ul li.active');
            actives.find(".dual-list-move-right").remove();
            actives.append($(move_left).clone());
            actives.clone().appendTo('.list-right ul').removeClass("active");
            actives.remove();

        }
        
        updateSelectedOptions();
        
    });
    
    function countLeftlist(){
        var right_list_search=document.querySelectorAll('#dual-list-right li').length;
        var left_list_search=document.querySelectorAll('#dual-list-left li').length;
        document.getElementById("right_list_search").setAttribute('placeholder','Total SKU displayed:  '+right_list_search);
        document.getElementById("left_list_search").setAttribute('placeholder','Total SKU displayed:  '+left_list_search);  
    }

    
    function updateSelectedOptions() {
        countLeftlist();
        update_buy_promo_list();
        $('#dual-list-options').find('option').remove();

        $('.list-left ul li').each(function(idx, opt) {
            $('#dual-list-options').append($("<option></option>")
                .attr("value", $(opt).data("value"))
                .text( $(opt).text())
                .prop("selected", "selected")
            ); 
        });
    }
    
    
    $('.dual-list .selector').click(function () {
        var $checkBox = $(this);
        if (!$checkBox.hasClass('selected')) {
            $checkBox.addClass('selected').closest('.well').find('ul li:not(.active)').addClass('active');
            $checkBox.removeClass('glyphicon-unchecked').addClass('glyphicon-check');
        } else {
            $checkBox.removeClass('selected').closest('.well').find('ul li.active').removeClass('active');
            $checkBox.removeClass('glyphicon-check').addClass('glyphicon-unchecked');
        }
    });
    
    
    $('[name="SearchDualList"]').keyup(function (e) {
        var code = e.keyCode || e.which;
        if (code == '9') return;
        if (code == '27') $(this).val(null);
        var $rows = $(this).closest('.dual-list').find('.list-group li');
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
        $rows.show().filter(function () {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });
    
    
    $(".glyphicon-search").on("click", function() {
        $(this).next("input").focus();
    });
    
    
    function sortUnorderedList(ul, sortDescending) {
        $("#" + ul + " li").sort(sort_li).appendTo("#" + ul);
        
        function sort_li(a, b){
            return ($(b).data('value')) < ($(a).data('value')) ? 1 : -1;    
        }
    }
        
    
    $("#dual-list-left li").append(move_right);
    $("#dual-list-right li").append(move_left);
}

</script>
</div>
</body>
</html>