<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="robots" content="noindex">
<title>Policies</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script> 
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
    <div class="container">
	<div class="page-header"><h4 class="text-center">Create Policy</h4></div>
        <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <i class="more-less glyphicon glyphicon-plus"></i>
                        Add New Policy
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                      <form class="form-horizontal">
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                        <input type="text" class="form-control" id="name" placeholder="Enter Policy name">
                      </div>
					  </div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                        <input type="text" class="form-control" id="attr" placeholder="Enter Policy Attributes">
                      </div>
					  </div>
					  <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                      <button type="submit" class="btn btn-success btn-xs btn-block">Submit</button>
					  </div>
					  </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
<?php if(!empty($policy_list)){?>
    <?php foreach($policy_list as $policy){?>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="<?php echo $policy['id'].'_'.$policy['slug']?>">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" onclick="get_all_policy_related_data(<?php echo $policy['id']?>)" href="#<?php echo $policy['id']?>" aria-expanded="true" aria-controls="<?php echo $policy['id']?>">
                        <i class="more-less glyphicon glyphicon-plus"></i>
                        <?php echo $policy['name']?>
                    </a>
                </h4>
            </div>
            <div id="<?php echo $policy['id']?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="<?php echo $policy['id'].'_'.$policy['slug']?>">
                <div class="panel-body">
                    
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   
   var table = $('#<?php echo $policy['id'].'_'.$policy['slug']?>_table').DataTable({
       'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': ['nosort']
        }],
        "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>admin/Policys/<?php echo $policy['slug']?>_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#<?php echo $policy['id'].'_'.$policy['slug']?>_table_processing").css("display","none");
            }
          },
          
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'10%',
         'className': 'dt-body-center'
      }],
      'order': [0, 'asc']
   });
}); 
</script> 
                    

                     <div class="row">
                         <div class="col-md-12">
                         <button class="btn btn-success btn-xs" onclick="javascript:location.href='<?php echo base_url()?>admin/Policys/create_<?php echo $policy['slug']?>_policy/<?php echo $policy['id']?>'">Create New Policy</button>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-md-12" id="<?php echo $policy['id']?>_existing_policy_list"></div>
                     </div>
                     <div class="row">
					 <div class="col-md-12">
                        <table id="<?php echo $policy['id'].'_'.$policy['slug']?>_table" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="nosort">S.No</th>
                                <th>Policy Type</th>
                                <th>Policy Applied On</th>
                                <th class="nosort">Action</th>
                                <th>Date Created</th>
                            </tr>
                        </thead>
                        </table>
                     </div>
					  </div>

                </div>
            </div>
        </div>
    </div>
    <?php }?>
    
  <?php }else{?>
    echo '<h4 class="text-center">No Policy Defined</h4>';
<?php }
?>  

    
    
</div><!-- container -->
    <script type="text/javascript">
    function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
    </script>
    <script>
        function delete_policy(obj){
        var policy_list_id=obj.getAttribute('policy_uid');
        var policy_type=obj.getAttribute('policy_type');
        var answer=confirm("Do you Want To Delete this Policy")
        //alert(obj.parentNode.parentNode.getAttribute('class'))
        if(answer){
            var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {
                                           
                    obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if(xhr.responseText==1){
                                var x = obj.parentNode.parentNode;
                                    x.parentNode.removeChild(x);
									location.reload();
                           }
                           else{
                               obj.innerHTML='<i class="fa fa-trash-o" aria-hidden="true"></i>'
                           }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Policys/delete_policy", true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('policy_list_id='+policy_list_id+"&policy_type="+policy_type);
                }
        }
        else{
            return false;
        }
        }
       function get_all_policy_related_data(obj){
        var policy_list_id=obj
        /*var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {
                                           
                    obj.innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>'
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if(xhr.responseText!=""){
                                var policy_list_arr=JSON.parse(xhr.responseText)
                                var str='<ul>'
                                for(var i=0;i<policy_list_arr.length;i++){
                                    str+='<li>'+policy_list_arr[i]['payment_policy_uid']+'</li>'
                                }
                                str+='<ul>';
                                //document.getElementById(policy_list_id+'_existing_policy_list').innerHTML=str;
                           }
                           else{
                               //document.getElementById(policy_list_id+'_existing_policy_list').innerHTML=""
                           }
                        }
                    }
                    xhr.open('POST', "<?php echo base_url()?>admin/Policys/delete_payment_policy", true);*/
                    /*xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('policy_list_id='+policy_list_id);
                }   */
        }
    </script>
	</div>
</body>
</html>
