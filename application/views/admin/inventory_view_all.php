<html lang="en">
<head>	
<link href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" rel="stylesheet" /> 
<link href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 


<style type="text/css">

.addon_form{
		background-color: rgb(247, 243, 243);
    padding: 10px;
    width: 100%;
    height: auto;
    float: left;
    border: 1px solid rgb(204, 204, 204);
    border-radius: 5px;
    display: block;
	}

.cursor-pointer {
    cursor: pointer !important;
}
.small_color_box{
	width: 30px;
	height: 30px;
	border: 1px solid #ccc;
	margin-top: 1px;
}

.loading {
	text-align: center; 
	margin:0;
	color:#fff;
	font-size:1em;
	font-weight:600;
}

.loading span {
	-webkit-animation-name: opacity;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: infinite;
	
	-moz-animation-name: opacity;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: infinite;
}

.loading span:nth-child(2) {
	-webkit-animation-delay: 100ms;
	-moz-animation-delay: 100ms;
}

.loading span:nth-child(3) {
	-webkit-animation-delay: 300ms;
	-moz-animation-delay: 300ms;
}
#wizardPictureSmall_picname{
	word-wrap: break-word;
}
#wizardPictureBig_picname{
	word-wrap: break-word;
}
#wizardPictureSmall_picname2{
	word-wrap: break-word;
}
#wizardPictureBig_picname2{
	word-wrap: break-word;
}
#wizardPictureSmall_picname3{
	word-wrap: break-word;
}
#wizardPictureBig_picname3{
	word-wrap: break-word;
}
#wizardPictureSmall_picname4{
	word-wrap: break-word;
}
#wizardPictureBig_picname4{
	word-wrap: break-word;
}
#wizardPictureSmall_picname5{
	word-wrap: break-word;
}
#wizardPictureBig_picname5{
	word-wrap: break-word;
}
textarea.form-control {
    height:36px;
}
.CamListDiv {
    height: 450px;
    float: left;
    overflow: scroll;
    overflow-x:hidden;
}
.well{
	width:100%;
}
</style>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js" ></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
<script type="text/javascript">
var table;

$(document).ready(function (){
	var obj=document.getElementsByName("common_checkbox");	
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	for(i=0;i<obj.length;i++){
		obj[i].checked=false;
	}
	$("#reset_form_button").click(function() {
		$(this).closest('form').find("select").val("");
	});
    var rows_selected = [];
    table = $('#table_inventory').DataTable({
	    "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url();?>admin/Catalogue/inventory_processing_all",
            data: function (d) {d.active=1;d.sku_status = $('#sku_status').val();},
            type: "post",
            error: function(){
              $("#table_inventory_processing").css("display","none");
            },
            "dataSrc": function ( json ) {				
                document.getElementById("inventory_count").innerHTML=json.recordsFiltered;
                //console.log(json.tot_sku_count_data.length)
                // if(json.tot_sku_count_data.total_available_sku){
                //     document.getElementById("total_available_sku").innerHTML=json.tot_sku_count_data.total_available_sku;
                //     document.getElementById("total_selected_sku").innerHTML=json.tot_sku_count_data.total_selected_sku;
                // }
                return json.data;
            }
          },		  
		'columnDefs': [{
        'targets': 0,
        'searchable':false,
        'orderable':false,
        'width':'5%',
        'className': 'dt-body-left'
      }],
      'order': [0, 'desc']
   });  
	$('#reset_form_button').on('click',function(){
		table.draw();
	});	
	$("#submit_button").click(function() {
		table.draw();
	});	
	
});	

function drawtable(obj=""){
	table.draw();
}
function selectAllFun(obj){
	var obj_arr=document.getElementsByName("selected_list");
	if(obj.checked){
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=true;
		}
	}
	else{
		for(i=0;i<obj_arr.length;i++){
			obj_arr[i].checked=false;
		}
	}
}
function unselectAllFun(){
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		obj_arr[i].checked=false;
	}
	$('#common_checkbox').attr('checked',false);
	
}
function multilpe_delete_fun(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
	swal({
			title: 'Are you sure?',
			text: "Archived Inventory",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Archive it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
		$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/update_inventory_selected",
		type:"post",
		data:"selected_list="+selected_list+"&active=0",
		
		success:function(data){
			if(data==true){
				
				swal({
						title:"Archived!", 
						text:"Given "+table+"(s) has been Moved to Trash successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}
			else{
				swal("Error", "Error in deleting this file", "error");
			}
		}
		});
	})
		    },
	allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}
function multilpe_delete_when_no_info(table){
	
	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			selected_list_arr.push(obj_arr[i].value);
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one "+table+"!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");
		swal({
			title: 'Are you sure?',
			text: "Delete Subcategory",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!',
			showLoaderOnConfirm: true,
			preConfirm: function(){
			return new Promise(function (resolve, reject) {
	$.ajax({
		url:"<?php echo base_url()?>admin/Catalogue/delete_inventory_when_no_info",
		type:"post",
		data:"selected_list="+selected_list,
		success:function(data){
			if(data==true){
				swal({
						title:"Deleted!", 
						text:"Given "+table+"(s) has been deleted successfully", 
						type: "success",
						allowOutsideClick: false
				}).then(function () {
					location.reload();
				});
			}else if(data==false){
				swal("Error", "Error in deleting this file", "error");
			}else{
				swal("Error", "Oops ..! You can't delete it.", "error");
			}
		}
	});
	})
		    },
   allowOutsideClick: false			  
		}).then(function() {
			}, function(dismiss) {
			  if (dismiss === 'cancel') {
				
				swal({
						title:"Cancelled", 
						text:"No action taken", 
						type: "error"
				}).then(function () {
					location.reload();
				});
			  }
			});
}

</script>
<script type="text/javascript">
	function display_color(color_value){
	var attribute_1_value=document.getElementById("attribute_1_value").value;
		//alert(color_value);
		colors_arr=color_value.split(":");
		color_code=colors_arr[1];
		color_name=colors_arr[0];
		if(attribute_1_value!=""){
		$('#small_color_box').css({"background-color": color_code});
		$('#small_color_box').attr('title',color_name);
		}if(attribute_1_value==""){
			$('#small_color_box').css({"background-color": ""});
		}
	}
</script>
<script type="text/javascript">
	function filter_display_color(color_value){
		//alert(color_value);
		colors_arr=color_value.split(":");
		color_code=colors_arr[1];
		color_name=colors_arr[0];
		$('#filter_small_color_box').css({"background-color": color_code});
		$('#filter_small_color_box').attr('title',color_name);
	}
</script>	
<script>
		function spec_fun(obj,specification_id){
			//document.getElementsByName("specification_textarea_value["+specification_id+"]")[0].setAttribute("style", "border: 1px solid #ccc;");
			if(obj.value=="Others"){
				document.getElementsByClassName("specification_textarea_value["+specification_id+"]")[0].style.display="block";
			}
			else{
				document.getElementsByClassName("specification_textarea_value["+specification_id+"]")[0].style.display="none";
			}
		}
</script>
<script type="text/javascript">
$(document).ready(function (){
$('#back').on('click',function(){	
		var form = document.getElementById('back_to_inventory');
		form.action='<?php echo base_url(); ?>admin/Catalogue/inventory_filter';
		form.submit();
	});
});		
</script>
<script>

function changeViewPrevious(){
	 window.location.href = '<?php echo base_url(); ?>admin/Catalogue/inventory_filter';
}
</script>
<script type="text/javascript">

function filter_details_form_valid()
{
	
	
	var form_status = $('<div class="form_status"></div>');		
	var form = $('#create_inventory');	
	var err = 0;
		var image = $('input[name="image1"]').val().trim();
		var thumbnail = $('input[name="thumbnail1"]').val().trim();
		if(image=='')
		{
		   $('div[class="picture"]').css("border", "2px solid #f44336");	   
		   err = 1;
		}
		else{
			$('div[class="picture"]').css({"border": "2px solid #ccc"});
		}
		if(thumbnail=='')
		{
		   $('div[class="picture"]').css("border", "2px solid #f44336");	   
		   err = 1;
		}
		else{
			$('div[class="picture"]').css({"border": "2px solid #ccc"});
		}
		subcat_id='<?php echo ($subcat_id) ? $subcat_id : '';?>';
		$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/get_all_filterbox_ids/"?>',
				type: 'POST',
				data:"subcat_id="+subcat_id,
				dataType: 'JSON',
			
			}).done(function(data)
			  {	

			});

	//form1=additional_details_form_valid();
	//form2=specification_details_form_valid();
	//form3=additional_details_form_valid();
	
	if(err==1)
	{
		//$('#validation_error').show();
		return false;
		
	}else{
			var data = $("#highlight").Editor("getText");
			$("#highlight").val(data);
			var form = $('#create_inventory');	
			var form_create = document.forms.namedItem("create_inventory");
			var ajaxData = new FormData(form_create);
swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
   allowOutsideClick: false,
   background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
				$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/add_inventory/"?>',
				type: 'POST',
				data: ajaxData,	
				contentType: false,       		
				cache: false,			
				processData:false, 		
				
			}).done(function(data)
			  {	
				if(data==true)
				{
					  swal({
						title:"Success", 
						text:"Inventory is successfully Created!", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						location.reload();

					});
				}
				else
				{
					swal(
						'Oops...',
						data,
						'error'
					)	
				}
		return true;
	});
	}
}]);
				
		return true;
	}
}
var val;
function specification_details_form_valid()
{
	
	var form_status = $('<div class="form_status"></div>');		
	var form = $('#create_inventory');	
	var err = 0;	
	subcat_id='<?php echo $subcat_id;?>';
		$.ajax({
				url: '<?php echo base_url()."admin/Catalogue/get_all_specification_ids/"?>',
				type: 'POST',
				async:false,
				data:"subcat_id="+subcat_id,
				dataType: 'JSON',	
			}).done(function(data)
			  {	
				if(data)
				{
					err = 0;
					for(i=0;i<data.length;i++){
						specification_id=data[i].specification_id;
						var specification_select_value=document.getElementsByName("specification_select_value["+specification_id+"]")[0];
						var specification_textarea_value=document.getElementsByName("specification_textarea_value["+specification_id+"]")[0];

						if(specification_select_value.value.trim() == '')
						{	
							document.getElementsByName("specification_select_value["+specification_id+"]")[0].setAttribute("style", "border: 1px solid red;");
							err++;				
						}
						else if(specification_select_value.value.trim() == 'Others' && specification_textarea_value.value.trim() == ''){	
							document.getElementsByName("specification_textarea_value["+specification_id+"]")[0].setAttribute("style", "border: 1px solid red;");
							document.getElementsByName("specification_select_value["+specification_id+"]")[0].setAttribute("style", "border: 1px solid #ccc;");err++;								
							
						}else if(specification_select_value.value.trim() == 'Others' && specification_textarea_value.value.trim() != '')
						{	
							document.getElementsByName("specification_select_value["+specification_id+"]")[0].setAttribute("style", "border: 1px solid #ccc;");	
							document.getElementsByName("specification_textarea_value["+specification_id+"]")[0].setAttribute("style", "border: 1px solid #ccc;");
							
						}else if(specification_select_value.value.trim() != '' && specification_select_value.value.trim() != 'Others')
						{	
							document.getElementsByName("specification_select_value["+specification_id+"]")[0].setAttribute("style", "border: 1px solid #ccc;");	
					
						}
						
					}
					
					if(err!=0)
					{
						val=false;
				
					}else{
						val=true;
					}
					
				}
				else
				{
					alert('error');
					
				}
	});
return val;
}


function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}
function editInventoryFun(inventory_id,product_id){
	location.href="<?php echo base_url()?>admin/Catalogue/edit_inventory_form/"+inventory_id+"/"+product_id;

}
function view_delivered_orders_by_inventory_id(inventory_id){
	location.href="<?php echo base_url()?>admin/Orders/delivered_orders_inventory_wise/"+inventory_id;
}
$(document).ready(function(){
	<?php
		if($create_editview=="create"){
			?>
			$("#viewDiv1").css({"display":"none"});
			$("#createDiv2").css({"display":""});
			<?php
		}
		else if($create_editview=="view"){
			?>
			$("#createDiv2").css({"display":"none"});
			$("#viewDiv1").css({"display":""});
			<?php
		}
	?>
});
</script>		
</head>
<body>
<div
  class="animsition"
  data-animsition-in-class="fade-in"
  data-animsition-in-duration="500"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="400"
>
<div class="container">


<div class="row">
<div class="wizard-header text-center">
    <br>
    
	<h5 style="">
		<span id="requestvalue">Number of SKUs </span> <span class="badge" style="font-size: 16px;"><div id="inventory_count"></div></span> 
	</h5>
	
</div>
<table id="table_inventory" class="table table-bordered table-striped" cellspacing="0" width="100%">
<thead>
	<tr>
		<th colspan="5">
            
                    <?php 
                    
                    $admin_user_type=$this->session->userdata("user_type");
                   
				   ?>

   
				<form id="export_form" action="" method="post">
					<input type="hidden" value="" name="export_inventory_selected" id="export_inventory_selected">
				</form>
					<button id="export_as_csv" class="btn btn-warning btn-xs" onclick="export_as_csv()">Download CSV</button>
				

				<button class="btn btn-primary btn-xs" type="button" onclick="changeViewPrevious()">Change View</button>
			

            <!---import uantity ---->

            <!-- Import link -->
            <div class="col-md-12 head">

            </div>

            <!-- File upload form -->
            
            <!---import uantity ---->

		</th>
	</tr>
	<tr>
            
		<th>
		<input type="checkbox" id="common_checkbox" name="common_checkbox" onclick="selectAllFun(this)" title="select SKU for Export CSV">      
		</th>
		<th>Image</th>
		<th>SKU Details</th>
		<th>Details</th>
		<th>Promotion Details</th>
		<th>Action</th>		
	</tr>
</thead>

</table>
</div>

</div>

<form name="back_to_inventory" id="back_to_inventory" method="post">
    <input type="hidden" value="<?php echo $pcat_id; ?>" name="parent_category">
    <input type="hidden" value="<?php echo $cat_id; ?>" name="categories">
    <input type="hidden" value="<?php echo $subcat_id; ?>" name="subcategories">
    <input type="hidden" value="<?php echo $brand_id; ?>" name="brands">
    <input type="hidden" value="<?php echo $product_id; ?>" name="products">
		
</form>
</div>

<div class="modal" id="status_modal">
     <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Update Inventory Status</h4>
           </div>
           <div  class="modal-body" id="model_content">
				<form method="post" id="request_form" name="request_form">
		  			<input type="hidden" name="app_id" id="app_id" value="">
		  			<input type="hidden" name="inventory_id" id="inventory_id" value="">
		  			

					<div class="row">

					<div class="col-md-11 col-md-offset-1"> 
							<div class="form-group">
								<label class="control-label">Status
								</label>
								<select name="status" id="status" type="text" class="form-control" required onchange="show_rejected_comments()"/>
								<option value=""> - Select status - </option>
								<option value="1">Publish</option>
								<option value="2">Unpublish</option>
								<option value="3">Reject</option>
								</select>
							</div>
						</div>
  
						<div class="col-md-11 col-md-offset-1" id="show_rejected_comments_div" style="display:none;" > 	
							<div class="form-group">
							<label class="control-label">Rejections Comments if any 
							</label>
							<textarea name="comments" id="comments" class="form-control"></textarea>
							</div>
						</div>

						<div class="col-md-11 col-md-offset-1"> 	
							<div class="form-group text-center">
							
							<button class="btn btn-info btn-sm" type="button" onclick="change_status()" id="submit-data"></i>Submit</button>
							</div>
						</div>
					</div>



				</form>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>
     </div>
    </div>
</div>


<script  type="text/javascript" src="<?php echo base_url();?>assets/richte/editor.js" ></script>
<script>


$(document).ready(function() {
	var textarea = $("#highlight");
	textarea.Editor();
 });
 
 /*function toggle_highlight(){
	 $('#highlight_div').toggle();
 }*/
 function enterCurrentTabFun(){
	$(".tab-content .tab-pane.active").each(function(){
		if($(this).attr("id")=="step-21"){
			vendor_id=$("#vendors").val();
			//if($("#logistics_parcel_category").val()!=null){
			if(0){
				logistics_parcel_category_in=$("#logistics_parcel_category").val().join("-");
				
				$.ajax({
					url:"<?php echo base_url()?>admin/Catalogue/check_selection_in_all_parcel_categories",
					type:"post",
					data:"vendor_id="+vendor_id+"&logistics_parcel_category_in="+logistics_parcel_category_in,
					success:function(data){
						if(data=="no"){
							swal({
								title:"Info", 
								text:"Please select atleast one parcel category from logisitcs", 
								type: "info"
						}).then(function () {
							
						});
						$("#previous_button").click();
						}
						else{
							
						}
					}
				});
			}
	
		}		
	});
 }
 
 function calculateSelling_priceDiscountFun(){
 
        selling_price=$("#selling_price").val();
        max_selling_price=$("#max_selling_price").val();
        
        if(selling_price=="" || max_selling_price==""){
            $("#selling_discount").val("");
	}
        if(selling_price!="" && max_selling_price!=""){
            if(parseFloat(max_selling_price)>=parseFloat(selling_price)){
                wsp_dis=Math.round(((parseFloat(max_selling_price)-parseFloat(selling_price))/parseFloat(max_selling_price))*100).toFixed(2);
                $("#selling_discount").val(wsp_dis);
                $("#selling_discount_div").removeAttr("class");
		$("#selling_discount_div").attr("class","form-group label-floating is-focused");
                
            }else{
                swal({
                        title:"Error", 
                        text:"MRP should be greater or equal to WSP", 
                        type: "error",
                        allowOutsideClick: false
                }).then(function () {
                        $("#selling_discount").val(""); 
                });
               
            }
            //alert(wsp_dis);
	}
 }
  function calculateTax_priceFun(){
	 
        tax=$("#tax").val();
        msp=$("#max_selling_price").val();
        selling_price=$("#selling_price").val();
        
        if(selling_price!="" && tax!=""){
            
            //taxable_price=(parseFloat(msp)-parseFloat(tax_percent_price));
            taxable_price=(parseFloat(selling_price)/(1+parseFloat(tax)/100)).toFixed(2);
            tax_percent_price=(parseFloat(selling_price)-parseFloat(taxable_price)).toFixed(2);
            //alert(taxable_price);
            $("#tax_percent_price").val(tax_percent_price);
            $("#taxable_price").val(taxable_price);
            
            $("#tax_percent_price_div").removeAttr("class");
            $("#tax_percent_price_div").attr("class","form-group label-floating is-empty is-focused");
            $("#taxable_price_div").removeAttr("class");
            $("#taxable_price_div").attr("class","form-group label-floating is-empty is-focused");
        }
         /* calculate tax price and taxable vale */
 }

 /* Export inventory */

function export_inventory(table){

    $.ajax({
        url:"<?php echo base_url()?>admin/Catalogue/export_inventory",
        type:"post",
        data:"table="+table+"&active=0",
        success:function(data){
            if(data==true){

            } else{
                swal("Error", "Error in exporting inventories", "error");
            }
        }
    });
}
 /* Export inventory */
 
function draw_table(){
	 var gettingvalue=document.getElementById("sku_status").value;
	 
	  if(gettingvalue=="all"){
		document.getElementById("requestvalue").innerHTML="Number of All SKUs";
		
	  }
	  if(gettingvalue=="selected"){
		document.getElementById("requestvalue").innerHTML="Number of Selected SKUs"; 
		
	  }
	  if(gettingvalue=="unselected"){
		document.getElementById("requestvalue").innerHTML="Number of Unselected SKUs";  
		
	  }
	
        $('#loading').fadeIn();
	table.draw();
        $('#loading').fadeOut();
	
	
}
function export_as_csv(){

	var selected_list="";
	var selected_list_arr=new Array();
	var obj_arr=document.getElementsByName("selected_list");
	for(i=0;i<obj_arr.length;i++){
		if(obj_arr[i].checked){
			if(obj_arr[i].value!=''){
				selected_list_arr.push(obj_arr[i].value);
			}
		}
	}
	if(selected_list_arr.length==0){
		alert("Choose atleast one SKU!");
		return false;
	}
	
	selected_list=selected_list_arr.join(",");

	$('#export_inventory_selected').val(selected_list);
	$('#export_form').attr('action','<?php echo base_url(); ?>admin/Catalogue/inventory_all')
	$('#export_form').submit();
	$('#export_inventory_selected').val('');
	unselectAllFun();
}
function export_csv_single(inv_id){

	if(inv_id!=''){
		$('#export_inventory_selected').val(inv_id);
		$('#export_form').attr('action','<?php echo base_url(); ?>admin/Catalogue/inventory_all')
		$('#export_form').submit();
		$('#export_inventory_selected').val('');
	}

}

function view_status_modal(app_id,inventory_id,status){
	$("#app_id").val(app_id);
	$("#inventory_id").val(inventory_id);
	$("#status").val(status);
	$("#status_modal").modal("show");
}

function change_status(){

	app_id=$("#app_id").val();
	inventory_id=$("#inventory_id").val();
	status=$("#status").val();
	comments=$("#comments").val();

    if(app_id!='' && status!=''){
        
        swal.queue
([{
  title: '<p class="loading">Loading<span>.</span><span>.</span><span>.</span></p>',
  showCancelButton: false,
  showConfirmButton: false,
  allowOutsideClick: false,
  background: 'rgba(0,0,0,0)',
  onOpen: function()
  {
        $.ajax({
            url:"<?php echo base_url()?>admin/Catalogue/inventory_change_status",
            type:"POST",
            data:"vendor_id="+app_id+"&inventory_id="+inventory_id+"&status="+status+"&comments="+comments,
            dataType: "JSON",                
            asyc:false,
            success:function(data){
                if (parseInt(data.flag) == 1) {
                    swal(
                        data.message,
                        '',
                        'info'
                    ) 
                    drawtable();
                } 
            }
        });
        
         }
}]);
    }
    
}

function show_rejected_comments(){
	status=$("#status").val();
	if(status=='3'){
		$('#show_rejected_comments_div').show();
	}else{
		$('#show_rejected_comments_div').hide();
	}
}

function show_addon_product_count(id){
	$('#addon_'+id).toggle();
}
function change_limit(id){
	addon_selection_type=$('#addon_selection_type_'+id).val();
	
	if(addon_selection_type=='single'){
		$('#addon_product_allowed_'+id).val(1);
		$('#addon_product_allowed_'+id).attr('readOnly',true);
	}else{
		$('#addon_product_allowed_'+id).val('');
		$('#addon_product_allowed_'+id).attr('readOnly',false);
	}
}

function addon_product_allowed(id){
	
	val=$('#addon_product_allowed_'+id).val();
	addon_selection_type=$('#addon_selection_type_'+id).val();

	if(parseInt(val)>10){
		alert();
	}

	$.ajax({
			url: '<?php echo base_url()."admin/Catalogue/update_addon_product_allowed/";?>',
			type: 'POST',
			async:false,
			data:"id="+id+"&addon_product_allowed="+val+"&addon_selection_type="+addon_selection_type,	
	}).done(function(data){
			if(data){
				alert('Updated Successfully!');
				table.draw();

			}else{
				alert('Not updated');
			}

	});

}

 </script>
</body>
</html>
