<html lang="en">
<head>
<title>Add Delivery Staff</title>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrapValidator.css"/>
<link href="<?php echo base_url();?>assets/css/datepicker.css" rel="stylesheet" /> 
<style>
#datetimePicker .has-feedback .form-control-feedback {
top: 0;
right: -20px;
}
#datetimePicker .has-feedback .input-group .form-control-feedback {
top: 0;
right: -30px;
}
[required]{
border-left:5px solid red;
}
</style>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrapValidator.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>	
<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js" ></script>
<script type="text/javascript">
// When the document is ready
$(document).ready(function () {
$('#dob').datepicker({
format: "dd/mm/yyyy"
});  

});
</script>

<script>
$(document).ready(function(){	
    $('#edit_subadmins_form').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'The Name is required and cannot be empty'
                    },
					stringLength: {
                        min: 3,
                        max: 15,
                        message: 'The Name must be more than 3 and less than 15 characters long'
                    },
                    regexp: {
                        regexp: /^[A-Z\s]+$/i,
                        message: 'The Name can only consist of alphabetical characters and spaces'
                    }
                }
            },
			email: {
				validators: {
					notEmpty: {
						message: ''
					},
					emailAddress: {
						message: ''
					},
					remote: {
						type: 'POST',
						url: '<?php echo base_url()?>admin/Admin_manage/check_email_subadmins_edit',
						message: 'The email is already in use',
						delay: 2000
					}
				}
			},
			mobile: {
				validators: {
					digits: {
						message: 'The value can contain only digits'
					},
					stringLength: {
						min: 10,
						max: 12,
						message: 'The Mobile must be more than 10 and less than 12 digits long'
					},
					notEmpty: {
						message: 'The Mobile must not be empty'
					},
					remote: {
						type: 'POST',
						url: '<?php echo base_url()?>admin/Admin_manage/check_mobile_subadmins_edit',
						message: 'The mobile is already in use',
						delay: 2000
					}
				}
            },
			landline: {
				validators: {
					digits: {
						message: 'The value can contain only digits'
					},
					stringLength: {
						min: 10,
						max: 15,
						message: 'The Landline must be more than 10 and less than 15 digits long'
					}
				}
            },
			dob: {
                validators: {
                    notEmpty: {
                        message: 'The DOB is required'
                    }
                }
            },
			user_type: {
                validators: {
                    notEmpty: {
                        message: 'The User Type is required'
                    }
                }
            },
			panel_access: {
                validators: {
                    notEmpty: {
                        message: 'The Panel Access is required'
                    }
                }
            }
        }
    })
	.on('success.form.bv', function(e) {
	
		
    var form = $('#edit_subadmins_form');
	form.submit(function(event){
		event.preventDefault();
		
				var valuesToSubmit = $(this).serialize();
				
				$.ajax({
						url: "<?php echo base_url();?>admin/Admin_manage/edit_subadmins_action",
						type:"POST",
						data: new FormData(this),
						contentType: false,      
						cache: false,				
						processData:false,
						beforeSend: function(){
							$("#edit_subadmin_profile_btn").html('<i class="fa fa-refresh fa-spin"></i> Processing');
						}
					}).done(function(data){
						//alert(data);
						$("#edit_subadmin_profile_btn").html('Submit');
						//alert(data);
						if(data==true){
							alert("profile updated successfully!");
							location.href="<?php echo base_url();?>admin/Admin_manage/view_subadmins";
						}
						else{
							alert("profile not yet updated!");
						}
						
					});
				
	});
   });
		 });
</script>
</head>
<body>
<?php
	//print_r($delivery_staff);
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="page-header"><h4>Add Sub Admin For Country</h4></div>
<div class="col-sm-12">
<form id="edit_subadmins_form" enctype="multipart/form-data" class="form-horizontal" method="post">
<input type="hidden" class="form-control" name="edit_subadmin_user_id"  value="<?php echo $edit_subadmin_user_id;?>">
<div class="form-group">
<div class="col-sm-6">
<input type="text" class="form-control" name="name" placeholder="Enter Name" required value="<?php echo $delivery_staff->name;?>">
</div>
</div>
<div class="form-group">
<div class="col-sm-6">
<input type="text" class="form-control" name="email" placeholder="Enter Email" required value="<?php echo $delivery_staff->email;?>">
</div>
</div>
<div class="form-group">
<div class="col-sm-6">
<input type="text" class="form-control" name="mobile" placeholder="Enter Mobile" required value="<?php echo $delivery_staff->mobile;?>">
</div>
</div>
<div class="form-group">
<div class="col-sm-6">
<input type="text" class="form-control" name="landline" placeholder="Enter Landline" value="<?php echo $delivery_staff->landline;?>">
</div>
</div>
<div class="form-group">
<div class="col-sm-6">
<input type="radio" name="gender"  value="Male" <?php if($delivery_staff->gender=="Male"){ echo "checked";}?>> Male
<input type="radio" name="gender"  value="Female" <?php if($delivery_staff->gender=="Female"){ echo "checked";}?>> Female
</div>
</div>
<div class="form-group">
<div class="col-sm-6">
<input type="text" class="form-control" name="dob" id="dob"  placeholder="Date of Birth" required value="<?php echo $delivery_staff->dob;?>">
</div>
</div>
<div class="form-group">
	<div class="col-sm-6">
		<select name="user_type" class="form-control" required>
			<option value=""  <?php if($delivery_staff->user_type==""){ echo "selected";}?>>-Select User Type-</option>
			<option value="Sub World"  <?php if($delivery_staff->user_type=="Sub World"){ echo "selected";}?>>Sub Admin For World</option>
			<option value="Master Country"  <?php if($delivery_staff->user_type=="Master Country"){ echo "selected";}?>>Master Admin For Country</option>
			<option value="Sub Country"  <?php if($delivery_staff->user_type=="Sub Country"){ echo "selected";}?>>Sub Admin For Country</option>
		</select>
	</div>
</div>
<div class="form-group">
<div class="col-sm-6">
	<select name="panel_access[]" class="form-control" required multiple>
			<option value="1" <?php if(in_array("1",explode(",",$delivery_staff->panel_access))){ echo "selected";}?>>Orders</option>
			<option value="2" <?php if(in_array("2",explode(",",$delivery_staff->panel_access))){ echo "selected";}?>>Complains</option>
			<option value="3" <?php if(in_array("3",explode(",",$delivery_staff->panel_access))){ echo "selected";}?>>Finance</option>
		</select>
</div>
</div>




<div class="form-group">
<div class="col-sm-6">
<input type="file" name="profile_pic"  id="profile_pic" class="form-control"/>
</div>
<img src="<?php echo base_url().$delivery_staff->profile_pic; ?>" width="50" height="50">
</div>

<div class="form-group">
<div class="col-sm-3">
<button type="submit" class="col-md-12 btn btn-success btn-lg" id="edit_subadmin_profile_btn">Submit</button>
</div>
</div>
</form>
</div>
</div>
</div>
</body>

</html>