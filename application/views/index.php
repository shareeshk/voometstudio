<?php 
		$bg="";
		$ft="";
		if(!empty($ui_arr["bg_img"])){
			$bg=$ui_arr["bg_img"]->background_image;
		}
		if(!empty($ui_arr["ft_img"])){
			$ft=$ui_arr["ft_img"]->background_image;
		}
		$bg="assets/data/ui/88640.jpeg"; // hardcoded on 28th sep 2023
		$ft="assets/data/ui/59809.jpeg"; // hardcoded on 28th sep 2023
	?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/skins/jquery-ui-like/progressbar.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/product_comparision/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/product_comparision/w3.css" />
<style type="text/css">
	
	.product-name a {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
}
@media only screen and (max-width: 600px){
.heightmatched{
font-size: 1.0em !important;
}
}
/* compare checkbox*/
checkbox label:after
{
content: '';
display: table;
clear: both;
}

.checkbox .cr
{
position: relative;
display: inline-block;
border: 1px solid #a9a9a9;
border-radius: .25em;
width: 1.3em;
height: 1.3em;
float: left;
margin-right: .5em;
}


.checkbox .cr .cr-icon
{
position: absolute;
font-size: .8em;
line-height: 0;
top: 50%;
left: 20%;
}

.radio .cr .cr-icon {
margin-left: 0.04em;
}
.checkbox label{
padding-left: 0px;
line-height: 1.3;
}
.checkbox label input[type="checkbox"]{
display: none;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
transform: scale(3) rotateZ(-20deg);
opacity: 0;
transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
transform: scale(1) rotateZ(0deg);
opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
opacity: .5;
}
/* compare checkbox*/
.compare_close{
margin-top: -8px;
background-color: #a39999;
border-radius: 11px;
width: 18px;
height: 18px;
line-height: 17px;
text-align: center;
color: #f9ecec;
text-decoration: none;
float: right;
font-size: 18px;
}
.compare_lineheight{
line-height:80px;
}
.comparePanle{
width:100%;
background-color: #fff;
border-top: 1px solid #eee;
bottom:0px;
padding: 10px;
}
.horizontal_bar{
width:60%;float:left;margin-bottom: 10px;
color:#ff9900;
}
.star_rate_1{
float:left;width:100%;
}
.star_rate_2{
float:left;width:30%;
}
.star_rate_3{
float:left;width:10%;
}
.star_rate{
width: 30%;
float: left;
margin-right: 6px;
}
.popover {
width: 700px;
}
div.rater{
width:200px;
min-height:40px; 
background: black;
color:white;
padding:5px;
position:relative;
word-wrap:break-word;
-moz-border-radius:5px; 
-webkit-border-radius:5px;
border-radius:5px;
margin-bottom:2em;
margin-top:8px;
}
div.rater:after{
content:'';
display:block;
position:absolute;
top:-20px;
left:30px;
width:0;
height:0;
border-color: transparent transparent black transparent;
border-style: solid;
border-width: 10px;
}

.img-cat-display{
width:300px;height:366px;
}
/*.display-sortP-option{
position: absolute;
top: 0;
right: 63px;
min-width: 200px;
height: 30px;
margin-top: 0px;
margin-right: 5px;
}
#top-sortPagi .sortPagiBar .sort-product,#top-sortPagi .sortPagiBar .show-product-item {
margin-left: 21px;
}
@media (max-width: 480px) {
.display-sortP-option{
position: relative;
top: 0;
right: 0px;
min-width: 200px;
height: 30px;
margin-top: -10px;
margin-right: 0px;
margin-bottom: 10px;
}
}*/
#WarningModal{
z-index:9999;
}
.line-break{
margin-top:10px;
margin-bottom:0;
}
body{
/* background-color: #f1f3f6; */
background-color: #fff; 

}
.breadcrumb{
padding: 15px 0 0 15px;
}
@media (min-width: 768px) {
.center_column{
padding-left:0;
}

}

.list-inline li{
line-height:24px;	
}
.list-inline li a{
cursor:pointer;
}
.list-inline li a.active{
font-weight:600;
color:#eda900;
}
.product-name a{
color:#000;
}
.product-name a:hover{
color:#777;
}
.attribute-col{
color:#777;
}
.content_price{
padding-top:10px;
}
</style>

<style>
.services2 .services2-item,.services2{
border:0px;
}
.services2 .services2-item .image{
padding-left:120px;
}
.form-group .help-block {
display: none;
}
.form-group.has-error .help-block {
display: block;
}


.well{
-webkit-box-shadow: 0 8px 20px rgba(0, 0, 0, 0.1);
-moz-box-shadow: 0 8px 20px rgba(0, 0, 0, 0.1);
box-shadow: 0 8px 20px rgba(0, 0, 0, 0.1);
background-color:#f5f5f5;
transition:all 0.3s ease-in-out 0s;
}
.assignproducts_section{
background-color:#ffffff;
padding:2rem 0 2rem 0;
}
.block-trending{
margin-top:0px !important;
}
</style>

	
	<style type="text/css">

	.bodyimage {
	<?php if($bg!=''){
		?>
	background: url('<?php echo base_url().$bg;?>') no-repeat;	
	
		<?php 
	}else{
		?>
		background:#b3c95c;
		<?php
		
	}?>
	background-size: 100% auto;
	  z-index:-100;
	}
	</style>
	
	
	<style type="text/css">
.form-group .help-block {
  display: none;
}
 
.form-group.has-error .help-block {
  display: block;
}
</style>

<style>

.rfq_input{
    height: 38px;
    line-height: 38px \0;
    padding: 0 12px;
    font-size: 16px;
    border-radius: 5px;
}
.rfq_btn{
    display: inline-block;
    padding: 7px 20px;
    background: #f60;
    border-radius: 18px;
    font-size: 16px;
    color: #fff;
    margin-top: 15px;
}
.menuIcon img:active {
  transform: scale(0.9);
}
.carousel-indicators .active{
	background-color:#1429a0;
	border:1px solid #1429a0;
}
.carousel-indicators li{
	background-color:#e1e1e1;
	border:1px solid #e1e1e1;
}
.parentimageslider {
  position:relative;
}

.percent{
	text-align: initial;
    font-weight: 700;
    font-style: normal;
    text-decoration: none;
    opacity: 1;
    mix-blend-mode: normal;
    font-size: 14.1771px;
    color: rgb(162, 141, 117);
}
.shopnow_class{
	border: 2px solid #000;
    border-radius: 20px;
    width: 100px;
    text-align: center;
    margin-top: 20px;
	margin-top: 20px;
    font-weight: bold;
}


@media (min-width: 768px) {
	.childheadingbanner{
		padding:45px;
		margin-top:0rem;
		position:absolute;
		z-index:200;
		right:10%;
		background-color:#fff;
		/* padding:0.5rem 2rem; */
		/* border-radius:0rem 0rem 1rem 1rem; */
		top: 30%;
		bottom: 0%;
		left: 10%;
		width: 25%;

		
		opacity: 1;
		mix-blend-mode: normal;
		
		background: rgb(231, 208, 182);

		border-radius: 5px;
		line-height:40px;
		letter-spacing: 0px;
		z-index: 11;
		text-align: initial;
    font-weight: 500;
    font-style: normal;
    text-decoration: none;

	}
	.childheadingbanner h2{
		font-weight:600;
	}
	
	
	.whyimage{
		padding:0px;
		height:400px;
		
		
	}
	
	.whysamric{
		padding:5rem 3rem 3rem 3rem;
		
	}
	.whysamric h4{
		margin-bottom:2rem;
		color:#052670;
	}
	
	.whyimage .whysamriccard{ 
		margin:1rem 0rem;
		padding:1rem;
		border-radius:1rem;
		font-weight:700;
		background-color:#8da6c9;
		box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
		color:#052670;
	}
	
}
@media (max-width: 767px) {
	.childheadingbanner{
		margin-top:0rem;
		position:absolute;
		right:17%;
		z-index:200;
		background-color:#fff;
		padding:0.5rem 2rem;
		border-radius:0rem 0rem 0.5rem 0.5rem;
	}
	.childheadingbanner h2{
		font-weight:600;
		font-size:1.5rem;
	}
	
	.whyimage{
		padding:0px;
		
		--s: 20px; /* control the size */
		  --c1:#c2def0;
		  --c2:#c2d5f0;
		  
		  background:
			repeating-conic-gradient(var(--c1) 0 25%,#0000 0 50%)
			 0 0/calc(4*var(--s)) calc(2*var(--s)),
			conic-gradient(#0000 50%,var(--c2) 0)
			 calc(var(--s)/2) 0/calc(2*var(--s)) 1%,
			radial-gradient(var(--c2) 70%,var(--c1) 72%)
			 0 0/var(--s) var(--s)
	}
	
	.whysamric{
		padding:3rem 1rem 1rem 1rem;
	}
	
	.whysamric h4{
		margin-bottom:2rem;
		color:#052670;
	}
	
	.whyimage .whysamriccard{ 
		margin:1.5rem 5rem;
		border-radius:1rem;
		padding:0.5rem 0rem;
		font-weight:700;
		background-color:#8da6c9;
		box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
		color:#052670;
	}
	
}

.tit_text{
		font-size:34px;
	}

/* responsive design  */
.carousel-inner{
		height: 500px;
		min-height: 450px;
	}
@media only screen and (max-width: 600px) {
.rb-dd-banner-subtitle1_new{
	width: 100% !important;
}
	.subscribe
{
	background: #ff6f00;
    padding: 10px !important;
	margin-right: auto;
    margin-left: auto;
    position: relative;
	display: inline-block !important;
    -webkit-box-pack: center;
	width:100%;
}
	.shopnow_class{
	border: 2px solid #000;
    border-radius: 20px;
    width: 90px;
    text-align: center;
    margin-top: 5px;
	margin-top: 5px;
    font-weight: bold;
	font-size: 12px;
}
	.percent{
	text-align: initial;
    font-weight: 700;
    font-style: normal;
    text-decoration: none;
    opacity: 1;
    mix-blend-mode: normal;
    font-size: 12.1771px;
    color: rgb(162, 141, 117);
}
	.tit_text{
		font-size:16px;
	}
	.childheadingbanner{
		padding:10px;
		margin-top:0rem;
		position:absolute;
		z-index:200;
		right:10%;
		background-color:#fff;
		/* padding:0.5rem 2rem; */
		/* border-radius:0rem 0rem 1rem 1rem; */
		top: 10%;
		bottom: 10%;
		left: 4%;
		width: 40%;
		opacity: 1;
		mix-blend-mode: normal;
		
		background: rgb(231, 208, 182);

		border-radius: 5px;
		line-height:20px;
		letter-spacing: 0px;
		z-index: 11;
		text-align: initial;
    font-weight: 500;
    font-style: normal;
    text-decoration: none;

	}
	.childheadingbanner h2{
		font-weight:600;
	}
	.carousel-inner{
		height: 100%;
		min-height: 100%;
	}
	
}

/* responsive design  */

/* tabs */
.nav-tabs {
    border-bottom: 1px solid transparent;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
	border:none;
}

.nav-tabs .nav-item.show .nav-link, .nav-tabs li.active {
    color: #ff6600;
    background-color: transparent;
    border-color: transparent transparent #f3f3f3;
    border-bottom: 3px solid !important;
    font-size: 16px;
    font-weight: bold;
}
.nav-tabs>li>a {
    margin-right: 2px;
    line-height: 1.42857143;
    border: 1px solid transparent;
    border-radius: 4px 4px 0 0;
    font-size: 16px;
    text-transform: uppercase;
	cursor: pointer;
}
/* tabs */


</style>



<script>
    base_url="<?php echo base_url();?>";
</script>


<div class="bodyimage1" id="bodyimage">
	<div class="container-fluid" style="padding:0" align="center">
<section class="section8  parallax">



    <div class="section-container" style="display:block;">
        <div class="col-md-12 col-xl-12 col-12 parentimageslider" style="padding:0px;">
			
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				
				<ol class="carousel-indicators">
				  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				  <li data-target="#myCarousel" data-slide-to="1"></li>
				</ol>

				
				<div class="carousel-inner" >

				  <div class="item active">
					<img src="<?php echo base_url();?>assets/images/slider1.png" alt="Los Angeles" width="100%" class="img-responsive">
					<div class="childheadingbanner">
				
				<!--new section -->
				<div class="rb-wrapper rb-in-out" class="rb-layer" src="https://cdn.shopify.com/s/files/1/0550/8336/8541/files/slider2-3.png?v=1635393688" alt="" data-rb-slidein="1" data-rb-slideout="1"></div>
				<img style=" margin: 0px; z-index: auto; width: 36.8604px; height: 25.5188px; padding: 0px; border-width: 0px; border-radius: 0px;" class="rb-layer" src="https://cdn.shopify.com/s/files/1/0550/8336/8541/files/slider2-3.png?v=1635393688" alt="">
					<div class="percent">300+ Designs</p></div>
					<div class="tit_text">Sesom <br><strong>Reclining Sofa</strong></p></div>
					<div class="shopnow_class">Explore</div>

					
				<!--new section -->
			</div>
				  </div>

			<div class="item">
					<img src="<?php echo base_url();?>assets/images/slider2.png" alt="Chicago" width="100%" class="img-responsive">
					<div class="childheadingbanner" style="background-color:#a0897a;">
				
				<!--new section -->
				<div class="rb-wrapper rb-in-out" class="rb-layer" src="<?php echo base_url();?>assets/images/kitchen-icon.png" alt="" data-rb-slidein="1" data-rb-slideout="1"></div>
				<img style=" margin: 0px; z-index: auto; width: 36.8604px; height: 25.5188px; padding: 0px; border-width: 0px; border-radius: 0px;" class="rb-layer" src="<?php echo base_url();?>assets/images/kitchen-icon.png" alt="">
					<div class="percent" style="color:#000;">45+ Designs</p></div>
					<div class="tit_text">Voomet <br><strong>Modular Kitchen</strong></p></div>
					<div class="shopnow_class">Explore</div>

					
				<!--new section -->
			</div>
				  </div>
				
			  
				</div>
			  </div>
			  
  
        </div>
		 <div class="col-md-3 col-xl-3 col-12 whyimage" style="display:none;">
				<div class="whysamric">
					<h4><strong>Why Voomet Studio?</strong></h4>
					<div class="row">
						<div class="col-md-6">
							<div class="card whysamriccard">Design Lead</div>
						</div>
						<div class="col-md-6">
							<div class="card whysamriccard">Economical</div>
						</div>
					
						<div class="col-md-6">
							<div class="card whysamriccard">Low Maintenance</div>
						</div>
						<div class="col-md-6">
							<div class="card whysamriccard">Heavy Duty</div>
						</div>
					
						<div class="col-md-6">
							<div class="card whysamriccard">Wide Range</div>
						</div>
						<div class="col-md-6">
							<div class="card whysamriccard">Full Stack Support</div>
						</div>
					</div>
				</div>
        </div>
    </div>
</section>

<div class="">
<?php include('delivered_fast.php'); ?>
</div>

</div>
    </div>

<?php
/*
?>
<div class="bodyimage" id="bodyimage">
	<div class="container-fluid" align="center">
	<img src="<?php echo base_url().$bg;?>">
	</div>
</div>
<?php
*/
?>


<?php if(!empty($ui_arr["testi"])){
$testi=$ui_arr["testi"];
?>
<section class="section8 block-testimonials parallax">
<div class="section-container">
<h3 class="section-title">Testimonials</h3>
<div class="section-content">
<div class="container-fluid">
<div class="testimonial-wapper">
<div class="testimonials">
<ul class="testimonial testimonial-carousel">
<?php foreach($testi as $tes){ ?>
<li>
<div class="testimonial-image">
<a href="#"><img src="<?php echo base_url().$tes->image;?>" alt="testimonials"></a>
</div>
<div class="info">
<p><?php echo html_entity_decode($tes->content);?></p>
<p class="testimonial-nane"><?php echo $tes->name; ?> - <?php echo $tes->designation; ?></p>
</div>
</li>
<?php } ?>
</ul>
</div>
</div>
<div class="testimonial-caption"></div>
</div>
</div>
</div>
</section>
<?php } ?>

<!-- Top banner -->
<div class="margin100"></div>

<?php 
$content=$ui_arr["post_to_frontend_blogs"];
if(!empty($content)){ ?>
<div class="block-banner8" id="why_content">
    <div class="container">
		<div class="section8">
		 <h3 class="section-title section-long">Blogs</h3>
		</div>
		
		<div class="row content-gap mx-auto">
		
			<?php 
			
		if(!empty($content)){
			foreach($content as $cont){
			?>
			
			<div class="col-md-4 text-center margin-bottom">
			<div class="card" style="background-color:#fff;box-shadow: rgba(0, 0, 0, 0.15) 0px 5px 15px 0px;">
				<div class="thumbnail">
					<?php
						if($cont["blog_image"]!=""){
							$blog_image=base_url().$cont["blog_image"];
						}
						else{
							$blog_image=base_url()."assets/blog_images/missing.png";
						}
					?>
					<img src="<?php echo $blog_image; ?>" src="" class="img-responsive" />
					  
				</div>
				<a target="_blank" style="margin-bottom:10px;" href="<?php echo base_url().'get_blog_post/'.$cont['slug'].'/'.$cont['customer_id'] ?>" class="btn btn-default btn-sm">Read More</a>
			  </div>
			</div>
			<?php }
			}	
		?>	
		</div>
		</div>
   
</div>
<?php } ?>
<!-- ./ top banner -->



<!-------- ourproducts section starts -------------->
<?php 
$products_obj=$ui_arr["invs_ourproducts"];
?>
<?php if(!empty($products_obj)){ ?>

<section class=" section8" id="products_range">
<div class="section-container">

<div class="section-content">
<div class="">

<style>
.rb-dd-banner-content1 {
    padding: 20px 15px 30px;
    left: 40px;
    right: 40px;
    margin: 0 auto;
    text-align: center;
    position: absolute;
    z-index: 9;
    top: 35%;
    left: 10%;
    transform: translateY(-50%);
}
.rb-dd-banner-subtitle1_new{
	width: 40% ;
}
.rb-dd-banner-subtitle1{
		
	color: #000;
    padding-bottom: 20px;
    position: relative;
    margin-bottom: 28px;
    display: block;
    font-weight: 700;
    border-bottom: 2px solid #ff6f00;
    width: 40%;
    float: left;
}
.rb-dd-banner-title2{
	width: 100%;
	float: left;
	margin: 0 0 15px;
    line-height: 1.2;
	font-size: 40px;
	font-weight: 500;
	color: #000;
	text-align: left;
}
.rb-dd-banner-description1{
	width: 100%;
	float: left;
	font-size: 22px;
    font-weight: 700;
    margin-bottom: 30px;
    text-transform: uppercase;
	color: #000;
	text-align: left;
}
.rb-dd-banner-description1 span{
	color: #ff6f00;
}
.mt-20{
	margin-top: 20px;
}
.new_title{
	font-size: 20px;
	font-weight: 700;
	text-transform: uppercase;
}
.rb-button2 {
    color: #000;
    background: transparent;
    font-weight: 700;
    border: 2px solid #000;
	font-size: 16px;
    line-height: 38px;
    display: inline-block;
    padding: 0 28px;
	position: absolute;
    bottom: 0%;
    text-align: left;
    left: 5%;
    border-radius: 20px;
}
.bg-img{
	background:url("<?php echo base_url() ?>assets/images/footer_banner.jpg") no-repeat center center;
    background-size:cover;
    /* width:100%; */
}
.product-name{
	line-height:20px;
}

</style>
<div class="col-md-4 text-left">
	<!---footer_banner.jpg-->
	<img src="<?php echo base_url() ?>assets/images/banner2.png" style="height: 445px;
    width: 430px;">
	<div class="rb-dd-banner-content1">
		<span class="rb-dd-banner-subtitle1 rb-dd-banner-subtitle1_new" style="text-transform:capitalize;">New Arrivals</span>
		<h4 class="rb-dd-banner-title2">
			Living furniture your
			love</h4>
		<div class="rb-dd-banner-description1">
			Sale up top <span>20%
				off</span>
			</div>

			<div class=" rb-button2 btn">
				<span class="rb-button-text">Shop now</span>
			</div>
		<div>
			
		</div>
	</div>
</div>

<div class="col-md-8" style="background-color:#fff;">

<h3 class="mb-20 mt-20 text-left" style="text-transform:capitalize;">NEW ARRIVALS</h3>

<style>
.nav-tabs li a.active {
    border-bottom: none;
    color: #000;
    background: transparent;
}
.nav-tabs li a:before {
    height: 2px;
    background: #ff6f00;
}
.nav-tabs li a.active:before {
    width: 100%;
}
.nav-tabs>li {
    float: right;
}

</style>
<!---tab--->
<div class="project-tab">

	<ul class="nav nav-tabs" id="myTab">
	<?php 

$products_obj_new=provide_parent_category();
krsort($products_obj_new);
?>
<?php 

$l=1;

if(!empty($products_obj_new)){
	
	foreach($products_obj_new as $key=>$products){
		$active1='active';
		if($l==count($products_obj_new)){
			$active1='active';
		}else{
			$active1='';
		}
		?>
		<li class="<?php echo $active1; ?>"><a data-target="#<?php echo ($l); ?>" data-toggle="tab" style="text-transform:capitalize;"><?php echo $products->pcat_name; ?></a></li>
		<?php
		$l++;
	}
}
	
	?>

		
		<!-- <li class="pull-right"><a data-target="#2" data-toggle="tab">Office Furniture</a></li> -->
	</ul>

	<div class="tab-content">
	<?php if(!empty($products_obj_new)){
	$j=1;
	foreach($products_obj_new as $key=>$products){
		
		if($j==count($products_obj_new)){
			$active='active';
		}else{
			$active='';
		}
		$products_obj =get_all_inventory_data_by_pcat($products->pcat_id);
		
		?>
		<div class="tab-pane <?php echo $active; ?>" id="<?php echo ($j); ?>">  <?php //echo $key; ?>

<ul class="row product-list grid">

<?php 
$j++;
$k=0;
foreach($products_obj as $pro){
$inventory_id=$pro->id;
$product_id=$pro->product_id;
$selling_price=$pro->selling_price;
$max_selling_price=$pro->max_selling_price;
$selling_discount=$pro->selling_discount;

//$product_name=$pro->product_name;
//$product_description=$pro->product_description;

$inv_discount_data=$controller->get_default_discount_for_inv($inventory_id,$max_selling_price,$product_id);
///////////////  No of Offers starts ///////////////
$promo_numbers=0;
$promotions=$controller->get_number_of_offers($inventory_id);
$inventory_product_info_obj=$controller->get_inventory_product_info_by_inventory_id($inventory_id);

if(isset($inventory_product_info_obj->stock_available)){
	$stock_available=$inventory_product_info_obj->stock_available;
}else{
	$stock_available=0;
}

if(!empty($promotions) && ($stock_available>0)){
$i=1; 
$defaultDiscounts_num=0;
foreach($promotions as $promotion){
foreach($promotion as $promo){

if (preg_match_all('/\d+(?=%)/', $promo-> get_type, $match)&&($promo->to_buy==1)){
$i++; 
if($promo->to_buy==1){
$defaultDiscounts_num=1;
}
}
}
}

if($defaultDiscounts_num!=0){
$promo_numbers=count($promotions)-$defaultDiscounts_num;
}else{
$promo_numbers=count($promotions);
}

}
//////////////   No of offers ends    ///////////////

//print_r($inv_discount_data);

$pro_name=($pro->sku_name!='') ?$pro->sku_name : $pro->product_name;
?>
<li class="col-xs-6 col-sm-3">
<div class="product-container selectProduct" data-id="<?php echo ucwords($pro_name)?>" data-title="<?php echo $inventory_id;?>">
<div class="left-block">
<?php 
$rand_1=$controller->sample_code();
$rand_2=$controller->sample_code();
?>
<a href="<?php echo base_url()?>detail/<?php echo "{$rand_1}{$inventory_id}";?>">


<img class="img-responsive productImg_<?php echo $inventory_id;?>" alt="productsku" src="<?php echo base_url().$pro->common_image;?>"/>

</a>

<div class="quick-view">
<?php 
if($this->session->userdata("customer_id")){ 
if($controller->check_wishlist($inventory_id)>0){
?>
<script>
$(document).ready(function(){
$("#wishlist_ourproducts_"+<?php echo $inventory_id;?>).css({"color":""});
$("#wishlist_ourproducts_"+<?php echo $inventory_id;?>).css({"color":"#ff4343;"});
});
</script>
<?php
$val="Added to Wishlist";
$style_wishlist_label="style='color:#eda900;'";
}
else{ 
$val="Add to Wishlist";
$style_wishlist_label="";
}
}else{
$val="Add to Wishlist";
$style_wishlist_label="";
}
?>
<?php
if($this->session->userdata("customer_id")){
?>
<span style="cursor:pointer;" id="anchor_ourproducts_<?php echo $inventory_id;?>" onclick="add_to_wishlistFun(<?php echo $inventory_id;?>,'ourproducts');return false;"  title="<?php echo $val; ?>" class="wishlist"><i id="<?php echo "wishlist_ourproducts_".$inventory_id;?>" class="fa fa-heart" style='color:#c2c2c2;font-size: 1.2em;'></i></span>
<?php
}
else{
?>
<span style="cursor:pointer" id="anchor_ourproducts_<?php echo $inventory_id;?>" onclick="add_to_wishlistFun(<?php echo $inventory_id;?>,'ourproducts');return false;"  title="<?php echo $val; ?>" class="wishlist"><i id="<?php echo "wishlist_ourproducts_".$inventory_id;?>" class="fa fa-heart" style='color:#c2c2c2;font-size: 1.2em;'></i></span>
<?php	
}
?>


</div>
<?php if(!empty($inv_discount_data)){ ?>
<div class="price-percent-reduction2" <?php if($inv_discount_data['discount']<10){echo "style='padding-top:18px'";}?>>
<?php echo  $inv_discount_data['discount'] ?>% OFF



</div>
<?php }else{?>
                                                            <?php
                                                            if($selling_discount>0){
                                                            ?>
                                                                <div class="price-percent-reduction2" <?php if ($selling_discount < 10) {
                                                                echo "style='padding-top:18px'";
                                                            } ?>>
                                                                <?php echo round($selling_discount); ?>% OFF
                                                            </div>

                                                            <?php
                                                            }
                                                            ?>


<?php
}
if($promo_numbers>0){ 
?>
<div class="price-percent-reduction_offers">
<?php

if($promo_numbers>1){echo $promo_numbers.' Offers';}else{echo $promo_numbers.' Offer';}
?>
</div>
<?php
}
?>



</div>
<?php
/*
?>
<div class="product-info text-center">
<div class="product-name">
<a href="<?php echo base_url()?>detail/<?php echo "{$rand_1}{$inventory_id}";?>"><?php echo $pro->product_name;?></a>
</div>
<div class="button-control">

<a title="Add to Cart" class="btn btn-default" href="<?php echo base_url()?>detail/<?php echo "{$rand_1}{$inventory_id}";?>">Explore</a>

</div>

</div>
<?php
*/
$pro_name=($pro->sku_name!='') ?$pro->sku_name : $pro->product_name;
?>



<div class="right-block heightmatched_quickbuy">
<div class="row mb-10">
<div class="col-xs-12 col-sm-12">
<h5 class="product-name heightmatchedforquickbuy" style="font-size:1.05em;padding-bottom:0!important;word-break:break-word;line-height:1.3;"><a href="<?php echo base_url()?>detail/<?php echo "{$rand_1}{$inventory_id}";?>"  title="<?php
$str=$pro_name;
$str=str_replace("\r\n", '', $str);
$str=str_replace('\r\n', '', $str);
echo ucwords($str);
?>">

<?php
$str=$pro_name;
$str=str_replace("\r\n", '', $str);
$str=str_replace('\r\n', '', $str);
echo ucwords($str);
?>

<?php
$data=$controller->get_inventory_info_by_inventory_id($inventory_id);
?>

<!---added attributes ---->
<span style="<?php echo ($pro->sku_name!='') ? 'display:none;' : ''; ?>">
<?php
$color_attribute_is_there="no";
if(strtolower($data['attribute_1'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_1_value'])[0];
}
if(strtolower($data['attribute_2'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_2_value'])[0];
}
if(strtolower($data['attribute_3'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_3_value'])[0];
}
if(strtolower($data['attribute_4'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_4_value'])[0];
}
?>
<?php
if($color_attribute_is_there=="yes"){
if(strtolower($data['attribute_1'])!="color"){
if($data['attribute_1_value']!=""){
echo " - ".$data['attribute_1_value'];
}
}
if(strtolower($data['attribute_2'])!="color"){
if($data['attribute_2_value']!=""){
echo " - ".$data['attribute_2_value'];
}
}
if(strtolower($data['attribute_3'])!="color"){
if($data['attribute_3_value']!=""){
echo " - ".$data['attribute_3_value'];
}
}
if(strtolower($data['attribute_4'])!="color"){
if($data['attribute_4_value']!=""){
echo " - ".$data['attribute_4_value'];
}
}
}
else{
$noncolor_attribute_first_is_there="no";
$noncolor_attribute_second_is_there="no";

if(strtolower($data['attribute_1'])!="color"){
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_1_value'];
}
if(strtolower($data['attribute_2'])!="color"){
if($noncolor_attribute_first_is_there=="yes"){
$noncolor_attribute_second_is_there="yes";
if($data['attribute_2_value']!=""){
echo " - ".$data['attribute_2_value'];
}
}
else{
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_2_value'];
}
}
if(strtolower($data['attribute_3'])!="color"){
if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
if($noncolor_attribute_first_is_there=="yes"){
$noncolor_attribute_second_is_there="yes";
if($data['attribute_3_value']!=""){
echo " - ".$data['attribute_3_value'];
}
}
else{
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_3_value'];
}
}
else{
	if($data['attribute_3_value']!=''){
		echo " - ".$data['attribute_3_value'];
	}
}
}
if(strtolower($data['attribute_4'])!="color"){
if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
if($noncolor_attribute_first_is_there=="yes"){
$noncolor_attribute_second_is_there="yes";
if($data['attribute_4_value']!=""){
echo " - ".$data['attribute_4_value'];
}
}
else{
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_4_value'];
}
}
else{
	if($data['attribute_4_value']!=''){
		echo " - ".$data['attribute_4_value'];
	}
}
}


}
$color_attribute_is_there="no";
$noncolor_attribute_first_is_there="no";
$noncolor_attribute_second_is_there="no";
?>
</span>
<!---added attributes ---->

</a>
</h5>
</div>
</div>
<!---ratings---added------------->								
<div class="row mb-15">
<div class="col-xs-12 col-sm-12">
<?php

$inventory_id=$inventory_id;
$rate_number=$controller->number_of_ratings($inventory_id);
$number_ratings_one_to_five=$controller->number_ratings_one_to_five($inventory_id);
$total_ratings=$rate_number;

if($total_ratings!=0){
foreach($number_ratings_one_to_five as $rating=>$rating_count){
if($rating=="one"){
$one_rate_percent=($rating_count/$total_ratings)*100;
$one_rate_count=$rating_count;
}
if($rating=="two"){
$two_rate_percent=($rating_count/$total_ratings)*100;
$two_rate_count=$rating_count;
}
if($rating=="three"){
$three_rate_percent=($rating_count/$total_ratings)*100;
$three_rate_count=$rating_count;
}
if($rating=="four"){
$four_rate_percent=($rating_count/$total_ratings)*100;
$four_rate_count=$rating_count;
}
if($rating=="five"){
$five_rate_percent=($rating_count/$total_ratings)*100;
$five_rate_count=$rating_count;
}
}
}else{
$one_rate_percent=0;$two_rate_percent=0;$three_rate_percent=0;$four_rate_percent=0;$five_rate_percent=0;
$one_rate_count=0;$two_rate_count=0;$three_rate_count=0;$four_rate_count=0;$five_rate_count=0;
}

$avg_value=$controller->get_average_value_of_ratings($inventory_id);
//$avg_value;
//unset($avg);		
$username = $this->session->userdata('customer_name');                   

$review_number=$controller->number_of_reviews($inventory_id);               
//echo"&nbsp;"."$review_number".'&nbsp;Reviews &nbsp;';
?>

<div class="product-star has-popover_ourproducts_<?php echo $inventory_id;?>" rel="popover" style="background-color:#388e3c;color:#fff;padding:4px 8px;border-radius:2px;">
<?php
echo $avg_value;
//echo "(".$rate_number.")";
?>
<i class="fa fa-star" style="margin-left:0.5em;line-height: 1.5;font-size: 0.85em;"></i>

</div><!---product-star-->

<script>
$(function () {

var showPopover = function () {
//$(".infopoint").popover(options);
$(this).popover('show');
progressBar_ourproducts(<?php echo $five_rate_percent; ?>, $('#progressBar_ourproducts_<?php echo $inventory_id; ?>_5'));
progressBar_ourproducts(<?php echo $four_rate_percent; ?>, $('#progressBar_ourproducts_<?php echo $inventory_id; ?>_4'));
progressBar_ourproducts(<?php echo $three_rate_percent; ?>, $('#progressBar_ourproducts_<?php echo $inventory_id; ?>_3'));
progressBar_ourproducts(<?php echo $two_rate_percent; ?>, $('#progressBar_ourproducts_<?php echo $inventory_id; ?>_2'));
progressBar_ourproducts(<?php echo $one_rate_percent; ?>, $('#progressBar_ourproducts_<?php echo $inventory_id; ?>_1'));

}
, hidePopover = function () {
$(this).popover('hide');
};

$('.has-popover_ourproducts_<?php echo $inventory_id; ?>').popover({
placement: function(tip, element) {
var $element, above, actualHeight, actualWidth, below, boundBottom, boundLeft, boundRight, boundTop, elementAbove, elementBelow, elementLeft, elementRight, isWithinBounds, left, pos, right;
isWithinBounds = function(elementPosition) {
return boundTop < elementPosition.top && boundLeft < elementPosition.left && boundRight > (elementPosition.left + actualWidth) && boundBottom > (elementPosition.top + actualHeight);
};
$element = $(element);
pos = $.extend({}, $element.offset(), {
width: element.offsetWidth,
height: element.offsetHeight
});
actualWidth = 283;
actualHeight = 117;
boundTop = $(document).scrollTop();
boundLeft = $(document).scrollLeft();
boundRight = boundLeft + $(window).width();
boundBottom = boundTop + $(window).height();
elementAbove = {
top: pos.top - actualHeight,
left: pos.left + pos.width / 2 - actualWidth / 2
};
elementBelow = {
top: pos.top + pos.height,
left: pos.left + pos.width / 2 - actualWidth / 2
};
elementLeft = {
top: pos.top + pos.height / 2 - actualHeight / 2,
left: pos.left - actualWidth
};
elementRight = {
top: pos.top + pos.height / 2 - actualHeight / 2,
left: pos.left + pos.width
};
above = isWithinBounds(elementAbove);
below = isWithinBounds(elementBelow);
left = isWithinBounds(elementLeft);
right = isWithinBounds(elementRight);
if (right) {
return "right";
} else {
if (left) {
return "left";

} else {
if (top) {
return "top";
} else {
if (below) {
return "bottom";
} else {
return "right";
}
}
}
}
},
html : true,
content: function(){return $("#ratings_ourproducts_<?php echo $inventory_id; ?>").html();},
trigger: 'manual'

})
.focus(showPopover)
.blur(hidePopover)
.hover(showPopover,hidePopover);
});

function progressBar_ourproducts(percent, $element) {
var progressBarWidth = percent * $element.width() / 100;
//$element.find('div').css({ width: progressBarWidth }).html( "&nbsp;");
$element.find('div').animate({ width: progressBarWidth }, 500).html( "&nbsp;");
}


</script>

<div id="ratings_ourproducts_<?php echo $inventory_id; ?>" class="popover" style="display:none;color:#808080;">
<div style="width:40%;float:left;color:#808080;">
<b> Ratings </b><br>
<i class="fa fa-star fa-2x"> &nbsp;<?php echo $avg_value;?></i><br>

Average of <?php echo $rate_number; ?>
</div>
<div class="horizontal_bar" style="color:#808080;">
<div class="star_rate_1">
<div class="star_rate_2">5 star</div><div id="progressBar_ourproducts_<?php echo $inventory_id; ?>_5" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $five_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">4 star</div><div id="progressBar_ourproducts_<?php echo $inventory_id; ?>_4" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $four_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">3 star</div><div id="progressBar_ourproducts_<?php echo $inventory_id; ?>_3" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $three_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">2 star</div><div id="progressBar_ourproducts_<?php echo $inventory_id; ?>_2" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $two_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">1 star</div><div id="progressBar_ourproducts_<?php echo $inventory_id; ?>_1" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $one_rate_count ?>)&nbsp;</div>
</div>

</div>		
</div>
</div>
</div>
<!---ratings---added------------->

<div class="row">
<div class="col-xs-12 col-sm-12">
<h5 class="product-name pb-2" style="font-size:1em;color:#95918c;">
<b><?php echo $controller->get_brand_name($pro->brand_id);?></b>
</h5>								

<?php
/*
<h5 class="attribute-col">&nbsp;
<?php

if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4_value"])[0];
}



if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1_value"]!=""){
echo "<h5 class='attribute-col' style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2_value"]!=""){
echo "<h5 class='attribute-col'  style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3_value"]!=""){
echo "<h5 class='attribute-col'  style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4_value"]!=""){
echo "<h5 class='attribute-col' style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4_value"])."</h5>";
}
}



?>
</h5>
*/
?>

<div class="content_price">
<?php if(!empty($inv_discount_data)){ ?>
<span class="price product-price"><?php echo curr_sym; ?><?php echo $inv_discount_data['current_price']?></span>
<span class="price old-price"><?php echo curr_sym; ?><?php echo $inv_discount_data['inventory_price']?></span>
<small style="line-height: 1.8;">(Inclusive of Taxes)</small>
<?php }else{ ?>

<?php
if(get_pricetobeshown()=="selling_price"){
?>
<span class="price product-price"><?php echo curr_sym; ?><?php echo $pro->selling_price?></span> 
                                        <?php
                                        if(intval($selling_price)<intval($max_selling_price)){
                                        ?>
                                        <span class="price old-price"><?php echo curr_sym; ?><?php echo round($max_selling_price); ?></span>
                                        <?php
                                        }
                                        ?>
<small>(Inclusive of Taxes)</small>
<?php
}
else{
?>
<span class="price product-price"><?php echo curr_sym; ?><?php echo $pro->base_price?> <font style="font-size:0.6em;line-height:2.4"> + GST</font></span> <small>(Inclusive of Taxes)</small>
<?php
}
?>

<?php } ?>

</div>
</div>
</div>
<?php
/*
if(get_pricetobeshown()=="selling_price"){
?>
<div class="row">
<div class="col-md-12">
<h5>
<small>
(Inclusive of Taxes)
</small>
</h5>
<!---add to compare--->
<?php
$inventory_comparision=$controller->inventory_comparision_yes_or_no($inventory_id);
if($inventory_comparision=="yes"){
?>
<!--<a title="Add to compare" class="compare addToCompare" href="#" id="inventory_dealoftheday_<?php echo $inventory_id;?>"></a>-->
<div class="checkbox">
<label>
<input type="checkbox" value="" class="compare addToCompare" type="checkbox" id="inventory_<?php echo $inventory_id;?>">
<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
Add to Compare
</label>
</div>
<?php
}
?>
<!---add to compare--->
</div>
</div>
<?php } 
*/
?>
<div class="info-orther">
<p>Item Code: #453217907</p>
<p class="availability">Availability: <span>In stock</span></p>
<div class="product-desc more">
<?php echo $pro->product_description?>
</div>
</div>
</div>
</div>
</li>
<?php	
}
?>


</ul>



</div>

<?php 

	}
}
?>
		<!-- <div class="tab-pane" id="2">Profile</div> -->
	</div>

</div>
<!---tab--->


</div>
</div>
</div>
</section>


<?php } ?>


<!-------- ourproducts section ends -------------->

<!---new division---->

<style>
	.subscribe
{
	background: #ff6f00;
    padding: 30px 40px;
	margin-right: auto;
    margin-left: auto;
    position: relative;
	display: flex;
    -webkit-box-pack: center;
}
.rb-text-newsletter
{
	align-items: center;
}
.rb-text-newsletter h2 i {
    
    font-size: 68px;
    padding-right: 30px;
    position: relative;
    top: -10px;
	color:#fff;
	white-space: nowrap;
    font-weight: 700;
    text-transform: uppercase;
	line-height: 1.1;
}
.rb-text-newsletter p {
    display: inline-block;
    margin: 0;
    color: #fff;
    font-size: 16px;
    line-height: 24px;
}
.rb-text-newsletter h2 {
    margin: 0;
    position: relative;
    font-size: 18px;
    color: #FFF;
    border-right: 1px solid rgba(255,255,255,.28);
    padding: 20px 60px 25px 0;
    margin-right: 45px;
    white-space: nowrap;
    font-weight: 700;
    text-transform: uppercase;
	display: inline-flex;
	align-items: center;
}
.rb-text-newsletter p {
    display: inline-block;
    margin: 0;
    color: #fff;
    font-size: 16px;
    line-height: 24px;
	margin-top: 35px;
}
.block_content {
    max-width: 400px;
	width: 100%;
    margin-left: auto;
	margin-top:30px;
}
.block_content form {
    width: 100%;
    position: relative;
}
.block_content .input-group {
    position: relative;
    width: 100%;
    display: table;
    border-collapse: separate;
	border-radius: 3px !important;
}
.block_content form .btn {
    position: absolute;
    z-index: 8;
    background: #052670;
    border: none;
    color: #FFF;
    padding: 0 20px;
    height: 55px;
    line-height: 55px;
    font-size: 12px;
    font-weight: 700;
    right: 0;
    text-transform: uppercase;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
.block_content form input
{
	border: 2px solid #eaeaea;
    color: #a3a3a3;
    background-color: #FFF;
    font-size: 14px;
    height: 55px;
    width: 100%;
    padding: 0 20px;
	box-shadow: none;
}
	</style>

	<div class="clearfix"></div>

<section class="section8 section-top" style="margin-top:0px">
<div class="section-container">

<div class="section-content">
<div class="">


<div class="subscribe">
	<div class="rb-text-newsletter col-md-8">
		
		<h2><i class="fa fa-envelope-open-o" aria-hidden="true"></i>Sign up for <br>  Voomet newsletter</h2>
		<p>Subscribe to our newsletter to stay in touch with the <br>latest designs from around the world!</p>
	</div>
	<div class="col-md-4 block_content">

	<form onsubmit="return addNewsLetterSubscription_new(this)" action="<?php echo base_url()?>add_news_letter_subscription"  method="post">
		<div class="input-group newsletter-input-group ">
			<input class="form-control input-subscription" id="email_address_nl_new" name="email_address" type="email" value="" placeholder="Your email address" aria-labelledby="block-newsletter-label" required="" style="border-radius:3px !important;">
			<button class="btn btn-outline" name="submitNewsletter" type="submit">
				<i class="fa fa-paper-plane" aria-hidden="true"></i>
				<span>Subscribe</span>
			</button>
		</div>
		<input type="hidden" name="blockHookName" value="displayHome">
		<input type="hidden" name="action" value="0">
	</form>
	<span class="help" id="subscibing_info_new"></span>
</div>



</div>


</div>
</div>
</div>

</section>
<!---new division---->



<!-------- mostpurchased section starts -------------->
<?php 
$products_obj=$ui_arr["invs_mostpurchased"];
?>
<?php if(!empty($products_obj)){ ?>
<section class="pt-20 section8 mb-0 block-trending assignproducts_section" id="mostpurchased_range" style="background-color:#fff;">
<div class="section-container">

<h3 class="mb-30 section-title text-center" style="text-transform:capitalize;">Best Sellers</h3>

<div class="section-content">
<div class="container">
<ul class="products-style8 owl-carousel"  data-dots="false" data-loop="true" data-nav = "true" data-margin = "29" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":4},"1000":{"items":4}}'>
<!--<ul class="row product-list grid">-->

<?php 

foreach($products_obj as $pro){
$inventory_id=$pro->inventory_id;
$product_id=$pro->product_id;
$selling_price=$pro->selling_price;
$max_selling_price=$pro->max_selling_price;
$selling_discount=$pro->selling_discount;

//$product_name=$pro->product_name;
//$product_description=$pro->product_description;

$inv_discount_data=$controller->get_default_discount_for_inv($inventory_id,$max_selling_price,$product_id);
///////////////  No of Offers starts ///////////////
$promo_numbers=0;
$promotions=$controller->get_number_of_offers($inventory_id);
$inventory_product_info_obj=$controller->get_inventory_product_info_by_inventory_id($inventory_id);

if(!empty($promotions) && ($inventory_product_info_obj->stock_available>0)){
$i=1; 
$defaultDiscounts_num=0;
foreach($promotions as $promotion){
foreach($promotion as $promo){

if (preg_match_all('/\d+(?=%)/', $promo-> get_type, $match)&&($promo->to_buy==1)){
$i++; 
if($promo->to_buy==1){
$defaultDiscounts_num=1;
}
}
}
}

if($defaultDiscounts_num!=0){
$promo_numbers=count($promotions)-$defaultDiscounts_num;
}else{
$promo_numbers=count($promotions);
}

}
//////////////   No of offers ends    ///////////////

//print_r($inv_discount_data);
$pro_name=($pro->sku_name!='') ?$pro->sku_name : $pro->product_name;
?>
<li class="">
<div class="product-container selectProduct" style="border:none;" data-id="<?php echo ucwords($pro_name)?>" data-title="<?php echo $inventory_id;?>">
<div class="left-block">
<?php 
$rand_1=$controller->sample_code();
$rand_2=$controller->sample_code();
?>
<a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pro->pcat_id}"; ?>/<?php echo "{$rand_2}{$pro->cat_id}"; ?>">


<img class="img-responsive productImg_<?php echo $inventory_id;?>" alt="productsku" src="<?php echo base_url().$pro->common_image;?>"/>

</a>

<!--<div class="quick-view">
<?php 
/*
if($this->session->userdata("customer_id")){ 
if($controller->check_wishlist($inventory_id)>0){
?>
<script>
$(document).ready(function(){
$("#wishlist_mostpurchased_"+<?php echo $inventory_id;?>).css({"color":""});;
$("#wishlist_mostpurchased_"+<?php echo $inventory_id;?>).css({"color":"#ff4343;"});;
});
</script>
<?php
$val="Added to Wishlist";
$style_wishlist_label="style='color:#eda900;'";
}
else{ 
$val="Add to Wishlist";
$style_wishlist_label="";
}
}else{
$val="Add to Wishlist";
$style_wishlist_label="";
}
?>
<?php
if($this->session->userdata("customer_id")){
?>
<span style="cursor:pointer;" id="anchor_mostpurchased_<?php echo $inventory_id;?>" onclick="add_to_wishlistFun(<?php echo $inventory_id;?>,'mostpurchased');return false;"  title="<?php echo $val; ?>" class="wishlist"><i id="<?php echo "wishlist_mostpurchased_".$inventory_id;?>" class="fa fa-heart" style='color:#c2c2c2;font-size: 1.2em;'></i></span>
<?php
}
else{
?>
<span style="cursor:pointer" id="anchor_mostpurchased_<?php echo $inventory_id;?>" onclick="add_to_wishlistFun(<?php echo $inventory_id;?>,'mostpurchased');return false;"  title="<?php echo $val; ?>" class="wishlist"><i id="<?php echo "wishlist_mostpurchased_".$inventory_id;?>" class="fa fa-heart" style='color:#c2c2c2;font-size: 1.2em;'></i></span>
<?php	
}
*/
?>

</div>-->
<?php if(!empty($inv_discount_data)){ ?>
<div class="price-percent-reduction2" <?php if($inv_discount_data['discount']<10){echo "style='padding-top:18px'";}?>>
<?php echo  $inv_discount_data['discount'] ?>% OFF

 

</div>
<?php }else{ ?>

    <?php
                                                            if($selling_discount>0){
                                                            ?>
                                                                <div class="price-percent-reduction2" <?php if ($selling_discount < 10) {
                                                                echo "style='padding-top:18px'";
                                                            } ?>>
                                                                <?php echo round($selling_discount); ?>% OFF
                                                            </div>

                                                            <?php
                                                            }
                                                            ?>


<?php
}
if($promo_numbers>0){ 
?>
<div class="price-percent-reduction_offers">
<?php

if($promo_numbers>1){echo $promo_numbers.' Offers';}else{echo $promo_numbers.' Offer';}
?>
</div>
<?php
}
?>



</div>
<?php
/*
?>
<div class="product-info text-center">
<div class="product-name">
<a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pro->pcat_id}"; ?>/<?php echo "{$rand_2}{$pro->cat_id}"; ?>"><?php echo $pro->product_name;?></a>
</div>
<div class="button-control">

<a title="Add to Cart" class="btn btn-default" href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pro->pcat_id}"; ?>/<?php echo "{$rand_2}{$pro->cat_id}"; ?>">Explore</a>

</div>

</div>
<?php
*/
$pro_name=($pro->sku_name!='') ?$pro->sku_name : $pro->product_name;
?>



<div class="right-block heightmatched_mostpurchased">
<div class="row">
<div class="col-xs-12 col-sm-12">
<h5 class="product-name heightmatchedformostpurchased mb-70" style="font-size:1.05em;padding-bottom:0!important;word-break:break-word;line-height:1.3;"><a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pro->pcat_id}"; ?>/<?php echo "{$rand_2}{$pro->cat_id}"; ?>"  title="<?php
$str=$pro_name;
$str=str_replace("\r\n", '', $str);
$str=str_replace('\r\n', '', $str);
echo ucwords($str);
?>">

<?php
$str=$pro_name;
$str=str_replace("\r\n", '', $str);
$str=str_replace('\r\n', '', $str);
echo ucwords($str);
?>
<?php
$data=$controller->get_inventory_info_by_inventory_id($inventory_id);
?>

<!---added attributes ---->
<span style="<?php echo ($pro->sku_name!='') ? 'display:none;' : ''; ?>">
<?php
$color_attribute_is_there="no";
if(strtolower($data['attribute_1'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_1_value'])[0];
}
if(strtolower($data['attribute_2'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_2_value'])[0];
}
if(strtolower($data['attribute_3'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_3_value'])[0];
}
if(strtolower($data['attribute_4'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_4_value'])[0];
}
?>
<?php
if($color_attribute_is_there=="yes"){
if(strtolower($data['attribute_1'])!="color"){
if($data['attribute_1_value']!=""){
echo " - ".$data['attribute_1_value'];
}
}
if(strtolower($data['attribute_2'])!="color"){
if($data['attribute_2_value']!=""){
echo " - ".$data['attribute_2_value'];
}
}
if(strtolower($data['attribute_3'])!="color"){
if($data['attribute_3_value']!=""){
echo " - ".$data['attribute_3_value'];
}
}
if(strtolower($data['attribute_4'])!="color"){
if($data['attribute_4_value']!=""){
echo " - ".$data['attribute_4_value'];
}
}
}
else{
$noncolor_attribute_first_is_there="no";
$noncolor_attribute_second_is_there="no";

if(strtolower($data['attribute_1'])!="color"){
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_1_value'];
}
if(strtolower($data['attribute_2'])!="color"){
if($noncolor_attribute_first_is_there=="yes"){
$noncolor_attribute_second_is_there="yes";
if($data['attribute_2_value']!=""){
echo " - ".$data['attribute_2_value'];
}
}
else{
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_2_value'];
}
}
if(strtolower($data['attribute_3'])!="color"){
if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
if($noncolor_attribute_first_is_there=="yes"){
$noncolor_attribute_second_is_there="yes";
if($data['attribute_3_value']!=""){
echo " - ".$data['attribute_3_value'];
}
}
else{
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_3_value'];
}
}
else{
	if($data['attribute_3_value']!=''){
		echo " - ".$data['attribute_3_value'];
	}
}
}
if(strtolower($data['attribute_4'])!="color"){
if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
if($noncolor_attribute_first_is_there=="yes"){
$noncolor_attribute_second_is_there="yes";
if($data['attribute_4_value']!=""){
echo " - ".$data['attribute_4_value'];
}
}
else{
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_4_value'];
}
}
else{
	if($data['attribute_4_value']!=''){
		echo " - ".$data['attribute_4_value'];
	}
}
}


}
$color_attribute_is_there="no";
$noncolor_attribute_first_is_there="no";
$noncolor_attribute_second_is_there="no";
?>
</span>
<!---added attributes ---->

</a></h5>
</div>
</div>

<!---ratings---added------------->								
<?php

$inventory_id=$inventory_id;
$rate_number=$controller->number_of_ratings($inventory_id);
$number_ratings_one_to_five=$controller->number_ratings_one_to_five($inventory_id);
$total_ratings=$rate_number;

if($total_ratings!=0){
foreach($number_ratings_one_to_five as $rating=>$rating_count){
if($rating=="one"){
$one_rate_percent=($rating_count/$total_ratings)*100;
$one_rate_count=$rating_count;
}
if($rating=="two"){
$two_rate_percent=($rating_count/$total_ratings)*100;
$two_rate_count=$rating_count;
}
if($rating=="three"){
$three_rate_percent=($rating_count/$total_ratings)*100;
$three_rate_count=$rating_count;
}
if($rating=="four"){
$four_rate_percent=($rating_count/$total_ratings)*100;
$four_rate_count=$rating_count;
}
if($rating=="five"){
$five_rate_percent=($rating_count/$total_ratings)*100;
$five_rate_count=$rating_count;
}
}
}else{
$one_rate_percent=0;$two_rate_percent=0;$three_rate_percent=0;$four_rate_percent=0;$five_rate_percent=0;
$one_rate_count=0;$two_rate_count=0;$three_rate_count=0;$four_rate_count=0;$five_rate_count=0;
}

$avg_value=$controller->get_average_value_of_ratings($inventory_id);
//$avg_value;
//unset($avg);		
$username = $this->session->userdata('customer_name');                   

$review_number=$controller->number_of_reviews($inventory_id);               
//echo"&nbsp;"."$review_number".'&nbsp;Reviews &nbsp;';
?>

<div class="product-star has-popover_mostpurchased_<?php echo $inventory_id;?> mb-10" rel="popover" style="background-color:#388e3c;color:#fff;padding:4px 8px;border-radius:2px;width: fit-content;">
<?php
echo $avg_value;
//echo "(".$rate_number.")";
?>
<i class="fa fa-star" style="margin-left:0.5em;line-height: 1.7;font-size: 0.85em;"></i>

</div><!---product-star-->

<script>
$(function () {

var showPopover = function () {
//$(".infopoint").popover(options);
$(this).popover('show');
progressBar_mostpurchased(<?php echo $five_rate_percent; ?>, $('#progressBar_mostpurchased_<?php echo $inventory_id; ?>_5'));
progressBar_mostpurchased(<?php echo $four_rate_percent; ?>, $('#progressBar_mostpurchased_<?php echo $inventory_id; ?>_4'));
progressBar_mostpurchased(<?php echo $three_rate_percent; ?>, $('#progressBar_mostpurchased_<?php echo $inventory_id; ?>_3'));
progressBar_mostpurchased(<?php echo $two_rate_percent; ?>, $('#progressBar_mostpurchased_<?php echo $inventory_id; ?>_2'));
progressBar_mostpurchased(<?php echo $one_rate_percent; ?>, $('#progressBar_mostpurchased_<?php echo $inventory_id; ?>_1'));

}
, hidePopover = function () {
$(this).popover('hide');
};

$('.has-popover_mostpurchased_<?php echo $inventory_id; ?>').popover({
placement: function(tip, element) {
var $element, above, actualHeight, actualWidth, below, boundBottom, boundLeft, boundRight, boundTop, elementAbove, elementBelow, elementLeft, elementRight, isWithinBounds, left, pos, right;
isWithinBounds = function(elementPosition) {
return boundTop < elementPosition.top && boundLeft < elementPosition.left && boundRight > (elementPosition.left + actualWidth) && boundBottom > (elementPosition.top + actualHeight);
};
$element = $(element);
pos = $.extend({}, $element.offset(), {
width: element.offsetWidth,
height: element.offsetHeight
});
actualWidth = 283;
actualHeight = 117;
boundTop = $(document).scrollTop();
boundLeft = $(document).scrollLeft();
boundRight = boundLeft + $(window).width();
boundBottom = boundTop + $(window).height();
elementAbove = {
top: pos.top - actualHeight,
left: pos.left + pos.width / 2 - actualWidth / 2
};
elementBelow = {
top: pos.top + pos.height,
left: pos.left + pos.width / 2 - actualWidth / 2
};
elementLeft = {
top: pos.top + pos.height / 2 - actualHeight / 2,
left: pos.left - actualWidth
};
elementRight = {
top: pos.top + pos.height / 2 - actualHeight / 2,
left: pos.left + pos.width
};
above = isWithinBounds(elementAbove);
below = isWithinBounds(elementBelow);
left = isWithinBounds(elementLeft);
right = isWithinBounds(elementRight);
if (right) {
return "right";
} else {
if (left) {
return "left";

} else {
if (top) {
return "top";
} else {
if (below) {
return "bottom";
} else {
return "right";
}
}
}
}
},
html : true,
content: function(){return $("#ratings_mostpurchased_<?php echo $inventory_id; ?>").html();},
trigger: 'manual'

})
.focus(showPopover)
.blur(hidePopover)
.hover(showPopover,hidePopover);
});

function progressBar_mostpurchased(percent, $element) {
var progressBarWidth = percent * $element.width() / 100;
//$element.find('div').css({ width: progressBarWidth }).html( "&nbsp;");
$element.find('div').animate({ width: progressBarWidth }, 500).html( "&nbsp;");
}


</script>

<div id="ratings_mostpurchased_<?php echo $inventory_id; ?>" class="popover" style="display:none;color:#808080;">
<div style="width:40%;float:left;color:#808080;">
<b> Ratings </b><br>
<i class="fa fa-star fa-2x"> &nbsp;<?php echo $avg_value;?></i><br>

Average of <?php echo $rate_number; ?>
</div>
<div class="horizontal_bar" style="color:#808080;">
<div class="star_rate_1">
<div class="star_rate_2">5 star</div><div id="progressBar_mostpurchased_<?php echo $inventory_id; ?>_5" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $five_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">4 star</div><div id="progressBar_mostpurchased_<?php echo $inventory_id; ?>_4" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $four_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">3 star</div><div id="progressBar_mostpurchased_<?php echo $inventory_id; ?>_3" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $three_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">2 star</div><div id="progressBar_mostpurchased_<?php echo $inventory_id; ?>_2" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $two_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">1 star</div><div id="progressBar_mostpurchased_<?php echo $inventory_id; ?>_1" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $one_rate_count ?>)&nbsp;</div>
</div>
</div>		
</div>


<!---ratings---added------------->
<div class="row">
<div class="col-xs-12 col-sm-12">
<h5 class="product-name pb-2" style="font-size:1em;color:#95918c;">
<b><?php echo $controller->get_brand_name($pro->brand_id);?></b>
</h5>								
<?php
/*
<h5 class="attribute-col">&nbsp;
<?php

if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4_value"])[0];
}



if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1_value"]!=""){
echo "<h5 class='attribute-col' style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2_value"]!=""){
echo "<h5 class='attribute-col'  style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3_value"]!=""){
echo "<h5 class='attribute-col'  style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4_value"]!=""){
echo "<h5 class='attribute-col' style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4_value"])."</h5>";
}
}



?>
</h5>
*/
?>

<div class="content_price mb-20">
<?php if(!empty($inv_discount_data)){ ?>
<span class="price product-price"><?php echo curr_sym; ?><?php echo $inv_discount_data['current_price']?></span>
<span class="price old-price"><?php echo curr_sym; ?><?php echo $inv_discount_data['inventory_price']?></span>
<small style="line-height: 1.8;">(Inclusive of Taxes)</small>
<?php }else{ ?>

<?php
if(get_pricetobeshown()=="selling_price"){
?>
<span class="price product-price"><?php echo curr_sym; ?><?php echo $pro->selling_price?></span>
                                        <?php
                                        if(intval($selling_price)<intval($max_selling_price)){
                                        ?>
                                        <span class="price old-price"><?php echo curr_sym; ?><?php echo round($max_selling_price); ?></span>
                                        <?php
                                        }
                                        ?>
<small>(Inclusive of Taxes)</small>
<?php
}
else{
?>
<span class="price product-price"><?php echo curr_sym; ?><?php echo $pro->base_price?> <font style="font-size:0.6em;line-height:2.4"> + GST</font></span> <small>(Inclusive of Taxes)</small>
<?php
}
?>

<?php } ?>

</div>
</div>
</div>
<?php
/*
if(get_pricetobeshown()=="selling_price"){
?>
<div class="row">
<div class="col-md-12">
<!---add to compare--->
<?php
$inventory_comparision=$controller->inventory_comparision_yes_or_no($inventory_id);
if($inventory_comparision=="yes"){
?>
<!--<a title="Add to compare" class="compare addToCompare" href="#" id="inventory_dealoftheday_<?php echo $inventory_id;?>"></a>-->
<div class="checkbox">
<label>
<input type="checkbox" value="" class="compare addToCompare" type="checkbox" id="inventory_<?php echo $inventory_id;?>">
<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
Add to Compare
</label>
</div>
<?php
}
?>
<!---add to compare--->
</div>
</div>
<?php } 
*/
?>
<div class="info-orther">
<p>Item Code: #453217907</p>
<p class="availability">Availability: <span>In stock</span></p>
<div class="product-desc more">
<?php echo $pro->product_description?>
</div>
</div>
</div>
</div>
</li>
<?php	
}

?>


</ul>
</div>
</div>
</div>
</section>

<?php } ?>

<!-------- mostpurchased section ends -------------->



<!-------- shopbyparentcategory section starts -------------->
<?php 
$products_obj=$ui_arr["invs_shopbyparentcategory"];
?>
<?php if(!empty($products_obj)){ 

?>
<section class=" section8 block-trending assignproducts_section" id="shopbyparentcategory_range">
<div class="section-container">
<h3 class="section-title" style="text-transform:capitalize;">Categories</h3>
<!-- Parent Categories -->
<div class="section-content">
<div class="container">
<ul class="products-style8 owl-carousel"  data-dots="false" data-loop="true" data-nav = "true" data-margin = "29" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":4}}'>
<!--<ul class="row product-list grid">-->

<?php 

foreach($products_obj as $pro){

?>
<li class="">
<div class="product-container selectProduct">
<div class="left-block">
<?php 
$rand_1=$controller->sample_code();
$rand_2=$controller->sample_code();
?>
<a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pro->pcat_id}"; ?>">


<img class="img-responsive" alt="productsku" src="<?php echo base_url().$pro->image_shopbyparentcategory;?>"/>

</a>

<div class="quick-view">



</div>







</div>




<div class="right-block" style="padding:15px">
<div class="text-center">
<h5 class="product-name heightmatched_shopbyparentcategory mb-10" style="font-size:1.5em;padding-bottom:0!important;padding-top:10px!important;word-break:break-word;line-height:1.3;"><a style="color:#052670;" href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pro->pcat_id}"; ?>"  title="<?php echo $pro->pcat_name; ?>">

<?php
echo $pro->pcat_name;
?>
</a></h5>
</div>
<h5 class="product-name pb-2 text-center mb-10" style="font-size:0.9em;">
<b>
<?php
$cat_id_list=$pro->cat_id_list;
$cat_list_name_arr=[];
if(!empty($cat_id_list)){
$cat_id_list_arr=explode(",",$cat_id_list);
foreach($cat_id_list_arr as $cat_id){
$cat_list_name_arr[]=$controller->get_category_name($cat_id);
}
echo implode(", ",$cat_list_name_arr);
}
?>

</b>
</h5>								

<p class="attribute-col text-center">
<?php
echo $pro->legend1;
?>
</p>
<p class="attribute-col text-center">
<?php
echo $pro->legend2;
?>
</p>
<p class="attribute-col text-center">
<?php
echo $pro->legend3;
?>
</p>





</div>




</div>
</li>
<?php	
}

?>


</ul>
</div>
</div>
</div>
</section>

<?php } ?>

<!-------- shopbyparentcategory section ends -------------->



<!-------- shopbycategory section starts -------------->
<?php 
$products_obj=$ui_arr["invs_shopbycategory"];
?>
<?php if(!empty($products_obj)){ 

?>
<section class=" section8 block-trending assignproducts_section" id="shopbycategory_range">
<div class="section-container">
<h3 class="section-title" style="text-transform:capitalize;">Shop by Type</h3>
<div class="section-content">
<div class="container">
<ul class="products-style8 owl-carousel"  data-dots="false" data-loop="true" data-nav = "true" data-margin = "29" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":4},"1000":{"items":4}}'>
<!--<ul class="row product-list grid">-->

<?php 

foreach($products_obj as $pro){

$cat_id=$pro->cat_id;
?>
<li class="">
<div class="product-container selectProduct">
<div class="left-block">
<?php 
$rand_1=$controller->sample_code();
$rand_2=$controller->sample_code();
?>
<a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pro->pcat_id}"; ?>/<?php echo "{$rand_2}{$pro->cat_id}"; ?>">


<img class="img-responsive" alt="productsku" src="<?php echo base_url().$pro->image_shopbycategory;?>"/>

</a>

<div class="quick-view">



</div>







</div>




<div class="right-block">
<div class="text-center">
<h5 class="product-name heightmatched_shopbycategory" style="font-size:1.05em;padding-bottom:0!important;word-break:break-word;line-height:1.3;"><a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pro->pcat_id}"; ?>/<?php echo "{$rand_2}{$pro->cat_id}"; ?>"  title="<?php echo $pro->cat_name; ?>">

<?php
//echo $pro->cat_name;
?>

<?php
//echo $controller->get_parentcategory_name($pro->pcat_id);
?>
</a></h5>
</div>
<h5 class="product-name pb-2 text-center mb-20" style="font-size:0.9em;">
<b>
<?php
$brand_id_list=$pro->brand_id_list;
$brand_list_name_arr=[];
if(!empty($brand_id_list)){
$brand_id_list_arr=explode(",",$brand_id_list);
foreach($brand_id_list_arr as $brand_id){
$brand_list_name_arr[]=$controller->get_brand_name($brand_id);
}
echo implode(", ",$brand_list_name_arr);
}
?>

</b>
</h5>								

<h4 class="attribute-col text-center"><strong>
<?php
echo $pro->legend1;
?></strong>
</h4>
<p class="attribute-col text-center" style="color:#fc6938">
<?php
echo $pro->legend2;
?>
</p>
<p class="attribute-col text-center mb-20">
<?php
echo $pro->legend3;
?>
</p>





</div>




</div>
</li>
<?php	
}

?>


</ul>
</div>
</div>
</div>
</section>

<?php } ?>

<!-------- shopbycategory section ends -------------->



<!-------- shopbybrand section starts -------------->
<?php 
$products_obj=$ui_arr["invs_shopbybrand"];
?>
<?php if(!empty($products_obj)){ 

?>
<section class=" section8 block-trending assignproducts_section" id="shopbybrand_range">
<div class="section-container">
<h3 class="section-title" style="text-transform:capitalize;">Shop by Brands</h3>
<!-- Parent Categories -->
<div class="section-content">
<div class="container">
<!--<ul class="products-style8 owl-carousel"  data-dots="true" data-loop="true" data-nav = "false" data-margin = "29" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":2},"1000":{"items":5}}'>--->

<?php
//Columns must be a factor of 12 (1,2,3,4,6,12)
$numOfCols = 3;
$rowCount = 0;
$bootstrapColWidth = 12 / $numOfCols;
$bootstrapColOffset = "";
?>
<ul class="row product-list grid">
<?php
foreach ($products_obj as $pro){
	if($rowCount==1){
        $bootstrapColOffset="col-md-offset-2";
     } 
?>  
		<li class="col-sm-<?php echo $bootstrapColWidth; ?> <?php if($rowCount% $numOfCols==0){ echo $bootstrapColOffset;} ?>">
            <div class="product-container selectProduct">
			<div class="left-block">
			<?php 
			$rand_1=$controller->sample_code();
			$rand_2=$controller->sample_code();
			?>
<a href="<?php echo base_url();?>catalog_shopbybrand/shopbybrand_<?php echo $pro->brand_name;?>">
<img class="img-responsive" style="width:50%;" alt="productsku" src="<?php echo base_url().$pro->image_shopbybrand;?>"/>
</a>
            </div>
			</div>
        </li>
<?php
    $rowCount++;
    if($rowCount % $numOfCols == 0) echo '</ul><ul class="row product-list grid">';

}
?>
</ul>
<?php 
/*
foreach($products_obj as $pro){

?>
<li class="col-xs-6 col-sm-4">
<div class="product-container selectProduct">
<div class="left-block">
<?php 
$rand_1=$controller->sample_code();
$rand_2=$controller->sample_code();
?>
<?php
/*
?>
<a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pro->brand_name}"; ?>">


<img class="img-responsive" alt="productsku" src="<?php echo base_url().$pro->image_shopbybrand;?>"/>

</a>
<?php

?>
<a href="<?php echo base_url();?>catalog_shopbybrand/shopbybrand_<?php echo $pro->brand_name;?>">


<img class="img-responsive" style="width:50%;" alt="productsku" src="<?php echo base_url().$pro->image_shopbybrand;?>"/>

</a>
<div class="quick-view">



</div>







</div>




<!--<div class="right-block">
<div class="text-center">
<h5 class="product-name heightmatched_shopbybrand" style="font-size:1.05em;padding-bottom:0!important;word-break:break-word;line-height:1.3;"><a href="<?php //echo base_url();?>catalog_shopbybrand/shopbybrand_<?php //echo $pro->brand_name;?>"  title="<?php //echo $pro->brand_name; ?>">

<?php
//echo $pro->brand_name;
?>
</a></h5>
</div>							

<p class="attribute-col text-center">
<?php
//echo $pro->legend1;
?>
</p>
<p class="attribute-col text-center">
<?php
//echo $pro->legend2;
?>
</p>
<p class="attribute-col text-center">
<?php
//echo $pro->legend3;
?>
</p>





</div>-->




</div>
</li>
<?php	
}
*/
?>



</div>
</div>
</div>
</section>

<?php } ?>

<!-------- shopbybrand section ends -------------->


<!-------- knockoutdeals section starts -------------->
<?php 
$products_obj=$ui_arr["invs_knockoutdeals"];
?>
<?php if(!empty($products_obj)){ ?>
<section class=" section8 block-trending assignproducts_section" id="knockoutdeals_range">
<div class="section-container">
<h3 class="mb-30 section-title text-center">Knockout Deals</h3>
<div class="section-content">
<div class="container">
<!--<ul class="products-style8 owl-carousel"  data-dots="true" data-loop="true" data-nav = "false" data-margin = "29" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":2},"1000":{"items":5}}'>--->
<ul class="row product-list grid">

<?php 

foreach($products_obj as $pro){
$inventory_id=$pro->inventory_id;
$product_id=$pro->product_id;
$selling_price=$pro->selling_price;
$max_selling_price = $pro->max_selling_price;
$selling_discount = $pro->selling_discount;
//$product_name=$pro->product_name;
//$product_description=$pro->product_description;

$inv_discount_data=$controller->get_default_discount_for_inv($inventory_id,$max_selling_price,$product_id);
///////////////  No of Offers starts ///////////////
$promo_numbers=0;
$promotions=$controller->get_number_of_offers($inventory_id);
$inventory_product_info_obj=$controller->get_inventory_product_info_by_inventory_id($inventory_id);

if(!empty($promotions) && ($inventory_product_info_obj->stock_available>0)){
$i=1; 
$defaultDiscounts_num=0;
foreach($promotions as $promotion){
foreach($promotion as $promo){

if (preg_match_all('/\d+(?=%)/', $promo-> get_type, $match)&&($promo->to_buy==1)){
$i++; 
if($promo->to_buy==1){
$defaultDiscounts_num=1;
}
}
}
}

if($defaultDiscounts_num!=0){
$promo_numbers=count($promotions)-$defaultDiscounts_num;
}else{
$promo_numbers=count($promotions);
}

}
//////////////   No of offers ends    ///////////////

//print_r($inv_discount_data);
$pro_name=($pro->sku_name!='') ?$pro->sku_name : $pro->product_name;
?>
<li class="col-xs-6 col-sm-3">
<div class="product-container selectProduct" data-id="<?php echo ucwords($pro_name)?>" data-title="<?php echo $inventory_id;?>">
<div class="left-block">
<?php 
$rand_1=$controller->sample_code();
$rand_2=$controller->sample_code();
?>
<a href="<?php echo base_url()?>detail/<?php echo "{$rand_1}{$inventory_id}";?>">


<img class="img-responsive productImg_<?php echo $inventory_id;?>" alt="productsku" src="<?php echo base_url().$pro->common_image;?>"/>

</a>

<div class="quick-view">
<?php 
if($this->session->userdata("customer_id")){ 
if($controller->check_wishlist($inventory_id)>0){
?>
<script>
$(document).ready(function(){
$("#wishlist_knockoutdeals_"+<?php echo $inventory_id;?>).css({"color":""});
$("#wishlist_knockoutdeals_"+<?php echo $inventory_id;?>).css({"color":"#ff4343;"});
});
</script>
<?php
$val="Added to Wishlist";
$style_wishlist_label="style='color:#eda900;'";
}
else{ 
$val="Add to Wishlist";
$style_wishlist_label="";
}
}else{
$val="Add to Wishlist";
$style_wishlist_label="";
}
?>
<?php
if($this->session->userdata("customer_id")){
?>
<span style="cursor:pointer;" id="anchor_knockoutdeals_<?php echo $inventory_id;?>" onclick="add_to_wishlistFun(<?php echo $inventory_id;?>,'knockoutdeals');return false;"  title="<?php echo $val; ?>" class="wishlist"><i id="<?php echo "wishlist_knockoutdeals_".$inventory_id;?>" class="fa fa-heart" style='color:#c2c2c2;font-size: 1.2em;'></i></span>
<?php
}
else{
?>
<span style="cursor:pointer" id="anchor_knockoutdeals_<?php echo $inventory_id;?>" onclick="add_to_wishlistFun(<?php echo $inventory_id;?>,'knockoutdeals');return false;"  title="<?php echo $val; ?>" class="wishlist"><i id="<?php echo "wishlist_knockoutdeals_".$inventory_id;?>" class="fa fa-heart" style='color:#c2c2c2;font-size: 1.2em;'></i></span>
<?php	
}
?>


</div>
<?php if(!empty($inv_discount_data)){ ?>
<div class="price-percent-reduction2" <?php if($inv_discount_data['discount']<10){echo "style='padding-top:18px'";}?>>
<?php echo  $inv_discount_data['discount'] ?>% OFF



</div>
<?php }else{ ?>

<?php
                                                            if($selling_discount>0){
                                                            ?>
                                                                <div class="price-percent-reduction2" <?php if ($selling_discount < 10) {
                                                                echo "style='padding-top:18px'";
                                                            } ?>>
                                                                <?php echo round($selling_discount); ?>% OFF
                                                            </div>

                                                            <?php
                                                            }
                                                            ?>

<?php
}
if($promo_numbers>0){ 
?>
<div class="price-percent-reduction_offers">
<?php

if($promo_numbers>1){echo $promo_numbers.' Offers';}else{echo $promo_numbers.' Offer';}
?>
</div>
<?php
}
?>



</div>
<?php
/*
?>
<div class="product-info text-center">
<div class="product-name">
<a href="<?php echo base_url()?>detail/<?php echo "{$rand_1}{$inventory_id}";?>"><?php echo $pro->product_name;?></a>
</div>
<div class="button-control">

<a title="Add to Cart" class="btn btn-default" href="<?php echo base_url()?>detail/<?php echo "{$rand_1}{$inventory_id}";?>">Explore</a>

</div>

</div>
<?php
*/
$pro_name=($pro->sku_name!='') ?$pro->sku_name : $pro->product_name;
?>



<div class="right-block heightmatched_knockoutdeals">
<div class="row mb-10">
<div class="col-xs-12 col-sm-12">
<h5 class="product-name heightmatchedforknockoutdeals" style="font-size:1.05em;padding-bottom:0!important;word-break:break-word;line-height:1.3;"><a href="<?php echo base_url()?>detail/<?php echo "{$rand_1}{$inventory_id}";?>"  title="<?php
$str=$pro_name;
$str=str_replace("\r\n", '', $str);
$str=str_replace('\r\n', '', $str);
echo ucwords($str);
?>">

<?php
$str=$pro_name;
$str=str_replace("\r\n", '', $str);
$str=str_replace('\r\n', '', $str);
echo ucwords($str);
?>
<?php
$data=$controller->get_inventory_info_by_inventory_id($inventory_id);
?>

<!---added attributes ---->
<span style="<?php echo ($pro->sku_name!='') ? 'display:none;' : ''; ?>">
<?php
$color_attribute_is_there="no";
if(strtolower($data['attribute_1'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_1_value'])[0];
}
if(strtolower($data['attribute_2'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_2_value'])[0];
}
if(strtolower($data['attribute_3'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_3_value'])[0];
}
if(strtolower($data['attribute_4'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_4_value'])[0];
}
?>
<?php
if($color_attribute_is_there=="yes"){
if(strtolower($data['attribute_1'])!="color"){
if($data['attribute_1_value']!=""){
echo " - ".$data['attribute_1_value'];
}
}
if(strtolower($data['attribute_2'])!="color"){
if($data['attribute_2_value']!=""){
echo " - ".$data['attribute_2_value'];
}
}
if(strtolower($data['attribute_3'])!="color"){
if($data['attribute_3_value']!=""){
echo " - ".$data['attribute_3_value'];
}
}
if(strtolower($data['attribute_4'])!="color"){
if($data['attribute_4_value']!=""){
echo " - ".$data['attribute_4_value'];
}
}
}
else{
$noncolor_attribute_first_is_there="no";
$noncolor_attribute_second_is_there="no";

if(strtolower($data['attribute_1'])!="color"){
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_1_value'];
}
if(strtolower($data['attribute_2'])!="color"){
if($noncolor_attribute_first_is_there=="yes"){
$noncolor_attribute_second_is_there="yes";
if($data['attribute_2_value']!=""){
echo " - ".$data['attribute_2_value'];
}
}
else{
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_2_value'];
}
}
if(strtolower($data['attribute_3'])!="color"){
if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
if($noncolor_attribute_first_is_there=="yes"){
$noncolor_attribute_second_is_there="yes";
if($data['attribute_3_value']!=""){
echo " - ".$data['attribute_3_value'];
}
}
else{
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_3_value'];
}
}
else{
	if($data['attribute_3_value']!=''){
		echo " - ".$data['attribute_3_value'];
	}
}
}
if(strtolower($data['attribute_4'])!="color"){
if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
if($noncolor_attribute_first_is_there=="yes"){
$noncolor_attribute_second_is_there="yes";
if($data['attribute_4_value']!=""){
echo " - ".$data['attribute_4_value'];
}
}
else{
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_4_value'];
}
}
else{
	if($data['attribute_4_value']!=''){
		echo " - ".$data['attribute_4_value'];
	}
}
}


}
$color_attribute_is_there="no";
$noncolor_attribute_first_is_there="no";
$noncolor_attribute_second_is_there="no";
?>
</span>
<!---added attributes ---->

</a></h5>
</div>
</div>
<!---ratings---added------------->								
<div class="row mb-15">
<div class="col-xs-12 col-sm-12">
<?php

$inventory_id=$inventory_id;
$rate_number=$controller->number_of_ratings($inventory_id);
$number_ratings_one_to_five=$controller->number_ratings_one_to_five($inventory_id);
$total_ratings=$rate_number;

if($total_ratings!=0){
foreach($number_ratings_one_to_five as $rating=>$rating_count){
if($rating=="one"){
$one_rate_percent=($rating_count/$total_ratings)*100;
$one_rate_count=$rating_count;
}
if($rating=="two"){
$two_rate_percent=($rating_count/$total_ratings)*100;
$two_rate_count=$rating_count;
}
if($rating=="three"){
$three_rate_percent=($rating_count/$total_ratings)*100;
$three_rate_count=$rating_count;
}
if($rating=="four"){
$four_rate_percent=($rating_count/$total_ratings)*100;
$four_rate_count=$rating_count;
}
if($rating=="five"){
$five_rate_percent=($rating_count/$total_ratings)*100;
$five_rate_count=$rating_count;
}
}
}else{
$one_rate_percent=0;$two_rate_percent=0;$three_rate_percent=0;$four_rate_percent=0;$five_rate_percent=0;
$one_rate_count=0;$two_rate_count=0;$three_rate_count=0;$four_rate_count=0;$five_rate_count=0;
}

$avg_value=$controller->get_average_value_of_ratings($inventory_id);
//$avg_value;
//unset($avg);		
$username = $this->session->userdata('customer_name');                   

$review_number=$controller->number_of_reviews($inventory_id);               
//echo"&nbsp;"."$review_number".'&nbsp;Reviews &nbsp;';
?>

<div class="product-star has-popover_knockoutdeals_<?php echo $inventory_id;?>" rel="popover" style="background-color:#388e3c;color:#fff;padding:4px 8px;border-radius:2px;">
<?php
echo $avg_value;
//echo "(".$rate_number.")";
?>
<i class="fa fa-star" style="margin-left:0.5em;line-height: 1.5;font-size: 0.85em;"></i>

</div><!---product-star-->

<script>
$(function () {

var showPopover = function () {
//$(".infopoint").popover(options);
$(this).popover('show');
progressBar_knockoutdeals(<?php echo $five_rate_percent; ?>, $('#progressBar_knockoutdeals_<?php echo $inventory_id; ?>_5'));
progressBar_knockoutdeals(<?php echo $four_rate_percent; ?>, $('#progressBar_knockoutdeals_<?php echo $inventory_id; ?>_4'));
progressBar_knockoutdeals(<?php echo $three_rate_percent; ?>, $('#progressBar_knockoutdeals_<?php echo $inventory_id; ?>_3'));
progressBar_knockoutdeals(<?php echo $two_rate_percent; ?>, $('#progressBar_knockoutdeals_<?php echo $inventory_id; ?>_2'));
progressBar_knockoutdeals(<?php echo $one_rate_percent; ?>, $('#progressBar_knockoutdeals_<?php echo $inventory_id; ?>_1'));

}
, hidePopover = function () {
$(this).popover('hide');
};

$('.has-popover_knockoutdeals_<?php echo $inventory_id; ?>').popover({
placement: function(tip, element) {
var $element, above, actualHeight, actualWidth, below, boundBottom, boundLeft, boundRight, boundTop, elementAbove, elementBelow, elementLeft, elementRight, isWithinBounds, left, pos, right;
isWithinBounds = function(elementPosition) {
return boundTop < elementPosition.top && boundLeft < elementPosition.left && boundRight > (elementPosition.left + actualWidth) && boundBottom > (elementPosition.top + actualHeight);
};
$element = $(element);
pos = $.extend({}, $element.offset(), {
width: element.offsetWidth,
height: element.offsetHeight
});
actualWidth = 283;
actualHeight = 117;
boundTop = $(document).scrollTop();
boundLeft = $(document).scrollLeft();
boundRight = boundLeft + $(window).width();
boundBottom = boundTop + $(window).height();
elementAbove = {
top: pos.top - actualHeight,
left: pos.left + pos.width / 2 - actualWidth / 2
};
elementBelow = {
top: pos.top + pos.height,
left: pos.left + pos.width / 2 - actualWidth / 2
};
elementLeft = {
top: pos.top + pos.height / 2 - actualHeight / 2,
left: pos.left - actualWidth
};
elementRight = {
top: pos.top + pos.height / 2 - actualHeight / 2,
left: pos.left + pos.width
};
above = isWithinBounds(elementAbove);
below = isWithinBounds(elementBelow);
left = isWithinBounds(elementLeft);
right = isWithinBounds(elementRight);
if (right) {
return "right";
} else {
if (left) {
return "left";

} else {
if (top) {
return "top";
} else {
if (below) {
return "bottom";
} else {
return "right";
}
}
}
}
},
html : true,
content: function(){return $("#ratings_knockoutdeals_<?php echo $inventory_id; ?>").html();},
trigger: 'manual'

})
.focus(showPopover)
.blur(hidePopover)
.hover(showPopover,hidePopover);
});

function progressBar_knockoutdeals(percent, $element) {
var progressBarWidth = percent * $element.width() / 100;
//$element.find('div').css({ width: progressBarWidth }).html( "&nbsp;");
$element.find('div').animate({ width: progressBarWidth }, 500).html( "&nbsp;");
}


</script>

<div id="ratings_knockoutdeals_<?php echo $inventory_id; ?>" class="popover" style="display:none;color:#808080;">
<div style="width:40%;float:left;color:#808080;">
<b> Ratings </b><br>
<i class="fa fa-star fa-2x"> &nbsp;<?php echo $avg_value;?></i><br>

Average of <?php echo $rate_number; ?>
</div>
<div class="horizontal_bar" style="color:#808080;">
<div class="star_rate_1">
<div class="star_rate_2">5 star</div><div id="progressBar_knockoutdeals_<?php echo $inventory_id; ?>_5" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $five_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">4 star</div><div id="progressBar_knockoutdeals_<?php echo $inventory_id; ?>_4" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $four_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">3 star</div><div id="progressBar_knockoutdeals_<?php echo $inventory_id; ?>_3" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $three_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">2 star</div><div id="progressBar_knockoutdeals_<?php echo $inventory_id; ?>_2" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $two_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">1 star</div><div id="progressBar_knockoutdeals_<?php echo $inventory_id; ?>_1" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $one_rate_count ?>)&nbsp;</div>
</div>
</div>		
</div>
</div>		
</div>

<!---ratings---added------------->
<div class="row">
<div class="col-xs-12 col-sm-12">
<h5 class="product-name pb-2" style="font-size:1em;color:#95918c;">
<b><?php echo $controller->get_brand_name($pro->brand_id);?></b>
</h5>								
<?php
/*
<h5 class="attribute-col">&nbsp;
<?php

if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4_value"])[0];
}



if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1_value"]!=""){
echo "<h5 class='attribute-col' style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2_value"]!=""){
echo "<h5 class='attribute-col'  style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3_value"]!=""){
echo "<h5 class='attribute-col'  style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4_value"]!=""){
echo "<h5 class='attribute-col' style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4_value"])."</h5>";
}
}



?>
</h5>
*/
?>



<div class="content_price">
<?php if(!empty($inv_discount_data)){ ?>
<span class="price product-price"><?php echo curr_sym; ?><?php echo $inv_discount_data['current_price']?></span>
<span class="price old-price"><?php echo curr_sym; ?><?php echo $inv_discount_data['inventory_price']?></span>
<small style="line-height: 1.8;">(Inclusive of Taxes)</small>
<?php }else{ ?>

<?php
if(get_pricetobeshown()=="selling_price"){
?>
<span class="price product-price"><?php echo curr_sym; ?><?php echo $pro->selling_price?></span>
                                        <?php
                                        if(intval($selling_price)<intval($max_selling_price)){
                                        ?>
                                        <span class="price old-price"><?php echo curr_sym; ?><?php echo round($max_selling_price); ?></span>
                                        <?php
                                        }
                                        ?>
<small>(Inclusive of Taxes)</small>
<?php
}
else{
?>
<span class="price product-price"><?php echo curr_sym; ?><?php echo $pro->base_price?> <font style="font-size:0.6em;line-height:2.4"> + GST</font></span> <small>(Inclusive of Taxes)</small>
<?php
}
?>

<?php } ?>

</div>
</div>
</div>
<?php
/*
if(get_pricetobeshown()=="selling_price"){
?>
<div class="row">
<div class="col-md-12">
<!---add to compare--->
<?php
$inventory_comparision=$controller->inventory_comparision_yes_or_no($inventory_id);
if($inventory_comparision=="yes"){
?>
<!--<a title="Add to compare" class="compare addToCompare" href="#" id="inventory_dealoftheday_<?php echo $inventory_id;?>"></a>-->
<div class="checkbox">
<label>
<input type="checkbox" value="" class="compare addToCompare" type="checkbox" id="inventory_<?php echo $inventory_id;?>">
<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
Add to Compare
</label>
</div>
<?php
}
?>
<!---add to compare--->
</div>
</div>
<?php } 
*/
?>
<div class="info-orther">
<p>Item Code: #453217907</p>
<p class="availability">Availability: <span>In stock</span></p>
<div class="product-desc more">
<?php echo $pro->product_description?>
</div>
</div>
</div>
</div>
</li>
<?php	
}

?>


</ul>
</div>
</div>
</div>
</section>

<?php } ?>

<!-------- knockoutdeals section ends -------------->




<!-------- trendingproducts section starts -------------->
<?php 
$products_obj=$ui_arr["invs_trendingproducts"];
?>
<?php if(!empty($products_obj)){ ?>
<section class=" section8 block-trending assignproducts_section" id="trendingproducts_range">
<div class="section-container">
<h3 class="section-title mb-20">Trending Products</h3>
<div class="section-content">
<div class="container">
<!--<ul class="products-style8 owl-carousel"  data-dots="true" data-loop="true" data-nav = "false" data-margin = "29" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":2},"1000":{"items":5}}'>--->
<ul class="row product-list grid">

<?php 

foreach($products_obj as $pro){
$inventory_id=$pro->inventory_id;
$product_id=$pro->product_id;
$selling_price=$pro->selling_price;
$max_selling_price = $pro->max_selling_price;
$selling_discount = $pro->selling_discount;
//$product_name=$pro->product_name;
//$product_description=$pro->product_description;

$inv_discount_data=$controller->get_default_discount_for_inv($inventory_id,$max_selling_price,$product_id);
///////////////  No of Offers starts ///////////////
$promo_numbers=0;
$promotions=$controller->get_number_of_offers($inventory_id);
$inventory_product_info_obj=$controller->get_inventory_product_info_by_inventory_id($inventory_id);

if(!empty($promotions) && ($inventory_product_info_obj->stock_available>0)){
$i=1; 
$defaultDiscounts_num=0;
foreach($promotions as $promotion){
foreach($promotion as $promo){

if (preg_match_all('/\d+(?=%)/', $promo-> get_type, $match)&&($promo->to_buy==1)){
$i++; 
if($promo->to_buy==1){
$defaultDiscounts_num=1;
}
}
}
}

if($defaultDiscounts_num!=0){
$promo_numbers=count($promotions)-$defaultDiscounts_num;
}else{
$promo_numbers=count($promotions);
}

}
//////////////   No of offers ends    ///////////////

//print_r($inv_discount_data);
$pro_name=($pro->sku_name!='') ?$pro->sku_name : $pro->product_name;
?>
<li class="col-xs-6 col-sm-3">
<div class="product-container selectProduct" data-id="<?php echo ucwords($pro_name)?>" data-title="<?php echo $inventory_id;?>">
<div class="left-block">
<?php 
$rand_1=$controller->sample_code();
$rand_2=$controller->sample_code();
?>
<a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pro->pcat_id}"; ?>/<?php echo "{$rand_2}{$pro->cat_id}"; ?>">


<img class="img-responsive productImg_<?php echo $inventory_id;?>" alt="productsku" src="<?php echo base_url().$pro->common_image;?>"/>

</a>

<div class="quick-view">
<?php 
if($this->session->userdata("customer_id")){ 
if($controller->check_wishlist($inventory_id)>0){
?>
<script>
$(document).ready(function(){
$("#wishlist_trendingproducts_"+<?php echo $inventory_id;?>).css({"color":""});
$("#wishlist_trendingproducts_"+<?php echo $inventory_id;?>).css({"color":"#ff4343;"});
});
</script>
<?php
$val="Added to Wishlist";
$style_wishlist_label="style='color:#eda900;'";
}
else{ 
$val="Add to Wishlist";
$style_wishlist_label="";
}
}else{
$val="Add to Wishlist";
$style_wishlist_label="";
}
?>
<?php
if($this->session->userdata("customer_id")){
?>
<span style="cursor:pointer;" id="anchor_trendingproducts_<?php echo $inventory_id;?>" onclick="add_to_wishlistFun(<?php echo $inventory_id;?>,'trendingproducts');return false;"  title="<?php echo $val; ?>" class="wishlist"><i id="<?php echo "wishlist_trendingproducts_".$inventory_id;?>" class="fa fa-heart" style='color:#c2c2c2;font-size: 1.2em;'></i></span>
<?php
}
else{
?>
<span style="cursor:pointer" id="anchor_trendingproducts_<?php echo $inventory_id;?>" onclick="add_to_wishlistFun(<?php echo $inventory_id;?>,'trendingproducts');return false;"  title="<?php echo $val; ?>" class="wishlist"><i id="<?php echo "wishlist_trendingproducts_".$inventory_id;?>" class="fa fa-heart" style='color:#c2c2c2;font-size: 1.2em;'></i></span>
<?php	
}
?>

</div>
<?php if(!empty($inv_discount_data)){ ?>
<div class="price-percent-reduction2" <?php if($inv_discount_data['discount']<10){echo "style='padding-top:18px'";}?>>
<?php echo  $inv_discount_data['discount'] ?>% OFF



</div>
<?php }else{ ?>

    <?php
                                                            if($selling_discount>0){
                                                            ?>
                                                                <div class="price-percent-reduction2" <?php if ($selling_discount < 10) {
                                                                echo "style='padding-top:18px'";
                                                            } ?>>
                                                                <?php echo round($selling_discount); ?>% OFF
                                                            </div>

                                                            <?php
                                                            }
                                                            ?>


<?php
}
if($promo_numbers>0){ 
?>
<div class="price-percent-reduction_offers">
<?php

if($promo_numbers>1){echo $promo_numbers.' Offers';}else{echo $promo_numbers.' Offer';}
?>
</div>
<?php
}
?>



</div>
<?php
/*
?>
<div class="product-info text-center">
<div class="product-name">
<a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pro->pcat_id}"; ?>/<?php echo "{$rand_2}{$pro->cat_id}"; ?>"><?php echo $pro->product_name;?></a>
</div>
<div class="button-control">

<a title="Add to Cart" class="btn btn-default" href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pro->pcat_id}"; ?>/<?php echo "{$rand_2}{$pro->cat_id}"; ?>">Explore</a>

</div>

</div>
<?php
*/
$pro_name=($pro->sku_name!='') ?$pro->sku_name : $pro->product_name;
?>



<div class="right-block heightmatched_trendingproducts">
<div class="row">
<div class="col-xs-12 col-sm-12">
<h5 class="product-name heightmatchedfortrendingproducts mb-10" style="font-size:1.05em;padding-bottom:0!important;word-break:break-word;line-height:1.3;"><a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pro->pcat_id}"; ?>/<?php echo "{$rand_2}{$pro->cat_id}"; ?>"  title="<?php
$str=$pro_name;
$str=str_replace("\r\n", '', $str);
$str=str_replace('\r\n', '', $str);
echo ucwords($str);
?>">

<?php
$str=$pro_name;
$str=str_replace("\r\n", '', $str);
$str=str_replace('\r\n', '', $str);
echo ucwords($str);
?>
<?php
$data=$controller->get_inventory_info_by_inventory_id($inventory_id);
?>

<!---added attributes ---->
<span style="<?php echo ($pro->sku_name!='') ? 'display:none;' : ''; ?>">
<?php
$color_attribute_is_there="no";
if(strtolower($data['attribute_1'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_1_value'])[0];
}
if(strtolower($data['attribute_2'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_2_value'])[0];
}
if(strtolower($data['attribute_3'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_3_value'])[0];
}
if(strtolower($data['attribute_4'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_4_value'])[0];
}
?>
<?php
if($color_attribute_is_there=="yes"){
if(strtolower($data['attribute_1'])!="color"){
if($data['attribute_1_value']!=""){
echo " - ".$data['attribute_1_value'];
}
}
if(strtolower($data['attribute_2'])!="color"){
if($data['attribute_2_value']!=""){
echo " - ".$data['attribute_2_value'];
}
}
if(strtolower($data['attribute_3'])!="color"){
if($data['attribute_3_value']!=""){
echo " - ".$data['attribute_3_value'];
}
}
if(strtolower($data['attribute_4'])!="color"){
if($data['attribute_4_value']!=""){
echo " - ".$data['attribute_4_value'];
}
}
}
else{
$noncolor_attribute_first_is_there="no";
$noncolor_attribute_second_is_there="no";

if(strtolower($data['attribute_1'])!="color"){
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_1_value'];
}
if(strtolower($data['attribute_2'])!="color"){
if($noncolor_attribute_first_is_there=="yes"){
$noncolor_attribute_second_is_there="yes";
if($data['attribute_2_value']!=""){
echo " - ".$data['attribute_2_value'];
}
}
else{
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_2_value'];
}
}
if(strtolower($data['attribute_3'])!="color"){
if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
if($noncolor_attribute_first_is_there=="yes"){
$noncolor_attribute_second_is_there="yes";
if($data['attribute_3_value']!=""){
echo " - ".$data['attribute_3_value'];
}
}
else{
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_3_value'];
}
}
else{
	if($data['attribute_3_value']!=''){
		echo " - ".$data['attribute_3_value'];
	}
}
}
if(strtolower($data['attribute_4'])!="color"){
if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
if($noncolor_attribute_first_is_there=="yes"){
$noncolor_attribute_second_is_there="yes";
if($data['attribute_4_value']!=""){
echo " - ".$data['attribute_4_value'];
}
}
else{
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_4_value'];
}
}
else{
	if($data['attribute_4_value']!=''){
		echo " - ".$data['attribute_4_value'];
	}
}
}


}
$color_attribute_is_there="no";
$noncolor_attribute_first_is_there="no";
$noncolor_attribute_second_is_there="no";
?>
</span>
<!---added attributes ---->

</a></h5>
</div>
</div>

<!---ratings---added------------->								
<div class="row mb-15">
<div class="col-xs-12 col-sm-12">
<?php

$inventory_id=$inventory_id;
$rate_number=$controller->number_of_ratings($inventory_id);
$number_ratings_one_to_five=$controller->number_ratings_one_to_five($inventory_id);
$total_ratings=$rate_number;

if($total_ratings!=0){
foreach($number_ratings_one_to_five as $rating=>$rating_count){
if($rating=="one"){
$one_rate_percent=($rating_count/$total_ratings)*100;
$one_rate_count=$rating_count;
}
if($rating=="two"){
$two_rate_percent=($rating_count/$total_ratings)*100;
$two_rate_count=$rating_count;
}
if($rating=="three"){
$three_rate_percent=($rating_count/$total_ratings)*100;
$three_rate_count=$rating_count;
}
if($rating=="four"){
$four_rate_percent=($rating_count/$total_ratings)*100;
$four_rate_count=$rating_count;
}
if($rating=="five"){
$five_rate_percent=($rating_count/$total_ratings)*100;
$five_rate_count=$rating_count;
}
}
}else{
$one_rate_percent=0;$two_rate_percent=0;$three_rate_percent=0;$four_rate_percent=0;$five_rate_percent=0;
$one_rate_count=0;$two_rate_count=0;$three_rate_count=0;$four_rate_count=0;$five_rate_count=0;
}

$avg_value=$controller->get_average_value_of_ratings($inventory_id);
//$avg_value;
//unset($avg);		
$username = $this->session->userdata('customer_name');                   

$review_number=$controller->number_of_reviews($inventory_id);               
//echo"&nbsp;"."$review_number".'&nbsp;Reviews &nbsp;';
?>

<div class="product-star has-popover_trendingproducts_<?php echo $inventory_id;?>" rel="popover" style="background-color:#388e3c;color:#fff;padding:4px 8px;border-radius:2px;">
<?php
echo $avg_value;
//echo "(".$rate_number.")";
?>
<i class="fa fa-star" style="margin-left:0.5em;line-height: 1.5;font-size: 0.85em;"></i>

</div><!---product-star-->

<script>
$(function () {

var showPopover = function () {
//$(".infopoint").popover(options);
$(this).popover('show');
progressBar_trendingproducts(<?php echo $five_rate_percent; ?>, $('#progressBar_trendingproducts_<?php echo $inventory_id; ?>_5'));
progressBar_trendingproducts(<?php echo $four_rate_percent; ?>, $('#progressBar_trendingproducts_<?php echo $inventory_id; ?>_4'));
progressBar_trendingproducts(<?php echo $three_rate_percent; ?>, $('#progressBar_trendingproducts_<?php echo $inventory_id; ?>_3'));
progressBar_trendingproducts(<?php echo $two_rate_percent; ?>, $('#progressBar_trendingproducts_<?php echo $inventory_id; ?>_2'));
progressBar_trendingproducts(<?php echo $one_rate_percent; ?>, $('#progressBar_trendingproducts_<?php echo $inventory_id; ?>_1'));

}
, hidePopover = function () {
$(this).popover('hide');
};

$('.has-popover_trendingproducts_<?php echo $inventory_id; ?>').popover({
placement: function(tip, element) {
var $element, above, actualHeight, actualWidth, below, boundBottom, boundLeft, boundRight, boundTop, elementAbove, elementBelow, elementLeft, elementRight, isWithinBounds, left, pos, right;
isWithinBounds = function(elementPosition) {
return boundTop < elementPosition.top && boundLeft < elementPosition.left && boundRight > (elementPosition.left + actualWidth) && boundBottom > (elementPosition.top + actualHeight);
};
$element = $(element);
pos = $.extend({}, $element.offset(), {
width: element.offsetWidth,
height: element.offsetHeight
});
actualWidth = 283;
actualHeight = 117;
boundTop = $(document).scrollTop();
boundLeft = $(document).scrollLeft();
boundRight = boundLeft + $(window).width();
boundBottom = boundTop + $(window).height();
elementAbove = {
top: pos.top - actualHeight,
left: pos.left + pos.width / 2 - actualWidth / 2
};
elementBelow = {
top: pos.top + pos.height,
left: pos.left + pos.width / 2 - actualWidth / 2
};
elementLeft = {
top: pos.top + pos.height / 2 - actualHeight / 2,
left: pos.left - actualWidth
};
elementRight = {
top: pos.top + pos.height / 2 - actualHeight / 2,
left: pos.left + pos.width
};
above = isWithinBounds(elementAbove);
below = isWithinBounds(elementBelow);
left = isWithinBounds(elementLeft);
right = isWithinBounds(elementRight);
if (right) {
return "right";
} else {
if (left) {
return "left";

} else {
if (top) {
return "top";
} else {
if (below) {
return "bottom";
} else {
return "right";
}
}
}
}
},
html : true,
content: function(){return $("#ratings_trendingproducts_<?php echo $inventory_id; ?>").html();},
trigger: 'manual'

})
.focus(showPopover)
.blur(hidePopover)
.hover(showPopover,hidePopover);
});

function progressBar_trendingproducts(percent, $element) {
var progressBarWidth = percent * $element.width() / 100;
//$element.find('div').css({ width: progressBarWidth }).html( "&nbsp;");
$element.find('div').animate({ width: progressBarWidth }, 500).html( "&nbsp;");
}


</script>

<div id="ratings_trendingproducts_<?php echo $inventory_id; ?>" class="popover" style="display:none;color:#808080;">
<div style="width:40%;float:left;color:#808080;">
<b> Ratings </b><br>
<i class="fa fa-star fa-2x"> &nbsp;<?php echo $avg_value;?></i><br>

Average of <?php echo $rate_number; ?>
</div>
<div class="horizontal_bar" style="color:#808080;">
<div class="star_rate_1">
<div class="star_rate_2">5 star</div><div id="progressBar_trendingproducts_<?php echo $inventory_id; ?>_5" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $five_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">4 star</div><div id="progressBar_trendingproducts_<?php echo $inventory_id; ?>_4" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $four_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">3 star</div><div id="progressBar_trendingproducts_<?php echo $inventory_id; ?>_3" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $three_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">2 star</div><div id="progressBar_trendingproducts_<?php echo $inventory_id; ?>_2" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $two_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">1 star</div><div id="progressBar_trendingproducts_<?php echo $inventory_id; ?>_1" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $one_rate_count ?>)&nbsp;</div>
</div>
</div>		
</div>
</div>		
</div>

<!---ratings---added------------->
<div class="row">
<div class="col-xs-12 col-sm-12">
<h5 class="product-name pb-2" style="font-size:1em;color:#95918c;">
<b><?php echo $controller->get_brand_name($pro->brand_id);?></b>
</h5>								
<?php /*
<h5 class="attribute-col">&nbsp;
<?php

if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4_value"])[0];
}



if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1_value"]!=""){
echo "<h5 class='attribute-col' style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2_value"]!=""){
echo "<h5 class='attribute-col'  style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3_value"]!=""){
echo "<h5 class='attribute-col'  style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4_value"]!=""){
echo "<h5 class='attribute-col' style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4_value"])."</h5>";
}
}



?>
</h5>
*/
?>


<div class="content_price">
<?php if(!empty($inv_discount_data)){ ?>
<span class="price product-price"><?php echo curr_sym; ?><?php echo $inv_discount_data['current_price']?></span>
<span class="price old-price"><?php echo curr_sym; ?><?php echo $inv_discount_data['inventory_price']?></span>
<small style="line-height: 1.8;">(Inclusive of Taxes)</small>
<?php }else{ ?>

<?php
if(get_pricetobeshown()=="selling_price"){
?>
<span class="price product-price"><?php echo curr_sym; ?><?php echo $pro->selling_price?></span>
                                        <?php
                                        if(intval($selling_price)<intval($max_selling_price)){
                                        ?>
                                        <span class="price old-price"><?php echo curr_sym; ?><?php echo round($max_selling_price); ?></span>
                                        <?php
                                        }
                                        ?>
<small>(Inclusive of Taxes)</small>
<?php
}
else{
?>
<span class="price product-price"><?php echo curr_sym; ?><?php echo $pro->base_price?> <font style="font-size:0.6em;line-height:2.4"> + GST</font></span> <small>(Inclusive of Taxes)</small>
<?php
}
?>

<?php } ?>

</div>
</div>
</div>
<?php
/*
if(get_pricetobeshown()=="selling_price"){
?>
<div class="row">
<div class="col-md-12">
<!---add to compare--->
<?php
$inventory_comparision=$controller->inventory_comparision_yes_or_no($inventory_id);
if($inventory_comparision=="yes"){
?>
<!--<a title="Add to compare" class="compare addToCompare" href="#" id="inventory_dealoftheday_<?php echo $inventory_id;?>"></a>-->
<div class="checkbox">
<label>
<input type="checkbox" value="" class="compare addToCompare" type="checkbox" id="inventory_<?php echo $inventory_id;?>">
<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
Add to Compare
</label>
</div>
<?php
}
?>
<!---add to compare--->
</div>
</div>
<?php } 
*/
?>
<div class="info-orther">
<p>Item Code: #453217907</p>
<p class="availability">Availability: <span>In stock</span></p>
<div class="product-desc more">
<?php echo $pro->product_description?>
</div>
</div>
</div>




</div>
</li>
<?php	
}

?>


</ul>
</div>
</div>
</div>
</section>

<?php } ?>

<!-------- trendingproducts section ends -------------->



<!-------- dealoftheday section starts -------------->
<?php 
$products_obj=$ui_arr["invs_dealoftheday"];
?>
<?php if(!empty($products_obj)){ ?>
<section class=" section8 block-trending assignproducts_section" id="dealoftheday_range">
<div class="section-container">
<h3 class="section-title mb-20" style="text-transform: unset;">Hot Selling Designs</h3>
<div class="section-content">
<div class="container">
<!--<ul class="products-style8 owl-carousel"  data-dots="true" data-loop="true" data-nav = "false" data-margin = "29" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":2},"1000":{"items":5}}'>--->
<ul class="row product-list grid">

<?php 

foreach($products_obj as $pro){
$inventory_id=$pro->inventory_id;
$product_id=$pro->product_id;
$selling_price=$pro->selling_price;
$max_selling_price = $pro->max_selling_price;
$selling_discount = $pro->selling_discount;
//$product_name=$pro->product_name;
//$product_description=$pro->product_description;

$inv_discount_data=$controller->get_default_discount_for_inv($inventory_id,$max_selling_price,$product_id);
///////////////  No of Offers starts ///////////////
$promo_numbers=0;
$promotions=$controller->get_number_of_offers($inventory_id);
$inventory_product_info_obj=$controller->get_inventory_product_info_by_inventory_id($inventory_id);

if(!empty($promotions) && ($inventory_product_info_obj->stock_available>0)){
$i=1; 
$defaultDiscounts_num=0;
foreach($promotions as $promotion){
foreach($promotion as $promo){

if (preg_match_all('/\d+(?=%)/', $promo-> get_type, $match)&&($promo->to_buy==1)){
$i++; 
if($promo->to_buy==1){
$defaultDiscounts_num=1;
}
}
}
}

if($defaultDiscounts_num!=0){
$promo_numbers=count($promotions)-$defaultDiscounts_num;
}else{
$promo_numbers=count($promotions);
}

}
//////////////   No of offers ends    ///////////////

//print_r($inv_discount_data);
$pro_name=($pro->sku_name!='') ?$pro->sku_name : $pro->product_name;
?>
<li class="col-xs-6 col-sm-3">
<div class="product-container selectProduct" data-id="<?php echo ucwords($pro_name)?>" data-title="<?php echo $inventory_id;?>">
<div class="left-block">
<?php 
$rand_1=$controller->sample_code();
$rand_2=$controller->sample_code();
?>
<a href="<?php echo base_url()?>detail/<?php echo "{$rand_1}{$inventory_id}";?>">


<img class="img-responsive productImg_<?php echo $inventory_id;?>" alt="productsku" src="<?php echo base_url().$pro->common_image;?>"/>

</a>

<div class="quick-view">
<?php 
if($this->session->userdata("customer_id")){ 
if($controller->check_wishlist($inventory_id)>0){
?>
<script>
$(document).ready(function(){
$("#wishlist_dealoftheday_"+<?php echo $inventory_id;?>).css({"color":""});
$("#wishlist_dealoftheday_"+<?php echo $inventory_id;?>).css({"color":"#ff4343;"});
});
</script>
<?php
$val="Added to Wishlist";
$style_wishlist_label="style='color:#eda900;'";
}
else{ 
$val="Add to Wishlist";
$style_wishlist_label="";
}
}else{
$val="Add to Wishlist";
$style_wishlist_label="";
}
?>
<?php
if($this->session->userdata("customer_id")){
?>
<span style="cursor:pointer;" id="anchor_dealoftheday_<?php echo $inventory_id;?>" onclick="add_to_wishlistFun(<?php echo $inventory_id;?>,'dealoftheday');return false;"  title="<?php echo $val; ?>" class="wishlist"><i id="<?php echo "wishlist_dealoftheday_".$inventory_id;?>" class="fa fa-heart" style='color:#c2c2c2;font-size: 1.2em;'></i></span>
<?php
}
else{
?>
<span style="cursor:pointer" id="anchor_dealoftheday_<?php echo $inventory_id;?>" onclick="add_to_wishlistFun(<?php echo $inventory_id;?>,'dealoftheday');return false;"  title="<?php echo $val; ?>" class="wishlist"><i id="<?php echo "wishlist_dealoftheday_".$inventory_id;?>" class="fa fa-heart" style='color:#c2c2c2;font-size: 1.2em;'></i></span>
<?php	
}
?>

</div>
<?php if(!empty($inv_discount_data)){ ?>
<div class="price-percent-reduction2" <?php if($inv_discount_data['discount']<10){echo "style='padding-top:18px'";}?>>
<?php echo  $inv_discount_data['discount'] ?>% OFF



</div>
<?php }else{ ?>

<?php
                                                            if($selling_discount>0){
                                                            ?>
                                                                <div class="price-percent-reduction2" <?php if ($selling_discount < 10) {
                                                                echo "style='padding-top:18px'";
                                                            } ?>>
                                                                <?php echo round($selling_discount); ?>% OFF
                                                            </div>

                                                            <?php
                                                            }
                                                            ?>

<?php
}
if($promo_numbers>0){ 
?>
<div class="price-percent-reduction_offers">
<?php

if($promo_numbers>1){echo $promo_numbers.' Offers';}else{echo $promo_numbers.' Offer';}
?>
</div>
<?php
}
?>



</div>
<?php
/*
?>
<div class="product-info text-center">
<div class="product-name">
<a href="<?php echo base_url()?>detail/<?php echo "{$rand_1}{$inventory_id}";?>"><?php echo $pro->product_name;?></a>
</div>
<div class="button-control">

<a title="Add to Cart" class="btn btn-default" href="<?php echo base_url()?>detail/<?php echo "{$rand_1}{$inventory_id}";?>">Explore</a>

</div>

</div>
<?php
*/
$pro_name=($pro->sku_name!='') ?$pro->sku_name : $pro->product_name;
?>



<div class="right-block heightmatched_dealoftheday">
<div class="row">
<div class="col-xs-12 col-sm-12">
<h5 class="product-name heightmatchedfordealoftheday mb-10" style="font-size:1.05em;padding-bottom:0!important;word-break:break-word;line-height:1.3;"><a href="<?php echo base_url()?>detail/<?php echo "{$rand_1}{$inventory_id}";?>"  title="<?php
$str=$pro_name;
$str=str_replace("\r\n", '', $str);
$str=str_replace('\r\n', '', $str);
echo ucwords($str);
?>">

<?php
$str=$pro_name;
$str=str_replace("\r\n", '', $str);
$str=str_replace('\r\n', '', $str);
echo ucwords($str);
?>
<?php
$data=$controller->get_inventory_info_by_inventory_id($inventory_id);
?>

<!---added attributes ---->
<span style="<?php echo ($pro->sku_name!='') ? 'display:none;' : ''; ?>">
<?php
$color_attribute_is_there="no";
if(strtolower($data['attribute_1'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_1_value'])[0];
}
if(strtolower($data['attribute_2'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_2_value'])[0];
}
if(strtolower($data['attribute_3'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_3_value'])[0];
}
if(strtolower($data['attribute_4'])=="color"){
$color_attribute_is_there="yes";
echo " - ".explode(":",$data['attribute_4_value'])[0];
}
?>
<?php
if($color_attribute_is_there=="yes"){
if(strtolower($data['attribute_1'])!="color"){
if($data['attribute_1_value']!=""){
echo " - ".$data['attribute_1_value'];
}
}
if(strtolower($data['attribute_2'])!="color"){
if($data['attribute_2_value']!=""){
echo " - ".$data['attribute_2_value'];
}
}
if(strtolower($data['attribute_3'])!="color"){
if($data['attribute_3_value']!=""){
echo " - ".$data['attribute_3_value'];
}
}
if(strtolower($data['attribute_4'])!="color"){
if($data['attribute_4_value']!=""){
echo " - ".$data['attribute_4_value'];
}
}
}
else{
$noncolor_attribute_first_is_there="no";
$noncolor_attribute_second_is_there="no";

if(strtolower($data['attribute_1'])!="color"){
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_1_value'];
}
if(strtolower($data['attribute_2'])!="color"){
if($noncolor_attribute_first_is_there=="yes"){
$noncolor_attribute_second_is_there="yes";
if($data['attribute_2_value']!=""){
echo " - ".$data['attribute_2_value'];
}
}
else{
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_2_value'];
}
}
if(strtolower($data['attribute_3'])!="color"){
if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
if($noncolor_attribute_first_is_there=="yes"){
$noncolor_attribute_second_is_there="yes";
if($data['attribute_3_value']!=""){
echo " - ".$data['attribute_3_value'];
}
}
else{
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_3_value'];
}
}
else{
	if($data['attribute_3_value']!=''){
		echo " - ".$data['attribute_3_value'];
	}
}
}
if(strtolower($data['attribute_4'])!="color"){
if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
if($noncolor_attribute_first_is_there=="yes"){
$noncolor_attribute_second_is_there="yes";
if($data['attribute_4_value']!=""){
echo " - ".$data['attribute_4_value'];
}
}
else{
$noncolor_attribute_first_is_there="yes";
echo " - ".$data['attribute_4_value'];
}
}
else{
	if($data['attribute_4_value']!=''){
		echo " - ".$data['attribute_4_value'];
	}
}
}


}
$color_attribute_is_there="no";
$noncolor_attribute_first_is_there="no";
$noncolor_attribute_second_is_there="no";
?>
</span>
<!---added attributes ---->


</a></h5>
</div>
</div>

<!---ratings---added------------->								
<div class="row mb-15">
<div class="col-xs-12 col-sm-12">
<?php

$inventory_id=$inventory_id;
$rate_number=$controller->number_of_ratings($inventory_id);
$number_ratings_one_to_five=$controller->number_ratings_one_to_five($inventory_id);
$total_ratings=$rate_number;

if($total_ratings!=0){
foreach($number_ratings_one_to_five as $rating=>$rating_count){
if($rating=="one"){
$one_rate_percent=($rating_count/$total_ratings)*100;
$one_rate_count=$rating_count;
}
if($rating=="two"){
$two_rate_percent=($rating_count/$total_ratings)*100;
$two_rate_count=$rating_count;
}
if($rating=="three"){
$three_rate_percent=($rating_count/$total_ratings)*100;
$three_rate_count=$rating_count;
}
if($rating=="four"){
$four_rate_percent=($rating_count/$total_ratings)*100;
$four_rate_count=$rating_count;
}
if($rating=="five"){
$five_rate_percent=($rating_count/$total_ratings)*100;
$five_rate_count=$rating_count;
}
}
}else{
$one_rate_percent=0;$two_rate_percent=0;$three_rate_percent=0;$four_rate_percent=0;$five_rate_percent=0;
$one_rate_count=0;$two_rate_count=0;$three_rate_count=0;$four_rate_count=0;$five_rate_count=0;
}

$avg_value=$controller->get_average_value_of_ratings($inventory_id);
//$avg_value;
//unset($avg);		
$username = $this->session->userdata('customer_name');                   

$review_number=$controller->number_of_reviews($inventory_id);               
//echo"&nbsp;"."$review_number".'&nbsp;Reviews &nbsp;';
?>

<div class="product-star has-popover_dealoftheday_<?php echo $inventory_id;?>" rel="popover" style="background-color:#388e3c;color:#fff;padding:4px 8px;border-radius:2px;">
<?php
echo $avg_value;
//echo "(".$rate_number.")";
?>
<i class="fa fa-star" style="margin-left:0.5em;line-height: 1.5;font-size: 0.85em;"></i>

</div><!---product-star-->

<script>
$(function () {

var showPopover = function () {
//$(".infopoint").popover(options);
$(this).popover('show');
progressBar_dealoftheday(<?php echo $five_rate_percent; ?>, $('#progressBar_dealoftheday_<?php echo $inventory_id; ?>_5'));
progressBar_dealoftheday(<?php echo $four_rate_percent; ?>, $('#progressBar_dealoftheday_<?php echo $inventory_id; ?>_4'));
progressBar_dealoftheday(<?php echo $three_rate_percent; ?>, $('#progressBar_dealoftheday_<?php echo $inventory_id; ?>_3'));
progressBar_dealoftheday(<?php echo $two_rate_percent; ?>, $('#progressBar_dealoftheday_<?php echo $inventory_id; ?>_2'));
progressBar_dealoftheday(<?php echo $one_rate_percent; ?>, $('#progressBar_dealoftheday_<?php echo $inventory_id; ?>_1'));

}
, hidePopover = function () {
$(this).popover('hide');
};

$('.has-popover_dealoftheday_<?php echo $inventory_id; ?>').popover({
placement: function(tip, element) {
var $element, above, actualHeight, actualWidth, below, boundBottom, boundLeft, boundRight, boundTop, elementAbove, elementBelow, elementLeft, elementRight, isWithinBounds, left, pos, right;
isWithinBounds = function(elementPosition) {
return boundTop < elementPosition.top && boundLeft < elementPosition.left && boundRight > (elementPosition.left + actualWidth) && boundBottom > (elementPosition.top + actualHeight);
};
$element = $(element);
pos = $.extend({}, $element.offset(), {
width: element.offsetWidth,
height: element.offsetHeight
});
actualWidth = 283;
actualHeight = 117;
boundTop = $(document).scrollTop();
boundLeft = $(document).scrollLeft();
boundRight = boundLeft + $(window).width();
boundBottom = boundTop + $(window).height();
elementAbove = {
top: pos.top - actualHeight,
left: pos.left + pos.width / 2 - actualWidth / 2
};
elementBelow = {
top: pos.top + pos.height,
left: pos.left + pos.width / 2 - actualWidth / 2
};
elementLeft = {
top: pos.top + pos.height / 2 - actualHeight / 2,
left: pos.left - actualWidth
};
elementRight = {
top: pos.top + pos.height / 2 - actualHeight / 2,
left: pos.left + pos.width
};
above = isWithinBounds(elementAbove);
below = isWithinBounds(elementBelow);
left = isWithinBounds(elementLeft);
right = isWithinBounds(elementRight);
if (right) {
return "right";
} else {
if (left) {
return "left";

} else {
if (top) {
return "top";
} else {
if (below) {
return "bottom";
} else {
return "right";
}
}
}
}
},
html : true,
content: function(){return $("#ratings_dealoftheday_<?php echo $inventory_id; ?>").html();},
trigger: 'manual'

})
.focus(showPopover)
.blur(hidePopover)
.hover(showPopover,hidePopover);
});

function progressBar_dealoftheday(percent, $element) {
var progressBarWidth = percent * $element.width() / 100;
//$element.find('div').css({ width: progressBarWidth }).html( "&nbsp;");
$element.find('div').animate({ width: progressBarWidth }, 500).html( "&nbsp;");
}


</script>

<div id="ratings_dealoftheday_<?php echo $inventory_id; ?>" class="popover" style="display:none;color:#808080;">
<div style="width:40%;float:left;color:#808080;">
<b> Ratings </b><br>
<i class="fa fa-star fa-2x"> &nbsp;<?php echo $avg_value;?></i><br>

Average of <?php echo $rate_number; ?>
</div>
<div class="horizontal_bar" style="color:#808080;">
<div class="star_rate_1">
<div class="star_rate_2">5 star</div><div id="progressBar_dealoftheday_<?php echo $inventory_id; ?>_5" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $five_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">4 star</div><div id="progressBar_dealoftheday_<?php echo $inventory_id; ?>_4" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $four_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">3 star</div><div id="progressBar_dealoftheday_<?php echo $inventory_id; ?>_3" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $three_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">2 star</div><div id="progressBar_dealoftheday_<?php echo $inventory_id; ?>_2" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $two_rate_count ?>)&nbsp;</div>
</div>
<div class="star_rate_1">
<div class="star_rate_2">1 star</div><div id="progressBar_dealoftheday_<?php echo $inventory_id; ?>_1" class="jquery-ui-like"><div class="hbar"></div></div><div class="star_rate_3">&nbsp;(<?php echo $one_rate_count ?>)&nbsp;</div>
</div>

</div>		
</div>
</div>
</div>
<!---ratings---added------------->
<div class="row">
<div class="col-xs-12 col-sm-12">
<h5 class="product-name pb-2" style="font-size:1em;color:#95918c;">
<b><?php echo $controller->get_brand_name($pro->brand_id);?></b>
</h5>								
<?php
/*
<h5 class="attribute-col">&nbsp;
<?php

if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4_value"])[0];
}



if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1_value"]!=""){
echo "<h5 class='attribute-col' style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_1_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2_value"]!=""){
echo "<h5 class='attribute-col'  style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_2_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3_value"]!=""){
echo "<h5 class='attribute-col'  style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_3_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4"])!="color"){
if($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4_value"]!=""){
echo "<h5 class='attribute-col' style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($inventory_id)["attribute_4_value"])."</h5>";
}
}



?>
</h5>
*/
?>



<div class="content_price">
<?php if(!empty($inv_discount_data)){ ?>
<span class="price product-price"><?php echo curr_sym; ?><?php echo $inv_discount_data['current_price']?></span>
<span class="price old-price"><?php echo curr_sym; ?><?php echo $inv_discount_data['inventory_price']?></span>
<small style="line-height: 1.8;">(Inclusive of Taxes)</small>
<?php }else{ ?>

<?php
if(get_pricetobeshown()=="selling_price"){
?>
<span class="price product-price"><?php echo curr_sym; ?><?php echo $pro->selling_price?></span>
                                        <?php
                                        if(intval($selling_price)<intval($max_selling_price)){
                                        ?>
                                        <span class="price old-price"><?php echo curr_sym; ?><?php echo round($max_selling_price); ?></span>
                                        <?php
                                        }
                                        ?>
<small>(Inclusive of Taxes)</small>
<?php
}
else{
?>
<span class="price product-price"><?php echo curr_sym; ?><?php echo $pro->base_price?> <font style="font-size:0.6em;line-height:2.4"> + GST</font></span> <small>(Inclusive of Taxes)</small>
<?php
}
?>

<?php } ?>

</div>
</div>
</div>
<?php
/*
if(get_pricetobeshown()=="selling_price"){
?>
<div class="row">
<div class="col-md-12">
<h5>
<small>
(Inclusive of Taxes)
</small>
</h5>
<!---add to compare--->
<?php
$inventory_comparision=$controller->inventory_comparision_yes_or_no($inventory_id);
if($inventory_comparision=="yes"){
?>
<!--<a title="Add to compare" class="compare addToCompare" href="#" id="inventory_dealoftheday_<?php echo $inventory_id;?>"></a>-->
<div class="checkbox">
<label>
<input type="checkbox" value="" class="compare addToCompare" type="checkbox" id="inventory_<?php echo $inventory_id;?>">
<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
Add to Compare
</label>
</div>
<?php
}
?>
<!---add to compare--->
</div>
</div>
<?php } 
*/
?>
<div class="info-orther">
<p>Item Code: #453217907</p>
<p class="availability">Availability: <span>In stock</span></p>
<div class="product-desc more">
<?php echo $pro->product_description?>
</div>
</div>
</div>
</div>
</li>
<?php	
}

?>


</ul>
</div>
</div>
</div>
</section>

<?php } ?>

<!-------- dealoftheday section ends -------------->

<style>
.rb-section-testimonial .rb-testimonial-meta-inner:nth-child(odd) .item {
    background: #e6f1f4;
}
.rb-section-testimonial .item{
	background: #f4ece6;
	padding: 16px;
	height: 400px;
}
.rb-widget-testimonial .star {
    margin-bottom: 10px;
    width: auto;
    float: none;
    display: -webkit-box;
    display: -moz-box;
    display: box;
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flexbox;
    display: flex;
	color: #d8d8d8;
	overflow: hidden;
	height: auto;
    cursor: pointer;
    font-size: 14px;
	margin: 0 1px 0 0;
	text-align: center;
}
.rb-widget-testimonial .star.star-3:before {
    content: "\f005\f005\f005";
}
.rb-widget-testimonial .star:before, .rb-widget-testimonial .star:after {
    display: inline-block;
    color: #ffc107;
    font-family: "FontAwesome";
    font-size: 13px;
    line-height: 1;
    letter-spacing: 4px;
}
.rb-widget-testimonial .star:after {
    color: #e1e1e1;
}
.rb-widget-testimonial .star.star-3:after {
    content: "\f005\f005";
}
.rb-section-testimonial .title-item {
    font-size: 24px;
    margin-bottom: 15px;
    color: #000;
}
.rb-section-testimonial .testimonial-customer-position {
    font-size: 16px;
}
.post-excerpt
{
	color: #a3a3a3;
	font-weight: 500;
}
.testimonial-item i{
	font-size: 13px;
    line-height: 1;
    letter-spacing: 4px;
    color: #e1e1e1;
}
.yellow{
	
	display: inline-block;
    color: #ffc107 !important;
    font-family: "FontAwesome";
    font-size: 13px;
    line-height: 1;
    letter-spacing: 4px;
}
.rb-section-testimonial
{
	margin-top: 20px;
	background-color: #fff;
}
.rb-section-testimonial .rb-testimonial-info {
    margin-top: 25px;
}
.rb-section-testimonial .rb-testimonial-image {
    width: 95px;
    height: 95px;
}
.rb-section-testimonial .rb-testimonial-image img {
    width: 95px;
    height: 95px;
    max-width: 95px;
	border-radius: 50%;
	border: none;
	box-shadow: none;
}
.rb-section-testimonial .rb-testimonial-details {
    /* padding-left: 30px; */
	margin-top: 20px;
}

.rb-section-testimonial .rb-testimonial-name {
    margin: 0 0 5px;
    font-size: 14px;
    color: #000;
    text-transform: uppercase;
	line-height: 1.5;
	font-weight: bold;
}
.rb-section-testimonial .rb-testimonial-job {
    font-size: 15px;
	color: #a3a3a3;
	font-weight: bold;
}
</style>

<!---- happy customers--->
<section class=" section8 block-trending assignproducts_section section-top" id="mostpurchased_range">
<div class="section-container">
<h3 class="mb-0 section-title text-center" style="text-transform:capitalize;">Happy Clients</h3>
<div class="section-content">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="products-style8 owl-carousel"  data-dots="false" data-loop="true" data-nav = "true" data-margin = "29" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":3}}'>
<div class="rb-section-testimonial">
<div class="item">
			<div class="rb-testimonial-content">
				<div class="testimonial-item">
					<div class="star star-3">
					</div>
					<h2 class="title-item">					
						Great
						quality!<br>
						<i class="fa fa-star yellow" aria-hidden="true"></i><i class="fa fa-star yellow" aria-hidden="true"></i><i class="fa fa-star yellow" aria-hidden="true"></i><i class="fa fa-star yellow" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>
					</h2>
					<div class="testimonial-customer-position">
						<p class="post-excerpt heightmatched_testimonials">
							Very happy with Voomet Studio. I got the best deal for a specific sofa that I had on my mind and wanted to buy. Very good prompt service and a great team to associate with.<br><br>
						</p>
					</div>
				</div>
			</div>

			<div class="rb-testimonial-info">
				<div class="rb-testimonial-image" style="float:left;width:40%;">
				<img alt="" src="assets/images/sanjay.png" style="opacity: 1;">
					<div class="rb-image-loading">
					</div>
				</div>

				<div class="rb-testimonial-details" style="float:left;width:50%;">
					<div class="rb-testimonial-name">
						Sanjay Singh
					</div>
					<!--<div class="rb-testimonial-job">
						Designer
					</div>-->
				</div>
			</div>
		</div>
</div>
<div class="rb-section-testimonial">
<div class="item" style="background: #e6f1f4;">
			<div class="rb-testimonial-content">
				<div class="testimonial-item">
					<div class="star star-3">
					</div>
					<h2 class="title-item">					
						Its Wow!!<br>
						<i class="fa fa-star yellow" aria-hidden="true"></i><i class="fa fa-star yellow" aria-hidden="true"></i><i class="fa fa-star yellow" aria-hidden="true"></i><i class="fa fa-star yellow" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>
					</h2>
					<div class="testimonial-customer-position">
						<p class="post-excerpt heightmatched_testimonials">
							I purchased my designer table vases from this site and I am happy to support Voomet Studio. Great team, they proactively called me to check if everything was ok. I am wishing them the best.
						</p>
					</div>
				</div>
			</div>

			<div class="rb-testimonial-info">
				<div class="rb-testimonial-image" style="float:left;width:40%;">
				<img alt="" src="assets/images/mahua.png" style="opacity: 1;">
					<div class="rb-image-loading">
					</div>
				</div>

				<div class="rb-testimonial-details" style="float:left;width:50%;">
					<div class="rb-testimonial-name">
						Mahua Chaudhary
					</div>
					<!--<div class="rb-testimonial-job">
						Designer
					</div>-->
				</div>
			</div>
		</div>
</div>
<div class="rb-section-testimonial">
<div class="item">
			<div class="rb-testimonial-content">
				<div class="testimonial-item">
					<div class="star star-3">
					</div>
					<h2 class="title-item">		
						Simply Supreb!<br>
						<i class="fa fa-star yellow" aria-hidden="true"></i><i class="fa fa-star yellow" aria-hidden="true"></i><i class="fa fa-star yellow" aria-hidden="true"></i><i class="fa fa-star yellow" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>
					</h2>
					<div class="testimonial-customer-position">
						<p class="post-excerpt heightmatched_testimonials">
							Good purchase experience. It was seamless. I got what I wanted and at a very good price with quick delivery. Thanks to Voomet Studio and wishing them good luck.<br><br>
						</p>
					</div>
				</div>
			</div>

			<div class="rb-testimonial-info">
				<div class="rb-testimonial-image" style="float:left;width:40%;">
				<img alt="" src="assets/images/soni.png" style="opacity: 1;">
					<div class="rb-image-loading">
					</div>
				</div>

				<div class="rb-testimonial-details" style="float:left;width:50%;">
					<div class="rb-testimonial-name">
						Soni Narang
					</div>
					<!--<div class="rb-testimonial-job">
						Designer
					</div>-->
				</div>
			</div>
		</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="section8" style="margin: 0px;">
	<div class="">
			
			<div class="row" style="background-color: #fff;">
				<div class="col-md-12 section-top text-center">

<img class="" src="<?php echo base_url() ?>assets/images/demo-decor-home-img-03.jpg" alt="">
</div>
</div>
</div>
</section>
<!---- happy customers--->







<!--<section class=" section8 block-trending mb-50 pt-50">
<div class="section-container">
<h3 class="section-title">Why Samrick?</h3>
<div class="section-content">
<div class="container">
<div class="services2">
<div class="service-wapper">
<div class="row">
<div class="col-sm-4 text-center">
<div class="well">
<div class="icon mb-20">
<img src="<?php //echo base_url();?>assets/data/whytgp/Icon1.png" alt="service">
</div>
<div class="text mb-20">
Packaging can be complex. You are sure to get the most innovative packaging here.
</div>
</div>
</div>
<div class="col-sm-4 text-center">
<div class="well">
<div class="icon mb-20">
<img src="<?php //echo base_url();?>assets/data/whytgp/Icon2.png" alt="service">
</div>
<div class="text mb-20">
Never get disappointed with the quality of your packaging. Get perfect quality now!
</div>
</div>
</div>
<div class="col-sm-4 text-center">
<div class="well">
<div class="icon mb-20">
<img src="<?php //echo base_url();?>assets/data/whytgp/Icon3.png" alt="service">
</div>
<div class="text mb-20">
All your packaging needs in one place, at best prices, delivered fastest. 
</div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</section>



<section class="section8 block-trending mb-20">
<div class="section-container">
<div class="section-content">
<div class="container">
<div class="row">
<div class="col-md-4 col-md-offset-4">
<div class="box-authentication pb-0"  ng-controller="EnquiryController">
<form name="enquiryForm" novalidate>
<h3 class="section-title mb-40">Connect With Us</h3>
<div class="form-group" show-errors>
<input ng-model="formData_enquiry.connect_name" name="connect_name" type="text" class="form-control" placeholder="Enter Your Name" required>
<p class="help-block" ng-if="enquiryForm.connect_name.$error.required">The name is required</p>
</div>
<div class="form-group" show-errors>
<input type="text" ng-minlength="10" 
ng-maxlength="10" min="0" name="connect_mobile" class="form-control" ng-model="formData_enquiry.connect_mobile" required allow-only-numbers placeholder="Enter Your Mobile No">

<p class="help-block" ng-show="enquiryForm.connect_mobile.$error.required || enquiryForm.connect_mobile.$error.number">Valid mobile number is required
</p>

<p class="help-block" ng-show="((enquiryForm.connect_mobile.$error.minlength ||
enquiryForm.connect_mobile.$error.maxlength) && 
enquiryForm.connect_mobile.$dirty) ">
mobile number should be 10 digits
</p>

</div>

<div class="form-group" show-errors>
<input type="email" name="connect_email" class="form-control" ng-model="formData_enquiry.connect_email" required placeholder="Enter Your Email">


<p class="help-block" ng-if="enquiryForm.connect_email.$error.required">The email address is required</p>
<p class="help-block" ng-if="enquiryForm.connect_email.$error.email">The email address is invalid</p>	



</div>



<div class="form-group">
<button class="btn btn-sm btn-block"  ng-click="save_enquiries();$event.stopPropagation();" ng-disabled="enquiryForm.$invalid"  style="background-color:#eda900;color:#fff;font-size:1.2em;opacity:1" id="enquiryForm_btn"><i class="fa fa-user"></i> SUBMIT</button>	
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</section>-->


<!-- Modal -->

<div class="modal" id="testing" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<div align="center"><img src="<?php echo base_url()?>assets/images/softlaunch.jpg" class="img-responsive"></div>
</div>
<div class="modal-body">
<p class="text-center fa-1x"><b>Currently, we are open for purchase by invitation only.</b></p>
</div>
<div class="modal-footer">
<button type="button" class="button" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>

<!----modal---------->


<script>
$(document).ready(function(){
//$('#testing').modal('show');
});

var module = angular.module('app', ['ui.bootstrap', 'angular-responsive']);
module.factory('Scopes', function ($rootScope) {
var mem = {};
return {
store: function (key, value) {
$rootScope.$emit('scope.stored', key);
mem[key] = value;
},
get: function (key) {
return mem[key];
}
};
});


function EnquiryController($scope,$rootScope,Scopes,$http) {


Scopes.store('EnquiryController', $scope);


$scope.save_enquiries = function() {
$scope.$broadcast('show-errors-check-validity');
if ($scope.enquiryForm.$valid) {


$data=$scope.formData_enquiry;
url="<?php echo base_url();?>enquiries_server";
$("#enquiryForm_btn").html('<i class="fa fa-spin fa-refresh"></i> Processing');
$http({
method : "POST",
url : url,
data: $data,
}).success(function mySucces(res) {
$("#enquiryForm_btn").html('<i class="fa fa-user"></i> Create');
if(res==true)
{
/*bootbox.alert({ 
size: "small",
message: 'Message Successfully Sent',
});*/
location.href="<?php echo base_url();?>thankyou-contactus";
}else{
    alert('Not Sent');
/*bootbox.alert({
size: "small",
message: 'Not Sent',
});	*/
}

},function myError(response) {

});	


}
}


}



module.directive('showErrors', function($timeout) {
return {
restrict: 'A',
require: '^form',
link: function (scope, el, attrs, formCtrl) {
var inputEl   = el[0].querySelector("[name]");
var inputNgEl = angular.element(inputEl);
var inputName = inputNgEl.attr('name');
var blurred = false;
inputNgEl.bind('blur', function() {
blurred = true;
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$watch(function() {
return formCtrl[inputName].$invalid
}, function(invalid) {
if (!blurred && invalid) { return }
el.toggleClass('has-error', invalid);
});

scope.$on('show-errors-check-validity', function() {
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$on('show-errors-reset', function() {
$timeout(function() {
el.removeClass('has-error');
}, 0, false);
});
}
}
});


</script>

<script>


    function add_to_wishlistFun(inventory_id, section_name_in_page_abbr) {

        document.getElementById('anchor_'+section_name_in_page_abbr+'_' + inventory_id).style.pointerEvents = 'none';

        <?php
        if(!$this->session->userdata("customer_id")){
        $this->session->set_userdata("cur_page", current_url());
        ?>
        if (confirm("Please login to add wishlist")) {
            location.href = "<?php echo base_url()?>login";
        }

        /*bootbox.confirm({
        message: "Please login to add wishlist",
        size: "small",
        callback: function (result) {
        if(result){
        location.href="<?php echo base_url()?>login";
}
}
});*/

        <?php
        }else{
        ?>

        // $('#wish_btn_'+inventory_id).prop('disabled',true);

        $.ajax({
            url: "<?php echo base_url()?>add_to_wishlist",
            type: "post",
            dataType: "json",
            data: "inventory_id=" + inventory_id,
            success: function (data) {
                if (data.exists == "no") {
                    alert('Added to wishlist');
                    /*bootbox.alert({
                    size: "small",
                    message: 'Added to wishlist',
                    });	*/
                    $("#wishlist_" + section_name_in_page_abbr + "_" + inventory_id).css({"color": ""});
                    $("#wishlist_" + section_name_in_page_abbr + "_" + inventory_id).css({"color": "#ff4343;"});
                    $("#wishlist_label_" + section_name_in_page_abbr + "_" + inventory_id).html("Added to wishlist");
                    $("#anchor_" + section_name_in_page_abbr + "_" + inventory_id).attr({"title": "Added to wishlist"});
                    $("#wishlist_label_" + section_name_in_page_abbr + "_" + inventory_id).css({"color": "#ff4343;"});
                } else if (data.exists == "yes") {
                    alert('Removed from wishlist');
                    /*bootbox.alert({
                    size: "small",
                    message: 'Removed from wishlist',
                    });*/
                    $("#wishlist_" + section_name_in_page_abbr + "_" + inventory_id).css({"color": ""});
                    $("#wishlist_" + section_name_in_page_abbr + "_" + inventory_id).css({"color": "#C2C2C2;"});
                    $("#wishlist_label_" + section_name_in_page_abbr + "_" + inventory_id).html("Removed from wishlist");
                    $("#anchor_" + section_name_in_page_abbr + "_" + inventory_id).attr({"title": "Removed from wishlist"});
                    $("#wishlist_label_" + section_name_in_page_abbr + "_" + inventory_id).css({"color": "#C2C2C2;"});


                } else {
                    alert('Error');
                    /*bootbox.alert({
                    size: "small",
                    message: 'Error',
                    });*/
                }

                document.getElementById('anchor_'+section_name_in_page_abbr+'_' + inventory_id).style.pointerEvents = 'auto';

            }
        });

        <?php } ?>
    }

</script>




<!--preview panel-->
<div class="container">
<div class="comparePanle">
<div class="row">
<div class="col-md-3 text-center">
<span class="compare_lineheight">Compare Products <small class="compare_lineheight" id="compare_line_text"> (Add <span id="no_of_products_count_compare">0</span> more to compare )</small></span>
</div>
<div class="comparePan col-md-5 cursor-pointer">
</div>
<div class="col-md-4 text-center">
&nbsp;
<form method="post" action="<?php echo base_url()?>compare_inventories">
<button type="submit" class="button btn-sm notActive cmprBtn" disabled>Compare <span id="no_of_products_compare" class="badge"></span></button>
<a href="#" class="btn preventDflt" type="button" onclick="clearComparePanel()"><small>Clear All</small></a>
<input type="hidden" name="inventory_id_list_for_compare" id="inventory_id_list_for_compare" value="">
</form>
</div>

</div>

</div>
</div>
<!--end of preview panel-->

<!-- comparision popup-->
<div id="id01" class="w3-animate-zoom w3-white w3-modal modPos">
<div class="w3-container">
<a onclick="document.getElementById('id01').style.display='none'" class="whiteFont w3-padding w3-closebtn closeBtn">&times;</a>
</div>
<div class="w3-row contentPop w3-margin-top">
</div>

</div>
<!--end of comparision popup-->

<!--  warning model  -->
<div id="WarningModal" class="w3-modal">
<div class="w3-modal-content warningModal">
<header class="w3-teal panel-heading">
<h3><span>&#x26a0;</span>&nbsp;Error</h3>
</header>
<div class="w3-container">
<p class="lead text-center">Maximum of Three products are allowed for comparision</p>
</div>
<footer class="w3-container w3-right-align">
<button id="warningModalClose" onclick="document.getElementById('id01').style.display='none'" class="w3-btn w3-hexagonBlue w3-margin-bottom  ">Close</button>
</footer>
</div>
</div>
<!--  end of warning model  -->


<script>
if(localStorage.getItem("comparePan") === null || localStorage.getItem("comparePan").trim()==""){}else{
$(".comparePanle").show();
$("#inventory_id_list_for_compare").val(localStorage.getItem("comparePan_inventory_ids"));
$(".comparePan").html(localStorage.getItem("comparePan"));
if($("#inventory_id_list_for_compare").val().split(",").length>1){		
$(".cmprBtn").attr({"disabled":false});
}
$("#no_of_products_compare").html($("#inventory_id_list_for_compare").val().split(",").length);
$("#no_of_products_count_compare").html(3-$("#inventory_id_list_for_compare").val().split(",").length);
if($("#no_of_products_count_compare")[0].innerHTML==0){
$("#compare_line_text").css("visibility","hidden");
}else{
$("#compare_line_text").css("visibility","visible");
}
}
function clearComparePanel(){
$(".addToCompare").each(function(){
$(this).attr("checked",false);
});
localStorage.removeItem("comparePan");
localStorage.removeItem("comparePan_inventory_ids");
$(".comparePan").html("");
$("#inventory_id_list_for_compare").val("");
$(".comparePanle").hide();

}
$(document).ready(function(){
showChar(200);
});


$(document).ready(function(){
if($(window).width() <= 480){
$(".collapse").removeClass("in");
}

toAdapt();
toAdaptforquickbuy();
toAdapt_shopbycategory();
//toAdapt_testimonials();
toAdapt_quickbuy();
toAdaptformostpurchased();
toAdapt_mostpurchased();
toAdaptforknockoutdeals();
toAdapt_knockoutdeals();
toAdaptfortrendingproducts();
toAdapt_trendingproducts();
toAdaptfordealoftheday();
toAdapt_dealoftheday();
window.onresize=function(){toAdapt;toAdaptforquickbuy();toAdapt_shopbycategory;toAdapt_quickbuy();toAdaptformostpurchased();toAdapt_mostpurchased();toAdaptforknockoutdeals();toAdapt_knockoutdeals();toAdaptfortrendingproducts();toAdapt_trendingproducts();toAdaptfordealoftheday();toAdapt_dealoftheday();};       

});


function toAdapt(){
var heights = $(".heightmatched").map(function() {
return $(this).height();
}).get(),

maxHeight = Math.max.apply(null, heights);

$(".heightmatched").height(maxHeight);
}
function toAdaptforquickbuy(){
var heights = $(".heightmatchedforquickbuy").map(function() {
return $(this).height();
}).get(),

maxHeight = Math.max.apply(null, heights);

$(".heightmatchedforquickbuy").height(maxHeight);
}
function toAdaptformostpurchased(){
var heights = $(".heightmatchedformostpurchased").map(function() {
return $(this).height();
}).get(),

maxHeight = Math.max.apply(null, heights);

$(".heightmatchedformostpurchased").height(maxHeight);
}
function toAdaptforknockoutdeals(){
var heights = $(".heightmatchedforknockoutdeals").map(function() {
return $(this).height();
}).get(),

maxHeight = Math.max.apply(null, heights);

$(".heightmatchedforknockoutdeals").height(maxHeight);
}
function toAdaptfortrendingproducts(){
var heights = $(".heightmatchedfortrendingproducts").map(function() {
return $(this).height();
}).get(),

maxHeight = Math.max.apply(null, heights);

$(".heightmatchedfortrendingproducts").height(maxHeight);
}
function toAdapt_trendingproducts(){
var heights_trendingproducts = $(".heightmatched_trendingproducts").map(function() {
return $(this).height();
}).get(),

maxHeight_trendingproducts = Math.max.apply(null, heights_trendingproducts);

$(".heightmatched_trendingproducts").height(maxHeight_trendingproducts);
}
function toAdaptfordealoftheday(){
var heights = $(".heightmatchedfordealoftheday").map(function() {
return $(this).height();
}).get(),

maxHeight = Math.max.apply(null, heights);

$(".heightmatchedfordealoftheday").height(maxHeight);
}
function toAdapt_dealoftheday(){
var heights_dealoftheday = $(".heightmatched_dealoftheday").map(function() {
return $(this).height();
}).get(),

maxHeight_dealoftheday = Math.max.apply(null, heights_dealoftheday);

$(".heightmatched_dealoftheday").height(maxHeight_dealoftheday);
}
function toAdapt_knockoutdeals(){
var heights_knockoutdeals = $(".heightmatched_knockoutdeals").map(function() {
return $(this).height();
}).get(),

maxHeight_knockoutdeals = Math.max.apply(null, heights_knockoutdeals);

$(".heightmatched_knockoutdeals").height(maxHeight_knockoutdeals);
}
function toAdapt_mostpurchased(){
var heights_mostpurchased = $(".heightmatched_mostpurchased").map(function() {
return $(this).height();
}).get(),

maxHeight_mostpurchased = Math.max.apply(null, heights_mostpurchased);

$(".heightmatched_mostpurchased").height(maxHeight_mostpurchased);
}
function toAdapt_quickbuy(){
var heights_quickbuy = $(".heightmatched_quickbuy").map(function() {
return $(this).height();
}).get(),

maxHeight_quickbuy = Math.max.apply(null, heights_quickbuy);

$(".heightmatched_quickbuy").height(maxHeight_quickbuy);
}
function toAdapt_shopbycategory(){
var heights_shopbycategory = $(".heightmatched_shopbycategory").map(function() {
return $(this).height();
}).get(),

maxHeight_shopbycategory = Math.max.apply(null, heights_shopbycategory);

$(".heightmatched_shopbycategory").height(maxHeight_shopbycategory);
}


function toAdapt_testimonials(){
var heights_testimonials = $(".heightmatched_testimonials").map(function() {
return $(this).height();
}).get(),

maxHeight_testimonials = Math.max.apply(null, heights_testimonials);

$(".heightmatched_testimonials").height(maxHeight_testimonials);
alert(heights_testimonials)
}


</script>
<script>
$(document).ready(function(){
//$('#testing').modal('show');
});

var module = angular.module('app', ['ui.bootstrap', 'angular-responsive']);
module.factory('Scopes', function ($rootScope) {
var mem = {};
return {
store: function (key, value) {
$rootScope.$emit('scope.stored', key);
mem[key] = value;
},
get: function (key) {
return mem[key];
}
};
});






function WebinarregistrationController($scope,$rootScope,Scopes,$http) {


Scopes.store('WebinarregistrationController', $scope);


$scope.save_webinarregistrations = function() {
$scope.$broadcast('show-errors-check-validity');
if ($scope.webinarregistrationForm.$valid) {


$data=$scope.webinarformData_enquiry;
webinar_title=document.getElementById("webinar_title").value;
url="<?php echo base_url();?>webinar_registration/"+webinar_title;
$("#webinarregistrationForm_btn").html('<i class="fa fa-spin fa-refresh"></i> Processing');
$http({
method : "POST",
url : url,
data: $data,
dataType:"JSON",
}).success(function mySucces(res) {
$("#webinarregistrationForm_btn").html('<i class="fa fa-user"></i> Register');
if(res.status==true)
{
	/*bootbox.alert({ 
	  size: "small",
	  message: 'Thank you for registration',
	  callback: function () { location.reload(); }
	});*/

webinar_registration_unique_id=res.webinar_registration_unique_id;
location.href="<?php echo base_url();?>thankyou_webinar/"+webinar_registration_unique_id;
	
}else{
    bootbox.alert({ 
	  size: "small",
	  message: 'Oops Error!',
	});	
}

},function myError(response) {

});	


}
}


}

module.directive('showErrors', function($timeout) {
return {
restrict: 'A',
require: '^form',
link: function (scope, el, attrs, formCtrl) {
var inputEl   = el[0].querySelector("[name]");
var inputNgEl = angular.element(inputEl);
var inputName = inputNgEl.attr('name');
var blurred = false;
inputNgEl.bind('blur', function() {
blurred = true;
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$watch(function() {
return formCtrl[inputName].$invalid
}, function(invalid) {
if (!blurred && invalid) { return }
el.toggleClass('has-error', invalid);
});

scope.$on('show-errors-check-validity', function() {
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$on('show-errors-reset', function() {
$timeout(function() {
el.removeClass('has-error');
}, 0, false);
});
}
}
});


</script>


 <!-- Modal Starts -->
  <div class="modal fade" id="registerForWebinar_modal" role="dialog">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Webinar Registration</h4>
        </div>
        <div class="modal-body">
          
		  
		  <div class="pb-0"  ng-controller="WebinarregistrationController">
							<form name="webinarregistrationForm" novalidate>
								<input type="hidden" name="webinar_title" id="webinar_title">
								<div class="form-group" show-errors>
									<input ng-model="webinarformData_enquiry.name" name="name" type="text" class="form-control" placeholder="Enter Name" required>
									<p class="help-block" ng-if="webinarregistrationForm.name.$error.required">Name is required</p>
								</div>
								
								
								
								
								
								<div class="form-group" show-errors>
										<input type="email" name="email" class="form-control" ng-model="webinarformData_enquiry.email" required placeholder="Enter Your Email">
				   
					
					<p class="help-block" ng-if="webinarregistrationForm.email.$error.required">Email address is required</p>
							<p class="help-block" ng-if="webinarregistrationForm.email.$error.email">Email address is invalid</p>	
				
					
					
								</div>
								
								<div class="form-group" show-errors>
										<input type="text" ng-minlength="10" 
                   ng-maxlength="10" min="0" name="mobile" class="form-control" ng-model="webinarformData_enquiry.mobile" required allow-only-numbers placeholder="Enter Your Mobile No">
				   
				    <p class="help-block" ng-show="webinarregistrationForm.mobile.$error.required || webinarregistrationForm.mobile.$error.number">Valid mobile number is required
					</p>
				
					<p class="help-block" ng-show="((webinarregistrationForm.mobile.$error.minlength ||
								   webinarregistrationForm.mobile.$error.maxlength) && 
								   webinarregistrationForm.mobile.$dirty) ">
								   Mobile number should be 10 digits
					</p>
					
								</div>
								
								
								
								<div class="form-group" show-errors>
										<input type="text" ng-minlength="6" 
                   ng-maxlength="6" min="0" name="postal_code" class="form-control" ng-model="webinarformData_enquiry.postal_code" allow-only-numbers placeholder="Enter Your Location Postal Code">
				   
				   
				
					<p class="help-block" ng-show="((webinarregistrationForm.postal_code.$error.minlength ||
								   webinarregistrationForm.postal_code.$error.maxlength) && 
								   webinarregistrationForm.postal_code.$dirty) ">
								   Postal code should be 6 digits
					</p>
					
								</div>
								
								
								
								
								<div class="form-group">
									<button class="btn btn-sm btn-block"  ng-click="save_webinarregistrations();$event.stopPropagation();" ng-disabled="webinarregistrationForm.$invalid"  style="background-color:#2e0000;color:#fff;font-size:1 em;opacity:1" id="webinarregistrationForm_btn"> Register</button>	
								</div>
							</form>
						</div>
						
						
						
        </div>
        
      </div>
      
    </div>
  </div>
<!-- Modal Ends -->  
  
<script>
	function registerForWebinarFun(webinar_title){
		$("#webinar_title").val(webinar_title)
		$("#registerForWebinar_modal").modal();
	}
</script>
<script>
if(localStorage.getItem("schedule_appointment")!==null){
	var element = document.getElementById('schedule_appointment');
	  
	var headerOffset = 1100;
		var elementPosition = element.getBoundingClientRect().top;
	  var offsetPosition = elementPosition + window.pageYOffset - headerOffset;
	$('html, body').animate({
        'scrollTop' : Math.abs(offsetPosition)
    },900);
	
	
	 
		localStorage.removeItem("schedule_appointment")
}
</script>

<script>


function pincode_length_onkeypress(obj){
    if(obj.value!=""){
            pincode=obj.value;
                len=pincode.length;
                if(len<6){
                    $('#pincode_err_msg').html('<span style="color:red;" class="pull-left">Please enter 6 digit valid pincode</span>');
					$('#pincode_success_msg').html('');
                    $('#rq_pincode').css({'border':'1px solid red'});
                    //return false;
                }else{
                    //alert(1);
                    $('#rq_pincode').css({'border':'1px solid green'});
                    $('#pincode_err_msg').html('');
					$('#pincode_success_msg').html('');
                }
    }else{
        $('#pincode_err_msg').html('Please enter 6 digit valid pincode');
		$('#pincode_success_msg').html('');
		document.getElementById("rq_pcat_id").innerHTML='<option value=""> - Choose Parent Category - </option>';
        document.getElementById("rq_cat_id").innerHTML='<option value=""> - Choose Category - </option>';
        document.getElementById("rq_subcat_id").innerHTML='<option value=""> - Choose Sub Category - </option>';
        document.getElementById("rq_brand_id").innerHTML='<option value=""> - Choose Brand - </option>';
        document.getElementById("rq_product_id").innerHTML='<option value=""> - Choose Product - </option>';
		document.getElementById("rq_inventory_id").innerHTML='<option value=""> - Choose SKU - </option>';
    }
}


</script>

<script>
$(document).ready(function(){
	var windowSize = $(window).width();
    if (windowSize >= 768) {
		toAdapt_whysamriccard();
	}
	window.onresize = function () {
		if (windowSize >= 768) {
			toAdapt_whysamriccard();
		}
	}
});
function toAdapt_whysamriccard() {
	var heights = $(".whysamriccard").map(function () {
			return $(this).height();
		}).get(),

		maxHeight = Math.max.apply(null, heights);

	$(".whysamriccard").height(maxHeight);
}

$(function () {
    //$('#myTab').tab('show')
})

function addNewsLetterSubscription_new(obj){
      var email_address_nl=document.getElementById('email_address_nl_new').value;
      if(email_address_nl==""){
          document.getElementById('subscibing_info_new').innerHTML='<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Add Email Address';
          return false;
      }
      
      var form_data=new FormData(obj);
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {                
                    document.getElementById('subscibing_info_new').innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i> Subscribing'
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if(xhr.responseText=="1"){
                               document.getElementById('subscibing_info_new').innerHTML='<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Subscribed Successfully'
							   document.getElementById('email_address_nl_new').value='';
                           }
                           else{
                              document.getElementById('subscibing_info_new').innerHTML='<i class="fa fa-exclamation-circle" aria-hidden="true"></i> <b>Server Error</b>. <br>Alternatively you can subscribe newsletter by sending mail to info@voometstudio.com'
                           }
                        }
                    }
                    xhr.open(obj.getAttribute('method'), obj.getAttribute('action'), true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('email_address_nl='+email_address_nl);
                }   
                return false;
  }
</script>




<div class="modal" id="attention_modal" role="dialog">
<div class="modal-dialog modal-sm fade-in">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-body">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<strong>Attention:</strong>
	<p class="align-top">This site is under production testing and not ready for purchase yet.</p>
</div>
</div>

</div>
</div>


<script>

$(document).ready(function(){
	//if(localStorage.getItem("attention_modal") === null || localStorage.getItem("attention_modal").trim()==""){
		//localStorage.setItem("attention_modal","yes")
		$('#attention_modal').modal('show');
	//}
});

</script>

