<style>
a.anchor {
    display: block;
    position: relative;
    top: -5vh;
    visibility: hidden;
}
</style>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo base_url()?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Returns</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">Links</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                 <ul class="tree-menu">
									<li><span></span><a href="<?php echo base_url();?>aboutus">About Us</a></li>
									
									<li <?php if($flag=="returns"){?> class='active' <?php } ?>><span></span><a href="<?php echo base_url();?>#returns">Returns</a></li>
									
									<li <?php if($flag=="replacements"){?> class='active' <?php } ?>><span></span><a href="<?php echo base_url();?>returns#replacements">Replacements</a></li>
									<li <?php if($flag=="refunds"){?> class='active' <?php } ?>><span></span><a href="<?php echo base_url();?>returns#refunds">Refunds</a></li>
									<li <?php if($flag=="cancellations"){?> class='active' <?php } ?>><span></span><a href="<?php echo base_url();?>returns#cancellations">Cancellations</a></li>
									<li <?php if($flag=="disclaimer"){?> class='active' <?php } ?>><span></span><a href="<?php echo base_url();?>returns#disclaimer">Disclaimer</a></li>
									
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
                
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- page heading-->
				<a  class="anchor" id="returns"></a>
                <h2 class="page-heading text-center">
                    <span class="page-heading-title2">Returns</span>
                </h2>
                <!-- Content page -->
                <div class="content-text clearfix">    

                  <p>
					Definition: 'Return' is defined as the action of giving back the item purchased by the Buyer to the Seller on <?php echo SITE_NAME; ?> website. Following situations may arise:</p>
				  
					<ol style="list-style-type:decimal;list-style-position:inside;">
						<li>Item was defective</li>
						<li>Item was damaged during the Shipping</li>
						<li>Products was / were missing</li>
						<li>Wrong item was sent by the Seller.</li>
					</ol>
					<p class="mt-10">
					Buyer can return the product(s) within 3 days from the date of delivery of the product(s). 
					</p>
					<p>Shipping cost for returning the product shall be borne by the Buyer and refunded by the Seller.</p>
					<p>
					We encourage the Buyer to review the listing before making the purchase decision. 
					Products cannot be returned in the following conditions: 
					</p>
					<ol style="list-style-type:decimal;list-style-position:inside;">
						<li>In case Buyer orders a wrong item, Buyer shall not be entitled to any return/refund.</li>
						<li>Items cannot be returned beyond the return window provided by the seller. </li>
						<li>If the product has been used by the Buyer. However, in special cases where the product has proven to be faulty and have a manufacturing defect which has resulted in breakdown during use, the product can be returned after confirmation by the Seller. </li> 
					</ol>
					
                </div>
                <!-- ./Content page -->
				
				
				
				
				
				  <!-- page heading-->
				<a class="anchor" id="replacements"></a>
                <h2 class="page-heading text-center">
                    <span class="page-heading-title2">Replacements</span>
                </h2>
                <!-- Content page -->
                <div class="content-text clearfix">    

                    <p>Definition: ‘Replacement’ is the action or process of replacing something in place of another. A Buyer can request for replacement if he is not happy with the item, reason being Damaged in shipping, Defective item, Item(s) missing, or wrong item is shipped.</p>
					
					<p>Buyer needs to raise the replacement request within 3 days from the date of delivery of product(s). Once the Buyer has raised a replacement request in his account by:</p> 
					
					<ol style="list-style-type: decimal;list-style-position:inside;">
					<li>Stating the “Reason for Return”. Among others, the following are the leading reasons are:
					<ul style="list-style-type: circle;margin-block-start:1em;margin-left:1em;list-style-position:inside;">
						<li>Shipping was damaged</li>
						<li>Item was defective</li>
						<li>Item is Dead on Arrival</li>
						<li>Item(s) were missing</li>
						<li>Wrong item sent</li>
					</ul>
					</li>
					<li class="mt-10">An intimation shall be provided to Seller seeking either “approval” or “rejection” of the replacement request.</li>
					<li class="mt-10">In case the Seller accepts the replacement request, Buyer shall be required to return the product to the Seller; and only after return of the product, the Seller shall be obliged to provide the replacement product to the Buyer.</li>
					<li class="mt-10">In case the seller does not accept the replacement request, the Buyer can contact the seller for clarification. The final decision on accepting the request rests with the seller.</li>
					<li class="mt-10">In case the product is out of stock, the Seller can provide the refund to the Buyer and Buyer shall be obligated to accept the refund in lieu of replacement.</li>
					</ol>
					<p>If the Seller doesn’t respond to the Buyer’s replacement request, within three (3) days from the date of replacement request placed by the Buyer, refund shall be processed in favour of Buyer and Seller shall be liable to refund amount paid to the Seller.</p>
					<p>All shipping replacement charges shall be paid by the Buyer and the Seller will refund the charges within 3 days of receiving the returned products.</p>
					<p>Products cannot be replaced in the following conditions: </p>
					<ol style="list-style-type: decimal;list-style-position:inside;">
					<li>If an eligible item is out of stock, it cannot be replaced. Only a refund against the returned item will be issued.</li>
					<li>If the product being returned is not in accordance with the parameters outlined in point 1 “Reason for Return”, then Buyer shall not be entitled to any refund of money from the Seller.</li>
					</ol>

                </div>
                <!-- ./Content page -->
				
				
				
				  <!-- page heading-->
				<a  class="anchor" id="refunds"></a>
                <h2 class="page-heading text-center">
                    <span class="page-heading-title2">Refunds</span>
                </h2>
                <!-- Content page -->
                <div class="content-text clearfix">    

                   <p>Definition: ‘Refund is the action or process of returning the amount equivalent to the one given by the Buyer while placing the order.</p> 
				   
				   <p>Buyer needs to raise the return within 3 days from the date of payment realization. Once the Buyer has raised the return request by contacting <?php echo SITE_ADDR; ?> on the Customer Care Number provided on the Website or sending an email, after meeting the following criteria:</p>
					
					<ol style="list-style-type: decimal;list-style-position:inside;">
						<li>
							The refund is applicable If an eligible item is out of stock, and it cannot be replaced. 
						</li>
						<li class="mt-10">
							If the Seller accepts the return request raised by the Buyer, the Buyer will have to return the product and then the refund shall be credited to the Buyers account. Refund in this case occurs in two instances:
						</li>
						<li class="mt-10">
							<ul style="list-style-type: circle;margin-left:1em;list-style-position:inside;">
								<li>
									Refund after shipment collection - Seller has agreed to wait for the logistics team to collect the shipment from the buyer before refunding) 
								</li>
								<li>
									Refund without shipment collection - Seller has agreed to refund the buyer without expecting the original shipment back)
								</li>
							</ul>
						</li>
						<li class="mt-10">
							In case the seller does not accept the return request, the Buyer can contact the seller for clarification. The final decision on accepting the request rests with the seller.
						</li>
						<li class="mt-10">
							In case the Seller doesn’t close the ticket in 3 days from the date of intimation to the Seller about the refund request, the refund request shall be settled in favour of the Buyer.
						</li>
					<ol>
					<p class="mt-10">
						 The refund time period for different modes of payments is provided below.
					</p>
					<table class="text-center table table-bordered">
						<tr>
							<th class="text-center">
								<b>Available refund method</b>
							</th>
							<th class="text-center">
								<b>Refund Time-frame</b>
							</th>
						</tr>
						<tr>
							<td>
								Credit Card/ Debit Card
							</td>
							<td>
								5-7 Business Days
							</td>
						</tr>
						<tr>
							<td>
								Net Banking Account (Credited to Bank Account)
							</td>
							<td>
								5-7 Business Days
							</td>
						</tr>
						<tr>
							<td>
								UPI Linked Bank Account
							</td>
							<td>
								5-7 Business Days
							</td>
						</tr>
						<tr>
							<td>
								NEFT to Bank Account
							</td>
							<td>
								5-7 Business Days
							</td>
						</tr>
						<tr>
							<td>
								Wallet 
							</td>
							<td>
								5-7 Business Days
							</td>
						</tr>
					</table>	
                </div>
                <!-- ./Content page -->
				
				
				
				  <!-- page heading-->
				<a  class="anchor" id="cancellations"></a>
                <h2 class="page-heading text-center">
                    <span class="page-heading-title2">Cancellations</span>
                </h2>
                <!-- Content page -->
                <div class="content-text clearfix">    

                   <p>You can cancel your orders only up to the point you get a confirmation that the products are shipped. You cannot cancel your order after it has been shipped. </p> 
				   <h4>Cancellation Process:</h4>
				   <ul style="list-style-type: decimal;margin-left:1em;list-style-position:inside;">
								<li class="mt-10">
									Go to Your Orders.
								</li>
								<li class="mt-10">
									For each order item, on the right hand side, there will be a cancel button. Click on it to proceed with cancellation.
								</li>
								<li class="mt-10">
									After submitting the cancellation request, we'll send you a confirmation message along with the refund details to the e-mail address on your account. 
								</li>
								<li class="mt-10">
									You can also confirm that the order was cancelled by visiting Your Orders. If you see the order in the Cancelled Orders section, it was successfully cancelled.
								</li>
							</ul>

                </div>
                <!-- ./Content page -->
				
				
				
				
				  <!-- page heading-->
				<a class="anchor" id="disclaimer"></a>
                <h2 class="page-heading text-center">
                    <span class="page-heading-title2">Disclaimer</span>
                </h2>
                <!-- Content page -->
                <div class="content-text clearfix">    

                    <p>The products sold on <?php echo SITE_ADDR; ?> are intended to offer replacement in the case of NoA (Nonusable on Arrival) </p>
					
					<p>Misorders or other non NOA related orders will not be entertained for replacements. We urge the buyers to ascertain the right requirements before placing their order. We will not be responsible if proper care is not taken by the buyer while ordering, resulting in mis-shipments. In such cases we will not entertain replacement or refund of the products.</p> 
					
					<p>The buyer is responsible for determining the suitability of <?php echo SITE_NAME; ?>'s products for their various needs and that the products are usable in condition that they arrive in.</p>
					

                </div>
                <!-- ./Content page -->
				
				
				
				
				
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<!-- ./page wapper-->
