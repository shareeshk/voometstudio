		<div id="recentlyViewed_div" >
				<!-- box product -->
				<div class="page-product-box" id="recentlyViewed">
					<h3 class="heading">Recently Viewed</h3>
					<ul id="recentlyViewedUL" class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":4}}'> 	
					</ul>
				</div>
				<!-- ./box product -->
		</div>
		
		<!--------->												
		
		<!--------->
	

<script>
function updatelocalstoragehtml(){
    var doc=""
    var basesUrl='<?php echo base_url()?>'
    //Object.keys(localStorage) 
     //   .forEach(function(key){
		 key="itemsArray";
            if(key=="itemsArray"){	

				var data=localStorage.getItem(key);
				if(data==null){
					$('#recentlyViewed_div').hide();
				}else{
					$('#recentlyViewed_div').show();
					var arrs=JSON.parse(data);
					if(arrs.length>0){
						for(var i=0;i<arrs.length;i++){
	   
							doc+='<li>';
								doc+='<div class="product-container" >';
									doc+='<div class="left-block">';
									doc+='<span class="pull-right" style="cursor:pointer" onclick="ClearSomeLocalStorage('+arrs[i].product_id+')"><i class="fa fa-times-circle" aria-hidden="true"></i></span>';
										doc+='<a href="'+basesUrl+arrs[i].product_garbage+'">';
										   doc+='<img class="img-responsive" alt="product" src="'+arrs[i].product_image+'" />';
										doc+='</a>';
										
										
										doc+='<div class="quick-view">';
												doc+='<a title="Add to my wishlist" class="heart" href="#" onclick="add_to_wishlistFun('+arrs[i].product_id+');return false;"></a>';
												doc+='<a title="Add to compare" class="compare" href="#"></a>';
											   
										doc+='</div>';
										if(arrs[i].discount_price!=0 && arrs[i].discount_price!=null){
											doc+='<div class="price-percent-reduction2">';
											doc+='-'+arrs[i].discount_price+' OFF';
											doc+='</div>';
										}						
										
									doc+='</div>';
									doc+='<div class="right-block">';
										doc+='<h5 class="product-name"><a href='+basesUrl+arrs[i].product_garbage+' >'+arrs[i].product_name+'</a></h5>';
										
										<!----attribute-------------->
										
										
										doc+='<small>'
                                                doc+='<table>';
                                                   
													if(arrs[i].attribute_1!=""){
														
														val=arrs[i].attribute_1_value.split(':');
														att_val=val[0];
														doc+='<tr>';
															doc+='<td> '+arrs[i].attribute_1+'&nbsp;</td>';
															doc+='<td> <b>'+att_val+'</b></td>';
														doc+='</tr>';
													
													}
                                                    if(arrs[i].attribute_2!=""){
														val=arrs[i].attribute_2_value.split(':');
														att_val=val[0];
	
														doc+='<tr>';
															 doc+='<td> '+arrs[i].attribute_2+'&nbsp;</td>';
															doc+='<td> <b>'+att_val+'</b></td>';
														doc+='</tr>';
													}
													 
                                                    if(arrs[i].attribute_3!=""){
														val=arrs[i].attribute_3_value.split(':');
														att_val=val[0];
	
														doc+='<tr>';
															doc+='<td>'+arrs[i].attribute_3+'&nbsp;</td>';
															doc+='<td> <b>'+att_val+'</b></td>';
														doc+=' </tr>';	
													}
													
                                                    if(arrs[i].attribute_4!=""){
														 
														val=arrs[i].attribute_4_value.split(':');
														att_val=val[0];
														
														doc+='<tr>';
														   doc+='<td>'+arrs[i].attribute_4+'&nbsp;</td>';
															doc+='<td> <b>'+att_val+'</b></td>';
														doc+='</tr>';
													}
                                                    
                                                 doc+='</table>';
                                            doc+='</small>';
										
										
										
										<!----attribute-------------->
										
										
									   <!-----------------rating-------->
			
			var avg_value=arrs[i].avg_ratings;	
			var ratings=arrs[i].total_reviews;	
			doc+='<div class="product-star">';
		
			var str_star='';
			if(avg_value==0.5){
				
				str_star+='<i class="fa fa-star-half-o"></i>';	
				str_star+='<i class="fa fa-star-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
								
			}else if(avg_value==1){
				
				str_star+='<i class="fa fa-star"></i>';	
				str_star+='<i class="fa fa-star-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
				
			}else if(avg_value==1.5){
				str_star+='<i class="fa fa-star"></i>';	
				str_star+='<i class="fa fa-star-half-o"></i>';	
				str_star+='<i class="fa fa-star-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
			}else if(avg_value==2){
				str_star+='<i class="fa fa-star"></i>';	
				str_star+='<i class="fa fa-star"></i>';	
				str_star+='<i class="fa fa-star-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
			}else if(avg_value==2.5){
				str_star+='<i class="fa fa-star"></i>';	
				str_star+='<i class="fa fa-star"></i>';	
				str_star+='<i class="fa fa-star-half-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
				
			}else if(avg_value==3){
				str_star+='<i class="fa fa-star"></i>';	
				str_star+='<i class="fa fa-star"></i>';	
				str_star+='<i class="fa fa-star"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
			}else if(avg_value==3.5){
				str_star+='<i class="fa fa-star"></i>';	
				str_star+='<i class="fa fa-star"></i>';	
				str_star+='<i class="fa fa-star"></i>';
				str_star+='<i class="fa fa-star-half-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
			}else if(avg_value==4){
				str_star+='<i class="fa fa-star"></i>';	
				str_star+='<i class="fa fa-star"></i>';	
				str_star+='<i class="fa fa-star"></i>';
				str_star+='<i class="fa fa-star"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
			}else if(avg_value==4.5){
				str_star+='<i class="fa fa-star"></i>';	
				str_star+='<i class="fa fa-star"></i>';	
				str_star+='<i class="fa fa-star"></i>';
				str_star+='<i class="fa fa-star"></i>';
				str_star+='<i class="fa fa-star-half-o"></i>';
			}else if(avg_value==5){
				str_star+='<i class="fa fa-star"></i>';	
				str_star+='<i class="fa fa-star"></i>';	
				str_star+='<i class="fa fa-star"></i>';
				str_star+='<i class="fa fa-star"></i>';
				str_star+='<i class="fa fa-star"></i>';
			}else{
				str_star+='<i class="fa fa-star-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
				str_star+='<i class="fa fa-star-o"></i>';
			}
			str_star+='&nbsp;|&nbsp;'+ratings+'&nbsp;';
			str_star+='</div>';
			doc+=str_star;	   

		 
								   <!-----------------rating-------->
										doc+='<div class="content_price">';
											doc+='<span class="price product-price">'+arrs[i].default_price+'</span>';
											if(arrs[i].old_prices!=0 && arrs[i].old_prices!=null){
												doc+='<span class="price old-price">'+arrs[i].old_prices+'</span>';
											}
											
										doc+='</div>';
									doc+='</div>';
								doc+='</div>';
							doc+='</li>';
						}
					
					}else{
						$('#recentlyViewed_div').hide();
					}
				}	
			}
			
       // });
    
    document.getElementById('recentlyViewedUL').innerHTML=doc
}

function ClearSomeLocalStorage(obj) {
    var ids=obj;
       //Object.keys(localStorage) 
     //   .forEach(function(key){
		 key="itemsArray";
        if(key=="itemsArray"){
            var arrs=JSON.parse(localStorage.getItem(key));
            for(var i=0;i<arrs.length;i++){
                if(arrs[i].product_id==ids){
                    arrs.splice(i, 1);
                }
            }
        }
        localStorage.setItem('itemsArray', JSON.stringify(arrs));
		
       // });
		
		var $owl = $('#recentlyViewedUL');
		$owl.trigger('destroy.owl.carousel');

		$owl.html($owl.find('#recentlyViewedUL .owl-stage-outer').html()).removeClass('owl-loaded');

		updatelocalstoragehtml();
		///////////////////////////////////////////////////
		
			
				$owl.owlCarousel(
                    {
                        dots:false,
                        nav:true,
                        navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
                        margin:25,
                        responsive : {
                          // breakpoint from 0 up
                          0 : {
                              items : 2,
                          },
                          // breakpoint from 480 up
                          480 : {
                              items : 2,
                          },
                          // breakpoint from 768 up
                          768 : {
                              items : 4,
                          },
                          1000 : {
                              items : 4,
                          }
                      }
                    }
                );
    //updatelocalstoragehtml();
	
}

$(document).ready(function(){
	updatelocalstoragehtml()
});

</script>