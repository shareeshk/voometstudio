	  <?php 
	  
	if(!empty($payment_policy_arr) || !empty($replacement_policy_arr) || !empty($refund_policy_arr) || !empty($cancellation_policy_arr)){
		?>
	<div class="form-option hide">
		<div class="form-option-title"> Services</div>
		
		<?php
	   if(!empty($payment_policy_arr)){?>

		   <?php foreach($payment_policy_arr[0] as $data){?>
		   
		   <!--<small><b>Payment</b></small>-->
		   
		   <ul>
		   
		   <?php $paymentAttrarr=explode(',',$data['attributes']);
		   foreach($paymentAttrarr as $payment){?>
			   <li>
			   <?php  if($payment=="COD"){ ?>
			   <span <?php if(!empty($data['cod_restrictions'])){ echo '';}?>><i class="fa fa-money" aria-hidden="true"></i> <?php echo $payment ?>
					<?php if(!empty($data['cod_restrictions'])){?>
					 <a href="#" data-toggle="popover" data-trigger="hover" data-content="accepted max:<?php echo $data['max_value']?> <?php echo $data['currency_type']?> <?php echo $data["message"]?>"><i class="fa fa-question-circle" aria-hidden="true"></i></a>
				   
			   <?php }else{
				   echo "Not Allowed.<br>";
			   }?>
				</span>
			  <?php }?>
			   <?php if($payment=="Coupon"){ ?>
			   <i class="fa fa-file-o" aria-hidden="true"></i> <?php echo $payment ?> Not Allowed
			   <?php } ?>
			   <?php if($payment=="Wallet"){ ?>
			   <i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo $payment ?> Not Allowed
			   <?php } ?>
			   <?php if($payment=="Payment Gateway"){ ?>
			   <i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo $payment ?> Not Allowed
			   <?php } ?>
			   </li>

		  <?php }?>
		  </ul>
		   <?php }?>
		  
	  <?php }
	   ?>    
			<?php 
			
			
			
	   if(!empty($replacement_policy_arr[0])){?>
		
		   <?php foreach($replacement_policy_arr[0] as $data){?>
			<!--<small><b>Replacement</b></small>-->
		   <ul >
		   <?php if($data['replacement_restrict']=="remove_replacement"){?>
			   <li><i class="fa fa-reply" aria-hidden="true"></i> No Replacement <a href="#" data-toggle="popover" data-trigger="hover" data-content="<?php echo $data["message"]?>"><i class="fa fa-question-circle" aria-hidden="true"></i></a> </li>
		   <?php } if($data['replacement_restrict']=="new_day_limit"){?>
			   <li>  <i class="fa fa-reply" aria-hidden="true"></i> <?php echo $data['new_days_for_replacement']?> days Replacement policy <a href="#" data-toggle="popover" data-trigger="hover" data-content="Replacement Allowed with in <?php echo $data['new_days_for_replacement']?> days after delivery"><i class="fa fa-question-circle" aria-hidden="true"></i></a> </li>
		   <?php }?>
				<li></li> 

		  </ul>
		   <?php }?>
		 
	  <?php }
	   ?>    

	   
	   <?php 
	   if(!empty($refund_policy_arr[0])){?>
	  
		   <?php foreach($refund_policy_arr[0] as $data){?>
		   <!--<small><b>Refund</b></small>-->
			<ul >
			  <?php if($data['refund_restrict']=="new_day_limit"){?>
				  <li> <i class="fa fa-repeat" aria-hidden="true"></i> <?php echo $data['new_days_for_refund']?> days Refund policy 
				  <a href="#" data-toggle="popover" data-trigger="hover" data-content="
				  
				  <?php if(!empty($data['refund_method'])){?>
					  Refund Method Applicable:<?php echo $data['refund_method'] ?>
				 <?php }?>
				  "><i class="fa fa-question-circle" aria-hidden="true"></i></a>
				  
			  <?php }else{?>
				 <li>No Refund 
				  <a href="#" data-toggle="popover" data-trigger="hover" data-content="<?php echo $data["message"]?>"><i class="fa fa-question-circle" aria-hidden="true"></i></a></li>
			  <?php }?>
			  </ul>
		<?php }?>
		 
	  <?php }
	   ?>     
	   
		<?php 
	   if(!empty($cancellation_policy_arr)){?>
		
		   <?php foreach($cancellation_policy_arr[0] as $data){?>
		   <!--<small><b>Cancellation</b></small>--->
		   <ul >
			  <?php if($data['cancellation_restrict']=="new_day_limit"){?>
			  <?php if(!empty($data['new_days_for_cancellation'])){?>
			  <li ><i class="fa fa-times" aria-hidden="true"></i> <?php echo $data['new_days_for_cancellation'] ?> day(s) Cancellation policy 
				  <?php }else{?>
					  <li ><i class="fa fa-times" aria-hidden="true"></i> <?php echo $data['new_hour_limit'] ?> hour(s) Cancellation policy 
				 <?php }?> 
			  
			   <a href="#" data-toggle="popover" data-trigger="hover" data-content="
			   <?php if(!empty($data['new_days_for_cancellation'])){?>
					  Cancellation allowed within <?php echo $data['new_days_for_cancellation'] ?> days after order
				 <?php }?>
				 <?php if(!empty($data['new_hour_limit'])){?>
					  Cancellation allowed within <?php echo $data['new_hour_limit'] ?> hours after order
				 <?php }?>
			   
			   "><i class="fa fa-question-circle" aria-hidden="true"></i></a></li>
				  
				 
			  <?php }else{?>
				 <li>No Cancellation <a href="#" data-toggle="popover" data-trigger="hover" data-content="<?php echo $data["message"]?>"><i class="fa fa-question-circle" aria-hidden="true"></i></a></li>
			  <?php }?>
			  </ul>
	   <?php }?>
		 
	  <?php }
	   ?>   


	<?php 
	   if(!empty($exchange_policy_arr[0])){ ?>
		
		 <!-- data-target="#exchangeModal"-->
		 
		  <a data-toggle="modal" href='#' ng-click="open_exchange_modal()" style="cursor:pointer;"><i class="fa fa-exchange" aria-hidden="true"></i> Exchange Offer</a>
		
	<!-- Modal -->
	<div id="exchangeModal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content" style="padding: 24px 30px;min-height: 242px;">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Exchange Offer</h4>
		  </div>
		  <div class="modal-body">
		   <div class="row">
		<div class="col-md-12">  
		   
		<?php
		
		foreach($exchange_policy_arr[0] as $data){
			//echo $data['item_level']."<br>"; 
			/* echo "<b>offer Starts:</b> ".date("D,j M Y h:i A", strtotime($data['exchange_start_date']))." <br>";
			 echo "<b> offer Ends:</b> ".date("D,j M Y h:i A", strtotime($data['exchange_end_date']))."<br>";*/ 
			  
			   ?>
			   <div class="col-md-6 align_top">
				
				<?php if(!empty($data['message_ui'])){ ?>
				<div class="col-sm-6">
					<div class="form-group">
					<?php echo $data['message_ui'];?>
					</div>
				</div>
				
				<div class="col-sm-6">
					<div class="form-group">
						<img src="<?php echo base_url().$data["image"];?>" style="width:100px;height:200px;">
					</div>
				</div>
				<?php }else{ ?>
				
				<div class="form-group">
					<img src="<?php echo base_url().$data["image"];?>" style="width:100px;height:200px;">
				</div>
				
				<?php } ?>
				<div class="form-group">
			 <div class="col-md-12 align_top"><ul ><li style="float: left;
	margin-right: 10px;">
	<b>Shipping: </b> <a href="#" data-toggle="popover" data-trigger="hover" data-content="<?php echo $data["shipping"]; ?>"><i class="fa fa-question-circle" aria-hidden="true"></i></a>
	</li>
			
				<?php 
				
				 if(!empty($data['message_disclaimer'])){
				  ?>
				  
				   <li  style="float: left;margin-right: 10px;"><b>Disclaimer: </b> <a href="#" data-toggle="popover" data-trigger="hover" data-content="<?php echo $data["message_disclaimer"];?>"><i class="fa fa-question-circle" aria-hidden="true"></i></a></li>

				<?php
				} 
				?>
				</ul></div><br>
				</div>
				<div class="row">
				<br>
				</div>
				<div class="form-group">
					
						<div class="checkbox">
							<label for="checkboxes-1">
							  <input required="" checked="checked" name="accept_terms_and_conditions" value="1" type="checkbox">
							  I agree to 
							  
	   
	   <a href="#" style="z-index:1240;" data-toggle="popover" data-trigger="hover" data-html="true"  data-content="<?php if(!empty($data['message_t_c'])){
				echo "<b>Terms And Condition : </b>"." ".$data['message_t_c']."<br>";
			}  ?>"> Terms and conditions </a>
							</label>
						</div>
					
				</div>
				
			   </div>
			   <div class="col-md-6 align_top">
			   
				   <table class="table">
					   <tr>
						   <td>Price</td>
						   <td class="text-right"><?php echo $inventory_product_info_obj->selling_price; ?> <?php echo $data['exchange_value_currency']; ?></td>
					   </tr>
					   <tr>
						   <td>Pickup charge</td>
						   <td class="text-right">+ <?php echo $data['pickup_charge']; ?> <?php echo $data['exchange_value_currency']; ?></td>
					   </tr>
					   <?php if($data['exchange_value_min']){?>
						<tr>
						   <td>Exchange Discount</td>
						   <td class="text-right"><span style="color:red;">- <?php echo $data['exchange_value_min']; ?> <?php echo $data['exchange_value_currency']; ?></span></td>
					   </tr>
					   <?php } ?>
					   <tr>
						   <td>Total Amount Payable</td>
						   <td class="text-right"><?php echo (($inventory_product_info_obj->selling_price+$data['pickup_charge'])-$data['exchange_value_min']) ?> <?php echo $data['exchange_value_currency']; ?></td>
					   </tr>
					   <tr>
						<td colspan="2"><button style="width:100%;" class="btn btn-info btn-lg"  onclick="check_avaliable_options_selected();">Exchange and Buy</button></td>
					   </tr>
				   </table>
			   </div>
			
		
			   <?php
			   
		}
	   ?>   
		</div>
	</div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>

	  </div>
	</div>


	   <?php 
	   
		}	 
?>
</div>
<?php 
	} ?>