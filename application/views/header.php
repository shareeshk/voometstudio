<?php
	if($this->session->userdata("customer_id")){
		$user_id=$this->session->userdata("customer_id");
		$user_id="user_voometstudio_".$user_id;
	}
	else{
		if(isset($_COOKIE["user_id"])) {
			$user_id="user_".$_COOKIE["user_id"];
	    }else{
			setcookie("user_id", "user_".time());
			$user_id="user_".time();
	    }
	}
	//3 glass legged wooden square marble top table above 50k
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon/VS_favicon.ico">
<!-- <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/VS_favicon.png"> -->
<title><?php echo SITE_NAME; ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min_latest.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/offsets.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/select2/select2.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/jquery.bxslider.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.fancybox.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/animate.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/reset.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/responsive.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/option5.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/custom.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.css" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.1.min.js"></script>	
<script src="<?php echo base_url();?>assets/js/angular.js" data-semver="1.2.5" data-require="angular.js@1.2.5"></script>
<script src="<?php echo base_url();?>assets/js/responsive-directive.js"></script>
<script src="<?php echo base_url();?>assets/js/ui-bootstrap-tpls-0.2.0.js"></script>
<script src="<?php echo base_url();?>assets/js/angular-route.js"></script>  
<script type="text/javascript" src="<?php echo base_url();?>assets/js/paginathing.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/product_comparision/Compare.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/jqueryui/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.elevatezoom.js"></script>
<script src="<?php echo base_url()?>assets/sweetalert2/sweetalert2.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/sweetalert2/sweetalert2.min.css">
<!--<link rel="stylesheet" href="<?php /*echo base_url()*/?>assets/js/bootstrap-tooltip.js">-->
<style type="text/css">
    .social_icons{
        position: absolute !important;
        right: 1%;
    }
.notification_class{
background: url('<?php echo base_url();?>assets/pictures/images/image_notify.png') no-repeat;
width: 191px;
height: 141px;
margin: 10px auto 24px; 
} 
#notification_wrapper {
background-color: #fff;
border:solid 1px rgba(100, 100, 100, .20);
-webkit-box-shadow:0 3px 8px rgba(0, 0, 0, .20);
position: absolute;
top: 52px;
width: 360px;
z-index: 9999;
display: none;
margin-left: -310px;
}
#notificationTitle {
z-index: 1000;
font-weight: bold;
padding: 1px;
font-size: 13px;
background-color: #ffffff;
width: 358px;
border-bottom: 1px solid #dddddd;
text-align:center;
}
#notificationsBody {
padding: 10px 0px 0px 0px !important;
max-height:300px;
overflow:auto;
}
#notificationFooter {
background-color: #e9eaed;
text-align: center;
font-weight: bold;
padding: 1px 8px;
font-size: 12px;
border-top: 1px solid #dddddd;
}
.media-left, .media>.pull-left{
padding:10px;
}
.media-body, .media-left, .media-right{
line-height:20px;
padding:6px 8px;
}
/*.noti_Counter {
display:block;
position:absolute;
background:#E1141E;
color:#FFF;
font-size:12px;
font-weight:normal;
padding:2px 4px;
margin:-3px 0 0 18px;
border-radius:2px;
-moz-border-radius:2px; 
-webkit-border-radius:2px;
z-index:1;
line-height:12px;
}*/
.noti_Counter {
    background:#E1141E;
    color:#FFF;
    font-size:12px;
    font-weight:normal;
    padding:2px 4px;
    border-radius:2px;
    -moz-border-radius:2px;
    -webkit-border-radius:2px;
    z-index:1;
    margin-left: -5px;
    margin-top: -5px;
}

.main-header-ontop .noti_Counter{
/* margin:1px 0px 0px 10px; */
}
#footer2 .footer-paralax{
<?php 
$ft_img="assets/data/ui/footer.jpg"; // hardcoded on 28th sep 2023
if($ft_img!=''){
?>
background-image: url('<?php echo base_url().$ft_img;?>');
background-repeat:no-repeat;
background-position:center;
background-size:cover;
<?php 
}?>
}
.thumbnail{padding:0px;}
.margin-top{margin-top:2em;}
.dropdown-menu>li>a{white-space:pre-line;}
.alert-default{background-color: #ffffff;}
.alert{border-radius:0;border:none;box-shadow:0px 1px 4px rgba(0, 0, 0, 0.1);transition:all 0.3s ease-in-out 0s;}
.panel {border:none;box-shadow:0px 1px 4px rgba(0, 0, 0, 0.1);transition:all 0.3s ease-in-out 0s;}
.panel-group .panel {border-radius:0;}
.panel-info>.panel-heading {background-color:#ffffff;}
/* notifications mobile icon css starts */
.option5 .group-button-header .btn-notifications {
background: url(<?php echo base_url()?>assets/images/notifications.png) no-repeat center center;
}
@media (max-width: 767px) and (min-width: 481px)
.group-button-header .btn-notifications, .group-button-header .btn-heart, .group-button-header .btn-compare {
float: none;
text-align: center;
margin: 0 auto;
}
.group-button-header .btn-notifications {
position: relative;
}
.group-button-header .btn-notifications, .group-button-header .btn-heart, .group-button-header .btn-compare{
width: 39px;
height: 39px;
}
.group-button-header .btn-notifications .notify-right {
top: -7px;
right: -12px;
}
/* notifications mobile icon css starts */
.scroll_top{
bottom:8px;
}
.bootbox-close-button.close{
display:none;
}
button:disabled{
cursor:no-drop;
}
@media (max-width: 768px) {
.navbar-collapse{
position: absolute !important;
z-index: 1;
width: 100%;
height: auto;
}
}

/*@media (max-width: 768px) {
#filterselection_div{
display:none;
}
#filterselection_heading{
display:none;
}
}
@media (min-width: 767px) {
#filterselection_div{
display:block;
}
}*/

/* right side cart */

.secondary{
    right: 0;
    top: 0;
    position: fixed;
    width: 100px;
    overflow-y: scroll;
    overflow-x: hidden;
    height: 100%;
    /**/
    background-color: #fff;
    z-index: 1000;
    box-shadow: -1px 0px 10px 1px #dad7d7;
}
    /**/
.secondary1{
    right:100px;
    top: 0;
    position: fixed;
    /**/
    color: #ccc;
    z-index: 1000;
    /**/
}
.secondary_corner{
    right:0px; /* can change right 0 */
    top: 0;
    position: fixed;
    /**/
    color: #ccc;
    z-index: 1000;
    /**/
}
.secondary_header{
    padding: 10px;
    background-color: #004b9b;
    font-weight: bolder;
    font-size: small;
    color:#fff;
}
.secondary_cart_cell{
    border-bottom: 1px solid #f3f0f0;
}
#cart_btn{
    margin: 8px;
}
/* right side cart */

@media (max-width: 767px){
	.negativemargin {
    display:none!important;
}
}


</style>


<style>
/***** type=number arrow hide starts ***/
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
/***** type=number arrow hide starts ***/
</style>


<style>
.align-top{
	margin-top:10px;
}

.align-bottom{
	margin-bottom:10px;
}

.align-left{
	margin-left:10px;
}

.align-right{
	margin-right:10px;
}
.menu_bg_color{
    background-color: #fff !important; 
}
.nav>li>a:focus, 
.nav>li>a:hover {
    text-decoration: none;
    /* background-color: #2e0000; */
    background-color: #eee;
}
</style>

<script>
$(function() {
$( "#search_keyword_header" ).autocomplete({
source: '<?php echo base_url()?>search_keywords'
});
/* right side cart */
   /* $("#cart_btn").click(function(){
        $("#cart_section").toggle(1000);
    });*/
/* right side cart */

});
</script>


<?php
$current_base_url=base_url();
if((preg_match("/voometstudio.com/",$current_base_url))){
?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WLP3V7B');</script>
<!-- End Google Tag Manager -->

<?php
}
?>




<script type="text/javascript">
$(window).load(function() {
$("#loading").fadeOut("slow");
})
</script>
<style>
    #loading {
  position: fixed;
  /*display: block;*/
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  text-align: center;
  opacity: 0.9;
  /*background-color: #fff;*/
  z-index: 99999;
}

.loader_1 
{
position: fixed;
left: 0px;
top: 0px;
width: 100%;
height: 100%;
z-index: 9999;
background: url('<?php echo base_url()?>assets/images/loader.gif') 50% 50% no-repeat rgba(0,0,0,0.4);
}

.loader_{
position: fixed;
left: 50%;
top: 50%;
z-index: 9999;
border: 10px solid #f3f3f3;
border-radius: 50%;
/* border-top: 10px solid #ff8a00;
border-right: 10px solid #005cb4;
border-bottom: 10px solid #ff8a00;
border-left: 10px solid #005cb4; */
border-top: 10px solid #ccc;
border-right: 10px solid #002770;
border-bottom: 10px solid #ccc;
border-left: 10px solid #002770;
width: 60px;
height: 60px;
-webkit-animation: spin 2s linear infinite;
animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
0% { -webkit-transform: rotate(0deg); }
100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
0% { transform: rotate(0deg); }
100% { transform: rotate(360deg); }
}


.navbar-nav>li {
    float: left;
    font-size: 16px;
}
/***** whatsapp starts ******/
.float{
position:fixed;
width:50px;
height:50px;
bottom:20px;
left:20px;
color: #fff;
padding: 5px 10px 0px 10px;
background-color: #05f74a;
border-radius: 10px;
text-align:center;
font-size:30px;
z-index:100;
}
.float:hover{
position:fixed;
width:50px;
height:50px;
bottom:20px;
left:20px;
background-color: #05f74a;
color:#FFF;
border-radius: 10px;
text-align:center;
font-size:30px;
z-index:100;
}

.my-float{
margin-top:10px;
line-height:0.7;
}

/*** whatsapp ends ******/


.negativemargin{
	background-color:#fff;
}
.negativemargin ul > li > a{
padding: 5px 15px;
}
.negativemargin ul > li > a.navcustommenu{
/* color:#2e0000; */
background:url('<?php echo base_url();?>assets/images/kak.png') no-repeat right center

}
.negativemargin ul > li > a:hover{
background-color: transparent;
color:#969899;
}
.negativemargin .dropdown {
font-size: 14px;    
}
.negativemargin ul > li > a {
margin: 6px 0;
color: #87898a;
}
.negativemargin ul >li:last-child>a {
    background: none;
}
.content-gap {
    padding-top: 35px;
}



.marquee {
  height: 50px;

  overflow: hidden;
  position: relative;
  background-color:#0088cc;
  color:#fff;
}

.marquee div {
  width: 200%;
  height: 50px;
  position: absolute;
  overflow: hidden;
  animation: marquee 15s linear infinite;
}

.marquee span {
  margin-top: 9px;
  font-size: 20px;
  font-weight:bold;
}

.marquee div:hover {
  animation-play-state: paused
}

@keyframes marquee {
  0% { left: 0; }
  100% { left: -100%; }
}
/* new */

.marquee {
  background-color: #fff;
  /* color:#383838; */
  color:#000;
  width: 100%;
  margin: 0 auto;
  overflow: hidden;
  white-space: nowrap;
  padding:5px;
  padding-bottom:0;
}
.marquee span {
  display: inline-block;
  font-size: 15px;
  position: relative;
  left: 100%;
  animation: marquee 35s linear infinite;
}
.marquee:hover span {
  animation-play-state: paused;
}

.marquee span:nth-child(1) {
  animation-delay: .5s;
}
.marquee span:nth-child(2) {
  animation-delay: 1.8s;
}
.marquee span:nth-child(3) {
  animation-delay: 2.6s;
}
.marquee span:nth-child(4) {
  animation-delay: 3.4s;
}
.marquee span:nth-child(5) {
  animation-delay: 4.2s;
}

@keyframes marquee {
  0%   { left: 100%; }
  100% { left: -100%; }
}


.icon_list{
    background-color: #ff6600;
    padding: 1px;
}
.whatsapp_class{
    font-size: 30px;
    margin-left: 20px;
    color: #fff;
    padding: 5px 10px 0px 10px;
    background-color: #05f74a;
    border-radius: 10px;
    height: 40px;
    margin-top: 15px;
    line-height: 30px;
}
</style>
<style>
#searchForm_org_jarvinai ::placeholder {
  color: #c2bfbf;
  opacity: 1; /* Firefox */
}

#searchForm_org_jarvinai ::-ms-input-placeholder { /* Edge 12-18 */
  color: #c2bfbf;
}
#searchForm_org_jarvinai #search_keywords_jarvinai{
	color: #000;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

<?php 
$request_uri=$_SERVER["REQUEST_URI"];
$previous_uri=(isset($_SERVER["HTTP_REFERER"])) ? $_SERVER["HTTP_REFERER"] : '';
$page_url=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

?>
<script type="text/javascript">
        $(function()
        {
            var start = null;
            $(window).load(function(event) {
                start = event.timeStamp;
            });
            $(window).on("beforeunload", function(event){
                //console.log('leaving page')
                request_uri='<?php echo $request_uri; ?>';
                previous_uri='<?php echo $previous_uri; ?>';
                page_url='<?php echo $page_url; ?>';
                var time = event.timeStamp - start;
                $.post('<?php echo base_url(); ?>capture_time_spent_on_page', {timeSpent: time,request_uri:request_uri,previous_uri:previous_uri,page_url:page_url});
                
            })
        });
</script>

</head>

<body class="home option5" ng-app="app" ng-cloak>



    <?php 
    $marquee=get_marquee();
    if(!empty($marquee)){

        ?>
    <p class="marquee" id="top-banner" style="color:#d9d9d9">
        <?php
        foreach($marquee as $mar){
            if($mar->text!=''){
            
            ?>
                <span><?php echo $mar->text;  ?></span>
            <?php

            }
        }
        ?>
        
    </p>
        <?php
    } 
    ?>
  
  <!-- <span>💥 CLEARANCE SALE - BUY 2 GET 1 use code B1G2 💥</span>
  <span>💥 CLEARANCE SALE - BUY 3 GET 1 use code B1G3 💥</span>
  <span>💥 CLEARANCE SALE - BUY 1 GET 1 use code B1G4 💥</span> -->


<?php
$current_base_url=base_url();
if((preg_match(SITE_TEXT,$current_base_url))){
?>
<!-- Google Tag Manager (noscript) -->

<!-- End Google Tag Manager (noscript) -->
<?php
}
?>

<nav id="sidebar">
<div id="dismiss">&times;</div>
<div class="sidebar-header">
<a href="<?php echo base_url(); ?>"><img alt="<?php echo SITE_NAME; ?>" class="w-50" src="<?php echo base_url();?>assets/images/VS_logo.png"/></a>
</div>
<ul class="list-unstyled components">
<?php
$p_category_count=0;
$id=$page_id;
$type=$page_type;
$name=$page_name;
$active_class='';
foreach ($p_category as $p_menu_value) {

$p_category_count++;
$rand_1=$controller->sample_code();

$menu_name=$p_menu_value['parent_menu'];
$menu_type=$p_menu_value['type'];

if($p_category_count==1 && $id=='' && $type=='' && $name==''){
//$name=$p_menu_value['parent_menu'];
$id=$p_menu_value['id'];
$type=$p_menu_value['type'];
}

if($menu_type=="parent_cat"){
$pcat_id=$p_menu_value['id'];			
$show_sub=$p_menu_value['show_sub_menu'];
if($show_sub==0){
?>
<p><a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pcat_id}"; ?>"><?php echo $menu_name;?></a></p>
<?php
}else{
?>
<p><a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pcat_id}"; ?>"><?php echo $menu_name;?></a></p>
<?php
// echo '<pre>';
// print_r($controller);
// echo '</pre>';

$cat=$controller->provide_category($pcat_id);
$len=count($cat);
if ($len!=0){
	$l=round(12/$len);
}
else { $l=0;}
?>
<?php
foreach($cat as $cat_value){
$rand_2=$controller->sample_code();
$cat_id=$cat_value->cat_id;
$cat_name=$cat_value->cat_name;
if(strtolower($name)==strtolower($cat_name)){
$active_class='active';
}else{
$active_class='';
}
?>
<li>
<a href="#pcatSubmenu<?php echo "{$cat_id}"; ?>" data-toggle="collapse" aria-expanded="false"><?php echo $cat_name;?></a>
<ul class="collapse list-unstyled" id="pcatSubmenu<?php echo "{$cat_id}"; ?>">
<?php
$sub_cat=$controller->provide_sub_category($cat_id);
foreach($sub_cat as $sub_cat_value) {

$rand_3=$controller->sample_code();

$subcat_id=$sub_cat_value["subcat_id"];
$subcat_value=$sub_cat_value["subcat_name"];

?>
<li><a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pcat_id}"; ?>/<?php echo "{$rand_2}{$cat_id}"; ?>/<?php echo "{$rand_3}{$subcat_id}"; ?>"><?php echo $subcat_value;?></a></li>
<?php
}	
?>
</ul></li>
<?php

}
?>				

<?php
}
}
if($menu_type=="cat"){
$rand_2=$controller->sample_code();

$cat_id=$p_menu_value['id'];
$cat_name=$p_menu_value['parent_menu'];
if(isset($p_menu_value['pcat_id'])){
$pcat_id=$p_menu_value['pcat_id'];
}
else{
$pcat_id=0;
}
if(strtolower($name)==strtolower($cat_name)){
$active_class='active';
}else{
$active_class='';
}
?>
<li>
<a href="#catSubmenu<?php echo "{$cat_id}"; ?>" data-toggle="collapse" aria-expanded="false" ><?php echo $cat_name;?></a>
<ul class="collapse list-unstyled" id="catSubmenu<?php echo "{$cat_id}"; ?>">
<?php
$sub_cat=$controller->provide_sub_category($cat_id);
foreach($sub_cat as $sub_cat_value) {

$rand_3=$controller->sample_code();

$subcat_id=$sub_cat_value["subcat_id"];
$subcat_value=$sub_cat_value["subcat_name"];

?>
<li><a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pcat_id}"; ?>/<?php echo "{$rand_2}{$cat_id}"; ?>/<?php echo "{$rand_3}{$subcat_id}"; ?>"><?php echo $subcat_value;?></a></li>
<?php
}	
?>
</ul>
</li>
<?php
}	
}
?>
</ul>
</nav>
<div id="loading">
<div class="loader_"></div>
</div>
<div id="header" class="header header8" ng-controller="CartController">
<?php include 'angular_cart_count.php';?>  
<div class="container-fluid main-header" id="main-header">
 <div class="row negativemargin topnavbar" id="middlediv">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 logo text-center">
                    	<ul class="nav navbar-nav" style="float:none;display:inline-block;">

								<li><a class="menu_bg_color" href="<?php echo base_url(); ?>whyvoometstudioproducts">Why <?php echo SITE_NAME;?></a></li>
								
								<li><a class="menu_bg_color" href="<?php echo base_url(); ?>blog">Blogs</a></li>


<li><a class="menu_bg_color" href="<?php echo base_url();?>becomeareseller" >Become A Reseller</a></li>
<li><a class="menu_bg_color" href="<?php echo base_url();?>becomeourfranchise" >Become Our Franchise</a></li>
<li><a class="menu_bg_color" href="<?php echo base_url();?>catalog_requestforquotation" >Request for Quotation</a></li>
<!-- <li><a class="menu_bg_color" href="<?php echo base_url();?>delivered_fast" >Delivered Fast</a></li> -->

<li class="social_icons">
    
    <a class="menu_bg_color pull-right" href="#" style="background:none;font-size:18px;padding:5px;"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a>
    <a class="menu_bg_color pull-right" href="#" style="background:none;font-size:18px;padding:5px;"><i class="fa fa-instagram"></i></a>
    <a class="menu_bg_color pull-right" href="#" style="background:none;font-size:18px;padding:5px;"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
</li>

											
					    </ul>

                        
                </div>

            </div>
<div class="row  mt-10">
<div class="col-xs-7 visible-xs">
<a href="<?php echo base_url(); ?>"><img alt="Flamingo" class="img-responsive" src="<?php echo base_url();?>assets/images/VS_logo.png"/></a>
</div>
<div class="col-xs-2 group-button-header visible-xs">
<div class="btn-cart" id="cart-block-mobile">
<a title="My cart" href="<?php echo base_url() ?>cart">Cart</a>
<span class="notify notify-right ng-binding cart_count">0</span>
</div>
</div>	


<div class="col-xs-2 group-button-header visible-xs">
<div class="btn-notifications" id="notifications-block-mobile" data-toggle="collapse" href="#collapse_1">
<span class="notify notify-right ng-binding noti_Counter">0</span>
</div>
</div>	


<div class="col-xs-12 visible-xs">
<div id="collapse_1" class="panel-collapse collapse">
<div class="panel-body">
<?php
if($this->session->userdata("customer_id")){
$order_item_notifi_obj=call_notification_function();

if(empty($admin_msg_detail) && empty($admin_msg_detail_repl) && empty($returns_notification) && empty($returns_notification) && empty($order_item_notifi_obj) && empty($admin_msg_detail_repl_stock)){
$no_details=1;
//show - no details - message
}else{
$no_details=0;
}

$str='<div >';	
if($no_details==0){
$str.='<div class="notifications">';
if(!empty($admin_msg_detail)){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$admin_msg_detail->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$admin_msg_detail->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>There is a message from seller for your refund request.</p>';
$admin_msg_detail->admin_msg_timestamp='';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($admin_msg_detail->admin_msg_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}
if(!empty($admin_msg_detail_repl)){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$admin_msg_detail_repl->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$admin_msg_detail_repl->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>There is a message from seller for your replacement request.</p>';
$admin_msg_detail_repl->admin_msg_timestamp='';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($admin_msg_detail_repl->admin_msg_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}
if(!empty($admin_msg_detail_repl_stock)){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$admin_msg_detail_repl_stock->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$admin_msg_detail_repl_stock->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>There is a message from seller for your replacement request.</p>';
$admin_msg_detail_repl_stock->admin_msg_timestamp='';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($admin_msg_detail_repl_stock->admin_msg_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}

foreach ($returns_notification as $rn){
if(($rn->refund_success_view==1 || $rn->refund_success_view==0) && $rn->refund_success_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$rn->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$rn->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>Refund is successful.</p>';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($rn->refund_success_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}elseif(($rn->admin_reject_status_view==1 || $rn->admin_reject_status_view==0) && $rn->admin_reject_status_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$rn->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$rn->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>Your request for refund is rejected.</p>';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($rn->admin_reject_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';


}elseif($rn->refund_label_status_view==1 && $rn->refund_label_status_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$rn->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$rn->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>Refund label is sent.</p>';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($rn->refund_label_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}elseif(($rn->admin_accept_status_view==1 || $rn->admin_accept_status_view==0) && $rn->admin_accept_status_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$rn->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$rn->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>Your refund request is accepted.</p>';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($rn->admin_accept_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}
}

foreach ($repl_notification as $repl){

if(($repl->admin_reject_status_view==1 || $repl->admin_reject_status_view==0) && $repl->admin_reject_status_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$repl->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$repl->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>Your request for replacement is rejected.</p>';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($repl->admin_reject_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}
elseif($repl->repl_label_status_view==1 && $repl->repl_label_status_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$repl->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$repl->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>Replacement label is sent.</p>';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($repl->repl_label_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}
elseif(($repl->admin_accept_status_view==1 || $repl->admin_accept_status_view==0) && $repl->admin_accept_status_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$repl->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$repl->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>Your replacement request is accepted.</p>';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($repl->admin_accept_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}
}

foreach ($order_item_notifi_obj as $order_item_notifi){

if(($order_item_notifi->cancelled_status_view==1 || $order_item_notifi->cancelled_status_view==0) && $order_item_notifi->cancelled_status_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$order_item_notifi->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$order_item_notifi->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
if($order_item_notifi->cancelled_status_view==1){
$str.='<p>';
}
$str.=''.$order_item_notifi->product_name.' you ordered is Cancelled.</span>';
if($order_item_notifi->prev_order_item_id!=''){
$str.='<br><span style="color:red;">Replaced Order</span>';
}
if($order_item_notifi->cancelled_status_view==1){
$str.='</p>';
}
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_cancelled_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';


}elseif(($order_item_notifi->delivered_status_view==1 || $order_item_notifi->delivered_status_view==0) && $order_item_notifi->delivered_status_view!=''){


$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$order_item_notifi->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$order_item_notifi->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
if($order_item_notifi->delivered_status_view==1){
$str.='<p>';
}
$str.=''.$order_item_notifi->product_name.' you ordered is delivered.</span>';
if($order_item_notifi->prev_order_item_id!=''){
$str.='<br><span style="color:red;">Replaced Order</span>';
}
if($order_item_notifi->delivered_status_view==1){
$str.='</p>';
}
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_delivered_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';



}elseif(($order_item_notifi->shipped_status_view==1 || $order_item_notifi->shipped_status_view==0) && $order_item_notifi->shipped_status_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$order_item_notifi->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$order_item_notifi->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
if($order_item_notifi->shipped_status_view==1){
$str.='<p>';
}
$str.=''.$order_item_notifi->product_name.' you ordered is shipped.</span>';
if($order_item_notifi->prev_order_item_id!=''){
$str.='<br><span style="color:red;">Replaced Order</span>';
}
if($order_item_notifi->shipped_status_view==1){
$str.='</p>';
}
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_shipped_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';


}elseif(($order_item_notifi->packed_status_view==1 || $order_item_notifi->packed_status_view==0 ) && $order_item_notifi->packed_status_view!='') {


$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$order_item_notifi->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$order_item_notifi->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
if($order_item_notifi->packed_status_view==1){
$str.='<p>';
}
$str.=''.$order_item_notifi->product_name.' you ordered is packed.</span>';
if($order_item_notifi->prev_order_item_id!=''){
$str.='<br><span style="color:red;">Replaced Order</span>';
}
if($order_item_notifi->packed_status_view==1){
$str.='</p>';
}
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_packed_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';


}elseif(($order_item_notifi->confirmed_status_view==1 || $order_item_notifi->confirmed_status_view==0 )&& $order_item_notifi->confirmed_status_view!=''){


$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$order_item_notifi->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$order_item_notifi->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
if($order_item_notifi->confirmed_status_view==1){
$str.='<p>';
}
$str.=''.$order_item_notifi->product_name.' you ordered is Confirmed.</span>';
if($order_item_notifi->prev_order_item_id!=''){
$str.='<br><span style="color:red;">Replaced Order</span>';
}
if($order_item_notifi->confirmed_status_view==1){
$str.='</p>';
}
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_confirmed_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';



}elseif(($order_item_notifi->approved_status_view==1 || $order_item_notifi->approved_status_view==0) && $order_item_notifi->approved_status_view!=''){


$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$order_item_notifi->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$order_item_notifi->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
if($order_item_notifi->approved_status_view==1){
$str.='<p>';
}
$str.=''.$order_item_notifi->product_name.' you ordered is approved.</span>';
if($order_item_notifi->prev_order_item_id!=''){
$str.='<br><span style="color:red;">Replaced Order</span>';
}
if($order_item_notifi->approved_status_view==1){
$str.='</p>';
}
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_placed_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';


}
}
$str.='</div>';
}
if($no_details==1){
$str.= '<div id="notificationsBody" class="notifications"><p class="text-center">No Details are available</p></div>';
}

$str.='<div id="notificationFooter" onclick="location.href=\''.base_url().'Account/notification\'" class="text-center text-justify cursor-pointer">See All</div>';


$str.='</div>';
echo $str;
?>



<?php }else{

?>
<div class="notifications">
<div class="notification_class"></div>
<p><b><h4 class="text-center text-justify">You are missing out!</h4></b></p>
<p class="text-center text-justify mt-10">Sign in to view personalized notifications and offers.</p>
<p class="text-center text-justify mt-10"><button class="button preventDflt" id="notify_login_btn"><i class="fa fa-lock"></i> Login</button></p>
</div>


<?php } ?>
</div>
</div>
</div>	
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center pl-0 pr-0">
<div class="logo hidden-xs" style="display:inline-block;margin-top: 5px;">
<a href="<?php echo base_url(); ?>"><img alt="Flamingo" src="<?php echo base_url();?>assets/images/VS_logo.png" width="200" style="margin-top: -20px;"/></a>
</div>

<div class="header-search-box mb-0 text-left" style="display:inline-block;margin-top:-0.5rem !important;">
<div style="text-align:right;">
	<!--<a href="javascript:switchToJarvinAIFun()" id="switch_to_jarvinai_btn"  style="display:block;font-weight:bold">Search Using Jarvin AI</a>-->
	<a href="javascript:switchToElasticSearchFun()" id="switch_to_elastic_search_btn" style="display:none;font-weight:bold">Switch to Elastic Search</a>
</div>
<!---
jarvin output  if it not come from either detail page or search category page then this will display automatically so the if condition in form 
--->
<form  class="form-inline" name="searchForm_org" id="searchForm_org" method="post" autocomplete="off" onsubmit="return searchFun('<?php echo $current_controller;?>')" <?php if(isset($jarvinaioutput) && $jarvinaioutput!=""){echo "style='display:none;background-color:#fafafa'";}else{echo "style='display:block;background-color:#fafafa'";}?>>    
<?php
if(!isset($_REQUEST['search_cat'])){
$_REQUEST['search_cat']="";
}
?>
<div class="form-group form-category"  style="display:none;">
<select class="select-category" name='search_cat' id='search_cat'>
<option value="" <?php if($_REQUEST['search_cat']==""){echo "selected";}?>>All Categories</option>
<?php
if(!empty($get_all_parent_categories)){
foreach($get_all_parent_categories as $parent_categories_arr){
?>
<option value="<?php echo $parent_categories_arr["pcat_id"]."_pcat"; ?>" <?php if($_REQUEST['search_cat']==$parent_categories_arr["pcat_id"]."_pcat"){ echo "selected";}?> ><?php echo $parent_categories_arr["pcat_name"];?></option>
<?php
}
}
?>
<?php
if(!empty($get_all_categories)){
foreach($get_all_categories as $categories_arr){
?>
<option value="<?php echo $categories_arr["pcat_id"]."_".$categories_arr["cat_id"];?>" <?php if($_REQUEST['search_cat']==$categories_arr["pcat_id"]."_".$categories_arr["cat_id"]){ echo "selected";} ?>><?php echo $categories_arr["cat_name"];?></option>
<?php
}
}
?>

</select>
</div>
<div class="form-group input-serach ui-widget">
<input type="text" onkeyup="doSearch()" id="search_keywords" name="search_keywords" placeholder="Search..." value="<?php if(!empty($_REQUEST["search_keyword"])){echo $_REQUEST["search_keyword"];}?>">
</div>
<div class="dropdown" id="dropssss" style="display:block;">
<ul class="dropdown-menu" id="search_listss" style="margin-top:8px;padding:2px 5px;width:100%;"></ul>
</div>
<input type="hidden" name="level_type" id="level_type" value="">
<input type="hidden" name="level_value" id="level_value"  value="">
<input type="hidden" name="level_name" id="level_name"  value="">
<input type="hidden" name="search_keyword_index_page" id="search_keyword_index_page" value="">
<button type="submit" class="pull-right btn-search preventDflt" id="search_keyword_btn"><i class="fa fa-search"></i></button>

</form>
<!---
jarvin output if it come from either detail page or search category page then this will display automatically so the if condition in form 
--->
<form  class="form-inline" name="searchForm_org_jarvinai" id="searchForm_org_jarvinai" method="post" autocomplete="off" style="background-color:#fafafa;<?php if(isset($jarvinaioutput) && $jarvinaioutput!=""){echo "display:block;";}else{echo "display:none;";}?>" >    
<div class="form-group input-serach ui-widget">
<input type="text" id="search_keywords_jarvinai" name="search_keywords_jarvinai" placeholder="Example Prompt: blue color vase with min 100 cm height..." onKeyPress="return triggerEnterKey(event);" >
</div>
<button type="button" class="pull-right btn-search preventDflt" id="search_keyword_jarvinai_btn" onclick="doJarvinAISearch()"><i class="fa fa-search"></i></button>
</form>
<p class="text-center" style="font-weight:bold;font-size:1.5rem;">
<?php 
	
	if(isset($jarvinaioutput) && $jarvinaioutput!=""){
		if($jarvinaioutput_number_of_skus==1){
			echo "Hey, found 1 product matching your prompt";
		}
		else{
			echo "Hey, found ".$jarvinaioutput_number_of_skus." products matching your prompt";
		}
	}
?>
</p>

</div>
<div class="support-link hidden-xs ml-20" style="display:inline-block;">
<?php
$badge='';
if($this->session->userdata("customer_id")){

?>
<?php 
$notification_count_arr=unviewed_notification();
$admin_msg_detail=$notification_count_arr["returns_msg_notification"];
$admin_msg_detail_repl=$notification_count_arr["repl_msg_notification"];
$admin_msg_detail_repl_stock=$notification_count_arr["repl_msg_notification_stock"];
$returns_notification=$notification_count_arr["returns_notification"];
$repl_notification=$notification_count_arr["replacement_notification"];
$repl_stock_notification=$notification_count_arr["repl_stock_notification"];
if($notification_count_arr["total_count"]>0){
$badge=$notification_count_arr["total_count"];
}else{
$badge='';
}
?>

<?php 
} ?>
<script>

$(document).ready(function () {
var badge='<?php echo $badge;?>';
if(badge!=''){
if ($(window).width() > 768) {
/// only desktop
$('.noti_Counter')
.css({ opacity: 0 })
.text(badge)           
.css({ top: '10px' })
.animate({ top: '4px', opacity: 1 }, 500);
}
else{
/// only mobile
$('.noti_Counter')
.css({ opacity: 1 })
.text(badge)           
.css({ top: '-7px' })
.animate({ top: '-7px', opacity: 1 }, 500);
}
}else{
if ($(window).width() > 768) {
/// only desktop
$('.noti_Counter')
.css({ opacity: 0 });
}
else{
/// only mobile
$('.noti_Counter')
.css({ opacity: 1 });
}
}
});
</script>

<a href='#' class='notificationLink' style="line-height:4;font-size: 1.3em;">
<i class="fa fa-bell" aria-hidden="true"></i>
<span class="noti_Counter"></span>
</a>
<!--notification popover---->


<?php
if($this->session->userdata("customer_id")){
$order_item_notifi_obj=call_notification_function();

if(empty($admin_msg_detail) && empty($admin_msg_detail_repl) && empty($returns_notification) && empty($returns_notification) && empty($order_item_notifi_obj) && empty($admin_msg_detail_repl_stock)){
$no_details=1;
//show - no details - message
}else{
$no_details=0;
}

$str='<div id="notification_wrapper">';	
$str.='<div id="notificationTitle">Notifications</div>';
if($no_details==0){
$str.='<div id="notificationsBody" class="notifications">';
if(!empty($admin_msg_detail)){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$admin_msg_detail->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$admin_msg_detail->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>There is a message from seller for your refund request.</p>';
$admin_msg_detail->admin_msg_timestamp='';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($admin_msg_detail->admin_msg_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}
if(!empty($admin_msg_detail_repl)){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$admin_msg_detail_repl->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$admin_msg_detail_repl->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>There is a message from seller for your replacement request.</p>';
$admin_msg_detail_repl->admin_msg_timestamp='';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($admin_msg_detail_repl->admin_msg_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}
if(!empty($admin_msg_detail_repl_stock)){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$admin_msg_detail_repl_stock->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$admin_msg_detail_repl_stock->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>There is a message from seller for your replacement request.</p>';
$admin_msg_detail_repl_stock->admin_msg_timestamp='';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($admin_msg_detail_repl_stock->admin_msg_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}

foreach ($returns_notification as $rn){
if(($rn->refund_success_view==1 || $rn->refund_success_view==0) && $rn->refund_success_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$rn->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$rn->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>Refund is successful.</p>';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($rn->refund_success_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}elseif(($rn->admin_reject_status_view==1 || $rn->admin_reject_status_view==0) && $rn->admin_reject_status_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$rn->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$rn->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>Your request for refund is rejected.</p>';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($rn->admin_reject_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';


}elseif($rn->refund_label_status_view==1 && $rn->refund_label_status_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$rn->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$rn->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>Refund label is sent.</p>';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($rn->refund_label_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}elseif(($rn->admin_accept_status_view==1 || $rn->admin_accept_status_view==0) && $rn->admin_accept_status_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$rn->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$rn->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>Your refund request is accepted.</p>';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($rn->admin_accept_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}
}

foreach ($repl_notification as $repl){

if(($repl->admin_reject_status_view==1 || $repl->admin_reject_status_view==0) && $repl->admin_reject_status_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$repl->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$repl->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>Your request for replacement is rejected.</p>';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($repl->admin_reject_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}
elseif($repl->repl_label_status_view==1 && $repl->repl_label_status_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$repl->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$repl->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>Replacement label is sent.</p>';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($repl->repl_label_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}
elseif(($repl->admin_accept_status_view==1 || $repl->admin_accept_status_view==0) && $repl->admin_accept_status_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$repl->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$repl->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
$str.='<p>Your replacement request is accepted.</p>';
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($repl->admin_accept_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';

}
}

foreach ($order_item_notifi_obj as $order_item_notifi){

if(($order_item_notifi->cancelled_status_view==1 || $order_item_notifi->cancelled_status_view==0) && $order_item_notifi->cancelled_status_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$order_item_notifi->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$order_item_notifi->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
if($order_item_notifi->cancelled_status_view==1){
$str.='<p>';
}
$str.=''.$order_item_notifi->product_name.' you ordered is Cancelled.</span>';
if($order_item_notifi->prev_order_item_id!=''){
$str.='<br><span style="color:red;">Replaced Order</span>';
}
if($order_item_notifi->cancelled_status_view==1){
$str.='</p>';
}
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_cancelled_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';


}elseif(($order_item_notifi->delivered_status_view==1 || $order_item_notifi->delivered_status_view==0) && $order_item_notifi->delivered_status_view!=''){


$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$order_item_notifi->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$order_item_notifi->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
if($order_item_notifi->delivered_status_view==1){
$str.='<p>';
}
$str.=''.$order_item_notifi->product_name.' you ordered is delivered.</span>';
if($order_item_notifi->prev_order_item_id!=''){
$str.='<br><span style="color:red;">Replaced Order</span>';
}
if($order_item_notifi->delivered_status_view==1){
$str.='</p>';
}
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_delivered_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';



}elseif(($order_item_notifi->shipped_status_view==1 || $order_item_notifi->shipped_status_view==0) && $order_item_notifi->shipped_status_view!=''){

$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$order_item_notifi->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$order_item_notifi->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
if($order_item_notifi->shipped_status_view==1){
$str.='<p>';
}
$str.=''.$order_item_notifi->product_name.' you ordered is shipped.</span>';
if($order_item_notifi->prev_order_item_id!=''){
$str.='<br><span style="color:red;">Replaced Order</span>';
}
if($order_item_notifi->shipped_status_view==1){
$str.='</p>';
}
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_shipped_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';


}elseif(($order_item_notifi->packed_status_view==1 || $order_item_notifi->packed_status_view==0 ) && $order_item_notifi->packed_status_view!='') {


$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$order_item_notifi->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$order_item_notifi->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
if($order_item_notifi->packed_status_view==1){
$str.='<p>';
}
$str.=''.$order_item_notifi->product_name.' you ordered is packed.</span>';
if($order_item_notifi->prev_order_item_id!=''){
$str.='<br><span style="color:red;">Replaced Order</span>';
}
if($order_item_notifi->packed_status_view==1){
$str.='</p>';
}
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_packed_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';


}elseif(($order_item_notifi->confirmed_status_view==1 || $order_item_notifi->confirmed_status_view==0 )&& $order_item_notifi->confirmed_status_view!=''){


$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$order_item_notifi->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$order_item_notifi->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
if($order_item_notifi->confirmed_status_view==1){
$str.='<p>';
}
$str.=''.$order_item_notifi->product_name.' you ordered is Confirmed.</span>';
if($order_item_notifi->prev_order_item_id!=''){
$str.='<br><span style="color:red;">Replaced Order</span>';
}
if($order_item_notifi->confirmed_status_view==1){
$str.='</p>';
}
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_confirmed_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';



}elseif(($order_item_notifi->approved_status_view==1 || $order_item_notifi->approved_status_view==0) && $order_item_notifi->approved_status_view!=''){


$str.=' <ul class="media-list cursor-pointer" onclick="location.href=\''.base_url().'Account/order_details/'.$order_item_notifi->order_id.'\'">';
$str.='<li class="media">';
$str.='<div class="media-left"><img src="'.base_url().$order_item_notifi->thumbnail.'" class="media-object" alt=""></div>';
$str.='<div class="media-body">';
if($order_item_notifi->approved_status_view==1){
$str.='<p>';
}
$str.=''.$order_item_notifi->product_name.' you ordered is approved.</span>';
if($order_item_notifi->prev_order_item_id!=''){
$str.='<br><span style="color:red;">Replaced Order</span>';
}
if($order_item_notifi->approved_status_view==1){
$str.='</p>';
}
$str.='<p><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_placed_timestamp)).'</p>';
$str.='</div>';
$str.='</li>';
$str.='</ul>';


}
}
$str.='</div>';
}
if($no_details==1){
$str.= '<div id="notificationsBody" class="notifications"><p class="text-center">No Details are available</p></div>';
}

$str.='<div id="notificationFooter" onclick="location.href=\''.base_url().'Account/notification\'" class="text-center text-justify cursor-pointer">See All</div>';


$str.='</div>';
echo $str;
?>



<?php }else{

?>

<div id="notification_wrapper">	
<div id="notificationTitle">Notifications</div>
<div id="notificationsBody" class="notifications">
<div class="notification_class"></div>
<p><b><h4 class="text-center text-justify">You are missing out!</h4></b></p>
<p class="text-center text-justify">Sign in to view personalized notifications and offers.</p>
<p class="text-center text-justify "><button class="button" id="notify_login_btn"><i class="fa fa-lock"></i> Login</button></p>
<hr>
</div>
</div>	

<?php } ?>
<?php if(!$this->session->userdata("customer_name")) { ?>
	
	<?php /****** Normal Login conditions starts  *******/?>
	<a href="<?php echo base_url();?>login" class="ml-10" style="line-height:4;font-size: 1.3em;"> <i class="fa fa-arrow-circle-o-right"></i> Login</a>
			
	<?php /****** Normal Login conditions ends *******/?>
	
	

<?php } ?>



	
<?php if($this->session->userdata("customer_name")){ ?>

<div class="dropdown ml-10" style="display:inline-block;line-height:4;font-size: 1.3em;">
<a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><span><?php echo mb_strimwidth($this->session->userdata("customer_name"), 0, 15, "..."); ?></span> <span class="caret"></span></a>
<ul class="dropdown-menu mega_dropdown" role="menu">



<?php 
//echo $this->session->userdata("user_type").'ccc';
if($this->session->userdata("user_type")=='franchise_customer'){

    ?>

<li><a href="<?php echo base_url();?>Account/franchise_account">My Account</a></li>
<li><a href="<?php echo base_url();?>Account/franchise_projects">My Projects</a></li>
    <?php 
}else{ 
    ?>

    <li><a href="<?php echo base_url();?>Account">My Account</a></li>
    <li><a href="<?php echo base_url();?>Account/my_order">Orders</a></li>
    <li><a href="<?php echo base_url();?>Account/wallet">Wallet</a></li>
    <li><a href="<?php echo base_url();?>Account/wishlist">Wishlists</a></li>
    <li><a href="<?php echo base_url();?>Account/reviews">Reviews &amp; Ratings</a></li>

    <?php 

?>
<?php } ?>

<li><a href="javascript:logoutFun();">Logout</a></li>

</ul>

</div>

<?php } ?>

<?php if($this->session->userdata("customer_name") && $this->session->userdata("user_type")=='customer') { ?>

<?php } ?>

</div>
    
    <?php
if(!isset($Custom404)){
?>
<div class="group-button-header hidden-xs ml-20" id="rightSideCartController_div" style="display:inline-block;" ng-controller="rightSideCartController" >
<div class="btn-cart" id="cart-block">
<a title="My cart" href="<?php echo base_url() ?>cart">Cart</a>
<span class="notify notify-right ng-binding cart_count">0</span>
</div>
    <!--- added --->


    <script type="text/javascript">

        function rightSideCartController($scope,$rootScope,Scopes,$http,$sce,$timeout) {
           // alert('tyy inside controlller')
            $rootScope.$on("CallRightSideCart", function(){
                $scope.getRightSideCart();
            });

            Scopes.store('rightSideCartController', $scope);
            $scope.promo_temp=0;
            $scope.getRightSideCart = function () {
                //alert('right side cart');

                c_id = '<?php echo $this->session->userdata("customer_id");?>';
                if (c_id != '' && c_id != null) {
                    $http({
                        method: "POST",
                        url: "<?php echo base_url();?>Account/get_cartinfo_in_json_format",
                        data: "1=2",
                        dataType: "JSON",
                    }).success(function mySucces(result) {

                        $scope.products_order_right = result;
                        $scope.order_product_count = $scope.products_order_right.length;
                        
                        $scope.get_promotion_active_status_all_header($scope.order_product_count,$scope.products_order_right,1);
                        
                        $scope.order_product_count = $scope.products_order_right.length;
                        $scope.update_cart_controller_scopes_header($scope.order_product_count, $scope.products_order_right, 11);

                        $timeout(function() {
							 $scope.$apply();
						});
                        $scope.openCartDiv();
                        //console.log(result);

                    }, function myError(response) {
                        alert("Please try again");
                    });
                } else {
                    if (localStorage.LocalCustomerCart != null && localStorage.LocalCustomerCart != "[]") {
                        result=JSON.parse(localStorage.LocalCustomerCart);

                        $scope.products_order_right = JSON.parse(localStorage.LocalCustomerCart);
                        $scope.order_product_count = $scope.products_order_right.length;

                        $scope.order_product_count = $scope.products_order_right.length;
                        $scope.update_cart_controller_scopes_header($scope.order_product_count, $scope.products_order_right, 10);
						$timeout(function() {
							 $scope.$apply();
						});
                        
                        $scope.openCartDiv();

                    } else {
                        /*hide the right side cart division*/
                    }
                }
            }
            /* code copied from cart  */
            $scope.update_cart_controller_scopes_header = function (order_product_count, products_order, w = '') {
                //alert('on load');

                var searched_pincode = localStorage.searched_pincode;
                var vendor_id = localStorage.vendor_id;
                $scope.pincode = searched_pincode;
                $scope.vendor_id = vendor_id;
                if (searched_pincode != null && searched_pincode != '') {
                    $scope.searched_pincode = searched_pincode;
                    $scope.pincode = $scope.searched_pincode;
                }

                $scope.count_of_free_items = new Array();

                $scope.out_of_stock_item_in_cart = [];
                $scope.applied_at_invoice_arr = [];
                $scope.order_product_count = order_product_count;
                $scope.products_order = products_order;

                $scope.final_count_free_items = 0;//newly added
                $scope.availed_offers_heading_flag = "no";

                /// this is added on 7_15_2021 because of shipping charges not coming in cart items if we not applied promotion starts
                $scope.eligible_free_shipping_display = 0;
                /// this is added on 7_15_2021 because of shipping charges not coming in cart items if we not applied promotion ends


                if ($scope.order_product_count == 0) {
                    $scope.show_cash_back_tr = 0;
                    $scope.eligible_cash_back_percentage_display = 0;
                    $scope.eligible_discount_percentage_display = 0;
                    $scope.eligible_free_shipping_display = 0;

                    $scope.eligible_free_surprise_gift_display = 0;
                    $scope.eligible_cash_back_percentage_display = 0;
                    $scope.discount_oninvoice = 0;
                    $scope.cash_back_oninvoice = 0;
                    $scope.free_shipping = 0;
                    $scope.surprise_gift = 0;
                    $scope.total_cash_back = 0;
                    $scope.show_surprise_gift_on_invoice = 0;
                    $scope.invoice_surprise_gift = 0;
                    $scope.final_count_free_items = 0;
                    return false;
                }

                //alert(w);

                $scope.total_cash_back_for_sku = 0;
                total_netprice = 0;
                individual_price_without_shipping = 0;
                total_shipping_price = 0;
                $scope.cash_back_tr_arr = [];
                $scope.total_addon_products=0;
                $scope.total_shipping_price_addon=0;
                if (localStorage.changed_moq_with_inventory_id != null && localStorage.changed_moq_with_inventory_id != '') {
                    changed_inv = localStorage.changed_moq_with_inventory_id;
                    changed_inv = JSON.parse(changed_inv);
                    inventory_id_to_update = changed_inv.inventory_id;
                } else {
                    inventory_id_to_update = '';
                }


                $scope.total_inventory_quantity_in_cart = 0;
                for (j = 0; j < $scope.order_product_count; j++) {

                    //alert($scope.products_order[j].shipping_charge);

                    $scope.total_inventory_quantity_in_cart += parseInt($scope.products_order[j].inventory_moq);
                    var val = $scope.products_order[j].attribute_1_value.split(':');
                    $scope.products_order[j].attribute_1_value = val[0];
                    //alert($scope.products_order[j].stock_available+"||"+parseInt($scope.products_order[j].inventory_moq));

                    if (parseInt($scope.products_order[j].stock_available) >= parseInt($scope.products_order[j].inventory_moq)) {

                        $scope.products_order[j].item_stock_available = 1;

                        if (c_id == null || c_id == '') {
                            $scope.products_order[j].selling_price = $scope.products_order[j].inventory_selling_price_with_out_discount;
                        }//without session


                        if (products_order[j].inventory_selling_price == null) {
                            //when fetching the data from db
                            $scope.products_order[j].selling_price = $scope.products_order[j].selling_price;
                            $scope.products_order[j].inventory_selling_price = $scope.products_order[j].selling_price;
                        } else {

                            $scope.products_order[j].selling_price = products_order[j].inventory_selling_price;
                        }

                        if ($scope.products_order[j].promotion_minimum_quantity != '') {

                            if (parseInt($scope.products_order[j].promotion_minimum_quantity) <= parseInt($scope.products_order[j].inventory_moq)) {
                                $scope.products_order[j].show_addon_items = 1;
                            } else {
                                $scope.products_order[j].show_addon_items = 0;
                            }
                        } else {
                            $scope.products_order[j].show_addon_items = 0;
                        }

                        $scope.products_order[j].final_product_individual = 0;

                        modulo = $scope.products_order[j].quantity_without_promotion;
                        quotient = $scope.products_order[j].quantity_with_promotion;

                        $scope.products_order[j].modulo = modulo;
                        $scope.products_order[j].quotient = quotient;
                        $scope.products_order[j].price_of_quotient_1 = Math.round($scope.products_order[j].individual_price_of_product_with_promotion);
                        $scope.products_order[j].price_of_modulo_1 = $scope.products_order[j].individual_price_of_product_without_promotion;
                        
                        /* for checking */
                           /* promo_id_selected =$scope.products_order[j].promotion_id_selected ;
                            //alert(promo_id_selected+"promo_id_selected");
                            //$scope.products_order[j].promotion_available == 1 && 
                            if (promo_id_selected!=null && promo_id_selected!='') {
                                //confirms promotion available
                                //$scope.get_promotion_active_status_header(promo_id_selected);
                                
                                if($scope.promo_temp==2){
                                    if($scope.products_order[j].promotion_available==1){
                                        //promotion active in localStorage but in backend it is inactive or expired
                                        $scope.products_order[j].promotion_available=0;
                                    }
                                }else{
                                    $scope.products_order[j].promotion_available=1;//
                                }
                                //alert($scope.promo_temp+'return_variable1');
                            }
                            */
                            /* for checking */
                            

                        if ($scope.products_order[j].promotion_available == 1) {

                       if ($scope.products_order[j].default_discount != "") {


                                if (modulo != 0 && ($scope.products_order[j].inventory_moq >= parseInt($scope.products_order[j].promotion_minimum_quantity))) {

                                    $scope.products_order[j].discount = 1;//default
                                    $scope.products_order[j].promotion = 2;


                                    $scope.products_order[j].price_after_default_discount = $scope.products_order[j].total_price_of_product_without_promotion;

                                    if (quotient != 0) {
                                        $scope.products_order[j].price_after_promotion_discount = $scope.products_order[j].total_price_of_product_with_promotion;
                                    }

                                    if (quotient == 0 && modulo != 0) {
                                        $scope.products_order[j].promotion_quote_show = 0;
                                    } else {
                                        $scope.products_order[j].promotion_quote_show = 1;
                                    }
                                    if (quotient != 0 && modulo == 0) {
                                        $scope.products_order[j].discount_quote_show = 0;
                                    } else {
                                        $scope.products_order[j].discount_quote_show = 1;
                                    }

                                    if ($scope.products_order[j].price_after_promotion_discount != null) {
                                        $scope.products_order[j].final_product_individual = parseInt($scope.products_order[j].price_after_promotion_discount) + parseInt($scope.products_order[j].price_after_default_discount);
                                    } else {
                                        $scope.products_order[j].promotion = "";
                                        $scope.products_order[j].discount = 1;
                                        $scope.products_order[j].final_product_individual = $scope.products_order[j].price_after_default_discount;
                                    }

                                } else {

                                    if ($scope.products_order[j].inventory_moq >= parseInt($scope.products_order[j].promotion_minimum_quantity)) {

                                        $scope.products_order[j].promotion_quote_show = 1;
                                        $scope.products_order[j].discount_quote_show = 0;

                                        $scope.products_order[j].price_after_promotion_discount = $scope.products_order[j].total_price_of_product_with_promotion;
                                        $scope.products_order[j].final_product_individual = $scope.products_order[j].price_after_promotion_discount;

                                        $scope.products_order[j].price_after_default_discount = 0;

                                        $scope.products_order[j].promotion = 1;
                                        $scope.products_order[j].discount = "";

                                    } else {

                                        if (quotient == 0 && modulo != 0) {
                                            $scope.products_order[j].promotion_quote_show = 0;
                                        } else {
                                            $scope.products_order[j].promotion_quote_show = 1;
                                        }
                                        if (quotient != 0 && modulo == 0) {
                                            $scope.products_order[j].discount_quote_show = 0;
                                        } else {
                                            $scope.products_order[j].discount_quote_show = 1;
                                        }

                                        $scope.products_order[j].price_after_default_discount = $scope.products_order[j].total_price_of_product_without_promotion;

                                        $scope.products_order[j].promotion_quote_show = 0;
                                        $scope.products_order[j].discount_quote_show = 1;

                                        $scope.products_order[j].promotion = "";
                                        $scope.products_order[j].discount = 1;
                                        $scope.products_order[j].final_product_individual = $scope.products_order[j].price_after_default_discount;

                                    }

                                }

                            }//discount promotion !=0

                            if ($scope.products_order[j].default_discount == "") {

                                if (modulo != 0) {

                                    $scope.products_order[j].discount = 1;
                                    $scope.products_order[j].promotion = 2;
                                    $scope.products_order[j].price_of_modulo_1 = $scope.products_order[j].selling_price;

                                    $scope.products_order[j].price_after_default_discount = $scope.products_order[j].total_price_of_product_without_promotion;

                                    if (quotient != 0) {

                                        $scope.products_order[j].price_of_quotient_1 = Math.round($scope.products_order[j].individual_price_of_product_with_promotion);

                                        $scope.products_order[j].price_after_promotion_discount = $scope.products_order[j].total_price_of_product_with_promotion;
                                    }

                                    if (quotient == 0 && modulo != 0) {
                                        $scope.products_order[j].promotion_quote_show = 0;
                                    } else {
                                        $scope.products_order[j].promotion_quote_show = 1;
                                    }
                                    if (quotient != 0 && modulo == 0) {
                                        $scope.products_order[j].discount_quote_show = 0;
                                    } else {
                                        $scope.products_order[j].discount_quote_show = 1;
                                    }

                                    if ($scope.products_order[j].price_after_promotion_discount != null) {
                                        $scope.products_order[j].final_product_individual = parseInt($scope.products_order[j].price_after_promotion_discount) + parseInt($scope.products_order[j].price_after_default_discount);
                                    } else {

                                        $scope.products_order[j].promotion = "";
                                        $scope.products_order[j].discount = 1;
                                        $scope.products_order[j].final_product_individual = $scope.products_order[j].price_after_default_discount;
                                    }

                                } else {

                                    $scope.products_order[j].promotion_quote_show = 1;
                                    $scope.products_order[j].discount_quote_show = 0;
                                    $scope.products_order[j].price_after_promotion_discount = $scope.products_order[j].total_price_of_product_with_promotion;

                                    $scope.products_order[j].final_product_individual = $scope.products_order[j].price_after_promotion_discount;
                                    $scope.products_order[j].price_after_default_discount = 0;
                                    $scope.products_order[j].promotion = 1;
                                    $scope.products_order[j].discount = "";

                                }

                            }

                        }//promo avai 1


                        if ($scope.products_order[j].promotion_available == 0) {
                            $scope.products_order[j].final_product_individual = $scope.products_order[j].selling_price * ($scope.products_order[j].inventory_moq);
                            $scope.products_order[j].discount_quote_show = 0;
                            $scope.products_order[j].promotion_quote_show = 0;
                        }

                        ///////////////////promotion/////////////////

                        $scope.products_order[j].inventory_selling_price_without_discount = (($scope.products_order[j].selling_price) * ($scope.products_order[j].inventory_moq));

                        $scope.products_order[j].inventory_selling_price_with_discount = ($scope.products_order[j].inventory_selling_price_without_discount);

                        if (parseInt($scope.products_order[j].shipping_charge) != parseInt(-1) && $scope.pincode != null && $scope.pincode != '') {

                            //alert($scope.products_order[j].final_product_individual+"final_price_ind");

                            if ($scope.products_order[j].final_product_individual != 0) {
                                $scope.products_order[j].subtotal = parseInt($scope.products_order[j].final_product_individual) + parseInt($scope.products_order[j].shipping_charge);
                            } else {
                                $scope.products_order[j].subtotal = parseInt($scope.products_order[j].inventory_selling_price_without_discount) + parseInt($scope.products_order[j].shipping_charge);

                            }

                            $scope.products_order[j].shipping_charge_msg = 0;
                            $scope.products_order[j].shipping_charge_view = 1;
                            if ($scope.products_order[j].shipping_charge == 0) {
                                $scope.products_order[j].msg_style = "color:green;";
                                $scope.products_order[j].delivery_service_msg = 'Free Delivery Service';
                            } else {
                                $scope.products_order[j].msg_style = "color:#e46c0a;";
                                $scope.products_order[j].delivery_service_msg = 'Service available at ' + $scope.pincode;
                            }
                        } else {

                            $scope.products_order[j].subtotal = parseInt($scope.products_order[j].inventory_selling_price_without_discount);
                            $scope.products_order[j].shipping_charge = 0;
                            $scope.products_order[j].shipping_charge_msg = "Not Applied";
                            $scope.products_order[j].shipping_charge_view = 0;
                            $scope.products_order[j].msg_style = "color:red;";
                            $scope.products_order[j].delivery_service_msg = 'Service is Not Available';
                        }

                        //alert($scope.products_order[j].shipping_charge);

                        if ($scope.products_order[j].final_product_individual) {
                            if (parseInt($scope.products_order[j].shipping_charge) != parseInt(-1)) {
                                $scope.products_order[j].subtotal = parseInt($scope.products_order[j].final_product_individual) + parseInt($scope.products_order[j].shipping_charge);
                                total_shipping_price += parseFloat($scope.products_order[j].shipping_charge);

                            } else {
                                $scope.products_order[j].subtotal = parseInt($scope.products_order[j].final_product_individual);
                                //total_shipping_price+=parseFloat($scope.products_order[j].shipping_charge);
                            }
                        }


                        //alert($scope.products_order[j].shipping_charge);

                        //alert($scope.products_order[j].subtotal+"subtotal");


                        if ($scope.products_order[j].promotion_cashback > 0) {

                            $scope.show_cash_back_tr = 1;

                            cash_back_amount = parseFloat($scope.products_order[j].cash_back_value);
                            $scope.total_cash_back_for_sku += parseFloat(cash_back_amount);

                            $scope.cash_back_tr_arr.push({
                                "promotion_cashback_percentage": $scope.products_order[j].promotion_cashback,
                                "product_name": $scope.products_order[j].product_name,
                                "inventory_sku": $scope.products_order[j].inventory_sku,
                                "cash_back_amount": cash_back_amount,
                                "sku_count": $scope.products_order[j].inventory_moq
                            });

                        }

                        /* addon products */
                        $scope.products_order[j].addon_count=0;
                            $scope.products_order[j].addon_count_updated=0;

                            if($scope.products_order[j].addon_products_status=='1'){
                                var response=$scope.products_order[j].addon_products;
                                console.log(response);
                                console.log(typeof response);

                                if(typeof response== 'string'){
                                     
                                    rr= $scope.products_order[j].addon_products;
                                    console.log( rr.charAt(0));
                                    if ( rr.charAt(0)=="\""){
                                        rr = rr.replace(/(^"|"$)/g, '');
                                        if(rr!=''){
                                        $scope.products_order[j].addon_products=JSON.parse(rr);
                                        }else{
                                             $scope.products_order[j].addon_products='';
                                        }
                                    }else{
                                        $scope.products_order[j].addon_products=JSON.parse(rr);
                                    }
                                    
                                }else{
                                    $scope.products_order[j].addon_products=$scope.products_order[j].addon_products;
                                }

                                $scope.products_order[j].addon_count_updated=1;
                                
                                $scope.products_order[j].addon_count=$scope.products_order[j].addon_products.length;
                                //$scope.show_addon_products(j,$scope.products_order[j].addon_inventories,$scope.products_order[j].inventory_id);

                                var addon_data=$scope.products_order[j].addon_products;
                                var tot=0; var ship=0;
                                for(u=0;u<(addon_data.length);u++){
                                    tot+=parseInt(addon_data[u].inv_price);
                                    ship+=parseInt(addon_data[u].inv_ship_price);
                                }
                                $scope.products_order[j].addon_total_price=parseInt(tot);
                                $scope.products_order[j].addon_total_shipping_price=parseInt(ship);
                                $scope.products_order[j].subtotal+=parseInt(tot);
                                $scope.total_addon_products+=parseInt(tot);
                                

                            }

                            /* addon products */


                    } else {

                        $scope.products_order[j].item_stock_available = 0;
                        $scope.products_order[j].subtotal = 0;

                        $scope.out_of_stock_item_in_cart.push(1);
                    }//checking out of stock


                    if(parseInt($scope.products_order[j].addon_total_shipping_price)>0){
                           
                        $scope.products_order[j].shipping_charge_total=(parseInt($scope.products_order[j].shipping_charge)+parseInt($scope.products_order[j].addon_total_shipping_price));
                        //alert($scope.products_order[j].shipping_charge);

                        $scope.products_order[j].subtotal+=parseInt($scope.products_order[j].addon_total_shipping_price);
                        //$scope.products_order[j].final_product_individual+=parseInt($scope.products_order[j].addon_total_shipping_price);

                        $scope.total_shipping_price+=parseInt($scope.products_order[j].addon_total_shipping_price);
                        $scope.total_shipping_price_addon+=parseInt($scope.products_order[j].addon_total_shipping_price);
                    }else{
                        $scope.products_order[j].shipping_charge_total=parseInt($scope.products_order[j].shipping_charge);
                    }

                    total_netprice += parseFloat($scope.products_order[j].subtotal);

                    individual_price_without_shipping += parseInt($scope.products_order[j].final_product_individual);

                }//forloop


                if ($scope.cash_back_tr_arr.length > 0) {
                    $scope.show_cash_back_tr = 1;
                } else {
                    $scope.show_cash_back_tr = 0;
                }

                if(parseFloat($scope.total_addon_products)>0){
                    individual_price_without_shipping+=parseFloat($scope.total_addon_products);
                }
                $scope.total_price_without_shipping_price = parseFloat(individual_price_without_shipping);

                $scope.grandtotal_without_discount = parseInt(total_netprice);
                $scope.grandTotal = parseInt(total_netprice);
                $scope.eligible_discount_percentage_display = 0;
                $scope.total_shipping_price = parseInt(total_shipping_price)+parseInt($scope.total_shipping_price_addon);

                $scope.grandTotal_for_discount = 0;
                $scope.total_shipping_price_actual = 0;

                <?php
                $price_list = array();
                $promotions_cart=get_promotions_cart();
                if(!empty($promotions_cart)){
                ?>

                $scope.eligible_cash_back_percentage_display = 0;
                $scope.eligible_discount_percentage_display = 0;
                $scope.eligible_free_shipping_display = 0;


                $scope.eligible_free_surprise_gift_display = 0;
                $scope.discount_oninvoice = 0;
                $scope.cash_back_oninvoice = 0;
                $scope.free_shipping = 0;
                $scope.surprise_gift = 0;
                $scope.show_surprise_gift_on_invoice = 0;
                $scope.invoice_surprise_gift = 0;

                $scope.promotions_all = [];

                <?php
                //$cart_promo_arr=array();
                foreach($promotions_cart as $data){

                $buy_type_arr = explode(',', $data['buy_type']);

                if (count($buy_type_arr) == 1) {
                    if (preg_match('/surprise/', strtolower($data["promo_quote"]))) {
                        array_push($price_list, $data);
                    }

                }

                ?>
                //alert('<?php echo json_encode($data); ?>');

                $scope.promotions_cart = [];
                var promotion_id = '<?php echo $data['promo_uid']; ?>';
                var promo_to_buy = '<?php echo $data['to_buy']; ?>';
                var promo_to_get = '<?php echo $data['to_get']; ?>';
                var promo_buy_type = '<?php echo $data['buy_type']; ?>';
                var promo_get_type = '<?php echo $data['get_type']; ?>';
                var promo_name = '<?php echo $data['promo_name']; ?>';
                var applied_at_invoice = '<?php echo $data['applied_at_invoice']; ?>';


                $scope.promotion_id_arr = promotion_id.split(',');
                $scope.promo_to_buy_arr = promo_to_buy.split(',');
                $scope.promo_to_get_arr = promo_to_get.split(',');
                $scope.promo_buy_type_arr = promo_buy_type.split(',');
                $scope.promo_get_type_arr = promo_get_type.split(',');
                $scope.promo_name_arr = promo_name.split(',');


                $scope.promotions_cart.push({
                    'promotion_id_arr': $scope.promotion_id_arr,
                    'promo_to_buy_arr': $scope.promo_to_buy_arr,
                    'promo_to_get_arr': $scope.promo_to_get_arr,
                    'promo_buy_type_arr': $scope.promo_buy_type_arr,
                    'promo_get_type_arr': $scope.promo_get_type_arr,
                    'promo_name_arr': $scope.promo_name_arr
                });
                //console.log("pomotions  cart");
                //console.log($scope.promotions_cart);
                for (var t = 0; t < $scope.promotions_cart.length; t++) {

                    //alert($scope.promotions_all[t][u].promotion_id_arr);

                    promo_to_buy_arr = $scope.promotions_cart[t].promo_to_buy_arr;
                    promo_to_get_arr = $scope.promotions_cart[t].promo_to_get_arr;
                    promo_buy_type_arr = $scope.promotions_cart[t].promo_buy_type_arr;
                    promo_get_type_arr = $scope.promotions_cart[t].promo_get_type_arr;
                    promo_name_arr = $scope.promotions_cart[t].promo_name_arr;

                    discount_oninvoice_str = '';

                    for (var v = 0; v < promo_to_buy_arr.length; v++) {

                        to_buy = promo_to_buy_arr[v];
                        to_buy_previous = promo_to_buy_arr[v - 1];
                        to_get = promo_to_get_arr[v];
                        to_get_previous = promo_to_get_arr[v - 1];
                        buy_type = promo_buy_type_arr[v];
                        get_type = promo_get_type_arr[v];
                        promo_name = promo_name_arr[v];

                        if ((to_get == '') && (get_type == '')) {

                            if ($scope.total_shipping_price == 0) {
                                $scope.free_shipping = 'Search the pincode to get <strong>Free shipping</strong> .';
                                $scope.explicitlyTrustedHtml_free_shipping = $sce.trustAsHtml($scope.free_shipping);
                                $scope.eligible_free_shipping_display = 0;
                                $scope.free_shipping = 1;
                            } else {
                                $scope.currency_type_free_shipping = buy_type;
                                $scope.eligible_free_shipping_display = 0;
                                $scope.free_shipping = 0;

                                if (parseInt(to_buy) > parseInt(individual_price_without_shipping)) {

                                    $scope.total_shipping_price_actual = 0;
                                    $scope.eligible_free_shipping_display = 0;
                                    $scope.free_shipping = 1;

                                } else {
                                    $scope.availed_offers_heading_flag = "yes";
                                    $scope.eligible_free_shipping_display = 1;

                                    $scope.total_shipping_price_actual = $scope.total_shipping_price;
                                    //$scope.free_shipping=0; // I commented in preparing the You are eligible for Free Shipping! message
                                }
                            }
                        }

                        if (get_type == "discount") {

                            //console.log("tobuy"+parseInt(to_buy)+"individualprice"+parseInt(individual_price_without_shipping)+"to get previous"+to_get_previous);

                            $scope.discount_oninvoice = 0;

                            if (parseInt(to_buy) > individual_price_without_shipping) {
                                if (to_get_previous > 0) {
                                    $scope.grandTotal_for_discount = Math.round((to_get_previous / 100) * individual_price_without_shipping);
                                    $scope.eligible_discount_percentage_display = 1;
                                    $scope.eligible_discount_percentage = to_get_previous;
                                    $scope.currency_type_discount = buy_type;
                                } else {
                                    $scope.eligible_discount_percentage_display = 0;
                                    $scope.explicitlyTrustedHtml_discount = $sce.trustAsHtml('');
                                }
                                if (individual_price_without_shipping < to_buy) {
                                    $scope.discount_oninvoice = 1;
//$scope.discount_oninvoice='Expend more '+parseInt(to_buy-individual_price_without_shipping)+' '+buy_type+' to get <strong>'+to_get+'% '+get_type+' on INVOICE</strong> .';

                                    $scope.discount_oninvoice = discount_oninvoice_str;

                                    $scope.explicitlyTrustedHtml_discount = $sce.trustAsHtml($scope.discount_oninvoice);
                                    

                                } else {
                                    $scope.discount_oninvoice = 0;
                                    $scope.explicitlyTrustedHtml_discount = $sce.trustAsHtml("");
                                }
                                break;

                            } else {
                                //individual_price_without_shipping - reached the maximum dicount price or percentage
                                if (to_get > 0) {

                                    $scope.grandTotal_for_discount = Math.round((to_get / 100) * individual_price_without_shipping)
                                    $scope.eligible_discount_percentage_display = 1;
                                    $scope.eligible_discount_percentage = to_get;
                                    $scope.currency_type_discount = buy_type;


                                } else {
                                    $scope.eligible_discount_percentage_display = 0;

                                }

                            }
                        }

                        if (get_type == "cash back") {
                            $scope.cash_back_oninvoice = 0;
                            if (parseInt(to_buy) > individual_price_without_shipping) {
                                if (to_get_previous > 0) {
                                    $scope.grandTotal_for_cash_back = parseFloat((to_get_previous / 100) * individual_price_without_shipping).toFixed(2);
                                    $scope.eligible_cash_back_percentage_display = 1;
                                    $scope.eligible_cash_back_percentage = to_get_previous;
                                    $scope.currency_type_cash_back = buy_type;
                                } else {
                                    $scope.eligible_cash_back_percentage_display = 0;
                                    //$scope.explicitlyTrustedHtml_cash_back = $sce.trustAsHtml('');
                                }
                                if (individual_price_without_shipping < to_buy) {
                                    $scope.cash_back_oninvoice = 1;
                                } else {
                                    $scope.cash_back_oninvoice = 0;
                                    //$scope.explicitlyTrustedHtml_cash_back = $sce.trustAsHtml("");
                                }
                                break;
                            } else {
                                if (to_get > 0) {

                                    $scope.grandTotal_for_cash_back = parseFloat((to_get / 100) * individual_price_without_shipping).toFixed(2);
                                    $scope.eligible_cash_back_percentage_display = 1;
                                    $scope.eligible_cash_back_percentage = to_get;
                                    $scope.currency_type_cash_back = buy_type;

                                } else {
                                    $scope.eligible_cash_back_percentage_display = 0;

                                }

                            }
                        }

                    }

                }

                <?php
                }

                $m = array();
                foreach ($price_list as $key => $row) {
                    $m[$key] = $row['to_buy'];
                }
                array_multisort($m, SORT_ASC, $price_list);

                }?>
                $scope.applied_at_invoice_arr =<?php echo json_encode($price_list); ?>;
                //alert(applied_at_invoice_arr);

                if ($scope.applied_at_invoice_arr.length > 0) {

                    $scope.show_surprise_gift_on_invoice = 1;

                    for (h = 0; h < $scope.applied_at_invoice_arr.length; h++) {

                        if (parseInt($scope.applied_at_invoice_arr[h].to_buy) > parseInt(individual_price_without_shipping)) {

                            //for top alert
                            expand_more = parseInt($scope.applied_at_invoice_arr[h].to_buy) - parseInt(individual_price_without_shipping);

                            $scope.invoice_surprise = ' To eligible for (' + $scope.applied_at_invoice_arr[h].promo_quote + ') expend more ' + expand_more + ' ' + $scope.applied_at_invoice_arr[h].buy_type;
                            $scope.explicitlyTrustedHtml_invoice_surprise_gift = $sce.trustAsHtml($scope.invoice_surprise);
                            $scope.invoice_surprise_gift = 1;

                            //for bootom messsage

                            if ($scope.applied_at_invoice_arr[h - 1] != null) {
                                $scope.invoice_surprise_result = $scope.applied_at_invoice_arr[h - 1].promo_quote;
                                $scope.explicitlyTrustedHtml_invoice_surprise_gift_result = $sce.trustAsHtml($scope.invoice_surprise_result);
                                $scope.invoice_surprise_gift_result = 1;
                            } else {

                                $scope.explicitlyTrustedHtml_invoice_surprise_gift_result = $sce.trustAsHtml("");
                                $scope.invoice_surprise_gift_result = 0;

                            }
                            break;

                        } else {
                            $scope.explicitlyTrustedHtml_invoice_surprise_gift = $sce.trustAsHtml("");
                            $scope.invoice_surprise_gift = 0;

                            $scope.invoice_surprise_result = $scope.applied_at_invoice_arr[h].promo_quote;
                            $scope.explicitlyTrustedHtml_invoice_surprise_gift_result = $sce.trustAsHtml($scope.invoice_surprise_result);
                            $scope.invoice_surprise_gift_result = 1;
                        }
                    }

                } else {
                    $scope.show_surprise_gift_on_invoice = 0;
                }


                if ($scope.eligible_discount_percentage_display == 1 || $scope.eligible_free_shipping_display == 1) {
                    if ($scope.total_shipping_price_actual) {
                        $scope.grandTotal_after_deduction = (parseInt($scope.grandtotal_without_discount) - parseInt($scope.grandTotal_for_discount) - parseInt($scope.total_shipping_price_actual));
                        $scope.amount_to_pay = $scope.grandTotal_after_deduction;
                    } else {
                        $scope.amount_to_pay = parseInt($scope.grandtotal_without_discount) - parseInt($scope.grandTotal_for_discount);
                    }

                } else {
                    $scope.amount_to_pay = parseInt($scope.grandTotal);
                }

                if ($scope.eligible_discount_percentage_display == 1 || $scope.eligible_free_shipping_display == 1) {

                    if (parseInt($scope.total_shipping_price_actual)) {
                        $scope.you_saved = parseInt($scope.grandTotal_for_discount) + parseInt($scope.total_shipping_price_actual);
                    } else {
                        $scope.you_saved = parseInt($scope.grandTotal_for_discount)
                    }

                } else {
                    $scope.you_saved = 0;
                }

                if ($scope.show_cash_back_tr == 1) {
                    if ($scope.grandTotal_for_cash_back == null) {
                        $scope.grandTotal_for_cash_back = 0;
                    }
                    if ($scope.total_cash_back_for_sku == null) {
                        $scope.total_cash_back_for_sku = 0;
                    }
                    $scope.total_cash_back = parseFloat($scope.grandTotal_for_cash_back) + parseFloat($scope.total_cash_back_for_sku);
                    $scope.total_cash_back = $scope.total_cash_back.toFixed(2);
                } else {
                    //alert($scope.grandTotal_for_cash_back);

                    if (parseInt($scope.grandTotal_for_cash_back) > 0) {

                        $scope.total_cash_back = parseInt($scope.grandTotal_for_cash_back);
                    } else {
                        $scope.total_cash_back = 0;
                    }
                }

                /*added*/
                if($scope.eligible_discount_percentage_display==1){
                  $scope.products_order_right_total=  $scope.grandtotal_without_discount;
                }

                if($scope.eligible_discount_percentage_display==0){
                    $scope.products_order_right_total=$scope.amount_to_pay;

                }
                //alert($scope.amount_to_pay+"amnt pay"+w + "$scope.products_order_right_total="+$scope.products_order_right_total);

                /*added*/

            }
            $scope.get_promotion_active_status_all_header=function(order_product_count, products_order, w = ''){
                        //alert(2);
                        $http({
                            method: "POST",
                            url: "<?php echo base_url();?>get_promotion_active_status_all",
                            data: {'products_order': products_order},
                            dataType: "JSON",
                            async: false
                        }).success(function mySucces(result) {
                            if (jQuery.isEmptyObject(result)) {
                                
                            }else{
                          
                                res=JSON.parse(JSON.stringify(result));
                                //console.log(res);
                                for($k=0;$k<res.length;$k++){
                                    //alert($scope.promo_avail_res[$k]);
                                    
                                    if(jQuery.isEmptyObject(res[$k])){
                                        //alert($scope.products_order[$k].promotion_available+"promo");
                                        if($scope.products_order[$k].promotion_available==1){
                                        //promotion active in localStorage but in backend it is inactive or expired
                                            $scope.products_order[$k].promotion_available=0;
                                            //alert($scope.products_order[$k].promo_id_selected);
                                        }
                                        
                                    }else{
                                        //alert(67);
                                        //$scope.products_order[$k].promo_temp=1;
                                    }
                                }
                            }
                           
                        });
                }
            
            /* code copied from cart  */
            $scope.openCartDiv=function(){
                //show cart
                $(".secondary").show();
                $("#cart_section").removeClass("hide_cart");
                $("#cart_section").addClass("show_cart");
                $(".openclose").html('<i class="fa fa-close"></i>');
                $("#opencloseCart").addClass("secondary1");
                $("#opencloseCart").removeClass("secondary_corner");
            }
            $scope.opencloseCartDiv=function(){
                var st=$("#cart_section").hasClass("show_cart");
                if(st){
                    $(".secondary").hide();
                    $("#cart_section").removeClass("show_cart");
                    $("#cart_section").addClass("hide_cart");
                    $(".openclose").html('<i class="fa fa-eye" title="view cart"></i>');
                    $("#opencloseCart").addClass("secondary_corner");
                    $("#opencloseCart").removeClass("secondary1");
                }else{
                    $(".secondary").show();
                    $("#cart_section").removeClass("hide_cart");
                    $("#cart_section").addClass("show_cart");
                    $(".openclose").html('<i class="fa fa-close"></i>');
                    $("#opencloseCart").addClass("secondary1");
                    $("#opencloseCart").removeClass("secondary_corner");
                }
            }
/////////// check current page is details or products starts /////////////////

$scope.is_current_page_products_or_detail="no";
			$scope.current_url="<?php echo $_SERVER['REQUEST_URI'];?>";
			if($scope.current_url.includes("/catalog/") || $scope.current_url.includes("/item/")){
				$scope.is_current_page_products_or_detail="yes";
				if($("#cart_section").hasClass("show_cart")){
					$("#cart_section").removeClass("show_cart")
				}
				if($("#opencloseCart").hasClass("secondary1")){
					$("#opencloseCart").removeClass("secondary1")
				}
				if(!$("#cart_section").hasClass("hide_cart")){
					$("#cart_section").addClass("hide_cart")
				}
				if(!$("#opencloseCart").hasClass("secondary_corner")){
					$("#opencloseCart").addClass("secondary_corner")
				}
				
				$timeout(function() {
					angular.element('#opencloseCart').triggerHandler('click');
				  });
				  
				//$scope.opencloseCartDiv();
			}
			else{
				$scope.is_current_page_products_or_detail="no";
			}
			//alert($scope.is_current_page_products_or_detail)
			
			
			
/////////// check current page is details or products ends /////////////////
            $scope.getRightSideCart();
        }

       
    </script>
    <!--- right side cart --->

    <!--- right side cart --->
    <div ng-if="order_product_count>0" ng-show="is_current_page_products_or_detail=='yes'" class="hidden-xs">
        <div class="secondary_corner" id="opencloseCart" ng-click="opencloseCartDiv()"><div id="cart_btn" ><span class="openclose"><i class="fa fa-eye fa-1x" title="view cart" ></i></span></div></div>
        <div class="secondary hide_cart" id="cart_section" style="display: none;" >

            <div class="row secondary_header">
                <small>
                <div class="col-md-12"> <a href="<?php echo base_url()?>cart"><i class="fa fa-shopping-cart fa-2x gotocartfun" style="color:#fff;"></i></a></div>
                    <div class="col-md-12"> Total Price</div>
                    <div class="col-md-12"> <?php echo curr_sym; ?>{{products_order_right_total}} </div>
                </small>
            </div>
            <div class="row secondary_cart_cell" ng-repeat="(key, value) in products_order_right track by $index">
                <div class="col-md-12 secondary-cell-full" id="sec_{{$index}}">
                    <div class="col-md-12 text-center">
                        <a href="<?php echo base_url()?>detail/<?php echo $controller->sample_code(); ?>{{value.inventory_id}}"><img ng-src="<?php echo base_url();?>{{value.inventory_image}}" alt="Product"></a>
                        <label><small>{{value.inventory_moq}} qty</small></label>
                       <!-- <pre>{{products_order_right|json}}</pre>--->
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--inside cartcontroller --->
    <!--- added --->

</div>
    <?php
}
?>


<!-- cart block section -->

<!---whatsapp icon
<a href="#" class="hidden-xs">
<i class="fa fa-whatsapp whatsapp_class"></i>
</a>
whatsapp icon--->

</div>
</div>
</div>
<div id="nav-top-menu" class="nav-top-menu">
<div class="container-fluid">
<div class="row">
<div id="main-menu" class="col-sm-12 main-menu">
<nav class="navbar navbar-default">
<div class="container-fluid">
<div class="navbar-header">
<!--<a class="navbar-brand pull-right mr-15 navbar-toggle collapsed" data-toggle="slide-collapse" data-target="#demo" aria-expanded="false" aria-controls="navbar" onclick="filterFun()">
<i class="fa fa-filter"></i> Filter</a>-->
<?php if($this->session->userdata("customer_name")) { ?>
<a class="navbar-brand pull-right mr-15 navbar-toggle collapsed" style="line-height: 30px;" data-toggle="collapse" data-target="#profile" aria-expanded="false" aria-controls="navbar"><span><?php echo mb_strimwidth($this->session->userdata("customer_name"), 0, 15, "..."); ?></span> <span class="caret"></span></a>
<?php 
}
else{
?>
<a class="navbar-brand pull-right mr-15 navbar-toggle collapsed" style="line-height: 30px;" href="<?php echo base_url();?>login" style="border-right:none;margin-left:5px;">Login</a>	
<?php
}
?>
<button type="button" class="pull-left ml-15 navbar-toggle collapsed" id="sidebarCollapse1" style="line-height: 30px;">
<i class="fa fa-align-left"></i>
</button>
</div>
<div class="visible-xs">
<div id="profile" class="navbar-collapse collapse">
<ul class="nav navbar-nav">
<?php 


if($this->session->userdata("user_type")=='franchise_customer'){

?>
<li><a href="<?php echo base_url();?>Account/franchise_account">My Account</a></li>
<li><a href="<?php echo base_url();?>Account/franchise_projects">Projects</a></li>
<?php 

}else{

 ?>

<li><a href="<?php echo base_url();?>Account">My Account</a></li>
<li><a href="<?php echo base_url();?>Account/my_order">Orders</a></li>
<li><a href="<?php echo base_url();?>Account/wallet">Wallet</a></li>
<li><a href="<?php echo base_url();?>Account/wishlist">Wishlists</a></li>
<li><a href="<?php echo base_url();?>Account/reviews">Reviews &amp; Ratings</a></li>

<?php 
} 
?>

<li><a href="javascript:logoutFun();">Logout</a></li>
</ul>
</div>
</div>

<div id="navbar" class="navbar-collapse collapse">
<ul class="nav navbar-nav">
<li class="icon_list"><a id="sidebarCollapse2" style="cursor:pointer;background:none;"><i class="fa fa-align-left"></i></a></li>
<?php
$p_category_count=0;
$id=$page_id;
$type=$page_type;
$name=$page_name;
$active_class='';
foreach ($p_category as $p_menu_value) {
if($p_category_count==6){
	break;
}
$p_category_count++;
$rand_1=$controller->sample_code();

$menu_name=$p_menu_value['parent_menu'];
$menu_type=$p_menu_value['type'];

if($p_category_count==1 && $id=='' && $type=='' && $name==''){
//$name=$p_menu_value['parent_menu'];
$id=$p_menu_value['id'];
$type=$p_menu_value['type'];
}

if($menu_type=="parent_cat"){
$pcat_id=$p_menu_value['id'];			
$show_sub=$p_menu_value['show_sub_menu'];
if($show_sub==0){
?>
<li><a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pcat_id}"; ?>"><?php echo $menu_name;?></a></li>
<?php
}else{
?>

<?php

/**************** Disease Care Hardcode starts ***/
/*
if($p_category_count==1){
	?>
	
	<li class="dropdown">
<a class="dropdown-toggle" href="" data-toggle="dropdown">Disease Care</a>
<?php
$cat=$controller->provide_category($pcat_id);
$len=count($cat);
if ($len!=0){
	$l=round(12/$len);
}
else { $l=0;}
?>
<!-- style="width: <?php echo ($len*280)?>px;" -->
<ul class="mega_dropdown dropdown-menu" >
<?php
$active_class='';
?>
<li class="block-container col-sm-12 <?php echo $active_class;?>">
	<ul class="block">
	<li class="link_container"><a href="<?php echo base_url()?>diseasecare/migraine">migraine</a></li>
		<li class="link_container"><a href="<?php echo base_url()?>diseasecare/osteoarthritis">osteoarthritis</a></li>
		<li class="link_container"><a href="<?php echo base_url()?>diseasecare/rheumatoidarthritis">rheumatoid arthritis</a></li>
		
		
		<li class="link_container"><a href="<?php echo base_url()?>diseasecare/jointpain">joint pain</a></li>
		
		<li class="link_container"><a href="<?php echo base_url()?>diseasecare/ibs">IBS</a></li>
		<li class="link_container"><a href="<?php echo base_url()?>diseasecare/gord">GERD</a></li>
		<li class="link_container"><a href="<?php echo base_url()?>diseasecare/obesity">obesity</a></li>
		<li class="link_container"><a href="<?php echo base_url()?>diseasecare/constipation">constipation</a></li>
	</ul>
</li>				
</ul>
</li>
	
	<?php
}
*/
/**************** Disease Care Hardcode ends ***/

?>

<li class="dropdown">
<a class="dropdown-toggle" href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pcat_id}"; ?>" data-toggle="dropdown"><?php echo $menu_name;?></a>
<?php
$cat=$controller->provide_category($pcat_id);
$len=count($cat);
if ($len!=0){
	$l=round(12/$len);
}
else { $l=0;}

$numOfCols = 6;
$rowCount = 0;
$bootstrapColWidth = 12 / $numOfCols;

?>
<ul class="mega_dropdown dropdown-menu  pt-0" style="width: <?php echo ($len*280)?>px;">
<span class="row">
<?php
foreach($cat as $cat_value){
$rand_2=$controller->sample_code();
$cat_id=$cat_value->cat_id;
$cat_name=$cat_value->cat_name;
if(strtolower($name)==strtolower($cat_name)){
$active_class='active';
}else{
$active_class='';
}
?>

<li class="block-container <?php echo $active_class;?>" style="float:left;">
<ul class="block">
<li class="link_container group_header">
<a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pcat_id}"; ?>/<?php echo "{$rand_2}{$cat_id}"; ?>"><?php echo $cat_name;?></a>
</li>

<?php
$sub_cat=$controller->provide_sub_category($cat_id);
foreach($sub_cat as $sub_cat_value) {

$rand_3=$controller->sample_code();

$subcat_id=$sub_cat_value["subcat_id"];
$subcat_value=$sub_cat_value["subcat_name"];

?>
<li class="link_container"><a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pcat_id}"; ?>/<?php echo "{$rand_2}{$cat_id}"; ?>/<?php echo "{$rand_3}{$subcat_id}"; ?>" style="text-transform:capitalize;"><?php echo $subcat_value;?></a></li>
<?php
}	
?>
</span>	
</ul></li>
<?php

}
?>				
</ul>
</li>
<?php
}
}
if($menu_type=="cat"){
$rand_2=$controller->sample_code();

$cat_id=$p_menu_value['id'];
$cat_name=$p_menu_value['parent_menu'];
if(isset($p_menu_value['pcat_id'])){
$pcat_id=$p_menu_value['pcat_id'];
}
else{
$pcat_id=0;
}
if(strtolower($name)==strtolower($cat_name)){
$active_class='active';
}else{
$active_class='';
}
?>
<li class="dropdown <?php echo $active_class;?>">
<a data-toggle="dropdown" href="<?php echo base_url(); ?>search_category/<?php echo "{$rand_1}{$pcat_id}"; ?>/<?php echo "{$rand_2}{$cat_id}"; ?>" ><?php echo $cat_name;?></a>
<ul class="dropdown-menu container-fluid" role="menu">
<li class="block-container">
<ul class="block">
<?php
$sub_cat=$controller->provide_sub_category($cat_id);
foreach($sub_cat as $sub_cat_value) {

$rand_3=$controller->sample_code();

$subcat_id=$sub_cat_value["subcat_id"];
$subcat_value=$sub_cat_value["subcat_name"];

?>
<li class="link_container"><a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pcat_id}"; ?>/<?php echo "{$rand_2}{$cat_id}"; ?>/<?php echo "{$rand_3}{$subcat_id}"; ?>"><?php echo $subcat_value;?></a></li>
<?php
}	
?>
</ul>
</li>
</ul>
</li>
<?php
}	
}
?>
    <!--<li style="cursor:pointer;"><a ><i class="fa fa-shopping-cart"></i> </a></li>--->
<?php 
$adm_settings=get_admin_combo_settings();
if(!empty($adm_settings)){

    $combo_pack_status=$adm_settings->adm_combo_pack_status;
    if($combo_pack_status=='active'){

?>
<li class="">
    <a class="" href="<?php echo base_url()."catalog_combo";?>" >Quick Buy</a>
    
</li>
<!---<li class="">
    <a class="" href="<?php //echo base_url()."becomeourfranchise";?>" >Become Our Franchise</a>
    
</li>
<li class="">
    <a class="" href="<?php //echo base_url()."catalog_requestforquotation";?>" >Request for Quotation</a>
    
</li>--->

<?php 
    }
}
?>
</ul>
</div>
</div>
</nav>
</div>
</div>
</div>
</div>
</div>


<script type="text/javascript">
var module = angular.module('app', ['ui.bootstrap', 'angular-responsive']);
module.factory('Scopes', function ($rootScope) {
var mem = {};
return {
store: function (key, value) {
$rootScope.$emit('scope.stored', key);
mem[key] = value;
},
get: function (key) {
return mem[key];
}
};
});
$('.notification').on('click', function (e) {
$('#badge').hide();

});

$(document).ready(function(){

    $('#notify_login_btn').on('click', function(){
    var url = '<?php echo base_url()?>';
    window.location = url + "login";
    });

});


$('body').on('click', function (e) {
$('[data-toggle=popover]').each(function () {
if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
$(this).popover('hide');
}
});
});

function clear_notifications(){

$.ajax({
url:'<?php echo base_url(); ?>Account/clear_notifications',
type:"POST",
data:'1=2',
success:function(data){
//alert(data);
//return false;
//location.href="<?php echo base_url();?>Account/order_details/"+order_item_id;

}

});

}
$(document).ready(function(){
$(".notificationLink").click(function(){
$("#notification_wrapper").fadeToggle(300);
//$("#badge").fadeOut("slow");
$('.noti_Counter').fadeOut('slow');  
clear_notifications();
return false;
});
});
$(document).click(function(){
$("#notification_wrapper").hide();
});
function UseSuggestKeyWord(obj){
document.getElementById('search_keywords').value=""
document.getElementById('search_keywords').value=obj.innerText;
document.getElementById('level_type').value=obj.getAttribute('level_type');
document.getElementById('level_value').value=obj.getAttribute('level_value');
document.getElementById('level_name').value=obj.getAttribute('level_name');
removeSugessation();
//	document.getElementById("searchForm_org").submit();
document.getElementById("search_keyword_btn").click();
//alert(11111111111);
}

function removeSugessation(){
document.getElementById('dropssss').classList.remove('open');
document.getElementById('search_listss').innerHTML=""
//document.getElementById('search_keywords').value=""
}

function doSearch(){
var search_cat=document.getElementById('search_cat').value;
var search_keyword=document.getElementById('search_keywords').value;
document.getElementById('search_listss').innerHTML=""

var xhr = false;
if (window.XMLHttpRequest) {
xhr = new XMLHttpRequest();
}
else {
xhr = new ActiveXObject("Microsoft.XMLHTTP");
}
var xhr = new XMLHttpRequest();
if (xhr) {
xhr.onreadystatechange = function () {
if (xhr.readyState == 4 && xhr.status == 200) {
if(xhr.responseText.length>2){

var dats=JSON.parse(xhr.responseText);
var str="";
/*str+='<li class="text-right"><a  href="javascript:removeSugessation(this);"> <i class="fa fa-times"></i></a></li>'*/
if(dats['exact']){
for(var i=0;i<dats['exact'].length;i++){
var keys = Object.keys(dats['exact'][i]); 
var attrs=keys[1];
var attrsk=keys[2];
var att0='level_type="'+attrsk+'"'

var att2=""

if(attrs=="sku_name"){
att2+='level_name="'+dats['exact'][i].sku_name+'"'
}

if(attrs=="product_name"){
att2+='level_name="'+dats['exact'][i].product_name+'"'
}
if(attrs=="brand_name"){
att2+='level_name="'+dats['exact'][i].brand_name+'"'
}
if(attrs=="subcat_name"){
att2+='level_name="'+dats['exact'][i].subcat_name+'"'
} 
var att1=""
if(attrsk=="inventory_id"){
att1+='level_value="'+dats['exact'][i].inventory_id+'"'
}
if(attrsk=="product_id"){
att1+='level_value="'+dats['exact'][i].product_id+'"'
}
if(attrsk=="brand_id"){
att1+='level_value="'+dats['exact'][i].brand_id+'"'
}
if(attrsk=="subcat_id"){
att1+='level_value="'+dats['exact'][i].subcat_id+'"'
} 
if(i==0){
	str+="<ul id='elastic_search_display_list'>";
}
str+='<li onclick="UseSuggestKeyWord(this)" '+att0+' '+att1+' '+att2+' ><a  href="javascript:void(0)"> '+dats['exact'][i].label+'</a></li>'
}
}
if(str!=""){
	str+="</ul>";
}
if(str==""){
	document.getElementById('search_listss').style.display="none";
}
else{
	document.getElementById('search_listss').style.display="";
	document.getElementById('search_listss').innerHTML=str
}
document.getElementById('dropssss').classList.add('open');
}else{
document.getElementById('dropssss').classList.remove('open');
}

}
}
xhr.open('POST', "<?php echo base_url()?>getSearchSugessations", true);
xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
xhr.send('search_cat='+search_cat+"&search_keyword="+search_keyword);
}
return false; 
}
function searchFun(current_controller){
var search_keyword=document.getElementById("search_keywords").value;
localStorage.search_keywords=search_keyword;
if(search_keyword.trim()==""){
location.reload();
return false;
}
var level_name=document.getElementById("level_name").value
if(level_name.trim()==""){
	if(document.getElementById("elastic_search_display_list")!==null){
		$('ul#elastic_search_display_list li:first').click();
	}
	else{
		alert('Searched Item does not exist');
		document.getElementById("search_keywords").value='';
		return false;
	}
}
if(current_controller=="search"){

var search_cat=document.getElementById("search_cat").value;
var search_keyword=document.getElementById("search_keywords").value;
document.getElementById("search_keyword_index_page").value=search_keyword;

if(search_cat==""){
action_url="<?php echo base_url()?>search_category/a8i5t9l5g0";
}
else{

search_cat_arr=search_cat.split("_");

if(search_cat_arr[1]=="pcat"){
action_url="<?php echo base_url()?>search_category/a8i5t9l5g"+search_cat_arr[0];
}
else{
action_url="<?php echo base_url()?>search_category/a8i5t9l5g"+search_cat_arr[0]+"/adomyhrxh"+search_cat_arr[1];
}
}
localStorage.action_url=action_url;
document.getElementById("searchForm_org").action=action_url;
document.getElementById("searchForm_org").submit();

}
else{

var search_cat=document.getElementById("search_cat").value;
var search_keyword=document.getElementById("search_keywords").value;
//var search_cat=document.getElementById("search_cat").value;
if(search_cat==""){
action_url="<?php echo base_url()?>search_category/a8i5t9l5g0";
}
else{
search_cat_arr=search_cat.split("_");
if(search_cat_arr[1]=="pcat"){
action_url="<?php echo base_url()?>search_category/a8i5t9l5g"+search_cat_arr[0];
}
else{
action_url="<?php echo base_url()?>search_category/a8i5t9l5g"+search_cat_arr[0]+"/adomyhrxh"+search_cat_arr[1];
}
}
localStorage.action_url=action_url;
document.getElementById("searchForm_org").action=action_url;
//document.searchForm_org.submit();


//if(search_keyword!=""){
$("input[name='search_keyword']").val(search_keyword);
//$("input[name='search_cat']").val(search_cat);
//
$("input[name='price_filter']").val("");
$("input[name='cat_filter']").val("");
$("input[name='brand_filter']").val("");
//
document.getElementById("searchForm_org").submit();
//}
}
}
$(document).ready(function(){
$(".ui-autocomplete").click(function(){

//alert($( "#search_keyword" ).val());
$("#search_keyword_btn").trigger("click");
})
})

$('body').on('click', 'a.disabled', function(event){
event.preventDefault();
});

if(localStorage.action_url===undefined || localStorage.action_url==null){

}
else{
current_page_url="<?php echo $_SERVER['REQUEST_URI']; ?>";
localstge_action_url=localStorage.action_url;
if(localstge_action_url.indexOf(current_page_url) !== -1){
if(localStorage.search_keywords===undefined || localStorage.search_keywords==null){

}
else{
//document.getElementById("search_keywords").value=localStorage.search_keywords; # this is commented because of jarvinai
}
}
else{
localStorage.removeItem("search_keywords");
localStorage.removeItem("action_url");
}
}
</script>
<script>
if(localStorage.getItem("checkingpincode_notshowloader")){
$("#loading").css({"display":"none"});
localStorage.removeItem("checkingpincode_notshowloader")
}
$(function(){
// mobile menu slide from the left
$('[data-toggle="slide-collapse"]').on('click', function() {
$navMenuCont = $($(this).data('target'));
$navMenuCont.animate({'width':'toggle'}, 300);
});
})
function filterFun(){
$("#filterselection_div").css({"display":"block"});
}
</script>


<script>
	/** jarvin coding starts */
	function switchToElasticSearchFun(){
		document.getElementById("searchForm_org").style.display="block";
		document.getElementById("searchForm_org_jarvinai").style.display="none";
		
		document.getElementById("switch_to_jarvinai_btn").style.display="block";
		document.getElementById("switch_to_elastic_search_btn").style.display="none";
		
		document.getElementById("search_keywords_jarvinai").value="";
	}
	function switchToJarvinAIFun(){
		document.getElementById("searchForm_org").style.display="none";
		document.getElementById("searchForm_org_jarvinai").style.display="block";
		
		document.getElementById("switch_to_jarvinai_btn").style.display="none";
		document.getElementById("switch_to_elastic_search_btn").style.display="block";
		
		document.getElementById("search_keywords_jarvinai").value="";
	}
	function doJarvinAISearch(){
		user_id="<?php echo $user_id;?>";
		base_url="<?php echo base_url();?>";
		if(base_url.indexOf("localhost") !== -1){
			url="http://localhost:8000/";
		}
		else{
			url="http://13.233.122.84:8000/";
		}
		question_to_jarvin=document.getElementById("search_keywords_jarvinai").value.trim();
		if(question_to_jarvin!=""){
			payload={"company_code":"voometstudio","jarv_or_user":"","logfile_path":"","previous_user_timestamp":"","question":question_to_jarvin,"user_id":user_id}
			$.ajax({
			  url: url,
			  type: "POST",
			  dataType: "json",
			  data: JSON.stringify({payload: payload,}),
			  headers: {
				"X-Requested-With": "XMLHttpRequest",
				"X-CSRFToken": getCookie("csrftoken"),  // don't forget to include the 'getCookie' function
			  },
			  success: (data) => {
				skuid_list=data["skuid_list"];
				number_of_skuids_in_result=data["number_of_skuids_in_result"];
				skuid_list=skuid_list.trim();
				//alert(skuid_list)
				if(number_of_skuids_in_result=="none"){
					alert("Sorry! No matching search results found.")
				}
				else if(number_of_skuids_in_result==1){
					get_inventory_id_by_sku_id(skuid_list);
				}
				else if(number_of_skuids_in_result>1){
					get_pcat_cat_subcat_id_by_sku_id_json(skuid_list);
				}
				
				
			  },
			  error: (error) => {
				console.log(error);
			  }
			});
		}
		else{
			alert("Please enter your question");
		}
	}
	function get_inventory_id_by_sku_id(sku_id){
		$.ajax({
		  url: "<?php echo base_url()?>get_inventory_id_by_sku_id/"+sku_id,
		  type: "POST",
		  data: "1=2",
		  dataType:"json",
		  success: (data) => {
		    if(data["response_from_fun"]!="none"){
				//alert(data["inventory_id"])
				location.href="<?php echo base_url()?>detail/"+data["inventory_id"]+"/jarvinaioutput";
			}
			else{
				alert("Sorry! No matching search results found.")
			}
		  },
		  error: (error) => {
			console.log(error);
		  }
		});
	}
	function get_pcat_cat_subcat_id_by_sku_id_json(skuid_list){
		skuid_list_arr=skuid_list.split(",");
		skuid_list_seperated=skuid_list.replaceAll(",","axdx");
		first_sku=skuid_list_arr[0];
		$.ajax({
		  url: "<?php echo base_url()?>get_pcat_cat_subcat_id_by_sku_id_json",
		  type: "POST",
		  data: "skuid_list="+skuid_list_seperated,
		  dataType:"json",
		  success: (data) => {
		    if(data["response_from_fun"]!="none"){
				inventory_id_list_seperated=data["inventory_id_seperated"];
				//alert(inventory_id_list_seperated);
				location.href="<?php echo base_url()?>search_category/123123123"+data["pcat_id"]+"/123123123"+data["cat_id"]+"/123123123"+data["subcat_id"]+"/"+inventory_id_list_seperated;
			}
			else{
				alert("Sorry! No matching search results found.")
			}
		  },
		  error: (error) => {
			console.log(error);
		  }
		});
	}
	
	function triggerEnterKey(evt){
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode == 13){
		    doJarvinAISearch();
		}
		
	}
	
	
	function getCookie(name) {
	  let cookieValue = null;
	  if (document.cookie && document.cookie !== "") {
		const cookies = document.cookie.split(";");
		for (let i = 0; i < cookies.length; i++) {
		  const cookie = cookies[i].trim();
		  // Does this cookie string begin with the name we want?
		  if (cookie.substring(0, name.length + 1) === (name + "=")) {
			cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
			break;
		  }
		}
	  }
	  return cookieValue;
	}
	/** jarvin coding ends */
</script>

