<?php
	$email_stuff_arr=email_stuff();
?>
<style>
 .center_column  a {
    color: #124b9b;
}  
</style>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
  
        <!-- row -->
        <div class="row">
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading text-center">
                    <span class="page-heading-title2">Intellectual Property</span>
                </h2>
                <!-- Content page -->
                <div class="content-text clearfix"> 
					<p>
						<?php echo SITE_ADDR; ?> a fully owned brand of Magnum Label is committed to completely avoiding infringing products from the site but will take immediate action to remove the same if in case of any unavoidable circumstance. It is in <?php echo SITE_ADDR; ?> interest to ensure that infringing products are removed from the site, as they erode Buyer and Seller trust and works against the company’s business etiquettes’.
					</p>
					
					<h4>Policy</h4>
					
					<p class="mt-10">
					<?php echo SITE_ADDR; ?> respects third party Intellectual Property rights and actively supports protection of all third party Intellectual Property including Copyrights and Trademarks (“IP”). It is our policy to expeditiously respond to clear notices of alleged IP infringement.
					</p>
					
					<p class="mt-10">
						If we receive proper notification of IP infringement, our response to such notices will include removing or disabling access to material claimed to be the subject of infringing activity. For a detailed term of use of our website, please visit  <a href="https://www.<?php echo SITE_ADDR; ?>/tandc">Terms and Conditions</a>
					</p>
					
					
					<h4 class="mt-10">How to report a listing</h4>
					<p class="mt-10">
						If you have a good faith belief that your IP right has been infringed by any of the products listed on <a href="<?php echo SITE_HREF; ?>"><?php echo SITE_ADDR; ?></a>, you may follow the below process: We require that the Intellectual Property right owner or authorized agent provide the following details and email it to <a href="mailto:<?php echo SITE_EMAIL_ENQ; ?> "><?php echo SITE_EMAIL_ENQ; ?> </a> The email should contain the below information.
					</p>
					
					
					<ol class="mt-10"  style="list-style-type: decimal;list-style-position:inside;">
						<li>
							Identification or description of the copyrighted work/trademark that has been infringed.
						</li>
						<li class="mt-10">
							Clear identification or description of where the material that you claim is infringing is located on <?php echo SITE_ADDR; ?> with adequate particulars; Product ID / website links of infringing products (in case of copyright infringement). 
							<ul>
							<li>
						(Note: <?php echo SITE_ADDR; ?> is unable to process requests which do not specify exact product IDs or URLs. Please do not provide links to browse pages or links of search queries as these pages are dynamic and their contents change with time)
						</li>
						</ul>
						</li>
						<li class="mt-10">
						Your address, telephone number, and email address.
						</li>
						<li class="mt-10">
							A statement by you that you have a good faith belief that the use of the material complained of is not authorized by the copyright or intellectual property owner, its agent, or the law.
						</li>
						<li class="mt-10">
							A statement by you, that the information in your notice is accurate and that you are the copyright or intellectual property owner or authorized to act on the copyright or intellectual property owner's behalf.
						</li>
						<li class="mt-10">
							Brand Name (in case of Trademark infringement)
						</li>
						<li class="mt-10">
							Details of the intellectual property being infringed (Provide copyrighted images or trademark certificates as attachments)
						</li>
					</ol>
					
					<h4 class="mt-20">Contact Us</h4>
					<p class="mt-10">
						Please contact us at  <a href="mailto:<?php echo SITE_EMAIL_ENQ; ?>
"><?php echo SITE_EMAIL_ENQ; ?></a> for any questions or comments (including all inquiries unrelated to copyright infringement) regarding this Website.
					</p>
					<p> or write to us at:</p>
					<ul class="mt-10">
						<li><?php echo SITE_ADDR; ?>, </li>
						<li>Fully owned by </li>
						<li><strong><?php echo SITE_ADDR; ?>,</strong> </li>
						<li>No. 988, Ground Floor,1st Cross,</li>
						<li>HAL 2nd Stage, Indira Nagar</li>
						<li>Bangalore – 560 008 </li>
					</ul>
					
					<ul class="mt-20">
						<li>Phone /Whatsapp:<a href="tel:+918050504950"> +91 – 80505 04950 </a></li>
						<li>Email: <a href="mailto:<?php echo SITE_EMAIL_ENQ; ?>"><?php echo SITE_EMAIL_ENQ; ?></a> </li>
						<li>Work Hours: Mon – sun (10:00am – 08:30pm) | Please check with us on public / company list of holidays </li>
					</ul>
					
                </div>
                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<!-- ./page wapper-->
