<?php
	/******/
	// jarvinai code for jarvin output skus starts
	if(isset($jarvinai_inventory_id_list)){
		if($jarvinai_inventory_id_list!=""){
			$jarvinai_inventory_id_arr=explode("axdx",$jarvinai_inventory_id_list);
			//print_r($jarvinai_inventory_id_arr);
		}
		else{
			$jarvinai_inventory_id_arr=array();
		}
	}
	else{
		$jarvinai_inventory_id_arr=array();
	}
	// jarvinai code for jarvin output skus starts
	/*****/
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/skins/jquery-ui-like/progressbar.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/product_comparision/style.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/product_comparision/w3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/Ion.RangeSlider/Ion.RangeSlider.css"/>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/Ion.RangeSlider/Ion.RangeSlider.js"/></script>
<style type="text/css">

    @media only screen and (max-width: 600px) {
        .heightmatched {
            font-size: 1em !important;
        }
    }
    @media (max-width: 480px) {
        .product-list.grid li {
            padding-right: 5px;
            padding-left: 5px;
        }
    }

    .product-list li .price-percent-reduction2 {
        line-height: 33px;
    }
    .product-list li .price-percent-reduction_offers {
        line-height: 17px;
    }

    .checkbox label:after {
        content: '';
        display: table;
        clear: both;
    }

    .checkbox .cr {
        position: relative;
        display: inline-block;
        border: 1px solid #a9a9a9;
        border-radius: .25em;
        width: 1.3em;
        height: 1.3em;
        float: left;
        margin-right: .5em;
    }


    .checkbox .cr .cr-icon {
        position: absolute;
        font-size: .8em;
        line-height: 0;
        top: 50%;
        left: 20%;
    }

    .radio .cr .cr-icon {
        margin-left: 0.04em;
    }

    .checkbox label {
        padding-left: 0px;
        line-height: 1.3;
    }

    .checkbox label input[type="checkbox"] {
        display: none;
    }

    .checkbox label input[type="checkbox"] + .cr > .cr-icon,
    .radio label input[type="radio"] + .cr > .cr-icon {
        transform: scale(3) rotateZ(-20deg);
        opacity: 0;
        transition: all .3s ease-in;
    }

    .checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
    .radio label input[type="radio"]:checked + .cr > .cr-icon {
        transform: scale(1) rotateZ(0deg);
        opacity: 1;
    }

    .checkbox label input[type="checkbox"]:disabled + .cr,
    .radio label input[type="radio"]:disabled + .cr {
        opacity: .5;
    }

    .product-list li .product-star {
        width: auto;
        float: left;
        color: #eda900;
        text-align: center;
        display: inline-block;
        padding-top: 12px;
        font-size: 0.9em;
    }

    .compare_close {
        margin-top: -8px;
        background-color: #a39999;
        border-radius: 11px;
        width: 18px;
        height: 18px;
        line-height: 17px;
        text-align: center;
        color: #f9ecec;
        text-decoration: none;
        float: right;
        font-size: 18px;
    }

    .compare_lineheight {
        line-height: 80px;
    }

    .comparePanle {
        width: 100%;
        background-color: #fff;
        border-top: 1px solid #eee;
        bottom: 0px;
        padding: 10px;
    }

    .horizontal_bar {
        width: 60%;
        float: left;
        margin-bottom: 10px;
        color: #ff9900;
    }

    .star_rate_1 {
        float: left;
        width: 100%;
    }

    .star_rate_2 {
        float: left;
        width: 30%;
    }

    .star_rate_3 {
        float: left;
        width: 10%;
    }

    .star_rate {
        width: 30%;
        float: left;
        margin-right: 6px;
    }

    .popover {
        width: 700px;
    }

    div.rater {
        width: 200px;
        min-height: 40px;
        background: black;
        color: white;
        padding: 5px;
        position: relative;
        word-wrap: break-word;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        border-radius: 5px;
        margin-bottom: 2em;
        margin-top: 8px;
    }

    div.rater:after {
        content: '';
        display: block;
        position: absolute;
        top: -20px;
        left: 30px;
        width: 0;
        height: 0;
        border-color: transparent transparent black transparent;
        border-style: solid;
        border-width: 10px;
    }

    .img-cat-display {
        width: 300px;
        height: 366px;
    }

    /*.display-sortP-option{
position: absolute;
top: 0;
right: 63px;
min-width: 200px;
height: 30px;
margin-top: 0px;
margin-right: 5px;
}
#top-sortPagi .sortPagiBar .sort-product,#top-sortPagi .sortPagiBar .show-product-item {
margin-left: 21px;
}
@media (max-width: 480px) {
.display-sortP-option{
position: relative;
top: 0;
right: 0px;
min-width: 200px;
height: 30px;
margin-top: -10px;
margin-right: 0px;
margin-bottom: 10px;
}
}*/
    #WarningModal {
        z-index: 9999;
    }

    .line-break {
        margin-top: 10px;
        margin-bottom: 0;
    }

    body {
        background-color: #f1f3f6;
    }

    .breadcrumb {
        padding: 15px 0 0 15px;
    }

    @media (min-width: 768px) {
        .center_column {
            padding-left: 0;
        }

    }

    .list-inline li {
        line-height: 24px;
    }

    .list-inline li a {
        cursor: pointer;
    }

    .list-inline li a.active {
        font-weight: 600;
        color: #eda900;
		border-bottom: 2px solid #eda900;
		padding-bottom: 4px;
    }

    .product-name a {
        color: #000;
    }

    .product-name a:hover {
        color: #777;
    }

    .attribute-col {
        font-size: 0.9em;
        color: #777;
        margin-bottom: 0.2em;
    }

    .content_price {
        padding-top: 5px;
    }

    .clearallfilterbtn {
        font-size: 12px;
        color: #0000ff;
        margin-top: 2px;
        text-transform: capitalize;
        margin-right: 0.5rem;
    }

    .layered .layered-content {
        border-bottom: 0px;
    }
	.add_to_compare_label{
		font-size: 1.3rem;
		margin-top: 0.3rem;
		display: inline-block;
	}
</style>

<script>
    base_url = "<?php echo base_url();?>";
</script>
<script>
    $(document).ready(function () {
        $(".star-rating").find("a").each(function () {
            $(this).html("");
        })
//	$(".jquery-ui-like").find(".hbar").each(function(){
//		$(this).html("");
//	})

    })
</script>

<?php
require_once 'assets/elastic/app/init_customer.php';

$veryfirst_condition_of_all_filters = "yes";

$filter_box_infoRes = $controller->get_filter_box_info($cat_current, $cat_current_id, $category_tree);
//$filter_box_infoRes="";

if (isset($cat_current)) {

    if ($cat_current == "subcat_id") {
        
		
		
		//$params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' =>

			//	['query' => ['filtered' => ['query' => ['match' => ['subcat_name' => $searched_key]], 'filter' => ['bool' => ['must' => [['term' => ['pcat_id' => $pcat_id]], ['term' => ['cat_id' => $cat_id]], ['term' => ['subcat_id' => $subcat_id]]]]]]]]];
			
		$params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' =>
            [
                'query' => [
                    'bool' => [
                        
                        "must"=> [
                            ["match"=> [
                              "subcat_name"=> $searched_key
                            ]]
                        ],
                        'filter' => [
							['term' => [
                                'pcat_id' => $pcat_id,
                            ]],
                            ['term' => [
                                'cat_id' => $cat_id,
                            ]],
                            ['term' => [
                                'subcat_id' => $subcat_id,
                            ]],
                        ],
                    ],
                ],
            ],
        ];
			
			
    }
    if ($cat_current == "cat_id") {
       
		
		//$params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' =>

				//['query' => ['filtered' => ['query' => ['match' => ['cat_name' => $searched_key]], 'filter' => ['bool' => ['must' => [['term' => ['pcat_id' => $pcat_id]], ['term' => ['cat_id' => $cat_id]]]]]]]]];
		 $params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' =>
            [
                'query' => [
                    'bool' => [
                        
                        "must"=> [
                            ["match"=> [
                              "cat_name"=> $searched_key
                            ]]
                        ],
                        'filter' => [
                            ['term' => [
                                'pcat_id' => $pcat_id,
                            ]],
                            ['term' => [
                                'cat_id' => $cat_id,
                            ]]
                           
                        ],
                    ],
                ],
            ],
        ];
    }
    if ($cat_current == "pcat_id") {
        
		//$params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' =>

			//	['query' => ['filtered' => ['query' => ['match' => ['pcat_name' => $searched_key]], 'filter' => ['bool' => ['must' => [['term' => ['pcat_id' => $pcat_id]]]]]]]]];
		
		$params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' =>

            [
                'query' => [
                    'bool' => [
                        
                        "must"=> [
                            ["match"=> [
                              "pcat_name"=> $searched_key
                            ]]
                        ],
                        'filter' => [
                            ['term' => [
                                'pcat_id' => $pcat_id,
                            ]]
                           
                        ],
                    ],
                ],
            ],
        
        ];
		
    }
    if ($cat_current == "brand_id") {
        
		
		//$params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' =>

			//	['query' => ['filtered' => ['query' => ['match' => ['brand_name' => $searched_key]], 'filter' => ['bool' => ['must' => [['term' => ['pcat_id' => $pcat_id]], ['term' => ['cat_id' => $cat_id]], ['term' => ['subcat_id' => $subcat_id]], ['term' => ['brand_id' => $brand_id]]]]]]]]];
			
			
			
			 $params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' =>

            [
                'query' => [
                    'bool' => [
                        
                        "must"=> [
                            ["match"=> [
                              "brand_name"=> $searched_key
                            ]]
                        ],
                        'filter' => [
                            ['term' => [
                                'pcat_id' => $pcat_id,
                            ]],
                            ['term' => [
                                'cat_id' => $cat_id,
                            ]],
                            ['term' => [
                                'subcat_id' => $subcat_id,
                            ]],
							['term' => [
                                'brand_id' => $brand_id,
                            ]],
                        ],
                    ],
                ],
            ],
        
        ];
		
    }
    if ($cat_current == "product_id") {
        
			
			//$params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' =>

				//['query' => ['filtered' => ['query' => ['match' => ['product_name' => $searched_key]], 'filter' => ['bool' => ['must' => [['term' => ['pcat_id' => $pcat_id]],
					//['term' => ['cat_id' => $cat_id]], ['term' => ['subcat_id' => $subcat_id]], ['term' => ['brand_id' => $brand_id]], ['term' => ['product_id' => $product_id]]]]]]]]];
		
		 $params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' =>

                [
                    'query' => [
                        'bool' => [
                            "must"=> [
                                "match"=> [
                                  "product_name"=> $searched_key
                                ]
                            ],
                            'filter' => [
                                ['term' => [
                                    'pcat_id' => $pcat_id,
                                ]],
                                ['term' => [
                                    'cat_id' => $cat_id,
                                ]],
                                ['term' => [
                                    'subcat_id' => $subcat_id,
                                ]],
                                ['term' => [
                                    'brand_id' => $brand_id,
                                ]],
								['term' => [
                                    'product_id' => $product_id,
                                ]],
                            ],
                        ],
                    ],
                ],
            
            
            ];
			
    }
	if ($cat_current == "inventory_id") {
        
			
			//$params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' =>

				//['query' => ['filtered' => ['query' => ['match' => ['product_name' => $searched_key]], 'filter' => ['bool' => ['must' => [['term' => ['pcat_id' => $pcat_id]],
					//['term' => ['cat_id' => $cat_id]], ['term' => ['subcat_id' => $subcat_id]], ['term' => ['brand_id' => $brand_id]], ['term' => ['product_id' => $product_id]]]]]]]]];
		
		 $params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1', 'body' =>

                [
                    'query' => [
                        'bool' => [
                            "must"=> [
                                "match"=> [
                                  "product_name"=> $searched_key
                                ]
                            ],
                            'filter' => [
                                ['term' => [
                                    'pcat_id' => $pcat_id,
                                ]],
                                ['term' => [
                                    'cat_id' => $cat_id,
                                ]],
                                ['term' => [
                                    'subcat_id' => $subcat_id,
                                ]],
                                ['term' => [
                                    'brand_id' => $brand_id,
                                ]],
								['term' => [
                                    'product_id' => $product_id,
                                ]],
								['term' => [
                                    'inventory_id' => $inventory_id,
                                ]],
                            ],
                        ],
                    ],
                ],
            
            
            ];
			
    }
}
/////////////////////////////////////


foreach ($filter_box_infoRes as $filter_box_infoObj) {
    $filter_box_infoObj->filterbox_name = preg_replace('/\s+/', '_', $filter_box_infoObj->filterbox_name);
    if (isset($_REQUEST[$filter_box_infoObj->filterbox_name . '_filter']) && $_REQUEST[$filter_box_infoObj->filterbox_name . '_filter'] != "") {
        $veryfirst_condition_of_all_filters = "no";
        //$filter_box_infoObj->filterbox_name . '_filter';
		//echo $_REQUEST[$filter_box_infoObj->filterbox_name . '_filter'];
        $color_filter = $_REQUEST[$filter_box_infoObj->filterbox_name . '_filter'];
		//echo $color_filter;
		if($filter_box_infoObj->type==="checkbox"){
		
			$color_filter_arr = explode("|", $color_filter);
			
			$color_filter_param_arr = array();
			foreach ($color_filter_arr as $colorFilterName) {
				//$color_filter_param_arr[]["term"]["filter_" . $filter_box_infoObj->filterbox_name . "_id"] = $colorFilterName; // old
				$color_filter_param_arr[]["bool"]["must"][]["match"]["filter_" . $filter_box_infoObj->filterbox_name . "_id"] = $colorFilterName;
			}
			
			//$params["body"]["query"]["filtered"]["filter"]["bool"]["must"][]["bool"]["should"] = $color_filter_param_arr; // old
			$params["body"]["query"]["bool"]["must"][]["bool"]["should"] = $color_filter_param_arr;
			
			
			
			
		}
		
		
    }
}


/*

$params["body"]=["query"=> [
    "bool"=> [
        "must"=> [
            [
                "bool"=> [
                    "should" => [
                        [
                            "bool"=> [
                                "must"=> [
                                    [ "match" => [ "filter_Colour_id" => "1" ] ],
									[ "match" => [ "filter_Drying_Time_id" => "3" ] ]
                                ]
                            ]
                        ],
						[
                            "bool"=> [
                                "must"=> [
                                    [ "match" => [ "filter_Colour_id" => "2" ] ],
									[ "match" => [ "filter_Drying_Time_id" => "3" ] ]
                                ]
                            ]
                        ]
						
                    ]
                ]
            ],
            [
                'match' => [ "pcat_name"=> $searched_key ]
            ],
        ]
    ]
]];
*/

//echo "<pre>";
//print_r($params);
//echo "</pre>";
//exit;
$params["from"] = 0;
$params["size"] = 10000;

$query = $es->search($params);

if ($query['hits']['total'] >= 1) {
    $results = $query['hits']['hits'];
} else {
    $results = $query['hits']['hits'];
}






$singleslider_arr=[];
$doubleslider_arr=[];



//print_r($params);
//print_r($results);
//print_r($filter_prod_arr);
$rows = count($results);
if (isset($_REQUEST["page_rows"])) {
    $page_rows = $_REQUEST["page_rows"];
} else {
    if (isMobileDevice()) {
        $page_rows = 16;
    } else {
        $page_rows = 24;
    }
}
$last = ceil($rows / $page_rows);

if ($last < 1) {
    $last = 1;
}

// Establish the $pagenum variable

// Get pagenum from URL vars if it is present, else it is = 1

if (isset($_REQUEST['pagenum'])) {
//$pagenum = preg_replace('#[^0-9]#', '', $_REQUEST['pagenum']);
    $pagenum = $_REQUEST['pagenum'];
} else {
    $pagenum = 1;
}

// This makes sure the page number isn't below 1, or more than our $last page
if ($pagenum < 1) {
    $pagenum = 1;
} else if ($pagenum > $last) {
    $pagenum = $last;
}

/////////////////////////
if (isset($_REQUEST["sorting_by"])) {
    $sorting_by = $_REQUEST["sorting_by"];

    if ($sorting_by == "default|SORT_ASC") {
        $sort_field_name = "inventory_id";
        $sort_flag = "SORT_ASC";
    } else {
//echo $sorting_by;
        $sorting_by_arr = explode("|", $sorting_by);
        $sort_field_name = $sorting_by_arr[0];
        if (isset($sorting_by_arr[1])) {
            $sort_flag = $sorting_by_arr[1];
        } else {
            $sort_flag = "SORT_ASC";
        }


    }
} else {
    $sorting_by = "default|SORT_ASC";
    $sort_field_name = "inventory_id";
    $sort_flag = "SORT_ASC";
}
$filter_prod_arr_with_filter_arr = array();
$sort_fields = array();
if (!empty($results)) {
    foreach ($results as $resObj) {
        if (isset($resObj["_source"]["inventory_id"])) {
            if (isset($resObj["_source"][$sort_field_name]))
                $sort_fields[] = $resObj["_source"][$sort_field_name];
            $filter_prod_arr_with_filter_arr[$resObj["_source"]["inventory_id"]] = $resObj["_source"]["inventory_id"];
        }
    }
}

//print_r($sort_fields);
//print_r($filter_prod_arr);
//echo $sort_field_name;
//echo $sorting_by;
//if($pagenum==1){

if (count($sort_fields) > 0) {
    if ($sort_flag == "SORT_DESC") {
        array_multisort($sort_fields, SORT_DESC, $results);
    }
    if ($sort_flag == "SORT_ASC") {
        array_multisort($sort_fields, SORT_ASC, $results);
    }
}
//}
//else{
//array_multisort($sort_fields,$sort_flag , $results);
//}

///////////////////////////

?>
<?php

$filter_prod_arr = array();
//echo '<pre>';
//print_r($results);
//echo '</pre>';

foreach ($results as $resObj1) {
    $selling_price_for_price_filter_arr[] = $resObj1["_source"]["selling_price"];
	
	/* take the value of singleslider values starts */
	foreach($resObj1["_source"] as $invkey => $invvalue){
		if($invvalue==="singleslider"){
			$remove_type=str_replace("_type","",$invkey);
			$remove_type_filter=str_replace("filter_","",$remove_type);
			$singleslider_arr[$remove_type_filter][]=$resObj1["_source"]["filter_".$remove_type_filter."_filter_options"];
		}
		if($invvalue==="doubleslider"){
			$remove_type=str_replace("_type","",$invkey);
			$remove_type_filter=str_replace("filter_","",$remove_type);
			$doubleslider_arr[$remove_type_filter][]=$resObj1["_source"]["filter_".$remove_type_filter."_filter_options"];
		}
	}
	/* take the value of singleslider values ends */
	
	
    if (isset($resObj1["_source"]["inventory_id"])) {
        $filter_prod_arr[$resObj1["_source"]["inventory_id"]] = $resObj1["_source"]["inventory_id"];
    }
}
//print_r($selling_price_for_price_filter_arr);
//print_r($singleslider_arr);
////////// newly added code for discount promotion price filter starts /////////////////

$discount_is_exists = "no";
$cur_result = array_slice($results, ($pagenum - 1) * $page_rows, $page_rows);
$selling_price_for_price_filter_discount_arr = [];
foreach ($cur_result as $searchRes) {
    if (isset($searchRes['_source']['inventory_id'])) {

        $inventory_id = $searchRes['_source']['inventory_id'];
        $product_id = $searchRes['_source']['product_id'];
        $selling_price = $searchRes["_source"]["selling_price"];
        $max_selling_price = $searchRes["_source"]["max_selling_price"];
        $selling_discount = $searchRes["_source"]["selling_discount"];

        $inv_discount_data = $controller->get_default_discount_for_inv($inventory_id, $max_selling_price, $product_id);
        if (!empty($inv_discount_data)) {
            $discount_is_exists = "yes";
            $selling_price_for_price_filter_discount_arr[] = $inv_discount_data["current_price"];
        }


    }
}
if ($discount_is_exists == "yes") {
    $selling_price_for_price_filter_arr = $selling_price_for_price_filter_discount_arr;
}

////////// newly added code for discount promotion price filter starts /////////////////








//////////////////////////////// this is for filtering according to price filter added manually starts //////////////////////////////////////////////////

//print_r($results);
if (!isset($_REQUEST["type_of_filter"])) {
    $type_of_filter = "";
} else {
    $type_of_filter = $_REQUEST["type_of_filter"];
}




if ($type_of_filter != "") {
    $Price_filter_info_for_populating_inventories = $_REQUEST["Price_filter"];
    $Price_filter_info_for_populating_inventories_arr = explode("-", $Price_filter_info_for_populating_inventories);
    $min_Price_filter_info_for_populating_inventories = $Price_filter_info_for_populating_inventories_arr[0];
    $max_Price_filter_info_for_populating_inventories = $Price_filter_info_for_populating_inventories_arr[1];
    $results_temp_arr = array();
///echo "<pre>";
//print_r($results);
//echo "</pre>";
    foreach ($results as $ind => $results_arr) {


        ////////// newly added code for discount promotion price filter starts /////////////////
        $inventory_id = $results[$ind]['_source']['inventory_id'];
        $product_id = $results[$ind]['_source']['product_id'];
        $selling_price = $results[$ind]["_source"]["selling_price"];
        $max_selling_price = $results[$ind]["_source"]["max_selling_price"];
        $selling_discount = $results[$ind]["_source"]["selling_discount"];

        $inv_discount_data = $controller->get_default_discount_for_inv($inventory_id, $max_selling_price, $product_id);
        if (!empty($inv_discount_data)) {
            if ($inv_discount_data["current_price"] >= $min_Price_filter_info_for_populating_inventories && $inv_discount_data["current_price"] <= $max_Price_filter_info_for_populating_inventories) {
                $results_temp_arr[] = $results_arr;
            }
        } ////////// newly added code for discount promotion price filter starts /////////////////
        else {


            if ($results[$ind]["_source"]["selling_price"] >= $min_Price_filter_info_for_populating_inventories && $results[$ind]["_source"]["selling_price"] <= $max_Price_filter_info_for_populating_inventories) {
                $results_temp_arr[] = $results_arr;
            }
        }
    }


    $results = $results_temp_arr;
}

//////////////////////////////// this is for filtering according to price filter added manually ends //////////////////////////////////////////////




/* this block is for setting the min and max default price filter values starts */
if ($type_of_filter == "") {
    if (!empty($selling_price_for_price_filter_arr)) {
        $min_val_of_price_filter = min($selling_price_for_price_filter_arr);
        $max_val_of_price_filter = max($selling_price_for_price_filter_arr);
        if ($min_val_of_price_filter == $max_val_of_price_filter) {
            $min_val_of_price_filter = 0;
        }
    } else {
        $min_val_of_price_filter = 0;
        $max_val_of_price_filter = 0;
    }
    $default_min_val_of_price_filter = $min_val_of_price_filter;
    $default_max_val_of_price_filter = $max_val_of_price_filter;
} else {
    $Price_filter_info_for_populating_inventories = $_REQUEST["Price_filter"];
    $Price_filter_info_for_populating_inventories_arr = explode("-", $Price_filter_info_for_populating_inventories);
    $min_val_of_price_filter = $Price_filter_info_for_populating_inventories_arr[0];
    $max_val_of_price_filter = $Price_filter_info_for_populating_inventories_arr[1];
    $Price_default_info_arr = explode("-", $_REQUEST["Price_default_info"]);
    $default_min_val_of_price_filter = $Price_default_info_arr[0];
    $default_max_val_of_price_filter = $Price_default_info_arr[1];
}
/* this block is for setting the min and max default price filter values ends */





/* this block is for setting the min and max default singleslider_arr filter values starts */

if ($type_of_filter == "") {
	foreach($singleslider_arr as $key => $value_arr){
		if(!empty($value_arr)){
			$singleslider_filter_arr[$key]["min"] = min($value_arr);
			$singleslider_filter_arr[$key]["max"]= max($value_arr);
			if ($singleslider_filter_arr[$key]["min"] == $singleslider_filter_arr[$key]["max"]) {
				$singleslider_filter_arr[$key]["min"] = 0;
			}
		}
		else {
			$singleslider_filter_arr[$key]["min"] = 0;
			$singleslider_filter_arr[$key]["max"]= 0;
		}
		$singleslider_filter_arr[$key]["default_min"] = min($value_arr);
		$singleslider_filter_arr[$key]["default_max"]= max($value_arr);
	}
} else {
	foreach($singleslider_arr as $key => $value_arr){
		$singleslider_default_info_arr = explode("-", $_REQUEST[$key."_default_info"]);
		$singleslider_filter_arr[$key]["default_min"] = $singleslider_default_info_arr[0];
		$singleslider_filter_arr[$key]["default_max"] = $singleslider_default_info_arr[1];
		
		
		$singleslider_filter_info_for_populating_inventories = $_REQUEST[$key."_filter"];
		$singleslider_filter_info_for_populating_inventories_arr = explode("-", $singleslider_filter_info_for_populating_inventories);
		$singleslider_filter_arr[$key]["min"] = $singleslider_filter_arr[$key]["default_min"];
		$singleslider_filter_arr[$key]["max"] = $singleslider_filter_info_for_populating_inventories_arr[1];
		
		//echo $_REQUEST[$key.'_filter'];
		//echo "<br><br>1111111111";
		
		//echo $singleslider_filter_arr[$key]["default_min"]."-".$singleslider_filter_info_for_populating_inventories_arr[0];
		
	}
}

/* this block is for setting the min and max default singleslider_arr filter values ends */





/* this block is for setting the min and max default doubleslider_arr filter values starts */

if ($type_of_filter == "") {
	foreach($doubleslider_arr as $key => $value_arr){
		if(!empty($value_arr)){
			$doubleslider_filter_arr[$key]["min"] = min($value_arr);
			$doubleslider_filter_arr[$key]["max"]= max($value_arr);
			if ($doubleslider_filter_arr[$key]["min"] == $doubleslider_filter_arr[$key]["max"]) {
				$doubleslider_filter_arr[$key]["min"] = 0;
			}
		}
		else {
			$doubleslider_filter_arr[$key]["min"] = 0;
			$doubleslider_filter_arr[$key]["max"]= 0;
		}
		$doubleslider_filter_arr[$key]["default_min"] = min($value_arr);
		$doubleslider_filter_arr[$key]["default_max"]= max($value_arr);
	}
} else {
	foreach($doubleslider_arr as $key => $value_arr){
		$doubleslider_default_info_arr = explode("-", $_REQUEST[$key."_default_info"]);
		$doubleslider_filter_arr[$key]["default_min"] = $doubleslider_default_info_arr[0];
		$doubleslider_filter_arr[$key]["default_max"] = $doubleslider_default_info_arr[1];
		
		
		$doubleslider_filter_info_for_populating_inventories = $_REQUEST[$key."_filter"];
		$doubleslider_filter_info_for_populating_inventories_arr = explode("-", $doubleslider_filter_info_for_populating_inventories);
		$doubleslider_filter_arr[$key]["min"] = $doubleslider_filter_info_for_populating_inventories_arr[0];
		$doubleslider_filter_arr[$key]["max"] = $doubleslider_filter_info_for_populating_inventories_arr[1];
		
		//echo $_REQUEST[$key.'_filter'];
		//echo "<br><br>1111111111";
		
		//$doubleslider_filter_arr[$key]["default_min"]."-".$doubleslider_filter_info_for_populating_inventories_arr[0];
		
	}
}

/* this block is for setting the min and max default doubleslider_arr filter values ends */




////////////////////////////////////////


?>













<?php



//////////////////////////////// this is for filtering according to singleslider filter added manually starts //////////////////////////////////////////////////

//print_r($results);
if (!isset($_REQUEST["filterbox_type"])) {
    $filterbox_type = "";
} else {
    $filterbox_type = $_REQUEST["filterbox_type"];
}



//if ($filterbox_type === "singleslider") {
	if(!empty($singleslider_arr)){
		foreach($singleslider_arr as $singleslider_key => $singleslider_value){
			$singleslider_filtering = $singleslider_filter_arr[$singleslider_key]["min"]."-".$singleslider_filter_arr[$singleslider_key]["max"];
			$singleslider_filtering_arr = explode("-", $singleslider_filtering);
			//print_r($singleslider_filtering_arr);
			$minvalue = $singleslider_filtering_arr[0];
			$maxvalue = $singleslider_filtering_arr[1];
			$results_temp_arr = array();


			foreach ($results as $ind => $results_arr) {


				
				$inventory_id = $results[$ind]['_source']['inventory_id'];
				$product_id = $results[$ind]['_source']['product_id'];
				$selling_price = $results[$ind]["_source"]["selling_price"];
				$max_selling_price = $results[$ind]["_source"]["max_selling_price"];
				$selling_discount = $results[$ind]["_source"]["selling_discount"];

				if(isset($results[$ind]["_source"]["filter_".$singleslider_key."_filter_options"])){
					if ($results[$ind]["_source"]["filter_".$singleslider_key."_filter_options"] >= $minvalue && $results[$ind]["_source"]["filter_".$singleslider_key."_filter_options"] <= $maxvalue) {
						$results_temp_arr[] = $results_arr;
					}
				}
				else{
					$results_temp_arr[] = $results_arr;
				}
			
			}


			$results = $results_temp_arr;
		}
	}
//}

//////////////////////////////// this is for filtering according to singleslider filter added manually ends //////////////////////////////////////////////




//////////////////////////////// this is for filtering according to doubleslider filter added manually starts //////////////////////////////////////////////////

//print_r($results);
if (!isset($_REQUEST["filterbox_type"])) {
    $filterbox_type = "";
} else {
    $filterbox_type = $_REQUEST["filterbox_type"];
}



//if ($filterbox_type === "doubleslider") {
	if(!empty($doubleslider_arr)){
		foreach($doubleslider_arr as $doubleslider_key => $doubleslider_value){
			$doubleslider_filtering = $doubleslider_filter_arr[$doubleslider_key]["min"]."-".$doubleslider_filter_arr[$doubleslider_key]["max"];
			$doubleslider_filtering_arr = explode("-", $doubleslider_filtering);
			//print_r($doubleslider_filtering_arr);
			$minvalue = $doubleslider_filtering_arr[0];
			$maxvalue = $doubleslider_filtering_arr[1];
			$results_temp_arr = array();


			foreach ($results as $ind => $results_arr) {


				
				$inventory_id = $results[$ind]['_source']['inventory_id'];
				$product_id = $results[$ind]['_source']['product_id'];
				$selling_price = $results[$ind]["_source"]["selling_price"];
				$max_selling_price = $results[$ind]["_source"]["max_selling_price"];
				$selling_discount = $results[$ind]["_source"]["selling_discount"];

				if(isset($results[$ind]["_source"]["filter_".$doubleslider_key."_filter_options"])){
					if ($results[$ind]["_source"]["filter_".$doubleslider_key."_filter_options"] >= $minvalue && $results[$ind]["_source"]["filter_".$doubleslider_key."_filter_options"] <= $maxvalue) {
						$results_temp_arr[] = $results_arr;
					}
				}
				else{
					$results_temp_arr[] = $results_arr;
				}
			
			}


			$results = $results_temp_arr;
		}
	}
//}

//////////////////////////////// this is for filtering according to singleslider filter added manually ends //////////////////////////////////////////////


/**** brand_customfilterbox_filter starts ****/

if (isset($_REQUEST['brand_customfilterbox_filter']) && $_REQUEST['brand_customfilterbox_filter'] != "") {
	$brand_filtered_arr=[];
	$brand_customfilterbox_filter = $_REQUEST['brand_customfilterbox_filter'];
	$brand_customfilterbox_filter_arr = explode("|", $brand_customfilterbox_filter);
	$brand_filter_param_arr = array();
	$results_temp_arr = array();
	foreach ($brand_customfilterbox_filter_arr as $brand_name_custom) {
		$brand_filtered_arr[]=$brand_name_custom;
	}
	foreach ($results as $ind => $results_arr) {
		if(in_array($results[$ind]["_source"]["brand_name"],$brand_filtered_arr)){
			$results_temp_arr[] = $results_arr;
		}
	}
	$results = $results_temp_arr;
}

/**** brand_customfilterbox_filter ends ****/

?>


<?php


$cur_result = array_slice($results, ($pagenum - 1) * $page_rows, $page_rows);

////////////////////////////////////////////////////////////////////////
// Establish the $paginationCtrls variable
$paginationCtrls = '';
if ($last != 1) {
    if ($pagenum > 1) {
        $previous = $pagenum - 1;
//$paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$previous.'" aria-label="Previous"><span aria-hidden="true">&laquo; Previous</span></a></li>';
        $paginationCtrls .= '<li><a href="javascript:paginationFun(' . $previous . ')" aria-label="Previous"><span aria-hidden="true">&laquo; Previous</span></a></li>';

        for ($i = $pagenum - 4; $i < $pagenum; $i++) {
            if ($i > 0) {
// $paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'">'.$i.'</a></li>';
                $paginationCtrls .= '<li><a href="javascript:paginationFun(' . $i . ')">' . $i . '</a></li>';
            }
        }
    }
    $paginationCtrls .= '<li class="active"><a href="#">' . $pagenum . '</a></li>';
    for ($i = $pagenum + 1; $i <= $last; $i++) {
//$paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'">'.$i.'</a></li>';
        $paginationCtrls .= '<li><a href="javascript:paginationFun(' . $i . ')">' . $i . '</a></li>';
        if ($i >= $pagenum + 4) {
            break;
        }
    }
    if ($pagenum != $last) {
        $next = $pagenum + 1;
//$paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$next.'" aria-label="Next"><span aria-hidden="true">Next &raquo;</span></a></li>';
        $paginationCtrls .= '<li><a href="javascript:paginationFun(' . $next . ')" aria-label="Next"><span aria-hidden="true">Next &raquo;</span></a></li>';
    }
}
//////////////////////////////////////////////////////////////////////
/*echo '<pre>';
print_r($results);
echo '</pre>';*/
//exit;


?>


<?php
$url = base_url();
$var = "search_category";
$rand1 = $controller->sample_code();
$rand2 = $controller->sample_code();
$rand3 = $controller->sample_code();
$s = "/";
$temp = "search";
$home = "{$url}{$temp}";
?>
<div class="columns-container">
    <div class="container-fluid" id="columns">
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <?php
                /* menu filter starts*/
                ?>
                <!-- block category -->
                <div class="block left-module hidden-xs">
                    <div class="row">
							<div class="col-md-7 title_block col-xs-6" style="font-size:1em;">Categories 
					   
					   
					
					   </div>
					   <div class="col-md-5 title_block col-xs-6">
					   <?php
						if($type_of_filter==""){
					?>
						
							<a class="pull-right clearallfilterbtn" style="cursor:default;color:#b3b3b3;">Reset Filters&nbsp;</a>
						<?php
							}
							else{
								?>
								<a class="pull-right clearallfilterbtn" onclick="clearAllFiltersfun()" style="cursor:pointer;">Reset Filters&nbsp;</a>
								<?php
							}
						?>
						</div>
					</div>
					
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content" style="border-bottom: 1px solid #eaeaea;">
                                <ul class="tree-menu">

                                    <?php
                                    $menu = '';
                                    $parent_cat_infoObj = $controller->get_parent_cat_info($pcat_id);
                                    $rand_1 = $controller->sample_code();
                                    $rand_1 = $controller->sample_code();
                                    $cl = '';
                                    if ($pcat_id == 0) {
                                        $cl = 'active';
                                    }
                                    $cat = $controller->provide_category($pcat_id);
                                    if ($pcat_id != '' && $pcat_id != 0) {
                                        if (!empty($parent_cat_infoObj)) {
                                            if (($pcat_id == $parent_cat_infoObj->pcat_id)) {
                                                $cl = 'active';
                                            }
                                            $menu .= '<li class="' . $cl . '">';
                                            if (count($cat) > 0 && $cl == "active") {
                                                $menu .= ' <span class="open"></span>';
                                            } else {
                                                $menu .= ' <span></span>';
                                            }
                                            $menu .= '<a href="' . base_url() . "search_category/{$rand_1}{$pcat_id}" . '">' . $parent_cat_infoObj->pcat_name . '</a>'; // category li starts
                                            $menu .= '<small class="pull-right" style="color:#797979;">(' . $controller->get_skus_under_chain($pcat_id, "pcat") . ')</small>';
                                        }
                                    }


                                    if (count($cat) > 0) {
                                        $style1 = '';
                                        if ($cl == "active") {
                                            $style1 = 'style="display: block;"';
                                        } else {
                                            $style1 = 'style="display: none;"';
                                        }
                                        $menu .= ' <li>';
                                        $menu .= '<ul ' . $style1 . '>'; // categories list starts


                                        foreach ($cat as $cat_value) {
                                            $rand_2 = $controller->sample_code();
                                            $cat_name = $cat_value->cat_name;
                                            $class = '';
                                            $class1 = '';

                                            $cat_id_m = $cat_value->cat_id;
                                            if (isset($cat_id)) {
                                                if ($cat_id == $cat_id_m) {
                                                    $class1 = "active";
                                                }
                                                if (($subcat_id == '') && ($cat_id == $cat_id_m)) {
                                                    $class = 'active';
                                                }
                                            }
                                            $sub_cat = $controller->provide_sub_category($cat_id_m);
                                            $menu .= ' <li class="' . $class . '">'; // subcategory li starts
                                            if (count($sub_cat) > 0 && $class1 == "active") {
                                                $menu .= ' <span class="open"></span>';
                                            } else {
                                                $menu .= ' <span></span>';
                                            }
                                            $menu .= '<a href="' . base_url() . "search_category/{$rand_1}{$pcat_id}/{$rand_2}{$cat_id_m}" . '">' . $cat_name . '</a>';
                                            $menu .= '<small class="pull-right" style="color:#797979;">(' . $controller->get_skus_under_chain($cat_id_m, "cat") . ')</small>';


                                            if (count($sub_cat) > 0) {
                                                $style = '';
                                                if ($class1 == "active") {
                                                    $style = 'style="display: block;"';
                                                } else {
                                                    $style = 'style="display: none;"';
                                                }
                                                $menu .= '<ul ' . $style . '>'; // subcategories list starts

                                                foreach ($sub_cat as $sub_cat_value) {
                                                    $class2 = '';
                                                    $rand_3 = $controller->sample_code();
                                                    $subcat_id_m = $sub_cat_value["subcat_id"];
                                                    $subcat_value = $sub_cat_value["subcat_name"];
                                                    if (isset($subcat_id)) {
                                                        if ($subcat_id == $subcat_id_m) {
                                                            $class2 = "active";
                                                        }
                                                    }

                                                    $menu .= '<li class="' . $class2 . '"><span></span><a href="' . base_url() . "search_category/{$rand_1}{$pcat_id}/{$rand_2}{$cat_id_m}/{$rand_3}{$subcat_id_m}" . '">' . $subcat_value . '</a>';
                                                    $menu .= '<small class="pull-right" style="color:#797979;">(' . $controller->get_skus_under_chain($subcat_id_m, "subcat") . ')</small>';
                                                    $menu .= '</li>';
                                                }
                                                $menu .= '</ul>'; // subcategories list ends
                                            }

                                            $menu .= '</li>'; // subcategory li ends
                                        }
                                        $menu .= '</ul>'; // categories list ends
                                        $menu .= ' </li>';
                                    }
                                    $menu .= '</li>';  // category li ends
                                    echo $menu;

                                    ?>

                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
                <?php
                /* menu filter ends*/
                ?>
				
				
				
				
				<!-------------------------- brand filter starts ------------------------->
				<!--- // jarvinai code  for hiding this section when jarvin result came-->
				<div class="block left-module" id="filterselection_div" <?php if(!empty($jarvinai_inventory_id_arr)){echo "style='display:none;'";}?>>
                    <div class="row">
					<div class="col-md-7 title_block col-xs-6" style="font-size:1em;cursor:pointer" data-toggle="collapse"
                       data-target="#demobrand" id="brandselection_heading">Brand 
					   
					   
					
					   </div>
					   <div class="col-md-5 title_block col-xs-6">
					   <?php
						if($type_of_filter==""){
					?>
						
							<a class="pull-right clearallfilterbtn" style="cursor:default;color:#b3b3b3;">Reset Filters&nbsp;</a>
						<?php
							}
							else{
								?>
								<a class="pull-right clearallfilterbtn" onclick="clearAllFiltersfun()" style="cursor:pointer;">Reset Filters&nbsp;</a>
								<?php
							}
						?>
						</div>
					</div>
					
					
                    <?php
                    //echo $cat_current;
                    //echo $cat_current_id;
                    //$filter_box_infoRes=$controller->get_filter_box_info($cat_current,$cat_current_id);
                   // echo "<pre>";
				//	print_r($filter_box_infoRes);
					//echo "</pre>";
                    ?>
                    <div class="block_content collapse in" id="demobrand">
                        <!-- layered -->
                        <div class="layered">
						
						

                                    <div class="layered-content">
                                      
                                        <ul class="check-box-list">
                                             <?php
											 
											 
														
														
											$result_all_brand_names_arr=$controller->get_all_brand_names();
											foreach($result_all_brand_names_arr as $arr){
													$specific_brand_name=$arr["brand_name"];
												
												
														$checked = "";
														if(isset($_REQUEST["brand_customfilterbox_filter"])){
															$fil_arr = explode("|", $_REQUEST["brand_customfilterbox_filter"]);


															if (isset($_REQUEST["brand_customfilterbox_filter"])) {
																if (in_array($specific_brand_name, $fil_arr)) {
																	$checked = "checked";
																}
															}
															
														}
														
														
														
									   ?>
                                                        <li>
															
																<input type="checkbox" id
																="<?php echo $specific_brand_name; ?>"
																	   name="brand_customfilterbox"
																	   value="<?php echo $specific_brand_name; ?>" onchange="filterColorFun('brand_customfilterbox','brand')" <?php echo $checked; ?>>


																<label 
																		for="<?php echo $specific_brand_name; ?>"><span
																			class="button"></span><?php
																	
																		echo ucfirst($specific_brand_name);
																	 ?></label>
																	
																
															</li>
														
														
														
														
														
														
														
															
                                                        <?php
                                                    
											}
												 

                                            

                                            
                                            ?>

                                        </ul>
                                    </div>
                                    <?php
								
                                


                            
                            ?>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
				
				<!-------------------------- brand filter ends ------------------------->
				

                <!-- block filter -->
				<!--- // jarvinai code  for hiding this section when jarvin result came-->
                <div class="block left-module" id="filterselection_div" <?php if(!empty($jarvinai_inventory_id_arr)){echo "style='display:none;'";}?>>
                    <div class="row">
						<div class="col-md-7 title_block col-xs-6" style="font-size:1em;cursor:pointer" data-toggle="collapse"
                       data-target="#demofilter" id="filterselection_heading">Filters 
					   </div>
					   
							<div class="col-md-5 title_block col-xs-6">
						   <?php
							if($type_of_filter==""){
						?>
							
								<a class="pull-right clearallfilterbtn" style="cursor:default;color:#b3b3b3;">Reset Filters&nbsp;</a>
							<?php
								}
								else{
									?>
									<a class="pull-right clearallfilterbtn" onclick="clearAllFiltersfun()" style="cursor:pointer;">Reset Filters&nbsp;</a>
									<?php
								}
							?>
							</div>
						</div>
                    <?php
                    //echo $cat_current;
                    //echo $cat_current_id;
                    //$filter_box_infoRes=$controller->get_filter_box_info($cat_current,$cat_current_id);
                    //print_r($filter_box_infoRes);

                    ?>
                    <div class="block_content collapse in" id="demofilter">
                        <!-- layered -->
                        <div class="layered">

                            <?php

                            /* Price filter starts */
                            //echo "|".$type_of_filter."|";

                            ?>
                            <div class="layered_subtitle">
                                Price (<?php echo curr_sym; ?>)
                            </div>
                            <div class="range-slider">
                                <input type="text" class="js-range-slider" value=""/>
                            </div>
                            <hr>
                            <div class="extra-controls form-inline">
                                <div class="form-group">
                                    <input type="hidden" class="js-input-from form-control" id="min_val_of_price_filter"
                                           value=""/>
                                    <input type="hidden" class="js-input-to form-control" id="max_val_of_price_filter"
                                           value=""/>
                                </div>
                            </div>

                            <script>
                                function saveResult() {
//alert($("#min_val_of_price_filter").val()+" "+$("#max_val_of_price_filter").val())
                                    filterColorFun('Price');
                                }

                                // Trigger

                                $(function () {

                                    var $range = $(".js-range-slider"),
                                        $inputFrom = $(".js-input-from"),
                                        $inputTo = $(".js-input-to"),
                                        instance,
                                        min = '<?php echo $default_min_val_of_price_filter;?>',
                                        max = '<?php echo $default_max_val_of_price_filter;?>',
                                        from = 0,
                                        to = 0;

                                    $range.ionRangeSlider({
                                        type: "double",
                                        min: min,
                                        max: max,
                                        from: '<?php echo $min_val_of_price_filter;?>',
                                        to: '<?php echo $max_val_of_price_filter;?>',
                                        prefix: '',
                                        onStart: updateInputs,
                                        onChange: updateInputs,
                                        onFinish: saveResult,
                                        step: 1,
                                        prettify_enabled: true,
                                        prettify_separator: ",",
                                        values_separator: " - ",
                                        force_edges: true


                                    });

                                    instance = $range.data("ionRangeSlider");

                                    function updateInputs(data) {
                                        from = data.from;
                                        to = data.to;

                                        $inputFrom.prop("value", from);
                                        $inputTo.prop("value", to);
                                    }

                                    $inputFrom.on("input", function () {
                                        var val = $(this).prop("value");

// validate
                                        if (val < min) {
                                            val = min;
                                        } else if (val > to) {
                                            val = to;
                                        }

                                        instance.update({
                                            from: val
                                        });
                                    });

                                    $inputTo.on("input", function () {
                                        var val = $(this).prop("value");

// validate
                                        if (val < from) {
                                            val = from;
                                        } else if (val > max) {
                                            val = max;
                                        }

                                        instance.update({
                                            to: val
                                        });
                                    });

                                });


                            </script>


                            <!--- Price filter ends --->


                            <?php


                            //echo $type_of_filter;
                            $color_prev_arr = array();
                            $size_prev_arr = array();
                            $brand_prev_arr = array();
                            $price_prev_arr = array();
                            if (!empty($filter_box_infoRes)) {
                                foreach ($filter_box_infoRes as $filter_box_infoObj) {
                                    $filter_box_infoObj->filterbox_name = preg_replace('/\s+/', '_', $filter_box_infoObj->filterbox_name);
//print_r($filter_box_infoObj);
                                    $count_filterbox_title = 0;
                                    ?>
                                    <div class="layered_subtitle <?php echo $filter_box_infoObj->filterbox_name . "_title"; ?>">
                                        <?php
                                        if (strpos($filter_box_infoObj->filterbox_name, "_") !== false) {
                                            $filterbox_name_arr = explode("_", $filter_box_infoObj->filterbox_name);
                                            echo implode(" ", $filterbox_name_arr);
                                        } else {
                                            echo $filter_box_infoObj->filterbox_name;
                                        }
                                        ?>

                                    </div>

                                    <div class="layered-content filter-<?php echo strtolower($filter_box_infoObj->filterbox_name); ?>">
                                        <?php
                                        $filter_infoRes = $controller->get_filter_info($filter_box_infoObj->filterbox_id);
                                        ?>
                                        <ul class="check-box-list">
                                            <?php

                                            foreach ($filter_infoRes as $filter_infoObj) {
                                                if (strtolower($filter_box_infoObj->filterbox_name) == "color") {

                                                    $color_arr = explode(':', $filter_infoObj->filter_options);
                                                    $background = $color_arr[1];
                                                    $title = ucfirst($color_arr[0]);
                                                } else {

                                                    $background = ucfirst($filter_infoObj->filter_options);
                                                    $title = ucfirst($filter_infoObj->filter_options);
                                                }
                                                if (!isset($_REQUEST[$filter_box_infoObj->filterbox_name . "_filter"])) {
                                                    $color_filter = "";
                                                } else {
                                                    $color_filter = $_REQUEST[$filter_box_infoObj->filterbox_name . "_filter"];
                                                }

                                                if ($type_of_filter == $filter_box_infoObj->filterbox_name) {

                                                    $filter_box_infoObj->filterbox_name = preg_replace('/\s+/', '_', $filter_box_infoObj->filterbox_name);

                                                    $color_current_arr = explode(",", $_REQUEST[$filter_box_infoObj->filterbox_name . "_prev"]);

                                                    if ($veryfirst_condition_of_all_filters == "no") {
                                                        $check_current_arr_condition = in_array($filter_infoObj->filter_id, $color_current_arr);
                                                    } else {
                                                        $check_current_arr_condition = true;
                                                    }
                                                    if ($check_current_arr_condition) {
                                                        $color_prev_arr[$filter_box_infoObj->filterbox_name][$filter_infoObj->filter_id] = $filter_infoObj->filter_id;
                                                        $count_filterbox_title++;

                                                        $checked = "";
                                                        $fil_arr = explode("|", $_REQUEST[$filter_box_infoObj->filterbox_name . "_filter"]);


                                                        if (isset($_REQUEST[$filter_box_infoObj->filterbox_name . "_filter"])) {
                                                            if (in_array($filter_infoObj->filter_id, $fil_arr)) {
                                                                $checked = "checked";
                                                            }
                                                        }

                                                        ?>
                                                        <li>
                                                            <input type="checkbox" id
                                                            ="<?php echo $filter_infoObj->filter_options; ?>"
                                                                   name="<?php echo $filter_box_infoObj->filterbox_name; ?>"
                                                                   value="<?php echo $filter_infoObj->filter_id; ?>"
                                                                   onchange="filterColorFun('<?php echo $filter_box_infoObj->filterbox_name; ?>')" <?php echo $checked; ?> />


                                                            <label <?php if ($filter_box_infoObj->type == "select") { ?> style="background:<?php echo $background; ?>" <?php } ?>
                                                                    for="<?php echo $filter_infoObj->filter_options; ?>"><span
                                                                        class="button"></span><?php if ($filter_box_infoObj->type != "select") {
                                                                    echo ucfirst($filter_infoObj->filter_options);
                                                                } ?></label>

                                                        </li>
                                                        <?php
                                                    }

                                                } else {

                                                    $count_filter_id_for_products = $controller->get_filter_availability_for_products($cat_current, $cat_current_id, $filter_infoObj->filter_id, $filter_prod_arr, $filter_prod_arr_with_filter_arr, $type_of_filter, $color_filter);

                                                    $filter_box_infoObj->filterbox_name = preg_replace('/\s+/', '_', $filter_box_infoObj->filterbox_name);

                                                    if ($count_filter_id_for_products != 0) {

                                                        $color_prev_arr[$filter_box_infoObj->filterbox_name][$filter_infoObj->filter_id] = $filter_infoObj->filter_id;
                                                        $count_filterbox_title++;

                                                        $checked = "";

                                                        if (isset($_REQUEST[$filter_box_infoObj->filterbox_name . "_filter"])) {
                                                            $fil_arr = explode("|", $_REQUEST[$filter_box_infoObj->filterbox_name . "_filter"]);


                                                            if (in_array($filter_infoObj->filter_id, $fil_arr)) {
                                                                $checked = "checked";

                                                            }
                                                        }


                                                        ?>
                                                        <li>
                                                            <input type="checkbox" id
                                                            ="<?php echo $filter_infoObj->filter_options; ?>"
                                                                   name="<?php echo $filter_box_infoObj->filterbox_name; ?>"
                                                                   value="<?php echo $filter_infoObj->filter_id; ?>"
                                                                   onchange="filterColorFun('<?php echo $filter_box_infoObj->filterbox_name; ?>')" <?php echo $checked; ?> <?php if (strtolower($filter_infoObj->filter_options) == strtolower($searched_key)) {
                                                                echo "checked disabled";
                                                            } ?> />


                                                            <label <?php if ($filter_box_infoObj->type == "select") { ?> style="background:<?php echo $background; ?>" title="<?php echo $title; ?>"  <?php } ?>
                                                                    for="<?php echo $filter_infoObj->filter_options; ?>"><span
                                                                        class="button"></span><?php
                                                                //echo strtoupper($filter_box_infoObj->type);
                                                                if ($filter_box_infoObj->type != "select") {
                                                                    echo ucfirst($filter_infoObj->filter_options);
                                                                } ?></label>
                                                        </li>
                                                        <?php
                                                    }
                                                }


                                            }

                                            if ($count_filterbox_title == 0) {
                                                ?>
                                                <script>
                                                    document.getElementsByClassName("<?php echo $filter_box_infoObj->filterbox_name; ?>_title")[0].style.display = "none";
                                                    document.getElementsByClassName("<?php echo 'filter-' . strtolower($filter_box_infoObj->filterbox_name); ?>")[0].style.border = "0px solid #eaeaea;";
                                                    document.getElementsByClassName("<?php echo 'filter-' . strtolower($filter_box_infoObj->filterbox_name); ?>")[0].style.display = "none";
                                                </script>
                                            <?php
                                            }
                                            else{
                                            ?>
                                                <script>
                                                    document.getElementsByClassName("<?php echo 'filter-' . strtolower($filter_box_infoObj->filterbox_name); ?>")[0].style.border = "1px solid #eaeaea;";
                                                </script>
                                                <?php
                                            }
                                            ?>

                                        </ul>
                                    </div>
                                    <?php

                                }


                            }
                            ?>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block filter  -->

            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <?php
                if (empty($cur_result)) {
                    ?>
                    <div id="view-product-list" class="view-product-list">
                        <div class="row" style="height:100vh;display:flex;align-items:center;text-align:center;">
                            <div class="col-md-12">
                                <h4>
                                    Your search did not yield any results. Try another one!
                                </h4>
                            </div>
                        </div>
                    </div>
                    <?php
                } else {
                    ?>
                    <!-- view-product-list-->
                    <div id="view-product-list" class="view-product-list">
                        <!-- breadcrumb -->
						<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>

                            <?php
                            if ($pcat_id != "") {
                                ?>

                                <?php

                                $parent_cat_infoObj = $controller->get_parent_cat_info($pcat_id);
                                if (!empty($parent_cat_infoObj)) {

                                    ?>

<?php
$url1 = "{$url}{$var}{$s}{$rand1}{$parent_cat_infoObj->pcat_id}";
echo '<li class="breadcrumb-item"><a href="' . $url1 . '" title="' . $parent_cat_infoObj->pcat_name . '">' . $parent_cat_infoObj->pcat_name . '</a></li>';
?>
</span>
                                    <?php
                                } else {
                                    $url1 = "{$url}{$var}{$s}{$rand1}{$pcat_id}";
//echo "All Categories";
                                }
                                ?>

                                <?php
                            }
                            ?>
                            <?php
                            if ($cat_id != "") {
$cat_infoObj = $controller->get_cat_info($cat_id);
$url2 = "{$url1}{$s}{$rand2}{$cat_infoObj->cat_id}";
//echo $cat_infoObj->cat_name;
echo '<li class="breadcrumb-item"><a href="' . $url2 . '" title="' . $cat_infoObj->cat_name . '">' . $cat_infoObj->cat_name . '</a></li>';
?>
</span>
                                <?php
                            }
                            ?>
                            <?php
                            if ($subcat_id != "") {
                                ?>
                                <span class="navigation-pipe">&nbsp;</span>
                                <span class="navigation_page">
<?php
$subcat_infoObj = $controller->get_subcat_info($subcat_id);
$url3 = "{$url2}{$s}{$rand3}{$subcat_infoObj->subcat_id}";
//echo $subcat_infoObj->subcat_name;
echo '<li class="breadcrumb-item"><a href="' . $url3 . '" title="' . $subcat_infoObj->subcat_name . '">' . $subcat_infoObj->subcat_name . '</a></li>';
?>
</span>
                                <?php
                            }
                            ?>
							</ol>
							</nav>
                        <!-- ./breadcrumb -->

                        <h2 class="mt-20 ml-10" style="display:inline-block;border-bottom: 2px solid #ff3300;padding-bottom:4px;font-size:1.5em;">
<!-- page title starts -->
<?php
if ($subcat_id != "") {

    $subcat_infoObj = $controller->get_subcat_info($subcat_id);
    echo $subcat_infoObj->subcat_name;

} else if ($cat_id != "") {

    $cat_infoObj = $controller->get_cat_info($cat_id);
    echo $cat_infoObj->cat_name;

} else if ($pcat_id != "") {
    $parent_cat_infoObj = $controller->get_parent_cat_info($pcat_id);
    if (!empty($parent_cat_infoObj)) {
        echo $parent_cat_infoObj->pcat_name;
    } else {
        echo "All Categories";
    }
}

?>
<!-- page title ends -->

                        </h2>
						<span class="mt-20" style="display:inline-block;line-height: 2;margin-left:10px;">(Total Products : <?php echo count($results);?>)</span>
                        <div id="top-sortPagi">
                            <div class="sortPagiBar display-sortP-option">
                                <!--<div class="show-product-item">
<select name="page_rows_limit" onchange="page_rows_limitFun(this)">
<option value="5" <?php //if($page_rows==5){echo "selected";}
                                ?>>Show 5</option>
<option value="10" <?php //if($page_rows==10){echo "selected";}
                                ?>>Show 10</option>
<option value="15" <?php //if($page_rows==15){echo "selected";}
                                ?>>Show 15</option>
<option value="20" <?php //if($page_rows==20){echo "selected";}
                                ?>>Show 20</option>
</select>
</div>-->
                                <div class="sort-product hidden-xs">
                                    <ul class="list-inline">
                                        <li><strong>Sort By</strong></li>
                                        <li>
                                            <a onclick="sorting_byFun('default|SORT_ASC')" <?php if ($sorting_by == "default|SORT_ASC") {
                                                echo "class='active'";
                                            } ?>>Product Name</a></li>
                                        <li>
                                            <a onclick="sorting_byFun('selling_price|SORT_ASC')" <?php if ($sorting_by == "selling_price|SORT_ASC") {
                                                echo "class='active'";
                                            } ?>>Price Low-High</a></li>
                                        <li>
                                            <a onclick="sorting_byFun('selling_price|SORT_DESC')" <?php if ($sorting_by == "selling_price|SORT_DESC") {
                                                echo "class='active'";
                                            } ?>>Price High-Low</a></li>
                                    </ul>
                                    <!--<select name="sorting_by_field" onchange="sorting_byFun(this)">
<option value="default|SORT_ASC"  <?php //if($sorting_by=="default|SORT_ASC"){echo "selected";}
                                    ?>>Sort By Name</option>
<option value="product_name|SORT_ASC" <?php //if($sorting_by=="product_name|SORT_ASC"){echo "selected";}
                                    ?>>Product Name</option>
<option value="selling_price|SORT_ASC" <?php //if($sorting_by=="selling_price|SORT_ASC"){echo "selected";}
                                    ?>>Price Low-High</option>
<option value="selling_price|SORT_DESC" <?php //if($sorting_by=="selling_price|SORT_DESC"){echo "selected";}
                                    ?>>Price High-Low</option>
</select>-->
                                </div>
                            </div>
                        </div>
                        <!---ff--->
                        <hr class="line-break">
                        <!---ff--->

                        <!-- <ul class="display-product-option">
<li class="view-as-grid selected">
<span>grid</span>
</li>
<li class="view-as-list">
<span>list</span>
</li>
</ul>-->
                        <!-- PRODUCT LIST -->
                        <ul class="row product-list grid">
                            <?php
                            //shuffle($cur_result);
                            foreach ($cur_result as $searchRes) {
                                if (isset($searchRes['_source']['inventory_id'])) {
									// jarvinai code  for only display the perticular skus when the jarvin search output come
									if(!empty($jarvinai_inventory_id_arr)){
										if(!in_array($searchRes['_source']['inventory_id'],$jarvinai_inventory_id_arr)){
											continue;
										}
									}
									

                                    $inventory_id = $searchRes['_source']['inventory_id'];
                                    $product_id = $searchRes['_source']['product_id'];
                                    $selling_price = $searchRes["_source"]["selling_price"];
                                    $max_selling_price = $searchRes["_source"]["max_selling_price"];
                                    $selling_discount = $searchRes["_source"]["selling_discount"];

                                    $inv_discount_data = $controller->get_default_discount_for_inv($inventory_id, $max_selling_price, $product_id);
///////////////  No of Offers starts ///////////////
                                    $promo_numbers = 0;
                                    $promotions = $controller->get_number_of_offers($inventory_id);
                                    $inventory_product_info_obj = $controller->get_inventory_product_info_by_inventory_id($inventory_id);

                                    if (!empty($promotions) && ($inventory_product_info_obj->stock_available > 0)) {
                                        $i = 1;
                                        $defaultDiscounts_num = 0;
                                        foreach ($promotions as $promotion) {
                                            foreach ($promotion as $promo) {

                                                if (preg_match_all('/\d+(?=%)/', $promo->get_type, $match) && ($promo->to_buy == 1)) {
                                                    $i++;
                                                    if ($promo->to_buy == 1) {
                                                        $defaultDiscounts_num = 1;
                                                    }
                                                }
                                            }
                                        }

                                        if ($defaultDiscounts_num != 0) {
                                            $promo_numbers = count($promotions) - $defaultDiscounts_num;
                                        } else {
                                            $promo_numbers = count($promotions);
                                        }

                                    }
                                    
                                    /*added*/
                                    if (empty($inv_discount_data) && ($selling_discount>0)){
                                        //$promo_numbers=($promo_numbers+1);
                                    }
                                    /*added*/
//////////////   No of offers ends    ///////////////
                                    ?>
                                    <li class="col-xs-6 col-sm-3">
                                        <div class="product-container selectProduct"
                                             data-id="<?php echo ucwords($searchRes["_source"]["product_name"]) ?>"
                                             data-title="<?php echo $searchRes['_source']['inventory_id']; ?>">
                                            <div class="left-block">
                                                <?php
                                                //echo $searchRes['_id'];
                                                $rand_1 = $controller->sample_code();
                                                $rand_2 = $controller->sample_code();
                                                ?>

                                                <a href="<?php echo base_url() ?>detail/<?php echo "{$rand_1}{$searchRes['_source']['inventory_id']}"; ?>">


                                                    <img class="img-responsive productImg_<?php echo $searchRes['_source']['inventory_id']; ?>"
                                                         alt="productsku"
                                                         src="<?php echo base_url() . $searchRes["_source"]["common_image"] ?>"/>

                                                </a>
                                                <div class="quick-view">
                                                    <?php
                                                    if ($this->session->userdata("customer_id")) {
                                                        if ($controller->check_wishlist($searchRes['_source']['inventory_id']) > 0) {
                                                            ?>
                                                            <script>
                                                                $(document).ready(function () {
                                                                    $("#wishlist_" +<?php echo $searchRes['_source']['inventory_id'];?>).css({"color": ""});
                                                                    $("#wishlist_" +<?php echo $searchRes['_source']['inventory_id'];?>).css({"color": "#ff4343;"});
                                                                });
                                                            </script>
                                                            <?php
                                                            $val = "Added to Wishlist";
                                                            $style_wishlist_label = "style='color:#eda900;'";
                                                        } else {
                                                            $val = "Add to Wishlist";
                                                            $style_wishlist_label = "";
                                                        }
                                                    } else {
                                                        $val = "Add to Wishlist";
                                                        $style_wishlist_label = "";
                                                    }
                                                    ?>
                                                    <?php
                                                    if ($this->session->userdata("customer_id")) {
                                                        ?>
                                                        <span style="cursor:pointer;"
                                                              id="anchor_<?php echo $searchRes['_source']['inventory_id']; ?>"
                                                              onclick="add_to_wishlistFun(<?php echo $searchRes['_source']['inventory_id']; ?>);return false;"
                                                              title="<?php echo $val; ?>" class="wishlist">
<i id="<?php echo "wishlist_" . $searchRes['_source']['inventory_id']; ?>" class="fa fa-heart"
   style='color:#c2c2c2;font-size: 1.2em;'></i>
</span>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <span style="cursor:pointer"
                                                              id="anchor_<?php echo $searchRes['_source']['inventory_id']; ?>"
                                                              onclick="add_to_wishlistFun(<?php echo $searchRes['_source']['inventory_id']; ?>);return false;"
                                                              title="<?php echo $val; ?>" class="wishlist"><i
                                                                    id="<?php echo "wishlist_" . $searchRes['_source']['inventory_id']; ?>"
                                                                    class="fa fa-heart"
                                                                    style='color:#c2c2c2;font-size: 1.2em;'></i></span>
                                                        <?php
                                                    }
                                                    ?>

                                                </div>

                                                <?php if (!empty($inv_discount_data)) { ?>
                                                    <div class="price-percent-reduction2" <?php if ($inv_discount_data['discount'] < 10) {
                                                        echo "style='padding-top:18px'";
                                                    } ?>>
                                                        <?php echo $inv_discount_data['discount'] ?>% Off


                                                    </div>
                                                <?php }else{ ?>

                                                <?php
                                                if($selling_discount>0){
                                                            ?>
                                                                <div class="price-percent-reduction2" style="top:15%;"  <?php if ($selling_discount < 10) {
                                                                //echo "style='padding-top:18px'";
                                                            } ?>>
                                                                <?php echo round($selling_discount); ?>% Off
                                                            </div>

                                                        <?php
                                                }
                                                ?>
												<!-- offer discount starts --->
												<?php
												$is_offer_discount_exists_flag=$controller->is_offer_discount_exists($searchRes['_source']['inventory_id']);
                                                if($is_offer_discount_exists_flag=="yes"){
                                                            ?>
                                                                <div class="price-percent-reduction_offers"  style="padding-top:1rem;font-size:1em;">
																	OFFER
																</div>

                                                        <?php
                                                }
                                                ?>
												<!-- offer discount ends --->
                                                <?php
                                                }
                                                if ($promo_numbers > 0) {
                                                    ?>
                                                    <div class="price-percent-reduction_offers" style="top:60%;">
                                                        <?php

                                                        if ($promo_numbers > 1) {
                                                            echo $promo_numbers . ' Offers';
                                                        } else {
                                                            echo $promo_numbers . ' Offer';
                                                        }
                                                        ?>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                            </div>
<script>
$(document).ready(function () {
$('#product-name<?php echo $inventory_id; ?>').html($('#product-name<?php echo $inventory_id; ?>').html().trim().substring(0,50)+" ...");		
});
</script>
                                            <div class="right-block heightmatched_sku">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <h5 class="product-name heightmatched"
                                                            style="font-size:1.05em;padding-bottom:0!important;word-break:break-word;line-height:1.3;">
                                                            <a id="product-name<?php echo $inventory_id; ?>" href="<?php echo base_url() ?>detail/<?php echo "{$rand_1}{$searchRes['_source']['inventory_id']}"; ?>"
                                                               title="<?php
                                                               //$str = $searchRes["_source"]["product_name"];
                                                               $str = ($searchRes["_source"]["sku_name"] !='') ? $searchRes["_source"]["sku_name"] : $searchRes["_source"]["product_name"];
                                                               $str = str_replace("\r\n", '', $str);
                                                               $str = str_replace('\r\n', '', $str);
                                                               echo ucwords($str);
                                                               ?>">

                                                                <?php
                                                                
                                                                //$str = $searchRes["_source"]["product_name"];
                                                                $str = ($searchRes["_source"]["sku_name"] !='') ? $searchRes["_source"]["sku_name"] : $searchRes["_source"]["product_name"];
                                                                $str = str_replace("\r\n", '', $str);
                                                                $str = str_replace('\r\n', '', $str);
                                                                echo ucwords($str);
                                                                ?>

                                                                <!---added attributes ---->
                                                                <span style="<?php echo ($searchRes["_source"]["sku_name"]!='') ? 'display:none;' : ''; ?>">
                                                                <?php
                                                                $color_attribute_is_there = "no";
                                                                if (strtolower($inventory_product_info_obj->attribute_1) == "color") {
                                                                    $color_attribute_is_there = "yes";
                                                                    echo " - " . explode(":", $inventory_product_info_obj->attribute_1_value)[0];
                                                                } if (strtolower($inventory_product_info_obj->attribute_2) == "color") {
                                                                    $color_attribute_is_there = "yes";
                                                                    echo " - " . explode(":", $inventory_product_info_obj->attribute_2_value)[0];
                                                                } if (strtolower($inventory_product_info_obj->attribute_3) == "color") {
                                                                    $color_attribute_is_there = "yes";
                                                                    echo " - " . explode(":", $inventory_product_info_obj->attribute_3_value)[0];
                                                                } if (strtolower($inventory_product_info_obj->attribute_4) == "color") {
                                                                    $color_attribute_is_there = "yes";
                                                                    echo " - " . explode(":", $inventory_product_info_obj->attribute_4_value)[0];
                                                                }
                                                                ?>
                                                                <?php
                                                                if ($color_attribute_is_there == "yes") {
                                                                    if (strtolower($inventory_product_info_obj->attribute_1) != "color") {
                                                                        if ($inventory_product_info_obj->attribute_1_value != "") {
                                                                            echo " - " . $inventory_product_info_obj->attribute_1_value;
                                                                        }
                                                                    } if (strtolower($inventory_product_info_obj->attribute_2) != "color") {
                                                                        if ($inventory_product_info_obj->attribute_2_value != "") {
                                                                            echo " - " . $inventory_product_info_obj->attribute_2_value;
                                                                        }
                                                                    } if (strtolower($inventory_product_info_obj->attribute_3) != "color") {
                                                                        if ($inventory_product_info_obj->attribute_3_value != "") {
                                                                            echo " - " . $inventory_product_info_obj->attribute_3_value;
                                                                        }
                                                                    } if (strtolower($inventory_product_info_obj->attribute_4) != "color") {
                                                                        if ($inventory_product_info_obj->attribute_4_value != "") {
                                                                            echo " - " . $inventory_product_info_obj->attribute_4_value;
                                                                        }
                                                                    }
                                                                } else {
                                                                    $noncolor_attribute_first_is_there = "no";
                                                                    $noncolor_attribute_second_is_there = "no";

                                                                    if (strtolower($inventory_product_info_obj->attribute_1) != "color") {
                                                                        $noncolor_attribute_first_is_there = "yes";
                                                                        echo " - " . $inventory_product_info_obj->attribute_1_value;
                                                                    }
                                                                    if (strtolower($inventory_product_info_obj->attribute_2) != "color") {
                                                                        if ($noncolor_attribute_first_is_there == "yes") {
                                                                            $noncolor_attribute_second_is_there = "yes";
                                                                            if ($inventory_product_info_obj->attribute_2_value != "") {
                                                                                echo " - " . $inventory_product_info_obj->attribute_2_value;
                                                                            }
                                                                        } else {
                                                                            $noncolor_attribute_first_is_there = "yes";
                                                                            echo " - " . $inventory_product_info_obj->attribute_2_value;
                                                                        }
                                                                    }
                                                                    if (strtolower($inventory_product_info_obj->attribute_3) != "color") {
                                                                        if ($noncolor_attribute_first_is_there == "no" || $noncolor_attribute_second_is_there == "no") {
                                                                            if ($noncolor_attribute_first_is_there == "yes") {
                                                                                $noncolor_attribute_second_is_there = "yes";
                                                                                if ($inventory_product_info_obj->attribute_3_value != "") {
                                                                                    echo " - " . $inventory_product_info_obj->attribute_3_value;
                                                                                }
                                                                            } else {
                                                                                $noncolor_attribute_first_is_there = "yes";
                                                                                echo " - " . $inventory_product_info_obj->attribute_3_value;
                                                                            }
                                                                        }
																		else{
																			if ($inventory_product_info_obj->attribute_3_value != "") {
																				echo " - " . $inventory_product_info_obj->attribute_3_value;
																			}
																		}
                                                                    }
                                                                    if (strtolower($inventory_product_info_obj->attribute_4) != "color") {
                                                                        if ($noncolor_attribute_first_is_there == "no" || $noncolor_attribute_second_is_there == "no") {
                                                                            if ($noncolor_attribute_first_is_there == "yes") {
                                                                                $noncolor_attribute_second_is_there = "yes";
                                                                                if ($inventory_product_info_obj->attribute_4_value != "") {
                                                                                    echo " - " . $inventory_product_info_obj->attribute_4_value;
                                                                                }
                                                                            } else {
                                                                                $noncolor_attribute_first_is_there = "yes";
                                                                                echo " - " . $inventory_product_info_obj->attribute_4_value;
                                                                            }
                                                                        }
																		else{
																			if ($inventory_product_info_obj->attribute_4_value != "") {
																				echo " - " . $inventory_product_info_obj->attribute_4_value;
																			}
																		}
                                                                    }
                                                                }
                                                                $color_attribute_is_there = "no";
                                                                $noncolor_attribute_first_is_there = "no";
                                                                $noncolor_attribute_second_is_there = "no";
                                                                ?>
                                                                <!---added attributes ---->

                                                                </span>
                                                            </a>
                                                        </h5>
                                                    </div>
                                                </div>
                                                <!---ratings---added------------->
                                                <div class="row mb-15" style="display:none;">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <?php

                                                        $inventory_id = $searchRes['_source']['inventory_id'];
                                                        $rate_number = $controller->number_of_ratings($inventory_id);
                                                        $number_ratings_one_to_five = $controller->number_ratings_one_to_five($inventory_id);
                                                        $total_ratings = $rate_number;

                                                        if ($total_ratings != 0) {
                                                            foreach ($number_ratings_one_to_five as $rating => $rating_count) {
                                                                if ($rating == "one") {
                                                                    $one_rate_percent = ($rating_count / $total_ratings) * 100;
                                                                    $one_rate_count = $rating_count;
                                                                }
                                                                if ($rating == "two") {
                                                                    $two_rate_percent = ($rating_count / $total_ratings) * 100;
                                                                    $two_rate_count = $rating_count;
                                                                }
                                                                if ($rating == "three") {
                                                                    $three_rate_percent = ($rating_count / $total_ratings) * 100;
                                                                    $three_rate_count = $rating_count;
                                                                }
                                                                if ($rating == "four") {
                                                                    $four_rate_percent = ($rating_count / $total_ratings) * 100;
                                                                    $four_rate_count = $rating_count;
                                                                }
                                                                if ($rating == "five") {
                                                                    $five_rate_percent = ($rating_count / $total_ratings) * 100;
                                                                    $five_rate_count = $rating_count;
                                                                }
                                                            }
                                                        } else {
                                                            $one_rate_percent = 0;
                                                            $two_rate_percent = 0;
                                                            $three_rate_percent = 0;
                                                            $four_rate_percent = 0;
                                                            $five_rate_percent = 0;
                                                            $one_rate_count = 0;
                                                            $two_rate_count = 0;
                                                            $three_rate_count = 0;
                                                            $four_rate_count = 0;
                                                            $five_rate_count = 0;
                                                        }

                                                        $avg_value = $controller->get_average_value_of_ratings($inventory_id);
                                                        //$avg_value;
                                                        //unset($avg);
                                                        $username = $this->session->userdata('customer_name');

                                                        $review_number = $controller->number_of_reviews($inventory_id);
                                                        //echo"&nbsp;"."$review_number".'&nbsp;Reviews &nbsp;';
                                                        ?>

                                                        <div class="product-star has-popover_<?php echo $inventory_id; ?>"
                                                             rel="popover" style="background:inherit;">
<span style="background-color:#388e3c;color:#fff;padding:4px 8px;border-radius:2px;">
<?php echo $avg_value; ?>
<i class="fa fa-star" style="margin-left:0.5em;line-height: 1.5;font-size: 0.85em;"></i>
</span>

                                                        </div><!---product-star-->

                                                        <script>
                                                            $(function () {

                                                                var showPopover = function () {
//$(".infopoint").popover(options);
                                                                    $(this).popover('show');
                                                                    progressBar(<?php echo $five_rate_percent; ?>, $('#progressBar_<?php echo $inventory_id; ?>_5'));
                                                                    progressBar(<?php echo $four_rate_percent; ?>, $('#progressBar_<?php echo $inventory_id; ?>_4'));
                                                                    progressBar(<?php echo $three_rate_percent; ?>, $('#progressBar_<?php echo $inventory_id; ?>_3'));
                                                                    progressBar(<?php echo $two_rate_percent; ?>, $('#progressBar_<?php echo $inventory_id; ?>_2'));
                                                                    progressBar(<?php echo $one_rate_percent; ?>, $('#progressBar_<?php echo $inventory_id; ?>_1'));

                                                                }
                                                                    , hidePopover = function () {
                                                                    $(this).popover('hide');
                                                                };

                                                                $('.has-popover_<?php echo $inventory_id; ?>').popover({
                                                                    placement: function (tip, element) {
                                                                        var $element, above, actualHeight, actualWidth,
                                                                            below, boundBottom, boundLeft, boundRight,
                                                                            boundTop, elementAbove, elementBelow,
                                                                            elementLeft, elementRight, isWithinBounds,
                                                                            left, pos, right;
                                                                        isWithinBounds = function (elementPosition) {
                                                                            return boundTop < elementPosition.top && boundLeft < elementPosition.left && boundRight > (elementPosition.left + actualWidth) && boundBottom > (elementPosition.top + actualHeight);
                                                                        };
                                                                        $element = $(element);
                                                                        pos = $.extend({}, $element.offset(), {
                                                                            width: element.offsetWidth,
                                                                            height: element.offsetHeight
                                                                        });
                                                                        actualWidth = 283;
                                                                        actualHeight = 117;
                                                                        boundTop = $(document).scrollTop();
                                                                        boundLeft = $(document).scrollLeft();
                                                                        boundRight = boundLeft + $(window).width();
                                                                        boundBottom = boundTop + $(window).height();
                                                                        elementAbove = {
                                                                            top: pos.top - actualHeight,
                                                                            left: pos.left + pos.width / 2 - actualWidth / 2
                                                                        };
                                                                        elementBelow = {
                                                                            top: pos.top + pos.height,
                                                                            left: pos.left + pos.width / 2 - actualWidth / 2
                                                                        };
                                                                        elementLeft = {
                                                                            top: pos.top + pos.height / 2 - actualHeight / 2,
                                                                            left: pos.left - actualWidth
                                                                        };
                                                                        elementRight = {
                                                                            top: pos.top + pos.height / 2 - actualHeight / 2,
                                                                            left: pos.left + pos.width
                                                                        };
                                                                        above = isWithinBounds(elementAbove);
                                                                        below = isWithinBounds(elementBelow);
                                                                        left = isWithinBounds(elementLeft);
                                                                        right = isWithinBounds(elementRight);
                                                                        if (right) {
                                                                            return "right";
                                                                        } else {
                                                                            if (left) {
                                                                                return "left";

                                                                            } else {
                                                                                if (top) {
                                                                                    return "top";
                                                                                } else {
                                                                                    if (below) {
                                                                                        return "bottom";
                                                                                    } else {
                                                                                        return "right";
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    },
                                                                    html: true,
                                                                    content: function () {
                                                                        return $("#ratings_<?php echo $inventory_id; ?>").html();
                                                                    },
                                                                    trigger: 'manual'

                                                                })
                                                                    .focus(showPopover)
                                                                    .blur(hidePopover)
                                                                    .hover(showPopover, hidePopover);
                                                            });

                                                            function progressBar(percent, $element) {
                                                                var progressBarWidth = percent * $element.width() / 100;
//$element.find('div').css({ width: progressBarWidth }).html( "&nbsp;");
                                                                $element.find('div').animate({width: progressBarWidth}, 500).html("&nbsp;");
                                                            }


                                                        </script>

                                                        <div id="ratings_<?php echo $inventory_id; ?>" class="popover"
                                                             style="display:none;color:#808080;">
                                                            <div style="width:40%;float:left;color:#808080;">
                                                                <b> Ratings </b><br>
                                                                <i class="fa fa-star fa-2x">
                                                                    &nbsp;<?php echo $avg_value; ?></i><br>

                                                                Average of <?php echo $rate_number; ?>
                                                            </div>
                                                            <div class="horizontal_bar" style="color:#808080;">
                                                                <div class="star_rate_1">
                                                                    <div class="star_rate_2">5 star</div>
                                                                    <div id="progressBar_<?php echo $inventory_id; ?>_5"
                                                                         class="jquery-ui-like">
                                                                        <div class="hbar"></div>
                                                                    </div>
                                                                    <div class="star_rate_3">
                                                                        &nbsp;(<?php echo $five_rate_count ?>)&nbsp;
                                                                    </div>
                                                                </div>
                                                                <div class="star_rate_1">
                                                                    <div class="star_rate_2">4 star</div>
                                                                    <div id="progressBar_<?php echo $inventory_id; ?>_4"
                                                                         class="jquery-ui-like">
                                                                        <div class="hbar"></div>
                                                                    </div>
                                                                    <div class="star_rate_3">
                                                                        &nbsp;(<?php echo $four_rate_count ?>)&nbsp;
                                                                    </div>
                                                                </div>
                                                                <div class="star_rate_1">
                                                                    <div class="star_rate_2">3 star</div>
                                                                    <div id="progressBar_<?php echo $inventory_id; ?>_3"
                                                                         class="jquery-ui-like">
                                                                        <div class="hbar"></div>
                                                                    </div>
                                                                    <div class="star_rate_3">
                                                                        &nbsp;(<?php echo $three_rate_count ?>)&nbsp;
                                                                    </div>
                                                                </div>
                                                                <div class="star_rate_1">
                                                                    <div class="star_rate_2">2 star</div>
                                                                    <div id="progressBar_<?php echo $inventory_id; ?>_2"
                                                                         class="jquery-ui-like">
                                                                        <div class="hbar"></div>
                                                                    </div>
                                                                    <div class="star_rate_3">
                                                                        &nbsp;(<?php echo $two_rate_count ?>)&nbsp;
                                                                    </div>
                                                                </div>
                                                                <div class="star_rate_1">
                                                                    <div class="star_rate_2">1 star</div>
                                                                    <div id="progressBar_<?php echo $inventory_id; ?>_1"
                                                                         class="jquery-ui-like">
                                                                        <div class="hbar"></div>
                                                                    </div>
                                                                    <div class="star_rate_3">
                                                                        &nbsp;(<?php echo $one_rate_count ?>)&nbsp;
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <!---ratings---added------------->
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <div class="brandtext">
                                                            <?php echo "{$searchRes['_source']['brand_name']}"; ?>
                                                        </div>
                                                        <?php
                                                        /*
<h5 class="attribute-col">
<?php

if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_1"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_1_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_2"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_2_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_3"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_3_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_4"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_4_value"])[0];
}



if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_1"])!="color"){
if($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_1_value"]!=""){
echo "<h5 class='attribute-col' style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_1_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_2"])!="color"){
if($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_2_value"]!=""){
echo "<h5 class='attribute-col'  style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_2_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_3"])!="color"){
if($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_3_value"]!=""){
echo "<h5 class='attribute-col'  style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_3_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_4"])!="color"){
if($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_4_value"]!=""){
echo "<h5 class='attribute-col' style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_4_value"])."</h5>";
}
}



?>
</h5>
*/
                                                        ?>
                                                    </div>
                                                </div>


                                                <div class="content_price">
                                                    <?php if (!empty($inv_discount_data)) { ?>
                                                        <span class="price product-price"><?php echo curr_sym; ?><?php echo $inv_discount_data['current_price'] ?></span>
                                                        <span class="price old-price"><?php echo curr_sym; ?><?php echo $inv_discount_data['inventory_price'] ?></span>
                                                        <small style="line-height: 1.8;display:none;">(Inclusive of Taxes)</small>
                                                    <?php } else { ?>

                                                        <?php
                                                        if (get_pricetobeshown() == "selling_price") {
                                                            ?>
                                                            <span class="price product-price"><?php echo curr_sym; ?><?php echo $searchRes["_source"]["selling_price"] ?></span>
                                                            
                                                            <?php
                                                            if(intval($selling_price)<intval($max_selling_price)){

                                                            ?>
                                                            <span class="price old-price"><?php echo curr_sym; ?><?php echo round($max_selling_price); ?></span>
                                                            <?php

                                                            }
                                                            ?>
                                                            <small style="display:none;">(Inclusive of Taxes)</small>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <span class="price product-price"><?php echo curr_sym; ?><?php echo $searchRes["_source"]["base_price"] ?> <font
                                                                        style="font-size:0.6em;line-height:2.4"> + GST</font></span>
                                                            <small style="display:none;">(Inclusive of Taxes)</small>
                                                            <?php
                                                        }
                                                        ?>

                                                    <?php } ?>

                                                </div>
                                                <?php
                                                if (get_pricetobeshown() == "selling_price") {
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <?php
                                                            $inventory_comparision = $controller->inventory_comparision_yes_or_no($searchRes['_source']['inventory_id']);
                                                            ?>
                                                            <?php
                                                            if ($inventory_comparision == "yes") {
                                                                ?>

                                                                <div class="checkbox mb-0">
                                                                    <label>
                                                                        <input type="checkbox" value=""
                                                                               class="compare addToCompare"
                                                                               type="checkbox"
                                                                               id="inventory_<?php echo $inventory_id; ?>">
                                                                        <span class="cr"><i
                                                                                    class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                        <span class="add_to_compare_label">Add to compare</span>
                                                                    </label>
                                                                </div>


                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="info-orther">
                                                    <p>Item Code: #453217907</p>
                                                    <p class="availability">Availability: <span>In stock</span></p>
                                                    <div class="product-desc more">
                                                        <?php echo $searchRes["_source"]["product_description"] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                        <!-- ./PRODUCT LIST -->
                    </div>
                    <!-- ./view-product-list-->
                    <div class="sortPagiBar">
                        <div class="bottom-pagination">
                            <nav>
                                <ul class="pagination">
                                    <?php echo $paginationCtrls; ?>
                                </ul>
                            </nav>
                        </div>
                    </div>

                    <?php
                }
                ?>

            </div>


            <?php include 'recently_viewed.php'; ?>

            <script>
                function add_to_wishlistFun(inventory_id) {
                    document.getElementById('anchor_' + inventory_id).style.pointerEvents = 'none';

                <?php
                    if(!$this->session->userdata("customer_id")){
                    $this->session->set_userdata("cur_page", current_url());
                    ?>
                    if (confirm("Please login to add wishlist!")) {
                        location.href = "<?php echo base_url()?>login";
                    }
                    /*bootbox.confirm({
message: "Please login to add wishlist",
size: "small",
callback: function (result) {
if(result){
location.href="<?php echo base_url()?>login";
}
}
});*/

                    <?php
                    }else{
                    ?>
                    $.ajax({
                        url: "<?php echo base_url()?>add_to_wishlist",
                        type: "post",
                        dataType: "json",
                        data: "inventory_id=" + inventory_id,
                        success: function (data) {
                            if (data.exists == "no") {
                                alert('Added to wishlist');

                                /*bootbox.alert({
                                    size: "small",
                                    message: 'Added to wishlist',
                                });*/
                                $("#wishlist_" + inventory_id).css({"color": ""});
                                $("#wishlist_" + inventory_id).css({"color": "#ff4343;"});
                                $("#wishlist_label_" + inventory_id).html("Added to wishlist");
                                $("#anchor_" + inventory_id).attr({"title": "Added to wishlist"});
                                $("#wishlist_label_" + inventory_id).css({"color": "#ff4343;"});

                            } else if (data.exists == "yes") {

                                alert('Removed from wishlist');

                               /* bootbox.alert({
                                    size: "small",
                                    message: 'Removed from wishlist',
                                });*/

                                $("#wishlist_" + inventory_id).css({"color": ""});
                                $("#wishlist_" + inventory_id).css({"color": "#C2C2C2"});
                                $("#wishlist_label_" + inventory_id).html("Removed from wishlist");
                                $("#anchor_" + inventory_id).attr({"title": "Removed from wishlist"});
                                $("#wishlist_label_" + inventory_id).css({"color": "#C2C2C2;"});


                            } else {

                                alert('Error');

                               /* bootbox.alert({
                                    size: "small",
                                    message: 'Error',
                                });*/
                            }
                            document.getElementById('anchor_' + inventory_id).style.pointerEvents = 'auto';
                        }
                    });

                    <?php } ?>
                }
            </script>


            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>


<script>
    function page_rows_limitFun(obj) {
        $("input[name='page_rows']").val(obj.value);
        document.searchForm.submit();
    }

    function sorting_byFun(value) {
        $("input[name='sorting_by']").val(value);
        document.searchForm.submit();
    }

    function paginationFun(pn) {
        $("input[name='pagenum']").val(pn);
        document.searchForm.submit();
    }

    function filterColorFun(type_of_filter) {
        if (type_of_filter == "Price") {
            $("input[name='" + type_of_filter + "_filter']").val($("#min_val_of_price_filter").val() + "-" + $("#max_val_of_price_filter").val());
            $("input[name='type_of_filter']").val(type_of_filter);
//alert($("input[name='"+type_of_filter+"_filter']").val())
        } else {
            var filter = "";
            var filter_name = "";
            $("input[name='" + type_of_filter + "']").each(function () {
                if ($(this).is(":checked")) {
                    filter += $(this).val() + "|";
                    filter_name += $(this).attr('id') + "|";
                }
            });
            filter = filter.substr(0, filter.length - 1);
            filter_name = filter_name.substr(0, filter_name.length - 1);

//alert(type_of_filter);
//alert(filter_name);

            $("input[name='" + type_of_filter + "_filter']").val(filter);
            $("input[name='type_of_filter']").val(type_of_filter);

            if (filter_name != "") {
               // filter_activity(type_of_filter, filter_name);
            }
        }
        document.searchForm.submit();
    }

    function filter_activity(type_of_filter, filter_names) {
        /*user filter activity*/

        var start = new Date();
        start = start.getTime();

        level_type = '<?php echo (isset($level_type)) ? $level_type : ''; ?>';
        level_name = '<?php echo (isset($searched_key)) ? $searched_key : ''; ?>';
        cat_current = '<?php echo (isset($cat_current)) ? $cat_current : ''; ?>';
        cat_current_id = '<?php echo (isset($cat_current_id)) ? $cat_current_id : ''; ?>';

        if (level_type == "pcat_id") {
            level_type = "pcat";
        }
        if (level_type == "cat_id") {
            level_type = "cat";
        }
        if (level_type == "subcat_id") {
            level_type = "subcat";
        }

        /*alert(level_type);
alert(level_name);
alert(cat_current);
alert(cat_current_id);*/

        temp = {
            "level_type": level_type,
            "level_name": level_name,
            "name": cat_current,
            "id": cat_current_id,
            "filter_group": type_of_filter,
            "checked_filters": filter_names,
            "time": start
        };

        var r_final = localStorage.getItem("filter");

        if (r_final != null) {
            var stored = JSON.parse(localStorage.getItem("filter"));
            stored.push(temp);
            localStorage.setItem("filter", JSON.stringify(stored));
        } else {
            var obj = [];
            obj.push(temp);
            localStorage.setItem("filter", JSON.stringify(obj));
        }

        var result = JSON.parse(localStorage.getItem("filter"));
//alert(JSON.stringify(result));

//return false;

        /*user filter activity*/
    }

</script>

<form method="post" name="searchForm">
    <input type="hidden" name="search_keyword" value="<?php if (isset($_REQUEST["search_keyword"])) {
        echo $_REQUEST["search_keyword"];
    } ?>">
    <input type="hidden" name="search_cat" value="<?php if (isset($_REQUEST["search_cat"])) {
        echo $_REQUEST["search_cat"];
    } ?>">
    <input type="hidden" name="page_rows" value="<?php echo $page_rows; ?>">
    <input type="hidden" name="pagenum" value="<?php echo $pagenum; ?>">
    <input type="hidden" name="sorting_by" value="<?php echo $sorting_by; ?>">

    <input type="hidden" name="pcat_id" value="<?php echo $pcat_id; ?>">
    <input type="hidden" name="cat_id" value="<?php echo $cat_id; ?>">
    <input type="hidden" name="subcat_id" value="<?php echo $subcat_id; ?>">
    <input type="hidden" name="brand_id" value="<?php echo $brand_id; ?>">
    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
    <input type="hidden" name="inventory_id" value="<?php echo $inventory_id; ?>">
    <input type="hidden" name="cat_current" value="<?php echo $cat_current; ?>">
    <input type="hidden" name="cat_current_id" value="<?php echo $cat_current_id; ?>">
    <input type="hidden" name="level_type" value="<?php echo $level_type; ?>">
    <input type="hidden" name="level_name" value="<?php echo $searched_key; ?>">
    <input type="hidden" name="level_value" value="<?php echo $level_value; ?>">

    
	<input type="hidden" name="filterbox_type" id="filterbox_type">
	
	
	<input type="hidden" name="type_of_filter" id="type_of_filter"
           value="<?php if (isset($_REQUEST["type_of_filter"]) && $_REQUEST["type_of_filter"] != "") {
               echo $_REQUEST["type_of_filter"];
           } ?>">
<input type="hidden" name="brand_customfilterbox_filter"
                   value="<?php if (isset($_REQUEST['brand_customfilterbox_filter']) && $_REQUEST['brand_customfilterbox_filter'] != "") {
                       echo $_REQUEST['brand_customfilterbox_filter'];
                   } ?>">
    <?php
    if (!empty($filter_box_infoRes)) {
        foreach ($filter_box_infoRes as $filter_box_infoObj) {
            $filter_box_infoObj->filterbox_name = preg_replace('/\s+/', '_', $filter_box_infoObj->filterbox_name);

            ?>
            <input type="hidden" name="<?php echo $filter_box_infoObj->filterbox_name; ?>_filter"
                   value="<?php if (isset($_REQUEST[$filter_box_infoObj->filterbox_name . '_filter']) && $_REQUEST[$filter_box_infoObj->filterbox_name . '_filter'] != "") {
                       echo $_REQUEST[$filter_box_infoObj->filterbox_name . "_filter"];
                   } ?>">

            <input type="hidden" name="<?php echo $filter_box_infoObj->filterbox_name; ?>_prev"
                   value="<?php if (isset($color_prev_arr[$filter_box_infoObj->filterbox_name])) {
                       echo implode(",", $color_prev_arr[$filter_box_infoObj->filterbox_name]);
                   } ?>">
            <?php

        }
    }
    ?>
	
	<?php
		/* Price filter hidden fields after form submit starts */
	?>
    <?php
    if ($type_of_filter == "") {
        $Price_default_info = $default_min_val_of_price_filter . "-" . $default_max_val_of_price_filter;
    } else {
        $Price_default_info = $_REQUEST["Price_default_info"];
    }
    ?>
    <input type="hidden" name="Price_default_info" value="<?php echo $Price_default_info; ?>">
    <input type="hidden" name="Price_filter"
           value="<?php if (isset($_REQUEST['Price_filter']) && $_REQUEST['Price_filter'] != "") {
               echo $_REQUEST['Price_filter'];
           } else {
               echo $Price_default_info;
           } ?>">
		   
		<?php
		/* Price filter hidden fields after form submit ends */
	?>
	
	
	
	
	
<?php
	
		/* Single slider filter hidden fields after form submit starts */
		
		foreach($singleslider_arr as $singleslider_key => $singleslider_value_arr){
	?>
    <?php
    if ($type_of_filter == "") {
        $singleslider_default_info = $singleslider_filter_arr[$singleslider_key]["default_min"] . "-" . $singleslider_filter_arr[$singleslider_key]["default_max"];
    } else {
        $singleslider_default_info = $_REQUEST[$singleslider_key."_default_info"];
    }
    ?>
    <input type="hidden" name="<?php echo $singleslider_key;?>_default_info" value="<?php echo $singleslider_default_info; ?>">
    <input type="hidden" name="<?php echo $singleslider_key;?>_filter"
           value="<?php if (isset($_REQUEST[$singleslider_key.'_filter']) && $_REQUEST[$singleslider_key.'_filter'] != "") {
               echo $_REQUEST[$singleslider_key.'_filter'];
           } else {
               echo $singleslider_default_info;
           } ?>">
		   
		<?php
		}
		
		/* Single slider filter hidden fields after form submit ends */
	?>
	
	
	
<?php
	
		/* Double slider filter hidden fields after form submit starts */
		
		foreach($doubleslider_arr as $doubleslider_key => $doubleslider_value_arr){
	?>
    <?php
    if ($type_of_filter == "") {
        $doubleslider_default_info = $doubleslider_filter_arr[$doubleslider_key]["default_min"] . "-" . $doubleslider_filter_arr[$doubleslider_key]["default_max"];
    } else {
        $doubleslider_default_info = $_REQUEST[$doubleslider_key."_default_info"];
    }
    ?>
    <input type="hidden" name="<?php echo $doubleslider_key;?>_default_info" value="<?php echo $doubleslider_default_info; ?>">
    <input type="hidden" name="<?php echo $doubleslider_key;?>_filter"
           value="<?php if (isset($_REQUEST[$doubleslider_key.'_filter']) && $_REQUEST[$doubleslider_key.'_filter'] != "") {
               echo $_REQUEST[$doubleslider_key.'_filter'];
           } else {
               echo $doubleslider_default_info;
           } ?>">
		   
		<?php
		}
		
		/* Double slider filter hidden fields after form submit ends */
	?>
	
	

</form>

<!--preview panel-->
<div class="container">
    <div class="comparePanle">
        <div class="row">
            <div class="col-md-3 text-center">
                <span class="compare_lineheight">Compare Products <small class="compare_lineheight"
                                                                         id="compare_line_text"> (Add <span
                                id="no_of_products_count_compare">0</span> more to compare )</small></span>
            </div>
            <div class="comparePan col-md-5 cursor-pointer">
            </div>
            <div class="col-md-4 text-center">
                &nbsp;
                <form method="post" action="<?php echo base_url() ?>compare_inventories">
                    <button type="submit" class="button btn-sm notActive cmprBtn" disabled>Compare <span
                                id="no_of_products_compare" class="badge"></span></button>
                    <a href="#" class="btn preventDflt" type="button" onclick="clearComparePanel()"><small>Clear
                            All</small></a>
                    <input type="hidden" name="inventory_id_list_for_compare" id="inventory_id_list_for_compare"
                           value="">
                </form>
            </div>

        </div>

    </div>
</div>
<!--end of preview panel-->

<!-- comparision popup-->
<div id="id01" class="w3-animate-zoom w3-white w3-modal modPos">
    <div class="w3-container">
        <a onclick="document.getElementById('id01').style.display='none'"
           class="whiteFont w3-padding w3-closebtn closeBtn">&times;</a>
    </div>
    <div class="w3-row contentPop w3-margin-top">
    </div>

</div>
<!--end of comparision popup-->

<!--  warning model  -->
<div id="WarningModal" class="w3-modal">
    <div class="w3-modal-content warningModal">
        <header class="w3-teal panel-heading">
            <h3><span>&#x26a0;</span>&nbsp;Error</h3>
        </header>
        <div class="w3-container">
            <p class="lead text-center">Maximum of Three products are allowed for comparision</p>
        </div>
        <footer class="w3-container w3-right-align">
            <button id="warningModalClose" onclick="document.getElementById('id01').style.display='none'"
                    class="w3-btn w3-hexagonBlue w3-margin-bottom preventDflt">Close
            </button>
        </footer>
    </div>
</div>
<!--  end of warning model  -->


<script>
    if (localStorage.getItem("comparePan") === null || localStorage.getItem("comparePan").trim() == "") {
    } else {
        $(".comparePanle").show();
        $("#inventory_id_list_for_compare").val(localStorage.getItem("comparePan_inventory_ids"));
        inventory_id_list_for_compare_str = $("#inventory_id_list_for_compare").val();
        inventory_id_list_for_compare_arr = inventory_id_list_for_compare_str.split(",");
        if (inventory_id_list_for_compare_arr.length > 0) {
            for (x in inventory_id_list_for_compare_arr) {
                $("#inventory_" + inventory_id_list_for_compare_arr[x]).attr("checked", true);
            }
        }
        $(".comparePan").html(localStorage.getItem("comparePan"));
        if ($("#inventory_id_list_for_compare").val().split(",").length > 1) {
            $(".cmprBtn").attr({"disabled": false});
        }
        $("#no_of_products_compare").html($("#inventory_id_list_for_compare").val().split(",").length);
        $("#no_of_products_count_compare").html(3 - $("#inventory_id_list_for_compare").val().split(",").length);
        if ($("#no_of_products_count_compare")[0].innerHTML == 0) {
            $("#compare_line_text").css("visibility", "hidden");
        } else {
            $("#compare_line_text").css("visibility", "visible");
        }
    }

    function clearComparePanel() {
        $(".addToCompare").each(function () {
            $(this).attr("checked", false);
        });

        localStorage.removeItem("comparePan");
        localStorage.removeItem("comparePan_inventory_ids");
        $(".comparePan").html("");
        $("#inventory_id_list_for_compare").val("");
        $(".comparePanle").hide();

    }

    $(document).ready(function () {
        showChar(200);
    });


    $(document).ready(function () {
        if ($(window).width() <= 480) {
            $(".collapse").removeClass("in");
        }

        toAdapt();
        toAdapt_sku();
        window.onresize = function () {
            toAdapt;
            toAdapt_sku;
        };

    });


    function toAdapt() {
        var heights = $(".heightmatched").map(function () {
                return $(this).height();
            }).get(),

            maxHeight = Math.max.apply(null, heights);

        $(".heightmatched").height(maxHeight);
    }

    function toAdapt_sku() {
        var heights_sku = $(".heightmatched_sku").map(function () {
                return $(this).height();
            }).get(),

            maxHeight_sku = Math.max.apply(null, heights_sku);

        $(".heightmatched_sku").height(maxHeight_sku);
    }

    function clearAllFiltersfun() {
        window.location.href = location.href;
    }
</script>