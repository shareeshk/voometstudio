<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/skins/jquery-ui-like/progressbar.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/product_comparision/style.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/product_comparision/w3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/Ion.RangeSlider/Ion.RangeSlider.css"/>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/Ion.RangeSlider/Ion.RangeSlider.js"/></script>
<style type="text/css">

    @media only screen and (max-width: 600px) {
        .heightmatched {
            font-size: 1em !important;
        }
    }
    @media (max-width: 480px) {
        .product-list.grid li {
            padding-right: 5px;
            padding-left: 5px;
        }
    }

    .checkbox label:after {
        content: '';
        display: table;
        clear: both;
    }

    .checkbox .cr {
        position: relative;
        display: inline-block;
        border: 1px solid #a9a9a9;
        border-radius: .25em;
        width: 1.3em;
        height: 1.3em;
        float: left;
        margin-right: .5em;
    }


    .checkbox .cr .cr-icon {
        position: absolute;
        font-size: .8em;
        line-height: 0;
        top: 50%;
        left: 20%;
    }

    .radio .cr .cr-icon {
        margin-left: 0.04em;
    }

    .checkbox label {
        padding-left: 0px;
        line-height: 1.3;
    }

    .checkbox label input[type="checkbox"] {
        display: none;
    }

    .checkbox label input[type="checkbox"] + .cr > .cr-icon,
    .radio label input[type="radio"] + .cr > .cr-icon {
        transform: scale(3) rotateZ(-20deg);
        opacity: 0;
        transition: all .3s ease-in;
    }

    .checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
    .radio label input[type="radio"]:checked + .cr > .cr-icon {
        transform: scale(1) rotateZ(0deg);
        opacity: 1;
    }

    .checkbox label input[type="checkbox"]:disabled + .cr,
    .radio label input[type="radio"]:disabled + .cr {
        opacity: .5;
    }

    .product-list li .product-star {
        width: auto;
        float: left;
        color: #eda900;
        text-align: center;
        display: inline-block;
        padding-top: 12px;
        font-size: 0.9em;
    }

    .compare_close {
        margin-top: -8px;
        background-color: #a39999;
        border-radius: 11px;
        width: 18px;
        height: 18px;
        line-height: 17px;
        text-align: center;
        color: #f9ecec;
        text-decoration: none;
        float: right;
        font-size: 18px;
    }
    .compare_lineheight {
        line-height: 80px;
    }
    
    .comparePanle {
        width: 100%;
        background-color: #fff;
        border-top: 1px solid #eee;
        bottom: 0px;
        padding: 10px;
    }
    
    .combo_close {
        /* margin-top: -8px; */
        background-color: #a39999;
        border-radius: 11px;
        width: 18px;
        height: 18px;
        line-height: 17px;
        text-align: center;
        color: #f9ecec;
        text-decoration: none;
        float: right;
        font-size: 18px;
    }
    .combo_lineheight {
        line-height: 20px;
    }
    
    .comboPanle {
        width: 100%;
        background-color: #fff;
        border-top: 1px solid #eee;
        bottom: 0px;
        padding: 10px;
    }
.comboPanle{
    width: 100%;
    background-color: #fff;
    border-top: 1px solid #eee;
    bottom: 0px;
    padding: 10px;
    position: fixed;
    z-index: 1;
    border-top: 2px solid #ff9f00 !important;
}
.comboPanle .button {
    padding: 10px 20px;
    border: 1px solid #ff3300;
    background: #ff3300;
    color: #fff;
}
.comboPan {
    /* width: 100%; */
    overflow-y: auto;
    max-height: 100px;
}
    
    .horizontal_bar {
        width: 60%;
        float: left;
        margin-bottom: 10px;
        color: #ff9900;
    }

    .star_rate_1 {
        float: left;
        width: 100%;
    }

    .star_rate_2 {
        float: left;
        width: 30%;
    }

    .star_rate_3 {
        float: left;
        width: 10%;
    }

    .star_rate {
        width: 30%;
        float: left;
        margin-right: 6px;
    }

    .popover {
        width: 700px;
    }

    div.rater {
        width: 200px;
        min-height: 40px;
        background: black;
        color: white;
        padding: 5px;
        position: relative;
        word-wrap: break-word;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        border-radius: 5px;
        margin-bottom: 2em;
        margin-top: 8px;
    }

    div.rater:after {
        content: '';
        display: block;
        position: absolute;
        top: -20px;
        left: 30px;
        width: 0;
        height: 0;
        border-color: transparent transparent black transparent;
        border-style: solid;
        border-width: 10px;
    }

    .img-cat-display {
        width: 300px;
        height: 366px;
    }

    /*.display-sortP-option{
position: absolute;
top: 0;
right: 63px;
min-width: 200px;
height: 30px;
margin-top: 0px;
margin-right: 5px;
}
#top-sortPagi .sortPagiBar .sort-product,#top-sortPagi .sortPagiBar .show-product-item {
margin-left: 21px;
}
@media (max-width: 480px) {
.display-sortP-option{
position: relative;
top: 0;
right: 0px;
min-width: 200px;
height: 30px;
margin-top: -10px;
margin-right: 0px;
margin-bottom: 10px;
}
}*/
    #WarningModal {
        z-index: 9999;
    }

    .line-break {
        margin-top: 10px;
        margin-bottom: 0;
    }

    body {
        background-color: #f1f3f6;
    }

    .breadcrumb {
        padding: 15px 0 0 15px;
    }

    @media (min-width: 768px) {
        .center_column {
            padding-left: 0;
        }

    }

    .list-inline li {
        line-height: 24px;
    }

    .list-inline li a {
        cursor: pointer;
    }

    .list-inline li a.active {
        font-weight: 600;
        color: #eda900;
    }

    .product-name a {
        color: #000;
    }

    .product-name a:hover {
        color: #777;
    }

    .attribute-col {
        font-size: 0.9em;
        color: #777;
        margin-bottom: 0.2em;
    }

    .content_price {
        padding-top: 5px;
    }

    .clearallfilterbtn {
        font-size: 12px;
        color: #0000ff;
        margin-top: 2px;
        text-transform: capitalize;
        margin-right: 0.5rem;
    }

    .layered .layered-content {
        border-bottom: 0px;
    }
    .price-percent-reduction2{
        /* display: none; */
    }
    .price-percent-reduction_offers{
        display: none;

    }
    .new_banner{
        padding: 20px;
        border: 1px solid #ccc;
        border-radius: 10px;
        line-height: 20px;
    }
    .new_banner_container{
        padding:20px;
    }
    .margin-top {
        margin-top: 10px;
    }
</style>

<script>
    base_url = "<?php echo base_url();?>";
</script>
<script>
    $(document).ready(function () {
        $(".star-rating").find("a").each(function () {
            $(this).html("");
        })
//	$(".jquery-ui-like").find(".hbar").each(function(){
//		$(this).html("");
//	})

    })
</script>

<?php
require_once 'assets/elastic/app/init_customer.php';

////////////////////////////////////////////////////////////////////////////////////////////

$is_filter_applied="no";

////////////////////////////////////////////////////////////////////////////////////////////

//print_r($tagged_inventory_id_arr);

$veryfirst_condition_of_all_filters = "yes";
$cat_current="all";
$filter_box_infoRes = $controller->get_filter_box_info($cat_current, $cat_current_id, $category_tree);
$filterbox_name_chosen_arr=[];
foreach ($filter_box_infoRes as $filter_box_infoObj) {
	$filterbox_name_chosen_arr[]=$filter_box_infoObj->filterbox_name;
}

									 
//$filter_box_infoRes="";
$params = ['index' => 'flamingo1', 'type' => 'flamingoproducts1'];
/////////////////////////////////////


foreach ($filter_box_infoRes as $filter_box_infoObj) {
    $filter_box_infoObj->filterbox_name = preg_replace('/\s+/', '_', $filter_box_infoObj->filterbox_name);
    if (isset($_REQUEST[$filter_box_infoObj->filterbox_name . '_filter']) && $_REQUEST[$filter_box_infoObj->filterbox_name . '_filter'] != "") {
        $veryfirst_condition_of_all_filters = "no";
        //$filter_box_infoObj->filterbox_name . '_filter';
		//echo $_REQUEST[$filter_box_infoObj->filterbox_name . '_filter'];
        $color_filter = $_REQUEST[$filter_box_infoObj->filterbox_name . '_filter'];
		//echo $color_filter;
		if($filter_box_infoObj->type==="checkbox"){
		
			$color_filter_arr = explode("|", $color_filter);
			
			$color_filter_param_arr = array();
			foreach ($color_filter_arr as $colorFilterName) {
				$colorFilterName_objRes=$controller->get_filter_id_by_filter_name($colorFilterName);
				foreach ($colorFilterName_objRes as $colorFilterName_obj) {
					//$params["body"]["query"]["bool"]["must"][]=array("match"=>array("filter_" . $filter_box_infoObj->filterbox_name . "_id"=>$colorFilterName_obj->filter_id));
					
					$color_filter_param_arr[]["bool"]["must"][]["match"]["filter_" . $filter_box_infoObj->filterbox_name . "_id"] = $colorFilterName_obj->filter_id;
					
				}
			}
			$params["body"]["query"]["bool"]["must"][]["bool"]["should"] = $color_filter_param_arr;
		}
		
		
    }
}
//echo "<pre>";
///print_r($params);
//echo "</pre>";
$params["from"] = 0;
$params["size"] = 10000;

$query = $es->search($params);

if ($query['hits']['total'] >= 1) {
    $results = $query['hits']['hits'];
} else {
    $results = $query['hits']['hits'];
}


//////////////////////////////// this is for filtering according to price filter added manually starts //////////////////////////////////////////////////
















//print_r($params);

$results_temp_arr=[];
foreach($results as $arr){
	if(in_array($arr["_source"]["inventory_id"],$tagged_inventory_id_arr)){
		$results_temp_arr[]=$arr;
	}
}
//echo "<pre>";
//print_r($results_temp_arr);
//echo "</pre>";
$results=$results_temp_arr;
$rows = count($results);
if (isset($_REQUEST["page_rows"])) {
    $page_rows = $_REQUEST["page_rows"];
} else {
    if (isMobileDevice()) {
        $page_rows = 16;
    } else {
        $page_rows = 24;
    }
}
$last = ceil($rows / $page_rows);

if ($last < 1) {
    $last = 1;
}

// Establish the $pagenum variable

// Get pagenum from URL vars if it is present, else it is = 1

if (isset($_REQUEST['pagenum'])) {
//$pagenum = preg_replace('#[^0-9]#', '', $_REQUEST['pagenum']);
    $pagenum = $_REQUEST['pagenum'];
} else {
    $pagenum = 1;
}

// This makes sure the page number isn't below 1, or more than our $last page
if ($pagenum < 1) {
    $pagenum = 1;
} else if ($pagenum > $last) {
    $pagenum = $last;
}

/////////////////////////
if (isset($_REQUEST["sorting_by"])) {
    $sorting_by = $_REQUEST["sorting_by"];

    if ($sorting_by == "default|SORT_ASC") {
        $sort_field_name = "inventory_id";
        $sort_flag = "SORT_ASC";
    } else {
//echo $sorting_by;
        $sorting_by_arr = explode("|", $sorting_by);
        $sort_field_name = $sorting_by_arr[0];
        if (isset($sorting_by_arr[1])) {
            $sort_flag = $sorting_by_arr[1];
        } else {
            $sort_flag = "SORT_ASC";
        }


    }
} else {
    $sorting_by = "default|SORT_ASC";
    $sort_field_name = "inventory_id";
    $sort_flag = "SORT_ASC";
}

$sort_fields = array();
if (!empty($results)) {
    foreach ($results as $resObj) {
        if (isset($resObj["_source"]["inventory_id"])) {
            if (isset($resObj["_source"][$sort_field_name]))
                $sort_fields[] = $resObj["_source"][$sort_field_name];
				
        }
    }
}

//print_r($sort_fields);

//echo $sort_field_name;
//echo $sorting_by;
//if($pagenum==1){

if (count($sort_fields) > 0) {
    if ($sort_flag == "SORT_DESC") {
        array_multisort($sort_fields, SORT_DESC, $results);
    }
    if ($sort_flag == "SORT_ASC") {
        array_multisort($sort_fields, SORT_ASC, $results);
    }
}
//}
//else{
//array_multisort($sort_fields,$sort_flag , $results);
//}

///////////////////////////

?>
<?php


//echo '<pre>';
//print_r($results);
//echo '</pre>';
//print_r($_REQUEST);
//echo $_SERVER["PHP_SELF"];
foreach ($results as $resObj1) {
    $selling_price_for_price_filter_arr[] = $resObj1["_source"]["combo_sel_price"];
	
	
	
	
    
}
//print_r($selling_price_for_price_filter_arr);

////////// newly added code for discount promotion price filter starts /////////////////

$discount_is_exists = "no";
$cur_result = array_slice($results, ($pagenum - 1) * $page_rows, $page_rows);
$selling_price_for_price_filter_discount_arr = [];
foreach ($cur_result as $searchRes) {
    if (isset($searchRes['_source']['inventory_id'])) {

        $inventory_id = $searchRes['_source']['inventory_id'];
        $product_id = $searchRes['_source']['product_id'];
        $selling_price = $searchRes["_source"]["selling_price"];
        $max_selling_price = $searchRes["_source"]["max_selling_price"];
        $selling_discount = $searchRes["_source"]["selling_discount"];

        $inv_discount_data = $controller->get_default_discount_for_inv($inventory_id, $max_selling_price, $product_id);
        if (!empty($inv_discount_data)) {
            $discount_is_exists = "yes";
            $selling_price_for_price_filter_discount_arr[] = $inv_discount_data["current_price"];
        }


    }
}
if ($discount_is_exists == "yes") {
    $selling_price_for_price_filter_arr = $selling_price_for_price_filter_discount_arr;
}

////////// newly added code for discount promotion price filter starts /////////////////








//////////////////////////////// this is for filtering according to price filter added manually starts //////////////////////////////////////////////////

//print_r($results);
if (!isset($_REQUEST["type_of_filter"])) {
    $type_of_filter = "";
} else {
    $type_of_filter = $_REQUEST["type_of_filter"];
}




if ($type_of_filter != "") {
    $Price_filter_info_for_populating_inventories = $_REQUEST["Price_filter"];
    $Price_filter_info_for_populating_inventories_arr = explode("-", $Price_filter_info_for_populating_inventories);
    $min_Price_filter_info_for_populating_inventories = $Price_filter_info_for_populating_inventories_arr[0];
    $max_Price_filter_info_for_populating_inventories = $Price_filter_info_for_populating_inventories_arr[1];
    $results_temp_arr = array();
///echo "<pre>";
//print_r($results);
//echo "</pre>";
    foreach ($results as $ind => $results_arr) {


        ////////// newly added code for discount promotion price filter starts /////////////////
        $inventory_id = $results[$ind]['_source']['inventory_id'];
        $product_id = $results[$ind]['_source']['product_id'];
        $selling_price = $results[$ind]["_source"]["selling_price"];
        $max_selling_price = $results[$ind]["_source"]["max_selling_price"];
        $selling_discount = $results[$ind]["_source"]["selling_discount"];

        $inv_discount_data = $controller->get_default_discount_for_inv($inventory_id, $max_selling_price, $product_id);
        if (!empty($inv_discount_data)) {
            if ($inv_discount_data["current_price"] >= $min_Price_filter_info_for_populating_inventories && $inv_discount_data["current_price"] <= $max_Price_filter_info_for_populating_inventories) {
                $results_temp_arr[] = $results_arr;
            }
        } ////////// newly added code for discount promotion price filter starts /////////////////
        else {


            if ($results[$ind]["_source"]["combo_sel_price"] >= $min_Price_filter_info_for_populating_inventories && $results[$ind]["_source"]["combo_sel_price"] <= $max_Price_filter_info_for_populating_inventories) {
                $results_temp_arr[] = $results_arr;
            }
        }
    }


    $results = $results_temp_arr;
}

//////////////////////////////// this is for filtering according to price filter added manually ends //////////////////////////////////////////////




/* this block is for setting the min and max default price filter values starts */
if ($type_of_filter == "") {
    if (!empty($selling_price_for_price_filter_arr)) {
        $min_val_of_price_filter = min($selling_price_for_price_filter_arr);
        $max_val_of_price_filter = max($selling_price_for_price_filter_arr);
        if ($min_val_of_price_filter == $max_val_of_price_filter) {
            $min_val_of_price_filter = 0;
        }
    } else {
        $min_val_of_price_filter = 0;
        $max_val_of_price_filter = 0;
    }
    $default_min_val_of_price_filter = $min_val_of_price_filter;
    $default_max_val_of_price_filter = $max_val_of_price_filter;
} else {
    $Price_filter_info_for_populating_inventories = $_REQUEST["Price_filter"];
    $Price_filter_info_for_populating_inventories_arr = explode("-", $Price_filter_info_for_populating_inventories);
    $min_val_of_price_filter = $Price_filter_info_for_populating_inventories_arr[0];
    $max_val_of_price_filter = $Price_filter_info_for_populating_inventories_arr[1];
    $Price_default_info_arr = explode("-", $_REQUEST["Price_default_info"]);
    $default_min_val_of_price_filter = $Price_default_info_arr[0];
    $default_max_val_of_price_filter = $Price_default_info_arr[1];
}
/* this block is for setting the min and max default price filter values ends */










////////////////////////////////////////


?>













<?php








?>


<?php


$cur_result = array_slice($results, ($pagenum - 1) * $page_rows, $page_rows);

////////////////////////////////////////////////////////////////////////
// Establish the $paginationCtrls variable
$paginationCtrls = '';
if ($last != 1) {
    if ($pagenum > 1) {
        $previous = $pagenum - 1;
//$paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$previous.'" aria-label="Previous"><span aria-hidden="true">&laquo; Previous</span></a></li>';
        $paginationCtrls .= '<li><a href="javascript:paginationFun(' . $previous . ')" aria-label="Previous"><span aria-hidden="true">&laquo; Previous</span></a></li>';

        for ($i = $pagenum - 4; $i < $pagenum; $i++) {
            if ($i > 0) {
// $paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'">'.$i.'</a></li>';
                $paginationCtrls .= '<li><a href="javascript:paginationFun(' . $i . ')">' . $i . '</a></li>';
            }
        }
    }
    $paginationCtrls .= '<li class="active"><a href="#">' . $pagenum . '</a></li>';
    for ($i = $pagenum + 1; $i <= $last; $i++) {
//$paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'">'.$i.'</a></li>';
        $paginationCtrls .= '<li><a href="javascript:paginationFun(' . $i . ')">' . $i . '</a></li>';
        if ($i >= $pagenum + 4) {
            break;
        }
    }
    if ($pagenum != $last) {
        $next = $pagenum + 1;
//$paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$next.'" aria-label="Next"><span aria-hidden="true">Next &raquo;</span></a></li>';
        $paginationCtrls .= '<li><a href="javascript:paginationFun(' . $next . ')" aria-label="Next"><span aria-hidden="true">Next &raquo;</span></a></li>';
    }
}
//////////////////////////////////////////////////////////////////////
/*echo '<pre>';
print_r($results);
echo '</pre>';*/
//exit;


?>



<?php
	
/* admin combo pack setup */
$combo_pack_min_items=2;
$combo_pack_discount=0;
$combo_pack_shipping_charge=0;
$combo_pack_status='active';

if(!empty($adm_settings)){
    $combo_pack_min_items=$adm_settings->adm_combo_pack_min_items;
    $combo_pack_discount=$adm_settings->adm_combo_pack_discount;
    $combo_pack_shipping_charge=$adm_settings->adm_combo_pack_shipping_charge;
    $combo_pack_status=$adm_settings->adm_combo_pack_status;
}

/* admin combo pack setup */
?>
<input type="hidden" name="combo_pack_min_items" value="<?php echo $combo_pack_min_items; ?>" id="combo_pack_min_items">
<input type="hidden" name="combo_pack_discount" value="<?php echo $combo_pack_discount; ?>" id="combo_pack_min_items">
<input type="hidden" name="combo_pack_shipping_charge" value="<?php echo $combo_pack_shipping_charge; ?>" id="combo_pack_shipping_charge">
<input type="hidden" name="combo_pack_status" value="<?php echo $combo_pack_status; ?>" id="combo_pack_status">

<?php 
$url = base_url();
$var = "search_category_combo";
$rand1 = $controller->sample_code();
$rand2 = $controller->sample_code();
$rand3 = $controller->sample_code();
$s = "/";
$temp = "search";
$home = "{$url}{$temp}";
?>
<div class="columns-container">
    <div class="container" id="columns">
        <!-- row -->
        <div class="row">
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <?php
				
				
				
			
				
                if (empty($cur_result)) {
                    ?>
                    <div id="view-product-list" class="view-product-list">
                        <div class="row" style="height:100vh;display:flex;align-items:center;text-align:center;">
                            <div class="col-md-12">
                                <h4>
                                    There are no related Products for choosen products. Please Checkout!
                                </h4>
                            </div>
                        </div>
                    </div>
                    <?php
                } 
				else {
										
                    ?>
                    <!-- view-product-list-->
                    <div id="view-product-list" class="view-product-list">


                        <!-- breadcrumb -->

                        <div class="breadcrumb clearfix" style="display: none;">
                            <a class="home" href="<?php echo base_url(); ?>" title="Return to Home">Home</a>
                            <?php
                            if ($pcat_id != "") {
                                ?>

                                <?php

                                $parent_cat_infoObj = $controller->get_parent_cat_info($pcat_id);
                                if (!empty($parent_cat_infoObj)) {

                                    ?>
                                    <span class="navigation-pipe">&nbsp;</span>
                                    <span class="navigation_page">
<?php
$url1 = "{$url}{$var}{$s}{$rand1}{$parent_cat_infoObj->pcat_id}";
echo '<a href="' . $url1 . '" title="' . $parent_cat_infoObj->pcat_name . '">' . $parent_cat_infoObj->pcat_name . '</a>';
?>
</span>
                                    <?php
                                } else {
                                    $url1 = "{$url}{$var}{$s}{$rand1}{$pcat_id}";
//echo "All Categories";
                                }
                                ?>

                                <?php
                            }
                            ?>
                            <?php
                            if ($cat_id != "") {
                                ?>
                                <span class="navigation-pipe">&nbsp;</span>
                                <span class="navigation_page">
<?php
$cat_infoObj = $controller->get_cat_info($cat_id);
$url2 = "{$url1}{$s}{$rand2}{$cat_infoObj->cat_id}";
//echo $cat_infoObj->cat_name;
echo '<a href="' . $url2 . '" title="' . $cat_infoObj->cat_name . '">' . $cat_infoObj->cat_name . '</a>';
?>
</span>
                                <?php
                            }
                            ?>
                            <?php
                            if ($subcat_id != "") {
                                ?>
                                <span class="navigation-pipe">&nbsp;</span>
                                <span class="navigation_page">
<?php
$subcat_infoObj = $controller->get_subcat_info($subcat_id);
$url3 = "{$url2}{$s}{$rand3}{$subcat_infoObj->subcat_id}";
//echo $subcat_infoObj->subcat_name;
echo '<a href="' . $url3 . '" title="' . $subcat_infoObj->subcat_name . '">' . $subcat_infoObj->subcat_name . '</a>';
?>
</span>
                                <?php
                            }
                            ?>
                        </div>
                        <!-- ./breadcrumb -->

                        <h2 class="page-heading" style="display: none;">
<span class="page-heading-title">
<!-- page title starts -->
<?php
if ($subcat_id != "") {

    $subcat_infoObj = $controller->get_subcat_info($subcat_id);
    echo $subcat_infoObj->subcat_name;

} else if ($cat_id != "") {

    $cat_infoObj = $controller->get_cat_info($cat_id);
    echo $cat_infoObj->cat_name;

} else if ($pcat_id != "") {
    $parent_cat_infoObj = $controller->get_parent_cat_info($pcat_id);
    if (!empty($parent_cat_infoObj)) {
        echo $parent_cat_infoObj->pcat_name;
    } else {
        echo "All Categories";
    }
}

?>
<!-- page title ends -->
</span>
                        </h2>
                        
                        <!---ff--->






                        <!-- PRODUCT LIST -->
                        <ul class="row product-list grid">
                            <?php
                            //shuffle($cur_result);
							$inventories_display_count=0;
                            foreach ($cur_result as $searchRes) {
                                if (isset($searchRes['_source']['inventory_id'])) {
										

                                    if(isset($searchRes["_source"]["selling_price"])){

                                    $inventory_id = $searchRes['_source']['inventory_id'];
                                    $product_id = $searchRes['_source']['product_id'];
                                    $selling_price = $searchRes["_source"]["selling_price"];
                                    $max_selling_price = $searchRes["_source"]["max_selling_price"];
                                    $selling_discount = $searchRes["_source"]["selling_discount"];

                                    $inv_discount_data = $controller->get_default_discount_for_inv($inventory_id, $max_selling_price, $product_id);
///////////////  No of Offers starts ///////////////
                                    $promo_numbers = 0;
                                    $promotions = $controller->get_number_of_offers($inventory_id);
                                    //echo $inventory_id;
                                    $inventory_product_info_obj = $controller->get_inventory_product_info_by_inventory_id($inventory_id);

                                    if (!empty($promotions) && ($inventory_product_info_obj->stock_available > 0)) {
                                        $i = 1;
                                        $defaultDiscounts_num = 0;
                                        foreach ($promotions as $promotion) {
                                            foreach ($promotion as $promo) {

                                                if (preg_match_all('/\d+(?=%)/', $promo->get_type, $match) && ($promo->to_buy == 1)) {
                                                    $i++;
                                                    if ($promo->to_buy == 1) {
                                                        $defaultDiscounts_num = 1;
                                                    }
                                                }
                                            }
                                        }

                                        if ($defaultDiscounts_num != 0) {
                                            $promo_numbers = count($promotions) - $defaultDiscounts_num;
                                        } else {
                                            $promo_numbers = count($promotions);
                                        }

                                    }
                                    
                                    /*added*/
                                    if (empty($inv_discount_data) && ($selling_discount>0)){
                                        //$promo_numbers=($promo_numbers+1);
                                    }
                                    /*added*/
//////////////   No of offers ends    ///////////////
									$inventories_display_count++;
                                    ?>
                                    <li class="col-xs-6 col-sm-3">
                                        <div class="product-container selectProduct"
                                             data-id="<?php echo ucwords($searchRes["_source"]["product_name"]) ?>"
                                             data-title="<?php echo $searchRes['_source']['inventory_id']; ?>">
                                            <div class="left-block">
                                                <?php
                                                //echo $searchRes['_id'];
                                                $rand_1 = $controller->sample_code();
                                                $rand_2 = $controller->sample_code();
                                                ?>

                                                <a href="<?php echo base_url() ?>detail/<?php echo "{$rand_1}{$searchRes['_source']['inventory_id']}"; ?>">


                                                    <img class="img-responsive productImg_<?php echo $searchRes['_source']['inventory_id']; ?>"
                                                         alt="productsku"
                                                         src="<?php echo base_url() . $searchRes["_source"]["common_image"] ?>"/>

                                                </a>
                                                <div class="quick-view">
                                                    <?php
                                                    if ($this->session->userdata("customer_id")) {
                                                        if ($controller->check_wishlist($searchRes['_source']['inventory_id']) > 0) {
                                                            ?>
                                                            <script>
                                                                $(document).ready(function () {
                                                                    $("#wishlist_" +<?php echo $searchRes['_source']['inventory_id'];?>).css({"color": ""});
                                                                    $("#wishlist_" +<?php echo $searchRes['_source']['inventory_id'];?>).css({"color": "#ff4343;"});
                                                                });
                                                            </script>
                                                            <?php
                                                            $val = "Added to Wishlist";
                                                            $style_wishlist_label = "style='color:#eda900;'";
                                                        } else {
                                                            $val = "Add to Wishlist";
                                                            $style_wishlist_label = "";
                                                        }
                                                    } else {
                                                        $val = "Add to Wishlist";
                                                        $style_wishlist_label = "";
                                                    }
                                                    ?>
                                                    <?php
                                                    if ($this->session->userdata("customer_id")) {
                                                        ?>
                                                        <span style="cursor:pointer;"
                                                              id="anchor_<?php echo $searchRes['_source']['inventory_id']; ?>"
                                                              onclick="add_to_wishlistFun(<?php echo $searchRes['_source']['inventory_id']; ?>);return false;"
                                                              title="<?php echo $val; ?>" class="wishlist">
<i id="<?php echo "wishlist_" . $searchRes['_source']['inventory_id']; ?>" class="fa fa-heart"
   style='color:#c2c2c2;font-size: 1.2em;'></i>
</span>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <span style="cursor:pointer"
                                                              id="anchor_<?php echo $searchRes['_source']['inventory_id']; ?>"
                                                              onclick="add_to_wishlistFun(<?php echo $searchRes['_source']['inventory_id']; ?>);return false;"
                                                              title="<?php echo $val; ?>" class="wishlist"><i
                                                                    id="<?php echo "wishlist_" . $searchRes['_source']['inventory_id']; ?>"
                                                                    class="fa fa-heart"
                                                                    style='color:#c2c2c2;font-size: 1.2em;'></i></span>
                                                        <?php
                                                    }
                                                    ?>

                                                </div>

                                                <?php //if (!empty($inv_discount_data)) { ?>
                                                <?php if (0) { ?>
                                                    <div class="price-percent-reduction2" <?php if ($inv_discount_data['discount'] < 10) {
                                                        echo "style='padding-top:18px'";
                                                    } ?>>
                                                        <?php echo $inv_discount_data['discount'] ?>% OFF


                                                    </div>
                                                <?php }else{ ?>

                                                <?php
                                                if($combo_pack_discount>0){
                                                            ?>
                                                    <div class="price-percent-reduction2" <?php if ($combo_pack_discount < 10) {
                                                        //echo "style='padding-top:18px'";
                                                    } ?>>
                                                        <?php echo round($combo_pack_discount); ?>% OFF
                                                    </div>

                                                        <?php
                                                }
                                                ?>
                                                <?php
                                                }
                                                if ($promo_numbers > 0) {
                                                    ?>
                                                    <div class="price-percent-reduction_offers">
                                                        <?php

                                                        if ($promo_numbers > 1) {
                                                            echo $promo_numbers . ' Offers';
                                                        } else {
                                                            echo $promo_numbers . ' Offer';
                                                        }
                                                        ?>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                            </div>

                                            <div class="right-block heightmatched_sku">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <h5 id="product_name_<?php echo $searchRes['_source']['inventory_id']; ?>" class="product-name heightmatched"
                                                            style="font-size:1.05em;padding-bottom:0!important;word-break:break-word;line-height:1.3;">
                                                            <a href="<?php echo base_url() ?>detail/<?php echo "{$rand_1}{$searchRes['_source']['inventory_id']}"; ?>"
                                                               title="<?php
                                                               //$str = $searchRes["_source"]["product_name"];
                                                               $str = ($searchRes["_source"]["sku_name"] !='') ? $searchRes["_source"]["sku_name"] : $searchRes["_source"]["product_name"];
                                                               $str = str_replace("\r\n", '', $str);
                                                               $str = str_replace('\r\n', '', $str);
                                                               echo ucwords($str);

                                                               ?>">

                                                                <?php
                                                                
                                                                //$str = $searchRes["_source"]["product_name"];
                                                                $str = ($searchRes["_source"]["sku_name"] !='') ? $searchRes["_source"]["sku_name"] : $searchRes["_source"]["product_name"];
                                                                $str = str_replace("\r\n", '', $str);
                                                                $str = str_replace('\r\n', '', $str);
                                                                echo ucwords($str);
                                                                ?>

                                                                <!---added attributes ---->
                                                                <span style="<?php echo ($searchRes["_source"]["sku_name"]!='') ? 'display:none;' : ''; ?>">
                                                                <?php
                                                                $attr_str='';
                                                                $color_attribute_is_there = "no";

                                                                if(isset($inventory_product_info_obj->attribute_1)){
                                                                if (strtolower($inventory_product_info_obj->attribute_1) == "color") {
                                                                    $color_attribute_is_there = "yes";
                                                                    $attr_str.= " - " . explode(":", $inventory_product_info_obj->attribute_1_value)[0];
                                                                } if (strtolower($inventory_product_info_obj->attribute_2) == "color") {
                                                                    $color_attribute_is_there = "yes";
                                                                    $attr_str.= " - " . explode(":", $inventory_product_info_obj->attribute_2_value)[0];
                                                                } if (strtolower($inventory_product_info_obj->attribute_3) == "color") {
                                                                    $color_attribute_is_there = "yes";
                                                                    $attr_str.= " - " . explode(":", $inventory_product_info_obj->attribute_3_value)[0];
                                                                } if (strtolower($inventory_product_info_obj->attribute_4) == "color") {
                                                                    $color_attribute_is_there = "yes";
                                                                    $attr_str.= " - " . explode(":", $inventory_product_info_obj->attribute_4_value)[0];
                                                                }
                                                                ?>
                                                                <?php
                                                                if ($color_attribute_is_there == "yes") {
                                                                    if (strtolower($inventory_product_info_obj->attribute_1) != "color") {
                                                                        if ($inventory_product_info_obj->attribute_1_value != "") {
                                                                            $attr_str.= " - " . $inventory_product_info_obj->attribute_1_value;
                                                                        }
                                                                    } if (strtolower($inventory_product_info_obj->attribute_2) != "color") {
                                                                        if ($inventory_product_info_obj->attribute_2_value != "") {
                                                                            $attr_str.= " - " . $inventory_product_info_obj->attribute_2_value;
                                                                        }
                                                                    } if (strtolower($inventory_product_info_obj->attribute_3) != "color") {
                                                                        if ($inventory_product_info_obj->attribute_3_value != "") {
                                                                            $attr_str.= " - " . $inventory_product_info_obj->attribute_3_value;
                                                                        }
                                                                    } if (strtolower($inventory_product_info_obj->attribute_4) != "color") {
                                                                        if ($inventory_product_info_obj->attribute_4_value != "") {
                                                                            $attr_str.= " - " . $inventory_product_info_obj->attribute_4_value;
                                                                        }
                                                                    }
                                                                } else {
                                                                    $noncolor_attribute_first_is_there = "no";
                                                                    $noncolor_attribute_second_is_there = "no";

                                                                    if (strtolower($inventory_product_info_obj->attribute_1) != "color") {
                                                                        $noncolor_attribute_first_is_there = "yes";
                                                                        $attr_str.= " - " . $inventory_product_info_obj->attribute_1_value;
                                                                    }
                                                                    if (strtolower($inventory_product_info_obj->attribute_2) != "color") {
                                                                        if ($noncolor_attribute_first_is_there == "yes") {
                                                                            $noncolor_attribute_second_is_there = "yes";
                                                                            if ($inventory_product_info_obj->attribute_2_value != "") {
                                                                                $attr_str.= " - " . $inventory_product_info_obj->attribute_2_value;
                                                                            }
                                                                        } else {
                                                                            $noncolor_attribute_first_is_there = "yes";
                                                                            $attr_str.= " - " . $inventory_product_info_obj->attribute_2_value;
                                                                        }
                                                                    }
                                                                    if (strtolower($inventory_product_info_obj->attribute_3) != "color") {
                                                                        if ($noncolor_attribute_first_is_there == "no" || $noncolor_attribute_second_is_there == "no") {
                                                                            if ($noncolor_attribute_first_is_there == "yes") {
                                                                                $noncolor_attribute_second_is_there = "yes";
                                                                                if ($inventory_product_info_obj->attribute_3_value != "") {
                                                                                    $attr_str.= " - " . $inventory_product_info_obj->attribute_3_value;
                                                                                }
                                                                            } else {
                                                                                $noncolor_attribute_first_is_there = "yes";
                                                                                $attr_str.= " - " . $inventory_product_info_obj->attribute_3_value;
                                                                            }
                                                                        }
																		else{
																			if ($inventory_product_info_obj->attribute_3_value != "") {
																				$attr_str.= " - " . $inventory_product_info_obj->attribute_3_value;
																			}
																		}
                                                                    }
                                                                    if (strtolower($inventory_product_info_obj->attribute_4) != "color") {
                                                                        if ($noncolor_attribute_first_is_there == "no" || $noncolor_attribute_second_is_there == "no") {
                                                                            if ($noncolor_attribute_first_is_there == "yes") {
                                                                                $noncolor_attribute_second_is_there = "yes";
                                                                                if ($inventory_product_info_obj->attribute_4_value != "") {
                                                                                    $attr_str.= " - " . $inventory_product_info_obj->attribute_4_value;
                                                                                }
                                                                            } else {
                                                                                $noncolor_attribute_first_is_there = "yes";
                                                                                $attr_str.= " - " . $inventory_product_info_obj->attribute_4_value;
                                                                            }
                                                                        }
																		else{
																			if ($inventory_product_info_obj->attribute_4_value != "") {
																				$attr_str.= " - " . $inventory_product_info_obj->attribute_4_value;
																			}
																		}
                                                                    }
                                                                }
                                                                }
                                                                $color_attribute_is_there = "no";
                                                                $noncolor_attribute_first_is_there = "no";
                                                                $noncolor_attribute_second_is_there = "no";

                                                                echo $attr_str;
                                                                ?>
                                                                <!---added attributes ---->

                                                                </span>
                                                            </a>
                                                        </h5>
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <h5 class="product-name pb-2"
                                                            style="font-size:1em;color:#95918c;">
                                                            <b><?php echo "{$searchRes['_source']['brand_name']}"; ?></b>
                                                        </h5>
                                                        <?php
                                                        /*
<h5 class="attribute-col">
<?php

if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_1"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_1_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_2"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_2_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_3"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_3_value"])[0];
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_4"])=="color"){
echo "  ".explode(":",$controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_4_value"])[0];
}



if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_1"])!="color"){
if($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_1_value"]!=""){
echo "<h5 class='attribute-col' style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_1_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_2"])!="color"){
if($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_2_value"]!=""){
echo "<h5 class='attribute-col'  style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_2_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_3"])!="color"){
if($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_3_value"]!=""){
echo "<h5 class='attribute-col'  style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_3_value"])."</h5>";
}
}
else if(strtolower($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_4"])!="color"){
if($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_4_value"]!=""){
echo "<h5 class='attribute-col' style='line-height:1.3;'>".trim($controller->get_inventory_info_by_inventory_id($searchRes["_source"]["inventory_id"])["attribute_4_value"])."</h5>";
}
}



?>
</h5>
*/
                                                        ?>
                                                    </div>
                                                </div>


                                                <div class="content_price">
                                                    <?php //if (!empty($inv_discount_data)) { ?>
                                                    <?php if (0) { ?>
                                                        <span class="price product-price"><?php echo curr_sym; ?><?php echo $inv_discount_data['current_price'] ?></span>
                                                        <span class="price old-price"><?php echo curr_sym; ?><?php echo $inv_discount_data['inventory_price'] ?></span>
                                                        <small style="line-height: 1.8;">(Inclusive of Taxes)</small>
                                                    <?php } else { ?>

                                                        <?php

                                                        $max_price=$max_selling_price;

                                                        if($combo_pack_discount>0){
                                                            $sel_price=round($max_price-($max_price*$combo_pack_discount/100));
                                                        }else{
                                                            $sel_price= $searchRes["_source"]["max_selling_price"];
                                                        }

                                                        $original_price=$max_selling_price;


                                                        if (get_pricetobeshown() == "selling_price") {
                                                            ?>
                                                            <span class="price product-price"><?php echo curr_sym; ?><?php echo $sel_price; ?></span>
                                                            
                                                            <?php
                                                            if(intval($sel_price)<intval($max_price)){

                                                            ?>
                                                            <span class="price old-price"><?php echo curr_sym; ?><?php echo round($max_price); ?></span>
                                                            <?php

                                                            }
                                                            ?>
                                                            <small>(Inclusive of Taxes)</small>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <span class="price product-price"><?php echo curr_sym; ?><?php echo $searchRes["_source"]["base_price"] ?> <font
                                                                        style="font-size:0.6em;line-height:2.4"> + GST</font></span>
                                                            <small>(Inclusive of Taxes)</small>
                                                            <?php
                                                        }
                                                        ?>

                                                    <?php } ?>

                                                </div>
												<div class="attributes row mb-10" style="line-height: 2.4;">

                                                <div class="col-md-12">
                                                    <div class="attribute-label widthmatched" style="display:inline-block;">Quantity</div>

                                                    <div class="input-group qty-arrange" style="display:inline-block;">
                                                        <input onkeyup="incrementdecrementtimerfun(<?php echo $inventory_product_info_obj->moq; ?>,<?php echo $inventory_product_info_obj->max_oq; ?>,this,<?php echo $inventory_id; ?>);"
                                                               class="form-control input-number text-center"
                                                               id="purchase_qty_input_<?php echo $inventory_id; ?>" allow-only-numbers type="number"
                                                               value="<?php echo $inventory_product_info_obj->moq; ?>" style="max-width:5em"
                                                               ng-paste="$event.preventDefault()"
                                                               maxlength="{{qty_maxLength}}"
                                                               onkeypress="return isNumber(event)"
                                                               length-checker="<?php echo $inventory_product_info_obj->moq; ?>"
                                                               onblur="inventory_moq_resetFun(<?php echo $inventory_product_info_obj->moq; ?>,<?php echo $inventory_id; ?>);incrementdecrementtimerfun(<?php echo $inventory_product_info_obj->moq; ?>,<?php echo $inventory_product_info_obj->max_oq; ?>,this,<?php echo $inventory_id; ?>);">

                                                    </div>
												<p style="color:#f00" id="quantity_status_info">
										<span style="color:blue">Enter between <?php echo $inventory_product_info_obj->moq;?> and <?php echo $inventory_product_info_obj->max_oq;?></span>

									</p>
                                                </div>
                                            </div>
                                                <?php
                                                if (get_pricetobeshown() == "selling_price") {
                                                    ?>

                                                    <!-- <div class="row" style="display:none;">
                                                        <div class="col-md-12">
                                                          
                                                                <div class="checkbox mb-0">
                                                                    <label for="inventory_<?php echo $inventory_id; ?>">
                                                                        <input type="checkbox" value=""
                                                                               class="compare addToCompare"
                                                                               type="checkbox"
                                                                               id="inventory_<?php echo $inventory_id; ?>">
                                                                        <span class="cr"><i
                                                                                    class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                        <b>Add to Compare</b> 
                                                                    </label>
                                                                </div>


                                                        </div>
                                                    </div> -->
                                                    
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                                <div class="checkbox mb-0">
                                                                    <label for="combo_inventory_<?php echo $inventory_id; ?>">
                                                                        <input type="checkbox" value=""
                                                                               class="addToCombo"
                                                                               type="checkbox"
                                                                               id="combo_inventory_<?php echo $inventory_id; ?>" data_id="<?php echo $inventory_id; ?>" data_price="<?php echo round($sel_price); ?>" data_max_price="<?php echo round($max_price); ?>" data_saved_price="<?php echo round($max_price-$sel_price); ?>" data_sku_id="<?php echo $searchRes["_source"]["sku_id"] ?>" data_image='<?php echo base_url().$searchRes["_source"]["common_image"]; ?>' data_name='<?php echo $str.$attr_str; ?>' >
                                                                        <span class="cr"><i
                                                                                    class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                        <b>Add to Combo Pack</b> 
                                                                    </label>
                                                                </div>
                                                        </div>
                                                    </div>


                                                <?php } ?>

                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }

                                }
                            }
                            ?>
                        </ul>
						
						
						<?php
							if($inventories_display_count==0){
						?>
						 <div class="row" style="height:100vh;display:flex;align-items:center;text-align:center;">
                            <div class="col-md-12">
                                <h4>
                                    There are no related Products for choosen products. Please Checkout!
                                </h4>
                            </div>
                        </div>
						<?php
								}
						?>
						
						
                        <!-- ./PRODUCT LIST -->
                    </div>
                    <!-- ./view-product-list-->
                    <div class="sortPagiBar">
                        <div class="bottom-pagination">
                            <nav>
                                <ul class="pagination">
                                    <?php echo $paginationCtrls; ?>
                                </ul>
                            </nav>
                        </div>
                    </div>

                    <?php
                }
                ?>

            </div>


            <?php include 'recently_viewed.php'; ?>

            <script>
                function add_to_wishlistFun(inventory_id) {
                    document.getElementById('anchor_' + inventory_id).style.pointerEvents = 'none';

                <?php
                    if(!$this->session->userdata("customer_id")){
                    $this->session->set_userdata("cur_page", current_url());
                    ?>
                    if (confirm("Please login to add wishlist!")) {
                        location.href = "<?php echo base_url()?>login";
                    }
                    /*bootbox.confirm({
message: "Please login to add wishlist",
size: "small",
callback: function (result) {
if(result){
location.href="<?php echo base_url()?>login";
}
}
});*/

                    <?php
                    }else{
                    ?>
                    $.ajax({
                        url: "<?php echo base_url()?>add_to_wishlist",
                        type: "post",
                        dataType: "json",
                        data: "inventory_id=" + inventory_id,
                        success: function (data) {
                            if (data.exists == "no") {
                                alert('Added to wishlist');

                                /*bootbox.alert({
                                    size: "small",
                                    message: 'Added to wishlist',
                                });*/
                                $("#wishlist_" + inventory_id).css({"color": ""});
                                $("#wishlist_" + inventory_id).css({"color": "#ff4343;"});
                                $("#wishlist_label_" + inventory_id).html("Added to wishlist");
                                $("#anchor_" + inventory_id).attr({"title": "Added to wishlist"});
                                $("#wishlist_label_" + inventory_id).css({"color": "#ff4343;"});

                            } else if (data.exists == "yes") {

                                alert('Removed from wishlist');

                               /* bootbox.alert({
                                    size: "small",
                                    message: 'Removed from wishlist',
                                });*/

                                $("#wishlist_" + inventory_id).css({"color": ""});
                                $("#wishlist_" + inventory_id).css({"color": "#C2C2C2"});
                                $("#wishlist_label_" + inventory_id).html("Removed from wishlist");
                                $("#anchor_" + inventory_id).attr({"title": "Removed from wishlist"});
                                $("#wishlist_label_" + inventory_id).css({"color": "#C2C2C2;"});


                            } else {

                                alert('Error');

                               /* bootbox.alert({
                                    size: "small",
                                    message: 'Error',
                                });*/
                            }
                            document.getElementById('anchor_' + inventory_id).style.pointerEvents = 'auto';
                        }
                    });

                    <?php } ?>
                }
            </script>


            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>


<script>
    

   

    function paginationFun(pn) {
        $("input[name='pagenum']").val(pn);
        document.searchForm.submit();
    }

    

    function filter_activity(type_of_filter, filter_names) {
        /*user filter activity*/

        var start = new Date();
        start = start.getTime();

        level_type = '<?php echo (isset($level_type)) ? $level_type : ''; ?>';
        level_name = '<?php echo (isset($searched_key)) ? $searched_key : ''; ?>';
        cat_current = '<?php echo (isset($cat_current)) ? $cat_current : ''; ?>';
        cat_current_id = '<?php echo (isset($cat_current_id)) ? $cat_current_id : ''; ?>';

        if (level_type == "pcat_id") {
            level_type = "pcat";
        }
        if (level_type == "cat_id") {
            level_type = "cat";
        }
        if (level_type == "subcat_id") {
            level_type = "subcat";
        }

        /*alert(level_type);
alert(level_name);
alert(cat_current);
alert(cat_current_id);*/

        temp = {
            "level_type": level_type,
            "level_name": level_name,
            "name": cat_current,
            "id": cat_current_id,
            "filter_group": type_of_filter,
            "checked_filters": filter_names,
            "time": start
        };

        var r_final = localStorage.getItem("filter");

        if (r_final != null) {
            var stored = JSON.parse(localStorage.getItem("filter"));
            stored.push(temp);
            localStorage.setItem("filter", JSON.stringify(stored));
        } else {
            var obj = [];
            obj.push(temp);
            localStorage.setItem("filter", JSON.stringify(obj));
        }

        var result = JSON.parse(localStorage.getItem("filter"));
//alert(JSON.stringify(result));

//return false;

        /*user filter activity*/
    }

</script>

<form method="post" name="searchForm">
	
	
	
    <input type="hidden" name="search_keyword" value="<?php if (isset($_REQUEST["search_keyword"])) {
        echo $_REQUEST["search_keyword"];
    } ?>">
    <input type="hidden" name="search_cat" value="<?php if (isset($_REQUEST["search_cat"])) {
        echo $_REQUEST["search_cat"];
    } ?>">
    <input type="hidden" name="page_rows" value="<?php echo $page_rows; ?>">
    <input type="hidden" name="pagenum" value="<?php echo $pagenum; ?>">
    <input type="hidden" name="sorting_by" value="<?php echo $sorting_by; ?>">

    <input type="hidden" name="pcat_id" value="<?php echo $pcat_id; ?>">
    <input type="hidden" name="cat_id" value="<?php echo $cat_id; ?>">
    <input type="hidden" name="subcat_id" value="<?php echo $subcat_id; ?>">
    <input type="hidden" name="brand_id" value="<?php echo $brand_id; ?>">
    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
    <input type="hidden" name="inventory_id" value="<?php echo $inventory_id; ?>">
    <input type="hidden" name="cat_current" value="<?php echo $cat_current; ?>">
    <input type="hidden" name="cat_current_id" value="<?php echo $cat_current_id; ?>">
    <input type="hidden" name="level_type" value="<?php echo $level_type; ?>">
    <input type="hidden" name="level_name" value="<?php echo $searched_key; ?>">
    <input type="hidden" name="level_value" value="<?php echo $level_value; ?>">
	
	
	<input type="hidden" name="filterbox_type" id="filterbox_type">

    
	<input type="hidden" name="type_of_filter" id="type_of_filter"
           value="<?php if (isset($_REQUEST["type_of_filter"]) && $_REQUEST["type_of_filter"] != "") {
               echo $_REQUEST["type_of_filter"];
           } ?>">
		   
		   
	

		   

    <?php
    if (!empty($filter_box_infoRes)) {
        foreach ($filter_box_infoRes as $filter_box_infoObj) {
            $filter_box_infoObj->filterbox_name = preg_replace('/\s+/', '_', $filter_box_infoObj->filterbox_name);

            ?>
            <input type="hidden" name="<?php echo $filter_box_infoObj->filterbox_name; ?>_filter"
                   value="<?php if (isset($_REQUEST[$filter_box_infoObj->filterbox_name . '_filter']) && $_REQUEST[$filter_box_infoObj->filterbox_name . '_filter'] != "") {
                       echo $_REQUEST[$filter_box_infoObj->filterbox_name . "_filter"];
                   } ?>">

            <input type="hidden" name="<?php echo $filter_box_infoObj->filterbox_name; ?>_prev"
                   value="<?php if (isset($color_prev_arr[$filter_box_infoObj->filterbox_name])) {
                       echo implode(",", $color_prev_arr[$filter_box_infoObj->filterbox_name]);
                   } ?>">
            <?php

        }
    }
    ?>
	
	<?php
		/* Price filter hidden fields after form submit starts */
	?>
    <?php
    if ($type_of_filter == "") {
        $Price_default_info = $default_min_val_of_price_filter . "-" . $default_max_val_of_price_filter;
    } else {
        $Price_default_info = $_REQUEST["Price_default_info"];
    }
    ?>
    <input type="hidden" name="Price_default_info" value="<?php echo $Price_default_info; ?>">
    <input type="hidden" name="Price_filter"
           value="<?php if (isset($_REQUEST['Price_filter']) && $_REQUEST['Price_filter'] != "") {
               echo $_REQUEST['Price_filter'];
           } else {
               echo $Price_default_info;
           } ?>">
		   
		<?php
		/* Price filter hidden fields after form submit ends */
	?>
	
	
	
	

	

	
	

</form>

<!--preview combo pack checkout panel-->

<div class="container" style="display:none;width:100%;" id="combo_products_div">
    <div class="comboPanle">
        <div class="row">
            <div class="col-md-3 text-center">
                <span class="combo_lineheight"><h4>Combo Pack</h4> 
                <span class="combo_lineheight" id="combo_line_text"><b><span id="no_of_products_count_combo">0</span></b> product(s) added to the cart</small>
                <br>
                    <small class="combo_lineheight text-danger" id="combo_line_text1"> <i class="fa fa-info-circle" aria-hidden="true"></i> Minimum <?php echo $combo_pack_min_items; ?> Products should be added</small>
                </span>
            </div>
            <div class="comboPan col-md-4 cursor-pointer">
            </div>
            <div class="col-md-4 text-center">
                <div class="col-md-6">
                   
                    Total Price : <?php echo curr_sym; ?><span id="combo_total"></span>
                    <?php if(intval($combo_pack_discount)>0){
                        ?>
                        <br><small class="text-success">(<?php echo round($combo_pack_discount); ?>% discount is applied)</small>
                        <?php 
                    }
                    ?>
                    <?php if(intval($combo_pack_discount)>0){
                        ?>
                    <br>Shipping Charge : <?php echo curr_sym; ?><?php echo round($combo_pack_shipping_charge); ?>
                    <?php }else{
                        ?>
                    <br> <span class="text-success">Free Shipping</span>
                        <?php 
                    } ?>

                    <br>Grand Total : <b><?php echo curr_sym; ?><span id="combo_grand_total"></span></b>
                </div>
                <div class="col-md-6">
                &nbsp;
                <!-- <form method="post" action="<?php echo base_url() ?>combo_inventories"> -->
                    <button type="button" class="button btn-lg notActive comboBtn" disabled onclick="proceedCheckout()"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> Checkout <span id="no_of_products_combo" class="badge"></span>
                    </button>
					<a href="<?php echo base_url()?>catalog_combo" class="btn preventDflt" type="button" title="Back to Combo Page"><small style="color: #052670;"><u>Go Back</u></small></a>
                    <!--<a href="#" class="btn preventDflt" type="button" onclick="clearComboPanel()" title="Clear all items in the Combo Pack list"><small style="color: red;"><u>Clear All</u></small></a>-->
                    <input type="hidden" name="inventory_id_list_for_combo" id="inventory_id_list_for_combo" value="">
                <!-- </form> -->
                </div>
            </div>

        </div>

    </div>
</div>

<!--end of preview combo pack checkout panel-->


<!--preview panel-->
<div class="container" style="display:none; ;">
    <div class="comparePanle">
        <div class="row">
            <div class="col-md-3 text-center">
                <span class="compare_lineheight">Compare Products <small class="compare_lineheight"
                                                                         id="compare_line_text"> (Add <span
                                id="no_of_products_count_compare">0</span> more to compare )</small></span>
            </div>
            <div class="comparePan col-md-5 cursor-pointer">
            </div>
            <div class="col-md-4 text-center">
                &nbsp;
                <form method="post" action="<?php echo base_url() ?>compare_inventories">
                    <button type="submit" class="button btn-sm notActive cmprBtn" disabled>Compare <span
                                id="no_of_products_compare" class="badge"></span></button>
                    <a href="#" class="btn preventDflt" type="button" onclick="clearComparePanel()"><small>Clear
                            All</small></a>
                    <input type="hidden" name="inventory_id_list_for_compare" id="inventory_id_list_for_compare"
                           value="">
                </form>
            </div>

        </div>

    </div>
</div>
<!--end of preview panel-->

<!-- comparision popup-->
<div id="id01" class="w3-animate-zoom w3-white w3-modal modPos">
    <div class="w3-container">
        <a onclick="document.getElementById('id01').style.display='none'"
           class="whiteFont w3-padding w3-closebtn closeBtn">&times;</a>
    </div>
    <div class="w3-row contentPop w3-margin-top">
    </div>

</div>
<!--end of comparision popup-->

<!--  warning model  -->
<div id="WarningModal" class="w3-modal">
    <div class="w3-modal-content warningModal">
        <header class="w3-teal panel-heading">
            <h3><span>&#x26a0;</span>&nbsp;Error</h3>
        </header>
        <div class="w3-container">
            <p class="lead text-center">Maximum of Three products are allowed for comparision</p>
        </div>
        <footer class="w3-container w3-right-align">
            <button id="warningModalClose" onclick="document.getElementById('id01').style.display='none'"
                    class="w3-btn w3-hexagonBlue w3-margin-bottom preventDflt">Close
            </button>
        </footer>
    </div>
</div>
<!--  end of warning model  -->


<script>
    if (localStorage.getItem("comparePan") === null || localStorage.getItem("comparePan").trim() == "") {
    } else {
        $(".comparePanle").show();
        $("#inventory_id_list_for_compare").val(localStorage.getItem("comparePan_inventory_ids"));
        inventory_id_list_for_compare_str = $("#inventory_id_list_for_compare").val();
        inventory_id_list_for_compare_arr = inventory_id_list_for_compare_str.split(",");
        if (inventory_id_list_for_compare_arr.length > 0) {
            for (x in inventory_id_list_for_compare_arr) {
                $("#inventory_" + inventory_id_list_for_compare_arr[x]).attr("checked", true);
            }
        }
        $(".comparePan").html(localStorage.getItem("comparePan"));
        if ($("#inventory_id_list_for_compare").val().split(",").length > 1) {
            $(".cmprBtn").attr({"disabled": false});
        }
        $("#no_of_products_compare").html($("#inventory_id_list_for_compare").val().split(",").length);
        $("#no_of_products_count_compare").html(3 - $("#inventory_id_list_for_compare").val().split(",").length);
        if ($("#no_of_products_count_compare")[0].innerHTML == 0) {
            $("#compare_line_text").css("visibility", "hidden");
        } else {
            $("#compare_line_text").css("visibility", "visible");
        }
    }

    function clearComparePanel() {
        $(".addToCompare").each(function () {
            $(this).attr("checked", false);
        });

        localStorage.removeItem("comparePan");
        localStorage.removeItem("comparePan_inventory_ids");
        $(".comparePan").html("");
        $("#inventory_id_list_for_compare").val("");
        $(".comparePanle").hide();

    }

    $(document).ready(function () {
        showChar(200);
    });


    $(document).ready(function () {
        if ($(window).width() <= 480) {
            $(".collapse").removeClass("in");
        }

        toAdapt();
        toAdapt_sku();
        window.onresize = function () {
            toAdapt;
            toAdapt_sku;
        };

    });


    function toAdapt() {
        var heights = $(".heightmatched").map(function () {
                return $(this).height();
            }).get(),

            maxHeight = Math.max.apply(null, heights);

        $(".heightmatched").height(maxHeight);
    }

    function toAdapt_sku() {
        var heights_sku = $(".heightmatched_sku").map(function () {
                return $(this).height();
            }).get(),

            maxHeight_sku = Math.max.apply(null, heights_sku);

        $(".heightmatched_sku").height(maxHeight_sku);
    }

    function clearAllFiltersfun() {
        window.location.href = location.href;
    }

    /* add to combo/pack */
    
    function sum_price(data){
        var sum=0;
        $.each(data, function(index, value) { 
            //console.log(value.inv_price+"price");
            var capacity = parseInt(value.inv_price, 10);
            sum += capacity;        
        });

        return sum;
    }
    function saved_price(data){
        var sum=0;
        $.each(data, function(index, value) { 
            //console.log(value.inv_price+"price");
            var capacity = parseInt(value.inv_saved_price, 10);
            sum += capacity;        
        });

        return sum;
    }

    function update_db_for_combo(clear_status=''){
        c_id = '<?php echo $this->session->userdata("customer_id");?>';
        if (c_id != '' && c_id != null) {

            if(clear_status==1){
                $.ajax({
                    url: "<?php echo base_url()?>Account/carttable_dump_combo_search_delete",
                    type: "post",
                    dataType: "json",
                    data:"clear="+clear_status,
                    success: function (result) {
                        
                        if(result){
                            alert('updated successfully');
                        }else{
                            alert('error');
                        }
                    }
                        
                });

            }else{
                //alert('Inside');
                
                localstorage_products=JSON.parse(localStorage.LocalCustomerComboCart);
                localstorage_products=JSON.stringify(localStorage.LocalCustomerComboCart);
                
                //console.log('--------');
                //console.log(localstorage_products);
                //console.log('--------');

                $.ajax({
                    url: "<?php echo base_url()?>Account/carttable_dump_combo_search",
                    type: "post",
                    dataType: "json",
                    data:"combo_products="+localstorage_products,
                    success: function (result) {
                        
                        if(result){
                            alert('updated successfully');
                        }else{
                            alert('error');
                        }
                    }
                        
                    });
            }
        }
    }


    var selected_products=[]; var combo_product_count=0;
    var inv_text=''; var total_combo_price=0;

    //localStorage.setItem("LocalCustomerComboCart",'');
    $(document).on('click', '.addToCombo', function () {
        cked= $(this).is(":checked");
        if(localStorage.LocalCustomerComboCart != null && localStorage.LocalCustomerComboCart != "[]" && localStorage.LocalCustomerComboCart!=''){
            selected_products=JSON.parse(localStorage.LocalCustomerComboCart);
        }
        
        

        inv_id=$(this).attr("data_id");
        inv_price=$(this).attr("data_price");
        inv_max_price=$(this).attr("data_max_price");
        inv_saved_price=$(this).attr("data_saved_price");
        inv_sku_id=$(this).attr("data_sku_id");
        inv_image=$(this).attr("data_image");
        inv_name=$(this).attr("data_name");
		inv_quantity=document.getElementById('purchase_qty_input_'+inv_id).value;
        //alert('inv_id='+inv_id+' inv_price='+inv_price+' inv_sku_id='+inv_sku_id+' inv_image='+inv_image+' inv_name='+inv_name);

        if(cked==true){
            var index = selected_products.findIndex(x => x.inv_id==inv_id); 
            if(index === -1){
                selected_products.push({'inv_id':inv_id,'inv_price':inv_price,'inv_sku_id':inv_sku_id,'inv_name':inv_name,'inv_image':inv_image,'inv_max_price':inv_max_price,'inv_saved_price':inv_saved_price,'inv_quantity':inv_quantity});
                inv_text+=inv_id+',';
            }
        }else{
            let index1 =selected_products.findIndex((element) => element["inv_id"] == inv_id);
            selected_products.splice(index1, 1); 
        }
        
        combo_product_count = selected_products.length;

        if (selected_products != null && selected_products != "[]") {
            //alert(selected_products);
            localStorage.setItem("LocalCustomerComboCart", JSON.stringify(selected_products));
            localStorage.setItem("LocalCustomerComboCartLength", combo_product_count);
            total=sum_price(selected_products);
            localStorage.setItem("LocalCustomerComboCartTotal", total);
            construct_html_for_combo(selected_products);
            $('#combo_total').html(total);
            combo_shipping_charge=$('#combo_pack_shipping_charge').val();
            grand_total=(parseInt(total)+parseInt(combo_shipping_charge));
            $('#combo_grand_total').html(grand_total);
            localStorage.setItem("LocalCustomerComboCartGrandTotal", grand_total);
            saved_amount=saved_price(selected_products);
            localStorage.setItem("LocalCustomerComboCartSavedTotal", saved_amount);
            $('#no_of_products_count_combo').html(localStorage.LocalCustomerComboCartLength)
            $('#combo_products_div').show();
            min_combo_length=$('#combo_pack_min_items').val();
            if(localStorage.LocalCustomerComboCartLength>=min_combo_length){
                $(".comboBtn").attr({"disabled": false});
            }else{
                $(".comboBtn").attr({"disabled": true});
            }
			
			if(combo_product_count==0){
				$(".comboPan").html('');
				clearComboPanel(); // this code for last item
			}
        }
        //console.log(localStorage.LocalCustomerComboCart);
        //console.log(localStorage.LocalCustomerComboCartLength);
        //console.log(localStorage.LocalCustomerComboCartTotal);
        //console.log(localStorage.LocalCustomerComboCartGrandTotal);


        update_db_for_combo();
       
    
    });

    function construct_html_for_combo(data){
        var sum;
        if(data!=''){
            $(".comboPan").html('');
            $.each(data, function(index, value) { 
                //console.log(value.inv_price+"price");
                var price = parseInt(value.inv_price, 10);
                sum += price;       
                displayTitle=value.inv_name;
                displayContent=displayTitle+' - <?php echo curr_sym; ?>'+price;
                $(".comboPan").append('<div id="' + value.inv_id + '" class="col-sm-3" title="'+displayContent+'"><div class="w3-white"><div class="f-row text-center"><a class="selectedItemCloseBtn_combo combo_close cursor">&times</a><img src="' + value.inv_image + '" alt="image" style="height:58px;"/><p id="prod_' +  value.inv_id + '" class="titleMargin1" title="'+displayTitle+'"><span class="small-text">' + displayTitle.substring(0,8) + '...</span></p></div></div></div>');
            });
        }
    }

    $(document).on('click', '.selectedItemCloseBtn_combo', function () {
		
        var test = $(this).siblings("p").attr('id');
        //alert(test);
		test_arr=test.split("_");
        inv_id=test_arr[1];
		var myEle = document.getElementById("inventory_" + inv_id);
		if(myEle) {
			document.getElementById("inventory_" + inv_id).checked = false;
		}
        
        if(localStorage.LocalCustomerComboCart != null && localStorage.LocalCustomerComboCart != "[]" && localStorage.LocalCustomerComboCart!=''){
            selected_products=JSON.parse(localStorage.LocalCustomerComboCart);
            let index1 =selected_products.findIndex((element) => element["inv_id"] == inv_id);
            selected_products.splice(index1, 1); 
            localStorage.setItem("LocalCustomerComboCart", JSON.stringify(selected_products));
            construct_combo_products(selected_products);
            update_db_for_combo();
        }

    });

   

    function clearComboPanel() {
        $(".addToCombo").each(function () {
            $(this).attr("checked", false);
        });

        localStorage.removeItem("comboPan");
        localStorage.removeItem("LocalCustomerComboCart");
        localStorage.removeItem("LocalCustomerComboCartLength");
        localStorage.removeItem("LocalCustomerComboCartTotal");
        localStorage.removeItem("LocalCustomerComboCartGrandTotal");
        localStorage.removeItem("LocalCustomerComboCartSavedTotal");

        $(".comboPan").html("");
        $("#inventory_id_list_for_combo").val("");
        $("#combo_products_div").hide();
        var clear_status=1;
		selected_products=[];
        update_db_for_combo(clear_status);

    }
    //clearComboPanel();


    function construct_combo_products(selected_products=''){

        if(localStorage.LocalCustomerComboCart != null && localStorage.LocalCustomerComboCart != "[]" && localStorage.LocalCustomerComboCart!=''){
            selected_products=JSON.parse(localStorage.LocalCustomerComboCart);
        }

        if(selected_products!= null && selected_products!= "[]" && selected_products!=""){
            $(".addToCombo").each(function () {
                inv_id=$(this).attr("data_id");
				    var index = selected_products.findIndex(x => x.inv_id==inv_id); 
					if(index !== -1){
						$(this).attr("checked", true); // when it exists on localstorage
						inv_price=$(this).attr("data_price");
						inv_max_price=$(this).attr("data_max_price");
						inv_saved_price=$(this).attr("data_saved_price");

						selected_products[index].inv_price=inv_price;
						selected_products[index].inv_max_price=inv_max_price;
						selected_products[index].inv_saved_price=inv_saved_price;
						
					}else{
						$(this).attr("checked", false);
					}
            });

            construct_html_for_combo(selected_products);

            total=sum_price(selected_products);
            localStorage.setItem("LocalCustomerComboCartTotal", total);
            
            $('#combo_total').html(total);
            combo_shipping_charge=$('#combo_pack_shipping_charge').val();
            grand_total=(parseInt(total)+parseInt(combo_shipping_charge));
            $('#combo_grand_total').html(grand_total);
            localStorage.setItem("LocalCustomerComboCartGrandTotal", grand_total);
            saved_amount=saved_price(selected_products);
            localStorage.setItem("LocalCustomerComboCartSavedTotal", saved_amount);


            $('#no_of_products_count_combo').html(localStorage.LocalCustomerComboCartLength)
            $('#combo_total').html(localStorage.LocalCustomerComboCartTotal);
            $('#combo_grand_total').html(localStorage.LocalCustomerComboCartGrandTotal);

            $("#combo_products_div").show();

            min_combo_length=$('#combo_pack_min_items').val();
            if(localStorage.LocalCustomerComboCartLength>=min_combo_length){
                $(".comboBtn").attr({"disabled": false});
            }else{
                $(".comboBtn").attr({"disabled": true});
            }
        }else{
			
			$(".addToCombo").each(function () { // this code for last item
				$(this).attr("checked", false); // this code for last item
			}); // this code for last item
			
			
            $("#combo_products_div").hide();

        }


    }


    $(document).ready(function () {
        //construct_combo_products();

        c_id = '<?php echo $this->session->userdata("customer_id");?>';
        if (c_id != '' && c_id != null) {
            $.ajax({
                url: "<?php echo base_url()?>Account/get_combo_cart",
                type: "post",
                dataType: "json",
                data:"1=2",
                success: function (result) {
                    //alert(result);
                    //console.log('Combo products');
                    //console.log(result);
                    
                    if(result){
                        if(result.combo_products!=''){
                            var combo_products=JSON.parse(JSON.stringify(result.combo_products));
                        }else{
                            var combo_products=[];
                        }
                       
                        var combo_number_of_products=result.combo_number_of_products;
                        var combo_total_price=Math.round(result.combo_total_price);
                        var combo_shipping_price=Math.round(result.combo_shipping_price);
                        var combo_grand_total=Math.round(result.combo_grand_total);
                        var combo_discount=Math.round(result.combo_discount);
                        
                        localStorage.setItem("LocalCustomerComboCart",combo_products);
                        localStorage.setItem("LocalCustomerComboCartLength",combo_number_of_products);
                        localStorage.setItem("LocalCustomerComboCartTotal",combo_total_price);
                        localStorage.setItem("LocalCustomerComboCartGrandTotal",combo_grand_total);
                        construct_combo_products();
                    }
                }
            });

        }else{
            construct_combo_products();
        }

    });
	 $(document).ready(function () {
	if(localStorage.LocalCustomerComboCart!=null){
	localstorage_products=JSON.parse(localStorage.LocalCustomerComboCart);
	//console.log(localstorage_products)
	var inv_id_arr=[];
	$.each(localstorage_products, function( key, value ) {
  inv_id_arr.push(value.inv_id);
});
	$.ajax({
                url: "<?php echo base_url()?>get_all_tagged_inventories_for_combo",
                type: "post",
                dataType: "json",
                data:"tagged_main_inventory_id_list_in="+inv_id_arr.join(","),
                success: function (result) {
                    alert(result);
					if(result.length>0){
						for(inv in result){
							
						}
					}
                    //console.log('Combo products');
                    //console.log(result);
                    
                    /*if(result){
                        if(result.combo_products!=''){
                            var combo_products=JSON.parse(JSON.stringify(result.combo_products));
                        }else{
                            var combo_products=[];
                        }
                       
                        var combo_number_of_products=result.combo_number_of_products;
                        var combo_total_price=Math.round(result.combo_total_price);
                        var combo_shipping_price=Math.round(result.combo_shipping_price);
                        var combo_grand_total=Math.round(result.combo_grand_total);
                        var combo_discount=Math.round(result.combo_discount);
                        
                        localStorage.setItem("LocalCustomerComboCart",combo_products);
                        localStorage.setItem("LocalCustomerComboCartLength",combo_number_of_products);
                        localStorage.setItem("LocalCustomerComboCartTotal",combo_total_price);
                        localStorage.setItem("LocalCustomerComboCartGrandTotal",combo_grand_total);
                        construct_combo_products();
                    }*/
                }
            });
	}
	});

    function proceedCheckout() {
            min_combo_length=$('#combo_pack_min_items').val();
            if(localStorage.LocalCustomerComboCartLength>=min_combo_length){
                $(".comboBtn").attr({"disabled": false});
            }else{
                $(".comboBtn").attr({"disabled": true});
                swal({
                    title: '',
                    html:"Please add minimum "+min_combo_length+ " Products",
                    type: 'info',
                    showConfirmButton: true
                });
                return false;
            }
            c_id = '<?php echo $this->session->userdata("customer_id");?>';
            if (c_id != '' && c_id != null) {
                location.href = "<?php echo base_url()?>checkout_combo";
            } else {
                <?php
                $this->session->set_userdata("cur_page", base_url() . "checkout_combo");
                ?>
                location.href = "<?php echo base_url()?>login";
            }
        }
    /* add to combo/pack */

</script>

    <link href="https://kendo.cdn.telerik.com/themes/6.7.0/default/default-main.css" rel="stylesheet" />
    <!--<script src="https://kendo.cdn.telerik.com/2023.2.829/js/jquery.min.js"></script>-->
    
    
    <script src="https://kendo.cdn.telerik.com/2023.2.829/js/kendo.all.min.js"></script>
    
  
	
<!--  warning model  -->
<div class="modal" id="category_treeview_order_modal">
     <div class="modal-dialog modal-md">
        <div class="modal-content">
           <div class="modal-header" style="border-bottom:0px;">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
			 <style>
			 #category_treeview{
				 border:1px solid #e1e1e1;
				 padding:1rem;
				 margin-top:1rem;
				 margin-bottom:1rem;
			 }
			 </style>
			 
              <h4 class="modal-title">Choose the products to buy</h4>
           </div>
           <div class="modal-body">
		   
			<div class="row">
				<div class="col-md-12 text-right">
					<button type="button" class="btn btn-primary" name="category_treeview_order_submit_1" onclick="category_treeview_order_modalFun()">Submit</button>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12" id="category_treeview">
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-right">
					<button type="button" class="btn btn-primary" name="category_treeview_order_submit_2" onclick="category_treeview_order_modalFun()">Submit</button>
				</div>
			</div>
			
    

    <script>
		function category_treeview_order_modalFun(){
			document.searchForm.action="<?php echo base_url()?>catalog_combo";
			document.searchForm.submit();
		}
		function treeviewData(pcat_cat_data){
			$("#category_treeview").kendoTreeView({
				checkboxes: {
					checkChildren: true
				},

				check: onCheck,

				dataSource: pcat_cat_data
			});
		}

        // function that gathers IDs of checked nodes
        function checkedNodeIds(nodes, checkedNodes) {
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].checked) {
                    checkedNodes.push(nodes[i].id);
                }

                if (nodes[i].hasChildren) {
                    checkedNodeIds(nodes[i].children.view(), checkedNodes);
                }
            }
        }

        // show checked node IDs on datasource change
        function onCheck() {
            var checkedNodes = [],
                category_treeview = $("#category_treeview").data("kendoTreeView"),
                message;

            checkedNodeIds(category_treeview.dataSource.view(), checkedNodes);

            if (checkedNodes.length > 0) {
                message = checkedNodes.join(",");
            } else {
                message = "";
            }

            //$("#result").html(message);
			
        }
    </script>

   



  
  
  
           </div>
       
        <!--<div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close" class="text-primary bold">Close</a>
        </div>-->
     </div>
    </div>
  </div>
<!--  end of warning model  -->

<script>
	
	
	
		function incrementdecrementtimerfun(moq_original, max_oq,obj,inventory_id){
			var moq=obj.value;
            if (parseInt(moq) > parseInt(max_oq) && isNaN(moq)==false) {
                document.getElementById('purchase_qty_input_'+inventory_id).value = max_oq;
            }
			if (parseInt(moq) < parseInt(moq_original) && isNaN(moq)==false) {
                document.getElementById('purchase_qty_input_'+inventory_id).value = moq_original;
            }
            if (moq == '' || isNaN(moq)==true) {
                $("#quantity_status_info").html('<span style="color:blue">Enter between ' + moq_original + ' and ' + max_oq + '</span>');

                return false;// retain the previos calculation for the input
            }else{
				
                if (moq != "") {
                    var moq_previous = moq;
                }

            }
			if ((moq == "" || parseInt(moq) == 0 || moq == null) && isNaN(moq)==true) {
                $("#quantity_status_info").html('<span style="color:blue">Enter between ' + moq_original + ' and ' + max_oq + '</span>');
                document.getElementById('purchase_qty_input_'+inventory_id).value = moq_original;
            }else {
                if (parseInt(moq) < parseInt(moq_original)) {
                    $("#quantity_status_info").html('<span style="color:blue">Enter between ' + moq_original + ' and ' + max_oq + '</span>');
					var stock_available="<?php echo $inventory_product_info_obj->stock_available;?>";
                } else if (parseInt(moq) <= parseInt(max_oq) && parseInt(moq) > parseInt(stock_available)) {
                    $("#quantity_status_info").html('Out of Stock!');
                } else if (parseInt(moq) > parseInt(max_oq)) {
                    $("#quantity_status_info").html('<span style="color:blue">Enter between ' + moq_original + ' and ' + max_oq + '</span>');
                    document.getElementById('purchase_qty_input_'+inventory_id).value = moq_original;

                }
            }
        };
		function inventory_moq_resetFun(moq,inventory_id) {
			var purchase_qty_input=document.getElementById('purchase_qty_input_'+inventory_id).value;
            if (purchase_qty_input == "") {
                document.getElementById('purchase_qty_input_'+inventory_id).value = moq;
            }
        }
	 
	 
	 function isNumber(evt) {
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
		return false;
	}
	return true;
}
</script>