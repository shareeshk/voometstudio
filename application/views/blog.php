
	<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo base_url(); ?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Blog</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- Blog category -->
                <div class="block left-module">
                    <p class="title_block">Blog Categories</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <?php  if(!empty($topic_sel)){
                                  foreach($topic_sel as $topic){?>
                                      <li><span></span><a href="<?php echo base_url()?>get_blogs_of_topic/<?php echo $topic['topic_sel']?>"><?php echo $topic['topic_sel']?></a></li>
                                 <?php }
                                  }?>
    
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./blog category  -->
                <?php if(!empty($popular_blogs)){?>
                <!-- Popular Posts -->
                <div class="block left-module">
                    <p class="title_block">Popular Posts</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered">
                            <div class="layered-content">
                                <ul class="blog-list-sidebar clearfix">
                                    <?php foreach($popular_blogs as $popular_blog){?>
                                  <li>
                                    <?php /*<div class="post-thumb">
                                        <a href="#"><!--<img src="assets/data/blog-thumb1.jpg" alt="Blog">--></a>
                                    </div>*/?>
                                    <div class="post-info" style="margin-left:0px">
                                        <h5 class="entry_title"><a href="#"><?php echo $popular_blog['blog_title'] ?></a></h5>
                                        <div class="post-meta">
                                            <span class="date"><i class="fa fa-calendar"></i> <?php echo $popular_blog['timestamp'] ?></span>
                                            <span class="comment-count">
                                                <i class="fa fa-thumbs-up"></i> <?php echo $popular_blog['count_likes'] ?>
                                            </span>
                                        </div>
                                    </div>
                                </li>      
                                   <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./Popular Posts -->
                    <?php }?>
                
                <!-- tags -->
                <div class="block left-module">
                    <p class="title_block">TAGS</p>
                    <div class="block_content">
                        <div class="tags">
                            <?php if(!empty($tags)){
                              foreach($tags as $tag){?>
                                  <a href="<?php echo base_url()?>get_blogs_of_tag/<?php echo $tag['tags']?>"><span class="level<?php echo rand ( 1 , 5 ) ?>"><?php echo $tag['tags']?></span></a>
                             <?php }
                              }?>
                            
                            
                            
                        </div>
                    </div>
                </div>
                <!-- ./tags -->

            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
           <div class="center_column col-xs-12 col-sm-9" id="center_column">
                
                <h2 class="page-heading">
                    <span class="page-heading-title2">Blog post  <?php if(!empty($interest_for)) echo 'for '.$interest_for; ?></span>
                </h2>
				
				<?php
if(is_array($all_blog)){
					if(count($all_blog)>5){
						$style='';
					}else{
						$style='style="display:none;"';
					}
}
else{
	$style='style="display:none;"';
}
				?>
				
				<div class="sortPagiBar clearfix" <?php echo $style; ?>>
                    <div class="bottom-pagination">
                        <span class="nav_pag"></span>
                    </div>
                </div>
				
              
                <ul class="blog-posts common">

                    
                    <?php 
					
					if($all_blog){
                        foreach($all_blog as $blog_data){?>
                            <li class="post-item">
                        <article class="entry">
                            <div class="row">
                                <?php
                                $first_img="";
                                    $texthtml = $blog_data['blog_content'];
                                   $doc = new DOMDocument();
    @$doc->loadHTML($texthtml);

    $tags = $doc->getElementsByTagName('img');
    foreach ($tags as $tag) {
           $first_img=$tag->getAttribute('src');
           break;
    }
    ?>
                                
                                
                                <div class="col-sm-7">
                                    <div class="entry-ci">
                                        <h3 class="entry-title"><a href="<?php echo base_url()?>get_blog_post/<?php echo $blog_data['slug'] ?>"><?php echo $blog_data['blog_title']?></a></h3>
                                        <div class="entry-meta-data">
                                            <span class="author">
                                            <i class="fa fa-user"></i> 
                                            <a href="#"><?php echo $blog_data['name']?></a></span>
                                            <span class="cat">
                                                <i class="fa fa-folder-o"></i>
                                                 <?php  if(!empty($topic_sel)){
                                                  foreach($topic_sel as $topic){
                                                      if($topic['topic_id']==$blog_data['topic_sel']){?>
                                                          <a href="<?php echo base_url()?>get_blogs_of_topic/<?php echo $topic['topic_sel']?>"><?php echo $topic['topic_sel']?></a>
                                                     <?php break;}
                                                      }
                                                  }?>
                                            </span>
                                            |
                                            <span class="comment-count">
                                                <a href="#comment-box"><i class="fa fa-comment-o" ></i></a> <?php if(!empty($blog_comment_data)){echo count($blog_comment_data);}else{ echo 0;}?>
                                            </span>
                                           |
                                            <span class="date"><i class="fa fa-calendar"></i> <?php echo $blog_data['timestamp']?></span>
                                            <span><i class="fa fa-thumbs-up"></i> &nbsp;<?php echo "<span id='like_counter'>".(count(json_decode('["'.$blog_data['likes'].'"]'))-1)."</span>";?></span> 
                                        </div>
                                        
                                        <?php 
                          $postContent = $blog_data['blog_content']; 
                          $word = str_word_count(strip_tags($postContent));
                          $m = floor($word / 200);
                          $s = floor($word % 200 / (200 / 60));
                        //($m == 1 ? '' : 's') .
                        //. ($s == 1 ? '' : 's')
                          $est = $m . ' Min' .  ' ' . $s . ' Sec' ;
                        ?>
                    
                       <small style="color:#f30">Estimated reading time: <?php echo $est; ?></small>
                                        <br>
                                        <div class="entry-excerpt">
                                            
                                            
                                            
                                            <?php $content = preg_replace("/<img[^>]+\>/i", "(image) ", $blog_data['blog_content']); 
   $content=strip_tags($content);
   if(strlen($content)>500){
        $c=substr($content,0,162)."...";
   }
   else{
       $c=$content;
   }
       echo $c;
   
   ?>
                                        </div>
                                        <div class="entry-more">
                                            <a href="<?php echo base_url()?>get_blog_post/<?php echo $blog_data['slug'] ?>">Read more</a>
                                        </div>
                                    </div>
                                </div> <!---col-sm-7--->

                                <?php
  
                                if($first_img!=""){?>
                                    <div class="col-sm-5">
                                        <div class="entry-thumb image-hover2">
                                            
                                            
                                            <a href="#">
                                                <img src="<?php echo $first_img ?> " alt="<?php echo $blog_data['blog_title']?>">
                                            </a>
                                        </div>
                                    </div>
                                <?php }?>

                            </div>
                        </article>
                    </li>
                       <?php 
					  
					   }
                    }else{
                        
                    }?>
                    
                    
                    
                </ul>
				
				
				
                <div class="sortPagiBar clearfix" <?php echo $style; ?>>
                    <div class="bottom-pagination">
                        <span class="nav_pag"></span>
                    </div>
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>


<script>
jQuery(document).ready(function($){	
		$('.common').paginathing({
	    perPage:5,
	    insertAfter: '.nav_pag'
		});
	});
</script>
