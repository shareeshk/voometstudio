<style type="text/css">
.sku_display_name_on_buttons,.sku_display_name_on_buttons:hover{
	line-height: 1.6rem;
    padding: 0.3rem;
	width: 100%;
	color:#fff;
	font-size: 1.02em;
	box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;
}
.image-checkbox .sku_display_name_on_buttons{
	background-color:#dd7973;
    white-space: normal;
}
.image-checkbox-checked .sku_display_name_on_buttons{
	background-color:#50d42c;
    white-space: normal;
    min-height: 60px;
}
.sku_display_name_on_buttons .sku_display_name_on_button_1{
	font-weight:600;
}
.sku_display_name_on_buttons .sku_display_name_on_button_2{
	font-weight:400;
}
    .yourprice_div{
	margin-bottom:-0.7rem;
}
.offerprice_addon{
	font-size:1.8rem;
	color:#004b9b;
}
.hurbztextcolor{
	color:#ff6600;
}
.sku_display_name_on_buttons,.sku_display_name_on_buttons:hover{
	line-height: 1.6rem;
    padding: 0.3rem;
	width: 100%;
	color:#fff;
	font-size: 1.02em;
	box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;
}
.image-checkbox .sku_display_name_on_buttons{
	background-color:#dd7973;
}
.image-checkbox-checked .sku_display_name_on_buttons{
	background-color:#50d42c;
}
.sku_display_name_on_buttons .sku_display_name_on_button_1{
	font-weight:600;
}
.sku_display_name_on_buttons .sku_display_name_on_button_2{
	font-weight:400;
}
/* addon css */

    .cart_summary .qty input {
        text-align: center;
        max-width: 50px;
        margin: 0 auto;
        border-radius: 0px;
        border: 1px solid #eaeaea;
    }

    .cart_summary .qty a {
        padding: 3px 5px 5px 5px;
        border: 1px solid #eaeaea;
        display: inline-block;
        width: auto;
        margin-top: 0px;
    }

    .borderless td, .borderless th {
        border: none;
    }

    .overflow {
        overflow: auto;
    }

    .left-align-cashback {
        width: 86%;
        float: left;
        text-align: right;
    }

    .left-align-cashback-total {
        width: 85%;
        float: left;
        text-align: right;
        font-size: .9em;
    }

    .cart_summary td {
        vertical-align: middle !important;
        padding: 10px;
    }

    .cart_summary .table > tbody > tr > td, .table > tbody > tr > th, .cart_summary .table > tfoot > tr > td, .table > tfoot > tr > th, .cart_summary .table > thead > tr > td {
        padding: 5px;
    }

    .table > thead > tr > th {
        padding: 6px;
    }

    @media (max-width: 480px) {
        .page-order .cart_navigation a {
            margin-top: 4%;
            margin-left: 20%;
            margin-bottom: 4%;
        }

        .page-order .cart_navigation a.next-btn {
            float: left;
        }
        .page-order .cart_navigation button.next-btn{
            width: 100%;
            margin: 10px 0px 10px 0px;
        }
        .page-order .cart_navigation button.prev-btn{
            width: 100%;
        }
        .shipping_note{
            line-height:25px;
        }
        #available_pincode_div
        {
            margin:10px 0px 10px 0px;
            float: right;
        }
        .my-cart {
            margin: 0.2em 0px 0 0px;
        }
        .min_width{
            min-width: 100px;
        }
    }

    .pincode-search {
        max-width: 50%;
    }

    .help-block {
        color: #ff6161;
    }

    .glyphicon {
        line-height: 1.4;
    }

    .input-group .form-control {
        z-index: 0;
    }

    .input-group-sm > .form-control, .input-group-sm > .input-group-addon, .input-group-sm > .input-group-btn > .btn {
        font-size: 1.1rem;
    }

    .well {
        border: none !important;
        background-color: #fff;
        -webkit-box-shadow: none;
        transition: all 0.3s ease-in-out 0s;
    }

    .my-cart {
        margin: 0.2em 0px 0 0px;
    }

    th {
        font-size: 0.8em;
        font-weight: 600;
    }

    .swal2-icon.swal2-info {
        color: #ff0000 !important;
        border-color: #ff0000 !important;
    }
    .shipping_note{
     /*line-height:25px;*/
    }
    .my_cart_note{
        margin: 0px 10px 10px 10px;
    }
    .noIDontwantOfferBtn {
        background-color: #ff0000;
        border-left-color: #ff0000;
        border-right-color: #ff0000;
        border: 0;
        border-radius: 3px;
        box-shadow: none;
        color: #fff;
        cursor: pointer;
        font-size: 17px;
        font-weight: 500;
        margin: 30px 5px 0px 5px;
        padding: 5px 32px;
    }
    .qty_loader{
       position: relative;
    }
    .qty_loader img{
       position: absolute;
       top: 25px;  /* position the top  edge of the element at the middle of the parent */
       left: 50%; /* position the left edge of the element at the middle of the parent */
       transform: translate(-50%, -50%);
    }


    /* Image checkbox */
    
    .nopad {
	padding-left: 0 !important;
	padding-right: 0 !important;
}
/*image gallery*/
.image-checkbox {
    
	cursor: pointer;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	border: 1px solid #eee;
	margin-bottom: 0;
	outline: 0;
    margin: 5px 5px 5px 5px;
    border-radius: 10px;
    line-height: 30px;
    font-size: 14px;
}
.image-checkbox img{

   border-radius: 10px;
    max-width: 50%;
    margin-left: auto;
    margin-right: auto;
}
.image-checkbox input[type="checkbox"] {
	display: none;
}

.image-checkbox-checked {
	border-color: #50d42c;
}
.image-checkbox-checked:hover {
	border-color: #50d42c;
}
.image-checkbox .fa {
    position: absolute;
    color: #fff;
    background-color: #50d42c;
    padding: 5px;
    top: 5px;
    right: 5px;
    border-radius: 12px;
    font-size: 14px;
    height: 25px;
}
.image-checkbox-checked .fa {
  display: block !important;
}
.image-checkbox:hover{
    border-color: #50d42c;
}

.margin-top{
    margin-top:10px;
} 
.margin-bottom{
    margin-bottom:10px;
}
    /* Image checkbox */

</style>

<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">

        <script type="text/javascript">
            //document.write(JSON.stringify(localStorage.LocalCustomerCart));
            //2nd controller//
            function OrderController($scope, $rootScope, Scopes, $http, $sce, $timeout) {

              

                Scopes.store('OrderController', $scope);
                //$scope.searched_pincode_msg='';
                c_id = '<?php echo $this->session->userdata("customer_id");?>';
                var searched_pincode = localStorage.searched_pincode;
                var vendor_id = localStorage.vendor_id;
                $scope.promo_temp=0;
                $scope.new_promo_avail=0;//common
                $scope.pincode = searched_pincode;
                $scope.vendor_id = vendor_id;
                if (searched_pincode != null && searched_pincode != '') {
                    $scope.searched_pincode = searched_pincode;
                    $scope.pincode = $scope.searched_pincode;
                }
                if (searched_pincode == '' || $scope.pincode == null) {
                    $scope.searched_pincode_msg = 'No pincode searched';
                } else {
                    $scope.searched_pincode_msg = 'pincode searched';
                }

                $scope.searched_pincode_msg = '';
                $scope.final_count_free_items = 0;
                $scope.show_cash_back_tr = 0;
                var cashbackoffer_swal_count = 0;
                var discountoffer_swal_count = 0;
                var freeshippingoffer_swal_count = 0;
                cashbackoffer_discountoffer_freeshippingoffer_swal_count = 0;
                cashbackoffer_discountoffer_freeshippingoffer_swal_str = "";

                $scope.update_cart_controller_scopes = function (order_product_count, products_order, w = '') {
                    //alert(w);
					
					$(".qtyinput").each(function(){
								idx=$(this).attr("id");
								qtyinput_index=idx.split("_")[1];
								$("#qtyinput_"+qtyinput_index).attr("disabled",false);
							});
					
					
                    //console.log(products_order);
                    //console.log(w+"w");
                    
                    $scope.count_of_free_items = new Array();

                    $scope.out_of_stock_item_in_cart = [];
                    $scope.applied_at_invoice_arr = [];
                    $scope.order_product_count = order_product_count;
                    $scope.products_order = products_order;
					//$scope.convert_inventory_number();

                    $scope.final_count_free_items = 0;//newly added
                    $scope.availed_offers_heading_flag = "no";

                    /// this is added on 7_15_2021 because of shipping charges not coming in cart items if we not applied promotion starts
                    $scope.eligible_free_shipping_display = 0;
                    /// this is added on 7_15_2021 because of shipping charges not coming in cart items if we not applied promotion ends


                    if ($scope.order_product_count == 0) {
                        $scope.show_cash_back_tr = 0;
                        $scope.eligible_cash_back_percentage_display = 0;
                        $scope.eligible_discount_percentage_display = 0;
                        $scope.eligible_free_shipping_display = 0;


                        $scope.eligible_free_surprise_gift_display = 0;
                        $scope.eligible_cash_back_percentage_display = 0;
                        $scope.discount_oninvoice = 0;
                        $scope.cash_back_oninvoice = 0;
                        $scope.free_shipping = 0;
                        $scope.surprise_gift = 0;
                        $scope.total_cash_back = 0;
                        $scope.show_surprise_gift_on_invoice = 0;
                        $scope.invoice_surprise_gift = 0;
                        $scope.final_count_free_items = 0;
                        return false;
                    }

                    //alert(w);

                    $scope.total_cash_back_for_sku = 0;
                    total_netprice = 0;
                    individual_price_without_shipping = 0;
                    total_shipping_price = 0;
                    $scope.cash_back_tr_arr = [];
                    $scope.total_addon_products=0;

                    if (localStorage.changed_moq_with_inventory_id != null && localStorage.changed_moq_with_inventory_id != '') {

                        changed_inv = localStorage.changed_moq_with_inventory_id;

                        changed_inv = JSON.parse(changed_inv);
                        inventory_id_to_update = changed_inv.inventory_id;
                    } else {
                        inventory_id_to_update = '';
                    }
                    $scope.total_inventory_quantity_in_cart = 0;
                    $scope.total_shipping_price_addon=0;
                    
                    for (j = 0; j < $scope.order_product_count; j++) {
                         
                       $scope.products_order[j].inventory_moq_display=$scope.products_order[j].inventory_moq;
                        $scope.total_inventory_quantity_in_cart += parseInt($scope.products_order[j].inventory_moq);
                        var val = $scope.products_order[j].attribute_1_value.split(':');
                        $scope.products_order[j].attribute_1_value = val[0];
                        
                        //console.log($scope.products_order[j].stock_available+"|$|"+parseInt($scope.products_order[j].inventory_moq)+"|$|"+parseInt($scope.products_order[j].inventory_moq_display));
			
                        if(isNaN($scope.products_order[j].inventory_moq)===true && isNaN($scope.products_order[j].inventory_moq_display)==true){
                            $scope.update_moq(j);
                        }
                        
                        if (parseInt($scope.products_order[j].stock_available) >= parseInt($scope.products_order[j].inventory_moq) && isNaN($scope.products_order[j].inventory_moq)===false) {

                            $scope.products_order[j].item_stock_available = 1;

                            if (c_id == null || c_id == '') {
                                $scope.products_order[j].selling_price = $scope.products_order[j].inventory_selling_price_with_out_discount;
                            }//without session


                            if (products_order[j].inventory_selling_price == null) {
                                //when fetching the data from db
                                $scope.products_order[j].selling_price = $scope.products_order[j].selling_price;
                                $scope.products_order[j].inventory_selling_price = $scope.products_order[j].selling_price;
                            } else {

                                $scope.products_order[j].selling_price = products_order[j].inventory_selling_price;
                            }

                            if (inventory_id_to_update != '') {
                                if ($scope.products_order[j].inventory_id == inventory_id_to_update) {

                                    $scope.products_order[j].inventory_moq = changed_inv.changed_moq;
                                    if (changed_inv.shipping_charge_from_detail_page != null) {
                                        $scope.products_order[j].shipping_charge = changed_inv.shipping_charge_from_detail_page;
                                        //alert($scope.products_order[j].shipping_charge+"shipping charge");
                                    }

                                    localStorage.removeItem('changed_moq_with_inventory_id');
                                }
                            }

                            //alert($scope.products_order[j].inventory_moq);

                            if ($scope.products_order[j].promotion_minimum_quantity != '' && $scope.products_order[j].promotion_available==1) {

                                if (parseInt($scope.products_order[j].promotion_minimum_quantity) <= parseInt($scope.products_order[j].inventory_moq)) {
                                    $scope.products_order[j].show_addon_items = 1;
                                } else {
                                    $scope.products_order[j].show_addon_items = 0;
                                }
                            } else {
                                $scope.products_order[j].show_addon_items = 0;
                            }

                            $scope.products_order[j].final_product_individual = 0;


                            modulo = $scope.products_order[j].quantity_without_promotion;
                            quotient = $scope.products_order[j].quantity_with_promotion;

                            $scope.products_order[j].modulo = modulo;
                            $scope.products_order[j].quotient = quotient;
                            $scope.products_order[j].price_of_quotient_1 = Math.round($scope.products_order[j].individual_price_of_product_with_promotion);  
                            $scope.products_order[j].price_of_modulo_1 = $scope.products_order[j].individual_price_of_product_without_promotion;
                            
                            /* for checking */
                            promo_id_selected =$scope.products_order[j].promotion_id_selected ;
                            inventory_id =$scope.products_order[j].inventory_id ;
                            //alert(promo_id_selected+"promo_id_selected"+j+$scope.products_order[j].promotion_available);
                            if($scope.products_order[j].promotion_available==0){
                                
                                /*$scope.get_promotion_availability(promo_id_selected,inventory_id,j);

                                //alert($scope.new_promo_avail);
                                if($scope.new_promo_avail==1){
                                    $scope.products_order[j].new_promo_avail=1;
                                }else{
                                    $scope.products_order[j].new_promo_avail=0;
                                }*/
                            }
                            //$scope.products_order[j].promotion_available == 1 && 
                            if (promo_id_selected!=null && promo_id_selected!='') {
                                //confirms promotion available
                                /*if(1){
                                        $scope.get_promotion_active_status(promo_id_selected,'1');
                                }
                                if($scope.products_order[j].promo_temp==2){
                                    if($scope.products_order[j].promotion_available==1){
                                        //promotion active in localStorage but in backend it is inactive or expired
                                        $scope.products_order[j].promotion_available=0;
                                    }
                                }else{
                                    $scope.products_order[j].promotion_available=1;//
                                }
                                //alert($scope.promo_temp+'return_variable1');
                                */
                            }
                           
                            /* for checking */
                            
                            
                            if ($scope.products_order[j].promotion_available == 1) {

                                ////////////

                                if ($scope.products_order[j].default_discount != "") {


                                    if (modulo != 0 && ($scope.products_order[j].inventory_moq >= parseInt($scope.products_order[j].promotion_minimum_quantity))) {

                                        $scope.products_order[j].discount = 1;//default
                                        $scope.products_order[j].promotion = 2;


                                        $scope.products_order[j].price_after_default_discount = $scope.products_order[j].total_price_of_product_without_promotion;

                                        if (quotient != 0) {
                                            $scope.products_order[j].price_after_promotion_discount = $scope.products_order[j].total_price_of_product_with_promotion;
                                        }

                                        if (quotient == 0 && modulo != 0) {
                                            $scope.products_order[j].promotion_quote_show = 0;
                                        } else {
                                            $scope.products_order[j].promotion_quote_show = 1;
                                        }
                                        if (quotient != 0 && modulo == 0) {
                                            $scope.products_order[j].discount_quote_show = 0;
                                        } else {
                                            $scope.products_order[j].discount_quote_show = 1;
                                        }

                                        if ($scope.products_order[j].price_after_promotion_discount != null) {
                                            $scope.products_order[j].final_product_individual = parseInt($scope.products_order[j].price_after_promotion_discount) + parseInt($scope.products_order[j].price_after_default_discount);
                                        } else {
                                            $scope.products_order[j].promotion = "";
                                            $scope.products_order[j].discount = 1;
                                            $scope.products_order[j].final_product_individual = $scope.products_order[j].price_after_default_discount;
                                        }

                                    } else {

                                        if ($scope.products_order[j].inventory_moq >= parseInt($scope.products_order[j].promotion_minimum_quantity)) {

                                            $scope.products_order[j].promotion_quote_show = 1;
                                            $scope.products_order[j].discount_quote_show = 0;

                                            $scope.products_order[j].price_after_promotion_discount = $scope.products_order[j].total_price_of_product_with_promotion;
                                            $scope.products_order[j].final_product_individual = $scope.products_order[j].price_after_promotion_discount;

                                            $scope.products_order[j].price_after_default_discount = 0;

                                            $scope.products_order[j].promotion = 1;
                                            $scope.products_order[j].discount = "";

                                        } else {

                                            if (quotient == 0 && modulo != 0) {
                                                $scope.products_order[j].promotion_quote_show = 0;
                                            } else {
                                                $scope.products_order[j].promotion_quote_show = 1;
                                            }
                                            if (quotient != 0 && modulo == 0) {
                                                $scope.products_order[j].discount_quote_show = 0;
                                            } else {
                                                $scope.products_order[j].discount_quote_show = 1;
                                            }


                                            $scope.products_order[j].price_after_default_discount = $scope.products_order[j].total_price_of_product_without_promotion;

                                            $scope.products_order[j].promotion_quote_show = 0;
                                            $scope.products_order[j].discount_quote_show = 1;

                                            $scope.products_order[j].promotion = "";
                                            $scope.products_order[j].discount = 1;
                                            $scope.products_order[j].final_product_individual = $scope.products_order[j].price_after_default_discount;

                                        }

                                    }

                                }//discount promotion !=0

                                if ($scope.products_order[j].default_discount == "") {

                                    if (modulo != 0) {

                                        $scope.products_order[j].discount = 1;
                                        $scope.products_order[j].promotion = 2;
                                        $scope.products_order[j].price_of_modulo_1 = $scope.products_order[j].selling_price;

                                        $scope.products_order[j].price_after_default_discount = $scope.products_order[j].total_price_of_product_without_promotion;

                                        if (quotient != 0) {

                                            $scope.products_order[j].price_of_quotient_1 = Math.round($scope.products_order[j].individual_price_of_product_with_promotion);

                                            $scope.products_order[j].price_after_promotion_discount = $scope.products_order[j].total_price_of_product_with_promotion;
                                        }

                                        if (quotient == 0 && modulo != 0) {
                                            $scope.products_order[j].promotion_quote_show = 0;
                                        } else {
                                            $scope.products_order[j].promotion_quote_show = 1;
                                        }
                                        if (quotient != 0 && modulo == 0) {
                                            $scope.products_order[j].discount_quote_show = 0;
                                        } else {
                                            $scope.products_order[j].discount_quote_show = 1;
                                        }

                                        if ($scope.products_order[j].price_after_promotion_discount != null) {
                                            $scope.products_order[j].final_product_individual = parseInt($scope.products_order[j].price_after_promotion_discount) + parseInt($scope.products_order[j].price_after_default_discount);
                                        } else {

                                            $scope.products_order[j].promotion = "";
                                            $scope.products_order[j].discount = 1;
                                            $scope.products_order[j].final_product_individual = $scope.products_order[j].price_after_default_discount;
                                        }

                                    } else {

                                        $scope.products_order[j].promotion_quote_show = 1;
                                        $scope.products_order[j].discount_quote_show = 0;
                                        $scope.products_order[j].price_after_promotion_discount = $scope.products_order[j].total_price_of_product_with_promotion;

                                        $scope.products_order[j].final_product_individual = $scope.products_order[j].price_after_promotion_discount;
                                        $scope.products_order[j].price_after_default_discount = 0;
                                        $scope.products_order[j].promotion = 1;
                                        $scope.products_order[j].discount = "";

                                    }

                                }
//promo avai 1
                            }else{
                                
                                $scope.products_order[j].promotion_clubed_with_default_discount='';
                                $scope.products_order[j].promotion_residual ='';
                                $scope.products_order[j].promotion_discount ='';
                                $scope.products_order[j].promotion_cashback='';
                                $scope.products_order[j].promotion_surprise_gift='';
                                $scope.products_order[j].promotion_surprise_gift_type ='';
                                $scope.products_order[j].promotion_surprise_gift_skus='';
                                $scope.products_order[j].promotion_surprise_gift_skus_nums='';
                                $scope.products_order[j].default_discount='';
                                $scope.products_order[j].promotion_item_multiplier='';
                                $scope.products_order[j].promotion_item='';
                                $scope.products_order[j].promotion_item_num='';
                                $scope.products_order[j].promotion_id_selected='';
                                $scope.products_order[j].promotion_type_selected='';
                                $scope.products_order[j].promotion_minimum_quantity='';
                                $scope.products_order[j].promotion_quote='';
                                $scope.products_order[j].promotion_default_discount_promo='';
                            }


                            if ($scope.products_order[j].promotion_available == 0) {
                                $scope.products_order[j].final_product_individual = $scope.products_order[j].selling_price * ($scope.products_order[j].inventory_moq);
                                $scope.products_order[j].discount_quote_show = 0;
                                $scope.products_order[j].promotion_quote_show = 0;
                            }


                            if ($scope.products_order[j].promotion_item != '' && $scope.products_order[j].promotion_available == 1) {

                                $scope.getFreeInventory($scope.products_order[j].promotion_item, $scope.products_order[j].inventory_id, $scope.products_order[j].promotion_item_num, $scope.products_order[j].quotient, $scope.products_order[j].show_addon_items, j, $scope.products_order[j].promotion_minimum_quantity, $scope.products_order[j].inventory_moq);

                            } else {

                                if (document.getElementById('total_num_of_free_items') != null) {
                                    document.getElementById('total_num_of_free_items').innerHTML = "";
                                }
                            }

                            ///////////////////promotion/////////////////

                            if ($scope.products_order[j].territory_duration != -1 && $scope.pincode != null && $scope.pincode != '') {
                                var months = new Array("January", "February", "March", "April", "May", "June", "July", "August", "Sep", "October", "November", "December");
                                var days = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Fri", "Saturday");

                                var d = new Date();
                                duration = parseInt($scope.products_order[j].territory_duration);
                                d.setDate(d.getDate() + duration);
                                $scope.products_order[j].delivery_date = "Delivery By " + days[d.getDay()] + " " + months[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear();
                            } else {
                                $scope.products_order[j].delivery_date = "";
                            }

                            if ($scope.products_order[j].inventory_weight_in_kg_for_unit != -1) {
                                $scope.products_order[j].inventory_weight_in_kg = parseInt($scope.products_order[j].inventory_moq) * parseFloat($scope.products_order[j].inventory_weight_in_kg_for_unit);
                                $scope.products_order[j].inventory_volume_in_kg = parseInt($scope.products_order[j].inventory_moq) * parseFloat($scope.products_order[j].inventory_volume_in_kg_for_unit);
                            }

                            $scope.products_order[j].inventory_selling_price_without_discount = (($scope.products_order[j].selling_price) * ($scope.products_order[j].inventory_moq));

                            $scope.products_order[j].inventory_selling_price_with_discount = ($scope.products_order[j].inventory_selling_price_without_discount);
                            if (parseInt($scope.products_order[j].logistics_id) != parseInt(-1) && $scope.pincode != null && $scope.pincode != '') {

                                //alert($scope.products_order[j].final_product_individual+"final_price_ind");

                                if ($scope.products_order[j].final_product_individual != 0) {
                                    $scope.products_order[j].subtotal = parseInt($scope.products_order[j].final_product_individual) + parseInt($scope.products_order[j].shipping_charge);
                                } else {
                                    $scope.products_order[j].subtotal = parseInt($scope.products_order[j].inventory_selling_price_without_discount) + parseInt($scope.products_order[j].shipping_charge);

                                }

                                $scope.products_order[j].shipping_charge_msg = "";
                                $scope.products_order[j].shipping_charge_view = 1;
								
								if ($scope.products_order[j].shipping_charge == 0) {
                                    $scope.products_order[j].msg_style = "color:green;";
                                    $scope.products_order[j].delivery_service_msg = 'Free Delivery Service';
                                } 
								 
								else {
                                    //$scope.products_order[j].msg_style = "color:#e46c0a;";
                                    //$scope.products_order[j].delivery_service_msg = 'Service available at ' + $scope.pincode;
									$scope.products_order[j].delivery_service_msg = ' at ' + $scope.pincode;
                                }
								$scope.buttonClicked=false;
                            } else {

                                $scope.products_order[j].subtotal = parseInt($scope.products_order[j].inventory_selling_price_without_discount);
                                $scope.products_order[j].shipping_charge = 0;
                                $scope.products_order[j].shipping_charge_msg = "";
                                $scope.products_order[j].shipping_charge_view = 0;
                                $scope.products_order[j].msg_style = "color:red;";
								if($scope.products_order[j].pincode_valid=='not_valid'){
                               $scope.products_order[j].delivery_service_msg = 'Invalid pincode';
                                    }else{
										if(typeof $scope.pincode==='undefined'){
								$scope.products_order[j].delivery_service_msg = 'Enter Your Delivery Location Pincode';
								}else{
								$scope.products_order[j].delivery_service_msg = 'Service Not Available at '+$scope.pincode;	
								}
                                    }
								
								//$scope.searched_common_err_pin='Invalid pincode';
								$scope.buttonClicked=true;
								
                            }

                            if ($scope.products_order[j].final_product_individual) {
                                if (parseInt($scope.products_order[j].shipping_charge_update) != parseInt(-1)) {
                                    $scope.products_order[j].subtotal = parseInt($scope.products_order[j].final_product_individual) + parseInt($scope.products_order[j].shipping_charge);
                                    total_shipping_price += parseFloat($scope.products_order[j].shipping_charge);

                                } else {
                                    $scope.products_order[j].subtotal = parseInt($scope.products_order[j].final_product_individual);
                                    //total_shipping_price+=parseFloat($scope.products_order[j].shipping_charge);
                                }
                            }

                            //alert($scope.products_order[j].subtotal+"subtotal");


                            if ($scope.products_order[j].promotion_cashback > 0) {

                                $scope.show_cash_back_tr = 1;

                                cash_back_amount = parseFloat($scope.products_order[j].cash_back_value);
                                $scope.total_cash_back_for_sku += parseFloat(cash_back_amount);

                                $scope.cash_back_tr_arr.push({
                                    "promotion_cashback_percentage": $scope.products_order[j].promotion_cashback,
                                    "product_name": $scope.products_order[j].product_name,
                                    "inventory_sku": $scope.products_order[j].inventory_sku,
                                    "cash_back_amount": cash_back_amount,
                                    "sku_count": $scope.products_order[j].inventory_moq
                                });

                            }

                            /* addon products */
                            $scope.products_order[j].addon_count=0;
                            $scope.products_order[j].addon_count_updated=0;

                            if($scope.products_order[j].addon_products_status=='1'){
                                var response=$scope.products_order[j].addon_products;
                                ////console.log('ddddddddd');
                                //console.log(typeof response);
                                if(typeof response== 'string'){
                                    rr= $scope.products_order[j].addon_products;
                                    //console.log( rr.charAt(0));
                                    if ( rr.charAt(0)=="\""){
                                        rr = rr.replace(/(^"|"$)/g, '');
                                        try {
                                            $scope.products_order[j].addon_products=JSON.parse(rr);
                                        }catch(e) {
                                            $scope.products_order[j].addon_products=rr;
                                        }

                                    }else{
                                       
                                        try {
                                            $scope.products_order[j].addon_products=JSON.parse(rr);
                                        }catch(e) {
                                            $scope.products_order[j].addon_products=rr;
                                        }
                                       
                                    }
                                }else{
                                    $scope.products_order[j].addon_products=$scope.products_order[j].addon_products;
                                }

                                $scope.products_order[j].addon_count_updated=1;
                                
                                $scope.products_order[j].addon_count=$scope.products_order[j].addon_products.length;
                                //$scope.show_addon_products(j,$scope.products_order[j].addon_inventories,$scope.products_order[j].inventory_id);

                                var addon_data=$scope.products_order[j].addon_products;
                                var tot=0; var ship=0;
                                for(u=0;u<(addon_data.length);u++){
                                    tot+=parseInt(addon_data[u].inv_price);
                                    ship+=parseInt(addon_data[u].inv_ship_price);
                                }
                                $scope.products_order[j].addon_total_price=parseInt(tot);
                                $scope.products_order[j].addon_total_shipping_price=parseInt(ship);
                                $scope.products_order[j].subtotal+=parseInt(tot);
                                $scope.total_addon_products+=parseInt(tot);
                                

                            }

                            /* addon products */



                        } else {

                            $scope.products_order[j].item_stock_available = 0;
                            $scope.products_order[j].subtotal = 0;

                            $scope.out_of_stock_item_in_cart.push(1);
                        }//checking out of stock

                        /* addon shipping price */

                        if(parseInt($scope.products_order[j].addon_total_shipping_price)>0){
                           
                            $scope.products_order[j].shipping_charge_total=(parseInt($scope.products_order[j].shipping_charge)+parseInt($scope.products_order[j].addon_total_shipping_price));
                            //alert($scope.products_order[j].shipping_charge);

                            $scope.products_order[j].subtotal+=parseInt($scope.products_order[j].addon_total_shipping_price);
                            //$scope.products_order[j].final_product_individual+=parseInt($scope.products_order[j].addon_total_shipping_price);

                            $scope.total_shipping_price+=parseInt($scope.products_order[j].addon_total_shipping_price);
                            $scope.total_shipping_price_addon+=parseInt($scope.products_order[j].addon_total_shipping_price);
                        }else{
                            $scope.products_order[j].shipping_charge_total=parseInt($scope.products_order[j].shipping_charge);
                        }

                        total_netprice += parseFloat($scope.products_order[j].subtotal);

                        individual_price_without_shipping += parseInt($scope.products_order[j].final_product_individual);

                        
                    }//forloop


                    if ($scope.cash_back_tr_arr.length > 0) {
                        $scope.show_cash_back_tr = 1;
                    } else {
                        $scope.show_cash_back_tr = 0;
                    }

                    //alert($scope.final_count_free_items+"fffff");

                    if ($scope.out_of_stock_item_in_cart.length > 0) {
                        $scope.out_of_stock_item_exists = 1;
                        $('#proceed_to_checkout_id').hide();
                        $('#continue_shopping_id').hide();

                    } else {
                        $scope.out_of_stock_item_exists = 0;
                        $('#proceed_to_checkout_id').show();
                        $('#continue_shopping_id').show();

                    }

                    if(parseFloat($scope.total_addon_products)>0){
                        individual_price_without_shipping+=parseFloat($scope.total_addon_products);
                    }

                    $scope.total_price_without_shipping_price = parseFloat(individual_price_without_shipping);


                    $scope.grandtotal_without_discount = parseInt(total_netprice);
                    $scope.grandTotal = parseInt(total_netprice);
                    $scope.eligible_discount_percentage_display = 0;
                    $scope.total_shipping_price = parseInt(total_shipping_price)+parseInt($scope.total_shipping_price_addon);

                    $scope.grandTotal_for_discount = 0;
                    $scope.total_shipping_price_actual = 0;

                    if (c_id != null && c_id == '') {

                        localStorage.setItem("LocalCustomerCart", JSON.stringify($scope.products_order));
                        
                    }


                    <?php
                    $price_list = array();
                    if(!empty($promotions_cart)){
                    ?>

                    $scope.eligible_cash_back_percentage_display = 0;
                    $scope.eligible_discount_percentage_display = 0;
                    $scope.eligible_free_shipping_display = 0;


                    $scope.eligible_free_surprise_gift_display = 0;
                    $scope.discount_oninvoice = 0;
                    $scope.cash_back_oninvoice = 0;
                    $scope.free_shipping = 0;
                    $scope.surprise_gift = 0;
                    $scope.show_surprise_gift_on_invoice = 0;
                    $scope.invoice_surprise_gift = 0;

                    $scope.promotions_all = [];
                    $scope.promo_avail_res=[];

                    
                    <?php
                    //$cart_promo_arr=array();
                    foreach($promotions_cart as $data){

                    $buy_type_arr = explode(',', $data['buy_type']);

                    if (count($buy_type_arr) == 1) {
                        if (preg_match('/surprise/', strtolower($data["promo_quote"]))) {
                            array_push($price_list, $data);
                        }

                    }

                    ?>
                    //alert('<?php echo json_encode($data); ?>');

                    $scope.promotions_cart = [];
                    var promotion_id = '<?php echo $data['promo_uid']; ?>';
                    var promo_to_buy = '<?php echo $data['to_buy']; ?>';
                    var promo_to_get = '<?php echo $data['to_get']; ?>';
                    var promo_buy_type = '<?php echo $data['buy_type']; ?>';
                    var promo_get_type = '<?php echo $data['get_type']; ?>';
                    var promo_name = '<?php echo $data['promo_name']; ?>';
                    var applied_at_invoice = '<?php echo $data['applied_at_invoice']; ?>';


                    $scope.promotion_id_arr = promotion_id.split(',');
                    $scope.promo_to_buy_arr = promo_to_buy.split(',');
                    $scope.promo_to_get_arr = promo_to_get.split(',');
                    $scope.promo_buy_type_arr = promo_buy_type.split(',');
                    $scope.promo_get_type_arr = promo_get_type.split(',');
                    $scope.promo_name_arr = promo_name.split(',');


                    $scope.promotions_cart.push({
                        'promotion_id_arr': $scope.promotion_id_arr,
                        'promo_to_buy_arr': $scope.promo_to_buy_arr,
                        'promo_to_get_arr': $scope.promo_to_get_arr,
                        'promo_buy_type_arr': $scope.promo_buy_type_arr,
                        'promo_get_type_arr': $scope.promo_get_type_arr,
                        'promo_name_arr': $scope.promo_name_arr
                    });
                    ////console.log("pomotions  cart");
                    ////console.log($scope.promotions_cart);
                    for (var t = 0; t < $scope.promotions_cart.length; t++) {

                        //alert($scope.promotions_all[t][u].promotion_id_arr);

                        promo_to_buy_arr = $scope.promotions_cart[t].promo_to_buy_arr;
                        promo_to_get_arr = $scope.promotions_cart[t].promo_to_get_arr;
                        promo_buy_type_arr = $scope.promotions_cart[t].promo_buy_type_arr;
                        promo_get_type_arr = $scope.promotions_cart[t].promo_get_type_arr;
                        promo_name_arr = $scope.promotions_cart[t].promo_name_arr;

                        discount_oninvoice_str = '';

                        for (var v = 0; v < promo_to_buy_arr.length; v++) {

                            to_buy = promo_to_buy_arr[v];
                            to_buy_previous = promo_to_buy_arr[v - 1];
                            to_get = promo_to_get_arr[v];
                            to_get_previous = promo_to_get_arr[v - 1];
                            buy_type = promo_buy_type_arr[v];
                            get_type = promo_get_type_arr[v];
                            promo_name = promo_name_arr[v];

                            if ((to_get == '') && (get_type == '')) {

                                if ($scope.total_shipping_price == 0) {
                                    $scope.free_shipping = 'Search the pincode to get <strong>Free shipping</strong> .';
                                    $scope.explicitlyTrustedHtml_free_shipping = $sce.trustAsHtml($scope.free_shipping);
                                    $scope.eligible_free_shipping_display = 0;
                                    $scope.free_shipping = 1;
                                } else {
                                    $scope.currency_type_free_shipping = buy_type;
                                    $scope.eligible_free_shipping_display = 0;
                                    $scope.free_shipping = 0;

                                    //alert(parseInt(to_buy));
                                    //alert(parseInt(individual_price_without_shipping));

                                    if (parseInt(to_buy) > parseInt(individual_price_without_shipping)) {

                                        $scope.total_shipping_price_actual = 0;
                                        //$scope.free_shipping='Expend more '+parseInt(to_buy-individual_price_without_shipping)+' '+buy_type+' to get <strong>Free shipping</strong> .';

                                        $scope.free_shipping = '<span class="shipping_note"><i class="fa fa-heart" style="color:#004b9b" class="not_yet_avail_offer"></i> If you purchase for <?php echo curr_sym; ?>' + parseInt(to_buy - individual_price_without_shipping) + ' more, you can avail <strong>Free shipping</strong></span>'
                                        if (freeshippingoffer_swal_count == 0) {
                                            cashbackoffer_discountoffer_freeshippingoffer_swal_str += $scope.free_shipping + "<br>";
                                        }
                                        freeshippingoffer_swal_count++;


                                        $scope.explicitlyTrustedHtml_free_shipping = $sce.trustAsHtml($scope.free_shipping);
                                        $scope.eligible_free_shipping_display = 0;


                                        $scope.free_shipping = 1;

                                    } else {
                                        $scope.availed_offers_heading_flag = "yes";
                                        $scope.free_shipping = '<i class="fa fa-heart" style="color:#ff0000"  class="already_availed_offer"></i>' + " <b>Bravo!</b> You are eligible for Free Shipping!";
                                        if (freeshippingoffer_swal_count == 0) {
                                            cashbackoffer_discountoffer_freeshippingoffer_swal_str += $scope.free_shipping + "<br>";
                                        }
                                        freeshippingoffer_swal_count++;
                                        $scope.explicitlyTrustedHtml_free_shipping = $sce.trustAsHtml($scope.free_shipping);
                                        //alert($scope.explicitlyTrustedHtml_free_shipping)

                                        //$scope.explicitlyTrustedHtml_free_shipping = $sce.trustAsHtml('');
                                        $scope.eligible_free_shipping_display = 1;


                                        $scope.total_shipping_price_actual = $scope.total_shipping_price;
                                        //$scope.free_shipping=0; // I commented in preparing the You are eligible for Free Shipping! message
                                    }
                                }
                            }

                            if (get_type == "discount") {

                                ////console.log("tobuy"+parseInt(to_buy)+"individualprice"+parseInt(individual_price_without_shipping)+"to get previous"+to_get_previous);

                                $scope.discount_oninvoice = 0;

                                if (parseInt(to_buy) > individual_price_without_shipping) {
                                    if (to_get_previous > 0) {
                                        $scope.grandTotal_for_discount = Math.round((to_get_previous / 100) * individual_price_without_shipping);
                                        $scope.eligible_discount_percentage_display = 1;
                                        $scope.eligible_discount_percentage = to_get_previous;
                                        $scope.currency_type_discount = buy_type;
                                    } else {
                                        $scope.eligible_discount_percentage_display = 0;
                                        $scope.explicitlyTrustedHtml_discount = $sce.trustAsHtml('');
                                    }
                                    if (individual_price_without_shipping < to_buy) {
                                        $scope.discount_oninvoice = 1;
//$scope.discount_oninvoice='Expend more '+parseInt(to_buy-individual_price_without_shipping)+' '+buy_type+' to get <strong>'+to_get+'% '+get_type+' on INVOICE</strong> .';


// Expend more 295 INR to get 30% on INVOICE .

//$scope.discount_oninvoice='On purchases above <?php echo curr_sym; ?> '+to_buy_previous+', you are eligible for '+to_get_previous+'% '+get_type+'.<br><br>To avail '+to_get+'% '+get_type+', kindly expend <?php echo curr_sym; ?> '+parseInt(to_buy-individual_price_without_shipping)+' more!';

                                        if (to_get_previous === undefined) {
                                            discount_oninvoice_str = '<span class="shipping_note"><i class="fa fa-heart" style="color:#004b9b" class="not_yet_avail_offer"></i> If you purchase for <?php echo curr_sym; ?>' + parseInt(to_buy - individual_price_without_shipping) + ' more, you can avail ' + to_get + '% discount!</span>'
                                        } else {
                                            $scope.availed_offers_heading_flag = "yes";
                                            discount_oninvoice_str = '<span class="shipping_note"><i class="fa fa-heart" style="color:#ff0000"  class="already_availed_offer"></i> You availed ' + to_get_previous + '% discount on purchase of above <?php echo curr_sym; ?> ' + to_buy_previous + '. <br><i class="fa fa-heart" style="color:#004b9b" class="not_yet_avail_offer"></i> If you purchase for <?php echo curr_sym; ?>' + parseInt(to_buy - individual_price_without_shipping) + ' more, you can avail ' + to_get + '% discount!</span>';
                                        }

                                        $scope.discount_oninvoice = discount_oninvoice_str;

                                        $scope.explicitlyTrustedHtml_discount = $sce.trustAsHtml($scope.discount_oninvoice);
                                        if (discountoffer_swal_count == 0) {
                                            //  cashbackoffer_discountoffer_freeshippingoffer_swal_str += discount_oninvoice_str + "<br>"; /// comented
                                        }
                                        discountoffer_swal_count++;

                                        /*
                                swal({
                                            title: '',
                                            html:"You are eligible for discount",
                                            type: 'info',
                                            showCancelButton: false,
                                            showConfirmButton: false
                                            }).then(function () {

                                                });*/

                                    } else {
                                        $scope.discount_oninvoice = 0;
                                        $scope.explicitlyTrustedHtml_discount = $sce.trustAsHtml("");
                                    }
                                    break;

                                } else {
                                    //individual_price_without_shipping - reached the maximum dicount price or percentage
                                    if (to_get > 0) {
                                        if (to_get_previous !== undefined) {

                                            $scope.availed_offers_heading_flag = "yes";
                                            discount_oninvoice_str = '<span class="shipping_note"><i class="fa fa-heart" style="color:#ff0000"  class="already_availed_offer"></i> You availed ' + to_get + '% discount on purchase of above <?php echo curr_sym; ?> ' + to_buy + '.</span>';
                                            $scope.discount_oninvoice = discount_oninvoice_str;

                                            $scope.explicitlyTrustedHtml_discount = $sce.trustAsHtml($scope.discount_oninvoice);

                                            /*  if (discountoffer_swal_count == 0) {
                                        cashbackoffer_discountoffer_freeshippingoffer_swal_str += discount_oninvoice_str + "<br>";
                                    }*/
                                            discountoffer_swal_count++;
                                        }
                                        $scope.grandTotal_for_discount = Math.round((to_get / 100) * individual_price_without_shipping)
                                        $scope.eligible_discount_percentage_display = 1;
                                        $scope.eligible_discount_percentage = to_get;
                                        $scope.currency_type_discount = buy_type;


                                    } else {
                                        $scope.eligible_discount_percentage_display = 0;

                                    }

                                }
                            }

                            if (get_type == "cash back") {
                                $scope.cash_back_oninvoice = 0;
                                if (parseInt(to_buy) > individual_price_without_shipping) {
                                    if (to_get_previous > 0) {
                                        $scope.grandTotal_for_cash_back = parseFloat((to_get_previous / 100) * individual_price_without_shipping).toFixed(2);
                                        $scope.eligible_cash_back_percentage_display = 1;
                                        $scope.eligible_cash_back_percentage = to_get_previous;
                                        $scope.currency_type_cash_back = buy_type;
                                    } else {
                                        $scope.eligible_cash_back_percentage_display = 0;
                                        $scope.explicitlyTrustedHtml_cash_back = $sce.trustAsHtml('');
                                    }
                                    if (individual_price_without_shipping < to_buy) {
                                        $scope.cash_back_oninvoice = 1;
//$scope.cash_back_oninvoice='Expend more '+parseInt(to_buy-individual_price_without_shipping)+' '+buy_type+' to get <strong>'+to_get+'% '+get_type+' on INVOICE</strong> .';

// Expend more 295 INR to get 30% cash back on INVOICE .

//We are crediting 25% cashback to your wallet. <br> If you purchase for Rs 300 more, you can get 30% cashback!
                                        if (to_get_previous === undefined) {
                                            cash_back_oninvoice_str = '<span class="shipping_note"><i class="fa fa-heart" style="color:#004b9b" class="not_yet_avail_offer"></i> If you purchase for <?php echo curr_sym; ?>' + parseInt(to_buy - individual_price_without_shipping) + ' more, you can avail ' + to_get + '% cashback!</span>'
                                        } else {
                                            $scope.availed_offers_heading_flag = "yes";
                                            cash_back_oninvoice_str = '<span class="shipping_note"><i class="fa fa-heart" style="color:#ff0000"  class="already_availed_offer"></i> We are crediting ' + to_get_previous + '% cashback to your wallet. <br><i class="fa fa-heart" style="color:#004b9b" class="not_yet_avail_offer"></i> If you purchase for <?php echo curr_sym; ?>' + parseInt(to_buy - individual_price_without_shipping) + ' more, you can avail ' + to_get + '% cashback!</span>';
                                        }

                                        $scope.cash_back_oninvoice = cash_back_oninvoice_str;


                                        $scope.explicitlyTrustedHtml_cash_back = $sce.trustAsHtml($scope.cash_back_oninvoice);
                                        if (cashbackoffer_swal_count == 0) {
                                            cashbackoffer_discountoffer_freeshippingoffer_swal_str += cash_back_oninvoice_str + "<br>";
                                        }
                                        cashbackoffer_swal_count++;

                                    } else {
                                        $scope.cash_back_oninvoice = 0;
                                        $scope.explicitlyTrustedHtml_cash_back = $sce.trustAsHtml("");
                                    }
                                    break;
                                } else {
                                    if (to_get > 0) {
                                    //console.log("to_get="+to_get+"to_get_previous="+to_get_previous);
                                        if (to_get_previous !== undefined) {
                                            $scope.availed_offers_heading_flag = "yes";
                                            cash_back_oninvoice_str = '<span class="shipping_note"><i class="fa fa-heart" style="color:#ff0000"  class="already_availed_offer"></i> We are crediting ' + to_get + '% cashback to your wallet.</span>';

                                            $scope.cash_back_oninvoice = cash_back_oninvoice_str;


                                            $scope.explicitlyTrustedHtml_cash_back = $sce.trustAsHtml($scope.cash_back_oninvoice);
                                            if (cashbackoffer_swal_count == 0) {
                                                cashbackoffer_discountoffer_freeshippingoffer_swal_str += cash_back_oninvoice_str + "<br>";
                                            }
                                            cashbackoffer_swal_count++;


                                        }

                                        $scope.grandTotal_for_cash_back = parseFloat((to_get / 100) * individual_price_without_shipping).toFixed(2);
                                        $scope.eligible_cash_back_percentage_display = 1;
                                        $scope.eligible_cash_back_percentage = to_get;
                                        $scope.currency_type_cash_back = buy_type;


                                    } else {
                                        $scope.eligible_cash_back_percentage_display = 0;

                                    }

                                }
                            }

                        }
                        if (discount_oninvoice_str != '') {
                            cashbackoffer_discountoffer_freeshippingoffer_swal_str += discount_oninvoice_str + "<br>";
                        }
                    }

                    <?php
                    }

                    $m = array();
                    foreach ($price_list as $key => $row) {
                        $m[$key] = $row['to_buy'];
                    }
                    array_multisort($m, SORT_ASC, $price_list);

                    }?>
                    $scope.applied_at_invoice_arr =<?php echo json_encode($price_list); ?>;
                    //alert(applied_at_invoice_arr);

                    if ($scope.applied_at_invoice_arr.length > 0) {

                        $scope.show_surprise_gift_on_invoice = 1;

                        for (h = 0; h < $scope.applied_at_invoice_arr.length; h++) {

                            if (parseInt($scope.applied_at_invoice_arr[h].to_buy) > parseInt(individual_price_without_shipping)) {

                                //for top alert
                                expand_more = parseInt($scope.applied_at_invoice_arr[h].to_buy) - parseInt(individual_price_without_shipping);

                                $scope.invoice_surprise = '<i class="fa fa-heart" style="color:#004b9b" class="not_yet_avail_offer"></i> To eligible for (' + $scope.applied_at_invoice_arr[h].promo_quote + ') expend more ' + expand_more + ' ' + $scope.applied_at_invoice_arr[h].buy_type;
                                $scope.explicitlyTrustedHtml_invoice_surprise_gift = $sce.trustAsHtml($scope.invoice_surprise);
                                $scope.invoice_surprise_gift = 1;

                                //for bootom messsage

                                if ($scope.applied_at_invoice_arr[h - 1] != null) {
                                    $scope.invoice_surprise_result = $scope.applied_at_invoice_arr[h - 1].promo_quote;
                                    $scope.explicitlyTrustedHtml_invoice_surprise_gift_result = $sce.trustAsHtml($scope.invoice_surprise_result);
                                    $scope.invoice_surprise_gift_result = 1;
                                } else {

                                    $scope.explicitlyTrustedHtml_invoice_surprise_gift_result = $sce.trustAsHtml("");
                                    $scope.invoice_surprise_gift_result = 0;

                                }
                                break;

                            } else {
                                
                                /*
                                $scope.explicitlyTrustedHtml_invoice_surprise_gift = $sce.trustAsHtml("");
                                $scope.invoice_surprise_gift = 0;
                                */
                               //commented to show on top message
                               $scope.invoice_surprise = '<i class="fa fa-heart" style="color:#ff0000"  class="already_availed_offer"></i> <b>Bravo!</b> You are eligible for surprise gift <i class="fa fa-gift" aria-hidden="true"></i>';
                               $scope.explicitlyTrustedHtml_invoice_surprise_gift = $sce.trustAsHtml($scope.invoice_surprise);;
                               $scope.invoice_surprise_gift = 1;
                               
                                $scope.invoice_surprise_result = $scope.applied_at_invoice_arr[h].promo_quote;
                                $scope.explicitlyTrustedHtml_invoice_surprise_gift_result = $sce.trustAsHtml($scope.invoice_surprise_result);
                                $scope.invoice_surprise_gift_result = 1;
                            }
                        }

                    } else {
                        $scope.show_surprise_gift_on_invoice = 0;
                    }


                    if ($scope.eligible_discount_percentage_display == 1 || $scope.eligible_free_shipping_display == 1) {
                        if ($scope.total_shipping_price_actual) {
                            $scope.grandTotal_after_deduction = (parseInt($scope.grandtotal_without_discount) - parseInt($scope.grandTotal_for_discount) - parseInt($scope.total_shipping_price_actual));
                            $scope.amount_to_pay = $scope.grandTotal_after_deduction;
                        } else {
                            $scope.amount_to_pay = parseInt($scope.grandtotal_without_discount) - parseInt($scope.grandTotal_for_discount);
                        }

                    } else {
                        $scope.amount_to_pay = parseInt($scope.grandTotal);
                    }

                    if ($scope.eligible_discount_percentage_display == 1 || $scope.eligible_free_shipping_display == 1) {

                        if (parseInt($scope.total_shipping_price_actual)) {
                            $scope.you_saved = parseInt($scope.grandTotal_for_discount) + parseInt($scope.total_shipping_price_actual) ;
                        } else {
                            $scope.you_saved = parseInt($scope.grandTotal_for_discount)
                        }

                    } else {
                        $scope.you_saved = 0;
                    }

                    if ($scope.show_cash_back_tr == 1) {
                        if ($scope.grandTotal_for_cash_back == null) {
                            $scope.grandTotal_for_cash_back = 0;
                        }
                        if ($scope.total_cash_back_for_sku == null) {
                            $scope.total_cash_back_for_sku = 0;
                        }
                        $scope.total_cash_back = parseFloat($scope.grandTotal_for_cash_back) + parseFloat($scope.total_cash_back_for_sku);
                        $scope.total_cash_back = $scope.total_cash_back.toFixed(2);
                    } else {
                        //alert($scope.grandTotal_for_cash_back);

                        if (parseInt($scope.grandTotal_for_cash_back) > 0) {

                            $scope.total_cash_back = parseInt($scope.grandTotal_for_cash_back);
                        } else {
                            $scope.total_cash_back = 0;
                        }
                    }

                    /// showing swal starts ////

                    if ($scope.availed_offers_heading_flag == "yes") {
                        //alert("yes")
                        cashbackoffer_discountoffer_freeshippingoffer_swal_title = '<h2 style="color:#004b9b">Congrats!</h2>';
                        $scope.explicitlyTrustedHtml_availed_offers_heading = $sce.trustAsHtml(cashbackoffer_discountoffer_freeshippingoffer_swal_title);
                    } else {

                        cashbackoffer_discountoffer_freeshippingoffer_swal_title = '<h2 style="color:#004b9b">You\'ve Got This!</h2>';
                        $scope.explicitlyTrustedHtml_availed_offers_heading = $sce.trustAsHtml(cashbackoffer_discountoffer_freeshippingoffer_swal_title);
                        //alert($scope.explicitlyTrustedHtml_availed_offers_heading)
                    }
                    if (cashbackoffer_discountoffer_freeshippingoffer_swal_count == 0) {
                        $scope.explicitlyTrustedHtml_availed_offers_heading = "";
                        if (cashbackoffer_discountoffer_freeshippingoffer_swal_str != "") {
                            if (cashbackoffer_discountoffer_freeshippingoffer_swal_str.indexOf("already_availed_offer") != -1) {
                                cashbackoffer_discountoffer_freeshippingoffer_swal_title = '<h2 style="color:#004b9b">Congrats!</h2>';
                                $scope.explicitlyTrustedHtml_availed_offers_heading = $sce.trustAsHtml(cashbackoffer_discountoffer_freeshippingoffer_swal_title);
                            } else {
                                cashbackoffer_discountoffer_freeshippingoffer_swal_title = '<h2 style="color:#004b9b">You\'ve Got This!</h2>';
                                $scope.explicitlyTrustedHtml_availed_offers_heading = $sce.trustAsHtml(cashbackoffer_discountoffer_freeshippingoffer_swal_title);
                            }
                            /*swal({
                                title: cashbackoffer_discountoffer_freeshippingoffer_swal_title,
                                html: cashbackoffer_discountoffer_freeshippingoffer_swal_str,

                                showCancelButton: false,
                                showConfirmButton: true,
                                confirmButtonText: 'Ok',
                            }).then(function () {
                                //cashbackoffer_discountoffer_freeshippingoffer_swal_str = "";
                            });*/
							cashbackoffer_discountoffer_freeshippingoffer_swal_str = "";
                        }
                    }
                    cashbackoffer_discountoffer_freeshippingoffer_swal_count++;
                    /// showing swal ends ////

                }

                
                $scope.get_promotion_active_status=function(promo_id_selected,ty=''){
                    
                //alert(promo_id_selected+"id selected");
                    //if promotion available check active and expired status

                       
                        $http({
                            method: "POST",
                            url: "<?php echo base_url();?>get_promotion_active_status",
                            data: {'promo_id_selected': promo_id_selected},
                            dataType: "JSON",
                            async: false
                        }).success(function mySucces(result) {
                            if (jQuery.isEmptyObject(result)) {
                                //alert('promo_inactive');
                                //return 2;//promo inactive or expired
                                $scope.promo_temp=2;
                            }else{
                                //alert('promo_active');
                                //return 1;//promo active
                                $scope.promo_temp=1;
                            }
                        });
                }
                $scope.get_promotion_active_status_all=function(order_product_count, products_order, w = ''){
                        //alert(2);
                        $http({
                            method: "POST",
                            url: "<?php echo base_url();?>get_promotion_active_status_all",
                            data: {'products_order': products_order},
                            dataType: "JSON",
                            async: false
                        }).success(function mySucces(result) {
                            if (jQuery.isEmptyObject(result)) {
                                
                            }else{
                          
                                res=JSON.parse(JSON.stringify(result));
                                ////console.log(res);
                                for($k=0;$k<res.length;$k++){
                                    
                                    if(jQuery.isEmptyObject(res[$k])){
                                        //alert($scope.products_order[$k].promotion_available+"promo");
                                        if($scope.products_order[$k].promotion_available==1){
                                        //promotion active in localStorage but in backend it is inactive or expired
                                            $scope.products_order[$k].promotion_available=0;
                                            //alert($scope.products_order[$k].promotion_available+"inside if" +$k);
                                            
                                            /* all other values reset*/
                                            
                                            $scope.products_order[$k].individual_price_of_product_with_promotion = '0';
                                            $scope.products_order[$k].individual_price_of_product_without_promotion = '0';
                                            $scope.products_order[$k].total_price_of_product_without_promotion = '0';
                                            $scope.products_order[$k].total_price_of_product_with_promotion = '0';
                                            $scope.products_order[$k].quantity_without_promotion = '0';
                                            $scope.products_order[$k].quantity_with_promotion = '0';
                                            cash_back_amount = "0";
                                            $scope.products_order[$k].cash_back_value = cash_back_amount;
                                            $scope.products_order[$k].cash_back_amount = "";
                                            /* all other values reset*/
                                            
                                            //alert(90);
                                            ////console.log($scope.products_order);
                                            
                                        }else{
                                            //$scope.products_order[$k].promotion_available=0;
                                        }
                                        
                                    }else{
                                        //$scope.products_order[$k].promotion_available=0;
                                        //$scope.products_order[$k].promo_temp=1;
                                    }
                                }
                            }
                            ////console.log($scope.products_order);
                            //return false;
                            $scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order,7);
                            if ($scope.pincode != null) {
                                //$scope.check_availability_pincodeFun();
                                $scope.get_corresponding_shipping_charge('onload');
                            }

                           
                        });
                }
                $scope.get_promotion_availability_all=function(order_product_count, products_order, w = ''){
                        //alert(2);
                        $http({
                            method: "POST",
                            url: "<?php echo base_url();?>get_promo_availability_all",
                            data: {'products_order': products_order},
                            dataType: "JSON",
                            async: false
                        }).success(function mySucces(result) {
                            if (jQuery.isEmptyObject(result)) {
                                $scope.promo_avail_res=[];
                            }else{
                                // need to overwrite scope.products_orders promo availability status
                                // $scope.products_order[j];   
                                // $scope.promo_avail_res=JSON.parse(result);
                                $scope.promo_avail_res=JSON.parse(JSON.stringify(result));
                                for($k=0;$k<$scope.promo_avail_res.length;$k++){
                                    //alert($scope.promo_avail_res[$k]);
                                    if(jQuery.isEmptyObject($scope.promo_avail_res[$k])){
                                        $scope.products_order[$k].new_promo_avail=0;
                                    }else{
                                        $scope.products_order[$k].new_promo_avail=1;
                                        $scope.new_promo_avail++;
                                    }
                                }
                                ////console.log($scope.promo_avail_res);
                                //alert(3);
                            }
                           
                        });
                }
                
                $scope.get_promotion_availability=function(promo_id_selected,inv_id,j){
                    

                //alert(promo_id_selected+"id selected");
                    //if promotion available check active and expired status
     
                        $http({
                            method: "POST",
                            url: "<?php echo base_url();?>get_promo_availability",
                            data: {'promo_id_selected': promo_id_selected,'inv_id':inv_id},
                            dataType: "JSON",
                            async: false
                        }).success(function mySucces(result) {
                            if (jQuery.isEmptyObject(result)) {
                                //alert('promo_inactive');
                                //return 2;//promo inactive or expired
                                $scope.new_promo_avail=0;
                                $scope.products_order[j].new_promo_avail=0;
                            }else{
                                //alert('promo_active');
                                //return 1;//promo active
                                $scope.new_promo_avail=1;
                                $scope.products_order[j].new_promo_avail=1;
                                
                            }
                           
                        });
                }
                
                $scope.avail_new_promotion=function(inventory_id,x){
                            swal({
                                title: "",
                                html: "This product contains new discout or offer(s).So you can check the price and add the product to cart again, Are you sure to avail the offer(s) ?"
                                ,
                                type: "info",
                                showCancelButton: true,
                                showConfirmButton: true,
                                confirmButtonText: 'Yes, Proceed',
                            }).then(function (res) {
                                if(res==true){
                                    $scope.removeItem(x);
                                    location.href="<?php echo base_url() ?>detail/<?php echo $controller->sample_code(); ?>"+inventory_id;
                                }
                            });
                }

                $scope.show_addon_products = function(j,addon_inventories,main_inventory_id){
                    $http({
                        method: "POST",
                        url: "<?php echo base_url();?>get_addon_products",
                        data: {'inv_index':j,'addon_inventories': addon_inventories, 'main_inventory_id': main_inventory_id},
                        dataType: "JSON",
                        async: false
                    }).success(function mySucces(result) {

                        if (document.getElementById('addon_products_' + j) != null) {
                            document.getElementById('addon_products_' + j).innerHTML = result;
                            db_val=$('#addon_total_price_'+main_inventory_id).val();
                            $scope.products_order[j].addon_total_price=parseInt(db_val);
                        }
                    });

                }
                $scope.remove_addon_products = function(j,addon_inv_id,addon_inv_price){

                    tt=confirm('Are you sure to remove the additional item?');
                    ////console.log(j+'||'+addon_inv_id+'||'+addon_inv_price)
                    if(tt){
                        //alert($scope.products_order[j].addon_products);

                        if($scope.products_order[j].addon_products){
                            let index1 = $scope.products_order[j].addon_products.findIndex((element) => element["inv_id"] == addon_inv_id);
                            $scope.products_order[j].addon_products.splice(index1, 1); 

                            
                            var addon_data=$scope.products_order[j].addon_products;
                            var tot=0; var ship=0;
                            for(u=0;u<(addon_data.length);u++){
                                tot+=parseInt(addon_data[u].inv_price);
                                ship+=parseInt(addon_data[u].inv_ship_price);
                            }
                            $scope.products_order[j].addon_total_price=parseInt(tot);
                            $scope.products_order[j].addon_total_shipping_price=parseInt(ship);
                            
                            
                        }   
                    }
                    var addon_status=jQuery.isEmptyObject($scope.products_order[j].addon_products);
                    var addon_products=JSON.stringify($scope.products_order[j].addon_products);//local variable

                    
                    if(addon_status==true){
                        addon_products_status='0';
                    }else{
                        addon_products_status='1';
                    }
                    var ai=$scope.products_order[j].addon_inventories.split(",");
                    removeItem=addon_inv_id;
                    ai = jQuery.grep(ai, function(value) {
                        return value != removeItem;
                    });
                    ai=ai.join(',');

                    $scope.products_order[j].addon_inventories=ai;
                    $scope.products_order[j].addon_products_status=addon_products_status;

                    $("#loading").fadeIn();
                    if (c_id != '' && c_id != null) {
                        total_netprice = 0;
                        var remove_cart_info_post_data = {'cart_id': $scope.products_order[j].cart_id,'addon_total_price':$scope.products_order[j].addon_total_price,'addon_products_status':addon_products_status,'addon_products':addon_products,'addon_inventories':ai,'addon_total_shipping_price':$scope.products_order[j].addon_total_shipping_price};
                        $http({
                            method: "POST",
                            url: "<?php echo base_url();?>Account/remove_cart_info_for_session_customer_addon",
                            data: remove_cart_info_post_data,
                            dataType: "JSON",
                        }).success(function mySucces(result) {

                            //$scope.products_order.splice(x, 1);
                            
                           
                            $scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order);
                            $scope.get_corresponding_shipping_charge('remove_addon');
                            $("#loading").fadeOut();
                        }, function myError(response) {

                        });
                    }else{
      
                        $scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order);
                        localStorage.setItem("LocalCustomerCart", JSON.stringify($scope.products_order));
                        
                        $scope.get_corresponding_shipping_charge('remove_addon');

                        $("#loading").fadeOut();
                
                    
                    return false;

                    if (document.getElementById('addon_inv_div_' + j+'_'+main_inventory_id+'_'+addon_inv_id) != null) {

                    }

                    /*$http({
                        method: "POST",
                        url: "<?php echo base_url();?>get_addon_products",
                        data: {'inv_index':j,'addon_inv_id': addon_inv_id, 'main_inventory_id': main_inventory_id},
                        dataType: "JSON",
                        async: false
                    }).success(function mySucces(result) {

                        if (document.getElementById('addon_products_' + j) != null) {
                            document.getElementById('addon_products_' + j).innerHTML = result;
                            db_val=$('#addon_total_price_'+main_inventory_id).val();
                            $scope.products_order[j].addon_total_price=db_val;
                        }
                    });*/
                }
            }

                $scope.getFreeInventory = function (obj, inv, num, quotient, addons_show, i, promotion_minimum_quantity, inventory_moq) {

                    $http({
                        method: "POST",
                        url: "<?php echo base_url();?>get_free_inventory_items_in_promotions",
                        data: {'free_inventory': obj, 'inv': inv},
                        dataType: "JSON",
                        async: false
                    }).success(function mySucces(result) {

                        //document.getElementsByClassName('extra_data')[i].innerHTML=result

                        if (document.getElementById('extra_data_' + i) != null) {

                            document.getElementById('extra_data_' + i).innerHTML = result
                        }

                        var total_num = 0;

                        var free_nums_for_invs = num.split(',')
                        var promotion_item_free = obj.split(',')

                        for (var k = 0; k < free_nums_for_invs.length; k++) {
                            quotient_q=0;//added new
                            
                            if (document.getElementById("free_inv_nums_" + promotion_item_free[k] + "_" + inv) != null) {

                                quotient_q = inventory_moq / promotion_minimum_quantity
                                quotient_q = Math.floor(quotient_q)
                                document.getElementById("free_inv_nums_" + promotion_item_free[k] + "_" + inv).innerHTML = quotient_q * free_nums_for_invs[k];
                                document.getElementById("free_inv_nums_quantity_show_frontend_strike_" + promotion_item_free[k] + "_" + inv).innerHTML = quotient_q * free_nums_for_invs[k];

                                document.getElementById("free_inv_nums_quantity_show_frontend_strike_totalprice_" + promotion_item_free[k] + "_" + inv).innerHTML = "<?php echo curr_sym; ?>" + (Math.round((parseFloat(document.getElementById("free_inv_nums_quantity_show_frontend_strike_" + promotion_item_free[k] + "_" + inv).innerHTML) * parseFloat(document.getElementById("free_inv_nums_quantity_show_frontend_strike_sellingprice_" + promotion_item_free[k] + "_" + inv).innerHTML)) * 100) / 100);

                            }
                            //alert(("free_inv_nums_"+promotion_item_free[k]+"_"+inv))
                            total_num += quotient_q * free_nums_for_invs[k];
                        }


                        if (document.getElementById('total_num_of_free_items') != null) {
                            if (total_num > 0) {
                                document.getElementById('total_num_of_free_items').innerHTML = " + " + total_num + " Free Items";
                            } else {
                                document.getElementById('total_num_of_free_items').innerHTML = "";
                            }
                        }

                        if (total_num != null && total_num != 0) {
                            $scope.calculate_free_items(total_num);
                        }

                    }, function myError(response) {

                    });

                }

                $scope.calculate_free_items = function (total_num) {
                    $scope.count_of_free_items.push(total_num);
                    temp = 0
                    for (c = 0; c < $scope.count_of_free_items.length; c++) {
                        temp += parseInt($scope.count_of_free_items[c]);
                    }
                    $scope.final_count_free_items = temp;
                    //alert($scope.final_count_free_items);
                }
                $scope.remove_error = function () {
                    $('#pincode_required').hide();
                }

                $scope.change_shipping_charge_change_button_text = function () {
                    
                    if ($("#check_shipping_charge_btn").html() == "Check") {
                        if ($scope.pincode != null && $scope.pincode != '') {
                            $("#check_shipping_charge_btn").html("Change");
                            $("#available_pincode").blur();
                            $("#available_pincode").css({"background-color": "#eee"});
                        }
                    } else {
                        $("#check_shipping_charge_btn").html("Check");
                        $("#available_pincode").focus();
                        $("#available_pincode").css({"background-color": "#fff"});
                    }
                }
                $scope.set_shipping_charge_change_button_text = function () {
                    $("#check_shipping_charge_btn").html("Check");
                    $("#available_pincode").css({"background-color": "#fff"});
                }
                $scope.hide_error = function () {
                    $scope.searched_common_err_pin='';
                }
                $scope.get_corresponding_shipping_charge = function (event_of_calling_the_fun) {
                    
                    
                    if ($("#check_shipping_charge_btn").html() == "Change" || ( $scope.pincode == "" || $scope.pincode==null || typeof $scope.pincode === "undefined")) {
                       if($scope.pincode == "" || $scope.pincode==null || typeof $scope.pincode === "undefined"){
                            $('#pincode_required').show();
                       }
					   if(event_of_calling_the_fun=="click"){
						return false;
					   }
                    }else{
                                                
                       $('#pincode_required').hide(); 
                    }
					
					
					
					
					
					$scope.order_product_count = $scope.products_order.length;
                    for (jx = 0; jx < $scope.order_product_count; jx++) {
					if (($scope.products_order[jx].inventory_moq === "" || parseInt($scope.products_order[jx].inventory_moq) === 0 || isNaN($scope.products_order[jx].inventory_moq)===true )) {
						$scope.products_order[jx].inventory_moq=$scope.products_order[jx].inventory_moq_display;
					}
					}
                    
                    if ($scope.pincode != null && $scope.pincode != '') {
                        data = {'pincode': $scope.pincode, 'cart_data': $scope.products_order};

                        $("#check_shipping_charge_btn").prop('disabled',true);
                        document.getElementById('check_shipping_charge_btn').style.pointerEvents = 'none';
                        
                        $http({
                            method: "POST",
                            url: "<?php echo base_url();?>get_corresponding_shipping_charge",
                            data: data,
                            dataType: "JSON",
                            async: false
                        }).success(function mySucces(result) {
                           $("#check_shipping_charge_btn").prop('disabled',false);
                           document.getElementById('check_shipping_charge_btn').style.pointerEvents = 'auto';
						   //alert(j)
                            localStorage.searched_pincode = $scope.pincode;
                            if (localStorage.searched_pincode != null && localStorage.searched_pincode != '') {
                                if (result.length > 0) {
                                    $("#available_pincode_div").show();
                                } else {
                                    $("#available_pincode_div").hide();
                                }

                            } else {
                                if (result.length > 0) {
                                    $("#available_pincode_div").show();
                                } else {
                                    $("#available_pincode_div").hide();
                                }


                            }

                            $scope.searched_common_err_pin='';


							
							
							
                            for (u = 0; u < result.length; u++) {
								//if(failcase_arr.includes(u)){
									//continue;
								//}
							
                                //alert(result[u].pincode_valid);
                                if (result[u].delivery_service == true) {
									//alert(JSON.stringify(result[u]))
                                    $scope.products_order[u].logistics_price = result[u].logistics_price;
                                    $scope.products_order[u].shipping_charge = result[u].shipping_charge;
                                    $scope.products_order[u].shipping_charge_update = 0;
                                    $scope.products_order[u].delivery_service = result[u].delivery_service;
                                    $scope.products_order[u].logistics_id = result[u].logistics_id;
                                    $scope.products_order[u].logistics_territory_id = result[u].logistics_territory_id;
                                    $scope.products_order[u].default_delivery_mode_id = result[u].default_delivery_mode_id;
                                    $scope.products_order[u].logistics_parcel_category_id = result[u].logistics_parcel_category_id;
                                    $scope.products_order[u].parcel_category_multiplier = result[u].parcel_category_multiplier;
                                    $scope.products_order[u].weight_multiplier = result[u].weight_multiplier;
                                    $scope.products_order[u].territory_duration = result[u].territory_duration_org;
									$scope.products_order[u].pincode_valid = result[u].pincode_valid;
                                    //$scope.products_order[u].delivery_service_msg = 'Service available at ' + $scope.pincode;
									$scope.products_order[u].delivery_service_msg = ' at ' + $scope.pincode;
                                    $scope.products_order[u].msg_style = "color:#333;";
                                    localStorage.setItem("LocalCustomerCart", JSON.stringify($scope.products_order))
                                }
                                if (result[u].delivery_service == false) {
                                    $scope.products_order[u].logistics_price = -1;
                                    $scope.products_order[u].shipping_charge = -1;
                                    $scope.products_order[u].shipping_charge_update = -1;
                                    $scope.products_order[u].delivery_service = false;
                                    $scope.products_order[u].logistics_id = -1;
                                    $scope.products_order[u].logistics_territory_id = -1;
                                    $scope.products_order[u].default_delivery_mode_id = -1;
                                    $scope.products_order[u].logistics_parcel_category_id = -1;
                                    $scope.products_order[u].parcel_category_multiplier = -1;
                                    $scope.products_order[u].weight_multiplier = -1;
                                    $scope.products_order[u].territory_duration = -1;
									$scope.products_order[u].pincode_valid = result[u].pincode_valid;
                                    $scope.products_order[u].msg_style = "color:red;";
                                    //$scope.products_order[u].delivery_service_msg = 'Service Not Available';

                                    if(result[u].pincode_valid=='not_valid'){
                                        $scope.products_order[u].delivery_service_msg = 'Invalid pincode';
                                        $('#common_err_pin').show();
                                        //$scope.searched_common_err_pin='Invalid pincode';
                                    }else{
										if($scope.pincode=='undefined'){
								$scope.products_order[u].delivery_service_msg = 'Enter Your Delivery Location Pincode';
								}else{
								$scope.products_order[u].delivery_service_msg = 'Service Not Available at '+$scope.pincode;	
								}
                                        $('#common_err_pin').hide();
                                    }

                                }
                                if (result[u].delivery_service == "free") {
                                    //alert('delivery charges free');
                                    $scope.products_order[u].logistics_price = 0;
                                    $scope.products_order[u].shipping_charge = 0;
                                    $scope.products_order[u].delivery_service = "free";
                                    $scope.products_order[u].msg_style = "color:green;";
                                    $scope.products_order[u].delivery_service_msg = 'Free Delivery service';
                                }


                            }
							//document.write(JSON.stringify($scope.products_order)+"xxx")
                            order_product_count = $scope.products_order.length;

                            var update_cart_info_post_data = $scope.products_order;
//alert(j+"=");
                            if (c_id != null && c_id != '') {
                                $scope.update_and_get_cartable_info_in_json_format_multiple(update_cart_info_post_data);
                            } else {
                                $scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order, 1);
                            }
                        }, function myError(response) {

                        });
                    } else {
                        /*bootbox.alert({
			  size: "small",
			  message: 'Please Enter pincode',
			});*/
                        $('#pincode_required').show();

                        //$scope.pincode='';
                        localStorage.removeItem("searched_pincode");
                        $scope.searched_pincode_msg = '';
                        $scope.pincode_has_no_service();
                    }
                    /* update right side cart */
                    if (angular.element(document.getElementById('rightSideCartController_div')).scope() !== undefined) {
                        //angular.element(document.getElementById('rightSideCartController_div')).scope().getRightSideCart();
                    }
					
					
					
                    /* update right side cart */
                }

                /* restrict deleted or inactive addons  */
                
                $scope.get_active_addons = function (order_product_count,products_order, w = ''){
                

                    //alert('inside')
                    var add_on=[];var add_main_ids=[];
                    for(r=0;r<order_product_count;r++){
                        add_on[r]=JSON.parse(JSON.stringify(products_order[r].addon_products));
                        add_main_ids.push(products_order[r].inventory_id);
                    }

                    if(add_on.length>0){
                        
                        $http({
                            method: "POST",
                            url: "<?php echo base_url();?>get_active_addons",
                            data: {'add_on': add_on,'products_order':products_order,'add_main_ids':add_main_ids},
                            dataType: "JSON",
                            async: false
                        }).success(function mySucces(result) {
                                //console.log('addon');

                                //console.log(result)
                                
                                if (jQuery.isEmptyObject(result)) {

                                    for(r=0;r<order_product_count;r++){
                                        $scope.products_order[r].addon_products=[];
                                        $scope.products_order[r].addon_products_status=0;
                                        $scope.products_order[r].addon_total_price=0;
                                        $scope.products_order[r].addon_inventories='';
                                    }
                                    
                                }else{
                                    
                                    for(r=0;r<order_product_count;r++){

                                        if($scope.products_order[r].addon_products_status==1){

                                                //add_on[r]=JSON.parse(JSON.stringify(products_order[r].addon_products));

                                                addon_tot=0;addon_inv=[];sub_addon_count=0;
                                                if( (typeof result[r] === "object") && (result[r] !== null) ){
                                                
                                                    //console.log('inside '+r+" "+result[r].length);

                                                    $scope.products_order[r].addon_products=result[r];
                                                    for(j=0;j<result[r].length;j++){
                                                        addon_tot+=parseInt(result[r][j].inv_price);
                                                        addon_inv.push(result[r][j].inv_id);
                                                        sub_addon_count++;
                                                    }
                                                    //alert(addon_tot);
                                                    if(sub_addon_count>0){
                                                        $scope.products_order[r].addon_inventories=addon_inv.join(", ");
                                                        $scope.products_order[r].addon_total_price=parseInt(addon_tot);
                                                        $scope.products_order[r].addon_products_status=1;
                                                    }
                                                }else{
                                                    //console.log('inside 2 '+r);
                                                    $scope.products_order[r].addon_products=[];
                                                    $scope.products_order[r].addon_inventories='';
                                                    $scope.products_order[r].addon_total_price=parseInt(addon_tot);
                                                    $scope.products_order[r].addon_products_status=0;
                                                }
                                            }
                                        
                                    }
                                }
                            
                        });

                     // //console.log(add_on);
                    }else{
                        /* no addon in cart  */
                    }

                  //  return false;
            }

                /* restrict deleted or inactive addons  */


                /* restrict deleted skus */
                $scope.get_active_products = function (order_product_count, products_order, w = ''){
                
                    $http({
                        method: "POST",
                        url: "<?php echo base_url();?>get_active_products",
                        data: {'products_order': products_order},
                        dataType: "JSON",
                        async: false
                    }).success(function mySucces(result) {
                            ////console.log(result)
                            if (jQuery.isEmptyObject(result)) {
                                
                                Object.keys(products_order).forEach(function(key) {
                                    $scope.removeItem(key);
                                   //with or without session delete all cart values. because no active products
                                });
                                
                            }else{
                                ////console.log("-------");
                                ////console.log(products_order);
                                $scope.active_inv=JSON.parse(JSON.stringify(result));
                                
                                    /**/
                                    Object.keys(products_order).forEach(function(key) {
                                        var value = products_order[key].inventory_id;
                                        
                                        exist=Object.values($scope.active_inv).indexOf(value);
                                        if(exist==-1){
                                            $scope.removeItem(key);
                                        }
                                        ////console.log(exist);
                                        
                                    });
                                    /**/
                                
                                ////console.log("-------");
                                ////console.log(products_order);
                            }
                            
                            $scope.get_promotion_availability_all($scope.order_product_count,$scope.products_order,8);
                            $scope.get_promotion_active_status_all($scope.order_product_count,$scope.products_order,8);
                    });
                }
                /* restrict deleted skus */
                
                
                if (c_id != '' && c_id != null) {
                    $http({
                        method: "POST",
                        url: "<?php echo base_url();?>Account/get_cartinfo_in_json_format",
                        data: "1=2",
                        dataType: "JSON",
                    }).success(function mySucces(result) {

                        $scope.products_order = result;
						//$scope.convert_inventory_number();
                        $scope.order_product_count = $scope.products_order.length;
                        
                        /*for(r=0;r<$scope.order_product_count;r++){
                            $scope.products_order[r].addon_products=JSON.parse(JSON.stringify($scope.products_order[r].addon_products));
                        }*/
                        
                        /////////////////////////////////
                        if ($scope.pincode != null && $scope.pincode != '' && $scope.order_product_count > 0) {
                            $("#check_shipping_charge_btn").html("Change");
                            $("#available_pincode").blur();
                            $("#available_pincode").css({"background-color": "#eee"});
                        } else {
                            $("#check_shipping_charge_btn").html("Check");
                            $("#available_pincode").focus();
                            $("#available_pincode").css({"background-color": "#fff"});
                        }
                        //////////////////////////////////////
                        total_netprice = 0;
                        if ($scope.order_product_count == 0) {
                            $scope.empty_cart_in_ordercontroller = 1;
                            $(".cart_summary").css({"display": "none"});
                            $(".proceed_to_checkout").css({"display": "none"});
                            $(".prev-btn").css({"display": "none"});
                        } else {
                            $scope.empty_cart_in_ordercontroller = 0;
                            $(".cart_summary").css({"display": ""});
                            $(".proceed_to_checkout").css({"display": ""});
                            $(".prev-btn").css({"display": ""});
                        }
                        
                        /*$scope.get_promotion_availability_all($scope.order_product_count,$scope.products_order,2);
                        $scope.get_promotion_active_status_all($scope.order_product_count,$scope.products_order,2);*/
                        $scope.get_active_products($scope.order_product_count,$scope.products_order,1);
                        $scope.get_active_addons($scope.order_product_count,$scope.products_order,1);

                        
                    }, function myError(response) {

                    });
                } else {
                    if (localStorage.LocalCustomerCart != null && localStorage.LocalCustomerCart != "[]") {
                        $scope.products_order = JSON.parse(localStorage.LocalCustomerCart);
                        //alert(11);
                        //console.log('|##|');
                        //console.log($scope.products_order);
                        //console.log('|##|');
                        
                        //$scope.convert_inventory_number();
                        $scope.order_product_count = $scope.products_order.length;

                       
                        /////////////////////////////////
                        if ($scope.pincode != null && $scope.pincode != '' && $scope.order_product_count > 0) {
                            $("#check_shipping_charge_btn").html("Change");
                            $("#available_pincode").blur();
                            $("#available_pincode").css({"background-color": "#eee"});
                        } else {
                            $("#check_shipping_charge_btn").html("Check");
                            $("#available_pincode").focus();
                            $("#available_pincode").css({"background-color": "#fff"});
                        }
                        //////////////////////////////////////
                        Scopes.store('OrderController', $scope);
                        total_netprice = 0;
                        if ($scope.order_product_count == 0) {
                            $scope.empty_cart_in_ordercontroller = 1;
                            $(".cart_summary").css({"display": "none"});
                            $(".proceed_to_checkout").css({"display": "none"});
                            $(".prev-btn").css({"display": "none"});
                        }

                        /*$scope.get_promotion_availability_all($scope.order_product_count,$scope.products_order,8);
                        $scope.get_promotion_active_status_all($scope.order_product_count,$scope.products_order,8);*/
                        $scope.get_active_products($scope.order_product_count,$scope.products_order,8);
                        $scope.get_active_addons($scope.order_product_count,$scope.products_order,2);

                        
                    } else {

                        $scope.empty_cart_in_ordercontroller = 1;
                        $(".cart_summary").css({"display": "none"});
                        $(".proceed_to_checkout").css({"display": "none"});
                        $(".prev-btn").css({"display": "none"});
                        $("#available_pincode_div").css({"display": "none"});

                    }
                }
                $scope.pincode_has_no_service = function () {
                    for (v = 0; v < $scope.order_product_count; v++) {

                        $scope.products_order[v].shipping_charge = -1;
                        $scope.products_order[v].shipping_charge_update = -1;
                        $scope.products_order[v].logistics_id = -1;
                        $scope.products_order[v].logistics_parcel_category_id = -1;
                        $scope.products_order[v].parcel_category_multiplier = -1;
                        $scope.products_order[v].weight_multiplier = -1;
                        $scope.products_order[v].default_delivery_mode_id = -1;
                        $scope.products_order[v].logistics_territory_id = -1;
                        $scope.products_order[v].territory_duration = -1;
                        $scope.products_order[v].msg_style = "color:red;";
						if($scope.products_order[j].pincode_valid=='not_valid'){
                               $scope.products_order[j].delivery_service_msg = 'Invalid pincode';
                                    }else{
						if($scope.pincode=='undefined'){
						$scope.products_order[v].delivery_service_msg = 'Enter Your Delivery Location Pincode';
						}else{
						$scope.products_order[v].delivery_service_msg = 'Service Not Available at '+$scope.pincode;	
						}
					}
                    }

                    var update_cart_info_post_data = $scope.products_order;
                    if (c_id != null && c_id != '') {
                        $scope.update_and_get_cartable_info_in_json_format_multiple(update_cart_info_post_data);
                    } else {
                        $scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order);
                    }
                }
				inventory_id_with_promotion_uid_before_refresh_arr={};
				inventory_id_with_promotion_uid_after_refresh_arr={};
                $scope.update_and_get_cartable_info_in_json_format_multiple = function (update_cart_info_post_data) {
					$("#loading").fadeIn();
					
					// Account/get_inventory_id_with_promotion_uid is for getting the inventory_id,cart_id,sku_id,promotion_id_selected
					$http({
                        method: "POST",
                        url: "<?php echo base_url();?>Account/get_inventory_id_with_promotion_uid",
                        data: update_cart_info_post_data,
                        dataType: "JSON",
                    }).success(function mySucces(result) {
						inventory_id_with_promotion_uid_before_refresh_arr={};
						inventory_id_with_promotion_uid_after_refresh_arr={};
						inventory_id_with_promotion_uid_before_refresh_arr=result;
						//alert("one "+JSON.stringify(inventory_id_with_promotion_uid_before_refresh_arr));
						
						
						////////////////////////////////////////
						 $http({
							method: "POST",
							url: "<?php echo base_url();?>Account/update_and_get_cartable_info_in_json_format_multiple",
							data: update_cart_info_post_data,
							dataType: "JSON",
						}).success(function mySucces(result) {
							//added coment
							
							for(q=0;q<result.length;q++){
								result[q].promotion_available=$scope.products_order[q].promotion_available;
								result[q].inventory_moq_display=$scope.products_order[q].inventory_moq;
								inventory_id_with_promotion_uid_after_refresh_arr[result[q].inventory_id]=result[q].promotion_id_selected
							}
							//alert("one "+JSON.stringify(inventory_id_with_promotion_uid_after_refresh_arr));
							// the below loop is for getting the promotion_id_selected and inventory_id after updating the promotion_id_selected
							promotion_differed_skus=[];
							promotion_differed_inventory_ids=[];
							promotion_differed_inventory_cart_ids=[];
							for(x in inventory_id_with_promotion_uid_before_refresh_arr){
								if(inventory_id_with_promotion_uid_before_refresh_arr[x]["promotion_id_selected"]!="" && inventory_id_with_promotion_uid_after_refresh_arr[x]==""){
									promotion_differed_skus.push(inventory_id_with_promotion_uid_before_refresh_arr[x]["sku_id"]);
									promotion_differed_inventory_ids.push(x);
									promotion_differed_inventory_cart_ids.push(inventory_id_with_promotion_uid_before_refresh_arr[x]["cart_id"]);
								}
							}
							//alert("one "+promotion_differed_skus)
							
							$scope.products_order = result;
							$scope.order_product_count = $scope.products_order.length;
							//$scope.convert_inventory_number();
							
							if ($scope.order_product_count == 0) {
								$("#available_pincode_div").css({"display": "none"});
							}
							$scope.get_promotion_availability_all($scope.order_product_count,$scope.products_order,6);
							//$scope.get_promotion_active_status_all($scope.order_product_count,$scope.products_order,6);
                            $scope.get_active_addons($scope.order_product_count,$scope.products_order,1);
							$scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order);
							
							// the below is for deleting the inventory_ids in cart when the adminpanel deactivate promotion or expired promotion, if the same sku is already exists in cart
							promotion_differed_inventory_cart_ids=promotion_differed_inventory_cart_ids.filter(x => x != null)
							if(promotion_differed_inventory_ids.length>0 && promotion_differed_inventory_cart_ids.length>0){
								
								
								$http({
									method: "POST",
									url: "<?php echo base_url();?>Account/delete_promotion_differed_inventory_ids",
									data: {"promotion_differed_inventory_cart_ids":promotion_differed_inventory_cart_ids.join("|")},
									dataType: "JSON",
								}).success(function mySucces(result) {
										swal({
									title:"", 
									text: "SKUs "+promotion_differed_skus.join(", ")+" promotion changed. Please add to cart again ",
									type: "info",
									allowOutsideClick: false
							}).then(function () {
								
									location.reload();
							
							});
							
							
										
										
									}, function myError(response) {
									$("#loading").fadeOut();
									alert('Error, please try again')
								});
								
								
								
								
						
							}
							
							
							
							$("#loading").fadeOut();
						}, function myError(response) {
							$("#loading").fadeOut();
							alert('Error, please try again')
						});
						//////////////////////////
					
					
					
					});
                    
                   
                }


                $scope.increment = function (x) {
                    if (parseInt($scope.products_order[x].inventory_moq) >= parseInt($scope.products_order[x].inventory_max_oq)) {

                        alert('This value is not allowed. Maximum order Quantity is ' + $scope.products_order[x].inventory_moq);

                       /* bootbox.alert({
                            size: "small",
                            message: 'This value is not allowed. Maximum order Quantity is ' + $scope.products_order[x].inventory_moq,
                        });*/
                        return;
                    }

                    if ($scope.products_order[x].inventory_moq == $scope.products_order[x].stock_available) {

                        alert('Sorry! There is no stock');

                        /*bootbox.alert({
                            size: "small",
                            message: 'Sorry! There is no stock',
                        });*/
                        return false;
                    }
                    $scope.products_order[x].inventory_moq++;
                    $scope.update_cart_by_order_quantity(x);
                };

                $scope.decrement = function (x) {
                    if (parseInt($scope.products_order[x].inventory_moq) <= parseInt($scope.products_order[x].inventory_moq_original)) {

                        alert('This value is not allowed. Minimum order Quantity is ' + $scope.products_order[x].inventory_moq);

                       /* bootbox.alert({
                            size: "small",
                            message: 'This value is not allowed. Minimum order Quantity is ' + $scope.products_order[x].inventory_moq,
                        });*/
                        return;
                    }
                    $scope.products_order[x].inventory_moq--;
                    $scope.update_cart_by_order_quantity(x);
                };

                var timeoutPromise;
                $scope.keypuptimerfun_callafter_fewseconds = function (evt, x,keyevent) {
                    return false; // this is placed because the input is now readonly changed from editable mode so we should not call below code.
                    $("#inventory_moq_span_"+x).show();
					
					/*if(keyevent=="keyup"){
						//$(".qtyinput").each(function(){
							//idx=$(this).attr("id");
							//qtyinput_index=idx.split("_")[1];
							//$("#qtyinput_"+qtyinput_index).attr("disabled",false);
						//});
						
						
						$(".qtyinput").each(function(){
							idx=$(this).attr("id");
							qtyinput_index=idx.split("_")[1];
							if(qtyinput_index==x){
								$("#qtyinput_"+qtyinput_index).attr("disabled",false);
							}
							else{
								//alert(qtyinput_index+"=="+x)
								$("#qtyinput_"+qtyinput_index).attr("disabled",true);
							}
						});
					}*/
					if(keyevent=="blur"){
						$(".qtyinput").each(function(){
							idx=$(this).attr("id");
							qtyinput_index=idx.split("_")[1];
							$("#qtyinput_"+qtyinput_index).attr("disabled",true);
						});
					}
					
                    if(isNaN($scope.products_order[x].inventory_moq)===true){
                        if(isNaN($scope.products_order[x].inventory_moq_display)===true || $scope.products_order[x].inventory_moq_display==null || $scope.products_order[x].inventory_moq_display==''){
                            return false;
                        }else{
                            $scope.products_order[x].inventory_moq=$scope.products_order[x].inventory_moq_display;
                        }
                    }
                   // setTimeout(function () {
                        $scope.keypuptimerfun(evt, x);
                   // }, 500);
                }
                $scope.keypuptimerfun = function (evt, x) {
                    $("#loading").fadeIn('fast');
                   /* //console.log($scope.products_order[x].inventory_moq_display+"|w|"+$scope.products_order[x].inventory_moq+"|w|");
                    //console.log($scope.products_order[x].stock_available+"|r|"+$scope.products_order[x].item_stock_available+"|r|");
                    */
                    if(isNaN($scope.products_order[x].inventory_moq)===true){
                        if(isNaN($scope.products_order[x].inventory_moq_display)===true || $scope.products_order[x].inventory_moq_display==null || $scope.products_order[x].inventory_moq_display==''){
                            return false;
                        }else{
                            $scope.products_order[x].inventory_moq=$scope.products_order[x].inventory_moq_display;
                        }
                    }
								
								
								
                    if ($scope.products_order[x].inventory_moq !== "" && parseInt($scope.products_order[x].inventory_moq) >= 0 && isNaN($scope.products_order[x].inventory_moq)===false ) {
                        if (parseInt($scope.products_order[x].inventory_moq) < parseInt($scope.products_order[x].promotion_minimum_quantity) && parseInt($scope.products_order[x].inventory_moq)>0) {
                            //var current_entered_quantity=$scope.products_order[x].inventory_moq;

                            swal({
                                title: "",
                                html: "The offer you have selected has Qty '" + $scope.products_order[x].promotion_minimum_quantity + "'. "+ ' Are you sure to proceed with no offer ?'
                                ,
                                type: "info",
                                showCancelButton: true,
                                showConfirmButton: true,
                                confirmButtonText: 'Yes, Sure',
                            }).then(function (res) {
                                if(res==true){
                                    //console.log('dd');
                                    //console.log($scope.products_order[x]);
                                    //console.log('dd');
                                    
                                    $scope.products_order[x].promotion_available=0;
                                    
                                     if ($scope.products_order[x].promotion_default_discount_promo != "") {
                                         
                                         /*                                        
                                        $scope.products_order[x].promotion_residual ='';
                                        $scope.products_order[x].promotion_discount ='';
                                        $scope.products_order[x].promotion_cashback='';
                                        $scope.products_order[x].promotion_surprise_gift='';
                                        $scope.products_order[x].promotion_surprise_gift_type ='';
                                        $scope.products_order[x].promotion_surprise_gift_skus='';
                                        $scope.products_order[x].promotion_surprise_gift_skus_nums='';
                                        $scope.products_order[x].promotion_item_multiplier='';
                                        $scope.products_order[x].promotion_item='';
                                        $scope.products_order[x].promotion_item_num='';
                                        $scope.products_order[x].promotion_id_selected=''; // need to save the default discount id
                                        $scope.products_order[x].promotion_type_selected='';
                                        $scope.products_order[x].promotion_minimum_quantity='';
                                        $scope.products_order[x].promotion_quote=$scope.products_order[x].promotion_default_discount_promo;
                                        
                                        $scope.products_order[x].promotion_quote_show=0;
                                        $scope.products_order[x].discount_quote_show=0;*/
                                        
                                     }else{
                                         
                                        $scope.products_order[x].promotion_available=0;
                                        $scope.products_order[x].promotion_quote_show=0;
                                        $scope.products_order[x].discount_quote_show=0;
                                        
                                     }
                                     $scope.products_order[x].new_promo_avail=1;
                                     ////console.log($scope.products_order[x].promotion_available+"||"+$scope.products_order[x].discount_quote_show+"||"+$scope.products_order[x].promotion_quote_show);
                                    $scope.update_cart_by_order_quantity(x);
                                }else{
                                    $scope.update_cart_by_order_quantity(x);
                                }
                            },
                            function () { 
                               //cancel button
                                $scope.products_order[x].promotion_quote_show=1;
                                $scope.products_order[x].discount_quote_show=0;
                                $scope.products_order[x].inventory_moq = parseInt($scope.products_order[x].promotion_minimum_quantity);
                                $scope.products_order[x].inventory_moq_display=$scope.products_order[x].inventory_moq;
                                $scope.update_cart_by_order_quantity(x);
                                
                            }
                                    );
                            

                        } else {
                            if ($scope.products_order[x].inventory_moq ==="" || isNaN($scope.products_order[x].inventory_moq)===true || parseInt($scope.products_order[x].inventory_moq)===0) {
                                $("#proceed_to_checkout_id").attr({"disabled": true});
                            } else {
                                $("#proceed_to_checkout_id").attr({"disabled": false});
                            }
                            $timeout.cancel(timeoutPromise);
                            timeoutPromise = $timeout(function () {
                                $scope.keypup(evt, x);
                            }, 500);
                        }
                    }else{
                        if ($scope.products_order[x].inventory_moq === "" || isNaN($scope.products_order[x].inventory_moq)===true || parseInt($scope.products_order[x].inventory_moq)!==0) {           
                         $("#proceed_to_checkout_id").attr({"disabled": true});
                        } else {
                            $("#proceed_to_checkout_id").attr({"disabled": false});
                        }
                        $timeout.cancel(timeoutPromise);
                        timeoutPromise = $timeout(function () {
                            $scope.keypup(evt, x);
                        }, 500);
                    }
					$("#loading").fadeOut();
                }

                $scope.keypup = function (evt, x) {
                    
                    
                    ///alert(isNaN($scope.products_order[x].inventory_moq));
                    
                    if ($scope.products_order[x].inventory_moq === "" || isNaN($scope.products_order[x].inventory_moq)===true || $scope.products_order[x].inventory_moq==null) {
                        
                        angular.element("#quantity_status_info_" + x).html('');
                        angular.element("#quantity_status_info_" + x).html('Please enter a number between ' + $scope.products_order[x].inventory_moq_original + ' (Min. Order Qty) and ' + $scope.products_order[x].inventory_max_oq + ' (Max. Order Qty)');
                        return;
                    }
                    angular.element("#quantity_status_info_" + x).html('');
                    if(parseInt($scope.products_order[x].inventory_moq)===0){
                        //angular.element("#quantity_status_info_" + x).html('Please enter a number between ' + $scope.products_order[x].inventory_moq_original + ' (Min. Order Qty) and ' + $scope.products_order[x].inventory_max_oq + ' (Max. Order Qty)');
                        $scope.products_order[x].inventory_moq = $scope.products_order[x].inventory_moq_display;
                        $scope.products_order[x].inventory_moq_display=$scope.products_order[x].inventory_moq;
                        $scope.update_cart_by_order_quantity(x);
                        return;
                    }
                    if (parseInt($scope.products_order[x].inventory_moq) < parseInt($scope.products_order[x].inventory_moq_original) || $scope.products_order[x].inventory_moq === "" || isNaN($scope.products_order[x].inventory_moq)===true) {
                        angular.element("#quantity_status_info_" + x).html('Please enter a number between ' + $scope.products_order[x].inventory_moq_original + ' (Min. Order Qty) and ' + $scope.products_order[x].inventory_max_oq + ' (Max. Order Qty)');
						setTimeout(function(){
							angular.element("#quantity_status_info_" + x).html('');
						},3000);
                        $scope.products_order[x].inventory_moq = $scope.products_order[x].inventory_moq_original;
                        $scope.products_order[x].inventory_moq_display=$scope.products_order[x].inventory_moq;
                        $scope.update_cart_by_order_quantity(x);
                        return;
                    } else if ((parseInt($scope.products_order[x].inventory_moq) <= parseInt($scope.products_order[x].inventory_max_oq) && parseInt($scope.products_order[x].inventory_moq) > parseInt($scope.products_order[x].stock_available)) && isNaN($scope.products_order[x].inventory_moq)===false) {
                        angular.element("#quantity_status_info_" + x).html('Out of Stock!');
                        $scope.products_order[x].inventory_moq = $scope.products_order[x].inventory_moq_original;
                        $scope.products_order[x].inventory_moq_display=$scope.products_order[x].inventory_moq;
                        $scope.update_cart_by_order_quantity(x);
                        return false;
                    } else if (parseInt($scope.products_order[x].inventory_moq) > parseInt($scope.products_order[x].inventory_max_oq)) {
                        angular.element("#quantity_status_info_" + x).html('Please enter a number between ' + $scope.products_order[x].inventory_moq_original + ' (Min. Order Qty) and ' + $scope.products_order[x].inventory_max_oq + ' (Max. Order Qty)');
                        setTimeout(function(){
                                angular.element("#quantity_status_info_" + x).html('');
                        },3000);
							
                        $scope.products_order[x].inventory_moq = $scope.products_order[x].inventory_max_oq;
                        $scope.products_order[x].inventory_moq_display=$scope.products_order[x].inventory_moq;
                        $scope.update_cart_by_order_quantity(x);
                        return false;
                    } else {
                        $scope.products_order[x].inventory_moq_display=$scope.products_order[x].inventory_moq;
                        $scope.update_cart_by_order_quantity(x);
                    }
					//$("#inventory_moq_span_"+x).html("");
                }
                $scope.update_moq = function (x) {
                    if ($scope.products_order[x].inventory_moq === "" || isNaN($scope.products_order[x].inventory_moq)===true || $scope.products_order[x].inventory_moq==null) {
                        
                        angular.element("#quantity_status_info_" + x).html('');
                        angular.element("#quantity_status_info_" + x).html('Please enter a number between ' + $scope.products_order[x].inventory_moq_original + ' (Min. Order Qty) and ' + $scope.products_order[x].inventory_max_oq + ' (Max. Order Qty)');
                        //return;
                    }
                    angular.element("#quantity_status_info_" + x).html('');
                    if(parseInt($scope.products_order[x].inventory_moq)===0){
                        //angular.element("#quantity_status_info_" + x).html('Please enter a number between ' + $scope.products_order[x].inventory_moq_original + ' (Min. Order Qty) and ' + $scope.products_order[x].inventory_max_oq + ' (Max. Order Qty)');
                        $scope.products_order[x].inventory_moq = $scope.products_order[x].inventory_moq_display;
                        $scope.products_order[x].inventory_moq_display=$scope.products_order[x].inventory_moq;
                        $scope.update_cart_by_order_quantity(x);
                        //return;
                    }
                    if (parseInt($scope.products_order[x].inventory_moq) < parseInt($scope.products_order[x].inventory_moq_original) || $scope.products_order[x].inventory_moq === "" || isNaN($scope.products_order[x].inventory_moq)===true) {
                        angular.element("#quantity_status_info_" + x).html('Please enter a number between ' + $scope.products_order[x].inventory_moq_original + ' (Min. Order Qty) and ' + $scope.products_order[x].inventory_max_oq + ' (Max. Order Qty)');
						setTimeout(function(){
							angular.element("#quantity_status_info_" + x).html('');
						},3000);
                        $scope.products_order[x].inventory_moq = $scope.products_order[x].inventory_moq_original;
                        $scope.products_order[x].inventory_moq_display=$scope.products_order[x].inventory_moq;
                        $scope.update_cart_by_order_quantity(x);
                        //return;
                    } else if ((parseInt($scope.products_order[x].inventory_moq) <= parseInt($scope.products_order[x].inventory_max_oq) && parseInt($scope.products_order[x].inventory_moq) > parseInt($scope.products_order[x].stock_available)) && isNaN($scope.products_order[x].inventory_moq)===false) {
                        angular.element("#quantity_status_info_" + x).html('Out of Stock!');
                        $scope.products_order[x].inventory_moq = $scope.products_order[x].inventory_moq_original;
                        $scope.products_order[x].inventory_moq_display=$scope.products_order[x].inventory_moq;
                        $scope.update_cart_by_order_quantity(x);
                        //return false;
                    } else if (parseInt($scope.products_order[x].inventory_moq) > parseInt($scope.products_order[x].inventory_max_oq)) {
                        angular.element("#quantity_status_info_" + x).html('Please enter a number between ' + $scope.products_order[x].inventory_moq_original + ' (Min. Order Qty) and ' + $scope.products_order[x].inventory_max_oq + ' (Max. Order Qty)');
                        setTimeout(function(){
                                angular.element("#quantity_status_info_" + x).html('');
                        },3000);
							
                        $scope.products_order[x].inventory_moq = $scope.products_order[x].inventory_max_oq;
                        $scope.products_order[x].inventory_moq_display=$scope.products_order[x].inventory_moq;
                        $scope.update_cart_by_order_quantity(x);
                        //return false;
                    } else {
                        $scope.products_order[x].inventory_moq_display=$scope.products_order[x].inventory_moq;
                        $scope.update_cart_by_order_quantity(x);
                    }
		
                }

                $scope.update_cart_by_order_quantity = function (x) {
                    
                    $scope.products_order[x].inventory_moq_display=$scope.products_order[x].inventory_moq;
                    if(isNaN($scope.products_order[x].inventory_moq)){
                        $scope.update_moq(x);
                       return false; 
                    }else{
                        $scope.products_order[x].inventory_moq_display=$scope.products_order[x].inventory_moq;
                    }
                    
                    if ($scope.products_order[x].inventory_moq === "" || isNaN($scope.products_order[x].inventory_moq)===true) {
                        $("#proceed_to_checkout_id").attr({"disabled": true});
                    } else {
                        $("#proceed_to_checkout_id").attr({"disabled": false});
                    }
                    //alert($scope.products_order[x].promotion_minimum_quantity);
                    $scope.products_order[x].cash_back_amount = "";

                    $scope.products_order[x].inventory_weight_in_kg = parseInt($scope.products_order[x].inventory_moq) * parseFloat($scope.products_order[x].inventory_weight_in_kg_for_unit);

                    $scope.products_order[x].inventory_volume_in_kg = parseInt($scope.products_order[x].inventory_moq) * parseFloat($scope.products_order[x].inventory_volume_in_kg_for_unit);

                    //////////////////////added/////////////////////

                    if ($scope.products_order[x].promotion_available == 1) {

                        if ($scope.products_order[x].default_discount != "" && $scope.products_order[x].default_discount>0) {

//console.log($scope.products_order[x].default_discount+"gggggggg"+$scope.products_order[x].price_of_quotient_1 +"++++"+$scope.products_order[x].max_selling_price + "|%%%|"+$scope.products_order[x].promotion_discount);


                            modulo = $scope.products_order[x].inventory_moq % $scope.products_order[x].promotion_minimum_quantity;

                            $scope.products_order[x].modulo = modulo;


                            if (modulo != 0 && ($scope.products_order[x].inventory_moq >= parseInt($scope.products_order[x].promotion_minimum_quantity))) {
  

                                $scope.products_order[x].price_of_modulo_1 = Math.round($scope.products_order[x].max_selling_price - Math.round(($scope.products_order[x].max_selling_price * $scope.products_order[x].default_discount) / 100));

                                $scope.products_order[x].price_after_default_discount = Math.round(modulo * $scope.products_order[x].price_of_modulo_1);
                                quotient = $scope.products_order[x].inventory_moq / $scope.products_order[x].promotion_minimum_quantity;
                                quotient = Math.floor(quotient);


                                if (quotient != 0) {
                                    $scope.products_order[x].quotient = $scope.products_order[x].promotion_minimum_quantity * quotient;

                                    $scope.products_order[x].price_of_quotient_1 = Math.round($scope.products_order[x].max_selling_price - Math.round(($scope.products_order[x].max_selling_price * $scope.products_order[x].promotion_discount) / 100));
                                    $scope.products_order[x].price_after_promotion_discount = Math.round($scope.products_order[x].promotion_minimum_quantity * quotient * $scope.products_order[x].price_of_quotient_1);

                                }

                                if ($scope.products_order[x].price_after_promotion_discount != null) {
                                    $scope.products_order[x].final_product_individual = parseInt($scope.products_order[x].price_after_promotion_discount) + parseInt($scope.products_order[x].price_after_default_discount);
                                } else {

                                    $scope.products_order[x].final_product_individual = $scope.products_order[x].price_after_default_discount;
                                }

                            } else {


                                if ($scope.products_order[x].inventory_moq >= parseInt($scope.products_order[x].promotion_minimum_quantity)) {

                                    /*$scope.products_order[x].price_of_quotient_1=$scope.products_order[x].selling_price-(($scope.products_order[x].selling_price*$scope.products_order[x].default_discount)/100);
			$scope.products_order[x].price_after_default_discount=$scope.products_order[x].quantity_with_promotion*$scope.products_order[x].price_of_modulo_1;
            alert($scope.products_order[x].default_discount)
            alert($scope.products_order[x].price_after_default_discount)
            return false;*/
                                    quotient = $scope.products_order[x].inventory_moq / $scope.products_order[x].promotion_minimum_quantity;
                                    quotient = Math.floor(quotient);
                                    $scope.products_order[x].quotient = $scope.products_order[x].promotion_minimum_quantity * quotient;
                                    $scope.products_order[x].price_of_quotient_1 = Math.round($scope.products_order[x].max_selling_price - Math.round(($scope.products_order[x].max_selling_price * $scope.products_order[x].promotion_discount) / 100));
                                    $scope.products_order[x].price_after_promotion_discount = Math.round($scope.products_order[x].promotion_minimum_quantity * quotient * $scope.products_order[x].price_of_quotient_1);

                                    $scope.products_order[x].final_product_individual = parseInt($scope.products_order[x].price_after_promotion_discount);

                                    $scope.products_order[x].price_after_default_discount = 0;

//console.log($scope.products_order[x].default_discount+"nnnnnn"+$scope.products_order[x].price_of_quotient_1 +"++++"+$scope.products_order[x].max_selling_price + "|%%%|"+$scope.products_order[x].promotion_discount);

                                } else {

                                    quotient = $scope.products_order[x].inventory_moq / $scope.products_order[x].promotion_minimum_quantity;
                                    quotient = Math.floor(quotient);
                                    $scope.products_order[x].quotient = $scope.products_order[x].promotion_minimum_quantity * quotient;

                                    $scope.products_order[x].price_of_modulo_1 = Math.round($scope.products_order[x].max_selling_price - Math.round(($scope.products_order[x].max_selling_price * $scope.products_order[x].default_discount) / 100));

                                    $scope.products_order[x].price_after_default_discount = modulo * $scope.products_order[x].price_of_modulo_1;

                                    $scope.products_order[x].final_product_individual = parseInt($scope.products_order[x].price_after_default_discount);

                                }

                            }

                        }//discount promotion !=0

//console.log($scope.products_order[x].default_discount+"mmmmm"+$scope.products_order[x].price_of_quotient_1 +"++++"+$scope.products_order[x].max_selling_price + "|%%%|"+$scope.products_order[x].promotion_discount);

//console.log($scope.products_order[x].promotion_discount+" Promo discount");

                       if(1){
                        
                            if ($scope.products_order[x].default_discount === "" || parseInt($scope.products_order[x].default_discount)==0) {
                               //console.log('geting inside');//this denotes straight percent not applied
                            modulo = $scope.products_order[x].inventory_moq % $scope.products_order[x].promotion_minimum_quantity;
                            $scope.products_order[x].modulo = modulo;
                            if (modulo != 0) {


                                $scope.products_order[x].price_of_modulo_1 = $scope.products_order[x].selling_price;

                                $scope.products_order[x].price_after_default_discount = modulo * $scope.products_order[x].price_of_modulo_1;
                                quotient = $scope.products_order[x].inventory_moq / $scope.products_order[x].promotion_minimum_quantity;
                                quotient = Math.floor(quotient);
                                if (quotient != 0) {
                                    
                                    $scope.products_order[x].quotient = $scope.products_order[x].promotion_minimum_quantity * quotient;
                                    if(parseInt($scope.products_order[x].promotion_discount)>0){
                                        $scope.products_order[x].price_of_quotient_1 = Math.round($scope.products_order[x].max_selling_price - Math.round(($scope.products_order[x].max_selling_price * $scope.products_order[x].promotion_discount) / 100));

                                         ////console.log($scope.products_order[x].max_selling_price + "|%%%|"+$scope.products_order[x].promotion_discount);
                                        $scope.products_order[x].price_after_promotion_discount = Math.round($scope.products_order[x].promotion_minimum_quantity * quotient * $scope.products_order[x].price_of_quotient_1);
                                    }else{
                                        $scope.products_order[x].price_of_quotient_1 = Math.round($scope.products_order[x].selling_price);
                                        $scope.products_order[x].price_after_promotion_discount = Math.round($scope.products_order[x].promotion_minimum_quantity * quotient * $scope.products_order[x].price_of_quotient_1);
                                    }
                                    
                                }


                                if ($scope.products_order[x].price_after_promotion_discount != null) {
                                    $scope.products_order[x].final_product_individual = parseInt($scope.products_order[x].price_after_promotion_discount) + parseInt($scope.products_order[x].price_after_default_discount);
                                } else {

                                    $scope.products_order[x].final_product_individual = parseInt($scope.products_order[x].price_after_default_discount);
                                }

                            } else {

                                quotient = $scope.products_order[x].inventory_moq / $scope.products_order[x].promotion_minimum_quantity;
                                quotient = Math.floor(quotient);
                                $scope.products_order[x].quotient = $scope.products_order[x].promotion_minimum_quantity * quotient;
                                
                                if(parseInt($scope.products_order[x].promotion_discount)>0){
                                    $scope.products_order[x].price_of_quotient_1 = Math.round($scope.products_order[x].max_selling_price - Math.round(($scope.products_order[x].max_selling_price * $scope.products_order[x].promotion_discount) / 100));
                                    $scope.products_order[x].price_after_promotion_discount = Math.round($scope.products_order[x].promotion_minimum_quantity * quotient * $scope.products_order[x].price_of_quotient_1);
                                }else{
                                    $scope.products_order[x].price_of_quotient_1 = Math.round($scope.products_order[x].selling_price);
                                    $scope.products_order[x].price_after_promotion_discount = Math.round($scope.products_order[x].promotion_minimum_quantity * quotient * $scope.products_order[x].price_of_quotient_1);
                                }

                                $scope.products_order[x].final_product_individual = $scope.products_order[x].price_after_promotion_discount;
                                $scope.products_order[x].price_after_default_discount = 0;

                            }
                        }

                        }

                        if ($scope.products_order[x].promotion_cashback > 0) {
                            cash_back_amount = parseFloat($scope.products_order[x].final_product_individual * ($scope.products_order[x].promotion_cashback / 100)).toFixed(2);
                            $scope.products_order[x].cash_back_value = parseFloat(cash_back_amount)
                        } else {

                            cash_back_amount = "0";
                            $scope.products_order[x].cash_back_value = cash_back_amount;
                        }

                    }//promo avai 1
                    else {
                        $scope.products_order[x].individual_price_of_product_without_promotion = '0';
                        $scope.products_order[x].total_price_of_product_without_promotion = '0';
                        $scope.products_order[x].total_price_of_product_with_promotion = '0';
                        $scope.products_order[x].quantity_without_promotion = '0';
                        $scope.products_order[x].quantity_with_promotion = '0';
                        cash_back_amount = "0";
                        $scope.products_order[x].cash_back_value = cash_back_amount;

                    }

                    
                    if ($scope.products_order[x].price_of_modulo_1) {
                        $scope.products_order[x].individual_price_of_product_without_promotion = $scope.products_order[x].price_of_modulo_1;
                    } else {
                        $scope.products_order[x].price_of_modulo_1 = '';
                        $scope.products_order[x].individual_price_of_product_without_promotion = $scope.products_order[x].price_of_modulo_1;
                    }

                    if ($scope.products_order[x].price_of_quotient_1) {
                        $scope.products_order[x].individual_price_of_product_with_promotion = $scope.products_order[x].price_of_quotient_1;
                    } else {
                        $scope.products_order[x].price_of_quotient_1 = '';
                        $scope.products_order[x].individual_price_of_product_with_promotion = $scope.products_order[x].price_of_quotient_1;
                    }


                    if ($scope.products_order[x].price_after_default_discount) {
                        $scope.products_order[x].total_price_of_product_without_promotion = $scope.products_order[x].price_after_default_discount;
                    } else {

                        $scope.products_order[x].price_after_default_discount = 0;
                        $scope.products_order[x].total_price_of_product_without_promotion = $scope.products_order[x].price_after_default_discount;
                    }

                    if ($scope.products_order[x].price_after_promotion_discount) {

                        $scope.products_order[x].total_price_of_product_with_promotion = $scope.products_order[x].price_after_promotion_discount;
                    } else {

                        $scope.products_order[x].price_after_promotion_discount = '';
                        $scope.products_order[x].total_price_of_product_with_promotion = $scope.products_order[x].price_after_promotion_discount;
                    }

                    if ($scope.products_order[x].modulo) {
                        $scope.products_order[x].quantity_without_promotion = $scope.products_order[x].modulo;
                    } else {
                        $scope.products_order[x].modulo = '';
                        $scope.products_order[x].quantity_without_promotion = $scope.products_order[x].modulo;
                    }

                    if ($scope.products_order[x].quotient) {
                        $scope.products_order[x].quantity_with_promotion = $scope.products_order[x].quotient;
                    } else {
                        $scope.products_order[x].quotient = '';
                        $scope.products_order[x].quantity_with_promotion = $scope.products_order[x].quotient;
                    }


                    //////////////////////added/////////////////////


                    if (c_id != '' && c_id != null) {


                        var update_cart_info_post_data = {
                            'cart_id': $scope.products_order[x].cart_id,
                            'inventory_moq': $scope.products_order[x].inventory_moq,
                            'inventory_weight_in_kg': $scope.products_order[x].inventory_weight_in_kg,
                            'logistics_id': $scope.products_order[x].logistics_id,
                            'inventory_volume_in_kg': $scope.products_order[x].inventory_volume_in_kg,
                            'logistics_territory_id': $scope.products_order[x].logistics_territory_id,
                            'default_delivery_mode_id': $scope.products_order[x].default_delivery_mode_id,
                            'parcel_category_multiplier': $scope.products_order[x].parcel_category_multiplier,
                            'logistics_parcel_category_id': $scope.products_order[x].logistics_parcel_category_id,
                            'territory_duration': $scope.products_order[x].territory_duration,
                            'inventory_weight_in_kg_org': $scope.products_order[x].inventory_weight_in_kg_org,
                            'inventory_weight_in_kg_for_unit': $scope.products_order[x].inventory_weight_in_kg_for_unit,
                            'shipping_charge': $scope.products_order[x].shipping_charge,
                            'individual_price_of_product_with_promotion': $scope.products_order[x].individual_price_of_product_with_promotion,
                            'individual_price_of_product_without_promotion': $scope.products_order[x].individual_price_of_product_without_promotion,
                            'total_price_of_product_without_promotion': $scope.products_order[x].total_price_of_product_without_promotion,
                            'total_price_of_product_with_promotion': $scope.products_order[x].total_price_of_product_with_promotion,
                            'quantity_without_promotion': $scope.products_order[x].quantity_without_promotion,
                            'quantity_with_promotion': $scope.products_order[x].quantity_with_promotion,
                            'promotion_available': $scope.products_order[x].promotion_available,
                            'cash_back_value': $scope.products_order[x].cash_back_value,
                            'logistics_parcel_category_id': $scope.products_order[x].logistics_parcel_category_id,
                            'inventory_id': $scope.products_order[x].inventory_id,
                            "addon_products_status":$scope.products_order[x].addon_products_status,
                            "addon_products":$scope.products_order[x].selected_products,
                            "addon_inventories":$scope.products_order[x].addon_inventories,
                            "addon_total_price":$scope.products_order[x].addon_total_price,
                            "addon_total_shipping_price":$scope.products_order[x].addon_total_shipping_price,
                        };
                        $("#loading").fadeIn();
						
						
						inventory_id_with_promotion_uid_before_refresh_arr={};
						inventory_id_with_promotion_uid_after_refresh_arr={};
				
				
				// Account/get_inventory_id_with_promotion_uid is for getting the inventory_id,cart_id,sku_id,promotion_id_selected
				$http({
                        method: "POST",
                        url: "<?php echo base_url();?>Account/get_inventory_id_with_promotion_uid",
                        data: update_cart_info_post_data,
                        dataType: "JSON",
                    }).success(function mySucces(result) {
						inventory_id_with_promotion_uid_before_refresh_arr={};
						inventory_id_with_promotion_uid_after_refresh_arr={};
						inventory_id_with_promotion_uid_before_refresh_arr=result;
						//alert(JSON.stringify(inventory_id_with_promotion_uid_before_refresh_arr));
						
						///////////////////////////////////////////////////////
						
                        $http({
                            method: "POST",
                            url: "<?php echo base_url();?>Account/update_and_get_cartable_info_in_json_format",
                            data: update_cart_info_post_data,
                            dataType: "JSON",
                        }).success(function mySucces(result) {
                            
                            for(q=0;q<result.length;q++){
                                result[q].promotion_available=$scope.products_order[q].promotion_available;
                                result[q].inventory_moq_display=$scope.products_order[q].inventory_moq;
								
								inventory_id_with_promotion_uid_after_refresh_arr[result[q].inventory_id]=result[q].promotion_id_selected
								
								
                            }
							
							
							//alert(JSON.stringify(inventory_id_with_promotion_uid_after_refresh_arr));
							// the below loop is for getting the promotion_id_selected and inventory_id after updating the promotion_id_selected
							promotion_differed_skus=[];
							promotion_differed_inventory_ids=[];
							promotion_differed_inventory_cart_ids=[];
							for(x in inventory_id_with_promotion_uid_before_refresh_arr){
								if(inventory_id_with_promotion_uid_before_refresh_arr[x]["promotion_id_selected"]!="" && inventory_id_with_promotion_uid_after_refresh_arr[x]==""){
									promotion_differed_skus.push(inventory_id_with_promotion_uid_before_refresh_arr[x]["sku_id"]);
									promotion_differed_inventory_ids.push(x);
									promotion_differed_inventory_cart_ids.push(inventory_id_with_promotion_uid_before_refresh_arr[x]["cart_id"]);
								}
							}
							//alert(promotion_differed_skus)
							
							
							
                            $scope.products_order = result;
                            $scope.order_product_count = $scope.products_order.length;
                           
                            //$scope.convert_inventory_number();
                            $scope.get_promotion_availability_all($scope.order_product_count,$scope.products_order,7);
                            $scope.get_active_addons($scope.order_product_count,$scope.products_order,1);
                            $scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order,x);
                            $("#loading").fadeOut("slow");
                            $("#inventory_moq_span_"+x).hide()
							
							$(".qtyinput").each(function(){
								idx=$(this).attr("id");
								qtyinput_index=idx.split("_")[1];
								$("#qtyinput_"+qtyinput_index).attr("disabled",false);
							});
							
							
							
							
							// the below is for deleting the inventory_ids in cart when the adminpanel deactivate promotion or expired promotion, if the same sku is already exists in cart
							promotion_differed_inventory_cart_ids=promotion_differed_inventory_cart_ids.filter(x => x != null)
							if(promotion_differed_inventory_ids.length>0 && promotion_differed_inventory_cart_ids.length>0){
								
								
								$http({
									method: "POST",
									url: "<?php echo base_url();?>Account/delete_promotion_differed_inventory_ids",
									data: {"promotion_differed_inventory_cart_ids":promotion_differed_inventory_cart_ids.join("|")},
									dataType: "JSON",
								}).success(function mySucces(result) {
										swal({
									title:"", 
									text: "SKU(s) "+promotion_differed_skus.join(", ")+" promotion changed. Please add to cart again ",
									type: "info",
									allowOutsideClick: false
							}).then(function () {
								
									location.reload();
							
							});
							
							
										
										
									}, function myError(response) {
									$("#loading").fadeOut();
									alert('Error, please try again')
								});
								
								
								
								
						
							}
							
							
							
							
							
                        }, function myError(response) {
                               alert('Error, Please try again');
                               location.reload();
                        });
						//////////////////////////////////////////////////////
							
						}, function myError(response) {
							   alert('Error, Please try again');
							   location.reload();
						});
						
						

                    } else {

                        var update_cart_info_post_data = {
                            'cart_id': $scope.products_order[x].cart_id,
                            'inventory_moq': $scope.products_order[x].inventory_moq,
                            'inventory_weight_in_kg': $scope.products_order[x].inventory_weight_in_kg,
                            'logistics_id': $scope.products_order[x].logistics_id,
                            'inventory_volume_in_kg': $scope.products_order[x].inventory_volume_in_kg,
                            'logistics_territory_id': $scope.products_order[x].logistics_territory_id,
                            'default_delivery_mode_id': $scope.products_order[x].default_delivery_mode_id,
                            'parcel_category_multiplier': $scope.products_order[x].parcel_category_multiplier,
                            'shipping_charge': $scope.products_order[x].shipping_charge,
                            'individual_price_of_product_with_promotion': $scope.products_order[x].individual_price_of_product_with_promotion,
                            'individual_price_of_product_without_promotion': $scope.products_order[x].individual_price_of_product_without_promotion,
                            'total_price_of_product_without_promotion': $scope.products_order[x].total_price_of_product_without_promotion,
                            'total_price_of_product_with_promotion': $scope.products_order[x].total_price_of_product_with_promotion,
                            'quantity_without_promotion': $scope.products_order[x].quantity_without_promotion,
                            'quantity_with_promotion': $scope.products_order[x].quantity_with_promotion,
                            'promotion_available': $scope.products_order[x].promotion_available,
                            'cash_back_value': $scope.products_order[x].cash_back_value,
                            'logistics_parcel_category_id': $scope.products_order[x].logistics_parcel_category_id,
                            'inventory_id': $scope.products_order[x].inventory_id,
                            "addon_products_status":$scope.products_order[x].addon_products_status,
                            "addon_products":$scope.products_order[x].selected_products,
                            "addon_inventories":$scope.products_order[x].addon_inventories,
                            "addon_total_price":$scope.products_order[x].addon_total_price,
                            "addon_total_shipping_price":$scope.products_order[x].addon_total_shipping_price
                            
                        };

//alert(JSON.stringify(update_cart_info_post_data));

                        $http({
                            method: "POST",
                            url: "<?php echo base_url();?>Account/get_shipping_charge_without_session",
                            data: update_cart_info_post_data,
                            dataType: "JSON",
                        }).success(function mySucces(result) {

                            //alert(JSON.stringify(result));
//alert(result.shipping_charge);
                            $scope.products_order[x].weight_multiplier = result.weight_multiplier;
                            $scope.products_order[x].shipping_charge = result.shipping_charge;
                            $scope.order_product_count = $scope.products_order.length;

                            $scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order, x);
                            $("#inventory_moq_span_"+x).hide()
							$(".qtyinput").each(function(){
								idx=$(this).attr("id");
								qtyinput_index=idx.split("_")[1];
								$("#qtyinput_"+qtyinput_index).attr("disabled",false);
							});
                        }, function myError(response) {

                        });
                        localStorage.setItem("LocalCustomerCart", JSON.stringify($scope.products_order));
                        $("#inventory_moq_span_"+x).hide()
						$(".qtyinput").each(function(){
								idx=$(this).attr("id");
								qtyinput_index=idx.split("_")[1];
								$("#qtyinput_"+qtyinput_index).attr("disabled",false);
							});
                    }
                    /* update right side cart */
                    if (angular.element(document.getElementById('rightSideCartController_div')).scope() !== undefined) {
                       // angular.element(document.getElementById('rightSideCartController_div')).scope().getRightSideCart();
                    }
                    /* update right side cart */
                }

                $scope.removeItem = function (x) {
                    if (document.getElementById('extra_data_' + x) != null) {
                        document.getElementById('extra_data_' + x).innerHTML = "";
                    }
                    if (document.getElementById('addon_products_' + x) != null) {
                        document.getElementById('addon_products_' + x).innerHTML = "";
                    }
                    $("#loading").fadeIn();
                    if (c_id != '' && c_id != null) {
                        total_netprice = 0;
                        var remove_cart_info_post_data = {'cart_id': $scope.products_order[x].cart_id};
                        $http({
                            method: "POST",
                            url: "<?php echo base_url();?>Account/remove_cart_info_for_session_customer",
                            data: remove_cart_info_post_data,
                            dataType: "JSON",
                        }).success(function mySucces(result) {
                            $scope.products_order.splice(x, 1);
                            $scope.order_product_count = $scope.products_order.length;
                            $scope.total_inventory_quantity_in_cart = 0;
                            for (j = 0; j < $scope.order_product_count; j++) {
                                $scope.total_inventory_quantity_in_cart += parseInt($scope.products_order[j].inventory_moq);
                            }
                            total_netprice = 0;
                            if ($scope.order_product_count == 0) {
                                $scope.empty_cart_in_ordercontroller = 1;
                                $(".cart_summary").css({"display": "none"});
                                $(".proceed_to_checkout").css({"display": "none"});
                                $(".prev-btn").css({"display": "none"});


                                $("#available_pincode_div").hide();
                            } else {
                                $scope.empty_cart_in_ordercontroller = 0;
                                $(".cart_summary").css({"display": ""});
                                $(".proceed_to_checkout").css({"display": ""});
                                $(".prev-btn").css({"display": ""});

                                $("#available_pincode_div").hide();
                            }

                            $scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order);
                            $cartElement = document.getElementsByClassName("cart_count");
                            for (i = 0; i < $cartElement.length; i++) {
                                $cartElement[i].innerHTML = $scope.order_product_count;
                            }
                            $("#loading").fadeOut();
                        }, function myError(response) {

                        });

                    } else {

                        $scope.errortext = "";
                        $scope.products_order.splice(x, 1);
                        localStorage.setItem("LocalCustomerCart", JSON.stringify($scope.products_order));
                        $scope.order_product_count = $scope.products_order.length;
                        $scope.total_inventory_quantity_in_cart = 0;
                        for (j = 0; j < $scope.order_product_count; j++) {
                            $scope.total_inventory_quantity_in_cart += parseInt($scope.products_order[j].inventory_moq);
                        }
                        //Scopes.get('CartController').cart_count=$scope.order_product_count;
                        $(".cart_count").html($scope.order_product_count);
                        total_netprice = 0;
                        if ($scope.order_product_count == 0) {
                            $scope.empty_cart_in_ordercontroller = 1;
                            $(".cart_summary").css({"display": "none"});
                            $(".proceed_to_checkout").css({"display": "none"});
                            $(".prev-btn").css({"display": "none"});


                            $("#available_pincode_div").hide();

                        }
                        $scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order);
                        $("#loading").fadeOut();
                    }
                    /* update right side cart */
                    if (angular.element(document.getElementById('rightSideCartController_div')).scope() !== undefined) {
                       // angular.element(document.getElementById('rightSideCartController_div')).scope().getRightSideCart();
                    }
                    /* update right side cart */
                }

//////////////////// changed for pincode search within session starts //////
                if (c_id != '' && c_id != null) {
                    $http({
                        method: "POST",
                        url: "<?php echo base_url();?>Account/get_cartinfo_in_json_format",
                        data: "1=2",
                        dataType: "JSON",
                    }).success(function mySucces(result) {
                        //alert(result.length);
                        //$scope.products_order=result;

                        //$scope.order_product_count=$scope.products_order.length;
                        //////////////////
                        if ($scope.searched_pincode != null && $scope.searched_pincode != '') {
                            $scope.pincode = $scope.searched_pincode;
                            if (result.length > 0) {
                                $("#available_pincode_div").show();
                            } else {
                                $("#available_pincode_div").hide();
                            }

                        } else {
                            if (result.length > 0) {
                                $("#available_pincode_div").show();
                            } else {
                                $("#available_pincode_div").hide();
                            }


                        }
                        //////////////////////////
                    })

                } else {
                    if (localStorage.LocalCustomerCart != null) {
                        $scope.products_order = JSON.parse(localStorage.LocalCustomerCart);
                        //$scope.convert_inventory_number();
                        ////console.log($scope.products_order);
                        
                        $scope.order_product_count = $scope.products_order.length;
                        //////////////////
                        if ($scope.searched_pincode != null && $scope.searched_pincode != '') {
                            $scope.pincode = $scope.searched_pincode;
                            if ($scope.order_product_count > 0) {
                                $("#available_pincode_div").show();
                            } else {
                                $("#available_pincode_div").hide();
                            }
                        } else {
                            if ($scope.order_product_count > 0) {
                                $("#available_pincode_div").show();
                            } else {
                                $("#available_pincode_div").hide();
                            }

                        }
                        //////////////////////////
                    }
                }
//////////////////// changed for pincode search within session ends //////
                $scope.continueShoppingFun = function (url) {
                    location.href = url;
                }
                $scope.buttonClicked = false;
                $scope.proceedCheckout = function () {
                    
                    /* added new */
                    var isValid = true;
                    $('.input-number').each(function() {
                      if ( $(this).val() === '' || $(this).val()== 0 || $(this).val() =='0')
                          isValid = false;
                    });
                    if(isValid==false){
                       
                        swal({
                            title: '',
                            html:"Please fill valid inputs for quantity ",
                            type: 'info',
                            showConfirmButton: true
                        });
                        $scope.buttonClicked = true;
                        return false;
                    }
                    /* added new */
  
                    $("#proceed_to_checkout_id").html("Proceed to Checkout <i class='fa fa-refresh fa-spin'></i>");
                    document.getElementById('proceed_to_checkout_id').style.pointerEvents = 'none';
                    $scope.buttonClicked = true;
                    if (c_id != '' && c_id != null) {
                        //document.write(JSON.stringify($scope.products_order));return false;
                        $("#proceed_to_checkout_id").html("Proceed to Checkout");
                        $scope.buttonClicked = false;
                        location.href = "<?php echo base_url()?>checkout";
                    } else {
                        $("#proceed_to_checkout_id").html("Proceed to Checkout");
                        $scope.buttonClicked = false;
                        <?php
                        $this->session->set_userdata("cur_page", base_url() . "checkout");
                        ?>
                        //document.write(JSON.stringify($scope.products_order));return false;
                        location.href = "<?php echo base_url()?>login";

                    }
                }
				$scope.convert_inventory_number=function(){
					if($scope.products_order.length>0){
						for(i=0;i<$scope.products_order.length;i++){
							$scope.products_order[i].inventory_moq=parseInt($scope.products_order[i].inventory_moq);
							$scope.products_order[i].inventory_moq_original=parseInt($scope.products_order[i].inventory_moq_original);
							$scope.products_order[i].inventory_max_oq=parseInt($scope.products_order[i].inventory_max_oq);
						}
					}
				}

            }

            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                //if(charCode > 31 && (charCode < 48 || charCode > 57 || charCode!=8 || charCode!=46)) {
                if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
                    return false;
                }
                return true;
            }
        </script>
        <?php

        //print_r($promotions_cart);

        ($promotions_cart); ?>
        <?php if (!empty($promotions_cart)) { ?>
            <ul class="nav nav-stacked hide" id="cart_promotion">
                <?php foreach ($promotions_cart as $data) {
                    $offer = htmlspecialchars_decode($data['promo_quote']);
                    $offer = preg_replace("/\([^)]+\)/", "", $offer);
                    ?>

                    <li id="<?php echo($data['promo_uid']); ?>"><?php echo $offer; ?></li>
                <?php } ?>
            </ul>
        <?php } ?>

        <div class="page-content page-order well pl-0 pr-0 pt-10" ng-controller="OrderController">

            <?php if (!empty($promotions_cart)) { ?>
                <div class="row" style="margin:-10px 0px 10px 0px;">
                    <div class="col-sm-12">
                        <div class="row"
                             ng-if="discount_oninvoice || cash_back_oninvoice || surprise_gift || free_shipping || invoice_surprise_gift">
                            <div class="col-sm-3 pl-0 pr-0" ng-bind-html="explicitlyTrustedHtml_availed_offers_heading"
                                 ng-if="availed_offers_heading_flag=='yes' || availed_offers_heading_flag=='no'">
                                <!--
										<h2 style="color:#004b9b"><i class="fa fa-heart" style="color:#004b9b"></i> Congrats!</h2>
									-->
                            </div>
                            <div class="col-sm-9">
                                <h4 style="line-height: 2;">
                                    <div ng-bind-html="explicitlyTrustedHtml_discount" ng-if="discount_oninvoice"></div>
                                </h4>
                                <h4 style="line-height: 2;">
                                    <div ng-bind-html="explicitlyTrustedHtml_cash_back"
                                         ng-if="cash_back_oninvoice"></div>
                                </h4>
                                <h4 style="line-height: 2;">
                                    <div ng-bind-html="explicitlyTrustedHtml_surprise_gift" ng-if="surprise_gift"></div>
                                </h4>
                                <h4 style="line-height: 2;">
                                    <div ng-bind-html="explicitlyTrustedHtml_free_shipping" ng-if="free_shipping"></div>
                                </h4>
                                <h4 style="line-height: 2;">
                                    <div ng-bind-html="explicitlyTrustedHtml_invoice_surprise_gift"
                                         ng-if="invoice_surprise_gift"></div>
                                </h4>

                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <div class="popular-tabs mt-0">
                <div class="row pl-10 pr-10">
                    <div class="col-md-8 bold my-cart" ng-if="order_product_count>0">My Cart: {{order_product_count}}
                        Items totalling {{total_inventory_quantity_in_cart}} Qty
                        <span ng-if="final_count_free_items>0"> + {{final_count_free_items}} Qty Free</span>
                    </div>
                    <div class="col-md-8 bold my-cart" ng-if="order_product_count==null">My Cart: 0 Items totalling
                        {{total_inventory_quantity_in_cart}} Qty
                        <span ng-if="final_count_free_items>0"> + {{final_count_free_items}} Qty Free</span>
                    </div>
                    <div class="col-md-8 bold my-cart" ng-if="order_product_count==0">My Cart: {{order_product_count}}
                        Items totalling {{total_inventory_quantity_in_cart}} Qty
                        <span ng-if="final_count_free_items>0"> + {{final_count_free_items}} Qty Free</span>
                    </div>
                    <div class="product-desc col-md-4" id="available_pincode_div" style="display:none;">
                        <form name="userForm" novalidate>


                            <div class="input-group input-group-sm pincode-search pull-right">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                                <input id="available_pincode" ng-model="pincode"
                                       name="available_pincode" class="form-control text-center min_width"
                                       placeholder="Enter Pincode" readOnly type="text"
                                        maxlength="6" style="border-color: #ccc !important;box-shadow: none !important;">
<!-- 
                                  ng-minlength="6" ng-maxlength="6" ng-click="remove_error();set_shipping_charge_change_button_text();" onkeypress="return isNumber(event)"  ng-paste="$event.preventDefault()" ng-pattern="/^[0-9]*$/" ng-keypress="hide_error()" -->
                                <span style="display:none;" class="input-group-addon cursor-pointer preventDflt" id="check_shipping_charge_btn"
                                      ng-click="get_corresponding_shipping_charge('click');change_shipping_charge_change_button_text()">Check</span>

                            </div>


                            <span class="help-block mt-0" id="pincode_required" style="display:none;">Valid Pincode is required</span>
                            <span class="help-block mt-0 text-center" ng-if="searched_common_err_pin">{{searched_common_err_pin}}</span>

                            <span class="help-block mt-0"
                                  ng-show="((userForm.available_pincode.$error.maxlength) && (userForm.available_pincode.$dirty) || (userForm.available_pincode.$error.minlength))">
								Pincode should be 6 digits
						</span>

                            <span ng-if="searched_pincode_msg">{{searched_pincode_msg}}</span>

                        </form>


                    </div>


                </div>

                <div class="row text-center pl-10 pr-10" ng-if="order_product_count==null">
                    <div class="col-md-4 col-md-offset-4">
                        <img src="<?php echo base_url(); ?>assets/data/cart_empty.png" class="img-responsive">
                    </div>
                    <div class="col-md-4 col-md-offset-4">
                        <h3 class="text-center">Your shopping cart is empty</h3>
                    </div>
                </div>

                <div class="row text-center pl-10 pr-10" ng-if="order_product_count==0">
                    <div class="col-md-4 col-md-offset-4">
                        <img src="<?php echo base_url(); ?>assets/data/cart_empty.png" class="img-responsive">
                    </div>
                    <div class="col-md-4 col-md-offset-4">
                        <h3 class="text-center">Your shopping cart is empty</h3>
                    </div>
                </div>
                <!---- added for pincode search---->
					
                        <div class="row" >
                            <div class="col-md-12">
									
									<div class="table-responsive">
                                
                                    <table class="table cart_summary mb-0">

<tbody>


                                    <tr ng-repeat="(key, value) in products_order track by $index" ng-init="parentIndex = $index">
                                        
                                    <td colspan="8" style="border:none;" class="pl-0 pr-0">

                                        <div class="row mb-10">
                                        <div class="col-md-12 text-center pl-0 pr-0" id="quantity_status_info_{{$index}}" style="color:#f00;border:none;">
                                        </div>
                                        </div>


                                    <table class="table table-bordered cart_summary mb-0">
                                        <thead>
                                        <tr>
                                            <th class="cart_product text-center" style="vertical-align: middle;">S.No.
                                                {{$index+1}}
                                            </th>
                                            <th class="text-center" style="vertical-align: middle;">Product
                                                Description
                                            </th>
                                            <th width="10%" class="text-center" style="vertical-align: middle;">Qty</th>
                                            <!--
											<th width="5%" ng-if="value.promotion_available==1" class="text-center"
                                                style="vertical-align: middle;">Break-up
                                            </th>
											-->

                                            <!---ng-if="value.promotion_available==1" both with or without discount--> 
                                            <th width="20%" ng-if="(value.promotion_available==1 || value.selling_discount>0)" class="text-center"
                                                style="vertical-align: middle;">Offer
                                            </th>
                                            <th width="10%" class="text-center" style="vertical-align: middle;">Price
                                                <br><small style="font-size:0.7em;">(Inclusive of GST)</small>
                                            </th>
                                            <th width="20%" class="text-center" style="vertical-align: middle;">Shipping
                                                and Delivery
                                            </th>
                                            <th width="10%" class="text-center" style="vertical-align: middle;">Subtotal<br><small
                                                        style="font-size:0.7em;">Excluding Shipping</small><br><small
                                                        style="font-size:0.7em;">(Inclusive of GST)</small></th>


                                        </tr>
                                        </thead>
                                        <tr>

                                            <td class="cart_product">

                                                <a href="<?php echo base_url() ?>detail/<?php echo $controller->sample_code(); ?>{{value.inventory_id}}"><img
                                                            ng-src="<?php echo base_url(); ?>{{value.inventory_image}}"
                                                            alt="Product"></a>

                                            </td>

                                            <td class="cart_description">
                                                <p class="product-name"><a
                                                            href="<?php echo base_url() ?>detail/<?php echo $controller->sample_code(); ?>{{value.inventory_id}}">{{value.product_name}} </a>
                                                            <div class="mb-10">{{value.product_description}}

                                                                
                                                            </div>
                                                </p>
                                                <small class="cart_ref" data-txt="1">{{value.inventory_sku}}</small><br>
                                                <small><a href="#">{{value.attribute_1}} :
                                                        {{value.attribute_1_value}}</a></small><br>
                                                <small ng-if="value.attribute_2!=''"><a href="#">{{value.attribute_2}} :
                                                        {{value.attribute_2_value}}</a></small>
                                                        <br>
                                                        <span >
                                                        <p  ng-if="value.new_promo_avail==1 && value.promotion_available==0" ng-click="avail_new_promotion(value.inventory_id,$index)"  style="cursor:pointer;"><small><span> <u> <i class="fa fa-gift" aria-hidden="true"></i>
                                                                        Avail Offer(s) </u></span></small>
                                                        </p>
                                                        </span>
                                            </td> 

                                            <td class="qty text-center">
                                                <p>
                                                    <input class="form-control input-number qtyinput" id="qtyinput_{{$index}}" ng-model="value.inventory_moq"
                                                           type="number"
                                                           ng-keyup="keypuptimerfun_callafter_fewseconds($event,$index,'keyup')"
                                                           style="border:1px solid #ccc;max-width:100%;min-width: 80px;"  ng-paste="$event.preventDefault()"  maxlength="value.inventory_max_oq" ng-parse-int ng-blur="keypuptimerfun_callafter_fewseconds($event,$index,'blur')" readonly="true">
                                                    <span class="text-center qty_loader" id="inventory_moq_span_{{$index}}" style="display:none;"><img src="<?php echo base_url(); ?>assets/pictures/images/loading.gif" class="img-responsive"></span>
														   
                                                </p>

                                            </td>
                                            <td colspan="5" class="text-center" ng-if="value.item_stock_available==0">
                                                <!---apply opacity over here--->
                                                <span ng-if="value.item_stock_available==0" style="color:red;">Out of stock</span>
                                                <span ng-if="(value.item_stock_available==0 && value.stock_available>0)">Available unit(s): {{value.stock_available}}</span>

                                            </td>

                                            <td colspan="5" ng-if="value.item_stock_available==1">
                                                <table class="" width="100%">
                                                    <tr ng-if="value.promotion_available==0">
                                                        
                                                        <!---added new -->
                                                        
                                                        <td width="5%" class="text-center" ng-if="value.selling_discount>0">
                                                            <!--{{value.promotion}}-->
                                                            1.
                                                        </td>
                                                        <td width="15%" ng-if="value.selling_discount>0">
                                                             
                                                            <p  ng-if="(value.selling_discount>0)"> {{value.selling_discount | ceil }}% Discount on SKU's  </p>
                                                        </td>


                                                        <td width="10%" class="text-center">
                                                           <span ng-if="value.inventory_moq_display">{{value.inventory_moq_display}}</span>
                                                            * <?php echo curr_sym; ?>{{value.selling_price}}
                                                        </td>
                                                        
                                                        <!---added new -->
                                                       
                                                        
                                                        <td width="20%">
                                                            <!--<span ng-if="value.shipping_charge_view==1"> <?php //echo curr_sym; ?> {{value.shipping_charge}}</span>--->
															<span ng-if="value.logistics_id!=-1">
															

                                                            <span ng-if="value.shipping_charge_view==0"
                                                                  ng-model="shipping_charge"><span
                                                                        style="{{value.msg_style}}">{{value.shipping_charge_msg}}</span></span>
                                                             <span class="in-stock ordrk-color"><b>{{value.delivery_date}} </b></span>
															
															</span>
															
                                                            <span ng-if="value.delivery_service_msg"> 
															
															
															<span
                                                                        style="{{value.msg_style}}">  {{value.delivery_service_msg}}</span>
																		
																		
																		
												</span>
                                                        </td>
                                                        <td width="10%" class="text-right">
                                                            <?php echo curr_sym; ?>{{value.inventory_selling_price_without_discount}}

                                                        </td>
                                                    </tr>


                                                    <tr ng-if="value.promotion_available==1 && value.promotion_quote_show==1">
                                                        <td width="5%" class="text-center">
                                                            <!--{{value.promotion}}-->
                                                            1.
                                                        </td>
                                                        <td width="20%"><span ng-if="value.promotion_quote_show==1">{{value.promotion_quote}}<br></span>
                                                        </td>


                                                        <td width="10%" class="text-center">
                                                            <span>{{value.quotient}} * <?php echo curr_sym; ?>{{value.price_of_quotient_1}}</span>

                                                        </td>


                                                        <td width="20%">
											<span ng-if="value.discount_quote_show==0">
											<!--
											<span ng-if="value.shipping_charge_view==1"> <?php //echo curr_sym; ?> {{value.shipping_charge}}</span>
											-->
										<span ng-if="value.logistics_id!=-1">	
										
									<span ng-if="value.shipping_charge_view==0" ng-model="shipping_charge">{{value.shipping_charge_msg}}</span>
									<span
                                                class="in-stock ordrk-color"><b>{{value.delivery_date}} </b></span>
												
										</span>
										
									<span ng-if="value.delivery_service_msg"> <span style="{{value.msg_style}}">{{value.delivery_service_msg}}</span>
												</span>
												
												
												
												</span>
                                                        </td>
                                                        <td width="10%" class="text-right"><span
                                                                    ng-if="value.price_after_promotion_discount>0"> <?php echo curr_sym; ?>{{value.price_after_promotion_discount}}</span>
                                                            <span ng-if="value.promotion_available==0">

										<?php echo curr_sym; ?>{{value.inventory_selling_price_without_discount}}
									</span>
                                                        </td>


                                                    </tr>


                                                    <tr ng-if="value.promotion_available==1 && value.discount_quote_show==1">
                                                        <td width="5%" class="text-center">
                                                            <!--{{value.discount}}-->
                                                            <span ng-if="value.promotion_available==1 && value.discount_quote_show==1 && value.promotion_quote_show==1 || value.promotion_available==1 && value.promotion_quote_show==0 && value.selling_discount>0" >2.</span>
                                                            <span ng-if="value.promotion_available==1 && value.discount_quote_show==1 && value.promotion_quote_show==0" >1.</span>
                                                        </td>
                                                        <td width="20%"><span ng-if="value.discount_quote_show==1">	{{value.promotion_default_discount_promo}}</span>
                                                            <span ng-if="value.promotion_available==1 && value.discount_quote_show==1 && value.selling_discount>0">
                                                               
                                                                <p  ng-if="(value.selling_discount>0)"> {{value.selling_discount | ceil }}% Discount on SKU's  </p>
                                                                
                                                            </span>
                                                        </td>
                                                        
                                                        <td width="10%" class="text-center"><span
                                                                    ng-if="value.modulo!=0">{{value.modulo}} * {{value.price_of_modulo_1}}<br></span>
                                                        </td>
                                                        <td width="20%">
                                                            <!--<span ng-if="value.shipping_charge_view==1"> <?php //echo curr_sym; ?> {{value.shipping_charge}}</span>-->
															
															<span ng-if="value.logistics_id!=-1">
                                                            <span ng-if="value.shipping_charge_view==0"
                                                                  ng-model="shipping_charge">{{value.shipping_charge_msg}}</span>
                                                            <span class="in-stock ordrk-color"><b>{{value.delivery_date}} </b></span>
															
															</span>
															
															
                                                            <span ng-if="value.delivery_service_msg"> <span
                                                                        style="{{value.msg_style}}">{{value.delivery_service_msg}}</span>
												</span>
                                                        </td>
                                                        <td width="10%" class="text-right"><span
                                                                    ng-if="value.price_after_default_discount>0"><?php echo curr_sym; ?>{{value.price_after_default_discount}}</span>
                                                        </td>
                                                    </tr>


                                                </table>
                                            </td>


                                        </tr>

                                        <tbody class="extra_data" id="extra_data_{{$index}}"
                                               ng-if="value.show_addon_items==1 && value.addon_count_updated==1">

                                        </tbody>
										
										<tbody class="extra_data" id="extra_data_{{$index}}"
                                               ng-if="value.show_addon_items==1 && value.addon_count_updated!=1">

                                        </tbody>
                                        
                                        <!--- addon products --->
                            <tbody class="extra_data" id="1addon_products_{{$index}}"  ng-if="value.addon_products_status=='1'">
                                                
                            <tr ng-if="value.addon_products.length>0">
                                <td colspan="5" >
                                        <div ng-repeat="addon in value.addon_products track by $index" class="col-xs-5 col-sm-5 col-md-3 nopad text-center" id="addon_{{$index}}">
                                            <label class="image-checkbox image-checkbox-checked" ng-click="remove_addon_products($parent.$index,addon.inv_id,addon.inv_price)">
                                            <img class="img-responsive" src="<?php echo base_url(); ?>{{addon.inv_image}}" />

                                            
                                            <div class="yourprice_div offerprice_addon" ng-if="addon.inv_price==0"><strong>FREE </strong></div>
                                            
                                            <div class="yourprice_div offerprice_addon" ng-if="addon.inv_price>0"><strong><?php echo curr_sym; ?>{{addon.inv_price}}  only</strong></div>
                                                    
                                            
                                            <div class="yourprice_div"><span class="hurbztextcolor">{{addon.offer_percentage}}% off</span> MRP : <?php echo curr_sym; ?>{{addon.inv_mrp_price}}</div>

                                            <i class="fa fa-check hidden" style="line-height: 15px;"></i>
                                            
                                            <button class="btn btn-warning btn-sm preventDflt sku_display_name_on_buttons align-top" style="width:95%;">{{addon.inv_display_name}}</button>
                                            <h5 class="margin-top margin-bottom"><b>{{addon.inv_sku_id}}</b></h5>
                                            </label>
                                        </div>
                                        

                                        
                                </td>
                                <td colspan="1" class="text-center">
                        
                                <span ng-if="value.addon_count">Addon Products : {{value.addon_count}}</span> 

                                </td>
                                <td class="text-center">
                                    <span id="addon_total_price_1"><?php echo curr_sym; ?>{{value.addon_total_price}}</span>
                                </td>
                            </tr>



                                        </tbody>
                                        <!--- addon products --->

                                        <!-- added starts -->
                                        <?php
                                        if ($get_shipping_module_info_obj->each_sku == 1) {
                                            ?>
                                            <tr ng-if="value.shipping_charge_view==1">
                                                <td colspan="8">
                                                    <table class="" width="100%">
                                                        <td class="text-right" colspan="8">
                                                            Shipping Charges :

                                                            <span class="text-right">
													  <span ng-if="eligible_free_shipping_display==0">
														<?php echo curr_sym; ?>{{(value.shipping_charge_total)}}
													</span>
													<span ng-if="eligible_free_shipping_display==1">
														<?php echo curr_sym; ?>{{value.shipping_charge_total}}
														<?php
														/*
														?>
														<strike><?php echo curr_sym; ?>{{value.shipping_charge}}</strike> Free
														<?php
														*/
														?>
													</span>

												</span>

                                                        </td>
                                                    </table>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        <!-- added starts -->


                                        <tr ng-if="value.promotion_available==1">
                                            <td colspan="8">
                                                <table class="" width="100%">

                                                    <td class="text-right" colspan="8">
                                                        
														 <?php
                                        if ($get_shipping_module_info_obj->each_sku == 1) {
                                            ?>
												<span ng-if="value.shipping_charge_view==1">Subtotal Including Shipping</span>
												<span ng-if="value.shipping_charge_view==0">Subtotal</span>
												 <?php
                                        }
                                            ?>		
														
														<span class="red"
                                                                                          style="float:left;"><a
                                                                    href='#' class="red btn-sm"
                                                                    ng-click="removeItem($index)"> Remove Item</a></span>

                                                        <span class="text-right" colspan="1"
                                                              ng-if="value.item_stock_available==1">
														<span>  <?php echo curr_sym; ?>{{value.subtotal}} </span>
													</span>
                                                    </td>
                                                </table>
                                            </td>

                                        </tr>

                                        <tr ng-if="value.promotion_available==0">
                                            <td colspan="8">
                                                <table class="" width="100%">
                                                    <td class="text-right" colspan="8">
                                                        
														
														<?php
                                        if ($get_shipping_module_info_obj->each_sku == 1) {
                                            ?>
												<span ng-if="value.shipping_charge_view==1">Subtotal Including Shipping</span>
												<span ng-if="value.shipping_charge_view==0">Subtotal</span>
												 <?php
                                        }
                                            ?>	


														<span class="red"
                                                                                          style="float:left;"><a
                                                                    href='#' class="red btn-sm"
                                                                    ng-click="removeItem($index)"> Remove Item</a></span>

                                                        <span class="text-right" ng-if="value.item_stock_available==1">

													<span>  <?php echo curr_sym; ?>{{value.subtotal}}</span>
												</span>

                                                    </td>
                                                </table>
                                            </td>
                                        </tr>


                                    </table>

                                </td>
                            </tr>


                            <?php if (!empty($promotions_cart)) { ?>
                                <tr style="font-size:.95em;">

                                    <td colspan="8" style="border:none;" class="pl-0 pr-0">
                                        <table width="100%" class="mt-10">

                                            <tfoot ng-if="out_of_stock_item_exists==0">


                                            <tr ng-if="eligible_discount_percentage_display==1">

                                                <td class="text-right" colspan="2" style="font-size:14px;">
                                                    <span ng-if="eligible_discount_percentage_display==1"> Eligible for discount on invoice {{eligible_discount_percentage}}% of {{total_price_without_shipping_price}} {{currency_type_discount}}</span>
                                                    <span ng-if="eligible_discount_percentage_display==1">- <?php echo curr_sym; ?>{{grandTotal_for_discount}}</span>
                                                </td>
                                            </tr>

                                            <tr ng-if="eligible_free_shipping_display==1">

                                                <td class="text-right" colspan="2" style="font-size:14px;">
                                                    <!--<span ng-if="eligible_free_shipping_display==1"> Eligible for  Free shipping on  invoice of shipping charges {{total_shipping_price}} {{currency_type_free_shipping}} </span>-->

                                                    <span ng-if="eligible_free_shipping_display==1"> Eligible for Free Shipping under offer Freeship! </span>

                                                    <span ng-if="eligible_free_shipping_display==1">
										- <?php echo curr_sym; ?>{{total_shipping_price}}
									 </span>
                                                </td>
                                            </tr>


                                            <tr ng-if="eligible_discount_percentage_display==1 || eligible_free_shipping_display==1">

                                                <td class="text-right" colspan="2" style="font-size:14px;">
                                                    <!--
								 <span ng-if="eligible_discount_percentage_display==1 || eligible_free_shipping_display==1"> To amount payable on invoice after <span ng-if="eligible_discount_percentage_display==1"> discount </span>

								 <span ng-if="eligible_discount_percentage_display==1 && eligible_free_shipping_display==1">  and </span>
								 </span>

								 <span ng-if="eligible_free_shipping_display==1"> after deduction shipping charges </span>
-->

                                                    Net Invoice Amount <?php echo curr_sym; ?>{{amount_to_pay}}
                                                </td>
                                            </tr>

                                            <tr>

                                                <td class="text-right" colspan="2">

                                                    <!--{{total_shipping_price}} and {{total_price_without_shipping_price}} -->

                                                    <h3 style="color:#004b9b;">
                                                        Total Price
                                                        <span ng-if="eligible_discount_percentage_display==1">
									+ <?php echo curr_sym; ?>{{grandtotal_without_discount}}
									</span>
                                                        <span ng-if="eligible_discount_percentage_display==0">
									 <?php echo curr_sym; ?>{{amount_to_pay}}
									</span>
                                                    </h3>
                                                </td>


                                            </tr>

                                            <tr ng-if="eligible_discount_percentage_display==1 || eligible_free_shipping_display==1">


                                                <td class="text-right" colspan="2" style="font-size:14px;">
								 <span ng-if="eligible_discount_percentage_display==1 || eligible_free_shipping_display==1"> <i
                                             class="fa fa-heart" style="color:#ff0000"></i>	You Saved
								 </span>

                                                    <?php echo curr_sym; ?>{{you_saved}}
                                                </td>
                                            </tr>
                                            <!--
							<tr ng-if="eligible_cash_back_percentage_display==1" class="bg-info">

									<td ><span ng-if="eligible_cash_back_percentage_display==1"> Total amount to be credited to your Wallet </span></td>
									<td  class="text-right" ng-if="eligible_cash_back_percentage_display==1">  <?php echo curr_sym; ?> {{total_cash_back}}</td>

							</tr>
							--->

                                            </tfoot>


                                        </table>
                                    </td>
                                </tr>

                            <?php } else { ?>

                                <tr ng-if="out_of_stock_item_exists==0" style="font-size:.95em;">

                                    <td colspan="8" style="border:none;" class="pl-0 pr-0">
                                        <table width="100%" class="mt-10">
                                            <tbody>
                                            <?php
                                            if ($get_shipping_module_info_obj->all_skus == 1) {
                                                ?>
                                                <tr class="bg-default">
                                                    <td class="text-right" style="font-weight:bold;" colspan="8">

                                                        Shipping Charges :

                                                        <span>xxxxxyyyyzzzz</span>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                            <tr class="bg-default">
                                                <td class="text-right"
                                                    style="font-weight:bold;color:red;font-size:1.2em;" colspan="8">

                                                    Total Price <!--{{total_price_without_shipping_price}}-->

                                                    <span><?php echo curr_sym; ?>{{amount_to_pay}}</span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                            <?php } ?>

                            <tr ng-if="out_of_stock_item_exists==0" style="font-size:.95em;">

                                <td colspan="8" style="border:none;">
                                    <table width="100%" class="mt-10">
                                        <?php if (!empty($promotions_cart)) { ?>
                                            <tfoot ng-if="show_surprise_gift_on_invoice==1 && out_of_stock_item_exists==0">
                                            <tr class="bg-warning" ng-if="invoice_surprise_gift_result">
                                                <td>
                                                    <span class="right-align-cashback"><i
                                                                    class="fa fa-gift" aria-hidden="true" style="color:#ff0000"  class="already_availed_offer"></i> Eligible for surprise gifts (<span
                                                                ng-bind-html="explicitlyTrustedHtml_invoice_surprise_gift_result"></span>)</span>
                                                    

                                                </td>

                                            </tr>
                                            </tfoot>
                                        <?php } ?>

                                        <tfoot ng-if="out_of_stock_item_exists==0">
                                        <tr ng-if="show_cash_back_tr==1" class="bg-info">
                                            <td colspan="2" style="padding: 1px;">
                                                <div class="panel-group bg-info" style="margin:0px;">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" data-toggle="collapse"
                                                             href="#collapse1"
                                                             style="background-color:#d9edf7;cursor:pointer;">
							<span style="font-size:.95em;">

							<span class="panel-title text-right">
							<i class="more-less glyphicon glyphicon-plus"></i> <span class="left-align-cashback-total">Total cash back for SKUs </span><span
                                        class="text-right" style="width:30%;font-size: .9em;"><?php echo curr_sym; ?>{{total_cash_back_for_sku}}</span>

							</span></span>
                                                        </div>
                                                        <div id="collapse1" class="panel-collapse collapse">
                                                            <ul class="list-group">
                                                                <li class="list-group-item"
                                                                    ng-repeat="(key,value) in cash_back_tr_arr"
                                                                    style="background-color:#d9edf7;"><span
                                                                            class="text-left left-align-cashback">
									 {{value.promotion_cashback_percentage}} % cash back on {{value.sku_count}} quantity of {{value.inventory_sku}} </span><span
                                                                            class="text-right"
                                                                            style="width:30%"><?php echo curr_sym; ?>{{value.cash_back_amount}}</span>
                                                                </li>
                                                            </ul>

                                                        </div>
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>


                                        <tr ng-if="eligible_cash_back_percentage_display==1">

                                            <td style="border:none;" style="font-size:14px;">


                                                <span ng-if="eligible_cash_back_percentage_display==1"
                                                      style="font-size:14px;"> <i class="fa fa-heart"
                                                                                  style="color:#ff0000"></i> Cashback to be credited to your wallet ({{eligible_cash_back_percentage}}%) </span>
                                                <span class="text-right"
                                                      ng-if="eligible_cash_back_percentage_display==1"
                                                      style="width:30%;font-size: 14px;"><?php echo curr_sym; ?>{{grandTotal_for_cash_back | number:0}}</span>


                                            </td>
                                        </tr>

                                        </tfoot>

                                    </table>
                                </td>
                            </tr>
                            
</tbody>
                                    </table>

                            </div>


                        </div>
					</div>
				<div class="cart_navigation">

                        <?php
                        if ($this->session->userdata("products_page")) {
                            $products_page = $this->session->userdata("products_page");
                            $url = $products_page;
                        } else {
                            $url = base_url();
                        }
                        ?>
                        <button id="proceed_to_checkout_id" class="next-btn proceed_to_checkout ml-10 preventDflt"
                                ng-click="proceedCheckout()" ng-disabled="buttonClicked">Proceed to Checkout
                        </button>
                        <button class="prev-btn preventDflt" ng-click="continueShoppingFun('<?php echo $url; ?>')"
                                id="continue_shopping_id">Continue Shopping
                        </button>


                    </div>
            </div><!---tab---->


        </div><!--page container----------->

    </div>
</div>


<script>
    window.onload = function () {
        var inv_elems_display_elems = document.getElementsByClassName('free_inv_nums')

        for (var i = 0; i < inv_elems_display_elems.length; i++) {
            inv_elems_display_elems[i].innerHTML = quotient
            //alert(quotient)
        }

    }

    $(document).ready(function () {

        function toggleIcon(e) {
            $(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('glyphicon-plus glyphicon-minus');
        }

        $('.panel-group').on('hidden.bs.collapse', toggleIcon);
        $('.panel-group').on('shown.bs.collapse', toggleIcon);
    });
	
        module.filter('ceil', function(){
            return function(input) {
              //return Math.ceil(+input);
              return Math.round(+input);
            };
        })

	module.directive('ngParseInt', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            scope.$watch(attrs.ngModel, function (v) {
              //  //console.log('value changed, new value is: ' + v);
               ngModel.$setViewValue(parseInt(v));
               ngModel.$render();
            });
        }
    };
});
$(document).ready(function(){
//$(document).on("input",".input-number", function () {
//if (/^0/.test(this.value)) {
//this.value = this.value.replace(/^0/, "")
//}
//});
});

</script>
<!-- ./page wapper-->
