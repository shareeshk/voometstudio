
<div class="columns-container">
    <div class="container" id="columns">
         <ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account/order_details/<?php echo $order_id; ?>">Order Details</a></li>
		  <li class="breadcrumb-item active">Contact us</li>
		</ol>
        <div class="row">

            <div class="center_column col-xs-12 col-sm-12 my_orders_links" id="center_column">
            	
            			<!-- page heading-->
		                <h2 class="page-heading margin-bottom">
		                    <span class="page-heading-title2">Offline Chat</span>
		                </h2>
		                
						<div class="row">
							<div class="margin-top margin-bottom col-sm-9">
							 Ordered on date <i class="separator_line"></i>  Order ID # <?php echo $order_id; ?>
							</div>
							
						</div>
							
						<!-- ../page heading-->
		
			<div class="panel panel-default">					
				<div class="panel-heading" role="tab">		
					<div class="row panel-title normal-text">
						<div class="col-md-12">
						
							<strong><span class="text-uppercase">Return Order ID </span>: # <?php echo $return_order_id; ?></strong>
							<span class="pull-right"><u><a href="<?php echo base_url(); ?>Account/order_details/<?php echo $order_id;?>#<?php echo $order_item_id; ?>">Back</a></u></span>
						</div>
					</div>

				</div>
								
				<!----conversation ------------->
				
				<div aria-expanded="true" id="collapseOne" class="">
					<div class="panel-body">
					<ul class="chat" style="list-style-type:none;">
						<?php
							
							//$replacements_conservation_obj_arr=$controller->get_returns_conservation_chain_in_customerpanel_for_replacement($return_order_id);
							?>
							<input type="hidden" id="return_order_id" value="<?php echo $return_order_id; ?>">
							<?php
							foreach($replacements_conservation_obj_arr as $replacements_conservation_obj){
								?>
								
								<li class="left clearfix">
                           <span>
                              <!--<img src="/assets/pictures/images/cust_a.png" alt="Customer" class="img-circle" />-->
                              <div class="header">
                                 <?php
									if($replacements_conservation_obj->user_type=="admin"){
								 ?>
								 <span style="color:#FD856A;">Admin :</span>
								 
								 <?php
									}
								 ?>
								  <?php
									if($replacements_conservation_obj->user_type=="customer"){
								 ?>
								 <span style="color:#ff00ff;"><?php echo $controller->get_customer_name($replacements_conservation_obj->customer_id);?> :</span>
								 <?php
									}
								 ?>
                                 <small class="text-muted pull-right">
                                 <span class="glyphicon glyphicon-time"></span> <?php echo date("D j M,Y h:i A",strtotime($replacements_conservation_obj->timestamp));?></small>
                              </div>
                           </span>
                           <div class="chat-body clearfix">
                              <p>
                                 <span style="color:#777777;" class="mes more"><?php echo $replacements_conservation_obj->description?></span>                                
                              </p>
							  <?php
								if($replacements_conservation_obj->image!=""){
							  ?>
							  <a download href="<?php echo base_url('Account/returns_file_download_replacement/'.$replacements_conservation_obj->id)?>"><button class="btn-xs btn-success btn common preventDflt">Download</button> </a>
							   <?php
								}
							  ?>
                           </div>
                        </li>
								<?php
							}
						?>
                     </ul>
					 
					</div>
					<div class="panel-footer">
				  						
						<form id="send_form_admin_contact_admin_form_stock" method="post" name="send_form_admin_contact_admin_form_stock" enctype="multipart/form-data">
						<input type="hidden" name="return_order_id" value="<?php echo $return_order_id; ?>">
						<input type="hidden" name="user_type" value="customer">
						<input type="hidden" name="status" value="pending">
						<input type="hidden" name="customer_id" value="<?php echo $customer_id; ?>">
						<?php
							
								$admin_action="stock";
								$customer_action="stock";			
						?>
						<input type="hidden" name="admin_action" value="<?php echo $admin_action; ?>">
						<input type="hidden" name="customer_action" value="<?php echo $customer_action; ?>">
                        <div class="input-group">
		
                           <textarea style="width:95%;" placeholder="Reply Message" name="description" id="description_rep" class="form-control input-sm" cols="50" rows="4"></textarea>
						   <input type="file" name="image_attachment">
                           <span class="input-group-btn">
                           <button class="btn btn-primary btn-lg preventDflt"  type="submit">Send</button>
                           </span>
                        </div>
						</form>
					 
					</div>
				</div>
          
				
				<!----conversation ------------->
				
			
			</div><!--panel default--->
				

            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>



<script>

$(document).ready(function(){
	
	$("#send_form_admin_contact_admin_form_stock").on('submit',(function(e) {
		e.preventDefault();
		var description=$("#description_rep").val();
		if(description!=""){
			$.ajax({
				url:"<?php echo base_url();?>Account/contact_admin_replacement",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					if(data){	

					    alert("Successfully sent");
                        location.reload();
                        /*
						bootbox.alert({ 
							  size: "small",
							  message: "Successfully sent",
							  callback: function () { 
								location.reload();
							  } 
							});
						*/
							
						
					}else{

					    alert( "not sent");

						/*	bootbox.alert({
							  size: "small",
							  message: "not sent"
							   
							});*/
					}
				}	        
		   });
		
		}else{

		    alert("Please enter description!");
			
			/*bootbox.alert({
							  size: "small",
							  message: "Please enter description!"
							   
							});*/
		}
	}));

});

$(document).ready(function(){
	
		$.ajax({
			url:"<?php echo base_url()?>Account/update_msg_status_replacement",
			type:"POST",
			data:"return_order_id="+$("#return_order_id").val(),
			success:function(data){
				//location.reload();
				$("#send_form_admin_contact_admin_form_stock")[0].reset();
				
			}
		});
    
})
$(document).ready(function(){
	showChar(500);
});
</script>

