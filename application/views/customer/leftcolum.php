<style>

a, abbr, acronym, address, applet, article, aside, audio, b, blockquote, big, body, center, canvas, caption, cite, code, command, datalist, dd, del, details, dfn, dl, div, dt, em, embed, fieldset, figcaption, figure, font, footer, form, h1, h2, h3, h4, h5, h6, header, hgroup, html, i, iframe, img, ins, kbd, keygen, label, legend, li, meter, nav, object, ol, output, p, pre, progress, q, s, samp, section, small, span, source, strike, strong, sub, sup, table, tbody, tfoot, thead, th, tr, tdvideo, tt, u, ul, var {
	/*vertical-align: baseline;*/
	}
	strong{
		color:#666;
	}
	.well{
		background-color:#fff
	}
	.link_active{
		color:#000;
		font-weight:bold ;
	}
	.left-module{
	background-color: #fff;
	}
	.columns-container{
	background-color: #f9f9f9;
	}
	sub{
		vertical-align: baseline !important;
	}
	.popover{
    max-width: 100%; /* Max Width of the popover (depending on the container!)*/
	}
	.detailed_order_item li {
	    float: left;
		margin-left:13px;
		padding:0px !important;
	}		
	.detailed_order_item ul
	{
	list-style-type: none;
	padding:0px !important;
	}
	.stage_order_cancelled{
		color:red
	}
	.stage_order_completed{
		color:green
	}
	.text_red{
		color:red;
		font-weight:400;
	}
	.view-more a {
    border: 1px solid #eaeaea;
    padding: 10px 12px;
    background: #eee;
	}
	.view-more a:hover {
    background: #382e03;
    color: #fff;
}
.border_bottom{
	border-bottom: 1px solid red;
}
.cursor_pointer{
	cursor:pointer;
	text-decoration: underline;
}
</style>
<style>/*******************************
* ACCORDION WITH TOGGLE ICONS
* Does not work properly if "in" is added after "collapse".
*******************************/
	.panel-group .panel {
		border-radius: 0;
		box-shadow: none;
		border-color: #EEEEEE;
	}

	/*.panel-default > .panel-heading {
		padding: 0;
		border-radius: 0;
		color: #212121;
		background-color: #FAFAFA;
		border-color: #EEEEEE;
	}*/

	.panel-title {
		font-size: 14px;
	}

	.panel-title > span {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.more-less {
		float: right;
		color: #212121;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border-top-color: #EEEEEE;
	}
.nav > li > a {
    position: relative;
    display: block;
    padding: 2px 15px;
}

	
</style>

            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block best sellers -->
                <div class="block left-module">
                    <p class="title_block">My Account</p>
                    <div class="block_content">
                        <ul class="products-block best-sell">
						<?php if($this->session->userdata("user_type")=='franchise_customer'){ ?>
						<li><strong>My Projects</strong>
									<ul class="nav nav-stacked">
									
                                    	<li>
                                    		<a href="<?php echo base_url()?>Account/franchise_projects" <?php if($cur_page=="projects"){echo 'class="link_active"';} ?> >My Projects
										
											<?php 
											$pro_count=get_franchise_projects_count();
											if(intval($pro_count)>0){

												?>
												<span class="badge"><?php echo $pro_count; ?></span>
												<?php
											}
											?>
										 
										</a>
                                    		
                                    	</li>
									</ul>
						</li>
						<?php } ?>

							<?php if($this->session->userdata("user_type")!='franchise_customer'){ ?>

                                <li><strong>ORDERS</strong>
                                    <ul class="nav nav-stacked">
                                    	<li>
                                    		<a href="<?php echo base_url();?>Account/my_order" <?php if($cur_page=="my_order"){echo 'class="link_active"';} ?> >My Orders</a>
                                    	</li>
                                    	<?php if($cur_page=="my_order_details"){ echo '<li><i class="link_active"><sub><b>'.$order_id.'</b><sub></i></li>';}?>
                                    	<?php if($cur_page=="cancel_order_item"){ echo '<li>'.$order_id.'<br><br><i > <sub><b>Cancel '.$order_item_id.'</b><sub></i></li>';}?>
                                    	<?php if($cur_page=="return_order_item"){ echo '<li>'.$order_id.'<br><br><i > <sub><b>Return '.$order_item_id.'</b><sub></i></li>';}?>
                                    </ul>
                                </li>
							<?php } ?>

                                <li><strong>PAYMENTS</strong>
                                    <ul class="nav nav-stacked">
									<?php if($this->session->userdata("user_type")!='franchise_customer'){ ?>
                                    	<li>
                                    		<a href="<?php echo base_url()?>Account/wallet" <?php if($cur_page=="wallet"){echo 'class="link_active"';} ?> >Wallet</a>
                                    		<?php
											/*
											?><a href="<?php echo base_url()?>Account/cards" <?php if($cur_page=="cards"){echo 'class="link_active"';} ?> >My Saved Cards</a>
											<?php
											*/
											?>
											
                                    		
                                    	</li>

										<li>
                                    		<a href="<?php echo base_url()?>Account/bank" <?php if($cur_page=="bank"){echo 'class="link_active"';} ?> >My Saved Bank Details</a>
                                    		
                                    	</li>
										<?php }else{

											?>
										<li>
                                    		<a href="<?php echo base_url()?>Account/franchise_bank_details" <?php if($cur_page=="bank"){echo 'class="link_active"';} ?> >My Bank Details</a>
                                    		
                                    	</li>

											<?php
										} ?>
										

                                    </ul>
                                </li>

								<?php if($this->session->userdata("user_type")!='franchise_customer'){ ?>

                                <li><strong>MY STUFF</strong>
                                    <ul class="nav nav-stacked">
                                    	<li>
                                    		<a href="<?php echo base_url() ?>Account/reviews" <?php if($cur_page=="reviews"){echo 'class="link_active"';} ?> >My Reviews &amp; Ratings</a>
                                    		<a href="<?php echo base_url() ?>Account/wishlist" <?php if($cur_page=="mywishlist"){echo 'class="link_active"';} ?> >My WishList</a>
                                    		<?php /*
											<a href="#" <?php if($cur_page=="orders"){echo 'class="link_active"';} ?> >Recommendations For You</a>*/ ?>
											
											
											
                                    	</li>
                                    	
                                    </ul>
                                </li>
								
							
								
                                <li>
				                    <span><strong style="cursor:pointer">MY COMPLAINTS</strong><span>
				                    <ul class="nav nav-stacked ">
				                        <li>
				                        	<a  href="<?php echo base_url();?>Account/complaint_history" <?php if($cur_page=="comp_cas_hist"){echo 'class="link_active"';} ?> >Case History</a>
				                        	<?php if(isset($comp_det_cas)){ ?> 
				                        		<a  href="<?php echo base_url();?>Account/complaint_history" <?php if($cur_page=="comp_cas_hist"){echo 'class="link_active"';} ?> >Case History</a>
				                        	<?php }?>
				                        	<a href="<?php echo base_url()?>Account/complaint_create" <?php if($cur_page=="comp_cret_cas"){echo 'class="link_active"';} ?> >Create Case</a></li>
				                    </ul>
						        </li>

								<?php 
								}
								?>
                                <li>
                                	<span><strong style="cursor:pointer">MY SETTINGS</strong><span>
                                    <ul class="nav nav-stacked" >
                                    	<li>
                                    		<a href="<?php echo base_url();?>Account" <?php if($cur_page=="p_info"){echo 'class="link_active"';} ?> >Personal Information</a>
                                    		<a href="<?php echo base_url()?>Account/change_password" <?php if($cur_page=="change_password"){echo 'class="link_active"';} ?> >Change Password</a>

											<?php if($this->session->userdata("user_type")!='franchise_customer'){ ?>
                                    		<a href="<?php echo base_url()?>Account/addresses" <?php if($cur_page=="addresses"){echo 'class="link_active"';} ?> >Addresses</a>
											<?php } ?>

                                    		<?php
											/*
											?><a href="<?php echo base_url()?>Account/profile" <?php if($cur_page=="profile"){echo 'class="link_active"';} ?> >Profile Settings</a><?php
											*/
											?>
                                    		<a href="<?php echo base_url()?>Account/login_credentials" <?php if($cur_page=="login_credentials"){echo 'class="link_active"';} ?> >Update Email/Mobile</a>

											<?php if($this->session->userdata("user_type")!='franchise_customer'){ ?>
                                    		
												<a href="<?php echo base_url()?>Account/deactivate_account" <?php if($cur_page=="deactivate_account"){echo 'class="link_active"';} ?> >Deactivate Account</a>
											
                                    			<a href="<?php echo base_url()?>Account/notification" <?php if($cur_page=="notification"){echo 'class="link_active"';} ?> >Notifications</a>
											
											<?php  } ?>
                                    	</li>
                                    </ul>
                                </li>
                            </ul>
                    </div>
                </div>

                

            </div>