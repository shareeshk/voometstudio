<style>
.nav > li > a {
    position: relative;
    display: block;
    padding: 2px 15px;
}
.top_align
{
	margin-bottom: 10px;
}
</style>
<div class="columns-container">
    <div class="container" id="columns">
       <ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account">My account</a></li>
		  <li class="breadcrumb-item active">Profile Settings</li>
		</ol>
        <div class="row">
			<?php require_once 'leftcolum.php';?>
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
				<div class="col-md-12">
				<div class="well">
				<div class="panel panel-default">
				  <div class="panel-heading">Additional Profile Settings</div>
				  <div class="panel-body">
					<form action="<?php echo base_url()?>Account/update_additional_profile" method="post" role="form" class="form-horizontal">
								   <?php 
								   $skill_material_arr=array();
								   if(isset($profile[0]["skill_material"])){
									   $skill_material_arr=explode(",",$profile[0]["skill_material"]);
								   }
								  
								  if(!empty($default_customer_skills)){
								  foreach($default_customer_skills as $customer_skills){ ?>
								  <div class="form-group">
								 
								  	<label class="control-label col-md-3"><?php echo $customer_skills["profile_name"];?></label>
									
									<div class="col-md-6">
									
									<select  name="prefer_material[]" class="form-control">
								  
								  	<option value="">-Choose One-</option>
									
								    <?php 
								  foreach($default_customer_preferred_material as $customer_preferred_material){ 
									if($customer_skills["id"]==$customer_preferred_material["addl_profile_name_id"]){
									?>
									<option value="<?php echo $customer_preferred_material['profile_value_id']?>" <?php if(in_array($customer_preferred_material['profile_value_id'],$skill_material_arr)){ echo "selected";}?>><?php echo $customer_preferred_material['profile_value']?></option>
								  <?php } 
								  }
								  ?>
								  </select>
								  </div>
								  </div>
									
								  <?php } ?>
								<?php } ?>
								<?php
									if(!empty($default_customer_skills)){
								?>
								 <div class="form-group">
								 <div class="col-md-6 col-md-offset-3">
							  <button type="submit" class="button preventDflt">Submit</button>
							  <button type="button" class="button preventDflt" onclick="hide_profile_form();">Cancel</button>
							  </div>
								  </div>
								  <?php } else{
										?>
										<div class="form-group">
								 <div class="col-md-6 col-md-offset-3">
										Profile settings has not been setup
										</div>
								  </div>
										<?php
								  }								  ?>
							</form>
				  </div>
				</div>
				</div>
				</div>
               

            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>