<style>
.panel-heading-padding{
	padding:0;
}
</style>
<div class="columns-container">
    <div class="container" id="columns">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account">My account</a></li>
		  <li class="breadcrumb-item active">Wallet Information</li>
		</ol>
        <div class="row">
            <!-- Left colunm -->
			<?php require_once 'leftcolum.php';?>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
					<div class="row">
					<div class="col-sm-12">     
                <div class="well">
					<div class="panel panel-default">
						<div class="panel-heading"><strong>Wallet Information</strong></div>
						<div class="panel-body">
							<div class="f-row product-price">
								Available Credit : <span style="color:#46b8da"><?php echo curr_sym; ?> <?php echo $wallet_amount ?></span>
							</div>
						</div>
					</div>
					</div>
					</div>
					</div>
	

<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   
   var table = $('#wallet_transaction').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>Account/wallet_transaction_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#wallet_transaction_processing").css("display","none");
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'10%',
         'className': 'dt-body-center'
      }],
      'order': [0, 'desc']
   });
});	
</script>        
				<div class="row">
				<div class="col-sm-12">     
                <div class="well">
                	<b>Transcation Details</b><hr>
                	<table id="wallet_transaction" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>S.No</th>
			                <th>Comments</th>
							<th>Credit</th>
			                <th>Type</th>
			                <th>Debit</th>
			                <th>Balance</th>
							<th>Last update</th>
			            </tr>
			        </thead>
			        </table>

                </div>
				</div>
				</div>
				
				<div class="common">
				<?php
				$i=1;
				foreach($wallet_transaction_bank_table as $wallet_transaction_bank_table_arr){ ?>
							<div class="row">
				<div class="col-sm-12">     
                <div class="well">
                	
<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
					<div class="panel panel-info">
						<div class="panel-heading panel-heading-padding" role="tab" id="headingThree">
							<div class="panel-title text-center">
								<span class="collapsed" >
									<i class="more-less glyphicon glyphicon-plus" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo<?php echo $wallet_transaction_bank_table_arr["id"] ?>" aria-expanded="true" aria-controls="collapseTwo"></i>
									<span><h4>
										<?php
											if($wallet_transaction_bank_table_arr["status"]=="accept" && $wallet_transaction_bank_table_arr["allow_update_bank_details"]=="updated"){
												echo "Refund Initiated Again";
											}
											else if($wallet_transaction_bank_table_arr["status"]=="pending"){
												echo "Refund Requested";
											}
											else if($wallet_transaction_bank_table_arr["status"]=="reject"){
												echo "Refund Rejected";
											}
											else if($wallet_transaction_bank_table_arr["status"]=="accept"){
												echo "Refund Accepted";
											}
											else if($wallet_transaction_bank_table_arr["status"]=="failure"){
												echo "Refund Failed";
											}
											else if($wallet_transaction_bank_table_arr["status"]=="success"){
												echo "Refunded to your Bank account";
											}
										?>
										</h4>
									</span>
								</span>
								
							</div>
						</div>
						
						<div id="collapseTwo<?php echo $wallet_transaction_bank_table_arr["id"] ?>" class="panel-collapse collapse wallet" role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body">
							<?php
							if($wallet_transaction_bank_table_arr["status"]=="reject"){?>
							<div class="row">
							<div class="col-md-12">
										
									<dl>	
									<dt><strong>Rejected Comments</strong></dt> 
									<dd class="more"><?php echo $wallet_transaction_bank_table_arr["rejected_comments"]; ?></dd>
									</dl>
									
								</div>
								</div>
								<?php
								}
								?>
								
								<?php
							if($wallet_transaction_bank_table_arr["status"]=="success"){
								$transaction_date=$wallet_transaction_bank_table_arr["transaction_date"];
								?>
								<div class="row">
							<div class="col-md-12">
								<dl class="dl-horizontal">
										<dt><strong>Amount Refunded</strong></dt> 
										<dd><?php echo curr_sym;?><?php echo $wallet_transaction_bank_table_arr["transaction_amount"] ?> was sent to your bank account</dd>
										<?php if($wallet_transaction_bank_table_arr["accepted_transaction_comments"]!=''){
											?>
											<dt><strong>Admin comments</strong></dt> 
											<dd><?php echo $wallet_transaction_bank_table_arr["accepted_transaction_comments"] ?> </dd>
											<?php 
										} ?>
										<dt><strong>Requested Amount</strong></dt> 
										<dd><?php echo curr_sym;?><?php echo $wallet_transaction_bank_table_arr["requested_transaction_amount"] ?> </dd>
										
										<dt><strong>Transaction ID</strong></dt> 
										<dd><?php echo $wallet_transaction_bank_table_arr["transaction_id"] ?></dd>
										<dt><strong>Transaction Date</strong></dt> 
										<dd><?php echo date("j F Y",strtotime($transaction_date)); ?></dd>
										<dt><strong>Transaction Time</strong></dt> 
										<dd><?php echo $wallet_transaction_bank_table_arr["transaction_time"] ?></dd>
										</dl>
										</div>
								</div>
								<?php } ?>
								<?php
							if($wallet_transaction_bank_table_arr["status"]!="success" && $wallet_transaction_bank_table_arr["status"]!="reject"){
								?>
									<div class="row">
							<div class="col-md-12">
									<dl class="dl-horizontal">
										<dt><strong>Bank Name</strong></dt> 
										<dd><?php echo $wallet_transaction_bank_table_arr['bank_name'] ?></dd>
										<dt><strong>Branch Name</strong></dt> 
										<dd><?php echo $wallet_transaction_bank_table_arr['branch_name'] ?></dd>
										<dt><strong>City</strong></dt> 
										<dd><?php echo $wallet_transaction_bank_table_arr['city'] ?></dd>
										<dt><strong>IFSC Code</strong></dt> 
										<dd><?php echo $wallet_transaction_bank_table_arr['ifsc_code'] ?></dd>
										<dt><strong>Account Number</strong></dt> 
										<dd><?php echo $wallet_transaction_bank_table_arr['account_number'] ?></dd>
										<dt><strong>Account Holder Name</strong></dt> 
										<dd><?php echo $wallet_transaction_bank_table_arr['account_holder_name'] ?></dd>
										<dt><strong>Mobile Number</strong></dt> 
										<dd><?php echo $wallet_transaction_bank_table_arr['mobile_number'] ?></dd>
										
										<dt><strong>Requested Amount</strong></dt> 
										<dd><?php echo curr_sym.''.$wallet_transaction_bank_table_arr['transaction_amount'] ?></dd>
									
									</dl>
									</div>
									</div>
									<?php } ?>
							</div>
						</div>
					</div>
				</div>                	
                	
                	
	                	
           </div>
		   </div>
		   </div>
						<?php 
						$i++;
						} ?>
				
				</div><!--common--->
				<?php
					
					if($i>3){
						$style='';
					}else{
						$style='style="display:none;"';
					}

				?>
				
				<div class="f-row margin-top" id="pagination_div" <?php echo $style; ?>>
					<div class="sortPagiBar">
					<div class="bottom-pagination">
						<span class="nav_pag"></span>
					</div>
					</div>
				</div>
				
				
				
                <?php
					if(!empty($customer_wallet_transaction_bank_details)){
				?>
				
		<p class="lead">Your bank details seems to be wrong. Please click on this link <a href="<?php echo base_url()?>Account/update_bank_details_wallet_bank/<?php echo $customer_wallet_transaction_bank_details['id'];?>"><input type="button" value="Update Bank Details"></a>.</p>
			
			<?php } ?>
			
                <?php
					$wallet_transaction_amount=0;
					$wallet_trans_id_arr=array();
					foreach($wallet_transaction as $transaction_arr){
						if($transaction_arr["credit_bank_transfer"]==1 && $transaction_arr["claim_status"]==0){
							$wallet_transaction_amount+=intval($transaction_arr["credit"]);
							$wallet_trans_id_arr[]=$transaction_arr["id"];
						}
					}
					//echo $wallet_transaction_amount;
					if($wallet_transaction_amount!=0){
						$wallet_trans_id_in=implode(",",$wallet_trans_id_arr);
				?>
				<div class="row">
                <div class="col-sm-12">
				<div class="well">
					<div class="panel panel-info">
						<div class="panel-heading" >
							<h2 class="panel-title text-center">
								Add Bank Account To Get Refund 
							</h2>
							
						</div>
							<div class="panel-body ">
<?php if(!empty($bank_detail)){ ?>	
	

<script>
	function use_this_account_data(obj){
		var bank_name=document.getElementById('bank_name_'+obj).innerHTML
		var branch_name=document.getElementById('branch_name_'+obj).innerHTML
		var city=document.getElementById('city_'+obj).innerHTML
		var ifsc=document.getElementById('ifsc_'+obj).innerHTML
		var account_number=document.getElementById('account_number_'+obj).innerHTML
		var account_holder_name=document.getElementById('account_holder_name_'+obj).innerHTML
		var account_holder_numer=document.getElementById('account_holder_numer_'+obj).innerHTML
		
		document.getElementById('bank_name').value=bank_name;
		document.getElementById('bank_branch_name').value=branch_name;
		document.getElementById('bank_city').value=city;
		document.getElementById('bank_ifsc_code').value=ifsc;
		document.getElementById('bank_account_number').value=account_number;
		document.getElementById('bank_account_confirm_number').value=account_number;
		document.getElementById('account_holder_name').value=account_holder_name;
		document.getElementById('bank_return_refund_phone_number').value=account_holder_numer;
		document.getElementById('bank_name').setAttribute('readOnly',true);
		document.getElementById('bank_branch_name').setAttribute('readOnly',true);
		document.getElementById('bank_city').setAttribute('readOnly',true);
		document.getElementById('bank_ifsc_code').setAttribute('readOnly',true);
		document.getElementById('bank_account_number').setAttribute('readOnly',true);
		document.getElementById('bank_account_confirm_number').setAttribute('readOnly',true);
		document.getElementById('account_holder_name').setAttribute('readOnly',true);
		document.getElementById('bank_return_refund_phone_number').setAttribute('readOnly',true);
		document.getElementById('save_this_bank_data').disabled=true;
		$('#all_bank_accounts').modal('hide');
		$('#add_new_bank_details_button').show();
		
	}
	function add_new_bank_data(){
		document.getElementById('bank_name').value="";
		document.getElementById('bank_branch_name').value="";
		document.getElementById('bank_city').value="";
		document.getElementById('bank_ifsc_code').value="";
		document.getElementById('bank_account_number').value="";
		document.getElementById('bank_account_confirm_number').value="";
		document.getElementById('account_holder_name').value="";
		document.getElementById('bank_return_refund_phone_number').value="";
		document.getElementById('bank_name').readOnly=false;
		document.getElementById('bank_branch_name').readOnly=false;
		document.getElementById('bank_city').readOnly=false;
		document.getElementById('bank_ifsc_code').readOnly=false;
		document.getElementById('bank_account_number').readOnly=false;
		document.getElementById('bank_account_confirm_number').readOnly=false;
		document.getElementById('account_holder_name').readOnly=false;
		document.getElementById('bank_return_refund_phone_number').readOnly=false;
		det_count=$('#saved_bank_details').val().trim();
		if(det_count<3){
			document.getElementById('save_this_bank_data').disabled=false;
		}
		
	}
</script>			
	
<?php }
?>								
								<p class="lead text-center">
								<?php
									
									$wallet_transaction_amount=0;
									foreach($wallet_transaction as $transaction_arr){
										if($transaction_arr["credit_bank_transfer"]==1 && $transaction_arr["claim_status"]==0){
											$wallet_transaction_amount+=$transaction_arr["credit"];
										}
									}
									?>
									
								<?php
									echo "Eligible money for bank transfer: ".curr_sym.$wallet_transaction_amount;
								?>	
								</p>
								
								<?php
								if(count($bank_detail)!=0){
									?>
								<p class="lead text-center">
								<button type="button"  data-toggle="modal" href="#all_bank_accounts" data-backdrop="static" class="button"> Use Previously Added Bank Account</button> <button type="button" onclick="add_new_bank_data()" id="add_new_bank_details_button" class="button" style="display:none"> Reset</button>
								</p>


								<?php
								}
									?>
								
<div class="modal" id="all_bank_accounts">
     <div class="modal-dialog modal-lg">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title"> Your Total Bank Account With Us : 
				<?php 
				
					if(!empty($bank_detail)){
						echo count($bank_detail);
					}	
					else{
						echo "0";
					}
					?>
			  </h4>
           </div>
           <div  class="modal-body">
						<div class="row">
						

<?php 
$i=0; foreach($bank_detail as $bank_data){ 
	$bank_data=$bank_data[0];
	?>
						
						<div class="col-sm-4">
						<div class="well">
						<dl>
						
										<dt><strong>Bank Name</strong></dt> 
										<dd id="bank_name_<?php echo $i ?>"><?php echo $bank_data['bank_name']?></dd>
										<dt><strong>Branch Name</strong></dt> 
										<dd id="branch_name_<?php echo $i ?>"><?php echo $bank_data['branch_name']?></dd>
										<dt><strong>City</strong></dt> 
										<dd id="city_<?php echo $i ?>"><?php echo $bank_data['city']?></dd>
										<dt><strong>IFSC Code</strong></dt> 
										<dd id="ifsc_<?php echo $i ?>"><?php echo $bank_data['ifsc_code']?></dd>
										<dt><strong>Account Number</strong></dt> 
										<dd id="account_number_<?php echo $i ?>"><?php echo $bank_data['account_number']?></dd>
										<dt><strong>Account Holder Name</strong></dt> 
										<dd id="account_holder_name_<?php echo $i ?>"><?php echo $bank_data['account_holder_name']?></dd>
										<dt><strong>Mobile Number</strong></dt> 
										<dd id="account_holder_numer_<?php echo $i ?>"><?php echo $bank_data['mobile_number']?></dd>
									
									</dl>
							
							
								<button type="button" class="btn btn-success btn-block preventDflt" onclick="use_this_account_data(<?php echo $i ?>)">USE THIS</button>
							

					</div>
					</div>
	<?php $i++;} ?>
	

						
					</div>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close"class="btn">Close</a>
        </div>
     </div>
    </div>
  </div>

<script>
function ValidateNo() {
        var phoneNo = document.getElementById('bank_return_refund_phone_number');

    if (phoneNo.value == "" || phoneNo.value == null) {
	
            //alert("Please enter your Mobile No.");
			phoneNo.value="";
			phoneNo.focus();
            return false;
        }
        else if (phoneNo.value.length < 10 || phoneNo.value.length > 10) {
		
            return false;
        }else{
		
		
		return true;
		}

        }
		
		
		
		
function isAlfa(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
        return false;
    }
    return true;
}
function isNumber(evt) {

    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
	
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
	
    return true;
}
function submitForm(){
	
      if(document.myForm.bank_name.value == ""){
	  
        //alert("Please enter some text first");
	
		myForm.bank_name.focus();
		return false;
      }else if(document.myForm.bank_branch_name.value == ""){
	  
        //alert("Please enter some text first");

		myForm.bank_branch_name.focus();
		return false;
      }else if(document.myForm.bank_city.value == ""){
	  
        //alert("Please enter some text first");
	
		myForm.bank_city.focus();
		return false;
	  }
	  else if(document.myForm.bank_ifsc_code.value.length!=6){
		
		myForm.bank_ifsc_code.focus();
		return false;
	  }
	  else if(document.myForm.bank_account_number.value.length!=15){
	       
		myForm.bank_account_number.focus();
		return false;
	  }
	  else if((document.myForm.bank_account_number.value)!=(document.myForm.bank_account_confirm_number.value)){
	        
		myForm.bank_account_confirm_number.focus();
		return false;
	  }
	  
	  else if(document.myForm.account_holder_name.value == ""){
	  
        //alert("Please enter some text first");
	
		myForm.account_holder_name.focus();
		return false;
	  }
	  else if(document.myForm.bank_return_refund_phone_number.value.length !=10){
	  
        //alert("Please enter some text first");

		myForm.bank_return_refund_phone_number.focus();
		return false;
	  
	  }else{
		//document.myForm.submit();
		$("#request_refund_btn").html('<i class="fa fa-spinner"></i> Processing ...');
		document.getElementById("request_refund_btn").disabled=true;
		$.ajax({
			url:'<?php echo base_url(); ?>Account/wallet_transfer_bank_processing',
			//url:'<?php echo base_url(); ?>Account/update_bank_details_for_wallet',
			data:$("#bank_transfer_form").serialize(),
			type:"POST",
			success:function(data){
				$("#request_refund_btn").html('Request To Bank Transfer');
				document.getElementById("request_refund_btn").disabled=false;
				if(data=="success"){
					/*bootbox.alert({
					  size: "small",
					  message: 'Submited successfully',
					  callback: function () { location.reload(); }
					});		*/
                    alert('Submited successfully');
                    location.reload();
				}else{
					/*bootbox.alert({
					  size: "small",
					  message: 'Sorry..! somthing went wrong',
					  callback: function () { location.reload(); }
					});*/
                    alert('Sorry..! somthing went wrong');
                    location.reload();
				}
				//location.reload();
			}
		});
   }
      
    }
	
	function validate_ifsc(){
		var value = document.getElementById('bank_ifsc_code').value;
		if (value.length != 6) {
			document.getElementById("errors").style.display = "block";
			
		}else if (value.length == 6){
	 
		document.getElementById("errors").style.display = "none";
		}
	}
	
	function ValidateNumber(obj) {
		var value = document.getElementById('bank_return_refund_phone_number').value;
		if (value.length != 10) {
			document.getElementById("errors3").style.display = "block";
			
		}else if (value.length == 10){
	 
		document.getElementById("errors3").style.display = "none";
		}
		val=obj.value;
		/*if(val!=""){
			if(val=="+91 | "){ obj.value="";}
			else if(val.indexOf("+91") !== -1){}
			else{ obj.value="+91 | "+val;}
		}*/
	}

	function validate_account(){
		 
		var value = document.getElementById('bank_account_number').value;
		if (value.length != 15) {
			document.getElementById("errors1").style.display = "block";
			
		}else if (value.length == 15){
	 
		document.getElementById("errors1").style.display = "none";
		}
	}
	
	function validate_confirm(){
		var value = document.getElementById('bank_account_confirm_number').value;
		if ((document.myForm.bank_account_number.value)!=(document.myForm.bank_account_confirm_number.value)) {
			document.getElementById("errors2").style.display = "block";
			
		}else if ((document.myForm.bank_account_number.value)==(document.myForm.bank_account_confirm_number.value)){
	 
		document.getElementById("errors2").style.display = "none";
		}
	}
	
	
</script>
								

							
						
						<form name="myForm" class="form-horizontal" id="bank_transfer_form">
						
							<input type="hidden" name="wallet_trans_id_in" value="<?php echo $wallet_trans_id_in; ?>"/>
							<input type="hidden" name="transaction_amount" value="<?php echo $wallet_transaction_amount; ?>"/>
							<input type="hidden" name="customer_id" value="<?php echo $customer_id; ?>"/>
							<input type="hidden" name="wallet_id" value="<?php echo $wallet_summary->wallet_id; ?>"/>

									<div class="form-group">
									<label class="col-md-4 control-label">Bank Name</label>
									  <div class="col-md-5">
									  <input id="bank_name" name="bank_name" type="text" placeholder="Bank Name"  class="form-control"> 
									  <span class="help-block"></span>  
									  
									  </div>
									</div>
								
									<div class="form-group">
									   <label class="col-md-4 control-label">Bank Branch Name</label> 
									  <div class="col-md-5">
									  <input id="bank_branch_name" name="bank_branch_name" type="text" placeholder="Bank Branch Name"  class="form-control"> 
									  <span class="help-block"></span>   
									  </div>
									</div>
									
								
								
									<div class="form-group">
									   <label class="col-md-4 control-label">Branch City</label>
									  <div class="col-md-5">
									  <input id="bank_city" name="bank_city" type="text" placeholder="Branch City"  class="form-control"> 
									  <span class="help-block"></span>   
									  </div>
									</div>
									
								
									<div class="form-group">
									 <label class="col-md-4 control-label">IFSC code</label>
									  <div class="col-md-5">
									 
									  <input id="bank_ifsc_code" onclick="validate_ifsc()" onkeyup="validate_ifsc()" name="bank_ifsc_code" type="text" placeholder="IFSC code"  class="form-control"> 
									  <span id="errors">IFSC code must be 6 numbers</span>
									 
									  <span class="help-block"></span> 
									  
									  </div>
									</div>

								
									<div class="form-group">
									  <label class="col-md-4 control-label">Bank Account Number</label> 
									  <div class="col-md-5">
									  <input id="bank_account_number"  onfocus="validate_account()" onkeyup="validate_account()" name="bank_account_number" type="text" placeholder="Bank Account Number"  class="form-control">
									   <span id="errors1">Bank account number must be 15 numbers</span>	
									  <span class="help-block"></span> 
																		  
									  </div>
									</div>
									
								
								
									<div class="form-group">
									   <label class="col-md-4 control-label">Confirm Bank Account Number</label>  
									  <div class="col-md-5">
									  <input id="bank_account_confirm_number" onfocus="validate_confirm()" onkeyup="validate_confirm()" name="bank_account_confirm_number" type="text" placeholder="Confirm Bank Account Number"  class="form-control">
									   <span id="errors2">Bank account number must be same</span>	
									  <span class="help-block"></span>  
									  </div>
									</div>
								
								
									<div class="form-group">
									<label class="col-md-4 control-label">Account holder Name</label>
									  <div class="col-md-5">
									  <input id="account_holder_name" onkeypress="return isAlfa(event)" name="account_holder_name" type="text" placeholder="Bank Account holder Name"  class="form-control">
									  <span class="help-block"></span>  
									  </div>
									</div>
								

			
									<div class="form-group">
									 <label class="col-md-4 control-label">Mobile Number</label>
									  <div class="col-md-5">
									  <input id="bank_return_refund_phone_number" onkeyup="ValidateNumber(this)"  onkeypress="return isNumber(event)" name="bank_return_refund_phone_number" type="text"  placeholder="Registered Mobile Number" class="form-control" maxlength="10">
									 <span id="errors3">Enter 10digit mobile number</span>
									 
									  
									  <span class="help-block"></span>  
									  </div>
									</div>

		
									<div class="form-group">
									  <div class="col-md-5 col-md-offset-4">
									  <label class="form-check-label">
									  
									  <?php
										$disabled='';
										$saved_bank_details=0;
										if(!empty($bank_detail[0])){
											$saved_bank_details=count($bank_detail[0]);									  
											if($saved_bank_details>=3){
												$disabled='disabled';
											}
										}
									  ?>
									  <input type="checkbox" name="save_this_bank_data" <?php echo $disabled; ?> id="save_this_bank_data" checked value="1">
									  <input type="hidden" name="saved_bank_details" id="saved_bank_details"  value="<?php echo $saved_bank_details; ?>">
									  Save for later use
									</label>
									 
									  </div>
									</div>	
									

								
									<div class="form-group" >
									<div class="col-md-5 col-md-offset-4">
										<button type="button" id="request_refund_btn" onclick="submitForm()" class="btn btn-primary btn-block preventDflt">Request To Bank Transfer</button>
									</div>
									</div>
																
						</form>
								
								
								
							</div>

					</div>
             	
                	
                	
	                	
           </div>
		   </div>
		   </div>
		    <?php } ?>
               
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
	</div>
<script type="text/javascript">
	jQuery(document).ready(function($){	
		$('.common').paginathing({
	    perPage:3,
	    insertAfter: '.nav_pag'
		});
	});
</script>