<?php 


$total_promo_discount_price=0;$promo_dis1=0;

$order_cancelled_by_admin_or_not=$controller->get_order_cancelled_by_admin_or_not($order_item['order_item_id']);	
		if($order_cancelled_by_admin_or_not){ ?>
		<tr class="margin-top margin-bottom">
		<td colspan="5">
		
<div class="panel-group" id="accordion_order_cancel_<?php echo $order_item["order_item_id"]?>">
        <div class="panel panel-default">
            <div class="panel-heading text-uppercase">
                    <a data-toggle="collapse" data-parent="#accordion_order_cancel_<?php echo $order_item["order_item_id"]?>" href="#collapseOne_order_cancel_<?php echo $order_item["order_item_id"]?>">
					 Order has been Cancelled
					</a>
               
            </div>
            <div id="collapseOne_order_cancel_<?php echo $order_item["order_item_id"]?>" class="panel-collapse">
                <div class="panel-body">
				<div class="row">
				<div class="col-md-4">
				<dl class="dl-horizontal">
				  <dt>Number of Qty</dt>
				  <dd class="text-left"><?php echo $order_item["quantity"];?></dd>
				  <dt>Refund Method</dt>
				  <dd class="text-left">Not updated</dd>
				  <?php if(!empty($cancellation_details_from_admin)){ ?>
			<?php $cancellation_reason_details_from_admin= $controller->get_cancellation_reason_details_from_admin($cancellation_details_from_admin[0]["reason_cancel_id"])?>
				  <dt>Cancel Reason</dt>
				  <dd class="text-left"><?php echo $cancellation_reason_details_from_admin->cancel_reason; ?></dd>
				  <dt>Cancel Comments</dt>
				  <dd class="text-left more"><?php echo $cancellation_details_from_admin[0]["cancel_reason_comments"]; ?></dd>
				  <?php } ?>
				</dl>
			</div>
			<div class="col-md-2 products-block">
				<?php if($orders_status_data["type_of_order"]==""){ ?>	
										<?php if($promotion_invoice_free_shipping>0){
											$subtotal=$order_item['subtotal']-$order_item_invoice_discount_value;
											echo curr_sym.$subtotal;
										}else{
											$grandtotal=$order_item['grandtotal']-$order_item_invoice_discount_value;
											echo curr_sym.$grandtotal;
										}
										?>
										<a class="cursor-pointer" data-trigger="click" data-placement="bottom" data-toggle="popover" data-container="body" type="button" data-html="true" id="price_details_cancel_<?php echo $order_item["order_item_id"]?>" content_id="#content_price_details_cancel_<?php echo $order_item["order_item_id"]; ?>" onclick="toggle_content(this)">
										
										[<i class="fa fa-question" aria-hidden="true"></i>] </a>
										
							<div id="content_price_details_cancel_<?php echo $order_item["order_item_id"]?>" style="display:none;">
							<table class="table table-bordered" style="font-size: .8em;text-align:justify;margin-bottom: 0px;">
                                                    <thead><tr><th style="padding:0px"></th><th width="25%" style="padding:0px"></th><th style="padding:0px" width="25%" style="text-align: right"></th></tr></thead>
                                            
<?php
											
													$total_product_price_normal=($order_item["quantity"]*round($order_item["ord_max_selling_price"]));
													$shipping_price_normal=$order_item["shipping_charge"];
													

													$total_amount_normal=($shipping_price_normal+$total_product_price_normal);
													
	
	?>
													
													<tr><td>Base product price</td><td><?php echo $order_item['quantity']; ?> x <?php echo $order_item['ord_max_selling_price']?></td><td class="text-right"><?php echo curr_sym;?><?php echo $total_product_price_normal; ?></td></tr>
													
													<tr><td colspan="2">Shipping Charge for <?php echo $order_item['quantity']; ?> unit(s)</td><td class="text-right"><?php echo curr_sym;?><?php echo $shipping_price_normal; ?></td></tr>
									
													<tr><td colspan="2">Total value </td><td class="text-right"><?php echo curr_sym;?><?php echo $total_amount_normal; ?></td></tr>
                                       <?php
											if($promotion_invoice_discount>0){
												?>
												<tr><td colspan="2">Discount price for <?php echo $order_item['quantity']; ?> unit(s)</td><td class="text-right" style="color:red;"><?php echo curr_sym;?><?php echo $order_item_invoice_discount_value; ?></td></tr>
												<?php 
											}
										?>
										
                                        <?php if($order_item['promotion_available']==1){ ?>   
										
                                                <?php if(intval($order_item['quantity_without_promotion'])>0 && $order_item['default_discount']>0){ ?>
												
												<?php
												
												$default_discount_price=floatval($order_item['ord_max_selling_price']-$order_item['individual_price_of_product_without_promotion']);
		// $default_discount_price=(($order_item['product_price']*$order_item['default_discount'])/100);
		$total_default_discount_price=($order_item['quantity_without_promotion']*$default_discount_price);
		$total_item_price_with_base_price=($order_item['product_price']*$order_item['quantity_without_promotion']);
		
												?>
                                                <tr><td> Discount @ (<?php echo $order_item['default_discount'] ?>%)<br>Eligible Quantity : <?php echo $order_item['quantity_without_promotion'] ?><br>For promotion <br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_default_discount_promo']; ?></small></td><td><?php echo $order_item['quantity_without_promotion'] ?> <i class="fa fa-times" aria-hidden="true"></i> <?php echo  $default_discount_price ?></td><td class="text-right" style="color:red"> <?php echo curr_sym;?><?php echo $total_default_discount_price;?> </td></tr>
												
												
                                                <?php }?>
                                                
                                                <?php if(intval($order_item['quantity_with_promotion'])>0){ ?>
                                                 
                                                 <?php if(intval($order_item['promotion_discount'])>0&& (intval($order_item['promotion_discount'])==intval($order_item['default_discount']))){?>
												 
												 <?php
												 
												$default_discount_price=floatval($order_item['ord_max_selling_price']-$order_item['individual_price_of_product_with_promotion']);
		// $default_discount_price=(($order_item['product_price']*$order_item['promotion_discount'])/100);
		$total_default_discount_price=($order_item['quantity_with_promotion']*$default_discount_price);
		$total_item_price_with_base_price=($order_item['product_price']*$order_item['quantity_with_promotion']);
		
												 ?>
                                                     <tr><td> Discount @ (<?php echo $order_item['promotion_discount'] ?>%) <br>Eligible Quantity : <?php echo $order_item['quantity_with_promotion'] ?><br>For promotion<br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_quote']; ?></small></td><td><?php echo $order_item['quantity_with_promotion'] ?> <i class="fa fa-times" aria-hidden="true"></i> <?php echo  $default_discount_price ?></td><td class="text-right" style="color:red"> <?php echo curr_sym;?><?php echo $total_default_discount_price;?> </td></tr>
													 
                                                 <?php }?>
                                                 
                                                 <?php if(intval($order_item['promotion_discount'])>0 && (intval($order_item['promotion_discount'])!=intval($order_item['default_discount']))){?>
												 
												 <?php
												 
												 $promo_discount_price=floatval($order_item['ord_max_selling_price']-$order_item['individual_price_of_product_with_promotion']);
												
		//$promo_discount_price=(($order_item['product_price']*$order_item['promotion_discount'])/100);
		$total_promo_discount_price=($order_item['quantity_with_promotion']*$promo_discount_price);
		$total_item_price_with_base_price=($order_item['product_price']*$order_item['quantity_with_promotion']);
		
												 ?>
                                                      <tr><td>Promotion Discount @ (<?php echo $order_item['promotion_discount'] ?>%)<br>Eligible Quantity : <?php echo $order_item['quantity_with_promotion'] ?><br>For Promotion<br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_quote']; ?></small></td><td><?php echo $order_item['quantity_with_promotion'] ?> <i class="fa fa-times" aria-hidden="true"></i> <?php echo $promo_discount_price; ?></td><td class="text-right" style="color:red;"><?php echo curr_sym;?><?php echo $total_promo_discount_price;?> </td></tr>
                                                 <?php }?>   
                                                    
                                                  <?php if(intval($order_item['promotion_discount'])==0 && $order_item['promotion_default_discount_promo']=="Price for remaining"){?>
												  
													<?php if(intval($order_item['default_discount']>0)){ ?>
												  
                                                     <tr><td>Standard MRP @ (<?php echo $order_item['product_price'] ?>) <br>Eligible Quantity : <?php echo $order_item['quantity_with_promotion'] ?><br>For Promotion<br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_quote']; ?></small></td><td><?php echo $order_item['quantity_with_promotion'] ?> <i class="fa fa-times" aria-hidden="true"></i> <?php echo $order_item['product_price'] ?></td><td class="text-right"><?php echo curr_sym;?><?php echo $order_item["total_price_of_product_with_promotion"];?> </td></tr>
													 
												  <?php }else{
													  ?>
													   <tr><td colspan="3">For Promotion<br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_quote']; ?></small></td></tr>
													  <?php
												  } ?>
												  
                                                 <?php }?>  
                                                    
                                                
                                                <?php }?>
                                                
                                                <?php if(intval($order_item['quantity_without_promotion'])>0 && $order_item['default_discount']==0){ ?>
                                                <!-- no quote and no default promotion---->
<?php /*?>											   <tr><td>Standard MRP @ (<?php echo $order_item['product_price'] ?>) <br>Eligible Quantity : <?php echo $order_item['quantity_without_promotion'] ?><br></td><td><?php echo $order_item['quantity_without_promotion'] ?>*<?php echo $order_item['product_price'] ?></td><td class="text-right"><?php echo curr_sym;?><?php echo $order_item["total_price_of_product_without_promotion"];?> </td></tr>
<?php */?>
                                                <?php }?>
                                                
                                                
                                                <!---added --->
                                                 <?php
                                                 //&& $order_item['promotion_quote']==$order_item['promotion_default_discount_promo']
                                                 if($order_item['promotion_default_discount_promo']!="" && $order_item['default_discount']>0 ){
                                                     /* straight discount */
                                                     
                                                 }else{
                                                     if($order_item['ord_selling_discount']>0){
                                                     
                                                         ?>
                                                <tr><td colspan="2">Discount @ (<?php echo round($order_item['ord_selling_discount']) ?>%)<br>Eligible Quantity : <?php echo $order_item['quantity'] ?><br></td><td class="text-right"><?php echo curr_sym; ?> <?php echo ($order_item['quantity']*($order_item['ord_max_selling_price']-$order_item['product_price'])); ?></td></tr>
                                                        <?php 
                                                        $discount_saved=round($order_item['quantity']*($order_item['ord_max_selling_price']-$order_item['product_price']));
                                                         
                                                     }
                                                     
                                                 }
                                                 
                                                 
                                                 ?>
                                                 <!---added --->
                                                
                                              <?php }else { 
                                                
                                                     if($order_item['ord_selling_discount']>0){
                                                     
                                                         ?>
                                                <tr><td colspan="2">Discount @ (<?php echo round($order_item['ord_selling_discount']) ?>%)<br>Eligible Quantity : <?php echo $order_item['quantity'] ?><br></td><td class="text-right"><?php echo curr_sym; ?> <?php echo ($order_item['quantity']*($order_item['ord_max_selling_price']-$order_item['product_price'])); ?></td></tr>
                                                        <?php 
                                                        $discount_saved=round($order_item['quantity']*($order_item['ord_max_selling_price']-$order_item['product_price'])); 
                                                     }
                                                      
                                                 } ?>
                                                  
                                              
            							<?php
					if($promotion_item!='' || ($promotion_surprise_gift_skus!='' && $order_delivered==1)){
							if($promotion_item!=''){

								foreach($free_items_arr as $free_items){
									?>
									<tr><td><?php echo "SKU : ".$free_items->sku_id."(free)"; ?></td><td><?php $id=$free_items->id;
										echo "Quantity ".($temp_quotient*$two[$id]); ?></td><td><del><?php echo curr_sym.(($temp_quotient*$two[$id])*$free_items->selling_price); ?></del></td></tr>
									<?php
								}
							}

							if($promotion_surprise_gift_type=='Qty'){

								foreach($free_surprise_items as $surprise_items){
									?>
									<tr><td><?php echo "SKU : ".$surprise_items->sku_id."(free)"; ?></td><?php $id=$surprise_items->id;
										echo "Quantity ".($gift_with_count[$id]); ?><td><del><?php echo curr_sym.(($gift_with_count[$id])*$surprise_items->selling_price); ?></del></td></tr>
									<?php
								}
							}
					}
												?>
                                                                        
                                                                        
                                            <!----sku coupon--->
                                                <?php
                                                
                                                $sku_coupon_used=0;
                                                if($order_item['ord_coupon_status']=='1'){ 
                                                    
                                                    $sku_coupon_used=round($order_item['ord_coupon_reduce_price']);
                                                    ?>
                                                
                                                <tr><td colspan="2">Coupon applied on SKU <b>(<?php echo ($order_item['ord_coupon_type']=='2') ? curr_sym : ''; ?><?php echo round($order_item['ord_coupon_value']) ?><?php echo ($order_item['ord_coupon_type']=='1') ? '%' : 'Flat'; ?>)</b>
                                                        
                                                        </td><td class="text-right"><?php echo curr_sym; ?> <?php echo round($order_item['ord_coupon_reduce_price']); ?></td></tr>
                                                
                                                <?php } ?>
                                            <!----sku coupon--->
												
                                              <tr>
                                                <?php 
												
												if($promotion_invoice_free_shipping>0){   ?>
			
													<td>Shipping Charge 
													( <span style="color:red">waived off</span> )
													</td>
                                                <td><?php echo $order_item['quantity'] ?> units(s)</td>
                                                <td class="text-right" style="color:red">

                                                    <?php echo curr_sym;?><?php echo $order_item["shipping_charge"]; ?>
                                                
                                                </td>
													<?php
												}?>

  
                                                </tr>
                                                
                                                <?php
                                                if($order_item["cash_back_value"]!="" && $order_item["cash_back_value"]!=0){                                          ?>
                                               <tr>
                                                 
                                                <td>Cashback value of <?php echo $order_item['promotion_cashback'] ?> % <br><em>(credited to wallet) </em></td>
                                                <td><?php echo $order_item['quantity'] ?> <i class="fa fa-times" aria-hidden="true"></i>  <?php echo round($order_item["cash_back_value"]/$order_item['quantity'])?></td>
                                                <td class="text-right" ><?php echo curr_sym;?><?php echo $order_item["cash_back_value"]; ?></td>
                                                </tr>
                                                
                                                <?php 
												
												}
												?>

												<!----addon section--->

												<?php 


													if($order_item["ord_addon_products_status"]=='1'){
														$total_amount_normal+=round($order_item["ord_addon_total_price"]);
														$addon=json_decode($order_item["ord_addon_products"]);
														//print_r($addon);
														$addon_count=count($addon);
														?>
												<tr>
												<td>Additional products</td>
												<td><?php echo $addon_count.' items';?></td>
												<td class="text-right" ><?php echo curr_sym; ?><?php echo round($order_item["ord_addon_total_price"]); ?></td>
												</tr>
														<?php 
													}
												?>

												<!----addon section--->

						<!---minus--->
                                                <?php 
                                                $minustext='';
                                                
                                                if($discount_saved>0 || $sku_coupon_used){
                                                    $minustext='(';
                                                    $minustext.=curr_sym.$total_amount_normal;
                                                    if(intval($discount_saved)>0){
                                                        $minustext.='-'.curr_sym.$discount_saved;
                                                    }
                                                    if($total_promo_discount_price>0){
                                                        $minustext.='-'.curr_sym.$total_promo_discount_price;
                                                        $promo_dis1+=round($total_promo_discount_price);
                                                    }
                                                    if($sku_coupon_used>0){
                                                        $minustext.='-'.curr_sym.$sku_coupon_used;
                                                    }

													if($promotion_invoice_free_shipping>0){
														$minustext.='-'.curr_sym.$order_item["shipping_charge"];
													}
                                                    $minustext.=')'; 
                                                }
                                               
                                                ?>
                                                <!---minus--->
                                                
												<?php
												/*echo '<pre>';
												print_r($order_item);
												echo '</pre>';*/

												?>
                                                <?php if($promotion_invoice_free_shipping>0){ ?>
                                                  
												  <tr><td colspan="2"><em><b>Total amount to be refunded</b></em><?php echo $minustext; ?></td><td class="text-right"><b><?php echo curr_sym;?>.<?php echo ($order_item["subtotal"]-$order_item_invoice_discount_value);?></b></td></tr>
												  
                                                <?php }else{ ?>
                                                    <tr><td colspan="2"><em><b>Total amount to be refunded</b></em><?php echo $minustext; ?></td><td class="text-right"><b><?php echo curr_sym;?><?php echo ($order_item["grandtotal"]-$order_item_invoice_discount_value);?></b></td></tr>
                                               <?php } ?>
                                                

                                               
											<?php
											if($total_amount_normal>$order_item["grandtotal"]){
												
											$you_saved=($total_amount_normal-$order_item["grandtotal"]);
                                                                                        
											if($promotion_invoice_free_shipping>0){
												$you_saved+=$shipping_price_normal;
											}
                                                                                        if(intval($sku_coupon_used)>0){
                                                                                            $you_saved+=$sku_coupon_used;
                                                                                        }
											?>
											
											<tr><td colspan="2"><em><b>You Saved</b></em></td><td class="text-right" style="color:green"><b><?php echo curr_sym;?><?php echo $you_saved;?></b></td></tr>
											<?php
											
											}
											?>
											</table>
											</div>
							
                                       <?php } ?>

											
	<?php 
	
	if($orders_status_data["type_of_order"]==""){      
			if($order_item['promotion_available']==1){
				
            if($order_item['total_price_of_product_with_promotion']!="" || $order_item['total_price_of_product_without_promotion']!=""){
				
					$r=0;
					if($order_item['promotion_quote']!=""){
						$r=1;
					}	
					if($order_item['promotion_default_discount_promo']!="" && $order_item['default_discount']>0 && $order_item['promotion_quote']!=$order_item['promotion_default_discount_promo']){
										
						$r+=1;
					}	
				
				?>                         		
			
							<a class="cursor-pointer" data-trigger="click" data-placement="bottom" data-toggle="popover" data-container="body" type="button" data-html="true" id="price_details_offer_<?php echo $order_item["order_item_id"];?>" content_id="#content_price_details_offer_<?php echo $order_item["order_item_id"]; ?>" onclick="toggle_content(this)">OFFER : <?php echo $r; ?></a>
							
							
							<div id="content_price_details_offer_<?php echo $order_item["order_item_id"]?>" style="display:none;">
							<table class="table table-bordered " style="font-size:0.8em;text-align:justify;margin-bottom: 0px;">
                                                <?php
$r=0;
												if($order_item['promotion_quote']!=""){
													$r=1;
													?>
                                                    <tr class="success"><td><?php echo $r.":"; ?></td><td><?php echo $order_item['promotion_quote'] ?></td></tr>
                                               <?php }?>
                                                <?php if($order_item['promotion_default_discount_promo']!="" && $order_item['default_discount']!=0){
$r+=1;
												?>
                                                   <tr  class="success"><td><?php echo $r.":"; ?></td><td><?php echo $order_item['promotion_default_discount_promo'] ?></td></tr>
                                               <?php }?>
                                                </table>
							</div>
											
											
											
											
											
											
											
											<?php
			}
			}else{
                            ?>

                                <a data-trigger="click" data-placement="bottom" data-toggle="price_details2" data-html="true" id="price_details_3_<?php echo $order_item_id; ?>" class="price_details_2 cursor-pointer" content_id="#content_price_details_3_<?php echo $order_item_id; ?>" onclick="toggle_content(this)">

                                OFFERS : 1 </a>

                                <div id="content_price_details_3_<?php echo $order_item_id; ?>" style="display:none;">
                                    <table class="table table-bordered" style="font-size: .8em;text-align:justify;margin-bottom: 0px;">

                                    <tr  class="success"><td colspan="2">Discount <?php echo round($order_item['ord_selling_discount']); ?> % Off </td></tr>

                                    </table>
                                </div>                                                                                
                            <?php
                        }
	}

			?>
		</div>
									
<!-----promo_popover2------------------>
			
			

						<div class="col-md-6">
						

						<?php 				
						$cancelled_order_customer_payment_type=$controller->get_cancelled_order_customer_payment_type($order_item['order_item_id']);
						
						if(strtolower($cancelled_order_customer_payment_type)!='cod'){
						
														?>
						<p>As Admin cancelled the order, it is required you to choose an refund option.</p>
														
						<form name="myForm" id="refund_method_form_cancel_<?php echo $order_item["order_item_id"];?>" method="post" class="form-horizontal">
						<input type="hidden" name="product_id_for_refund" id="product_id_for_refund" value="<?php echo $order_item['product_id'];?>">
						<input type="hidden" name="sku_id_for_refund" id="sku_id_for_refund" value="<?php echo $order_item['sku_id'];?>">
						<input type="hidden" name="customer_id_for_refund" id="customer_id_for_refund" value="<?php echo $order_item['customer_id'];?>">
						<input type="hidden" name="order_item_id_for_refund" id="order_item_id_for_refund" value="<?php echo $order_item['order_item_id'];?>">
						<input type="hidden" name="product_price_for_refund" id="product_price_for_refund" value="<?php echo $order_item['product_price'];?>">
						<input type="hidden" name="grandtotal_for_refund" id="grandtotal_for_refund" value="<?php echo $order_item['grandtotal'];?>">
						<input type="hidden" name="quantity_for_refund" id="quantity_for_refund" value="<?php echo $order_item['quantity'];?>">

							<div class="form-group">
							<label class="control-label col-md-4">Choose Refund Method</label>
								<div class="col-md-8">
							<label class="checkbox-inline">
							  	<input type="radio" id="wallet_radio_cancel_<?php echo $order_item["order_item_id"];?>"  name="refund_method_cancel_<?php echo $order_item["order_item_id"];?>" required onchange="setRefundMethod('<?php echo $order_item_id ?>')" value="Wallet"> Wallet  
							</label>
							<label class="checkbox-inline">
							  <input type="radio" name="refund_method_cancel_<?php echo $order_item["order_item_id"];?>" required onchange="setRefundMethod('<?php echo $order_item_id ?>')" value="Bank Transfer"> Bank Transfer
							</label>
							</div>
							</div>						

						
						<div id="bank_account_data_cancel_<?php echo $order_item["order_item_id"];?>" style="display:none;">
						<?php
								if(count($bank_detail)!=0){
									?>
						<div class="form-group">
								<div class="col-md-8 col-md-offset-4">
								<button type="button"  data-toggle="modal" href="#all_bank_accounts" data-backdrop="static" class="button preventDflt"> Use Saved Bank Account</button> <button type="button" onclick="add_new_bank_data('<?php echo $order_item_id; ?>')" id="add_new_bank_details_button_<?php echo $order_item["order_item_id"];?>" class="button preventDflt" style="display:none"> Reset</button>
								</div>
								</div>
								<?php
								}
									?>
	<div class="modal" id="all_bank_accounts">
     <div class="modal-dialog modal-lg">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title"> Your Total Bank Account With Us : 
				<?php 
				
					if(!empty($bank_detail[0])){
						echo count($bank_detail[0]);
					}	
					else{
						echo "0";
					}
					?>
			  </h4>
           </div>
           <div  class="modal-body">
						<div class="row">
						

<?php 
$i=0; foreach($bank_detail[0] as $bank_data){ ?>
						<div class="col-sm-4">
							<div class="well">
					<dl>
					<dt><strong>Bank Name</strong></dt> 
					<dd id="bank_name_cancel_<?php echo $i ?>" class="text-left"><?php echo $bank_data['bank_name']?></dd>
					<dt><strong>Branch Name</strong></dt> 
					<dd id="bank_branch_name_cancel_<?php echo $i ?>" class="text-left"><?php echo $bank_data['branch_name']?></dd>
					<dt><strong>City</strong></dt> 
					<dd id="bank_city_cancel_<?php echo $i ?>" class="text-left"><?php echo $bank_data['city']?></dd>
					<dt><strong>IFSC Code</strong></dt> 
					<dd id="bank_ifsc_code_cancel_<?php echo $i ?>" class="text-left"><?php echo $bank_data['ifsc_code']?></dd>
					<dt><strong>Account Number</strong></dt> 
					<dd id="bank_account_number_cancel_<?php echo $i ?>" class="text-left"><?php echo $bank_data['account_number']?></dd>
					<dt><strong>Account Holder Name</strong></dt> 
					<dd id="account_holder_name_cancel_<?php echo $i ?>" class="text-left"><?php echo $bank_data['account_holder_name']?></dd>
					<dt><strong>Mobile Number</strong></dt> 
					<dd id="bank_return_refund_phone_number_cancel_<?php echo $i ?>" class="text-left"><?php echo $bank_data['mobile_number']?></dd>
					</dl>
							
							
								<button type="button" class="button btn-block preventDflt" onclick="use_this_account_data(<?php echo $i ?>,'<?php echo $order_item_id; ?>')">USE THIS</button>
						</div>
					</div>
	<?php $i++;} ?>
	

						
					</div>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close"class="btn">Close</a>
        </div>
     </div>
    </div>
  </div>


								
							
								<div class="form-group">
								<label class="control-label col-md-4">Bank name</label> 
								 <div class="col-md-8">
								  <input id="bank_name_cancel_<?php echo $order_item["order_item_id"];?>" name="bank_name" type="text" placeholder="Enter Bank Name" class="form-control"> 
								  <span class="help-block"></span>   
								</div>
								</div>	
						
							
								<div class="form-group">
								<label class="control-label col-md-4">Branch name</label>
								 <div class="col-md-8">
								  <input id="bank_branch_name_cancel_<?php echo $order_item["order_item_id"];?>" name="bank_branch_name" type="text" placeholder="Enter Branch Name" class="form-control"> 
								  <span class="help-block"></span>   
								</div> 
								</div>
			
								<div class="form-group">
								<label class="control-label col-md-4">Branch city</label>
								 <div class="col-md-8">
								  <input id="bank_city_cancel_<?php echo $order_item["order_item_id"];?>" name="bank_city" type="text" placeholder="Enter City" class="form-control"> 
								  <span class="help-block"></span>   
								</div> 
								</div>

								<div class="form-group">
								<label class="control-label col-md-4">IFSC Code</label>								
								 <div class="col-md-8">
								  <input id="bank_ifsc_code_cancel_<?php echo $order_item["order_item_id"];?>" name="bank_ifsc_code" type="text" onclick="validate_ifsc('<?php echo $order_item_id; ?>')" onkeyup="validate_ifsc('<?php echo $order_item_id; ?>')" placeholder="Enter IFSC code" class="form-control">
								<span id="errors">IFSC code must be 6 numbers</span>								  
								  <span class="help-block"></span>   
								</div>
								</div>
	
								<div class="form-group">
								<label class="control-label col-md-4">Account Number</label>  
								 <div class="col-md-8">
								  <input id="bank_account_number_cancel_<?php echo $order_item["order_item_id"];?>" name="bank_account_number" onfocus="validate_account('<?php echo $order_item_id; ?>')" onkeyup="validate_account('<?php echo $order_item_id; ?>')" type="text" placeholder="Your Account Number" class="form-control">
								   <span id="errors1">Bank account number must be 15 numbers</span>
								  <span class="help-block"></span>  
								</div>
								</div>					
							
								<div class="form-group">
								<label class="control-label col-md-4">Confirm Number</label>
								 <div class="col-md-8">
								  <input id="bank_account_confirm_number_cancel_<?php echo $order_item["order_item_id"];?>" onfocus="validate_confirm('<?php echo $order_item_id; ?>')" onkeyup="validate_confirm('<?php echo $order_item_id; ?>')" name="bank_account_confirm_number" type="text" placeholder="Confirm Account Number" class="form-control">
								  <span id="errors2">Bank account number must be same</span>
								  <span class="help-block"></span>  
								</div>
								</div>
							
							
								<div class="form-group">
								<label class="control-label col-md-4">Account Holder</label> 
								 <div class="col-md-8">
								  <input id="account_holder_name_cancel_<?php echo $order_item["order_item_id"];?>" name="account_holder_name" onkeypress="return isAlfa(event)" type="text" placeholder="Enter Bank Account holder Name" class="form-control">
								  <span class="help-block"></span>  
								</div>
								</div>		
		
								<div class="form-group">
								<label class="control-label col-md-4">Mobile number</label>
								   <div class="col-md-8">
								  <input id="bank_return_refund_phone_number_cancel_<?php echo $order_item["order_item_id"];?>" onkeyup="ValidateNumber(this,'<?php echo $order_item_id; ?>')"  onkeypress="return isNumber(event)" name="bank_return_refund_phone_number" type="text" placeholder="Enter Your Registered Mobile Number" class="form-control">
								  <span id="errors3" style="color:red;display:none;">Enter 10 digit mobile number</span>
								  <span class="help-block"></span>  
								</div>
								</div>
							
									
								<div class="form-group">	
									<?php
										$disabled='';
										$saved_bank_details=0;
										if(!empty($bank_detail[0])){

											$saved_bank_details=count($bank_detail[0]);									  
											if($saved_bank_details>=3){
												$disabled='disabled';
											}
										}
									  ?>
									   <div class="col-md-8 col-md-offset-4">
								  <div class="checkbox">
								    <label for="checkboxes-0">
										<input type="checkbox" name="save_this_bank_data" id="save_this_bank_data_<?php echo $order_item["order_item_id"];?>"  value="1" <?php echo $disabled; ?>>
										<input type="hidden" name="saved_bank_details" id="saved_bank_details_<?php echo $order_item["order_item_id"];?>"  value="<?php echo $saved_bank_details; ?>">
								      Save for later use
								    </label>
									</div>
								
								</div>	            			
							</div>
						</div>					
						
						<div class="form-group">
								<div class="col-sm-8 col-md-offset-4">					
							<div class="checkbox">
								<label for="checkboxes-1">
								  <input type="checkbox" required="" name="accept_terms_and_conditions"  value="1" required>
								  I agree to <a href="#">terms and conditions</a>
								</label>
							</div>
							</div>
						</div>
						
						<div class="form-group">
							 <div class="col-sm-8 col-md-offset-4">
							  <button type="button" class="button btn-block preventDflt" onclick="submit_refund_stuff(<?php echo $order_item["order_item_id"];?>)"> Submit</button>
							</div>
						</div>
						
						</form>	
					<?php }else{ ?>
						<p>As you opted for COD payment method, there will be no refund.</p>
					<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</td>
		</tr>
	<?php } ?>	