<script src="<?php echo base_url()?>assets/js/md5.js"></script>
<div class="columns-container">
    <div class="container" id="columns">
	<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account">My account</a></li>
		  <li class="breadcrumb-item active">Deactivate Account</li>
		</ol>
        <div class="row">
            <!-- Left colunm -->
			<?php require_once 'leftcolum.php';?>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
               <!-- page heading-->
                
                <!-- ../page heading-->
                
				<div class="col-sm-12">
				<div class="well">
					<h2>
                    <span class="page-heading-title2">Deactivate Account</span>
                    
                </h2><hr>
            			
						<p class="lead">Your wallet amount with us is <?php echo curr_sym; ?> <?php echo $wallet_amount ?> <span class="view-more pull-right" ><a target='_blank' href="<?php echo base_url(); ?>Account/wallet">View Details</a></span></p>
            			
            	</div>
				</div>
            	
            	<?php if($products_to_be_delivered >0 ) {?>
            	<div class="col-sm-12 ">
            			<div class="well">
						<b>Products to be delivered</b><hr>
						<p class="lead">We have 
							<?php 
							if($products_to_be_delivered >0 )
							{
								if($products_to_be_delivered ==1 ){
								echo $products_to_be_delivered."product to be delivered";
								}
								else{
									echo $products_to_be_delivered." products to be delivered";
								}
							}
							else{
								echo " no product to be delivered";
							} 
						?>
											
						<span class="view-more pull-right" ><a target='_blank' href="<?php echo base_url()?>Account/my_order">View Details</a></span></p>
							
									
	            				
						</div>
						
            	</div>
            	<?php }?>
            	<?php if($wallet_amount !=0 || $products_to_be_delivered !=0){ ?>
            		
            	<div class="col-sm-12">
            		<b>Account Cannot Be Deactivated</b><hr>
            			<div class="well">
            			<?php if($wallet_amount > 0){ ?>
            				<p>
							You have wallet amount <b><?php echo curr_sym; ?> <?php echo $wallet_amount ?> pending</b> with us. Please purchase some product or request for refund
							</p>
							<hr>
            			<?php  }?>
						<?php if($products_to_be_delivered >0 ){ ?>
							<p>
								We have <?php if($products_to_be_delivered >0 )
								{
									if($products_to_be_delivered ==1 ){
									echo "<b> ".$products_to_be_delivered." product </b> to be delivered";
									}
									else{
										echo "<b> ".$products_to_be_delivered." products </b> to be delivered";
									}
								}?> .So please wait until our products are delivered or cancel the order.
							</p>
							<hr>
						<?php }?>
						
            			</div>
            	</div>
					
            	<?php } else{ ?> 
            	<div class="col-sm-12">
				<div class="well">
            		<script>
						function deactivateFun(){
							var pwd=document.getElementById("pwd").value.trim();
							
							if(pwd==""){
								
								/*bootbox.alert({
								  size: "small",
								  message: 'fill the password!',
								  callback: function () { location.reload(); }
								});	*/

                                alert('fill the password!');
                                location.reload();

								return false;
							}
							else if(calcMD5(pwd)!='<?php echo $myprofile["password"]?>'){
								
								/*bootbox.alert({
								  size: "small",
								  message: 'Invalid password!',
								  callback: function () { location.reload(); }
								});*/
                                alert('Invalid password!');
                                location.reload();
								return false;
							}else{
                                $("#btn-deactivate").prop('disabled', true);
								$.ajax({
									url:"<?php echo base_url()?>admin/Manage_cust/de_activate_customer_acc/<?php echo $myprofile["id"] ?>",
									data:"1=2",
									type:"POST",
									success:function(data){
										//alert(data);return false;
										if(data==true){
											
											/*bootbox.alert({
											  size: "small",
											  message: 'Successfully Deactivated! Please login again to reactivate your account',
											  callback: function () { location.href="<?php echo base_url()."Loginsession/logout";?>"; }
											  
											});*/

                                            alert('Successfully Deactivated! Please login again to reactivate your account');
                                            location.href="<?php echo base_url()."Loginsession/logout";?>";
											
										}else{
                                            alert('Error');
                                            $("#btn-deactivate").prop('disabled', false);
                                            location.reload();
                                        }
									}
								});

							}
						}
					</script>
					<form class="form-horizontal" method="post">
					<div class="form-group">
						  <label class="control-label col-sm-2" for="email">Email</label>
						  <div class="col-sm-10">
							<p class="form-control-static"><?php echo $this->session->userdata('customer_email')?></p>
						  </div>
					</div>
					<div class="form-group">
						  <label class="control-label col-sm-2" for="email">Mobile</label>
						  <div class="col-sm-10">
							<p class="form-control-static"><?php echo $this->session->userdata('customer_mobile')?></p>
						  </div>
					</div>
						<div class="form-group">
						  <label class="control-label col-sm-2" for="pwd">Password</label>
						  <div class="col-sm-4">
							<input type="password" class="form-control" id="pwd" >
						  </div>
						</div><br>
						<div class="form-group">
						  <div class="col-sm-offset-2 col-sm-10">
							<button type="button" id="btn-deactivate" class="button preventDflt" onclick="deactivateFun()">Confirm Deactivation</button>
						  </div>
						</div>
            		</form>	
						
            	</div>
				</div>
            	<?php } ?>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

