<div class="columns-container">
    <div class="container" id="columns">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account">My account</a></li>
		  <li class="breadcrumb-item active">Addresses</li>
		</ol>
        <div class="row">
			<?php require_once 'leftcolum.php';?>
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
				<div class="panel panel-default">
			  <div class="panel-heading">Addresses</div>
			  <div class="panel-body">
			  <div class="col-sm-12">
				<div class="row">
						
<?php if(!empty($all_addresses)){
	$i=0;
	foreach($all_addresses as $address_data){ 
			
			if($i%3==0){
				echo '</div><div class="row">';
			}
	?>
				
					<div class="col-sm-4">
					<div class="well" style="background-color:#eee;">
							<ul class="list-unstyled">
							<li><strong><?php echo $address_data['customer_name']?></strong></li>
							<li><?php echo $address_data['address1']?></li>
							<li><?php echo $address_data['address2']?></li>
							<li><?php echo $address_data['city']?> - <?php echo $address_data['pincode']?></li>
							<li><?php echo $address_data['state']?>, <?php echo $address_data['country']?></li>
							<li><?php echo $address_data['mobile']?></li>
							<hr>
							<li><?php if(count($all_addresses)>0){ ?>
									<?php if($address_data['make_default']!=0){?>
										<b><i class="fa fa-check green-color" aria-hidden="true"></i> Default Address</b>
									<?php }
									else{?> 
									<a href="<?php echo base_url()?>Account/set_this_address_default/<?php echo $address_data['customer_address_id']?>"><i class="fa fa-check grey-color" aria-hidden="true"></i> Make Default Address</a>
									<?php }
									
									 ?>
								<?php }?></li>
							<li><a href="<?php echo base_url()?>Account/edit_this_address/<?php echo $address_data['customer_address_id']?>">Edit this address</a></li>
							<li><a href="" actions="<?php echo base_url()?>Account/delete_this_address/<?php echo $address_data['customer_address_id']?>" onclick="askConfirmation(this)">Delete this address</a></li>
							</ul>
					</div>
					</div>
	<?php 
	$i++;
	}
	
}
else{
	echo "<div class='panel-body'><p class='lead text-center'>No address  added</p></div>";
}
?>
						
					</div>
				</div>
			  </div>
			</div>
					
				<div class="panel panel-default">
				<div class="panel-body">
					<h4>ADD NEW ADDRESS</h4>
					<hr>
					<form method="post" class="form-horizontal" onsubmit="add_new_addressFun()" id="add_new_addressForm">
							<div class="form-group">
								<label class="control-label col-md-3">Customer Name:</label>
								<div class="col-md-6">
								<input type="text" required class="form-control" name="name">
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Mobile Number:</label>
								<div class="col-md-6">
								<input type="number" required onblur="check_mobile_number()" onkeyup="check_mobile_number()" id="mobile" class="form-control" name="mobile"  onkeypress="return isNumber(event);">
								<span class="help-block" id="mobileerrormessage"></span>
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Door Address:</label>
								<div class="col-md-6">
								<input type="text" required class="form-control" name="door_address">
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Street Address:</label>
								<div class="col-md-6">
								<input type="text" required class="form-control" name="street_address">
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">City:</label>
								<div class="col-md-6">
								<input type="text" required class="form-control" name="city">
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">State/Province:</label>
								<div class="col-md-6">
								<select required class="form-control" name="state">
								<option value="">-Select-</option>
								<?php
									foreach($get_all_states_arr as $arr){
										?>
										<option value="<?php echo $arr["state_name"];?>"><?php echo $arr["state_name"];?></option>
										<?php
									}
								?>
								</select>
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Pin/Zip Code:</label>
								<div class="col-md-6">
								<input type="number" onblur="check_pin_number()" onkeyup="check_pin_number()" required class="form-control" id="pin" name="pin">
								<span class="help-block" id="pinerrormessage"></span>
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Country:</label>
								<div class="col-md-6">
								<input type="text" required class="form-control" name="country" value="India" readonly>
							</div>
							</div>
							<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
							<div class="checkbox">
							<label><input type="checkbox" name="make_default" value="1"> Make Default Address</label>
							</div>
							</div>
							</div>
							<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
								<button type="submit" id="submit-btn" class="button preventDflt">Submit</button>
								<button type="reset" class="button preventDflt">Reset</button>
								</div>
							</div>
            		</form>
				</div>
				</div>
        </div>
    </div>
</div>
</div>
<script>
	function askConfirmation(obj){
		/*bootbox.confirm({
			message: "Do you want to delete this Address ?",
			size: "small",
			callback: function (result) {
				if(result){
					window.location.href =obj.getAttribute('actions')
				}
			}
		});*/

        if(confirm( "Do you want to delete this Address ?")){
            window.location.href =obj.getAttribute('actions');
        }
				
	}
	var check_mobile_number_err=0;
	function check_mobile_number(){
		var mobile=document.getElementById('mobile').value;
		if(mobile.match(/^\d{10}$/)){
			document.getElementById('mobileerrormessage').innerHTML='';
			check_mobile_number_err=0;
		}
		else{
			document.getElementById('mobileerrormessage').innerHTML='<small class="text_red">mobile number should be 10 digits</small>';
			check_mobile_number_err=1;
		}
	}
	var check_pin_number_err=0;
	function check_pin_number(){
		var mobile=document.getElementById('pin').value;
		if(mobile.match(/^[0-9]{6}$/)){
			document.getElementById('pinerrormessage').innerHTML='';
			check_pin_number_err=0;
		}
		else{
			document.getElementById('pinerrormessage').innerHTML='<small class="text_red">pin number contain 6 digits</small>';
			check_pin_number_err=1;
		}
	}
	
	function add_new_addressFun(){
        $('#submit-btn').prop('disabled', true);

		if(check_mobile_number_err==0 && check_pin_number_err==0){
			document.getElementById("add_new_addressForm").action="<?php echo base_url()?>Account/add_new_address";
			document.getElementById("add_new_addressForm").submit();
		}else{
            $('#submit-btn').prop('disabled', false);
        }
		
	}

    $('form#add_new_addressForm').submit(function(){
        $(this).find(':button[type=submit]').prop('disabled', true);
    });
</script>