	
<?php 

$order_item_data=$controller->get_order_item_data($order_item_id);
/*echo '<pre>';
print_r($order_item_data);
echo '</pre>';
*/
if(!empty($order_item_data)){
	foreach($order_item_data as $data){

			$quantity=$data['quantity'];
			$product_price=$data['product_price']; 
			$inventory_id=$data["inventory_id"];
			$order_id=$data['order_id'];
			?>
			<input type="hidden" value="<?php echo $inventory_id; ?>" id="inventory_id" name="inv_id" >
			<input type="hidden" value="<?php echo $order_id; ?>" id="order_id" name="order_id" >
			<?php

			$order_quantity=$controller->get_minimum_and_maximum_order_quantity($inventory_id);
			$moq=$order_quantity["moq"];
			$max_oq=$order_quantity["max_oq"];

			$invoice_offer_data=$controller->get_all_order_invoice_offers($order_id);

			if($invoice_offer_data->promotion_invoice_free_shipping>0 && $invoice_offer_data->invoice_offer_achieved==1){
				$shipping_charge_waived="yes";
			}else{
				$shipping_charge_waived="no";
			}

			$order_id=$data['order_id'];
			 
			$invoice_offers=$controller->get_all_order_invoice_offers_arr($order_id);
		
			if(!empty($invoice_offers)){
									
				foreach($invoice_offers as $data_inv){
					
					$promotion_invoice_cash_back=$data_inv["promotion_invoice_cash_back"];
					$promotion_invoice_free_shipping=$data_inv["promotion_invoice_free_shipping"];
					$promotion_invoice_discount=$data_inv["promotion_invoice_discount"];
					$total_amount_cash_back=$data_inv["total_amount_cash_back"];
					$total_amount_saved=$data_inv["total_amount_saved"];
					$total_amount_to_pay=$data_inv["total_amount_to_pay"];
					$payment_method=$data_inv["payment_method"];
					$invoice_offer_achieved=$data_inv["invoice_offer_achieved"];
					
					$invoice_surprise_gift_promo_quote=$data_inv["invoice_surprise_gift_promo_quote"];
					$invoice_surprise_gift=$data_inv["invoice_surprise_gift"];
					$invoice_surprise_gift_type=$data_inv["invoice_surprise_gift_type"];
					$invoice_surprise_gift_skus=$data_inv["invoice_surprise_gift_skus"];
					$invoice_surprise_gift_skus_nums=$data_inv["invoice_surprise_gift_skus_nums"];
					$customer_response_for_surprise_gifts=$data_inv["customer_response_for_surprise_gifts"];
					$customer_response_for_invoice_cashback=$data_inv["customer_response_for_invoice_cashback"];
					$status_of_refund_for_cashback=$data_inv["status_of_refund_for_cashback"];
					$status_of_refund_for_surprise_gifts=$data_inv["status_of_refund_for_surprise_gifts"];
					$invoices_offers_id=$data_inv["id"];
					$cart_quantity=$data_inv["cart_quantity"];	
					$total_price_without_shipping_price=$data_inv["total_price_without_shipping_price"];	
					
				}
			}
?>	
			<!-- ../page heading-->
			

		<form id="shipping_charge_form" name="shipping_charge_form">
			
			<input type="hidden" id="shipping_price_waived" name="shipping_price_waived" value="<?php echo $shipping_charge_waived; ?>">
			
			<input type="hidden" id="logistics_id" name="logistics_id" value="<?php echo $data["logistics_id"]; ?>">
			<input type="hidden" id="logistics_delivery_mode_id" name="logistics_delivery_mode_id" value="<?php echo $data["logistics_delivery_mode_id"]; ?>">
			<input type="hidden" id="logistics_territory_id" name="logistics_territory_id" value="<?php echo $data["logistics_territory_id"]; ?>">
			<input type="hidden" id="logistics_parcel_category_id" name="logistics_parcel_category_id" value="<?php echo $data["logistics_parcel_category_id"]; ?>">
			<input type="hidden" id="order_item_id" name="order_item_id" order_item_id="<?php echo $order_item_id; ?>">
			<input type="hidden" id="ordered_quantity" name="ordered_quantity" ordered_quantity="<?php echo $data["quantity"]; ?>">
			<?php
			//print_r($data);
			?>
		</form>				
							
		<input type="hidden" id="price_paid" price_paid="<?php echo $controller->get_per_unit_price_of_order_item($data['order_item_id']) ?>">
		<input type="hidden" id="price_paid_shipping" price_paid_shipping="<?php echo $data['shipping_charge'];?>">
							
										<?php
		
		$all_order_items=$controller->get_order_items_in_order_id($order_id,$data['order_item_id']);
										
		foreach($all_order_items as $order_items){
			
			$all_order_items1=$controller->get_all_order_items($order_items['order_item_id'],$order_id);
			//print_r($all_order_items1);
			if($order_items["status"]=="completed" || $order_items["status"]=="returned"){
				$completed_table_or_not="no";
				$returned_table_or_not="no";
				$completed_table_or_not=$controller->get_in_completed_table_or_not($order_items["order_item_id"],$order_items["status"]);
				$returned_table_or_not=$controller->get_in_returned_table_or_not($order_items["order_item_id"],$order_items["status"]);
				if($completed_table_or_not=="yes" && $returned_table_or_not=="yes" && $order_items["status"]=="returned"){ continue;}
			}
			
			if(!empty($all_order_items1)){
				
				foreach($all_order_items1 as $order_item){

					$quantity=$order_item['quantity'];
					$shipping_charge=$order_item['shipping_charge'];
					$shipping_charge_per_quantity=round($shipping_charge/$quantity);
					$product_price=$order_item['product_price'];	

								
					if($promotion_invoice_discount>0){
						$subtotal=$order_item["subtotal"];
						$invoice_discount=($subtotal/$total_price_without_shipping_price)*100;					
						$each_invoice_amount=$promotion_invoice_discount*$invoice_discount/100;
						//$invoice_amount=round($promotion_invoice_discount-$each_invoice_amount,2);
						$order_item_invoice_discount_value=round($each_invoice_amount,2);
						$order_item_invoice_discount_value_each=round($order_item_invoice_discount_value/$quantity,2);
						//$order_item_invoice_discount_value=round($promotion_invoice_discount/$cart_quantity,2);
					?>
					
					<input type="hidden" value="<?php echo $order_item_invoice_discount_value; ?>" name="order_item_invoice_discount_value" id="order_item_invoice_discount_value">
					

					<?php
					
					}else{
						$order_item_invoice_discount_value_each=0;
					}
					
					?>
									
	
					<form id="promo_details_for_return" name="promo_details_for_return">

						<input type="hidden" id="promotion_available" name="promotion_available" value="<?php echo $order_item["promotion_available"];?>">
						
						<?php 
							
						if($order_item["promotion_available"]==1){
							$promotion_quote=$order_item["promotion_quote"];
							$promotion_quote=trim(preg_replace('/\s+/', ' ', $promotion_quote));
							?>

							<input type="hidden" id="promotion_minimum_quantity" name="promotion_minimum_quantity" value="<?php echo $order_item["promotion_minimum_quantity"];?>">
							<input type="hidden" id="individual_price_of_product_with_promotion" name="individual_price_of_product_with_promotion" value="<?php echo $order_item["individual_price_of_product_with_promotion"];?>">
							<input type="hidden" id="individual_price_of_product_without_promotion" name="individual_price_of_product_without_promotion" value="<?php echo $order_item["individual_price_of_product_without_promotion"]; ?>">
							<input type="hidden" id="promotion_quote" name="promotion_quote" value="<?php echo $promotion_quote; ?>">
							<input type="hidden" id="promotion_default_discount_promo" name="promotion_default_discount_promo" value="<?php echo $order_item["promotion_default_discount_promo"]; ?>">
							
							<input type="hidden" id="promotion_surprise_gift" name="promotion_surprise_gift" value="<?php echo $order_item["promotion_surprise_gift"]; ?>">
							<input type="hidden" id="promotion_surprise_gift_type" name="promotion_surprise_gift_type" value="<?php echo $order_item["promotion_surprise_gift_type"]; ?>">
							<input type="hidden" id="promotion_surprise_gift_skus" name="promotion_surprise_gift_skus" value="<?php echo $order_item["promotion_surprise_gift_skus"]; ?>">
							<input type="hidden" id="promotion_surprise_gift_skus_nums" name="promotion_surprise_gift_skus_nums" value="<?php echo $order_item["promotion_surprise_gift_skus_nums"]; ?>">


							<input type="hidden" id="promotion_item" name="promotion_item" value="<?php echo rtrim($order_item["promotion_item"],','); ?>">
							<input type="hidden" id="promotion_item_num" name="promotion_item_num" value="<?php echo $order_item["promotion_item_num"]; ?>">
							<input type="hidden" id="promotion_cashback" name="promotion_cashback" value="<?php echo $order_item["promotion_cashback"]; ?>">
							<input type="hidden" id="cash_back_value" name="cash_back_value" value="<?php echo $order_item["cash_back_value"]; ?>">
							<input type="hidden" id="promotion_item_multiplier" name="promotion_item_multiplier" value="<?php echo $order_item["promotion_item_multiplier"]; ?>">
							<input type="hidden" id="promotion_discount" name="promotion_discount" value="<?php echo $order_item["promotion_discount"]; ?>">
							<input type="hidden" id="default_discount" name="default_discount" value="<?php echo $order_item["default_discount"]; ?>">
							<input type="hidden" id="total_price_of_product_with_promotion" name="total_price_of_product_with_promotion" value="<?php echo $order_item["total_price_of_product_with_promotion"]; ?>">
							<input type="hidden" id="total_price_of_product_without_promotion" name="total_price_of_product_without_promotion" value="<?php echo $order_item["total_price_of_product_without_promotion"]; ?>">
							
							<?php
							$free_inventory_available=0;
							if($order_item["promotion_item"]!=""){
								$free_inventory_available=1;
								$promo_item=$order_item["promotion_item"];
								$promotion_item_num=$order_item["promotion_item_num"];
								$free_skus_from_inventory=$controller->get_free_skus_from_inventory($promo_item,$promotion_item_num);
								//print_r($free_skus_from_inventory);

							}else{
								
								$free_skus_from_inventory=array();
								$free_inventory_available=0;
							}

							if($order_item["promotion_surprise_gift_type"]=="Qty"){
								
								$surprise_promo_item=$order_item["promotion_surprise_gift_skus"];
								$surprise_promo_item_num=$order_item["promotion_surprise_gift_skus_nums"];
								$surprise_free_skus_from_inventory=$controller->get_free_skus_from_inventory($surprise_promo_item,$surprise_promo_item_num);
								$free_inventory_available=1;

							}else{
								
								$surprise_free_skus_from_inventory=array();
							}
							
							?>
							<input type="hidden" id="free_inventory_available" name="free_inventory_available" value="<?php echo $free_inventory_available; ?>">
							
							<?php
						
						}else{
							?>
							<input type="hidden" id="free_inventory_available" name="free_inventory_available" value="0">
							<?php
							$free_skus_from_inventory=array();
							$surprise_free_skus_from_inventory=array();
						}
													
													?>
					
					</form>
							
					<form id="refund_promo_data_form" id="refund_promo_data_form">
						
						<input type="hidden" id="req_promotion_available" name="req_promotion_available" value="">
						<input type="hidden" id="req_promotion_minimum_quantity" name="req_promotion_minimum_quantity" value="">
						<input type="hidden" id="req_promotion_quote" name="req_promotion_quote" value="">
						<input type="hidden" id="req_promotion_default_discount_promo" name="req_promotion_default_discount_promo" value="">
						<input type="hidden" id="req_promotion_item" name="req_promotion_item" value="">
						<input type="hidden" id="req_promotion_item_num" name="req_promotion_item_num" value="">
						<input type="hidden" id="req_promotion_cashback" name="req_promotion_cashback" value="">
						<input type="hidden" id="req_promotion_item_multiplier" name="req_promotion_item_multiplier" value="">
						<input type="hidden" id="req_promotion_discount" name="req_promotion_discount" value="">
						<input type="hidden" id="req_default_discount" name="req_default_discount" value="">
						
						<input type="hidden" id="req_promotion_surprise_gift" name="req_promotion_surprise_gift" value="">
						<input type="hidden" id="req_promotion_surprise_gift_type" name="req_promotion_surprise_gift_type" value="">
						<input type="hidden" id="req_promotion_surprise_gift_skus" name="req_promotion_surprise_gift_skus" value="">
						<input type="hidden" id="req_promotion_surprise_gift_skus_nums" name="req_promotion_surprise_gift_skus_nums" value="">
						

						<input type="hidden" id="req_quantity_without_promotion" name="req_quantity_without_promotion" value="">
						<input type="hidden" id="req_quantity_with_promotion" name="req_quantity_with_promotion" value="">
						
						<input type="hidden" id="req_cash_back_value" name="req_cash_back_value" value="">
						<input type="hidden" id="req_individual_price_of_product_with_promotion" name="req_individual_price_of_product_with_promotion" value="">
						<input type="hidden" id="req_individual_price_of_product_without_promotion" name="req_individual_price_of_product_without_promotion" value="">
						<input type="hidden" id="req_total_price_of_product_with_promotion" name="req_total_price_of_product_with_promotion" value="">
						<input type="hidden" id="req_total_price_of_product_without_promotion" name="req_total_price_of_product_without_promotion" value="">

					</form>		


<?php
				}
			}
			
		}

	}/*end foreach*/
}/*end empty*/
?>	