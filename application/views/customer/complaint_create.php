<style>
.help-block {
  color:#a94442;
}
</style>
<div class="columns-container">
    <div class="container" id="columns">
	<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account">My account</a></li>
		  <li class="breadcrumb-item active">Create Case</li>
		</ol>

        <div class="row">
            <!-- Left colunm -->
			<?php require_once 'leftcolum.php';?>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-md-9" id="center_column">
               
				<div class="well col-md-12 " ng-controller="createComplaintsController">
				<div class="page-header"><h4 class="text-center">Create Case</h4></div>
					<form id="complaints" name="myForm" action="<?php echo base_url()?>Account/create_complaint" method="post" enctype="multipart/form-data" class="form-horizontal">
            				<div class="form-group">
							 <div class="col-md-3 text-right">Full Name:</div>
							<div class="col-md-9">
							    <input type="hidden" class="form-control" name="name" value="<?php echo $this->session->userdata("customer_name")?>">
							    <?php echo $this->session->userdata("customer_name")?>
							</div>
							</div>
							<div class="form-group">
							<div class="col-md-3 text-right">Account Id:</div>
							<div class="col-md-9">
							    
							    <input type="hidden" class="form-control" name="acc_id" value="<?php echo $this->session->userdata("customer_id")?>">
							   <?php echo $this->session->userdata("customer_id")?>
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Severity:</label>
								<div class="col-md-9">
									  <label class="radio-inline"><input type="radio" name="severity" ng-model="severity" required value="Urgent">Urgent</label>
									  <label class="radio-inline"><input type="radio" name="severity" ng-model="severity" required value="Normal">Normal</label>
								</div>

							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Regarding:</label>
								<?php if(!empty($regarding)){?>
									<div class="col-md-9">
									<?php
									foreach($regarding as $data){?>
									
									  <label class="radio-inline"><input type="radio" name="regarding" ng-model="regarding" required value="<?php echo $data['regarding_id'] ?>"><?php echo ucfirst($data['regarding']) ?></label>
										
									<?php }?>
									</div>
									<?php
								}?>

							</div>
							<div class="form-group">
							  <label class="control-label col-md-3">Service:</label>
							  <div class="col-md-9">
							  <select class="form-control" name="service" ng-model="service" required  ng-change="getCategory(service)">
							  	<option value=""><strong>Select Service</strong></option>
							  	 <option ng-repeat="r in serviceList" value="{{r.service_id}}" >{{r.service|uppercase}}
   </option>
								    
							</select>
							<span style="color:red" ng-show="myForm.service.$dirty && myForm.service.$invalid">
<span ng-show="myForm.service.$error.required">Selection is required.</span>
</span>						</div>
							</div>
							<div class="form-group">
							  <label class="control-label col-md-3">Category:</label>
							  <div class="col-md-9">
							  <select class="form-control" name="category" required ng-model="category" >
							  	<option value="">Select Service</option>
								<option ng-repeat="r in categoryList" value="{{r.category_id}}" >{{r.category|uppercase}}
   </option>
								</select>
								<span class="help-block"></span>
								<span style="color:red" ng-show="myForm.category.$dirty && myForm.category.$invalid">
<span ng-show="myForm.category.$error.required">Selection is required.</span>
</span>						</div>
							</div>
							<div class="form-group">
							  <label class="control-label col-md-3">Subject:</label>
							  <div class="col-md-9">
							  <input type="text" class="form-control" name="subject" ng-model="subject" required id="subject">
							  <span style="color:red" ng-show="myForm.subject.$dirty && myForm.subject.$invalid">
<span ng-show="myForm.subject.$error.required">subject is required.</span>
</span>						</div>
							</div>
							<div class="form-group">
							  <label class="control-label col-md-3">Discription:</label>
							  <div class="col-md-9">
							  <textarea class="form-control" rows="5" name="discription" ng-model="discription" required id="discription"></textarea>
							  <span style="color:red" ng-show="myForm.discription.$dirty && myForm.discription.$invalid">
<span ng-show="myForm.discription.$error.required">discription is required.</span>
</span>						</div>
							</div>
							<div class="form-group">
							 <label class="control-label col-md-3">Attachments:</label>
							 <div class="col-md-9">
							  <input type="file" name="filesToUpload[]" ng-model="filesToUpload" id="filesToUpload" multiple="" onchange="makeFileList(this,'Name');"  multiple="">
							</div>
							<div class="col-md-9 col-md-offset-3">
									<ul id="fileList" class="list-unstyled uploaded_files margin-top margin-bottom" style="display:none;"><li></li></ul>
									 <span class="help-block mb-0">Maximum 3 attachments of size less than 6 mb are uploaded </span>
									 <span class="help-block mt-0">(Hold shift for selecting multiple files) </span>
							  <span style="color:red" ng-show="myForm.filesToUpload.$dirty && myForm.filesToUpload.$invalid">
<span ng-show="myForm.filesToUpload.$error.required">description is required.</span>
</span>
								</div>
							</div>
							<script>
								
								function makeFileList() {
									
									var input = document.getElementById("filesToUpload");
									var ul = document.getElementById("fileList");
									while (ul.hasChildNodes()) {
										ul.removeChild(ul.firstChild);
									}
									if(input.files.length<4){
										var li = document.createElement("li");
										li.innerHTML = '<li class="text-center"><b>Uploaded Files<b></li>';
										ul.appendChild(li);
										flag=0;
										
										$("#fileList").show();
										for (var i = 0; i < input.files.length; i++) {
											var li = document.createElement("li");
											//li.innerHTML = input.files[i].name;
											li.innerHTML ='<span class="fa-stack fa-lg text-left"><i class="fa fa-file fa-stack-1x "></i><strong class="fa-stack-1x" style="color:#FFF; font-size:12px; margin-top:2px;">'+(i+1)+'</strong></span>'+input.files[i].name;
											ul.appendChild(li);
										}
										if(!ul.hasChildNodes()) {
											var li = document.createElement("li");
											li.innerHTML = 'No Files Selected';
											ul.appendChild(li);
										}
									}
									else{
										$("#fileList").hide();
										input.value="";
										if(!ul.hasChildNodes()) {
											var li = document.createElement("li");
											li.innerHTML = '<span class="text_red">Maximum Limit is Three</span>';
											ul.appendChild(li);
										}
									}
		
								}
								function resetForm(){
								    setTimeout(function(){
								        var ul = document.getElementById("fileList");
										while (ul.hasChildNodes()) {
											ul.removeChild(ul.firstChild);
										}
								    }, 50);
								    return true;
								}

							</script>
							
							<div class="form-group">
								<label class="control-label col-md-3">Contact Method:</label>
								<div class="col-md-9">
								  <label class="radio-inline"><input type="radio" name="contact_method" required  ng-model="contact_method" value="phone" ng-change="contactMethod(contact_method)">Phone</label>
								  <label class="radio-inline"><input type="radio" name="contact_method" required ng-model="contact_method" value="web" ng-change="contactMethod(contact_method)">Web</label>
								</div>
								
							</div>
							<div id="phone_selector" ng-show="contact_method== 'phone'">
							<div class="form-group">
							<label class="control-label col-md-3">Country Code:</label>
							<div class="col-md-9">
							<select class="form-control" name="country_code" required ng-model="country_code" ng-required="contact_method == 'phone'" id="country_code">
							  	<option value=""><strong>Select Country Code</strong></option>
								<option ng-repeat="r in countryCodeList" value="{{r.country_code}}" ng-selected="selected" >{{r.country|uppercase}}
   </option>
								</select>
							<span style="color:red" ng-show="myForm.country_code.$dirty && myForm.country_code.$invalid">
								<span ng-show="myForm.country_code.$error.required">subject is required.</span>
							</span>
							</div>
							</div>
							<div class="form-group">

							<label class="control-label col-md-3">Phone Number:</label>
							<div class="col-md-9">
							<!--<input type="number" class="form-control" name="phone_number" ng-model="phone_number" ng-required="contact_method == 'phone'" id="phone_number" ng-maxlength="10" ng-minlength="10">
							<span style="color:red" ng-show="myForm.phone_number.$dirty && myForm.phone_number.$invalid">
								<span ng-show="myForm.phone_number.$error.required">Phone Number is required.</span>
								<span ng-show="myForm.phone_number.$error.maxlength">Max Ten Number Required</span>
								<span ng-show="myForm.phone_number.$error.minlength">Min Ten Number Required</span>
							</span>
							--->
							<input type="number" class="form-control" name="phone_number" ng-model="phone_number" ng-minlength="10" 
                   ng-maxlength="10" min="0" onpaste="return false;" equired allow-only-numbers mobile-exist-c="formData.mobile"   onkeypress="return isNumber(event)" id="phone_number">
				   
				   
				    <p class="help-block" ng-show="myForm.phone_number.$error.required || myForm.phone_number.$error.number">Valid mobile number is required
					</p>
				<p class="help-block" ng-show="myForm.phone_number.$error.mobileexistValidator">This mobile is registered already.</p>
				
					<p class="help-block" ng-show="((myForm.phone_number.$error.minlength ||
								   myForm.phone_number.$error.maxlength) && 
								   myForm.phone_number.$dirty) ">
								   mobile number should be 10 digits
					</p>
					
					
							
							</div>
							</div>
							<div class="form-group">
							<label class="control-label col-md-3">Extension Number:</label>
							<div class="col-md-9">
							<input type="number" class="form-control" name="ext_number" ng-model="ext_number"  id="ext_number">
							</div>
							</div>
							<div class="form-group">
								<div class="col-md-9 col-md-offset-3">
							  <button type="submit" class="button" ng-disabled="myForm.regarding.$invalid || myForm.service.$invalid || myForm.category.$invalid || myForm.subject.$invalid || myForm.discription.$invalid || myForm.contact_method.$invalid || myForm.country_code.$invalid || myForm.phone_number.$invalid || myForm.ext_number.$invalid"> Submit </button>
							  <button type="reset" class="button" onclick="return resetForm();"> Reset </button>
							</div>
							</div>
							</div>
							
							<div id="web_selector" ng-show="contact_method== 'web'">
							<div class="form-group">
								<div class="col-md-9 col-md-offset-3">
							  <button type="submit" class="button preventDflt" ng-disabled="myForm.regarding.$invalid || myForm.service.$invalid || myForm.category.$invalid || myForm.subject.$invalid || myForm.discription.$invalid || myForm.contact_method.$invalid || myForm.country_code.$invalid || myForm.phone_number.$invalid || myForm.ext_number.$invalid"> Submit </button>
							  <button type="reset" class="button" onclick="return resetForm();"> Reset </button>
							</div>
							</div>
							</div>
							
            		</form>
            	
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

<script>


function createComplaintsController($scope,Scopes,$http){
	
	$scope.serviceList = [];
			$http({
				method : "POST",
				url : "<?php echo base_url();?>Account/get_all_service_regarding",
				dataType:"JSON",
				}).success(function mySucces(result) {
					
					$scope.serviceList=result;
					
				}, function myError(response) {
					
				});
	$scope.countryCodeList = [];
			$http({
				method : "POST",
				url : "<?php echo base_url();?>Account/get_all_country_code",
				dataType:"JSON",
				}).success(function mySucces(result) {
					
					$scope.countryCodeList=result;
					
				}, function myError(response) {
					
				});
	
	
	$scope.getCategory=function(service){
	var serviceId=$scope.service;
	$scope.categoryList=[];
	
		$http({
			method : "POST",
			url : '<?php echo base_url();?>Account/get_all_category_service/'+serviceId,
			dataType:"JSON",
		}).success(function mySucces(result) {
			$scope.categoryList=result;
		}, function myError(response) {
			
		});
	
	}
	$scope.contactMethod=function(contact_method) {
       
 	}

	
	
	}
	
	
	function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
        return false;
    }
    return true;
}

$('form#complaints').submit(function(){
    $(this).find(':button[type=submit]').prop('disabled', true);
});
</script>            	
            