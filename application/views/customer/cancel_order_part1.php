<?php 
	
	$order_item_data=$controller->get_order_item_data($order_item_id);//take from active orders

	if(!empty($order_item_data)){
		foreach($order_item_data as $data){ $quantity=$data['quantity'];$product_price=$data['product_price']; 

		$inventory_id=$data["inventory_id"];
		$order_id=$data['order_id'];
		
		$order_quantity=$controller->get_minimum_and_maximum_order_quantity($inventory_id);
		$moq=$order_quantity["moq"];
		$max_oq=$order_quantity["max_oq"];

		$invoice_offer_data=$controller->get_all_order_invoice_offers($order_id);

		if($invoice_offer_data->promotion_invoice_free_shipping>0 && $invoice_offer_data->invoice_offer_achieved==1){
			$shipping_charge_waived="yes";
		}else{
			$shipping_charge_waived="no";
		}

		$order_id=$data['order_id'];
	 
		$invoice_offers=$controller->get_all_order_invoice_offers_arr($order_id);

		if(!empty($invoice_offers)){
								
			foreach($invoice_offers as $data_inv){
				
				$promotion_invoice_cash_back=$data_inv["promotion_invoice_cash_back"];
				$promotion_invoice_free_shipping=$data_inv["promotion_invoice_free_shipping"];
				$promotion_invoice_discount=$data_inv["promotion_invoice_discount"];
				$total_amount_cash_back=$data_inv["total_amount_cash_back"];
				$total_amount_saved=$data_inv["total_amount_saved"];
				$total_amount_to_pay=$data_inv["total_amount_to_pay"];
				$payment_method=$data_inv["payment_method"];
				$invoice_offer_achieved=$data_inv["invoice_offer_achieved"];
				$invoice_surprise_gift_promo_quote=$data_inv["invoice_surprise_gift_promo_quote"];
				
				$invoice_surprise_gift=$data_inv["invoice_surprise_gift"];
				$invoice_surprise_gift_type=$data_inv["invoice_surprise_gift_type"];
				$invoice_surprise_gift_skus=$data_inv["invoice_surprise_gift_skus"];
				$invoice_surprise_gift_skus_nums=$data_inv["invoice_surprise_gift_skus_nums"];
				$customer_response_for_surprise_gifts=$data_inv["customer_response_for_surprise_gifts"];
				$customer_response_for_invoice_cashback=$data_inv["customer_response_for_invoice_cashback"];
				$total_price_without_shipping_price=$data_inv["total_price_without_shipping_price"];	
			}
		}

		
		$all_order_items=$controller->get_order_items_in_order_id($order_id,$data['order_item_id']);
										
		foreach($all_order_items as $order_items){
			
			$all_order_items1=$controller->get_all_order_items($order_items['order_item_id'],$order_id);
			//print_r($all_order_items1);
			if($order_items["status"]=="completed" || $order_items["status"]=="returned"){
				$completed_table_or_not="no";
				$returned_table_or_not="no";
				$completed_table_or_not=$controller->get_in_completed_table_or_not($order_items["order_item_id"],$order_items["status"]);
				$returned_table_or_not=$controller->get_in_returned_table_or_not($order_items["order_item_id"],$order_items["status"]);
				if($completed_table_or_not=="yes" && $returned_table_or_not=="yes" && $order_items["status"]=="returned"){ continue;}
			}
			
			if(!empty($all_order_items1)){
				
					foreach($all_order_items1 as $order_item){

							$quantity=$order_item['quantity'];
							$shipping_charge=$order_item['shipping_charge'];
							$shipping_charge_per_quantity=round($shipping_charge/$quantity);
							$product_price=$order_item['product_price'];	

							$order_item_id=$order_item["order_item_id"];
	
							$promotion_item=$order_item["promotion_item"];
							$promotion_item_num=$order_item["promotion_item_num"];

							if($promotion_item!=''){
												
									$quantity=$order_item['quantity'];
									$promotion_minimum_quantity=$order_item['promotion_minimum_quantity'];
									
									$promo_item=rtrim($promotion_item,',');
									$promotion_item_num=rtrim($promotion_item_num,',');
									
									$promo_item_arr = explode(',', $promo_item);
									$promo_item_num_arr = explode(',', $promotion_item_num);
									
									$two=array_combine($promo_item_arr,$promo_item_num_arr);
									
									$temp_quotient=intval($quantity/$promotion_minimum_quantity);
									if($promotion_item!=''){
										$free_items_arr=$controller->get_free_skus_from_inventory_with_details($promotion_item,$promotion_item_num);
									}
											
							}
						

					}//inside foreach

					
			}
					
		}
		?>
									
<?php }/*end foreach*/
}/*end empty*/
?>