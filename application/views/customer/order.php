<?php
$email_stuff_arr = email_stuff();
?>
<style>
	@media only screen and (min-width: 768px) {
		.text-align-mobile-center {
			text-align: left !important
		}
	}

	@media only screen and (max-width: 768px) {
		.text-align-mobile-center {
			text-align: left !important;
			border-bottom: 1px solid #ccc;
			margin-bottom: 0.5em;
		}

		.text-align-mobile {
			text-align: left !important
		}

		.margin-top-bottom-mobile {
			margin: 1em 0;
		}
	}
</style>
<div class="columns-container">
	<div class="container" id="columns">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
			<li class="breadcrumb-item"><a href="<?php echo base_url() ?>Account">My account</a></li>
			<li class="breadcrumb-item active">My Orders</li>
		</ol>
		<div class="row">
			<div class="center_column col-xs-12 col-sm-12 my_orders_links" id="center_column">

				<!--- order search --->

				<div class="row">
					<div class="col-md-6 pull-right ">
					<form class="form-inline my-2 my-lg-0">

					<select class="form-control mr-sm-2" type="search" id="search_status" placeholder="Search By Order ID" aria-label="Search">
						<option value="">All Orders</option>	
						<option value="active" <?php echo ($search_status=='active') ? 'selected' : '' ?>>Active Orders</option>	
						<option value="completed" <?php echo ($search_status=='completed') ? 'selected' : '' ?>>Completed Orders</option>	
						<option value="cancelled" <?php echo ($search_status=='cancelled') ? 'selected' : '' ?>>Cancelled Orders</option>	
						<option value="returned" <?php echo ($search_status=='returned') ? 'selected' : '' ?>>Returned Orders</option>	
					</select>

					<input class="form-control mr-sm-2" id="search_order_id" value="<?php echo $search_order_id; ?>" type="search" placeholder="Search By Order ID" aria-label="Search">

					<button class="btn btn-success my-2 my-sm-0" style="background-color: #ff6600;border:1px solid #052670;" type="button" onclick="filter_orders()">Search</button>
					<button class="btn btn-default my-2 my-sm-0" style="background-color: #ccc;border:1px solid #052670;" type="button" onclick="filter_orders_reset()">Reset</button>
					</form>
					</div>
				</div>
				<br>

				<!--- order search --->

				<?php $s = 0; ?>
				<?php if (!empty($all_orders)) { ?>
					<div class="common">
						<?php
						foreach ($all_orders as $order_id => $arr) {
							?>
							<!-- order start-->
							<div class="panel-group margin-bottom" id="accordion<?php echo $order_id; ?>" role="tablist"
								aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading<?php echo $order_id; ?>">
										<div class="panel-title normal-text">
											<div class="row">
												<div class="col-md-3 text-align-mobile-center">
													<div class="f-row text-uppercase">
														Order placed
													</div>
													<div class="f-row normal-text mb-10">
														<?php echo date_format(date_create($controller->get_order_placed_date($order_id)), "D, jS M'y") ?>
													</div>
												</div>
												<div class="col-md-5 text-align-mobile-center">
													<div class="f-row text-uppercase">
														Ship to
													</div>
													<div class="f-row mb-10">
														<?php
														$shipp_obj = $controller->get_shipping_address_of_order($order_id);
														//print_r($shipp_obj);
														if (!empty($shipp_obj)) {
															?>
															<a class="cursor-pointer" data-placement="bottom" data-toggle="popover"
																data-title="Customer Address" data-container="body" type="button"
																data-html="true" id="login">
																<div id="popover-content" class="hide">
																	<ul class="pull-right">
																		<li><strong>
																				<?php echo $shipp_obj->customer_name; ?>
																			</strong></li>
																		<li>
																			<?php echo $shipp_obj->address1; ?>
																		</li>
																		<li>
																			<?php echo $shipp_obj->address2; ?>
																		</li>
																		<li>
																			<?php echo $shipp_obj->city; ?>,
																			<?php echo $shipp_obj->state; ?>
																		</li>
																		<li>
																			<?php echo $shipp_obj->pincode; ?>
																		</li>
																		<li>
																			<?php echo $shipp_obj->country; ?>
																		</li>
																		<li>
																			<?php echo "Phone: " . $shipp_obj->mobile; ?>
																		</li>
																	</ul>
																</div>
																<?php echo $shipp_obj->customer_name; ?> <i class="fa fa-caret-down"
																	aria-hidden="true"></i>
															</a>
														<?php } ?>
													</div>
												</div>
												<div class="col-md-4 text-right text-align-mobile">
													<div class="f-row text-uppercase">
														Order ID #
														<?php echo $order_id; ?>
													</div>
													<div class="f-row">
														<a
															href="<?php echo base_url() ?>Account/order_details/<?php echo $order_id; ?>">Order
															Details</a> <a href="#"> </a>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div id="<?php echo $order_id; ?>" class="panel-collapse collapse in" role="tabpanel"
										aria-labelledby="headingThree">
										<div class="panel-body">
											<ul class="products-block best-sell" id="common_<?php echo $order_id; ?>">
												<?php
												$i = 1;
												foreach ($arr as $ar1) {
													foreach ($ar1 as $ar) {
														$completed_table_or_not = "no";
														$returned_table_or_not = "no";
														if ($ar["status"] == "completed" || $ar["status"] == "returned") {
															$completed_table_or_not = $controller->get_in_completed_table_or_not($ar["order_item_id"], $ar["status"]);
															$returned_table_or_not = $controller->get_in_returned_table_or_not($ar["order_item_id"], $ar["status"]);
															if ($completed_table_or_not == "yes" && $returned_table_or_not == "yes" && $ar["status"] == "returned") {
																continue;
															}
														}
														$all_order_items = $controller->get_all_order_items($ar['order_item_id'], $order_id);
														if (!empty($all_order_items)) {
															foreach ($all_order_items as $order_item) {
																$inv_obj = $controller->get_inventory_details($order_item['inventory_id']);
																?>
																<li class="ml-0">
																	<!----single item --->
											<div class="row">
												<div class="col-md-12">
													<?php
													if ($order_item['ord_sku_name'] != '') {
														$product_name = $order_item['ord_sku_name'];
													} else {
														$product_name = $controller->get_product_name($order_item['product_id']);
													}
													?>
													<!---- addon products --->
													<?php

													//if($order_item["ord_addon_products_status"]=='1'){
/*
if(0){ // I modified to bypass the addon products


//$total_amount_normal+=round($order_item["ord_addon_total_price"]);
$addon=json_decode($order_item["ord_addon_products"]);
//print_r($addon);
$addon_count=count($addon);
?>

<div class="col-sm-4 products-block">


<?php 

if($order_item['ord_addon_inventories']!=''){

$i=1;
foreach($addon as $val){
$inv_id=$val->inv_id;
$name=$val->inv_name;		
$image=$val->inv_image;
$sku_id=$val->inv_sku_id;
$price=$val->inv_price;

?>
<div class="row">
<div class="col-sm-3 products-block">
<div class="products-block-left">
<a href="#">
<?php 
$img_path=get_inventory_image_path_combo($inv_id,$image);
?>
<img src="<?php echo $img_path;?>" alt="SPECIAL PRODUCTS">
</a>
</div>
</div>
<div class="col-sm-8 products-block">
<div class="f-row product-name">
<a href="<?php echo base_url()?>detail/<?php echo $controller->sample_code().$inv_id;?>"><?php echo $name; ?></a>


</div>
<div class="f-row small-text">SKU : <?php echo $sku_id; ?></div>
<div class="f-row small-text">Price : <?php echo curr_sym; ?><?php echo $price; ?></div>

</div>


</div>
<br>

<?php 

}
}
?>
</div>

<?php 

}*/

													//else{
							
													?>
													<!---- addon products --->
													<div class="row">
														<div class="col-sm-1 products-block">
															<div class="products-block-left">
																<a href="#">
																	<?php
																	$inventory_id = $order_item['inventory_id'];
																	$image = $order_item['image'];
																	$img_path = get_inventory_image_path($inventory_id, $image);
																	?>
																	<img src="<?php echo base_url() . $img_path; ?>"
																		alt="SPECIAL PRODUCTS">
																</a>
															</div>
														</div>
														<div class="col-sm-4 products-block">
															<div class="f-row product-name">
																<a
																	href="<?php echo base_url() ?>detail/<?php echo $controller->sample_code() . $order_item['inventory_id']; ?>">
																	<?php echo $product_name; ?>
																</a>
																<?php
																$product_details = get_product_details($order_item['product_id']);
																if (!empty($product_details)) {
																	echo ($product_details->product_description != '') ? '<div class="mb-10">' . $product_details->product_description . '</div>' : '';
																} ?>

															</div>
															<div class="f-row small-text">
																<?php
																//$inv_obj->attribute_1_value= substr($inv_obj->attribute_1_value, 0, strpos($inv_obj->attribute_1_value, ":"));
																if (!empty($inv_obj->attribute_1)) {
																	echo $inv_obj->attribute_1 . " : " . $inv_obj->attribute_1_value . '<br>';
																}
																if (!empty($inv_obj->attribute_2)) {
																	echo $inv_obj->attribute_2 . " : " . $inv_obj->attribute_2_value . '<br>';
																}
																if (!empty($inv_obj->attribute_3)) {
																	echo $inv_obj->attribute_3 . " : " . $inv_obj->attribute_3_value . '<br>';
																}
																if (!empty($inv_obj->attribute_4)) {
																	echo $inv_obj->attribute_4 . " : " . $inv_obj->attribute_4_value . '<br>';
																}
																?>
															</div>
															<div class="f-row small-text">SKU :
																<?php echo $order_item['sku_id']; ?>
															</div>
															<div class="f-row small-text">Quantity:
																<?php echo $order_item['quantity']; ?>
															</div>
															<div class="f-row small-text">Seller:
																<?php echo $email_stuff_arr["name_emailtemplate"] ?>
															</div>
														</div>
														<?php //}  ?>
														<!--- single product--->
														<div
															class="col-sm-2 products-block text-center text-align-mobile ">
															<div class="f-row">
																<?php echo curr_sym; ?>
																<?php echo round($order_item['grandtotal']); ?>
															</div>
															<?php
															$r = 0;
															if ($order_item['promotion_quote'] != "") {
																$r = 1;
															}
															if ($order_item['promotion_default_discount_promo'] != "" && $order_item['default_discount'] > 0 && ($order_item['promotion_quote'] != $order_item['promotion_default_discount_promo'])) {
																$r += 1;
															}
															if ($r > 0) {
																?>

															<div class="f-row">Offers:
																<?php echo $r; ?>
															</div>

															<?php } else {

																if ($order_item['ord_selling_discount'] > 0) {

																	?>
															<div class="f-row">Offers: 1 </div>
															<?php
																}
															}
															?>
														</div>
														<?php echo $controller->get_order_item_status_summary($order_item['order_item_id'], $order_id) ?>
													</div>
													<?php
													//if($order_item["ord_addon_single_or_multiple_tagged_inventories_in_frontend"]=='single'){
													if (0) {
														//$total_amount_normal+=round($order_item["ord_addon_total_price"]);
														$addon = json_decode($order_item["ord_addon_products"]);
														//print_r($addon);
														$addon_count = count($addon);
														if (!empty($addon)) {
															?>
													<div class="row align-top" style="background-color:#FFFFE0">
														<div class="col-sm-12 products-block">
															<?php
															$i = 1;
															foreach ($addon as $val) {
																$inv_id = $val->inv_id;
																$name = $val->inv_name;
																$image = $val->inv_image;
																$sku_id = $val->inv_sku_id;
																$price = $val->inv_price;
																$addon_inventory_mrp_price = $val->inv_mrp_price;
																?>
															<div class="row align-top">
																<div class="col-sm-1 products-block">
																	<div class="products-block-left">
																		<a href="#">
																			<?php
																			$img_path = get_inventory_image_path_combo($inv_id, $image);
																			?>
																			<img src="<?php echo base_url() . $img_path; ?>"
																				alt="SPECIAL PRODUCTS">
																		</a>
																	</div>
																</div>
																<div class="col-sm-4 products-block">
																	<div class="f-row small-text"><b>Addon For
																			<?php echo $product_name; ?>
																		</b></div>
																	<div class="f-row product-name">
																		<a
																			href="<?php echo base_url() ?>detail/<?php echo $controller->sample_code() . $inv_id; ?>">
																			<?php echo $name; ?>
																		</a>
																	</div>
																	<div class="f-row small-text">SKU :
																		<?php echo $sku_id; ?>
																	</div>
																	<?php
																	if ($addon_inventory_mrp_price != $price) {
																		?>
																	<div class="f-row small-text">Price : <del>
																			<?php echo curr_sym; ?>
																			<?php echo $addon_inventory_mrp_price; ?>
																		</del>
																		<?php echo curr_sym; ?>
																		<?php echo $price; ?>
																	</div>
																	<?php
																	} else {
																		?>
																	<div class="f-row small-text">Price :
																		<?php echo curr_sym; ?>
																		<?php echo $price; ?>
																	</div>
																	<?php
																	}
																	?>
																</div>
																<div class="col-sm-2 products-block text-center">
																</div>
															</div>
															<br>
															<?php
															}
															?>
														</div>
													</div>
													<?php
														}
													}
													?>
												</div>
											</div>
										</li>
										<?php
										$i++;
															}
														}
													}
												}
												?>
									</ul>
									<?php
									if ($i > 10) {
										$style = '';
									} else {
										$style = 'style="display:none;"';
									}
									?>
									<div class="f-row margin-top" id="pagination_div_<?php echo $order_id; ?>" <?php echo $style; ?>>
										<div class="sortPagiBar">
											<div class="bottom-pagination">
												<span class="nav_pag" id="nav_page_<?php echo $order_id; ?>"></span>
											</div>
										</div>
									</div>
									<script type="text/javascript">
										jQuery(document).ready(function ($) {
											$('#common_<?php echo $order_id; ?>').paginathing({
												perPage: 10,
												insertAfter: '#nav_page_<?php echo $order_id; ?>'
											});
										});
									</script>
								</div>
								<div class="panel-footer text-right">
									<div class="f-row text-uppercase">
										Order Total
										<?php echo curr_sym; ?>
										<?php echo $controller->get_order_grand_total($order_id) ?>
									</div>
								</div>
							</div>
						</div>
					</div><!-- ../order end-->
							<?php
							$s++;
						} /*end foreach order id*/ ?>
					</div>
				<?php }/*end empty orders*/ else {
					?>
					<div class="well page-header">
						<div class="row">
							<div class="col-md-12 text-center">
								<h3>No orders yet</h3>
							</div>
						</div>
					</div>
					<?php
				}
				?>
				<?php
				if ($s > 10) {
					$style = '';
				} else {
					$style = 'style="display:none;"';
				}
				?>
				<div class="f-row margin-top" id="pagination_div" <?php echo $style; ?>>
					<div class="sortPagiBar">
						<div class="bottom-pagination">
							<span class="nav_pag" id="nav_page"></span>
						</div>
					</div>
				</div>
				<script type="text/javascript">
					jQuery(document).ready(function ($) {
						$('.common').paginathing({
							perPage: 10,
							insertAfter: '#nav_page'
						});
					});
				</script>
			</div>
			<!-- ./ Center colunm -->
		</div>
		<!-- ./row-->
	</div>
</div>
<script>
	$(document).ready(function () {
		$("[data-toggle=popover]").popover({
			html: true,
			content: function () {
				return $('#popover-content').html();
			}
		});
		function toggleIcon(e) {
			$(e.target)
				.prev('.panel-heading1')
				.find(".more-less")
				.toggleClass('glyphicon-plus glyphicon-minus');
		}
		$('.panel-group').on('hidden.bs.collapse', toggleIcon);
		$('.panel-group').on('shown.bs.collapse', toggleIcon);

	});

	function filter_orders(){
		search_order_id=$('#search_order_id').val();
		search_status=$('#search_status').val();
		if(search_status==''){
			search_status='1';
		}
		window.location.href = "<?php echo base_url() ?>Account/my_order/"+search_status+"/"+search_order_id;

	}
	function filter_orders_reset(){
		window.location.href = "<?php echo base_url() ?>Account/my_order";
	}
</script>