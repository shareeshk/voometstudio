
<style class="cp-pen-styles">
.star-rating {
  font-size: 0;
  white-space: nowrap;
  display: inline-block;
  width: 250px;
  height: 50px;
  overflow: hidden;
  position: relative;
  background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjREREREREIiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
  background-size: contain;
}
.star-rating i {
  opacity: 0;
  position: absolute;
  left: 0;
  top: 0;
  height: 100%;
  width: 20%;
  z-index: 1;
  background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjRkZERjg4IiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
  background-size: contain;
}
.star-rating input {
  -moz-appearance: none;
  -webkit-appearance: none;
  opacity: 0;
  display: inline-block;
  width: 20%;
  height: 100%;
  margin: 0;
  padding: 0;
  z-index: 2;
  position: relative;
}
.star-rating input:hover + i,
.star-rating input:checked + i {
  opacity: 1;
}
.star-rating i ~ i {
  width: 40%;
}
.star-rating i ~ i ~ i {
  width: 60%;
}
.star-rating i ~ i ~ i ~ i {
  width: 80%;
}
.star-rating i ~ i ~ i ~ i ~ i {
  width: 100%;
}
.choice {
  font-size:1.5em;
  margin: 0px auto;
}


</style>

<div class="columns-container">
    <div class="container" id="columns">
	<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account">My account</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account/complaint_history">My Complaints history</a></li>
		   <li class="breadcrumb-item active">My Complaint thread</li>
		</ol>
        
        <div class="row">
            <!-- Left colunm -->
			<?php require_once 'leftcolum.php';?>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-md-9" id="center_column">				
					
					<div class="row">
						<div class="col-md-12">
						<div class="well">
			                
			                <?php if(!empty($master_complaint_data)){
			                	foreach($master_complaint_data as $complaint_data){
			                		$case_status= $complaint_data['close_case'];
									$complaint_thread_id=$complaint_data['complaint_thread_id'];
			                		?>	
			                <div class="row"><div class="col-md-12 "><p class="lead">Subject: <b><?php echo $complaint_data['subjects']?> </b></p></div></div>
							<div class="row">
			                <div class="col-md-6">
							<dl class="dl-horizontal">
							  <dt>Case ID:</dt>
							  <dd><?php echo $complaint_data['id']?></dd>
							  <dt>Created On:</dt>
							  <dd><?php echo date('M d, Y - H:i A',strtotime($complaint_data['timestamp'])) ?></dd>
							  <dt>Case type:</dt>
							  <dd><?php echo ucfirst($complaint_data['regarding'])?></dd>
							  <dt>By:</dt>
							  <dd><?php echo $controller->get_complaint_case_user_fullname($complaint_data['from_id'])?></dd>
							</dl>
							</div>
			                <div class="col-md-6">
							<dl class="dl-horizontal">
							  <dt>Status:</dt>
							  <dd id="status"><?php echo $controller->get_complaint_case_present_status($complaint_data['close_case']);?></dd>
							  <dt>Severity:</dt>
							  <dd><?php echo $complaint_data['severity']?></dd>
							  <dt>Category:</dt>
							  <dd><?php echo ucfirst($complaint_data['service'])?> , <?php echo ucfirst($complaint_data['category'])?></dd>
							</dl>
			                </div>
						</div>
						<div class="page-header">
						<div class="row">
						<div class="col-md-6">
							<h4>
			                    <span class="page-heading-title2">Correspondence</span>
			                </h4>
							</div>
							<div class="col-md-6">
			                <span class="pull-right" id="actionButtonContainer">
			                    	<?php if($case_status==2||$case_status==3 ||$case_status==4) { ?>
			                    		<button class="btn btn-success preventDflt" id="reopenCaseButton"  onclick="reopenThisComplaint('<?php echo $complaint_thread_id ?>')">Reopen case</button>&nbsp;
			                    	<?php }?>
			                    	<?php if($case_status!=2 && $case_status!=3 && $case_status!=4) { ?>
			                    		<button class="btn btn-default preventDflt" id="replyCaseButton" onclick="replyThisComplaint('<?php echo $complaint_thread_id ?>')">Reply</button>&nbsp;
			                    		<button class="btn btn-success" id="closeCaseButton" onclick="closeThisComplaint('<?php echo $complaint_thread_id ?>')">Close case</button>&nbsp;
			                    	<?php }?>
			                    	<span id="disButtonContainer">
			                    	<?php if($case_status!=4 && $case_status!=2 && $case_status!=3) { ?>
			                    	<button class="btn btn-danger preventDflt" id="disputCaseButton" onclick="disputThisComplaint('<?php echo $complaint_thread_id ?>')">Mark Disputed</button>
			                    	<?php }?>
			                    	</span>
			                    </span>
			                
						</div>
					</div>
					</div>
						<?php }
			                 ?>
			                	

					
					<div id="all_message_container">
						
					
					
					
					<div class="row">
						<div class="col-md-4 text-right">
            				<p><b>Amazon Web Services</b></p>
            				<p>Apr 22, 2016 </p><p>09:00 PM +0530</p>
            				<p><i class="fa fa-desktop fa-2x" aria-hidden="true"></i></p>
            			</div>
            			<div class="col-md-8 well more" style="background-color: #eee;">
            				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
            				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
            				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
            		
	            			<div class="row">
	            			<div class="col-md-12">
	            			<br>	
	            			<p>Thank you for using Amazon SES!</p>
	            			<br>
	            			<p>Best regards,</p>
	            			<br>
	            			<p>Noel M.</p>
	            			<p>Amazon Web Services</p>
	            			<p>We value your feedback. Please rate my response using the link below.</p>
	            			<br>
	            			<div class="elem"></div>
	            			</div>	
	            			</div>
	            			<div class="row">
	            				<br><br>
	            				<div class="col-md-12 well">
	            					<p>Was this response helpful? Click here to rate:</p>
	            					<div class="col-md-6 star-rating">
									  <input type="radio" name="rating" value="1"><i></i>
									  <input type="radio" name="rating" value="2"><i></i>
									  <input type="radio" name="rating" value="3"><i></i>
									  <input type="radio" name="rating" value="4"><i></i>
									  <input type="radio" name="rating" value="5"><i></i>
									</div>
									<div class="col-md-6 choice"></div>
	            				</div>	
	            			</div>	
            			</div>
					</div>
				</div>
				</div>
					</div>
					</div>
<script>

function clear_desc_error(){
	document.getElementById( "error_discription" ).innerHTML ="";
}
function check_desc_error(){
	var discription = document.getElementById( "discription" );
	if( discription.value == "" )
	{
	var error = "<small class='text_red'>Discription is Required. </small>";
	document.getElementById( "error_discription" ).innerHTML = error;

	}
}
function validate()
    {

    	var validD=1,validCM=1,validpCC=1;validpNo=1;
	    var error="";
		var discription = document.getElementById( "discription" );
		if( discription.value == "" )
		{
		error = "<small class='text_red'>Discription is Required. </small>";
		document.getElementById( "error_discription" ).innerHTML = error;
		validD=0;
		}
		else{
			document.getElementById( "error_discription" ).innerHTML ="";
			validD=1;
		}
		var contact_method_checked;
		var contact_method = document.getElementsByName( "contact_method" );
		
		for(i=0;i<contact_method.length;i++){
			
			if(contact_method[i].checked){
				contact_method_checked=1;
				if(contact_method[i].value=="phone"){
					var country_code=document.getElementById('country_code').value;
					var phone_number=document.getElementById('phone_number').value;
					if(country_code!=""){
						validpCC=1;
						document.getElementById( "country_code_error" ).innerHTML = ""
					}
					else{
						validpCC=0;
						document.getElementById( "country_code_error" ).innerHTML = "<small class='text_red'>Country Code Is Required</small>";
					}
					if(phone_number!=""){
						if(phone_number.length==10){
							validpNo=1;
							document.getElementById( "phone_number_error" ).innerHTML = ""
						}
						else{
							validpNo=0;
							document.getElementById( "phone_number_error" ).innerHTML = "<small class='text_red'>Phone Number Is 10 digits long</small>"
						}
					}else{
						validpNo=0;
						document.getElementById( "phone_number_error" ).innerHTML = "<small class='text_red'>Phone Number Is Required</small>";
					}
				}
			}
		}
		if(contact_method_checked!=1){
			error = "<small class='text_red'> Contact Method is Required. </small>";
			document.getElementById( "error_contact_method" ).innerHTML = error;
			validCM=0;
		}
		else{
			document.getElementById( "error_contact_method" ).innerHTML ="";
			validCM=1;
		}
		if(validD==1 && validCM==1 && validpNo==1 && validpCC==1){
			return true;
		}
		else{
			return false;
		}
    }
function send_complaint(){
	var form = document.getElementById('add_complaintData'); 
	if(validate()){
		var formData = new FormData(form);
	var xhr = false;
	if (window.XMLHttpRequest) {
	    xhr = new XMLHttpRequest();
	}
	else {
	    xhr = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var obj=document.getElementById('case_id').value;
	 var xhr = new XMLHttpRequest();
			
			if (xhr) {
	            xhr.onreadystatechange = function () {
	                if (xhr.readyState == 4 && xhr.status == 200) {
	                   //alert(xhr.responseText);
	                   document.getElementById('status').innerHTML="Pending Admin Action"
	                   var elementdisput=document.getElementById("disputCaseButton");
	                  if(elementdisput==null){
	                  	var btncontainer=document.getElementById('disButtonContainer');
						var disButton=document.createElement("button");
						disButton.innerHTML = "Mark Disputed";
						disButton.classList.add("btn","btn-danger");
						disButton.setAttribute("id","disputCaseButton");
						disButton.setAttribute("onclick","disputThisComplaint('"+obj+"')");
						btncontainer.parentNode.appendChild(disButton);
	                  }
	                   get_all_thread();
	                  var elementreq=document.getElementById("reopenCaseButton");
	                  if(elementreq!=null){
	                  	
	                  	var btncontainer=document.getElementById('actionButtonContainer');
						var replyButton=document.createElement("button");
						replyButton.innerHTML = "Reply";
						replyButton.classList.add("btn","btn-default","preventDflt");
						replyButton.setAttribute("id","replyCaseButton");
						replyButton.setAttribute("style","float:right;");
						replyButton.setAttribute("onclick","replyThisComplaint('"+obj+"')");
						btncontainer.parentNode.appendChild(replyButton);
						var reopenButton = document.getElementById("reopenCaseButton");
						var closeButton = document.createElement("button");
						closeButton.innerHTML = "Close case";
						closeButton.classList.add("btn","btn-success","preventDflt");
						closeButton.setAttribute("id","closeCaseButton");
						closeButton.setAttribute("onclick","closeThisComplaint('"+obj+"')");
						reopenButton.parentNode.replaceChild(closeButton, reopenButton);
						document.getElementById('replyCaseButton').style.display="";
	                  }
	                  else{
	                  	document.getElementById('replyCaseButton').style.display="";
	                  }

	                }
	            }
	            xhr.open('POST', "<?php echo base_url()?>Account/add_conversation_to_complaint", true);
	            xhr.send(formData);
	        }
	        
	}
	else{
		return false;
	}
	
}



function isNumber(evt) {
	document.getElementById("phone_number_error").innerHTML="";
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
		document.getElementById("phone_number_error").innerHTML="<small class='text_red'>Phone Number Is Max 10 Digits only</small>";
        return false;
    }
    return true;
}
function country_codeFun(){
	document.getElementById("country_code_error").innerHTML="";
}
function replyThisComplaint(obj){
	var elementreq=document.getElementById("replyCaseButton");
	 if(elementreq!=null){
	document.getElementById('replyCaseButton').style.display="none";
	}
	
	var form='<div class="row" id="new_form_container"><div class="col-md-8 col-md-push-4"><form id="add_complaintData" method="post" enctype="multipart/form-data"><div class="form-group"><input type="hidden" id="case_id" name="case_id" value="'+obj+'"><label for="comment">Discription:</label><textarea onfocus="clear_desc_error()" onblur="check_desc_error()" class="form-control" rows="10" name="discription"  required id="discription"></textarea><span class="help-block" id="error_discription"></span></div><div class="form-group"><label for="comment">Attachments:</label><input type="file" name="filesToUpload[]"  id="filesToUpload" multiple="" onchange="makeFileList();"><ul id="fileList"  style="list-style-type:none;"><li></li></ul><span class="help-block">Maximum 3 attachments of size less than 6 mb are uploaded </span></div><div class="form-group"><label for="name">Contact Method:</label><div class="radio"><label><input type="radio" name="contact_method" required   value="phone" onchange="contactMethod()">Phone</label></div><div class="radio"><label><input type="radio" name="contact_method" required  value="web" onchange="contactMethod()">Web</label></div><span class="help-block" id="error_contact_method"></span></div><div id="phone_selector" style="display:none"><div class="form-group"><label for="usr">Country Code:</label><select class="form-control" name="country_code"   id="country_code" required onchange="country_codeFun()"><option value="">Select Country Code</option></select><span class="help-block" id="country_code_error"></span></div><div class="form-group"><div class="row"><div class="col-md-8"><label for="usr">Phone Number:</label><input type="text" class="form-control" name="phone_number"  id="phone_number" onkeypress="return isNumber(event)" maxlength="10"><span class="help-block" id="phone_number_error"></span></div><div class="col-md-4"><div class="form-group"><label for="usr">Extension Number:</label><input type="number" class="form-control" name="ext_number" id="ext_number"></div></div></div></div></div><div class="form-group pull-right"><button type="button" class="btn btn-default preventDflt" onclick="close_reply_form()">Cancel</button>&nbsp;<button type="button" class="btn btn-success preventDflt" id="complaintReplyBtn" onclick="send_complaint()">Reply</button></div></form></div></div>'
		$('#all_message_container').prepend(form);
}

function close_reply_form(){
	var replyCaseButton=document.getElementById("replyCaseButton");
	 if(replyCaseButton!=null){
	document.getElementById('replyCaseButton').style.display="";
	}
var reopenCaseButton=document.getElementById("reopenCaseButton");
	 if(reopenCaseButton!=null){
	document.getElementById('reopenCaseButton').style.display="";
	}

	document.getElementById('new_form_container').remove();
}

function contactMethod(){
	document.getElementById('error_contact_method').innerHTML="";
	var checkedValueForContactMethod = null; 
	var inputElements = document.getElementsByName('contact_method');

	for(var i=0; inputElements[i]; ++i){
	      if(inputElements[i].checked){
	           checkedValueForContactMethod = inputElements[i].value;
	           break;
	      }
	}
	if(checkedValueForContactMethod=="phone"){
		document.getElementById('phone_selector').style.display=""
		document.getElementById('country_code').required=true
		document.getElementById('phone_number').required=true
		
		$.ajax({
            type: "post",
            url: '<?php echo base_url()?>Account/get_all_country_code',
            dataType:"JSON",
            success: function (data) {
            	str="";
            	str+='<option value="">Choose Country Code</option>'
            	for(i=0;i<data.length;i++){
            		str+='<option value='+data[i].country_code+'>'+data[i].country+'</option>'
            	}
                document.getElementById('country_code').innerHTML=str;
            }
		})
	}
	if(checkedValueForContactMethod=="web"){
		document.getElementById('phone_selector').style.display="none"
		document.getElementById('country_code').required=false
		document.getElementById('phone_number').required=false
		//document.getElementById('ext_number').required=false
	}
}

window.onload=get_all_thread();
function get_all_thread(){
	var complaint_id = '<?php echo $complaint_thread_id ?>';
        $.ajax({
            type: "post",
            url: '<?php echo base_url()?>Account/get_all_thread_complaint',
            data: "complaint_id="+complaint_id,
            beforeSend:function(){
            	document.getElementById('all_message_container').innerHTML="<div align='center' style='font-size:2em'><i class='fa fa-refresh fa-5x fa-spin'></i></div>"
            },
            success: function (data) {
                document.getElementById('all_message_container').innerHTML=data;
            }
        });

}
/*
 0=Pending Admin Action;
 1=Pending Customer Action;
 2=closed or Resolved By Customer;
 3=closed or Resolved By Admin;
 4=Disputed;
 */
function reopenThisComplaint(obj){
	document.getElementById('reopenCaseButton').style.display="none";
	replyThisComplaint(obj)
	document.getElementById('complaintReplyBtn').innerHTML="Reopen Case"
}

function makeFileList() {
var input = document.getElementById("filesToUpload");
var ul = document.getElementById("fileList");
while (ul.hasChildNodes()) {
	ul.removeChild(ul.firstChild);
}
if(input.files.length<4){
	var flag=0;
	for (var i = 0; i < input.files.length; i++) {
		if(input.files[i].size<=5242880){
			flag++;
		}
	}
	if(flag==input.files.length){
		flag=0;
		if(input.files.length!=1){
			for (var i = 0; i < input.files.length; i++) {
				var li = document.createElement("li");
				li.innerHTML = input.files[i].name;
				ul.appendChild(li);
			}
		}
	}
	else{
		var li = document.createElement("li");
		li.innerHTML = 'Some of the file size is > 5MB';
		ul.appendChild(li);
		document.getElementById("filesToUpload").value="";
	}
	if(!ul.hasChildNodes()) {
		if(input.files.length!=1){
			var li = document.createElement("li");
			li.innerHTML = 'No Files Selected';
			ul.appendChild(li);
		}
	}
}
else{
	input.value="";
	if(!ul.hasChildNodes()) {
		var li = document.createElement("li");
		li.innerHTML = '<span class="text_red">Maximum Limit is Three</span>';
			ul.appendChild(li);
		}
	}
	
	
}
function resetForm(){
    setTimeout(function(){
        var ul = document.getElementById("fileList");
		while (ul.hasChildNodes()) {
			ul.removeChild(ul.firstChild);
		}
    }, 50);
    return true;
}


function closeThisComplaint(obj){
	//alert(obj)
	document.getElementById("closeCaseButton").classList.remove("btn-success");
	document.getElementById('closeCaseButton').classList.add("btn-info");
	document.getElementById('closeCaseButton').innerHTML='Closing <i class="fa fa-refresh fa-spin" aria-hidden="true"></i>';
	var xhr = false;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    }
    else {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
	 var case_id = obj;
	 var action=2;
	
	 var xhr = new XMLHttpRequest();
			
		if (xhr) {
			
	        xhr.onreadystatechange = function () {
	            if (xhr.readyState == 4 && xhr.status == 200) {
	               if(xhr.responseText==1){
						var closeButton = document.getElementById("closeCaseButton");
						var openButton = document.createElement("button");
						openButton.innerHTML = "Reopen case";
						openButton.classList.add("btn","btn-success","preventDflt");
						openButton.setAttribute("id","reopenCaseButton");
						openButton.setAttribute("onclick","reopenThisComplaint('"+obj+"')");
						closeButton.parentNode.replaceChild(openButton, closeButton);
						document.getElementById("replyCaseButton").remove();
						document.getElementById("disputCaseButton").remove();
						document.getElementById('status').innerHTML="Resolved By Customer"
	               }
	               else{
					document.getElementById("closeCaseButton").classList.remove("btn-info");
					document.getElementById('closeCaseButton').classList.add("btn-danger");
					document.getElementById('closeCaseButton').setAttribute("disabled","disabled");
					document.getElementById('closeCaseButton').innerHTML='Error <i class="fa fa-times" aria-hidden="true"></i>'
	               }
	            }
	        }
	        xhr.open('POST', "<?php echo base_url()?>Account/change_complaint_case_status", true);
	        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	        xhr.send("case_id="+obj+"&action="+action);
	    }
}
function disputThisComplaint(obj){
	document.getElementById("disputCaseButton").classList.remove("btn-danger");
	document.getElementById('disputCaseButton').classList.add("btn-info");
	document.getElementById('disputCaseButton').innerHTML='Closing <i class="fa fa-refresh fa-spin" aria-hidden="true"></i>';
	var xhr = false;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    }
    else {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
	 var case_id = obj;
	 var action=4;
	
	 var xhr = new XMLHttpRequest();
			
		if (xhr) {
			
	        xhr.onreadystatechange = function () {
	            if (xhr.readyState == 4 && xhr.status == 200) {
	               if(xhr.responseText==1){
	               		
	               		var reopenCase=document.getElementById('reopenCaseButton')
	               		if(reopenCase!=null){
	               			document.getElementById('reopenCaseButton').remove();
	               		}
	               	
						var disputButton = document.getElementById("disputCaseButton");
						var openButton = document.createElement("button");
						openButton.innerHTML = "Reopen case";
						openButton.classList.add("btn","btn-success","preventDflt");
						openButton.setAttribute("id","reopenCaseButton");
						openButton.setAttribute("onclick","reopenThisComplaint('"+obj+"')");
						disputButton.parentNode.replaceChild(openButton, disputButton);
						document.getElementById("replyCaseButton").remove();
						document.getElementById("closeCaseButton").remove();
						document.getElementById('status').innerHTML="Disput"
	               }
	               else{
					document.getElementById("disputCaseButton").classList.remove("btn-info");
					document.getElementById('disputCaseButton').classList.add("btn-danger");
					document.getElementById('disputCaseButton').setAttribute("disabled","disabled");
					document.getElementById('disputCaseButton').innerHTML='Error <i class="fa fa-times" aria-hidden="true"></i>'
	               }
	            }
	        }
	        xhr.open('POST', "<?php echo base_url()?>Account/change_complaint_case_status", true);
	        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	        xhr.send("case_id="+obj+"&action="+action);
	    }
}	
	

</script>           
					
			<?php }
			else{
					echo "<h3>No Data to be shown</h3>";
				}
                ?>	
					
            	</div>	
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<script>

function save_star_val(obj){

	var xhr = false;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    }
    else {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
	 var thread_id = obj.getAttribute('thread_id');
	 var star_val=obj.value;
	
	 var xhr = new XMLHttpRequest();

		if (xhr) {

	        xhr.onreadystatechange = function () {
	            if (xhr.readyState == 4 && xhr.status == 200) {
	               if(xhr.responseText==1){

	               }
	            }
	        }
	        xhr.open('POST', "<?php echo base_url()?>Account/save_star_ratings_of_thread", true);
	        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	        xhr.send("thread_id="+thread_id+"&star_val="+star_val);
	    }
	}

function starFinalValue(obj){
	
	var thid=obj.getAttribute('thread_id');
	checked_val="";
		var ratingObj=document.getElementsByName(thid);
		for(i=0;i<ratingObj.length;i++){
			if(ratingObj[i].checked){
				checked_val=ratingObj[i].getAttribute('humaVal');
			}
		}
		if(checked_val!=""){
			document.getElementById("selectedStarVal_"+obj.getAttribute('thread_id')).innerHTML=checked_val;
		}
	

}
function starValue(obj){
	var thid=obj.getAttribute('thread_id');
	document.getElementById('selectedStarVal_'+thid).innerHTML=obj.getAttribute('humaVal');
}
$(document).ready(function(){
	showChar(500);
});
</script>