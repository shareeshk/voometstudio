<style>
.cart_summary .table>tbody>tr>td {
    padding: 10px;
}
.cont {
	display:none;
  /*border-radius: 25px;*/
  border: 1px solid #adadad;
 /* padding: 15px 15px 15px 15px;*/
  background: #E9E9E9;
  overflow: visible;
  box-shadow: 5px 5px 2px #f0ecec;
  position: relative;
  margin: 10px 0px 10px 0px;
}

.x {
    position: absolute;
    background: red;
    color: white;
    top: -10px;
    right: -10px;
	padding: 3px;
	border-radius: 13px;
	width: 20px;
	height: 20px;
	line-height: 10px;
}


</style>
<style>
.more-less {
        float: left;
        color: #212121;
    }
.align_top{
	margin-top:20px;
}
.title_style{
	text-align: center;
	background-color: #e7e7e7;
	padding: 5px;
}

.dl-horizontal dt{
	text-align:left;
}
dd{
	text-align:right;
}
/*a, abbr, acronym, address, applet, article, aside, audio, b, blockquote, big, body, center, canvas, caption, cite, code, command, datalist, dd, del, details, dfn, dl, div, dt, em, embed, fieldset, figcaption, figure, font, footer, form, h1, h2, h3, h4, h5, h6, header, hgroup, html, i, iframe, img, ins, kbd, keygen, label, legend, li, meter, nav, object, ol, output, p, pre, progress, q, s, samp, section, small, span, source, strike, strong, sub, sup, table, tbody, tfoot, thead, th, tr, tdvideo, tt, u, ul, var {
	vertical-align: baseline;
	}*/
	strong{
		color:#666;
	}
	.well{
		background-color:#fff
	}
	.link_active{
		color:#000;
		font-weight:bold ;
	}
	.left-module{
	background-color: #fff;
	}
	.columns-container{
	background-color: #f9f9f9;
	}
	sub{
		vertical-align: baseline !important;
	}
	.detailed_order_item li {
	    float: left;
		margin-left:2px;
		padding:0px !important;
	}		
	.detailed_order_item ul
	{
	list-style-type: none;
	padding:0px !important;
	}
	.stage_order_cancelled{
		color:red
	}
	.stage_order_completed{
		color:green
	}
	.text_red{
		color:red;
		font-weight:400;
	}
	.view-more a {
    border: 1px solid #eaeaea;
    padding: 10px 12px;
    background: #eee;
	}
	.view-more a:hover {
    background: #382e03;
    color: #fff;
}
.border_bottom{
	border-bottom: 1px solid red;
}
.cursor_pointer{
	cursor:pointer;
	text-decoration: underline;
}
.separator_line{
	border-right: 1px solid #ccc;
	margin-left:15px;
	margin-right:20px;
}
.order_status_line1{
	border-right: 1px solid #ccc;
	width:1px;
	height:5vh;
	right:12px;
}
.order_status_line2{
	border-right: 1px solid #ccc;
	width:1px;
	height:5vh;
	margin-left:1em;
}
.order_status_line3{
	border-right: 1px solid #ccc;
	width:1px;
	height:5vh;
	left:-30px;
}
.no-border li{
	border-bottom:none;
}
.top_space{
	margin-top:4px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;     
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
    
}


.stepwizard-step {    
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}

.panel-title{
	font-size:1.5rem;
}
.headingtext{
	font-size:1.5rem;
}
.price_details_1 + .popover {
   left: 911.11px !important;
}
.price_details_1 + .popover .arrow {
  left: 94% !important;
}
.price_details_replace + .popover {
   left: -90px !important;
   max-width:280px !important;
}
.price_details_replace + .popover .arrow {
  left: 94% !important;
}
.rep_amount + .popover {
   max-width:360px !important;
}
.admin_rep_amount + .popover {
   max-width:432px !important;
}
.cancel_rep_refund_details + .popover {
   max-width:386px !important;
}
.price_details_2 + .popover {
   left: 1023px !important;
}
.price_details_2 + .popover .arrow {
  left: 88% !important;
}
.price_details_3 + .popover {
   left: -85px !important;
}
.price_details_3 + .popover .arrow {
  left: 94% !important;
}
.refund_customer_request + .popover {
   max-width:286px !important;
}
.refund_admin_reply + .popover {
   max-width:393px !important;
}
.refund_details_initial_trans + .popover {
   max-width:300px !important;
}
.second_refund_request + .popover {
   max-width:300px !important;
}
.second_trans_details + .popover {
   max-width:360px !important;
}
.second_refund_admin_accep + .popover {
   max-width:300px !important;
}
.price_details_rsn + .popover {
   max-width:292px !important;
}
.bank_detail_replace_refund + .popover{
	max-width:400px !important;
}
.bank_detail_replace_refund1 + .popover{
	max-width:400px !important;
}
.bank_detail_replace_refund2 + .popover{
	max-width:400px !important;
}
.bank_detail_replace_refund3 + .popover{
	max-width:400px !important;
}
.bank_detail_second_refund + .popover{
	max-width:400px !important;
}
.bank_detail_stock_return + .popover{
	max-width:400px !important;
}
</style>
<script type="text/javascript">

function free_surprise_gift_fun(order_id,status){
	
$.ajax({
	type:'POST',
	url:'<?php echo base_url(); ?>Account/update_customer_status_surprise_gifts_on_invoice',
	data:"order_id="+order_id+"&status="+status,
	success:function(data){
		
		/*bootbox.alert({
			  size: "small",
			  message: 'successfully submited',
			  callback: function () { location.reload(); }
			});	*/
        alert('successfully submited');
		location.reload();
	}
});
}
function invoice_cash_back_fun(order_id,status,total_amount_cash_back=''){
	
$.ajax({
	type:'POST',
	url:'<?php echo base_url(); ?>Account/update_customer_response_for_invoice_cashback',
	data:"order_id="+order_id+"&status="+status+"&total_amount_cash_back="+total_amount_cash_back,
	success:function(data){
		
		/*bootbox.alert({
			  size: "small",
			  message: 'successfully submited',
			  callback: function () { location.reload(); }
			});*/
        alert('successfully submited');
		location.reload();
	}
});

}

</script>

<?php /*?><?php */?>

<?php
$you_saved_fin=0;$inc_txt='';$total_promo_discount_price=0;$promo_dis=0;$sku_coupon_used=0;
if(!empty($invoice_offers)){
                            
   foreach($invoice_offers as $data){
        $promotion_invoice_cash_back=$data["promotion_invoice_cash_back"];
        $promotion_invoice_free_shipping=$data["promotion_invoice_free_shipping"];
        $promotion_invoice_discount=$data["promotion_invoice_discount"];
        $total_amount_cash_back=$data["total_amount_cash_back"];
        $total_amount_saved=$data["total_amount_saved"];
        $total_amount_to_pay=$data["total_amount_to_pay"];
        $payment_method=$data["payment_method"];
        $invoice_offer_achieved=$data["invoice_offer_achieved"];
		
        $invoice_surprise_gift_promo_quote=$data["invoice_surprise_gift_promo_quote"];
        $invoice_surprise_gift=$data["invoice_surprise_gift"];
        $invoice_surprise_gift_type=$data["invoice_surprise_gift_type"];
        $invoice_surprise_gift_skus=$data["invoice_surprise_gift_skus"];
        $invoice_surprise_gift_skus_nums=$data["invoice_surprise_gift_skus_nums"];
        $customer_response_for_surprise_gifts=$data["customer_response_for_surprise_gifts"];
		$invoices_offers_id=$data["id"];
        $customer_response_for_surprise_gifts=$data["customer_response_for_surprise_gifts"];
        $customer_response_for_invoice_cashback=$data["customer_response_for_invoice_cashback"];
        $status_of_refund_for_cashback=$data["status_of_refund_for_cashback"];
        $status_of_refund_for_surprise_gifts=$data["status_of_refund_for_surprise_gifts"];
        $cart_quantity=$data["cart_quantity"];
        $total_price_without_shipping_price=$data["total_price_without_shipping_price"];
        $total_shipping_charge=$data["total_shipping_charge"];
        $total_coupon_used_amount=$data["total_coupon_used_amount"];
	$invoice_coupon_status=$data["invoice_coupon_status"];
	$invoice_coupon_status=$data["invoice_coupon_status"];
	$invoice_coupon_reduce_price=round($data["invoice_coupon_reduce_price"]);
        if($invoice_coupon_status=='1'){
            $inc_txt.='(';
            $inc_txt.=($data["invoice_coupon_type"]=='2') ? curr_sym : ''.'';
            $inc_txt.=round($data["invoice_coupon_value"]);
            $inc_txt.=($data["invoice_coupon_type"]=='1') ? '%' : 'Flat' ;
            $inc_txt.=')';
        }
		$inv_addon_products_amount=$data["inv_addon_products_amount"];	
        if($promotion_invoice_free_shipping>0 && $invoice_offer_achieved==1){
                $shipping_charge_waived="yes";
        }else{
                $shipping_charge_waived="no";
        }
   }

}
?>

<div class="columns-container">
    <div class="container" id="columns">
      <ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account/my_order">My Orders</a></li>
		  <li class="breadcrumb-item active">Order Details</li>
		</ol>
        <div class="row">
            <div class="center_column col-xs-12 col-sm-12 my_orders_links" id="center_column">
		                <h2 class="page-heading">
		                    <span class="page-heading-title2">Order Details</span>
		                </h2>
						<div class="row margin-top margin-bottom">
							<div class="col-sm-10">
							<?php $date=$controller->get_order_placed_date($order_id); ?>
							 Ordered on <?php echo date("j F Y",strtotime($date));?> <i class="separator_line"></i>  Order# <?php echo $order_id; ?> 
							</div>
							<!--<div class="col-sm-2 text-right">
							<a class="cursor-pointer" data-trigger="click" data-placement="bottom" data-toggle="popover" data-container="body" type="button" data-html="true" id="invoice">
							<div id="popover-content-invoice" class="hide">
							<ul class="list-unstyled">
							<li class="text-primary small">Request Invoice</li>
							<li class="text-primary small">Printable Order Summary</li>
							</ul>
							</div>
							Invoice <i class="fa fa-caret-down" aria-hidden="true"></i></a>
							</div>-->
						</div>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-body">
								<div class="row margin-bottom">
									<div class="col-md-3 col-xs-12">  
									<?php $shipp_obj=$controller->get_shipping_address_of_order($order_id); ?>
									<?php if(!empty($shipp_obj)){ ?>
									<div class="f-row text-capitalize margin-top">
										<strong>Shipping Address</strong>
									</div>
									<div class="f-row normal-font">
										<ul class="list-unstyled">
										  <li class="text-capitalize"><?php echo $shipp_obj->customer_name; ?></li>
										  <li><?php echo $shipp_obj->address1; ?></li>
										  <li><?php echo $shipp_obj->address2; ?></li>
										  <li class="text-uppercase"><?php echo $shipp_obj->city; ?>, <?php echo $shipp_obj->state; ?> <?php echo $shipp_obj->pincode; ?></li>
										  <li><?php echo $shipp_obj->country; ?> <?php echo $shipp_obj->mobile; ?></li>
										</ul>
									</div>
									<?php } ?>
									</div>
									<div class="col-md-5 col-xs-12">
										<div class="f-row text-capitalize normal-text margin-top">
											<strong>Payment Method</strong>
										</div>
										<div class="f-row normal-font">							
											<ul class="list-unstyled">
											  <li class="text-capitalize"><?php  echo $payment_method; ?></li>
											  <li>This Payment method subject to availability of payment device.Please check with the delivery agent.</li> 
											</ul>
										</div>
									</div>
									<div class="col-md-4 col-xs-12">
										<div class="f-row text-capitalize normal-text margin-top">
											<strong>Order Summary</strong>
										</div>
																
											<dl class="dl-horizontal">
												<dt>Item(s) Subtotal</dt> 
									<dd><?php echo curr_sym;?><?php echo round((intval($total_price_without_shipping_price))-intval($promotion_invoice_discount)); ?></dd>
											
												<dt>Shipping</dt> 
												<dd>
												<?php
													if($promotion_invoice_free_shipping>0){
												?>
													<?php echo curr_sym."0"; ?>
												<?php }else{ ?>
													<?php echo curr_sym.round($total_shipping_charge); ?>
												<?php } ?>	
												</dd>
											
												<dt>Total Coupon used </dt> 
                                                                                                <dd><?php echo curr_sym;?><?php echo round($total_coupon_used_amount); ?></dd>
<!--												<dt>Total</dt> 
												<dd><?php echo curr_sym;?><?php echo round($total_amount_to_pay); ?></dd>-->
												<dt><strong>Grand Total</strong></dt> 
												<dd><?php echo curr_sym;?><?php echo round($total_amount_to_pay); ?></dd>
											</dl> 					
									</div>
								</div>
							</div>
							</div>
						</div>
		                <?php require_once 'invoice_promo_details.php'; ?>
			
						<div class="row margin-top margin-bottom">
						<div class="col-md-12">
						<div class="table-responsive">
						<table class="table table-bordered cart_summary mb-0">
						<thead>
						<tr>
						<th style="vertical-align: middle;" colspan="2">Product Details</th>
						<th style="vertical-align: middle;">Order Status</th>
						<th style="vertical-align: middle;">Logistics & delivery</th>
						<th style="vertical-align: middle;">Subtotal</th>
						</tr>
						</thead>
						<tbody style="background-color: #fff;">
						<?php 
$subtotal_cal=0;
	foreach($all_order_items as $order_items_status){
		
		foreach($order_items_status as $order_items){

			$all_order_items1=$controller->get_all_order_items($order_items['order_item_id'],$order_id);
			
			//get_order_summary_mail_data($order_id)

			if(!empty($all_order_items1)){

				foreach($all_order_items1 as $order_item){

					//print_r($order_item);
					//exit;

					$total_promo_discount_price=0;$grand_total_default_discount_price=0;
                                    
					$quantity=$order_item['quantity'];
					$order_item_id=$order_item['order_item_id'];
					$shipping_charge=$order_item['shipping_charge'];
					$inventory_id=$order_item['inventory_id'];
					if($quantity!=0){
						$shipping_charge_per_quantity=round(floatval($shipping_charge/$quantity),2);
					}
					$product_price=$order_item['product_price'];	
					
					

					$inv_obj=$controller->get_details_of_products($inventory_id);
					
								if($promotion_invoice_discount>0){
									$subtotal=$order_item["subtotal"];
									$invoice_discount=($subtotal/$total_price_without_shipping_price)*100;					
									$each_invoice_amount=$promotion_invoice_discount*$invoice_discount/100;
									//$invoice_amount=round($promotion_invoice_discount-$each_invoice_amount,2);
									$order_item_invoice_discount_value=round($each_invoice_amount,2);
									$order_item_invoice_discount_value_each=round($order_item_invoice_discount_value/$quantity,2);
									//$order_item_invoice_discount_value=round($promotion_invoice_discount/$cart_quantity,2);
                                ?>
								
								<input type="hidden" value="<?php echo $order_item_invoice_discount_value; ?>" name="order_item_invoice_discount_value" id="order_item_invoice_discount_value_<?php echo $order_item_id;?>">
								

								<?php 
								}else{
									$order_item_invoice_discount_value=0;
									$order_item_invoice_discount_value_each=0;
								} ?>
										
								<input type="hidden" value="<?php echo (isset($order_item_invoice_discount_value_each))? $order_item_invoice_discount_value_each : 0; ?>" name="order_item_invoice_discount_value_each" id="order_item_invoice_discount_value_each">
								
			<tr id="<?php echo $order_item_id; ?>">
			<td style="width:10%;">
													<a href="#">
														<img src="<?php echo base_url().$inv_obj->thumbnail; ?>" alt="SPECIAL PRODUCTS">
	                                        		</a>
												</td>
												<td style="width:25%;">
													<div class="product-name f-row">
													
													<?php 
			//$product_name=$controller->get_product_name($order_item['product_id']);
                        if($order_item['ord_sku_name']!=''){
                            $product_name=$order_item['ord_sku_name'];
                        }else{
                            $product_name=$controller->get_product_name($order_item['product_id']);
                        }
			//$product_name=$order_item['ord_sku_name'];
			
			?>
			<form target="_blank" id="write_review_<?php echo $order_items['order_item_id']; ?>" action="<?php echo base_url()."write_review" ?>" method="post">
			<input type="hidden" value="<?php echo $order_item['inventory_id']; ?>" name="inv_id" id="inv_id">
			<input type="hidden" value="<?php echo $product_name; ?>" name="pro_name">
			<input type="hidden" value="<?php echo $this->session->userdata('customer_id') ?>" name="customer_id" id="customer_id">
			<input type="hidden" value="1" name="rate_order" id="rate_order">
			<input type="hidden" value="<?php echo $order_items['order_item_id']; ?>" name="rate_order_order_item_id" id="rate_order_order_item_id">
			</form>
															<?php
																$orders_status_data=$controller->get_orders_status_data($order_item['order_item_id']);
																if($orders_status_data["type_of_order"]=="replaced"){
																	echo "<font color='red'>Replacement </font><br>";
																}	
															?>

															<?php //echo $orders_status_data["type_of_order"]; ?>
				                                            <a href="<?php echo base_url()?>detail/<?php echo $controller->sample_code().$order_item['inventory_id'];?>"><?php echo $order_item['ord_sku_name'];?></a>	
															<?php
															$product_details=get_product_details($order_item['product_id']);
															if(!empty($product_details)){
																echo ($product_details->product_description!='') ? '<div class="mb-10">'.$product_details->product_description.'</div>': '';
															} 
															
															?>
															
													

				                                    </div>
													
				<div class="f-row small-text">
										
						<?php
					//$inv_obj->attribute_1_value= substr($inv_obj->attribute_1_value, 0, strpos($inv_obj->attribute_1_value, ":"));
					
					if(!empty($order_item['attribute_1']))
						echo '<div class="f-row">'.$order_item['attribute_1']." : ".$order_item['attribute_1_value'].'</div>';
					// if(!empty($inv_obj->attribute_2))
					// 	echo '<div class="f-row">'.$inv_obj->attribute_2." : ".$inv_obj->attribute_2_value.'</div>';
					// if(!empty($inv_obj->attribute_3))
					// 	echo '<div class="f-row">'.$inv_obj->attribute_3." : ".$inv_obj->attribute_3_value.'</div>';
					// if(!empty($inv_obj->attribute_4))
					// 	echo '<div class="f-row">'.$inv_obj->attribute_4." : ".$inv_obj->attribute_4_value.'</div>';
					?>
				
				</div>
				<div class="f-row small-text">SKU : <?php echo $order_item['sku_id']; ?></div>
				<div class="f-row small-text">
					Quantity : <?php echo $quantity; ?>
				</div>
				
												

			</td>
			<td style="width:35%;">
			<div class="products-block detailed_order_item" style="width: 380px;">
											<div class="stepwizard">
												<div class="stepwizard-row">
												<?php echo $controller->get_order_details_in_steps_status($order_item['order_item_id'])?>
												</div>
												
											</div>
											
										</div>
			</td>
			<div class="f-row col-sm-12 cont" id="cont_<?php echo $order_item_id; ?>">
										<button class = "x" content_id="order_status_<?php echo $order_item_id;?>" onclick="hide_order_status(this,<?php echo $order_item_id;?>)">
											<i class="fa fa-times" aria-hidden="true"></i>
										</button>
										<div id="order_status_<?php echo $order_item_id;?>">
										</div>
										
										</div>
	<?php
			
	$get_refund_data=$controller->get_order_details_admin_acceptance_refund_data($order_item['order_item_id']);
	$get_replacement_data=$controller->get_order_details_admin_acceptance_refund_data_for_replacement($order_item['order_item_id']);
	?>
			<td style="width:20%;" align="right">
			<?php if($order_item['tracking_number']!=""){ ?>
				<div class="f-row"> <?php echo $order_item['logistics_name'] ?> <br></div><div class="f-row"><a href="<?php echo $order_item['logistics_weblink'] ?>" target="_blank">Track Your Shipment on tracking number : <b><?php echo $order_item['tracking_number']; ?></b></a> </div>
											<?php }else{ 
												
												$del='';
												$del_close='';
												if(!empty($ord_status)){
													if($ord_status["order_cancelled"]=='1'){
														$del='<del>';
														$del_close='</del>';
													}
												}
											
											?>
											
											<div class="f-row"> <?php echo $order_item['logistics_name'] ?> - <?php echo "Not yet Shipped" ?></div>
											<?php 
											echo $del;
											?>
											<div class="f-row">You can access Tracking number, once the order is shipped.</div>

											<?php echo $del_close; ?>

											<?php } ?>
											<?php echo $controller->get_order_details_order_status_achieved($order_item['order_item_id']) ?>
			</td>
			<!-----promo_popover------------------>
<!----free items---------->
<?php
	$discount_saved=0;
	$order_item_id=$orders_status_data["order_item_id"];
	$order_delivered=$orders_status_data["order_delivered"];
	
	$promotion_item=$order_item["promotion_item"];
	$promotion_item_num=$order_item["promotion_item_num"];
	
	$promotion_surprise_gift_skus=$order_item["promotion_surprise_gift_skus"];
	$promotion_surprise_gift_skus_nums=$order_item["promotion_surprise_gift_skus_nums"];
	$promotion_surprise_gift=$order_item["promotion_surprise_gift"];
	$promotion_surprise_gift_type=$order_item["promotion_surprise_gift_type"];
	
		if($promotion_item!='' || ($promotion_surprise_gift_skus!='' && $order_delivered==1)){
						
				$quantity=$order_item['quantity'];
				$promotion_minimum_quantity=$order_item['promotion_minimum_quantity'];
				
				$promo_item=rtrim($promotion_item,',');
				$promotion_item_num=rtrim($promotion_item_num,',');
				
				$promo_item_arr = explode(',', $promo_item);
				$promo_item_num_arr = explode(',', $promotion_item_num);
				
				$two=array_combine($promo_item_arr,$promo_item_num_arr);
				
				$temp_quotient=intval($quantity/$promotion_minimum_quantity);
				
				//Surprise
				
				$promotion_surprise_gift_skus=rtrim($promotion_surprise_gift_skus,',');
				$promotion_surprise_gift_skus_nums=rtrim($promotion_surprise_gift_skus_nums,',');
				
				$gift_skus_arr = explode(',', $promotion_surprise_gift_skus);
				$gift_skus_nums_arr = explode(',', $promotion_surprise_gift_skus_nums);
				
				$gift_with_count=array_combine($gift_skus_arr,$gift_skus_nums_arr);
				if($promotion_item!=''){
					$free_items_arr=$controller->get_free_skus_from_inventory_with_details($promotion_item,$promotion_item_num);
				}
				if($promotion_surprise_gift_type=='Qty'){
					$free_surprise_items=$controller->get_free_skus_from_inventory_with_details($promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums);
				}
}
?>
<!-----------free items------->
			<td align="right" style="width:10%;">
										<input type="hidden" id="price_paid" price_paid="<?php echo $order_item['product_price'];?>">
										

 
									<?php if($orders_status_data["type_of_order"]==""){ ?>	

										<p class="text-muted">
										<?php if($promotion_invoice_free_shipping>0){
											echo curr_sym.round($order_item['subtotal']);
											$subtotal_cal+=round($order_item['subtotal']);
										}else{
											echo curr_sym.round($order_item['grandtotal']);
											$subtotal_cal+=round($order_item['grandtotal']);
										}
										?>
										
					
                                                  <a data-trigger="click" data-placement="bottom" data-toggle="popover" data-html="true" id="price_details_1_<?php echo $order_item_id;?>" content_id="#content_price_details_1_<?php echo $order_item_id; ?>" class="price_details_1 cursor-pointer" onclick="toggle_content(this)">[<i class="fa fa-question" aria-hidden="true" ></i>]</a> 
												  
							<div id="content_price_details_1_<?php echo $order_item_id; ?>" style="display:none;">
							<table class="table table-bordered" style="font-size:0.8em;text-align:justify;margin-bottom: 0px;">
                                                    <thead><tr><th style="padding:0px"></th><th width="25%" style="padding:0px"></th><th style="padding:0px" width="30%" style="text-align: right"></th></tr></thead>


													<?php
													//$order_item["ord_addon_products_status"]=='1'
														if(0){
													
														
															//$total_amount_normal+=round($order_item["ord_addon_total_price"]);
															$addon=json_decode($order_item["ord_addon_products"]);
															//print_r($addon);
															$addon_count=count($addon);
															
															?>
															<tr>
															<td>Total Price</td>
															<td><?php echo $addon_count.' items';?></td>
															<td class="text-right" ><?php echo curr_sym; ?><?php echo round($order_item["subtotal"]); ?></td>
															</tr>

															<tr>
															<td colspan="3">Applied Discount <?php echo round($order_item["ord_selling_discount"]); ?>%</td>
															</tr>

															<tr>
															<td colspan="2">Shipping Charge</td>
															<td class="text-right" ><?php echo curr_sym; ?><?php echo round($order_item["shipping_charge"]); ?></td>
															</tr>

															<tr>
															<td colspan="2">Grand Total</td>
															<td class="text-right" ><?php echo curr_sym; ?><?php echo round($order_item["ord_addon_total_price"]); ?></td>
															</tr>
												
													<?php 
														
														}else{

															?>


                                            <!---normal items popup------>
                                            
<?php
											
													$total_product_price_normal=($order_item["quantity"]*round($order_item["ord_max_selling_price"]));
													$shipping_price_normal=$order_item["shipping_charge"];
													

													$total_amount_normal=($shipping_price_normal+$total_product_price_normal);
													
	
		?>
													
													<tr><td>Base product price</td><td><?php echo $order_item['quantity']; ?> x <?php echo round($order_item['ord_max_selling_price']);?></td><td class="text-right"><?php echo curr_sym;?><?php echo $total_product_price_normal; ?></td></tr>
													
													<tr><td colspan="2">Shipping Charge for <?php echo $order_item['quantity']; ?> unit(s)</td><td class="text-right"><?php echo curr_sym;?><?php echo $shipping_price_normal; ?></td></tr>
													
													<tr><td colspan="2">Total value </td><td class="text-right"><?php echo curr_sym;?><?php echo $total_amount_normal; ?></td></tr>
                                                
                                        <?php if($order_item['promotion_available']==1){ ?>   
										
                                                <?php if(intval($order_item['quantity_without_promotion'])>0 && $order_item['default_discount']>0){ ?>
												
												<?php
												
												$default_discount_price=floatval($order_item['ord_max_selling_price']-$order_item['individual_price_of_product_without_promotion']);
		// $default_discount_price=(($order_item['product_price']*$order_item['default_discount'])/100);
		$total_default_discount_price=($order_item['quantity_without_promotion']*$default_discount_price);
                $grand_total_default_discount_price+=intval($total_default_discount_price);
		$total_item_price_with_base_price=($order_item['product_price']*$order_item['quantity_without_promotion']);
		
												?>
                                                <tr><td> Discount @ (<?php echo $order_item['default_discount'] ?>%)<br>Eligible Quantity : <?php echo $order_item['quantity_without_promotion'] ?><br>For promotion <br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_default_discount_promo']; ?></small></td><td><?php echo $order_item['quantity_without_promotion'] ?> <i class="fa fa-times" aria-hidden="true"></i> <?php echo  $default_discount_price ?></td><td class="text-right" style="color:red"> <?php echo curr_sym;?><?php echo $total_default_discount_price;?> </td></tr>
												
												
                                                <?php }?>
                                                
                                                <?php if(intval($order_item['quantity_with_promotion'])>0){ ?>
                                                 
                                                 <?php if(intval($order_item['promotion_discount'])>0&& (intval($order_item['promotion_discount'])==intval($order_item['default_discount']))){?>
												 
												 <?php
												 
												$default_discount_price=floatval($order_item['ord_max_selling_price']-$order_item['individual_price_of_product_with_promotion']);
		// $default_discount_price=(($order_item['product_price']*$order_item['promotion_discount'])/100);
		$total_default_discount_price=($order_item['quantity_with_promotion']*$default_discount_price);
                $grand_total_default_discount_price+=intval($total_default_discount_price);
		$total_item_price_with_base_price=($order_item['product_price']*$order_item['quantity_with_promotion']);
		
												 ?>
                                                     <tr><td> Discount @ (<?php echo $order_item['promotion_discount'] ?>%) <br>Eligible Quantity : <?php echo $order_item['quantity_with_promotion'] ?><br>For promotion<br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_quote']; ?></small></td><td><?php echo $order_item['quantity_with_promotion'] ?> <i class="fa fa-times" aria-hidden="true"></i> <?php echo  $default_discount_price ?></td><td class="text-right" style="color:red"> <?php echo curr_sym;?><?php echo $total_default_discount_price;?> </td></tr>
													 
                                                 <?php }?>
                                                 
                                                 <?php if(intval($order_item['promotion_discount'])>0 && (intval($order_item['promotion_discount'])!=intval($order_item['default_discount']))){?>
												 
												 <?php
												 
												 $promo_discount_price=floatval($order_item['ord_max_selling_price']-$order_item['individual_price_of_product_with_promotion']);
												
		//$promo_discount_price=(($order_item['product_price']*$order_item['promotion_discount'])/100);
		$total_promo_discount_price=($order_item['quantity_with_promotion']*$promo_discount_price);
		$total_item_price_with_base_price=($order_item['product_price']*$order_item['quantity_with_promotion']);
		
												 ?>
                                                      <tr><td>Promotion Discount @ (<?php echo $order_item['promotion_discount'] ?>%)<br>Eligible Quantity : <?php echo $order_item['quantity_with_promotion'] ?><br>For Promotion<br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_quote']; ?></small></td><td><?php echo $order_item['quantity_with_promotion'] ?> <i class="fa fa-times" aria-hidden="true"></i> <?php echo $promo_discount_price; ?></td><td class="text-right" style="color:red;"><?php echo curr_sym;?><?php echo $total_promo_discount_price;?> </td></tr>
                                                 <?php }?>   
                                                    
                                                  <?php if(intval($order_item['promotion_discount'])==0 && $order_item['promotion_default_discount_promo']=="Price for remaining"){ ?>
												  
												  <?php if(intval($order_item['default_discount']>0)){ ?>
												  
                                                     <tr><td>Standard MRP @ (<?php echo $order_item['product_price'] ?>) <br>Eligible Quantity : <?php echo $order_item['quantity_with_promotion'] ?><br>For Promotion<br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_quote']; ?></small></td><td><?php echo $order_item['quantity_with_promotion'] ?> <i class="fa fa-times" aria-hidden="true"></i> <?php echo $order_item['product_price'] ?></td><td class="text-right"><?php echo curr_sym;?><?php echo $order_item["total_price_of_product_with_promotion"];?> </td></tr>
													 
												  <?php }else{
													  ?>
													   <tr><td colspan="3">For Promotion<br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_quote']; ?></small></td></tr>
													  <?php
												  }  ?>
                                                 <?php }?>  
                                                    
                                                
                                                <?php }?>
                                                
                                                <?php if(intval($order_item['quantity_without_promotion'])>0 && $order_item['default_discount']==0){ ?>
                                               <!-- no quote and no default promotion---->
								<?php /*?>											   <tr><td>Standard MRP @ (<?php echo $order_item['product_price'] ?>) <br>Eligible Quantity : <?php echo $order_item['quantity_without_promotion'] ?><br></td><td><?php echo $order_item['quantity_without_promotion'] ?>*<?php echo $order_item['product_price'] ?></td><td class="text-right"><?php echo curr_sym;?><?php echo $order_item["total_price_of_product_without_promotion"];?> </td></tr>
								<?php */?>
																				
																				<?php }?>
																				
																				<?php
													if($promotion_item!='' || ($promotion_surprise_gift_skus!='' && $order_delivered==1)){
															if($promotion_item!=''){

																foreach($free_items_arr as $free_items){
																	?>
																	<tr><td><?php echo "SKU : ".$free_items->sku_id."(free)"; ?></td><td><?php $id=$free_items->id;
																		echo "Quantity ".($temp_quotient*$two[$id]); ?></td><td class="text-right"><del><?php echo curr_sym.(($temp_quotient*$two[$id])*$free_items->selling_price); ?></del></td></tr>
																	<?php
																}
															}

															if($promotion_surprise_gift_type=='Qty'){

																foreach($free_surprise_items as $surprise_items){
																	?>
																	<tr><td><?php echo "SKU : ".$surprise_items->sku_id."(free)"; ?></td><td><?php $id=$surprise_items->id;
																		echo "Quantity ".($gift_with_count[$id]); ?></td><td class="text-right"><del><?php echo curr_sym.($gift_with_count[$id]*$surprise_items->selling_price); ?></del></td></tr>
																	<?php
																}
															}
													}
												?>
												
							
                                                        <!---added --->
                                                 <?php
                                                 //&& $order_item['default_discount']>0 && $order_item['promotion_quote']==$order_item['promotion_default_discount_promo']
                                                 if($order_item['promotion_default_discount_promo']!="" ){
                                                     /* straight discount */
                                                     
                                                 }else{
                                                     if($order_item['ord_selling_discount']>0){
                                                     
                                                         ?>
                                                <tr><td colspan="2">Discount @ (<?php echo round($order_item['ord_selling_discount']) ?>%)<br>Eligible Quantity : <?php echo $order_item['quantity'] ?><br></td><td class="text-right"><?php echo curr_sym; ?><?php echo round($order_item['quantity']*($order_item['ord_max_selling_price']-$order_item['product_price']));
                                                
                                                $discount_saved=round($order_item['quantity']*($order_item['ord_max_selling_price']-$order_item['product_price']));
                                                ?></td></tr>
                                                        <?php 
                                                         
                                                     }
                                                     
                                                 }
                                                 
                                                 
                                                 ?>
                                                 <!---added --->
                                                
                                              <?php 
                                              
                                              //promo available ;
                                                } else { 
                                                
                                                     if($order_item['ord_selling_discount']>0){
                                                     
                                                         ?>
                                                <tr><td colspan="2">Discount @ (<?php echo round($order_item['ord_selling_discount']) ?>%)<br>Eligible Quantity : <?php echo $order_item['quantity'] ?><br></td><td class="text-right"><?php echo curr_sym; ?><?php echo round($order_item['quantity']*($order_item['ord_max_selling_price']-$order_item['product_price']));
                                                
                                                $discount_saved=round($order_item['quantity']*($order_item['ord_max_selling_price']-$order_item['product_price']));
                                                        
                                                ?></td></tr>
                                                        <?php 
                                                         
                                                     }
                                                      
                                                 } ?>
                                                    
                                              
                                                <!----sku coupon--->
                                                <?php
                                                
                                                $sku_coupon_used=0;
                                                if($order_item['ord_coupon_status']=='1'){ 
                                                    
                                                    $sku_coupon_used=round($order_item['ord_coupon_reduce_price']);
                                                    ?>
                                                
                                                <tr><td colspan="2">Coupon applied on SKU <b>(<?php echo ($order_item['ord_coupon_type']=='2') ? curr_sym : ''; ?><?php echo round($order_item['ord_coupon_value']) ?><?php echo ($order_item['ord_coupon_type']=='1') ? '%' : 'Flat'; ?>)</b>
                                                        
                                                        </td><td class="text-right"><?php echo curr_sym; ?><?php echo round($order_item['ord_coupon_reduce_price']); ?></td></tr>
                                                
                                                <?php } ?>
                                                <!----sku coupon--->
                                                
                                              <tr>
                                                <?php 
												
												if($promotion_invoice_free_shipping>0){   ?>
			
													<td>Shipping Charge 
													( <span style="color:red">waived off</span> )
													</td>
                                                <td><?php echo $order_item['quantity'] ?> units(s)</td>
                                                <td class="text-right" style="color:red">

                                                    <?php echo curr_sym;?><?php echo $order_item["shipping_charge"]; ?>
                                                
                                                </td>
													<?php
												} ?>

  
                                                </tr>
                                                
                                                <?php
												
										if($order_item["cash_back_value"]!="" && $order_item["cash_back_value"]!=0){                                          
			  ?>
                                               <tr>
                                                 
                                                <td>Cashback value of <?php echo $order_item['promotion_cashback'] ?> % <br><em>(credited to wallet) </em></td>
                                                <td><?php echo $order_item['quantity'] ?> <i class="fa fa-times" aria-hidden="true"></i>  <?php echo round($order_item["cash_back_value"]/$order_item['quantity'])?></td>
                                                <td class="text-right" ><?php echo curr_sym;?><?php echo $order_item["cash_back_value"]; ?></td>
                                                </tr>
                                                
                                                <?php 
												
												}
												?>
                                                

                                                <!---minus--->
                                                <?php 
                                                $minustext='';
                                                
                                                if($discount_saved>0 || $sku_coupon_used>0 || $promotion_invoice_free_shipping>0){
                                                    $minustext='(';
                                                    $minustext.=curr_sym.$total_amount_normal;
                                                    if(intval($discount_saved)>0){
                                                        $minustext.='-'.curr_sym.$discount_saved;
                                                    }
                                                    if($total_promo_discount_price>0){
                                                        $minustext.='-'.curr_sym.$total_promo_discount_price;
                                                        $promo_dis+=round($total_promo_discount_price);
                                                    }
                                                    if(intval($grand_total_default_discount_price)>0){
                                                        $minustext.='-'.curr_sym.$grand_total_default_discount_price;
                                                        $promo_dis+=round($grand_total_default_discount_price);
                                                    }
                                                    if($sku_coupon_used>0){
                                                        $minustext.='-'.curr_sym.$sku_coupon_used;
                                                    }

													if($promotion_invoice_free_shipping>0){
														$minustext.='-'.curr_sym.$order_item["shipping_charge"];
													}

                                                    $minustext.=')'; 
                                                }
                                               
                                                ?>
                                                <!---minus--->
                                                
                                                <?php if($promotion_invoice_free_shipping>0){ ?>
                                                  
												  <tr><td colspan="2"><em><b>You Pay </b></em><?php echo $minustext; ?></td><td class="text-right"><b><?php echo curr_sym;?><?php echo ($order_item["subtotal"] - $sku_coupon_used);?></b></td></tr>
												  
                                                <?php }else{ ?>
                                                                                       
                                                                                                  
                                                    <tr><td colspan="2"><em><b>You Pay </b></em><?php echo $minustext; ?></td><td class="text-right"><b><?php echo curr_sym;?><?php echo ($order_item["grandtotal"] - $sku_coupon_used);?></b></td></tr>
                                               <?php } ?>
                                                

                                               
											<?php
                                                                                        $you_saved_discount=0;
											if($total_amount_normal>$order_item["grandtotal"]){
												
											$you_saved=($total_amount_normal-$order_item["grandtotal"]);
											$you_saved_discount=($total_amount_normal-$order_item["grandtotal"]);
											$you_saved_fin+=$you_saved;
											if($promotion_invoice_free_shipping>0){
												$you_saved+=$shipping_price_normal;
											}
                                                                                        if(intval($sku_coupon_used)>0){
                                                                                            $you_saved+=$sku_coupon_used;
                                                                                        }
											?>
											
											<tr><td colspan="2"><em><b>You Saved </b></em></td><td class="text-right" style="color:green"><b><?php echo curr_sym;?><?php echo $you_saved;?></b></td></tr>
											<?php
												//$you_saved_fin+=$you_saved;
											}
											?>


<?php } //addon else condition ?>
											</table>
							</div>
										   
<?php } ?>
</p>

											
	<?php
	if($orders_status_data["type_of_order"]==""){ 
			if($order_item['promotion_available']==1){
				
			if($order_item['total_price_of_product_with_promotion']!="" || $order_item['total_price_of_product_without_promotion']!=""){
					$r=0;
					if($order_item['promotion_quote']!=""){
						$r=1;
					}	
					if($order_item['promotion_default_discount_promo']!="" && $order_item['default_discount']>0 && $order_item['promotion_quote']!=$order_item['promotion_default_discount_promo']){
										
						$r+=1;
					}					
					?>                         
					
					<a data-trigger="click" data-placement="bottom" data-toggle="price_details2" data-html="true" id="price_details_2_<?php echo $order_item_id; ?>" class="price_details_2 cursor-pointer" content_id="#content_price_details_2_<?php echo $order_item_id; ?>" onclick="toggle_content(this)">
				
					OFFERS : <?php echo $r; ?></a>

					
				
					<div id="content_price_details_2_<?php echo $order_item_id; ?>" style="display:none;">
							<table class="table table-bordered" style="font-size: .8em;text-align:justify;margin-bottom: 0px;">
					<?php
					$r=0;
					if($order_item['promotion_quote']!=""){
						$r=1;
						
						?>
						
						<tr class="success"><td><?php echo $r.":"; ?></td><td><?php echo $order_item['promotion_quote'] ?></td></tr>
				   <?php
					
					} 
					if($order_item['promotion_default_discount_promo']!="" && $order_item['default_discount']>0 && $order_item['promotion_quote']!=$order_item['promotion_default_discount_promo']){
					
					$r+=1;
					
					?>
					   <tr  class="success"><td><?php echo $r.":"; ?></td><td><?php echo $order_item['promotion_default_discount_promo'] ?></td></tr>
				   <?php 
				  
					}
				   ?>
					</table>
					</div>
				
				

	<?php } }else{
            if(round($order_item['ord_selling_discount'])!=0){
            ?>
            	<a data-trigger="click" data-placement="bottom" data-toggle="price_details2" data-html="true" id="price_details_3_<?php echo $order_item_id; ?>" class="price_details_2 cursor-pointer" content_id="#content_price_details_3_<?php echo $order_item_id; ?>" onclick="toggle_content(this)">
				
					OFFERS : 1 </a>
				
					<div id="content_price_details_3_<?php echo $order_item_id; ?>" style="display:none;">
                                            <table class="table table-bordered" style="font-size: .8em;text-align:justify;margin-bottom: 0px;">
					
					   <tr  class="success"><td colspan="2">Discount <?php echo round($order_item['ord_selling_discount']); ?> % Off </td></tr>
				   
					</table>
					</div>
<?php
			}

        }
        
        
        
        
                                        }?>

				<!----for replaced item	price display---------->
<?php 
		if($orders_status_data["type_of_order"]=="replaced"){
			$get_replacement_data=$controller->get_order_details_admin_acceptance_refund_data_for_replacement($order_item['prev_order_item_id']);
			$total_pro_price=($get_replacement_data['quantity']*$get_replacement_data['replacement_value_each_inventory_id']);
			$total_price_repl=($total_pro_price+$get_replacement_data['shipping_charge_replacement']);
			?>
			 
			 <p class="text-muted">
			 <strong><?php echo curr_sym;?><?php echo $total_price_repl; ?> </strong>
			 <a data-trigger="click" data-placement="left" data-toggle="popover" data-html="true" id="price_details_3_<?php echo $order_item_id; ?>" class="price_details_3 cursor-pointer" content_id="#content_price_details_3_<?php echo $order_item_id; ?>" onclick="toggle_content(this)"> [<i class="fa fa-question" aria-hidden="true"></i>] </a>
			 
					<div id="content_price_details_3_<?php echo $order_item_id;?>" style="display:none;">
					<table class="table table-bordered" style="font-size: .8em;text-align:justify;margin-bottom: 0px;">
							<thead><tr><th style="padding:0px"></th><th width="25%" style="padding:0px"></th><th style="padding:0px" width="25%" style="text-align: right"></th></tr></thead>
							
							<tr><td>Base product price</td><td><?php echo $get_replacement_data['quantity']; ?> x <?php echo $get_replacement_data['replacement_value_each_inventory_id']; ?></td><td class="text-right"><?php echo curr_sym;?><?php echo $total_pro_price; ?></td></tr>
							
							<tr><td colspan="2">Shipping Charge for <?php echo $get_replacement_data['quantity']; ?> unit(s)</td><td class="text-right"><?php echo curr_sym;?><?php echo $get_replacement_data['shipping_charge_replacement']; ?></td></tr>
							
							<tr><td colspan="2">Total value </td><td class="text-right"><?php echo curr_sym;?><?php echo $total_price_repl;; ?></td></tr>
					</table>
					</div>
			 </p>

						<?php 
		}

			?>				

			</td>
			</tr>	
<?php 							
//if($order_item['ord_addon_single_or_multiple_tagged_inventories_in_frontend']=='single'	){
if(0){
$arr=json_decode($order_item['ord_addon_products']);
$count=count($arr);
if(!empty($arr)){
?>
<tr style="background-color:#FFFFE0">
<td class="products-block">


<!-----addon products-->
										


											
											<?php /* ?><h5 class=""><i><b>Combo products (<?php echo $count; ?>)</b></i></h5><?php */ ?>

											
													<?php 

													//if($order_item['ord_addon_inventories']!=''){
														if(1){

														$i=1;
														foreach($arr as $val){
															
															
																$inv_id=$val->inv_id;
																$name=$val->inv_name;
																$image=$val->inv_image;
																$sku_id=$val->inv_sku_id;
																$price=$val->inv_price;
																$inv_mrp_price=$val->inv_mrp_price;

																?>

														
															<div class="row margin-top">
																<div class="col-md-3 products-block">
																	<div class="products-block-left">
																	<?php 
																		$img_path=get_inventory_image_path_combo($inv_id,$image);
																	?>
																			<img src="<?php echo base_url().$img_path; ?>" alt="ADDITIONAL PRODUCTS">
																		
																	</div>		
																</div>

																<div class="col-md-9 products-block">
																	<div class="f-row product-name ordrk-color">
																		<div class="f-row small-text"><b>Addon for <?php echo $order_item['ord_sku_name'];?></b></div>
																		<a><?php echo $name; ?></a>													
																			
																		<div class="f-row small-text">SKU :<b> <?php echo $sku_id; ?></b></div>
																		<?php
																			if(round($inv_mrp_price)!=round($price)){
																		?>
																		<div class="f-row small-text">Price :<b> <del><?php echo curr_sym.round($inv_mrp_price); ?></del> <?php echo curr_sym.round($price); ?></b></div>
																		<?php
																			}
																			else{
																				?>
																		<div class="f-row small-text">Price :<b> <?php echo curr_sym.round($price); ?></b></div>
																		<?php
																			}
																			?>
																		
																	</div>
																</div>

															</div>

															
																<?php 

																$i++;
															}
														}

													?>
												

											

										<!-----addon products-->
	</td>
</tr>
<?php 
										}
}

										?>
											
	<?php  $order_details_order_status=$controller->get_order_details_order_status($order_item['order_item_id'],$order_id);
	
	if(!empty($order_details_order_status)){
		?>	
		<tr class="margin-top margin-bottom">
		<td colspan="5">
		<?php echo $order_details_order_status;?>
		</td>
		</tr>
		<?php
	} ?>
											
	
	<?php 	include 'order_item_promo_details.php'; ?>
	
			<?php $cancellation_details_from_admin= $controller->get_cancellation_details_from_admin($order_item['order_item_id'])?>
			
	
<!----------------------refund method---------------------------------------------------->									
	
	<?php 	include 'refund_method_cancel_form.php'; ?>								
				
<!-------------------------------------------------------------------------->
	
	
	
	
	
	
	<?php 
	$repl_obj=$controller->get_replaced_orders_by_prev_order_item_id_from_history_orders($order_item['order_item_id']);

		if(!empty($repl_obj)){
			?>
			<tr class="margin-top margin-bottom">
		<td colspan="5">
			<?php
			include 'replacement_inv_details.php';?>
			</td>
		</tr>
			<?php
		}
	?>
	<!---replacement inv details-----of-----original-----order-id---------->
		

	<?php
	
	include 'returns_details.php'; ?>
		
			
	<?php 	include 'cancellled_order_details.php'; ?>			
	
						

			<!-------diff_divs--order_item_id------>

<script>
$(document).ready(function(){
	var order_item_id='<?php echo $order_item_id;?>';
	$("#price_details_offer_"+order_item_id).popover({
		html: true, 
		content: function() {
				if($(window).width() > 480){
					$('#content_price_details_offer_'+order_item_id).hide();
					return $('#content_price_details_offer_'+order_item_id).html();
				}
			}
	});
	$("#price_details_cancel_"+order_item_id).popover({
		html: true, 
		content: function() {
				if($(window).width() > 480){
					$('#content_price_details_cancel_'+order_item_id).hide();
					return $('#content_price_details_cancel_'+order_item_id).html();
				}
			}
	});
	
	$("#price_details_1_"+order_item_id).popover({
		html: true, 
		content: function() {
				if($(window).width() > 480){
					$('#content_price_details_1_'+order_item_id).hide();
					return $('#content_price_details_1_'+order_item_id).html();	
				}
			}
	});

	$("#price_details_2_"+order_item_id).popover({
		html: true, 
		content: function() {	 
				if($(window).width() > 480){
					$('#content_price_details_2_'+order_item_id).hide();
					return $('#content_price_details_2_'+order_item_id).html();
				}
			}
	});
	$("#price_details_3_"+order_item_id).popover({
		html: true, 
		content: function() {
				if($(window).width() > 480){
					$('#content_price_details_3_'+order_item_id).hide();
					return $('#content_price_details_3_'+order_item_id).html();
				}
			}
	});

	$("#order_placed_"+order_item_id).popover({
		html: true, 
		content: function() {
				if($(window).width() > 480){
					$('#cont_'+order_item_id).hide();	
					$('#order_status_'+order_item_id).html('');
					return $('#content_order_placed_'+order_item_id).html();
				}
			}
	});
	$("#order_confirmed_"+order_item_id).popover({
		html: true, 
		content: function() {
				if($(window).width() > 480){
					$('#cont_'+order_item_id).hide();	
					$('#order_status_'+order_item_id).html('');
					return $('#content_order_confirmed_'+order_item_id).html();
				}
			}
	});
	$("#order_packed_"+order_item_id).popover({
		html: true, 
		content: function() {
				if($(window).width() > 480){
					$('#cont_'+order_item_id).hide();	
					$('#order_status_'+order_item_id).html('');
					return $('#content_order_packed_'+order_item_id).html();
				}
			}
	});
	$("#order_shipped_"+order_item_id).popover({
		html: true, 
		content: function() {
				if($(window).width() > 480){
					$('#cont_'+order_item_id).hide();	
					$('#order_status_'+order_item_id).html('');
					return $('#content_order_shipped_'+order_item_id).html();
				}
			}
	});
	$("#order_delivered_"+order_item_id).popover({

		html: true, 
		content: function() {
				if($(window).width() > 480){
					$('.cont').hide();
					$('#order_status_'+order_item_id).html('');
					return $('#content_order_delivered_'+order_item_id).html();
				}
			}
	});
});
</script>
<!-------diff_divs--order_item_id------>

<?php 



				}/*end foreach*/

			}/*end empty*/
			
		
			?>

			<?php

		}
}
?>
						</tbody>
						<tfoot>
                                                                <tr>
                                                                    <td align="right" colspan="5"><small><b>
                                                                        <ul>
									   <?php
									   if($subtotal_cal>0){
										?>
										<li>Total Amount: <?php echo curr_sym; ?><?php echo $subtotal_cal; ?></li>
										<?php } ?>
                                                                                
                                                                                
                                                                                <?php
                                                                                if(intval($sku_coupon_used)>0){
										?>
                                                                                <li>Total Coupon used Amount on SKUs: <?php echo curr_sym; ?><?php echo $sku_coupon_used; ?></li>
                                                                                <?php } ?>
                                                                                
									   	<?php
                                                                                if($invoice_coupon_status=='1'){
										?>
										<li>Total Invoice Coupon <?php echo $inc_txt; ?>: <?php echo curr_sym; ?><?php echo round($invoice_coupon_reduce_price); ?></li>
										<?php } ?>
                                                                                
                                                                                <!--- with sku coupon used amount ---->
										<?php 
                                                                            if(intval($sku_coupon_used)>0 && $invoice_coupon_status=='1'){
										?>
										<li>Total Coupon Discount: <?php echo curr_sym; ?><?php echo round($total_coupon_used_amount); ?></li>
                                                                            <?php } ?>
										
										</ul>
									
									
                       </b></small>
                                                                    </td>
                                                                        
                                                                </tr>
								<tr>
										<td align="right" colspan="3"><small><b><?php 
										if($total_amount_saved>0){
											//echo "Your Total Saving is ".curr_sym."".($total_amount_saved+$you_saved_fin+$promo_dis);
											echo "Your Total Saving is ".curr_sym."".round($total_amount_saved);
										}elseif($you_saved_fin>0){
											echo "Your Total Saving is ".curr_sym."".$you_saved_fin;
										}else{
											echo "";
										}

										?> </b> </small></td><td align="right" colspan="2">Grand Total <span class="order_grand_total"><?php echo curr_sym;?><?php //echo $total_amount_to_pay?> <?php echo (intval($subtotal_cal)-intval($total_coupon_used_amount)); ?></span></td>
									
								</tr>
						</tfoot>
						</table>
						</div>
						</div>
						</div>



            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

<script>


function toggle_content(ele){
	var cont_id=$(ele).attr("content_id");
	//alert(cont_id)
	if($(window).width() <= 480){
		$(ele).popover('hide');
		$(cont_id).toggle();
	}
}

function toggle_content_order_status(ele,order_item_id){
	var cont_id=$(ele).attr("content_id");
	
	if($(window).width() <= 480){
		$('#cont_'+order_item_id).show();	
		$(ele).popover('hide');
		html_ele=$(cont_id).html();
		$('#order_status_'+order_item_id).html(html_ele);
		//$('#order_status').toggle();
	}
}
function hide_order_status(ele,order_item_id){
	$('#cont_'+order_item_id).hide();	
}

$(document).ready(function(){

$("#invoice").popover({
    html: true, 
	content: function() {
          return $('#popover-content-invoice').html();
        }
});

/*$("[data-toggle=popover]").popover({
    html: true, 
	content: function() {
          return $('#popover-content').html();
        }
});
*/
});
function accept_reject_refund_statusFun(st,order_item_id){
	$.ajax({
		url:"<?php echo base_url()?>Account/accept_reject_refund_status",
		type:"post",
		data:"order_item_id="+order_item_id+"&st="+st,
		success:function(data){
			if(data){
				if(st=="accept"){
					$("#accept_reject_refund_status_"+order_item_id).html("<td><p><small>Customer Status:</td></p></small><td><p><small> Accepted</td></p></small>");
					/*bootbox.alert({
					  size: "small",
					  message: 'Status has been successfully updated',
					  callback: function () { location.reload(); }
					});*/

                    alert('Status has been successfully updated');
					location.reload();
				}
				if(st=="reject"){
					$("#accept_reject_refund_status_"+order_item_id).html("<td><p><small>Customer Status</td></p></small><td><p><small> Rejected</td></p></small>");
					/*bootbox.alert({
					  size: "small",
					  message: 'status has been successfully updated',
					  callback: function () { location.reload(); }
					  
					});*/

                    alert('Status has been successfully updated');
                    location.reload();
				}
				$('#accept_reject_block').hide();
			}
		}
	})
}

					
	function setRefundMethod_Rep(order_item_id){
		document.getElementById('bank_account_data_rep_'+order_item_id).style.display = 'none';
		//document.getElementById('bank_account_data').style.display = 'none';
		var checkedValue = null; 
		var inputElements = document.getElementsByName('refund_method_'+order_item_id);
		
			for(var i=0; inputElements[i]; ++i){
				  if(inputElements[i].checked){
					   checkedValue = inputElements[i].value;
					   break;
				  }
			}
			if(checkedValue=="Bank Transfer"){
				document.getElementById('bank_account_data_rep_'+order_item_id).style.display = 'block';
				document.getElementById('bank_ifsc_code_rep_'+order_item_id).required = true;
				document.getElementById('bank_account_number_rep_'+order_item_id).required = true;
				document.getElementById('bank_account_confirm_number_rep_'+order_item_id).required = true;
				document.getElementById('account_holder_name_rep_'+order_item_id).required = true;
				document.getElementById('bank_return_refund_phone_number_rep_'+order_item_id).required = true;
			}
			else{
				document.getElementById('bank_account_data_rep_'+order_item_id).style.display = 'none';
				document.getElementById('bank_ifsc_code_rep_'+order_item_id).required = false;
				document.getElementById('bank_account_number_rep_'+order_item_id).required = false;
				document.getElementById('bank_account_confirm_number_rep_'+order_item_id).required = false;
				document.getElementById('account_holder_name_rep_'+order_item_id).required = false;
				document.getElementById('bank_return_refund_phone_number_rep_'+order_item_id).required = false; 
			}
		
	}	
				
	
	
function setRefundMethod(order_item_id){
	document.getElementById('bank_account_data_cancel_'+order_item_id).style.display = 'none';
	var checkedValue = null; 
	var inputElements = document.getElementsByName('refund_method_cancel_'+order_item_id);
	for(var i=0; inputElements[i]; ++i){
	      if(inputElements[i].checked){
	           checkedValue = inputElements[i].value;
	           break;
	      }
	}
	if(checkedValue=="Bank Transfer"){
		document.getElementById('bank_account_data_cancel_'+order_item_id).style.display = 'block';
		document.getElementById('bank_ifsc_code_cancel_'+order_item_id).required = true;
		document.getElementById('bank_account_number_cancel_'+order_item_id).required = true;
		document.getElementById('bank_account_confirm_number_cancel_'+order_item_id).required = true;
		document.getElementById('account_holder_name_cancel_'+order_item_id).required = true;
		document.getElementById('bank_return_refund_phone_number_cancel_'+order_item_id).required = true;
	}
	else{
		document.getElementById('bank_account_data_cancel_'+order_item_id).style.display = 'none';
		document.getElementById('bank_ifsc_code_cancel_'+order_item_id).required = false;
		document.getElementById('bank_account_number_cancel_'+order_item_id).required = false;
		document.getElementById('bank_account_confirm_number_cancel_'+order_item_id).required = false;
		document.getElementById('account_holder_name_cancel_'+order_item_id).required = false;
		document.getElementById('bank_return_refund_phone_number_cancel_'+order_item_id).required = false; 
	}
}
function submit_refund_stuff(cancelled_order_item_id){
	var inputElements = document.getElementsByName('refund_method_cancel_'+cancelled_order_item_id);
	 
	 checkedValue=null;
	 
	for(var i=0; inputElements[i]; ++i){
	      if(inputElements[i].checked){
	           checkedValue = inputElements[i].value;
	           break;
	      }
	}
	
	if(checkedValue==null){
		/*bootbox.alert({
			  size: "small",
			  message: 'Please choose refund method',
			  callback: function () { return false; }
			  
			});*/

        alert('Please choose refund method');
        return false;
		
	}
	
	if(checkedValue=="Bank Transfer"){
		if(document.getElementById("bank_name_cancel_"+cancelled_order_item_id).value==""){	
	  
        //alert("Please enter some text first");
	
		document.getElementById("bank_name_cancel_"+cancelled_order_item_id).focus();
		return false;
      }
	  
	  else if(document.getElementById("bank_branch_name_cancel_"+cancelled_order_item_id).value==""){
	  
        //alert("Please enter some text first");

		document.getElementById("bank_branch_name_cancel_"+cancelled_order_item_id).focus();
		return false;
      }
	  
	  else if(document.getElementById("bank_city_cancel_"+cancelled_order_item_id).value==""){
	  
        //alert("Please enter some text first");
	
		document.getElementById("bank_city_cancel_"+cancelled_order_item_id).focus();
		return false;
	  }
	  
	  else if(document.getElementById("bank_ifsc_code_cancel_"+cancelled_order_item_id).value.length!=6){
		
		document.getElementById("bank_ifsc_code_cancel_"+cancelled_order_item_id).focus();
		return false;
	  }
	  
	  else if(document.getElementById("bank_account_number_cancel_"+cancelled_order_item_id).value.length!=15){
	       
		document.getElementById("bank_account_number_cancel_"+cancelled_order_item_id).focus();
		return false;
	  }
	  else if((document.getElementById("bank_account_number_cancel_"+cancelled_order_item_id).value)!=(document.getElementById("bank_account_confirm_number_cancel_"+cancelled_order_item_id).value)){
	        
		document.getElementById("bank_account_confirm_number_cancel_"+cancelled_order_item_id).focus();
		return false;
	  }
	  
	  else if(document.getElementById("account_holder_name_cancel_"+cancelled_order_item_id).value == ""){
	  
        //alert("Please enter some text first");
	
		document.getElementById("account_holder_name_cancel_"+cancelled_order_item_id).focus();
		return false;
	  }
	  
	  else if(document.getElementById("bank_return_refund_phone_number_cancel_"+cancelled_order_item_id).value.length !=10){
	  
        //alert("Please enter some text first");

		document.getElementById("bank_return_refund_phone_number_cancel_"+cancelled_order_item_id).focus();
		return false;
	  }
	  
	  else
   {
   var wallet_or_not=$("#wallet_radio_cancel_"+cancelled_order_item_id).is(":checked");
	$.ajax({
		url:"<?php echo base_url()?>Account/refund_to_customer",
		type:"post",
		data:$("#refund_method_form_cancel_"+cancelled_order_item_id).serialize(),
		success:function(data){
			///alert(data);
			if(data){
				if(wallet_or_not){
					
					/*bootbox.alert({
					  size: "small",
					  message: 'Transfer has been done to your wallet',
					  callback: function () { $("#refund_method_form").hide("fast"); }
					  
					});	*/

                    alert('Transfer has been done to your wallet');
                    $("#refund_method_form").hide("fast");
					//return false;
				}
				else{
					
					/*bootbox.alert({
					  size: "small",
					  message: "Your bank details has been updated! We will credit to your a/c soon!",
					  callback: function () { $("#refund_method_form_cancel_"+cancelled_order_item_id).hide("fast"); }
					  
					});*/

                    alert( "Your bank details has been updated! We will credit to your a/c soon!");
                    $("#refund_method_form_cancel_"+cancelled_order_item_id).hide("fast");
					//return false;
				}
				location.reload();
			}
		}
	})
   }
	
}else{
	
	var wallet_or_not=$("#wallet_radio_cancel_"+cancelled_order_item_id).is(":checked");
	
	$.ajax({
		url:"<?php echo base_url()?>Account/refund_to_customer",
		type:"post",
		data:$("#refund_method_form_cancel_"+cancelled_order_item_id).serialize(),
		success:function(data){
			///alert(data);
			if(data){
				if(wallet_or_not){
					
					/*bootbox.alert({
					  size: "small",
					  message: "Transfer has been done to your wallet",
					  callback: function () { $("#refund_method_form").hide("fast"); }
					});*/
					alert("Transfer has been done to your wallet");
                    $("#refund_method_form").hide("fast");
					//return false;
				}
				else{
				
					/*bootbox.alert({
					  size: "small",
					  message: "your bank details has been updated! We will credit to your a/c soon!",
					  callback: function () { $("#refund_method_form_cancel_"+cancelled_order_item_id).hide("fast"); }
					  
					});*/
                    alert("Your bank details has been updated! We will credit to your a/c soon!");
                    $("#refund_method_form_cancel_"+cancelled_order_item_id).hide("fast");
                    //return false;
				}
				location.reload();
			}
		}
	})
	}
}

</script>

<script type="text/javascript">

function showRefundMoreDiv(order_item_id){
	
	$("#refund_more_"+order_item_id).toggle("fast");
	var refund_remain_quant=$("#refund_remaining_quantities_"+order_item_id).is(':checked');
	var refund_remain_deduc=$("#refund_remaining_deductions_"+order_item_id).is(':checked');
	var refund_all=$("#refund_all_"+order_item_id).is(':checked');
	if(refund_all==false && refund_remain_quant==false && refund_remain_deduc==false){
		$('#sub_request_refund_btn_'+order_item_id).prop('disabled', true);
	}else{
		$('#sub_request_refund_btn_'+order_item_id).prop('disabled', false);
	}
	
}
function refund_remaining_deductionsFun(order_item_id){
	
	var refund_remain_quant=$("#refund_remaining_quantities_"+order_item_id).is(':checked');
	var refund_remain_deduc=$("#refund_remaining_deductions_"+order_item_id).is(':checked');
	var refund_all=$("#refund_all_"+order_item_id).is(':checked');
	
	if(refund_all==true || refund_remain_quant==true || refund_remain_deduc==true){
		$('#sub_request_refund_btn_'+order_item_id).prop('disabled', false);
	}else{
		$('#sub_request_refund_btn_'+order_item_id).prop('disabled', true);
	}
	document.getElementById("refund_all_"+order_item_id).checked=false;
	
	if(refund_remain_quant==false){
		quant=$("#quantity_left_original_"+order_item_id).val();
		
		$("#refund_remaining_quantities_input_"+order_item_id).val(quant);
		document.getElementById('refund_order_summary_'+order_item_id).innerHTML="";	
		$('#refund_remaining_quantities_input_'+order_item_id).attr('readonly',true);
	}else{
		$('#refund_remaining_quantities_input_'+order_item_id).attr('readonly',false);
	}
}

function refund_remaining_quantitiesFun(obj,order_item_id){
	
	var refund_remain_quant=$("#refund_remaining_quantities_"+order_item_id).is(':checked');
	var refund_remain_deduc=$("#refund_remaining_deductions_"+order_item_id).is(':checked');
	var refund_all=$("#refund_all_"+order_item_id).is(':checked');
	
	if(refund_all==true || refund_remain_quant==true || refund_remain_deduc==true){
		$('#sub_request_refund_btn_'+order_item_id).prop('disabled', false);
	}else{
		$('#sub_request_refund_btn_'+order_item_id).prop('disabled', true);
	}
	
	if(refund_remain_quant==false){
		quant=$("#quantity_left_original_"+order_item_id).val();
		
		$("#refund_remaining_quantities_input_"+order_item_id).val(quant);
		document.getElementById('refund_order_summary_'+order_item_id).innerHTML="";	
		$('#refund_remaining_quantities_input_'+order_item_id).attr('readonly',true);
	}else{
		$('#refund_remaining_quantities_input_'+order_item_id).attr('readonly',false);
	}
	
	if(refund_all==true || refund_remain_quant==true){
		
		var remaining_quantity=$("#refund_remaining_quantities_input_"+order_item_id).val();
		var promo_available=$("#promotion_available_"+order_item_id).val();
		if(promo_available==1){
			return_va=calculate_promo_details_for_refund_more(order_item_id,remaining_quantity);	
			if(return_va==false){	
				return false;
			}
		}
	}else{
		
		document.getElementById('refund_order_summary_'+order_item_id).innerHTML="";	
	}
	
	document.getElementById("refund_all_"+order_item_id).checked=false;
	if(obj.checked){
		document.getElementById("refund_remaining_quantities_input_div_"+order_item_id).style.display="";
		document.getElementById("refund_remaining_quantities_input_"+order_item_id).disabled=false;
	}
	else{
		document.getElementById("refund_remaining_quantities_input_div_"+order_item_id).style.display="none";
		document.getElementById("refund_remaining_quantities_input_"+order_item_id).disabled=true;
	}
}
function refundAllFun(order_item_id){
	if(document.getElementById("refund_remaining_deductions_"+order_item_id)!=null ){
	document.getElementById("refund_remaining_deductions_"+order_item_id).checked=false;
	}
	document.getElementById("refund_remaining_quantities_"+order_item_id).checked=false;
	
	var refund_remain_quant=$("#refund_remaining_quantities_"+order_item_id).is(':checked');
	if(document.getElementById("refund_remaining_deductions_"+order_item_id)!=null ){
	var refund_remain_deduc=$("#refund_remaining_deductions_"+order_item_id).is(':checked');
	}
	var refund_all=$("#refund_all_"+order_item_id).is(':checked');
	
	if(refund_all==true || refund_remain_quant==true || refund_remain_deduc==true){
		$('#sub_request_refund_btn_'+order_item_id).prop('disabled', false);
	}else{
		$('#sub_request_refund_btn_'+order_item_id).prop('disabled', true);
	}
	
	if(refund_remain_quant==false){
		
		quant=$("#quantity_left_original_"+order_item_id).val();	
		$("#refund_remaining_quantities_input_"+order_item_id).val(quant);
		document.getElementById('refund_order_summary_'+order_item_id).innerHTML="";
		$('#refund_remaining_quantities_input_'+order_item_id).attr('readonly',true);
	}else{
		
		$('#refund_remaining_quantities_input_'+order_item_id).attr('readonly',false);
	}
	
	
	
	if(refund_all==true || refund_remain_quant==true){
		
		var remaining_quantity=$("#refund_remaining_quantities_input_"+order_item_id).val();
		var promo_available=$("#promotion_available_"+order_item_id).val();
		if(promo_available==1){
			return_va=calculate_promo_details_for_refund_more(order_item_id,remaining_quantity);	
			if(return_va==false){	
				return false;
			}
		}
		
	}else{
		
		document.getElementById('refund_order_summary_'+order_item_id).innerHTML="";	
	}

}

function sub_request_refundFun(order_item_id){
	
	var refund_remain_quant=$("#refund_remaining_quantities_"+order_item_id).is(':checked');
	var refund_remain_deduc=$("#refund_remaining_deductions_"+order_item_id).is(':checked');
	var refund_all=$("#refund_all_"+order_item_id).is(':checked');
	
	if(refund_all==false && refund_remain_quant==false && refund_remain_deduc==false){
		
		/*bootbox.alert({
		  size: "small",
		  message: "Please check any of three",
		  callback: function () { return false; }
		  
		});*/

        alert("Please check any of three");
        return false;
	}
	if(refund_all==true || refund_remain_quant==true || refund_remain_deduc==true){
		$('#sub_request_refund_btn_'+order_item_id).prop('disabled', false);
	}
	if(refund_all==true || refund_remain_quant==true){
		
		var remaining_quantity=$("#refund_remaining_quantities_input_"+order_item_id).val();
		var promo_available=$("#promotion_available_"+order_item_id).val();
		if(promo_available==1){
			return_va=calculate_promo_details_for_refund_more(order_item_id,remaining_quantity);	
			if(return_va==false){	
				
				return false;
			}
		}
	}
	
	//alert($("#sub_request_refund_"+order_item_id).serialize());

    $('#sub_request_refund_btn_'+order_item_id).prop('disabled', true);
	$.ajax({
		url:"<?php echo base_url()?>Account/sub_request_refund",
		type:"POST",
		data:$("#sub_request_refund_"+order_item_id).serialize(),
		success:function(data){
			
			//alert(data);
			//return false;
			if(data){
				/*bootbox.alert({
				  size: "small",
				  message: "Submitted successfully",
				  callback: function () { location.reload(); }
				});*/

                alert("Submitted successfully");
				location.reload();
			}else{
					/*bootbox.alert({
							  size: "small",
							  message: "Error"
							   
							});*/
                alert('Error');
                location.reload();

			}
		}
	});
}
function review_service(order_item_id){
	//alert(order_item_id);
	document.getElementById('write_review_'+order_item_id).submit();
}
function call_function_promo(obj,order_item_id){
	var typed_quantity=obj.value;
	var promo_available=$("#promotion_available_"+order_item_id).val();
	if(promo_available==1){
		calculate_promo_details_for_refund_more(order_item_id,typed_quantity);	
	}
}

function calculate_promo_details_for_refund_more(order_item_id,desired_quantity_refund){

            var curr_sym='<?php echo curr_sym; ?>';
			if(document.getElementById('order_item_invoice_discount_value_'+order_item_id)!=null){	
				var order_item_invoice_discount_value=parseFloat(document.getElementById('order_item_invoice_discount_value_'+order_item_id).value) || 0;//"falsey" value to 0
			}else{
				var order_item_invoice_discount_value=0;
			}
			
			var ordered_quantity=parseInt(document.getElementById('ordered_quantity_'+order_item_id).value);
			var price_paid_shipping=parseInt(document.getElementById('price_paid_shipping_'+order_item_id).value);
			var price_paid_perunit=parseInt(document.getElementById('price_paid_perunit_'+order_item_id).value);
			var shipping_price_waived=document.getElementById('shipping_price_waived_'+order_item_id).value;
			
			var promotion_available=parseInt(document.getElementById('promotion_available_'+order_item_id).value);
			var promotion_minimum_quantity=parseInt(document.getElementById('promotion_minimum_quantity_'+order_item_id).value);
			var promotion_quote=document.getElementById('promotion_quote_'+order_item_id).value
			var promotion_default_discount_promo=document.getElementById('promotion_default_discount_promo_'+order_item_id).value
			var promotion_surprise_gift_type=document.getElementById('promotion_surprise_gift_type_'+order_item_id).value;
			var promotion_item=document.getElementById('promotion_item_'+order_item_id).value
			var promotion_item_num=document.getElementById('promotion_item_num_'+order_item_id).value
			var promotion_cashback=parseInt(document.getElementById('promotion_cashback_'+order_item_id).value);
			var promotion_item_multiplier=parseInt(document.getElementById('promotion_item_multiplier_'+order_item_id).value);
			var default_discount=parseInt(document.getElementById('default_discount_'+order_item_id).value);
			var promotion_discount=parseInt(document.getElementById('promotion_discount_'+order_item_id).value);

			var cash_back_value=parseInt(document.getElementById('cash_back_value_'+order_item_id).value);
			var status_of_refund_for_cashback_on_sku=$("#status_of_refund_for_cashback_on_sku_"+order_item_id).val();
			
			var individual_price_of_product_with_promotion=parseFloat(document.getElementById('individual_price_of_product_with_promotion_'+order_item_id).value);
			var individual_price_of_product_without_promotion=parseFloat(document.getElementById('individual_price_of_product_without_promotion_'+order_item_id).value);
			var total_price_of_product_with_promotion=parseFloat(document.getElementById('total_price_of_product_with_promotion_'+order_item_id).value) || 0;
			var total_price_of_product_without_promotion=parseFloat(document.getElementById('total_price_of_product_without_promotion_'+order_item_id).value) || 0;
			
			if(promotion_available==1){

				$("#req_individual_price_of_product_without_promotion_"+order_item_id).val('');
				$("#req_total_price_of_product_without_promotion_"+order_item_id).val('');
				$("#req_quantity_without_promotion_"+order_item_id).val('');
				$("#req_individual_price_of_product_with_promotion_"+order_item_id).val('');
				$("#req_total_price_of_product_with_promotion_"+order_item_id).val('');
				$("#req_quantity_with_promotion_"+order_item_id).val('');
				$("#req_cash_back_value_"+order_item_id).val('');
			}
			//req_quantity_with_promotion
			//req_quantity_without_promotion
			
			var quotient=0;
			var modulo=0;
			var promo_str="";
			var total_amount=0;
			var str="";
			
		if(promotion_available==1){
			
			if((promotion_discount!=default_discount) && (promotion_discount>0)){
				promo_discount_available=1;
			}else{
				promo_discount_available=0;
			}
			
			
			modulo=parseInt(desired_quantity_refund%promotion_minimum_quantity);//remainder
			quotient=Math.floor(parseInt(desired_quantity_refund/promotion_minimum_quantity));//answer
			
			modulo_purchased=parseInt(ordered_quantity%promotion_minimum_quantity);//remainder
			quotient_purchased=Math.floor(parseInt(ordered_quantity/promotion_minimum_quantity));//answer

			var number_of_free_items=0;
			if(promotion_item_num!=''){
				
				var promo_item_arr = promotion_item_num.split(',');
				var free_item_count=0;
				number_of_free_items=promo_item_arr.length;
				for(h=0;h<number_of_free_items;h++){
					free_item_count+=parseInt(promo_item_arr[h]);
				}
			}
			
			if(promotion_surprise_gift_type=='Qty'){
				quantity_filled=parseInt($('#quantity_filled_'+order_item_id).val());
				remain=parseInt(ordered_quantity-desired_quantity_refund-quantity_filled);
				
				if(remain<promotion_minimum_quantity){
					return_surprise_gift=1;
				}else{
					return_surprise_gift=0;
				}
			}
			var promotion_surprise_gift=document.getElementById('promotion_surprise_gift_'+order_item_id).value;

			if(promotion_surprise_gift_type=='<?php echo curr_code; ?>'){
				quantity_filled=parseInt($('#quantity_filled_'+order_item_id).val());
				
				all_qty=parseInt(quantity_filled)+parseInt(desired_quantity_refund);
				
				if(ordered_quantity==all_qty){
					return_surprise_gift_amount=1;
				}else{
					return_surprise_gift_amount=0;
				}
			}
			
		if(ordered_quantity >= promotion_minimum_quantity){
			
			if(desired_quantity_refund<promotion_minimum_quantity && (modulo_purchased!=modulo) && desired_quantity_refund!=quotient && (free_item_count>0 || promo_discount_available==1) && promo_discount_available != 1){
				if(modulo_purchased>0){
					
					/*bootbox.alert({
					  size: "small",
					  message: "You cannot request for this quantity.You can give request for multiples of "+promotion_minimum_quantity+ " or " +modulo_purchased,
					});*/

                    alert("You cannot request for this quantity.You can give request for multiples of "+promotion_minimum_quantity+ " or " +modulo_purchased);

				}else{
					
					/*bootbox.alert({
					  size: "small",
					  message: "You cannot request for this quantity.You can give request for multiples of "+promotion_minimum_quantity,
					});*/

                    alert("You cannot request for this quantity.You can give request for multiples of "+promotion_minimum_quantity);

				}
				document.getElementById('refund_order_summary_'+order_item_id).innerHTML=str;
				$('#sub_request_refund_btn_'+order_item_id).prop('disabled', true);
				
				return false;
			}else if(desired_quantity_refund>promotion_minimum_quantity && modulo_purchased==0 && modulo>0 && promo_discount_available != 1){
				
				/*bootbox.alert({
				  size: "small",
				  message: "You cannot request for this quantity.You can give request for multiples of multiples of "+promotion_minimum_quantity,
				});*/

                alert("You cannot request for this quantity.You can give request for multiples of multiples of "+promotion_minimum_quantity);
				
				document.getElementById('refund_order_summary_'+order_item_id).innerHTML=str;
				$('#sub_request_refund_btn_'+order_item_id).prop('disabled', true);
				return false;
			}else if(desired_quantity_refund>promotion_minimum_quantity && modulo_purchased>0 && modulo_purchased!=modulo && modulo!=0 && promo_discount_available != 1){
				
				/*bootbox.alert({
				  size: "small",
				  message: "You cannot request for this quantity.You can give request for multiples of "+promotion_minimum_quantity+ " or " +modulo_purchased,
				});*/

                alert("You cannot request for this quantity.You can give request for multiples of "+promotion_minimum_quantity+ " or " +modulo_purchased);
				document.getElementById('refund_order_summary_'+order_item_id).innerHTML=str;
				
				$('#sub_request_refund_btn_'+order_item_id).prop('disabled', true);
				return false;
			}
			
			$('#sub_request_refund_btn_'+order_item_id).prop('disabled', false);
			
			if(desired_quantity_refund >= promotion_minimum_quantity){
				
				if(promo_discount_available != 1){
					
					if(quotient>0){
						
						indi_price_with_promo=parseInt(individual_price_of_product_with_promotion/quotient);
						
						if(number_of_free_items>0){
							count_quotient=(quotient)*promotion_minimum_quantity;
						}else{
							count_quotient=quotient*promotion_minimum_quantity;
						}
						
						//alert(quotient);
						//alert(promotion_minimum_quantity);

						total_price_with_promo=count_quotient*individual_price_of_product_with_promotion;
						
						promo_str+='<tr><td>'+count_quotient+' * '+individual_price_of_product_with_promotion+'<br><small style="color:green;"><b><i>'+promotion_quote+' for quantity ('+count_quotient+')</i></b></small></td><td>'+curr_sym+total_price_with_promo+'</td></tr>';
						total_amount+=parseFloat(total_price_with_promo);
						
						
						$("#req_individual_price_of_product_with_promotion_"+order_item_id).val(individual_price_of_product_with_promotion);
						$("#req_total_price_of_product_with_promotion_"+order_item_id).val(total_price_with_promo);
						$("#req_quantity_with_promotion_"+order_item_id).val(count_quotient);
					
						if(promotion_cashback>0){
							if(status_of_refund_for_cashback_on_sku=="refunded"){
								cash_back_value=parseFloat(individual_price_of_product_with_promotion*(promotion_cashback/100)).toFixed(2);
								cash_back_value_with_quotient=parseFloat(quotient*cash_back_value).toFixed(2);
								promo_str+='<tr><td><span style="color:red;">Cashback value for quantity ('+quotient+')</span></td><td> <span style="color:red;"> -'+curr_sym+' '+cash_back_value_with_quotient+'</span></td></tr>';
								total_amount-=parseFloat(cash_back_value_with_quotient);
							
							}else{
								cash_back_value_with_quotient=0;
							}
							$("#req_cash_back_value_"+order_item_id).val(cash_back_value_with_quotient);
						}
					}
					
					if(number_of_free_items>0){
						
						free_skus_from_inventory=$.parseJSON($("#free_inventory_available_"+order_item_id).val());
						
						free_str="";
						
						//alert(free_skus_from_inventory);
						if(free_skus_from_inventory!=null){
							if(free_skus_from_inventory.length>0){
								
								for(y=0;y<free_skus_from_inventory.length;y++){
									
									//free_str+=free_skus_from_inventory[y].str+" <small>("+quotient*parseInt(free_skus_from_inventory[y].count)+")</small> <br>";
									free_str+="<tr><td><img src="+'<?php echo base_url() ?>'+free_skus_from_inventory[y].image+" style='width:50px;height:60px'></td><td>"+free_skus_from_inventory[y].str+" <small> Quantity - "+quotient*parseInt(free_skus_from_inventory[y].count)+"</small></td></tr>";
								}
							}
							
						}

						promo_str+='<tr><td colspan="2"><b><h6 style="color:blue;"></b> Free Items to be returned with the requested Quantity of product</h6>'+free_str+'</td><tr>';
					}
					if(promotion_surprise_gift_type=='Qty'){
						if(return_surprise_gift==1){
							
							surprise_free_skus_from_inventory=$.parseJSON($("#surprise_free_skus_from_inventory_"+order_item_id).val());
							surprise_free_str="";
						
							//alert(free_skus_from_inventory);
							if(surprise_free_skus_from_inventory!=null){
								if(surprise_free_skus_from_inventory.length>0){
									
									for(y=0;y<surprise_free_skus_from_inventory.length;y++){
										//alert(free_skus_from_inventory[y].image);
										surprise_free_str+="<tr><td><img src="+'<?php echo base_url() ?>'+surprise_free_skus_from_inventory[y].image+" style='width:50px;height:60px'></td><td>"+surprise_free_skus_from_inventory[y].str+" <small><br> Quantity - "+parseInt(surprise_free_skus_from_inventory[y].count)+"</small></td></tr>";
									}
								}
								
							}
							promo_str+='<tr><td colspan="2"><b><h6 style="color:blue;"></b> Free Items to be returned with the requested Quantity of product</h6>'+surprise_free_str+'</td></tr>';
						}
					}
					
					if(promotion_surprise_gift_type=='<?php echo curr_code; ?>'){
						if(return_surprise_gift_amount==1){
							promo_str+='<tr><td><h6 style="color:blue;">'+curr_sym+' '+promotion_surprise_gift+' was credited to customer wallet. So this amount will be reduced from the total item price.</h6> </td><td style="color:red"> - '+' '+promotion_surprise_gift+'</td></tr>';
							total_amount-=parseFloat(promotion_surprise_gift);
						}
					}
					
					if(modulo>0){
						//breakup 2
						indi_price_without_promo=parseInt(individual_price_of_product_without_promotion/modulo);
						
						total_price_without_promo=parseInt(modulo*individual_price_of_product_without_promotion);
						
						promo_str+='<tr><td>'+modulo+' * '+individual_price_of_product_without_promotion+'<br><small style="color:green;"><b><i>'+promotion_default_discount_promo+' for quantity ('+modulo+')</i></b></small></td><td> '+curr_sym+' '+total_price_without_promo+'</td></tr>';
						total_amount+=parseFloat(total_price_without_promo);
						$("#req_individual_price_of_product_without_promotion_"+order_item_id).val(individual_price_of_product_without_promotion);
						$("#req_total_price_of_product_without_promotion_"+order_item_id).val(total_price_without_promo);
						$("#req_quantity_without_promotion_"+order_item_id).val(modulo);
					}
					
				}
				if(promo_discount_available == 1){
					total_price_of_product=parseFloat(total_price_of_product_with_promotion+total_price_of_product_without_promotion).toFixed(2);
					indi_price_with_promo=parseFloat(total_price_of_product/ordered_quantity).toFixed(2);
					total=parseFloat(indi_price_with_promo*desired_quantity_refund).toFixed(2);
					promo_str+='<tr><td>'+desired_quantity_refund+' * '+indi_price_with_promo+'</td><td>'+curr_sym+total+'</td></tr>';
					total_amount+=parseFloat(total);
					$("#req_individual_price_of_product_with_promotion_"+order_item_id).val(indi_price_with_promo);
					$("#req_total_price_of_product_with_promotion_"+order_item_id).val(total);
					$("#req_quantity_with_promotion_"+order_item_id).val(desired_quantity_refund);
				}
			
				
			}else{
				if(promo_discount_available != 1){
					if(modulo>0){
						//breakup 2
						indi_price_without_promo=parseInt(individual_price_of_product_without_promotion/modulo);
						
						total_price_without_promo=parseInt(modulo*individual_price_of_product_without_promotion);
						
						promo_str+='<tr><td>'+modulo+' * '+individual_price_of_product_without_promotion+'<br><small style="color:green;"><b><i>'+promotion_default_discount_promo+' for quantity ('+modulo+')</i></b></small></td><td> '+curr_sym+total_price_without_promo+'</td></tr>';
						
						total_amount+=parseFloat(total_price_without_promo);
						$("#req_individual_price_of_product_without_promotion_"+order_item_id).val(individual_price_of_product_without_promotion);
						$("#req_total_price_of_product_without_promotion_"+order_item_id).val(total_price_without_promo);
						$("#req_quantity_without_promotion_"+order_item_id).val(modulo);
						
						
					}
				}
				
				if(promo_discount_available == 1){
					total_price_of_product=parseFloat(total_price_of_product_with_promotion+total_price_of_product_without_promotion).toFixed(2);
					indi_price_with_promo=parseFloat(total_price_of_product/ordered_quantity).toFixed(2);
					total=parseFloat(indi_price_with_promo*desired_quantity_refund).toFixed(2);
					promo_str+='<tr><td>'+desired_quantity_refund+' * '+indi_price_with_promo+'</td><td>'+curr_sym+total+'</td></tr>';
					total_amount+=parseFloat(total);
			
					$("#req_individual_price_of_product_with_promotion_"+order_item_id).val(indi_price_with_promo);
					$("#req_total_price_of_product_with_promotion_"+order_item_id).val(total);
					$("#req_quantity_with_promotion_"+order_item_id).val(desired_quantity_refund);
				}
			}

			
		}else{
			//alert('else condi');
		}


		}//promo_available
		
		if(shipping_price_waived=="yes"){
			shipping_price_str="";
			shipping_charge=0;		
		}else{

			shipping_charge=Math.round(parseInt(price_paid_shipping)/parseInt(ordered_quantity))*parseInt(desired_quantity_refund);			
			shipping_price_str='<tr><td>Shipping Charge  for  '+desired_quantity_refund+'unit(s)</td><td>'+curr_sym+parseInt(shipping_charge)+'</td></tr>';
		}
			str_invoice_discount="";		

			if(promotion_available==0){
				total_product_price=(parseInt(return_value)*parseInt(desired_quantity_refund));

				if(order_item_invoice_discount_value>0){
					each_inv_dis=(Math.round((order_item_invoice_discount_value/ordered_quantity)*100))/100;
					
					inv_discount=parseFloat((each_inv_dis)*desired_quantity_refund).toFixed(2);
					total_product_price-=parseFloat(inv_discount);					
					str_invoice_discount='<tr><td style="color:blue;"> Invoice discount price which has to be deducted for this '+desired_quantity_refund+' <br>('+desired_quantity_refund+' * '+each_inv_dis+') </td><td style="color:red;">- '+curr_sym+inv_discount+'</td></tr>';
					
				}
				total_price=total_product_price+parseInt(shipping_charge);
				total_amount=parseFloat(total_price).toFixed(2);
			}
			if(promotion_available==1){
			
				if(order_item_invoice_discount_value>0){
					//invoice_discount=
					
					each_inv_dis=Math.round((order_item_invoice_discount_value/ordered_quantity)*100)/100;
					inv_discount=parseFloat((each_inv_dis)*desired_quantity_refund).toFixed(2);
					total_product_price=parseFloat(total_amount-inv_discount).toFixed(2);
					
					str_invoice_discount='<tr><td style="color:blue;"> Invoice discount price which has to be deducted for this '+desired_quantity_refund+' quantity <br>('+desired_quantity_refund+' * '+each_inv_dis+') </td><td style="color:red;">- '+curr_sym+inv_discount+'</td></tr>';
					
					total_amount=parseFloat(parseFloat(total_product_price)+parseFloat(shipping_charge)).toFixed(2);
				
				}else{
					total_amount=parseFloat(total_amount)+parseFloat(shipping_charge);	
				}				
				
			}
			
		str='<table class="table table-bordered table-hover"><thead><tr><th>Net Amount Calculations</th><th>Value</th><tr></thead><tbody>';
			
			
		str+=promo_str;
		str+=str_invoice_discount;
		str+=shipping_price_str;
		
		str+='<tr><td>Total Refund Amount </td><td>'+curr_sym+total_amount+'</td></tr><tr><td colspan=2><i>{Refund will take effect after successful reciept of the products by Admin}</i></td></tr></tbody></table>';
			
		document.getElementById('refund_order_summary_'+order_item_id).innerHTML=str;
	
	}
	

$(document).ready(function(){
	
	function toggleIcon(e) {
		$(e.target)
			.prev('.panel-heading')
			.find(".more-less")
			.toggleClass('glyphicon-plus glyphicon-minus');
	}
	$('.panel-group').on('hidden.bs.collapse', toggleIcon);
	$('.panel-group').on('shown.bs.collapse', toggleIcon);
});

</script>

<script type="text/javascript">

function open_replacement_cancel_divFun(order_item_id){
	//alert("customer_replacement_cancel_"+order_item_id);
	$("#customer_replacement_cancel_"+order_item_id).toggle();
	
	//document.getElementById("customer_replacement_cancel_"+order_item_id).style.display="";
}
function replacement_cancel_fun(order_item_id){
	var err=0;
	var quantity_replacement=$("input[name=quantity_replacement]").val();
	var replacement_with_inventory_id=$("input[name=replacement_with_inventory_id]").val();
	var comments=$('#cancel_replacement_comments_'+order_item_id).val();
	
	if(document.getElementById('cancel_replacement_comments_'+order_item_id).style.display=="none"){
		err = 2;
	}
	if(comments=='' && document.getElementById('cancel_replacement_comments_'+order_item_id).style.display!="none"){
	   $('#cancel_replacement_comments_'+order_item_id).css("border", "1px solid red");	   
	   err = 1;
	}else{			
		$('#cancel_replacement_comments_'+order_item_id).css({"border": "1px solid #ccc"});
	}
	
	var checkedValue = null; 
	var inputElements = document.getElementsByName('refund_method_'+order_item_id);
				
	for(var i=0; inputElements[i]; ++i){
		if(inputElements[i].checked){
		checkedValue = inputElements[i].value;
		break;
		}
	}

	if(inputElements[i]){
		if(checkedValue==null){
			
			/*bootbox.alert({
			  size: "small",
			  message: 'Please Choose refund method',
			  callback: function () { return false; }
			  
			});	*/

            alert('Please Choose refund method');
            return false;
		}
	
	}

	if(checkedValue=="Bank Transfer"){
		
		if($('#bank_name_rep_'+order_item_id).val()==""){
			$('#bank_name_rep_'+order_item_id).focus();
			return false;
		}else if($('#bank_branch_name_rep_'+order_item_id).val()==""){
			$('#bank_branch_name_rep_'+order_item_id).focus();
			return false;
		}else if($('#bank_city_rep_'+order_item_id).val()==""){
			$('#bank_city_rep_'+order_item_id).focus();
			return false;
		}else if($('#bank_ifsc_code_rep_'+order_item_id).val().length!=6){
			$('#bank_ifsc_code_rep_'+order_item_id).focus();
			return false;
		}else if($('#bank_account_number_rep_'+order_item_id).val()==""){
			$('#bank_account_number_rep_'+order_item_id).focus();
			return false;
		}else if($('#bank_account_number_rep_'+order_item_id).val()!=$('#bank_account_confirm_number_rep_'+order_item_id).val()){
			$('#bank_account_confirm_number_rep_'+order_item_id).focus();
			return false;
		}else if($('#account_holder_name_rep_'+order_item_id).val()==""){
			$('#account_holder_name_rep_'+order_item_id).focus();
			return false;
		}else if($('#bank_return_refund_phone_number_rep_'+order_item_id).val().length !=10){
			$('#bank_return_refund_phone_number_rep_'+order_item_id).focus();
			return false;
		}else if($('#cancel_replacement_comments'+order_item_id).val()==""){
			$('#cancel_replacement_comments'+order_item_id).focus();
			return false;
		}
	}
		
	paid_by=document.getElementById("paid_by_"+order_item_id).value;
	cancel_replacement_comments=document.getElementById("cancel_replacement_comments_"+order_item_id).value;
	
	type_of_replacement_cancel="refund_back";

    $('#cancel_replacement_btn_'+order_item_id).prop('disabled', true);//button will be disabled untill reload

	if(err==0){
		$.ajax({
		url:"<?php echo base_url();?>Account/replacement_cancel",
		type:"POST",      				// Type of request to be send, called as method
		data:"order_item_id="+order_item_id+"&cancel_replacement_comments="+cancel_replacement_comments+"&type_of_replacement_cancel="+type_of_replacement_cancel+"&paid_by="+paid_by+"&"+$("#refund_method_for_cancel_form_"+order_item_id).serialize()+"&replacement_with_inventory_id="+replacement_with_inventory_id+"&quantity_replacement="+quantity_replacement,	
		success: function(data) {
			if(data){
			
				/*bootbox.alert({
				  size: "small",
				  message: 'Replacement cancelled successfully',
				  callback: function () { location.reload(); }
				});	*/

                alert('Replacement cancelled successfully');
                location.reload();
			
			}else{
				
				/*bootbox.alert({
				  size: "small",
				  message: 'Error',
				  callback: function () { location.reload(); }
				});*/
                alert('Error');
                location.reload();
			}
			//location.reload();
		}	        
		});
	}
	if(err==2){
		$.ajax({
		url:"<?php echo base_url();?>Account/replacement_cancel",
		type:"POST",      				// Type of request to be send, called as method
		data:"order_item_id="+order_item_id+"&cancel_replacement_comments="+cancel_replacement_comments+"&type_of_replacement_cancel="+type_of_replacement_cancel+"&paid_by="+paid_by+"&"+$("#refund_method_for_cancel_form_"+order_item_id).serialize()+"&replacement_with_inventory_id="+replacement_with_inventory_id+"&quantity_replacement="+quantity_replacement,	
		success: function(data) {
			if(data){
				/*bootbox.alert({
				  size: "small",
				  message: 'Successfully submitted',
				  callback: function () { location.reload(); }
				});	*/

                alert('Successfully submitted');
                location.reload();
			}else{
				/*bootbox.alert({
				  size: "small",
				  message: 'Error',
				  callback: function () { location.reload(); }
				});*/

                alert('Error',);
                location.reload();
			}
			//location.reload();
		}	        
		});
	}
				
}
					</script>
					
					<script type="text/javascript">
		function use_this_account_data_rep(obj,order_item_id){
			document.getElementById('errorsrep').style.display = 'none';
			document.getElementById('errorsrep1').style.display = 'none';
			document.getElementById('errorsrep2').style.display = 'none';
			document.getElementById('errorsrep3').style.display = 'none';
			document.getElementById('add_new_bank_details_button_rep_'+order_item_id).style.display = '';
			var bank_name=document.getElementById('bank_name_rep_'+obj).innerHTML
			var branch_name=document.getElementById('branch_name_rep_'+obj).innerHTML
			var city=document.getElementById('city_rep_'+obj).innerHTML
			var ifsc=document.getElementById('ifsc_rep_'+obj).innerHTML
			var account_number=document.getElementById('account_number_rep_'+obj).innerHTML
			var account_holder_name=document.getElementById('account_holder_name_rep_'+obj).innerHTML
			var account_holder_numer=document.getElementById('account_holder_numer_rep_'+obj).innerHTML
			
			document.getElementById('bank_name_rep_'+order_item_id).value=bank_name;
			document.getElementById('bank_branch_name_rep_'+order_item_id).value=branch_name;
			document.getElementById('bank_city_rep_'+order_item_id).value=city;
			document.getElementById('bank_ifsc_code_rep_'+order_item_id).value=ifsc;
			document.getElementById('bank_account_number_rep_'+order_item_id).value=account_number;
			document.getElementById('bank_account_confirm_number_rep_'+order_item_id).value=account_number;
			document.getElementById('account_holder_name_rep_'+order_item_id).value=account_holder_name;
			document.getElementById('bank_return_refund_phone_number_rep_'+order_item_id).value=account_holder_numer;
			document.getElementById('bank_name_rep_'+order_item_id).setAttribute('readOnly',true);
			document.getElementById('bank_branch_name_rep_'+order_item_id).setAttribute('readOnly',true);
			document.getElementById('bank_city_rep_'+order_item_id).setAttribute('readOnly',true);
			document.getElementById('bank_ifsc_code_rep_'+order_item_id).setAttribute('readOnly',true);
			document.getElementById('bank_account_number_rep_'+order_item_id).setAttribute('readOnly',true);
			document.getElementById('bank_account_confirm_number_rep_'+order_item_id).setAttribute('readOnly',true);
			document.getElementById('account_holder_name_rep_'+order_item_id).setAttribute('readOnly',true);
			document.getElementById('bank_return_refund_phone_number_rep_'+order_item_id).setAttribute('readOnly',true);
			document.getElementById('save_this_bank_data_rep_'+order_item_id).disabled=true;
			$('#all_bank_accounts_rep_'+order_item_id).modal('hide');
			
		}
		function add_new_bank_data_rep(order_item_id){
			document.getElementById('bank_name_rep_'+order_item_id).value="";
			document.getElementById('bank_branch_name_rep_'+order_item_id).value="";
			document.getElementById('bank_city_rep_'+order_item_id).value="";
			document.getElementById('bank_ifsc_code_rep_'+order_item_id).value="";
			document.getElementById('bank_account_number_rep_'+order_item_id).value="";
			document.getElementById('bank_account_confirm_number_rep_'+order_item_id).value="";
			document.getElementById('account_holder_name_rep_'+order_item_id).value="";
			document.getElementById('bank_return_refund_phone_number_rep_'+order_item_id).value="";
			document.getElementById('bank_name_rep_'+order_item_id).readOnly=false;
			document.getElementById('bank_branch_name_rep_'+order_item_id).readOnly=false;
			document.getElementById('bank_city_rep_'+order_item_id).readOnly=false;
			document.getElementById('bank_ifsc_code_rep_'+order_item_id).readOnly=false;
			document.getElementById('bank_account_number_rep_'+order_item_id).readOnly=false;
			document.getElementById('bank_account_confirm_number_rep_'+order_item_id).readOnly=false;
			document.getElementById('account_holder_name_rep_'+order_item_id).readOnly=false;
			document.getElementById('bank_return_refund_phone_number_rep_'+order_item_id).readOnly=false;
			det_count=$('#saved_bank_details_rep_'+order_item_id).val().trim();
			if(det_count<3){
				document.getElementById('save_this_bank_data_rep_'+order_item_id).disabled=false;
			}
		}
		

function isAlfa(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
        return false;
    }
    return true;
}
function isNumber(evt) {

    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
	
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
	
    return true;
}
	
	function validate_ifsc_rep(order_item_id){
		var value = document.getElementById('bank_ifsc_code_rep_'+order_item_id).value;
		if (value.length != 6) {
			document.getElementById("errorsrep").style.display = "block";
			
		}else if (value.length == 6){
	 
		document.getElementById("errorsrep").style.display = "none";
		}
	}
	
	function ValidateNumberRep(obj,order_item_id) {
		var value = document.getElementById('bank_return_refund_phone_number_rep_'+order_item_id).value;
		if (value.length != 10) {
			document.getElementById("errorsrep3").style.display = "block";
			
		}else if (value.length == 10){
	 
		document.getElementById("errorsrep3").style.display = "none";
		}
		val=obj.value;
		
	}

	function validate_account_rep(order_item_id){
		 
		var value = document.getElementById('bank_account_number_rep_'+order_item_id).value;
		if (value.length != 15) {
			document.getElementById("errorsrep1").style.display = "block";
			
		}else if (value.length == 15){
	 
		document.getElementById("errorsrep1").style.display = "none";
		}
	}
	
	function validate_confirm_rep(order_item_id){
		var value = document.getElementById('bank_account_confirm_number_rep_'+order_item_id).value;
		var value_1 = document.getElementById('bank_account_number_rep_'+order_item_id).value;
		
		
		if (value_1!=value) {
			document.getElementById("errorsrep2").style.display = "block";
			
		}else if (value_1==value){
	 
		document.getElementById("errorsrep2").style.display = "none";
		}
	}
	
	
</script>

<script>
	function call_conversation(order_item_id){
		
		$("#send_form_admin_contact_admin_form_rep_"+order_item_id).attr('action','<?php echo base_url();?>Account/offline_contact_admin_replacement');
		$("form[name=send_form_admin_contact_admin_form_rep_"+order_item_id+"]").submit();	
	}
	</script>
	
		<script>
	function open_return_cancel_divFun(order_item_id){
		$('#customer_return_cancel_'+order_item_id).toggle();
		
		//document.getElementById("customer_return_cancel_"+order_item_id).style.display="";
	}
	function return_cancel_fun(order_item_id){
		cancel_returns_comments=document.getElementById("cancel_returns_comments_"+order_item_id).value;
		$.ajax({
			url:"<?php echo base_url();?>Account/return_cancel",
			type: "POST",      				// Type of request to be send, called as method
			data:  "order_item_id="+order_item_id+"&cancel_returns_comments="+cancel_returns_comments,	
			success: function(data) {
			if(data){
				
				/*bootbox.alert({
				  size: "small",
				  message: 'Return cancelled successfully',
				  callback: function () { location.reload(); }
				});	*/

                alert('Return cancelled successfully');
                location.reload();

			}else{
					/*bootbox.alert({
				  size: "small",
				  message: 'Error',
				  callback: function () { location.reload(); }
				});*/
                alert('Error');
                location.reload();

			}
			//location.reload();
			}	        
		});
	}
	
	
	function call_conversation_refund(order_item_id){
		
		$("#send_form_admin_contact_admin_form_"+order_item_id).attr('action','<?php echo base_url();?>Account/offline_contact_admin');
		$("form[name=send_form_admin_contact_admin_form_"+order_item_id+"]").submit();	
	}
	
	</script>
	
	
	<script>
function ValidateNo() {
        var phoneNo = document.getElementById('bank_return_refund_phone_number');

    if (phoneNo.value == "" || phoneNo.value == null) {
	
            //alert("Please enter your Mobile No.");
			phoneNo.value="";
			phoneNo.focus();
            return false;
        }
        else if (phoneNo.value.length < 10 || phoneNo.value.length > 10) {
		
            return false;
        }else{
		
		
		return true;
		}

        }
		
		
		
		
function isAlfa(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
        return false;
    }
    return true;
}
function isNumber(evt) {

    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
	
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
	
    return true;
}
	
	function validate_ifsc(order_item_id){
		var value = document.getElementById('bank_ifsc_code_cancel_'+order_item_id).value;
		if (value.length != 6) {
			document.getElementById("errors").style.display = "block";
			
		}else if (value.length == 6){
	 
		document.getElementById("errors").style.display = "none";
		}
	}
	
	function ValidateNumber(obj,order_item_id) {
		var value = document.getElementById('bank_return_refund_phone_number_cancel_'+order_item_id).value;
		if (value.length != 10) {
			document.getElementById("errors3").style.display = "block";
			
		}else if (value.length == 10){
	 
		document.getElementById("errors3").style.display = "none";
		}
		val=obj.value;
		/*if(val!=""){
			if(val=="+91 | "){ obj.value="";}
			else if(val.indexOf("+91") !== -1){}
			else{ obj.value="+91 | "+val;}
		}*/
	}

	function validate_account(order_item_id){
		 
		var value = document.getElementById('bank_account_number_cancel_'+order_item_id).value;
		if (value.length != 15) {
			document.getElementById("errors1").style.display = "block";
			
		}else if (value.length == 15){
	 
		document.getElementById("errors1").style.display = "none";
		}
	}
	
	function validate_confirm(order_item_id){
		var value = document.getElementById('bank_account_confirm_number_cancel_'+order_item_id).value;
		var value_1 = document.getElementById('bank_account_number_cancel_'+order_item_id).value;
		
		if(value!=value_1){
			document.getElementById("errors2").style.display = "block";
		}else if(value==value_1){
			document.getElementById("errors2").style.display = "none";
		}

	}
	
	
</script>
<script>
	function use_this_account_data(obj,order_item_id){
		document.getElementById('errors').style.display = 'none';
		document.getElementById('errors1').style.display = 'none';
		document.getElementById('errors2').style.display = 'none';
		document.getElementById('errors3').style.display = 'none';
		document.getElementById('add_new_bank_details_button_'+order_item_id).style.display = '';
		var bank_name=document.getElementById('bank_name_cancel_'+obj).innerHTML
		var branch_name=document.getElementById('bank_branch_name_cancel_'+obj).innerHTML
		var city=document.getElementById('bank_city_cancel_'+obj).innerHTML
		var ifsc=document.getElementById('bank_ifsc_code_cancel_'+obj).innerHTML
		var account_number=document.getElementById('bank_account_number_cancel_'+obj).innerHTML
		var account_holder_name=document.getElementById('account_holder_name_cancel_'+obj).innerHTML
		var account_holder_numer=document.getElementById('bank_return_refund_phone_number_cancel_'+obj).innerHTML
		
		document.getElementById('bank_name_cancel_'+order_item_id).value=bank_name;
		document.getElementById('bank_branch_name_cancel_'+order_item_id).value=branch_name;
		document.getElementById('bank_city_cancel_'+order_item_id).value=city;
		document.getElementById('bank_ifsc_code_cancel_'+order_item_id).value=ifsc;
		document.getElementById('bank_account_number_cancel_'+order_item_id).value=account_number;
		document.getElementById('bank_account_confirm_number_cancel_'+order_item_id).value=account_number;
		document.getElementById('account_holder_name_cancel_'+order_item_id).value=account_holder_name;
		document.getElementById('bank_return_refund_phone_number_cancel_'+order_item_id).value=account_holder_numer;
		document.getElementById('bank_name_cancel_'+order_item_id).setAttribute('readOnly',true);
		document.getElementById('bank_branch_name_cancel_'+order_item_id).setAttribute('readOnly',true);
		document.getElementById('bank_city_cancel_'+order_item_id).setAttribute('readOnly',true);
		document.getElementById('bank_ifsc_code_cancel_'+order_item_id).setAttribute('readOnly',true);
		document.getElementById('bank_account_number_cancel_'+order_item_id).setAttribute('readOnly',true);
		document.getElementById('bank_account_confirm_number_cancel_'+order_item_id).setAttribute('readOnly',true);
		document.getElementById('account_holder_name_cancel_'+order_item_id).setAttribute('readOnly',true);
		document.getElementById('bank_return_refund_phone_number_cancel_'+order_item_id).setAttribute('readOnly',true);
		document.getElementById('save_this_bank_data_'+order_item_id).disabled=true;
		$('#all_bank_accounts').modal('hide');
	}
	function add_new_bank_data(order_item_id){
		document.getElementById('bank_name_cancel_'+order_item_id).value="";
		document.getElementById('bank_branch_name_cancel_'+order_item_id).value="";
		document.getElementById('bank_city_cancel_'+order_item_id).value="";
		document.getElementById('bank_ifsc_code_cancel_'+order_item_id).value="";
		document.getElementById('bank_account_number_cancel_'+order_item_id).value="";
		document.getElementById('bank_account_confirm_number_cancel_'+order_item_id).value="";
		document.getElementById('account_holder_name_cancel_'+order_item_id).value="";
		document.getElementById('bank_return_refund_phone_number_cancel_'+order_item_id).value="";
		document.getElementById('bank_name_cancel_'+order_item_id).readOnly=false;
		document.getElementById('bank_branch_name_cancel_'+order_item_id).readOnly=false;
		document.getElementById('bank_city_cancel_'+order_item_id).readOnly=false;
		document.getElementById('bank_ifsc_code_cancel_'+order_item_id).readOnly=false;
		document.getElementById('bank_account_number_cancel_'+order_item_id).readOnly=false;
		document.getElementById('bank_account_confirm_number_cancel_'+order_item_id).readOnly=false;
		document.getElementById('account_holder_name_cancel_'+order_item_id).readOnly=false;
		document.getElementById('bank_return_refund_phone_number_cancel_'+order_item_id).readOnly=false;
		
		
		det_count=$('#saved_bank_details_'+order_item_id).val().trim();
		if(det_count<3){
			document.getElementById('save_this_bank_data_'+order_item_id).disabled=false;
		}
	}

function remove_error(order_item_id){
	$('#cancel_replacement_comments_'+order_item_id).css({"border": "1px solid #ccc"});
	
}
function is_mobile(){
	var isMobile = false; //initiate as false
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

return isMobile;
}


$(document).ready(function(){
	showChar(200);
});
</script>  