<?php
		$delivered_flag=$controller->check_all_order_items_delivered_report($order_id);
		$cancelled_flag=$controller->check_all_order_items_canceled($order_id);
		
		if(!empty($invoice_offers)){
			
		   if($invoice_offer_achieved==1){ 
						   
			?>
			<div class="panel-group">
				<div class="panel panel-default"> 
                             
					<div class="panel-body">
						   <?php
						   if($total_amount_cash_back>0){   
							?>
							<div class="row">
							<div class="col-md-12 text-center">
							<ul class="list-inline">
							  <li class="bold">Total Invoice cash back:</li>
							  <li><?php echo curr_sym;?><?php echo $promotion_invoice_cash_back; ?></li>
							</ul>
							</div>
							</div>                              
							
							<?php 
						
							if($delivered_flag==1){
								
								if($customer_response_for_invoice_cashback==''){
								
								?>
							<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-cash" aria-hidden="true"></i> Eligible for Invoice Cashback</i></b></span>
							</div>
							</div> 

							<div class="row">
							<div class="col-md-9">
							If you don't want to return the delivered items, Please click the <b>'ACCEPT'</b> button and cashback amount will be credited to your wallet.
							</div>
							<div class="col-md-3">
							<button class="btn btn-success preventDflt" id="accept_cashback" onclick="invoice_cash_back_fun('<?php echo $order_id; ?>','accept','<?php echo $promotion_invoice_cash_back; ?>')">ACCEPT</button> <button id="reject_cashback" class="btn btn-danger preventDflt" onclick="invoice_cash_back_fun('<?php echo $order_id; ?>','reject')">REJECT</button>
							</div>
							</div>			
									<?php
									
								}
								if($customer_response_for_invoice_cashback=='reject' && $status_of_refund_for_cashback==''){
									?>
							<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> <del>Eligible for Invoice Cashback.</del> Rejected </i></b></span>
							</div>
							</div>
									
									
									<?php
								}
								if($customer_response_for_invoice_cashback=='accept' &&  $status_of_refund_for_cashback=='refunded'){
									?>
									<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-cash" aria-hidden="true"></i> Cash back amount <?php echo curr_sym;?><?php echo $promotion_invoice_cash_back; ?> was credited to your wallet.</i></b></span>
							</div>
							</div>
									
									<?php
								}
								if($customer_response_for_invoice_cashback=='accept' &&  $status_of_refund_for_cashback=='pending'){
									?>
									<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-cash" aria-hidden="true"></i> Cash back amount <?php echo curr_sym;?><?php echo $promotion_invoice_cash_back; ?> will be credited to your wallet soon.</i></b></span>
							</div>
							</div>
									
									<?php
								}
							}else{ //delivered_flag
									
								if($customer_response_for_invoice_cashback=='reject' && $status_of_refund_for_cashback==''){
									?>
							<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> <del>Eligible for Invoice Cashback.</del> Rejected</i></b></span>
							</div>
							</div>

									
									<?php
								
								}else{
									//check the invoice msg cancel that there cuustomer cancels or admin cancels
									if($cancelled_flag==1){
										?>
							<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-cash" aria-hidden="true"></i> All ordered items are cancelled . So this invoice is not eligible for Invoice Cash back offer </i></b></span>
							</div>
							</div>
										
										<?php		
									}
									if($cancelled_flag!=1){
										?>
							<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-cash" aria-hidden="true"></i> This cash back amount will be credited to your wallet when all items are delivered under this order </i></b></span>
							</div>
							</div>
										
										<?php
									}
								}
									
							}
							?>
							
							<?php } ?>
							<div class="row">
							<div class="col-md-12 text-center">
							<ul class="list-inline">
							  <li class="bold">Item(s) Subtotal <!--without any Promotion-->:</li>
							  <li><?php echo curr_sym;?><?php echo $total_price_without_shipping_price; ?></li>
							</ul>
							</div>
							</div>
						   
						   <?php
							if($promotion_invoice_free_shipping>0){
							?>
							<div class="row">
							<div class="col-md-12 text-center">
							<ul class="list-inline">
							  <li class="bold">Shipping Charges waived on invoice</li>
							  <li><?php echo curr_sym;?><?php echo $promotion_invoice_free_shipping; ?></li>
							</ul>
							</div>
							</div>
							                           
							<?php } ?>
							
							<?php
							if($promotion_invoice_discount>0){
	   
							?>

							<div class="row">
							<div class="col-md-12 text-center">
							<ul class="list-inline">
							  <li class="bold">Total Invoice Discount</li>
							  <li><?php echo curr_sym;?><?php echo $promotion_invoice_discount; ?></li>
							</ul>
							</div>
							</div>
							                           
							
							<?php } ?>
							
                                            <?php
                                                        if($invoice_coupon_status=='1'){
										?>
                                                        <div class="row">
                                                        <div class="col-md-12 text-center">
							<ul class="list-inline">
                                                            <li class="bold">Total Coupon Discount <?php echo $inc_txt; ?></li>
                                                            <li> <?php echo curr_sym; ?><?php echo round($invoice_coupon_reduce_price); ?></li>
                                                        </ul>
							</div>
							</div>		
                                                            <?php } ?>
                                                                                
                                            <?php /*
							if($total_coupon_used_amount>0){
	   
							?>

							<div class="row">
							<div class="col-md-12 text-center">
							<ul class="list-inline">
							  <li class="bold">Total Coupon Discount</li>
							  <li><?php echo curr_sym;?><?php echo round($total_coupon_used_amount); ?></li>
							</ul>
							</div>
							</div>
							                           
							
							<?php } 
                                             */ ?>
							
                                            
							<?php
							if($total_amount_saved>0){
							?>
							<div class="row">
							<div class="col-md-12 text-center">
							<ul class="list-inline">
							  <li class="bold">Total amount you saved on invoice</li>
							  <li><?php echo curr_sym;?><?php echo $total_amount_saved; ?></li>
							</ul>
							</div>
							</div>                           
							<?php } ?>
							
							<?php
							if($total_amount_saved>0){
							?>
							<div class="row">
							<div class="col-md-12 text-center">
							<ul class="list-inline">
							  <li class="bold">Total amount paid on invoice</li>
							  <li><?php echo curr_sym;?><?php echo $total_amount_to_pay; ?></li>
							</ul>
							</div>
							</div>
							                          
							<?php } ?>
							
							<?php

							if($invoice_surprise_gift!=''){
								
								$free_items_status_arr=$controller->get_status_of_free_items($invoices_offers_id);
								
								//print_r($free_items_status_arr);
								
								$array_delivered=$free_items_status_arr["array_delivered"];
								$array_shipped=$free_items_status_arr["array_shipped"];
								$array_comments=$free_items_status_arr["array_comments"];
								$array_undelivered=$free_items_status_arr["array_undelivered"];
								
								if(!in_array("0",$array_delivered)){
									$free_items_status="delivered";
								}else{
									if(!in_array("0",$array_shipped)){
										$free_items_status="shipped";
									}else{
										$free_items_status="processing";
									}	
								}
								
								
								
								//echo $delivered_flag;echo $cancelled_flag;
								
									if($invoice_surprise_gift_type=='Qty'){
									
										if($delivered_flag==1){
										
										//show the free items in accordian after delivered
										//check all items are delivered under this order
										//check customer accept or reject for surprise gifts
										if($customer_response_for_surprise_gifts==''){
											
										?>
										<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> Eligible for surprise gifts (On Invoice)</i></b></span>
							</div>
							</div>
							<div class="row">
							<div class="col-md-9">
							If you don't want to return the delivered items, Please click the <b>'ACCEPT'</b> button and receive surprise Gifts.
							</div>
							<div class="col-md-3">
							<button class="btn btn-success preventDflt" id="accept_free_surprise_gift" onclick="free_surprise_gift_fun('<?php echo $order_id; ?>','accept')">ACCEPT</button> <button id="reject_free_surprise_gift" class="btn btn-danger preventDflt" onclick="free_surprise_gift_fun('<?php echo $order_id; ?>','reject')">REJECT</button>
							</div>
							</div>								
											<?php
									
										}

										if($customer_response_for_surprise_gifts=='reject' &&  $status_of_refund_for_surprise_gifts==''){
											?>
											<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> <del>Eligible for surprise gifts (On Invoice).</del> Rejected</i></b></span>
							</div>
							</div>

											
											<?php
										}
										
										if($customer_response_for_surprise_gifts=='accept'){
											
										?>
							<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> Eligible for surprise gifts (On Invoice).</i></b></span>
							</div>
							</div>
			
		<div class="row">
		<div class="col-md-12">
		<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          <b><i> <i class="fa fa-gift" aria-hidden="true"></i> Free Surprise Gifts </i></b>
        </a>
      </h4>
    </div>

						<div id="collapseOne" class="panel-collapse">
						<div class="panel-body">
							
							<?php
						
						$invoice_surprise_gift_skus=rtrim($invoice_surprise_gift_skus,',');
						$invoice_surprise_gift_skus_nums=rtrim($invoice_surprise_gift_skus_nums,',');
						
						$invoice_surprise_gift_skus_arr = explode(',', $invoice_surprise_gift_skus);
						$invoice_surprise_gift_skus_nums_arr = explode(',', $invoice_surprise_gift_skus_nums);
						
						$two=array_combine($invoice_surprise_gift_skus_arr,$invoice_surprise_gift_skus_nums_arr);	
						
						if($invoice_surprise_gift_skus!=''){
							
							$invoice_free_items_arr=$controller->get_free_skus_from_inventory_with_details($invoice_surprise_gift_skus,$invoice_surprise_gift_skus_nums);
							$j=0;
							foreach($invoice_free_items_arr as $invoice_free_items){
							
							?>
							<div class="row free_items_style margin-top">
							
								<div class="col-md-2 text-center">						
								<img src="<?php echo base_url().$invoice_free_items->image?>" alt="" style="height: 16vh;">
								</div>
								
								<div class="col-md-4">
								<ul class="list-unstyled">
								<li><?php echo ($invoice_free_items->sku_name!='') ? $invoice_free_items->sku_name : $invoice_free_items->product_name; ?></li>
								<li><small>
									<?php
									$invoice_free_items->attribute_1_value= substr($invoice_free_items->attribute_1_value, 0, strpos($invoice_free_items->attribute_1_value, ":"));
									
									if(!empty($invoice_free_items->attribute_1))
										echo '<div class="f-row">'.$invoice_free_items->attribute_1." : ".$invoice_free_items->attribute_1_value.'</div>';
									if(!empty($invoice_free_items->attribute_2))
										echo '<div class="f-row">'.$invoice_free_items->attribute_2." : ".$invoice_free_items->attribute_2_value.'</div>';
									if(!empty($invoice_free_items->attribute_3))
										echo '<div class="f-row">'.$invoice_free_items->attribute_3." : ".$invoice_free_items->attribute_3_value.'</div>';
									if(!empty($invoice_free_items->attribute_4))
										echo '<div class="f-row">'.$invoice_free_items->attribute_4." : ".$invoice_free_items->attribute_4_value.'</div>';
									?>
									</small></li>
									<li>Quantity : <?php 
									$id=$invoice_free_items->id;
									echo ($two[$id]);?></li>
								</ul>
								</div>
								<div class="col-md-2">
									<del> <?php 
									echo curr_sym.$invoice_free_items->selling_price;?></del>
								</div>
								<div class="col-md-4">
								<ul class="list-unstyled">
								  <li>Status: <?php
									if($array_undelivered[$j]==1){
										echo 'UnDelivered';
									}else{
										
										if($array_delivered[$j]==1){
											echo 'Delivered';
										}else{										
											if($array_delivered[$j]==0 && $array_shipped[$j]==1){
												echo 'Shipped';
											}elseif($array_delivered[$j]==0 && $array_shipped[$j]==0){
												echo 'ready to place';
											}
										}
									}

									?></li>
									<li>comments: <?php if(!empty($array_comments[$j])){ 
											echo '<span class="more">'.$array_comments[$j].'</span>';
									}
									?></li>
								</ul>
									
									
									
								</div>
								
							</div>
							<?php
							$j++;
							}
						}		
							?>
						
						</div>
						</div>
					
				</div>
				</div>
		</div>
									
							<?php
	
										}//customer_accepts
										
										}else{ //delivered_flag
										
										
										if($customer_response_for_surprise_gifts=='reject' &&  $status_of_refund_for_surprise_gifts==''){
											?>
											<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> <del>Eligible for surprise gifts (On Invoice).</del> Rejected</i></b></span>
							</div>
							</div>			
											<?php
										}else{
											
										
										
												//check the invoice msg cancel that there cuustomer cancels or admin cancels
												if($cancelled_flag!=1){		
												
												?>
												<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> Eligible for surprise gifts (On Invoice)</i></b></span>
							</div>
							</div>

												
												<?php
												
												}elseif($cancelled_flag==1){//order cancelled
												
												?>
												<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> <del>Eligible for surprise gifts (On Invoice)</del>  All order items are cancelled</i></b></span>
							</div>
							</div>
												
												
												<?php
												//debited from wallet
												}else{
													////processing
												}
										}
										
										}
										
									}//gift_type Qty
									
									if($invoice_surprise_gift_type==curr_code){

										if($delivered_flag==1){
											
											if($customer_response_for_surprise_gifts==''){
											
												?>
												<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> Eligible for surprise gifts (On Invoice)</i></b></span>
							</div>
							</div>
											<div class="row">
							<div class="col-md-9">
							If you don't want to return the delivered items, Please click the <b>'ACCEPT'</b> button and receive surprise Gifts.
							</div>
							<div class="col-md-3">
							<button class="btn btn-sm btn-success preventDflt" id="accept_free_surprise_gift" onclick="free_surprise_gift_fun('<?php echo $order_id; ?>','accept')">ACCEPT</button> <button id="reject_free_surprise_gift" class="btn btn-sm btn-danger preventDflt" onclick="free_surprise_gift_fun('<?php echo $order_id; ?>','reject')">REJECT</button>
							</div>
							</div>									
													<?php
											
												}

												if($customer_response_for_surprise_gifts=='reject' && $status_of_refund_for_surprise_gifts==""){
													
													?>
													<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> <del> Eligible for surprise gifts (On Invoice).</del> Rejected</i></b></span>
							</div>
							</div>

													
													<?php
												}
												
												if($customer_response_for_surprise_gifts=='accept' && $status_of_refund_for_surprise_gifts=="refunded"){
													
													?>
											<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> <?php echo curr_sym;?><?php echo $invoice_surprise_gift; ?> was credited to your wallet (surprise gift)</i></b></span>
							</div>
							</div>
												<?php
													
												}
												if($customer_response_for_surprise_gifts=='accept' && $status_of_refund_for_surprise_gifts=="pending"){
													
													?>
											<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> <?php echo curr_sym;?><?php echo $invoice_surprise_gift; ?> will be credited to your wallet (surprise gift) Soon</i></b></span>
							</div>
							</div>
											
												
												<?php
													
												}
										
										
										}else{

											if($customer_response_for_surprise_gifts=='reject' &&  $status_of_refund_for_surprise_gifts==''){
												?>
												<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> <del>Eligible for surprise gifts (On Invoice).</del> Rejected</i></b></span>
							</div>
							</div>

												
												<?php
											}else{
												
											
												if($cancelled_flag!=1){
													?>
												<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> Eligible for surprise gifts (On Invoice).</i></b></span>
							</div>
							</div>

													
													<?php
											
												}elseif($cancelled_flag==1){
													//all order items are cancelled
													?>
													<div class="row">
							<div class="col-md-12 text-center">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i><del> Eligible for surprise gifts (On Invoice)</del> All items are cancelled </i></b></span>
							</div>
							</div>
															
													<?php
													
												}else{
													//processing
												}
											}
										}
											
									}//gift type

							}//surprise gift
								
								?>
								
								
					</div>
                            
				</div>
			</div><!---panel group----->      
	  <?php
	  
	   }
	}
	
	 ?>
						 