<?php 
if(count($get_data_from_return_stock_notification_return_order_id[0])!=0){
			foreach($get_data_from_return_stock_notification_return_order_id as $stock_return_data){
				$return_order_id=$stock_return_data['return_order_id'];
			?>
		
	<div class="row">
		<div class="col-md-6">
		<div class="row">
			<?php 
			$rep_inventory_id=$stock_return_data['replacement_with_inventory_id']; 
			$inv_obj=$controller->get_details_of_products($rep_inventory_id);
			?>
			<div class="col-md-3">
				<img src="<?php echo base_url().$inv_obj->thumbnail; ?>" alt="">
			</div>
														
			<div class="col-md-9">
				<div class="headingtext bold"><u>Replacement SKU is</u></div>
				<div class="f-row small-text">
									
					<?php
					$inv_obj->attribute_1_value= substr($inv_obj->attribute_1_value, 0, strpos($inv_obj->attribute_1_value, ":"));
					
					if(!empty($inv_obj->attribute_1))
						echo '<div class="f-row">'.$inv_obj->attribute_1." : ".$inv_obj->attribute_1_value.'</div>';
					if(!empty($inv_obj->attribute_2))
						echo '<div class="f-row">'.$inv_obj->attribute_2." : ".$inv_obj->attribute_2_value.'</div>';
					if(!empty($inv_obj->attribute_3))
						echo '<div class="f-row">'.$inv_obj->attribute_3." : ".$inv_obj->attribute_3_value.'</div>';
					if(!empty($inv_obj->attribute_4))
						echo '<div class="f-row">'.$inv_obj->attribute_4." : ".$inv_obj->attribute_4_value.'</div>';
					?>
				
				</div>
				<div class="f-row small-text">SKU : <?php echo $inv_obj->sku_id; ?></div>
				<div class="f-row small-text">
					Quantity : <?php echo $stock_return_data['quantity_replacement']; ?>
				</div>
			
			</div>
		</div>
		</div>

		<div class="col-md-6">
		<div class="row">
		<div class="col-md-12">
		
		<div class="f-row">
			<?php
				$purchased_item_price=($stock_return_data["return_value_each_inventory_id"])*($stock_return_data["quantity_replacement"]);
			
				$total_item_price=($stock_return_data["replacement_value_each_inventory_id"])*($stock_return_data["quantity_replacement"]);
			?>	
			Total Item Price <?php echo curr_sym;?><?php echo $total_item_price; ?>
			
			<span id="rep_stock_<?php echo $return_order_id; ?>" data-toggle="popover" data-trigger="click" data-placement="bottom" data-container="body" class="cursor-pointer bold" content_id="#rep_stock_div_<?php echo $return_order_id; ?>" onclick="toggle_content(this)">[<i class="fa fa-question" aria-hidden="true"></i>]</span>
			
			<script>
			$(document).ready(function(){
				$('#rep_stock_<?php echo $return_order_id; ?>').popover({
					html : true,
					content: function() {
						if($(window).width() > 480){
							$('#rep_stock_div_<?php echo $return_order_id; ?>').hide();
							return $('#rep_stock_div_<?php echo $return_order_id; ?>').html();	
						}
					}
				});
			});
			</script>
			
		<div id="rep_stock_div_<?php echo $return_order_id;?>" style="display:none;">
		
			<table class="table table-hover table-bordered">
				<tbody>
				
					<tr>
						<td><p><small>Quantity Requested</small></p></td><td><p><small> <?php echo $stock_return_data["quantity_replacement"];?></small></p></td>
					</tr>
					
					<tr>
						<td><p><small>Replacement Requested</small></p></td><td><p><small> <?php echo $stock_return_data["quantity_replacement"];?></small></p></td>
					</tr>
				
	
					<tr>
						<td><p><small>Item Price</small></p></td><td><p><small> <?php echo curr_sym.$stock_return_data["replacement_value_each_inventory_id"]." each";?></small></p></td>
					</tr>
					
					
					<tr>
						<td><p><small>Total Item Price</small></p></td><td><p><small><?php echo curr_sym.$total_item_price;?></small></p></td>
					</tr>
					
					
					<tr>
						<td><p><small>Purchased Item Price</small></p></td><td><p><small> <?php echo curr_sym.$purchased_item_price;?></small></p></td>
					</tr>
					<?php
						if($total_item_price>$purchased_item_price){
							?>
						<tr>
							<td><p><small>You Pay</small></p></td><td><p><small><?php echo curr_sym.($total_item_price-$purchased_item_price);?></small></p></td>
						</tr>
							<?php
						}
					?>
					<?php
						if($total_item_price<$purchased_item_price){
							?>
							<tr>
								<td><p><small>You Get</small></p></td><td><p><small><?php echo curr_sym.($purchased_item_price-$total_item_price);?></small></p></td>
							</tr>
							<?php
						}
					?>
				</tbody>
			</table>
		
		</div>	
		
		</div>
		
		<div class="f-row">
			<?php
				$date=date_create($stock_return_data['timestamp']);
				date_add($date,date_interval_create_from_date_string($stock_return_data["timelimitdefined"]." days"));
				$target_date=date_format($date,"D, jS M'y");
			?>
			
			<?php
			
			if($stock_return_data['status']=="active"  && $stock_return_data['move_to_refund_flag']=='' && $stock_return_data['reserved_stock']==""){ ?>
			
				<div class="f-row">
				
				If the stock does not become available within <?php echo $target_date;?>. After this period full refund amount (<b><?php echo curr_sym;?><?php echo ($stock_return_data['quantity_replacement']*$stock_return_data["return_value_each_inventory_id"])  ?></b>) will be initiated
				through <?php echo $stock_return_data['refund_method'];?>
				
				</div>
				
				<?php 
				
					echo '<div class="f-row">Refund Method: ';
						
						if($stock_return_data['refund_method']=="Bank Transfer"){
							$refund_bank_id=$stock_return_data['refund_bank_id'];	
							$bank_detail_arr=$controller->get_bank_details($refund_bank_id);
							
							echo '<a id="return_stock_bank_detail_'.$return_order_id.'" data-toggle="popover" data-trigger="hover" data-placement="bottom" class="bank_detail_stock_return" style="cursor:pointer;"><u>Bank Account</u></a>';
							
							echo '<div id="return_stock_bank_detail_div_'.$return_order_id.'" class="hide">'.$bank_detail_arr.'</div>';
						}else{
							echo $stock_return_data['refund_method'];
						}
					echo '</div>';

				?>
				<script>
				$(document).ready(function(){
					$('#return_stock_bank_detail_<?php echo $return_order_id; ?>').popover({
						html : true,
						content: function() {
						  return $('#return_stock_bank_detail_div_<?php echo $return_order_id; ?>').html();
						}
					});
				});
				</script>
				<?php 
				
			}else if($stock_return_data['status']=="active" && $stock_return_data['move_to_refund_flag']=='1' && $stock_return_data['customer_decision']==''){
					
					?>
					<div class="f-row">
						Confirmation:your decision for move to refund?	
					</div>
					
					<div class="f-row">
					
					<?php 
						if($order_item['promotion_cashback']>0){
						$order_item_offers_obj=$controller->get_details_of_order_item($order_item['order_item_id']);
						$status_of_refund_for_cashback_on_sku=$order_item_offers_obj->status_of_refund;
							if($order_item_offers_obj->status_of_refund=="pending"){
								echo '<label class="alert alert-warning">';
								echo '<i>There is pending Cash back offer.If You request for refund the <b>Cash back </b>offer will be rejected</i>';
								echo '</label>';
								
								?>
								
								<input type="hidden" name="status_of_refund_for_cashback_on_sku_stock_<?php echo $order_item['order_item_id']; ?>" id="status_of_refund_for_cashback_on_sku_stock_<?php echo $order_item['order_item_id']; ?>" value="<?php echo $status_of_refund_for_cashback_on_sku; ?>">
								
								<?php 
							}
						}
					
					?>
					
					</div>
					
					<div class="f-row">
						<input type="button" value="Yes" class="btn btn-success btn-sm preventDflt" onclick="customerDecisionForMoveToRefundFromStockFun(<?php echo $stock_return_data['order_item_id'];?>,'yes');">
						<input type="button" value="No" class="btn btn-danger btn-sm preventDflt" onclick="customerDecisionForMoveToRefundFromStockFun(<?php echo $stock_return_data['order_item_id'];?>,'no');">
					</div>
					
					<?php
			
			}else if($stock_return_data['status']=="active" && $stock_return_data['reserved_stock']!=""){
				?>
					<div class="f-row">
						Stock is available now. Do you want to proceed for replacement?
					</div>
					<div class="f-row margin-top">
					
					<form name="return_stock_notification_form_<?php echo $stock_return_data['order_item_id']; ?>" id="return_stock_notification_form_<?php echo $stock_return_data['order_item_id']; ?>">	
					<input type="hidden" name="quantity_replacement" value="<?php echo $stock_return_data['quantity_replacement']; ?>">
					<input type="hidden" name="reserved_stock" value="<?php echo $stock_return_data['reserved_stock']; ?>">
					<input type="hidden" name="replacement_with_inventory_id" value="<?php echo $stock_return_data['replacement_with_inventory_id']; ?>">
					<input type="hidden" name="r_order_item_id" value="<?php echo $stock_return_data['order_item_id']; ?>">
					</form>
					
						<input type="button" value="Accept" class="btn btn-success btn-sm preventDflt" onclick="proceedForReplacementFromStock(<?php echo $stock_return_data['order_item_id'];?>);">
						<input type="button" value="Reject" class="btn btn-danger btn-sm preventDflt" onclick="proceedForCancellationFromStock(<?php echo $stock_return_data['order_item_id'];?>);">
					</div>
				<?php
			
			}else if($stock_return_data['status']=="cancelled"){
					echo '<div class="f-row"> Request has been cancelled </div>';
			}
			?>
		</div>
		
		</div>
		</div>
		</div>
	</div>
	<div class="row margin-top">
			<div class="col-md-12">
			
				<!--------------------return stock cancel--------------------------------->
				
				<!--------data-target="#customer_comment_modal_stock_<?php //echo $order_item['order_item_id'];?>"----->
				
				<button class="btn btn-default btn-sm text-uppercase preventDflt" data-toggle="modal" onclick="call_conversation_return_stock('<?php echo $order_item['order_item_id']; ?>')" >Contact us
				
				<?php
				$return_order_id=$stock_return_data["return_order_id"];
				$unread_msg=$controller->get_unread_return_msg_replacement($return_order_id);
				if($unread_msg==0){
					?>
					<span class="badge"></span>
					<?php
				}else{
					?>
					<span class="badge"><?php echo $unread_msg; ?></span>
					
					<?php
				}
				?>
			</button>
				<?php
				if($stock_return_data["status"]!="cancelled"){				
				?>
					<button class="btn btn-default btn-sm text-uppercase preventDflt" onclick="cancel_refund_request(<?php echo $stock_return_data["return_stock_notification_id"];?>)">Cancel</button>
				<?php 
				}else{
					//echo 'Return Stock Notification is Cancelled';	
				}
				?>
			
			</div>	
	</div>			
			<!---------------Contact Adminr replacement Modal starts------------------------------>
	
								

<div class="modal" id="customer_comment_modal_stock_<?php echo $order_item['order_item_id'];?>" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
		 <?php //echo $returns_refund_orders_obj->return_reason_comments; ?>
            <div class="panel panel-primary">
               <div id="accordion<?php echo $order_item['order_item_id'];?>" class="panel-heading" style="cursor:pointer;">
                  <span class="glyphicon glyphicon-comment"></span> Conversation
                  <span style="text-decoration:none;"><span data-dismiss="modal" aria-hidden="true" class="fa fa-times pull-right" id="modal_close_rep_<?php echo $order_item['order_item_id'];?>"></span></span>
               </div>
               <div aria-expanded="true" id="collapseOne" class="">
                  <div class="panel-body">
                     <ul class="chat" style="list-style-type:none;">
						<?php
							
							$replacements_conservation_obj_arr=$controller->get_returns_conservation_chain_in_customerpanel_for_replacement($return_order_id);
							?>
							<input type="hidden" id="return_order_id_for_msg_stock_<?php echo $order_item['order_item_id'];?>" value="<?php echo $return_order_id; ?>">
							<?php
							foreach($replacements_conservation_obj_arr as $replacements_conservation_obj){
								?>
								
								<li class="left clearfix">
                           <span>
                              <!--<img src="/assets/pictures/images/cust_a.png" alt="Customer" class="img-circle" />-->
                              <div class="header">
                                 <?php
									if($replacements_conservation_obj->user_type=="admin"){
								 ?>
								 <span style="color:#FD856A;">Admin :</span>
								 
								 <?php
									}
								 ?>
								  <?php
									if($replacements_conservation_obj->user_type=="customer"){
								 ?>
								 <span style="color:#ff00ff;"><?php echo $controller->get_customer_name($replacements_conservation_obj->customer_id);?> :</span>
								 <?php
									}
								 ?>
                                 <small class="text-muted pull-right">
                                 <span class="glyphicon glyphicon-time"></span> <?php echo date("D j M,Y h:i A",strtotime($replacements_conservation_obj->timestamp));?></small>
                              </div>
                           </span>
                           <div class="chat-body clearfix">
                              <p>
                                 <span style="color:#777777;" class="mes more"><?php echo $replacements_conservation_obj->description?></span>                                
                              </p>
							  <?php
								if($replacements_conservation_obj->image!=""){
							  ?>
							  <a download href="<?php echo base_url('Account/returns_file_download_replacement/'.$replacements_conservation_obj->id)?>"><button class="btn-xs btn-success btn common preventDflt">Download</button> </a>
							   <?php
								}
							  ?>
                           </div>
                        </li>
								<?php
							}
						?>
                     </ul>
                  </div>
                  <div class="panel-footer">
				  <?php
							$get_replacement_data=$controller->get_order_details_admin_acceptance_refund_data_for_replacement($order_item['order_item_id']);
							//print_r($get_replacement_data);
						?>
						
                     <form id="send_form_admin_contact_admin_form_stock_<?php echo $order_item['order_item_id'];?>" method="post" name="send_form_admin_contact_admin_form_stock_<?php echo $order_item['order_item_id'];?>" enctype="multipart/form-data">
						<input type="hidden" name="return_order_id" value="<?php echo $return_order_id; ?>">
						<input type="hidden" name="user_type" value="customer">
						<input type="hidden" name="status" value="pending">
						<input type="hidden" name="customer_id" value="<?php echo $order_item['customer_id']; ?>">
						<input type="hidden" name="order_item_id" value="<?php echo $order_item['order_item_id']; ?>">
						<input type="hidden" name="order_id" value="<?php echo $order_id; ?>">
						<?php
							
								$admin_action="stock";
								$customer_action="stock";			
						?>
						<input type="hidden" name="admin_action" value="<?php echo $admin_action; ?>">
						<input type="hidden" name="customer_action" value="<?php echo $customer_action; ?>">
                        <div class="input-group">
		
                           <textarea style="width:95%;" placeholder="Reply Message" name="description" id="description_rep_<?php echo $order_item['order_item_id'];?>" class="form-control input-sm" cols="50" rows="4"></textarea>
						   <input type="file" name="image_attachment">
                           <span class="input-group-btn">
                           <button class="btn btn-primary btn-lg preventDflt"  type="submit">Send</button>
                           </span>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!---------------Contact Admin r replacement Modal ends------------------------------>

	<script>

/*	$(document).ready(function(){
		
	$("#send_form_admin_contact_admin_form_stock_<?php echo $order_item['order_item_id'];?>").on('submit',(function(e) {
		e.preventDefault();
		var description=$("#description_rep_<?php echo $order_item['order_item_id'];?>").val();
		if(description!=""){
			$.ajax({
				url:"<?php echo base_url();?>Account/contact_admin_replacement",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					if(data){
						
						alert("Successfully sent");
						location.reload();
						$("#modal_close_rep_<?php echo $order_item['order_item_id'];?>").trigger("click");
					}else{
						alert("not sent");
					}
				}	        
		   });
		
		}else{
			alert("Please enter description!");
		}
	}));

});


$(document).ready(function(){
	$("#customer_comment_modal_stock_<?php echo $order_item['order_item_id'];?>").on('hidden.bs.modal', function () {
		$.ajax({
			url:"<?php echo base_url()?>Account/update_msg_status_replacement",
			type:"POST",
			data:"return_order_id="+$("#return_order_id_for_msg_stock_<?php echo $order_item['order_item_id'];?>").val(),
			success:function(data){
				//location.reload();
				$("#send_form_admin_contact_admin_form_stock_<?php echo $order_item['order_item_id'];?>")[0].reset();
				$(".stock_div .badge").html("");
			}
		});
    });
})

*/
</script> 
				
				<!--------------------return stock cancel--------------------------------->
			
	
	<?php 
			}
			
	} 
	?>
							
	<!-------------- customer requested details in replacement ends --------------------->
	<script>
	
	function customerDecisionForMoveToRefundFromStockFun(order_item_id,st){
		var status_of_refund_for_cashback_on_sku=$("#status_of_refund_for_cashback_on_sku_stock_"+order_item_id).val();
			
		$.ajax({
			url:"<?php echo base_url()?>Account/customer_decision_for_move_to_refund_from_stock",
			type:"post",
			data:"order_item_id="+order_item_id+"&customer_decision="+st+"&status_of_refund_for_cashback_on_sku="+status_of_refund_for_cashback_on_sku,
			success:function(data){

				
				if(data){
					if(st=="yes"){
						
						/*bootbox.alert({
						  size: "small",
						  message: 'Updated',
						  callback: function () { location.reload(); }
						});*/

                        alert('Updated');
                        location.reload();
					}
					if(st=="no"){
						/*bootbox.alert({
						  size: "small",
						  message: 'Cancelled',
						  callback: function () { location.reload(); }
						});*/
                        alert('Cancelled');
                        location.reload();

                    }

				}
				else{
					/*bootbox.alert({
					  size: "small",
					  message: 'Error',
					  callback: function () { location.reload(); }
					});*/

                    alert('Error');
                    location.reload();
				}
			}
		})
	}
														
	function proceedForReplacementFromStock(order_item_id){
		$.ajax({
			url:"<?php echo base_url()?>Account/proceed_for_replacement_from_stock",
			type:"post",
			data:"order_item_id="+order_item_id,
			success:function(data){
				if(data){
					
					/*bootbox.alert({
					  size: "small",
					  message: 'Replacement accepted',
					  callback: function () { location.reload(); }
					});*/
                    alert('Replacement accepted');
                    location.reload();
				}
				else{
					/*bootbox.alert({
					  size: "small",
					  message: 'Error',
					  callback: function () { location.reload(); }
					});*/
                    alert('Error');
                    location.reload();
				}
			}
		})
	}
	function proceedForCancellationFromStock(order_item_id){
		$.ajax({
			url:"<?php echo base_url()?>Account/proceed_for_cancellation_from_stock",
			type:"post",
			data:$('#return_stock_notification_form_'+order_item_id).serialize(),
			success:function(data){
				
				if(data){
					/*bootbox.alert({
					  size: "small",
					  message: 'Cancelled',
					  callback: function () { location.reload(); }
					});*/

                    alert('Cancelled');
                    location.reload();
				}else{
					/*bootbox.alert({
					  size: "small",
					  message: 'Error',
					  callback: function () { location.reload(); }
					});*/
                    alert('Error');
                    location.reload();
				}
			}
		})
	}
	function call_conversation_return_stock(order_item_id){
		
		$("#send_form_admin_contact_admin_form_stock_"+order_item_id).attr('action','<?php echo base_url();?>Account/offline_contact_admin_return_stock');
		$("form[name=send_form_admin_contact_admin_form_stock_"+order_item_id+"]").submit();	
	}
	</script>