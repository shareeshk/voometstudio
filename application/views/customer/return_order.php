<?php
    $dilivered_time_arr=$controller->get_delivered_time_of_order_item($order_id,$order_item_id);
    $order_refund="0";
    $msg_ref="";
    $order_replacement="0";
    $msg_rep="";
 if(!empty($dilivered_time_arr)){
 	foreach($dilivered_time_arr as $time){
				
			$hour=date("H:i:s", strtotime($time["order_delivered_time"]));
			$date=date("Y:m:d", strtotime($time["order_delivered_date"]));
			$dilivered_time=$date." ".$hour;
	}
            if(!empty($refund_policy)){

                foreach($refund_policy as $data){
                    if($data['new_days_for_refund']!=""){
                        $dateTimeValid = strtotime('+'.$data['new_days_for_refund'].' day', strtotime($dilivered_time) );
                        if(time() > $dateTimeValid){
                            $order_refund=1;
                            $msg_ref='This product has revised Refund period of '.$data["new_days_for_refund"].' day(s)from day of delivery.';
                        }else{
                            $order_refund=2;
                            $msg_ref='This product has revised Refund period of '.$data["new_days_for_refund"].' day(s) from day of delivery.And refunds are done through '.$data["refund_method"].' only.';
                        }
                        

                    }
                    if($data['refund_restrict']=="remove_refund"){
                        $msg_ref="".$data['message']."";
                        $order_refund=1;        
                    }
                    
                }
                
            }
          
            if(!empty($replacement_policy)){
                
                foreach($replacement_policy as $data){
                    if($data['new_days_for_replacement']!=""){
                        $dateTimeValid = strtotime('+'.$data['new_days_for_replacement'].' day', strtotime($dilivered_time) );
                        if(time() > $dateTimeValid){
                            $order_replacement=1;
                        }

                        $msg_rep='This product has revised Replacement period of '.$data["new_days_for_replacement"].' day(s) from the day of delivery.';

                    }
                    if($data['replacement_restrict']=="remove_replacement"){
                        $msg_rep=''.$data["message"].'';
                        $order_replacement=1;
                    }
                    
                }
                
            }
 }             
                    
?> 
<div class="columns-container">
    <div class="container" id="columns">
	<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account/my_order">My Orders</a></li>
		  <li class="breadcrumb-item active">Return an Order</li>
		</ol>
        <div class="row">
            <div class="center_column" id="center_column">
			<?php require_once 'return_order_part1.php'; ?>

			<div class="col-md-3">
<?php 

/* added new */

//print_r($order_item_data_all);
$ord_addon_products_status=0;
$ord_addon_products_count=0;
if(!empty($order_item_data_all)){
	$ord_addon_products_status=$order_item_data_all->ord_addon_products_status;
	$shipp_obj=$controller->get_shipping_address_of_order($order_id);	

	if($ord_addon_products_status=='1'){

		$ord_addon_products=$order_item_data_all->ord_addon_products;
		$arr=json_decode($ord_addon_products);
		$count=count($arr);
		$ord_addon_products_count=$count;

		?>

		<h5 class=""><i><b>Combo products (<?php echo $count; ?>)</b></i></h5>

		<div class="row">
									<div class="col-md-12" style="max-height: 300px;overflow: auto;">
										<?php 

										if($order_item_data_all->ord_addon_inventories!=''){

											$i=1;
											foreach($arr as $val){
												
												
													$name=$val->inv_name;
													$image=$val->inv_image;
													$sku_id=$val->inv_sku_id;
													$price=$val->inv_price;

													?>

											
												<div class="row margin-top" >
													<div class="col-md-4 products-block">
														<div class="products-block-left">
															
																<img src="<?php echo $image; ?>" alt="ADDITIONAL PRODUCTS">
															
														</div>		
													</div>

													<div class="col-md-8 products-block">
														<div class="f-row product-name ordrk-color">
															<a><?php echo $name; ?></a>													
																
															<div class="f-row small-text">SKU :<b> <?php echo $sku_id; ?></b></div>
															<div class="f-row small-text">Price :<b> <?php echo curr_sym.round($price); ?></b></div>
															
														</div>
													</div>

												</div>

												
													<?php 

													$i++;
												}
											}

										?>
									</div>
								</div>
		<?php



	}else{

	
/* added new */


			$inv_obj=$controller->get_details_of_products($inventory_id);
					
			if(!empty($inv_obj)){
				//print_r($inv_detail_obj);	
                            $pro_name=($inv_obj->sku_name!='') ? $inv_obj->sku_name : $inv_obj->product_name;
			?>
			<input type="hidden" id="return_product_id" return_product_id="<?php echo $inv_obj->product_id;?>">
			<input type="hidden" id="return_product_name" value="<?php  echo $pro_name;?>">
			
				<h2 class="text-uppercase margin-bottom">Item Details</h2>
				<div class="f-row">
					<img src="<?php echo base_url().$inv_obj->thumbnail;?>" class="img-responsive">
				</div>
				<div class="f-row">
					<?php  echo $pro_name;?>
					
				</div>
				<div class="f-row small-text">
										
						<?php
					$inv_obj->attribute_1_value= substr($inv_obj->attribute_1_value, 0, strpos($inv_obj->attribute_1_value, ":"));
					
					if(!empty($inv_obj->attribute_1))
						echo '<div class="f-row">'.$inv_obj->attribute_1." : ".$inv_obj->attribute_1_value.'</div>';
					if(!empty($inv_obj->attribute_2))
						echo '<div class="f-row">'.$inv_obj->attribute_2." : ".$inv_obj->attribute_2_value.'</div>';
					if(!empty($inv_obj->attribute_3))
						echo '<div class="f-row">'.$inv_obj->attribute_3." : ".$inv_obj->attribute_3_value.'</div>';
					if(!empty($inv_obj->attribute_4))
						echo '<div class="f-row">'.$inv_obj->attribute_4." : ".$inv_obj->attribute_4_value.'</div>';
					?>
				
				</div>
				<div class="f-row small-text">
				SKU : <?php echo  $inv_obj->sku_id; ?>
				</div>
				<div class="f-row small-text">
				Quantity : <?php echo $quantity; ?>
				</div>
										
				
			
			<?php 
				
			}
			
		}
	}

	 if(!empty($shipp_obj)){ ?>
				
		<div class="f-row text-uppercase normal-text margin-top margin-bottom">
			<strong>pick up address</strong>
		</div>
		<div class="f-row small-font">
			<ul class="list-unstyled">
			  <li><strong class="text-uppercase small-font"><?php echo $shipp_obj->customer_name; ?></strong></li>
			  <li><?php echo $shipp_obj->address1; ?>,<?php echo $shipp_obj->address2; ?></li>
			  <li><?php echo $shipp_obj->city; ?>,<?php echo $shipp_obj->state; ?></li>
			  <li><?php echo $shipp_obj->pincode; ?>,</li>
			  <li><?php echo $shipp_obj->country; ?></li>
			  <li><?php echo "Phone: ".$shipp_obj->mobile; ?></li>
			  
			</ul>
		</div>
		
		<?php } ?>
	</div>

			
			
      		<div class="col-md-9">
			<h3 class="text-uppercase margin-bottom col-md-offset-1">Return Order - Order ID : <?php echo $order_id; ?></h3>
            			<form name="myForm" id="myForm" method="post" novalidate class="form-horizontal">
							<?php 
							if($order_item["promotion_available"]==1){
								if($order_item["total_price_of_product_without_promotion"]==""){
									//$order_item["total_price_of_product_without_promotion"]=$order_item["total_price_of_product_with_promotion"];
								}
								$purchased_price_per_unit=($order_item["total_price_of_product_without_promotion"]+$order_item["total_price_of_product_with_promotion"])/$order_item["quantity"];
							/*	if($order_item["default_discount"]>0){
									if($order_item["individual_price_of_product_without_promotion"]>0){
										$purchased_price_per_unit=$order_item["individual_price_of_product_without_promotion"];
									}else{
										$purchased_dis_per_unit=round($order_item["product_price"]*($order_item["default_discount"]/100));
										$purchased_price_per_unit=($order_item["product_price"]-$purchased_dis_per_unit);
									}
								}else{
									$purchased_price_per_unit=$order_item["product_price"];
								}*/
								
							}else{
								$purchased_price_per_unit=$order_item["product_price"];
							}
							?>
							
						<input type="hidden" value="<?php echo (isset($order_item_invoice_discount_value_each))? $order_item_invoice_discount_value_each : 0; ?>" name="order_item_invoice_discount_value_each" id="order_item_invoice_discount_value_each">
							
						<input type="hidden" value="<?php echo $purchased_price_per_unit;?>" name="purchased_price_per_unit" id="purchased_price_per_unit">
	
						<input type="hidden" value="<?php echo $inventory_id; ?>" name="original_inventory_id" >
            			
            				<div class="form-group">
							  <label class="control-label col-md-3">Reason For Return</label>
							  <div class="col-md-9">
							  <select class="form-control" id="return_reason" name="cancel_reason" required onchange="choose_sub_issueFun(this.value)">
							  		<option value="">Please Choose</option>
							  		<?php foreach($primary_reason_return as $reason){?>
							  			<option value="<?php echo $reason['reason_id'] ?>"><?php echo ucfirst($reason['reason_name']) ?></option>
							  		<?php } ?>

							  </select>
							  </div>
							</div>
            			
            			
            				<div class="form-group" id="sub_issue_div">
							  <label class="control-label col-md-3">Issue With Item</label>
							  <div class="col-md-9">
							  <select class="form-control" name="sub_cancel_reason" id="issue_reason" disabled>
							  		<option value="">Please Choose</option>
							  		<?php foreach($issue_reason_return as $reason){?>
							  			<option value="<?php echo $reason['sub_issue_id'] ?>"><?php echo ucfirst($reason['sub_issue_name']) ?></option>
							  		<?php } ?>

							  </select>
							  </div>
							</div>
            			
            			
	            		
	            			<div class="form-group">
							  <label class="control-label col-md-3">Comment</label>
							  <div class="col-md-9">
							  <textarea class="form-control" rows="3" name="cancel_reason_comment" id="comment" onkeyup="remove_error();"></textarea>
							  </div>
							</div>
	
							<?php
							$status_of_refund_for_cashback_on_sku="";
								if($order_item['promotion_cashback']>0){
									echo '<div class="form-group"><div class="col-md-9 col-md-offset-3"><label class="alert alert-warning">';
								
									$order_item_offers_obj=$controller->get_details_of_order_item($order_item['order_item_id']);
									$status_of_refund_for_cashback_on_sku=$order_item_offers_obj->status_of_refund;
									if($order_item_offers_obj->status_of_refund=="pending"){
										echo '<i>There is pending Cash back offer.If You request for refund the <b>Cash back </b>offer will be rejected</i>';
									}
									if($order_item_offers_obj->status_of_refund=="refunded"){
										echo '<i>There is Cash back offer.If You request for refund the <b>Cash back </b> amount will be deducted from each item price</i>';
									}
									echo '</label></div></div>';
								}
								
							?>
							<input type="hidden" name="status_of_refund_for_cashback_on_sku" id="status_of_refund_for_cashback_on_sku" value="<?php echo $status_of_refund_for_cashback_on_sku; ?>">
							

							
							<?php 
							$rep_disabled="";
							$ref_disabled="";
							
							//echo $order_replacement.'kk';
							//echo $order_refund;
							
							if(($order_replacement!=0 && $order_replacement!='0') || ($order_refund==1 || ($invoice_surprise_gift_promo_quote!="" && $customer_response_for_surprise_gifts!="reject") || ($promotion_invoice_cash_back>0 && $customer_response_for_invoice_cashback!="reject") || $order_refund==2)){
							
								?>
								<div class="form-group">
								<div class="col-md-9 col-md-offset-3">
								<div class="alert alert-info">
								
									
								<?php 
								if($order_replacement!=0 && $order_replacement!='0'){
									echo '<ul class="list-unstyled"><h5><strong>Replacement</strong></h5>';
									 if($order_replacement==1){
										 $rep_disabled='disabled';
										 echo '<li>'.$msg_rep.'</li>';	 
									 }else{
										  if($order_replacement==2){
											echo '<li>'.$msg_rep.'</li>';
										  }
									 }
									echo '</ul>';
								}
							
								
									if($order_refund==1){
										echo '<ul class="list-unstyled"><h5><strong>Refund</strong></h5>';
										$ref_disabled='disabled';
										echo '<li>'.$msg_ref.'</li>';	
										echo '</ul>';
									}else{ 
										if($invoice_surprise_gift_promo_quote!="" || $promotion_invoice_cash_back>0 || $order_refund==2){
											echo '<ul class="list-unstyled"><h5><strong>Refund</strong></h5>';
											if($invoice_surprise_gift_promo_quote!=""){
												
												if($customer_response_for_surprise_gifts=="reject"){
													// echo $customer_response_for_surprise_gifts;
													if($order_refund==2){
														 echo '<li>'.$msg_ref.'</li>';
													}							 
												}
												if($customer_response_for_surprise_gifts=="accept"){
													$ref_disabled='disabled';									
													echo '<li>You accepted to receive surprise gift. So there is no refund option.</li>';
							
												}
												if($customer_response_for_surprise_gifts==""){
													$ref_disabled='disabled';	
													echo '<li>There is a pending response to receive surprise gift. So there is no refund option</li>';									
												}
											
											}elseif($promotion_invoice_cash_back>0){
												
													if($customer_response_for_invoice_cashback=="reject"){				
														if($order_refund==2){
															 echo '<li>'.$msg_ref.'</li>';
														}
													}
													if($customer_response_for_invoice_cashback=="accept"){
														$ref_disabled='disabled';
														echo '<li>You accepted for cash back amount. So there is no refund option</li>';
													}
													if($customer_response_for_invoice_cashback==""){
														$ref_disabled='disabled';
														 
														echo '<li>There is a pending response to credit cash back amount . So there is no refund option</li>';
														  
													}
												
											}else{
												//normal flow
												
												if($order_refund==2){
													 echo '<li>'.$msg_ref.'</li>';
												}	
											}
											echo '</ul>';
										}
									}//common else
									
	
									?>
								
								</div>
								</div>
								</div>
								
								<?php 
								
							}

							?>
							
							
							<div class="form-group">							
								<label class="control-label col-md-3">Return Action</label>
								
								<div class="col-md-9">
									<select name="return_type" id="return_type" class="form-control">
									<option value="" selected>Please Choose Return Type</option>


									<?php if($ord_addon_products_status!='1'){ ?>

									<option value="Replacement" <?php echo $rep_disabled; ?> >Replacement</option>

									<?php } ?>
									
									<option value="Refund" <?php echo $ref_disabled; ?> id="request_for_refund">Refund</option>
									
									</select>
								
								</div>
							</div>
						<script>
						$('#return_type').on('change', function() {
						  var value = $(this).val();
							  if(value=="Replacement"){
								  setReturnMethod();
								  $('#refund_selection_container').hide();
							  }
							  if(value=="Refund"){
								  setReturnMethod();
								  $("#replacement_selection_container").hide();
							  }
							  if(value==''){
								  $("#replacement_selection_container").hide();
								  $('#refund_selection_container').hide();
							  }
						});
						</script>
						<div class="" id="replacement_selection_container" style="display:none">
	
							<?php if($quantity>1){ ?>
								<div class="form-group">
								<label  class="control-label col-md-3">Select Quantity</label>
								<div class="col-md-9">				  
								<select class="form-control" name="desired_quantity_replacement" id="desired_quantity_replacement" onchange="checkRepQuantityNumber()" required>
								<option value="">Quantity</option>

								<?php
								for($m=1;$m<=$quantity;$m++){
								echo '<option value="'.$m.'">'.$m.'</option>';
								}
								?>

								</select> 
								<small id="replacement_quantity_error" class="help-block text_red"></small>
								</div>							  
								</div>
								
							<?php }
							else{ ?>
								<input type="hidden" required  name="desired_quantity_replacement" id="desired_quantity_replacement" value="<?php echo $quantity; ?>">
							<?php }?>
							
							<div class="form-group">
								<div class="col-md-9 col-md-offset-3">
								<button id="availabel_option_button_controller" type="button" class="button btn-block preventDflt" data-toggle="modal" href="#new_piece_options" data-backdrop="static" style="background-color:#607d8b;border:none;"> Choose an item for replacement</button>
								</div>
							</div>
							
							<?php if($quantity>1){ ?>
								<script type="text/javascript">
									document.getElementById('availabel_option_button_controller').disabled=true;
									$('#availabel_option_button_controller').hide();
									
									function checkRepQuantityNumber(){
										$('#desired_quantity_replacement').css({"border": "1px solid #ccc"});
										var desired_quantity_replacement=document.getElementById('desired_quantity_replacement').value
										
										if(desired_quantity_replacement > <?php echo $quantity ?>){
											document.getElementById('replacement_quantity_error').innerHTML="Desired quantity must be less than or equals <?php echo $quantity ?>"
											document.getElementById('availabel_option_button_controller').disabled=true;
										}
										if(desired_quantity_replacement==0){
											document.getElementById('replacement_quantity_error').innerHTML="Desired quantity must be more than 0 and equals <?php echo $quantity ?>"
											document.getElementById('availabel_option_button_controller').disabled=true;
										}
										if(desired_quantity_replacement >0 && desired_quantity_replacement < <?php echo $quantity+1 ?> ){
											document.getElementById('availabel_option_button_controller').disabled=false;
											$('#availabel_option_button_controller').show();
											document.getElementById('replacement_quantity_error').innerHTML=""
										}
										if(isNaN(desired_quantity_replacement)==true){
											document.getElementById('desired_quantity_replacement').innerHTML="";
											document.getElementById('replacement_quantity_error').innerHTML="Desired quantity must be more than 0 and equals <?php echo $quantity ?>"
											document.getElementById('availabel_option_button_controller').disabled=true;
											
										}
										
										document.getElementById('summary_for_replacement_div').innerHTML="";
										document.getElementById('ref_opt_div').innerHTML="";
										
										document.getElementById('rep_options').innerHTML="";
										
									}
								</script>
							<?php }?>
							
							<div class="row margin-bottom margin-top">
								<div class="col-md-12" id="rep_options">
								</div>
								<div class="col-md-3" style="display:none;">
									<div id="checkedValueForReturnInventoryId"></div>
								</div>
								<div class="col-md-9 col-md-offset-3" id="summary_for_replacement_div">
									
								</div>
								<div class="col-md-12" id="ref_opt_div">
									
								</div>
								
							</div>
						
							<!----------------------------------------------------->
					<div style="display:none;" id="refund_method_rep_div">	
	            		
	            		<div class="form-group">
                            <label class="control-label col-md-3">Choose Refund Method</label>
                            <div class="col-md-9">

                                <?php if($order_payment_type=='Razorpay'){ ?>
                                <label class="checkbox-inline">
                                    <input type="radio" checked name="refund_method" required onchange="setRefundMethod_Repl()" value="Razorpay"> Razorpay
                                </label>

                                <?php }else{ ?>

                                <label class="checkbox-inline">
                                    <input type="radio" id="wallet_radio_rep"  name="refund_method_rep" required onchange="setRefundMethod_Repl()" value="Wallet"> Wallet
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" name="refund_method_rep" required onchange="setRefundMethod_Repl()" value="Bank Transfer"> Bank Transfer
                                </label>

                                <?php } ?>
							</div>
						</div>
						<div class="form-group">
						<div class="col-md-9 col-md-offset-3 small-text">
							<i id="replacement_text">(This refund method will be applied for both replacement and refund request)</i>
						</div>
						</div>
						
					</div>
						
<script type="text/javascript">				
function setRefundMethod_Repl(){
	document.getElementById('bank_account_data_rep').style.display = 'none';
	document.getElementById('bank_account_data').style.display = 'none';
	var checkedValue = null; 
	var inputElements = document.getElementsByName('refund_method_rep');
	
		for(var i=0; inputElements[i]; ++i){
		      if(inputElements[i].checked){
		           checkedValue = inputElements[i].value;
		           break;
		      }
		}

	
		if(checkedValue=="Bank Transfer"){

			document.getElementById('bank_account_data_rep').style.display = 'block';
			document.getElementById('bank_ifsc_code_rep').required = true;
			document.getElementById('bank_account_number_rep').required = true;
			document.getElementById('bank_account_confirm_number_rep').required = true;
			document.getElementById('account_holder_name_rep').required = true;
			document.getElementById('bank_return_refund_phone_number_rep').required = true;
		}
		else{
			document.getElementById('bank_account_data_rep').style.display = 'none';
			document.getElementById('bank_ifsc_code_rep').required = false;
			document.getElementById('bank_account_number_rep').required = false;
			document.getElementById('bank_account_confirm_number_rep').required = false;
			document.getElementById('account_holder_name_rep').required = false;
			document.getElementById('bank_return_refund_phone_number_rep').required = false; 
		}
}	
			
</script>


							<!--------------------------------------------->
<div id="bank_account_data_rep" style="display: none;">

							<?php
								if(count($bank_detail)!=0){
									?>
								
								
								<div class="form-group">
									<div class="col-md-9 col-md-offset-3">
										<button type="button" data-toggle="modal" href="#all_bank_accounts_rep" data-backdrop="static" class="button preventDflt"> Use Saved Bank Accounts</button> <button type="button" onclick="add_new_bank_data_rep()" id="add_new_bank_details_button_rep" class="button preventDflt" style="display:none"> Reset</button>
									</div>
								</div>

								<?php
								}
									?>

								
<div class="modal" id="all_bank_accounts_rep">
     <div class="modal-dialog modal-lg">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title"> Your Total Bank Account With Us: 
				<?php 
					//print_r($bank_detail);
					if(!empty($bank_detail[0])){
						echo count($bank_detail[0]);
					}	
					else{
						echo "0";
					}
					?>
			</h4>
           </div>
           <div  class="modal-body">
						<div class="row">
						

<?php $i=0; foreach($bank_detail[0] as $bank_data){ ?>
						<div class="col-md-4">
						<div class="well">
						<dl>
						
										<dt><strong>Bank Name</strong></dt> 
										<dd id="bank_name_rep_<?php echo $i ?>"><?php echo $bank_data['bank_name']?></dd>
										<dt><strong>Branch Name</strong></dt> 
										<dd id="branch_name_rep_<?php echo $i ?>"><?php echo $bank_data['branch_name']?></dd>
										<dt><strong>City</strong></dt> 
										<dd id="city_rep_<?php echo $i ?>"><?php echo $bank_data['city']?></dd>
										<dt><strong>IFSC Code</strong></dt> 
										<dd id="ifsc_rep_<?php echo $i ?>"><?php echo $bank_data['ifsc_code']?></dd>
										<dt><strong>Account Number</strong></dt> 
										<dd id="account_number_rep_<?php echo $i ?>"><?php echo $bank_data['account_number']?></dd>
										<dt><strong>Account Holder Name</strong></dt> 
										<dd id="account_holder_name_rep_<?php echo $i ?>"><?php echo $bank_data['account_holder_name']?></dd>
										<dt><strong>Mobile Number</strong></dt> 
										<dd id="account_holder_numer_rep_<?php echo $i ?>"><?php echo $bank_data['mobile_number']?></dd>
									
									</dl>
							
							
								<button type="button" class="btn btn-success btn-block preventDflt" onclick="use_this_account_data_rep(<?php echo $i ?>)">USE THIS</button>

						
					</div>
					</div>
	<?php $i++;} ?>
	

						
					</div>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close"class="btn">Close</a>
        </div>
     </div>
    </div>
  </div>
  
<script>

	function validate_ifsc_rep(){
		var value = document.getElementById('bank_ifsc_code_rep').value;
		if (value.length != 6) {
			document.getElementById("errorsrep").style.display = "block";
			
		}else if (value.length == 6){
	 
		document.getElementById("errorsrep").style.display = "none";
		}
	}
	
	function ValidateNumberRep(obj) {
		var value = document.getElementById('bank_return_refund_phone_number_rep').value;
		if (value.length != 10) {
			document.getElementById("errorsrep3").style.display = "block";
			
		}else if (value.length == 10){
	 
		document.getElementById("errorsrep3").style.display = "none";
		}
		val=obj.value;
		
	}

	function validate_account_rep(){
		 
		var value = document.getElementById('bank_account_number_rep').value;
		if (value.length != 15) {
			document.getElementById("errorsrep1").style.display = "block";
			
		}else if (value.length == 15){
	 
		document.getElementById("errorsrep1").style.display = "none";
		}
	}
	
	function validate_confirm_rep(){
		var value = document.getElementById('bank_account_confirm_number_rep').value;
		if ((document.myForm.bank_account_number_rep.value)!=(document.myForm.bank_account_confirm_number_rep.value)) {
			document.getElementById("errorsrep2").style.display = "block";
			
		}else if ((document.myForm.bank_account_number_rep.value)==(document.myForm.bank_account_confirm_number_rep.value)){
	 
		document.getElementById("errorsrep2").style.display = "none";
		}
	}
	
	
</script> 
<script>
	function use_this_account_data_rep(obj){
		
		document.getElementById('errorsrep').style.display = 'none';
		document.getElementById('errorsrep1').style.display = 'none';
		document.getElementById('errorsrep2').style.display = 'none';
		document.getElementById('errorsrep3').style.display = 'none';
		document.getElementById('add_new_bank_details_button_rep').style.display = '';
		var bank_name=document.getElementById('bank_name_rep_'+obj).innerHTML
		var branch_name=document.getElementById('branch_name_rep_'+obj).innerHTML
		var city=document.getElementById('city_rep_'+obj).innerHTML
		var ifsc=document.getElementById('ifsc_rep_'+obj).innerHTML
		var account_number=document.getElementById('account_number_rep_'+obj).innerHTML
		var account_holder_name=document.getElementById('account_holder_name_rep_'+obj).innerHTML
		var account_holder_numer=document.getElementById('account_holder_numer_rep_'+obj).innerHTML
		
		document.getElementById('bank_name_rep').value=bank_name;
		document.getElementById('bank_branch_name_rep').value=branch_name;
		document.getElementById('bank_city_rep').value=city;
		document.getElementById('bank_ifsc_code_rep').value=ifsc;
		document.getElementById('bank_account_number_rep').value=account_number;
		document.getElementById('bank_account_confirm_number_rep').value=account_number;
		document.getElementById('account_holder_name_rep').value=account_holder_name;
		document.getElementById('bank_return_refund_phone_number_rep').value=account_holder_numer;
		document.getElementById('bank_name_rep').setAttribute('readOnly',true);
		document.getElementById('bank_branch_name_rep').setAttribute('readOnly',true);
		document.getElementById('bank_city_rep').setAttribute('readOnly',true);
		document.getElementById('bank_ifsc_code_rep').setAttribute('readOnly',true);
		document.getElementById('bank_account_number_rep').setAttribute('readOnly',true);
		document.getElementById('bank_account_confirm_number_rep').setAttribute('readOnly',true);
		document.getElementById('account_holder_name_rep').setAttribute('readOnly',true);
		document.getElementById('bank_return_refund_phone_number_rep').setAttribute('readOnly',true);
		document.getElementById('save_this_bank_data_rep').disabled=true;
		$('#all_bank_accounts_rep').modal('hide');
		$('#add_new_bank_details_button_rep').show();
	}
	function add_new_bank_data_rep(){
		document.getElementById('bank_name_rep').value="";
		document.getElementById('bank_branch_name_rep').value="";
		document.getElementById('bank_city_rep').value="";
		document.getElementById('bank_ifsc_code_rep').value="";
		document.getElementById('bank_account_number_rep').value="";
		document.getElementById('bank_account_confirm_number_rep').value="";
		document.getElementById('account_holder_name_rep').value="";
		document.getElementById('bank_return_refund_phone_number_rep').value="";
		document.getElementById('bank_name_rep').readOnly=false;
		document.getElementById('bank_branch_name_rep').readOnly=false;
		document.getElementById('bank_city_rep').readOnly=false;
		document.getElementById('bank_ifsc_code_rep').readOnly=false;
		document.getElementById('bank_account_number_rep').readOnly=false;
		document.getElementById('bank_account_confirm_number_rep').readOnly=false;
		document.getElementById('account_holder_name_rep').readOnly=false;
		document.getElementById('bank_return_refund_phone_number_rep').readOnly=false;
		det_count=$('#saved_bank_details_rep').val().trim();
		if(det_count<3){
			document.getElementById('save_this_bank_data_rep').disabled=false;
		}
		
	}
</script>	  	  
			
							
							
								<div class="form-group">
								   <label class="control-label col-md-3">Bank name</label> 
								 <div class="col-md-9">
								  <input id="bank_name_rep" name="bank_name_rep"  type="text" placeholder="Enter Bank Name" class="form-control refund_return_replace_bank"> 
								  <span class="help-block"></span>   
								  </div>
								</div>
	
								<div class="form-group">
								   <label class="control-label col-md-3">Branch name</label>
								  <div class="col-md-9">
								  <input id="bank_branch_name_rep" name="bank_branch_name_rep" type="text" placeholder="Enter Branch Name" class="form-control refund_return_replace_bank"> 
								  <span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								   <label class="control-label col-md-3">Bank City</label>
								   <div class="col-md-9">
									<input id="bank_city_rep" name="bank_city_rep" type="text" placeholder="Enter City" class="form-control refund_return_replace_bank"> 
									<span class="help-block"></span>   
									</div>
								</div>

								<div class="form-group">
								   <label class="control-label col-md-3">IFSC code</label>
								 <div class="col-md-9">
								  <input id="bank_ifsc_code_rep" onclick="validate_ifsc_rep()" onkeyup="validate_ifsc_rep()" name="bank_ifsc_code_rep" type="text" placeholder="Enter IFSC code" class="form-control  refund_return_replace_bank">
								 <span id="errorsrep" style="display:none;color:red;">IFSC code must be 6 numbers</span>	<span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								<label class="control-label col-md-3">Account number</label>
								 <div class="col-md-9">
								  <input id="bank_account_number_rep" onfocus="validate_account_rep()" onkeyup="validate_account_rep()" name="bank_account_number_rep" type="text" placeholder="Your Account Number" class="form-control refund_return_replace_bank">
								  <span id="errorsrep1" style="display:none;color:red;">Bank account number must be 15 numbers</span>	
								  <span class="help-block"></span>  
								  </div>
								</div>
								
								<div class="form-group">
								<label class="control-label col-md-3">Confirm number</label>
								  <div class="col-md-9">
								  <input id="bank_account_confirm_number_rep" onfocus="validate_confirm_rep()" onkeyup="validate_confirm_rep()" name="bank_account_confirm_number_rep" type="text" placeholder="Confirm Account Number" class="form-control refund_return_replace_bank">
								  <span id="errorsrep2" style="display:none;color:red;">Bank account number must be same</span>
								  <span class="help-block"></span>  
								  </div>
								</div>
													
								<div class="form-group">
								  <label class="control-label col-md-3">Account Holder</label>
								  <div class="col-md-9">
								  <input id="account_holder_name_rep" onkeypress="return isAlfa(event)" name="account_holder_name_rep" type="text" placeholder="Enter Bank Account holder Name" class="form-control refund_return_replace_bank">
								  <span class="help-block"></span>  
								  </div>
								</div>

								<div class="form-group">
								<label class="control-label col-md-3">Phone number</label> 
								  <div class="col-md-9">
								  <input id="bank_return_refund_phone_number_rep" onkeyup="ValidateNumberRep(this)"  onkeypress="return isNumber(event)" name="bank_return_refund_phone_number_rep" type="text" placeholder="Enter Your Registered Mobile Number" class="form-control refund_return_replace_bank" maxlength="10">
								  <span id="errorsrep3" style="display:none;color:red;">Enter 10digit mobile number</span>
								  <span class="help-block"></span>  
								  </div>
								</div>

								<div class="form-group">
									<?php
										$disabled='';
										$saved_bank_details=0;
										if(!empty($bank_detail[0])){

											$saved_bank_details=count($bank_detail[0]);									  
											if($saved_bank_details>=3){
												$disabled='disabled';
											}
										}
									  ?>
								   <div class="col-md-9 col-md-offset-3">
								  <div class="checkbox">
								    <label for="checkboxes-0">
								      <input type="checkbox" name="save_this_bank_data_rep" class="refund_return_replace_bank" id="save_this_bank_data_rep" <?php echo $disabled; ?>  value="1">
									  <input type="hidden" name="saved_bank_details" id="saved_bank_details_rep"  value="<?php echo $saved_bank_details; ?>">
								      Save for later use
								    </label>
									</div>
								  </div>
								</div>	            			
					
					
					</div>
							
							
						</div>
						<div class="" id="refund_selection_container" style="display:none">
							<?php if($quantity>1){ ?>
							 	<div class="form-group">
								  <label class="control-label col-md-3">Select Quantity</label>
	<div class="col-md-9">
<?php

	if(($order_item['promotion_available']==1 && $order_item['promotion_item_num']!="") || ($order_item['promotion_available']==1 && $order_item['promotion_surprise_gift_type']!="")){
		
		$promotion_minimum_quantity=$order_item['promotion_minimum_quantity'];
		$modulo=intval($order_item['quantity']%$promotion_minimum_quantity);//remainder
		$quotient=intval($order_item['quantity']/$promotion_minimum_quantity);
		$refund_option_arr=array();
		
		//echo $promotion_minimum_quantity;
		for($u=1;$u<=$quantity;$u++){
			
			$r=intval($u*$promotion_minimum_quantity);
			//if($r>=$moq){
				if($r<=$quantity){
					
					$refund_option_arr[]=$r;
					
					if($modulo>0){
						//if ($u % $modulo != 0)
						$refund_option_arr[]=intval($r+$modulo);
					}
				}
			//}
		}
		//print_r($refund_option_arr);
		
		?>
		
		<select class="form-control" onchange="checkQuantityNumber()"  name="desired_quantity_refund" id="desired_quantity_refund" required>

		<option value="">Quantity</option>
		<?php
			foreach($refund_option_arr as $qty){
				
				echo '<option value="'.$qty.'">'.$qty.'</option>';
			}
			
		?>
		</select>
		
		<?php
	}else{
		
		?>
		<select class="form-control" name="desired_quantity_refund" id="desired_quantity_refund" onchange="checkQuantityNumber()" required>
		<option value="">Quantity</option>
		
		<?php
		
			for($m=1;$m<=$quantity;$m++){
				echo '<option value="'.$m.'">'.$m.'</option>';
			}
		?>
		
		</select>
		
		<?php /*?> <input type="text" onkeyup="checkQuantityNumber()" required class="form-control"  name="desired_quantity_refund" id="desired_quantity_refund" placeholder="less than or equals <?php echo $quantity; ?> item" min="1" max="<?php echo $quantity;?>">
		
		<?php */?>
		 
		<?php
	}

?>								  
								 
								  
								 <!-- <small style="color: #8a8282;">Minimum Order Quantity is <?php //echo $moq; ?> & Maximum is Order Quantity <?php //echo $quantity;?></small>-->
								  <small id="desired_quantity_refund_error" class="help-block text_red"></small>
	</div>
								</div>

								<script>
									document.getElementById('availabel_option_button_controller').disabled=true;
									function checkQuantityNumber(){
										document.getElementById('refund_flow').style.display = 'none';
										$('#desired_quantity_refund').css({"border": "1px solid #ccc"});
										document.getElementById('refund_order_summary').innerHTML=""
										
										var desired_quantity_refund=document.getElementById('desired_quantity_refund').value
										
										if(desired_quantity_refund > <?php echo $quantity ?>){
											document.getElementById('desired_quantity_refund').value=""
											document.getElementById('desired_quantity_refund_error').innerHTML="Desired quantity must be less than or equals <?php echo $quantity ?>"

										}
										if(desired_quantity_refund==0){
											document.getElementById('desired_quantity_refund').value=""
											document.getElementById('desired_quantity_refund_error').innerHTML="Desired quantity must be more than 0 and equals <?php echo $quantity ?>"
										}
										if(desired_quantity_refund >0 && desired_quantity_refund < <?php echo $quantity+1 ?> ){

											show_balance_amount_totalrefund();
											document.getElementById('desired_quantity_refund_error').innerHTML="";
											document.getElementById('refund_flow').style.display = 'block';
										}
										if(isNaN(desired_quantity_refund)==true){
											document.getElementById('desired_quantity_refund').value=""
											document.getElementById('desired_quantity_refund_error').innerHTML="Desired quantity must be more than 0 and equals <?php echo $quantity ?>"
										}
										
									}
								</script>
								
								
							<?php }else{ ?>
								<input type="hidden" required  name="desired_quantity_refund" id="desired_quantity_refund" value="1">
								
							<?php }?>

							<div class="form-group">
							<div class="col-md-9 col-md-offset-3" id="refund_order_summary">

							</div>
							</div>
							
						</div>

						<input type="hidden" name="ord_addon_products_status" id="ord_addon_products_status" value="<?php echo $ord_addon_products_status; ?>">
						<input type="hidden" name="ord_addon_products_count" id="ord_addon_products_count" value="<?php echo $ord_addon_products_count; ?>">

<div class="modal" id="new_piece_options">
     <div class="modal-dialog modal-lg">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title">Replacement Options For <b id="return_product_name_display"></b></h4>
           </div>
           <div  class="modal-body">
			<div id="available_options_row"></div>
           </div>
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close"class="btn">Close</a>
        </div>
     </div>
    </div>
  </div>						
						
						
<div class="" id="refund_flow" style="display:none"><!--Dispaly based on return (new /refund)-->						
						<div class="form-group">
							<label class="control-label col-md-3">Choose Refund Method</label>
	            			<div class="col-md-9">	

                            <?php if($order_payment_type=='Razorpay'){ ?>
                                <label class="checkbox-inline">
                                    <input type="radio" checked name="refund_method" required onchange="setRefundMethod()" value="Razorpay"> Razorpay
                                </label>
                            <?php }else{
                                ?>
                                <label class="checkbox-inline">
                                    <input type="radio" id="wallet_radio"  name="refund_method" required onchange="setRefundMethod()" value="Wallet"> Wallet
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" name="refund_method" required onchange="setRefundMethod()" value="Bank Transfer"> Bank Transfer
                                </label>
                                <?php
                            } ?>

							<label class="checkbox-inline">
							<span id="error_refund_method" class="red"></span>
							</label>				
							</div>
						</div>
							
					<div class="margin-top" id="bank_account_data" style="display:none;">
							<?php if(count($bank_detail)!=0){ ?>
						<div class="form-group">
								<div class="col-md-9 col-md-offset-3">
								<button type="button"  data-toggle="modal" href="#all_bank_accounts" data-backdrop="static" class="button preventDflt">Use Saved Bank Accounts</button> <button type="button" onclick="add_new_bank_data()" id="add_new_bank_details_button" class="button preventDflt" style="display:none">Reset</button>
								</div>
						</div>
							<?php } ?>
								
<div class="modal" id="all_bank_accounts">
     <div class="modal-dialog modal-lg">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title"> Your Total Bank Account With Us: 
				<?php 
				
					if(!empty($bank_detail[0])){
						echo count($bank_detail[0]);
					}else{
						echo "0";
					}
					?>
			  </h4>
           </div>
           <div  class="modal-body">
			<div class="row">
						<?php 
						$i=0; foreach($bank_detail[0] as $bank_data){ ?>
						<div class="col-md-4">
						<div class="well">
						<dl>
						<dt><strong>Bank Name</strong></dt> 
						<dd id="bank_name_<?php echo $i ?>"><?php echo $bank_data['bank_name']?></dd>
						<dt><strong>Branch Name</strong></dt> 
						<dd id="branch_name_<?php echo $i ?>"><?php echo $bank_data['branch_name']?></dd>
						<dt><strong>City</strong></dt> 
						<dd id="city_<?php echo $i ?>"><?php echo $bank_data['city']?></dd>
						<dt><strong>IFSC Code</strong></dt> 
						<dd id="ifsc_<?php echo $i ?>"><?php echo $bank_data['ifsc_code']?></dd>
						<dt><strong>Account Number</strong></dt> 
						<dd id="account_number_<?php echo $i ?>"><?php echo $bank_data['account_number']?></dd>
						<dt><strong>Account Holder Name</strong></dt> 
						<dd id="account_holder_name_<?php echo $i ?>"><?php echo $bank_data['account_holder_name']?></dd>
						<dt><strong>Mobile Number</strong></dt> 
						<dd id="account_holder_numer_<?php echo $i ?>"><?php echo $bank_data['mobile_number']?></dd>		</dl>
						<button type="button" class="btn btn-success btn-block preventDflt" onclick="use_this_account_data(<?php echo $i ?>)">USE THIS</button>
						</div>
						</div>					
						<?php $i++;} ?>
						</div>
						</div>
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close"class="btn">Close</a>
        </div>
     </div>
    </div>
  </div>	
 

									<div class="form-group">
									  <label class="control-label col-md-3">Bank name</label>
									  <div class="col-md-9">
									  <input id="bank_name" name="bank_name" type="text" placeholder="Enter Bank Name"  class="form-control"> 
									  <span class="help-block"></span>  
									  </div>
									</div>

									<div class="form-group">
									   <label class="control-label col-md-3">Branch name</label>  
									  <div class="col-md-9">
									  <input id="bank_branch_name" name="bank_branch_name" type="text" placeholder="Enter Bank Branch Name"  class="form-control"> 
									  <span class="help-block"></span>   
									  </div>
									</div>
								
									<div class="form-group">
									   <label class="control-label col-md-3">Branch city</label>
									  <div class="col-md-9">
									  <input id="bank_city" name="bank_city" type="text" placeholder="Enter Branch City"  class="form-control"> 
									  <span class="help-block"></span>   
									  </div>
									</div>
								
									<div class="form-group">
										<label class="control-label col-md-3">IFSC Code</label>
										 <div class="col-md-9">
										  <input id="bank_ifsc_code" onclick="validate_ifsc()" onkeyup="validate_ifsc()" name="bank_ifsc_code" type="text" placeholder="Enter IFSC code"  class="form-control"> 
										  <span id="errors">IFSC code must be 6 numbers</span>
										  <span class="help-block"></span> 
										</div>
									</div>

									<div class="form-group">
									  <label class="control-label col-md-3">Account Number</label> 
									  <div class="col-md-9">
									  <input id="bank_account_number"  onfocus="validate_account()" onkeyup="validate_account()" name="bank_account_number" type="text" placeholder="Enter your Bank Account Number"  class="form-control">
									   <span id="errors1">Bank account number must be 15 numbers</span>	
									  <span class="help-block"></span> 								  
									  </div>
									</div>

									<div class="form-group">
									   <label class="control-label col-md-3">Confirm Number</label> 
									  <div class="col-md-9">
									  <input id="bank_account_confirm_number" onfocus="validate_confirm()" onkeyup="validate_confirm()" name="bank_account_confirm_number" type="text" placeholder="Confirm Bank Account Number"  class="form-control">
									   <span id="errors2">Bank account number must be same</span>	
									  <span class="help-block"></span>  
									  </div>
									</div>

								
									<div class="form-group">
									   <label class="control-label col-md-3">Account Holder</label>
									  <div class="col-md-9">
									  <input id="account_holder_name" onkeypress="return isAlfa(event)" name="account_holder_name" type="text" placeholder="Enter Bank Account holder Name"  class="form-control">
									  <span class="help-block"></span>  
									  </div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-3">Phone number</label>
										  <div class="col-md-9">
										  <input id="bank_return_refund_phone_number" onkeyup="ValidateNumber(this)"  onkeypress="return isNumber(event)" name="bank_return_refund_phone_number" type="text"  placeholder="Enter Your Registered Mobile Number" class="form-control" maxlength="16">
										 <span id="errors3" style="color:red;display:none;">Enter 10digit mobile number</span>
										  <span class="help-block"></span>  
										  </div>
									</div>

									<div class="form-group">
										<?php
										$disabled='';
										$saved_bank_details=0;
										if(!empty($bank_detail[0])){

											$saved_bank_details=count($bank_detail[0]);									  
											if($saved_bank_details>=3){
												$disabled='disabled';
											}
										}
									  ?>
									  <div class="col-md-9 col-md-offset-3">
									  <div class="checkbox">
									    <label for="checkboxes-0">
									      <input type="checkbox" name="save_this_bank_data" id="save_this_bank_data"  value="1" <?php echo $disabled; ?>>
										  <input type="hidden" name="saved_bank_details" id="saved_bank_details"  value="<?php echo $saved_bank_details; ?>">
									      Save for later use
									    </label>
										</div>
									  </div>
									</div>	            			
								</div>
							</div>
						
			
						<div class="form-group">
							<div class="col-md-9 col-md-offset-3">
								<div class="checkbox">
									<label for="checkboxes-1">
									  <input type="checkbox" required="" name="accept_terms_and_conditions"  value="1" onclick="remove_error();">
									  I agree to <a href="#">terms and conditions</a> &nbsp;&nbsp;<span id="error_accept_terms_and_conditions" class="red"></span>
									</label>
								</div>
							</div>
						</div>					
<!--for refund-->						
						<input type="hidden" name="shipping_charge_purchased" id="shipping_charge_purchased" value="<?php echo $data['shipping_charge']?>">
						<input type="hidden" name="shipping_charge_requested" id="shipping_charge_requested" value="0">
						<input type="hidden" name="shipping_charge_percent_paid_by_customer" id="shipping_charge_percent_paid_by_customer" value="0">
						
						<div class="form-group">
						<div class="col-md-9 col-md-offset-3 text-right">
							<button type="button" onclick="submitForm()" id="myForm_btn" class="button text-uppercase preventDflt"/>Request Return</button>
						</div>
						</div>
						</form>
		            </div>
				
            </div>
        </div>
    </div>
</div>
<script>
function ValidateNo() {
        var phoneNo = document.getElementById('bank_return_refund_phone_number');

    if (phoneNo.value == "" || phoneNo.value == null) {
	
            //alert("Please enter your Mobile No.");
			phoneNo.value="";
			phoneNo.focus();
            return false;
        }
        else if (phoneNo.value.length < 10 || phoneNo.value.length > 10) {
		
            return false;
        }else{
		
		
		return true;
		}

        }
		
		
		
		
function isAlfa(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
        return false;
    }
    return true;
}


function isNumber(evt) {

    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
	
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function remove_error(){
	$('#comment').css({"border": "1px solid #ccc"});
	$('#error_accept_terms_and_conditions').html('');
	$('#pay_balance_method').css({"border": "1px solid #ccc"});
}
function submitForm(){
	
	err=0;
	var return_type = null; 
	var inputElements = document.getElementsByName('return_type');
	
	return_type=$('#return_type').val();
	/*for(var i=0; inputElements[i]; ++i){
	      if(inputElements[i].checked){
	           return_type = inputElements[i].value;
	           break;
	      }
	}*/

	if(return_type==null || return_type==''){
		$('#return_type').css("border", "1px solid red");	
		 err = 1;
		 
	}else{
		$('#return_type').css({"border": "1px solid #ccc"});
	}

	return_reason=$("#return_reason").val();
	comment=$("#comment").val();
	desired_quantity_refund=$("#desired_quantity_refund").val();
	desired_quantity_replacement=$("#desired_quantity_replacement").val();

	if(return_reason==''){
	   $('#return_reason').css("border", "1px solid red");	   
	   err = 1;
	}else{			
		$('#return_reason').css({"border": "1px solid #ccc"});
	}
	if(comment==''){
	   $('#comment').css("border", "1px solid red");	   
	   err = 1;
	}else{			
		$('#comment').css({"border": "1px solid #ccc"});
	}


	if(return_type=="Refund"){
		if($('input[name=refund_method]').is(':checked')) {	
			 $('#error_refund_method').html('');
		}else{
			$('#error_refund_method').html('Pls choose refund method');
			err=1;
		}
		if(desired_quantity_refund==''){
		   $('#desired_quantity_refund').css("border", "1px solid red");	   
		   err = 1;
		}else{			
			$('#desired_quantity_refund').css({"border": "1px solid #ccc"});
		}
	}else{
		
		if(desired_quantity_replacement==''){
		   $('#desired_quantity_replacement').css("border", "1px solid red");	   
		   err = 1;
		}else{			
			$('#desired_quantity_replacement').css({"border": "1px solid #ccc"});
		}
		
		rep_1=$('#rep_options').html();
		rep_2=$('#summary_for_replacement_div').html();
		
		if(rep_1=="" && rep_2==""){
			/*bootbox.alert({
			  size: "small",
			  message: 'Please select replacement options',
			});*/
            alert('Please select replacement options');
			err = 1;
			
		}
		
		if(rep_1!=''){
	
			var inputElements = document.getElementsByName('replacement_limited_method');
			checkedValue=false;
			checkedValue_v='';
			for(var i=0; inputElements[i]; ++i){
				  if(inputElements[i].checked){
					   checkedValue = true;
					   checkedValue_v = inputElements[i].value;
					   break;
				  }
			}
			
			if(checkedValue){
				//alert("it's checked"); 
			}else{
				/*bootbox.alert({
				  size: "small",
				  message: 'Please choose replacement options',
				});*/
                alert('Please choose replacement options');
				err = 1;	
			}
			
			if(checkedValue_v=='procced_with_replace_all_when_stock_become_available'){
				timelimitdefined=$('#timelimitdefined').val();
				if(timelimitdefined==''){
				   $('#timelimitdefined').css("border", "1px solid red");	   
				   err = 1;
				}else{			
					$('#timelimitdefined').css({"border": "1px solid #ccc"});
				}
			}
		}
		
		
		if ($('#pay_balance_method').length) {
			pay_balance_method=$('#pay_balance_method').val();
			if(pay_balance_method==''){
			   $('#pay_balance_method').css("border", "1px solid red");	   
			   err = 1;
			}else{			
				$('#pay_balance_method').css({"border": "1px solid #ccc"});
			}
		}
	}
	
	if($('input[name=accept_terms_and_conditions]').is(':checked')) {	
		 $('#error_accept_terms_and_conditions').html('');
	}else{
		$('#error_accept_terms_and_conditions').html('Accept terms and conditions');
		err=1;
	}
	
	if(err==1){
		return false;
	}
	
	if(return_type=="Replacement"){
		var checkedValue_rep=null;
		var inputElements_rep = document.getElementsByName('refund_method_rep');

		for(var i=0; inputElements_rep[i]; ++i){
			  if(inputElements_rep[i].checked){
				   checkedValue_rep= inputElements_rep[i].value;
				   break;
			  }
		}
		if(checkedValue_rep=="Bank Transfer"){
			
			if(document.myForm.bank_name_rep.value == ""){
				myForm.bank_name_rep.focus();	
			}else if(document.myForm.bank_branch_name_rep.value == ""){
				myForm.bank_branch_name_rep.focus();
			}else if(document.myForm.bank_city_rep.value == ""){
				myForm.bank_city_rep.focus();
			}else if(document.myForm.bank_ifsc_code_rep.value.trim().length!=6){
				myForm.bank_ifsc_code_rep.focus();
			}else if(document.myForm.bank_account_number_rep.value.trim().length !=15){
				myForm.bank_account_number_rep.focus();
			}else if(document.myForm.bank_account_confirm_number_rep.value.trim().length !=15){
				myForm.bank_account_confirm_number_rep.focus();
			}else if(document.myForm.bank_account_confirm_number_rep.value != document.myForm.bank_account_number_rep.value){
				myForm.bank_account_confirm_number_rep.focus();
			}else if(document.myForm.account_holder_name_rep.value == ""){
				myForm.account_holder_name_rep.focus();
			}else if(document.myForm.bank_return_refund_phone_number_rep.value.trim().length !=10){
				myForm.bank_return_refund_phone_number_rep.focus();
			}else{				
				//document.myForm.submit();	
				//$("#form_id").attr("action", "/script.php")
				return_form_submit("replace");
			}
		}else{			
			//document.myForm.submit(); 
			//$("#form_id").attr("action", "/script.php");
			return_form_submit("replace");
		}
		
	}else{
		var checkedValue_refund=null;
		var inputElements_refund = document.getElementsByName('refund_method');

		for(var i=0; inputElements_refund[i]; ++i){
			if(inputElements_refund[i].checked){
			   checkedValue_refund = inputElements_refund[i].value;
			   break;
			}
		}
		if(checkedValue_refund=="Bank Transfer"){
			if(document.myForm.bank_name.value == ""){
				myForm.bank_name.focus();	
			}else if(document.myForm.bank_branch_name.value == ""){
				myForm.bank_branch_name.focus();
			}else if(document.myForm.bank_city.value == ""){
				myForm.bank_city.focus();
			}else if(document.myForm.bank_ifsc_code.value.trim().length !=6){
				myForm.bank_ifsc_code.focus();
			}else if(document.myForm.bank_account_number.value.trim().length !=15){
				myForm.bank_account_number.focus();
			}else if(document.myForm.bank_account_confirm_number.value.trim().length !=15){
				myForm.bank_account_confirm_number.focus();
				
			}else if(document.myForm.bank_account_confirm_number.value != document.myForm.bank_account_number.value){
				myForm.bank_account_confirm_number.focus();
			}else if(document.myForm.account_holder_name.value == ""){
				myForm.account_holder_name.focus();
			}else if(document.myForm.bank_return_refund_phone_number.value.trim().length !=10){
				myForm.bank_return_refund_phone_number.focus();
			}else{
				//document.myForm.submit();
				return_form_submit("refund");
			}
		}else{
			//document.myForm.submit();
			return_form_submit("refund");
		}
		
	}
///////////////////////	
				
 }

function return_form_submit(type){
	
	//alert("return type");
	
	
	url='<?php echo base_url()?>Account/submit_return_order_item/<?php echo $order_item_id ?>';
	
	var promotion_available=parseInt(document.getElementById('promotion_available').value);
	var order_id=parseInt(document.getElementById('order_id').value);

	if($('#replace_and_refund').is(':checked')){
		var replacement_limited_method=1;
	}else{
		var replacement_limited_method=0;
	}

	if((promotion_available==1) && (replacement_limited_method==0)){
		if(type=="replace"){
			if("procced_with_replace_all_when_stock_become_available"){
				data=$("#myForm").serialize()+"&"+$("#refund_promo_data_form").serialize();
			}else{
				data=$("#myForm").serialize();
			}
			
		}else{
			data=$("#myForm").serialize()+"&"+$("#refund_promo_data_form").serialize();
		}
	}else if((replacement_limited_method==1) && (promotion_available==1)){
		data=$("#myForm").serialize()+"&"+$("#refund_promo_data_form").serialize();	
	}else if(promotion_available==1){
		
		data=$("#myForm").serialize()+"&"+$("#refund_promo_data_form").serialize();
	}else{
		data=$("#myForm").serialize();
	}
	$("#myForm_btn").html('<i class="fa fa-spinner"></i> Processing ...');
	document.getElementById("myForm_btn").disabled=true;



	$.ajax({
		url:url,
		type:'POST',
		data:data,
		asyn:false,
		success:function(res){
			//alert(data);
			//return false;
			$("#myForm_btn").html('Request Return');

			if(res){
				/*bootbox.alert({
				  size: "small",
				  message: 'successfully submitted',
				  callback: function () { window.location='<?php echo base_url()."Account/order_details/"?>'+order_id; }
				  
				});*/
                document.getElementById("myForm_btn").disabled=true;//going to redirect
                alert('successfully submitted');
                window.location='<?php echo base_url()."Account/order_details/"?>'+order_id;
				
			}else{
                alert('Error');
                document.getElementById("myForm_btn").disabled=true;//going to redirect
                location.reload();
            }
		}
		
	})
	
}
 
	function validate_ifsc(){
		var value = document.getElementById('bank_ifsc_code').value;
		if (value.trim().length != 6) {
			document.getElementById("errors").style.display = "block";
			
		}else if (value.trim().length == 6){
	 
		document.getElementById("errors").style.display = "none";
		}
	}
	
	function displayKeyCode(evt){
		var textBox = getObject('bank_return_refund_phone_number');
		var charCode = (evt.which) ? evt.which : event.keyCode
		textBox.value = String.fromCharCode(charCode);
		if (charCode == 8){
			//alert(textBox.value);
		}
		
	}
	function ValidateNumber(obj) {
		var value = document.getElementById('bank_return_refund_phone_number').value;
		if (value.trim().length != 10) {
			document.getElementById("errors3").style.display = "block";
			
		}else if (value.trim().length == 10){
	 
		document.getElementById("errors3").style.display = "none";
		}
		val=obj.value;
		
	}

	function validate_account(){
		 
		var value = document.getElementById('bank_account_number').value;
		if (value.trim().length != 15) {
			document.getElementById("errors1").style.display = "block";
			
		}else if (value.trim().length == 15){
	 
		document.getElementById("errors1").style.display = "none";
		}
	}
	
	function validate_confirm(){
		var value = document.getElementById('bank_account_confirm_number').value;
		if ((document.myForm.bank_account_number.value)!=(document.myForm.bank_account_confirm_number.value)) {
			document.getElementById("errors2").style.display = "block";
			
		}else if ((document.myForm.bank_account_number.value)==(document.myForm.bank_account_confirm_number.value)){
	 
		document.getElementById("errors2").style.display = "none";
		}
	}
	
	
</script>
<script type="text/javascript">
	function setReturnMethod(){
	document.getElementById('checkedValueForReturnInventoryId').innerHTML="";
	document.getElementById('ref_opt_div').innerHTML="";
	document.getElementById('summary_for_replacement_div').innerHTML="";
	document.getElementById('refund_order_summary').innerHTML="";
	document.getElementById('refund_flow').style.display = 'none';
	
	/*var checkedValueForReturn = null; 
	var inputElements = document.getElementsByName('return_type');

	for(var i=0; inputElements[i]; ++i){
	      if(inputElements[i].checked){
	           checkedValueForReturn = inputElements[i].value;
	           break;
	      }
	}*/
	checkedValueForReturn=$('#return_type').val();
	if(checkedValueForReturn=="Refund"){
		
		var input_elems_inside_refund_section=document.getElementById('refund_selection_container').getElementsByTagName('input');
		for(i=0;i<input_elems_inside_refund_section.length;i++){
			input_elems_inside_refund_section[i].required=true;
		}
		
		var input_elems_inside_replacement_section=document.getElementById('replacement_selection_container').getElementsByTagName('input');
		for(i=0;i<input_elems_inside_replacement_section.length;i++){
			input_elems_inside_replacement_section[i].required=false;
			if(document.getElementById('desired_quantity_replacement').value!=1){
			input_elems_inside_replacement_section[i].value="";
			}
		}
		//return false;
		var desired_quantity_refund=document.getElementById('desired_quantity_refund').value;
		if(desired_quantity_refund!=""){
			show_balance_amount_totalrefund();
		}
		
		document.getElementById('refund_selection_container').style.display = '';
		document.getElementById('replacement_selection_container').style.display = 'none';
		document.getElementById('refund_flow').style.display = 'block';
		var refund_method_ems = document.getElementsByName('refund_method');
		for(i=0;i<refund_method_ems.length;i++){
			refund_method_ems[i].required=true;
		}
		document.getElementById('desired_quantity_refund').required = true;
		document.getElementById('desired_quantity_replacement').required = false;
		
		document.getElementById('bank_name_rep').required = false;
		document.getElementById('bank_branch_name_rep').required = false;
		document.getElementById('bank_city_rep').required = false;
		document.getElementById('bank_ifsc_code_rep').required = false;
		document.getElementById('bank_account_number_rep').required = false;
		document.getElementById('bank_account_confirm_number_rep').required = false;
		document.getElementById('account_holder_name_rep').required = false;
		document.getElementById('bank_return_refund_phone_number_rep').required = false;
		
		var refund_return_bank_ems=document.getElementsByName('refund_return_bank');
		for(i=0;i<refund_return_bank_ems.length;i++){
			refund_return_bank_ems[i].required=true;
		}
		var refund_return_replace_bank_ems=document.getElementsByName('refund_return_replace_bank');
		for(i=0;i<refund_return_replace_bank_ems.length;i++){
			refund_return_replace_bank_ems[i].value="";
		}
		
		//document.getElementById('desired_quantity_replacement').disable=true;
	}
	if(checkedValueForReturn!="Refund"){
	
		var input_elems_inside_refund_section=document.getElementById('refund_selection_container').getElementsByTagName('input');
		for(i=0;i<input_elems_inside_refund_section.length;i++){
			input_elems_inside_refund_section[i].required=false;
			input_elems_inside_refund_section[i].value="";
		}
		
		var input_elems_inside_replacement_section=document.getElementById('replacement_selection_container').getElementsByTagName('input');
		for(i=0;i<input_elems_inside_replacement_section.length;i++){
			input_elems_inside_replacement_section[i].required=true;
		}
		document.getElementById('refund_selection_container').style.display = 'none';
		document.getElementById('desired_quantity_replacement').required = true;
		document.getElementById('desired_quantity_refund').required = false;
		document.getElementById('replacement_selection_container').style.display = '';
		document.getElementById('desired_quantity_replacement').required = true;
		document.getElementById('refund_flow').style.display = 'none';
		document.getElementById('bank_ifsc_code').required = false;
		document.getElementById('bank_account_number').required = false;
		document.getElementById('bank_account_confirm_number').required = false;
		document.getElementById('account_holder_name').required = false;
		document.getElementById('bank_return_refund_phone_number').required = false;
		var refund_method_ems = document.getElementsByName('refund_method');
		for(i=0;i<refund_method_ems.length;i++){
			refund_method_ems[i].required=false;
		}
		
		var refund_return_replace_bank_ems=document.getElementsByName('refund_return_replace_bank');
		for(i=0;i<refund_return_replace_bank_ems.length;i++){
			refund_return_replace_bank_ems[i].required=true;
		}
		var refund_return_bank_ems=document.getElementsByName('refund_return_bank');
		for(i=0;i<refund_return_bank_ems.length;i++){
			refund_return_bank_ems[i].value="";
		}
	}
	}
	
	function show_balance_amount_totalrefund(note=''){
		//
		var desired_quantity_refund=document.getElementById('desired_quantity_refund').value
		var ord_addon_products_status=document.getElementById('ord_addon_products_status').value

		if(note=='full_stock_notification'){
			var desired_quantity_refund=document.getElementById('desired_quantity_replacement').value
		}

		var shipping_price_waived=document.getElementById('shipping_price_waived').value
		
		var price_paid_perunit=document.getElementById('price_paid').getAttribute('price_paid');
		var status_of_refund_for_cashback_on_sku=$("#status_of_refund_for_cashback_on_sku").val();
		//alert("price_paid_perunit"+"vvv"+price_paid_perunit);
		
		var price_paid_shipping=document.getElementById('price_paid_shipping').getAttribute('price_paid_shipping');
		var order_item_id=document.getElementById('order_item_id').getAttribute('order_item_id');
		var ordered_quantity=document.getElementById('ordered_quantity').getAttribute('ordered_quantity');
		
		var requested_unitsmarker=""
		if(desired_quantity_refund >1){
			requested_unitsmarker="units";
		}
		if(desired_quantity_refund <2){
			requested_unitsmarker="unit";
		}
		
		if(document.getElementById('order_item_invoice_discount_value')!=null){
			var order_item_invoice_discount_value=parseFloat(document.getElementById('order_item_invoice_discount_value').value) || 0;//"falsey" value to 0
		}else{
			var order_item_invoice_discount_value=0;
		}
		
		
			
			var promotion_available=parseInt(document.getElementById('promotion_available').value);
			
			if(promotion_available==1){
			
			var promotion_minimum_quantity=parseInt(document.getElementById('promotion_minimum_quantity').value);
			var promotion_quote=document.getElementById('promotion_quote').value
			var promotion_default_discount_promo=document.getElementById('promotion_default_discount_promo').value;
			
			var promotion_surprise_gift=document.getElementById('promotion_surprise_gift').value;
			var promotion_surprise_gift_type=document.getElementById('promotion_surprise_gift_type').value;
			var promotion_surprise_gift_skus=document.getElementById('promotion_surprise_gift_skus').value;
			var promotion_surprise_gift_skus_nums=document.getElementById('promotion_surprise_gift_skus_nums').value;
			
			var promotion_item=document.getElementById('promotion_item').value
			var promotion_item_num=document.getElementById('promotion_item_num').value
			var promotion_cashback=parseInt(document.getElementById('promotion_cashback').value);
			var promotion_item_multiplier=parseInt(document.getElementById('promotion_item_multiplier').value);
			var default_discount=parseInt(document.getElementById('default_discount').value);
			var promotion_discount=parseInt(document.getElementById('promotion_discount').value);

			var cash_back_value=parseInt(document.getElementById('cash_back_value').value);
			var individual_price_of_product_with_promotion=parseFloat(document.getElementById('individual_price_of_product_with_promotion').value);
			var individual_price_of_product_without_promotion=parseFloat(document.getElementById('individual_price_of_product_without_promotion').value);
			var total_price_of_product_with_promotion=parseFloat(document.getElementById('total_price_of_product_with_promotion').value) || 0;
			
			var total_price_of_product_without_promotion=parseFloat(document.getElementById('total_price_of_product_without_promotion').value) || 0;//"falsey" value to 0
			
				$("#req_promotion_available").val(promotion_available);
				$("#req_promotion_minimum_quantity").val(promotion_minimum_quantity);
				$("#req_promotion_quote").val(promotion_quote);
				$("#req_promotion_default_discount_promo").val(promotion_default_discount_promo);
				
				$("#req_promotion_surprise_gift").val(promotion_surprise_gift);
				$("#req_promotion_surprise_gift_type").val(promotion_surprise_gift_type);
				$("#req_promotion_surprise_gift_skus").val(promotion_surprise_gift_skus);
				$("#req_promotion_surprise_gift_skus_nums").val(promotion_surprise_gift_skus_nums);
				
				$("#req_promotion_item").val(promotion_item);
				$("#req_promotion_item_num").val(promotion_item_num);
				$("#req_promotion_cashback").val(promotion_cashback);
				$("#req_promotion_item_multiplier").val(promotion_item_multiplier);
				$("#req_default_discount").val(default_discount);
				$("#req_promotion_discount").val(promotion_discount);
			
				$("#req_individual_price_of_product_without_promotion").val(individual_price_of_product_without_promotion);
				$("#req_total_price_of_product_without_promotion").val('');
				$("#req_quantity_without_promotion").val('');
				$("#req_individual_price_of_product_with_promotion").val(individual_price_of_product_with_promotion);
				$("#req_total_price_of_product_with_promotion").val('');
				$("#req_quantity_with_promotion").val('');
				$("#req_cash_back_value").val('');
			}
			//req_quantity_with_promotion
			//req_quantity_without_promotion
			
			var quotient=0;
			var modulo=0;
			var promo_str="";
			var total_amount=0;
		
		if(promotion_available==1){
			

			if((promotion_discount!=default_discount) && (promotion_discount>0)){
				promo_discount_available=1;
			}else{
				promo_discount_available=0;
			}
			
			modulo=parseInt(desired_quantity_refund%promotion_minimum_quantity);//remainder
			quotient=Math.floor(parseInt(desired_quantity_refund/promotion_minimum_quantity));//answer
			
			modulo_purchased=parseInt(ordered_quantity%promotion_minimum_quantity);//remainder
			quotient_purchased=Math.floor(parseInt(ordered_quantity/promotion_minimum_quantity));//answer

			var number_of_free_items=0;
			if(promotion_item_num!=''){
				
				var promo_item_arr = promotion_item_num.split(',');
				var free_item_count=0;
				number_of_free_items=promo_item_arr.length;
				for(h=0;h<number_of_free_items;h++){
					free_item_count+=parseInt(promo_item_arr[h]);
				}
			}
			
			if(promotion_surprise_gift_type=='Qty'){
				remain=parseInt(ordered_quantity-desired_quantity_refund);
				
				if(remain<promotion_minimum_quantity){
					return_surprise_gift=1;
				}else{
					return_surprise_gift=0;
				}
			}
			if(promotion_surprise_gift_type=='<?php echo curr_code; ?>'){
				if(ordered_quantity==desired_quantity_refund){
					return_surprise_gift_amount=1;
				}else{
					return_surprise_gift_amount=0;
				}
			}
			
		if(ordered_quantity >= promotion_minimum_quantity){
			
			if(desired_quantity_refund<promotion_minimum_quantity && (modulo_purchased!=modulo) && desired_quantity_refund!=quotient && (free_item_count>0 || promo_discount_available==1) && promo_discount_available != 1){
				if(modulo_purchased>0){
					
					/*bootbox.alert({
					  size: "small",
					  message: "You cannot request for this quantity.You can give request for multiples of "+promotion_minimum_quantity+ " or " +modulo_purchased,
					});*/
                    alert("You cannot request for this quantity.You can give request for multiples of "+promotion_minimum_quantity+ " or " +modulo_purchased);
				}else{
					
					/*bootbox.alert({
					  size: "small",
					  message: "You cannot request for this quantity.You can give request for multiples of "+promotion_minimum_quantity,
					});*/
                    alert("You cannot request for this quantity.You can give request for multiples of "+promotion_minimum_quantity);
				}
				return false;
			}else if(desired_quantity_refund>promotion_minimum_quantity && modulo_purchased==0 && modulo>0 && promo_discount_available != 1){
				
				/*bootbox.alert({
					  size: "small",
					  message: "You cannot request for this quantity.You can give request for multiples of multiples of "+promotion_minimum_quantity,
					});*/
                alert("You cannot request for this quantity.You can give request for multiples of multiples of "+promotion_minimum_quantity);
				return false;
			}else if(desired_quantity_refund>promotion_minimum_quantity && modulo_purchased>0 && modulo_purchased!=modulo && modulo!=0 && promo_discount_available != 1){
				
				/*bootbox.alert({
					  size: "small",
					  message: "You cannot request for this quantity.You can give request for multiples of "+promotion_minimum_quantity+ " or " +modulo_purchased,
					});*/
					alert( "You cannot request for this quantity.You can give request for multiples of "+promotion_minimum_quantity+ " or " +modulo_purchased);
				return false;
			}
			
			if(desired_quantity_refund >= promotion_minimum_quantity){
				
				if(promo_discount_available != 1){
					
					if(quotient>0){
						
						indi_price_with_promo=parseInt(individual_price_of_product_with_promotion/quotient);
						
						if(number_of_free_items>0){
							count_quotient=(quotient)*promotion_minimum_quantity;
						}else{
							count_quotient=quotient*promotion_minimum_quantity;
						}
						
						//alert(quotient);
						//alert(promotion_minimum_quantity);

						total_price_with_promo=count_quotient*individual_price_of_product_with_promotion;
						
						promo_str+='<tr><td>'+count_quotient+' * '+individual_price_of_product_with_promotion+'<br><small style="color:green;"><b><i>'+promotion_quote+' for quantity ('+count_quotient+')</i></b></small></td><td> <?php echo curr_sym;?>'+total_price_with_promo+'</td></tr>';
						total_amount+=parseFloat(total_price_with_promo);
	
						$("#req_individual_price_of_product_with_promotion").val(individual_price_of_product_with_promotion);
						$("#req_total_price_of_product_with_promotion").val(total_price_with_promo);
						$("#req_quantity_with_promotion").val(count_quotient);
					
						if(promotion_cashback>0){
							
							if(status_of_refund_for_cashback_on_sku=="refunded"){
								cash_back_value=parseFloat(individual_price_of_product_with_promotion*(promotion_cashback/100)).toFixed(2);
								cash_back_value_with_quotient=parseFloat(quotient*cash_back_value).toFixed(2);
								promo_str+='<tr><td><span style="color:red;">Cashback value for quantity ('+quotient+')</span></td><td> <span style="color:red;"> -<?php echo curr_sym;?>'+cash_back_value_with_quotient+'</span></td></tr>';
								total_amount-=parseFloat(cash_back_value_with_quotient);
							}else{
								cash_back_value_with_quotient=0;
							}
							$("#req_cash_back_value").val(cash_back_value_with_quotient);	
						}
					}
					
					if(number_of_free_items>0){
						
						free_skus_from_inventory=<?php echo json_encode($free_skus_from_inventory);?>;
						
						free_str="";
						
						//alert(free_skus_from_inventory);
						if(free_skus_from_inventory!=null){
							if(free_skus_from_inventory.length>0){
								
								for(y=0;y<free_skus_from_inventory.length;y++){
									//alert(free_skus_from_inventory[y].image);
									free_str+="<tr><td><img src="+'<?php echo base_url() ?>'+free_skus_from_inventory[y].image+" style='width:50px;height:60px'></td><td>"+free_skus_from_inventory[y].str+" <small><br> Quantity - "+quotient*parseInt(free_skus_from_inventory[y].count)+"</small></td></tr>";
								}
							}
							
						}

						promo_str+='<tr><td colspan="2"><b><h6 style="color:blue;"></b> Free Items to be returned with the requested Quantity of product</h6>'+free_str+'</td></tr>';
					}
					
					if(promotion_surprise_gift_type=='Qty'){
						if(return_surprise_gift==1){
							surprise_free_skus_from_inventory=<?php echo json_encode($surprise_free_skus_from_inventory);?>;
							surprise_free_str="";
						
							//alert(free_skus_from_inventory);
							if(surprise_free_skus_from_inventory!=null){
								if(surprise_free_skus_from_inventory.length>0){
									
									for(y=0;y<surprise_free_skus_from_inventory.length;y++){
										//alert(free_skus_from_inventory[y].image);
										surprise_free_str+="<tr><td><img src="+'<?php echo base_url() ?>'+surprise_free_skus_from_inventory[y].image+" style='width:50px;height:60px'></td><td>"+surprise_free_skus_from_inventory[y].str+" <small><br> Quantity - "+parseInt(surprise_free_skus_from_inventory[y].count)+"</small></td></tr>";
									}
								}
								
							}
							promo_str+='<tr><td colspan="2"><b><h6 style="color:blue;"></b> Free Items to be returned with the requested Quantity of product</h6>'+surprise_free_str+'</td></tr>';
						}
					}
					if(promotion_surprise_gift_type=='<?php echo curr_code; ?>'){
						if(return_surprise_gift_amount==1){
							promo_str+='<tr><td><h6 style="color:blue;"><?php echo curr_sym;?>'+promotion_surprise_gift+' was credited to your wallet. So this amount will be reduced from your total item price.</h6> </td><td style="color:red"> - <?php echo curr_sym;?>'+promotion_surprise_gift+'</td></tr>';
							total_amount-=parseFloat(promotion_surprise_gift);
						}
						
					}
					if(modulo>0){
						//breakup 2
						indi_price_without_promo=parseInt(individual_price_of_product_without_promotion/modulo);
						
						total_price_without_promo=parseInt(modulo*individual_price_of_product_without_promotion);
						
						promo_str+='<tr><td>'+modulo+' * '+individual_price_of_product_without_promotion+'<br><small style="color:green;"><b><i>'+promotion_default_discount_promo+' for quantity ('+modulo+')</i></b></small></td><td> <?php echo curr_sym;?>'+total_price_without_promo+'</td></tr>';
						total_amount+=parseFloat(total_price_without_promo);
						$("#req_individual_price_of_product_without_promotion").val(individual_price_of_product_without_promotion);
						$("#req_total_price_of_product_without_promotion").val(total_price_without_promo);
						$("#req_quantity_without_promotion").val(modulo);
					}
					
				}
				if(promo_discount_available == 1){
					
					
					total_price_of_product=parseFloat(total_price_of_product_with_promotion+total_price_of_product_without_promotion).toFixed(2);
					
					indi_price_with_promo=parseFloat(total_price_of_product/ordered_quantity).toFixed(2);
					
					//alert((22.34944656465).toFixed(2));
					//alert(indi_price_with_promo);
					//alert(parseInt(85.223333));
					
					total=parseFloat(indi_price_with_promo*desired_quantity_refund).toFixed(2);
					promo_str+='<tr><td>'+desired_quantity_refund+' * '+indi_price_with_promo+'</td><td><?php echo curr_sym;?>'+total+'</td></tr>';
					total_amount+=parseFloat(total);
					
					$("#req_individual_price_of_product_with_promotion").val(indi_price_with_promo);
					$("#req_total_price_of_product_with_promotion").val(total);
					$("#req_quantity_with_promotion").val(desired_quantity_refund);
				}
			
				
			}else{
				if(promo_discount_available != 1){
					if(modulo>0){
						//breakup 2
						indi_price_without_promo=parseInt(individual_price_of_product_without_promotion/modulo);
						
						total_price_without_promo=parseInt(modulo*individual_price_of_product_without_promotion);
						
						promo_str+='<tr><td>'+modulo+' * '+individual_price_of_product_without_promotion+'<br><small style="color:green;"><b><i>'+promotion_default_discount_promo+' for quantity ('+modulo+')</i></b></small></td><td> <?php echo curr_sym;?>'+total_price_without_promo+'</td></tr>';
						
						total_amount+=parseFloat(total_price_without_promo);
						$("#req_individual_price_of_product_without_promotion").val(individual_price_of_product_without_promotion);
						$("#req_total_price_of_product_without_promotion").val(total_price_without_promo);
						$("#req_quantity_without_promotion").val(modulo);
						
						
					}
				}
				
				if(promo_discount_available == 1){
					total_price_of_product=parseFloat(total_price_of_product_with_promotion+total_price_of_product_without_promotion).toFixed(2);
					indi_price_with_promo=parseFloat(total_price_of_product/ordered_quantity).toFixed(2);
					total=parseFloat(indi_price_with_promo*desired_quantity_refund).toFixed(2);
					promo_str+='<tr><td>'+desired_quantity_refund+' * '+indi_price_with_promo+'</td><td><?php echo curr_sym;?>'+total+'</td></tr>';
					total_amount+=parseFloat(total).toFixed(2);
			
					$("#req_individual_price_of_product_with_promotion").val(indi_price_with_promo);
					$("#req_total_price_of_product_with_promotion").val(total);
					$("#req_quantity_with_promotion").val(desired_quantity_refund);
				}
			}

			
			
			
		}else{
			//alert('else condi');
		}
			

				/*if(quotient>0){
					//breakup 1
					indi_price_with_promo=parseInt(individual_price_of_product_with_promotion/quotient);
					
					total_price_with_promo=quotient*individual_price_of_product_with_promotion;
					
					promo_str+='<tr><td>'+quotient+' * '+individual_price_of_product_with_promotion+'<br><small style="color:green;"><b><i>'+promotion_quote+' for quantity ('+quotient+')</i></b></small></td><td> <?php echo curr_sym;?>'+total_price_with_promo+'</td></tr>';
					total_amount+=total_price_with_promo;
				}*/
			
				
			

		}//promo_available
		
		if(shipping_price_waived=="yes"){
			shipping_price_str="";
			shipping_charge=0;
			//total_price=((parseInt(price_paid_perunit)*parseInt(desired_quantity_refund))+parseInt(shipping_charge))
			
		}else{
			
			//alert(price_paid_shipping+"price_paid_shipping");
			//alert(parseInt(price_paid_shipping)/parseInt(ordered_quantity)+"price_paid_shipping");
			
			shipping_charge=Math.round((parseInt(price_paid_shipping)/parseInt(ordered_quantity))*parseInt(desired_quantity_refund));
		}
			str_invoice_discount="";		
			
			if(promotion_available==0){
				total_product_price=(parseInt(price_paid_perunit)*parseInt(desired_quantity_refund));
				
				
				if(order_item_invoice_discount_value>0){
					
					each_inv_dis=(Math.round((order_item_invoice_discount_value/ordered_quantity)*100))/100;
					inv_discount=parseFloat((each_inv_dis)*desired_quantity_refund).toFixed(2);
					
					total_product_price-=parseFloat(inv_discount);					
					str_invoice_discount='<tr><td style="color:blue;"> Invoice discount price which has to be deducted for this '+desired_quantity_refund+' '+requested_unitsmarker+' ('+desired_quantity_refund+' * '+each_inv_dis+') </td><td style="color:red;">- <?php echo curr_sym;?>'+inv_discount+'</td></tr>';
					
				}
				total_price=total_product_price+parseInt(shipping_charge);
				total_amount=parseFloat(total_price).toFixed(2);
			}
			if(promotion_available==1){
			
				if(order_item_invoice_discount_value>0){
					//invoice_discount=
					
					each_inv_dis=Math.round((order_item_invoice_discount_value/ordered_quantity)*100)/100;
					inv_discount=parseFloat((each_inv_dis)*desired_quantity_refund).toFixed(2);
					total_product_price=parseFloat(total_amount-inv_discount).toFixed(2);
					
					str_invoice_discount='<tr><td style="color:blue;"> Invoice discount price which has to be deducted for this '+desired_quantity_refund+' '+requested_unitsmarker+' ('+desired_quantity_refund+' * '+each_inv_dis+') </td><td style="color:red;">- <?php echo curr_sym;?>'+inv_discount+'</td></tr>';
					
					total_amount=parseFloat(parseFloat(total_product_price)+parseFloat(shipping_charge)).toFixed(2);
				
				}else{
					total_amount=parseFloat(total_amount)+parseFloat(shipping_charge);	
				}				
				
			}
			shipping_price_str='<tr><td>Shipping Charge  for  '+desired_quantity_refund+' '+requested_unitsmarker+' </td><td><?php echo curr_sym;?>'+parseInt(shipping_charge)+'</td></tr>';
		

		str='<table class="table table-bordered table-hover"><thead><tr><th>Net Amount Calculations</th><th>Value</th><tr></thead><tbody>';
			
			if(promotion_available!=1){

				if(ord_addon_products_status=='1'){
					str+='<tr><td> Total Product price </td><td><?php echo curr_sym;?>'+parseInt(price_paid_perunit)+'</td></tr>';
				}else{
					str+='<tr><td>'+price_paid_perunit+' * '+desired_quantity_refund+' '+requested_unitsmarker+' </td><td><?php echo curr_sym;?>'+parseInt(price_paid_perunit)*parseInt(desired_quantity_refund)+'</td></tr>';
				}
			}
			str+=promo_str;
			str+=str_invoice_discount;
			str+=shipping_price_str;
			
			str+='<tr><td>Total Refund Amount </td><td><?php echo curr_sym;?>'+total_amount+'</td></tr><tr><td colspan=2><i>{Refund will take effect after successful reciept of the products by Admin}</i></td></tr></tbody></table>'
			document.getElementById('refund_order_summary').innerHTML=str;
	
	}
	
function setRefundMethod(){
	$('#error_refund_method').html('');
	document.getElementById('bank_account_data_rep').style.display = 'none';
	document.getElementById('bank_account_data').style.display = 'none';
	var checkedValue = null; 
	var inputElements = document.getElementsByName('refund_method');
	
		for(var i=0; inputElements[i]; ++i){
		      if(inputElements[i].checked){
		           checkedValue = inputElements[i].value;
		           break;
		      }
		}
		if(checkedValue=="Bank Transfer"){
			document.getElementById('bank_account_data').style.display = 'block';
			document.getElementById('bank_ifsc_code').required = true;
			document.getElementById('bank_account_number').required = true;
			document.getElementById('bank_account_confirm_number').required = true;
			document.getElementById('account_holder_name').required = true;
			document.getElementById('bank_return_refund_phone_number').required = true;
		}
		else{
			document.getElementById('bank_account_data').style.display = 'none';
			document.getElementById('bank_ifsc_code').required = false;
			document.getElementById('bank_account_number').required = false;
			document.getElementById('bank_account_confirm_number').required = false;
			document.getElementById('account_holder_name').required = false;
			document.getElementById('bank_return_refund_phone_number').required = false; 
		}
	
}


$(document).ready(function(){
	$('#new_piece_options').on('shown.bs.modal', showAvailableOptions);
	$('#new_piece_options').on('hidden.bs.modal', showReplacedOptions);
	
	/*$('#new_piece_options').on('shown.bs.modal', function() {
	   showAvailableOptions();
	});
	$('#new_piece_options').on('hidden.bs.modal', function () {
		showReplacedOptions();
	});*/
	
	$('.panel-group').on('hidden.bs.collapse', toggleIcon);
	$('.panel-group').on('shown.bs.collapse', toggleIcon);
	$('.panel-group_bank').on('hidden.bs.collapse', add_bank);
	$('.panel-group_bank').on('shown.bs.collapse', use_bank);
});		

//<!--script used in customer accounts starts-->

function toggleIcon(e) {

    $(e.target).prev('.panel-heading').find('.more-less').toggleClass('glyphicon-plus glyphicon-minus');
}



/*function used in return cancel wallet page*/
function add_bank(){
	$('#bank_data_selector').html('Use Previously Added Bank Account');
}
function use_bank(){
	$('#bank_data_selector').html('Add New Bank Account');
	document.getElementById('bank_ifsc_code').setAttribute('readOnly',true);
	document.getElementById('bank_account_number').setAttribute('readOnly',true);
	document.getElementById('bank_account_confirm_number').setAttribute('readOnly',true);
	document.getElementById('account_holder_name').setAttribute('readOnly',true);
	document.getElementById('bank_return_refund_phone_number').setAttribute('readOnly',true);
	document.getElementById('save_this_bank_data').disabled=true;
}

function getting_shipping_charge_for_ReplacedOptions(v='',available_units='',price_difference=''){
	
	if(document.getElementById('order_item_id')!=null){
		var order_item_id=document.getElementById('order_item_id').getAttribute('order_item_id');
	}
	var checkedValueForReturnInventoryId = null; 
	var inputElements = document.getElementsByName('replacement_select');
	var checkedValueForReturnInventoryIdattr="";
	for(var i=0; inputElements[i]; ++i){
	      if(inputElements[i].checked){
	           checkedValueForReturnInventoryId = inputElements[i];
	           checkedValueForReturnInventoryIdattr=inputElements[i].getAttribute('inventory_id');
	           break;
	      }
	}

	if(v=="replace_and_refund"){
		if(checkedValueForReturnInventoryIdattr==""){
			selected_inventory_id=document.getElementById('replace_and_refund').getAttribute('remaining_item_id');
			checkedValueForReturnInventoryIdattr=selected_inventory_id;
		}
		
		var original_desired_quantity =available_units;
	
	var url_u = "<?php echo base_url('Account/get_corresponding_shipping_charge');?>";
		$.ajax({
		url:url_u,
		type:'POST',
		data:"order_item_id="+order_item_id+"&desired_quantity_refund="+original_desired_quantity+"&selected_inventory_id="+checkedValueForReturnInventoryIdattr,
		asyn:false,
		dataType:"JSON",
		beforeSend: function() {
        document.getElementById('refund_order_summary').innerHTML="Processing..";
		},
		success:function(res){
			//alert(res);
			//alert(res.customer_has_to_pay);
			//alert(res.shipping_charge);
			//customer_has_to_pay is a pecentage
			
			customer_has_to_pay=parseInt(res.customer_has_to_pay);
			$("#shipping_charge_percent_paid_by_customer").val(customer_has_to_pay);
			
			shipping_charge_org=parseFloat(res.shipping_charge);
			
			shipping_charge=(parseInt(customer_has_to_pay)/100)*shipping_charge_org;
			//alert(shipping_charge);
			$("#shipping_charge_requested").val(shipping_charge);
			
			shipping_charge=-shipping_charge;
			
			if(price_difference==0){
				$(".xx:last").after('<tr><td colspan="2">Shipping charge for a replacement item of '+original_desired_quantity+' units <i style="color:blue;">('+customer_has_to_pay+' % of original shipping charge)</i></td><td>'+Math.abs(shipping_charge)+'</td></tr><tr><td colspan="2">Total differential amount to be paid by you</td><td>'+Math.abs(shipping_charge)+'</td></tr>');
				
				if(Math.abs(shipping_charge)>0){
					$(".customer_payment").html('<select name="pay_balance_method" class="form-control" id="pay_balance_method" onchange="remove_error();"><option value="" selected>Select Payment Method</option><option value="cod">Cod</option><option value="wallet">Wallet</option><option value="online_payment">Online Payment</option></select>');
					customer_payment_flag="";	
				}else{
					customer_payment_flag="yes";	
				}
				
			}
			//else if(price_difference>0){
			else{
				var cal_final_amount=parseFloat(price_difference)+parseFloat(shipping_charge);
				if(cal_final_amount>0){
					$(".xx:last").after('<tr><td colspan="2">Shipping charge for a replacement item of '+original_desired_quantity+' units<i style="color:blue;">('+customer_has_to_pay+' % of original shipping charge)</i></td><td>'+Math.abs(shipping_charge)+'</td></tr><tr><td colspan="2">Total differential amount to be refunded to you</td><td>'+Math.abs(cal_final_amount)+'</td></tr>');
				}
				if(cal_final_amount<0){
					$(".xx:last").after('<tr><td colspan="2">Shipping charge for a replacement item of '+original_desired_quantity+' units <i style="color:blue;">('+customer_has_to_pay+' % original of shipping charge)</i></td><td>'+Math.abs(shipping_charge)+'</td></tr><tr><td colspan="2">Total differential amount to be paid by you</td><td>'+Math.abs(cal_final_amount)+'</td></tr>');
					$("#payment_method_admin_div").css({"display":"none"});
					$(".customer_payment").html('<select name="pay_balance_method" class="form-control" id="pay_balance_method" onchange="remove_error();"><option value="" selected>Select Payment Method</option><option value="cod">Cod</option><option value="wallet">Wallet</option><option value="online_payment">Online Payment</option></select><br>');
					customer_payment_flag="yes";
				
				
				}
				if(cal_final_amount==0){
					$(".xx:last").after('<tr><td colspan="2">Shipping charge for a replacement item of '+original_desired_quantity+' units <i style="color:blue;">('+customer_has_to_pay+' % of original shipping charge)</i></td><td>'+Math.abs(shipping_charge)+'</td></tr><tr><td colspan="2">Total Differential Amount</td><td>Nill</td></tr>');
					$("#payment_method_admin_div").css({"display":"none"});
				}
			}
				
		}
		});
	
	}else{
	
	if(checkedValueForReturnInventoryIdattr!=""){
		
	var original_desired_quantity =document.getElementById('original_desired_quantity_'+checkedValueForReturnInventoryIdattr).value;
	
	var url_u = "<?php echo base_url('Account/get_corresponding_shipping_charge');?>";
		$.ajax({
		url:url_u,
		type:'POST',
		data:"order_item_id="+order_item_id+"&desired_quantity_refund="+original_desired_quantity+"&selected_inventory_id="+checkedValueForReturnInventoryIdattr,
		asyn:false,
		dataType:"JSON",
		beforeSend: function() {
        document.getElementById('refund_order_summary').innerHTML="Processing..";
		},
		success:function(res){
			//alert(res.customer_has_to_pay);
			//customer_has_to_pay is a pecentage
			customer_has_to_pay=res.customer_has_to_pay;
			$("#shipping_charge_percent_paid_by_customer").val(customer_has_to_pay);
			
			shipping_charge_org=res.shipping_charge;
			shipping_charge=Math.round((parseInt(customer_has_to_pay)/100)*shipping_charge_org);
			//alert(shipping_charge);
			$("#shipping_charge_requested").val(shipping_charge);
			shipping_charge=-shipping_charge;
			if(price_difference==0){
				$(".xx:last").after('<tr><td colspan="2">Shipping charge for a replacement item of '+original_desired_quantity+' units <i style="color:blue;">('+customer_has_to_pay+' % of shipping charge)</i></td><td>'+Math.abs(shipping_charge)+'</td></tr><tr><td colspan="2">Total differential amount to be paid by you</td><td>'+Math.abs(shipping_charge)+'</td></tr>');
				if(Math.abs(shipping_charge)>0){
					$(".customer_payment").html('<select name="pay_balance_method" class="form-control" id="pay_balance_method" onchange="remove_error();"><option value="" selected>Select Payment Method</option><option value="cod">Cod</option><option value="wallet">Wallet</option><option value="online_payment">Online Payment</option></select><br>');
					customer_payment_flag="";	
				}else{
					customer_payment_flag="yes";	
				}
			}
			//else if(price_difference>0){
			else{
				var cal_final_amount=parseFloat(price_difference)+parseFloat(shipping_charge);
				
				if(cal_final_amount>0){
					$(".xx:last").after('<tr><td colspan="2">Shipping charge for a replacement item of '+original_desired_quantity+' units <i style="color:blue;">('+customer_has_to_pay+' % of original shipping charge)</i></td><td>'+Math.abs(shipping_charge)+'</td></tr><tr><td colspan="2">Total differential amount to be refunded to you</td><td>'+Math.abs(cal_final_amount)+'</td></tr>');
				}
				if(cal_final_amount<0){
					$(".xx:last").after('<tr><td colspan="2">Shipping charge for a replacement item of '+original_desired_quantity+' units <i style="color:blue;">('+customer_has_to_pay+' % of original shipping charge)</i></td><td>'+Math.abs(shipping_charge)+'</td></tr><tr><td colspan="2">Total differential amount to be paid by you</td><td>'+Math.abs(cal_final_amount)+'</td></tr>');
					$("#payment_method_admin_div").css({"display":"none"});
					
					//if(Math.abs(shipping_charge)>0){
						$(".customer_payment").html('<select name="pay_balance_method" class="form-control" id="pay_balance_method" onchange="remove_error();"><option value="" selected>Select Payment Method</option><option value="cod">Cod</option><option value="wallet">Wallet</option><option value="online_payment">Online Payment</option></select><br>');
					//	customer_payment_flag="";	
					//}else{
						customer_payment_flag="yes";	
					//}
				}
				if(cal_final_amount==0){
					$(".xx:last").after('<tr><td colspan="2">Shipping charge for a replacement item of '+original_desired_quantity+' units <i style="color:blue;">('+customer_has_to_pay+' % of original shipping charge)</i></td><td>'+Math.abs(shipping_charge)+'</td></tr><tr><td colspan="2">Total Differential Amount</td><td>Nill</td></tr>');
					$("#payment_method_admin_div").css({"display":"none"});
					
				}
			}

		}
		});
	}
	}
}

function showReplacedOptions(){ 


	document.getElementById('summary_for_replacement_div').innerHTML="";
	document.getElementById('ref_opt_div').innerHTML="";
	document.getElementById('checkedValueForReturnInventoryId').innerHTML="";
	
	var checkedValueForReturnInventoryId = null; 
	var inputElements = document.getElementsByName('replacement_select');
	var checkedValueForReturnInventoryIdattr="";
	for(var i=0; inputElements[i]; ++i){
	      if(inputElements[i].checked){
	           checkedValueForReturnInventoryId = inputElements[i];
	           checkedValueForReturnInventoryIdattr=inputElements[i].getAttribute('inventory_id');
	           break;
	      }
	}
	
	var selected_inv_id=checkedValueForReturnInventoryIdattr;
	
	attribute_1=$('#rep_attribute_1_'+selected_inv_id).val();
	attribute_1_value=$('#rep_attribute_val_1_'+selected_inv_id).val();
	
	var attr_str=attribute_1+' : '+attribute_1_value;
	
	if($('#rep_attribute_2_'+selected_inv_id).val()){
		attribute_2=$('#rep_attribute_2_'+selected_inv_id).val();
		attribute_2_value=$('#rep_attribute_val_2_'+selected_inv_id).val();
		attr_str+=', '+attribute_2+' : '+attribute_2_value;
	}
	
	
	
	/*
	alert(attribute_1);
	alert(attribute_1_value);
	alert(attribute_2);
	alert(attribute_2_value);
	*/
	
	//alert(checkedValueForReturnInventoryIdattr+"new alert");
	
	if(checkedValueForReturnInventoryIdattr!=""){
	document.getElementById("selling_price_of_single_id_"+checkedValueForReturnInventoryIdattr).setAttribute('name','individual_net_price_of_replaced')
	}
	
	if(checkedValueForReturnInventoryIdattr!=""){
		//getting_shipping_charge_for_ReplacedOptions();
		var original_desired_quantity =document.getElementById('original_desired_quantity_'+checkedValueForReturnInventoryIdattr).value;
		var return_available_quantity =document.getElementById('return_available_quantity_'+checkedValueForReturnInventoryIdattr).value;
		var remainingUnits=Math.abs(original_desired_quantity-return_available_quantity)
		original_desired_quantity=parseInt(original_desired_quantity)
		return_available_quantity=parseInt(return_available_quantity)
		
		//calculate shipping charge
		if(document.getElementById('order_item_id')!=null){

		str="";str_opt="";
		if(original_desired_quantity>return_available_quantity){
			
			str_opt+='<div class="form-group"><div class="col-md-9 col-md-offset-3">Selected SKU is Out of Stock. Choose an option below to proceed.</div></div><div class="form-group"><div class="col-md-9 col-md-offset-3"><div class="row"><div class="col-md-12"><label class="radio-inline"><input type="radio" name="replacement_limited_method" onchange="set_optional_replacement_way()" value="go_with_another_selection"> Choose another SKU</label></div></div>';
			if(return_available_quantity>0){
				str_opt+='<div class="row"><div class="col-md-12"><label class="radio-inline"><input type="radio" name="replacement_limited_method"  onchange="procced_with_replace_and_refund(this);"  remaining_item_id="'+checkedValueForReturnInventoryIdattr+'" value="procced_with_replace_and_refund" id="replace_and_refund"> &nbsp;Replace '+return_available_quantity+' units and refund remaining '+(original_desired_quantity-return_available_quantity)+' units</label></div></div>';
			}
			if(return_available_quantity==0){
				str_opt+='<div class="row"><div class="col-md-12"><label class="radio-inline"><input type="radio" name="replacement_limited_method"  onchange="procced_with_replace_and_refund(this);" remaining_item_id="'+checkedValueForReturnInventoryIdattr+'" value="procced_with_replace_and_refund"> Refund '+(original_desired_quantity-return_available_quantity)+' units</label></div></div>';
			}
			str_opt+='<div class="row"><div class="col-md-12"><label class="radio-inline"><input type="radio" name="replacement_limited_method"  onchange="replace_when_stock_become_available(this)" remaining_item_id="'+checkedValueForReturnInventoryIdattr+'" value="procced_with_replace_all_when_stock_become_available"> Replace all '+original_desired_quantity+' units when stock becomes available</label></div></div></div></div>';
			
			var replacement_value=document.getElementById('replacement_value_'+checkedValueForReturnInventoryIdattr).value
			//////////
			
			///////////////
			checkedValueForReturnInventoryId=$(checkedValueForReturnInventoryId).closest('div').prev('div').html();
			checkedValueForReturnInventoryId=checkedValueForReturnInventoryId.concat(str);
			document.getElementById('checkedValueForReturnInventoryId').innerHTML=checkedValueForReturnInventoryId;
			document.getElementById('rep_options').innerHTML=str_opt;
		}
		else{
			
			
			checkedValueForReturnInventoryId=$(checkedValueForReturnInventoryId).closest('div').prev('div').html();
			
			var replacement_value=document.getElementById('replacement_value_'+checkedValueForReturnInventoryIdattr).value
			
			var return_value=document.getElementById('return_value_'+checkedValueForReturnInventoryIdattr).value
			
			var requested_units=document.getElementById('original_desired_quantity_'+checkedValueForReturnInventoryIdattr).value
			var available_units=document.getElementById('return_available_quantity_'+checkedValueForReturnInventoryIdattr).value;
			
			
			
			var replaced_value=(replacement_value)*parseInt(requested_units)
			var returned_value=(return_value)*parseInt(requested_units)
			var price_difference=parseFloat((returned_value)-(replaced_value)).toFixed(2);
			getting_shipping_charge_for_ReplacedOptions('','',price_difference);
				//alert(price_difference);
			var available_unitsmarker=""
			if(available_units >1){
				available_unitsmarker="units";
			}
			if(available_units <2){
				available_unitsmarker="unit";
			}
			var requested_unitsmarker=""
			if(requested_units >1){
				requested_unitsmarker="units";
			}
			if(requested_units <2){
				requested_unitsmarker="unit";
			}

			var final_val=returned_value-replaced_value;
				var final_statement=""
				if(final_val>0){
					document.getElementById('bank_account_data_rep').style.display="none";
					final_statement+='<td colspan="2">Final Amount to be Refunded to You</td><td>'+Math.abs(final_val)+'</td>'
					document.getElementById('bank_ifsc_code_rep').required=true;
					document.getElementById('bank_account_number_rep').required=true;
					document.getElementById('bank_account_confirm_number_rep').required=true;
					document.getElementById('account_holder_name_rep').required=true;
					document.getElementById('bank_return_refund_phone_number_rep').required=true;
					document.getElementById('save_this_bank_data_rep').required=true;	
				}
				if(final_val<0){
					document.getElementById('bank_account_data_rep').style.display="none";
					final_statement+='<td colspan="2">Final Amount to be Paid by You</td><td>'+Math.abs(final_val)+'</td>'
				}
				
				str+='<table class="table table-bordered table-hover"><thead><tr><th>Details</th><th>Net Amount Calculation</th><th>Value</th></tr></thead><tbody><tr><td>Selected Product for Replacement <br><small>('+attr_str+')</small> </td><td>'+replacement_value+' * '+requested_units+' '+available_unitsmarker+'</td><td>'+(replacement_value)*parseInt(requested_units)+'</td></tr><tr><td>Purchased Product </td><td>'+return_value+' * '+requested_units+' '+requested_unitsmarker+'</i></td><td>'+(return_value)*parseInt(requested_units)+'</td></tr><tr class="xx">'+final_statement+'</tr>';
			
				if(price_difference==0){
					str+='<tr class="xx"><td colspan="2">Differential Amount</td><td>NILL</td></tr>';
				}
				str+='</tbody></table>';
				
				
				str+='<div class="customer_payment"></div>';
				
			
				str+='<input type="hidden" name="selected_inventory_id_for_rep"  value="'+checkedValueForReturnInventoryIdattr+'">'
				
				str+='<input type="hidden" name="stock_quantity_available_at_replacement" value="'+available_units+'"><input type="hidden" name="stock_quantity_desired_at_replacement" value="'+requested_units+'"><input type="hidden" name="return_value_each" value="'+return_value+'"><input type="hidden" name="replacement_value_each" value="'+replacement_value+'">'
			
		
			if(final_val>0){
				ref_opt='<div class="form-group" id="payment_method_admin_div"><label class="control-label col-md-3">Choose Refund Method</label><div class="col-md-9">';

                <?php if($order_payment_type=='Razorpay'){ ?>
                ref_opt+=' <label class="checkbox-inline"><input type="radio" checked name="refund_method" required onchange="setRefundMethod_rep()" value="Razorpay"> Razorpay</label>';

                <?php }else{
                    ?>
                ref_opt+='<label class="checkbox-inline"><input type="radio" name="refund_method_rep"   onchange="setRefundMethod_rep()" value="Wallet"> Wallet</label><label class="checkbox-inline"><input type="radio" name="refund_method_rep"  onchange="setRefundMethod_rep()" value="Bank Transfer"> Bank Transfer</label>';
                <?php
                } ?>
                ref_opt+='</div></div>';
				document.getElementById('ref_opt_div').innerHTML=ref_opt;
			}	
				if(final_val<0){
					document.getElementById('bank_ifsc_code_rep').required=false;
					document.getElementById('bank_account_number_rep').required=false;
					document.getElementById('bank_account_confirm_number_rep').required=false;
					document.getElementById('account_holder_name_rep').required=false;
					document.getElementById('bank_return_refund_phone_number_rep').required=false;
					document.getElementById('save_this_bank_data_rep').required=false;
					//str+='<label for="sel1">Select Method For Payment3:</label><select name="pay_balance_method" class="form-control"><option value="cod">Cod</option><option value="wallet">Wallet</option><option value="online_payment">Online Payment</option></select>'
				}
				if(final_val==0){
					document.getElementById('bank_ifsc_code_rep').required=false;
					document.getElementById('bank_account_number_rep').required=false;
					document.getElementById('bank_account_confirm_number_rep').required=false;
					document.getElementById('account_holder_name_rep').required=false;
					document.getElementById('bank_return_refund_phone_number_rep').required=false;
					document.getElementById('save_this_bank_data_rep').required=false;
				}
				

			document.getElementById('checkedValueForReturnInventoryId').innerHTML=checkedValueForReturnInventoryId		
			document.getElementById('summary_for_replacement_div').innerHTML=str;
			
			
		}
	
		}
	}
}

function setRefundMethod_rep(){

	document.getElementById('bank_account_data_rep').style.display=""
	var checkedValue = null; 
	var inputElements = document.getElementsByName('refund_method_rep');
	for(var i=0; inputElements[i]; ++i){
	      if(inputElements[i].checked){
	           checkedValue = inputElements[i].value;
	           break;
	      }
	}
	if(checkedValue=="Bank Transfer"){
		document.getElementById('bank_account_data_rep').style.display = 'block';
		document.getElementById('bank_name_rep').required = true;
		document.getElementById('bank_branch_name_rep').required = true;
		document.getElementById('bank_city_rep').required = true;
		document.getElementById('bank_ifsc_code_rep').required = true;
		document.getElementById('bank_account_number_rep').required = true;
		document.getElementById('bank_account_confirm_number_rep').required = true;
		document.getElementById('account_holder_name_rep').required = true;
		document.getElementById('bank_return_refund_phone_number_rep').required = true;
		document.getElementById('save_this_bank_data_rep').required = true;
	}
	else{
		document.getElementById('bank_account_data_rep').style.display = 'none';
		document.getElementById('bank_name_rep').required = false;
		document.getElementById('bank_branch_name_rep').required = false;
		document.getElementById('bank_city_rep').required = false;
		document.getElementById('bank_ifsc_code_rep').required = false;
		document.getElementById('bank_account_number_rep').required = false;
		document.getElementById('bank_account_confirm_number_rep').required = false;
		document.getElementById('account_holder_name_rep').required = false;
		document.getElementById('bank_return_refund_phone_number_rep').required = false;
		document.getElementById('save_this_bank_data_rep').required = false; 
	}
}

function set_optional_replacement_way(){
	document.getElementById('bank_account_data_rep').style.display="none";
	document.getElementById('checkedValueForReturnInventoryId').innerHTML="";
	document.getElementById('rep_options').innerHTML="";
	document.getElementById('summary_for_replacement_div').innerHTML="";
	document.getElementById('ref_opt_div').innerHTML="";
	document.getElementById('availabel_option_button_controller').click();
}

function replace_when_stock_become_available(obj){
	show_balance_amount_totalrefund('full_stock_notification');
	/*document.getElementById('bank_ifsc_code_rep').required=false;
	document.getElementById('bank_account_number_rep').required=false;
	document.getElementById('bank_account_confirm_number_rep').required=false;
	document.getElementById('account_holder_name_rep').required=false;
	document.getElementById('bank_return_refund_phone_number_rep').required=false;
	document.getElementById('save_this_bank_data_rep').required=false;*/
	
	//document.getElementById('bank_account_data_rep').style.display="";
	document.getElementById('refund_method_rep_div').style.display="";
	var remaining_item_id=$(obj).attr('remaining_item_id');
	var replacement_value=$('#replacement_value_'+remaining_item_id).val();
	var return_value=$('#return_value_'+remaining_item_id).val();
	
	var original_desired_quantity=$('#original_desired_quantity_'+remaining_item_id).val();
	
	getting_shipping_charge_for_ReplacedOptions();
	
	str="";
	str+='<input type="hidden" name="selected_inventory_id_for_rep"  value="'+remaining_item_id+'"><input type="hidden" name="replacement_value_each" value="'+replacement_value+'"><input type="hidden" name="return_value_each" value="'+return_value+'"><input type="hidden" name="quantity_replacement" value="'+original_desired_quantity+'">'
	document.getElementById('summary_for_replacement_div').innerHTML='<div class="margin-bottom">Your email ID listed with us is <strong>'+'<?php echo $this->session->userdata("customer_email"); ?>'+'</strong>. We will initiate the replacement process once the stock is available. If the stock is not fulfilled within given days then we will prompt you for returns and we will initiate refunds from our side.</div> '+str+'<div class="f-row"><select name="timelimitdefined" id="timelimitdefined" class="form-control" required><option value="" selected>Select number of days you wait for stock availability</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select></div>';
	
}
var shipping_charge="0";

customer_payment_flag="";

function procced_with_replace_and_refund(obj){
	
	//alert(shipping_charge);
	if(document.getElementById('order_item_id')!=null){
		var order_item_id=document.getElementById('order_item_id').getAttribute('order_item_id');
	}
	
	str="";
	var remaining_item_id=$(obj).attr('remaining_item_id')
	
	
	attribute_1=$('#rep_attribute_1_'+remaining_item_id).val();
	attribute_1_value=$('#rep_attribute_val_1_'+remaining_item_id).val();
	
	var attr_str=attribute_1+' : '+attribute_1_value;
	
	if($('#rep_attribute_2_'+remaining_item_id).val()){
		attribute_2=$('#rep_attribute_2_'+remaining_item_id).val();
		attribute_2_value=$('#rep_attribute_val_2_'+remaining_item_id).val();
		attr_str+=', '+attribute_2+' : '+attribute_2_value;
	}
	
	
	var replacement_value=document.getElementById('replacement_value_'+remaining_item_id).value

	var replacement_shipping_value=document.getElementById('replacement_shipping_value_'+remaining_item_id).value
	var return_value=document.getElementById('return_value_'+remaining_item_id).value
	var return_shipping_value=document.getElementById('return_shipping_value_'+remaining_item_id).value
	
	var requested_units=document.getElementById('requested_units_'+remaining_item_id).getAttribute('desired_quantity_replacement')
	var available_units=document.getElementById('available_units_'+remaining_item_id).getAttribute('available_units')
	var replaced_value=parseInt(replacement_value)*parseInt(available_units)
	var replaced_shipping_value=parseInt(replacement_shipping_value)*parseInt(available_units)
	var returned_value=parseInt(return_value)*parseInt(available_units)
	var returned_shipping_value=parseInt(return_shipping_value)*parseInt(available_units)
	var price_difference=(parseInt(returned_value)+parseInt(returned_shipping_value))-(parseInt(replaced_value)+parseInt(replaced_shipping_value));

	var ordered_quantity=document.getElementById('ordered_quantity').getAttribute('ordered_quantity');
	
getting_shipping_charge_for_ReplacedOptions('replace_and_refund',available_units,price_difference);

	var available_unitsmarker=""
	if(available_units >1){
		available_unitsmarker="units";
	}
	if(available_units <2){
		available_unitsmarker="unit";
	}
	var requested_unitsmarker=""
	if(requested_units >1){
		requested_unitsmarker="units";
	}
	if(requested_units <2){
		requested_unitsmarker="unit";
	}
	//customer_payment_flag="";
	var differentila_statement=""
			if(price_difference>0){
				differentila_statement+='<td colspan="2">Differential Amount for '+available_units+' '+available_unitsmarker+' (You Get)</td>\
				<td>'+Math.abs(price_difference)+'</td>'
				document.getElementById('bank_ifsc_code_rep').required=false;
				document.getElementById('bank_account_number_rep').required=false;
				document.getElementById('bank_account_confirm_number_rep').required=false;
				document.getElementById('account_holder_name_rep').required=false;
				document.getElementById('bank_return_refund_phone_number_rep').required=false;
				document.getElementById('save_this_bank_data_rep').required=false;
				
			}
			if(price_difference==0){
				differentila_statement+='<td colspan="2">Balance Squared for '+available_units+' '+available_unitsmarker+'</td><td>NILL</td>'
				document.getElementById('bank_ifsc_code_rep').required=false;
				document.getElementById('bank_account_number_rep').required=false;
				document.getElementById('bank_account_confirm_number_rep').required=false;
				document.getElementById('account_holder_name_rep').required=false;
				document.getElementById('bank_return_refund_phone_number_rep').required=false;
				document.getElementById('save_this_bank_data_rep').required=false;
				
			}
			if(price_difference<0){
				//differentila_statement+='<td colspan="2">Differential Amount to be paid by you</td><td> '+available_units+' '+available_unitsmarker+' </td>\
				differentila_statement+='<td colspan="2">Differential Amount to be paid by you</td><td>'+Math.abs(price_difference)+'</td>'
				document.getElementById('bank_ifsc_code_rep').required=true;
				document.getElementById('bank_account_number_rep').required=true;
				document.getElementById('bank_account_confirm_number_rep').required=true;
				document.getElementById('account_holder_name_rep').required=true;
				document.getElementById('bank_return_refund_phone_number_rep').required=true;
				document.getElementById('save_this_bank_data_rep').required=true;
			}
			
	if(available_units!=0){
		//calculate shipping charge///////
	
	str+='<table class="table table-bordered table-hover"><thead><tr><th>Details</th><th>Differential Amount Calculations</th><th>Value</th><tr></thead><tbody><tr><td>Selected product for Replacement <br><small>('+attr_str+')</small> </td><td>'+replacement_value+' * '+available_units+' '+available_unitsmarker+'</td><td>'+(replacement_value)*parseInt(available_units)+'</td></tr><tr class="xx"><td>Purchased product</td><td>('+return_value+' * '+available_units+' '+available_unitsmarker+')</td><td>'+(return_value)*parseInt(available_units)+'</td></tr><tr class="xx">'+differentila_statement+'</tr></tbody></table>';
	
	if(price_difference<0){
		customer_payment_flag="yes";
		str+='<select name="pay_balance_method" class="form-control" id="pay_balance_method" onchange="remove_error();"><option value="" selected>Select Payment Method</option><option value="cod">Cod</option><option value="wallet">Wallet</option><option value="online_payment">Online Payment</option></select><br>'
	}
		if(!(price_difference<0 && customer_payment_flag=="yes")){
			str+='<div class="customer_payment"></div>';
		}
	}
			remainingUnits=parseInt(requested_units)-parseInt(available_units)
			
			
			var remainingUnits_unitsmarker=""
				if(remainingUnits >1){
					remainingUnits_unitsmarker="units";
				}
				if(remainingUnits <2){
					remainingUnits_unitsmarker="unit";
				}
				
				//alert(remainingUnits);
				
			var refundremaining_amount=parseInt(return_value)*parseInt(remainingUnits);
			//var refundremaining_shipping_amount=parseInt(return_shipping_value)*parseInt(remainingUnits);

			var shipping_price_waived=document.getElementById('shipping_price_waived').value;

			if(shipping_price_waived=="yes"){
				shipping_price_str_2="";
				shipping_charge=0;
				return_shipping_value=0;

			}else{

				shipping_charge=Math.round(parseInt(return_shipping_value)/parseInt(ordered_quantity))*parseInt(remainingUnits);	
			}
			var refundremaining_shipping_amount=shipping_charge;
			
			//alert(price_difference);
			var final_statement=""
			var final_statement_shipping=""
			var final_statement_total=""
			if((parseInt(refundremaining_amount)+parseInt(price_difference))>0){
				
				//document.getElementById('bank_account_data_rep').style.display="";
				//these 2 lines are commented//
				document.getElementById('refund_method_rep_div').style.display="";
				//final_statement+='<td colspan="2">Final Item Amount to be Refunded to You</td><td>'+(parseInt(refundremaining_amount)+parseInt(price_difference))+'</td>';
				
				final_statement_shipping+='<td>Final Shipping Price</td><td> '+Math.round(parseInt(return_shipping_value)/parseInt(ordered_quantity))+'*'+remainingUnits+' unit(s)</td><td>'+(parseInt(shipping_charge))+'</td>';
				
				document.getElementById('bank_ifsc_code_rep').required=false;
				document.getElementById('bank_account_number_rep').required=false;
				document.getElementById('bank_account_confirm_number_rep').required=false;
				document.getElementById('account_holder_name_rep').required=false;
				document.getElementById('bank_return_refund_phone_number_rep').required=false;
				document.getElementById('save_this_bank_data_rep').required=false;
			}
			
			if((parseInt(refundremaining_amount)+parseInt(price_difference))<0){
				document.getElementById('bank_account_data_rep').style.display="none";
				/*final_statement+='<td colspan="2">Final Item Amount to be Paid by You</td>\<td>'+Math.abs(parseInt(refundremaining_amount)+parseInt(price_difference))+'</td>'*/
				final_statement_shipping+='<td>Shipping price to refund </td><td> '+Math.round(parseInt(return_shipping_value)/parseInt(ordered_quantity))+'*'+remainingUnits+' unit(s)</td><td>'+(parseInt(refundremaining_shipping_amount))+'</td>'
				document.getElementById('bank_ifsc_code_rep').required=true;
				document.getElementById('bank_account_number_rep').required=true;
				document.getElementById('bank_account_confirm_number_rep').required=true;
				document.getElementById('account_holder_name_rep').required=true;
				document.getElementById('bank_return_refund_phone_number_rep').required=true;
				document.getElementById('save_this_bank_data_rep').required=true;
			}
			//// shareesh added starts /////
			//total_amount_refunded_to_you=parseInt(parseInt(refundremaining_amount)+parseInt(price_difference))+parseInt(parseInt(refundremaining_shipping_amount));
	
				
			//// shareesh added ends /////
			
			if((parseInt(refundremaining_amount)+parseInt(price_difference))<0){
				flag_pay=0;
				payorget="( You Pay)";
			}
			if((parseInt(refundremaining_amount)+parseInt(price_difference))>0){
				flag_pay=1;
				payorget="( You Get)";
			}
			
			if(parseInt(price_difference)==0){
				payorget="";
			}
			
			//alert(payorget);
			/////////////////////promo_code starts/////////////////////
			
		var promotion_available=parseInt(document.getElementById('promotion_available').value);
		if(promotion_available==1){
		var status_of_refund_for_cashback_on_sku=$("#status_of_refund_for_cashback_on_sku").val();	
		var promotion_minimum_quantity=parseInt(document.getElementById('promotion_minimum_quantity').value);
		var promotion_quote=document.getElementById('promotion_quote').value
		
		var promotion_default_discount_promo=document.getElementById('promotion_default_discount_promo').value;
		
		var promotion_surprise_gift=document.getElementById('promotion_surprise_gift').value;
		var promotion_surprise_gift_type=document.getElementById('promotion_surprise_gift_type').value;
		var promotion_surprise_gift_skus=document.getElementById('promotion_surprise_gift_skus').value;
		var promotion_surprise_gift_skus_nums=document.getElementById('promotion_surprise_gift_skus_nums').value;
		var individual_price_of_product_with_promotion=parseInt(document.getElementById('individual_price_of_product_with_promotion').value);
		var individual_price_of_product_without_promotion=parseInt(document.getElementById('individual_price_of_product_without_promotion').value);
		
		var promotion_cashback=parseInt(document.getElementById('promotion_cashback').value);
		var default_discount=parseInt(document.getElementById('default_discount').value);
		var promotion_discount=parseInt(document.getElementById('promotion_discount').value);
		var cash_back_value=parseInt(document.getElementById('cash_back_value').value);
		var total_price_of_product_with_promotion=parseInt(document.getElementById('total_price_of_product_with_promotion').value);
		var total_price_of_product_without_promotion=parseInt(document.getElementById('total_price_of_product_without_promotion').value) || 0;
		}
		var quotient=0;
		var modulo=0;
		var promo_str="";
		var total_amount=0;

		
		
		if(promotion_available==1){
				
				$("#req_promotion_available").val(promotion_available);
				$("#req_promotion_minimum_quantity").val(promotion_minimum_quantity);
				$("#req_promotion_quote").val(promotion_quote);
				$("#req_promotion_default_discount_promo").val(promotion_default_discount_promo);
				
				$("#req_promotion_surprise_gift").val(promotion_surprise_gift);
				$("#req_promotion_surprise_gift_type").val(promotion_surprise_gift_type);
				$("#req_promotion_surprise_gift_skus").val(promotion_surprise_gift_skus);
				$("#req_promotion_surprise_gift_skus_nums").val(promotion_surprise_gift_skus_nums);
				
				$("#req_promotion_cashback").val(promotion_cashback);
				$("#req_default_discount").val(default_discount);
				$("#req_promotion_discount").val(promotion_discount);
			
			
				$("#req_individual_price_of_product_without_promotion").val('');
				$("#req_total_price_of_product_without_promotion").val('');
				$("#req_quantity_without_promotion").val('');
				$("#req_individual_price_of_product_with_promotion").val('');
				$("#req_total_price_of_product_with_promotion").val('');
				$("#req_quantity_with_promotion").val('');
				$("#req_cash_back_value").val('');
			}
		if(promotion_available==1){
			
			if((promotion_discount!=default_discount) && (promotion_discount>0)){
				promo_discount_available=1;
			}else{
				promo_discount_available=0;
			}
			
			desired_quantity_refund=remainingUnits;
			
			modulo=parseInt(desired_quantity_refund%promotion_minimum_quantity);//remainder
			quotient=Math.floor(parseInt(desired_quantity_refund/promotion_minimum_quantity));//answer
			
			if(ordered_quantity >= promotion_minimum_quantity){

				if(desired_quantity_refund >= promotion_minimum_quantity){
		
					if(promo_discount_available != 1){
						
						if(quotient>0){
							
							indi_price_with_promo=parseInt(individual_price_of_product_with_promotion/quotient);
			
							count_quotient=(quotient)*promotion_minimum_quantity;

							total_price_with_promo=count_quotient*individual_price_of_product_with_promotion;
							
							promo_str+='<tr><td><small style="color:green;"><b>Promotion<br><i>'+promotion_quote+' for quantity ('+count_quotient+')</i></b></small></td><td>'+count_quotient+' * '+individual_price_of_product_with_promotion+'<br></td><td> <?php echo curr_sym;?>'+total_price_with_promo+'</td></tr>';
							total_amount+=parseFloat(total_price_with_promo);
							
							$("#req_individual_price_of_product_with_promotion").val(individual_price_of_product_with_promotion);
							$("#req_total_price_of_product_with_promotion").val(total_price_with_promo);
							$("#req_quantity_with_promotion").val(count_quotient);
						
							if(promotion_cashback>0){
								
								if(status_of_refund_for_cashback_on_sku=="refunded"){
									
									cash_back_value=parseFloat(individual_price_of_product_with_promotion*(promotion_cashback/100)).toFixed(2);
									cash_back_value_with_quotient=parseFloat(quotient*cash_back_value).toFixed(2);
									promo_str+='<tr><td><span style="color:red;">Cashback value for quantity ('+quotient+')</span></td><td>'+quotient+' * '+cash_back_value+'</td><td> <span style="color:red;"> -<?php echo curr_sym;?>'+cash_back_value_with_quotient+'</span></td></tr>';
									total_amount-=parseFloat(cash_back_value_with_quotient);
								}else{
									cash_back_value_with_quotient=0;
								}
								$("#req_cash_back_value").val(cash_back_value_with_quotient);
							}
							
						}

						if(modulo>0){
							//breakup 2
							indi_price_without_promo=parseInt(individual_price_of_product_without_promotion/modulo);
							
							total_price_without_promo=parseInt(modulo*individual_price_of_product_without_promotion);
							
							promo_str+='<tr><td><small style="color:green;"><b>Promotion<br><i>'+promotion_default_discount_promo+' for quantity ('+modulo+')</i></b></small></td><td>'+modulo+' * '+individual_price_of_product_without_promotion+'<br></td><td> <?php echo curr_sym;?>'+total_price_without_promo+'</td></tr>';
							total_amount+=parseFloat(total_price_without_promo);
							
							$("#req_individual_price_of_product_without_promotion").val(individual_price_of_product_without_promotion);
							$("#req_total_price_of_product_without_promotion").val(total_price_without_promo);
							$("#req_quantity_without_promotion").val(modulo);
						}
						
					}
					if(promo_discount_available == 1){
						total_price_of_product=parseFloat(total_price_of_product_with_promotion+total_price_of_product_without_promotion).toFixed(2);
						indi_price_with_promo=parseFloat(total_price_of_product/ordered_quantity).toFixed(2);
						total=parseFloat(indi_price_with_promo*desired_quantity_refund).toFixed(2);
						
						promo_str+='<tr><td>Purchsed Item price</td><td>'+desired_quantity_refund+' * '+indi_price_with_promo+'</td><td><?php echo curr_sym;?>'+total+'</td></tr>';
						total_amount+=parseFloat(total);
						
						$("#req_individual_price_of_product_with_promotion").val(indi_price_with_promo);
						$("#req_total_price_of_product_with_promotion").val(total);
						$("#req_quantity_with_promotion").val(desired_quantity_refund);
					}
					
				}else{
					
					if(promo_discount_available != 1){
						if(modulo>0){
							//breakup 2
							indi_price_without_promo=parseInt(individual_price_of_product_without_promotion/modulo);
							
							total_price_without_promo=parseInt(modulo*individual_price_of_product_without_promotion);
							
							promo_str+='<tr><td><small style="color:green;">Promotion<br><b><i>'+promotion_default_discount_promo+' for quantity ('+modulo+')</i></b></small></td><td>'+modulo+' * '+individual_price_of_product_without_promotion+'<br></td><td> <?php echo curr_sym;?>'+total_price_without_promo+'</td></tr>';
							total_amount+=parseFloat(total_price_without_promo);
							
							$("#req_individual_price_of_product_without_promotion").val(individual_price_of_product_without_promotion);
							$("#req_total_price_of_product_without_promotion").val(total_price_without_promo);
							$("#req_quantity_without_promotion").val(modulo);
						}
					}
					
					if(promo_discount_available == 1){
						total_price_of_product=parseFloat(total_price_of_product_with_promotion+total_price_of_product_without_promotion).toFixed(2);
						indi_price_with_promo=parseFloat(total_price_of_product/ordered_quantity);
						total=parseFloat(indi_price_with_promo*desired_quantity_refund).toFixed(2);
						
						promo_str+='<tr><td>Purchsed Item price</td><td>'+desired_quantity_refund+' * '+indi_price_with_promo+'</td><td><?php echo curr_sym;?>'+total+'</td></tr>';
						total_amount+=parseFloat(total);
						
						$("#req_individual_price_of_product_with_promotion").val(indi_price_with_promo);
						$("#req_total_price_of_product_with_promotion").val(total);
						$("#req_quantity_with_promotion").val(desired_quantity_refund);
					}
				}

			}
			//total_amount=total_amount+parseInt(shipping_charge);

		}else{//promo_available
		
			promo_str+='<tr><td>Item price to be refunded </td><td>'+return_value+' * '+remainingUnits+' '+remainingUnits_unitsmarker+'</td><td>'+parseInt(return_value)*parseInt(remainingUnits)+'</td></tr>';
			
			total_amount=parseInt(parseInt(refundremaining_amount))+parseInt(parseInt(shipping_charge));
		}	
			/////////////////////promo_code ends/////////////////////
	
			if(shipping_price_waived=="yes"){
				shipping_price_str_2='';
			}else{
				shipping_price_str_2='<tr>'+final_statement_shipping+'</tr>';
			}
		
			str_invoice_discount="";		
			if(document.getElementById('order_item_invoice_discount_value')!=null){
				var order_item_invoice_discount_value=parseFloat(document.getElementById('order_item_invoice_discount_value').value) || 0;//"falsey" value to 0
			}else{
				var order_item_invoice_discount_value=0;
			}
			if(promotion_available==0){
				
				total_product_price=(parseInt(return_value)*parseInt(remainingUnits));
				desired_quantity_refund=remainingUnits;
				if(order_item_invoice_discount_value>0){
					each_inv_dis=(Math.round((order_item_invoice_discount_value/ordered_quantity)*100))/100;
					
					inv_discount=parseFloat((each_inv_dis)*desired_quantity_refund).toFixed(2);
					total_product_price-=parseFloat(inv_discount);					
					str_invoice_discount='<tr><td style="color:blue;"> Invoice discount price which has to be deducted for this '+desired_quantity_refund+' '+remainingUnits_unitsmarker+' </td><td>('+desired_quantity_refund+' * '+each_inv_dis+') </td><td style="color:red;">- <?php echo curr_sym;?>'+inv_discount+'</td></tr>';
					
				}
				total_price=total_product_price+parseInt(shipping_charge);
				
				total_amount=parseFloat(total_price).toFixed(2);
			}
			if(promotion_available==1){
			
				if(order_item_invoice_discount_value>0){
					//invoice_discount=
					
					each_inv_dis=Math.round((order_item_invoice_discount_value/ordered_quantity)*100)/100;
					inv_discount=parseFloat((each_inv_dis)*desired_quantity_refund).toFixed(2);
					total_product_price=parseFloat(total_amount-inv_discount).toFixed(2);
					
					str_invoice_discount='<tr><td style="color:blue;"> Invoice discount price which has to be deducted for this '+desired_quantity_refund+' '+remainingUnits_unitsmarker+' </td><td>('+desired_quantity_refund+' * '+each_inv_dis+') </td><td style="color:red;">- <?php echo curr_sym;?>'+inv_discount+'</td></tr>';
					
					total_amount=parseFloat(parseFloat(total_product_price)+parseFloat(shipping_charge)).toFixed(2);
				
				}else{
					total_amount=parseFloat(total_amount)+parseFloat(shipping_charge);	
				}				
				
			}
			
			str+='<table class="table table-bordered table-hover"><thead><tr><th>Details</th><th>Net Amount Calculations </th><th>Value</th></tr></thead><tbody>';
			
			str+=promo_str;
			str+=str_invoice_discount;
			final_statement_total+='<td colspan="2">Final Total Amount to be Refunded to You</td><td> <?php echo curr_sym;?>'+total_amount+'</td>'
			
			str+='<tr>'+final_statement+'</tr>'+shipping_price_str_2+'<tr>'+final_statement_total+'</tr></tbody></table>'
			
			///////////refund table formation///////////////////
	
			str+='<input type="hidden" name="selected_inventory_id_for_rep"  value="'+remaining_item_id+'">';
			str+='<input type="hidden" name="stock_quantity_available_at_replacement" value="'+available_units+'"><input type="hidden" name="stock_quantity_desired_at_replacement" value="'+requested_units+'"><input type="hidden" name="return_value_each" value="'+return_value+'"><input type="hidden" name="replacement_value_each" value="'+replacement_value+'"><input type="hidden" name="return_shipping_value_each" value="'+return_shipping_value+'"><input type="hidden" name="replacement_shipping_value_each" value="'+replacement_shipping_value+'">';
			
			if((parseInt(refundremaining_amount)+parseInt(price_difference))<0 && customer_payment_flag!="yes"){
			//	if((parseInt(refundremaining_amount))>0){
				//alert((parseInt(refundremaining_amount)));
				//alert((parseInt(price_difference)));
				str+='<select name="pay_balance_method" class="form-control" id="pay_balance_method" onchange="remove_error();"><option value="" selected>Select Payment Method</option><option value="cod">Cod</option><option value="wallet">Wallet</option><option value="online_payment">Online Payment</option></select>'
				
				document.getElementById('bank_ifsc_code_rep').required=false;
				document.getElementById('bank_account_number_rep').required=false;
				document.getElementById('bank_account_confirm_number_rep').required=false;
				document.getElementById('account_holder_name_rep').required=false;
				document.getElementById('bank_return_refund_phone_number_rep').required=false;
				document.getElementById('save_this_bank_data_rep').required=false;	
				
			}
			
			if(parseInt(refundremaining_amount)>0){
				if(flag_pay==1){
					$('#replacement_text').show();
				}else{
					$('#replacement_text').hide();
				}
				//alert('inside condition');
				document.getElementById('refund_method_rep_div').style.display="";
			}
			
	document.getElementById('summary_for_replacement_div').innerHTML=str;
	
}

function selected_option(inventory_id,rep_opt_show){
	//alert(rep_opt_show);
	
	$('#selected_option_val_'+inventory_id).attr('checked', true);
	$('#new_piece_options').modal('hide');
	
	if(rep_opt_show==2){
		document.getElementById('rep_options').innerHTML="";
	}else{
		//document.getElementById('rep_options').innerHTML="";
	}
	
}

function showAvailableOptions(){
	
	document.getElementById('bank_account_data_rep').style.display="none";
	document.getElementById('summary_for_replacement_div').innerHTML="";
	document.getElementById('ref_opt_div').innerHTML="";
	document.getElementById('checkedValueForReturnInventoryId').innerHTML="";
	
	var product_id=$('#return_product_id').attr('return_product_id');
	var inventory_id=$('#inventory_id').val();
	var price_paid_perunit=$('#price_paid').attr('price_paid');
	var price_paid_shipping_perunit=$('#price_paid_shipping').attr('price_paid_shipping');
	//var grand_total=$('#price_paid').attr('price_paid');
	var grand_total=$('#grand_total').attr('grand_total');
	var return_product_name=$('#return_product_name').html();
	var desired_quantity_replacement=document.getElementById('desired_quantity_replacement').value.trim();
	
	var free_inventory_available=document.getElementById('free_inventory_available').value.trim();
	
	var promotion_invoice_cash_back='<?php echo (isset($promotion_invoice_cash_back)) ? $promotion_invoice_cash_back : 0; ?>';
	var promotion_invoice_discount='<?php echo (isset($promotion_invoice_discount)) ? $promotion_invoice_discount : 0; ?>';
	var invoice_surprise_gift='<?php echo (isset($invoice_surprise_gift)) ? $invoice_surprise_gift : 0; ?>';
	
	if(document.getElementById('promotion_cashback')){
		var promotion_cashback=$("#promotion_cashback").val();
	}else{
		var promotion_cashback=0;
	}
	
	
	if((promotion_invoice_cash_back!=null && promotion_invoice_cash_back!=0 && promotion_invoice_cash_back!='') || (promotion_invoice_discount!=null && promotion_invoice_discount!=0 && promotion_invoice_discount!='') || (promotion_cashback!=null && promotion_cashback!=0 && promotion_cashback!='') || (invoice_surprise_gift!=null && invoice_surprise_gift!=0 && invoice_surprise_gift!='')){
		var hide_low_price_data=1;
	}else{
		var hide_low_price_data=0;
	}
	
	//alert(order_item_id);
	if(document.getElementById('request_for_refund').hasAttribute('disabled')){
		no_refund=1;
	}else{
		no_refund=0;
	}
	
	//alert(document.getElementById('desired_quantity_replacement').value)
	$('#return_product_name_display').html(return_product_name);
	$.ajax({
		url:"<?php echo base_url()?>Account/show_available_replacement_options",
		type:"post",
		data:"product_id="+product_id+"&desired_quantity_replacement="+desired_quantity_replacement+"&inventory_id="+inventory_id+"&free_inventory_available="+free_inventory_available+"&no_refund="+no_refund,
		dataType:"JSON",
		beforeSend:function(){
			
		},
		success:function(data){
			//alert(data);
			//return false;
			inventory_json=data;
			str="";
			str+='<div class="row hhhh">';
			var j=0;
			if(inventory_json.length==0){
				str+='<div class="col-md-12 alert alert-warning text-center">Stock is not available. Either you can reduce the quantity or Give a Request for Refund.</div>';
			}
			r=0;
			for(i=0;i<inventory_json.length;i++){

				if(inventory_id==inventory_json[i].id){
					//for same product the price per unit should be the price of purchased item.// not consider the discounts
					inventory_json[i].selling_price=$('#purchased_price_per_unit').val();
					inventory_json[i].selling_price=parseFloat(inventory_json[i].selling_price).toFixed(2);
				}else{
					inventory_json[i].selling_price=Math.round(inventory_json[i].selling_price);
					
				}

				if(((hide_low_price_data==1 && inventory_json[i].selling_price>=price_paid_perunit) || (hide_low_price_data==0)) &&((free_inventory_available==1 && inventory_json[i].selling_price>=price_paid_perunit) || (free_inventory_available==0))){
					

					////////
					j=r+1;
					r++;//for displayed items
					var rep_opt_show='';
					if(inventory_json[i].status_quantity<0){
						var rep_opt_show=1;
					}else{
						var rep_opt_show=2;
					}
					
					str+='<div class="col-md-2"><div class="cursor-pointer" onclick="selected_option('+inventory_json[i].id+','+rep_opt_show+')" style="background-color: #eee;"> <input type="hidden" id="selling_price_of_single_id_'+inventory_json[i].id+'" value="'+inventory_json[i].selling_price+'" >';
					
				str+='<div class="f-row"><img src="<?php echo base_url()?>'+inventory_json[i].image+'" class="img-responsive img-thumbnail"></div>';
				
				var val = inventory_json[i].attribute_1_value.split(':');
				
				str+='<div class="f-row margin-top"><input type="hidden" id="rep_attribute_1_'+inventory_json[i].id+'" name="rep_attribute_1_'+inventory_json[i].id+'" value="'+inventory_json[i].attribute_1+'"><input type="hidden" id="rep_attribute_val_1_'+inventory_json[i].id+'" name="rep_attribute_val_1_'+inventory_json[i].id+'" value="'+val[0]+'"><small><label>'+inventory_json[i].attribute_1+'</label>&nbsp;:&nbsp;<b>'+val[0]+'</b></small></div>';
				str+='<div class="f-row"><input type="hidden" id="rep_attribute_2_'+inventory_json[i].id+'" name="rep_attribute_2_'+inventory_json[i].id+'" value="'+inventory_json[i].attribute_2+'"><input type="hidden" id="rep_attribute_val_2_'+inventory_json[i].id+'" name="rep_attribute_val_2_'+inventory_json[i].id+'" value="'+inventory_json[i].attribute_2_value+'"><small><label>'+inventory_json[i].attribute_2+'</label>&nbsp;:&nbsp;<b>'+inventory_json[i].attribute_2_value+'</b></small></div>';
				
				
				if(inventory_id==inventory_json[i].id){
					str+='<div class="text-left"><small><strong>(Purchased Product) </strong></small></div>';
				}
				if(inventory_json[i].stock==0){
					str+='<div class="text-left"><small><strong>(Out Of Stock)</small></strong></div>';
				}
				
				str+='<div class="control-label col-md-12 hide"><label>Price per unit&nbsp; </label>';
				
				str+='<b class="pull-right">'+inventory_json[i].selling_price+'</b>';
				
				str+='</div>';

str+='<input type="hidden" id="original_desired_quantity_'+inventory_json[i].id+'" value="'+parseInt(desired_quantity_replacement)+'">';
str+='<input type="hidden" id="return_available_quantity_'+inventory_json[i].id+'" value="'+inventory_json[i].stock+'">';
str+='<input type="hidden" value="'+inventory_json[i].selling_price+'" id="replacement_value_'+inventory_json[i].id+'">'
str+='<input type="hidden" value="'+price_paid_perunit+'" id="return_value_'+inventory_json[i].id+'">'

str+='<input type="hidden" value="'+price_paid_shipping_perunit+'" id="replacement_shipping_value_'+inventory_json[i].id+'">'				
str+='<input type="hidden" value="'+price_paid_shipping_perunit+'" id="return_shipping_value_'+inventory_json[i].id+'">'

				if(inventory_json[i].status_quantity<0){
					
						str+='<div class="control-label col-md-12 hide" style="font-size:.8em;border-top:1px solid red;border-bottom:1px solid red;margin-bottom:5px"><p><del>Requested Units: <b class="text_red" desired_quantity_replacement='+desired_quantity_replacement+' id="requested_units_'+inventory_json[i].id+'">'+parseInt(desired_quantity_replacement)+' units</b></del></p>';
						
						desired_quantity_replacementn=inventory_json[i].stock;
						// when stock is 0 then take the desired replacement quantity
						if(desired_quantity_replacementn==0){
							desired_quantity_replacementn=desired_quantity_replacement;
						}
						str+='<p><label>Available Units:</label><b class="text_red" available_units='+inventory_json[i].stock+' id="available_units_'+inventory_json[i].id+'">'+parseInt(inventory_json[i].stock)+' units</b></p><p><label>Return Value:</label><b> '+((price_paid_perunit)*parseInt(desired_quantity_replacementn))+'</b></p><p><label>Replacement Value :</label><b> '+(inventory_json[i].selling_price)*parseInt(desired_quantity_replacementn)+'</b></p></div>';
						
					var replacement_value=((price_paid_perunit)*parseInt(desired_quantity_replacementn));
					var returned_value=((inventory_json[i].selling_price)*parseInt(desired_quantity_replacementn))
				
				
						if(replacement_value-returned_value>0){

							str+='<button type="button" class="btn-block btn btn-success exchange_button hide" >You Get:<b> '+parseInt(replacement_value-returned_value)+'</b></button>';
						}
						if(replacement_value-returned_value<0){

							str+='<button type="button" class="btn-block btn btn-danger exchange_button hide">You Pay: <b> '+parseInt(returned_value-replacement_value)+'</b></button>';
						}
						if(replacement_value-returned_value==0){
							str+='<button type="button" class="btn-block btn btn-info exchange_button hide">Balance Squared: </button>';
						}

				}else{
						str+='<div class="control-label col-md-12 hide" style="font-size:.8em;border-top:1px solid red;border-bottom:1px solid red;margin-bottom:5px"><p><label>Return Units:</label><b class="text_red">'+parseInt(desired_quantity_replacement)+' units</b></p><p><label>Return Value:</label><b> '+((price_paid_perunit)*parseInt(desired_quantity_replacement))+'</b></p><p><label>Replacement Value :</label><b> '+(inventory_json[i].selling_price)*parseInt(desired_quantity_replacement)+'</b></p></div>';
						
						var replacement_value=((price_paid_perunit)*parseInt(desired_quantity_replacement));
						var returned_value=(inventory_json[i].selling_price*parseInt(desired_quantity_replacement))
						
						if(replacement_value-returned_value>0){
							str+='<button type="button" class="btn-block btn btn-success exchange_button hide">You Get:<b> '+parseInt(replacement_value-returned_value)+'</b></button>';
						}
						if(replacement_value-returned_value<0){
							str+='<button type="button" class="btn-block btn btn-danger exchange_button hide">You Pay: <b> '+parseInt(returned_value-replacement_value)+'</b></button>';
						}
						if(replacement_value-returned_value==0){
							str+='<button type="button" class="btn-block btn btn-info exchange_button hide">Balance Squared:</button>';
						}
				
				}
				
				str+='</div>';
				
				str+='<div class="col-md-12 hide"><span data-dismiss="modal" aria-hidden="true"><input type="checkbox" name="replacement_select" inventory_id="'+inventory_json[i].id+'" id="selected_option_val_'+inventory_json[i].id+'" value="'+inventory_json[i].id+'"></span>Select</div>';
				
				str+='</div>';
				
				//alert(replacement_value);alert(returned_value);alert(replacement_value-returned_value); return false;
				replacement_value=0;
				returned_value=0;
				//////
					
				}//checking price
				
				if(j%6==0){
					str+='</div>';
					str+='<div class="row hhhh">';
				}
			
			}//forloop
			str+='</div>';
			document.getElementById('refund_method_rep_div').style.display="none";
			$("#available_options_row").html(str);
		}
	})
}
	function free_surprise_gift_fun(order_id,status){
			
		$.ajax({
			type:'POST',
			url:'<?php echo base_url(); ?>Account/update_customer_status_surprise_gifts_on_invoice',
			data:"order_id="+order_id+"&status="+status,
			success:function(data){
				
				/*bootbox.alert({
				  size: "small",
				  message: 'successfully submited',
				  callback: function () { location.reload(); }
				});*/

                alert('successfully submited');
				location.reload();
			}
		});
	}

	function invoice_cash_back_fun(order_id,status,total_amount_cash_back=''){
		
		$.ajax({
			type:'POST',
			url:'<?php echo base_url(); ?>Account/update_customer_response_for_invoice_cashback',
			data:"order_id="+order_id+"&status="+status+"&total_amount_cash_back="+total_amount_cash_back,
			success:function(data){
				
				/*bootbox.alert({
				  size: "small",
				  message: 'successfully submited',
				  callback: function () { location.reload(); }
				});*/
                alert('successfully submited');
				location.reload();
			}
		});
	}


	function choose_sub_issueFun(reason_val){
		
		$('#return_reason').css({"border": "1px solid #ccc"});
		
		$.ajax({
			url:'<?php echo base_url();?>Account/get_sub_reasons',
			type:'POST',
			data:"reason_id="+reason_val,
			dataType:"html",
			success:function(data){	
				
				if(data!=''){
					$('#issue_reason').attr("disabled", false);
					html_str='<option value="">Please Choose</option>'+data;
				}else{
					html_str='<option value="">Please Choose</option>';
					$('#issue_reason').attr("disabled", true);
				}
				
				$('#issue_reason').html(html_str);
				
				return false;
			}
			
		})
		
		/*if(reason_val==1){
			document.getElementById("sub_issue_div").style.display="";
			document.getElementById('issue_reason').required = true;
		}
		else{
			document.getElementById("issue_reason").value="";
			document.getElementById("sub_issue_div").style.display="none";
			document.getElementById('issue_reason').required = false;
		}*/
	}
            			
	function use_this_account_data(obj){
	
		document.getElementById('errors').style.display = 'none';
		document.getElementById('errors1').style.display = 'none';
		document.getElementById('errors2').style.display = 'none';
		document.getElementById('errors3').style.display = 'none';
		document.getElementById('add_new_bank_details_button').style.display = '';
		var bank_name=document.getElementById('bank_name_'+obj).innerHTML
		var branch_name=document.getElementById('branch_name_'+obj).innerHTML
		var city=document.getElementById('city_'+obj).innerHTML
		var ifsc=document.getElementById('ifsc_'+obj).innerHTML
		var account_number=document.getElementById('account_number_'+obj).innerHTML
		var account_holder_name=document.getElementById('account_holder_name_'+obj).innerHTML
		var account_holder_numer=document.getElementById('account_holder_numer_'+obj).innerHTML
		
		document.getElementById('bank_name').value=bank_name;
		document.getElementById('bank_branch_name').value=branch_name;
		document.getElementById('bank_city').value=city;
		document.getElementById('bank_ifsc_code').value=ifsc;
		document.getElementById('bank_account_number').value=account_number;
		document.getElementById('bank_account_confirm_number').value=account_number;
		document.getElementById('account_holder_name').value=account_holder_name;
		document.getElementById('bank_return_refund_phone_number').value=account_holder_numer;
		document.getElementById('bank_name').setAttribute('readOnly',true);
		document.getElementById('bank_branch_name').setAttribute('readOnly',true);
		document.getElementById('bank_city').setAttribute('readOnly',true);
		document.getElementById('bank_ifsc_code').setAttribute('readOnly',true);
		document.getElementById('bank_account_number').setAttribute('readOnly',true);
		document.getElementById('bank_account_confirm_number').setAttribute('readOnly',true);
		document.getElementById('account_holder_name').setAttribute('readOnly',true);
		document.getElementById('bank_return_refund_phone_number').setAttribute('readOnly',true);
		document.getElementById('save_this_bank_data').disabled=true;
		$('#all_bank_accounts').modal('hide');
		$('#add_new_bank_details_button').show();
	}
	function add_new_bank_data(){
		document.getElementById('bank_name').value="";
		document.getElementById('bank_branch_name').value="";
		document.getElementById('bank_city').value="";
		document.getElementById('bank_ifsc_code').value="";
		document.getElementById('bank_account_number').value="";
		document.getElementById('bank_account_confirm_number').value="";
		document.getElementById('account_holder_name').value="";
		document.getElementById('bank_return_refund_phone_number').value="";
		document.getElementById('bank_name').readOnly=false;
		document.getElementById('bank_branch_name').readOnly=false;
		document.getElementById('bank_city').readOnly=false;
		document.getElementById('bank_ifsc_code').readOnly=false;
		document.getElementById('bank_account_number').readOnly=false;
		document.getElementById('bank_account_confirm_number').readOnly=false;
		document.getElementById('account_holder_name').readOnly=false;
		document.getElementById('bank_return_refund_phone_number').readOnly=false;
		
		det_count=$('#saved_bank_details').val().trim();
		if(det_count<3){
			document.getElementById('save_this_bank_data').disabled=false;
		}
	}

</script>							