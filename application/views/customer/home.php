<div class="columns-container">
    <div class="container" id="columns">
        <ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item active">My account</li>
		</ol>
        <div class="row">
			<?php require_once 'leftcolum.php';?>
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
				<div class="panel panel-default">
    <div class="panel-heading">Personal Information</div>
    <div class="panel-body">
		
				<form id="personal_info" action="<?php echo base_url()?>Account/update_basic_info" method="post" class="form-horizontal">
            				<div class="form-group">
							 <label for="name" class="control-label col-sm-3">Full Name</label>
							<div class="col-sm-6">
							    <input type="text" class="form-control" name="name" value="<?php echo $this->session->userdata("customer_name")?>">
							</div>
							</div>
							
							<div class="form-group">
							<label for="name" class="control-label col-sm-3">Gender</label>
							<div class="col-sm-6">
							  <select class="form-control" name="gender">
							    <option value="male" <?php if($this->session->userdata("customer_gender")=="male"){echo "selected";}?>>Male</option>
							    <option value="female" <?php if($this->session->userdata("customer_gender")=="female"){echo "selected";}?>>Female</option>
							  </select>
							</div>
							</div>

							<?php if(!empty($cust_info)){
								?>

<div class="form-group">
								<label class="control-label col-md-3">Door Address:</label>
								<div class="col-md-6">
								<input type="text" required class="form-control" name="address1" value="<?php echo $cust_info->address1;?>" maxlength="20">
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Street Address:</label>
								<div class="col-md-6">
								<input type="text" required class="form-control" name="address2" value="<?php echo $cust_info->address2;?>" maxlength="20">
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">City:</label>
								<div class="col-md-6">
								<input type="text" required class="form-control" name="city" value="<?php echo $cust_info->city;?>" maxlength="20">
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">State/Province:</label>
								<div class="col-md-6">
								<input type="text" required class="form-control" name="state" value="<?php echo $cust_info->state;?>" maxlength="20">
								
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Pin/Zip Code:</label>
								<div class="col-md-6">
								<input type="number" onblur="check_pin_number()" onkeyup="check_pin_number()" required class="form-control" id="pin" name="pin" value="<?php echo $cust_info->pincode;?>" maxlength="6">
								<span class="help-block" id="pinerrormessage"></span>
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Country:</label>
								<div class="col-md-6">
								<input type="text" required class="form-control" name="country" value="India" readonly>
							</div>
							</div>

								<?php 
							} ?>
							<div class="form-group">
							<div class="col-sm-6 col-sm-offset-3">
							  <button type="submit" class="button btn-block preventDflt"> Submit </button>
							</div>
							</div>
            			</div>
				</form>

					</div>	
				</div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

<script>
	var check_pin_number_err=0;
	function check_pin_number(){
		var mobile=document.getElementById('pin').value;
		if(mobile.match(/^[0-9]{6}$/)){
			document.getElementById('pinerrormessage').innerHTML='';
			check_pin_number_err=0;
		}
		else{
			document.getElementById('pinerrormessage').innerHTML='<small class="text_red">pin number contain 6 digits</small>';
			check_pin_number_err=1;
		}
	}
    $('form#personal_info').submit(function(){
        $(this).find(':button[type=submit]').prop('disabled', true);
    });
</script>