<?php
$wishlist_products_arr=$controller->get_all_products_from_wishlist();
$rand=$controller->sample_code();
 ?>
<div class="columns-container">
    <div class="container" id="columns">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account">My account</a></li>
		  <li class="breadcrumb-item active">My Wishlist</li>
		</ol>
        <div class="row">
			<?php require_once 'leftcolum.php';?>
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
				<div class="panel panel-default">
				<div class="panel-heading lead">My Wishlist (<?php echo count($wishlist_products_arr); ?>)</div>
				<div class="panel-body">

				<ul class="row product-list list">
				<?php 
				
				/*echo '<pre>';
				print_r($wishlist_products_arr);
				echo '</pre>';
				*/
				foreach($wishlist_products_arr as $wishlist_products_k=>$wishlist_products_v ){ ?>
				
				
                    <li class="col-sx-12 col-sm-4">
						
						<div class="product-container selectProduct">
                                <div class="left-block text-center">
																		
                                    <a href="<?php echo base_url()?>detail/<?php echo "{$rand}{$wishlist_products_v['inventory_id']}";?>">
									<img src="<?php echo base_url().$wishlist_products_v["product_image"];?>" alt="Product" >
									</a>
									                                   
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name">
									<a href="<?php echo base_url()?>detail/<?php echo "{$rand}{$wishlist_products_v['inventory_id']}";?>"><?php echo $wishlist_products_v["product_name"];?></small></a>
									</h5>
                    		

                                    <div class="content_price">
										<span class="price product-price"><?php echo curr_sym;?><?php echo $wishlist_products_v['selling_price']; ?></span>
                                    </div>
                                    <div class="info-orther">
                                        <p>Item Code: #<?php echo $wishlist_products_v['sku_id']; ?></p>
                                        <p class="availability">Availability: <span><?php echo $wishlist_products_v['product_status']; ?></span></p>
                                        <div class="product-desc">
                                        </div>
                                    </div>
									<div class="info-orther">
									<button class="button btn-sm preventDflt" id="btn_wish_<?php echo $wishlist_products_v['w_id']; ?>" onclick="wishlistRemoveFun(<?php echo $wishlist_products_v['w_id']; ?>)">Remove</button>
									</div>
                                </div>
                        </div>
					</li>
					
					
				<?php
                  }
				
				?>  
                   
                </ul>
				
				</div>
			    </div>
               
    
            </div>
        </div>
    </div>
</div>
<script>
function wishlistRemoveFun(wishlist_id){

    $("#btn_wish_"+wishlist_id).html('<i class="fa fa-spin fa-refresh"></i> Processing');
    document.getElementById("btn_wish_"+wishlist_id).disabled = true;

	$.ajax({
		url:"<?php echo base_url()?>Account/remove_wishlist",
		type:"post",
		data:"wishlist_id="+wishlist_id,
		success:function(data){
			if(data){
				alert('Removed successfully');
				location.reload();
			}else{
				alert('Not yet removed');
				location.reload();
			}
            $("#btn_wish_"+wishlist_id).html('Remove');
            document.getElementById("btn_wish_"+wishlist_id).disabled = false;
		}
	})
	
}
</script>