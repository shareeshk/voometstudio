<div class="columns-container">
    <div class="container" id="columns">
       <ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account/Addresses">Addresses</a></li>
		  <li class="breadcrumb-item active">Edit the Address</li>
		</ol>
        <div class="row">
			<?php require_once 'leftcolum.php';?>
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
			<div class="panel panel-info">
			  <div class="panel-heading">Edit the Address</div>
			  <div class="panel-body">
			  <?php if(!empty($this_address)){
							foreach($this_address as $address){ ?>
					<form id="addressForm" action="<?php echo base_url()?>Account/save_edit_address/<?php echo $address['customer_address_id'];?>" method="post" class="form-horizontal">
							<div class="form-group">
								<label class="control-label col-md-3">Customer Name</label>
								<div class="col-md-6">
								<input type="text" required class="form-control" name="name" value="<?php echo $address['customer_name'];?>" >
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Mobile Number</label>
								<div class="col-md-6">
								<input type="number" required onblur="check_mobile_number()" onkeyup="check_mobile_number()" id="mobile" class="form-control" name="mobile" value="<?php echo $address['mobile'];?>">
								<span class="help-block" id="mobileerrormessage"></span>
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Door Address</label>
								<div class="col-md-6">
								<input type="text" required class="form-control" name="door_address" value="<?php echo $address['address1'];?>">
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Street Address</label>
								<div class="col-md-6">
								<input type="text" required class="form-control" name="street_address" value="<?php echo $address['address2'];?>">
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">City</label>
								<div class="col-md-6">
								<input type="text" required class="form-control" name="city" value="<?php echo $address['city'];?>">
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">State/Province</label>
								<div class="col-md-6">
								
								<select required class="form-control" name="state" value="<?php echo $address['state'];?>">
								<option value="">-Select-</option>
								<?php
									foreach($get_all_states_arr as $arr){
										?>
										<option value="<?php echo $arr["state_name"];?>" <?php if(strtolower($address['state'])==strtolower($arr["state_name"])){echo "selected";}?>><?php echo $arr["state_name"];?></option>
										<?php
									}
								?>
								</select>
								
								
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Pin/Zip Code</label>
								<div class="col-md-6">
								<input type="number" onblur="check_pin_number()" onkeyup="check_pin_number()" required class="form-control" id="pin" name="pin" value="<?php echo $address['pincode'];?>">
								<span class="help-block" id="pinerrormessage"></span>
							</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Country</label>
								<div class="col-md-6">
								<input type="text" required class="form-control" name="country" value="<?php echo $address['country'];?>">
							</div>
							</div>
							<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
							<div class="checkbox">
								<label><input type="checkbox" name="make_default" <?php if($address['make_default']==1){ echo "checked";}?> value="1"> Make Default Address</label>
							</div>
							</div>
							</div>
							<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
							<button type="submit" id="submit-btn" class="button">Submit</button> <button type="reset" class="button preventDflt">Reset</button> <a href="<?php echo base_url()?>Account/Addresses"><button type="button" class="button preventDflt">Cancel</button></a>
							</div>
							</div>
            		</form>
            				<?php }
						} ?>
			  </div>
			</div>
				</div>
        </div>
    </div>
</div>
<script>

	
	function check_mobile_number(){
		var mobile=document.getElementById('mobile').value;
		if(mobile.match(/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/) && ! (mobile.match(/0{7,}/)) ){
			document.getElementById('mobileerrormessage').innerHTML='';
		}
		else{
			document.getElementById('mobileerrormessage').innerHTML='<small class="text_red">mobile number may start only with either 7, 8 or 9 and contain 10 digits without country codes</small>';
		}
	}
	
	function check_pin_number(){
		var mobile=document.getElementById('pin').value;
		if(mobile.match(/^[0-9]{6}$/)){
			document.getElementById('pinerrormessage').innerHTML='';
		}
		else{
			document.getElementById('pinerrormessage').innerHTML='<small class="text_red">pin number contain 6 digits</small>';
		}
	}
    $('form#addressForm').submit(function(){
        $(this).find(':button[type=submit]').prop('disabled', true);
    });
</script>