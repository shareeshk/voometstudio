<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 
<script  type="text/javascript" src="<?php echo base_url();?>assets/richte/editor.js" ></script>var
<script>
$(document).ready(function() {
	//var textarea = $("#project_requirements");
	//textarea.Editor();
	
 });
 
 </script>
<div class="columns-container">
    <div class="container" id="columns">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account/franchise_projects">Project</a></li>
		  <li class="breadcrumb-item active">Update Project Details</li>
		</ol>
        <div class="row">
            <!-- Left colunm -->
			<?php require_once 'leftcolum.php';?>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
				<div class="well">
				<?php
					$project_detail=$project_detail;
				?>
            	<p class="lead">Update Project Details</p><hr>
						<form id="update_project_details" method="post" class="form-horizontal"  enctype="multipart/form-data">
						<input  type="hidden" name="project_id" value="<?php echo $project_detail['project_id']?>" >
						
						
								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Company Name / Project Name</label>  
								  <div class="col-md-6">
								  <input id="project_name" name="project_name" type="text" placeholder="Enter Project Name" class="form-control input-md "  value="<?php echo $project_detail['project_name'];?>" maxlength="20" required> 
								  <span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Client Name</label>  
								  <div class="col-md-6">
								  <input id="client_name" name="client_name" type="text" placeholder="Enter Client Name" class="form-control input-md "  value="<?php echo $project_detail['client_name']?>" maxlength="20" required> 
								  <span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Client Email</label>  
								  <div class="col-md-6">
								  <input id="client_email" name="client_email" type="text" placeholder="Enter Client Email" class="form-control input-md "  value="<?php echo $project_detail['client_email']?>" maxlength="20" required> 
								  <span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Client Mobile</label>  
								  <div class="col-md-6">
								  <input id="client_mobile" name="client_mobile" type="text" placeholder="Enter Client Mobile" class="form-control input-md "  value="<?php echo $project_detail['client_mobile']?>" maxlength="10" onkeypress="return isNumber(event)" maxlength="20" required> 
								  <span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Project Location</label>  
								  <div class="col-md-6">
								  <input id="project_location" name="project_location" type="text" placeholder="Enter Project location" class="form-control input-md "  value="<?php echo $project_detail['project_location']?>" maxlength="20" required> 
								  <span class="help-block"></span>   
								  </div>
								</div>
								

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Type</label>  
								  <div class="col-md-6">
								  <select id="project_type" name="project_type" type="text" class="form-control input-md" required>
								
								<option value=''>Select Type </option>
								<option value='home interiors' <?php  echo ($project_detail['project_type']=='home interiors') ? 'selected' : ''; ?> >Home Interiors</option>
								<option value='kitchen' <?php  echo ($project_detail['project_type']=='kitchen') ? 'selected' : ''; ?>>Kitchen</option>
								</select>

								  <span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Project file</label>  
								  <div class="col-md-6">
								  <input id="project_file" name="project_file" type="file" accept="image/png, image/jpeg, image/bmp,.pdf,text/plain,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.csv,.xlsx, .xls,.txt"> 
								  <span class="help-block"></span>   
								  

								  <!---old file--->

								  <input type="hidden" name="old_project_file" value="<?php echo $project_detail['project_file']; ?>">

								  
							 <?php 
							 if($project_detail['project_file']!=''){

								if(file_exists($project_detail['project_file'])){
								?>
							<br>
							<u class="text-info"><a href="<?php echo base_url().$project_detail['project_file'] ?>" target="_blank"><i class="fa fa-paperclip" aria-hidden="true"></i> Uploaded Attachment</a></u>

								<?php 
							 	}
							}
							 ?>
								  <!---old file--->
								 </div>

								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Project Requirements</label>  
								  <div class="col-md-6">
								  <textarea id="project_requirements" name="project_requirements" type="text" placeholder="Enter Project Requirements" class="form-control input-md"><?php echo $project_detail['project_requirements']?></textarea>
								  <span class="help-block"></span>   
								  </div>
								</div>


								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Project Deadline</label>  
								  <div class="col-md-6">
								  <input id="project_deadline" name="project_deadline" type="type" placeholder="Enter Project Deadline" class="form-control input-md"  value="<?php echo ($project_detail['project_deadline']!='') ? date('d-m-Y',strtotime($project_detail['project_deadline'])) : ''; ?>" maxlength="10" required> 
								  <span class="help-block"></span>
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Project Tentative Budget (Rs.)</label>  
								  <div class="col-md-6">
								  <input id="project_tentative_budget" name="project_tentative_budget" type="text" placeholder="Enter Project Budget" class="form-control input-md "  value="<?php echo $project_detail['project_tentative_budget']?>" maxlength="10" onkeypress="return isNumber(event)" required > 
								  <span class="help-block"></span>
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Project Status</label>  
								  <div class="col-md-6">
									<select id="project_status" name="project_status" type="text" class="form-control input-md" maxlength="20" required>
									
										<option value=''>Select Status </option>
										<option value='Pending review from VS' <?php echo ($project_detail['project_status']=='Pending review from VS') ? 'selected' : ''; ?> >Pending review from VS</option>
										<option value='Approved' <?php echo ($project_detail['project_status']=='Approved') ? 'selected' : ''; ?>>Approved</option>
										<option value='Work-inprogress' <?php echo ($project_detail['project_status']=='Work-inprogress') ? 'selected' : ''; ?>>Work-inprogress</option>
										<option value='Completed' <?php echo ($project_detail['project_status']=='Completed') ? 'selected' : ''; ?>>Completed</option>
										<option value='Signedoff' <?php echo ($project_detail['project_status']=='Signedoff') ? 'selected' : ''; ?>>Signed-off</option>
										<option value='Payment Received' <?php echo ($project_detail['project_status']=='Payment Received') ? 'selected' : ''; ?>>Payment Received</option>
									</select>
									<span class="help-block"></span>   
								  </div>
								</div>
								
		
								<div class="form-group">
								 
								  <div class="col-md-6 col-md-offset-3">
										<input type="submit" class="button btn-block preventDflt" id="submit_btn" value="Submit">
								  </div>
								</div>	            			
								

					</form>
					
            	</div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<script>

$(document).ready(function () {
 $('#project_deadline').datepicker({
	dateFormat: 'dd-mm-yy' 
 });
 
	var textarea = $("#project_requirements");
	textarea.Editor();
	str='<?php echo $project_detail['project_requirements']; ?>';
	data=decodeHtml(str);
	$('#project_requirements').siblings("div").children("div.Editor-editor").html(data);

});
function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

var form = $('#update_project_details');
form.submit(function(event){

	//alert('form inside');
	
	var data = $("#project_requirements").Editor("getText");
	$("#project_requirements").val(data);
	var text = $('textarea[name="project_requirements"]').val().trim();



	$("#submit_btn").html('<i class="fa fa-spin fa-refresh"></i> Processing');
	document.getElementById("submit_btn").disabled = true;

	$.ajax({
		url:"<?php echo base_url()?>Account/update_project_details_action",
		type:"POST",
		data: new FormData(this),
		dataType: 'json',
		contentType: false,      
		cache: false,				
		processData:false,
		beforeSend: function(){
			$("#submit_btn").html('<i class="fa fa-refresh fa-spin"></i> Processing');
		}
	}).done(function(res){
						$("#submit_btn").html('Submit');
						document.getElementById("submit_btn").disabled = false;
						//alert(res);
						if(res.status==true){
							swal({
								title:"Success", 
								text:"Successfully updated!", 
								type: "success",
								allowOutsideClick: false
							}).then(function () {
								//location.reload();
								location.href="<?php echo base_url()?>Account/franchise_projects";

							});
						}else{
							alert('Not updated') //no fields
							location.reload();
						}
					});
		
		
});
function update_project_detailsFun(){

	var data = $("#project_requirements").Editor("getText");
	$("#project_requirements").val(data);

	$("#submit_btn").html('<i class="fa fa-spin fa-refresh"></i> Processing');
	document.getElementById("submit_btn").disabled = true;

	$.ajax({
		url:"<?php echo base_url()?>Account/update_project_details_action",
		type:"POST",
		data:$("#update_project_details").serialize(),
		success:function(data){
			//alert(data);
			if(data){
				
				/*bootbox.alert({
					  size: "small",
					  message: 'Updated successfully',
					  callback: function () { location.href="<?php echo base_url()?>Account/bank"; }
					  
					});*/

                alert('Updated successfully');
                location.href="<?php echo base_url()?>Account/franchise_projects";
			}else{
				/*bootbox.alert({
					  size: "small",
					  message: 'Not yet updated',
					  callback: function () { location.reload(); }fr
					});*/

                alert('Not yet updated');
				$("#submit_btn").html('Submit');
	            document.getElementById("submit_btn").disabled = false;
                location.reload();
            }
		}
	})
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
        return false;
    }
    return true;
}
</script>