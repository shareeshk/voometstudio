<link href="<?php echo base_url();?>assets/richte/editor.css" rel="stylesheet" /> 
<script  type="text/javascript" src="<?php echo base_url();?>assets/richte/editor.js" ></script>
<script>
$(document).ready(function() {
	var textarea = $("#project_requirements");
	textarea.Editor();
	
 });
 
 </script>

<div class="columns-container">
    <div class="container" id="columns">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account/franchise_projects">Project</a></li>
		  <li class="breadcrumb-item active">Update Project Details</li>
		</ol>
        <div class="row">
            <!-- Left colunm -->
			<?php require_once 'leftcolum.php';?>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
				<div class="well">
				
            	<p class="lead">Create Project </p><hr>
						<form id="create_project_details" method="post" class="form-horizontal"  enctype="multipart/form-data">
						
						
								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Company Name / Project Name</label>  
								  <div class="col-md-6">
								  <input id="project_name" name="project_name" type="text" placeholder="Enter Project Name" class="form-control input-md "  value="" maxlength="20" required> 
								  <span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Client Name</label>  
								  <div class="col-md-6">
								  <input id="client_name" name="client_name" type="text" placeholder="Enter Client Name" class="form-control input-md "  value="" maxlength="20" required> 
								  <span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Client Email</label>  
								  <div class="col-md-6">
								  <input id="client_email" name="client_email" type="text" placeholder="Enter Client Email" class="form-control input-md "  value="" maxlength="30" required> 
								  <span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Client Mobile</label>  
								  <div class="col-md-6">
								  <input id="client_mobile" name="client_mobile" type="text" placeholder="Enter Client Mobile" class="form-control input-md "  value="" maxlength="10" onkeypress="return isNumber(event)" required> 
								  <span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Project Location</label>  
								  <div class="col-md-6">
								  <input id="project_location" name="project_location" type="text" placeholder="Enter Project location" class="form-control input-md "  value="" maxlength="20" required> 
								  <span class="help-block"></span>   
								  </div>
								</div>
								

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Type</label>  
								  <div class="col-md-6">
								  <select id="project_type" name="project_type" type="text" class="form-control input-md" required>
								
								<option value=''>Select Type </option>
								<option value='home interiors'  >Home Interiors</option>
								<option value='kitchen' >Kitchen</option>
								</select>

								  <span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Project file</label>  
								  <div class="col-md-6">
								  <input id="project_file" name="project_file" type="file" accept="image/png, image/jpeg, image/bmp,.pdf,text/plain,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.csv,.xlsx, .xls,.txt"> 
								  <span class="help-block"></span>   
								  </div>
								 </div>
								<!-- -->
								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Project Requirements</label>  
								  <div class="col-md-6">
								  <textarea id="project_requirements" name="project_requirements" type="text" placeholder="Enter Project Requirements" class="form-control input-md"></textarea>
								  <span class="help-block"></span>   
								  </div>
								</div>


								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Project Deadline</label>  
								  <div class="col-md-6">
								  <input id="project_deadline" name="project_deadline" type="text" placeholder="Enter Project Deadline" class="form-control input-md"  value="" maxlength="10" required> 
								  <span class="help-block"></span>
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Project Tentative Budget (Rs.)</label>  
								  <div class="col-md-6">
								  <input id="project_tentative_budget" name="project_tentative_budget" type="text" placeholder="Enter Project Budget" class="form-control input-md "  value="" maxlength="10" onkeypress="return isNumber(event)" required> 
								  <span class="help-block"></span>
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Project Status</label>  
								  <div class="col-md-6">
									<select id="project_status" name="project_status" type="text" class="form-control input-md" required>
									
										<option value=''>Select Status </option>
										<option value='Pending review from VS'>Pending review from VS</option>
										<option value='Approved'>Approved</option>
										<option value='Work-inprogress'>Work-inprogress</option>
										<option value='Completed'>Completed</option>
										<option value='Signedoff'>Signed-off</option>
										<option value='Payment Received'>Payment Received</option>
									</select>
									<span class="help-block"></span>   
								  </div>
								</div>
								
		
								<div class="form-group">
								 
								  <div class="col-md-6 col-md-offset-3">
										<input id="submit_btn" type="submit" class="button btn-block preventDflt" value="Submit">
								  </div>
								</div>	            			
								

					</form>
					
            	</div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<script>

$(document).ready(function () {
	$('#project_deadline').datepicker({
		dateFormat: 'dd-mm-yy' 
 	});
});

var form = $('#create_project_details');
form.submit(function(event){

	//alert('form inside');
	
	var data = $("#project_requirements").Editor("getText");
	$("#project_requirements").val(data);
	var text = $('textarea[name="project_requirements"]').val().trim();



	$("#submit_btn").html('<i class="fa fa-spin fa-refresh"></i> Processing');
	document.getElementById("submit_btn").disabled = true;

	$.ajax({
		url:"<?php echo base_url()?>Account/create_project_details_action",
		type:"POST",
		data: new FormData(this),
		dataType: 'json',
		contentType: false,      
		cache: false,				
		processData:false,
		beforeSend: function(){
			$("#submit_btn").html('<i class="fa fa-refresh fa-spin"></i> Processing');
		}
	}).done(function(res){
						$("#submit_btn").html('Submit');
					//	alert(res);
						if(res.status==true){
							swal({
								title:"Success", 
								text:"Successfully created!", 
								type: "success",
								allowOutsideClick: false
							}).then(function () {
								//location.reload();
								location.href="<?php echo base_url()?>Account/franchise_projects";

							});
						}else{
							alert('Not created')
						}
					});
		
		
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
        return false;
    }
    return true;
}
</script>