<style>
#current_email_display{
	margin-bottom:20px;
}
</style>
<div class="columns-container">
    <div class="container" id="columns">
	<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account">My account</a></li>
		  <li class="breadcrumb-item active">Login Credentials</li>
		</ol>
        <div class="row">
			<?php require_once 'leftcolum.php';?>
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <?php
				//print_r($login_credentials);
                	foreach($login_credentials as $credentials){
                		$email=$credentials['email'];
						$email_v=$credentials['email_verification'];
						$mobile=$credentials['mobile'];
						$mobile_v=$credentials['mobile_verification'];
                	}
                 ?>
				 <input type="hidden" name="current_email" id="current_email" value="<?php echo $email; ?>">
				<input type="hidden" name="current_mobile" id="current_mobile" value="<?php echo $mobile; ?>">
				<div class="well">
				<p class="lead">Update Email/Mobile</p>
				<hr>
            				<div class="row" id="current_email_display">
							<div class="col-md-6">
							<strong>Email: </strong><?php echo $email; ?>
							
							</div>
							<div class="col-md-6">
            					<span class="pull-right">
            					<?php if($email_v==0){?>
            						Not Verified
            						<span>
	            						<a href="#" onclick="send_mail_to_customer_email()" id="email_verify_btn" class="btn btn-sm btn-success">
	            							 Verify </a>
	            					</span>	
            					<?php }
								else{?>
									Verified	
								<?php }
            					?>
            						<button type="button" id="email_change_btn" onclick="show_email_update_form()" class="btn btn-sm btn-success preventDflt">
            							 Change 
            						</button>
            					</span>
            				</div>
							</div>
            				<form id="update_email_form" action="<?php echo base_url()?>Account/change_email_customer" method="post" style="display:none" class="form-horizontal">
	            				<div class="form-group">
								    <label class="control-label col-md-3">Email</label>
									<div class="col-md-6">
								    <input type="text" class="form-control" name="email" id="change_email" onkeyup="check_email_in_database()" value="<?php echo $email?>">
								    <span id="change_email_message"></span>
									</div>
								</div>
								<div class="form-group">
								<div class="col-md-6 col-md-offset-3">
								  <button type="button" onclick="hide_email_update_form()" class="btn btn-sm btn-info preventDflt"> Cancel </button>
								  <button type="submit" disabled id="email_update_button" class="btn btn-sm btn-success preventDflt"> Update </button>
								  </div>
								</div>
							</form>
							<form id="verify_email_form" method="post" style="display:none" class="form-horizontal">
	            				<div class="form-group">
								    <label class="control-label col-md-3">Enter the Verification Code</label>
									<div class="col-md-6">
								    <input type="text" class="form-control" name="verify_email_code" id="verify_email_code" value="">
									<span id="verify_email_message"></span>
								    <input type="hidden" class="form-control" name="email" id="ver_email" value="<?php echo $email?>">
								    
									</div>
								</div>
								<div class="form-group">
								<div class="col-md-6 col-md-offset-3">
								  <button type="button" onclick="hide_email_verify_form()" class="btn btn-sm btn-info preventDflt"> Cancel </button>
								  <button type="submit" id="email_verify_button" class="btn btn-sm btn-success preventDflt"> Verify </button>
								</div>
								</div>
							</form>

							<div class="row" id="current_mobile_display">
							<div class="col-md-6">
							<b>Mobile: </b><?php echo $mobile ?> 
							</div>
							<div class="col-md-6">
								<span class="pull-right">
            					<?php if($mobile_v==0){?>
            						Not Verified
            						<span>
	            						<a href="#" onclick="send_mobile_verify_code()" id="mobile_verify_btn" class="btn btn-sm btn-success preventDflt">
	            							 Verify 
	            						</a>
	            					</span>	
            					<?php }
								else{?>
									Verified	
								<?php }
            					?>
            						<button type="button" id="mobile_change_btn" onclick="show_mobile_update_form()" class="btn btn-sm btn-success preventDflt">
            							 Change 
            						</button>
            					</span>
							</div>
							</div>
							<form id="update_mobile_form" action="<?php echo base_url()?>Account/change_mobile_customer" method="post" style="display:none" class="form-horizontal">
	            				<div class="form-group">
								    <label class="control-label col-md-3">Mobile Number:</label>
									<div class="col-md-6">
								    <input type="number" class="form-control" name="mobile" id="mobile_change" onpaste="return false;" onkeyup="check_mobile_in_database()" allow-only-numbers onkeypress="return isNumber(event);" value="<?php echo $mobile ?>">
								    <span id="change_mobile_message"></span>
								</div>
								</div>
								<div class="form-group">
								<div class="col-md-6 col-md-offset-3">
									<button type="button" onclick="hide_mobile_update_form()" class="btn btn-sm btn-info preventDflt"> Cancel </button>
								  <button type="submit" id="mobile_update_button" disabled class="btn btn-sm btn-success preventDflt"> Update </button>
								</div>
								</div>
							</form>
							<form id="verify_mobile_form" method="post" style="display:none" class="form-horizontal">
	            				<div class="form-group">
								<label class="control-label col-md-3">Enter OTP sent to mobile here</label>
								<div class="col-md-6">
								    <input type="text" class="form-control" name="otp_mobile" id="otp_mobile"  value="">
								    <span id="verify_mobile_message"></span>
									<input type="hidden" class="form-control" name="mobile" id="ver_mobil" value="<?php echo $mobile?>">
								</div>
								</div>
								<div class="form-group">
								<div class="col-md-6 col-md-offset-3">
									<button type="button" onclick="hide_mobile_verify_form()" class="btn btn-sm btn-info preventDflt"> Cancel </button>
								  <button type="submit" id="mobile_verify_button" class="btn btn-sm btn-success preventDflt"> Verify </button>
								</div>
								</div>
							</form>
            	</div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<script>
	function show_email_update_form(){
		document.getElementById('current_email_display').style.display ="none"
		document.getElementById('update_email_form').style.display ="block"
	}
	function hide_email_update_form(){
		document.getElementById('current_email_display').style.display ="block"
		document.getElementById('update_email_form').style.display ="none"
	}
	function show_email_verify_form(){
		document.getElementById('current_email_display').style.display ="none";
		document.getElementById('verify_email_form').style.display ="block";
	}
	function hide_email_verify_form(){
		document.getElementById('current_email_display').style.display ="block"
		document.getElementById('verify_email_form').style.display ="none"
	}
	function show_mobile_update_form(){
		document.getElementById('current_mobile_display').style.display ="none"
		document.getElementById('update_mobile_form').style.display ="block"
	}
	function hide_mobile_update_form(){
		document.getElementById('current_mobile_display').style.display ="block"
		document.getElementById('update_mobile_form').style.display ="none"
	}
	function show_mobile_verify_form(){
		document.getElementById('current_mobile_display').style.display ="none"
		document.getElementById('verify_mobile_form').style.display ="block"
	}
	function hide_mobile_verify_form(){
		document.getElementById('current_mobile_display').style.display ="block"
		document.getElementById('verify_mobile_form').style.display ="none"
	}

	function check_email_in_database() {
		var new_pass=document.getElementById('change_email').value;
		var pass_ex=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			
			if(pass_ex.test(new_pass)){
				document.getElementById('email_update_button').disabled = false;
				var xhr = false;
			    if (window.XMLHttpRequest) {
			        xhr = new XMLHttpRequest();
			    }
			    else {
			        xhr = new ActiveXObject("Microsoft.XMLHTTP");
			    }
				 var change_email = document.getElementById('change_email').value; 
				
				 var xhr = new XMLHttpRequest();
						
						if (xhr) {
							
				            xhr.onreadystatechange = function () {
				                if (xhr.readyState == 4 && xhr.status == 200) {
				                   if(xhr.responseText==1){
				                   	document.getElementById('change_email_message').innerHTML="Email Already Exist, Can not Use"
				                   	document.getElementById('email_update_button').disabled = true;
				                   }
				                   else{
				                   	document.getElementById('change_email_message').innerHTML=""
				                   	document.getElementById('email_update_button').disabled = false;
			
				                   }
				                }
				            }
				            xhr.open('POST', "<?php echo base_url()?>Account/check_change_email", true);
				            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				            xhr.send("change_email="+change_email);
				        }
			}
			else{
				document.getElementById('email_update_button').disabled = true;
			}
		
	}
	function check_mobile_in_database() {
		
		var new_pass=document.getElementById('mobile_change').value;
			
			if(new_pass.match(/^(\+\d{1,3}[- ]?)?\d{10}$/) && ! (new_pass.match(/0{5,}/)) ){
				document.getElementById('mobile_update_button').disabled = false;
				var xhr = false;
			    if (window.XMLHttpRequest) {
			        xhr = new XMLHttpRequest();
			    }
			    else {
			        xhr = new ActiveXObject("Microsoft.XMLHTTP");
			    }
				 var mobile_change = document.getElementById('mobile_change').value; 
				
				 var xhr = new XMLHttpRequest();
						
						if (xhr) {
							
				            xhr.onreadystatechange = function () {
				                if (xhr.readyState == 4 && xhr.status == 200) {
				                   if(xhr.responseText==1){
				                   	document.getElementById('change_mobile_message').innerHTML="Mobile Already Exist, Can not Use"
				                   	document.getElementById('mobile_update_button').disabled = true;
				                   }
				                   else{
				                   	document.getElementById('change_mobile_message').innerHTML=""
				                   	document.getElementById('mobile_update_button').disabled = false;
			
				                   }
				                }
				            }
				            xhr.open('POST', "<?php echo base_url()?>Account/check_change_mobile", true);
				            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				            xhr.send("mobile_change="+mobile_change);
				        }
			}
			else{
				document.getElementById('mobile_update_button').disabled = true;
			}
		
	}
	
	function send_mail_to_customer_email(){
		
		email=$('#current_email').val();
		
		$.ajax({
		url:"<?php echo base_url()?>Account/send_mail_to_customer_email",
		type:"POST",
		data:"email="+email,
		beforeSend: function(){
			$("#email_verify_btn").html('<i class="fa fa-refresh fa-spin"></i> Processing');
			$("#email_verify_btn").attr({"disabled":true})
			$("#email_change_btn").attr({"disabled":true})
			$("#mobile_verify_btn").attr({"disabled":true})
			$("#mobile_change_btn").attr({"disabled":true})
		},
		success:function(data){
			$("#email_verify_btn").attr({"disabled":false})
			$("#email_change_btn").attr({"disabled":false})
			$("#mobile_verify_btn").attr({"disabled":false})
			$("#mobile_change_btn").attr({"disabled":false})
			$("#email_verify_btn").html('Verify');
				if(data){
					alert( 'A verification code has been sent to your mail.');
                    show_email_verify_form();

                    /*bootbox.alert({
					  size: "small",
					  message: 'A verification code has been sent to your mail.',
					  callback: function () { show_email_verify_form(); }
					  
					});		
					*/
				}
		}
	})
		
		
	}
	
	$("#verify_email_form").on('submit',(function(e) {
	e.preventDefault();
	verify_email_code=$('#verify_email_code').val();
	if(verify_email_code==""){

	    alert('Please Enter Verification code');
        return false;

		/*bootbox.alert({
					  size: "small",
					  message: 'Please Enter Verification code',
					  callback: function () { return false; }
					  
					});	
		*/
	}
	
	$.ajax({
		url:"<?php echo base_url()?>Account/verify_email_customer",
		type:'POST',
		data:new FormData(this),
		contentType: false,
		cache: false,	
		processData:false,
		beforeSend: function(){
			$("#email_verify_button").html('<i class="fa fa-refresh fa-spin"></i> Processing');
            $("#email_verify_button").prop('disabled', true);
		},
		success:function(data){
			
			if(data){
				$("#email_verify_button").html('Verified');
                $("#email_verify_button").prop('disabled', false);
				$('#verify_email_message').html('');
				alert('successfully Verified');
                location.reload();
                /*
				bootbox.alert({ 
					  size: "small",
					  message: 'successfully Verified',
					  callback: function () { location.reload(); }
					});*/
				//location.reload();
				//hide_email_verify_form();
			}else{
				$("#email_verify_button").html('Verify');
				$('#verify_email_message').html('Please check the Code');
			}
		}
	});
}));

function send_mobile_verify_code(){
		
		mobile=$('#current_mobile').val();
		
		$.ajax({
		url:"<?php echo base_url()?>Account/send_msg_to_customer_mobile",
		type:"POST",
		data:"mobile="+mobile,
		beforeSend: function(){
			$("#mobile_verify_btn").html('<i class="fa fa-refresh fa-spin"></i> Processing');
			$("#email_verify_btn").attr({"disabled":true})
			$("#email_change_btn").attr({"disabled":true})
			$("#mobile_verify_btn").attr({"disabled":true})
			$("#mobile_change_btn").attr({"disabled":true})
		},
		success:function(data){
			$("#email_verify_btn").attr({"disabled":false})
			$("#email_change_btn").attr({"disabled":false})
			$("#mobile_verify_btn").attr({"disabled":false})
			$("#mobile_change_btn").attr({"disabled":false})
			$("#mobile_verify_btn").html('Verify');
				if(data){
				    alert('A verification code has been sent to your mobile number.');
                    show_mobile_verify_form();

                    /*bootbox.alert({
					  size: "small",
					  message: 'A verification code has been sent to your mobile number.',
					  callback: function () { show_mobile_verify_form(); }
					  
					});
					*/
				}
		}
	})
		
		
	}
	
	$("#verify_mobile_form").on('submit',(function(e) {
	e.preventDefault();
	otp_mobile=$('#otp_mobile').val();
	if(otp_mobile==""){

	    alert('Please Enter Verification code');
        return false;
        /*bootbox.alert({
					  size: "small",
					  message: 'Please Enter Verification code',
					  callback: function () { return false; }
					});
		*/
	}
	
	$.ajax({
		url:"<?php echo base_url()?>Account/verify_mobile_customer",
		type:'POST',
		data:new FormData(this),
		contentType: false,
		cache: false,	
		processData:false,
		beforeSend: function(){
			$("#mobile_verify_button").html('<i class="fa fa-refresh fa-spin"></i> Processing');
            $("#mobile_verify_button").prop('disabled', true);
		},
		success:function(data){

			
			if(data){
				$("#mobile_verify_button").html('Verified');
                $("#mobile_verify_button").prop('disabled', false);
				$('#verify_mobile_message').html('');

				alert('Successfully verified');
                location.reload();

				/*bootbox.alert({
					  size: "small",
					  message: 'Successfully verified',
					 callback: function () { location.reload(); } 
					});*/
				//location.reload();
				//hide_email_verify_form();
			}else{
				$("#mobile_verify_button").html('Verify');
				$('#verify_mobile_message').html('Please check the Code');
			}
		}
	});
}));
function isNumber(evt){
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if(charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>