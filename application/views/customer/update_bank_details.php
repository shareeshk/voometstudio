<div class="columns-container">
    <div class="container" id="columns">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account/wallet">Wallet</a></li>
		  <li class="breadcrumb-item active">Update Bank Details</li>
		</ol>
        <div class="row">
            <!-- Left colunm -->
			<?php require_once 'leftcolum.php';?>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
				<div class="well">
				<?php
					$bank_detail=$bank_detail[0];
				?>
            	<p class="lead">Update Bank Details</p><hr>
						<form id="update_bank_details" class="form-horizontal">
						<input  type="hidden" name="bank_id" value="<?php echo $bank_detail['id']?>">
						<input  type="hidden" name="order_item_id" value="<?php echo $order_item_id?>">
						<input  type="hidden" name="wallet_bank_transfer" value="<?php echo $wallet_bank_transfer?>">
						<input  type="hidden" name="transaction_amount" value="<?php if(isset($bank_detail['transaction_amount'])){ echo $bank_detail['transaction_amount']; } else { echo '0';}?>">


								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Bank Name</label>  
								  <div class="col-md-6">
								  <input id="bank_name_rep" name="bank_name_rep" type="text" placeholder="Enter Bank Name" class="form-control input-md refund_return_replace_bank"  value="<?php echo $bank_detail['bank_name']?>"> 
								  <span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Branch Name</label>  
								  <div class="col-md-6">
								  <input id="bank_branch_name_rep" name="bank_branch_name_rep" type="text" placeholder="Enter Branch Name" class="form-control input-md refund_return_replace_bank"  value="<?php echo $bank_detail['branch_name']?>"> 
								  <span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">City</label>  
								  <div class="col-md-6">
								  <input id="bank_city_rep" name="bank_city_rep" type="text" placeholder="Enter City" class="form-control input-md refund_return_replace_bank"  value="<?php echo $bank_detail['city']?>"> 
								  <span class="help-block"></span>   
								  </div>
								</div>
								

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">IFSC Code</label>  
								  <div class="col-md-6">
								  <input id="bank_ifsc_code_rep" name="bank_ifsc_code_rep" type="text" placeholder="Enter IFSC code" class="form-control input-md refund_return_replace_bank"  value="<?php echo $bank_detail['ifsc_code']?>"> 
								  <span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Account Number</label>  
								  <div class="col-md-6">
								  <input id="bank_account_number_rep" name="bank_account_number_rep" type="text" placeholder="Your Account Number" class="form-control input-md refund_return_replace_bank" value="<?php echo $bank_detail['account_number']?>">
								  <span class="help-block"></span>  
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Confirm Account Number</label>  
								  <div class="col-md-6">
								  <input id="bank_account_confirm_number_rep" name="bank_account_confirm_number_rep" type="text" placeholder="Confirm Account Number" class="form-control input-md refund_return_replace_bank"  value="<?php echo $bank_detail['confirm_account_number']?>">
								  <span class="help-block"></span>  
								  </div>
								</div>
								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Account Holder</label>  
								  <div class="col-md-6">
								  <input id="account_holder_name_rep" name="account_holder_name_rep" type="text" placeholder="Name" class="form-control input-md refund_return_replace_bank"   value="<?php echo $bank_detail['account_holder_name']?>">
								  <span class="help-block"></span>  
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Phone Number</label>  
								  <div class="col-md-6">
								  <input id="bank_return_refund_phone_number_rep" name="bank_return_refund_phone_number_rep" type="text" placeholder="" class="form-control input-md refund_return_replace_bank"    value="<?php echo $bank_detail['mobile_number']?>">
								  <span class="help-block"></span>  
								  </div>
								</div>

		
								<div class="form-group">
								 
								  <div class="col-md-6 col-md-offset-3">
										<input type="button" class="button btn-block preventDflt" value="Submit" onclick="update_bank_detailsFun()">
								  </div>
								</div>	            			
								

					</form>
					
            	</div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<script>
function update_bank_detailsFun(){
	$.ajax({
		url:"<?php echo base_url()?>Account/update_bank_details_action",
		type:"POST",
		data:$("#update_bank_details").serialize(),
		success:function(data){
			//alert(data);
			if(data){
				
				/*bootbox.alert({
					  size: "small",
					  message: 'Updated successfully',
					  callback: function () { location.href="<?php echo base_url()?>Account/bank"; }
					  
					});*/

                alert('Updated successfully');
                location.href="<?php echo base_url()?>Account/bank";
			}else{
				/*bootbox.alert({
					  size: "small",
					  message: 'Not yet updated',
					  callback: function () { location.reload(); }fr
					});*/

                alert('Not yet updated');
                location.reload();
            }
		}
	})
}
</script>