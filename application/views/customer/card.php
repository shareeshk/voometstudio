<style>
    hr{
		margin:0;
	}
</style>


<div class="columns-container">
    <div class="container" id="columns">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account">My account</a></li>
		  <li class="breadcrumb-item active">Card Information</li>
		</ol>
        <div class="row">
			<?php require_once 'leftcolum.php';?>
            <div class="center_column col-xs-12 col-sm-9 cards" id="center_column">
				<div class="panel panel-default">
					<div class="panel-heading">
					<strong>Card Information</strong>
					</div>
					<div class="panel-body">
						<div class="f-row product-price">
							Total Saved Cards : <?php  if(!empty($card)){ echo count($card); }else{ echo "0";} ?>		
					
						<span class="pull-right"><button class="button btn-sm preventDflt" onclick="displayAddCardForm()"><i class="fa fa-plus" aria-hidden="true"></i> Add New Card</button></span>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default" id="add_card_container" style="display: none;">
					
					<div class="panel-heading"><strong>Add Card</strong></div>
					<div class="panel-body">
                	<form action="<?php echo base_url()?>Account/add_customer_new_card" method="post" role="form" class="form-horizontal">
					  <div class="form-group">
					    <label class="control-label col-md-3">Card Number</label>
						<div class="col-md-6">
					    <input type="text" class="form-control" required name="card_number" id="card_number" onkeypress="return isNumber(event);" maxlength="16" minlength="16" placeholder="Enter you 15 digit number">
					  </div>
					  </div>
					  <div class="form-group">
					    <label class="control-label col-md-3">Name On Card</label>
						<div class="col-md-6">
					    <input type="text" name="name_on_card" required class="form-control" id="name_on_card" maxlength="40" placeholder="Enter Name which is on card">
					  </div>
					  </div>
					  <div class="form-group">
					    <label class="control-label col-md-3">Card Type</label>
						<div class="col-md-6">
					    <select name="card_type" required class="form-control">
					    	<option value="">Please Choose Card Type</option>
					    	<option value="Visa">Visa</option>
					    	<option value="AMEX">AMEX</option>
					    	<option value="Master">Master</option>
					    </select>
					  </div>
					  </div>
					  <div class="form-group">
					    <label class="control-label col-md-3">Expiry Date </label>
					    	<div class="col-md-3 col-xs-12">
					    		<select class="form-control" name="month" required>
							    	<option value="">Month</option>
							    	<option value="Jan">Jan</option>
							    	<option value="Feb">Feb</option>
							    	<option value="Mar">Mar</option>
							    	<option value="Apr">Apr</option>
							    	<option value="May">May</option>
							    	<option value="Jun">Jun</option>
							    	<option value="Jul">Jul</option>
							    	<option value="Aug">Aug</option>
							    	<option value="Sep">Sep</option>
							    	<option value="Oct">Oct</option>
							    	<option value="Nov">Nov</option>
							    	<option value="Dec">Dec</option>
							    </select>
					    	</div>
					    	<div class="col-md-3 col-xs-12">
					    		<select class="form-control" name="year" required>
							    	<option value="">Year</option>
							    	
							    	<?php $i=date("Y");
							    		for($j=0;$j<30;$j++){ ?>
							    			<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
							    	<?php $i++;}
									 ?>
							    </select>
					    	</div>
					  </div>
					  <div class="form-group">
					    <label class="control-label col-md-3">Card Label</label>
						<div class="col-md-6">
					    <input type="text" name="card_label" class="form-control" id="card_label" maxlength="40" placeholder="Eg: My Dad's Card">
					  </div>
					  </div>
					  <div class="form-group">
					  <div class="col-md-6 col-md-offset-4">
						<button type="submit" class="button preventDflt">Save Card</button>
						<button type="button" class="button preventDflt" onclick="hideAddCardForm()"> <i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
					  </div>
					  </div>
					</form>
				</div>
				</div>
				
				<!------card details------->
				<div class="panel panel-default">
					<div class="panel-heading"><strong>Details</strong></div>
					
					<div class="common">
					
				<?php
				if(!empty($card)){ 
				
						$i=1;foreach($card as $data){
						?>
						
					<div class="panel-body border-down">
						<dl class="dl-horizontal col-sm-6">
						
							<dt><strong>Type</strong></dt> 
							<dd><?php echo $data['card_type'] ?></dd>
							
							<dt><strong>Credit Card No.</strong></dt> 
							<dd><?php echo $data['card_number'] ?></dd>
							
							<dt><strong>Expiry On</strong></dt> 
							<dd><?php echo $data['exp_month'] ?> / <?php echo $data['exp_year'] ?> </dd>
							<dt><strong>Name On Card</strong></dt> 
							<dd><?php echo $data['name_on_card'] ?></dd>
							<dt><strong>Card Label</strong></dt> 
							<dd><?php echo $data['bank_name'] ?></dd>
						</dl> 
						<div class="col-md-6 text-right">
						
							<?php if($data['card_default']!=1){ ?>
					        	<a actions="<?php echo base_url()?>Account/make_this_card_default/<?php echo $data['card_id'] ?>" onclick="askConfirmationCardDefault(this)"><button class="btn btn-sm btn-success preventDflt" data-toggle="tooltip" title="Make Default"><i class="fa fa-check" aria-hidden="true"></i></button></a>
					        <?php }
else{echo '<button class="btn btn-sm btn-info preventDflt" data-toggle="tooltip" title="Default Card"><i class="fa fa-star" aria-hidden="true"></i></button>';}
					        ?> &nbsp; <a actions="<?php echo base_url()?>Account/delete_this_card/<?php echo $data['card_id'] ?>" onclick="askConfirmationCardDelete(this)"><button class="btn btn-sm btn-danger preventDflt" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></button></a>
							
						
						</div>
						
					</div>	
					
						<?php
						$i++;
						}
						?>
						
					
				
						<?php 
				}else{
					echo "<div class='panel-body border-down'><div class='row text-center'><div class='col-md-12'><p class='lead'>No Cards were added</p></div></div></div>";
				} ?>
				</div>
				
				
				
				</div>
				
				<?php
					
					if($i>5){
						$style='';
					}else{
						$style='style="display:none;"';
					}

				?>
				
				<div class="f-row margin-top" id="pagination_div" <?php echo $style; ?>>
					<div class="sortPagiBar">
					<div class="bottom-pagination">
						<span class="nav_pag"></span>
					</div>
					</div>
				</div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<script>
	function displayAddCardForm(){
		document.getElementById('add_card_container').style.display='block';
	}
	function hideAddCardForm(){
		document.getElementById('add_card_container').style.display='none';
	}
</script>
<script>
	function askConfirmationCardDefault(obj){
		var resp = confirm("Do you want to make this card Default ?");
		   if (resp == true) {
		      window.location.href =obj.getAttribute('actions')
		   } 
		   else {
		      
		   }
				
	}
	function askConfirmationCardDelete(obj){
		var resp = confirm("Do you want to delete this card ?");
		   if (resp == true) {
		      window.location.href =obj.getAttribute('actions')
		   } 
		   else {
		      
		   }
				
	}
	function isNumber(evt){
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if(charCode > 31 && (charCode < 48 || charCode > 57) && charCode!=46) {
        return false;
    }
    return true;
}
</script>


<script type="text/javascript">
	jQuery(document).ready(function($){	
		$('.common').paginathing({
	    perPage:5,
	    insertAfter: '.nav_pag'
		});
	});
</script>