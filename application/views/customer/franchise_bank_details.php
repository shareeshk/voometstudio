<div class="columns-container">
    <div class="container" id="columns">
        <ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item active">My account</li>
		</ol>
        <div class="row">
			<?php require_once 'leftcolum.php';?>
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
				<div class="panel panel-default">
    <div class="panel-heading">Bank Deatails</div>
    <div class="panel-body">
		
				<form id="update_bank_details" class="form-horizontal">
            				

							<?php if(!empty($cust_info)){
								?>

<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Bank Name</label>  
								  <div class="col-md-6">
								  <input id="bank_name_rep" name="bank_name" type="text" placeholder="Enter Bank Name" class="form-control input-md"  value="<?php echo $cust_info->bank_name; ?>" required  maxlength="20"> 
								  <span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Branch Name</label>  
								  <div class="col-md-6">
								  <input id="bank_branch_name_rep" name="bank_branch_name" type="text" placeholder="Enter Branch Name" class="form-control input-md"  value="<?php echo $cust_info->branch_name;?>" required  maxlength="20"> 
								  <span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">City</label>  
								  <div class="col-md-6">
								  <input id="bank_branch_city_rep" name="bank_branch_city" type="text" placeholder="Enter City" class="form-control input-md"  value="<?php echo $cust_info->branch_city;?>"  maxlength="20" required> 
								  <span class="help-block"></span>   
								  </div>
								</div>
								

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">IFSC Code</label>  
								  <div class="col-md-6">
								  <input id="bank_ifsc_code_rep" name="bank_ifsc_code" type="text" placeholder="Enter IFSC code" class="form-control input-md"  value="<?php echo $cust_info->ifsc_code;?>" required  maxlength="20"> 
								  <span class="help-block"></span>   
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Account Number</label>  
								  <div class="col-md-6">
								  <input id="bank_account_number_rep" name="bank_account_number" type="text" placeholder="Your Account Number" class="form-control input-md" value="<?php echo $cust_info->account_number;?>" required  maxlength="20" onkeypress="return isNumber(event)">
								  <span class="help-block"></span>  
								  </div>
								</div>

								
								<div class="form-group">
								  <label class="col-md-3 control-label text-right" for="textinput">Account Holder</label>  
								  <div class="col-md-6">
								  <input id="account_holder_name_rep" name="account_holder_name" type="text" placeholder="Name" class="form-control input-md"   value="<?php echo $cust_info->account_holder_name;?>" required  maxlength="20">
								  <span class="help-block"></span>  
								  </div>
								</div>

								
								<?php 
							} ?>
							<div class="form-group">
							<div class="col-sm-6 col-sm-offset-3">
							  <button type="submit" class="button btn-block preventDflt" onclick="update_bank_detailsFun()"> Submit </button>
							</div>
							</div>
            			</div>
				</form>

					</div>	
				</div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>


<script>
function update_bank_detailsFun(){
	$.ajax({
		url:"<?php echo base_url()?>Account/update_bank_details_franchise",
		type:"POST",
		data:$("#update_bank_details").serialize(),
		success:function(data){
			//alert(data);
			if(data){
				
				/*bootbox.alert({
					  size: "small",
					  message: 'Updated successfully',
					  callback: function () { location.href="<?php echo base_url()?>Account/franchise_bank_details"; }
					  
					});*/

                alert('Updated successfully');
                location.href="<?php echo base_url()?>Account/franchise_bank_details";
			}else{
				/*bootbox.alert({
					  size: "small",
					  message: 'Not yet updated',
					  callback: function () { location.reload(); }
					});*/

                alert('Not yet updated');
              	location.reload();
            }
		}
	})
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
        return false;
    }
    return true;
}
</script>