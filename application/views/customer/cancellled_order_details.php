<?php								
		$cancels_table_data_arr=$controller->get_data_cancels_table($order_items["order_item_id"]);
		if(!empty($cancels_table_data_arr)){
			//$refund_table_data_arr=$controller->get_data_refund_table($cancels_table_data_arr["cancel_id"]);
		}
		$cancel_refund_table_data_arr=$controller->get_data_cancel_refund_table($order_items["order_item_id"]);

		

							
		$cancel_refund_table_data_arr=$controller->get_data_cancels_table($order_items["order_item_id"]);
		$cancels_orders_data_arr=$controller->get_data_cancelled_orders($order_items["order_item_id"]);
		
		if(!empty($cancel_refund_table_data_arr) && $cancel_refund_table_data_arr["refund_type"]!=""){
?>	
<tr class="margin-top margin-bottom">
		<td colspan="5">
		
    <div class="panel-group" id="accordion_<?php echo $order_item["order_item_id"]?>">
        <div class="panel panel-default">
            <div class="panel-heading text-uppercase">
                
                    <a data-toggle="collapse" data-parent="#accordion_<?php echo $order_item["order_item_id"]?>" href="#collapseOne_<?php echo $order_item["order_item_id"]?>">
					<?php
						if($cancel_refund_table_data_arr["status"]==""){
							echo "Refund will be processed soon.";
						}
					
						if($cancel_refund_table_data_arr["status"]=="refund" && $cancel_refund_table_data_arr["status_of_refund"]==""){
							echo "Refund Initiated.";
						}
				
						if($cancel_refund_table_data_arr["status"]=="refunded"){
							echo "Refund Successful.";
						}
					
						if($cancel_refund_table_data_arr["status"]=="not refunded"){
							echo "Refund Failed.";
						}
						if($cancel_refund_table_data_arr["status"]=="refund" && $cancel_refund_table_data_arr["status_of_refund"]=="no"){
							echo "Refund Initiated again.";
						}
					?>
					</a>
               
            </div>
            <div id="collapseOne_<?php echo $order_item["order_item_id"]?>" class="panel-collapse">
                <div class="panel-body">
                    <div class="row">
					<div class="col-md-12">
					<div class="row">
						<div class="col-md-4">

						<?php if($order_item["ord_addon_products_status"]=='1'){
							
							$addon=json_decode($order_item["ord_addon_products"]);
							//print_r($addon);
							$addon_count=count($addon);
							?>

							<div class="f-row"> Quantity: <?php echo $addon_count;?> <i>(combo products)</i> </div>

						<?php }else{
							?>
								<div class="f-row"> Quantity: <?php echo $order_item["quantity"];?> </div>
							<?php 
						} ?>

							<?php 
							
							echo '<div class="f-row">Refund to: ';
							
							if($cancel_refund_table_data_arr['refund_type']=="Bank Transfer"){
								$refund_bank_id=$cancel_refund_table_data_arr['refund_bank_id'];	
								$bank_detail_arr=$controller->get_bank_details($refund_bank_id);
								
								echo '<a id="bank_detail_cancel_'.$order_item_id.'" data-toggle="popover" data-trigger="click" data-placement="bottom" data-container="body" class="cursor-pointer" data-original-title="" title=""><u>'.$cancel_refund_table_data_arr['refund_type'].'</u></a>';
								
								echo '<div id="bank_detail_cancel_div_'.$order_item_id.'" class="hide">'.$bank_detail_arr.'</div>';
							}else{
								echo $cancel_refund_table_data_arr['refund_type'];
							}

							if($cancel_refund_table_data_arr["refund_type_updated_by"]=="admin"){	
								echo ' <small class="small-text">(Admin Selected)</small>';
							}

							echo '</div>';
							
							if($cancel_refund_table_data_arr["refund_type_comments_admin"]!=""){
									?>
							
									<div class="f-row small-text more">
										<?php echo $cancel_refund_table_data_arr["refund_type_comments_admin"]; ?>
									</div>
									
									<?php 
								
								}
						
						
							if($cancel_refund_table_data_arr["cancelled_by"]=="customer"){
							$order_cancel_reason_arr=$controller->get_order_cancel_reason_by_reason_cancel_id($cancel_refund_table_data_arr["reason_cancel_id"]);
							echo '<div class="f-row"><b>Reason:</b>'.$order_cancel_reason_arr["reason_name"].'</div>';
							}
							if($cancel_refund_table_data_arr["cancelled_by"]=="admin"){
								
							$order_cancel_admin_reason_arr=$controller->get_order_cancel_admin_reason_by_reason_cancel_id($cancel_refund_table_data_arr["reason_cancel_id"]);
							echo '<div class="f-row"><b>Reason:</b>'.$order_cancel_admin_reason_arr["cancel_reason"].'</div>';
							}
							?>
							
							<?php
							echo '<div class="f-row"><b>Comments:</b><span class="more">'.$cancel_refund_table_data_arr["cancel_reason_comments"].'</span></div>';
							
							?>
							<script>
							$(document).ready(function(){
								$('#bank_detail_cancel_<?php echo $order_item_id; ?>').popover({
									html : true,
									content: function(){
									  return $('#bank_detail_cancel_div_<?php echo $order_item_id; ?>').html();
									}
								});
								$('#price_details_of_cancellation_<?php echo $order_item_id;?>').popover({
									html : true,
									content: function(){
									  return $('#popover-content-price_details_of_cancellation_<?php echo $order_item_id;?>').html();
									}
								});
							});
							</script>
							
						</div>
						
							
<!-----promo_popover2------------------>

	<div class="col-md-3 products-block">
 
<?php if($orders_status_data["type_of_order"]==""){ 
?>	
									
											


										<?php if($promotion_invoice_free_shipping>0){
											echo "(".curr_sym.($order_item['subtotal']-$order_item_invoice_discount_value).")";
										}else{
											echo "(".curr_sym.($order_item['grandtotal']-$order_item_invoice_discount_value).")";
										}
										?>	
										
												<a  class="cursor-pointer preventDflt"  data-trigger="click" data-placement="bottom" data-toggle="popover" data-container="body" type="button" data-html="true" id="price_details_of_cancellation_<?php echo $order_item_id;?>" content_id="#popover-content-price_details_of_cancellation_<?php echo $order_item_id; ?>" onclick="toggle_content(this)">[<i class="fa fa-question" aria-hidden="true"></i>]</a>
							<div id="popover-content-price_details_of_cancellation_<?php echo $order_item_id;?>" style="display:none;">
							<table class="table table-bordered" style="font-size: .8em;text-align:justify;margin-bottom: 0px;">
                                                    <thead><tr><th style="padding:0px"></th><th width="25%" style="padding:0px"></th><th style="padding:0px" width="25%" style="text-align: right"></th></tr></thead>


                                            <?php
														if($order_item["ord_addon_products_status"]=='1'){
													
														
															//$total_amount_normal+=round($order_item["ord_addon_total_price"]);
															$addon=json_decode($order_item["ord_addon_products"]);
															//print_r($addon);
															$addon_count=count($addon);
															
															?>
															<tr>
															<td>Total Price</td>
															<td><?php echo $addon_count.' items';?></td>
															<td class="text-right" ><?php echo curr_sym; ?><?php echo round($order_item["subtotal"]); ?></td>
															</tr>

															<tr>
															<td colspan="3">Applied Discount <?php echo round($order_item["ord_selling_discount"]); ?>%</td>
															</tr>

															<tr>
															<td colspan="2">Shipping Charge</td>
															<td class="text-right" ><?php echo curr_sym; ?><?php echo round($order_item["shipping_charge"]); ?></td>
															</tr>

															<tr>
															<td colspan="2">Grand Total</td>
															<td class="text-right" ><?php echo curr_sym; ?><?php echo round($order_item["ord_addon_total_price"]); ?></td>
															</tr>

															<tr><td colspan="2"><em><b>Total amount to be refunded</b></em><?php //echo $minustext; ?></td><td class="text-right"><b><?php echo curr_sym;?><?php echo round($order_item["grandtotal"]);?></b></td></tr>
												
													<?php 
														
														}else{

															?>


                                            <!---normal items popup------>

<?php
											
													$total_product_price_normal=($order_item["quantity"]*round($order_item["ord_max_selling_price"]));
													$shipping_price_normal=$order_item["shipping_charge"];
													

													$total_amount_normal=($shipping_price_normal+$total_product_price_normal);
													
	
	?>
													
                                                    <tr><td>Base product price</td><td><?php echo $order_item['quantity']; ?> x <?php echo round($order_item['ord_max_selling_price']);?></td><td class="text-right"><?php echo curr_sym;?><?php echo $total_product_price_normal; ?></td></tr>
													
													<tr><td colspan="2">Shipping Charge for <?php echo $order_item['quantity']; ?> unit(s)</td><td class="text-right"><?php echo curr_sym;?><?php echo $shipping_price_normal; ?></td></tr>
									
													<tr><td colspan="2">Total value </td><td class="text-right"><?php echo curr_sym;?><?php echo $total_amount_normal; ?></td></tr>
                                       <?php
											if($promotion_invoice_discount>0){
												?>
												<tr><td colspan="2">Discount price for <?php echo $order_item['quantity']; ?> unit(s)</td><td class="text-right" style="color:red;"><?php echo curr_sym;?><?php echo $order_item_invoice_discount_value; ?></td></tr>
												<?php 
											}
										?>
										
                                        <?php if($order_item['promotion_available']==1){ ?>   
										
                                                <?php if(intval($order_item['quantity_without_promotion'])>0 && $order_item['default_discount']>0){ ?>
												
												<?php
												
												$default_discount_price=floatval($order_item['ord_max_selling_price']-$order_item['individual_price_of_product_without_promotion']);
		// $default_discount_price=(($order_item['product_price']*$order_item['default_discount'])/100);
		$total_default_discount_price=($order_item['quantity_without_promotion']*$default_discount_price);
		$total_item_price_with_base_price=($order_item['product_price']*$order_item['quantity_without_promotion']);
		
												?>
                                                <tr><td> Discount @ (<?php echo $order_item['default_discount'] ?>%)<br>Eligible Quantity : <?php echo $order_item['quantity_without_promotion'] ?><br>For promotion <br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_default_discount_promo']; ?></small></td><td><?php echo $order_item['quantity_without_promotion'] ?> <i class="fa fa-times" aria-hidden="true"></i> <?php echo  $default_discount_price ?></td><td class="text-right" style="color:red"> <?php echo curr_sym;?><?php echo $total_default_discount_price;?> </td></tr>
												
												
                                                <?php }?>
                                                
                                                <?php if(intval($order_item['quantity_with_promotion'])>0){ ?>
                                                 
                                                 <?php if(intval($order_item['promotion_discount'])>0&& (intval($order_item['promotion_discount'])==intval($order_item['default_discount']))){?>
												 
												 <?php
												 
												$default_discount_price=floatval($order_item['ord_max_selling_price']-$order_item['individual_price_of_product_with_promotion']);
		// $default_discount_price=(($order_item['product_price']*$order_item['promotion_discount'])/100);
		$total_default_discount_price=($order_item['quantity_with_promotion']*$default_discount_price);
		$total_item_price_with_base_price=($order_item['product_price']*$order_item['quantity_with_promotion']);
		
												 ?>
                                                     <tr><td> Discount @ (<?php echo $order_item['promotion_discount'] ?>%) <br>Eligible Quantity : <?php echo $order_item['quantity_with_promotion'] ?><br>For promotion<br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_quote']; ?></small></td><td><?php echo $order_item['quantity_with_promotion'] ?> <i class="fa fa-times" aria-hidden="true"></i> <?php echo  $default_discount_price ?></td><td class="text-right" style="color:red"> <?php echo curr_sym;?><?php echo $total_default_discount_price;?> </td></tr>
													 
                                                 <?php }?>
                                                 
                                                 <?php if(intval($order_item['promotion_discount'])>0 && (intval($order_item['promotion_discount'])!=intval($order_item['default_discount']))){?>
												 
												 <?php
												 
												 $promo_discount_price=floatval($order_item['ord_max_selling_price']-$order_item['individual_price_of_product_with_promotion']);
												
		//$promo_discount_price=(($order_item['product_price']*$order_item['promotion_discount'])/100);
		$total_promo_discount_price=($order_item['quantity_with_promotion']*$promo_discount_price);
		$total_item_price_with_base_price=($order_item['product_price']*$order_item['quantity_with_promotion']);
		
												 ?>
                                                      <tr><td>Promotion Discount @ (<?php echo $order_item['promotion_discount'] ?>%)<br>Eligible Quantity : <?php echo $order_item['quantity_with_promotion'] ?><br>For Promotion<br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_quote']; ?></small></td><td><?php echo $order_item['quantity_with_promotion'] ?> <i class="fa fa-times" aria-hidden="true"></i> <?php echo $promo_discount_price; ?></td><td class="text-right" style="color:red;"><?php echo curr_sym;?><?php echo $total_promo_discount_price;?> </td></tr>
                                                 <?php }?>   
                                                    
                                                  <?php if(intval($order_item['promotion_discount'])==0 && $order_item['promotion_default_discount_promo']=="Price for remaining"){?>
												  
													<?php if(intval($order_item['default_discount']>0)){ ?>
												  
                                                     <tr><td>Standard MRP @ (<?php echo $order_item['product_price'] ?>) <br>Eligible Quantity : <?php echo $order_item['quantity_with_promotion'] ?><br>For Promotion<br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_quote']; ?></small></td><td><?php echo $order_item['quantity_with_promotion'] ?> <i class="fa fa-times" aria-hidden="true"></i> <?php echo $order_item['product_price'] ?></td><td class="text-right"><?php echo curr_sym;?><?php echo $order_item["total_price_of_product_with_promotion"];?> </td></tr>
													 
												  <?php }else{
													  ?>
													   <tr><td colspan="3">For Promotion<br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_quote']; ?></small></td></tr>
													  <?php
												  } ?>
												  
                                                 <?php }?>  
                                                    
                                                
                                                <?php }?>
                                                
                                                <?php if(intval($order_item['quantity_without_promotion'])>0 && $order_item['default_discount']==0){ ?>
                                                <!-- no quote and no default promotion---->
<?php /*?>											   <tr><td>Standard MRP @ (<?php echo $order_item['product_price'] ?>) <br>Eligible Quantity : <?php echo $order_item['quantity_without_promotion'] ?><br></td><td><?php echo $order_item['quantity_without_promotion'] ?>*<?php echo $order_item['product_price'] ?></td><td class="text-right"><?php echo curr_sym;?><?php echo $order_item["total_price_of_product_without_promotion"];?> </td></tr>
<?php */?>
                                                <?php }?>
                                                
                                                <!---added --->
                                                 <?php
                                                 //&& $order_item['promotion_quote']==$order_item['promotion_default_discount_promo']
                                                 if($order_item['promotion_default_discount_promo']!="" && $order_item['default_discount']>0 ){
                                                     /* straight discount */
                                                     
                                                 }else{
                                                     if($order_item['ord_selling_discount']>0){
                                                     
                                                         ?>
                                                <tr><td colspan="2">Discount @ (<?php echo round($order_item['ord_selling_discount']) ?>%)<br>Eligible Quantity : <?php echo $order_item['quantity'] ?><br></td><td class="text-right"><?php echo curr_sym; ?> <?php echo ($order_item['quantity']*($order_item['ord_max_selling_price']-$order_item['product_price'])); ?></td></tr>
                                                        <?php 
                                                         
                                                     }
                                                     
                                                 }
                                                 
                                                 
                                                 ?>
                                                 <!---added --->
                                                
                                                
                                              <?php }else{ 
                                                  if($order_item['ord_selling_discount']>0){
                                                     
                                                         ?>
                                                <tr><td colspan="2">Discount @ (<?php echo round($order_item['ord_selling_discount']) ?>%)<br>Eligible Quantity : <?php echo $order_item['quantity'] ?><br></td><td class="text-right"><?php echo curr_sym; ?> <?php echo ($order_item['quantity']*($order_item['ord_max_selling_price']-$order_item['product_price'])); ?></td></tr>
                                                        <?php 
                                                         
                                                     } 
                                                     
                                                  } ?>                                                
	<?php
					if($promotion_item!='' || ($promotion_surprise_gift_skus!='' && $order_delivered==1)){
							if($promotion_item!=''){

								foreach($free_items_arr as $free_items){
									?>
									<tr><td><?php echo "SKU : ".$free_items->sku_id."(free)"; ?></td><td><?php $id=$free_items->id;
										echo "Quantity ".($temp_quotient*$two[$id]); ?></td><td><del><?php echo curr_sym.(($temp_quotient*$two[$id])*$free_items->selling_price); ?></del></td></tr>
									<?php
								}
							}

							if($promotion_surprise_gift_type=='Qty'){

								foreach($free_surprise_items as $surprise_items){
									?>
									<tr><td><?php echo "SKU : ".$surprise_items->sku_id."(free)"; ?></td><?php $id=$surprise_items->id;
										echo "Quantity ".($gift_with_count[$id]); ?><td><del><?php echo curr_sym.(($gift_with_count[$id])*$surprise_items->selling_price); ?></del></td></tr>
									<?php
								}
							}
					}
												?>
						
                                                                        
                                                                        
                                                                        
                                                
                                                <!----sku coupon--->
                                                <?php
                                                
                                                $sku_coupon_used=0;
                                                if($order_item['ord_coupon_status']=='1'){ 
                                                    
                                                    $sku_coupon_used=round($order_item['ord_coupon_reduce_price']);
                                                    ?>
                                                
                                                <tr><td colspan="2">Coupon applied on SKU <b>(<?php echo ($order_item['ord_coupon_type']=='2') ? curr_sym : ''; ?><?php echo round($order_item['ord_coupon_value']) ?><?php echo ($order_item['ord_coupon_type']=='1') ? '%' : 'Flat'; ?>)</b>
                                                        
                                                        </td><td class="text-right"><?php echo curr_sym; ?> <?php echo round($order_item['ord_coupon_reduce_price']); ?></td></tr>
                                                
                                                <?php } ?>
                                                <!----sku coupon--->
                                                
                                                
                                                                        
                                              <tr>
                                                <?php 
												
												if($promotion_invoice_free_shipping>0){   ?>
			
													<td>Shipping Charge 
													( <span style="color:red">waived off</span> )
													</td>
                                                <td><?php echo $order_item['quantity'] ?> units(s)</td>
                                                <td class="text-right" style="color:red">

                                                    <?php echo curr_sym;?><?php echo $order_item["shipping_charge"]; ?>
                                                
                                                </td>
													<?php
												}?>

  
                                                </tr>
                                                
                                                <?php
                                                if($order_item["cash_back_value"]!="" && $order_item["cash_back_value"]!=0){                                          ?>
                                               <tr>
                                                 
                                                <td>Cashback value of <?php echo $order_item['promotion_cashback'] ?> % <br><em>(credited to wallet) </em></td>
                                                <td><?php echo $order_item['quantity'] ?> <i class="fa fa-times" aria-hidden="true"></i>  <?php echo round($order_item["cash_back_value"]/$order_item['quantity'])?></td>
                                                <td class="text-right" ><?php echo curr_sym;?><?php echo $order_item["cash_back_value"]; ?></td>
                                                </tr>
                                                
                                                <?php 
												
												}
												?>
												<!----addon section--->

												<?php 
													if($order_item["ord_addon_products_status"]=='1'){
														$total_amount_normal+=round($order_item["ord_addon_total_price"]);
														$addon=json_decode($order_item["ord_addon_products"]);
														//print_r($addon);
														$addon_count=count($addon);
														?>
												<tr>
												<td>Additional products</td>
												<td><?php echo $addon_count.' items';?></td>
												<td class="text-right" ><?php echo curr_sym; ?><?php echo round($order_item["ord_addon_total_price"]); ?></td>
												</tr>
														<?php 
													}
												?>

												<!----addon section--->
												
                                                <?php if($promotion_invoice_free_shipping>0){ ?>
                                                  
												  <tr><td colspan="2"><em><b>Total amount to be refunded </b></em><?php echo $minustext; ?></td><td class="text-right"><b><?php echo curr_sym;?><?php echo ($order_item["subtotal"]-$order_item_invoice_discount_value);?></b></td></tr>
												  
                                                <?php }else{ ?>
                                                    <tr><td colspan="2"><em><b>Total amount to be refunded</b></em><?php echo $minustext; ?></td><td class="text-right"><b><?php echo curr_sym;?><?php echo ($order_item["grandtotal"]-$order_item_invoice_discount_value);?></b></td></tr>
                                               <?php } ?>
                                                

                                               
											<?php
											if($total_amount_normal>$order_item["grandtotal"]){
												
											$you_saved=($total_amount_normal-$order_item["grandtotal"]);
											if($promotion_invoice_free_shipping>0){
												$you_saved+=$shipping_price_normal;
											}
											?>
											
											<tr><td colspan="2"><em><b>You Saved</b></em></td><td class="text-right" style="color:green"><b><?php echo curr_sym;?><?php echo $you_saved;?></b></td></tr>
											<?php
											
											}
											?>

<?php } //addon else condition ?>
											</table>
							</div>
                                                   


											   
											   
<?php 
}//checking type of order

?>
											
	<?php if($orders_status_data["type_of_order"]==""){ 
			if($order_item['promotion_available']==1){

			if($order_item['total_price_of_product_with_promotion']!="" || $order_item['total_price_of_product_without_promotion']!=""){
				
				
				$r=0;
				if($order_item['promotion_quote']!=""){
					$r=1;
				}	
				if($order_item['promotion_default_discount_promo']!="" && $order_item['default_discount']>0 && $order_item['promotion_quote']!=$order_item['promotion_default_discount_promo']){
									
					$r+=1;
				}
				
				?>                     
										<a  class="cursor-pointer" data-trigger="click" data-placement="bottom" data-toggle="popover" data-container="body" type="button" data-html="true" id="price_details_offer_<?php echo $order_item["order_item_id"]?>" content_id="#content_price_details_offer_<?php echo $order_item["order_item_id"]; ?>" onclick="toggle_content(this)">OFFER : <?php echo $r; ?></a>
                                            
										<div id="content_price_details_offer_<?php echo $order_item["order_item_id"];?>" style="display:none;">
                                                <table class="table table-bordered " style="font-size: .8em;text-align:justify;margin-bottom: 0px;">
                                                <?php
$r=0;
												if($order_item['promotion_quote']!=""){
													$r=1;
													?>
                                                    <tr class="success"><td><?php echo $r.":"; ?></td><td><?php echo $order_item['promotion_quote'] ?></td></tr>
                                               <?php }?>
                                                <?php 	if($order_item['promotion_default_discount_promo']!="" && $order_item['default_discount']>0 && $order_item['promotion_quote']!=$order_item['promotion_default_discount_promo']){
$r+=1;
												?>
                                                   <tr  class="success"><td><?php echo $r.":"; ?></td><td><?php echo $order_item['promotion_default_discount_promo'] ?></td></tr>
                                               <?php }?>
                                                </table>
                                                
										</div>		

											
                                            <?php 
			}
											
			}else{
                            ?>

                                <a data-trigger="click" data-placement="bottom" data-toggle="price_details2" data-html="true" id="price_details_3_<?php echo $order_item_id; ?>" class="price_details_2 cursor-pointer" content_id="#content_price_details_3_<?php echo $order_item_id; ?>" onclick="toggle_content(this)">

                                OFFERS : 1 </a>

                                <div id="content_price_details_3_<?php echo $order_item_id; ?>" style="display:none;">
                                    <table class="table table-bordered" style="font-size: .8em;text-align:justify;margin-bottom: 0px;">

                                    <tr  class="success"><td colspan="2">Discount <?php echo round($order_item['ord_selling_discount']); ?> % Off </td></tr>

                                    </table>
                                </div>                                                                                
                            <?php
                        }
																			
	}//checking type_order
											?>
		</div>
									
<!-----promo_popover2------------------>
							
						<div class="col-md-5">
						
						<?php 
						
						//print_r($cancels_orders_data_arr);
						//$cancels_orders_data_arr["order_item_invoice_discount_value"] is $order_item_invoice_discount_value

						?>
	
						<?php if($promotion_invoice_free_shipping>0){ 
                                                  
						$total_refund=($order_item["subtotal"]-$order_item_invoice_discount_value);
						  
						}else{ 
						 $total_refund=($order_item["grandtotal"]-$order_item_invoice_discount_value);
					    
						} ?>
						
						<?php if($cancel_refund_table_data_arr["status"]==""){?>
						
						<div class="f-row">
							Refund will be initiated <?php echo curr_sym;?><?php echo $total_refund;?> will be remitted through <b><?php echo $cancel_refund_table_data_arr["refund_type"];?></b>.Your refund is awaiting response from seller.We will get back to you soon.
						</div>
						<?php } ?>
						<?php if($cancel_refund_table_data_arr["status"]=="refund" && $cancel_refund_table_data_arr["status_of_refund"]==""){ ?>
						
						<div class="f-row">
						Refund had been processed for <?php echo curr_sym;?><?php echo $total_refund;?>. will be remitted through <b><?php echo $cancel_refund_table_data_arr["refund_type"];?></b>. We will get back to you, once we transfer your money successfully.
						</div>
						
						<?php } ?>
						<?php if($cancel_refund_table_data_arr["status"]=="not refunded"){ ?>
						
						<div class="f-row">
						Refund had been failed for <?php echo curr_sym;?><?php echo $total_refund;?> through <b><?php echo $cancel_refund_table_data_arr["refund_type"];?></b>.
						</div>
						<div class="f-row">
						Due to <span class="more"><?php echo $cancel_refund_table_data_arr["refund_fail_comments"];?></span>
						</div>
						
						<div class="f-row">
						So, <?php if ($cancel_refund_table_data_arr["allow_update_bank_details"]){?>Please update your Bank details above.
						<?php } ?>
						</div>
						
						<?php } ?>
						<?php if($cancel_refund_table_data_arr["status"]=="refund" && $cancel_refund_table_data_arr["status_of_refund"]=="no"){ ?>
						
						<div class="f-row">
						Refund had been processed again for <?php echo curr_sym;?><?php echo $total_refund;?>. will be remitted through <b><?php echo $cancel_refund_table_data_arr["refund_type"];?></b>. We will get back to you, once we transfer your money successfully.
						</div>
						
						<?php } ?>
					<?php
					
					
					if($cancels_table_data_arr['status']=='refunded'){
					if($cancels_table_data_arr['refund_type']=='Bank Transfer'){
												?>
												<div class="f-row"><b style="color:green;">Your Items has been refunded successfully. Please find transaction details below.</b>
												</div>
												<?php
													echo '<div class="f-row"><strong>Transaction ID: </strong>'.$cancels_table_data_arr['transaction_id'].'</div>';
													echo '<div class="f-row"><strong>Transaction Date: </strong>'.$cancels_table_data_arr['transaction_date'].'</div>';
													echo '<div class="f-row"><strong>Transaction Time: </strong>'.$cancels_table_data_arr['transaction_time'].'</div>';
													$get_refund_data=$controller->get_order_details_admin_acceptance_refund_data($order_item['order_item_id']);
					}
												?>
												<div class="f-row"><strong>Transaction Amount Refunded: </strong> <?php echo curr_sym;?><?php echo $total_refund;?>
												</div>
												<?php
												}
												?>
												
						</div>
					
				
					</div>
					</div>
                </div>
				<div class="row">
				<div class="col-md-12">
				<?php
				if(count($cancels_table_data_arr)>0){
			/////////////////////////////////////
			if(($cancels_table_data_arr['status']=='not refunded' && $cancels_table_data_arr['allow_update_bank_details']=='yes')  || ($cancels_table_data_arr['status']=='not refunded' && $cancels_table_data_arr['allow_update_bank_details']=='updated')){
				?>
				<div class="row margin-top">
					<div class="col-md-12">
					<button type="button" class="btn btn-default text-uppercase btn-sm preventDflt" onclick="location.href='<?php echo base_url()?>Account/update_bank_details/<?php echo $cancels_table_data_arr['refund_bank_id'];?>/<?php echo $cancels_table_data_arr['order_item_id'];?>'">Update Bank Details</button> (Your bank details seems to be wrong. Please update it.)
					</div>
					
				</div>
				
				<?php
				}					
									/////////////////////////////////////
		}
		?>
				</div>
				</div>
            </div>
        </div>
        
        
    </div>
	
</div>
			</td>
		</tr>
<?php } ?>