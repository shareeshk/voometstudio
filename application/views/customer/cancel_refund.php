	<div class="row margin-top">	
		<div class="col-md-12">	
		<button class="btn btn-default btn-sm text-uppercase preventDflt" data-toggle="modal"  onclick="call_conversation_refund('<?php echo $order_item['order_item_id']; ?>')">Contact Us
		<?php
			$return_order_id=$controller->get_return_order_id_by_order_item_id($order_item['order_item_id']);
			$unread_msg=$controller->get_unread_return_msg($return_order_id);
			if($unread_msg==0){
			?>
				<span class="badge"></span>
				<?php
			}else{
				?>
				<span class="badge"><?php echo $unread_msg; ?></span>
				<?php
			}
			?>
		</button>
		<?php
				$get_refund_data_view_vendor_addr=$controller->get_order_details_admin_acceptance_refund_data($order_item['order_item_id']);
				$orders_status_data=$controller->get_orders_status_data($order_item['order_item_id']);
				if(!empty($get_refund_data_view_vendor_addr) && $get_refund_data_view_vendor_addr['vendor_id']!="" && $orders_status_data["order_return_pickup"]!=1 && $get_refund_data_view_vendor_addr['customer_status']!="cancel"){
					?>
					<button class="btn btn-default text-uppercase btn-sm preventDflt" onclick="window.open('<?php echo base_url()?>assets/pictures/return_lables/vendor_<?php echo $order_item['order_item_id'];?>.pdf')">Shipping Label</button>
					<?php
				}
				if(($return_data['order_return_decision_status']=='not refunded' && $return_data['allow_update_bank_details']=='yes')  || ($return_data['order_return_decision_status']=='not refunded' && $return_data['allow_update_bank_details']=='updated')){
				?>
				<button type="button" class="btn btn-default text-uppercase btn-sm preventDflt" onclick="location.href='<?php echo base_url()?>Account/update_bank_details/<?php echo $return_data['refund_bank_id'];?>/<?php echo $return_data['order_item_id'];?>'">Update Bank Details</button>
				<p>Your bank details seems to be wrong. Please update by clicking above button.</p>
				<?php
				}
				?>
		<?php
			$get_refund_data=$controller->get_order_details_admin_acceptance_refund_data($order_item['order_item_id']);
			$orders_status_data=$controller->get_orders_status_data($order_item["order_item_id"]);
			
			if(!empty($get_refund_data)){
				
			if(($orders_status_data["order_return_pickup"]==0 && $get_refund_data["pickup_identifier"]=="after return") || ($orders_status_data["order_return_pickup"]==0 && $get_refund_data["pickup_identifier"]=="before return")){
			?>
			
				<?php 
				if($return_data['order_return_decision_customer_status']=="pending" || $return_data['order_return_decision_customer_status']==""){
					if($return_data["order_return_decision_status"]=="pending" || $return_data["order_return_decision_status"]=="accept" || $return_data["order_return_decision_status"]==""){
						if($return_data["order_return_decision_status"]!="reject"){
					
					?>
						<input type="button" class="btn btn-default text-uppercase btn-sm preventDflt" value="Cancel Refund Request" onclick="open_return_cancel_divFun(<?php echo $order_item['order_item_id'];?>)">
						<div id="customer_return_cancel_<?php echo $order_item['order_item_id'];?>" style="display:none;" class="form-horizontal margin-top">
						<div class="form-group">
						<div class="col-md-6">
						<textarea class="form-control" id="cancel_returns_comments_<?php echo $order_item['order_item_id'];?>" rows="3" cols="25" placeholder="Enter your comments for cancelling refund"></textarea>
						</div>
						</div>
						<div class="form-group">
						<div class="col-md-6">
						<input type="button" class="button text-uppercase margin-top margin-bottom margin-top preventDflt" onclick="return_cancel_fun(<?php echo $order_item['order_item_id'];?>)" value="Request Cancel">
						</div>
						</div>
						</div>

						
						<?php
						}
					}
				}
				?>

				<?php 
			}
			
			}else{

				if($return_data['order_return_decision_customer_status']=="pending" || $return_data['order_return_decision_customer_status']==""){
					if($return_data["order_return_decision_status"]=="pending" || $return_data["order_return_decision_status"]=="accept" || $return_data["order_return_decision_status"]==""){
						if($return_data["order_return_decision_status"]!="reject"){
					
					?>
						<input type="button" class="btn btn-default text-uppercase btn-sm preventDflt" value="Cancel Refund Request" onclick="open_return_cancel_divFun(<?php echo $order_item['order_item_id'];?>)">
						
						<div id="customer_return_cancel_<?php echo $order_item['order_item_id'];?>" style="display:none;" class="form-horizontal margin-top">
						<div class="form-group">
						<div class="col-md-6">
						<textarea class="form-control" id="cancel_returns_comments_<?php echo $order_item['order_item_id'];?>" rows="3" cols="25"  placeholder="Enter your comments for cancelling refund"></textarea>
						</div>
						</div>
						<div class="form-group">
						<div class="col-md-6">
						<input type="button" class="button text-uppercase margin-top margin-bottom preventDflt" onclick="return_cancel_fun(<?php echo $order_item['order_item_id'];?>)" value="Request Cancel">
						</div>
						</div>
						</div>
						
						<?php
						}
					}
				}
				
				
			}
			
				$return_order_id=$controller->get_return_order_id_by_order_item_id($order_item['order_item_id']);
				$returns_conservation_obj_arr=$controller->get_returns_conservation_chain_in_customerpanel($return_order_id);
			?>
				<input type="hidden" id="return_order_id_for_msg_op_<?php echo $order_item['order_item_id'];?>" value="<?php echo $return_order_id; ?>">

			<?php
				$get_refund_data=$controller->get_order_details_admin_acceptance_refund_data($order_item['order_item_id']);
				//print_r($get_return_desired_refund_data);
			?>
			
			<form id="send_form_admin_contact_admin_form_<?php echo $order_item['order_item_id'];?>" name="send_form_admin_contact_admin_form_<?php echo $order_item['order_item_id'];?>" method="post"  enctype="multipart/form-data" class="hide">
				<input type="hidden" name="return_order_id" value="<?php echo $return_order_id; ?>">
				<input type="hidden" name="user_type" value="customer">
				<input type="hidden" name="status" value="pending">
				<input type="hidden" name="customer_id" value="<?php echo $order_item['customer_id']; ?>">
				<input type="hidden" name="order_item_id" value="<?php echo $order_item['order_item_id']; ?>">
				<input type="hidden" name="order_id" value="<?php echo $order_id; ?>">
				
				<?php

				if(count($get_refund_data)>0){
					
					$admin_action=$get_refund_data["status"];
					$customer_action=$get_refund_data["customer_decision"];
					
					if((($orders_status_data["order_return_pickup"]=='1' && $orders_status_data["initiate_return_pickup"]!='0' && $orders_status_data["initiate_return_pickup"]!='1') || ($orders_status_data["order_return_pickup"]='0' && $orders_status_data["initiate_return_pickup"]='1') || ($orders_status_data["order_return_pickup"]='0' && $orders_status_data["initiate_return_pickup"]='0'))){
					$customer_action="initiate_refund";	
					}

					if(($orders_status_data["order_return_pickup_again"]=='1' && trim($orders_status_data["initiate_return_pickup_again"])==trim('')) || ($orders_status_data["order_return_pickup_again"]=='0' && $orders_status_data["initiate_return_pickup_again"]=='1') || ($orders_status_data["order_return_pickup_again"]=='0' && $orders_status_data["initiate_return_pickup_again"]=='0')){
						
						$admin_action="refund_again";	
						$customer_action="initiate_refund_again";								
					}
					
					if($get_return_desired_refund_data[0]["order_return_decision_status"]=="not refunded"){
						
						$admin_action="not refunded";
						$customer_action="not refunded";	
					}
																
				}else{
						$admin_action="pending";
						$customer_action="pending";
				}
					
					
				?>
				<input type="hidden" name="admin_action" value="<?php echo $admin_action; ?>">
				<input type="hidden" name="customer_action" value="<?php echo $customer_action; ?>">
				<div class="input-group">
				   <textarea style="width:95%;" placeholder="Reply Message" name="description" id="description_<?php echo $order_item['order_item_id'];?>" class="form-control input-sm" cols="50" rows="4" <?php if(count($get_refund_data)>0 && $get_refund_data["disable_comm_flag"]=='1'){ echo "disabled";}?>></textarea>
				   <input type="file" name="image_attachment">
				   <span class="input-group-btn">
				   <button class="btn btn-primary btn-lg preventDflt"  type="submit">Send</button>
				   </span>
				</div>
			</form>
		</div>	
	</div><!---row-->