<?php 
	if($orders_status_data["type_of_order"]==""){ ?>

						
<?php	if($promotion_item!='' || ($promotion_surprise_gift_skus!='' && $order_delivered==1)){
?>
<tr class="margin-top margin-bottom">
		<td colspan="5">
	<div class="panel panel-default">	

					<div class="panel-heading  bg-warning" role="tab" data-toggle="collapse" href="#free_items_<?php echo $order_item_id;?>" style="cursor:pointer;">
						<h4 class="panel-title">Free Items	</h4>
					</div>
					<div id="free_items_<?php echo $order_item_id;?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
						
						<div class="panel-body">
						
						<?php 
						//print_r($order_item);
						
						if($promotion_item!=''){
							//$free_items_arr=$controller->get_free_skus_from_inventory_with_details($promotion_item,$promotion_item_num);
							foreach($free_items_arr as $free_items){
							?>
							<div class="free_items_style row">
							
								<div class="col-sm-5">
								<div class="row">
									<div class="col-md-4">						
									<img src="<?php echo base_url().$free_items->thumbnail?>" alt="FREE PRODUCTS">
									</div>
									<div class="col-md-8">						
										<div class="f-row"><?php echo ($free_items->sku_name!='') ? $free_items->sku_name: $free_items->product_name; ?></div>
										
										<small class="small-text">
										<?php
										$free_items->attribute_1_value= substr($free_items->attribute_1_value, 0, strpos($free_items->attribute_1_value, ":"));
										
										if(!empty($free_items->attribute_1))
											echo '<div class="f-row">'.$free_items->attribute_1." : ".$free_items->attribute_1_value.'</div>';
										if(!empty($free_items->attribute_2))
											echo '<div class="f-row">'.$free_items->attribute_2." : ".$free_items->attribute_2_value.'</div>';
										if(!empty($free_items->attribute_3))
											echo '<div class="f-row">'.$free_items->attribute_3." : ".$free_items->attribute_3_value.'</div>';
										if(!empty($free_items->attribute_4))
											echo '<div class="f-row">'.$free_items->attribute_4." : ".$free_items->attribute_4_value.'</div>';
										?>
										<div class="f-row">
										SKU : <?php echo $free_items->sku_id; ?>
										</div>
										
										<div class="f-row">
											Quantity : <?php 
											$id=$free_items->id;
											echo ($temp_quotient*$two[$id]);?>
										</div>
										<div class="f-row capitalize">
											Price per item :&nbsp;<del> <?php echo curr_sym;?><?php
											echo $free_items->selling_price;?>
											</del>
										</div>
										
										</small>
									</div>
								</div>
								</div>
							</div>
							<?php
							
							}
							
							
						}

						
						if($promotion_surprise_gift_type=='Qty'){
							//$free_surprise_items=$controller->get_free_skus_from_inventory_with_details($promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums);
							//print_r($free_surprise_items);
							//print_r($gift_with_count);
							
							foreach($free_surprise_items as $surprise_items){
							?>
							<div class="row free_items_style">
								<div class="col-sm-5">
									<div class="row">
									<div class="col-md-4">						
									<img src="<?php echo base_url().$surprise_items->thumbnail?>" alt="FREE PRODUCTS" >
									</div>
									<div class="col-md-8">						
										<div class="f-row"><?php echo ($surprise_items->sku_name!='') ? $surprise_items->sku_name : $surprise_items->product_name; ?></div>
										
										<small class="small-text">
										<?php
										$surprise_items->attribute_1_value= substr($surprise_items->attribute_1_value, 0, strpos($surprise_items->attribute_1_value, ":"));
										
										if(!empty($surprise_items->attribute_1))
											echo '<div class="f-row">'.$surprise_items->attribute_1." : ".$surprise_items->attribute_1_value.'</div>';
										if(!empty($surprise_items->attribute_2))
											echo '<div class="f-row">'.$surprise_items->attribute_2." : ".$surprise_items->attribute_2_value.'</div>';
										if(!empty($surprise_items->attribute_3))
											echo '<div class="f-row">'.$surprise_items->attribute_3." : ".$surprise_items->attribute_3_value.'</div>';
										if(!empty($surprise_items->attribute_4))
											echo '<div class="f-row">'.$surprise_items->attribute_4." : ".$surprise_items->attribute_4_value.'</div>';
										?>
											<div class="f-row">
											SKU : <?php echo $surprise_items->sku_id; ?>
											</div>
											<div class="f-row">
												Quantity : <?php 
												$id=$surprise_items->id;
												echo $gift_with_count[$id];?>
											</div>
											
											<div class="f-row capitalize">
												Price per item :&nbsp;
												<del> <?php 
												echo curr_sym.$surprise_items->selling_price;?></del>
											</div>
											
										</small>
										
									</div>
								</div>
								</div>
							</div>
							<?php
							
							}
							
						}
						?>
						
						</div>
					
					</div>
				
	</div><!---panel-body-->
	</td>
		</tr>
	<?php 
	}//checking promo free items
	
	if($promotion_surprise_gift_type!='' && $order_delivered!=1){
		?>
		<tr class="margin-top margin-bottom">
		<td colspan="5">
		<div class="row margin-top">
			<div class="col-md-12 bg-warning text-center">
			<span style="color:#d65f08;"><b><i><i class="fa fa-gift" aria-hidden="true"></i> This item is eligible for surprise Gift</i></b></span>
			</div>
		</div>
		</td>
		</tr>
		<?php
	}
	if(($promotion_surprise_gift_type==curr_code) && $order_delivered==1){
		
		$order_item_offer_obj=$controller->get_details_of_order_item($order_item_id);
		if(!empty($order_item_offer_obj)){
			
			if($order_item_offer_obj->status_of_refund=="refunded"){
			?>
			<tr class="margin-top margin-bottom">
		<td colspan="5">
			<div class="row margin-top">
				<div class="col-md-12 bg-warning text-center">
				<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> <?php echo curr_sym;?><?php echo $order_item['promotion_surprise_gift']; ?> was credited to your wallet (surprise gift)</i></b></span>
				</div>
			</div>
			</td>
		</tr>
			<?php
			}
			if($order_item_offer_obj->status_of_refund=="pending"){
				?>
				<tr class="margin-top margin-bottom">
		<td colspan="5">
			<div class="row margin-top">
				<div class="col-md-12 bg-warning text-center margin-top">
				<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> <?php echo curr_sym;?><?php echo $order_item['promotion_surprise_gift']; ?> will be credited to your wallet (surprise gift) Soon</i></b></span>
				</div>
			</div>
			</td>
		</tr>
				<?php
				
			}
		}
	}
	
	?>
	
		<?php
	} 
	?>