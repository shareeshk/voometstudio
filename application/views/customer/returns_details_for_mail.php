<?php 
			$get_returns_data=$controller->get_order_details_return_data_for_mail($order_item['order_item_id']);
			if(!empty($get_returns_data)){
			foreach($get_returns_data as $returns_data){ 
				 ?>				 
                <div id="accordion<?php echo $returns_data['return_order_id']; ?>" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="heading<?php echo $returns_data['return_order_id']; ?>">
							<div class="row">
							<div class="col-md-10">
							<?php
										
										$status1='';
										$status2='';
										
										$replaced_orders_by_prev_order_item_id_arr=$controller->get_replaced_orders_by_prev_order_item_id($order_item['order_item_id']);
										if(!empty($replaced_orders_by_prev_order_item_id_arr)){
											
										}
										$get_replacement_desired_refund_data=$controller->get_data_from_replacement_desired_return_order_id_for_mail($returns_data['return_order_id']);
										//print_r($get_replacement_desired_refund_data);
										$get_return_stock_notification_data=$controller->get_data_from_return_stock_notification_return_order_id_for_mail($returns_data['return_order_id']);
										
										if(!empty($get_return_stock_notification_data[0])){
												$status1.='Replacement';
												$status2.="<font style='color:red;font-weight:bold;'>Out of stock</font>";
										}else if(count($get_replacement_desired_refund_data[0])>0){
											$status1.='Replacement';
											
											if($get_replacement_desired_refund_data[0]["order_replacement_decision_status"]=="refunded"){
												
													$status2.="Completed (Refund Success)";
												}
												else if($get_replacement_desired_refund_data[0]["order_replacement_decision_status"]=="not refunded"){
													$status2.="Completed (Refund Failed)";
												}
												else if($get_replacement_desired_refund_data[0]["order_replacement_decision_status"]=="reject"){
													$status2.=" Rejected";
												}
												else if($get_replacement_desired_refund_data[0]["order_replacement_decision_status"]=="replace" && $get_replacement_desired_refund_data[0]["order_replacement_decision_customer_status"]!="cancel"){
													$status2.="Initiated";
												}
												else if($get_replacement_desired_refund_data[0]["order_replacement_decision_status"]=="replaced" || $get_replacement_desired_refund_data[0]["order_replacement_decision_status"]=="refund"){
													$status2.="Completed";
												}
												else if($get_replacement_desired_refund_data[0]["order_replacement_decision_customer_status"]=="cancel"){
													$status2.="Cancelled";
												}
												
												else if($get_replacement_desired_refund_data[0]["order_replacement_decision_customer_decision"]=="accept" && $get_replacement_desired_refund_data[0]["order_replacement_decision_customer_status"]!="cancel"){
													$status2.="Accepted";
												}
												else{
													$status2.="Requested";
												}
										}else{
										/////////////////////////
											$status1.="Refund";
											$get_return_desired_refund_data=$controller->get_data_from_returns_desired_return_order_id_for_mail($returns_data['return_order_id']);
											
											$return_refund_request_again_arr=$controller->return_refund_request_again($order_item['order_item_id']);
											if(count($return_refund_request_again_arr)>0){
												if($return_refund_request_again_arr["admin_action"]=="refunded"){
													$status2.="successful (second)";
												}
												else if($return_refund_request_again_arr["admin_action"]=="accepted"){
													$status2.="Accepted (second)";
												}
												else if($return_refund_request_again_arr["admin_action"]=="refund"){
													$status2.="Initiated (second)";
												}
												else{
													$status2.="Requested (second)";
												}
											}else{
	
												
												if($get_return_desired_refund_data[0]["order_return_decision_status"]=="refund"){
													$status2.="Initiated";
												}
												else if($get_return_desired_refund_data[0]["order_return_decision_status"]=="refunded"){
													$status2.="Successful";
												}
												else if($get_return_desired_refund_data[0]["order_return_decision_status"]=="not refunded"){
													$status2.="Failed";
												}
												else if($get_return_desired_refund_data[0]["order_return_decision_customer_status"]=="cancel"){
													$status2.="Cancelled";
												}
												else if($get_return_desired_refund_data[0]["order_return_decision_status"]=="reject"){
													$status2.="Rejected";
												}
												else if($get_return_desired_refund_data[0]["order_return_decision_customer_decision"]=="accept"){
													$status2.="Accepted";
												}
												else{
													$status2.="Requested";
												}
											}
										}
										
										echo '<span class="text-capitalize">'.$status1.'</span> <span class="text-capitalize">(Return ID : '.$returns_data['return_order_id'].' )</span> - <span class="text-uppercase bold">'.$status2.'</span>';
										
										?>
							</div>
							<div class="col-md-2 text-right">
							<i class="glyphicon glyphicon-plus" role="button" data-toggle="collapse" data-parent="#accordion<?php echo $returns_data['return_order_id']; ?>" href="#<?php echo $returns_data['return_order_id']; ?>" aria-expanded="true" aria-controls="<?php echo $order_id; ?>"></i>
							</div>
							</div>
						</div>
						
						<!--add class 'collapse' ----->
						
						<div id="<?php echo $returns_data['return_order_id']; ?>" class="panel-collapse collapse in replacement_div" role="tabpanel" aria-labelledby="headingThree">
							<div class="panel-body">
		
										<?php						
										
										$get_data_from_replacement_desired_return_order_id=$controller->get_data_from_replacement_desired_return_order_id_for_mail($returns_data['return_order_id']);
										
										$get_data_from_returns_desired_return_order_id=$controller->get_data_from_returns_desired_return_order_id_for_mail($returns_data['return_order_id']);
											
											$get_data_from_return_stock_notification_return_order_id=$controller->get_data_from_return_stock_notification_return_order_id_for_mail($returns_data['return_order_id']);
		
											if(!empty($get_data_from_replacement_desired_return_order_id)){
												/*echo '<pre>';
												print_r($get_data_from_replacement_desired_return_order_id);
												echo '</pre>';*/
												
												foreach($get_data_from_replacement_desired_return_order_id as $return_replacement_data){ ?>
												
												
	<?php
	
	$get_replacement_data=$controller->get_order_details_admin_acceptance_refund_data_for_replacement_for_mail($return_replacement_data["order_item_id"]);
	$sku_requested=$controller->get_inventory_info_by_inventory_id($return_replacement_data["replacement_with_inventory_id"])->sku_id;
	if(!empty($get_data_from_returns_desired_return_order_id)){
		$refund_requested=$get_data_from_returns_desired_return_order_id[0]["quantity_refund"];
	}else{
		$refund_requested=0;
	}
	//"Quantity Requested":$return_replacement_data["quantity_replacement"]+$refund_requested;
	
	$each_replaced_data=$controller->get_replacement_item_data($return_replacement_data['replacement_with_inventory_id']);

	
	//$get_returns_data=$controller->get_order_details_return_data($order_item['order_item_id']);
	//$get_data_from_replacement_desired_return_order_id=$controller->get_data_from_replacement_desired_return_order_id($returns_data['return_order_id']);
	
	/*echo '<pre>';
	print_r($get_data_from_replacement_desired_return_order_id);
	echo '</pre>';
		*/		
		?>
		
	<div class="row">
	<?php foreach($each_replaced_data as $details){ ?>
	<div class="col-md-6">
			<div class="row">
			<div class="col-md-3">
				<a href="<?php echo base_url()?>detail/<?php echo $controller->sample_code().$details['id'];?>">
				<img src="<?php echo base_url().$details['thumbnail']?>" alt="" class="img-responsive"></a>		
			</div>
														
			<div class="col-md-9">
				<div class="headingtext bold"><u>Request Details</u></div>
				<?php 
                                //$product_name=$controller->get_product_name($order_item['product_id']);
                                
                                if($order_item['ord_sku_name']!=''){
                                    $product_name=$order_item['ord_sku_name'];
                                }else{
                                    $product_name=$controller->get_product_name($order_item['product_id']);
                                }
                                ?>
				<form target="_blank" id="write_review_<?php echo $order_items['order_item_id']; ?>" action="<?php echo base_url()."write_review" ?>" method="post">
				<input type="hidden" value="<?php echo $order_item['inventory_id']; ?>" name="inv_id" id="inv_id">
				<input type="hidden" value="<?php echo $product_name; ?>" name="pro_name">
				<input type="hidden" value="<?php echo $this->session->userdata('customer_id') ?>" name="customer_id" id="customer_id">
				<input type="hidden" value="1" name="rate_order" id="rate_order">
				<input type="hidden" value="<?php echo $order_items['order_item_id']; ?>" name="rate_order_order_item_id" id="rate_order_order_item_id">
				</form>
				
				<a href="<?php echo base_url()?>detail/<?php echo $controller->sample_code().$details['id'];?>"><?php echo $product_name;?></a>
				

				
				<div class="f-row small-text">
											
					<?php
						//$details['attribute_1_value']= substr($details['attribute_1_value'], 0, strpos($details['attribute_1_value'], ":"));
						
						if(!empty($details['attribute_1']))
							echo '<div class="f-row">'.$details['attribute_1']." : ".$details['attribute_1_value'].', ';
						if(!empty($details['attribute_2']))
							echo ''.$details['attribute_2']." : ".$details['attribute_2_value'].'';
							echo '</div>';
						if(!empty($details['attribute_3']))
							echo '<div class="f-row">'.$details['attribute_3']." : ".$details['attribute_3_value'].'</div>';
						if(!empty($inv_obj->attribute_4))
							echo '<div class="f-row">'.$details['attribute_4']." : ".$details['attribute_4_value'].'</div>';
					?>
					<div class="f-row">SKU : <?php echo $sku_requested; ?></div>
					<div class="f-row">Quantity : <?php echo $return_replacement_data["quantity_replacement"]; ?></div>
				</div>															 
								
			</div>
		
	</div>									
	</div><!---col-md-6---->
	
		<?php 
		
	}
		?>
																
																
	<div class="col-md-6">

			<div class="f-row">
				
				Differential amount to be paid by <?php echo $return_replacement_data["paid_by_original"]; ?>:  
				
				<?php 
				if(!empty($get_replacement_data)){
				?>
				<strike><?php echo curr_sym.$return_replacement_data["amount_paid_original"]?></strike>
				<?php
				}else{
				echo curr_sym.$return_replacement_data["amount_paid_original"];
				?> <span id="rep_amount_<?php echo $returns_data['return_order_id'] ?>" data-toggle="popover" data-trigger="click" data-placement="bottom" class="cursor-pointer rep_amount"  content_id="#customer_rep_req_<?php echo $returns_data['return_order_id']; ?>" onclick="toggle_content(this)">[<i class="fa fa-question" aria-hidden="true"></i>]</span>
				<?php } ?>
				
			<script>
			$(document).ready(function(){
				$('#rep_amount_<?php echo $returns_data['return_order_id'] ?>').popover({
					html : true,
					content: function() {
						if($(window).width() > 480){
							$('#customer_rep_req_<?php echo $returns_data['return_order_id'] ?>').hide();
							return $('#customer_rep_req_<?php echo $returns_data['return_order_id'] ?>').html();	
						}
					}
				});
			});
			</script>
					

					<div id="customer_rep_req_<?php echo $returns_data['return_order_id'] ?>" style="display:none;">
					
					<table class="table table-hover table-bordered" >
				
					<tbody>
					<?php
						//print_r($return_replacement_data);
						
						$purchased_item_price=($return_replacement_data["return_value_each_inventory_id"])*($return_replacement_data["quantity_replacement"]);
					?>
					<tr>
						<td><p><small>Purchased Item Price</small></p></td>
						<td><p><small><?php echo $return_replacement_data["quantity_replacement"]." * ".curr_sym.$return_replacement_data["return_value_each_inventory_id"]."";?></small></p></td>
						<td><p><small><?php echo curr_sym.$purchased_item_price;?></small></p></td>
					</tr>
					<?php
						$total_item_price=($return_replacement_data["replacement_value_each_inventory_id"])*($return_replacement_data["quantity_replacement"]);
					?>	
					<tr>
						<td><p><small>Replacement Item Price</small></p></td>
						<td><p><small><?php echo $return_replacement_data["quantity_replacement"]." * ".curr_sym.$return_replacement_data["replacement_value_each_inventory_id"]."";?></small></p></td>
						<td><p><small><?php echo curr_sym.$total_item_price;?></small></p></td>
					</tr>

					<tr>
						<td colspan="2"><p><small>Differential Item Price</small></p></td>
						<td><p><small><?php echo curr_sym.$return_replacement_data["amount_paid_item_price_original"];?></small></p></td>
					</tr>
					<tr>
					
						<td colspan="2"><p><small>
						
						Shipping Charge for Replacement of <?php echo $return_replacement_data["quantity_replacement"];?> units 
						<?php
						$percentage=$return_replacement_data["shipping_charge_percent_paid_by_customer"];
						//if($percentage!=100){
						echo '<br><i style="color:blue;">( '.$percentage.' % of original shipping charge ) </i>';
						//}
						?>
						</small></p></td>
						
						<td><p><small><?php echo curr_sym.$return_replacement_data["shipping_charge_requested"];?></small></p></td>
					</tr>
					
					<?php
					if($total_item_price>$purchased_item_price){
						?>
					<!--	<tr>
						<td><p><small>You Pay1</small></p></td> <td><p><small><?php //echo curr_sym.($total_item_price-$purchased_item_price);?></small></p></td>
						</tr>-->
						
						<?php
					}
					?>
					<?php
						if($total_item_price<$purchased_item_price){
							?>
						<!--<tr>
						<td><p><small>You Get </small></p></td>
						<td><p><small><?php //echo curr_sym.($purchased_item_price-$total_item_price);?></small></p></td>
						</tr>-->

							<?php
						}
					?>
					<?php if($return_replacement_data["amount_paid_original"]>=0){ ?>
					<tr>
						<td colspan="2"><p><small>Total amount to be paid by <?php echo $return_replacement_data["paid_by_original"]; ?></small></p></td>
						<td><p><small><?php echo curr_sym.($return_replacement_data["amount_paid_original"]);?></small></p></td>
					</tr>
					<?php }?>
					
					
					</tbody>
					</table>					
					
					</div>					
			</div>			
			<div class="f-row">
			<?php
				if($return_replacement_data["paid_by_original"]=="admin"){
					echo "Refund Method : ".$return_replacement_data["refund_method"]."";
				}
				if($return_replacement_data["paid_by_original"]=="customer"){
					if($return_replacement_data["balance_amount_paid_by"]!=""){
					echo "Payment Method : ".$return_replacement_data["balance_amount_paid_by"];
					}
				}
				/*if($return_replacement_data["paid_by"]!=""){
					echo "<tr><td><p><small>Will be paid by</small></p></td><td><p><small>".$return_replacement_data["paid_by"]."</small></p></td></tr>";
				}*/
				
			?>
			</div>			
	<!-------------- customer requested details in replacement ends --------------------->
														
	</div>
	
	</div><!---row1---->												
			<!----------- contact buyer replacement starts------------------>	

	
	<!-------------- admin accepted details in replacement starts ------------------->
	<?php
	
		$get_replacement_data=$controller->get_order_details_admin_acceptance_refund_data_for_replacement_for_mail($order_item['order_item_id']);

		if(!empty($get_replacement_data)){

		//it returns empty array() when order_replacement_decision_data is empty with the order_item_id;
		
		//print_r($get_replacement_data)
		
				$str="";
				
				if($return_replacement_data["paid_by"]=="customer"){
					if($return_replacement_data["paid_by"]=="customer" && $return_replacement_data["payment_status"]=="paid"){
						if($return_replacement_data["balance_amount_paid_by"]=="cod"){
							$str="(Amount paid by COD)";
						}
						else{
							$str="(Paid through ".$return_replacement_data['balance_amount_paid_by'].")";
						}
					}
					if($return_replacement_data["paid_by"]=="customer" &&  $return_replacement_data["payment_status"]=="pending"){
						$str="(Amount Due)";
					}
				}else{
					$str="(".ucwords($get_replacement_data["status"]).")";
				}
				
	
	?>
	
		<div class="row margin-bottom">
		<div class="col-md-6">
		<div class="row">
			<div class="col-sm-9 col-sm-offset-3">
				<div class="f-row bold">
				<u>Vendor response</u>
</div>
				<div class="f-row">
				Quantity Accepted : <?php echo $get_replacement_data["quantity"];?>
				</div>
				<div class="f-row">
					 Differential price: 
					 <?php echo curr_sym;?><?php echo $get_replacement_data["balance_amount"]; ?> <span id="admin_rep_amount_<?php echo $returns_data['return_order_id'] ?>" data-toggle="popover" data-trigger="click" data-placement="bottom" class="cursor-pointer admin_rep_amount" content_id="#admin_rep_amount_content<?php echo $returns_data['return_order_id'] ?>" onclick="toggle_content(this)">[<i class="fa fa-question" aria-hidden="true"></i>] </span> <?php //echo $str ?>
					 
					<script>
					$(document).ready(function(){
						$('#admin_rep_amount_<?php echo $returns_data['return_order_id'] ?>').popover({
							html : true,
							content: function() {
							  
								if($(window).width() > 480){
									$('#admin_rep_amount_content<?php echo $returns_data['return_order_id'] ?>').hide();
									return $('#admin_rep_amount_content<?php echo $returns_data['return_order_id'] ?>').html();	
								}
							}
						});
					});
					</script>
								
					<div id="admin_rep_amount_content<?php echo $returns_data['return_order_id']; ?>" style="display:none;">
						
						<table class="table table-hover table-bordered">
						
						<tbody>

						<?php
							$total_item_price=($get_replacement_data["replacement_value_each_inventory_id"])*($get_replacement_data["quantity"]);
						?>

						<tr>				
						<td><p><small>Total Requested Item Price </small></p></td>
						<td><p><small><?php echo $get_replacement_data["quantity"]." * ".curr_sym.$get_replacement_data["replacement_value_each_inventory_id"]." each";?></small></p></td>
						<td><p><small><?php echo curr_sym.$total_item_price;?></small></p></td>
						
						</tr>
							
						<?php
							$purchased_item_price=($return_replacement_data["return_value_each_inventory_id"])*($get_replacement_data["quantity"]);
						?>
						<tr>				
						<td><p><small>Total Purchased Item Price</small></p></td>
						<td><p><small> <?php echo $get_replacement_data["quantity"]." * ".curr_sym.$return_replacement_data["return_value_each_inventory_id"];?></small></p></td>
						<td><p><small><?php echo curr_sym.$purchased_item_price;?></small></p></td>
						</tr>
						<tr>
							<td colspan="2"><p><small>Differential Item Price</small></p></td>
							<td><p><small><?php echo curr_sym.$get_replacement_data["balance_amount_item_price"];?></small></p></td>
						</tr>
						<?php
							if(count($get_replacement_data)>0){
						?>
						<tr>				
						<td colspan="2"><p><small>Shipping Charge for Replacement of <?php echo $get_replacement_data["quantity"];?> units
						<?php
							//$percentage=$controller->get_customer_pays_shipping_charge_percentage($data_from_replacement_desired_return_order_id["replacement_with_inventory_id"]);
							if($percentage!=100){
							echo '<br><i style="color:blue;">( '.$percentage.' % of original shipping charge ) </i>';
							}
						?>
						</small></p></td><td><p><small> 
						<?php echo curr_sym.$get_replacement_data["shipping_charge_replacement"];?></small></p></td>
						</tr>
						<?php
							}
						?>
								
					
						<tr>				
						<td colspan="2"><p><small> Total balance amount  
						
						<?php echo ($get_replacement_data["paid_by"]!="")? "To be Paid By ".$get_replacement_data["paid_by"]."<i>".$str."</i>" : "";?> 
						
						</small></p></td><td><p><small> <?php echo curr_sym.$get_replacement_data["balance_amount"]; ?></small></p></td>
						</tr>
						
						<?php
							if($total_item_price>$purchased_item_price){
								?>
							<!--	<tr>				
						<td><p><small> You Pay</small></p></td><td><p><small>  <?php //echo curr_sym.$get_replacement_data["balance_amount"];?>
								
								</small></p></td>
						</tr>-->
								<?php
							}
						?>
						<?php
							if($total_item_price<$purchased_item_price){
								?>
					<!--	<tr>				
						<td><p><small>You Get2</small></p></td><td><p><small> </b> <?php //echo curr_sym.$get_replacement_data["balance_amount"];?></small></p></td>
						/tr>-->
								<?php
							}
						?>
												
					</tbody>
					</table>
					
					</div> 
					
				</div>
				
				
				<?php
			if($get_replacement_data["return_shipping_concession"]!="0" && $get_replacement_data["pickup_identifier"]!="before replace"){
			?>
				<div class="f-row">
				Return shipping Concession : <?php echo curr_sym;?><?php echo $get_replacement_data["return_shipping_concession"];?>
				</div>
				
				<?php if(($get_replacement_data["paid_by"]=="admin") && ($get_replacement_data["balance_amount"]>0)){
					?>
					
				<div class="f-row">Total Amount Admin has to pay : <?php echo curr_sym;?><?php echo ($get_replacement_data["return_shipping_concession"]+$get_replacement_data["balance_amount"]);?>
				</div>
				
				<?php
				}
				
			}
				?>
				<div class="f-row">
				<p><strong>Comments:</strong>
				<span class="more">
				<?php echo ucwords($get_replacement_data["comments"]);?></p>
				</span>
				</div>
				<div class="f-row" id="accept_reject_replacement_status_<?php echo $get_replacement_data["order_item_id"];?>">
				
				<?php
						if($get_replacement_data["customer_decision"]=="pending" && $get_replacement_data["customer_status"]=="pending"){
					?>
					<input type="button" class="btn btn-success btn-sm preventDflt" value="Accept" onclick="accept_reject_replacement_statusFun('accept',<?php echo $get_replacement_data
					["order_item_id"];?>)">
					<input type="button" class="btn btn-danger btn-sm preventDflt" value="Reject" onclick="accept_reject_replacement_statusFun('reject',<?php echo $get_replacement_data
					["order_item_id"];?>)">
					<?php
						}
					?>
					
				<?php
					if($get_replacement_data["customer_decision"]=="pending" && $get_replacement_data["customer_status"]=="cancel"){
				?>
					<p class="text-warning "><strong>Customer Decision: No Decision Made</strong></p>
				<?php
					}
				?>
				
				<?php
					if($get_replacement_data["allow_cust_to_make_decision_flag"]=="0" && $get_replacement_data["customer_decision"]=="accept"){
						echo "<b>Status:</b> Admin Accepted";
					}
					else if($get_replacement_data["allow_cust_to_make_decision_flag"]=="1" && $get_replacement_data["customer_decision"]=="accept"){
						echo "<b>Status:</b> Customer Accepted";
					}
					else if($get_replacement_data["customer_decision"]=="reject"){
						echo "<b>Status:</b> Request rejected";
					}
				?>
				
				</div>
				
	
	
			</div>
			</div>
		</div>
		
		<div class="col-sm-6">
			<div class="f-row">	
<?php if($get_replacement_data["status"]!="reject"){?>
			Type of Replacement :
		<?php }else{?>
			Reason : 
		<?php } ?>			
		<?php echo $controller->get_primary_replacement_reason_admin($get_replacement_data["sub_reason_return_id"]);?>
			</div>
			
			
			<div class="f-row">
			
				<?php 
				$orders_status_data=$controller->get_orders_status_data($order_item["order_item_id"]);
			
				/////////////////////
				
				if($get_replacement_data["pickup_identifier"]=="before replace" && $return_replacement_data['order_replacement_decision_customer_status']!="cancel"){
							
							if($orders_status_data["initiate_replacement_pickup"]=='1' && $orders_status_data["order_replacement_pickup"]==0){
								$replacement_option_status='It is required to ship the purchased item';
							}
							if($orders_status_data["initiate_replacement_pickup"]=='1' && $orders_status_data["order_replacement_pickup"]==1){
								$replacement_option_status='We have received your item.';
							}
							if($orders_status_data["initiate_replacement_pickup"]=='0' && $orders_status_data["order_replacement_pickup"]==0){
								$replacement_option_status='There is no need to return purchased item';
							}
							if($orders_status_data["initiate_replacement_pickup"]=='' && $orders_status_data["order_replacement_pickup"]==0){
								$replacement_option_status='';
							}
							echo $replacement_option_status;
				}
				if($get_replacement_data["pickup_identifier"]=="after replace" && $return_replacement_data['order_replacement_decision_customer_status']!="cancel"){
						if($orders_status_data["order_replacement_pickup"]==1 ){
							$replacement_option_status="We have received your item. Please wait while we initiate an action";
						}else{
							$replacement_option_status='';
						}
						
						echo $replacement_option_status;
				}
				
				?>
			</div>
			<?php if($get_replacement_data["pickup_identifier"]!="before replace" && $return_replacement_data['order_replacement_decision_customer_status']!="cancel"){ ?>
			<div class="f-row">
				<?php 
				if($get_replacement_data["return_shipping_concession_chk"]=="yes"){
					echo "Return shipping charge: To be borne by Admin.";
				}else{
					echo "Return shipping charge: To be borne by customer.";
				}
				?>
			</div>
			
			<?php
					if(($return_replacement_data["paid_by"]=="customer" || $return_replacement_data["paid_by"]=="") && ($get_replacement_data["return_shipping_concession_chk"]=="yes")){
						
						if($get_replacement_desired_refund_data[0]["order_replacement_decision_status"]=="replaced"){
							echo '<div class="f-row">(Please wait while we initiate for a refund.)</div>';
						}
						if($get_replacement_desired_refund_data[0]["order_replacement_decision_status"]=="refund"){
							echo '<div class="f-row">(Refund initiated.)</div>';
						}
						if($get_replacement_desired_refund_data[0]["order_replacement_decision_status"]!="replaced" && $get_replacement_desired_refund_data[0]["order_replacement_decision_status"]!="refund" && $get_replacement_desired_refund_data[0]["order_replacement_decision_status"]!="refunded"){
							echo '<div class="f-row">(Return Shipping charge will be refunded once replacement is completed.)</div>';
						 }
						if($get_replacement_desired_refund_data[0]["order_replacement_decision_status"]=="refunded"){
							
							if($get_replacement_desired_refund_data[0]["refund_method"]=="Bank Transfer"){
								echo '<div class="f-row">Refunded to ';
								$return_order_id=$get_replacement_desired_refund_data[0]["return_order_id"];
								$refund_bank_id=$get_replacement_desired_refund_data[0]['refund_bank_id'];	
								$bank_detail_arr=$controller->get_bank_details($refund_bank_id);
								
								echo '<a id="bank_detail_ret_'.$return_order_id.'" data-toggle="popover" data-trigger="click" data-placement="bottom" class="bank_detail_replace_refund1" content_id="#bank_detail_ret_div_'.$return_order_id.'" onclick="toggle_content(this)" style="cursor:pointer;"><u>Bank Account</u></a>';
								
								
								echo '<div id="bank_detail_ret_div_'.$return_order_id.'" style="display:none;">'.$bank_detail_arr.'</div>';
								echo '</div>';
								?>
								
							<script>
							$(document).ready(function(){
								$('#bank_detail_ret_<?php echo $return_order_id; ?>').popover({
									html : true,
									content: function() {
										if($(window).width() > 480){
											$('#bank_detail_ret_div_<?php echo $return_order_id; ?>').hide();
											return $('#bank_detail_ret_div_<?php echo $return_order_id; ?>').html();	
										}
									}
								});
							});
							</script>
								
								<?php 
								
								echo '<div class="f-row">Transaction ID: '.$get_replacement_desired_refund_data[0]['transaction_id'].'</div>';
								echo '<div class="f-row">Transaction Date: '.date("d-m-Y", strtotime($get_replacement_desired_refund_data[0]['transaction_date'])).'</div>';
								echo '<div class="f-row">Transaction Time: '.$get_replacement_desired_refund_data[0]['transaction_time'].'</div>';
							}else{
								echo '<div class="f-row">Refunded to '.$get_replacement_desired_refund_data[0]["refund_method"].'</div>';
							}
							
						 } 
						
					 } 
					 ?>
			<?php } ?>
			
			<div class="f-row">
			<div class="form-horizontal">
			<?php
			if(count($get_data_from_replacement_desired_return_order_id)!=0){ 
			
				foreach($get_data_from_replacement_desired_return_order_id as $data_from_replacement_desired_return_order_id){
					
					if($data_from_replacement_desired_return_order_id["paid_by"]=="customer" && $data_from_replacement_desired_return_order_id["payment_status"]=="pending" && $data_from_replacement_desired_return_order_id["order_replacement_decision_customer_status"]!='cancel'){
						
						?>
						
						<?php
							if($data_from_replacement_desired_return_order_id["balance_amount_paid_by"]!="cod"){
						?>
						<?php
							$orders_status_data=$controller->get_orders_status_data($order_item['order_item_id']);
							 	
							if(($orders_status_data["order_replacement_pickup"]=='1' && $orders_status_data["initiate_replacement_pickup"]=='') || ($orders_status_data["order_replacement_pickup"]=='1' && $orders_status_data["initiate_replacement_pickup"]!='') || ($orders_status_data["order_replacement_pickup"]=='0' && $orders_status_data["initiate_replacement_pickup"]!='')){
								$paynowbutton="yes";
							}
							else{
								$paynowbutton="no";
							}
							
					if($paynowbutton=="yes"){
						?>
						<div class="form-group">
						<div class="col-md-12 bold">
						<?php
								if($data_from_replacement_desired_return_order_id["balance_amount_paid_by"]!=""){
									?>
									Please pay the differential item price, so that we can process your request.
									<?php
								}else{
							?>
							Please select the payment method and pay the differential item price, so that we can process your request.
							<?php } ?>
						</div>
						</div>
						<div class="form-group">
						<div class="col-md-6">
						<select  class="form-control" id="customer_payment_ways_<?php echo $data_from_replacement_desired_return_order_id['order_item_id']?>" onchange="customer_payment_waysFun(this)" <?php if($paynowbutton=="no"){echo "disabled";}?>>
						<?php
								if($data_from_replacement_desired_return_order_id["balance_amount_paid_by"]==""){
									?>
									<option value="" selected>- Choose Payment Option</option>
									<?php
								}
							?>
						<option value="online_payment" <?php if($data_from_replacement_desired_return_order_id["balance_amount_paid_by"]=="online_payment"){ echo "selected";}?>>Payment Gateway</option>
						<option value="wallet" <?php if($data_from_replacement_desired_return_order_id["balance_amount_paid_by"]=="wallet"){ echo "selected";}?>>Wallet</option>
						<option value="cod">COD</option>		
						</select>
						</div>
						</div>
						<div class="form-group">
						<div class="col-md-6" id="wallet_<?php echo $data_from_replacement_desired_return_order_id['order_item_id']?>" <?php if($data_from_replacement_desired_return_order_id["balance_amount_paid_by"]=="wallet"){ echo "style='display:block;'";}else{ echo "style='display:none;'";}?>>
						[Your wallet balance is <?php echo $controller->get_customer_wallet_amount($this->session->userdata("customer_id"));?>]
						</div>
						</div>
						
						<script>
						function  customer_payment_waysFun(obj){
							if(obj.value=="wallet"){
								document.getElementById("wallet_<?php echo $data_from_replacement_desired_return_order_id['order_item_id']?>").style.display="block";
							}
							else{
								document.getElementById("wallet_<?php echo $data_from_replacement_desired_return_order_id['order_item_id']?>").style.display="none";
							}
						}
						function payCustomerBalanceFun(){
							var customer_payment_ways=document.getElementById("customer_payment_ways_<?php echo $data_from_replacement_desired_return_order_id['order_item_id']?>").value;
							if(customer_payment_ways=="wallet"){
								var wallet_payment_exists_or_not="<?php echo ($data_from_replacement_desired_return_order_id['amount_paid']-($controller->get_customer_wallet_amount($this->session->userdata("customer_id"))));?>";
								if(wallet_payment_exists_or_not<0){
									$.ajax({
										url:"<?php echo base_url()?>Account/payment_by_customer",
										type:"post",
										data:"amount_paid=<?php echo $data_from_replacement_desired_return_order_id['amount_paid'];?>&return_order_id=<?php echo $returns_data['return_order_id'];?>&order_item_id=<?php echo $data_from_replacement_desired_return_order_id['order_item_id'];?>&customer_payment_ways="+customer_payment_ways,
										success:function(data){
											if(data){
												if(customer_payment_ways=="cod"){
													
													/*bootbox.alert({
													  size: "small",
													  message: 'Sucessfully submited',
													  callback: function () { location.reload(); }
													});	*/
                                                    alert('Sucessfully submited');
													location.reload();
												}else{
													/*bootbox.alert({
													  size: "small",
													  message: 'Sucessfully Paid',
													  callback: function () { location.reload(); }
													});*/
                                                    alert('Sucessfully Paid');
													location.reload();
												}
											}
										}
									})
								}
								else{
									
									/*bootbox.alert({
									  size: "small",
									  message: 'Insufficient wallet amount. Please select the other payment mode.',
									  callback: function () { return false; }
									  
									});	*/

                                    alert('Insufficient wallet amount. Please select the other payment mode.');
                                    return false;
								}
							}
							else{
									$.ajax({
										url:"<?php echo base_url()?>Account/payment_by_customer",
										type:"post",
										data:"amount_paid=<?php echo $data_from_replacement_desired_return_order_id['amount_paid'];?>&return_order_id=<?php echo $returns_data['return_order_id'];?>&order_item_id=<?php echo $data_from_replacement_desired_return_order_id['order_item_id'];?>&customer_payment_ways="+customer_payment_ways,
										success:function(data){
											if(data){
												if(customer_payment_ways=="cod"){
												
													/*bootbox.alert({
													  size: "small",
													  message: 'Your payment option has been successfully submitted',
													  callback: function () { location.reload(); }
													});*/
                                                    alert('Your payment option has been successfully submitted');
                                                    location.reload();
												}
												else{
													
													/*bootbox.alert({
													  size: "small",
													  message: 'Successfully paid',
													  callback: function () { location.reload(); }
													});*/
                                                    alert('Successfully paid');
                                                    location.reload();
													//location.reload();
												}
											}
										}
									})
								
							}
						}
						</script>
						<div class="form-group">
						<div class="col-md-6">
						<input type="button" class="button btn-block preventDflt" value="Pay Now" onclick="payCustomerBalanceFun()" <?php if($paynowbutton=="no"){echo "disabled";}?>>
						</div>
						</div>
									<?php
									}
								}
							}

					if($data_from_replacement_desired_return_order_id["paid_by"]=="admin" && $data_from_replacement_desired_return_order_id["status_of_refund"]!="yes"  && $data_from_replacement_desired_return_order_id["order_replacement_decision_status"]!="refund" && $data_from_replacement_desired_return_order_id["order_replacement_decision_customer_status"]!='cancel'){
						echo '<div class="f-row">Please wait while we initiate refund.</div>';
					}
					if($data_from_replacement_desired_return_order_id["paid_by"]=="admin" && $data_from_replacement_desired_return_order_id["status_of_refund"]!="yes" && $data_from_replacement_desired_return_order_id["order_replacement_decision_status"]=="refund"){
						echo '<div class="f-row">Refund is initiated.</div>';
					}
					if($data_from_replacement_desired_return_order_id["paid_by"]=="admin" && $data_from_replacement_desired_return_order_id["status_of_refund"]=="yes" && $data_from_replacement_desired_return_order_id["order_replacement_decision_status"]=="refunded"){
						
						
						if($data_from_replacement_desired_return_order_id['refund_method']=="Bank Transfer"){
								echo '<div class="f-row">Refunded to ';
								$return_order_id=$data_from_replacement_desired_return_order_id["return_order_id"];
								$refund_bank_id=$data_from_replacement_desired_return_order_id['refund_bank_id'];	
								$bank_detail_arr=$controller->get_bank_details($refund_bank_id);
								
								echo '<a id="bank_detail_rett_'.$return_order_id.'" data-toggle="popover" data-trigger="click" data-placement="bottom" class="bank_detail_replace_refund" content_id="#bank_detail_rett_div_'.$return_order_id.'" onclick="toggle_content(this)" style="cursor:pointer;"><u>Bank Account</u></a>';
								
								
								echo '<div id="bank_detail_rett_div_'.$return_order_id.'" style="display:none;">'.$bank_detail_arr.'</div>';
								echo '</div>';
								?>
								<script>
								$(document).ready(function(){
									$('#bank_detail_rett_<?php echo $return_order_id; ?>').popover({
										html : true,
										content: function() {
											if($(window).width() > 480){
												$('#bank_detail_rett_div_<?php echo $return_order_id; ?>').hide();
												return $('#bank_detail_rett_div_<?php echo $return_order_id; ?>').html();	
											}
										}
									});
								});
								</script>
								
								<?php 
								echo '<div class="f-row">Transaction ID: '.$data_from_replacement_desired_return_order_id['transaction_id'].'</div>';
								echo '<div class="f-row">Transaction Date: '.date("d-m-Y", strtotime($data_from_replacement_desired_return_order_id['transaction_date'])).'</div>';
								echo '<div class="f-row">Transaction Time: '.$data_from_replacement_desired_return_order_id['transaction_time'].'</div>';
						}else{
							echo '<div class="f-row">Refunded to '.$data_from_replacement_desired_return_order_id["refund_method"].'</div>';
						}
					}
				}
			}
		?>
			</div>
			</div>
			
			<?php if($return_replacement_data['order_replacement_decision_customer_status']=="cancel"){ ?>
			<div class="f-row">
			<?php
					if($orders_status_data["order_replacement_pickup"]==0 && $get_replacement_data["pickup_identifier"]=="after replace"){
						echo '<div class="f-row">We have cancelled your replacement request.</div>';
					}
					if($orders_status_data["order_replacement_pickup"]==1 && $get_replacement_data["pickup_identifier"]=="after replace"){
						echo '<div class="f-row">We have cancelled your replacement request. Please wait as we initiate for a refund.</div>';
					}
					if($orders_status_data["order_replacement_pickup"]==0 && $get_replacement_data["pickup_identifier"]=="before replace" &&  ($return_replacement_data['payment_status']=="pending" || $return_replacement_data['payment_status']=="")){
						echo '<div class="f-row">We have cancelled your replacement request.</div>';
					
					}
					if($orders_status_data["order_replacement_pickup"]==0 && ($get_replacement_data["pickup_identifier"]=="before replace" &&  $return_replacement_data['payment_status']=="paid")){
					echo '<div class="f-row">We have cancelled your replacement request successfully. please wait as we initiate for a refund</div>';
					}
					if($return_replacement_data["order_replacement_decision_refund_status"]=="refund"){
						echo '<div class="f-row">Refund initiated</div>';
					}
					if($return_replacement_data["order_replacement_decision_refund_status"]=="refunded"){
						echo '<div class="f-row">Refunded to '.$return_replacement_data["refund_method"].'</div>';
					}

			?>
			</div>
			<?php } ?>
		</div>
	</div>

		<!---replacement cancel status--->
	
			<?php
				//$get_data_from_replacement_desired_return_order_id=$controller->get_data_from_replacement_desired_return_order_id($returns_data['return_order_id']);

		if($return_replacement_data['order_replacement_decision_customer_status']=="cancel"){
			$return_order_id=$return_replacement_data['return_order_id'];
			
			?>
			
	<div class="row margin-bottom margin-top">	
		<div class="col-sm-6">
		<div class="row">
			<div class="col-sm-9 col-sm-offset-3">
		<?php	
			if($return_replacement_data['order_replacement_decision_customer_status']=="cancel"){
				if(($orders_status_data["order_replacement_pickup"]==0 && $get_replacement_data["pickup_identifier"]=="before replace" &&  $return_replacement_data['payment_status']=="paid") || ($orders_status_data["order_replacement_pickup"]==1 && $get_replacement_data["pickup_identifier"]=="after replace")){
				?>
				<div class="f-row bold"><u>Refund Details</u></div>
				
				
				<?php
				
					$total_refund_to_be_done=0;
					$purchased_item_price=($order_item["product_price"])*($get_replacement_data["quantity"]);
					$total_refund_to_be_done+=$purchased_item_price;
					//print_r($data_from_replacement_desired_return_order_id);	//$purchased_item_price_shipping_charge=($order_item["shipping_charge"])*($get_replacement_data["quantity"]);
					$purchased_item_price_shipping_charge=round(($order_item["shipping_charge"]/$order_item["quantity"])*$get_replacement_data["quantity"]);
				?>
				<?php //print_r($get_replacement_data); ?>
				
					<?php
					if($return_replacement_data["shipping_charge_applied_cancels_chk"]==1){
						$shipping_charge_for_purchased_item=($order_item["shipping_charge"]/$order_item["quantity"])*($get_replacement_data["quantity"]);
					
						$total_refund_to_be_done+=$shipping_charge_for_purchased_item;
						?>
					<?php
					}
					?>
					
					<?php
					if($return_replacement_data["return_shipping_concession_cancels_chk"]==1){
					
						$total_refund_to_be_done+=$get_replacement_data["return_shipping_concession"];
					}
					?>
					
					<?php
					if($get_replacement_data["pickup_identifier"]=="before replace"){
						$total_refund_to_be_done=$total_refund_to_be_done-$purchased_item_price;
					}
					if($return_replacement_data["paid_by"]=="customer" && $return_replacement_data["payment_status"]=="paid"){
						
						$total_refund_to_be_done+=$return_replacement_data["amount_paid"];
					}
					?>
					
				<div class="f-row">Total Refund : <?php echo curr_sym;?><?php echo $total_refund_to_be_done;?> <span id="cancel_rep_refund_details_<?php echo $return_order_id; ?>" data-toggle="popover" data-trigger="click" data-placement="bottom" class="cursor-pointer cancel_rep_refund_details" content_id="#cancel_rep_refund_details_div_<?php echo $return_order_id; ?>" onclick="toggle_content(this)" >[<i class="fa fa-question" aria-hidden="true"></i>]</span>
					
					
					<script>
					$(document).ready(function(){
						$('#cancel_rep_refund_details_<?php echo $return_order_id; ?>').popover({
							html : true,
							content: function() {
								if($(window).width() > 480){
									$('#cancel_rep_refund_details_div_<?php echo $return_order_id; ?>').hide();
									return $('#cancel_rep_refund_details_div_<?php echo $return_order_id; ?>').html();	
								}
							}
						});
					});
					</script>
					
				<div id="cancel_rep_refund_details_div_<?php echo $return_order_id; ?>" style="display:none;">
				
				<table class="table table-hover table-bordered">
				
					<tbody>
					
					<tr>
						<td><p><small>Replacement Item Price </small></p></td>
						<td><p><small> <?php echo curr_sym;?><?php echo $get_replacement_data["quantity"]*$return_replacement_data["replacement_value_each_inventory_id"]; ?></small></p></td>
					</tr>
					
					<tr>
						<td><p><small>Puchased Item Price </small></p></td>
						<td><p><small><?php echo curr_sym;?><?php echo $purchased_item_price;?></small></p></td>
					</tr>
					
					<?php if($purchased_item_price_shipping_charge!=0){ ?>
					<?php if($return_replacement_data["order_replacement_decision_refund_status"]=="refunded" && $return_replacement_data["shipping_charge_applied_cancels_chk"]=="1"){
						?>
						<tr>
							<td><p><small>Shipping Charge for purchased item </small></p></td>
							<td><p><small><?php echo curr_sym;?><?php echo $purchased_item_price_shipping_charge;?></small></p></td>
						</tr>
						<?php
					} 
					if($return_replacement_data["order_replacement_decision_refund_status"]!="refunded"){ ?>
						<tr>
							<td><p><small>Shipping Charge for Purchased Item <?php echo $get_replacement_data["quantity"]; ?> quantity </small></p></td>
							<td><p><small><?php echo curr_sym;?><?php echo $purchased_item_price_shipping_charge;?>
					<?php
						if($return_replacement_data["order_replacement_decision_refund_status"]=="need_to_refund"){
							echo "(Decision pending)";
						}
						if($return_replacement_data["order_replacement_decision_refund_status"]=="refund" && $return_replacement_data["shipping_charge_applied_cancels_chk"]=="1"){
							echo "(Refunded)";
						}
						if($return_replacement_data["order_replacement_decision_refund_status"]=="refund" && $return_replacement_data["shipping_charge_applied_cancels_chk"]=="0"){
							echo "(Not refunded)";
						}
						?>
						</small></p></td>
						</tr>
						<?php
						
					}
					
					?>
					
					<?php } ?>
					
					<?php if($get_replacement_data["return_shipping_concession"]!=0){ ?>
					<?php if($return_replacement_data["order_replacement_decision_refund_status"]=="refunded" && $return_replacement_data["return_shipping_concession_cancels_chk"]=="1"){
						?>
						<tr>
						<td><p><small>Return shipping Concession </small></p></td>
						<td><p><small> <?php echo curr_sym.$get_replacement_data["return_shipping_concession"];?></small></p></td>
						</tr>
						<?php
					} 
					if($return_replacement_data["order_replacement_decision_refund_status"]!="refunded"){ ?>
					
					<tr>
						<td><p><small>Return shipping Concession</small></p></td><td><p><small> <?php echo curr_sym.$get_replacement_data["return_shipping_concession"];?>
					
					<?php
						if($return_replacement_data["order_replacement_decision_refund_status"]=="need_to_refund"){
							echo "(Decision pending)";
						}
						if($return_replacement_data["order_replacement_decision_refund_status"]=="refund" && $return_replacement_data["return_shipping_concession_cancels_chk"]=="1"){
							echo "(Refunded)";
						}
						if($return_replacement_data["order_replacement_decision_refund_status"]=="refund" && $return_replacement_data["return_shipping_concession_cancels_chk"]=="0"){
							echo "(Not refunded)";
						}
						?>
						</small></p></td>
					</tr>
						
						<?php
					}
					?>
					
					<?php } ?>
					<?php 
				
					if($return_replacement_data["paid_by"]=="customer"){ 
					if($return_replacement_data["payment_status"]=="paid"){ ?>
					
					
					<tr>
						<td><p><small>Differential Item Price</small></p></td>
						<td><p><small><?php echo curr_sym;?><?php echo $return_replacement_data["amount_paid_item_price"];?></small></p></td>
					</tr>
					
					<tr>
						<td><p><small>Shipping Charge for <?php echo $get_replacement_data["quantity"]; ?> Quantity </small></p></td>
						<td><p><small> <?php echo curr_sym;?><?php echo $get_replacement_data["shipping_charge_replacement"]; ?> </small></p></td>
					</tr>
					
					<tr>
						<td><p><small>Balance Amount </small></p></td><td><p><small> <?php echo curr_sym;?><?php echo $return_replacement_data["amount_paid"];
					
						if($return_replacement_data["paid_by"]=="customer" && $return_replacement_data["payment_status"]=="paid"){
							echo "(Paid)";
							//$total_refund_to_be_done+=$return_replacement_data["amount_paid"];
						}
						if($return_replacement_data["paid_by"]=="customer" && $return_replacement_data["payment_status"]=="pending"){
							echo "(Not Paid)";
						}
						
					?>
					</small></p></td>
					</tr>
					<?php } 
					}
					?>
					<tr>
						<td><p><small>Total Refund </small></p></td>
						<td><p><small> <?php echo curr_sym;?><?php echo $total_refund_to_be_done;?> </small></p></td>
					</tr>
					
					</tbody>
				</table>
				
				</div>
					
				</div>
					
					
					<?php
					if($return_replacement_data["order_replacement_decision_refund_status"]=="refunded"){
						if($return_replacement_data["refund_method"]=="Bank Transfer"){
								echo '<div class="f-row">Refunded to ';
								$return_order_id=$return_replacement_data["return_order_id"];
								$refund_bank_id=$return_replacement_data['refund_bank_id'];	
								$bank_detail_arr=$controller->get_bank_details($refund_bank_id);
								
								echo '<a id="bank_details_rep_cancel_'.$return_order_id.'" data-toggle="popover" data-trigger="click" data-placement="bottom" class="bank_detail_replace_refund2" content_id="#bank_detail_ret_div_'.$return_order_id.'" onclick="toggle_content(this)" style="cursor:pointer;"><u>Bank Account</u></a>';
								
								
								echo '<div id="bank_details_rep_cancel_div_'.$return_order_id.'" style="display:none;">'.$bank_detail_arr.'</div>';
								echo '</div>';
								?>
								
							<script>
							$(document).ready(function(){
								$('#bank_details_rep_cancel_<?php echo $return_order_id; ?>').popover({
									html : true,
									content: function() {
										if($(window).width() > 480){
											$('#bank_details_rep_cancel_div_<?php echo $return_order_id; ?>').hide();
											return $('#bank_details_rep_cancel_div_<?php echo $return_order_id; ?>').html();	
										}
									}
								});
							});
							</script>
								
								<?php 
								
								echo '<div class="f-row">Transaction ID: '.$return_replacement_data['transaction_id'].'</div>';
								echo '<div class="f-row">Transaction Date: '.date("d-m-Y", strtotime($get_replacement_desired_refund_data[0]['transaction_date'])).'</div>';
								echo '<div class="f-row">Transaction Time: '.$return_replacement_data['transaction_time'].'</div>';
							}else{
								echo '<div class="f-row">Refunded to '.$return_replacement_data["refund_method"].'</div>';
							}
					}
					
				}
				?>
				
				<?php
					if($return_replacement_data['order_replacement_decision_customer_status']=="cancel"){
				?>
				<div class="f-row">Replacement Order has been cancelled.</div>
				<div class="f-row"><p><b>Comments:</b>
				<span class="more">
				<?php
					echo $return_replacement_data['order_replacement_decision_customer_status_comments'];
				?>
				</span>
				</p>
				</div>
				<?php
					}

				?>
					
				<?php
			
			}
			?>
			</div>
		</div>
		</div>
	</div>	
			<?php
		}	
	?>
	
	<?php
	
		}
		//$get_data_from_replacement_desired_return_order_id=$controller->get_data_from_replacement_desired_return_order_id($returns_data['return_order_id']);
		
	?>			
	<div class="row margin-bottom margin-top">
		<div class="col-md-12">
			<?php include 'cancel_replacement.php'; ?>
		</div>
	</div>	
	<!-------------- admin accepted details in replacement ends ------------------->
<script>

function accept_reject_replacement_statusFun(st,order_item_id){
	$.ajax({
		url:"<?php echo base_url()?>Account/accept_reject_replacement_status",
		type:"post",
		data:"order_item_id="+order_item_id+"&st="+st,
		success:function(data){
			
			if(data){
				if(st=="accept"){
					$("#accept_reject_replacement_status_"+order_item_id).html("<b>Status:</b> Accepted");
					/*bootbox.alert({
					  size: "small",
					  message: 'Status has been successfully updated',
					  callback: function () { location.reload(); }
					});*/

                    alert('Status has been successfully updated');
					location.reload();
				}
				if(st=="reject"){
					$("#accept_reject_replacement_status_"+order_item_id).html("<b>Status:</b> Rejected");
					/*bootbox.alert({
					  size: "small",
					  message: 'Status has been successfully updated',
					  callback: function () { location.reload(); }
					});*/

                    alert('Status has been successfully updated');
                    location.reload();
				}
			}
		}
	})
}

/*
$(document).ready(function(){
	$("#send_form_admin_contact_admin_form_rep_<?php echo $order_item['order_item_id'];?>").on('submit',(function(e) {
		e.preventDefault();
		var description=$("#description_rep_<?php echo $order_item['order_item_id'];?>").val();
		if(description!=""){
			$.ajax({
				url:"<?php echo base_url();?>Account/contact_admin_replacement",
				type: "POST",      				// Type of request to be send, called as method
				data:  new FormData(this),	// Data sent to server, a set of key/value pairs representing form fields and values 
				contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
				cache: false,					// To unable request pages to be cached
				processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
				success: function(data)  		// A function to be called if request succeeds
				{
					if(data){
						
						alert("Successfully sent");
						location.reload();
						$("#modal_close_rep_<?php echo $order_item['order_item_id'];?>").trigger("click");
					}
					else{
						alert("not sent");
					}
				}	        
		   });
		}
		else{
			alert("Please enter description!");
		}
	}));
	

});


$(document).ready(function(){
	$("#customer_comment_modal_rep_<?php echo $order_item['order_item_id'];?>").on('hidden.bs.modal', function () {
			$.ajax({
				url:"<?php echo base_url()?>Account/update_msg_status_replacement",
				type:"POST",
				data:"return_order_id="+$("#return_order_id_for_msg_op_<?php echo $order_item['order_item_id'];?>").val(),
				success:function(data){
					//location.reload();
					$("#send_form_admin_contact_admin_form_rep_<?php echo $order_item['order_item_id'];?>")[0].reset();
					$(".replacement_div .badge").html("");
				}
			})
    });
})
*/
</script> 
<!------------------------------------------- contact buyerr replacement ends ------------------------------------------>

												
											<?php
												}
											}
							?>				

							
<!---------------------------------------------replacement ended------------------------------------------------------------>
<!---/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////--->
	
		<?php		
						
	//////////////////////////////////////// returns desired starts //////////////////////////////////////////////

//original_inventory_id
	if(!empty($get_data_from_returns_desired_return_order_id)){
		
		foreach($get_data_from_returns_desired_return_order_id as $return_data){
			
		$refund_requested=$return_data["quantity_refund"];
		$return_order_id=$return_data["return_order_id"];
		
		?>
			
<div id="refund_div">
	<!--<h3 class="title_style">Returns</h3>-->
		
	<div class="row">
	
		<div class="col-md-6 products-block">
		<div class="row">
		<div class="col-md-3">
			<a href="#">
				<img src="<?php echo base_url().$inv_obj->thumbnail;?>" alt="SPECIAL PRODUCTS">
			</a>
		</div>
		<div class="col-md-9">
			
			<div class="product-name">
					<?php 
			//$product_name=$controller->get_product_name($order_item['product_id']);
			
                        if($order_item['ord_sku_name']!=''){
                            $product_name=$order_item['ord_sku_name'];
                        }else{
                            $product_name=$controller->get_product_name($order_item['product_id']);
                        }
			
			?>
			<form target="_blank" id="write_review_<?php echo $order_items['order_item_id']; ?>" action="<?php echo base_url()."write_review" ?>" method="post">
			<input type="hidden" value="<?php echo $order_item['inventory_id']; ?>" name="inv_id" id="inv_id">
			<input type="hidden" value="<?php echo $product_name; ?>" name="pro_name">
			<input type="hidden" value="<?php echo $this->session->userdata('customer_id') ?>" name="customer_id" id="customer_id">
			<input type="hidden" value="1" name="rate_order" id="rate_order">
			<input type="hidden" value="<?php echo $order_items['order_item_id']; ?>" name="rate_order_order_item_id" id="rate_order_order_item_id">
			</form>
					<a href="<?php echo base_url()?>detail/<?php echo $controller->sample_code().$order_item['inventory_id'];?>"><?php echo $product_name;?></a>
					
					<!--------contact admin starts-------------->
					<?php
						$availability_of_order_item_id_in_refunds_replacements=$controller->get_availability_of_order_item_id_in_refunds_replacements($order_item['order_item_id']);
						//if($availability_of_order_item_id_in_refunds_replacements=="no"){
					?>
					
													 
			</div><!--product_name-->
			<div class="f-row small-text">
										
						<?php
					//$inv_obj->attribute_1_value= substr($inv_obj->attribute_1_value, 0, strpos($inv_obj->attribute_1_value, ":"));
					if(!empty($inv_obj->attribute_1))
						echo '<div class="f-row">'.$inv_obj->attribute_1." 1: ".$inv_obj->attribute_1_value.'</div>';
					if(!empty($inv_obj->attribute_2))
						echo '<div class="f-row">'.$inv_obj->attribute_2." : ".$inv_obj->attribute_2_value.'</div>';
					if(!empty($inv_obj->attribute_3))
						echo '<div class="f-row">'.$inv_obj->attribute_3." : ".$inv_obj->attribute_3_value.'</div>';
					if(!empty($inv_obj->attribute_4))
						echo '<div class="f-row">'.$inv_obj->attribute_4." : ".$inv_obj->attribute_4_value.'</div>';
					?>
				
				</div>
				<div class="f-row small-text">SKU : <?php echo $inv_obj->sku_id; ?></div>
				<div class="f-row small-text">
					Quantity : <?php echo $refund_requested ?>
				</div>
				
			
		</div>
		</div>
		</div>
									
	<!-------------- customer requested details in returns starts ------------------->
	
			<?php
			//$get_returns_data=$controller->get_order_details_return_data($order_item['order_item_id']);	
			//$get_data_from_returns_desired_return_order_id=$controller->get_data_from_returns_desired_return_order_id($returns_data['return_order_id']);
			
			/*echo '<pre>';			
			print_r($get_data_from_returns_desired_return_order_id);
			echo '</pre>';*/
			
			//data_from_returns_desired_return_order_id//removed
					
			?>									
		<div class="col-md-6">
			<div class="row">
			<div class="col-md-12">
			
			
			<div class="f-row">
				<?php 	//$get_returns_data=$controller->get_order_details_return_data($order_item['order_item_id']);
				$transaction_reasons=$controller->get_reason_for_return_order_id($returns_data['return_order_id']);
				foreach($transaction_reasons as $reasons){ ?>
					<dl class="dl-horizontal">
					  <dt>Refund reason</dt>
					  <dd class="text-left"><?php echo $controller->get_primary_return_reason($reasons['reason_return_id'])?></dd>
					  <dt>Sub reason</dt>
					  <dd class="text-left"><?php 
					if(isset($reasons['sub_issue_id'])){?> 
						<?php echo $controller->get_sub_return_reason($reasons['sub_issue_id'])?>
					<?php 
					}?></dd>
					<dt>Comments</dt>
					  <dd class="text-left more"><?php 
					if(isset($reasons['return_reason_comments'])){
						echo '<span class="more">'.$reasons['return_reason_comments'].'</span>'; 
					}?></dd>
					</dl>
					<?php } ?>
			</div>
			
			<div class="f-row">
				<dl class="dl-horizontal">
						<?php 
							$promotion_available=$order_item["promotion_available"];
								
							if($promotion_available==1){
								$style='style="display:none;"';
								$total_item_price=(floatval($return_data["total_price_of_product_with_promotion"])+floatval($return_data["total_price_of_product_without_promotion"]));

								if($return_data["cash_back_value"]>0){
									$deducted_total_price=($total_item_price-$return_data["cash_back_value"]);
								}
								if($return_data["promotion_surprise_gift_type"]==curr_code){
									if($order_item["quantity"]==$return_data["quantity_refund"]){
										$deducted_total_price=($total_item_price-$return_data["promotion_surprise_gift"]);
									}
								}
							}else{
								$style='';	
								$total_item_price=$order_item["product_price"]*($return_data["quantity_refund"]);
							}
							
							if($promotion_invoice_discount>0){
								$order_item_invoice_discount_value_each=$return_data["order_item_invoice_discount_value_each"];
								$qty_to_reduce=($order_item_invoice_discount_value_each*$return_data["quantity_refund"]);
								$total_item_price=$total_item_price-$qty_to_reduce;
							}
							if($return_data["cash_back_value"]>0){
								$total_item_price=$deducted_total_price;
							}
							if(($return_data["promotion_surprise_gift_type"]==curr_code) && ($order_item["quantity"]==$return_data["quantity_refund"])){
								$total_item_price=$deducted_total_price;
							}
							
							$calculated_shipping=round($shipping_charge_per_quantity*$return_data["quantity_refund"]);				
							if($promotion_invoice_free_shipping>0){ 
								$calculated_shipping=0;
							}

						?>
					  <dt>Refund amount</dt>
					  
					  <dd class="text-left"><?php echo curr_sym;?><?php echo $total_item_price+$calculated_shipping;?> <span id="refund_customer_request_<?php echo $return_order_id; ?>" data-toggle="popover" data-trigger="click" data-placement="bottom" class="cursor-pointer refund_customer_request" content_id="#refund_customer_request_div_<?php echo $return_order_id; ?>" onclick="toggle_content(this)">[<i class="fa fa-question" aria-hidden="true"></i>]</span></dd>
				</dl>
				
				<script>
			$(document).ready(function(){
				$('#refund_customer_request_<?php echo $return_order_id; ?>').popover({
					html : true,
					content: function() {  
						if($(window).width() > 480){
							$('#refund_customer_request_div_<?php echo $return_order_id; ?>').hide();
							return $('#refund_customer_request_div_<?php echo $return_order_id; ?>').html();	
						}
					}
				});
			});
			</script>
			
			<div style="display:none;" id="refund_customer_request_div_<?php echo $return_order_id; ?>">
			
				<?php
				
				
				$promotion_available=$order_item["promotion_available"];
				
				if($promotion_available==1){
					$style='style="display:none;"';
					$total_item_price=(floatval($return_data["total_price_of_product_with_promotion"])+floatval($return_data["total_price_of_product_without_promotion"]));

					if($return_data["cash_back_value"]>0){
						$deducted_total_price=($total_item_price-$return_data["cash_back_value"]);
					}
					if($return_data["promotion_surprise_gift_type"]==curr_code){
						if($order_item["quantity"]==$return_data["quantity_refund"]){
							$deducted_total_price=($total_item_price-$return_data["promotion_surprise_gift"]);
						}
					}
				}else{
					$style='';	
					$total_item_price=$order_item["product_price"]*($return_data["quantity_refund"]);
				}
				
			
				?>	
				
				<table class="table table-hover table-bordered">

				<tbody>
				
				<tr <?php echo $style; ?>>
					<td><p><small>	Item Price (each)</small></p></td>	
					
					<td><p><small>	<?php echo curr_sym.$order_item["product_price"]." ";?></small></p></td>
				</tr>
				<?php
				if($promotion_available==1){
					?>
				
				<tr>
					<td><p><small>Applied Promotion</small></p></td>	
					<td><p><small style="color:green;">	<?php echo " ".$order_item["promotion_quote"]." ";?></small></p></td>	
				</tr>
					
					<?php
					
				}			
				
				?>
				<tr>
					<td><p><small>	Shipping Charge (each)</small></p></td>	
					<td><p><small>	<?php echo curr_sym.$shipping_charge_per_quantity." ";?></small></p></td>
				</tr>
				
				<?php if($return_data["cash_back_value"]>0){
					?>
				<tr>
					<td><p><small>Item Price for <?php echo $return_data["quantity_refund"];?> quantity</small></p></td>	
					<td><p><small>	<?php echo curr_sym.$total_item_price;?></small></p></td>
				</tr>
				<tr>
					<td><p><small>Cash back value deducted for <?php echo $return_data["quantity_refund"];?> quantity</small></p></td>	
					<td><p><small style="color:red">	<?php echo curr_sym.$return_data["cash_back_value"];?></small></p></td>
				</tr>
				
				<tr>
					<td><p><small>Total item price value for <?php echo $return_data["quantity_refund"];?> quantity</small></p></td>	
					<td><p><small>	<?php echo curr_sym.$deducted_total_price;?></small></p></td>
				</tr>
					<?php
					$total_item_price=$deducted_total_price;
				
				}
				
				if(($return_data["promotion_surprise_gift_type"]==curr_code) && ($order_item["quantity"]==$return_data["quantity_refund"])){

					?>
					
				<tr>
					<td><p><small>Item Price for <?php echo $return_data["quantity_refund"];?> quantity</small></p></td>	
					<td><p><small>	<?php echo curr_sym.$total_item_price;?></small></p></td>
				</tr>
				<tr>
					<td><p><small>Amount has to be reduced (surprise gift) </small></p></td>	
					<td><p><small style="color:red">	<?php echo curr_sym.$return_data["promotion_surprise_gift"];?></small></p></td>
				</tr>
				
				<tr>
					<td><p><small>Total item price value for <?php echo $return_data["quantity_refund"];?> quantity</small></p></td>	
					<td><p><small>	<?php echo curr_sym.$deducted_total_price;?></small></p></td>
				</tr>
					<?php
					
					$total_item_price=$deducted_total_price;
				
				}
					
					if($promotion_invoice_discount>0){
						
						?>
						
						<tr>
							<td><p><small>Item Price for <?php echo $return_data["quantity_refund"];?> quantity</small></p></td>	
							<td><p><small>	<?php echo curr_sym.$total_item_price;?></small></p></td>
						</tr>
						
						<?php
						$order_item_invoice_discount_value_each=$return_data["order_item_invoice_discount_value_each"];
						$qty_to_reduce=($order_item_invoice_discount_value_each*$return_data["quantity_refund"]);
						$total_item_price=$total_item_price-$qty_to_reduce;
						?>
						
						<tr>
							<td><p><small>Invoice discount price which has to be deducted (<?php echo $return_data["quantity_refund"]; ?>  * <?php echo $order_item_invoice_discount_value_each ?> )</small></p></td>	
							<td><p><small style="color:red;">	<?php echo curr_sym.$qty_to_reduce;?></small></p></td>
						</tr>
					
						<tr>
							<td><p><small>Total Item Price for <?php echo $return_data["quantity_refund"];?> quantity</small></p></td>	
							<td><p><small>	<?php echo curr_sym.$total_item_price;?></small></p></td>
						</tr>
					
						<?php
					
					}
					
					if(($promotion_invoice_discount==0 || $promotion_invoice_discount=='') && ($return_data["cash_back_value"]==0 || $return_data["cash_back_value"]=='')){
					
					?>
						
					<tr>
						<td><p><small>Total Item Price for <?php echo $return_data["quantity_refund"];?> quantity</small></p></td>	
						<td><p><small>	<?php echo curr_sym.$total_item_price;?></small></p></td>
					</tr>
						
						<?php
					}
					?>

				<?php	
				$calculated_shipping=round($shipping_charge_per_quantity*$return_data["quantity_refund"]);				
				if($promotion_invoice_free_shipping>0){  
				?>
				<tr>
					<td><p><small>Shipping Charge for <?php echo $return_data["quantity_refund"];?> quantity</small></p></td>	
					<td><p><small><del><?php echo curr_sym.$calculated_shipping." ";?></del></small></p></td>
				</tr>
				<tr>
					<td><p><small>Shipping charges </small></p></td>	
					<td><p><small style="color:red"> waived off </small></p></td>	
						
				</tr>
				<?php 
				
				$calculated_shipping=0;
				
				}else{

				?>

				<tr>
					<td><p><small>Shipping Charge for <?php echo $return_data["quantity_refund"];?> quantity</small></p></td>	
					<td><p><small><?php echo curr_sym.$calculated_shipping." ";?></small></p></td>
				</tr>
				
				<?php } ?>
				<tr>
				
					<td><p><small>Total Amount to be refunded</small></p></td>	
					<td><p><small><?php echo curr_sym.($total_item_price+$calculated_shipping);?></small></p></td>
				</tr>
							
				</tbody>
				
				</table>

<!-------------- customer requested details in returns ends --------------------->
			</div>
			
			
				
			</div>
			
			<div class="f-row">
			<dl class="dl-horizontal">
					  <dt>Refund method</dt>
					  <dd class="text-left"><?php echo $return_data["refund_method"];?>	</dd>
				</dl>	
			</div>
			
			
		</div>
		</div>		
		</div><!--col-md-6--->
		
	</div><!--row--1---->
	
	
<?php
	$get_refund_data=$controller->get_order_details_admin_acceptance_refund_data_for_mail($order_item['order_item_id']);
	//admin accepted data
	if(count($get_refund_data)>0){
	
		$promotion_item=$get_refund_data["promotion_item"];
		$promotion_item_num=$get_refund_data["promotion_item_num"];

		$promo_item=rtrim($promotion_item,',');
		$promotion_item_num=rtrim($promotion_item_num,',');

					
		if($promo_item!=''){
			
			$promo_item_arr = explode(',', $promo_item);
			$promo_item_num_arr = explode(',', $promotion_item_num);

			$two=array_combine($promo_item_arr,$promo_item_num_arr);

			$quantity=$get_refund_data["quantity"];
			$promotion_minimum_quantity=$get_refund_data["promotion_minimum_quantity"];

			$temp_quotient=intval($quantity/$promotion_minimum_quantity);
			$return_promotion_available=$get_refund_data["promotion_available"];
			$free_skus_has_to_be_returned_arr=$controller->get_free_skus_from_inventory_with_details($promo_item,$promotion_item_num);

			if(count($free_skus_has_to_be_returned_arr)>0){
					
				foreach($free_skus_has_to_be_returned_arr as $free_skus_has_to_be_returned){
					
				?>
				<div class="row margin-top">
					<div class="col-md-6">
					<div class="row">
					<div class="col-sm-3">
						<a href="#">
							<img src="<?php echo base_url().$free_skus_has_to_be_returned->thumbnail; ?>" alt="SPECIAL PRODUCTS">
						</a>
					</div>
					<div class="col-sm-9">
					
						<div class="f-row product-name">
							<a href="<?php echo base_url()?>detail/<?php echo $controller->sample_code().$free_skus_has_to_be_returned->id;?>"><?php echo ($free_skus_has_to_be_returned->sku_name!='') ? $free_skus_has_to_be_returned->sku_name : $free_skus_has_to_be_returned->product_name; ?>
							</a>
						</div>
						<div class="f-row small-text">
										
							<?php
							$free_skus_has_to_be_returned->attribute_1_value= substr($free_skus_has_to_be_returned->attribute_1_value, 0, strpos($free_skus_has_to_be_returned->attribute_1_value, ":"));
							
							if(!empty($free_skus_has_to_be_returned->attribute_1))
								echo '<div class="f-row">'.$free_skus_has_to_be_returned->attribute_1." : ".$free_skus_has_to_be_returned->attribute_1_value.'</div>';
							if(!empty($free_skus_has_to_be_returned->attribute_2))
								echo '<div class="f-row">'.$free_skus_has_to_be_returned->attribute_2." : ".$free_skus_has_to_be_returned->attribute_2_value.'</div>';
							if(!empty($free_skus_has_to_be_returned->attribute_3))
								echo '<div class="f-row">'.$free_skus_has_to_be_returned->attribute_3." : ".$free_skus_has_to_be_returned->attribute_3_value.'</div>';
							if(!empty($free_skus_has_to_be_returned->attribute_4))
								echo '<div class="f-row">'.$free_skus_has_to_be_returned->attribute_4." : ".$free_skus_has_to_be_returned->attribute_4_value.'</div>';
							?>
						
						</div>
						<div class="f-row small-text">SKU : <?php echo $free_skus_has_to_be_returned->sku_id; ?></div>
						<div class="f-row small-text">
							Quantity : <?php echo ($temp_quotient*$two[$free_skus_has_to_be_returned->id]);?>
						</div>
						
					
					</div>
					</div>
					</div><!--col-md-6--->
					
					<div class="col-sm-6">
					<div class="row">
					<div class="col-sm-12">
						Free Items Customer has to return (first) Admin accepted
					</div>
					</div>
					</div>
				
				</div><!--row--->		
							
				<?php
				
				}//foreach

			}//free sku is there
		}//promo item not empty


		if($promotion_surprise_gift_type=='Qty'){
			
			
			$quantity=$get_refund_data["quantity"];
			$purchased_quantity=$order_item["quantity"];
			$remain=intval($purchased_quantity-$quantity);
			
				if($remain<$promotion_minimum_quantity){

					$promotion_minimum_quantity=$get_refund_data["promotion_minimum_quantity"];

					$temp_quotient=intval($quantity/$promotion_minimum_quantity);
					
					$promotion_surprise_gift_skus=rtrim($promotion_surprise_gift_skus,',');
					$promotion_surprise_gift_skus_nums=rtrim($promotion_surprise_gift_skus_nums,',');
					
					$gift_skus_arr = explode(',', $promotion_surprise_gift_skus);
					$gift_skus_nums_arr = explode(',', $promotion_surprise_gift_skus_nums);
					
					$gift_with_count=array_combine($gift_skus_arr,$gift_skus_nums_arr);
				
						
						foreach($free_surprise_items as $surprise_items){
												
												?>
							<div class="row margin-top">
								<div class="col-md-6">
								<div class="row">
								<div class="col-sm-3">
									<a href="#">
										<img src="<?php echo base_url().$surprise_items->thumbnail; ?>" alt="SPECIAL PRODUCTS">
									</a>
								</div>
								<div class="col-sm-9">
								
									<div class="f-row product-name">
										<a href="<?php echo base_url()?>detail/<?php echo $controller->sample_code().$surprise_items->id;?>"><?php echo ($surprise_items->sku_name!='') ? $surprise_items->sku_name : $surprise_items->product_name; ?>
										</a>
									</div>
									<div class="f-row small-text">
													
										<?php
										$surprise_items->attribute_1_value= substr($surprise_items->attribute_1_value, 0, strpos($surprise_items->attribute_1_value, ":"));
										
										if(!empty($surprise_items->attribute_1))
											echo '<div class="f-row">'.$surprise_items->attribute_1." : ".$surprise_items->attribute_1_value.'</div>';
										if(!empty($surprise_items->attribute_2))
											echo '<div class="f-row">'.$surprise_items->attribute_2." : ".$surprise_items->attribute_2_value.'</div>';
										if(!empty($surprise_items->attribute_3))
											echo '<div class="f-row">'.$surprise_items->attribute_3." : ".$surprise_items->attribute_3_value.'</div>';
										if(!empty($surprise_items->attribute_4))
											echo '<div class="f-row">'.$surprise_items->attribute_4." : ".$surprise_items->attribute_4_value.'</div>';
										?>
									
									</div>
									<div class="f-row small-text">SKU : <?php echo $surprise_items->sku_id; ?></div>
									<div class="f-row small-text">
										Quantity : <?php 
														$id=$surprise_items->id;
														echo $gift_with_count[$id];?>
									</div>
									
								
								</div>
								</div>
								</div><!--col-md-6--->
								
								<div class="col-sm-6">
								<div class="row">
								<div class="col-sm-12">
									Free Items (surprise gift) Customer has to return (first) Admin accepted
								</div>
								</div>
								</div>
							
							</div><!--row--->											
									
							<?php
												
						}
										
				}
		}//surprise quantity
	
	
	}else{
		

		$promotion_item=$return_data["promotion_item"];
		$promotion_item_num=$return_data["promotion_item_num"];

		$promo_item=rtrim($promotion_item,',');
		$promotion_item_num=rtrim($promotion_item_num,',');

		if($promo_item!=''){
				
			$promo_item_arr = explode(',', $promo_item);
			$promo_item_num_arr = explode(',', $promotion_item_num);

			$two=array_combine($promo_item_arr,$promo_item_num_arr);

			$quantity=$return_data["quantity_refund"];
			$promotion_minimum_quantity=$return_data["promotion_minimum_quantity"];

			$temp_quotient=intval($quantity/$promotion_minimum_quantity);

			$return_promotion_available=$return_data["promotion_available"];
			$free_skus_has_to_be_returned_arr=$controller->get_free_skus_from_inventory_with_details($promo_item,$promotion_item_num);

			if(count($free_skus_has_to_be_returned_arr)>0){
			

				foreach($free_skus_has_to_be_returned_arr as $free_skus_has_to_be_returned){
					
					?>
					
					<div class="row margin-top">
						<div class="col-md-6">
						<div class="row">
						<div class="col-sm-3">
							<a href="#">
								<img src="<?php echo base_url().$free_skus_has_to_be_returned->thumbnail; ?>" alt="SPECIAL PRODUCTS">
							</a>
						</div>
						<div class="col-sm-9">
						
							<div class="f-row product-name">
								<a href="<?php echo base_url()?>detail/<?php echo $controller->sample_code().$free_skus_has_to_be_returned->id;?>"><?php echo ($free_skus_has_to_be_returned->sku_name!='') ? $free_skus_has_to_be_returned->sku_name: $free_skus_has_to_be_returned->product_name; ?>
								</a>
							</div>
							<div class="f-row small-text">
											
								<?php
								$free_skus_has_to_be_returned->attribute_1_value= substr($free_skus_has_to_be_returned->attribute_1_value, 0, strpos($free_skus_has_to_be_returned->attribute_1_value, ":"));
								
								if(!empty($free_skus_has_to_be_returned->attribute_1))
									echo '<div class="f-row">'.$free_skus_has_to_be_returned->attribute_1." : ".$free_skus_has_to_be_returned->attribute_1_value.'</div>';
								if(!empty($free_skus_has_to_be_returned->attribute_2))
									echo '<div class="f-row">'.$free_skus_has_to_be_returned->attribute_2." : ".$free_skus_has_to_be_returned->attribute_2_value.'</div>';
								if(!empty($free_skus_has_to_be_returned->attribute_3))
									echo '<div class="f-row">'.$free_skus_has_to_be_returned->attribute_3." : ".$free_skus_has_to_be_returned->attribute_3_value.'</div>';
								if(!empty($free_skus_has_to_be_returned->attribute_4))
									echo '<div class="f-row">'.$free_skus_has_to_be_returned->attribute_4." : ".$free_skus_has_to_be_returned->attribute_4_value.'</div>';
								?>
							
							</div>
							<div class="f-row small-text">SKU : <?php echo $free_skus_has_to_be_returned->sku_id; ?></div>
							<div class="f-row small-text">
								Quantity : <?php echo ($temp_quotient*$two[$free_skus_has_to_be_returned->id]);?>
							</div>
							
						
						</div>
						</div>
						</div><!--col-md-6--->
						
						<div class="col-sm-6">
						<div class="row">
						<div class="col-sm-3">
							Free Items Customer has to return (first)
						</div>
						</div>
						</div>
					</div><!--row--->	
					
					<?php
					
				}//foreach

			}//free sku is there
		}//promo item not empty

		if($promotion_surprise_gift_type=='Qty'){
			
			$promotion_surprise_gift_skus=rtrim($promotion_surprise_gift_skus,',');
			$promotion_surprise_gift_skus_nums=rtrim($promotion_surprise_gift_skus_nums,',');
			
			
			$quantity=$return_data["quantity_refund"];
			$promotion_minimum_quantity=$return_data["promotion_minimum_quantity"];

			$purchased_quantity=$order_item["quantity"];
			$remain=intval($purchased_quantity-$quantity);
			if($remain<$promotion_minimum_quantity){
				
			$temp_quotient=intval($quantity/$promotion_minimum_quantity);
			
			$gift_skus_arr = explode(',', $promotion_surprise_gift_skus);
			$gift_skus_nums_arr = explode(',', $promotion_surprise_gift_skus_nums);
			
			$gift_with_count=array_combine($gift_skus_arr,$gift_skus_nums_arr);

				
				foreach($free_surprise_items as $surprise_items){
										
										?>
						<div class="row margin-top">
								<div class="col-md-6">
								<div class="row">
								<div class="col-sm-3">
									<a href="#">
										<img src="<?php echo base_url().$surprise_items->thumbnail; ?>" alt="SPECIAL PRODUCTS">
									</a>
								</div>
								<div class="col-sm-9">
								
									<div class="f-row product-name">
										<a href="<?php echo base_url()?>detail/<?php echo $controller->sample_code().$surprise_items->id;?>"><?php echo ($surprise_items->sku_name!='') ? $surprise_items->sku_name :$surprise_items->product_name ; ?>
										</a>
									</div>
									<div class="f-row small-text">
													
										<?php
										$surprise_items->attribute_1_value= substr($surprise_items->attribute_1_value, 0, strpos($surprise_items->attribute_1_value, ":"));
										
										if(!empty($surprise_items->attribute_1))
											echo '<div class="f-row">'.$surprise_items->attribute_1." : ".$surprise_items->attribute_1_value.'</div>';
										if(!empty($surprise_items->attribute_2))
											echo '<div class="f-row">'.$surprise_items->attribute_2." : ".$surprise_items->attribute_2_value.'</div>';
										if(!empty($surprise_items->attribute_3))
											echo '<div class="f-row">'.$surprise_items->attribute_3." : ".$surprise_items->attribute_3_value.'</div>';
										if(!empty($surprise_items->attribute_4))
											echo '<div class="f-row">'.$surprise_items->attribute_4." : ".$surprise_items->attribute_4_value.'</div>';
										?>
									
									</div>
									<div class="f-row small-text">SKU : <?php echo $surprise_items->sku_id; ?></div>
									<div class="f-row small-text">
										Quantity : <?php 
												$id=$surprise_items->id;
												//echo $temp_quotient."|||||";
												echo $gift_with_count[$id];?>
									</div>
									
								
								</div>
								</div>
								</div><!--col-md-6--->
								
								<div class="col-sm-6">
								<div class="row">
								<div class="col-sm-3">
									Free Items (surprise gift) Customer has to return (first) customer requested
								</div>
								</div>
								</div>
							
						</div><!--row--->	
										<?php
										
				}
								
			}
		}//surprise quantity
	}//customer accepted else
?>



<?php	//$get_returns_data=$controller->get_order_details_return_data($order_item['order_item_id']);
			
			if(count($get_refund_data)!=0){ 
			
			/*echo '<pre>';
			print_r($get_refund_data);
			echo '</pre>';
			*/
			$promotion_available=$get_refund_data["promotion_available"];
				
				if($promotion_available==1){
					$style='style="display:none;"';
					$total_item_price_admin=(floatval($get_refund_data["total_price_of_product_with_promotion"])+floatval($get_refund_data["total_price_of_product_without_promotion"]));
					if($get_refund_data["cash_back_value"]>0){
						$deducted_total_price_admin=($total_item_price_admin-$get_refund_data["cash_back_value"]);
					}
					
				}else{
					$style='';	
					$total_item_price_admin=$get_refund_data["product_price"]*($get_refund_data["quantity"]);
				}
			?>
			
			
			<div class="row">
			<div class="col-sm-6">
			<div class="row">
			<div class="col-sm-9 col-sm-offset-3">
				<div class="f-row"><strong>Vendor response </strong></div>
				<div class="f-row">Quantity accepted : <?php echo $get_refund_data
					["quantity"];?></div>
				
				<div class="f-row">
				
					Refund amount
						
						<?php
						echo "(".curr_sym.$get_refund_data
						["product_price_with_offer"]+$get_refund_data
						["shipping_charge_with_offer"].")";
						?>
					
					<span id="refund_admin_reply_<?php echo $return_order_id; ?>" data-toggle="popover" data-trigger="click" data-placement="bottom" class="cursor-pointer refund_admin_reply" data-original-title="" title="" content_id="#refund_admin_reply_div_<?php echo $return_order_id; ?>" onclick="toggle_content(this)">[<i class="fa fa-question" aria-hidden="true"></i>]</span>
					
					<div id="refund_admin_reply_div_<?php echo $return_order_id; ?>" style="display:none;">

				<table class="table table-hover table-bordered">
					
					<tbody>
	
					<?php 
					if($get_refund_data["cash_back_value"]>0){
						?>
					<tr>
						<td><p><small>Item Price for <?php echo $get_refund_data
					["quantity"];?></small></p></td>
					
						<td><p><small><?php echo curr_sym;?><?php echo $total_item_price_admin;?></small></p></td>
					</tr>
					<tr>
						<td><p><small>Cash back value deducted for <?php echo $get_refund_data
					["quantity"];?></small></p></td>
					
						<td><p><small style="color:red;"><?php echo curr_sym;?><?php echo $get_refund_data["cash_back_value"];?></small></p></td>
					</tr>
					<tr>
						<td><p><small>Total Item Price for <?php echo $get_refund_data
					["quantity"];?></small></p></td>
					
						<td><p><small><?php echo curr_sym;?><?php echo $deducted_total_price_admin;?></small></p></td>
					</tr>
						<?php
					
					}else{
						
						?>
						
						<?php

						if($promotion_invoice_discount>0){
						
						?>
						
						<tr>
							<td><p><small>Item Price for <?php echo $get_refund_data["quantity"];?> quantity</small></p></td>	
							<td><p><small>	<?php echo curr_sym.$total_item_price_admin;?></small></p></td>
						</tr>
						
						<?php
						
						$qty_to_reduce=($order_item_invoice_discount_value_each*$get_refund_data["quantity"]);
						$total_item_price_admin=$total_item_price_admin-$qty_to_reduce;
						?>
						
						<tr>
							<td><p><small>Invoice discount price which has to be deducted (<?php echo $get_refund_data["quantity"]; ?>  * <?php echo $order_item_invoice_discount_value_each ?> )</small></p></td>	
							<td><p><small style="color:red;">	<?php echo curr_sym.$qty_to_reduce;?></small></p></td>
						</tr>
					
						<tr>
							<td><p><small>Total Item Price for <?php echo $get_refund_data["quantity"];?> quantity</small></p></td>	
							<td><p><small>	<?php echo curr_sym.$total_item_price_admin;?></small></p></td>
						</tr>
					
						<?php
					
						}else{
						
						?>
							
						<tr>
							<td><p><small>Total Item Price for <?php echo $get_refund_data
						["quantity"];?></small></p></td>
						
							<td><p><small><?php echo curr_sym;?><?php echo $total_item_price_admin;?></small></p></td>
						</tr>
							
							<?php
						}
						?>			
					
					
						<?php
					}
					
					?>
					
					<tr>
						<td><p><small>Total Shipping Charge for <?php echo $get_refund_data
					["quantity"];?></small></p></td>
						<td><p><small><?php echo curr_sym;?><?php echo round($get_refund_data["shipping_charge"]*$get_refund_data["quantity"]);?></small></p></td>
					</tr>
					
					
					<tr>
						<td><p><small>Total Item Price with deduction to Refund</small></p></td>
						<td><p><small><?php echo curr_sym;?><?php echo $get_refund_data
					["product_price_with_offer"];?> <?php echo ($get_refund_data
					["product_price_with_offer_percentage"]!=0)?'('.$get_refund_data
					["product_price_with_offer_percentage"].'% deducted)' :'';?></small></p></td>
					</tr>
					

					<tr>
						<td><p><small>Total Shipping Charge with deduction to Refund</small></p></td>
						<td><p><small>
						
					<?php echo curr_sym.$get_refund_data["shipping_charge_with_offer"];
					
					echo ($get_refund_data
					["shipping_charge_with_offer_percentage"]!=0)?'('.$get_refund_data
					["shipping_charge_with_offer_percentage"].'% deducted)':'';?> 
					<?php
						if($get_refund_data["shipping_charge_with_offer"]==0){
							echo "No Refund";
						}
					?></small></p></td>
					
					</tr>
					<tr>
						<td><p><small>Total amount to be refunded</small></p></td>
						<td><p><small>
						<?php
						echo ($get_refund_data
						["product_price_with_offer"]+$get_refund_data
						["shipping_charge_with_offer"]);
						?>
						</small></p></td>
					</tr>
					
					</tbody>
				</table>
				
				</div>
				
				</div>
				<script>
				$(document).ready(function(){
					$('#refund_admin_reply_<?php echo $return_order_id; ?>').popover({
						html : true,
						content: function() {
							if($(window).width() > 480){
								$('#refund_admin_reply_div_<?php echo $return_order_id; ?>').hide();
								return $('#refund_admin_reply_div_<?php echo $return_order_id; ?>').html();	
							}
						}
					});
				});
				</script>
				
				
				<div class="f-row">
					Comments:<span class="more"><?php echo ucwords($get_refund_data["comments"]);?>	</span>
				</div>
				
				<div class="f-row">
				
				<span id="accept_reject_refund_status_<?php echo $get_refund_data
					["order_item_id"];?>">
						
						<?php
						if($get_refund_data["customer_decision"]=="pending" && $get_refund_data["customer_status"]=="cancel"){
						?>
						Customer Decision : No Decision Made
					<?php
						}

						if($get_refund_data["allow_cust_to_make_decision_flag"]=="0" && $get_refund_data["customer_decision"]=="accept"){
							echo 'Status:Admin Accepted';
						}
						else if($get_refund_data["allow_cust_to_make_decision_flag"]=="1" && $get_refund_data["customer_decision"]=="accept"){
							echo 'Status: Customer Accepted';
						}
						else if($get_refund_data["customer_decision"]=="reject"){
							echo 'Status:Rejected';
						}
					?>	
				</span>
					
				</div>
				
				<?php if($get_refund_data["status"]=="reject"){?>
				<div class="f-row">
					Status : Admin rejected the request
				</div>
					<?php }?>
					
				<?php		
				if($return_data['order_return_decision_customer_status']=="cancel"){
				?>
				<div class="f-row">
					Return Order has been cancelled.
					<b> Comments:</b>
					
					<span class="more">
					<?php
						echo $return_data['order_return_decision_customer_status_comments'];
					?>
					</span>
				</div>
					
				<?php
					}
				?>
				
			</div>
			</div>	
			</div>
				
			<div class="col-md-6">
			<div class="row">
			<div class="col-md-12">
				<div class="f-row">
				
				  <?php if($get_refund_data["status"]!="reject"){?>
						Type of refund : 
					<?php }else{?>
						Reason : 
					<?php } ?>
				  <?php 
					$return_options=$controller->get_primary_return_reason_admin($get_refund_data["sub_reason_return_id"]);	
					?>
					<?php echo $return_options;?>
				
				</div>

				<?php
				$order_status_data=$controller->get_orders_status_data($order_item['order_item_id']);
				
				if($get_refund_data["pickup_identifier"]=="before return"){
					$return_option_status='';
					if($order_status_data["initiate_return_pickup"]=='1' && $order_status_data["order_return_pickup"]==0){
						$return_option_status='It is required to ship the purchased item.';
					}
					if($order_status_data["initiate_return_pickup"]=='1' && $order_status_data["order_return_pickup"]==1){
						$return_option_status='We have received your item.';
					}
					if($order_status_data["initiate_return_pickup"]=='0' && $order_status_data["order_return_pickup"]==0){
						$return_option_status='There is no need to return purchased item';
					}
					if($order_status_data["initiate_return_pickup"]=='' && $order_status_data["order_return_pickup"]==0){
						$return_option_status='';
					}
					echo '<div class="f-row">'.$return_option_status.'</div>';
				}
				if($get_refund_data["pickup_identifier"]=="after return"){
						$return_option_status='';
						if($order_status_data["order_return_pickup"]==0 && $get_refund_data["customer_decision"]!="reject"){
							$return_option_status='It is required to ship the purchased item';
						}
						if($order_status_data["order_return_pickup"]==1){
							$return_option_status='We have received your item.';
						}
						echo '<div class="f-row">'.$return_option_status.'</div>';
				}
				
				?>
				
				<div id="accept_reject_block" class="f-row margin-top">
				

					<?php
					if(($get_refund_data["customer_decision"]=="pending" && $get_refund_data["customer_status"]=="pending") && ($get_refund_data["status"]!="reject")){
					?>
					<input type="button" class="btn btn-success btn-sm preventDflt" value="Accept" onclick="accept_reject_refund_statusFun('accept',<?php echo $get_refund_data
					["order_item_id"];?>)" />
					<input type="button" class="btn btn-danger btn-sm preventDflt" value="Reject" onclick="accept_reject_refund_statusFun('reject',<?php echo $get_refund_data
					["order_item_id"];?>)" />
					<?php
					}
					?>
				
				</div>
				</div>
				</div>
			</div><!--col-md-6-->
			
			</div>
		
		<?php

			}else{					

			
				if($return_data['order_return_decision_customer_status']=="cancel"){
			?>
			
			<div class="row">
			<div class="col-md-6">
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<div class="f-row">		
						Return Order has been cancelled.
					</div>
					<div class="f-row">	
						<strong>Comments :</strong>
						<span class="more">
						<?php
							echo $return_data['order_return_decision_customer_status_comments'];
						?>
						</span>
					</div>
				</div>
				</div>
				</div>
			</div>
			<?php
				
				}

			}
		?>
		
		<!---free sku return display--------->


<!------------------------ Shareesh added starts ----------------------->


		<?php
	//if($orders_status_data["order_return_pickup"]==0){
	
		$return_refund_table=$controller->get_return_refund_table_by_order_item_id($order_item['order_item_id']);
				
		if(count($return_refund_table)>0 && $return_data["order_return_decision_status"]=="refunded"){
			
		?>
		
	<div class="row margin-top">
			<div class="col-md-6">
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<div class="f-row bold">
						<u>Refund Details Of Initial Transaction</u>
					</div>

					<div class="f-row">
						Refunded amount: <?php echo curr_sym;?><?php echo $return_refund_table
						["total_refund_with_concession"];?>
						
						<span id="refund_details_initial_trans_<?php echo $return_order_id; ?>" data-toggle="popover" data-trigger="click" data-placement="bottom" class="cursor-pointer refund_details_initial_trans" data-original-title="" title="" content_id="#refund_details_initial_trans_div_<?php echo $return_order_id; ?>" onclick="toggle_content(this)">[<i class="fa fa-question" aria-hidden="true"></i>]</span>
						
						<div id="refund_details_initial_trans_div_<?php echo $return_order_id; ?>" style="display:none;">
			
							<table class="table table-hover table-bordered">
								
							<tbody>
								
								<tr>
								<td><p><small>Quantity:</small></p></td><td><p><small>  <?php echo $return_refund_table
								["quantity"];?></small></p></td>
								</tr>
								
								<tr>
								<td><p><small>Total Item Price:</small></p></td><td><p><small>  <?php echo curr_sym;?><?php echo $return_refund_table
								["item_price_without_concession"];?></small></p></td>
								</tr>
								
								
								<tr>
								<td><p><small>Total Shipping Charge:</small></p></td><td><p><small>  <?php echo curr_sym;?><?php echo $return_refund_table
								["shipping_price_without_concession"];?></small></p></td>
								</tr>
								
								<tr>
								<td><p><small>Total Price:</small></p></td><td><p><small>   <?php echo curr_sym;?><?php echo $return_refund_table
								["total_refund_without_concession"];?>
								</small></p></td>
								
								</tr>
								
								<tr>
								<td><p><small>Total Item Price Refunded:</small></p></td><td><p><small>  <?php echo curr_sym;?><?php echo $return_refund_table
								["item_with_deductions_concession"];?>
								<?php echo ($return_refund_table["item_deduction_percentage"]!=0)?'('.$return_refund_table["item_deduction_percentage"].'% deducted)' :'';?>
								</small></p></td>
								</tr>
								<tr>
								<td><p><small>Total Shipping Charge Refunded:</small></p></td><td><p><small>  <?php echo curr_sym;?><?php echo $return_refund_table
								["shipping_with_deductions_concession"];?> 
								<?php echo ($return_refund_table["shipping_deduction_percentage"]!=0)?'('.$return_refund_table["shipping_deduction_percentage"].'% deducted)' :'';?></small></p></td>
								</tr>
								
								
								<tr>
								<td><p><small>Return Shipping Concession:</small></p></td><td><p><small> <?php echo curr_sym;?><?php echo $return_refund_table
								["return_shipping_concession"];?></small></p></td>
								</tr>
								<tr>
								<td><p><small>Return Other Concession:</small></p></td><td><p><small>  <?php echo curr_sym;?><?php echo $return_refund_table
								["return_other_concession"];?></small></p></td>
								</tr>
								<tr>
								<td><p><small>Total Refunded:</small></p></td><td><p><small> <?php echo curr_sym;?><?php echo $return_refund_table
								["total_refund_with_concession"];?></small></p></td>
								</tr>

							</tbody>
							
							</table>
							
						</div>
						
					</div>
					
					<script>
					$(document).ready(function(){
						$('#refund_details_initial_trans_<?php echo $return_order_id; ?>').popover({
							html : true,
							content: function() {
								if($(window).width() > 480){
									$('#refund_details_initial_trans_div_<?php echo $return_order_id; ?>').hide();
									return $('#refund_details_initial_trans_div_<?php echo $return_order_id; ?>').html();	
								}
							}
						});
					});
					</script>
					
					<div class="f-row">
						<?php 
						
						if($order_item["quantity"]==$return_refund_table["quantity"]){
							echo '<div class="f-row">Invoice Cancelled</div>';					
						}
						if($return_data['order_return_decision_status']=='refunded'){
							echo '<div class="f-row">Refund Method: ';
							
							if($return_data['refund_method']=="Bank Transfer"){
								$refund_bank_id=$return_data['refund_bank_id'];	
								$bank_detail_arr=$controller->get_bank_details($refund_bank_id);
								
								echo '<a id="bank_detail_'.$return_order_id.'" data-toggle="popover" data-trigger="click" data-placement="bottom" class="bank_detail_replace_refund3" content_id="#bank_detail_div_'.$return_order_id.'" onclick="toggle_content(this)" style="cursor:pointer;"><u>Bank Account</u></a>';
								
								echo '<div id="bank_detail_div_'.$return_order_id.'" style="display:none;">'.$bank_detail_arr.'</div>';
							}else{
								echo $return_data['refund_method'];
							}
							
							echo '</div>';
							
							?>
							
							<script>
							$(document).ready(function(){
								$('#bank_detail_<?php echo $return_order_id; ?>').popover({
									html : true,
									content: function() {
									  
										if($(window).width() > 480){
											$('#bank_detail_div_<?php echo $return_order_id; ?>').hide();
											return $('#bank_detail_div_<?php echo $return_order_id; ?>').html();	
										}
									}
								});
							});
							</script>
							
							<?php
							
							if($return_data['refund_method']=="Bank Transfer"){
											
								echo '<div class="f-row margin-top"> Transaction ID: '.$return_data['transaction_id'].'</div>';
								echo '<div class="f-row"> Transaction Date: '.date("d-m-Y", strtotime($return_data['transaction_date'])).'</div>';
								
								echo '<div class="f-row">Transaction Time: '.$return_data['transaction_time'].'</div>';
								$get_refund_data=$controller->get_order_details_admin_acceptance_refund_data($order_item['order_item_id']);
							}
							?>
							<?php if($return_data['refund_method']=="Bank Transfer"){ ?>
							
							<div class="f-row">
							Transaction Amount Refunded: <?php echo curr_sym;?><?php echo $return_refund_table["total_refund_with_concession"];?>
							</div>

							<?php } ?>
							
							<?php
							
							}
						?>
	
					</div>
					
				</div>
			</div>
			</div>
		
		<div class="col-md-6 entire">
		<div class="row">
		<div class="col-md-12 entire">
	
			<?php
				
				//print_r($order_item);
				
				$temp_flag=0;
				
				$refund_more_btn=false;
				
				$replacement_desired_arr=$controller->get_replacement_desired_data_by_order_item_id($order_item['order_item_id']);
				
				$order_replacement_decision_arr=$controller->get_replaced_quantity_in_replacements_by_order_item_id($order_item['order_item_id']);
				$order_status_data=$controller->get_orders_status_data($order_item['order_item_id']);
				if(!empty($replacement_desired_arr)){

						if(!empty($order_replacement_decision_arr)){
						
							if($order_replacement_decision_arr["status"]=="replaced" || $order_replacement_decision_arr["status"]=="refunded" || $order_replacement_decision_arr["status"]=="refund" ||$order_replacement_decision_arr["status"]=="reject" || ($replacement_desired_arr["order_replacement_decision_customer_status"]=="cancel"  && $order_status_data["order_replacement_pickup"]==0 && $get_replacement_data["pickup_identifier"]=="after replace")  || ($replacement_desired_arr["order_replacement_decision_customer_status"]=="cancel" && $replacement_desired_arr["order_replacement_decision_refund_status"]=="refunded" && $order_status_data["order_replacement_pickup"]==1 && $get_replacement_data["pickup_identifier"]=="after replace")){
								
								$temp_flag++;			
							
								$return_refund_request_again_arr=$controller->return_refund_request_again($order_item['order_item_id']);
								
								if(count($return_refund_request_again_arr)==0){
									$temp_flag++;	
								}
								if(($order_item["quantity"]*($order_item["product_price"]+$order_item["shipping_charge"]))>($return_refund_table["item_with_deductions_concession"]+$return_refund_table["shipping_with_deductions_concession"])){
									
									$temp_flag++;	
								}
							}							
						
						}else{
							if($replacement_desired_arr["order_replacement_decision_customer_status"]=="cancel"){
								
								$temp_flag++;
							}
						}					
				}else{
					
					$temp_flag++;
				}
				
				
				if(!empty($replacement_desired_arr)){
					
					if(($replacement_desired_arr["order_replacement_decision_customer_status"]=="cancel"  && $order_status_data["order_replacement_pickup"]==0 && $get_replacement_data["pickup_identifier"]=="before replace")  || ($replacement_desired_arr["order_replacement_decision_customer_status"]=="cancel"  && $order_status_data["order_replacement_pickup"]==0 && $get_replacement_data["pickup_identifier"]=="before replace" && $replacement_desired_arr["order_replacement_decision_refund_status"]=="refunded" && $replacement_desired_arr["payment_status"]=="paid")){
						
							$temp_flag++;
					}
				}
				
				if($temp_flag!=0){
					$refund_more_btn=true;
				}else{
					$refund_more_btn=false;
				}
				if(count($return_refund_request_again_arr)>0){
					
					$refund_more_btn=false;
				}
				
				if($order_status_data["order_return_pickup"]==0 && $order_status_data["initiate_return_pickup"]!=0){
					
					$refund_more_btn=false;
				}
			if($refund_more_btn){
			//	print_r($get_refund_data);
	
			$quantity_filled=0;
			
			$sub_refunded_details_from_returned_orders_table_arr=$controller->get_sub_refunded_details_from_returned_orders_table($order_item['order_item_id']);

			if(count($return_refund_table)>0){
				$quantity_filled+=($return_refund_table["quantity"]);
			}
			if(count($sub_refunded_details_from_returned_orders_table_arr)>0){
				$quantity_filled+=($sub_refunded_details_from_returned_orders_table_arr['quantity']);	
			}
			if((count($order_replacement_decision_arr)>0 && $order_replacement_decision_arr["status"]=="refund") || (count($order_replacement_decision_arr)>0 && $order_replacement_decision_arr["status"]=="replaced") || (count($order_replacement_decision_arr)>0 && $order_replacement_decision_arr["status"]=="refunded") || (count($order_replacement_decision_arr)>0 && $order_replacement_decision_arr["customer_status"]=="cancel" && $order_replacement_decision_arr["refund_status"]=="refunded" && $order_status_data["order_replacement_pickup"]==1)){
				$quantity_filled+=$order_replacement_decision_arr["quantity"];				
			}	
				
			$quantity_left=$order_item['quantity']-($quantity_filled);
			
			if($get_refund_data["product_price_with_offer_percentage"]==0 && $get_refund_data["shipping_charge_with_offer_percentage"]==0 && $quantity_left==0){
				$refund_more_btn=false;	
			}
			if($refund_more_btn){
			?>
			
			<?php
					
						$status_of_refund_for_cashback_on_sku="";
						if($order_item['promotion_cashback']>0){
						?>
						<label class="alert alert-warning">
									<?php
									$order_item_offers_obj=$controller->get_details_of_order_item($order_item['order_item_id']);
										
										$status_of_refund_for_cashback_on_sku=$order_item_offers_obj->status_of_refund;
										if($order_item_offers_obj->status_of_refund=="pending" && $order_item_offers_obj->customer_response=="reject"){
											echo 'Customer rejected the Cash back offer';
										}
			
									?>
									<input type="hidden" name="status_of_refund_for_cashback_on_sku" id="status_of_refund_for_cashback_on_sku_<?php echo $order_item['order_item_id'] ?>" value="<?php echo $status_of_refund_for_cashback_on_sku; ?>">
						</label>
							
							<?php
						}
							?>


				<input type="button" value="Request Refund" class="button preventDflt" onclick="showRefundMoreDiv(<?php echo $order_item['order_item_id'];?>)">
		
				<input type="hidden" id="quantity_filled_<?php echo $order_item['order_item_id'];?>" name="quantity_filled" value="<?php echo $quantity_filled;?>" >
			<?php } ?>
			
			
			<div class="row align_top" id="refund_more_<?php echo $order_item['order_item_id'];?>" style="display:none;">
				<?php
					
					$order_item_id=$order_item['order_item_id'];

				?>
		<div class="col-md-12">
		<form id="sub_request_refund_<?php echo $order_item['order_item_id'];?>" class="form-horizontal">
				
				<input type="hidden" name="order_item_id" value="<?php echo $order_item['order_item_id'];?>">
				<input type="hidden" name="return_order_id" value="<?php echo $return_refund_table['return_order_id'];?>">
				<input type="hidden" name="refund_method" value="<?php echo $return_refund_table['refund_method'];?>">
				<input type="hidden" name="refund_bank_id" value="<?php echo $return_refund_table['refund_bank_id'];?>">
				<input type="hidden" name="remaining_quantity" value="<?php echo $quantity_left;?>">
				
				
				<!----promo details-------------->
				<input type="hidden" id="promotion_available_<?php echo $order_item_id;?>" name="promotion_available" value="<?php echo $order_item["promotion_available"];?>">
				<?php 
				if($order_item["promotion_available"]==1){
					
					$promotion_quote=$order_item["promotion_quote"];
					$promotion_quote=trim(preg_replace('/\s+/', ' ', $promotion_quote));
				?>
				
				<input type="hidden" id="shipping_price_waived_<?php echo $order_item_id;?>" name="shipping_price_waived" value="<?php echo $shipping_charge_waived; ?>">
				
				<input type="hidden" id="ordered_quantity_<?php echo $order_item_id;?>" name="ordered_quantity" value="<?php echo $order_item["quantity"];?>">
				<input type="hidden" id="price_paid_perunit_<?php echo $order_item_id;?>" name="price_paid_perunit" value="<?php echo $order_item["product_price"];?>">
				
				<input type="hidden" id="price_paid_shipping_<?php echo $order_item_id;?>" name="price_paid_shipping" value="<?php echo $order_item["shipping_charge"];?>">
				
				<input type="hidden" id="ordered_quantity_<?php echo $order_item_id;?>" name="ordered_quantity" value="<?php echo $order_item["quantity"];?>">
				
				
				<input type="hidden" id="promotion_minimum_quantity_<?php echo $order_item_id;?>" name="promotion_minimum_quantity" value="<?php echo $order_item["promotion_minimum_quantity"];?>">
				
				
				<input type="hidden" id="promotion_quote_<?php echo $order_item_id;?>" name="promotion_quote" value="<?php echo $promotion_quote; ?>">
				<input type="hidden" id="promotion_default_discount_promo_<?php echo $order_item_id;?>" name="promotion_default_discount_promo" value="<?php echo $order_item["promotion_default_discount_promo"]; ?>">
				
				<input type="hidden" id="promotion_item_<?php echo $order_item_id;?>" name="promotion_item" value="<?php echo rtrim($order_item["promotion_item"],','); ?>">
				<input type="hidden" id="promotion_item_num_<?php echo $order_item_id;?>" name="promotion_item_num" value="<?php echo $order_item["promotion_item_num"]; ?>">
				
				<input type="hidden" id="promotion_surprise_gift_<?php echo $order_item_id;?>" name="promotion_surprise_gift" value="<?php echo $order_item["promotion_surprise_gift"]; ?>">
				<input type="hidden" id="promotion_surprise_gift_type_<?php echo $order_item_id;?>" name="promotion_surprise_gift_type" value="<?php echo $order_item["promotion_surprise_gift_type"]; ?>">
				<input type="hidden" id="promotion_surprise_gift_skus_<?php echo $order_item_id;?>" name="promotion_surprise_gift_skus" value="<?php echo $order_item["promotion_surprise_gift_skus"]; ?>">
				<input type="hidden" id="promotion_surprise_gift_skus_nums_<?php echo $order_item_id;?>" name="promotion_surprise_gift_skus_nums" value="<?php echo $order_item["promotion_surprise_gift_skus_nums"]; ?>">
				
				<input type="hidden" id="promotion_cashback_<?php echo $order_item_id;?>" name="promotion_cashback" value="<?php echo $order_item["promotion_cashback"]; ?>">
				<input type="hidden" id="cash_back_value_<?php echo $order_item_id;?>" name="cash_back_value" value="<?php echo $order_item["cash_back_value"]; ?>">
				<input type="hidden" id="promotion_item_multiplier_<?php echo $order_item_id;?>" name="promotion_item_multiplier" value="<?php echo $order_item["promotion_item_multiplier"]; ?>">
				<input type="hidden" id="promotion_discount_<?php echo $order_item_id;?>" name="promotion_discount" value="<?php echo $order_item["promotion_discount"]; ?>">
				<input type="hidden" id="default_discount_<?php echo $order_item_id;?>" name="default_discount" value="<?php echo $order_item["default_discount"]; ?>">
				
				<input type="hidden" id="individual_price_of_product_with_promotion_<?php echo $order_item_id;?>" name="individual_price_of_product_with_promotion" value="<?php echo $order_item["individual_price_of_product_with_promotion"];?>">
				
				<input type="hidden" id="individual_price_of_product_without_promotion_<?php echo $order_item_id;?>" name="individual_price_of_product_without_promotion" value="<?php echo $order_item["individual_price_of_product_without_promotion"]; ?>">
				<input type="hidden" id="total_price_of_product_with_promotion_<?php echo $order_item_id;?>" name="total_price_of_product_with_promotion" value="<?php echo $order_item["total_price_of_product_with_promotion"]; ?>">
				<input type="hidden" id="total_price_of_product_without_promotion_<?php echo $order_item_id;?>" name="total_price_of_product_without_promotion" value="<?php echo $order_item["total_price_of_product_without_promotion"]; ?>">
				
				<!---parameters will change based on the remaining quantity---->
				
				<input type="hidden" id="req_quantity_without_promotion_<?php echo $order_item_id;?>" name="req_quantity_without_promotion" value="">
				<input type="hidden" id="req_quantity_with_promotion_<?php echo $order_item_id;?>" name="req_quantity_with_promotion" value="">
				
				<input type="hidden" id="req_cash_back_value_<?php echo $order_item_id;?>" name="req_cash_back_value" value="">
				<input type="hidden" id="req_individual_price_of_product_with_promotion_<?php echo $order_item_id;?>" name="req_individual_price_of_product_with_promotion" value="">
				<input type="hidden" id="req_individual_price_of_product_without_promotion_<?php echo $order_item_id;?>" name="req_individual_price_of_product_without_promotion" value="">
				<input type="hidden" id="req_total_price_of_product_with_promotion_<?php echo $order_item_id;?>" name="req_total_price_of_product_with_promotion" value="">
				<input type="hidden" id="req_total_price_of_product_without_promotion_<?php echo $order_item_id;?>" name="req_total_price_of_product_without_promotion" value="">
				
				<input type="hidden" id="free_inventory_available_<?php echo $order_item_id ?>" name="free_inventory_available" value="">				
				<input type="hidden" id="surprise_free_skus_from_inventory_<?php echo $order_item_id ?>" name="surprise_free_skus_from_inventory" value="">				
				
			<?php
			if($order_item["promotion_item"]!=''){
				$free_inventory_available=1;
				$promo_item=$order_item["promotion_item"];
				$promotion_item_num=$order_item["promotion_item_num"];
				$free_skus_from_inventory=$controller->get_free_skus_from_inventory($promo_item,$promotion_item_num);
				
				?>
				<script>
				var order_item_id='<?php echo $order_item_id; ?>';
				var js_value=JSON.stringify(<?php echo json_encode($free_skus_from_inventory); ?>);
				$("#free_inventory_available_"+order_item_id).val(js_value);
				</script>
				
				
				<?php
				//print_r($free_skus_from_inventory);

			}else{
				$free_inventory_available=0;
				$free_skus_from_inventory=array();
				?>
				<script>
				var order_item_id='<?php echo $order_item_id; ?>';
				var js_value=JSON.stringify(<?php echo json_encode($free_skus_from_inventory); ?>);
				$("#free_inventory_available_"+order_item_id).val(js_value);
				</script>
				<?php
				

			}
			
			?>

			<?php
			if($order_item["promotion_surprise_gift_type"]=='Qty'){
				$surprise_promo_item=$order_item["promotion_surprise_gift_skus"];
				$surprise_promo_item_num=$order_item["promotion_surprise_gift_skus_nums"];
				$surprise_free_skus_from_inventory=$controller->get_free_skus_from_inventory($surprise_promo_item,$surprise_promo_item_num);
				
				?>
				<script>
				var order_item_id='<?php echo $order_item_id; ?>';
				var js_value=JSON.stringify(<?php echo json_encode($surprise_free_skus_from_inventory); ?>);
				$("#surprise_free_skus_from_inventory_"+order_item_id).val(js_value);
				</script>
				
				
				<?php
				//print_r($free_skus_from_inventory);

			}else{
				
				$surprise_free_skus_from_inventory=array();
				?>
				<script>
				var order_item_id='<?php echo $order_item_id; ?>';
				var js_value=JSON.stringify(<?php echo json_encode($surprise_free_skus_from_inventory); ?>);
				$("#surprise_free_skus_from_inventory_"+order_item_id).val(js_value);
				</script>
				<?php
				

			}
			
			?>
			
				<?php
				
				}//if promotion available
				
				?>
				<!----promo details-------------->
				
				<?php

				if($get_refund_data["product_price_with_offer_percentage"]==0 && $get_refund_data["shipping_charge_with_offer_percentage"]==0){
						//both are not deducted.
				}else{
					
				?>
				<div class="form-group">
				<div class="col-md-12">
					<input type="checkbox" id="refund_remaining_deductions_<?php echo $order_item['order_item_id'];?>" name="refund_remaining_deductions" value="refund_remaining_deductions" onclick="refund_remaining_deductionsFun(<?php echo $order_item['order_item_id'];?>)"> Refund remaining deductions
				</div>
				</div>
				<?php
				}
				?>
				
			<?php if($quantity_left>0){ ?>	
				<div class="form-group">
				<div class="col-md-12">
					<input type="checkbox" id="refund_remaining_quantities_<?php echo $order_item['order_item_id'];?>" name="refund_remaining_quantities" value="refund_remaining_quantities" onclick="refund_remaining_quantitiesFun(this,<?php echo $order_item['order_item_id'];?>)"> Refund remaining quantities
	
					<div style="display:none" id="refund_remaining_quantities_input_div_<?php echo $order_item['order_item_id'];?>" >
									
						<input type="hidden" name="quantity_left_original" id="quantity_left_original_<?php echo $order_item['order_item_id']; ?>" value="<?php echo $quantity_left; ?>">

									(Max remaining quantities <?php echo $quantity_left;?>)
						
									<?php

								if(($order_item['promotion_available']==1 && $order_item['promotion_item_num']!="") || ($order_item['promotion_available']==1 && $order_item['promotion_surprise_gift']!="") ){
									
									$promotion_minimum_quantity=$order_item['promotion_minimum_quantity'];
									$modulo=intval($order_item['quantity']%$promotion_minimum_quantity);//remainder
									$quotient=intval($order_item['quantity']/$promotion_minimum_quantity);
									$refund_option_arr=array();
									
									//echo $promotion_minimum_quantity;
									for($u=1;$u<=$quantity_left;$u++){
										
										$r=intval($u*$promotion_minimum_quantity);
										//if($r>=$moq){
											if($r<=$quantity_left){
												
												$refund_option_arr[]=$r;
												if($modulo>0){
													$refund_option_arr[]=intval($r+$modulo);
												}
											}
										//}
									}
									//print_r($refund_option_arr);
									
									?>
									
									<select class="form-control" onchange="call_function_promo(this,'<?php echo $order_item_id; ?>')"  name="refund_remaining_quantities_input" id="refund_remaining_quantities_input_<?php echo $order_item['order_item_id'];?>" required>
									
									<?php
										if(!in_array($quantity_left,$refund_option_arr)){
											if($quantity_left<$promotion_minimum_quantity){
												echo '<option value="'.$quantity_left.'" selected>'.$quantity_left.'</opion>';
											}
										}
										foreach($refund_option_arr as $qty){
											
											echo '<option value="'.$qty.'">'.$qty.'</option>';
										}
										
									?>
									</select>
									
									<?php
								
								}else{
									
									?>
									<select class="form-control" name="refund_remaining_quantities_input" id="refund_remaining_quantities_input_<?php echo $order_item['order_item_id'];?>" onchange="call_function_promo(this,'<?php echo $order_item_id; ?>')" required>

									<?php
									
										for($m=1;$m<=$quantity_left;$m++){
											echo '<option value="'.$m.'">'.$m.'</option>';
										}
									?>
									
									</select>
									
									<?php /*?> <input type="number" name="refund_remaining_quantities_input" id="refund_remaining_quantities_input_<?php echo $order_item['order_item_id'];?>"  value="<?php echo $quantity_left;?>" min="<?php if($quantity_left==0){ echo "0";}else{ echo "1";}?>" max="<?php echo $quantity_left;?>" style="background-color:lightgrey" disabled onchange="call_function_promo(this,'<?php echo $order_item_id; ?>')">
									
									<?php */?>
									 
									<?php
								}
							?>							
						
					</div>
				</div>
				</div>
				<div class="form-group">
				<div class="col-md-12">
					<input type="checkbox" id="refund_all_<?php echo $order_item['order_item_id'];?>" name="refund_all" value="refund_all" onclick="refundAllFun(<?php echo $order_item['order_item_id'];?>)"> Refund all
				</div>
				</div>
			<?php } ?>
				<div class="form-group">
				<div class="col-md-12" id="refund_order_summary_<?php echo $order_item_id; ?>">
				
				</div>
				</div>
				<div class="form-group">
				<div class="col-md-12">
					<input type="button" value="Submit" class="btn btn-success text-uppercase preventDflt" onclick="sub_request_refundFun(<?php echo $order_item['order_item_id'];?>)" id="sub_request_refund_btn_<?php echo $order_item_id;?>">
				</div>
				</div>
				
		</form>
			</div>
			</div>
			<?php
			}
			?>
			
		</div>
		</div>
		</div>
		
	</div>
		
		<?php }//} ?>

		
		
		<?php include 'second_refund_request.php';?>

		<?php include 'cancel_refund.php' ?>	
		
		
		</div><!----refund_div------>
		
		<?php 
		
		}//foreach
		
	}//returns if
					if(!empty($get_data_from_return_stock_notification_return_order_id)){ ?>
										<div class="stock_div">
										<?php include 'return_stock_notification.php' ?>
										</div>
										<?php } ?>
		
							</div>
						</div>
					</div>
				</div>		
										
											
									<?php } 
									 }
									?>
<!-------------------------------------accordian ends ---------------------------------------------------->


<script>
function cancel_refund_request(return_stock_notification_id){
	
	/*bootbox.confirm({
		message: "Are u sure to cancel this request?",
		size: "small",
		callback: function (result) {
			if(result){*/
			if(confirm("Are u sure to cancel this request?")){

			$.ajax({
			url:"<?php echo base_url()?>Account/cancel_refund_request",
			type:"POST",
			data:"return_stock_notification_id="+return_stock_notification_id,
			success:function(data){
				
				if(data=true){
					$("#accept_reject_replacement_status_"+order_item_id).html("<b>Status:</b> Rejected");
					/*bootbox.alert({
						  size: "small",
						  message: 'Successfully cancelled',
						   callback: function () { location.reload(); }
						  
						});*/
                    alert('Successfully cancelled');
					location.reload();
				}else{
					$("#accept_reject_replacement_status_"+order_item_id).html("<b>Status:</b> Rejected");
					/*bootbox.alert({
					  size: "small",
					  message: 'error',
					  callback: function () { location.reload(); }
					  
					});*/
                    alert('Error');
                    location.reload();
				}
				
				
			}
		});
			}
	/*	}
	});
	*/
}
</script>