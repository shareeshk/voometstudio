<style>
    hr{
		margin:0;
	}
	.comment {
		width: 400px;
		background-color: #f0f0f0;
		margin: 10px;
	}
</style>
<div class="columns-container">
    <div class="container" id="columns">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account">My account</a></li>
		  <li class="breadcrumb-item active">Reviews</li>
		</ol>
        <div class="row">
			<?php require_once 'leftcolum.php';?>
            <div class="center_column col-xs-12 col-sm-9 cards" id="center_column">
				<div class="panel panel-default">
					<div class="panel-heading lead">Reviews by <?php echo $customer_name; ?> (<?php echo count($reviews); ?>)</div>
					

					
				<?php
				if(!empty($reviews)){ 
						$i=1;
						?>
						<div class="panel-body border-down common">
						<?php
						foreach($reviews as $rev_obj){
							?>
							
							
							
								<div class="row">
								<div class="col-sm-12">
								<div class="well">
								<div class="row">
									<div class="col-sm-2 products-block">
										<div class="text-center">
                                        <a href="#">
                                            <img src="<?php echo base_url().$rev_obj->thumbnail; ?>" alt="SPECIAL PRODUCTS" style="height:25vh;">
                                        </a>
                                    </div>
									</div>
									<div class="col-sm-10 products-block">
										<div class="f-row product-name">
										<strong>
                                            <a href="<?php echo base_url()?>detail/<?php echo $controller->sample_code().$rev_obj->inventory_id;?>"><?php echo $controller->get_product_name($rev_obj->product_id);?></a>
										</strong>
                                        </div>
										<div class="f-row text-primary">
										
											<?php
										$rev_obj->attribute_1_value= substr($rev_obj->attribute_1_value, 0, strpos($rev_obj->attribute_1_value, ":"));
										
										if(!empty($rev_obj->attribute_1))
											echo '<div class="f-row">'.$rev_obj->attribute_1." : ".$rev_obj->attribute_1_value.'</div>';
										if(!empty($rev_obj->attribute_2))
											echo '<div class="f-row">'.$rev_obj->attribute_2." : ".$rev_obj->attribute_2_value.'</div>';
										if(!empty($rev_obj->attribute_3))
											echo '<div class="f-row">'.$rev_obj->attribute_3." : ".$rev_obj->attribute_3_value.'</div>';
										if(!empty($rev_obj->attribute_4))
											echo '<div class="f-row">'.$rev_obj->attribute_4." : ".$rev_obj->attribute_4_value.'</div>';
										?>
											
										
										</div>
										
                                       <div class="f-row">
											
											<?php 
											echo $customer_name." rated :";
											?>
										
											
											<?php 
											$rating=$rev_obj->rating;
											for($k=0;$k<$rating;$k++){
												echo ' <i class="fa fa-star" style="color:#ff9900;"></i>';
											}
											$minus=5-$rating;
											if($minus!=0){
												for($l=0;$l<$minus;$l++)
												{
													echo '<i class="fa fa-star" style="color:#ccc;"></i>';
												}
											}
											
											?>
										</div>
										<div  class="f-row margin-top"><strong><?php echo $rev_obj->review_title; ?></strong> on <?php echo date_format(date_create($rev_obj->posteddate), " j F y") ?></div>
										
										<div class="f-row margin-top more">
											<?php echo $rev_obj->review; ?>	
										</div>
                                   
									</div>
									</div>
								</div>
								</div>
								</div>

								
								
								
						
							<?php
							$i++;
						}
						?>
						</div>
					
				
						<?php 
				}else{
					$i=0;
					echo "<p class='lead text-center big'>No Reviews were added</p>";
				} ?>

				
				
				
				</div>
				
				<?php
					
					if($i>5){
						$style='';
					}else{
						$style='style="display:none;"';
					}

				?>
				
				<div class="f-row margin-top" id="pagination_div" <?php echo $style; ?>>
					<div class="sortPagiBar">
					<div class="bottom-pagination">
						<span class="nav_pag"></span>
					</div>
					</div>
				</div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<script>
	function displayAddCardForm(){
		document.getElementById('add_card_container').style.display='block';
	}
	function hideAddCardForm(){
		document.getElementById('add_card_container').style.display='none';
	}
</script>
<script>
	function askConfirmationCardDefault(obj){

		/*bootbox.confirm({
			message: "Do you want to make this card Default ?",
			size: "small",
			callback: function (result) {
				if(result){
					window.location.href =obj.getAttribute('actions')
				}
			}
		});*/

        if(confirm("Do you want to make this card Default ?")){
            window.location.href =obj.getAttribute('actions');
        }
		
				
	}
	function askConfirmationCardDelete(obj){
		
		/*bootbox.confirm({
			message: "Do you want to delete this card ?",
			size: "small",
			callback: function (result) {
				if(result){
					window.location.href =obj.getAttribute('actions')
				}
			}
		});*/

        if(confirm("Do you want to delete this card ?")){
            window.location.href =obj.getAttribute('actions');
        }
		
				
	}
	function isNumber(evt){
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if(charCode > 31 && (charCode < 48 || charCode > 57) && charCode!=46) {
        return false;
    }
    return true;
}
</script>


<script type="text/javascript">
	jQuery(document).ready(function($){	
		$('.common').paginathing({
	    perPage:5,
	    insertAfter: '.nav_pag'
		});
	});

$(document).ready(function(){
	showChar(200);
});
</script>