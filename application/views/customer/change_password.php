<style>
#current_password_message,#new_password_message,#confirm_new_password_message{
	color:#ff0000;
}
</style>
<div class="columns-container">
    <div class="container" id="columns">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account">My account</a></li>
		  <li class="breadcrumb-item active">Change Password</li>
		</ol>
        <div class="row">
			<?php require_once 'leftcolum.php';?>
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
				<div class="panel panel-default">
				<div class="panel-heading">Change Password</div>
				<div class="panel-body">
				<div class="form-horizontal">
				<div id="current_password_container" class="form-group">
					<label class="control-label col-md-3">Current Password</label>
					<div class="col-md-6">
					<input type="password" class="form-control" id="current_password" name="current_password" onblur="checkCurrentPassword()" value="" placeholder="Enter your current password">
					<span id="current_password_message"></span>
				</div>
				</div>
				</div>
				<div id="new_password_fields">
				<form id="change_password" action="<?php echo base_url()?>Account/update_password" method="post" class="form-horizontal">
					<div class="form-group">
					<label class="control-label col-md-3">New Password:</label>
					<div class="col-md-6">
					<input type="password" class="form-control" onblur="check_password_strength()"  id="new_password" name="new_password" value="" placeholder="Enter your new password">
					<span id="new_password_message"></span>
					</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Confirm Password:</label>
						<div class="col-md-6">
						<input type="password" class="form-control" onkeyup="check_password_match()" disabled id="confirm_new_password" name="confirm_new_password" value="" placeholder="Confirm the above password">
						<span id="confirm_new_password_message"></span>
						</div>
					</div>
					<div class="form-group">
					<div class="col-md-6 col-md-offset-3">
					  <button type="submit" disabled id="submit_button" class="button btn-block preventDflt"> Update </button>
					</div>
				</form>
				</div>
				</div>
			  </div>
            				
							

            		
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
</div>
<script>
	function checkCurrentPassword() {
		var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
		 var current_password = document.getElementById('current_password').value; 
		
		 var xhr = new XMLHttpRequest();
				
				if (xhr) {
					
		            xhr.onreadystatechange = function () {
		                if (xhr.readyState == 4 && xhr.status == 200) {
		                   if(xhr.responseText==1){
		                   	document.getElementById('current_password_message').innerHTML=""
		                   	document.getElementById('confirm_new_password').disabled = false;
		                   	document.getElementById('new_password').disabled = false;
		                   	document.getElementById('current_password_container').remove();
		                   }
		                   else{
		                   	document.getElementById('current_password_message').innerHTML="Please enter your valid password";
		                   	document.getElementById('confirm_new_password').disabled = true;
		                   	document.getElementById('new_password').disabled = true;

		                   }
		                }
		            }
		            xhr.open('POST', "<?php echo base_url()?>Account/check_current_password", true);
		            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		            xhr.send("current_password="+current_password);
		        }
		}
	 function check_password_strength(){
			var new_pass=document.getElementById('new_password').value;
			var pass_ex=/(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z]).{6,10}/;
			
			if(pass_ex.test(new_pass)){
				if(document.getElementById('confirm_new_password').value!=""){
					check_password_match();
					document.getElementById('confirm_new_password').disabled = false;
				}
				else{
					document.getElementById('confirm_new_password').disabled = false;
					document.getElementById('new_password_message').innerHTML=""
				}
				
			}
			else{
				document.getElementById('confirm_new_password').disabled = true;
				document.getElementById('new_password_message').innerHTML="Must contain one lower & uppercase letter, and one non-alpha character (a number or a symbol.), min six characters to max ten characters"
				document.getElementById('submit_button').disabled = true;
			}
		}
	function check_password_match(){
		var new_pass=document.getElementById('new_password').value;
		var confirm_new_password=document.getElementById('confirm_new_password').value;
		if(new_pass==confirm_new_password){
			document.getElementById('submit_button').disabled = false;
			document.getElementById('confirm_new_password_message').innerHTML="";
			document.getElementById('new_password_message').innerHTML=""
		}
		if(new_pass!=confirm_new_password){
			document.getElementById('submit_button').disabled = true;
			document.getElementById('confirm_new_password_message').innerHTML="New password and confirm password do not match"
		}
	}

    $('form#change_password').submit(function(){
        $(this).find(':button[type=submit]').prop('disabled', true);
    });
</script>
