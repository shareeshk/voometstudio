<style>
.nav > li > a {
    position: relative;
    display: block;
    padding: 2px 15px;
}
.top_align
{
	margin-bottom: 10px;
}
a:hover {
    color: #ccc;
    text-decoration: none;
    transition: all 0.25s;
}
</style>
<div class="columns-container">
    <div class="container" id="columns">
       <ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account">Blogs</a></li>
		  <li class="breadcrumb-item active">All Blogs</li>
		</ol>
        <div class="row">
			<?php require_once 'leftcolum.php';?>
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
				<div class="panel panel-default">
				  <div class="panel-heading">All Blogs</div>
				  <div class="panel-body">
				  <div class="row">
				      <div class="col-sm-12">
				          <table id="all_blogs_table" class="table table-striped table-bordered dt-responsive no-wrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Sl.No</th>
                            <th>Title</th>
                            <th>Topic</th>
                            <th>Modified</th>
                            <th>Status</th>
                            <th>Preview</th>
                        </tr>
                    </thead>
                    <tbody>
  
                    </tbody>
                </table>
				      </div>
					  </div>
				  </div>
				</div>
               
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   
   var table = $('#all_blogs_table').DataTable({
       
        "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>Account/getAllBlogs", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#all_blogs_table_processing").css("display","none");
            }
          },
          
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'1%',
         'className': 'dt-body-center'
      }],
      'order': [0, 'desc']
   });
}); 
</script> 
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>