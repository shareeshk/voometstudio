<div class="columns-container">
    <div class="container" id="columns">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account">My account</a></li>
		  <li class="breadcrumb-item active">Bank Information</li>
		</ol>
        <div class="row">
			<?php require_once 'leftcolum.php';?>
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
				<div class="well">
				<div class="panel panel-default">
    <div class="panel-heading">Bank Information</div>
    <div class="panel-body"><?php if(!empty($bank_detail)){ ?>
					<table id="bank_details" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>S.No</th>
			                <th>Action</th>
							<th>Bank Name</th>
							<th>Branch Name</th>
							<th>City</th>
			                <th>IFSC code</th>
			                <th>Account number</th>
			                <th>Account holder name</th>
			                <th>Mobile Number</th>
			                <th>Date Added</th>
			                
			            </tr>
			        </thead>
			        <tbody>
			        	
			        	<?php $i=1; foreach ($bank_detail as $bank){
							$bank=$bank[0];
							?>
			        	<tr>
			                <td><?php echo $i ?></td>
			                <td><button class="btn btn-sm btn-warning preventDflt" onclick="update_bank_details(<?php echo $bank['id']?>)">Update</button> &nbsp;<button class="btn btn-sm btn-danger preventDflt">Delete</button></td>
							<td><?php echo $bank['bank_name']?></td>
							<td><?php echo $bank['branch_name']?></td>
							<td><?php echo $bank['city']?></td>
			                <td><?php echo $bank['ifsc_code']?></td>
			                <td><?php echo $bank['account_number']?></td>
			                <td><?php echo $bank['account_holder_name']?></td>
			                <td><?php echo $bank['mobile_number']?></td>
			                <td><?php echo $bank['timestamp']?></td>
			                
			            </tr>
			            
			        	<?php $i++; }
						?>
			        </tbody>
			    </table>
				
			    <?php }
else{
	echo "<b>No Bank Details To Be Shown</b>";
}
			    ?></div>
  </div>
            	
            	</div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<script>
function update_bank_details(bank_id){
	location.href="<?php echo base_url()?>Account/update_bank_details/"+bank_id;
}
</script>