<div class="row">
						<div class="col-md-4 products-block">
							<div class="row">
								<div class="col-md-3">
									<a href="#">
										<img src="<?php echo base_url().$repl_obj->thumbnail; ?>" alt="SPECIAL PRODUCTS">
									</a>
								</div>
								<div class="col-md-9">
								<?php echo "<div class='f-row'><font color='red'>Replacement Order</font></div>"; ?>
									<div class="product-name f-row">
													<a href="<?php echo base_url()?>detail/<?php echo $controller->sample_code().$repl_obj->inventory_id;?>"><?php echo ($repl_obj->ord_sku_name!='') ? $repl_obj->ord_sku_name: $repl_obj->product_name;?></a>			
											</div>
													
				<div class="f-row small-text">
										
						<?php
					//$repl_obj->attribute_1_value= substr($repl_obj->attribute_1_value, 0, strpos($repl_obj->attribute_1_value, ":"));
					
					if(!empty($repl_obj->attribute_1))
						echo '<div class="f-row">'.$repl_obj->attribute_1." : ".$repl_obj->attribute_1_value.'</div>';
					if(!empty($repl_obj->attribute_2))
						echo '<div class="f-row">'.$repl_obj->attribute_2." : ".$repl_obj->attribute_2_value.'</div>';
					if(!empty($repl_obj->attribute_3))
						echo '<div class="f-row">'.$repl_obj->attribute_3." : ".$repl_obj->attribute_3_value.'</div>';
					if(!empty($repl_obj->attribute_4))
						echo '<div class="f-row">'.$repl_obj->attribute_4." : ".$repl_obj->attribute_4_value.'</div>';
					?>
				
				</div>
				<div class="f-row small-text">
					Number of Qty : <?php echo $repl_obj->quantity; ?>
				</div>
				
												</div>
											</div>
											
											<?php $order_details_order_status=$controller->get_order_details_order_status($repl_obj->order_item_id,$order_id); 
											if(!empty($order_details_order_status)){
											?>	
											<div class="row margin-top margin-bottom">
											<div class="col-md-6">
											<?php echo $order_details_order_status;?>
											</div>
											</div>
											<?php
											} ?>
											
											
										</div>
										
										<div class="col-md-4 products-block detailed_order_item">
											<div class="stepwizard">
												<div class="stepwizard-row">
												<?php echo $controller->get_order_details_in_steps_status($repl_obj->order_item_id)?>
												</div>
											</div>
										</div>
										<div class="f-row col-sm-12 cont" id="cont_<?php echo $repl_obj->order_item_id; ?>">
										<button class = "x" content_id="order_status_<?php echo $repl_obj->order_item_id?>" onclick="hide_order_status(this,<?php echo $repl_obj->order_item_id;?>)">
											<i class="fa fa-times" aria-hidden="true"></i>
										</button>
										<div id="order_status_<?php echo $repl_obj->order_item_id;?>">
										</div>
										
										</div>
										<div class="col-md-2 text-right">
										
											<?php echo $controller->get_order_details_order_status_achieved($repl_obj->order_item_id) ?>
									
											<!--<div class="f-row"><strong><?php //echo $repl_obj->delivery_mode; ?> Delivery</strong></div>-->
											
										</div>
<!-----promo_popover------------------>

				
				<div class="col-md-2 text-right">
				<!----for replaced item	price display---------->
<?php 

		//echo 	$repl_obj->order_item_id.'';
		
			$get_replacement_data=$controller->get_order_details_admin_acceptance_refund_data_for_replacement($repl_obj->prev_order_item_id);
			$total_pro_price=($get_replacement_data['quantity']*$get_replacement_data['replacement_value_each_inventory_id']);
			$total_price_repl=($total_pro_price+$get_replacement_data['shipping_charge_replacement']);
			?>


                    <?php echo curr_sym;?><?php echo $total_price_repl; ?>
			 <a data-trigger="click" data-placement="bottom" data-toggle="popover" data-html="true" id="price_details_replace_<?php echo $repl_obj->order_item_id;?>" class="price_details_replace cursor-pointer" content_id="#content_price_details_replace_<?php echo $repl_obj->order_item_id; ?>" onclick="toggle_content(this)">[<i class="fa fa-question" aria-hidden="true"></i>]</a> 
												  
							<div id="content_price_details_replace_<?php echo $repl_obj->order_item_id; ?>" style="display:none;">
							<table class="table table-bordered" style="font-size: .8em;text-align:justify;margin-bottom: 0px;">
																<thead><tr><th style="padding:0px"></th><th width="25%" style="padding:0px"></th><th style="padding:0px" width="25%" style="text-align: right"></th></tr></thead>
																
																<tr><td>Base product price</td><td><?php echo $get_replacement_data['quantity']; ?> x <?php echo $get_replacement_data['replacement_value_each_inventory_id']; ?></td><td class="text-right"><?php echo curr_sym;?><?php echo $total_pro_price; ?></td></tr>
																
																<tr><td colspan="2">Shipping Charge for <?php echo $get_replacement_data['quantity']; ?> unit(s)</td><td class="text-right"><?php echo curr_sym;?><?php echo $get_replacement_data['shipping_charge_replacement']; ?></td></tr>
																
																<tr><td colspan="2">Total value </td><td class="text-right"><?php echo curr_sym;?><?php echo $total_price_repl;; ?></td></tr>
																</table>
															</div>
				</div>
	
	</div>
	<?php 
	$repl_order_item_id=$repl_obj->order_item_id;
	?>
	<script>
	$(document).ready(function(){
	var repl_order_item_id='<?php echo $repl_order_item_id;?>';
	
	$("#price_details_replace_"+repl_order_item_id).popover({
		html: true, 
		content: function() {
				if($(window).width() > 480){
					$('#content_price_details_replace_'+repl_order_item_id).hide();
					return $('#content_price_details_replace_'+repl_order_item_id).html();	
				}
			}
	});

	$("#order_placed_"+repl_order_item_id).popover({
		html: true, 
		content: function() {	
				if($(window).width() > 480){
					$('#cont_'+repl_order_item_id).hide();	
					$('#order_status_'+repl_order_item_id).html('');
					return $('#content_order_placed_'+repl_order_item_id).html();
				}
			}
	});
	$("#order_confirmed_"+repl_order_item_id).popover({
		html: true, 
		content: function() {  
				if($(window).width() > 480){
					$('#cont_'+repl_order_item_id).hide();	
					$('#order_status_'+repl_order_item_id).html('');
					return $('#content_order_confirmed_'+repl_order_item_id).html();
				}
			}
	});
	$("#order_packed_"+repl_order_item_id).popover({
		html: true, 
		content: function() {
				if($(window).width() > 480){
					$('#cont_'+repl_order_item_id).hide();	
					$('#order_status_'+repl_order_item_id).html('');
					return $('#content_order_packed_'+repl_order_item_id).html();
				}
			}
	});
	$("#order_shipped_"+repl_order_item_id).popover({
		html: true, 
		content: function() {
				if($(window).width() > 480){
					$('#cont_'+repl_order_item_id).hide();	
					$('#order_status_'+repl_order_item_id).html('');
					return $('#content_order_shipped_'+repl_order_item_id).html();
				}
			}
	});
	$("#order_delivered_"+repl_order_item_id).popover({
		html: true, 
		content: function() {
				if($(window).width() > 480){
					$('#cont_'+repl_order_item_id).hide();	
					$('#order_status_'+repl_order_item_id).html('');
					return $('#content_order_delivered_'+repl_order_item_id).html();
				}
			}
	});
});
	</script>