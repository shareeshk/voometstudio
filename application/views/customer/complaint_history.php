<style>
.or-color:hover{
	color:#999;
}
</style>
<div class="columns-container">
    <div class="container" id="columns">
        <ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account">My account</a></li>
		  <li class="breadcrumb-item active">My Complaints history</li>
		</ol>
        <div class="row">
            <!-- Left colunm -->
			<?php require_once 'leftcolum.php';?>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
				
				
				<div class="panel panel-default">
					<div class="panel-heading">
					<strong>Support Center</strong>
					</div>
					<div class="panel-body">
						<div class="f-row product-price">
                	<p>Dear Customer, This section has been designed to ensure that your grievances is (a) documented in one place (b) tracked for resolution by stages and responses (c) Retrieved / Referred to as the need arise and (d) Escalated to the right authority if not resolved within a given timeline.</p>
<br>
<p>It is simple to use. Just begin with creating a case and choose a method for us to contact you. Either a phone call back to you or via communication in this section.</p>
<br>
<p>We will track everything for resolution.</p>	
					
						
						<span class="pull-right"><a href="<?php echo base_url()?>Account/complaint_create"><button class="button preventDflt">Create Case</button></a></span></span>
						</div>
					</div>
				</div>
				
               
                <div class="well col-sm-12 ">
                <?php if(!empty($all_master_complaint_data)){ ?>
                	<span id="now_showing" class="lead">Recent Cases</span> &nbsp;<span class="cursor_pointer" id="all_cases_controller" onclick="showAllCases()">see all cases</span><span class="cursor_pointer" id="recent_cases_controller" onclick="showRecentCases()" style="display:none">recent cases</span>

                	<div id="recent_cases_table_container" class="table-responsive">	
					<table id="recent_cases_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>Created</th>
			                <th>Subject</th>
			                <th>Case ID</th>
			                <th>Type</th>
			                <th>Severity</th>
			                <th>Status</th>
			            </tr>
			        </thead>
			        <tbody>
			        	<?php 
			        	foreach($all_master_complaint_data as $data){ ?>
							
						
			            <tr>
			                <td><?php echo date_format(date_create($data['timestamp']), "D, jS M'y")?></td>
			                <td><a class="or-color" href="<?php echo base_url()?>Account/complaint_thread/<?php echo $data['complaint_thread_id'] ?>"><?php echo $data['subjects'] ?></a></td>
			                <td><a class="or-color" href="<?php echo base_url()?>Account/complaint_thread/<?php echo $data['complaint_thread_id'] ?>"><?php echo $data['id'] ?></a></td>
			                <td><?php echo ucfirst($data['regarding']) ?></td>
			                <td><?php echo $data['severity'] ?></td>
			                <td><?php echo $controller->get_complaint_case_present_status($data['close_case']); ?></td>
			            </tr>
			            
			            <?php } ?>
			        </tbody>
			    </table>
			    </div>
			    
<script type="text/javascript">

$(document).ready(function (){
   // Array holding selected row IDs
   var rows_selected = [];
   
   var table = $('#all_cases_table').DataTable({
	   
	    "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo base_url();?>Account/all_complaint_processing", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#all_cases_table_processing").css("display","none");
            }
          },
		  
      //'ajax': 'https://api.myjson.com/bins/1us28',
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'5%',
         'className': 'dt-body-center'
      }],
      'order': [0, 'desc']
   });
});	
</script> 			    
			    	
			    <div id="all_cases_table_container" style="display:none">
			    <table id="all_cases_table" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>Created</th>
			                <th>Subject</th>
			                <th>Case ID</th>
			                <th>Type</th>
			                <th>Severity</th>
			                <th>Status</th>
			            </tr>
			        </thead>
			        </table>
				</div>	
					
			<?php }
				else{
					echo "<h3>No Cases are added for you.</h3>";
				}
			?>
			
					
                </div>
                
                
                
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<script>
	function showRecentCases(){
		document.getElementById('now_showing').innerHTML="Recent Cases";
		document.getElementById('recent_cases_table_container').style.display=""
		document.getElementById('all_cases_table_container').style.display="none"
		document.getElementById('recent_cases_controller').style.display="none"
		document.getElementById('all_cases_controller').style.display=""
	}
	function showAllCases(){
		document.getElementById('now_showing').innerHTML="All Cases";
		document.getElementById('recent_cases_table_container').style.display="none"
		document.getElementById('all_cases_table_container').style.display=""
		document.getElementById('recent_cases_controller').style.display=""
		document.getElementById('all_cases_controller').style.display="none"
	}
</script>