<style>
.table>tbody>tr>td{
	line-height: 22px;
}
</style>
<div class="columns-container">
    <div class="container" id="columns">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account">My account</a></li>
		  <li class="breadcrumb-item active">Projects</li>
		</ol>
        <div class="row">
			<?php require_once 'leftcolum.php';?>
            
			<div class="center_column col-xs-12 col-sm-9" id="center_column">
			
			<div class="well">
			
			<div class="panel panel-default">
    <div class="panel-heading">Projects</div>
    <div class="panel-body">
		
	<div class="col-md-12">
		<a href="<?php echo base_url().'Account/create_project' ?>" class="btn btn-sm btn-info preventDflt pull-right">Create New Project</a>
		</div>
		<br>
		<br>
		
	<?php if(!empty($project_detail)){ ?>


		
		<div class="col-md-12">

		<table id="projects_details" class="table table-striped table-bordered table-responsive ">
			        <thead>
			            <tr>
			                <th>S.No</th>
			                <th>Action</th>
							<th>Project </th>
							<th>Client Details</th>
			                <th>Requirements</th>
			                <th>Project Details</th>
			                <th>Last updated on</th>
			                
			            </tr>
			        </thead>
			        <tbody>
			        	
			        	<?php $i=1; foreach ($project_detail as $pro){ ?>
			        	<tr>
			                <td><?php echo $i ?></td>
							<td><button class="btn btn-sm btn-warning preventDflt" onclick="update_project_details(<?php echo $pro['project_id']?>)">Update</button> &nbsp;
							<!-- <button class="btn btn-sm btn-danger preventDflt">Delete</button> -->
							</td>
							<td><?php echo $pro['project_name'].'<br><small><b>Type : </b></small>'.$pro['project_type']; ?><br><small><b>Location : </b></small><?php echo $pro['project_location']?></td>

							<td><?php echo '<small><b>Name : </b></small>'.$pro['client_name'].'<br><small><b>Email : </b></small>'.$pro['client_email'].'<br><small><b>Mobile : </b></small>'.$pro['client_mobile']?></td>
							
			                <td>
							<span style="cursor:pointer;" data-toggle="modal" data-target="#Modal_<?php echo $pro['project_id']; ?>">
							<u class="text-info">View all requirements</u></span>

							<?php //echo $pro['project_file'];
							//echo base_url().$pro['project_file'];
							?>
							 <?php if($pro['project_file']!=''){

								if(file_exists($pro['project_file'])){
								?>
							<br>
							<u class="text-info"><a href="<?php echo base_url().$pro['project_file'] ?>" target="_blank"><i class="fa fa-paperclip" aria-hidden="true"></i> Attachment</a></u>

								<?php 
							 	}
							}
							 ?>
							
							
	
							
							<!-- Modal -->

<div class="modal fade" id="Modal_<?php echo $pro['project_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel_<?php echo $pro['project_id']; ?>"><?php echo $pro['project_name']; ?> - Requirements</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <?php echo $pro['project_requirements'];?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
							</td>
			                <td>
								<?php 
								
								echo '<small><b>Deadline : </b></small>'.date('d-m-Y',strtotime($pro['project_deadline']));
								echo '<br><small><b>Budget : </b></small>'.$pro['project_tentative_budget'];
								echo '<br><small><b>Status : </b></small><br><b>'.ucfirst($pro['project_status']).'</b>';
								
								?>
						
							</td>
			                
			                <td><?php echo date('d-m-Y H:i:s',strtotime($pro['project_last_updated_on']));?></td>
			                
			            </tr>
			            
			        	<?php
						


						
						$i++; }
						
						?>
			        </tbody>
		</table>

		</div>

						
			    <?php }
else{
	echo "<b>No Project details To Be Shown</b>";
}
			    ?>
				
			</div><!---body--->
  </div>
            	
            	</div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<script>
function update_project_details(project_id){
	location.href="<?php echo base_url()?>Account/update_project_details/"+project_id;
}
</script>