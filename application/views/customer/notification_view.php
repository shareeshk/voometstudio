<style type="text/css">
.inactiveLink {
   pointer-events: none;
   cursor: default;
}
.media-body p{
	margin-bottom: 10px;
}
</style>
<div class="columns-container">
    <div class="container" id="columns">
	<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account">My account</a></li>
		  <li class="breadcrumb-item active">Notifications</li>
		</ol>
        <div class="row">
			<?php require_once 'leftcolum.php';?>
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
				<div class="panel panel-default">
					<div class="panel-heading"><strong>Notifications</strong></div>
					
					<div class="common">
					
				<?php 
				
				$order_item_notifi_obj=call_notification_function();

				
				$notification_count=count($order_item_notifi_obj);
				
				if($notification_count==0){
					?>
					
					<p class="lead text-center">No Information available yet</p>
					
					<?php 
				}
				?>
				
				<?php
		if($this->session->userdata("customer_id")){

			?>
					
						<?php 
						//(approved_status_view='1' or confirmed_status_view='1' or packed_status_view='1' or shipped_status_view='1' or delivered_status_view='1' or cancelled_status_view='1')
					$i=0;
						$str='';		
						
						$order_item_notifi_obj=call_notification_function();
						/*
						echo '<pre>';
						print_r($order_item_notifi_obj);
						echo '</pre>';
						*/
						$notification_count_arr=unviewed_notification();
						$admin_msg_detail=$notification_count_arr["returns_msg_notification"];
						$admin_msg_detail_repl=$notification_count_arr["repl_msg_notification"];
						$admin_msg_detail_repl_stock=$notification_count_arr["repl_msg_notification_stock"];
						$returns_notification=$notification_count_arr["returns_notification"];
						$repl_notification=$notification_count_arr["replacement_notification"];
						$repl_stock_notification=$notification_count_arr["repl_stock_notification"];
				
						if(!empty($admin_msg_detail)){
							$i++;
							
							$str.='<div class="panel-body border-down" data-position="1">';
							$str.='<div class="media"><a href="'.base_url().'Account/order_details/'.$admin_msg_detail->order_id.'" >';
								$str.='<div class="media-left"><img class="media-object" src="'.base_url().$admin_msg_detail->thumbnail.'" style="height:15vh"></div>';
								$str.='<div class="media-body">';
								$str.='<p>There is a message from seller for your refund request. Order ID is <b>'.$admin_msg_detail->order_id.'</b></p>';
								$str.='<h4 class="media-heading text-right">';
								$admin_msg_detail->admin_msg_timestamp='';
								$str.='<small><i>Posted on <i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($admin_msg_detail->admin_msg_timestamp)).'</i></small></h4>';
								$str.='</div></a></div>';	
							
							$str='</div>';
						}
						if(!empty($admin_msg_detail_repl)){
							$i++;
							$str.='<div class="panel-body border-down" data-position="2">';
							$str.='<div class="media"><a href="'.base_url().'Account/order_details/'.$admin_msg_detail_repl->order_id.'" >';
								$str.='<div class="media-left"><img class="media-object" src="'.base_url().$admin_msg_detail_repl->thumbnail.'" style="height:15vh"></div>';
								$str.='<div class="media-body">';
								$str.='<p>There is a message from seller for your replacement request. Order ID is <b>'.$admin_msg_detail_repl->order_id.'</b></p>';
								$str.='<h4 class="media-heading text-right">';
								$admin_msg_detail_repl->admin_msg_timestamp='';
								$str.='<small><i>Posted on <i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($admin_msg_detail_repl->admin_msg_timestamp)).'</i></small></h4>';
								$str.='</div></a></div>';
							
							$str.='</div>';
						}
						if(!empty($admin_msg_detail_repl_stock)){
								$str.='<div class="panel-body border-down" data-position="3">';
							$str.='<div class="media"><a href="'.base_url().'Account/order_details/'.$admin_msg_detail_repl_stock->order_id.'" >';
								$str.='<div class="media-left"><img class="media-object" src="'.base_url().$admin_msg_detail_repl_stock->thumbnail.'" style="height:15vh"></div>';
								$str.='<div class="media-body">';
								$str.='<p>There is a message from seller for your replacement request.. Order ID is <b>'.$admin_msg_detail_repl_stock->order_id.'</b></p>';
								$str.='<h4 class="media-heading text-right">';
								$admin_msg_detail_repl_stock->admin_msg_timestamp='';
								$str.='<small><i>Posted on <i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($admin_msg_detail_repl_stock->admin_msg_timestamp)).'</i></small></h4>';
								$str.='</div></a></div>';
							
							$str.='</div>';
								
						}
						
						foreach ($returns_notification as $rn){
							$i++;
							$str.='<div class="panel-body border-down" data-position="4">';
							if(($rn->refund_success_view==1 || $rn->refund_success_view==0) && $rn->refund_success_view!=''){
								$str.='<div class="media"><a href="'.base_url().'Account/order_details/'.$rn->order_id.'" >';
								$str.='<div class="media-left"><img class="media-object" src="'.base_url().$rn->thumbnail.'" style="height:15vh"></div>';
								$str.='<div class="media-body">';
								$str.='<p>Refund is done successfully. Order ID is <b>'.$rn->order_id.'</b></p>';
								$str.='<h4 class="media-heading text-right">';
								$str.='<small><i>Posted on <i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($rn->refund_success_timestamp)).'</i></small></h4>';
								$str.='</div></a></div>';
								
							}elseif(($rn->admin_reject_status_view==1 || $rn->admin_reject_status_view==0) && $rn->admin_reject_status_view!=''){
								$str.='<div class="media"><a href="'.base_url().'Account/order_details/'.$rn->order_id.'" >';
								$str.='<div class="media-left"><img class="media-object" src="'.base_url().$rn->thumbnail.'" style="height:15vh"></div>';
								$str.='<div class="media-body">';
								$str.='<p>Your request for refund is rejected. Order ID is <b>'.$rn->order_id.'</b></p>';
								$str.='<h4 class="media-heading text-right">';
								$str.='<small><i>Posted on <i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($rn->admin_reject_timestamp)).'</i></small></h4>';
								$str.='</div></a></div>';
								
							}elseif($rn->refund_label_status_view==1 && $rn->refund_label_status_view!=''){
								$str.='<div class="media"><a href="'.base_url().'Account/order_details/'.$rn->order_id.'" >';
								$str.='<div class="media-left"><img class="media-object" src="'.base_url().$rn->thumbnail.'" style="height:15vh"></div>';
								$str.='<div class="media-body">';
								$str.='<p>Refund label is sent. Order ID is <b>'.$rn->order_id.'</b></p>';
								$str.='<h4 class="media-heading text-right">';
								$str.='<small><i>Posted on <i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($rn->refund_label_timestamp)).'</i></small></h4>';
								$str.='</div></a></div>';
								
							}elseif(($rn->admin_accept_status_view==1 || $rn->admin_accept_status_view==0) && $rn->admin_accept_status_view!=''){
								
								$str.='<div class="media"><a href="'.base_url().'Account/order_details/'.$rn->order_id.'" >';
								$str.='<div class="media-left"><img class="media-object" src="'.base_url().$rn->thumbnail.'" style="height:15vh"></div>';
								$str.='<div class="media-body">';
								$str.='<p>Your refund request is accepted. Order ID is <b>'.$rn->order_id.'</b></p>';
								$str.='<h4 class="media-heading text-right">';
								$str.='<small><i>Posted on <i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($rn->admin_accept_timestamp)).'</i></small></h4>';
								$str.='</div></a></div>';
								
							}
							$str.='</div>';
						}
						
						foreach ($repl_notification as $repl){
							$i++;
							$str.='<div class="panel-body border-down" data-position="5">';
							if(($repl->admin_reject_status_view==1 || $repl->admin_reject_status_view==0) && $repl->admin_reject_status_view!=''){
								$str.='<div class="media"><a href="'.base_url().'Account/order_details/'.$repl->order_id.'" >';
								$str.='<div class="media-left"><img class="media-object" src="'.base_url().$repl->thumbnail.'" style="height:15vh"></div>';
								$str.='<div class="media-body">';
								$str.='<p>Your request for replacement is rejected. Order ID is <b>'.$repl->order_id.'</b></p>';
								$str.='<h4 class="media-heading text-right">';
								$str.='<small><i>Posted on <i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($repl->admin_reject_timestamp)).'</i></small></h4>';
								$str.='</div></a></div>';
							}elseif($repl->repl_label_status_view==1 && $repl->repl_label_status_view!=''){
								$str.='<div class="media"><a href="'.base_url().'Account/order_details/'.$repl->order_id.'" >';
								$str.='<div class="media-left"><img class="media-object" src="'.base_url().$repl->thumbnail.'" style="height:15vh"></div>';
								$str.='<div class="media-body">';
								$str.='<p>Replacement label is sent . Order ID is <b>'.$repl->order_id.'</b></p>';
								$str.='<h4 class="media-heading text-right">';
								$str.='<small><i>Posted on <i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($repl->repl_label_timestamp)).'</i></small></h4>';
								$str.='</div></a></div>';
								
							}elseif(($repl->admin_accept_status_view==1 || $repl->admin_accept_status_view==0) && $repl->admin_accept_status_view!=''){
								$str.='<div class="media"><a href="'.base_url().'Account/order_details/'.$repl->order_id.'" >';
								$str.='<div class="media-left"><img class="media-object" src="'.base_url().$repl->thumbnail.'" style="height:15vh"></div>';
								$str.='<div class="media-body">';
								$str.='<p>Your replacement request is accepted. Order ID is <b>'.$repl->order_id.'</b></p>';
								$str.='<h4 class="media-heading text-right">';
								$str.='<small><i>Posted on <i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($repl->admin_accept_timestamp)).'</i></small></h4>';
								$str.='</div></a></div>';
								
							}
							
							$str.='</div>';
						}
						$not_str='';
						foreach ($order_item_notifi_obj as $order_item_notifi){
							$i++;
							$pro_name=($order_item_notifi->ord_sku_name!='') ? $order_item_notifi->ord_sku_name:$order_item_notifi->product_name;
							
							if(($order_item_notifi->cancelled_status_view==1 || $order_item_notifi->cancelled_status_view==0) && $order_item_notifi->cancelled_status_view!=''){
								$not_str.='<div class="media"><a href="'.base_url().'Account/order_details/'.$order_item_notifi->order_id.'" >';
								$not_str.='<div class="media-left"><img class="media-object" src="'.base_url().$order_item_notifi->thumbnail.'" style="height:15vh"></div>';
								$not_str.='<div class="media-body">';
								$not_str.='<p>'.$pro_name.' you ordered is Cancelled. Order ID is <b>'.$order_item_notifi->order_id.'</b></p>';
								$not_str.='<h4 class="media-heading text-right">';
								if($order_item_notifi->prev_order_item_id!=''){
									$not_str.='<span style="color:red;">Replaced Order</span>';
								}
								$not_str.='<small><i>Posted on <i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_cancelled_timestamp)).'</i></small></h4>';
								$not_str.='</div></a></div>';	
								
							}elseif(($order_item_notifi->delivered_status_view==1 || $order_item_notifi->delivered_status_view==0) && $order_item_notifi->delivered_status_view!=''){
								$not_str.='<div class="media"><a href="'.base_url().'Account/order_details/'.$order_item_notifi->order_id.'" >';
								$not_str.='<div class="media-left"><img class="media-object" src="'.base_url().$order_item_notifi->thumbnail.'" style="height:15vh"></div>';
								$not_str.='<div class="media-body">';
								$not_str.='<p>'.$pro_name.' you ordered is delivered. Order ID is <b>'.$order_item_notifi->order_id.'</b></p>';
								$not_str.='<h4 class="media-heading text-right">';
								if($order_item_notifi->prev_order_item_id!=''){
									$not_str.='<span style="color:red;">Replaced Order</span>';
								}
								$not_str.='<small><i>Posted on <i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_delivered_timestamp)).'</i></small></h4>';
								$not_str.='</div></a></div>';
								
							}elseif(($order_item_notifi->shipped_status_view==1 || $order_item_notifi->shipped_status_view==0) && $order_item_notifi->shipped_status_view!=''){
								$not_str.='<div class="media"><a href="'.base_url().'Account/order_details/'.$order_item_notifi->order_id.'" >';
								$not_str.='<div class="media-left"><img class="media-object" src="'.base_url().$order_item_notifi->thumbnail.'" style="height:15vh"></div>';
								$not_str.='<div class="media-body">';
								$not_str.='<p>'.$pro_name.' you ordered is shipped. Order ID is <b>'.$order_item_notifi->order_id.'</b></p>';
								$not_str.='<h4 class="media-heading text-right">';
								if($order_item_notifi->prev_order_item_id!=''){
									$not_str.='<span style="color:red;">Replaced Order</span>';
								}
								$not_str.='<small><i>Posted on <i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_shipped_timestamp)).'</i></small></h4>';
								$not_str.='</div></a></div>';
								
								
							}elseif(($order_item_notifi->packed_status_view==1 || $order_item_notifi->packed_status_view==0 ) && $order_item_notifi->packed_status_view!='') {
								
								$not_str.='<div class="media"><a href="'.base_url().'Account/order_details/'.$order_item_notifi->order_id.'" >';
								$not_str.='<div class="media-left"><img class="media-object" src="'.base_url().$order_item_notifi->thumbnail.'" style="height:15vh"></div>';
								$not_str.='<div class="media-body">';
								$not_str.='<p>'.$pro_name.' you ordered is packed. Order ID is <b>'.$order_item_notifi->order_id.'</b></p>';
								$not_str.='<h4 class="media-heading text-right">';
								if($order_item_notifi->prev_order_item_id!=''){
									$not_str.='<span style="color:red;">Replaced Order</span>';
								}
								$not_str.='<small><i>Posted on <i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_packed_timestamp)).'</i></small></h4>';
								$not_str.='</div></a></div>';
								
							
							}elseif(($order_item_notifi->confirmed_status_view==1 || $order_item_notifi->confirmed_status_view==0 )&& $order_item_notifi->confirmed_status_view!=''){
								
								$not_str.='<div class="media"><a href="'.base_url().'Account/order_details/'.$order_item_notifi->order_id.'" >';
								$not_str.='<div class="media-left"><img class="media-object" src="'.base_url().$order_item_notifi->thumbnail.'" style="height:15vh"></div>';
								$not_str.='<div class="media-body">';
								$not_str.='<p>'.$pro_name.' you ordered is Confirmed. Order ID is <b>'.$order_item_notifi->order_id.'</b></p>';
								$not_str.='<h4 class="media-heading text-right">';
								if($order_item_notifi->prev_order_item_id!=''){
									$not_str.='<span style="color:red;">Replaced Order</span>';
								}
								$not_str.='<small><i>Posted on <i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_confirmed_timestamp)).'</i></small></h4>';
								$not_str.='</div></a></div>';
											
							}elseif(($order_item_notifi->approved_status_view==1 || $order_item_notifi->approved_status_view==0) && $order_item_notifi->approved_status_view!=''){
								$not_str.='<div class="media"><a href="'.base_url().'Account/order_details/'.$order_item_notifi->order_id.'" >';
								$not_str.='<div class="media-left"><img class="media-object" src="'.base_url().$order_item_notifi->thumbnail.'" style="height:15vh"></div>';
								$not_str.='<div class="media-body">';
								$not_str.='<p>'.$pro_name.' you ordered is approved. Order ID is <b>'.$order_item_notifi->order_id.'</b></p>';
								$not_str.='<h4 class="media-heading text-right">';
								if($order_item_notifi->prev_order_item_id!=''){
									$not_str.='<span style="color:red;">Replaced Order</span>';
								}
								$not_str.='<small><i>Posted on <i class="fa fa-clock-o" aria-hidden="true"></i> '.date("D j M, Y H:i",strtotime($order_item_notifi->order_placed_timestamp)).'</i></small></h4>';
								$not_str.='</div></a></div>';
								
							}


							if($not_str!=''){
								$str.='<div class="panel-body border-down" data-position="6">';
								$str.=$not_str;
								$str.='</div>';
							}
							
						}
						echo $str;
						//print_r($datail);
				
		}

		?>		
					</div><!--common---->
				</div>
				
				<?php
					
					if($i>5){
						$style='';
					}else{
						$style='style="display:none;"';
					}

				?>
				
				
				<div class="f-row margin-top" id="pagination_div" <?php echo $style; ?>>
					<div class="sortPagiBar">
					<div class="bottom-pagination">
						<span class="nav_pag"></span>
					</div>
					</div>
				</div>
				
            	
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<script>
$(document).ready(function(){
	clear_notifications();
});

function clear_notifications(){

	$.ajax({
		url:'<?php echo base_url(); ?>Account/clear_notifications',
		type:"POST",
		data:'1=2',
		success:function(data){
			//alert(data);
			//return false;
			//location.href="<?php echo base_url();?>Account/order_details/"+order_item_id;	
		}	
	});

}

function update_bank_detailsFun(){
	$.ajax({
		url:"<?php echo base_url()?>Account/update_bank_details_action",
		type:"POST",
		data:$("#update_bank_details").serialize(),
		success:function(data){
			//alert(data);
			if(data){

			    alert('updated successfully');
                location.href="<?php echo base_url()?>Account/bank";

				/*bootbox.alert({
					  size: "small",
					  message: 'updated successfully',
					  callback: function () { location.href="<?php echo base_url()?>Account/bank"; }
					  
					});
				*/
			}else{
			    alert('not yet updated');
                location.reload();
                /*
				bootbox.alert({ 
					  size: "small",
					  message: 'not yet updated',
					  callback: function () { location.reload(); }
					});*/
			}
		}
	});
}

	jQuery(document).ready(function($){	
		$('.common').paginathing({
	    perPage:5,
	    insertAfter: '.nav_pag'
		});
	});
	
$( "#notificationLink" ).addClass("inactiveLink");

</script>