<style type="text/css">
    .cart_summary .qty input {
        text-align: center;
        max-width: 50px;
        margin: 0 auto;
        border-radius: 0px;
        border: 1px solid #eaeaea;
    }

    .cart_summary .qty a {
        padding: 3px 5px 5px 5px;
        border: 1px solid #eaeaea;
        display: inline-block;
        width: auto;
        margin-top: 0px;
    }

    .borderless td, .borderless th {
        border: none;
    }

    .overflow {
        overflow: auto;
    }

    .left-align-cashback {
        width: 86%;
        float: left;
        text-align: right;
    }

    .left-align-cashback-total {
        width: 85%;
        float: left;
        text-align: right;
        font-size: .9em;
    }

    .cart_summary td {
        vertical-align: middle !important;
        padding: 10px;
    }

    .cart_summary .table > tbody > tr > td, .table > tbody > tr > th, .cart_summary .table > tfoot > tr > td, .table > tfoot > tr > th, .cart_summary .table > thead > tr > td {
        padding: 5px;
    }

    .table > thead > tr > th {
        padding: 6px;
    }

    @media (max-width: 480px) {
        .page-order .cart_navigation a {
            margin-top: 4%;
            margin-left: 20%;
            margin-bottom: 4%;
        }

        .page-order .cart_navigation a.next-btn {
            float: left;
        }
        .page-order .cart_navigation button.next-btn{
            width: 100%;
            margin: 10px 0px 10px 0px;
        }
        .page-order .cart_navigation button.prev-btn{
            width: 100%;
        }
        .shipping_note{
            line-height:25px;
        }
        #available_pincode_div
        {
            margin:10px 0px 10px 0px;
            float: right;
        }
        .my-cart {
            margin: 0.2em 0px 10px 0px;
        }
    }

    .pincode-search {
        max-width: 50%;
    }

    .help-block {
        color: #ff6161;
    }

    .glyphicon {
        line-height: 1.4;
    }

    .input-group .form-control {
        z-index: 0;
    }

    .input-group-sm > .form-control, .input-group-sm > .input-group-addon, .input-group-sm > .input-group-btn > .btn {
        font-size: 1.1rem;
    }

    .well {
        border: none !important;
        background-color: #fff;
        -webkit-box-shadow: none;
        transition: all 0.3s ease-in-out 0s;
    }

    .my-cart {
        margin: 0.2em 0px 10px 0px;
    }

    th {
        font-size: 0.8em;
        font-weight: 600;
    }

    .swal2-icon.swal2-info {
        color: #ff0000 !important;
        border-color: #ff0000 !important;
    }
    .shipping_note{
     /*line-height:25px;*/
    }
    .my_cart_note{
        margin: 0px 10px 10px 10px;
    }
</style>

<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">

        
       
       

        <div class="page-content page-order well pl-0 pr-0 pt-10">

            

            <div class="popular-tabs mt-0">
                

                <div class="row text-center pl-10 pr-10">
                    <div class="col-md-4 col-md-offset-4">
                        <img src="<?php echo base_url(); ?>assets/data/notfound.jpg" class="img-responsive">
                    </div>
                    <div class="col-md-4 col-md-offset-4">
                        <h3 class="text-center">The requested URL or product is not found in this site. Please cross check!</h3>
                    </div>
                </div>
                <!---- added for pincode search---->
                
            </div><!---tab---->


        </div><!--page container----------->

    </div>
</div>


<script>
   
    $(document).ready(function () {

        function toggleIcon(e) {
            $(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('glyphicon-plus glyphicon-minus');
        }

        $('.panel-group').on('hidden.bs.collapse', toggleIcon);
        $('.panel-group').on('shown.bs.collapse', toggleIcon);
    });
</script>
<!-- ./page wapper-->
