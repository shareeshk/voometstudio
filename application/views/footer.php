<?php
	$email_stuff_arr=email_stuff();
?>
<!-- Footer -->
<!-- Footer -->
<footer id="footer2" class="footer-style8 parallax">
     <div class="footer-top " style="display: none;">
         <div class="container">
             <div class="row">
                 
                
             </div>
         </div>
     </div>

     <!-- footer paralax-->
     <div class="footer-paralax" >
         <div class="footer-row footer-center" id="footer_span">
             <div class="container">
                 <h4 class="text-capitalize">Signup </h4><br>
                              <form class="form-inline form-subscribe" onsubmit="return addNewsLetterSubscription(this)" action="<?php echo base_url()?>add_news_letter_subscription" method="post" name="newsLetterSubscription">
                      <div class="form-group">
                        <input type="email" class="form-control" name="email_address" id="email_address_nl" placeholder="Enter Your E-mail Address">
                        <input type="checkbox" name="contact_me" value="1"  id="contact_me" style="display:none !important" tabindex="-1" autocomplete="off">
                        <button type="submit" class="btn btn-default"><i class="fa fa-paper-plane-o"></i></button>
                      </div>
                    
                </form>
                <span class="help" id="subscibing_info"></span>
				
				
             </div>
         </div>
         <div class="footer-row">
             <div class="container">
                 <div class="row">
                     <div class="col-sm-4">
                         <div class="widget-container">
                             <h4 class="widget-title">Voomet Studio LLP </h4>
                             <div class="widget-body">
 <p>51 , Arehalli Guddadahalli, DITPL KIADB Apparel park,<br>Doddaballapura, Karnataka -561203</p>
                                 <ul>
    								
									 <li><a class="email_footer email" href="voometstudio@testmail.com">voometstudio@testmail.com</a></li>
									 <a href="https://api.whatsapp.com/send?phone=918050504950" class="float" target="_blank" style="z-index:999">
										<i class="fa fa-whatsapp my-float"></i>
									 </a>
                                 </ul>
								  <div class="">
                     <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
<!--<![endif]-->
<div style="padding-top: 8px;">
<span style="padding-right: 4px;">
<a  target="_blank" href="https://www.facebook.com/VoometStudio//"><img style="border: 0;" src="<?php echo base_url()?>assets/images/underconstruction/facebook.png" alt="Facebook" height="30" width="30" cm_dontimportimage=""></a>
</span>
<span style="padding-right: 4px;">
<a  target="_blank" href="https://twitter.com/VoometStudio"><img style="border: 0;" src="<?php echo base_url()?>assets/images/underconstruction/twitter.png" alt="Twitter" height="30" width="30" cm_dontimportimage=""></a>
</span>
<span style="padding-right: 4px;">
<a target="_blank" href="https://www.linkedin.com/company/74900160/admin/"><img style="border: 0;" src="<?php echo base_url()?>assets/images/underconstruction/linkedin.png" alt="Linkedin" height="30" width="30" cm_dontimportimage=""></a>
</span>
<span style="padding-right: 4px;">
<a target="_blank" href="https://www.instagram.com/VoometStudio/"><img style="border: 0;" src="<?php echo base_url()?>assets/images/underconstruction/instagram.png" alt="Linkedin" height="30" width="30" cm_dontimportimage=""></a>
</span>
<span style="padding-right: 4px;">
<a target="_blank" href="https://in.pinterest.com/VoometStudio/_created/"><img style="border: 0;" src="<?php echo base_url()?>assets/images/underconstruction/piniterest.png" alt="Linkedin" height="30" width="30" cm_dontimportimage=""></a>
</span>
</div>
<!--[if (!mso)&(!IE)]><!-->
</div>
                 </div>
                             </div>
                         </div>
                     </div>
                     <div class="col-sm-2">
                         <div class="widget-container">
                             <h5 class="widget-title">COMPANY</h5>
                             <div class="widget-body">
                                 <ul>
                                     <li><a href="<?php echo base_url();?>aboutus">About Voomet Studio</a></li>
                                    
                                 </ul>
                             </div>
                         </div>
                     </div>
                     <div class="col-sm-2">
                         <div class="widget-container">
                             <h5 class="widget-title">Seller</h5>
                             <div class="widget-body">
                                 <ul>
								 <li><a href="<?php echo base_url() ?>seller_registration">Seller Registration</a></li>
                                   <li><a href="<?php echo base_url();?>sellthroughus">Sell Through Us</a></li>   
                                 </ul>
                             </div>
                         </div>
                     </div>
					 <?php
							//if(!$this->session->userdata("customer_id")){
								if(1){
						?>
									 <div class="col-sm-2">
										 <div class="widget-container">
											 <h5 class="widget-title">Franchise</h5>
											 <div class="widget-body">
												 <ul>
													
													<li><a href="<?php echo base_url()?>becomeourfranchise">Become Our Franchise</a></li>
													<?php 
													if($this->session->userdata("user_type")!='franchise_customer'){
														?>
													<li><a href="<?php echo base_url()?>franchise_login">Franchise Login</a></li>
													<?php } ?>
													 
												 </ul>
											 </div>
										 </div>
									 </div>
					 
					 
					 <?php
							}
						?>
						 <div class="col-sm-2">
                         <div class="widget-container">
                             <h5 class="widget-title">Customer</h5>
                             <div class="widget-body">
                                 <ul>								 
                                     <li><a href="#" onclick="myPersonalInfoFun()">Profile</a></li>
                                 </ul>
                             </div>
                         </div>
                     </div>				
										
                     
                 </div>
				<!-- #trademark-box -->
				<div id="trademark-box">
						<ul id="trademark-list" class="row">
							<li id="payment-methods" class="col-md-4" style="color:#4dad24">Products</li>	
							
						</ul> 
				</div> <!-- /#trademark-box -->
				 
				<!-- #trademark-text-box -->
				<div id="trademark-text-box" class="row">
					<div class="col-sm-12">
			
					</div>
					
<!--newly enabled--->
		<?php	
		
$p_category_count=0;		
foreach ($p_category as $p_menu_value) {
	$p_category_count++;
	$rand_1=$controller->sample_code();
	
	$menu_name=$p_menu_value['parent_menu'];
	$menu_type=$p_menu_value['type'];
	
	if($menu_type=="parent_cat")
	{
		$pcat_id=$p_menu_value['id'];			
		$show_sub=$p_menu_value['show_sub_menu'];
		if($show_sub!=0){	
		
			
				$cat=$controller->provide_category($pcat_id);
				$len=count($cat);
				//$l=round(12/$len);
		
				foreach($cat as $cat_value) {

					$rand_2=$controller->sample_code();
					$cat_id=$cat_value->cat_id;
					$cat_name=$cat_value->cat_name;
					?>
					<div class="col-sm-12">	
						<ul id="trademark-tv-list" class="trademark-list">	
							  
								<?php /* echo base_url();?>search_category/<?php echo "{$rand_1}{$pcat_id}"; ?>/<?php echo "{$rand_2}{$cat_id}";*/ ?>
							  
								<li class="trademark-text-tit">
									<a href="#" style="color:#fff;"><?php echo $cat_name;?></a>
								</li>

					<?php
					$sub_cat=$controller->provide_sub_category($cat_id);
					foreach($sub_cat as $sub_cat_value) {
						
						$rand_3=$controller->sample_code();
					
						$subcat_id=$sub_cat_value["subcat_id"];
						$subcat_value=$sub_cat_value["subcat_name"];

						?>
						<li class="link_container"><a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pcat_id}"; ?>/<?php echo "{$rand_2}{$cat_id}"; ?>/<?php echo "{$rand_3}{$subcat_id}"; ?>"><?php echo $subcat_value;?></a></li>
						<?php
					}	
					?>
						</ul>
					</div>
					<?php
					
				}
		}
	}
	if($menu_type=="cat")
	{
		$rand_2=$controller->sample_code();
					
		$cat_id=$p_menu_value['id'];
		$cat_name=$p_menu_value['parent_menu'];
		
		if(isset($p_menu_value['pcat_id'])){
		 $pcat_id=$p_menu_value['pcat_id'];
		}else{
			$pcat_id=0;
		}	
		?>
			
				<div class="col-sm-12">	
						<ul id="trademark-tv-list" class="trademark-list">	
						
					<li class="trademark-text-tit"><a href="<?php echo base_url();?>category/cat/<?php echo "{$rand_2}{$cat_id}"; ?>" style="color:#fff;" ><?php echo $cat_name;?></a></li>
						
					<?php
					$sub_cat=$controller->provide_sub_category($cat_id);
					foreach($sub_cat as $sub_cat_value){
						$rand_3=$controller->sample_code();
						$subcat_id=$sub_cat_value["subcat_id"];
						$subcat_value=$sub_cat_value["subcat_name"];

						?>
						<li ><a href="<?php echo base_url();?>search_category/<?php echo "{$rand_1}{$pcat_id}"; ?>/<?php echo "{$rand_2}{$cat_id}"; ?>/<?php echo "{$rand_3}{$subcat_id}"; ?>"><?php echo $subcat_value;?></a></li>
						<?php
					}	
					?>
					</ul>
					</div>
					<?php
	}
}

					
					?>

					
				   
				</div><!-- /#trademark-text-box -->
					
             </div>
         </div>
		 
		  
		 
         <div class="footer-bottom">
             <div class="container">
                 <div class="footer-bottom-wapper">
                     <div class="row">
						<div class="col-sm-12">
							<ul class="footer-menu-list">
								<li><a href="<?php echo base_url();?>termsandconditions">Terms of Use</a></li>
								<li><a href="<?php echo base_url();?>policy">Privacy Policy</a></li>
								<li><a href="<?php echo base_url();?>business_policy" >Business Policies</a></li>
								<!-- <li><a href="<?php //echo base_url();?>payments" >Payments</a></li>
								<li><a href="<?php //echo base_url();?>delivery" >Delivery</a></li>
								<li><a href="<?php //echo base_url();?>warranty" >Warranty</a></li> -->
							</ul>

						</div>
						
                         <div class="col-sm-12 margin-top">
                             <div class="footer-coppyright" style="color:#fff;text-align:center;">
                              <small>  Copyrights &#169; 2024 Voomet Studio. This site runs on AI Powered ecommerce platform from <a href="http://www.axonlabs.ai" target="_blank" style="color:#ff3300">Axonlabs Consulting</a></small> 
                             </div>

                         </div>
                         
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <!-- ./footer paralax-->
</footer>

<a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>
<!-- Script-->
<a href="https://api.whatsapp.com/send?phone=917411722599" class="float" target="_blank">
<i class="fa fa-whatsapp my-float"></i>
</a>



<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script><!--updated--->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.plugin.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.countdown.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.actual.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/theme-script.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/option8.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/datepicker/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/datepicker/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/responsiveDatatable/responsive.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootbox.min.js"></script>	
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/js/visitor_tracking.js"></script> -->
<script>
$(document).ready(function(){
	var $owl = $('.owl-carousel').owlCarousel();
	$owl.owlCarousel().removeClass('owl-loaded');

// Show the carousel and trigger refresh
$owl.show(function(){
  $(this).addClass('owl-loaded').trigger('refresh.owl.carousel');
})


    $('[data-toggle="popover"]').popover({
	trigger: 'click hover',
	container: 'body',
	placement: 'auto',
	delay: {show: 50, hide: 400}
});
});

//<![CDATA[
$(window).load(function(){
var originalLeave = $.fn.popover.Constructor.prototype.leave;
$.fn.popover.Constructor.prototype.leave = function(obj){
  var self = obj instanceof this.constructor ?
    obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
  var container, timeout;

  originalLeave.call(this, obj);

  if(obj.currentTarget) {
    container = $(obj.currentTarget).siblings('.popover')
    timeout = self.timeout;
    container.one('mouseenter', function(){
      //We entered the actual popover – call off the dogs
      clearTimeout(timeout);
      //Let's monitor popover content instead
      container.one('mouseleave', function(){
        $.fn.popover.Constructor.prototype.leave.call(self, self);
      });
    })
  }
};


$('body').popover({
	selector: '[data-popover]',
	trigger: 'click hover',
	container: 'body',
	placement: 'auto',
	delay: {show: 50, hide: 400}
});

 $(function () {
	    $('#dob').datetimepicker({
	     format: 'DD/MM/YYYY',
	         maxDate:new Date(),
	   });
	   $('#profession_since').datetimepicker({
	     	format: 'DD/MM/YYYY',
	         maxDate:new Date(),
	   });
   });
  //$('select').select2();
  
  
});//]]> 

</script>
<script type='text/javascript'>

$(document).ready(function() {
    $('#wallet_transaction').DataTable();
    $('#bank_details').DataTable();
	$('#all_cases_table').DataTable();
});

function showChar(num){
	var showChar = num;
	var ellipsestext = "...";
	var moretext = "more";
	var lesstext = "less";
	$('.more').each(function() {
		var content = $(this).html().trim();

		if(content.length > showChar) {

			var c = content.substr(0, showChar);
			var h = content.substr(showChar-0, content.length - showChar);

			var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

			$(this).html(html);
		}

	});
}

$(document).ready(function() {
	var moretext = "more";
	var lesstext = "less";
	$(".morelink").click(function(){
		if($(this).hasClass("less")) {
			$(this).removeClass("less");
			$(this).html(moretext);
		} else {
			$(this).addClass("less");
			$(this).html(lesstext);
		}
		$(this).parent().prev().toggle();
		$(this).prev().toggle();
		return false;
	});
});

function logoutFun(){
	localStorage.removeItem('LocalCustomerCart');
	//if(confirm("Your cart will be empty. Are you sure to logout?")){
		location.href="<?php echo base_url();?>Loginsession/logout";
			/*$.ajax({
					url:"<?php echo base_url()?>Account/delete_carttable_for_customer",
					type:"post",
					data:"1=2",
					success:function(res){
						location.href="<?php echo base_url();?>Loginsession/logout";
					}
					
				})*/
	//}
}
</script>

<script src="<?php echo base_url();?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script>
$(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            // hide sidebar
            $('#sidebar').removeClass('active');
            // hide overlay
            $('.overlay').removeClass('active');
        });

        $('#sidebarCollapse1,#sidebarCollapse2').on('click', function () {
            // open sidebar
            $('#sidebar').addClass('active');
            // fade in the overlay
            $('.overlay').addClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });

	<!---user activity--->

 $(window).on('load', function(){
	
	// section_activity=localStorage.getItem("section_activity");
	
	// 	var current_page_old = localStorage.getItem("current_page");
		
	// 	var current_page='<?php echo $_SERVER['REQUEST_URI'];?>';
		
	// alert(current_page_old + "||||"+current_page + "section_activity" + section_activity);
		
	// 	if((current_page_old!=current_page) && current_page_old!=null && section_activity!=null){
			
			
	// 		$.ajax({
	// 			url:"<?php echo base_url()?>dump_section_activity",
	// 			type:"POST",
	// 			data:"section_activity="+section_activity+"&current_page="+current_page_old,
	// 			dataType: 'json',
	// 			success:function(res){
	// 				alert(res);

	// 				localStorage.removeItem('current_page');
	// 				localStorage.removeItem('section_activity');
	// 				localStorage.setItem("current_page", '<?php echo $_SERVER['REQUEST_URI'];?>');
	// 			}		
	// 		});
 
	// 	}else{
	// 		localStorage.setItem("current_page", current_page);
	// 	}
		
	// 	reviews=localStorage.getItem("reviews");
	// 	if(reviews!=null && reviews!=''){
	// 		$.ajax({
	// 			url:"<?php echo base_url()?>dump_reviews_read",
	// 			type:"POST",
	// 			data:"reviews="+reviews,
	// 			dataType: 'json',
	// 			success:function(res){
	// 				localStorage.removeItem('reviews');	
	// 			}		
	// 		});
	// 	}
		
	// 	filter=localStorage.getItem("filter");
	// 	if(filter!=null && filter!=''){
	// 		$.ajax({
	// 			url:"<?php echo base_url()?>dump_filter_checked",
	// 			type:"POST",
	// 			data:"filter="+filter,
	// 			dataType: 'json',
	// 			success:function(res){
	// 				localStorage.removeItem('filter');	
	// 			}		
	// 		});
	// 	}
	// 	return false;
  });

  <!---user activity--->

  function addNewsLetterSubscription(obj){
      var email_address_nl=document.getElementById('email_address_nl').value;
      if(email_address_nl==""){
          document.getElementById('subscibing_info').innerHTML='<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Add Email Address';
          return false;
      }
      var contact_me=document.getElementById('contact_me');
      if(contact_me.checked){
          return false;
      }
      var form_data=new FormData(obj);
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
         var xhr = new XMLHttpRequest();
                if (xhr) {                
                    document.getElementById('subscibing_info').innerHTML='<i class="fa fa-refresh fa-spin" aria-hidden="true"></i> Subscribing'
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            if(xhr.responseText=="1"){
                               document.getElementById('subscibing_info').innerHTML='<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Subscribed Successfully'
							   document.getElementById('email_address_nl').value='';
                           }
                           else{
                              document.getElementById('subscibing_info').innerHTML='<i class="fa fa-exclamation-circle" aria-hidden="true"></i> <b>Server Error</b>. <br>Alternatively you can subscribe newsletter by sending mail to info@voometstudio.com'
                           }
                        }
                    }
                    xhr.open(obj.getAttribute('method'), obj.getAttribute('action'), true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.send('email_address_nl='+email_address_nl);
                }   
                return false;
  }

  
  
  
									 
function myOrdersFun(){
	<?php
		if($this->session->userdata("customer_id")){
			?>
			location.href="<?php echo base_url()?>Account/my_order";
			<?php
		}
		else{
			?>
			location.href="<?php echo base_url()?>login";
			<?php
		}
	?>
}
function myAddressesFun(){
	<?php
		if($this->session->userdata("customer_id")){
			?>
			location.href="<?php echo base_url()?>Account/addresses";
			<?php
		}else{
			?>
			location.href="<?php echo base_url()?>login";
			<?php
		}
	?>
}
function myPersonalInfoFun(){
	<?php
		if($this->session->userdata("customer_id")){
			?>
			location.href="<?php echo base_url()?>Account";
			<?php
		}else{
			?>
			location.href="<?php echo base_url()?>login";
			<?php
		}
	?>
}


function schedule_appointmentFun(){
	  //element = document.getElementById("schedule_appointment");
	//  element.scrollIntoView({behavior: "smooth"});
	  
	  
	   var element = document.getElementById('schedule_appointment');
	   if(element!==null){
			  var headerOffset = 150;
				var elementPosition = element.getBoundingClientRect().top;
			  var offsetPosition = elementPosition + window.pageYOffset - headerOffset;
			  
			  window.scrollTo({
				  top: offsetPosition,
				  behavior: "smooth"
			  }); 
			  
			  
			
	   }
	   else{
		   localStorage.setItem("schedule_appointment","diffpage")
		   location.href="<?php echo base_url()?>";
		  // setTimeout(xxx(),1000)
		   
		   
			  
	   }
	  
}

</script>
<?php
if($this->session->flashdata("notification"))
{
    echo "<script type='text/javascript'> alert(\"".$this->session->flashdata("notification")."\"); </script>";
    unset($_SESSION['notification']);
}
if($this->session->flashdata("error"))
{
    echo "<script type='text/javascript'> alert(\"".$this->session->flashdata("error")."\"); </script>";
    unset($_SESSION['error']);
}
?>






<script>
$(document).on('click', '.bootbox.modal', function (event) {
    bootbox.hideAll()
});
$( ".preventDflt" ).click(function( event ) {
    //event.preventDefault();
});


									
</script>
 <div class="overlay"></div>
</body>
</html>
