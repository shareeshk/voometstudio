<?php 
		$bg="";
		$ft="";
		if(!empty($ui_arr["bg_img"])){
			$bg=$ui_arr["bg_img"]->background_image;
		}
		if(!empty($ui_arr["ft_img"])){
			$ft=$ui_arr["ft_img"]->background_image;
		}
	?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/skins/jquery-ui-like/progressbar.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/product_comparision/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/product_comparision/w3.css" />
<style type="text/css">
body{
background-color: #f1f3f6;
}



</style>

<style>
.services2 .services2-item,.services2{
border:0px;
}
.services2 .services2-item .image{
padding-left:120px;
}
.form-group .help-block {
display: none;
}
.form-group.has-error .help-block {
display: block;
}
.block-trending{
margin-top:0px !important;
}
</style>

	
	<style type="text/css">

	.bodyimage {
	<?php if($bg!=''){
		?>
	background: url('<?php echo base_url().$bg;?>') no-repeat;	
	
		<?php 
	}else{
		?>
		background:#b3c95c;
		<?php
		
	}?>
	background-size: 100% auto;
	  z-index:-100;
	}
	</style>
	
	
	<style type="text/css">
.form-group .help-block {
  display: none;
}
 
.form-group.has-error .help-block {
  display: block;
}
</style>


<style>
.swal2-modal .swal2-styled{
	background-color:#1429a0 !important;
}
</style>
<style>
.becomeareseller_para{
	text-align:justify;
}
</style>




<!-- Prakruthi Pariksha Details Starts -->
<section class="section8  content-gap  block-trending" id="book_a_doctor_appointment">
<!--<section class="section8 block-trending" id="products_range">--->
    <div class="section-container">
        
		</div>
        <div class="section-content">
			<a id="schedule_appointment"></a>
            <div class="container">
				<div class="row">

					<div class="col-md-6">
					<h3 class="section-title text-center" style="margin-bottom:2rem;line-height:30px;">Become A Reseller</h3>
						<div class="align-top" style="margin-top:20px;line-height:25px;">
							<p class="becomeareseller_para">
							We have lined up a very wide range of inventory for our resellers. We now carry more than 10 brands from luxury to premium to economy to cater to all kinds of market requirements. Our resellers are assured of high ROI on their investments.

							</p>
<p class="align-top becomeareseller_para">

What more? Each reseller will have a specific protected territoty to operate in and we will protect their business interest end to end. On an investment of 50L, we estimate a break-even period of 24 months and an operating margin of well over 15%

</p>

<h4 class="align-top" style="text-align: center;margin-top:3rem;">
	Connect to us if our product line and market opportunity interests you!
</h4>

							</p>
						</div>
					</div>

					<div class="col-md-4" >
					
						<div class="box-authentication pb-0"  ng-controller="becomearesellerController">
							<form name="becomearesellerForm" novalidate>
							
								<div class="form-group" show-errors>
									<input ng-model="formData_enquiry.reseller_companyname" name="reseller_companyname" type="text" class="form-control" placeholder="Enter Your Company Name" required>
									<p class="help-block" ng-if="becomearesellerForm.reseller_companyname.$error.required">Company Name is required</p>
								</div>
								
								
								<div class="form-group" show-errors>
									<input ng-model="formData_enquiry.reseller_name" name="reseller_name" type="text" class="form-control" placeholder="Enter Your Person Name" required>
									<p class="help-block" ng-if="becomearesellerForm.reseller_name.$error.required">Name is required</p>
								</div>
								
								
								
								
							
								
								
								
								<div class="form-group" show-errors>
										<input type="email" name="reseller_email" class="form-control" ng-model="formData_enquiry.reseller_email" required placeholder="Enter Your Email">
				   
					
					<p class="help-block" ng-if="becomearesellerForm.reseller_email.$error.required">Email address is required</p>
							<p class="help-block" ng-if="becomearesellerForm.reseller_email.$error.email">Email address is invalid</p>	
				
					
					
								</div>
								
								<div class="form-group" show-errors>
										<input type="text" name="reseller_mobile" class="form-control" ng-model="formData_enquiry.reseller_mobile" allow-only-numbers placeholder="Enter Your Mobile No"  onKeyPress="return isNumber(event)" ng-minlength="10" 
                   ng-maxlength="10" min="0" maxLength="10" required>
								
								<p class="help-block" ng-show="becomearesellerForm.reseller_mobile.$error.required || becomearesellerForm.reseller_mobile.$error.number">Valid mobile number is required
								</p>
							
								<p class="help-block" ng-show="((becomearesellerForm.reseller_mobile.$error.minlength ||
								becomearesellerForm.reseller_mobile.$error.maxlength) && 
								becomearesellerForm.reseller_mobile.$dirty) ">
											Mobile number should be 10 digits
								</p>
								
								</div>
								
								<div class="form-group" show-errors>
									<input type="text" name="reseller_city" class="form-control" ng-model="formData_enquiry.reseller_city" placeholder="Enter Your City" required>
									<p class="help-block" ng-if="becomearesellerForm.reseller_city.$error.required">City is required</p>
								</div>
								

								<div class="form-group" show-errors>
									<select ng-model="formData_enquiry.reseller_type" name="reseller_type" class="form-control" required>
										
										<option value="" disabled selected hidden>I am a</option>
										<option value="Experienced">Experienced</option>
										<option value="First Time">First Time</option>
										
									</select>
									<p class="help-block" ng-if="becomearesellerForm.reseller_type.$error.required">Type is required</p>
								</div>


								

								<div class="form-group">
									<button class="btn btn-sm btn-block"  ng-click="save_becomeareseller();$event.stopPropagation();" ng-disabled="becomearesellerForm.$invalid"  style="background-color:#1429a0;color:#fff;font-size:1 em;opacity:1" id="becomearesellerForm_btn"> SUBMIT </button>	
								</div>
							</form>
						</div>
					</div>
				</div>
				
				
            </div>
        </div>
    </div>
</section>


<!-- Prakruthi Pariksha Details Ends -->








<script>


var module = angular.module('app', ['ui.bootstrap', 'angular-responsive']);
module.factory('Scopes', function ($rootScope) {
var mem = {};
return {
store: function (key, value) {
$rootScope.$emit('scope.stored', key);
mem[key] = value;
},
get: function (key) {
return mem[key];
}
};
});






module.directive('showErrors', function($timeout) {
return {
restrict: 'A',
require: '^form',
link: function (scope, el, attrs, formCtrl) {
var inputEl   = el[0].querySelector("[name]");
var inputNgEl = angular.element(inputEl);
var inputName = inputNgEl.attr('name');
var blurred = false;
inputNgEl.bind('blur', function() {
blurred = true;
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$watch(function() {
return formCtrl[inputName].$invalid
}, function(invalid) {
if (!blurred && invalid) { return }
el.toggleClass('has-error', invalid);
});

scope.$on('show-errors-check-validity', function() {
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$on('show-errors-reset', function() {
$timeout(function() {
el.removeClass('has-error');
}, 0, false);
});
}
}
});


</script>








<script>

var module = angular.module('app', ['ui.bootstrap', 'angular-responsive']);
module.factory('Scopes', function ($rootScope) {
var mem = {};
return {
store: function (key, value) {
$rootScope.$emit('scope.stored', key);
mem[key] = value;
},
get: function (key) {
return mem[key];
}
};
});


function becomearesellerController($scope,$rootScope,Scopes,$http) {


Scopes.store('becomearesellerController', $scope);


$scope.save_becomeareseller = function() {
$scope.$broadcast('show-errors-check-validity');
if ($scope.becomearesellerForm.$valid) {


$data=$scope.formData_enquiry;
url="<?php echo base_url();?>add_save_becomeareseller";
$("#becomearesellerForm_btn").html('<i class="fa fa-spin fa-refresh"></i> Processing');
$http({
method : "POST",
url : url,
data: $data,
dataType:"JSON",
}).success(function mySucces(res) {
$("#becomearesellerForm_btn").html('<i class="fa fa-user"></i> Create');
if(res.status=="yes"){
	secure_id=res.secure_id;
 	

	

	swal({
		title: '',
		html: '<p>Thank you for showing interest with <?php echo SITE_NAME; ?>. Our executive will get in touch with you soon to further the next steps</p>',
		type: 'success',
		showCancelButton: false,
		showConfirmButton: true,
		confirmButtonText: 'Ok',
	}).then(function () {
		location.reload();
	});


}else{
   // alert('Not Sent');
	bootbox.alert({
	size: "small",
	message: 'Oops Error',
	});	
}

},function myError(response) {

});	


}
}


}

module.directive('allowOnlyNumbers', function () {  
            return {  
                restrict: 'A',  
                link: function (scope, elm, attrs, ctrl) {  
                    elm.on('keydown', function (event) {  
                        if (event.which == 64 || event.which == 16 || event.which == 46 || event.which == 86) {  
                            // to allow numbers  
                            return false;  
                        } else if (event.which >= 48 && event.which <= 57) {  
                            // to allow numbers  
                            return true;  
                        } else if (event.which >= 96 && event.which <= 105) {  
                            // to allow numpad number  
                            return true;  
                        } else if ([8, 9, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {  
                            // to allow backspace, enter, escape, arrows  
                            return true;  
                        } else {  
                            event.preventDefault();  
                            // to stop others  
                            return false;  
                        }  
                    });  
                }  
            }  
        });




module.directive('showErrors', function($timeout) {
return {
restrict: 'A',
require: '^form',
link: function (scope, el, attrs, formCtrl) {
var inputEl   = el[0].querySelector("[name]");
var inputNgEl = angular.element(inputEl);
var inputName = inputNgEl.attr('name');
var blurred = false;
inputNgEl.bind('blur', function() {
blurred = true;
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$watch(function() {
return formCtrl[inputName].$invalid
}, function(invalid) {
if (!blurred && invalid) { return }
el.toggleClass('has-error', invalid);
});

scope.$on('show-errors-check-validity', function() {
el.toggleClass('has-error', formCtrl[inputName].$invalid);
});

scope.$on('show-errors-reset', function() {
$timeout(function() {
el.removeClass('has-error');
}, 0, false);
});
}
}
});

$(document).ready(function () {
// $('#patient_dob').datepicker({
// format: "dd/mm/yyyy"
// });

// $('#patient_dob').bootstrapMaterialDatePicker
// 			({
// 				time: false,
// 				clearButton: true,
// 				weekStart: 0, 
// 				format: 'DD/MM/YYYY'
// 			});

});
function isNumber(evt) {

evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : evt.keyCode;

if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	return false;
}

return true;
}
</script>

