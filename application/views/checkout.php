<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/front_end.css">
<!---for paytm blink --->
<!--<script type="application/javascript" crossorigin="anonymous" src="<?php /*echo PAYTM_ENVIRONMENT; */?>/merchantpgpui/checkoutjs/merchants/<?php /*echo PAYTM_MID; */?>.js"></script>
-->
<?php
/*require_once("paytm/config.php");
$result = getTransactionToken();
//print_r($result);
//exit;*/
?>
<!---for paytm blink --->
<style type="text/css">
	/* addon css */

	.sku_display_name_on_buttons,
	.sku_display_name_on_buttons:hover {
		line-height: 1.6rem;
		padding: 0.3rem;
		width: 100%;
		color: #fff;
		font-size: 1.02em;
		box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;
	}

	.image-checkbox .sku_display_name_on_buttons {
		background-color: #dd7973;
		white-space: normal;
	}

	.image-checkbox-checked .sku_display_name_on_buttons {
		background-color: #50d42c;
		white-space: normal;
		min-height: 60px;
	}

	.sku_display_name_on_buttons .sku_display_name_on_button_1 {
		font-weight: 600;
	}

	.sku_display_name_on_buttons .sku_display_name_on_button_2 {
		font-weight: 400;
	}

	.yourprice_div {
		margin-bottom: -0.7rem;
	}

	.offerprice_addon {
		font-size: 1.8rem;
		color: #004b9b;
	}

	.hurbztextcolor {
		color: #ff6600;
	}

	.sku_display_name_on_buttons,
	.sku_display_name_on_buttons:hover {
		line-height: 1.6rem;
		padding: 0.3rem;
		width: 100%;
		color: #fff;
		font-size: 1.02em;
		box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;
	}

	.image-checkbox {
		cursor: pointer;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		border: 1px solid #eee;
		margin-bottom: 0;
		outline: 0;
		margin: 5px 5px 5px 5px;
		border-radius: 10px;
		line-height: 30px;
		font-size: 14px;
	}

	.image-checkbox .sku_display_name_on_buttons {
		background-color: #dd7973;
	}

	.image-checkbox-checked .sku_display_name_on_buttons {
		background-color: #50d42c;
	}

	.sku_display_name_on_buttons .sku_display_name_on_button_1 {
		font-weight: 600;
	}

	.sku_display_name_on_buttons .sku_display_name_on_button_2 {
		font-weight: 400;
	}

	/* addon css */

	.cart_summary .qty input {
		text-align: center;
		max-width: 50px;
		margin: 0 auto;
		border-radius: 0px;
		border: 1px solid #eaeaea;
	}

	.cart_summary .qty a {
		padding: 3px 5px 5px 5px;
		border: 1px solid #eaeaea;
		display: inline-block;
		width: auto;
		margin-top: 0px;
	}

	.overflow {
		overflow: auto;
	}

	.left-align-cashback {
		width: 86%;
		float: left;
		text-align: right;
	}

	.left-align-cashback-total {
		width: 85%;
		float: left;
		text-align: right;
		font-size: .9em;
	}

	.cart_summary td {
		vertical-align: middle !important;
		padding: 10px;
	}

	.cart_summary .table>tbody>tr>td,
	.table>tbody>tr>th,
	.cart_summary .table>tfoot>tr>td,
	.table>tfoot>tr>th,
	.cart_summary .table>thead>tr>td {
		padding: 5px;
	}

	.table>thead>tr>th {
		padding: 6px;
	}

	.cod_align {
		margin-top: 10px;
		float: left;
		width: 100%;
		border: 1px solid #ccc;
		padding: 15px;
	}

	.font_alignment {
		font-size: 14px;
		margin-bottom: 10px;
		/*float: left;*/
	}

	/*//payment// */

	.set_align {
		word-wrap: break-word;
		margin-top: 10px;
		margin-left: 30px;
		padding: 0px 10px;
	}

	.manage_address {
		/*margin-top:10px;
margin-left: 20px;*/
	}

	.address_div {
		margin-left: 20px;
	}

	.address_edit {
		width: 48%;
		/*margin-right:10px;*/
		float: left;
		height: 25px;
		margin: 10px 0px;
	}

	.address_delete {
		width: 48%;
		float: right;
		height: 25px;
		margin: 10px 0px;
	}

	.logout_notice {
		padding: 6px;
		font-size: 14px;
		border: 1px solid rgb(225, 224, 136);
		/*width: 100%;*/
		background-color: rgb(246, 242, 203);
		margin-top: 20px;
	}

	.carousel-control.right {
		left: auto;
		right: 15px;
		display: none;
		cursor: pointer;
	}

	.carousel-control.left {
		right: auto;
		/*left: -15px;*/
		cursor: pointer;
	}

	@media (max-width: 480px) {
		.carousel-control.left {
			right: auto;
			/*left: 0px;*/
			cursor: pointer;
		}
	}

	.carousel-indicators {
		display: none;
	}

	.carousel-control {
		position: absolute;
		top: 40%;
		left: 15px;
		width: 40px;
		height: 40px;
		margin-top: -20px;
		font-size: 30px;
		font-weight: 100;
		line-height: 27px;
		color: #ffffff;
		text-align: center;
		background: #222222;
		border: 3px solid #ffffff;
		-webkit-border-radius: 23px;
		-moz-border-radius: 23px;
		border-radius: 23px;
		opacity: 0.5;
		filter: alpha(opacity=50);
	}

	.form-group .help-block {
		display: none;
	}

	.form-group.has-error .help-block {
		display: block;
	}

	#mydiv {
		position: absolute;
		top: 13px;
		left: 0;
		width: 100%;
		height: 100%;
		z-index: 1000;
		/* background-color:#fff;*/
		opacity: .8;
	}

	.ajax-loader {
		position: absolute;
		left: 50%;
		top: 50%;
		margin-left: -32px;
		/* -1 * image width / 2 */
		margin-top: -32px;
		/* -1 * image height / 2 */
		display: block;
	}

	#mydiv_re {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		z-index: 1000;
		/* background-color:#fff;*/
		opacity: .8;
	}

	#mydiv_pay {
		position: absolute;
		/*top: 82px;
left: 31px;*/
		width: 100%;
		height: 100%;
		z-index: 1000;
		/* background-color:#fff;*/
		opacity: .8;
		display: none;
	}

	.ajax-loader_re {
		position: absolute;
		left: 50%;
		top: 32%;
		margin-left: -32px;
		/* -1 * image width / 2 */
		margin-top: -32px;
		/* -1 * image height / 2 */
		display: block;
	}

	.ajax-loader_pay {
		position: absolute;
		left: 50%;
		top: 3%;
		margin-left: -32px;
		/* -1 * image width / 2 */
		margin-top: -32px;
		/* -1 * image height / 2 */
		display: block;
	}

	/*.nav>li.disabled>a:focus, .nav>li.disabled>a:hover{		
cursor:pointer;		
}
.nav > li.disabled > a {
cursor: not-allowed !important;
}
.page-content{
cursor: not-allowed !important;			margin-top:0;
}*/
	.well {
		border: none !important;
		background-color: #fff;
		-webkit-box-shadow: none;
		transition: all 0.3s ease-in-out 0s;
	}

	.my-cart {
		margin-top: 0.2em;
	}

	th {
		font-size: 0.8em;
		font-weight: 600;
	}

	.editcartinfo u a {
		color: #31708f;
		font-size: 1.5rem;
	}

	.editcartinfo {
		margin-bottom: 1rem;
		text-align: center;
	}

	.editcartinfo u {
		text-decoration-style: dotted;
	}



	/* Image checkbox */

	.nopad {
		padding-left: 0 !important;
		padding-right: 0 !important;
	}

	/*image gallery*/

	.image-checkbox img {
		border-radius: 10px;

		cursor: pointer;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		border: 1px solid #eee;
		margin-bottom: 0;
		outline: 0;
		margin: 5px 5px 5px 5px;
		border-radius: 10px;
		line-height: 30px;
		font-size: 14px;
		border-radius: 10px;
		max-width: 50%;
		margin-left: auto;
		margin-right: auto;
	}

	.image-checkbox img {

		border-radius: 10px;
		max-width: 50%;
		margin-left: auto;
		margin-right: auto;
	}

	.image-checkbox input[type="checkbox"] {
		display: none;
	}

	.image-checkbox-checked {
		border-color: #50d42c;
	}

	.image-checkbox-checked:hover {
		border-color: #50d42c;
	}

	.image-checkbox .fa {
		position: absolute;
		color: #fff;
		background-color: #50d42c;
		padding: 5px;
		top: 5px;
		right: 5px;
		border-radius: 12px;
		font-size: 14px;
		height: 25px;
	}

	.image-checkbox-checked .fa {
		display: block !important;
	}

	.image-checkbox:hover {
		border-color: #50d42c;
	}

	.margin-top {
		margin-top: 10px;
	}

	.margin-bottom {
		margin-bottom: 10px;
	}

	/* Image checkbox */
</style>
<script type="text/javascript">
	module.directive('dnDisplayMode', function ($window) {
		return {
			restrict: 'A',
			scope: {
				dnDisplayMode: '='
			},
			template: '<span class="mobile"></span><span class="tablet"></span><span class="tablet-landscape"></span><span class="desktop"></span>',
			link: function (scope, elem, attrs) {
				var markers = elem.find('span');

				function isVisible(element) {
					return element && element.style.display != 'none' && element.offsetWidth && element.offsetHeight;
				}

				function update() {
					angular.forEach(markers, function (element) {
						if (isVisible(element)) {
							scope.dnDisplayMode = element.className;
							return false;
						}
					});
				}

				var t;
				angular.element($window).bind('resize', function () {
					clearTimeout(t);
					t = setTimeout(function () {
						update();
						// scope.$apply();
					});
				});

				update();
			}
		};
	});
	module.directive('emailExistC', function ($http) {
		return {
			restrict: 'A',
			require: 'ngModel',
			link: function (scope, element, attr, ctrl) {
				function customValidator(ngModelValue) {
					ctrl.$setValidity('emailexistValidator', true);
					var FormData = { 'email': ngModelValue };

					$http({
						method: "POST",
						url: "<?php echo base_url(); ?>Loginsession/check_email",
						data: FormData,
						dataType: 'json',
					}).success(function mySucces(res) {
						if (res.valid != null) {
							if (res.valid == true) {
								ctrl.$setValidity('emailexistValidator', true);
							}
							if (res.valid == false) {
								ctrl.$setValidity('emailexistValidator', false);
							}
						}
						else {
							ctrl.$setValidity('emailexistValidator', true);
						}

					}, function myError(response) {
						alert('Error!');
						location.reload();
						/*bootbox.alert({
						size: "small",
						message: 'Error!',
						callback: function () { location.reload(); } 
						});*/
					});

					return ngModelValue;
				}
				ctrl.$parsers.push(customValidator);
			}
		};
	});

	module.directive('mobileExistC', function ($http) {
		return {
			restrict: 'A',
			require: 'ngModel',
			link: function (scope, element, attr, ctrl) {

				function custommValidator(ngModelValue) {
					ctrl.$setValidity('mobileexistValidator', true);
					var FormData = { 'mobile': ngModelValue };
					$http({
						method: "POST",
						url: "<?php echo base_url(); ?>Loginsession/check_mobile",
						data: FormData,
						dataType: 'json',
					}).success(function mySucces(res) {
						if (res.valid != null) {
							if (res.valid == true) {
								ctrl.$setValidity('mobileexistValidator', true);
							}
							if (res.valid == false) {
								ctrl.$setValidity('mobileexistValidator', false);
							}
						} else {
							ctrl.$setValidity('mobileexistValidator', true);
						}

					}, function myError(response) {
						alert('Error!');
						location.reload();
						/*bootbox.alert({
						size: "small",
						message: 'Error!',
						callback: function () { location.reload(); } 
						});*/
					});

					return ngModelValue;
				}
				ctrl.$parsers.push(custommValidator);
			}
		};
	});


	module.directive('validPasswordC', function () {

		return {
			require: 'ngModel',
			scope: {
				reference: '=validPasswordC'
			},
			link: function (scope, elm, attrs, ctrl) {
				ctrl.$setValidity('noMatch', true);
				ctrl.$parsers.unshift(function (viewValue, $scope) {

					var noMatch = viewValue != scope.reference
					ctrl.$setValidity('noMatch', !noMatch);
					return (noMatch) ? noMatch : !noMatch;
				});

				scope.$watch("reference", function (value) {
					;
					ctrl.$setValidity('noMatch', value === ctrl.$viewValue);
				});
			}
		}
	});

	module.directive('allowOnlyNumbers', function () {
		return {
			restrict: 'A',
			link: function (scope, elm, attrs, ctrl) {
				elm.on('keydown', function (event) {
					if (event.which == 64 || event.which == 16 || event.which == 46 || event.which == 86) {
						// to allow numbers  
						return false;
					} else if (event.which >= 48 && event.which <= 57) {
						// to allow numbers  
						return true;
					} else if (event.which >= 96 && event.which <= 105) {
						// to allow numpad number  
						return true;
					} else if ([8, 9, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {
						// to allow backspace, enter, escape, arrows  
						return true;
					} else {
						event.preventDefault();
						// to stop others  
						return false;
					}
				});
			}
		}
	});

	module.filter('ceil', function () {
		return function (input) {
			//return Math.ceil(+input);
			return Math.round(+input);
		};
	})

	module.directive('showErrors', function ($timeout) {
		return {
			restrict: 'A',
			require: '^form',
			link: function (scope, el, attrs, formCtrl) {
				// find the text box element, which has the 'name' attribute
				var inputEl = el[0].querySelector("[name]");
				// convert the native text box element to an angular element
				var inputNgEl = angular.element(inputEl);
				// get the name on the text box
				var inputName = inputNgEl.attr('name');

				// only apply the has-error class after the user leaves the text box
				var blurred = false;
				inputNgEl.bind('blur', function () {
					blurred = true;
					el.toggleClass('has-error', formCtrl[inputName].$invalid);
				});

				scope.$watch(function () {
					return formCtrl[inputName].$invalid
				}, function (invalid) {
					// we only want to toggle the has-error class after the blur
					// event or if the control becomes valid
					if (!blurred && invalid) { return }
					el.toggleClass('has-error', invalid);
				});

				scope.$on('show-errors-check-validity', function () {
					el.toggleClass('has-error', formCtrl[inputName].$invalid);
				});

				scope.$on('show-errors-reset', function () {
					$timeout(function () {
						el.removeClass('has-error');
					}, 0, false);
				});
			}
		}
	});



	function generate_random_number() {
		//localStorage.removeItem('random_no');
		ra = Math.floor(100000 + Math.random() * 900000);
		//localStorage.setItem('random_no', ra);
		return ra;
	}

	module.directive('showOtperrors', function ($timeout) {
		return {
			restrict: 'A',
			require: '^form',
			link: function (scope, el, attrs, formCtrl) {
				var inputEl = el[0].querySelector("[name]");
				var inputNgEl = angular.element(inputEl);
				var inputName = inputNgEl.attr('name');
				var blurred = false;
				inputNgEl.bind('blur', function () {
					blurred = true;
					el.toggleClass('has-error', formCtrl[inputName].$invalid);
				});

				scope.$watch(function () {
					return formCtrl[inputName].$invalid
				}, function (invalid) {
					if (!blurred && invalid) { return }
					el.toggleClass('has-error', invalid);
				});

				scope.$on('show-errors-check-validity', function () {
					el.toggleClass('has-error', formCtrl[inputName].$invalid);
				});

				scope.$on('show-errors-reset', function () {
					$timeout(function () {
						el.removeClass('has-error');
					}, 0, false);
				});
			}
		}
	});
	module.directive('userExistC', function ($http) {
		return {
			restrict: 'A',
			require: 'ngModel',
			link: function (scope, element, attr, ctrl) {
				function custmValidator(ngModelValue) {
					ctrl.$setValidity('userexistValidator', true);

					return ngModelValue;
				}
				ctrl.$parsers.push(custmValidator);
			}
		};
	});

	module.directive('passwordCheckC', function ($http) {
		return {
			restrict: 'A',
			require: 'ngModel',
			link: function (scope, element, attr, ctrl) {
				function custmmValidator(ngModelValue) {
					ctrl.$setValidity('passwordwrongValidator', true);
					return ngModelValue;
				}
				ctrl.$parsers.push(custmmValidator);
			}
		};
	});

	function NewUserController($scope, Scopes, $http, $timeout) {

		Scopes.store('NewUserController', $scope);

		$scope.save = function () {
			$scope.$broadcast('show-errors-check-validity');

			if ($scope.userForm.$valid) {
				password = $scope.formData.password;
				original_password = $scope.formData.password;
				var temppwd = "";
				for (i = 0; i < password.length; i++) {
					temppwd += String.fromCharCode(password.charCodeAt(i) - 6);
				}

				$data = $scope.formData;
				$data.password = temppwd;

				if (Scopes.get('NewUserController').c_id != null) {
					c_id = Scopes.get('NewUserController').c_id;
					$data.c_id = c_id;
					url = "<?php echo base_url(); ?>Loginsession/update_customer";
				} else {

					url = "<?php echo base_url(); ?>Loginsession/add_customer";
				}
				$scope.formData.password = original_password;

				$http({
					method: "POST",
					url: url,
					data: $data,
					dataType: 'json',
				}).success(function mySucces(res) {

					if (res.data == true) {
						$scope.c_id = res.c_id;
						$("#createuser").hide();
						$("#mobile_verification").show();
						$scope.setvariables();
						//$scope.reset_for_change(); 
					} else {
						alert('New User not created');
						location.reload();

						/*bootbox.alert({
						size: "small",
						message: 'New User not created',
						callback: function () { location.reload(); } 
						});*/
					}

				}, function myError(response) {
					alert('Error');
					location.reload();

					/*bootbox.alert({
					size: "small",
					message: 'Error',
					callback: function () { location.reload(); } 
					});*/
				});


			}
		};

		$scope.setvariables = function () {
			if (Scopes.get('NewUserController').c_id != null) {
				c_id = Scopes.get('NewUserController').c_id;
				mobile = Scopes.get('NewUserController').formData.mobile;
				email = Scopes.get('NewUserController').formData.email;
				name = Scopes.get('NewUserController').formData.name;
				//alert('c_id='+c_id+',mobile='+mobile+',email='+email);
				$scope.$broadcast('show-errors-reset');
				$scope.formData = { name: name, email: email, mobile: mobile, password: '', password_c: '' };
			}
		}

		$scope.reset = function () {
			$scope.$broadcast('show-errors-reset');
			$scope.formData = { name: '', email: '', mobile: '', password: '', password_c: '' };
		}

	}

	function MobileOtpController($scope, Scopes, $http, $timeout) {

		Scopes.store('MobileOtpController', $scope);
		$scope.change_mobile = function () {

			c_id = Scopes.get('NewUserController').c_id;
			$("#createuser").show();
			$("#mobile_verification").hide();

		};

		$scope.resend_code = function () {
			c_id = Scopes.get('NewUserController').c_id;
			mobile = Scopes.get('NewUserController').formData.mobile;
			$data.c_id = c_id;
			$data.val = mobile;
			$("#createuser").hide();
			$("#mobile_verification").show();
			url = "<?php echo base_url(); ?>Loginsession/resend_code_to_newuser";
			$http({
				method: "POST",
				url: url,
				data: $data,
				dataType: 'json',
			}).success(function mySucces(res) {

				if (res.data == true) {

					alert('Code has been resent to your mobile number');
					location.reload();

					/*bootbox.alert({
					size: "small",
					message: 'Code has been resent to your mobile number',
					callback: function () { location.reload(); } 
					});*/
				} else {
					alert('New User not created');
					location.reload();

					/*bootbox.alert({
					size: "small",
					message: 'New User not created',
					callback: function () { location.reload(); } 
					});*/
				}

			}, function myError(response) {
				alert('Error');
				location.reload();

				/*bootbox.alert({
				size: "small",
				message: 'Error',
				callback: function () { location.reload(); } 
				});*/
			});


		}

		$scope.otpverify = function () {

			c_id = Scopes.get('NewUserController').c_id;

			if ($scope.otp_code != null) {
				var otpData = { 'otp_code': $scope.otp_code, 'c_id': c_id };
				$http({
					method: "POST",
					url: "<?php echo base_url(); ?>Loginsession/check_otpcode",
					data: otpData,
					dataType: 'json',
				}).success(function mySucces(res) {


					if (res.data != null) {
						if (res.data == true) {

							c_id = Scopes.get('NewUserController').c_id;
							var active_data = { 'c_id': c_id };

							url = "<?php echo base_url(); ?>Loginsession/activate_new_user";
							$http({
								method: "POST",
								url: url,
								data: active_data,
								dataType: 'json',
							}).success(function mySucces(res) {
								//alert(res.data);
								if (res.data == true) {
									alert('New User has Been Activated');
									location.reload();

									/*bootbox.alert({
									size: "small",
									message: 'New User has Been Activated',
									callback: function () { location.reload(); } 
									});*/

									////////////////////////////////
									if (localStorage.LocalCustomerCart != null) {
										localstorage_products = JSON.parse(localStorage.LocalCustomerCart);
										$http({
											method: "POST",
											url: "<?php echo base_url(); ?>Account/carttable_dump",
											data: localstorage_products,
											dataType: 'json',
										}).success(function mySucces(result) {
											//alert(result);
											if (result) {
												//alert('Login success');
												//alert("Updated");
												localStorage.clear();
												$scope.mobileOtp.otp_code.$setValidity("otpsameValidator", true);
												jQuery('#mobileOtp')[0].reset();
												$('#result').html('<p class="text-success">Mobile number has been verified Successfully</p>').delay(3000).fadeOut();
												//window.location="<?php echo base_url(); ?>";
												location.reload();
											}
											else {
												alert('Not Updated');
												location.reload();

												/*bootbox.alert({
												size: "small",
												message: 'Not Updated',
												callback: function () { location.reload(); } 
												});*/
											}
										}, function myError(response) {

										});
									}
									else {
										$scope.mobileOtp.otp_code.$setValidity("otpsameValidator", true);
										jQuery('#mobileOtp')[0].reset();
										$('#result').html('<p class="text-success">Mobile number has been verified Successfully</p>').delay(3000).fadeOut();
										//window.location="<?php echo base_url(); ?>";
										location.reload();
									}
									//////////////////////////////////////


								} else {
									alert('New User not activated');
									location.reload();

									/*bootbox.alert({
									size: "small",
									message: 'New User not activated',
									callback: function () { location.reload(); } 
									});*/
								}

							}, function myError(response) {
								alert('Error');
								location.reload();

								/*bootbox.alert({
								size: "small",
								message: 'Error',
								callback: function () { location.reload(); } 
								});*/
							});

						}
						if (res.data == false) {
							$scope.mobileOtp.otp_code.$setValidity("otpsameValidator", false);
						}
					} else {
						$scope.mobileOtp.otp_code.$setValidity("otpsameValidator", true);
					}

				}, function myError(response) {
					alert('Error');

					/*bootbox.alert({
					size: "small",
					message: 'Error',
					});*/
				});

				$scope.$broadcast('show-errors-check-validity');

			}//if condition
		};
	}

	function LoginController($scope, Scopes, $http, $timeout) {
		$('#mydiv').hide();
		Scopes.store('LoginController', $scope);
		$scope.new_user_reg = function () {
			$('#login_form').hide();
			$('#createuser').show();
			$('#forgot_form').hide();
			$('#mobile_verification').hide();

		}

		$scope.login_auth = function (type = '') {
			$scope.$broadcast('show-errors-check-validity');
			username = $scope.username;
			password = $scope.password;
			$scope.loginForm.username.$setValidity('userexistValidator', true);
			$scope.loginForm.password.$setValidity('passwordwrongValidator', true);

			$scope.loginForm.username.$setValidity('userUnBlockedValidator', true);

			if (username != null) {
				$scope.check_user();
				if (type == 're_activate') {
					$scope.loginForm.username.$setValidity('userUnBlockedValidator', true);
				} else {
					if ($scope.loginForm.$valid) {
					} else {
						return false;
					}
				}
			}


			if (password != null && username != null) {

				var temppwd = "";
				for (i = 0; i < password.length; i++) {
					temppwd += String.fromCharCode(password.charCodeAt(i) - 7);
				}

				var FormData = { 'password': temppwd, 'username': username, 'type': type };
				$http({
					method: "POST",
					url: "<?php echo base_url(); ?>Loginsession/customer_login",
					data: FormData,
					dataType: 'json',
				}).success(function mySucces(resul) {
					if (resul.valid != null) {
						if (resul.valid == true) {
							$scope.loginForm.password.$setValidity('passwordwrongValidator', true);
							$scope.loginForm.username.$setValidity('userUnBlockedValidator', true);

							if ($scope.loginForm.$valid) {
								if (localStorage.LocalCustomerCart != null) {
									localstorage_products = JSON.parse(localStorage.LocalCustomerCart);

									$http({
										method: "POST",
										url: "<?php echo base_url(); ?>Account/carttable_dump",
										data: localstorage_products,
										dataType: 'json',
									}).success(function mySucces(result) {
										//alert(result);
										if (result) {
											//alert('Login success');
											//alert("Updated");
											localStorage.clear();
											//window.location="<?php echo base_url(); ?>";
											location.reload();
										}
										else {
											alert('Not Updated');
											location.reload();

											/*bootbox.alert({
											size: "small",
											message: 'Not Updated',
											callback: function () { location.reload(); } 
											});*/
										}
									}, function myError(response) {

									});
								}
								else {
									//window.location="<?php echo base_url(); ?>";
									location.reload();
								}
							}
						}
						if (resul.valid == false) {
							$scope.loginForm.password.$setValidity('passwordwrongValidator', false);
							$scope.loginForm.username.$setValidity('userUnBlockedValidator', true);
						}
					} else {

						$scope.loginForm.password.$setValidity('passwordwrongValidator', true);
						$scope.loginForm.username.$setValidity('userUnBlockedValidator', true);
					}

				}, function myError(response) {
					$scope.loginForm.password.$setValidity('passwordwrongValidator', false);
				});
			}


		};
		$scope.forget_password = function () {

			username = $scope.username;

			if (username != null) {
				$scope.$broadcast('show-errors-check-validity');

				var FormData = { 'val': username };

				$http({
					method: "POST",
					url: "<?php echo base_url(); ?>Loginsession/customer_check_user",
					data: FormData,
					dataType: 'json',
				}).success(function mySucces(res) {
					if (res.valid != null) {
						if (res.valid == false) {
							$scope.loginForm.username.$setValidity('userexistValidator', true);
							$('#mydiv').show();
							$http({
								method: "POST",
								url: "<?php echo base_url(); ?>Loginsession/resend_code_to_setpassword",
								data: FormData,
								dataType: 'json',
							}).success(function mySucces(res) {
								if (res.valid != null) {
									if (res.valid == true) {
										$('#mydiv').hide();
										$scope.customer_id = res.c_id;
										$scope.field = res.field;
										$scope.value_f = res.value;

										$('#login_form').hide();
										$('#forgot_form').show();
										$('#text_title').html("<span> Verification code has been sent to your " + $scope.field + "</span><span><b> " + $scope.value_f + "</b></span>");

										//$scope.$broadcast('show-errors-reset');
										//$scope = {username: username, password: ''};

									}
								}

							}, function myError(response) {

								alert('Forget password error');
								location.reload();
								/*
								bootbox.alert({ 
								size: "small",
								message: 'Forget password error',
								callback: function () { location.reload(); } 
								});*/

							});
						}
						else {
							$scope.loginForm.username.$setValidity('userexistValidator', false);
						}
					}
					else {
						$scope.loginForm.username.$setValidity('userexistValidator', false);
					}

				}, function myError(response) {

					$scope.loginForm.username.$setValidity('userexistValidator', false);

				});

			} else {
				$scope.$broadcast('show-errors-check-validity');
			}

		};
		$scope.check_user = function () {
			$scope.loginForm.username.$setValidity('userUnBlockedValidator', true);

			var FormData = { 'val': username };

			$http({
				method: "POST",
				url: "<?php echo base_url(); ?>Loginsession/customer_check_user",
				data: FormData,
				dataType: 'json',
			}).success(function mySucces(res) {
				if (res.valid != null) {
					if (res.valid == true) {
						$scope.loginForm.username.$setValidity('userexistValidator', false);//show the error
					}
					if (res.valid == false) {
						$scope.loginForm.username.$setValidity('userexistValidator', true);	//no error	
					}
					if (res.is_user_blocked == 1 && res.active == 0 && res.deactivated_by == "admin") {

						$scope.loginForm.username.$setValidity('userBlockedValidator', false);
					}
					if (res.is_user_blocked == 0 && res.active == 0 && res.deactivated_by == "") {
						$("#login_btn").hide();
						$("#reactivate_btn").show();

						$scope.loginForm.username.$setValidity('userUnBlockedValidator', false);
						$scope.flag = 1;
					} else {
						$scope.loginForm.username.$setValidity('userUnBlockedValidator', true);
					}
				} else {
					$scope.loginForm.username.$setValidity('userexistValidator', false);
					$scope.loginForm.username.$setValidity('userUnBlockedValidator', true);
				}


			}, function myError(response) {

				$scope.loginForm.username.$setValidity('userexistValidator', false);

			});

		};



	}
	function fogotPasswController($scope, Scopes, $http, $timeout) {
		$("#mydiv_re").hide();
		Scopes.store('fogotPasswController', $scope);

		$scope.set_password = function () {

			$scope.$broadcast('show-errors-check-validity');
			customer_id = Scopes.get('LoginController').customer_id;

			if ($scope.forgotForm.$valid) {
				password = $scope.f_password;
				var temppwd = "";
				for (i = 0; i < password.length; i++) {
					temppwd += String.fromCharCode(password.charCodeAt(i) - 7);
				}

				var sec_data = { 'otp_code': $scope.f_otp, 'c_id': customer_id, password: temppwd };
				url = "<?php echo base_url(); ?>Loginsession/reset_password";
				$http({
					method: "POST",
					url: url,
					data: sec_data,
					dataType: 'json',
				}).success(function mySucces(res) {

					if (res.data == true) {
						$scope.c_id = res.c_id;
						$("#createuser").hide();
						$("#forgot_form").hide();

						f_value = Scopes.get('LoginController').value_f;
						var FormData = { 'password': temppwd, 'username': f_value };
						$http({
							method: "POST",
							url: "<?php echo base_url(); ?>Loginsession/customer_login",
							data: FormData,
							dataType: 'json',
						}).success(function mySucces(resul) {
							if (resul.valid != null) {
								if (resul.valid == true) {
									if (localStorage.LocalCustomerCart != null) {
										localstorage_products = JSON.parse(localStorage.LocalCustomerCart);

										$http({
											method: "POST",
											url: "<?php echo base_url(); ?>Account/carttable_dump",
											data: localstorage_products,
											dataType: 'json',
										}).success(function mySucces(result) {
											//alert(result);
											if (result) {
												localStorage.clear();
												alert('Successfully Logged');
												location.reload();
												/*bootbox.alert({
												size: "small",
												message: 'Successfully Logged',
												callback: function () { location.reload(); } 
												});*/
												//alert("Updated");

												//window.location="<?php echo base_url(); ?>";
												//location.reload();
											}
											else {
												alert('Not Updated');
												location.reload();

												/*bootbox.alert({
												size: "small",
												message: 'Not Updated',
												callback: function () { location.reload(); } 
												});*/
											}
										}, function myError(response) {

										});
									}
									else {
										//window.location="<?php echo base_url(); ?>";
										location.reload();
									}

								} else {
									alert('Error');
									/* bootbox.alert({
									size: "small",
									message: 'Error',
									});*/
								}

							}

						}, function myError(response) {

						});


					} else {

						alert('password is not reset Try again');

						/*bootbox.alert({
						size: "small",
						message: 'password is not reset Try again',
						});*/
					}

				}, function myError(response) {

				});

			}


			if (Scopes.get('LoginController').customer_id != null) {
				customer_id = Scopes.get('LoginController').customer_id;
				field = Scopes.get('LoginController').field;
				f_value = Scopes.get('LoginController').value_f;

				if ($scope.f_otp != null) {
					var otpData = { 'otp_code': $scope.f_otp, 'c_id': customer_id };
					$http({
						method: "POST",
						url: "<?php echo base_url(); ?>Loginsession/check_otpcode",
						data: otpData,
						dataType: 'json',
					}).success(function mySucces(res) {

						if (res.data != null) {
							if (res.data == true) {
								$scope.forgotForm.f_otp.$setValidity("otpsameValidator", true);
							}
							if (res.data == false) {
								$scope.forgotForm.f_otp.$setValidity("otpsameValidator", false);
							}
						} else {
							$scope.forgotForm.f_otp.$setValidity("otpsameValidator", true);
						}

					}, function myError(response) {
						alert('Error');

						/*bootbox.alert({
						size: "small",
						message: 'Error',
						});*/
					});

					$scope.$broadcast('show-errors-check-validity');

				}//if condition f_otp

			}

		};


		$scope.resend_code_fp = function () {

			customer_id = Scopes.get('LoginController').customer_id;
			field = Scopes.get('LoginController').field;
			f_value = Scopes.get('LoginController').value_f;
			$("#mydiv_re").show();

			if (customer_id != null) {
				var data = { 'c_id': customer_id, val: f_value };

				$("#createuser").hide();
				$("#mobile_verification").hide();
				url = "<?php echo base_url(); ?>Loginsession/resend_code_to_newpassword";
				$http({
					method: "POST",
					url: url,
					data: data,
					dataType: 'json',
				}).success(function mySucces(res) {

					if (res.valid == true) {
						alert('code has been resent');

						/*bootbox.alert({
						size: "small",
						message: 'code has been resent',
						});*/
						$("#mydiv_re").hide();
					} else {
						alert('code has not been resent');

						/*bootbox.alert({
						size: "small",
						message: 'code has not been resent',
						});*/
					}

				}, function myError(response) {
					alert('Error');
					/*bootbox.alert({
					size: "small",
					message: 'Error',
					});*/
				});



			}

		};

		$scope.change_user_f = function () {
			//location.reload();
			$("#forgot_form").hide();
			$("#login_form").show();
		};
	}

</script>

<!-- page wapper-->
<div class="columns-container">
	<div class="container" id="columns">
		<!-- breadcrumb 
<div class="breadcrumb clearfix">
<a class="home" href="<?php //echo base_url()?>" title="Return to Home">Home</a>
<span class="navigation-pipe">&nbsp;</span>
<span class="navigation_page">Checkout</span>
</div>-->
		<!-- ./breadcrumb -->
		<!-- page heading-->
		<!-- ../ <h2 class="page-heading no-line">
<span class="page-heading-title2">Shopping Cart Summary</span>
</h2>




page heading-->
		<div class="page-content page-order">

			<div class="popular-tabs">
				<ul class="nav step">
					<!----data-toggle="tab" href="#tab-1"
<li class="disabled"><a href="#">01. Sign In</a></li>--->
					<li class="disabled"><a href="#tab-2">01. Delivery Address</a></li>
					<li class="disabled"><a href="#tab-3">02. Order Summary</a></li>
					<li class="disabled"><a href="#tab-4">03. Payment Method</a></li>
				</ul>

				<div class="tab-container">
					<?php /*
		  <div id="tab-1" class="tab-panel active">

		  <div class="page-content">
		  <div class="row">

		  <div class="col-sm-12" id="createuser" style="display:none;">

		  <div class="col-sm-6">

		  <div class="box-authentication" ng-controller="NewUserController">
		  <h3>Create an account</h3>
		  <form name="userForm" novalidate>
		  <p>Please enter your details to create an account.</p>
		  <div class="form-group" show-errors>
		  <label for="">Name</label>
		  <input id="" name="name" ng-model="formData.name" required placeholder="Enter Name" type="text" class="form-control">
		  <p class="help-block" ng-if="userForm.name.$error.required">The user's name is required</p>
		  </div>

		  <div class="form-group" show-errors>
		  <label for="">Email address</label>
		  <input type="email" class="form-control" name="email" ng-model="formData.email" required placeholder="Email" email-exist-c="formData.email" id="email" />

		  <p class="help-block" ng-if="userForm.email.$error.emailexistValidator">This Email is  registered already.</p>

		  <p class="help-block" ng-if="userForm.email.$error.required">The user's email is required</p>
		  <p class="help-block" ng-if="userForm.email.$error.email">The email address is invalid</p>				

		  </div>

		  <div class="form-group" show-errors>
		  <label for="">Mobile</label>
		  <input type="text" ng-minlength="10" 
		  ng-maxlength="10" min="0" name="mobile" class="form-control" ng-model="formData.mobile" required allow-only-numbers mobile-exist-c="formData.mobile" >

		  <p class="help-block" ng-show="userForm.mobile.$error.required || userForm.mobile.$error.number">Valid mobile number is required
		  </p>
		  <p class="help-block" ng-show="userForm.mobile.$error.mobileexistValidator">This mobile is registered already.</p>

		  <p class="help-block" ng-show="((userForm.mobile.$error.minlength ||
		  userForm.mobile.$error.maxlength) && 
		  userForm.mobile.$dirty) ">
		  mobile number should be 10 digits
		  </p>

		  </div>

		  <div class="form-group" show-errors>
		  <label for="">Password</label>
		  <input id="" type="password" name="password" ng-model="formData.password" required class="form-control" ng-minlength="6" ng-maxlength="10">
		  <p class="help-block" ng-if="userForm.password.$error.required">The password is required</p>

		  <p ng-show="userForm.password.$error.minlength" class="help-block">
		  Passwords must be between 6 and 10 characters.</p>


		  </div>

		  <div class="form-group" show-errors ng-class="{'has-error':formData.password_c.$invalid && !formData.password_c.$pristine}">
		  <label for="password_c">Confirm Password</label>
		  <input type="password" id="password_c" name="password_c" ng-model="formData.password_c" valid-password-c="formData.password" required class="form-control" />

		  <p ng-if="userForm.password_c.$error.noMatch" class="help-block">Passwords do not match.</p>
		  <p ng-if="userForm.password_c.$error.required" class="help-block">Confirm Password</p>
		  </div>

		  <button class="button" ng-click="save()"><i class="fa fa-user"></i> Create an account</button>

		  <button class="button" ng-click="reset()">Reset</button>

		  </form>

		  </div>
		  </div>
		  <div class="col-sm-3"></div>
		  </div>

		  <div class="col-sm-12" id="mobile_verification" style="display:none;">

		  <div class="col-sm-6">
		  <div class="box-authentication" ng-controller="MobileOtpController">
		  <h3>Mobile number Verification</h3>
		  <form name="mobileOtp" novalidate id="mobileOtp">

		  <label id="result"></label>

		  <label for="verification_code">Verification code is sent to your mobile number <a href=""><span ng-click="change_mobile()" style="color:#382e03;"><u>Change Mob.No</u></span></a></label>
		  <label for="verification_code">Verification code is sent to your mobile number <a href=""><span ng-click="resend_code()" style="color:#382e03;"><u>Resend Code</u></span></a></label>

		  <div class="form-group" show-Otperrors>

		  <input id="otp_code" type="text" class="form-control" placeholder="Verification Code" name="otp_code" required ng-model="otp_code" otp-check-c allow-only-numbers ng-minlength="4" ng-maxlength="6" min="0">

		  <p ng-if="mobileOtp.otp_code.$error.otpsameValidator" class="help-block">Verification code is Wrong</p>

		  <p ng-if="mobileOtp.otp_code.$error.required" class="help-block">Verification code is required</p>

		  <p class="help-block" ng-if="((mobileOtp.otp_code.$error.minlength ||
		  mobileOtp.otp_code.$error.maxlength) && 
		  mobileOtp.otp_code.$dirty) ">
		  Verification code is not to be more than 6
		  </p>
		  </div>	
		  <button class="button" ng-click="otpverify()"><i class="fa fa-check"></i> Verify</button>
		  </form>

		  </div>
		  </div>
		  <div class="col-sm-3"></div>
		  </div>

		  <div class="col-sm-12" id="login_form">
		  <?php
		  if($this->session->userdata("customer_id")){
		  ?>
		  <div class="col-sm-12">
		  <h2>Logged in as <?php echo $this->session->userdata("customer_email");?></h2>
		  <br>
		  <button class="col-md-3 button" onclick="javascript:logoutFun();"><i class="fa fa-lock"></i> LOGOUT</button>
		  <br><br>
		  <h4 class="logout_notice col-md-10 col-sm-10">Note: If you  click on "LOGOUT" you will lose items in your cart and will be redirected to <b> Livingseed </b> home page.</h4>
		  </div>
		  <?php
		  }
		  else{
		  ?>
		  <div class="col-sm-6">	
		  <div class="box-authentication" ng-controller="LoginController">

		  <h3>LOGIN</h3>
		  <form name="loginForm" novalidate id="loginForm">

		  <div class="form-group" show-errors>
		  <input id="" name="username" type="text" class="form-control" placeholder="Email/Mobile" required ng-model="username" user-exist-c="username">
		  <p ng-if="loginForm.username.$error.required" class="help-block">Please enter Email/Mobile</p>

		  <p class="help-block" ng-if="loginForm.username.$error.userexistValidator">Customer is Not Yet Registered</p>
		  <p class="help-block" ng-if="loginForm.username.$error.userBlockedValidator">Your ID is blocked.Please Contact admin.</p>
		  <p ng-if="loginForm.username.$error.userUnBlockedValidator">Your ID is blocked. Click Re-activate button with old password to activate your account.</p>
		  </div>

		  <div class="form-group" show-errors>
		  <input name="password" id="password" type="password" class="form-control" placeholder="Enter Password" required ng-model="password" password-check-c="password">


		  <p ng-if="loginForm.password.$error.required" class="help-block">Please enter password</p>

		  <p ng-if="loginForm.password.$error.passwordwrongValidator" class="help-block">Please Check the password</p>

		  </div>

		  <p class="forgot-pass"><a href="" ng-click="forget_password()">Forgot your password?</a></p>
		  <div class="form-group">
		  <button class="button" ng-click="login_auth()"><i class="fa fa-lock"></i> LOGIN</button>
		  <button class="button" ng-click="new_user_reg()"> Register</button>
		  </div>
		  <div id="mydiv">

		  <img src="<?php echo base_url(); ?>assets/pictures/images/loading.gif" style="margin-top: -16px;" class="ajax-loader"/> 
		  </div>

		  </form>
		  </div>

		  </div>
		  <?php
		  }
		  ?>

		  <div class="col-sm-3"></div>
		  </div>

		  <div class="col-sm-12" id="forgot_form"  style="display:none;">

		  <div class="col-sm-6">
		  <div class="box-authentication" ng-controller="fogotPasswController">
		  <h3>LOGIN - Set Password</h3>
		  <form name="forgotForm" novalidate>

		  <div class="form-group">
		  <label for="verification_code">
		  <span id="text_title"></span>
		  <a href=''><span ng-click='change_user_f()' style='color:#382e03;'><u> Change</u></span></a>
		  </label>
		  </div>

		  <div class="form-group">
		  <label for="verification_code"><a href=""><span ng-click="resend_code_fp()" style="color:#382e03;"><u>Resend Code</u></span></a></label>
		  </div>
		  <div id="mydiv_re">
		  <img src="<?php echo base_url(); ?>assets/pictures/images/loading.gif" style="margin-top: -16px;" class="ajax-loader_re"/>
		  </div>

		  <div class="form-group" show-errors>



		  <input id="f_otp" type="text" class="form-control" placeholder="Verification Code" name="f_otp" required ng-model="f_otp" otp-check-c allow-only-numbers ng-minlength="4" ng-maxlength="6" min="0">

		  <p ng-if="forgotForm.f_otp.$error.otpsameValidator" class="help-block">Verification code is Wrong</p>

		  <p ng-if="forgotForm.f_otp.$error.required" class="help-block">Verification code is required</p>

		  <p class="help-block" ng-if="((forgotForm.f_otp.$error.minlength ||
		  forgotForm.f_otp.$error.maxlength) && 
		  forgotForm.f_otp.$dirty) ">
		  Verification code is not to be more than 6
		  </p>



		  </div>


		  <div class="form-group" show-errors>
		  <label for="">Set Password</label>
		  <input id="" type="password" name="f_password" ng-model="f_password" required class="form-control" ng-minlength="6" ng-maxlength="10">
		  <p class="help-block" ng-if="forgotForm.f_password.$error.required">The password is required</p>

		  <p ng-show="forgotForm.f_password.$error.minlength" class="help-block">
		  Passwords must be between 6 and 10 characters.</p>

		  </div>

		  <div class="form-group" show-errors ng-class="{'has-error':f_password_c.$invalid && !f_password_c.$pristine}">
		  <label for="f_password_c">Confirm Password</label>
		  <input type="password" id="f_password_c" name="f_password_c" ng-model="f_password_c" valid-password-c="f_password" required class="form-control" />

		  <p ng-if="forgotForm.f_password_c.$error.noMatch" class="help-block">Passwords do not match.</p>
		  <p ng-if="forgotForm.f_password_c.$error.required" class="help-block">Confirm Password</p>
		  </div>

		  <button class="button" ng-click="set_password()"><i class="fa fa-user"></i> Submit</button>


		  </form>

		  </div>
		  </div>
		  <div class="col-sm-3"></div>
		  </div>

		  </div><!---row--------->
		  <div class="cart_navigation">	
		  <?php
		  if($this->session->userdata("customer_id")){
		  ?>

		  <a class="next-btn" data-toggle="tab" href="#tab-2">Continue</a>
		  <?php
		  }
		  ?>
		  </div>
		  </div><!---page-content----------->

		  </div><!---tab1-->
		  */?>


					<div id="tab-2" class="tab-panel" ng-controller="DeliveryAddressController">

						<div class="row">

							<script>


								function DeliveryAddressController($scope, Scopes, $http, $timeout) {
									$scope.delivery_address_pin_error = 0;
									$('#mydiv_pay').hide();
									$scope.items1 = [1, 2, 3, 4, 5];
									$scope.items2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
									$scope.item_set_count = 1;
									$scope.displayMode = 'desktop';
									if (window.innerWidth < 768 && window.innerWidth >= 300) {
										$scope.displayMode = "mobile";
										$scope.item_set_count = 1;
									} else if (window.innerWidth <= 1000 && window.innerWidth >= 768) {
										$scope.displayMode = "tablet";
										$scope.item_set_count = 2;
									} else if (window.innerWidth > 1000) {
										$scope.displayMode = "desktop";
										$scope.item_set_count = 3;
									}
									$scope.$watch('displayMode', function (value) {
										slides = [];
										switch (value) {
											case 'desktop':
												//console.log(value);
												//$scope.item_set_count=4;
												break;
											case 'mobile':
												//console.log(value);
												//$scope.item_set_count=1;
												break;
											case 'tablet':
												//console.log(value);
												//$scope.item_set_count=2;
												break;
										}
									});
									// var whatDevice = $scope.nowDevice;
									$scope.myInterval = 7000;

									$http({
										method: "POST",
										url: "<?php echo base_url(); ?>Account/get_customer_all_address_in_json_format",
										data: "1=2",
										dataType: "JSON",
									}).success(function mySucces(result) {

										if (result != "false") {
											$scope.all_addresses = result;
											$scope.all_addresses_count = $scope.all_addresses.length;
											$scope.count_addresses_for_continue_button_hiding = $scope.all_addresses_count;
											$scope.slides = $scope.all_addresses;
											$scope.add_for_slide();
											//alert($scope.item_set_count);
											if ($scope.all_addresses_count > $scope.item_set_count && $scope.all_addresses_count != 0) {
												$(".carousel-control.right").css({ display: 'inline' });
												$(".carousel-control.left").css({ display: 'inline' });

											} else {
												$(".carousel-control.right").css({ display: 'none' });
												$(".carousel-control.left").css({ display: 'none' });
											}
										} else {
											$scope.count_addresses_for_continue_button_hiding = 0;
											$("#no_address_text").show();
											$("#no_address_text").html("Address is a must. Add an address before proceeding.");
											$(".carousel-control.right").css({ display: 'none' });
											$(".carousel-control.left").css({ display: 'none' });
										}

									}, function myError(response) {

									});

									$scope.markAsDefaultAddr = function (customer_address_id) {
										$http({
											method: "POST",
											url: "<?php echo base_url() ?>set_this_address_default/" + customer_address_id,
											data: "1=2",
											//dataType:"JSON",
										}).success(function mySucces(result) {

											if (result) {
												//alert("Selected address is set as default address successfully");	
												//$scope.get_customer_all_address();
												for (x in $scope.slides) {

													if ($scope.slides[x].customer_address_id == customer_address_id) {
														$scope.slides[x].make_default = 1;
													}
													else {
														$scope.slides[x].make_default = 0;
													}
												}
												//$scope.slides.SortByName(make_default);
												//slides = $scope.slides;
												$scope.add_for_slide();
											}


										}, function myError(response) {
											alert('Error');
											/*
											bootbox.alert({ 
											size: "small",
											message: 'Error',
											
											});*/
										});
									}

									$scope.add_for_slide = function () {
										//slides = $scope.slides = [];
										for (var s = 0; s < $scope.all_addresses_count; s++) {
											$scope.addSlide($scope.all_addresses[s]);
										}
									}

									$scope.SortByName = function (a, b) {
										var aName = a.name.toLowerCase();
										var bName = b.name.toLowerCase();
										return ((bName < aName) ? -1 : ((bName > aName) ? 1 : 0));
									}
									$scope.addSlide = function (all_addresses) {

										var newWidth = 600 + slides.length + 1;
										slides.push(all_addresses);
										//alert(JSON.stringify(slides));

										var i, first = [],
											second, third;
										var many = 1;
										$scope.displayMode = "desktop";

										if (window.innerWidth < 768 && window.innerWidth >= 300) {
											$scope.displayMode = "mobile";
										} else if (window.innerWidth <= 1000 && window.innerWidth >= 768) {
											$scope.displayMode = "tablet";
										} else if (window.innerWidth > 1000) {
											$scope.displayMode = "desktop";
										}

										if ($scope.displayMode == "mobile") { many = 1; }
										else if ($scope.displayMode == "tablet") { many = 2; }
										else { many = 3; }

										//alert($scope.displayMode);

										for (i = 0; i < $scope.slides.length; i += many) {

											//alert(JSON.stringify($scope.slides[i]));
											second = {
												image1: $scope.slides[i]
											};
											if (many == 1) { }
											if ($scope.slides[i + 1] && (many == 2 || many == 3)) {
												second.image2 = $scope.slides[i + 1];
											}
											if ($scope.slides[i + (many - 1)] && many == 3) {
												second.image3 = $scope.slides[i + 2];
											}
											first.push(second);
										}
										$scope.groupedSlides = first;
									};
									$scope.buttonClicked_continue = false;
									$scope.check_pincode_availability = function () {
										$scope.buttonClicked_continue = true;
										$("#mydiv_pay").show();
										Scopes.get('OrderController').common();
										$scope.buttonClicked_continue = false;
										$("#mydiv_pay").hide();

										/*$scope.service_available_on_pincode=0;
										$http({
										method : "POST",
										url:"<?php echo base_url() ?>Account/get_availability_of_shipping",
										data:"1=2",
										dataType:"JSON",
										}).success(function mySucces(result) {
										
										pincode=result.pincode;
										city=result.city;
										flag=result.flag;
										Scopes.get('OrderController').common(flag,pincode,city);
										return false;
										if(flag){
										
										$scope.service_available_on_pincode=1;
										
										$("#pin_availability_chk").html("");
										$("#tabtoordersumm_btn").show();
										
										Scopes.get('OrderController').common(flag,pincode,city);
										
										//$('.nav a[href="#tab-4"]').tab('show');
										}else{
										$scope.service_available_on_pincode=0;
										Scopes.get('OrderController').common(flag,pincode,city);
										$("#pin_availability_chk").html("<span style='margin-left:20px;'  class='logout_notice'><font color='red'>No address found. Shipping is not available.</font></span>");
										$("#tabtoordersumm_btn").hide();
										
										}
										
										}, function myError(response) {
										alert("error");
										});*/

									}
									$scope.get_customer_all_address = function () {
										$http({
											method: "POST",
											url: "<?php echo base_url(); ?>Account/get_customer_all_address_in_json_format",
											data: "1=2",
											dataType: "JSON",
										}).success(function mySucces(result) {
											if (result != "false") {
												$scope.all_addresses = result;
												$scope.all_addresses_count = $scope.all_addresses.length;
												$scope.count_addresses_for_continue_button_hiding = $scope.all_addresses_count;

												if ($scope.all_addresses_count == 0) {
													$scope.count_addresses_for_continue_button_hiding = 0;
													$("#no_address_text").show();
													$("#no_address_text").html("Address is a must. Add an address before proceeding.");
												} else {
													$("#no_address_text").hide();

												}

												if ($scope.all_addresses_count > 3 && $scope.all_addresses_count != 0) {
													$(".carousel-control.right").css({ display: 'inline' });
													$(".carousel-control.left").css({ display: 'inline' });

												} else {
													$(".carousel-control.right").css({ display: 'none' });
													$(".carousel-control.left").css({ display: 'none' });
												}
												$scope.slides = $scope.all_addresses;
												$scope.add_for_slide();
											} else {
												$scope.count_addresses_for_continue_button_hiding = 0;
												$("#no_address_text").show();
												$("#no_address_text").html("Address is a must. Add an address before proceeding.");
												$(".carousel-control.right").css({ display: 'none' });
												$(".carousel-control.left").css({ display: 'none' });
											}

										}, function myError(response) {

										});
									};

									$scope.askConfirmation = function (customer_address_id, x) {

										if (confirm("Do you want to delete this Address ?")) {
											result = true;
											$scope.askConfirmation_1(customer_address_id, x, result);
										}

										/*bootbox.confirm({
										message: "Do you want to delete this Address ?",
										size: "small",
										callback: function (result) {
										if(result){
										$scope.askConfirmation_1(customer_address_id,x,result);
										}
										}
										});
										*/
										/*var resp = confirm("Do you want to delete this Address ?");
										if(resp== true){
										
										}else{
										return false;
										}*/
									};
									$scope.askConfirmation_1 = function (customer_address_id, x, resp) {

										var remove_cart_info_post_data = { 'customer_address_id': customer_address_id };
										//var resp = confirm("Do you want to delete this Address ?");
										var resp = true;
										if (resp == true) {
											$http({
												url: "<?php echo base_url() ?>delete_this_address/" + customer_address_id,
												method: "POST",
												data: "1=2",
												dataType: "JSON",
											}).success(function mySucces(result) {

												//$scope.all_addresses.splice(x, 1);		
												//alert(JSON.stringify($scope.all_addresses));					
												for (k in $scope.all_addresses) {
													if ($scope.all_addresses[k].customer_address_id == customer_address_id) {
														delete $scope.all_addresses[k];
														//$scope.all_addresses.remove(k);

													}
												}

												for (x in $scope.slides) {
													if ($scope.slides[x].customer_address_id == customer_address_id) {
														$scope.slides.remove(x);
													}
												}

												$scope.get_customer_all_address();
												//$scope.slides.SortByName(make_default);

												slides = $scope.slides;
												//alert(JSON.stringify($scope.slides));
												$scope.add_for_slide();



												/*if(data){
												$("#add_container_"+customer_address_id).remove();
												alert("Selected address is removed successfully");
												}
												else{
												alert("error");
												}*/
											});

										}
										else {

										}

									}
									$scope.buttonClicked_addaddress = false;
									$scope.add_new_address_form_fun = function () {
										$('#mydiv_pay').show();
										$scope.buttonClicked_addaddress = true;
										$scope.reset();
										$scope.formData.country = "India";
										$("#add_delivery_address").show();
										//$('html, body').animate({scrollTop:580},800);
										$scope.formData.name = "<?php echo $this->session->userdata('customer_name') ?>";
										$scope.formData.mobile = "<?php echo $this->session->userdata('customer_mobile') ?>";
										$scope.title_name_address = "Add";
										$scope.add_edit_flag = "add";
										$("#add_edit_address_btn").html('<i class="fa fa-user"></i> Add');
										$('#mydiv_pay').hide();
										$scope.buttonClicked_addaddress = false;

									}
									$scope.editCustomerAddress = function (customer_address_id) {
										$scope.buttonClicked_continue = true;
										$('#mydiv_pay').show();
										$("#add_delivery_address").show();
										//$('html, body').animate({scrollTop:580},800);
										$scope.title_name_address = "Edit";
										$("#add_edit_address_btn").html('<i class="fa fa-user"></i> Apply');
										var Data = { 'customer_address_id': customer_address_id };
										$http({
											method: "POST",
											url: "<?php echo base_url(); ?>Account/get_customer_address",
											data: Data,
											dataType: "JSON",
										}).success(function mySucces(result) {
											$('#mydiv_pay').hide();
											$scope.customer_address_id = result.customer_address_id;
											name = result.customer_name;
											mobile = result.mobile;
											customer_gstin = result.customer_gstin;
											door_address = result.address1;
											street_address = result.address2;
											city = result.city;
											state = result.state;
											pin = result.pincode;
											country = result.country;
											if (result.make_default == 1) {
												make_default = true;
											} else {
												make_default = false;
											}
											$scope.add_edit_flag = "edit";
											$scope.formData = { name: name, mobile: mobile, customer_gstin: customer_gstin, door_address: door_address, street_address: street_address, city: city, state: state, pin: pin, country: country, make_default: make_default };

										}, function myError(response) {

										});

									}
									$scope.reset = function () {
										$scope.$broadcast('show-errors-reset');
										$scope.formData = { name: '', mobile: '', door_address: '', street_address: '', city: '', state: '', pin: '', country: '', make_default: false };
									}

									$scope.add_edit_address_fun = function () {
										$scope.$broadcast('show-errors-check-validity');
										//alert("|"+$scope.add_edit_flag+"|");
										if ($scope.add_edit_flag == "edit") {
											add_edit_flag_for_title = "Edit";
											url = "<?php echo base_url(); ?>Account/edit_delivery_address/" + $scope.customer_address_id;
										}
										else {
											add_edit_flag_for_title = "Add";
											url = "<?php echo base_url(); ?>Account/add_new_delivery_address";
										}
										if (!$scope.delivery_address_form.$valid) {
											alert('Kindly check errors for registration');

											/*bootbox.alert({
											size: "small",
											message: 'Kindly check errors for registration'
											});*/
										}
										if ($scope.delivery_address_form.$valid) {
											$data = $scope.formData;
											$("#add_edit_address_btn").html('<i class="fa fa-spin fa-refresh"></i> Processing');
											$http({
												method: "POST",
												url: url,
												data: $data,
												dataType: 'json',
											}).success(function mySucces(res) {
												$scope.buttonClicked_continue = false;
												if (add_edit_flag_for_title == "Edit") {
													$("#add_edit_address_btn").html('<i class="fa fa-user"></i> Apply');
												}
												else {
													$("#add_edit_address_btn").html('<i class="fa fa-user"></i> ' + add_edit_flag_for_title);
												}
												if (res) {
													if ($scope.add_edit_flag == "edit") {

														alert('Address updated successfully');
														window.scrollTo(0, 0);

														/*bootbox.alert({
														size: "small",
														message: 'Address updated successfully',
														callback: function () { window.scrollTo(0, 0); } 
														});*/


														$scope.get_customer_all_address();
													}
													else {

														//alert('Address added successfully');
														window.scrollTo(0, 0);

														/*bootbox.alert({
														size: "small",
														message: 'Address added successfully',
														callback: function () {  window.scrollTo(0, 0); } 
														});*/
														$scope.get_customer_all_address();
													}
													$scope.reset();
													$("#no_address_text").hide();
													$("#add_delivery_address").hide();
													//$("#add_delivery_address").show();
													//$('html, body').animate({scrollTop:0},800);
												}

											}, function myError(response) {

												alert('Error');

												/*bootbox.alert({
												size: "small",
												message: 'Error',
												});*/
											});

										}

									}
									$scope.get_city_state_details_by_pincodeFun = function () {
										pin = $scope.formData.pin;

										if (pin === undefined) {
										}
										else {
											$http({
												method: "POST",
												url: "<?php echo base_url(); ?>Account/get_city_state_details_by_pincode",
												data: { 'pin': pin },
												dataType: "JSON",
											}).success(function mySucces(result) {
												$scope.formData.city = result.city;
												$scope.formData.state = result.state;

												if (parseInt(result.count) == 0) {
													$scope.delivery_address_pin_error = 1;
													$('.delivery_address_pin_error').show();
													$('#pin_code_check').addClass('has-error');
												} else {
													//alert(result.count+"no error");
													$scope.delivery_address_pin_error = 0;
													$('.delivery_address_pin_error').hide();
													$('#pin_code_check').removeClass('has-error');
												}

											}, function myError(response) {

											});
										}
									}
									$scope.cancel = function () {
										$scope.reset();
										$('#add_delivery_address').hide();
										//$('html, body').animate({scrollTop:0},700);
										$scope.buttonClicked_continue = false;
									}

								}



							</script>

							<div class="display-mode" dn-display-mode="displayMode"></div>
							<div class="col-md-12">
								<p id="no_address_text" class="text-muted lead text-center"></p>
								<carousel interval="" id="myCarousel">
									<slide ng-repeat="address_value in groupedSlides track by $index"
										active="slide.active">
										<div>
											<div class="col-md-3 col-sm-6 set_align">

												<div id="add_container_{{address_value.image1.customer_address_id}}"
													class="address_div">
													<div class="control-label col-md-12 text-left ">
														<div class="">
															<h4>{{address_value.image1.customer_name}}</h4>
														</div>
														<div class=""> {{address_value.image1.address1}}</div>
														<div class=""> {{address_value.image1.address2}}</div>
														<div class=""> {{address_value.image1.city}} -
															{{address_value.image1.pincode}}</div>
														<div class=""> {{address_value.image1.state}},
															{{address_value.image1.country}}</div>
														<div class=""> {{address_value.image1.mobile}}</div>
														<div class=""> {{address_value.image1.customer_gstin}}</div>
														<!--<div class="control-label col-sm-12"> {{address_value.make_default}}</div>-->
													</div>

													<div class="manage_address control-label col-sm-12 text-center">
														<button class="col-sm-12 button preventDflt"
															ng-model="address_value.image1.make_default"
															ng-if="address_value.image1.make_default==1"
															data="{{address_value.image1.make_default}}"
															ng-value="{{address_value.image1.make_default}}"
															id="make_default{{address_value.image1.customer_address_id}}"
															ng-click="markAsDefaultAddr(address_value.image1.customer_address_id)"><i
																class="fa fa-check"></i> Default Address</button>

														<button class="col-sm-12 button preventDflt"
															ng-model="address_value.image1.make_default"
															ng-if="address_value.image1.make_default==0"
															data="{{address_value.image1.make_default}}"
															ng-value="{{address_value.image1.make_default}}"
															id="make_default{{address_value.image1.customer_address_id}}"
															ng-click="markAsDefaultAddr(address_value.image1.customer_address_id)">Use
															This Address</button>

													</div>


													<div class="control-label col-sm-12 text-center manage_address">

														<button class="address_edit btn btn-xs btn-default preventDflt"
															ng-click="editCustomerAddress(address_value.image1.customer_address_id)"
															ng-if="address_value.image1.customer_address_id"
															style="border:1px solid #448bfc;">Edit</button>
														<button
															class="address_delete btn btn-xs btn-default preventDflt"
															ng-click="askConfirmation(address_value.image1.customer_address_id,$index)"
															ng-if="address_value.image1.customer_address_id"
															style="border:1px solid #ff0000;">Delete</button>


													</div>

												</div><!--id--differ-->
											</div>

											<div class="col-md-3 col-sm-6 set_align">
												<div id="add_container_{{address_value.image2.customer_address_id}}">
													<div class="control-label col-md-12 text-left manage_address">
														<div class="">
															<h4>{{address_value.image2.customer_name}}</h4>
														</div>
														<div class=""> {{address_value.image2.address1}}</div>
														<div class=""> {{address_value.image2.address2}}</div>
														<div class=""> {{address_value.image2.country}}</div>
														<div class=""> {{address_value.image2.city}}</div>
														<div class=""> {{address_value.image2.pincode}}</div>
														<div class=""> {{address_value.image2.mobile}}</div>
														<!--<div class="control-label col-sm-12"> {{address_value.make_default}}</div>-->
													</div>

													<div class="manage_address control-label col-sm-12 text-center">
														<button class="col-sm-12 button preventDflt"
															ng-model="address_value.image2.make_default"
															ng-if="address_value.image2.make_default==1"
															data="{{address_value.image2.make_default}}"
															ng-value="{{address_value.image2.make_default}}"
															id="make_default{{address_value.image2.customer_address_id}}"
															ng-click="markAsDefaultAddr(address_value.image2.customer_address_id)"><i
																class="fa fa-check"></i> Default Address</button>

														<button class="col-sm-12 button preventDflt"
															ng-model="address_value.image2.make_default"
															ng-if="address_value.image2.make_default==0"
															data="{{address_value.image2.make_default}}"
															ng-value="{{address_value.image2.make_default}}"
															id="make_default{{address_value.image2.customer_address_id}}"
															ng-click="markAsDefaultAddr(address_value.image2.customer_address_id)">Use
															This Address</button>

													</div>


													<div class="control-label col-sm-12 text-center manage_address">

														<button class="address_edit btn btn-xs btn-default preventDflt"
															ng-click="editCustomerAddress(address_value.image2.customer_address_id) "
															ng-if="address_value.image2.customer_address_id"
															style="border:1px solid #448bfc;">Edit</button>
														<button
															class="address_delete btn btn-xs btn-default preventDflt"
															ng-click="askConfirmation(address_value.image2.customer_address_id,$index)"
															ng-if="address_value.image2.customer_address_id"
															style="border:1px solid #ff0000;">Delete</button>


													</div>

												</div><!--id--differ-->
											</div>

											<div class="col-md-3 col-sm-6 set_align">
												<div id="add_container_{{address_value.image3.customer_address_id}}">
													<div class="control-label col-md-12 text-left manage_address">
														<div class="">
															<h4>{{address_value.image3.customer_name}}</h4>
														</div>
														<div class=""> {{address_value.image3.address1}}</div>
														<div class=""> {{address_value.image3.address2}}</div>
														<div class=""> {{address_value.image3.country}}</div>
														<div class=""> {{address_value.image3.city}}</div>
														<div class=""> {{address_value.image3.pincode}}</div>
														<div class=""> {{address_value.image3.mobile}}</div>
														<!--<div class="control-label col-sm-12"> {{address_value.make_default}}</div>-->
													</div>

													<div class="manage_address control-label col-sm-12 text-center">
														<button class="col-sm-12 button preventDflt"
															ng-model="address_value.image3.make_default"
															ng-if="address_value.image3.make_default==1"
															data="{{address_value.image3.make_default}}"
															ng-value="{{address_value.image3.make_default}}"
															id="make_default{{address_value.image3.customer_address_id}}"
															ng-click="markAsDefaultAddr(address_value.image3.customer_address_id)"><i
																class="fa fa-check"></i> Default Address</button>

														<button class="col-sm-12 button preventDflt"
															ng-model="address_value.image3.make_default"
															ng-if="address_value.image3.make_default==0"
															data="{{address_value.image3.make_default}}"
															ng-value="{{address_value.image3.make_default}}"
															id="make_default{{address_value.image3.customer_address_id}}"
															ng-click="markAsDefaultAddr(address_value.image3.customer_address_id)">Use
															This Address</button>

													</div>


													<div class="control-label col-md-12 text-center manage_address">


														<button class="address_edit btn btn-xs btn-default preventDflt"
															ng-click="editCustomerAddress(address_value.image3.customer_address_id)"
															ng-if="address_value.image3.customer_address_id"
															style="border:1px solid #448bfc;">Edit</button>

														<button
															class="address_delete btn btn-xs btn-default preventDflt"
															ng-click="askConfirmation(address_value.image3.customer_address_id,$index)"
															ng-if="address_value.image3.customer_address_id"
															style="border:1px solid #ff0000;">Delete</button>


													</div>

												</div><!--id--differ-->

											</div>

										</div>
									</slide>
								</carousel>
							</div>

							<!---added--->

						</div>
						<div class="row">
							<div class="manage_address">
								<div class="col-md-6" id="add_delivery_address"
									style="display:none;border:1px solid #333;margin-top:5em;">
									<div class="box-authentication ">
										<h3>{{title_name_address}} Address</h3>
										<form method="post" novalidate id="delivery_address_form"
											name="delivery_address_form">

											<div class="form-group" show-errors>

												<input name="name" ng-model="formData.name" required placeholder="Name"
													type="text" class="form-control">
												<p class="help-block"
													ng-if="delivery_address_form.formData.name.$error.required">The
													Customer Name is required</p>
											</div>

											<div class="form-group" show-errors>

												<input type="text" ng-minlength="10" ng-maxlength="10" maxlength="10"
													onpaste="return false;" onkeypress="return isNumber(event);" min="0"
													name="mobile" class="form-control" ng-model="formData.mobile"
													required allow-only-numbers placeholder='Mobile No'>

												<p class="help-block"
													ng-show="delivery_address_form.mobile.$error.required || delivery_address_form.mobile.$error.number">
													Valid mobile number is required
												</p>


												<p class="help-block" ng-show="((delivery_address_form.mobile.$error.minlength ||
delivery_address_form.mobile.$error.maxlength) && 
delivery_address_form.mobile.$dirty) ">
													mobile number should be 10 digits
												</p>

											</div>

											<div class="form-group" show-errors>

												<input name="customer_gstin" ng-model="formData.customer_gstin"
													placeholder="GST No.(Not Mandatory)" type="text"
													class="form-control">

											</div>

											<div class="form-group" show-errors>

												<input name="door_address" ng-model="formData.door_address" required
													placeholder="Door Number" type="text" class="form-control">
												<p class="help-block"
													ng-if="delivery_address_form.door_address.$error.required">The Door
													Number is required</p>
											</div>

											<div class="form-group" show-errors>

												<input name="street_address" ng-model="formData.street_address" required
													placeholder="Street Address" type="text" class="form-control">
												<p class="help-block"
													ng-if="delivery_address_form.street_address.$error.required">The
													Street Address is required</p>
											</div>

											<div class="form-group" show-errors id="pin_code_check">

												<input type="text" ng-minlength="6" ng-maxlength="6" name="pin"
													class="form-control" ng-model="formData.pin" required
													allow-only-numbers placeholder='PIN/ZIP Code'
													onkeypress="return isNumber(event)"
													ng-paste="$event.preventDefault()" ng-pattern="/^[0-9]*$/"
													oninput="if(value.length>6)value=value.slice(0,6)"
													ng-keyup="get_city_state_details_by_pincodeFun()">

												<p class="help-block"
													ng-show="delivery_address_form.pin.$error.required || delivery_address_form.pin.$error.number">
													Valid pin code is required
												</p>

												<p class="help-block" ng-show="((delivery_address_form.pin.$error.minlength ||
delivery_address_form.pin.$error.maxlength) && 
delivery_address_form.pin.$dirty) ">
													Pincode should be 6 digits
												</p>
												<span class="delivery_address_pin_error"
													style="display:none;color:#a94442">
													Please enter valid pincode
												</span>

											</div>


											<div class="form-group" show-errors>

												<input name="city" ng-model="formData.city" required placeholder="City"
													type="text" class="form-control" readonly>
												<p class="help-block"
													ng-if="delivery_address_form.city.$error.required">The City is
													required</p>
											</div>

											<div class="form-group" show-errors>
												<input name="state" ng-model="formData.state" required
													placeholder="State/Province" type="text" class="form-control"
													readonly>


												<p class="help-block"
													ng-if="delivery_address_form.state.$error.required">The State is
													required</p>
											</div>



											<div class="form-group" show-errors>
												<input name="country" ng-model="formData.country" required
													placeholder="Country" type="text" class="form-control" readonly>
												<p class="help-block"
													ng-if="delivery_address_form.country.$error.required">The Country is
													required</p>
											</div>

											<div class="form-group">
												<label><input type="checkbox" class="form-control"
														ng-model="formData.make_default" name="make_default"
														value="{{formData.make_default}}"> Make Default Address</label>
											</div>
											<button class="button preventDflt" ng-click="add_edit_address_fun()"
												id="add_edit_address_btn"><i class="fa fa-user"></i>
												{{title_name_address}}</button>

											<button type="reset" ng-click="reset()"
												class="button preventDflt">Reset</button>
											<button ng-click="cancel()" class="button preventDflt">Cancel</button>

										</form>

									</div>
								</div>
							</div>


							<div class="cart_navigation" style="margin-top:5em;">
								<button class="button preventDflt" ng-click="add_new_address_form_fun()"
									ng-disabled="buttonClicked_addaddress">Add New Address</button>

								<button ng-show="count_addresses_for_continue_button_hiding>=1"
									class="next-btn preventDflt" data-toggle="tab" href="#tab-3"
									ng-click="check_pincode_availability()"
									ng-disabled="buttonClicked_continue">Continue</button>


							</div>
						</div>

					</div>


					<div id="tab-3" class="tab-panel">

						<?php /*?> 

			 <div class="col-sm-12" style="margin-bottom: 20px;" >

			 <?php ($promotions_cart);?>
			 <?php if(!empty($promotions_cart)){?>
			 <ul>
			 <?php foreach($promotions_cart as $data){?>
			 <li><?php //echo htmlspecialchars_decode($data['promo_quote']);?></li>
			 <?php }?>
			 </ul>
			 <?php } ?>
			 </div>  

			 <?php */?>

						<script>

							//2nd controller//

							function OrderController($scope, $rootScope, Scopes, $http, $sce, $timeout) {
								Scopes.store('OrderController', $scope);
								c_id = '<?php echo $this->session->userdata("customer_id"); ?>';
								$scope.eligible_free_shipping_display = 0;//this line added to display the shippingcharge Rs.99 in checkout page even if free promotion not applied (added on May 5th 2022)

								$scope.curr_sym = "<?php echo curr_sym; ?>";
								$scope.curr_txt = "<?php echo curr_txt; ?>";
								$scope.final_count_free_items = 0;
								$scope.show_cash_back_tr = 0;
								$scope.promo_temp = 0;
								$scope.new_promo_avail = 0;
								$scope.products_coupon = {};
								$scope.invoice_coupon = {};

								$scope.invoice_coupon_avail_status = parseInt('<?php echo $invoice_coupon; ?>');
								$scope.invoice_coupon_applied_status = 0;
								$scope.invoice_coupon_code_appied_txt = '';//invoice coupon code
								$scope.invoice_applied_price = 0;
								$scope.invoice_coupon_reduce_price = 0;
								$scope.amount_without_invoice_coupon = 0;
								$scope.continueToPaymentSection = function () {
									window.scrollTo(0, 0)
								}

								$scope.update_cart_controller_scopes = function (order_product_count, products_order) {

									$scope.count_of_free_items = new Array();
									$scope.final_count_free_items = 0;//newly added
									$scope.products_order = products_order;
									$scope.order_product_count = order_product_count;
									$scope.total_inventory_quantity_in_cart = 0;
									$scope.total_coupon_used_amount = 0;
									$scope.inv_addon_products_amount = 0;

									$scope.total_shipping_price_addon = 0;

									for (j = 0; j < $scope.order_product_count; j++) {
										$scope.total_inventory_quantity_in_cart += parseInt($scope.products_order[j].inventory_moq);
									}

									if ($scope.order_product_count == 0) {
										$scope.show_cash_back_tr = 0;
										$scope.eligible_cash_back_percentage_display = 0;
										$scope.eligible_discount_percentage_display = 0;
										$scope.eligible_free_shipping_display = 0;
										$scope.eligible_free_surprise_gift_display = 0;
										$scope.eligible_cash_back_percentage_display = 0;
										$scope.discount_oninvoice = 0;
										$scope.cash_back_oninvoice = 0;
										$scope.free_shipping = 0;
										$scope.surprise_gift = 0;
										$scope.total_cash_back = 0;
										$scope.show_surprise_gift_on_invoice = 0;
										$scope.invoice_surprise_gift = 0;
										$scope.final_count_free_items = 0;
										window.location.href = '<?php echo base_url(); ?>';
										return false;
									}

									payment_method_restrictions = new Array();
									payment_method_cod_max_value = 0;
									$scope.total_cash_back_for_sku = 0;
									total_netprice = 0;
									individual_price_without_shipping = 0;
									total_shipping_price = 0;
									$scope.out_of_stock_item_in_cart = [];
									$scope.logistics_service_availability = [];
									$scope.cash_back_tr_arr = [];
									$scope.applied_at_invoice_arr = [];
									$scope.total_addon_products = 0;
									for (j = 0; j < $scope.order_product_count; j++) {

										var val = $scope.products_order[j].attribute_1_value.split(':');
										$scope.products_order[j].attribute_1_value = val[0];

										if ($scope.products_order[j].cod_restrictions == "on") {
											payment_method_cod_max_value = $scope.products_order[j].max_value;
										}
										//alert($scope.products_order[j].stock_available+"stock_available");


										if (parseInt($scope.products_order[j].stock_available) >= parseInt($scope.products_order[j].inventory_moq)) {


											$scope.products_order[j].item_stock_available = 1;

											///cut///

											if (products_order[j].inventory_selling_price == null) {
												//when fetching the data from db	
												$scope.products_order[j].selling_price = $scope.products_order[j].selling_price;

											} else {
												$scope.products_order[j].selling_price = products_order[j].inventory_selling_price;
											}


											payment_method_restrictions.push($scope.products_order[j].attributes);

											cart_id = $scope.products_order[j].cart_id;

											if ($scope.products_order[j].logistics_id == -1) {

												$scope.products_order[j].msg_style = "color:red;";
												$scope.products_order[j].delivery_service_msg = 'Service is Not available';
												$('#delivery_speed_block_' + cart_id).hide();
												$('#parcel_category_block_' + cart_id).hide();
												$scope.logistics_service_availability.push(1);
											} else {

												$scope.products_order[j].msg_style = "color:#333;";
												$scope.products_order[j].delivery_service_msg = 'Service is available';
												$('#delivery_speed_block_' + cart_id).show();
												$('#parcel_category_block_' + cart_id).show();
											}

											if ($scope.products_order[j].promotion_minimum_quantity != '') {
												if ($scope.products_order[j].promotion_minimum_quantity <= parseInt($scope.products_order[j].inventory_moq)) {
													$scope.products_order[j].show_addon_items = 1;
												} else {
													$scope.products_order[j].show_addon_items = 0;
												}
											} else {
												$scope.products_order[j].show_addon_items = 0;
											}


											$scope.products_order[j].final_product_individual = 0;

											modulo = $scope.products_order[j].quantity_without_promotion;
											quotient = $scope.products_order[j].quantity_with_promotion;

											$scope.products_order[j].modulo = modulo;
											$scope.products_order[j].quotient = quotient;
											$scope.products_order[j].price_of_quotient_1 = parseInt($scope.products_order[j].individual_price_of_product_with_promotion);
											$scope.products_order[j].price_of_modulo_1 = parseInt($scope.products_order[j].individual_price_of_product_without_promotion);

											/* for checking */
											promo_id_selected = $scope.products_order[j].promotion_id_selected;
											inventory_id = $scope.products_order[j].inventory_id;
											//alert(promo_id_selected+"promo_id_selected");
											if ($scope.products_order[j].promotion_available == 0) {
												$scope.get_promotion_availability(promo_id_selected, inventory_id, j);

												//alert($scope.new_promo_avail);
												if ($scope.new_promo_avail == 1) {
													$scope.products_order[j].new_promo_avail = 1;
												} else {
													$scope.products_order[j].new_promo_avail = 0;
												}
											}
											//$scope.products_order[j].promotion_available == 1 && 
											if (promo_id_selected != null && promo_id_selected != '') {
												//confirms promotion available
												$scope.get_promotion_active_status(promo_id_selected);

												if ($scope.promo_temp == 2) {
													if ($scope.products_order[j].promotion_available == 1) {
														//promotion active in localStorage but in backend it is inactive or expired
														$scope.products_order[j].promotion_available = 0;
													}
												} else {
													$scope.products_order[j].promotion_available = 1;//
												}
												//alert($scope.promo_temp+'return_variable1');
											}

											/* for checking */

											if ($scope.products_order[j].promotion_available == 1) {

												////////////

												if ($scope.products_order[j].default_discount != "") {


													if (modulo != 0 && ($scope.products_order[j].inventory_moq >= parseInt($scope.products_order[j].promotion_minimum_quantity))) {

														$scope.products_order[j].discount = 1;//default
														$scope.products_order[j].promotion = 2;


														$scope.products_order[j].price_after_default_discount = $scope.products_order[j].total_price_of_product_without_promotion;

														if (quotient != 0) {

															$scope.products_order[j].price_after_promotion_discount = $scope.products_order[j].total_price_of_product_with_promotion;

														}

														if (quotient == 0 && modulo != 0) {
															$scope.products_order[j].promotion_quote_show = 0;
														} else {
															$scope.products_order[j].promotion_quote_show = 1;
														}
														if (quotient != 0 && modulo == 0) {
															$scope.products_order[j].discount_quote_show = 0;
														} else {
															$scope.products_order[j].discount_quote_show = 1;
														}

														if ($scope.products_order[j].price_after_promotion_discount != null) {
															$scope.products_order[j].final_product_individual = parseInt($scope.products_order[j].price_after_promotion_discount) + parseInt($scope.products_order[j].price_after_default_discount);
														} else {
															$scope.products_order[j].promotion = "";
															$scope.products_order[j].discount = 1;
															$scope.products_order[j].final_product_individual = parseInt($scope.products_order[j].price_after_default_discount);
														}

													} else {

														if ($scope.products_order[j].inventory_moq >= parseInt($scope.products_order[j].promotion_minimum_quantity)) {

															$scope.products_order[j].promotion_quote_show = 1;
															$scope.products_order[j].discount_quote_show = 0;

															$scope.products_order[j].price_after_promotion_discount = $scope.products_order[j].total_price_of_product_with_promotion;
															$scope.products_order[j].final_product_individual = parseInt($scope.products_order[j].price_after_promotion_discount);

															$scope.products_order[j].price_after_default_discount = 0;

															$scope.products_order[j].promotion = 1;
															$scope.products_order[j].discount = "";

														} else {

															if (quotient == 0 && modulo != 0) {
																$scope.products_order[j].promotion_quote_show = 0;
															} else {
																$scope.products_order[j].promotion_quote_show = 1;
															}
															if (quotient != 0 && modulo == 0) {
																$scope.products_order[j].discount_quote_show = 0;
															} else {
																$scope.products_order[j].discount_quote_show = 1;
															}


															$scope.products_order[j].price_after_default_discount = $scope.products_order[j].total_price_of_product_without_promotion;

															$scope.products_order[j].promotion_quote_show = 0;
															$scope.products_order[j].discount_quote_show = 1;

															$scope.products_order[j].promotion = "";
															$scope.products_order[j].discount = 1;
															$scope.products_order[j].final_product_individual = parseInt($scope.products_order[j].price_after_default_discount);

														}

													}

												}//discount promotion !=0

												if ($scope.products_order[j].default_discount == "") {


													if (modulo != 0) {

														$scope.products_order[j].discount = 1;
														$scope.products_order[j].promotion = 2;
														$scope.products_order[j].price_of_modulo_1 = $scope.products_order[j].selling_price;

														$scope.products_order[j].price_after_default_discount = $scope.products_order[j].total_price_of_product_without_promotion;

														if (quotient != 0) {


															$scope.products_order[j].price_of_quotient_1 = parseInt($scope.products_order[j].individual_price_of_product_with_promotion);


															$scope.products_order[j].price_after_promotion_discount = $scope.products_order[j].total_price_of_product_with_promotion;
														}

														if (quotient == 0 && modulo != 0) {
															$scope.products_order[j].promotion_quote_show = 0;
														} else {
															$scope.products_order[j].promotion_quote_show = 1;
														}
														if (quotient != 0 && modulo == 0) {
															$scope.products_order[j].discount_quote_show = 0;
														} else {
															$scope.products_order[j].discount_quote_show = 1;
														}

														if ($scope.products_order[j].price_after_promotion_discount != null) {
															$scope.products_order[j].final_product_individual = parseInt($scope.products_order[j].price_after_promotion_discount) + parseInt($scope.products_order[j].price_after_default_discount);
														} else {

															$scope.products_order[j].promotion = "";
															$scope.products_order[j].discount = 1;
															$scope.products_order[j].final_product_individual = parseInt($scope.products_order[j].price_after_default_discount);
														}


													} else {

														$scope.products_order[j].promotion_quote_show = 1;
														$scope.products_order[j].discount_quote_show = 0;
														$scope.products_order[j].price_after_promotion_discount = $scope.products_order[j].total_price_of_product_with_promotion;

														$scope.products_order[j].final_product_individual = parseInt($scope.products_order[j].price_after_promotion_discount);
														$scope.products_order[j].price_after_default_discount = 0;
														$scope.products_order[j].promotion = 1;
														$scope.products_order[j].discount = "";

													}

												}


											}//promo avai 1


											if ($scope.products_order[j].promotion_available == 0) {
												$scope.products_order[j].final_product_individual = $scope.products_order[j].selling_price * ($scope.products_order[j].inventory_moq);
												$scope.products_order[j].discount_quote_show = 0;
												$scope.products_order[j].promotion_quote_show = 0;
											}

											if ($scope.products_order[j].promotion_item != '' && $scope.products_order[j].promotion_available == 1) {

												$scope.getFreeInventory($scope.products_order[j].promotion_item, $scope.products_order[j].inventory_id, $scope.products_order[j].promotion_item_num, $scope.products_order[j].quotient, $scope.products_order[j].show_addon_items, j, $scope.products_order[j].promotion_minimum_quantity, $scope.products_order[j].inventory_moq);


											} else {
												if (document.getElementById('total_num_of_free_items') != null) {
													document.getElementById('total_num_of_free_items').innerHTML = "";
												}
											}

											if ($scope.products_order[j].territory_duration != -1) {
												var months = new Array("January", "February", "March", "April", "May", "June", "July", "August", "Sep", "October", "November", "December");
												var days = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Fri", "Saturday");

												var d = new Date();
												duration = parseInt($scope.products_order[j].territory_duration);
												d.setDate(d.getDate() + duration);
												$scope.products_order[j].delivery_date = days[d.getDay()] + " " + months[d.getMonth()] + " " + d.getDate() + "," + d.getFullYear();
											} else {
												$scope.products_order[j].delivery_date = "Not applied";
											}

											if ($scope.products_order[j].inventory_weight_in_kg_for_unit != -1) {
												$scope.products_order[j].inventory_weight_in_kg = parseInt($scope.products_order[j].inventory_moq) * parseFloat($scope.products_order[j].inventory_weight_in_kg_for_unit);
											}



											$scope.products_order[j].inventory_selling_price_without_discount = (($scope.products_order[j].selling_price) * ($scope.products_order[j].inventory_moq));

											$scope.products_order[j].inventory_selling_price_with_discount = ($scope.products_order[j].inventory_selling_price_without_discount);



											/*coupon calculation*/
											if ($scope.products_order[j].coupon_applied_status == 1) {

												coupon_reduce_price = $scope.products_order[j].coupon_reduce_price;

												if ($scope.products_order[j].promotion_available == 0) {
													//$scope.products_order[j].inventory_selling_price_without_discount=parseInt($scope.products_order[j].inventory_selling_price_without_discount-coupon_reduce_price);
													$scope.products_order[j].promo_updated_coupon_price = parseInt($scope.products_order[j].inventory_selling_price_without_discount - coupon_reduce_price);

												}
												if ($scope.products_order[j].promotion_available == 1) {
													prc = 0;
													if ($scope.products_order[j].promotion_quote_show == 1) {
														if ($scope.products_order[j].quotient > 0) {
															prc += parseInt($scope.products_order[j].price_after_promotion_discount);
															//$scope.products_order[j].price_after_promotion_discount=parseInt($scope.products_order[j].price_after_promotion_discount-coupon_reduce_price);
														}
													}
													if ($scope.products_order[j].discount_quote_show == 1) {
														prc += parseInt($scope.products_order[j].price_after_default_discount);
													}

													$scope.products_order[j].promo_updated_coupon_price = parseInt(prc - coupon_reduce_price);
												} else {
													$scope.products_order[j].promo_updated_coupon_price = 0;
												}

												if ($scope.products_order[j].final_product_individual) {
													$scope.products_order[j].final_product_individual = parseInt($scope.products_order[j].final_product_individual - coupon_reduce_price);
												}
												$scope.total_coupon_used_amount += parseInt(coupon_reduce_price);
											}
											/*coupon calculation*/


											if (parseInt($scope.products_order[j].shipping_charge) != parseInt(-1)) {

												$scope.products_order[j].subtotal = parseInt($scope.products_order[j].inventory_selling_price_without_discount) + parseInt($scope.products_order[j].shipping_charge);
												$scope.products_order[j].shipping_charge_msg = 0;
												$scope.products_order[j].shipping_charge_view = 1;
												if ($scope.products_order[j].shipping_charge == 0) {
													$scope.products_order[j].msg_style = "color:green;";
													$scope.products_order[j].delivery_service_msg = 'Free Delivery service';
												} else {
													//alert($scope.products_order[j].shipping_charge);
													$scope.products_order[j].msg_style = "color:#333;";
													$scope.products_order[j].delivery_service_msg = 'Service is available';
												}


											} else {
												$scope.products_order[j].subtotal = parseInt($scope.products_order[j].inventory_selling_price_without_discount);
												$scope.products_order[j].shipping_charge_msg = "Not applied";
												$scope.products_order[j].shipping_charge_view = 0;
												$scope.products_order[j].msg_style = "color:red;";
												$scope.products_order[j].delivery_service_msg = 'Service is not available';
											}

											if ($scope.products_order[j].final_product_individual) {
												if (parseInt($scope.products_order[j].shipping_charge) != parseInt(-1)) {
													$scope.products_order[j].subtotal = parseInt($scope.products_order[j].final_product_individual) + parseInt($scope.products_order[j].shipping_charge);
													total_shipping_price += parseFloat($scope.products_order[j].shipping_charge);

												} else {
													$scope.products_order[j].subtotal = parseInt($scope.products_order[j].final_product_individual);
													//total_shipping_price+=parseFloat($scope.products_order[j].shipping_charge);
												}
											}


											default_delivery_mode_id = $scope.products_order[j].default_delivery_mode_id;

											if ($scope.products_order[j].delivery_modes !== undefined) {
												for (x = 0; x < $scope.products_order[j].delivery_modes.length; x++) {

													logistics_delivery_mode_id = $scope.products_order[j].delivery_modes[x].logistics_delivery_mode_id;
													if (logistics_delivery_mode_id == default_delivery_mode_id) {
														$scope.products_order[j].delivery_modes[x].checked = "true";

													} else {
														$scope.products_order[j].delivery_modes[x].checked = "false";
													}
												}
											}

											////////////////////////////
											for (x in $scope.products_order) {
												if ($scope.products_order[x].cart_id == cart_id) {
													if ($scope.products_order[x].get_delivery_mode_and_parcel_category_combinations_arr !== undefined) {


														if ($scope.products_order[x].get_delivery_mode_and_parcel_category_combinations_arr[default_delivery_mode_id].length == 1) {
															$scope.products_order[x].get_delivery_mode_and_parcel_category_combinations_arr[default_delivery_mode_id][0].checked = true;
														}
														else {

															for (dx = 0; dx < $scope.products_order[x].get_delivery_mode_and_parcel_category_combinations_arr[default_delivery_mode_id].length; dx++) {
																if ($scope.products_order[x].get_delivery_mode_and_parcel_category_combinations_arr[default_delivery_mode_id][dx]["default_parcel_category"] == 1) {
																	$scope.products_order[x].get_delivery_mode_and_parcel_category_combinations_arr[default_delivery_mode_id][dx].checked = true;
																}
																else {
																	$scope.products_order[x].get_delivery_mode_and_parcel_category_combinations_arr[default_delivery_mode_id][dx].checked = false;
																}
															}



														}
														$scope.products_order[x].parcel_categories = $scope.products_order[x].get_delivery_mode_and_parcel_category_combinations_arr[default_delivery_mode_id];

													}

												}
											}
											///////////////////////////


											logistics_parcel_category_id = $scope.products_order[j].logistics_parcel_category_id;
											if ($scope.products_order[j].parcel_categories !== undefined) {

												for (y = 0; y < $scope.products_order[j].parcel_categories.length; y++) {
													logistics_parcel_category_id_each = $scope.products_order[j].parcel_categories[y].logistics_parcel_category_id;

													if (logistics_parcel_category_id == logistics_parcel_category_id_each) {
														$scope.products_order[j].parcel_categories[y].checked = "true";
													} else {
														$scope.products_order[j].parcel_categories[y].checked = "false";
													}
												}

											}


											if ($scope.products_order[j].promotion_cashback > 0) {

												$scope.show_cash_back_tr = 1;

												cash_back_amount = parseFloat($scope.products_order[j].cash_back_value);

												$scope.total_cash_back_for_sku += parseFloat(cash_back_amount);

												$scope.cash_back_tr_arr.push({
													"promotion_cashback_percentage": $scope.products_order[j].promotion_cashback,
													"product_name": $scope.products_order[j].product_name,
													"inventory_sku": $scope.products_order[j].inventory_sku,
													"cash_back_amount": cash_back_amount,
													"sku_count": $scope.products_order[j].inventory_moq
												});

											}

											if ($scope.products_order[j].addon_products_status == '1') {
												var response = $scope.products_order[j].addon_products;
												//console.log('ddddddddd');
												//console.log(typeof response);
												if (typeof response == 'string') {
													rr = $scope.products_order[j].addon_products;
													console.log(rr.charAt(0));
													if (rr.charAt(0) == "\"") {
														rr = rr.replace(/(^"|"$)/g, '');
														$scope.products_order[j].addon_products = JSON.parse(rr);
													} else {
														$scope.products_order[j].addon_products = JSON.parse(rr);
													}
												} else {
													$scope.products_order[j].addon_products = $scope.products_order[j].addon_products;
												}

												$scope.products_order[j].addon_count_updated = 1;

												$scope.products_order[j].addon_count = $scope.products_order[j].addon_products.length;
												//$scope.show_addon_products(j,$scope.products_order[j].addon_inventories,$scope.products_order[j].inventory_id);

												var addon_data = $scope.products_order[j].addon_products;
												var tot = 0; var ship = 0;
												for (u = 0; u < (addon_data.length); u++) {
													tot += parseInt(addon_data[u].inv_price);
													ship += parseInt(addon_data[u].inv_ship_price);
												}
												$scope.products_order[j].addon_total_price = tot;
												$scope.products_order[j].addon_total_shipping_price = ship;
												$scope.products_order[j].subtotal += tot;
												$scope.inv_addon_products_amount += parseFloat(tot);
												$scope.total_addon_products += tot;
											}

											/* addon products */

										} else {

											$scope.products_order[j].item_stock_available = 0;
											$scope.products_order[j].subtotal = 0;

											$scope.out_of_stock_item_in_cart.push(1);
										}//checking out of stock



										/* addon shipping price */

										if (parseInt($scope.products_order[j].addon_total_shipping_price) > 0) {

											$scope.products_order[j].shipping_charge_total = (parseInt($scope.products_order[j].shipping_charge) + parseInt($scope.products_order[j].addon_total_shipping_price));
											//alert($scope.products_order[j].shipping_charge);

											$scope.products_order[j].subtotal += parseInt($scope.products_order[j].addon_total_shipping_price);
											//$scope.products_order[j].final_product_individual+=parseInt($scope.products_order[j].addon_total_shipping_price);
											$scope.total_shipping_price += parseInt($scope.products_order[j].addon_total_shipping_price);
											$scope.total_shipping_price_addon += parseInt($scope.products_order[j].addon_total_shipping_price);
										} else {
											$scope.products_order[j].shipping_charge_total = parseInt($scope.products_order[j].shipping_charge);
										}

										total_netprice += parseFloat($scope.products_order[j].subtotal);

										individual_price_without_shipping += parseInt($scope.products_order[j].final_product_individual);



									}//forloop


									//alert($scope.out_of_stock_item_in_cart+"outside");

									if ($scope.out_of_stock_item_in_cart.length > 0) {
										$scope.out_of_stock_item_exists = 1;
										$("#tabtoordersumm_btn").hide();
										//$('#payment').addClass('disabled');
										//$('#payment').find('a').removeAttr("data-toggle");

									} else {
										//$('#payment').addClass('disabled');
										//$('#payment').find('a').removeAttr("data-toggle");
										$scope.out_of_stock_item_exists = 0;
										availability_pin = $scope.logistics_service_availability.length;
										if (availability_pin == 0) {
											$("#tabtoordersumm_btn").show();
										} else {
											//service is not available
											$("#tabtoordersumm_btn").hide();
										}
									}

									if ($scope.cash_back_tr_arr.length > 0) {
										$scope.show_cash_back_tr = 1;
									} else {
										$scope.show_cash_back_tr = 0;
									}

									if (parseFloat($scope.total_addon_products) > 0) {
										individual_price_without_shipping += parseFloat($scope.total_addon_products);
									}

									$scope.total_price_without_shipping_price = parseInt(individual_price_without_shipping);

									$scope.grandtotal_without_discount = total_netprice;
									$scope.grandTotal = total_netprice;
									$scope.eligible_discount_percentage_display = 0;

									$scope.total_shipping_price = (parseInt(total_shipping_price) + parseInt($scope.total_shipping_price_addon));

									$scope.grandTotal_for_discount = 0;
									$scope.total_shipping_price_actual = 0;

									if (c_id != null && c_id == '') {
										localStorage.setItem("LocalCustomerCart", JSON.stringify($scope.products_order));
									}

<?php
$price_list = array();
if (!empty($promotions_cart)) {

	?>

											$scope.eligible_cash_back_percentage_display=0;
										$scope.eligible_discount_percentage_display = 0;
										$scope.eligible_free_shipping_display = 0;
										$scope.eligible_free_surprise_gift_display = 0;
										$scope.eligible_cash_back_percentage_display = 0;
										$scope.discount_oninvoice = 0;
										$scope.cash_back_oninvoice = 0;
										$scope.free_shipping = 0;
										$scope.surprise_gift = 0;
										$scope.show_surprise_gift_on_invoice = 0;
										$scope.invoice_surprise_gift = 0;
										$scope.eligible_surprise_gift_on_invoice_uid = 0;
										$scope.promotion_invoice_cashback_quote = '';

										$scope.promotions_all = [];
<?php
		//$cart_promo_arr=array();
		foreach ($promotions_cart as $data) {


			$buy_type_arr = explode(',', $data['buy_type']);
			if (count($buy_type_arr) == 1) {
				if (preg_match('/surprise/', strtolower($data["promo_quote"]))) {
					array_push($price_list, $data);
				}
			}

			?>
												$scope.promotions_cart =[];
											var promotion_id = '<?php echo $data['promo_uid']; ?>';
											var promo_to_buy = '<?php echo $data['to_buy']; ?>';
											var promo_to_get = '<?php echo $data['to_get']; ?>';
											var promo_buy_type = '<?php echo $data['buy_type']; ?>';

											var promo_get_type = '<?php echo $data['get_type']; ?>';
											var promo_name = '<?php echo $data['promo_name']; ?>';

											//var promo_quote='<?php echo $data['promo_quote']; ?>';	
											var promo_quote = '<?php echo rtrim($data['promo_quote'], '&lt;br&gt;'); ?>';
											var applied_at_invoice = '<?php echo $data['applied_at_invoice']; ?>';

											if (applied_at_invoice == 1) {
												$scope.applied_at_invoice_arr.push(applied_at_invoice);
											}

											$scope.promotion_id_arr = promotion_id.split(',');
											$scope.promo_to_buy_arr = promo_to_buy.split(',');
											$scope.promo_to_get_arr = promo_to_get.split(',');
											$scope.promo_buy_type_arr = promo_buy_type.split(',');
											$scope.promo_get_type_arr = promo_get_type.split(',');
											$scope.promo_name_arr = promo_name.split(',');
											$scope.promo_quote_arr = promo_quote.split('&lt;br&gt;');

											$scope.promotions_cart.push({
												'promotion_id_arr': $scope.promotion_id_arr,
												'promo_to_buy_arr': $scope.promo_to_buy_arr,
												'promo_to_get_arr': $scope.promo_to_get_arr,
												'promo_buy_type_arr': $scope.promo_buy_type_arr,
												'promo_get_type_arr': $scope.promo_get_type_arr,
												'promo_name_arr': $scope.promo_name_arr,
												'promo_quote_arr': $scope.promo_quote_arr
											});

											for (var t = 0; t < $scope.promotions_cart.length; t++) {

												promo_to_buy_arr = $scope.promotions_cart[t].promo_to_buy_arr;
												promo_to_get_arr = $scope.promotions_cart[t].promo_to_get_arr;
												promo_buy_type_arr = $scope.promotions_cart[t].promo_buy_type_arr;
												promo_get_type_arr = $scope.promotions_cart[t].promo_get_type_arr;
												promo_name_arr = $scope.promotions_cart[t].promo_name_arr;
												promo_quote_arr = $scope.promotions_cart[t].promo_quote_arr;

												for (var v = 0; v < promo_to_buy_arr.length; v++) {

													to_buy = promo_to_buy_arr[v];
													to_buy_previous = promo_to_get_arr[v - 1];
													to_get = promo_to_get_arr[v];
													to_get_previous = promo_to_get_arr[v - 1];
													buy_type = promo_buy_type_arr[v];
													get_type = promo_get_type_arr[v];
													promo_name = promo_name_arr[v];
													promo_quote = promo_quote_arr[v];
													promo_quote_prevoius = promo_quote_arr[v - 1];

													if ((to_get == '') && (get_type == '')) {

														if ($scope.total_shipping_price == 0) {
															$scope.free_shipping = 'Search the pincode to get <strong>Free shipping</strong> .';
															//$scope.explicitlyTrustedHtml_free_shipping = $sce.trustAsHtml($scope.free_shipping);
															$scope.eligible_free_shipping_display = 0;
															$scope.free_shipping = 1;
														} else {

															$scope.currency_type_free_shipping = buy_type;
															$scope.eligible_free_shipping_display = 0;
															$scope.free_shipping = 0;
															if (parseInt(to_buy) > parseInt(individual_price_without_shipping)) {

																$scope.total_shipping_price_actual = 0;
																$scope.free_shipping = 'Expend more ' + parseInt(to_buy - individual_price_without_shipping) + ' ' + buy_type + ' to get <strong>Free shipping</strong> .';
																//$scope.explicitlyTrustedHtml_free_shipping = $sce.trustAsHtml($scope.free_shipping);
																$scope.eligible_free_shipping_display = 0;
																$scope.free_shipping = 1;

															} else {
																//$scope.explicitlyTrustedHtml_free_shipping = $sce.trustAsHtml('');
																$scope.eligible_free_shipping_display = 1;
																$scope.total_shipping_price_actual = $scope.total_shipping_price;
																$scope.free_shipping = 0;
															}
														}
													}


													if (get_type == "discount") {

														$scope.discount_oninvoice = 0;
														if (parseInt(to_buy) > individual_price_without_shipping) {
															if (to_get_previous > 0) {
																$scope.grandTotal_for_discount = Math.round((to_get_previous / 100) * individual_price_without_shipping);
																$scope.eligible_discount_percentage_display = 1;
																$scope.eligible_discount_percentage = to_get_previous;
																$scope.currency_type_discount = buy_type;
															} else {
																$scope.eligible_discount_percentage_display = 0;
																//$scope.explicitlyTrustedHtml_discount = $sce.trustAsHtml('');
															}
															if (individual_price_without_shipping < to_buy) {
																$scope.discount_oninvoice = 1;
																$scope.discount_oninvoice = 'Expend more ' + parseInt(to_buy - parseInt(individual_price_without_shipping)) + ' ' + buy_type + ' to get <strong>' + to_get + '% ' + get_type + ' on INVOICE</strong> .';
																//$scope.explicitlyTrustedHtml_discount = $sce.trustAsHtml($scope.discount_oninvoice);
															} else {
																$scope.discount_oninvoice = 0;
																//$scope.explicitlyTrustedHtml_discount = $sce.trustAsHtml("");
															}
															break;
														} else {
															if (to_get > 0) {


																$scope.grandTotal_for_discount = Math.round((to_get / 100) * individual_price_without_shipping)
																$scope.eligible_discount_percentage_display = 1;
																$scope.eligible_discount_percentage = to_get;
																$scope.currency_type_discount = buy_type;


															} else {
																$scope.eligible_discount_percentage_display = 0;

															}

														}
													}
													//$scope.grandTotal_for_cash_back=0;


													if (get_type == "cash back") {

														$scope.grandTotal_for_cash_back = 0;
														$scope.cash_back_oninvoice = 0;


														if (parseInt(to_buy) > individual_price_without_shipping) {

															if (to_get_previous > 0) {

																$scope.grandTotal_for_cash_back = parseFloat((to_get_previous / 100) * individual_price_without_shipping).toFixed(2);

																$scope.eligible_cash_back_percentage_display = 1;

																$scope.promotion_invoice_cashback_quote = promo_quote_prevoius;
																$scope.eligible_cash_back_percentage = to_get_previous;
																$scope.currency_type_cash_back = buy_type;

															} else {

																$scope.eligible_cash_back_percentage_display = 0;
																//$scope.explicitlyTrustedHtml_cash_back = $sce.trustAsHtml('');

															}
															if (individual_price_without_shipping < to_buy) {

																$scope.cash_back_oninvoice = 1;
																$scope.cash_back_oninvoice = 'Expend more ' + parseInt(to_buy - parseInt(individual_price_without_shipping)) + ' ' + buy_type + ' to get <strong>' + to_get + '% ' + get_type + ' on INVOICE</strong> .';
																//$scope.explicitlyTrustedHtml_cash_back = $sce.trustAsHtml($scope.cash_back_oninvoice);

															} else {
																$scope.cash_back_oninvoice = 0;
																//$scope.explicitlyTrustedHtml_cash_back = $sce.trustAsHtml("");
															}
															break;
														} else {
															if (to_get > 0) {


																$scope.grandTotal_for_cash_back = parseFloat((to_get / 100) * individual_price_without_shipping).toFixed(2);
																$scope.eligible_cash_back_percentage_display = 1;
																$scope.eligible_cash_back_percentage = to_get;
																$scope.promotion_invoice_cashback_quote = promo_quote;
																$scope.currency_type_cash_back = buy_type;


															} else {
																$scope.eligible_cash_back_percentage_display = 0;

															}

														}
													}

												}

											}

<?php
		}

		$m = array();
		foreach ($price_list as $key => $row) {
			$m[$key] = $row['to_buy'];
		}
		array_multisort($m, SORT_ASC, $price_list);
} ?>

										$scope.applied_at_invoice_arr=<?php echo json_encode($price_list); ?>;
									//alert(applied_at_invoice_arr);

									//alert($scope.promotion_invoice_cashback_quote);


									if ($scope.applied_at_invoice_arr.length > 0) {

										$scope.show_surprise_gift_on_invoice = 1;

										for (h = 0; h < $scope.applied_at_invoice_arr.length; h++) {

											if (parseInt($scope.applied_at_invoice_arr[h].to_buy) > parseInt(individual_price_without_shipping)) {
												//for top alert	
												expand_more = parseInt($scope.applied_at_invoice_arr[h].to_buy) - parseInt(individual_price_without_shipping);

												$scope.invoice_surprise = ' To eligible for (' + $scope.applied_at_invoice_arr[h].promo_quote + ') expend more ' + expand_more + ' ' + $scope.applied_at_invoice_arr[h].buy_type;
												//$scope.explicitlyTrustedHtml_invoice_surprise_gift = $sce.trustAsHtml($scope.invoice_surprise);
												$scope.invoice_surprise_gift = 1;

												//for bottom messsage
												//explicitlyTrustedHtml_invoice_surprise_gift_result
												if ($scope.applied_at_invoice_arr[h - 1] != null) {
													$scope.invoice_surprise_result = $scope.applied_at_invoice_arr[h - 1].promo_quote;
													$scope.explicitlyTrustedHtml_invoice_surprise_gift_result = $sce.trustAsHtml($scope.invoice_surprise_result);
													$scope.invoice_surprise_gift_result = 1;
													$scope.applies_surprise_gift_on_invoice = 1;
													$scope.eligible_surprise_gift_on_invoice_uid = $scope.applied_at_invoice_arr[h - 1].promo_uid;
												} else {
													$scope.explicitlyTrustedHtml_invoice_surprise_gift_result = $sce.trustAsHtml("");
													$scope.invoice_surprise_gift_result = 0;
													$scope.applies_surprise_gift_on_invoice = 0;

												}
												break;

											} else {
												$scope.applies_surprise_gift_on_invoice = 1;

												//$scope.explicitlyTrustedHtml_invoice_surprise_gift = $sce.trustAsHtml("");
												$scope.invoice_surprise_gift = 0;

												$scope.invoice_surprise_result = $scope.applied_at_invoice_arr[h].promo_quote;
												$scope.explicitlyTrustedHtml_invoice_surprise_gift_result = $sce.trustAsHtml($scope.invoice_surprise_result);

												$scope.eligible_surprise_gift_on_invoice_uid = $scope.applied_at_invoice_arr[h].promo_uid;
												$scope.invoice_surprise_gift_result = 1;
											}
										}

									} else {
										$scope.show_surprise_gift_on_invoice = 0;
									}

									if ($scope.eligible_discount_percentage_display == 1 || $scope.eligible_free_shipping_display == 1) {
										if ($scope.total_shipping_price_actual) {
											$scope.grandTotal_after_deduction = (parseInt($scope.grandtotal_without_discount) - parseInt($scope.grandTotal_for_discount) - parseInt($scope.total_shipping_price_actual));
											$scope.amount_to_pay = $scope.grandTotal_after_deduction;
										}
										else {
											$scope.amount_to_pay = parseInt($scope.grandtotal_without_discount) - parseInt($scope.grandTotal_for_discount);
										}

									} else {
										$scope.amount_to_pay = parseInt($scope.grandTotal);
									}

									//alert($scope.total_shipping_price);
									//alert($scope.total_cash_back);

									if ($scope.eligible_discount_percentage_display == 1 || $scope.eligible_free_shipping_display == 1) {

										if (parseInt($scope.total_shipping_price_actual)) {
											$scope.you_saved = parseInt($scope.grandTotal_for_discount) + parseInt($scope.total_shipping_price_actual);
										} else {
											$scope.you_saved = parseInt($scope.grandTotal_for_discount)
										}

									} else {
										$scope.you_saved = 0;
									}

									$scope.amount_without_invoice_coupon = 0;

									if (parseInt($scope.invoice_coupon_applied_status) == 1) {

										if ($scope.eligible_discount_percentage_display == 1) {
											$scope.amount_without_invoice_coupon = $scope.grandtotal_without_discount;
											$scope.grandtotal_without_discount = parseInt($scope.grandtotal_without_discount - $scope.invoice_coupon_reduce_price);
										}
										if ($scope.eligible_discount_percentage_display == 0) {
											$scope.amount_without_invoice_coupon = $scope.amount_to_pay;
											$scope.amount_to_pay = parseInt($scope.amount_to_pay - $scope.invoice_coupon_reduce_price);

										}
										$scope.total_coupon_used_amount += parseInt($scope.invoice_coupon_reduce_price);
									} else {
										if ($scope.eligible_discount_percentage_display == 1) {
											$scope.amount_without_invoice_coupon = $scope.grandtotal_without_discount;
										}
										if ($scope.eligible_discount_percentage_display == 0) {
											$scope.amount_without_invoice_coupon = $scope.amount_to_pay;
										}
									}

									if ($scope.total_coupon_used_amount > 0) {
										$scope.you_saved += parseInt($scope.total_coupon_used_amount);
									}

									if ($scope.show_cash_back_tr == 1) {

										if ($scope.grandTotal_for_cash_back == null) {
											$scope.grandTotal_for_cash_back = 0;
										}
										if ($scope.total_cash_back_for_sku == null) {
											$scope.total_cash_back_for_sku = 0;
										}
										$scope.total_cash_back = parseFloat($scope.grandTotal_for_cash_back) + parseFloat($scope.total_cash_back_for_sku);
										$scope.total_cash_back = $scope.total_cash_back.toFixed(2);
									} else {

										//alert($scope.grandTotal_for_cash_back);

										if (parseInt($scope.grandTotal_for_cash_back) > 0) {
											$scope.total_cash_back = parseFloat($scope.grandTotal_for_cash_back);
										} else {
											$scope.total_cash_back = 0;
											$scope.grandTotal_for_cash_back = 0;
										}
									}


									$scope.amount_payable_fun($scope.grandtotal_without_discount, $scope.grandTotal_for_discount, $scope.total_shipping_price_actual, $scope.grandTotal_for_discount, $scope.you_saved, $scope.grandTotal_for_cash_back, $scope.total_cash_back_for_sku, $scope.total_cash_back, $scope.amount_to_pay, $scope.eligible_surprise_gift_on_invoice_uid, $scope.applies_surprise_gift_on_invoice, $scope.promotion_invoice_cashback_quote, $scope.total_price_without_shipping_price, $scope.total_shipping_price, $scope.total_coupon_used_amount, $scope.products_coupon, $scope.invoice_coupon, $scope.invoice_coupon_applied_status, $scope.inv_addon_products_amount);

									$scope.payment_method_restrictions_fun(payment_method_restrictions, payment_method_cod_max_value);


								}
								$scope.get_promotion_active_status = function (promo_id_selected) {


									//alert(promo_id_selected+"id selected");
									//if promotion available check active and expired status

									//promo_data='<?php echo 'get_promotion_active_status('; ?>'+promo_id_selected+'<?php echo ")"; ?>';

									$http({
										method: "POST",
										url: "<?php echo base_url(); ?>get_promotion_active_status",
										data: { 'promo_id_selected': promo_id_selected },
										dataType: "JSON",
										async: false
									}).success(function mySucces(result) {
										if (jQuery.isEmptyObject(result)) {
											//alert('promo_inactive');
											//return 2;//promo inactive or expired
											$scope.promo_temp = 2;
										} else {
											//alert('promo_active');
											//return 1;//promo active
											$scope.promo_temp = 1;
										}
									});
								}
								$scope.get_promotion_availability = function (promo_id_selected, inv_id, j) {


									//alert(promo_id_selected+"id selected");
									//if promotion available check active and expired status

									//promo_data='<?php echo 'get_promotion_active_status('; ?>'+promo_id_selected+'<?php echo ")"; ?>';

									$http({
										method: "POST",
										url: "<?php echo base_url(); ?>get_promo_availability",
										data: { 'promo_id_selected': promo_id_selected, 'inv_id': inv_id },
										dataType: "JSON",
										async: false
									}).success(function mySucces(result) {
										if (jQuery.isEmptyObject(result)) {
											//alert('promo_inactive');
											//return 2;//promo inactive or expired
											$scope.new_promo_avail = 0;
											$scope.products_order[j].new_promo_avail = 0;
										} else {
											//alert('promo_active');
											//return 1;//promo active
											$scope.new_promo_avail = 1;
											$scope.products_order[j].new_promo_avail = 1;

										}
									});
								}

								$scope.avail_new_promotion = function (inventory_id, x) {
									swal({
										title: "",
										html: "This product contains new discout or offer(s)! You can check the offers and add the product to cart again."
										,
										type: "info",
										showCancelButton: true,
										showConfirmButton: true,
										confirmButtonText: 'Yes, take me to offers',
									}).then(function (res) {
										if (res == true) {
											$scope.removeItem(x);
											location.href = "<?php echo base_url() ?>detail/<?php echo $controller->sample_code(); ?>" + inventory_id;
										}
									});
								}
								$scope.getFreeInventory = function (obj, inv, num, quotient, addons_show, i, promotion_minimum_quantity, inventory_moq) {

									$http({
										method: "POST",
										url: "<?php echo base_url(); ?>get_free_inventory_items_in_promotions",
										data: { 'free_inventory': obj, 'inv': inv },
										dataType: "JSON",
										async: false
									}).success(function mySucces(result) {

										if (document.getElementById('extra_data_' + i) != null) {
											//alert('extra_data_'+i);
											document.getElementById('extra_data_' + i).innerHTML = result;

										}

										var total_num = 0;

										var free_nums_for_invs = num.split(',')
										var promotion_item_free = obj.split(',')
										//alert(free_nums_for_invs.length);			
										for (var k = 0; k < free_nums_for_invs.length; k++) {
											inv = parseInt(inv);
											quotient_q = 0;
											if (document.getElementById("free_inv_nums_" + promotion_item_free[k] + "_" + inv) != null) {

												quotient_q = inventory_moq / promotion_minimum_quantity
												quotient_q = Math.floor(quotient_q)
												document.getElementById("free_inv_nums_" + promotion_item_free[k] + "_" + inv).innerHTML = quotient_q * free_nums_for_invs[k];

												document.getElementById("free_inv_nums_quantity_show_frontend_strike_" + promotion_item_free[k] + "_" + inv).innerHTML = quotient_q * free_nums_for_invs[k];

												document.getElementById("free_inv_nums_quantity_show_frontend_strike_totalprice_" + promotion_item_free[k] + "_" + inv).innerHTML = '<?php echo curr_sym; ?>' + (Math.round((parseFloat(document.getElementById("free_inv_nums_quantity_show_frontend_strike_" + promotion_item_free[k] + "_" + inv).innerHTML) * parseFloat(document.getElementById("free_inv_nums_quantity_show_frontend_strike_sellingprice_" + promotion_item_free[k] + "_" + inv).innerHTML)) * 100) / 100);


											}
											//alert(("free_inv_nums_"+promotion_item_free[k]+"_"+inv))
											total_num += quotient_q * free_nums_for_invs[k];
										}


										if (document.getElementById('total_num_of_free_items') != null) {
											if (total_num > 0) {
												document.getElementById('total_num_of_free_items').innerHTML = " + " + total_num + " Free Items";
											} else {
												document.getElementById('total_num_of_free_items').innerHTML = "";
											}
										}

										if (total_num != null && total_num != 0) {
											$scope.calculate_free_items(total_num);
										}

										//	alert($scope.count_of_free_items);

									}, function myError(response) {

									});

								}

								$scope.calculate_free_items = function (total_num) {

									$scope.count_of_free_items.push(total_num);
									temp = 0
									for (c = 0; c < $scope.count_of_free_items.length; c++) {
										temp += parseInt($scope.count_of_free_items[c]);
									}
									$scope.final_count_free_items = temp;

								}

								$scope.amount_payable_fun = function (grandtotal_without_discount, grandTotal_for_discount, total_shipping_price_actual, grandTotal_for_discount, you_saved, grandTotal_for_cash_back, total_cash_back_for_sku, total_cash_back, amount_to_pay, eligible_surprise_gift_on_invoice_uid, applies_surprise_gift_on_invoice, promotion_invoice_cashback_quote, total_price_without_shipping_price, total_shipping_price, total_coupon_used_amount, products_coupon, invoice_coupon, invoice_coupon_applied_status, inv_addon_products_amount) {

									$scope.amount_payable = amount_to_pay;

									Scopes.get('PaymentController').amount_fun(grandtotal_without_discount, grandTotal_for_discount, total_shipping_price_actual, you_saved, grandTotal_for_cash_back, total_cash_back_for_sku, total_cash_back, amount_to_pay, eligible_surprise_gift_on_invoice_uid, applies_surprise_gift_on_invoice, promotion_invoice_cashback_quote, total_price_without_shipping_price, total_shipping_price, total_coupon_used_amount, products_coupon, invoice_coupon, invoice_coupon_applied_status, inv_addon_products_amount);
								}
								$scope.payment_method_restrictions_fun = function (payment_method_restrictions, payment_method_cod_max_value) {
									//$scope.amount_payable=payment_method_restrictions;
									Scopes.get('PaymentController').payment_method_restrictions_fun(payment_method_restrictions, payment_method_cod_max_value);
								}
								$scope.common = function () {
									$("#loading").fadeIn("slow");
									$http({
										method: "POST",
										url: "<?php echo base_url(); ?>Account/get_cartinfo_in_json_format_checkout",
										data: "1=2",
										dataType: "JSON",
										async: false,
									}).success(function mySucces(result) {
										$("#loading").fadeOut("slow");
										if (result.check_availability_at_lease_one == "no") {
											$scope.products_order = new Array();
											$scope.out_of_stock_item_exists = 1;
											$("#pin_availability_chk").html("<span style='margin-left:20px;'  class='logout_notice'><font color='red'>No address found. Shipping is not available.</font></span>");
											$("#tabtoordersumm_btn").hide();
										}
										else {
											$("#pin_availability_chk").html("");
											$("#tabtoordersumm_btn").show();

											if (result.check_availability == "yes") {
												$scope.pincode = result.pincode;
												
												if($scope.pincode!=localStorage.searched_pincode){
													$('#pincode_change_txt').show();
												}else{
													$('#pincode_change_txt').hide();
												}
												$scope.products_order = result.get_cartinfo_in_json_format_checkout;
												$scope.order_product_count = result.get_cartinfo_in_json_format_checkout.length;
												if ($scope.order_product_count > 0) {
													$("#available_pincode_status_div").show();
													$("#available_pincode_div").hide();
													$scope.get_corresponding_shipping_charge($scope.order_product_count, $scope.products_order);
													$scope.searched_pincode_msg = "This Pincode has service";
												} else {
													$("#available_pincode_status_div").show();
													$("#available_pincode_div").hide();
													$scope.searched_pincode_msg = "No Service for this pincode";
													$scope.pincode_has_no_service();
												}
											}
											else {
												$("#available_pincode_status_div").show();
												$("#available_pincode_div").hide();

											}
										}

										//$scope.update_cart_controller_scopes($scope.order_product_count,result);	
									}, function myError(response) {

									});

								}

								$scope.check_availability_pincodeFun = function (order_product_count, products_order) {

									if ($scope.pincode != null && $scope.pincode != '') {

										$http({
											method: "POST",
											url: "<?php echo base_url(); ?>check_pincode_which_has_service",
											data: { 'pincode': $scope.pincode },
											dataType: "JSON",
											async: false
										}).success(function mySucces(result) {
											localStorage.searched_pincode = $scope.pincode;
											$scope.pincode_has_service = result;
											if (result == true) {
												$("#available_pincode_status_div").show();
												$("#available_pincode_div").hide();
												$scope.get_corresponding_shipping_charge(order_product_count, products_order);
												$scope.searched_pincode_msg = "This Pincode has service";
											} else {
												$("#available_pincode_status_div").show();
												$("#available_pincode_div").hide();
												$scope.searched_pincode_msg = "No Service for this pincode";
												$scope.pincode_has_no_service();
											}
										}, function myError(response) {

										});

									} else {
										localStorage.removeItem("searched_pincode");
										$scope.searched_pincode_msg = '';
										$scope.pincode_has_no_service();
									}

								}


								$scope.get_default_address_pincode_and_city = function () {
									$http({
										method: "POST",
										url: "<?php echo base_url(); ?>Account/get_default_address_pincode_and_city",
										data: "1=2",
										dataType: "JSON",
										async: false,
									}).success(function mySucces(result) {

										$scope.pincode = result.pincode;
										$scope.check_availability_pincodeFun($scope.order_product_count, $scope.products_order);

									}, function myError(response) {

									});
								}

								$scope.pincode_has_no_service = function () {
									for (v = 0; v < $scope.order_product_count; v++) {

										cart_id = $scope.products_order[v].cart_id;
										$scope.products_order[v].shipping_charge = -1;
										$scope.products_order[v].logistics_id = -1;
										$scope.products_order[v].logistics_parcel_category_id = -1;

										$scope.products_order[v].logistics_price = -1;
										$scope.products_order[v].default_delivery_mode_id = -1;
										$scope.products_order[v].logistics_territory_id = -1;
										$scope.products_order[v].territory_duration = -1;
										$scope.products_order[v].msg_style = "color:red;";
										$scope.products_order[v].delivery_service_msg = 'Service Not is available';
										$('#delivery_speed_block_' + cart_id).hide();
										$('#parcel_category_block_' + cart_id).hide();

									}

									var update_cart_info_post_data = $scope.products_order
									$scope.update_and_get_cartable_info_in_json_format_multiple(update_cart_info_post_data);

								}

								$scope.get_corresponding_shipping_charge = function (order_product_count, products_order) {
									$scope.products_order = products_order;
									$scope.order_product_count = order_product_count;
									data = { 'pincode': $scope.pincode, 'cart_data': $scope.products_order };

									$http({
										method: "POST",
										url: "<?php echo base_url(); ?>get_corresponding_shipping_charge",
										data: data,
										dataType: "JSON",
										async: false
									}).success(function mySucces(result) {
										for (u = 0; u < result.length; u++) {

											if (result[u].delivery_service == true) {

												$scope.products_order[u].shipping_charge = result[u].shipping_charge;
												$scope.products_order[u].delivery_service = result[u].delivery_service;
												$scope.products_order[u].logistics_id = result[u].logistics_id;
												$scope.products_order[u].logistics_territory_id = result[u].logistics_territory_id;
												$scope.products_order[u].default_delivery_mode_id = result[u].default_delivery_mode_id;
												$scope.products_order[u].logistics_parcel_category_id = result[u].logistics_parcel_category_id;

												$scope.products_order[u].logistics_price = result[u].logistics_price;
												$scope.products_order[u].territory_duration = result[u].territory_duration_org;
												$scope.products_order[u].delivery_service_msg = 'Service is available';
												$scope.products_order[u].msg_style = "color:#333;";
												localStorage.setItem("LocalCustomerCart", JSON.stringify($scope.products_order));
											}

											if (result[u].delivery_service == false) {
												$scope.products_order[u].shipping_charge = -1;
												$scope.products_order[u].delivery_service = false;
												$scope.products_order[u].logistics_id = -1;
												$scope.products_order[u].logistics_territory_id = -1;
												$scope.products_order[u].default_delivery_mode_id = -1;
												$scope.products_order[u].logistics_parcel_category_id = -1;

												$scope.products_order[u].logistics_price = -1;
												$scope.products_order[u].territory_duration = -1;

												$scope.products_order[u].msg_style = "color:red;";
												$scope.products_order[u].delivery_service_msg = 'Service is not available';
												$scope.products_order[u].shipping_charge_view = 0;
											}

											if (result[u].delivery_service == "free") {
												//alert('delivery charges free');
												$scope.products_order[u].shipping_charge = 0;
												$scope.products_order[u].delivery_service = "free";

												$scope.products_order[u].msg_style = "color:green;";
												$scope.products_order[u].delivery_service_msg = 'Free Delivery service';
											}


										}
										order_product_count = $scope.products_order.length;

										var update_cart_info_post_data = $scope.products_order;

										$scope.update_and_get_cartable_info_in_json_format_multiple(update_cart_info_post_data);

									}, function myError(response) {

									});

								}
								$scope.update_and_get_cartable_info_in_json_format_multiple = function (update_cart_info_post_data) {
									$http({
										method: "POST",
										url: "<?php echo base_url(); ?>Account/update_and_get_cartable_info_in_json_format_multiple_checkout",
										data: update_cart_info_post_data,
										dataType: "JSON",
									}).success(function mySucces(result) {

										$scope.products_order = result.get_cartinfo_in_json_format_checkout;

										$scope.order_product_count = $scope.products_order.length;
										$scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order);

									}, function myError(response) {

									});
								}

								$scope.apply_coupon_code = function (x, id, inventory_sku) {
									//alert(id)
									//alert($('#coupon_code_'+id).val());
									code = $('#coupon_code_' + id).val();
									if (code != '') {

										if ($scope.products_order[x].promotion_available == 0) {
											price = $scope.products_order[x].inventory_selling_price_without_discount;
										}

										if ($scope.products_order[x].promotion_available == 1) {
											price = 0;
											if ($scope.products_order[x].promotion_quote_show == 1) {
												if ($scope.products_order[x].quotient > 0) {
													price += parseInt($scope.products_order[x].price_after_promotion_discount);
												}
											}
											if ($scope.products_order[x].discount_quote_show == 1) {
												if ($scope.products_order[x].price_after_default_discount > 0) {
													price += parseInt($scope.products_order[x].price_after_default_discount);
												}
											}

										}


										data = { 'coupon_code': code, 'inventory_id': id, 'inventory_sku': inventory_sku };

										$http({
											method: "POST",
											url: "<?php echo base_url(); ?>Account/apply_coupon_code_sku",
											data: data,
											dataType: "JSON",
										}).success(function mySucces(result) {
											console.log("coupon result" + result);
											if (result.status == 1) {
												coupon_arr = result.coupon_arr;
												console.log(result.coupon_arr)
												type = coupon_arr.type;
												coupon_name = coupon_arr.coupon_name;
												value = coupon_arr.value;
												txt = '';


												$scope.products_order[x].coupon_strike_price = parseInt(price);
												$scope.products_order[x].coupon_code_appied = code;
												$scope.products_order[x].coupon_name_appied = coupon_name;
												$scope.products_order[x].coupon_type_appied = type;
												$scope.products_order[x].coupon_value_appied = value;
												new_price = price;
												reduce_price = 0;
												if (parseInt(type) == 2) {
													//flat
													if (parseInt(price) < parseInt(value)) {
														alert('Please purchase price should be greater than ' + curr_sym + price + ' to avail this coupon');
													} else {
														reduce_price = value;
														new_price = (parseInt(price) - parseInt(value));
														txt += 'Coupon : ' + coupon_name + ' ( ' + code + ' ) applied on the SKU ' + inventory_sku + ' value of flat ' + '<?php echo curr_code; ?>' + " " + value;
													}
												}
												if (parseInt(type) == 1) {
													//percentage
													reduce_price = Math.round(parseInt(price) * parseInt(value) / 100);
													new_price = Math.round(parseInt(price) - (parseInt(price) * parseInt(value) / 100));
													txt += 'Coupon : ' + coupon_name + ' ( ' + code + ' ) applied on the SKU ' + inventory_sku + ' value of ' + value + '%';
													//+reduce_price + "||" + new_price;

												}
												$scope.products_order[x].coupon_txt_appied = $sce.trustAsHtml(txt);

												$scope.products_order[x].coupon_reduce_price = reduce_price;
												$scope.products_order[x].coupon_applied_status = 1;

												$scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order);
												//$scope.update_and_get_cartable_info_in_json_format_multiple($scope.products_order);
												//alert(result.msg)
												$scope.products_coupon[x] = { "inventory_id": id, "coupon_applied_status": $scope.products_order[x].coupon_applied_status, "coupon_code_appied": $scope.products_order[x].coupon_code_appied, "coupon_name_appied": $scope.products_order[x].coupon_name_appied, "coupon_type_appied": $scope.products_order[x].coupon_type_appied, "coupon_value_appied": $scope.products_order[x].coupon_value_appied, "coupon_reduce_price": $scope.products_order[x].coupon_reduce_price };
											}
											if (result.status == 2) {

												$scope.products_order[x].coupon_strike_price = '';
												$scope.products_order[x].coupon_code_appied = '';
												$scope.products_order[x].coupon_name_appied = '';
												$scope.products_order[x].coupon_type_appied = '';
												$scope.products_order[x].coupon_applied_status = 0;
												$scope.products_order[x].coupon_txt_appied = '';
												$scope.products_order[x].coupon_reduce_price = 0;
												alert(result.msg);
												$scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order);
												//$scope.update_and_get_cartable_info_in_json_format_multiple($scope.products_order);
												$scope.products_coupon[x] = { "inventory_id": id, "coupon_applied_status": $scope.products_order[x].coupon_applied_status, "coupon_code_appied": $scope.products_order[x].coupon_code_appied, "coupon_name_appied": $scope.products_order[x].coupon_name_appied, "coupon_type_appied": $scope.products_order[x].coupon_type_appied, "coupon_value_appied": $scope.products_order[x].coupon_value_appied, "coupon_reduce_price": $scope.products_order[x].coupon_reduce_price };
											}
										}, function myError(response) {

										});
									} else {
										alert('Please enter coupon code');
									}

								}
								$scope.apply_coupon_code_invoice = function (btn = 0) {

									//alert(id)
									//alert($('#coupon_code_'+id).val());
									code = $('#coupon_code_invoice').val();
									//console.log('coupon called '+btn+"coupon code"+code+"||"+$scope.invoice_coupon_code_appied_txt)

									//invoice_coupon_code_appied_txt

									if ((code != '' && code != null && code != undefined) || ($scope.invoice_coupon_code_appied_txt != '')) {

										if (code == undefined && $scope.invoice_coupon_code_appied_txt != '') {
											code = $scope.invoice_coupon_code_appied_txt;
										}

										price = $scope.amount_without_invoice_coupon;

										/*if($scope.eligible_discount_percentage_display==1){
										price=$scope.grandtotal_without_discount;
										console.log("price1="+price);
										}
										if($scope.eligible_discount_percentage_display==0){
										price=$scope.amount_to_pay;
										console.log("price2="+price);
										}*/
										console.log("price=" + price);



										$('#coupon_loader').html('<i class="fa fa-spinner"></i> Processing');
										data = { 'coupon_code': code };

										$http({
											method: "POST",
											url: "<?php echo base_url(); ?>Account/apply_coupon_code_invoice",
											data: data,
											dataType: "JSON"
										}).success(function mySucces(result) {

											//console.log("invoice coupon result"+result);
											if (result.status == 1) {
												coupon_arr = result.coupon_arr;
												//console.log( result.coupon_arr)
												type = coupon_arr.type;
												coupon_name = coupon_arr.coupon_name;
												value = coupon_arr.value;
												tot_cart_val = coupon_arr.tot_cart_val;

												coupon_per_user = coupon_arr.coupon_per_user;
												coupon_used_count = coupon_arr.coupon_used_count;

												txt = '';

												new_price = price;
												reduce_price = 0;

												coupon_strike_price = parseInt(price);
												coupon_code_appied = code;
												coupon_name_appied = coupon_name;
												coupon_type_appied = type;
												coupon_value_appied = value;

												$('#coupon_loader').html('Apply');
												if (parseInt(coupon_per_user) <= parseInt(coupon_used_count)) {
													alert('Usage limit is crossed for this coupon.Please try another.');
													return false;
												} else {

													console.log("tot_cart_val" + tot_cart_val + "||" + price);

													if (parseInt(tot_cart_val) < parseInt(price)) {


														if (parseInt(type) == 2) {
															//flat
															console.log("totamount to pay" + price + "coupon value" + value);

															if (parseInt(price) < parseInt(value)) {
																alert('Purchase price should be greater than ' + $scope.curr_txt + " " + price + ' to avail this coupon');
																//return false;//need to remove this line
																//console.log($scope.invoice_coupon_applied_status);
																if (parseInt($scope.invoice_coupon_applied_status) == 1) {
																	$scope.remove_invoice_coupon();
																}
															} else {
																reduce_price = value;
																new_price = (parseInt(price) - parseInt(value));
																txt += 'Invoice Coupon : ' + coupon_name + ' ( ' + code + ' ) applied on the invoice value of flat ' + '<?php echo curr_code; ?>' + " " + value;
															}
														}
														if (parseInt(type) == 1) {
															//percentage
															reduce_price = Math.round(parseInt(price) * parseInt(value) / 100);
															new_price = Math.round(parseInt(price) - (parseInt(price) * parseInt(value) / 100));
															txt += 'Invoice Coupon : ' + coupon_name + ' ( ' + code + ' ) applied on the invoice value of ' + value + '%';

														}

														$scope.invoice_applied_price = price;
														coupon_txt_appied = $sce.trustAsHtml(txt);

														coupon_reduce_price = reduce_price;
														coupon_applied_status = 1;
														$scope.invoice_coupon_applied_txt = coupon_txt_appied;
														$scope.invoice_coupon_code_appied_txt = coupon_code_appied;

														$scope.invoice_coupon = { "invoice_coupon_applied_status": coupon_applied_status, "invoice_coupon_code_appied": coupon_code_appied, "invoice_coupon_name_appied": coupon_name_appied, "invoice_coupon_type_appied": coupon_type_appied, "invoice_coupon_value_appied": coupon_value_appied, "invoice_coupon_reduce_price": coupon_reduce_price };
														$scope.invoice_coupon_applied_status = coupon_applied_status;
														$scope.invoice_coupon_reduce_price = coupon_reduce_price;
														$scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order);
														alert(result.msg);

													} else {
														alert('Your cart payable amount should be greater than ' + $scope.curr_txt + " " + parseInt(tot_cart_val));
														if (parseInt($scope.invoice_coupon_applied_status) == 1) {
															$scope.remove_invoice_coupon();
														}
														return false;

													}
												}

											}
											if (result.status == 2) {

												coupon_strike_price = '';
												coupon_code_appied = '';
												coupon_name_appied = '';
												coupon_type_appied = '';
												coupon_applied_status = 0;
												coupon_txt_appied = '';
												coupon_reduce_price = 0;
												coupon_value_appied = '';
												$scope.invoice_applied_price = 0;
												alert(result.msg);
												//$scope.update_cart_controller_scopes($scope.order_product_count,$scope.products_order);
												//$scope.update_and_get_cartable_info_in_json_format_multiple($scope.products_order);
												$scope.invoice_coupon = { "invoice_coupon_applied_status": coupon_applied_status, "invoice_coupon_code_appied": coupon_code_appied, "invoice_coupon_name_appied": coupon_name_appied, "invoice_coupon_type_appied": coupon_type_appied, "invoice_coupon_value_appied": coupon_value_appied, "invoice_coupon_reduce_price": coupon_reduce_price };
												$scope.invoice_coupon_applied_status = 0;
												$scope.invoice_coupon_reduce_price = 0;
												$scope.invoice_coupon_applied_txt = coupon_code_appied;

												$scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order);
												$('#coupon_loader').html('Apply');
											}



										}, function myError(response) {

										});
									} else {
										if (btn == 1) {
											alert('Please enter coupon code');

										}
									}

								}

								$scope.remove_coupon = function (x, id, inventory_sku) {
									$scope.products_order[x].coupon_strike_price = '';
									$scope.products_order[x].coupon_code_appied = '';
									$scope.products_order[x].coupon_name_appied = '';
									$scope.products_order[x].coupon_type_appied = '';
									$scope.products_order[x].coupon_applied_status = 0;
									$scope.products_order[x].coupon_txt_appied = '';
									$scope.products_order[x].coupon_reduce_price = 0;
									alert('Coupon removed successfully');
									$scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order);
									//$scope.update_and_get_cartable_info_in_json_format_multiple($scope.products_order);

								}
								$scope.remove_invoice_coupon = function () {
									coupon_strike_price = '';
									coupon_code_appied = '';
									coupon_name_appied = '';
									coupon_type_appied = '';
									coupon_applied_status = 0;
									coupon_txt_appied = '';
									coupon_reduce_price = 0;
									coupon_value_appied = '';

									//$scope.update_cart_controller_scopes($scope.order_product_count,$scope.products_order);
									//$scope.update_and_get_cartable_info_in_json_format_multiple($scope.products_order);
									$scope.invoice_coupon = { "invoice_coupon_applied_status": coupon_applied_status, "invoice_coupon_code_appied": coupon_code_appied, "invoice_coupon_name_appied": coupon_name_appied, "invoice_coupon_type_appied": coupon_type_appied, "invoice_coupon_value_appied": coupon_value_appied, "invoice_coupon_reduce_price": coupon_reduce_price };
									$scope.invoice_coupon_applied_status = 0;
									$scope.invoice_coupon_reduce_price = 0;
									$scope.invoice_applied_price = 0;
									$scope.invoice_coupon_applied_txt = coupon_code_appied;
									alert('Invoice coupon removed successfully');
									$scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order);
									//$scope.update_and_get_cartable_info_in_json_format_multiple($scope.products_order);

								}
								$scope.show_availability_pincode_div_fun = function () {
									$("#available_pincode_div").css({ "display": "block" });
									$("#available_pincode_status_div").hide();
								}

								$scope.update_delivery_speed = function (products_order_index, delivery_mode_index, logistics_delivery_mode_id, cart_id) {
									default_logistics_parcel_category_id = angular.element("#handlingdefaultprocess_" + cart_id).val();
									if (default_logistics_parcel_category_id !== undefined) {
										$scope.products_order[products_order_index].logistics_parcel_category_id = default_logistics_parcel_category_id;
									}
									//alert($scope.products_order[products_order_index].logistics_parcel_category_id);
									update_cart_info_post_data = { 'logistics_delivery_mode_id': logistics_delivery_mode_id, 'cart_id': cart_id, 'cart_data': $scope.products_order[products_order_index] };

									$http({
										method: "POST",
										url: "<?php echo base_url(); ?>Account/update_delivery_speed_and_get_cartable_info_in_json_format_checkout",
										data: update_cart_info_post_data,
										dataType: "JSON",
									}).success(function mySucces(result) {
										//document.write(JSON.stringify(result));
										//return false;

										$scope.products_order = result.get_cartinfo_in_json_format_checkout;

										$scope.order_product_count = $scope.products_order.length;
										$scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order);


										for (x in $scope.products_order) {
											if ($scope.products_order[x].cart_id == cart_id) {
												if ($scope.products_order[x].get_delivery_mode_and_parcel_category_combinations_arr[logistics_delivery_mode_id].length == 1) {
													$scope.products_order[x].get_delivery_mode_and_parcel_category_combinations_arr[logistics_delivery_mode_id][0].checked = true;
												}
												else {
													for (dx = 0; dx < $scope.products_order[x].get_delivery_mode_and_parcel_category_combinations_arr[logistics_delivery_mode_id].length; dx++) {
														if ($scope.products_order[x].get_delivery_mode_and_parcel_category_combinations_arr[logistics_delivery_mode_id][dx]["default_parcel_category"] == 1) {
															$scope.products_order[x].get_delivery_mode_and_parcel_category_combinations_arr[logistics_delivery_mode_id][dx].checked = true;
														}
														else {
															$scope.products_order[x].get_delivery_mode_and_parcel_category_combinations_arr[logistics_delivery_mode_id][dx].checked = false;
														}
													}

												}
												$scope.products_order[x].parcel_categories = $scope.products_order[x].get_delivery_mode_and_parcel_category_combinations_arr[logistics_delivery_mode_id];
											}
										}

									}, function myError(response) {

									});

								}

								$scope.update_parcel_category = function (products_order_index, parcel_categories_index, logistics_parcel_category_id, cart_id) {

									update_cart_info_post_data = { 'logistics_parcel_category_id': logistics_parcel_category_id, 'cart_id': cart_id, 'cart_data': $scope.products_order[products_order_index] };

									$http({
										method: "POST",
										url: "<?php echo base_url(); ?>Account/update_parcel_category_and_get_cartable_info_in_json_format_checkout",
										data: update_cart_info_post_data,
										dataType: "JSON",
									}).success(function mySucces(result) {
										//return false;

										$scope.products_order = result.get_cartinfo_in_json_format_checkout;
										$scope.order_product_count = $scope.products_order.length;
										$scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order);

									}, function myError(response) {

									});

								}

								$scope.increment = function (x) {

									if (parseInt($scope.products_order[x].inventory_moq) >= parseInt($scope.products_order[x].inventory_max_oq)) {
										alert('This value is not allowed. Maximum order Quantity is ' + $scope.products_order[x].inventory_moq);
										/*bootbox.alert({
										size: "small",
										message: 'This value is not allowed. Maximum order Quantity is '+$scope.products_order[x].inventory_moq,
										});*/
										return;
									}
									$scope.products_order[x].inventory_moq++;

									$scope.update_cart_by_order_quantity(x);


								};

								$scope.decrement = function (x) {

									if (parseInt($scope.products_order[x].inventory_moq) <= parseInt($scope.products_order[x].inventory_moq_original)) {
										alert('This value is not allowed. Minimum order Quantity is ' + $scope.products_order[x].inventory_moq);
										/*bootbox.alert({
										size: "small",
										message: 'This value is not allowed. Minimum order Quantity is '+$scope.products_order[x].inventory_moq,
										});*/
										return;
									}
									$scope.products_order[x].inventory_moq--;
									$scope.update_cart_by_order_quantity(x);

								};

								var timeoutPromise;
								$scope.keypuptimerfun = function (evt, x) {
									if ($scope.products_order[x].inventory_moq == "") {
										$("#tabtoordersumm_btn").attr({ "disabled": true });
									}
									else {
										$("#tabtoordersumm_btn").attr({ "disabled": false });
									}


									$timeout.cancel(timeoutPromise);
									timeoutPromise = $timeout(function () {
										$scope.keypup(evt, x);
									}, 1000);
								}

								$scope.keypup = function (evt, x) {
									angular.element("#quantity_status_info_" + x).html('');
									if (parseInt($scope.products_order[x].inventory_moq) < parseInt($scope.products_order[x].inventory_moq_original) || $scope.products_order[x].inventory_moq == "") {
										angular.element("#quantity_status_info_" + x).html('Please enter a number between ' + $scope.products_order[x].inventory_moq_original + ' (Min. Order Qty) and ' + $scope.products_order[x].inventory_max_oq + ' (Max. Order Qty)');
										$scope.products_order[x].inventory_moq = $scope.products_order[x].inventory_moq_original;
										$scope.update_cart_by_order_quantity(x);
										return;
									}
									else if (parseInt($scope.products_order[x].inventory_moq) <= parseInt($scope.products_order[x].inventory_max_oq) && parseInt($scope.products_order[x].inventory_moq) > parseInt($scope.products_order[x].stock_available)) {
										angular.element("#quantity_status_info_" + x).html('Out of Stock!');
										$scope.products_order[x].inventory_moq = $scope.products_order[x].inventory_moq_original;
										$scope.update_cart_by_order_quantity(x);
										return false;
									}
									else if (parseInt($scope.products_order[x].inventory_moq) > parseInt($scope.products_order[x].inventory_max_oq)) {
										angular.element("#quantity_status_info_" + x).html('Please enter a number between ' + $scope.products_order[x].inventory_moq_original + ' (Min. Order Qty) and ' + $scope.products_order[x].inventory_max_oq + ' (Max. Order Qty)');
										$scope.products_order[x].inventory_moq = $scope.products_order[x].inventory_moq_original;
										$scope.update_cart_by_order_quantity(x);
										return;
									}
									else {
										$scope.update_cart_by_order_quantity(x);
									}

								}


								$scope.update_cart_by_order_quantity = function (x) {
									if ($scope.products_order[x].inventory_moq == "") {
										$("#tabtoordersumm_btn").attr({ "disabled": true });
									}
									else {
										$("#tabtoordersumm_btn").attr({ "disabled": false });
									}
									$scope.products_order[x].inventory_weight_in_kg = parseInt($scope.products_order[x].inventory_moq) * parseFloat($scope.products_order[x].inventory_weight_in_kg_for_unit);

									$scope.products_order[x].inventory_volume_in_kg = parseInt($scope.products_order[x].inventory_moq) * parseFloat($scope.products_order[x].inventory_volume_in_kg_for_unit);


									if ($scope.products_order[x].promotion_available == 1) {

										if ($scope.products_order[x].default_discount != "") {


											modulo = $scope.products_order[x].inventory_moq % $scope.products_order[x].promotion_minimum_quantity;

											$scope.products_order[x].modulo = modulo;



											if (modulo != 0 && ($scope.products_order[x].inventory_moq >= parseInt($scope.products_order[x].promotion_minimum_quantity))) {


												$scope.products_order[x].price_of_modulo_1 = Math.round($scope.products_order[x].selling_price - (($scope.products_order[x].selling_price * $scope.products_order[x].default_discount) / 100));

												$scope.products_order[x].price_after_default_discount = modulo * $scope.products_order[x].price_of_modulo_1;
												quotient = $scope.products_order[x].inventory_moq / $scope.products_order[x].promotion_minimum_quantity;
												quotient = Math.floor(quotient);


												if (quotient != 0) {
													$scope.products_order[x].quotient = $scope.products_order[x].promotion_minimum_quantity * quotient;

													$scope.products_order[x].price_of_quotient_1 = Math.round($scope.products_order[x].selling_price - (($scope.products_order[x].selling_price * $scope.products_order[x].promotion_discount) / 100));
													$scope.products_order[x].price_after_promotion_discount = $scope.products_order[x].promotion_minimum_quantity * quotient * $scope.products_order[x].price_of_quotient_1;

												}

												if ($scope.products_order[x].price_after_promotion_discount != null) {
													$scope.products_order[x].final_product_individual = parseInt($scope.products_order[x].price_after_promotion_discount) + parseInt($scope.products_order[x].price_after_default_discount);
												} else {

													$scope.products_order[x].final_product_individual = parseInt($scope.products_order[x].price_after_default_discount);
												}

											} else {


												if ($scope.products_order[x].inventory_moq >= parseInt($scope.products_order[x].promotion_minimum_quantity)) {

													quotient = $scope.products_order[x].inventory_moq / $scope.products_order[x].promotion_minimum_quantity;
													quotient = Math.floor(quotient);
													$scope.products_order[x].quotient = $scope.products_order[x].promotion_minimum_quantity * quotient;
													$scope.products_order[x].price_of_quotient_1 = Math.round($scope.products_order[x].selling_price - (($scope.products_order[x].selling_price * $scope.products_order[x].promotion_discount) / 100));
													$scope.products_order[x].price_after_promotion_discount = $scope.products_order[x].promotion_minimum_quantity * quotient * $scope.products_order[x].price_of_quotient_1;

													$scope.products_order[x].final_product_individual = parseInt($scope.products_order[x].price_after_promotion_discount);

													$scope.products_order[x].price_after_default_discount = 0;



												} else {

													quotient = $scope.products_order[x].inventory_moq / $scope.products_order[x].promotion_minimum_quantity;
													quotient = Math.floor(quotient);
													$scope.products_order[x].quotient = $scope.products_order[x].promotion_minimum_quantity * quotient;

													$scope.products_order[x].price_of_modulo_1 = Math.round($scope.products_order[x].selling_price - (($scope.products_order[x].selling_price * $scope.products_order[x].default_discount) / 100));

													$scope.products_order[x].price_after_default_discount = modulo * $scope.products_order[x].price_of_modulo_1;

													$scope.products_order[x].final_product_individual = parseInt($scope.products_order[x].price_after_default_discount);

												}

											}

										}//discount promotion !=0

										if ($scope.products_order[x].default_discount == "") {

											modulo = $scope.products_order[x].inventory_moq % $scope.products_order[x].promotion_minimum_quantity;
											$scope.products_order[x].modulo = modulo;
											if (modulo != 0) {


												$scope.products_order[x].price_of_modulo_1 = $scope.products_order[x].selling_price;

												$scope.products_order[x].price_after_default_discount = modulo * $scope.products_order[x].price_of_modulo_1;
												quotient = $scope.products_order[x].inventory_moq / $scope.products_order[x].promotion_minimum_quantity;
												quotient = Math.floor(quotient);
												if (quotient != 0) {
													$scope.products_order[x].quotient = $scope.products_order[x].promotion_minimum_quantity * quotient;
													$scope.products_order[j].price_of_quotient_1 = Math.round($scope.products_order[x].selling_price - (($scope.products_order[x].selling_price * $scope.products_order[x].promotion_discount) / 100));

													$scope.products_order[x].price_after_promotion_discount = $scope.products_order[x].promotion_minimum_quantity * quotient * $scope.products_order[x].price_of_quotient_1;
												}


												if ($scope.products_order[x].price_after_promotion_discount != null) {
													$scope.products_order[x].final_product_individual = parseInt($scope.products_order[x].price_after_promotion_discount) + parseInt($scope.products_order[x].price_after_default_discount);
												} else {

													$scope.products_order[x].final_product_individual = parseInt($scope.products_order[x].price_after_default_discount);
												}


											} else {

												quotient = $scope.products_order[x].inventory_moq / $scope.products_order[x].promotion_minimum_quantity;
												quotient = Math.floor(quotient);
												$scope.products_order[x].quotient = $scope.products_order[x].promotion_minimum_quantity * quotient;
												$scope.products_order[x].price_of_quotient_1 = Math.round($scope.products_order[x].selling_price - (($scope.products_order[x].selling_price * $scope.products_order[x].promotion_discount) / 100));
												$scope.products_order[x].price_after_promotion_discount = $scope.products_order[x].promotion_minimum_quantity * quotient * $scope.products_order[x].price_of_quotient_1;

												$scope.products_order[x].final_product_individual = parseInt($scope.products_order[x].price_after_promotion_discount);
												$scope.products_order[x].price_after_default_discount = 0;


											}

										}
										if ($scope.products_order[x].promotion_cashback > 0) {
											cash_back_amount = parseFloat($scope.products_order[x].final_product_individual * ($scope.products_order[x].promotion_cashback / 100)).toFixed(2);
											$scope.products_order[x].cash_back_value = parseFloat(cash_back_amount)
										} else {

											cash_back_amount = "";
											$scope.products_order[x].cash_back_value = cash_back_amount;
										}

									}//promo avai 1
									else {
										$scope.products_order[x].individual_price_of_product_without_promotion = '';
										$scope.products_order[x].total_price_of_product_without_promotion = '';
										$scope.products_order[x].total_price_of_product_with_promotion = '';
										$scope.products_order[x].quantity_without_promotion = '';
										$scope.products_order[x].quantity_with_promotion = '';


									}


									if ($scope.products_order[x].price_of_modulo_1) {
										$scope.products_order[x].individual_price_of_product_without_promotion = $scope.products_order[x].price_of_modulo_1;
									} else {
										$scope.products_order[x].price_of_modulo_1 = ''; $scope.products_order[x].individual_price_of_product_without_promotion = $scope.products_order[x].price_of_modulo_1;
									}

									if ($scope.products_order[x].price_of_quotient_1) {
										$scope.products_order[x].individual_price_of_product_with_promotion = $scope.products_order[x].price_of_quotient_1;
									} else {
										$scope.products_order[x].price_of_quotient_1 = ''; $scope.products_order[x].individual_price_of_product_with_promotion = $scope.products_order[x].price_of_quotient_1;
									}


									if ($scope.products_order[x].price_after_default_discount) {
										$scope.products_order[x].total_price_of_product_without_promotion = $scope.products_order[x].price_after_default_discount;
									} else {

										$scope.products_order[x].price_after_default_discount = '';
										$scope.products_order[x].total_price_of_product_without_promotion = $scope.products_order[x].price_after_default_discount;
									}

									if ($scope.products_order[x].price_after_promotion_discount) {

										$scope.products_order[x].total_price_of_product_with_promotion = $scope.products_order[x].price_after_promotion_discount;
									} else {

										$scope.products_order[x].price_after_promotion_discount = ''; $scope.products_order[x].total_price_of_product_with_promotion = $scope.products_order[x].price_after_promotion_discount;
									}

									if ($scope.products_order[x].modulo) {
										$scope.products_order[x].quantity_without_promotion = $scope.products_order[x].modulo;
									} else {
										$scope.products_order[x].modulo = '';
										$scope.products_order[x].quantity_without_promotion = $scope.products_order[x].modulo;
									}

									if ($scope.products_order[x].quotient) {
										$scope.products_order[x].quantity_with_promotion = $scope.products_order[x].quotient;
									} else {
										$scope.products_order[x].quotient = '';
										$scope.products_order[x].quantity_with_promotion = $scope.products_order[x].quotient;
									}

									$scope.products_order[x].inventory_weight_in_kg = parseInt($scope.products_order[x].inventory_moq) * parseFloat($scope.products_order[x].inventory_weight_in_kg_for_unit);

									var update_cart_info_post_data = { 'cart_id': $scope.products_order[x].cart_id, 'inventory_moq': $scope.products_order[x].inventory_moq, 'inventory_weight_in_kg': $scope.products_order[x].inventory_weight_in_kg, 'logistics_id': $scope.products_order[x].logistics_id, 'inventory_volume_in_kg': $scope.products_order[x].inventory_volume_in_kg, 'inventory_volume_in_kg_for_unit': $scope.products_order[x].inventory_volume_in_kg_for_unit, 'logistics_territory_id': $scope.products_order[x].logistics_territory_id, 'default_delivery_mode_id': $scope.products_order[x].default_delivery_mode_id, 'logistics_parcel_category_id': $scope.products_order[x].logistics_parcel_category_id, 'territory_duration': $scope.products_order[x].territory_duration, 'inventory_weight_in_kg_org': $scope.products_order[x].inventory_weight_in_kg_org, 'inventory_weight_in_kg_for_unit': $scope.products_order[x].inventory_weight_in_kg_for_unit, 'shipping_charge': $scope.products_order[x].shipping_charge, 'individual_price_of_product_with_promotion': $scope.products_order[x].individual_price_of_product_with_promotion, 'individual_price_of_product_without_promotion': $scope.products_order[x].individual_price_of_product_without_promotion, 'total_price_of_product_without_promotion': $scope.products_order[x].total_price_of_product_without_promotion, 'total_price_of_product_with_promotion': $scope.products_order[x].total_price_of_product_with_promotion, 'quantity_without_promotion': $scope.products_order[x].quantity_without_promotion, 'quantity_with_promotion': $scope.products_order[x].quantity_with_promotion, 'promotion_available': $scope.products_order[x].promotion_available, 'cash_back_value': $scope.products_order[x].cash_back_value };
									$http({
										method: "POST",
										url: "<?php echo base_url(); ?>Account/update_and_get_cartable_info_in_json_format_checkout",
										data: update_cart_info_post_data,
										dataType: "JSON",
									}).success(function mySucces(result) {
										//alert(JSON.stringify(result));return false;
										$scope.products_order = result.get_cartinfo_in_json_format_checkout;
										$scope.order_product_count = $scope.products_order.length;
										$scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order);

									}, function myError(response) {

									});
								}

								$scope.removeItem = function (x) {

									if (document.getElementById('extra_data_' + x) != null) {
										document.getElementById('extra_data_' + x).innerHTML = "";
									}
									total_netprice = 0;

									var remove_cart_info_post_data = { 'cart_id': $scope.products_order[x].cart_id };
									$http({
										method: "POST",
										url: "<?php echo base_url(); ?>Account/remove_cart_info_for_session_customer",
										data: remove_cart_info_post_data,
										dataType: "JSON",
									}).success(function mySucces(result) {

										$scope.products_order.splice(x, 1);
										$scope.order_product_count = $scope.products_order.length;

										$scope.total_inventory_quantity_in_cart = 0;
										for (j = 0; j < $scope.order_product_count; j++) {
											$scope.total_inventory_quantity_in_cart += parseInt($scope.products_order[j].inventory_moq);
										}



										total_netprice = 0;
										if ($scope.order_product_count == 0) {
											$scope.empty_cart_in_ordercontroller = 1;
											$(".cart_summary").css({ "display": "none" });
											$(".proceed_to_checkout").css({ "display": "none" });
										}
										else {
											$scope.empty_cart_in_ordercontroller = 0;
											$(".cart_summary").css({ "display": "" });
											$(".proceed_to_checkout").css({ "display": "" });
										}

										$scope.update_cart_controller_scopes($scope.order_product_count, $scope.products_order);

										if ($scope.invoice_coupon_applied_status) {
											$scope.apply_coupon_code_invoice(0);
										}
										$cartElement = document.getElementsByClassName("cart_count");
										for (i = 0; i < $cartElement.length; i++) {
											$cartElement[i].innerHTML = $scope.order_product_count;
										}


									}, function myError(response) {

									});

								}
								$scope.capitalizeText = function (input) {
									return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
								}
								if ($scope.searched_pincode != null) {
									$scope.pincode = $scope.searched_pincode;
									$("#available_pincode_status_div").show();
									$("#available_pincode_div").hide();
								} else {
									$("#available_pincode_div").show();
									$("#available_pincode_status_div").hide();

								}
							}
							function isNumber(evt) {
								evt = (evt) ? evt : window.event;
								var charCode = (evt.which) ? evt.which : evt.keyCode;
								if (charCode > 31 && (charCode < 48 || charCode > 57)) {
									return false;
								}
								return true;
							}
							function go(logistics_delivery_mode_id, logistics_territory_id) {
								//alert(logistics_delivery_mode_id);
							}
						</script>

						<?php /*		
			 <?php 

			 print_r($promotions_cart);

			 ($promotions_cart);?>
			 <?php if(!empty($promotions_cart)){?>
			 <ul class="nav nav-stacked" id="cart_promotion">
			 <?php foreach($promotions_cart as $data){?>
			 <li id="<?php echo ($data['promo_uid']);?>"><?php //echo htmlspecialchars_decode($data['promo_quote']);?></li>
			 <?php }?>
			 </ul>
			 <?php } ?>		

			 */?>

						<div class="popular-tabs">

							<div class="page-content page-order well pl-0 pr-0 pt-10" ng-controller="OrderController">

								<?php
								/*
								?>
								<div class="row">
								<?php if(!empty($promotions_cart)){
								?>
								<div class="alert alert-info" ng-bind-html="explicitlyTrustedHtml_discount" ng-if="discount_oninvoice" ></div>
								<div class="alert alert-success" ng-bind-html="explicitlyTrustedHtml_cash_back" ng-if="cash_back_oninvoice"></div>
								<div class="alert alert-warning" ng-bind-html="explicitlyTrustedHtml_surprise_gift" ng-if="surprise_gift"></div>
								<div class="alert alert-danger" ng-bind-html="explicitlyTrustedHtml_free_shipping" ng-if="free_shipping"></div>
								<div class="alert alert-danger" ng-bind-html="explicitlyTrustedHtml_invoice_surprise_gift" ng-if="invoice_surprise_gift"></div>
								<?php } ?>
								</div>
								<?php
								*/
								?>


								<div class="text-left my-cart" style="margin-left:1em;">
									<!--<h4 class="editcartinfo">
<u><a href="<?php //echo base_url()?>cart"> Changed your mind? To edit quantity click here </a></u>
</h4>-->
									<b>My Cart:

										<span>{{order_product_count}} Items totalling
											{{total_inventory_quantity_in_cart}} Quantity</span><span
											ng-if="final_count_free_items>0"> + {{final_count_free_items}} Quantity
											Free</span></b>
									<span id="pin_availability_chk"> </span>

									<h5 class="text-right" id="pincode_change_txt" style="display:none;color:red;">
									<i class="fa fa-info-circle" aria-hidden="true"></i> Shipping Price is calculated based on the delivery address pincode
									</h5>

									<div ng-model="empty_cart_in_ordercontroller"
										ng-if="empty_cart_in_ordercontroller==1">
										<button class='button preventDflt'
											onclick="location.href='<?php echo base_url(); ?>'">Proceed Shopping</button>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<p align="center" id="quantity_status_info_{{$index}}" style="color:#f00"></p>
										<div class="">


										<table class="table table-responsive cart_summary mb-0">

<tbody>


                                    <tr ng-repeat="(key, value) in products_order track by $index" ng-init="parentIndex = $index">
                                        
                                    <td colspan="8" style="border:none;" class="pl-0 pr-0">

                                        <div class="row mb-10">
                                        <div class="col-md-12 text-center pl-0 pr-0" id="quantity_status_info_{{$index}}" style="color:#f00;border:none;">
                                        </div>
                                        </div>


											<table class="table table-bordered table-responsive cart_summary"
												style="margin-bottom:0;">
												<thead>
													<tr>
														<th class="cart_product text-center"
															style="vertical-align: middle;">S.No. {{$index+1}}</th>

														<th class="text-center" style="vertical-align: middle;">Product
															Description</th>
														<th width="10%" class="text-center"
															style="vertical-align: middle;">Qty</th>
														<!--										<th width="5%" ng-if="value.promotion_available==1" class="text-center" style="vertical-align: middle;">Break-up</th>-->

														<th width="20%"
															ng-if="(value.promotion_available==1  || value.selling_discount>0)"
															class="text-center" style="vertical-align: middle;">Offer
														</th>
														<th width="10%" class="text-center"
															style="vertical-align: middle;">Price
															<br><small style="font-size:0.7em;">(Inclusive of
																GST)</small>
														</th>
														<th width="20%" class="text-center"
															style="vertical-align: middle;">Delivery Details</th>
														<th width="10%" class="text-center"
															style="vertical-align: middle;">Subtotal<br><small
																style="font-size:0.7em;">Excluding
																Shipping</small><br><small
																style="font-size:0.7em;">(Inclusive of GST)</small></th>


													</tr>
												</thead>
												<tbody>
													<tr>
														<td class="cart_product">
															<a ng-if="value.item_stock_available==1"
																href="<?php echo base_url() ?>detail/<?php echo $controller->sample_code(); ?>{{value.inventory_id}}"><img
																	ng-src="<?php echo base_url(); ?>{{value.inventory_image}}"
																	alt="Product" /></a>
															<a ng-if="value.item_stock_available==0"
																href="<?php echo base_url() ?>detail/<?php echo $controller->sample_code(); ?>{{value.inventory_id}}"><img
																	ng-src="<?php echo base_url(); ?>{{value.inventory_image}}"
																	alt="Product" /></a>
														</td>
														<td class="cart_description">
															<p class="product-name"><a
																	href="<?php echo base_url() ?>detail/<?php echo $controller->sample_code(); ?>{{value.inventory_id}}">{{value.product_name}}
																</a>
															<div class="mb-10">{{value.product_description}}
																
															</div>
															</p>
															<small class="cart_ref">{{value.inventory_sku}}</small><br>
															<small><a href="#">{{value.attribute_1}} :
																	{{value.attribute_1_value}}</a></small><br>
															<small ng-if="value.attribute_2!=''"><a
																	href="#">{{value.attribute_2}} :
																	{{value.attribute_2_value}}</a></small>
															<br>
															<p ng-if="value.new_promo_avail==1"
																ng-click="avail_new_promotion(value.inventory_id,$index)"
																style="cursor:pointer;"><small><span> <u> <i
																				class="fa fa-gift"
																				aria-hidden="true"></i>
																			Avail Offer(s) </u></span></small></p>
														</td>
														<td class="qty" align="center">
															<!--<div class="input-group">--->
															<?php
															/*
															?>
															<span class="input-group-btn">
															<button type="button" class="btn btn-default" ng-click="decrement($index);"><i class="fa fa-minus" ></i></button>
															</span>
															<?php
															*/
															?>
															<!--<input class="form-control" ng-model="value.inventory_moq" type="text" onkeypress="return isNumber(event);" ng-keyup="keypuptimerfun($event,$index)" style="width:50px;border:1px solid #ccc;">--->
															{{value.inventory_moq}}
															<?php
															/*
															?>
															<span class="input-group-btn">
															<button type="button" class="btn btn-default" ng-click="increment($index);" ><i class="fa fa-plus" ></i></button>
															</span>
															<?php
															*/
															?>
															<!--</div>--->
														</td>

														<td colspan="5" class="text-center"
															ng-if="value.item_stock_available==0">
															<!---apply opacity over here--->
															<span ng-if="value.item_stock_available==0"
																style="color:red;">Out of stock</span>
															<span
																ng-if="(value.item_stock_available==0 && value.stock_available>0)">Available
																unit(s): {{value.stock_available}}</span>
														</td>
														<td colspan="5" ng-if="value.item_stock_available==1">
															<table class="" width="100%">
																<tr ng-if="value.promotion_available==0">
																	<td width="5%" class="text-center"
																		ng-if="value.selling_discount>0">
																		<!--{{value.promotion}}-->
																		1.
																	</td>
																	<td width="15%" ng-if="value.selling_discount>0">
																		<p ng-if="(value.selling_discount>0)">
																			{{value.selling_discount | ceil }}% Discount
																			on SKU's </p>
																	</td>
																	<td width="10%">
																		{{value.inventory_moq}} *
																		{{value.selling_price}}
																	</td>
																	<td width="20%">
																		<!--		<span ng-if="value.shipping_charge_view==1">Shipping Charge  &#8377;{{value.shipping_charge}}</span>

<span ng-if="value.shipping_charge_view==0" ng-model="shipping_charge">Shipping Charge {{value.shipping_charge_msg}}</span>
<span ng-if="value.delivery_service_msg"><br> <span style="{{value.msg_style}}">{{value.delivery_service_msg}}</span></span>
<span><br>Delivery By {{value.delivery_date}}</span>
<br>	--->
																		<span
																			ng-if="value.delivery_service_msg && value.shipping_charge_view==0"><span
																				style="{{value.msg_style}}">{{value.delivery_service_msg}}</span></span>
																		<div ng-if="value.shipping_charge_view==1">
																			<div
																				ng-repeat="modes in value.delivery_modes">
																				Delivery by
																				<b>{{modes.logistics_name}}</b>
																			</div>
																		</div>
																		<div ng-if="value.shipping_charge_view==1"
																			style="display:none;">
																			<div id="delivery_speed_block_{{value.cart_id}}"
																				style="margin-top:10px;">
																				<h5><b>Choose a Delivery Speed</b></h5>
																				<div
																					ng-repeat="modes in value.delivery_modes">
																					<input type="radio"
																						class="delivery_mode_id_{{value.cart_id}}"
																						name="delivery_mode_id[{{value.cart_id}}][]"
																						value="{{modes.logistics_delivery_mode_id}}"
																						ng-checked="{{modes.checked}}"
																						ng-click="update_delivery_speed($parent.$index,$index, modes.logistics_delivery_mode_id,value.cart_id)">
																					{{modes.delivery_mode}}<br>
																					<!-- <a ng-click="addSomething($parent.$index)">Add Something</a>-->
																				</div>
																			</div>
																			<div id="parcel_category_block_{{value.cart_id}}"
																				style="margin-top:10px;">
																				<h5><b>Choose a Handling Process</b>
																				</h5>
																				<div
																					ng-repeat="handling in value.parcel_categories">
																					<input type="hidden"
																						value="{{handling.logistics_parcel_category_id}}"
																						ng-if="handling.default_parcel_category==1"
																						id="handlingdefaultprocess_{{value.cart_id}}">
																					<input type="radio"
																						name="logistics_parcel_category_id[{{value.cart_id}}][]"
																						value="{{handling.logistics_parcel_category_id}}"
																						ng-checked="{{handling.checked}}"
																						ng-click="update_parcel_category($parent.$index,$index, handling.logistics_parcel_category_id,value.cart_id)">
																					{{capitalizeText(handling.parcel_type)}}<br>
																					<!-- <a ng-click="addSomething($parent.$index)">Add Something</a>-->
																				</div>
																			</div>
																		</div>
																	</td>
																	<td width="10%" class="text-right">
																		<?php echo curr_sym; ?><span id="amout_to_cp">
																			{{value.inventory_selling_price_without_discount}}</span>
																	</td>
																</tr>
																<tr
																	ng-if="value.promotion_available==1 && value.promotion_quote_show==1">
																	<td width="5%">
																		1.
																		<!--{{value.promotion}}--->
																	</td>
																	<td width="20%"><span
																			ng-if="value.promotion_quote_show==1">
																			{{value.promotion_quote}}<br></span></td>
																	<td width="10%" class="text-center"><span
																			ng-if="value.quotient>0"> {{value.quotient}}
																			*
																			<?php echo curr_sym; ?>{{value.price_of_quotient_1}}
																		</span>
																		<span ng-if="value.promotion_available==0">
																			{{value.inventory_moq}} *
																			<?php echo curr_sym; ?>{{value.selling_price}}
																		</span>
																	</td>
																	<td width="20%">
																		<!---<span ng-if="value.shipping_charge_view==1">Shipping Charge &#8377;{{value.shipping_charge}}</span>

<span ng-if="value.shipping_charge_view==0" ng-model="shipping_charge">Shipping Charge {{value.shipping_charge_msg}}</span>
<span ng-if="value.delivery_service_msg"><br> <span style="{{value.msg_style}}">{{value.delivery_service_msg}}</span></span>
<span><br>Delivery By {{value.delivery_date}}</span>
<br>--->
																		<span ng-if=" value.discount_quote_show==0">
																			<div
																				ng-repeat="modes in value.delivery_modes">
																				Delivery by
																				<b>{{modes.logistics_name}}</b>
																			</div>
																		</span>

																		<span ng-if=" value.discount_quote_show==0"
																			style="display:none;">
																			<span
																				ng-if="value.delivery_service_msg && value.shipping_charge_view==0"><span
																					style="{{value.msg_style}}">{{value.delivery_service_msg}}</span></span>

																			<div ng-if="value.shipping_charge_view==1">

																				<div id="delivery_speed_block_{{value.cart_id}}"
																					style="margin-top:10px;">
																					<h5><b>Choose a Delivery Speed</b>
																					</h5>

																					<div
																						ng-repeat="modes in value.delivery_modes">

																						<input type="radio"
																							class="delivery_mode_id_{{value.cart_id}}"
																							name="delivery_mode_id[{{value.cart_id}}][]"
																							value="{{modes.logistics_delivery_mode_id}}"
																							ng-checked="{{modes.checked}}"
																							ng-click="update_delivery_speed($parent.$index,$index, modes.logistics_delivery_mode_id,value.cart_id)">
																						{{modes.delivery_mode}}<br>
																						<!-- <a ng-click="addSomething($parent.$index)">Add Something</a>-->
																					</div>

																				</div>
																			</div>

																			<div id="parcel_category_block_{{value.cart_id}}"
																				style="margin-top:10px;">
																				<h5><b>Choose a Handling Process</b>
																				</h5>

																				<div
																					ng-repeat="handling in value.parcel_categories">
																					<input type="hidden"
																						value="{{handling.logistics_parcel_category_id}}"
																						ng-if="handling.default_parcel_category==1"
																						id="handlingdefaultprocess_{{value.cart_id}}">
																					<input type="radio"
																						name="logistics_parcel_category_id[{{value.cart_id}}][]"
																						value="{{handling.logistics_parcel_category_id}}"
																						ng-checked="{{handling.checked}}"
																						ng-click="update_parcel_category($parent.$index,$index, handling.logistics_parcel_category_id,value.cart_id)">
																					{{capitalizeText(handling.parcel_type)}}<br>
																					<!-- <a ng-click="addSomething($parent.$index)">Add Something</a>-->
																				</div>



																			</div>
																		</span>
																	</td>
																	<td width="10%" class="text-right">
																		<span ng-if="value.quotient>0">
																			<?php echo curr_sym; ?><span
																				id="amout_to_cp">{{value.price_after_promotion_discount}}</span>
																		</span>
																		<span ng-if="value.promotion_available==0">
																			<?php echo curr_sym; ?><span
																				id="amout_to_cp">{{value.inventory_selling_price_without_discount}}</span>
																		</span>
																	</td>
																</tr>

																<tr
																	ng-if="value.promotion_available==1 && value.discount_quote_show==1">

																	<td width="5%">
																		<span
																			ng-if="value.promotion_available==1 && value.discount_quote_show==1 && value.promotion_quote_show==1 || value.promotion_available==1 && value.promotion_quote_show==0 && value.selling_discount>0">2.</span>
																		<span
																			ng-if="value.promotion_available==1 && value.discount_quote_show==1 && value.promotion_quote_show==0">1.</span>
																		<!--{{value.discount}}-->

																	</td>
																	<td width="20%"><span
																			ng-if="value.discount_quote_show==1">
																			{{value.promotion_default_discount_promo}}</span>
																		<span
																			ng-if="value.promotion_available==1 && value.discount_quote_show==1 && value.selling_discount>0">

																			<p ng-if="(value.selling_discount>0)">
																				{{value.selling_discount | ceil }}%
																				Discount on SKU's </p>

																		</span>

																	</td>
																	<td width="10%" class="text-center"><span
																			ng-if="value.modulo!=0">{{value.modulo}} *
																			<?php echo curr_sym; ?>{{value.price_of_modulo_1}}<br>
																		</span></td>
																	<td width="20%">

																		<!--	<span ng-if="value.shipping_charge_view==1">Shipping Charge &#8377;{{value.shipping_charge}}</span>

<span ng-if="value.shipping_charge_view==0" ng-model="shipping_charge">Shipping Charge {{value.shipping_charge_msg}}</span>
<span ng-if="value.delivery_service_msg"><br> <span style="{{value.msg_style}}">{{value.delivery_service_msg}}</span></span>
<span><br>Delivery By {{value.delivery_date}}</span>
<br>			

--->
																		<span
																			ng-if="value.delivery_service_msg && value.shipping_charge_view==0"><span
																				style="{{value.msg_style}}">{{value.delivery_service_msg}}</span></span>

																		<div ng-if="value.shipping_charge_view==1">
																			<div
																				ng-repeat="modes in value.delivery_modes">
																				Delivery by
																				<b>{{modes.logistics_name}}</b>
																			</div>
																		</div>

																		<div ng-if="value.shipping_charge_view==1"
																			style="display:none;">

																			<div id="delivery_speed_block_{{value.cart_id}}"
																				style="margin-top:10px;">
																				<h5><b>Choose a Delivery Speed</b></h5>

																				<div
																					ng-repeat="modes in value.delivery_modes">

																					<input type="radio"
																						class="delivery_mode_id_{{value.cart_id}}"
																						name="delivery_mode_id[{{value.cart_id}}][]"
																						value="{{modes.logistics_delivery_mode_id}}"
																						ng-checked="{{modes.checked}}"
																						ng-click="update_delivery_speed($parent.$index,$index, modes.logistics_delivery_mode_id,value.cart_id)">
																					{{modes.delivery_mode}}<br>
																					<!-- <a ng-click="addSomething($parent.$index)">Add Something</a>-->
																				</div>



																			</div>
																			<div id="parcel_category_block_{{value.cart_id}}"
																				style="margin-top:10px;">
																				<h5><b>Choose a Handling Process</b>
																				</h5>

																				<div
																					ng-repeat="handling in value.parcel_categories">
																					<input type="hidden"
																						value="{{handling.logistics_parcel_category_id}}"
																						ng-if="handling.default_parcel_category==1"
																						id="handlingdefaultprocess_{{value.cart_id}}">
																					<input type="radio"
																						name="logistics_parcel_category_id[{{value.cart_id}}][]"
																						value="{{handling.logistics_parcel_category_id}}"
																						ng-checked="{{handling.checked}}"
																						ng-click="update_parcel_category($parent.$index,$index, handling.logistics_parcel_category_id,value.cart_id)">
																					{{capitalizeText(handling.parcel_type)}}<br>
																					<!-- <a ng-click="addSomething($parent.$index)">Add Something</a>-->
																				</div>

																			</div>
																		</div>
																	</td>
																	<td width="10%" class="text-right"><span
																			ng-if="value.price_after_default_discount>0">

																			<!--                                                                                    <span ng-if="value.coupon_applied_status==1">

<strike> sas <?php echo curr_sym; ?> {{value.coupon_strike_price}}</strike><br>
</span>-->
																			<?php echo curr_sym; ?><span
																				id="amout_to_cp">{{value.price_after_default_discount}}</span>
																		</span></td>
																</tr>
															</table>
														</td>
													</tr>
												</tbody>
												<tbody class="extra_data" id="extra_data_{{$index}}"
													ng-if="value.show_addon_items==1">

												</tbody>
												<!--- addon products --->
												<tbody class="extra_data" id="1addon_products_{{$index}}"
													ng-if="value.addon_products_status=='1'">
													<tr ng-if="value.addon_count_updated==1">
														<td colspan="5">
															<div ng-repeat="addon in value.addon_products track by $index"
																class="col-xs-5 col-sm-5 col-md-3 nopad text-center"
																id="addon_{{$index}}">
																<label class="image-checkbox image-checkbox-checked"
																	ng-click="remove_addon_products($parent.$index,addon.inv_id,addon.inv_price)">
																	<img class="img-responsive"
																		src="<?php echo base_url(); ?>{{addon.inv_image}}">


																	<div class="yourprice_div offerprice_addon"
																		ng-if="addon.inv_price==0"><strong>FREE
																		</strong></div>

																	<div class="yourprice_div offerprice_addon"
																		ng-if="addon.inv_price>0"><strong>
																			<?php echo curr_sym; ?>{{addon.inv_price}}
																			only
																		</strong></div>


																	<div class="yourprice_div"><span
																			class="hurbztextcolor">{{addon.offer_percentage}}%
																			off</span> MRP :
																		<?php echo curr_sym; ?>{{addon.inv_mrp_price}}
																	</div>

																	<!-- <i class="fa fa-times hidden" style="line-height: 15px;"></i> -->
																	<i class="fa fa-check hidden"
																		style="line-height: 15px;"></i>

																	<button
																		class="btn btn-warning btn-sm preventDflt sku_display_name_on_buttons align-top"
																		style="width:95%;">{{addon.inv_display_name}}</button>
																	<h5 class="margin-top margin-bottom">
																		<b>{{addon.inv_sku_id}}</b></h5>

															</div>
														</td>
														<td colspan="1" class="text-center">
															<span ng-if="value.addon_count">Addon Products :
																{{value.addon_count}}</span>
														</td>
														<td class="text-center">
															<span id="addon_total_price_1">
																<?php echo curr_sym; ?>{{value.addon_total_price}}
															</span>
														</td>
													</tr>
												</tbody>
												<!--- addon products --->
												<!---coupon-->
												<tr
													ng-if="value.promotion_available==1 && value.cp_available_status==1">
													<td colspan="8">
														<table class="" width="100%">
															<tr>
																<td class="text-left" colspan="4">
																	<div class="row"
																		ng-if="value.coupon_applied_status!=1">

																		<div class="col-md-4 col-md-offset-left-6">
																			<input type="text"
																				id="coupon_code_{{value.inventory_id}}"
																				value="" placeholder="Enter Coupon code"
																				class="form-control">
																		</div>
																		<div class="col-md-2">
																			<button class="btn btn-success btn-sm"
																				ng-click="apply_coupon_code($index,value.inventory_id,value.inventory_sku)">Apply</button>
																		</div>
																	</div>
																	<div class="row"
																		ng-if="value.coupon_applied_status==1">
																		<div class="col-md-12">
																			{{value.coupon_txt_appied}} <span
																				style="cursor:pointer;color:red"
																				ng-click="remove_coupon($index,value.inventory_id,value.inventory_sku)"><u>Remove</u></span>
																		</div>
																	</div>
																	<!--- addon products --->
																	<tbody class="extra_data"
																		id="1addon_products_{{$index}}"
																		ng-if="value.addon_products_status=='1'">

																		<tr ng-if="value.addon_count_updated==1">
																			<td colspan="5">
																				<div ng-repeat="addon in value.addon_products track by $index"
																					class="col-xs-5 col-sm-5 col-md-3 nopad text-center"
																					id="addon_{{$index}}">

																					<!-- ng-click="remove_addon_products($parent.$index,addon.inv_id,addon.inv_price)" -->

																					<label
																						class="image-checkbox image-checkbox-checked">
																						<img class="img-responsive"
																							src="<?php echo base_url(); ?>{{addon.inv_image}}">


																						<div class="yourprice_div offerprice_addon"
																							ng-if="addon.inv_price==0">
																							<strong>FREE </strong></div>

																						<div class="yourprice_div offerprice_addon"
																							ng-if="addon.inv_price>0">
																							<strong>
																								<?php echo curr_sym; ?>{{addon.inv_price}}
																								only
																							</strong></div>


																						<div class="yourprice_div"><span
																								class="hurbztextcolor">{{addon.offer_percentage}}%
																								off</span> MRP :
																							<?php echo curr_sym; ?>{{addon.inv_mrp_price}}
																						</div>

																						<!-- <i class="fa fa-times hidden" style="line-height: 15px;"></i> -->
																						<!-- <i class="fa fa-check hidden" style="line-height: 15px;"></i> -->

																						<button
																							class="btn btn-warning btn-sm preventDflt sku_display_name_on_buttons align-top"
																							style="width:95%;">{{addon.inv_display_name}}</button>
																						<h5
																							class="margin-top margin-bottom">
																							<b>{{addon.inv_sku_id}}</b>
																						</h5>

																				</div>

																			</td>
																			<td colspan="1" class="text-center">

																				<span ng-if="value.addon_count">Addon
																					Products :
																					{{value.addon_count}}</span>

																			</td>
																			<td class="text-right" colspan="4">
																				<span
																					ng-if="value.coupon_applied_status==1">

																					<strike>
																						<?php echo curr_sym; ?>
																						{{value.coupon_strike_price}}
																					</strike><br>
																					<span
																						ng-if="value.promo_updated_coupon_price>0">
																						<?php echo curr_sym; ?>
																						{{value.promo_updated_coupon_price}}
																					</span><br>
																				</span>
																			</td>
																		</tr>

														</table>
													</td>
												</tr>
												<!---coupon-->
												<tr ng-if="value.promotion_available==1">
													<td colspan="8">
														<table class="" width="100%">
															<tr>
																<td class="text-right" colspan="8">
																	<span> Delivery By - <span
																			class="ordrk-color"><b>{{value.delivery_date}}</b></span>
																		| </span>
																	<span ng-if="value.shipping_charge_view==0"
																		ng-model="shipping_charge">Shipping Charge
																		{{value.shipping_charge_msg}} |</span>
																	<span ng-if="value.shipping_charge_view==1">
																		Shipping Charge </span>
																	<span ng-if="value.shipping_charge_view==1">
																		<span ng-if="eligible_free_shipping_display==0">
																			<?php echo curr_sym; ?>{{value.shipping_charge_total}}
																		</span>
																		<span ng-if="eligible_free_shipping_display==1">
																			<strike>
																				<?php echo curr_sym; ?>{{value.shipping_charge_total}}
																			</strike> Free
																		</span>
																	</span>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr ng-if="value.promotion_available==1">
													<td colspan="8">
														<table class="" width="100%">
															<td class="text-right" colspan="8">
																Subtotal Including Shipping <span
																	class="red pull-left"><a href='#' class="red btn-sm"
																		ng-click="removeItem($index)"> Remove
																		Item</a></span>
																<span class="text-right" colspan="1"
																	ng-if="value.item_stock_available==1">
																	<span>
																		<?php echo curr_sym; ?>{{value.subtotal}}
																	</span>
																</span>
															</td>
														</table>
													</td>
												</tr>
												<!---coupon-->
												<tr
													ng-if="value.promotion_available==0 && value.cp_available_status==1">
													<td colspan="8">
														<table class="" width="100%">
															<td class="text-left" colspan="4">
																<div class="row" ng-if="value.coupon_applied_status!=1">

																	<div class="col-md-4 col-md-offset-left-6">
																		<input type="text"
																			id="coupon_code_{{value.inventory_id}}"
																			value="" placeholder="Enter Coupon code"
																			class="form-control">
																	</div>
																	<div class="col-md-2">
																		<button class="btn btn-success btn-sm"
																			ng-click="apply_coupon_code($index,value.inventory_id,value.inventory_sku)">Apply</button>
																	</div>
																</div>
																<div class="row" ng-if="value.coupon_applied_status==1">
																	<div class="col-md-12">
																		{{value.coupon_txt_appied}} <span
																			style="cursor:pointer;color:red"
																			ng-click="remove_coupon($index,value.inventory_id,value.inventory_sku)"><u>Remove</u></span>
																	</div>
																</div>
															</td>
															<td class="text-right" colspan="4">
																<span ng-if="value.coupon_applied_status==1">
																	<strike>
																		<?php echo curr_sym; ?>
																		{{value.coupon_strike_price}}
																	</strike><br>
																	<span ng-if="value.promo_updated_coupon_price>0">
																		<?php echo curr_sym; ?>
																		{{value.promo_updated_coupon_price}}
																	</span><br>
																</span>
															</td>
														</table>
													</td>
												</tr>
												<!---coupon-->
												<tr ng-if="value.promotion_available==0">
													<td colspan="8">
														<table class="" width="100%">
															<td class="text-right" colspan="8">
																<span> Delivery By - <span
																		class="ordrk-color"><b>{{value.delivery_date}}</b></span>
																	| </span>
																<span ng-if="value.shipping_charge_view==0"
																	ng-model="shipping_charge">Shipping Charge
																	{{value.shipping_charge_msg}} |</span>
																<span ng-if="value.shipping_charge_view==1"> Shipping
																	Charge </span>
																<span ng-if="value.shipping_charge_view==1">
																	<span ng-if="eligible_free_shipping_display==0">
																		<?php echo curr_sym; ?>{{value.shipping_charge_total}}
																	</span>
																	<span ng-if="eligible_free_shipping_display==1">
																		<strike>
																			<?php echo curr_sym; ?>{{value.shipping_charge_total}}
																		</strike> Free
																	</span>
																</span>
															</td>
														</table>
													</td>
												</tr>
												<tr ng-if="value.promotion_available==0">
													<td colspan="8">
														<table class="" width="100%">
															<td class="text-right" colspan="8">
																Subtotal Including Shipping
																<span class="red pull-left"><a href='#'
																		class="red btn-sm"
																		ng-click="removeItem($index)"> Remove
																		Item</a></span>
																<span class="text-right"
																	ng-if="value.item_stock_available==1">
																	<span>
																		<?php echo curr_sym; ?>{{value.subtotal}}
																	</span>
																</span>
															</td>
														</table>
													</td>
												</tr>


											</table>
									</td>
									</tr>
												<?php if (!empty($promotions_cart)) { ?>
													<tr style="font-size:.95em;">
														<td colspan="8" style="border:none;">
															<table width="100%">
																<tfoot ng-if="out_of_stock_item_exists==0">
																	<tr ng-if="eligible_discount_percentage_display==1">
																		<td class="text-right" colspan="2"
																			style="font-size:14px;">
																			<span
																				ng-if="eligible_discount_percentage_display==1">
																				Eligible for discount on invoice
																				{{eligible_discount_percentage}}% of
																				{{total_price_without_shipping_price}}
																				{{currency_type_discount}}</span>
																			<span
																				ng-if="eligible_discount_percentage_display==1">
																				-
																				<?php echo curr_sym; ?>{{grandTotal_for_discount}}
																			</span>
																		</td>
																	</tr>
																	<tr ng-if="eligible_free_shipping_display==1">
																		<td class="text-right" colspan="2"
																			style="font-size:14px;">
																			<span ng-if="eligible_free_shipping_display==1">
																				Eligible for Free Shipping under offer
																				Freeship! </span>
																			<!--
<span ng-if="eligible_free_shipping_display==1"> You are eligible for  Free shipping on  invoice of shipping charges {{total_shipping_price}} {{currency_type_free_shipping}} </span>
-->
																			<span
																				ng-if="eligible_free_shipping_display==1">-
																				<?php echo curr_sym; ?>{{total_shipping_price}}
																			</span>
																		</td>
																	</tr>
																	<tr ng-if="invoice_coupon_avail_status==1">
																		<td class="text-right" colspan="8">
																			<div class="row"
																				ng-if="invoice_coupon_applied_status!=1">

																				<div class="col-md-4 col-md-offset-left-6">
																					<input type="text"
																						id="coupon_code_invoice" value=""
																						placeholder="Enter Coupon code"
																						class="form-control">
																				</div>
																				<div class="col-md-2">
																					<button class="btn btn-success btn-sm"
																						ng-click="apply_coupon_code_invoice(1)"
																						id="coupon_loader">Apply</button>
																				</div>
																			</div>
																			<!---coupon applied text--->
																		<span
																			ng-if="(invoice_coupon_applied_status==1)">
																			<i class="fa fa-heart"
																				style="color:#ff0000"></i>
																			{{invoice_coupon_applied_txt}} <span
																				style="cursor:pointer;color:red"
																				ng-click="remove_invoice_coupon()"><u>Remove</u></span>
																		</span>
																		<!---coupon applied text--->
																	</td>
																</tr>
																<!---coupon--->
																<tr ng-if="total_coupon_used_amount>0">
																	<td class="text-right" colspan="2"
																		style="font-size:14px;">
																		<span> <i class="fa fa-heart"
																				style="color:#ff0000"></i> Total coupon
																			used amount
																		</span>
																		<?php echo curr_sym; ?>{{total_coupon_used_amount}}
																	</td>
																</tr>
																<!---coupon--->
																<tr
																	ng-if="eligible_discount_percentage_display==1 || eligible_free_shipping_display==1">
																	<td class="text-right" colspan="2"
																		style="font-size:14px;">
																		<!--
<span ng-if="eligible_discount_percentage_display==1 || eligible_free_shipping_display==1"> To amount payable on invoice after <span ng-if="eligible_discount_percentage_display==1"> discount </span>

<span ng-if="eligible_discount_percentage_display==1 && eligible_free_shipping_display==1">  and </span>
</span>

<span ng-if="eligible_free_shipping_display==1"> after deduction shipping charges </span>
--->
																		Net Invoice Amount
																		<?php echo curr_sym; ?>{{amount_to_pay}}
																	</td>
																</tr>
																<tr>
																	<!--
<td><b>Total Price
</b>
{{total_shipping_price}} and {{total_price_without_shipping_price}}
</td> -->

																		<td class="text-right" colspan="2">
																			<h3 style="color:#004b9b;">
																				Total Price
																				<span
																					ng-if="eligible_discount_percentage_display==1">
																					+
																					<?php echo curr_sym; ?>{{grandtotal_without_discount}}
																				</span>
																				<span
																					ng-if="eligible_discount_percentage_display==0">
																					<?php echo curr_sym; ?>{{amount_to_pay}}
																				</span>
																			</h3>
																		</td>
																	</tr>
																	<tr
																		ng-if="(eligible_discount_percentage_display==1 || eligible_free_shipping_display==1) ||(you_saved>0) ">
																		<td class="text-right" colspan="2"
																			style="font-size:14px;">
																			<span
																				ng-if="(eligible_discount_percentage_display==1 || eligible_free_shipping_display==1) ||(you_saved>0) ">
																				<i class="fa fa-heart"
																					style="color:#ff0000"></i> You Saved
																			</span>
																			<?php echo curr_sym; ?>{{you_saved}}
																		</td>
																	</tr>
																	<!--<tr ng-if="eligible_cash_back_percentage_display==1" class="bg-info"> 

<td ><span ng-if="eligible_cash_back_percentage_display==1"> Total amount to be credited to your Wallet </span></td>
<td  class="text-right" ng-if="eligible_cash_back_percentage_display==1">  &#8377;{{total_cash_back}}</td>

</tr>-->
																</tfoot>
															</table>
														</td>
													</tr>

												<?php } else { ?>

													<tr ng-if="out_of_stock_item_exists==0" style="font-size:.95em;">
														<td colspan="8" style="border:none;padding-bottom:0;">
															<table width="100%">
																<tbody>
																	<tr ng-if="invoice_coupon_avail_status==1">
																		<td class="text-right" colspan="8">
																			<div class="row"
																				ng-if="invoice_coupon_applied_status!=1">
																				<div class="col-md-4 col-md-offset-left-6">
																					<input type="text"
																						id="coupon_code_invoice" value=""
																						placeholder="Enter Coupon code"
																						class="form-control">
																				</div>
																				<div class="col-md-2">
																					<button
																						class="btn btn-success btn-sm pull-left"
																						ng-click="apply_coupon_code_invoice(1)"
																						id="coupon_loader">Apply</button>
																				</div>
																			</div>
																			<!---coupon applied text--->
																		<span
																			ng-if="(invoice_coupon_applied_status==1)">
																			<i class="fa fa-heart"
																				style="color:#ff0000"></i>
																			{{invoice_coupon_applied_txt}} <span
																				style="cursor:pointer;color:red"
																				ng-click="remove_invoice_coupon()"><u>Remove</u>
																			</span>
																		</span>
																		<!---coupon applied text--->
																	</td>
																</tr>
																<!---coupon--->
																<tr ng-if="total_coupon_used_amount>0">
																	<td class="text-right" colspan="2"
																		style="font-size:14px;">
																		<span> <i class="fa fa-heart"
																				style="color:#ff0000"></i> Total coupon
																			used amount
																		</span>
																		<?php echo curr_sym; ?>{{total_coupon_used_amount}}
																	</td>
																</tr>
																<!---coupon--->
																<tr class="bg-default">
																	<td class="text-right"
																		style="font-weight:bold;color:red;font-size:1.2em;padding-bottom:0;"
																		colspan="8">
																		<b>
																			Total
																			Price<!--{{total_price_without_shipping_price}}-->
																				<span>
																					<?php echo curr_sym; ?>{{amount_to_pay}}
																				</span>
																			</b>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>

												<?php } ?>
												<tr ng-if="out_of_stock_item_exists==0" style="font-size:.95em;">
													<td colspan="8" style="border:none;">
														<table width="100%">
															<?php if (!empty($promotions_cart)) { ?>
																<tfoot
																	ng-if="show_surprise_gift_on_invoice==1 && out_of_stock_item_exists==0">
																	<tr class="bg-warning"
																		ng-if="invoice_surprise_gift_result">
																		<td>
																			<span class="right-align-cashback"><i
																					class="fa fa-gift" style="color:#ff0000"
																					aria-hidden="true"></i> Eligible for
																				surprise gifts (<span
																					ng-bind-html="explicitlyTrustedHtml_invoice_surprise_gift_result"></span></span>
																		</td>
																	</tr>
																</tfoot>
															<?php } ?>
															<tfoot ng-if="out_of_stock_item_exists==0">
																<tr ng-if="show_cash_back_tr==1" class="bg-info">
																	<td colspan="2" style="padding: 1px;">
																		<div class="panel-group bg-info"
																			style="margin:0px;">
																			<div class="panel panel-default">
																				<div class="panel-heading"
																					data-toggle="collapse"
																					href="#collapse1"
																					style="background-color:#d9edf7;cursor:pointer;">
																					<span style="font-size:.95em;">

																						<span
																							class="panel-title text-right">
																							<i
																								class="more-less glyphicon glyphicon-plus"></i>
																							<span
																								class="left-align-cashback-total">Total
																								cash back for SKUs
																							</span><span
																								class="text-right"
																								style="width:30%;font-size: .9em;">
																								<?php echo curr_sym; ?>{{total_cash_back_for_sku}}
																							</span>

																						</span></span>
																				</div>
																				<div id="collapse1"
																					class="panel-collapse collapse">
																					<ul class="list-group">
																						<li class="list-group-item"
																							ng-repeat="(key,value) in cash_back_tr_arr"
																							style="background-color:#d9edf7;">
																							<span
																								class="text-left left-align-cashback">
																								{{value.promotion_cashback_percentage}}
																								% cash back on
																								{{value.sku_count}}
																								quantity of
																								{{value.inventory_sku}}
																							</span><span
																								class="text-right"
																								style="width:30%">
																								<?php echo curr_sym; ?>{{value.cash_back_amount}}
																							</span>
																						</li>
																					</ul>

																				</div>
																			</div>
																		</div>

																	</td>
																</tr>


																<tr ng-if="eligible_cash_back_percentage_display==1">

																	<td style="border:none;" style="font-size:14px;">
																		<span
																			ng-if="eligible_cash_back_percentage_display==1">
																			<i class="fa fa-heart"
																				style="color:#ff0000"></i> Cashback to
																			be credited to your wallet
																			{{eligible_cash_back_percentage}}% </span>
																		<span class="text-right"
																			ng-if="eligible_cash_back_percentage_display==1"
																			style="width:30%;font-size: .9em;">
																			<?php echo curr_sym; ?>{{grandTotal_for_cash_back
																			| number:0}}
																		</span>
																	</td>
																</tr>

															</tfoot>

														</table>
													</td>
												</tr>
											
</tbody>
										</table>


										</div>
									</div>
								</div>

								
								<div class="cart_navigation">
									<button class="prev-btn preventDflt" data-toggle="tab"
										href="#tab-2">Previous</button>
									<button class="next-btn preventDflt" data-toggle="tab" id="tabtoordersumm_btn"
										href="#tab-4" ng-click="continueToPaymentSection()">Continue</button>
								</div>
								<!--order-detail-content--->
								<!---tab---->
							</div><!--page container----------->
						</div>
					</div><!---tab---->

					<div id="tab-4" class="tab-panel" ng-controller="PaymentController">

						<!--vertical tab--->

						<div id="order-detail-content">

							<div class="bhoechie-tab-container">
								<div class="row">
									<?php
									foreach ($payment_options_obj as $payment_options) {
										$op_name = $payment_options->payment_option_name;
									}
									if (count($payment_options_obj) == 1 && $op_name!="Razorpay") {
										//if(0){
										?>

									<div class="col-md-9 col-md-9 col-sm-9 col-xs-9">
										<div class="list-group">
											<?php

											$str = '';

											//print_r($payment_options_obj);
										
											foreach ($payment_options_obj as $payment_options) {

												$op_name = $payment_options->payment_option_name;
												if (($wallet_amount == false || $wallet_amount == 0) && strtolower($op_name) == "wallet") {
													continue;
												}

												if ($op_name == "COD") {
													$active = "active";
													$disabled = "";
													$payment_option_name = $op_name;
													$payment_option_name_id = str_replace(' ', '_', $op_name);
												} else {
													$active = "";
													$disabled = "";
													if ($op_name != "coupon" && $op_name != "Wallet") {
														$payment_option_name_id = "payment_gateway_" . str_replace(' ', '_', $op_name);
													} else {
														$payment_option_name_id = str_replace(' ', '_', $op_name);
													}

													$payment_option_name = ucwords($op_name);

												}

												$icon = '';

												if ($op_name == "COD") {
													$icon = '<i class="fa fa-money fa-2x" aria-hidden="true"></i>';
												}

												if ($op_name == "Credit Card" || $op_name == "Debit Card") {
													$icon = '<i class="fa fa-credit-card fa-2x" aria-hidden="true"></i>';
												}
												if ($op_name == "Net Banking") {
													$icon = '<i class="fa fa-wifi fa-2x" aria-hidden="true"></i>';
												}
												if ($op_name == "EMI") {
													$icon = '<i class="fa fa-calendar fa-2x" aria-hidden="true"></i>';
												}
												if ($op_name == "Coupon") {
													$icon = '<i class="fa fa-gift fa-2x" aria-hidden="true"></i>';
												}
												if ($op_name == "Wallet") {
													$icon = '<i class="fa fa-money fa-2x" aria-hidden="true"></i>';
												}
												if ($op_name == "Paytm") {
													$icon = '<i class="fa fa-money fa-2x" aria-hidden="true"></i>';
												}
												if ($op_name == "Razorpay") {
													$icon = '<i class="fa fa-money fa-2x" aria-hidden="true"></i>';
												}
												?>

											<a href="#" payment_method="<?php echo $op_name; ?>"
												ng-click="payment_method_fun('<?php echo $op_name; ?>')"
												id="<?php echo $payment_option_name_id; ?>"
												class="list-group-item <?php echo $active; ?> text-center"
												style="padding:2px 5px;display:none;">
												<?php echo $icon; ?> <br />
												<?php echo $payment_option_name ?>
											</a>

											<?php
											}
											//echo $str;
											?>

										</div>



										<?php foreach ($payment_options_obj as $payment_options) {

											if ($payment_options->payment_option_name == "COD") {
												$active = "active";
												$disabled = "";
												$payment_option_name = $payment_options->payment_option_name;
												$payment_option_name_id = "content_" . str_replace(' ', '_', $payment_options->payment_option_name);

											} else {
												$active = "";
												$disabled = "";
												if ($payment_options->payment_option_name != "coupon" && $payment_options->payment_option_name != "Wallet") {
													$payment_option_name_id = "content_payment_gateway_" . str_replace(' ', '_', $payment_options->payment_option_name);
												} else {
													$payment_option_name_id = "content_" . str_replace(' ', '_', $payment_options->payment_option_name);
												}

												$payment_option_name = ucwords($payment_options->payment_option_name);

											}


											?>


										<div class="bhoechie-tab-content" id="<?php echo $payment_option_name_id; ?>">
											<!-- <div><img src="assets/data/<?php echo strtolower($payment_option_name); ?>.svg" style="width: 20%;"></div> -->
													<div class="brandtext">
														Upon order confirmation from our team, we will send you a payment link
														or payment QR code. <br>
														Kindly give your consent before proceeding ahead to place the order.
													</div>

													<div class="form-group align-top">
														<label><input type="checkbox" ng-model="i_agree_payment_terms_chk"
																name="i_agree_payment_terms_chk"
																style="transform: scale(1.5,1.5);"> <span
																style="font-size: 1.2em;line-height: 25px;margin-left: 10px;">
																<strong>I Agree</strong></span></label>
													</div>


													<div class="cart_navigation cod_align mb-20 row ml-0">
														<div class="col-md-4 col-md-offset-right-8">

															<div class="row font_alignment"><span
																	class="text-left pull-left">Total Amount </span><span
																	class="text-right pull-right">
																	<?php echo curr_sym; ?>{{payment}}
																</span></div>

															<div class="row font_alignment"> <span
																	class="text-left pull-left">Amount Payable </span> <span
																	class="text-center pull-right">
																	<?php echo curr_sym; ?>{{payment}}
																</span></div>

															<?php

															//echo $order_id;
															?>

															<form method="post" id="paymentgateway_form" action="#">

																<table border="1">
																	<tbody>

																		<input type="hidden" name="ORDER_ID" id="ORDER_ID"
																			value='<?php echo $order_id; ?>'>
																		<input type="hidden" name="CUST_ID"
																			value='<?php echo $this->session->userdata("customer_id"); ?>'>

																		<input type="hidden" name="INDUSTRY_TYPE_ID"
																			value="Retail">

																		<input type="hidden" name="CHANNEL_ID" value="WEB">
																		<input type="hidden" name="TXN_AMOUNT" id="TXN_AMOUNT">




																	</tbody>
																</table>

															</form>
															<?php
															$description = "Product Description";
															$txnid = date("YmdHis");
															$currency_code = $currency_code;
															$total = (1 * 100); // 100 = 1 indian rupees
															$amount = 1;
															// $merchant_order_id  = "PKSBZR-".date("YmdHis");
															$card_holder_name = $this->session->userdata("customer_name");
															$email = $this->session->userdata("customer_email");
															$phone = $this->session->userdata("customer_mobile");
															//$name               = "RazorPay Infovistar";
															$name = "VoometStudio";
															?>

															<form name="razorpay-form" id="razorpay-form"
																action="<?php echo $callback_url; ?>" method="POST">
																<input type="hidden" name="ORDER_ID" id="ORDER_ID"
																	value='<?php echo $order_id; ?>'>
																<input type="hidden" name="CUST_ID"
																	value='<?php echo $this->session->userdata("customer_id"); ?>'>
																<input type="hidden" name="razorpay_payment_id"
																	id="razorpay_payment_id" />
																<input type="hidden" name="razorpay_order_id"
																	id="razorpay_order_id" />
																<input type="hidden" name="razorpay_signature"
																	id="razorpay_signature" />
																<input type="hidden" name="merchant_order_id"
																	id="merchant_order_id" value="<?php echo $order_id; ?>" />
																<input type="hidden" name="merchant_trans_id"
																	id="merchant_trans_id" value="<?php echo $txnid; ?>" />
																<input type="hidden" name="merchant_product_info_id"
																	id="merchant_product_info_id"
																	value="<?php echo $description; ?>" />
																<input type="hidden" name="merchant_surl_id"
																	id="merchant_surl_id" value="<?php echo $surl; ?>" />
																<input type="hidden" name="merchant_furl_id"
																	id="merchant_furl_id" value="<?php echo $furl; ?>" />
																<input type="hidden" name="card_holder_name_id"
																	id="card_holder_name_id"
																	value="<?php echo $card_holder_name; ?>" />
																<input type="hidden" name="merchant_total" id="merchant_total"
																	value="0" />
																<input type="hidden" name="merchant_amount" id="merchant_amount"
																	value="{{payment*100}}" />
															</form>


															<input style="display: none;" id="pay-btn" type="submit"
																onclick="razorpaySubmit(this);" value="Pay Now"
																class="btn btn-primary preventDflt" />
														</div>
													</div>







												</div>

											<?php } ?>

											<div id="mydiv_pay">
												<img src="<?php echo base_url(); ?>assets/pictures/images/loading.gif"
													class="ajax-loader_pay" />
											</div>

										</div>
										<?php
									}
									
									else{
									//if (count($payment_options_obj) > 1) {
										//if(1){
										?>
										<div class="col-md-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
											<div class="list-group py-10">
												<?php

												$str = '';

												//print_r($payment_options_obj);
											
												foreach ($payment_options_obj as $payment_options) {

													$op_name = $payment_options->payment_option_name;
													if (($wallet_amount == false || $wallet_amount == 0) && strtolower($op_name) == "wallet") {
														continue;
													}

													if ($op_name == "COD") {
														$active = "active";
														$disabled = "";
														$payment_option_name = $op_name;
														$payment_option_name_id = str_replace(' ', '_', $op_name);
													} else {
														$active = "";
														$disabled = "";
														if ($op_name != "coupon" && $op_name != "Wallet") {
															$payment_option_name_id = "payment_gateway_" . str_replace(' ', '_', $op_name);
														} else {
															$payment_option_name_id = str_replace(' ', '_', $op_name);
														}

														$payment_option_name = ucwords($op_name);

													}

													$icon = '';

													if ($op_name == "COD") {
														$icon = '<i class="fa fa-money fa-2x" aria-hidden="true"></i>';
													}

													if ($op_name == "Credit Card" || $op_name == "Debit Card") {
														$icon = '<i class="fa fa-credit-card fa-2x" aria-hidden="true"></i>';
													}
													if ($op_name == "Net Banking") {
														$icon = '<i class="fa fa-wifi fa-2x" aria-hidden="true"></i>';
													}
													if ($op_name == "EMI") {
														$icon = '<i class="fa fa-calendar fa-2x" aria-hidden="true"></i>';
													}
													if ($op_name == "Coupon") {
														$icon = '<i class="fa fa-gift fa-2x" aria-hidden="true"></i>';
													}
													if ($op_name == "Wallet") {
														$icon = '<i class="fa fa-money fa-2x" aria-hidden="true"></i>';
													}
													if ($op_name == "Paytm") {
														$icon = '<i class="fa fa-money fa-2x" aria-hidden="true"></i>';
													}
													if ($op_name == "Razorpay") {
														$icon = '<i class="fa fa-money fa-2x" aria-hidden="true"></i>';
													}
													?>

													<a href="#" payment_method="<?php echo $op_name; ?>"
														ng-click="payment_method_fun('<?php echo $op_name; ?>')"
														id="<?php echo $payment_option_name_id; ?>"
														class="list-group-item <?php echo $active; ?> text-center">
														<?php
														if ($op_name == "Razorpay") {
															?>
															<img src="assets/data/<?php echo strtolower($payment_option_name); ?>.svg"
																style="width:50%;">
															<?php
														} else {
															?>
															<?php echo $payment_option_name; ?>
															<?php
														}
														?>

													</a>

													<?php
												}
												//echo $str;
												?>

											</div>
										</div>
										<div class="col-md-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">


											<?php foreach ($payment_options_obj as $payment_options) {

												if ($payment_options->payment_option_name == "COD") {
													$active = "active";
													$disabled = "";
													$payment_option_name = $payment_options->payment_option_name;
													$payment_option_name_id = "content_" . str_replace(' ', '_', $payment_options->payment_option_name);

												} else {
													$active = "";
													$disabled = "";
													if ($payment_options->payment_option_name != "coupon" && $payment_options->payment_option_name != "Wallet") {
														$payment_option_name_id = "content_payment_gateway_" . str_replace(' ', '_', $payment_options->payment_option_name);
													} else {
														$payment_option_name_id = "content_" . str_replace(' ', '_', $payment_options->payment_option_name);
													}

													$payment_option_name = ucwords($payment_options->payment_option_name);

												}


												?>


												<div class="bhoechie-tab-content" id="<?php echo $payment_option_name_id; ?>"
													selected_payment_option_name="<?php echo $payment_option_name; ?>">
													
													
													
													<div class="brandtext" ng-show="false">
														Upon order confirmation from our team, we will send you a payment link
														or payment QR code. <br>
														Kindly give your consent before proceeding ahead to place the order.
													</div>

													<div class="form-group align-top" ng-show="false">
														<label><input type="checkbox" ng-model="i_agree_payment_terms_chk"
																name="i_agree_payment_terms_chk"
																style="transform: scale(1.5,1.5);"> <span
																style="font-size: 1.2em;line-height: 25px;margin-left: 10px;">
																<strong>I Agree</strong></span></label>
													</div>
													

													<div class="cart_navigation cod_align mb-20">
														<div class="text-danger wallet_insufficient_balance_div align-bottom">
														</div>
														<div class="col-md-4 col-md-offset-right-8">

															<div class="row font_alignment"><span
																	class="text-left pull-left">Total Amount </span><span
																	class="text-right pull-right">
																	<?php echo curr_sym; ?>{{payment}}
																</span></div>

															<div class="row font_alignment"> <span
																	class="text-left pull-left">Amount Payable </span> <span
																	class="text-center pull-right">
																	<?php echo curr_sym; ?>{{payment}}
																</span></div>

															<?php

															//echo $order_id;
															?>

															<form method="post" id="paymentgateway_form" action="#">

																<table border="1">
																	<tbody>

																		<input type="hidden" name="ORDER_ID" id="ORDER_ID"
																			value='<?php echo $order_id; ?>'>
																		<input type="hidden" name="CUST_ID"
																			value='<?php echo $this->session->userdata("customer_id"); ?>'>

																		<input type="hidden" name="INDUSTRY_TYPE_ID"
																			value="Retail">

																		<input type="hidden" name="CHANNEL_ID" value="WEB">
																		<input type="hidden" name="TXN_AMOUNT" id="TXN_AMOUNT">




																	</tbody>
																</table>

															</form>
															<?php
															$description = "Product Description";
															$txnid = date("YmdHis");
															$currency_code = $currency_code;
															$total = (1 * 100); // 100 = 1 indian rupees
															$amount = 1;
															// $merchant_order_id  = "PKSBZR-".date("YmdHis");
															$card_holder_name = $this->session->userdata("customer_name");
															$email = $this->session->userdata("customer_email");
															$phone = $this->session->userdata("customer_mobile");
															//$name               = "RazorPay Infovistar";
															$name = "VoometStudio";
															?>

															<form name="razorpay-form" id="razorpay-form"
																action="<?php echo $callback_url; ?>" method="POST">
																<input type="hidden" name="ORDER_ID" id="ORDER_ID"
																	value='<?php echo $order_id; ?>'>
																<input type="hidden" name="CUST_ID"
																	value='<?php echo $this->session->userdata("customer_id"); ?>'>
																<input type="hidden" name="razorpay_payment_id"
																	id="razorpay_payment_id" />
																<input type="hidden" name="razorpay_order_id"
																	id="razorpay_order_id" />
																<input type="hidden" name="razorpay_signature"
																	id="razorpay_signature" />
																<input type="hidden" name="merchant_order_id"
																	id="merchant_order_id" value="<?php echo $order_id; ?>" />
																<input type="hidden" name="merchant_trans_id"
																	id="merchant_trans_id" value="<?php echo $txnid; ?>" />
																<input type="hidden" name="merchant_product_info_id"
																	id="merchant_product_info_id"
																	value="<?php echo $description; ?>" />
																<input type="hidden" name="merchant_surl_id"
																	id="merchant_surl_id" value="<?php echo $surl; ?>" />
																<input type="hidden" name="merchant_furl_id"
																	id="merchant_furl_id" value="<?php echo $furl; ?>" />
																<input type="hidden" name="card_holder_name_id"
																	id="card_holder_name_id"
																	value="<?php echo $card_holder_name; ?>" />
																<input type="hidden" name="merchant_total" id="merchant_total"
																	value="0" />
																<input type="hidden" name="merchant_amount" id="merchant_amount"
																	value="{{payment*100}}" />
															</form>


															<input style="display: none;" id="pay-btn" type="submit"
																onclick="razorpaySubmit(this);" value="Pay Now"
																class="btn btn-primary preventDflt" />
														</div>
													</div>







												</div>

											<?php } ?>

											<div id="mydiv_pay">
												<img src="<?php echo base_url(); ?>assets/pictures/images/loading.gif"
													class="ajax-loader_pay" />
											</div>

										</div>


										<?php
									}
									?>



								</div>
							</div>
						</div>
						<!---vertical tab--->

						<div class="cart_navigation">
							<button class="prev-btn preventDflt" data-toggle="tab" href="#tab-3">Previous</button>
							<button href="#" class="next-btn text-center preventDflt"
								ng-click="confirm_order_fun(payment,$event);"
								ng-disabled="buttonClicked==false && i_agree_payment_terms_chk==false">Confirm
								Order</button>
						</div>

					</div><!---tab--->
					<script src="https://checkout.razorpay.com/v1/checkout.js">
					</script>
					<script>

					</script>

					<script type="text/javascript">
						$.extend({
							distinct: function (anArray) {
								var result = [];
								$.each(anArray, function (i, v) {
									if ($.inArray(v, result) == -1) result.push(v);
								});
								return result;
							}
						});
						function PaymentController($scope, $rootScope, Scopes, $http, $timeout) {

							$scope.i_agree_payment_terms_chk = true;

							$scope.promotion_invoice_cash_back = "";
							$scope.promotion_invoice_free_shipping = "";
							$scope.promotion_invoice_discount = "";
							$scope.total_amount_cash_back = "";
							$scope.total_amount_saved = "";
							$scope.total_amount_to_pay = "";
							$scope.eligible_surprise_gift_on_invoice_uid = 0;
							$scope.applies_surprise_gift_on_invoice = 0;
							$scope.promotion_invoice_cashback_quote = "";
							$scope.total_price_without_shipping_price = 0;
							$scope.total_shipping_price = 0;

							Scopes.store('PaymentController', $scope);
							$('#mydiv_pay').hide();

							$scope.unique = function (list) {
								var result = [];
								$.each(list, function (i, e) {
									if ($.inArray(e, result) == -1) result.push(e);
								});
								return result;
							}

							payment_options = '<?php echo json_encode($payment_options_obj); ?>';

							payment_options = JSON.parse(payment_options);

							payment_option_all = [];
							$scope.pay_gateway_first = "";
							for (var k in payment_options) {
								for (var l in payment_options[k]) {
									if (l == "payment_option_name") {
										val = payment_options[k][l];

										if (val == "Credit Card" || val == "Net Banking" || val == "EMI" || val == "Debit Card") {
											if ($scope.pay_gateway_first == "") {
												$scope.pay_gateway_first = val;
											}
											val = "Payment Gateway";

										}
										payment_option_all.push(val);
									}
								}
							}

							payment_option_all = $scope.unique(payment_option_all);

							$scope.payment_method_restrictions_fun = function (payment_method_restrictions, payment_method_cod_max_value) {
								payment_to_be_restricted = $scope.unique(payment_method_restrictions);
								payment_to_be_restricted_str = payment_to_be_restricted.join(",");
								payment_to_be_restricted_arr = payment_to_be_restricted_str.split(",");

								available_payment_options = [];

								$.grep(payment_option_all, function (el) {
									if ($.inArray(el, payment_to_be_restricted_arr) == -1) available_payment_options.push(el);
								})

								first_active = available_payment_options[0];

								for (p = 0; p < payment_to_be_restricted_arr.length; p++) {
									//alert(payment_to_be_restricted_arr[p]);
									if (payment_to_be_restricted_arr[p] == "COD") {

										if ($scope.payment > payment_method_cod_max_value) {
											$("#COD").addClass("disabled");
										}
										else {
											if ($("#COD").hasClass("disabled")) {
												$("#COD").removeClass("disabled");
											}
										}
										$("#COD").removeClass("active");
										$("#cod_content").removeClass("active");
									}
									if (payment_to_be_restricted_arr[p] == "Credit Card") {
										//credit card	//net banking	//EMI	//debit card

										$('[id^=payment_gateway_Credit_Card]').addClass("disabled");
										$('[id^=payment_gateway_Credit_Card]').removeClass("active");;

									}
									if (payment_to_be_restricted_arr[p] == "Net Banking") {
										//credit card	//net banking	//EMI	//debit card

										$('[id^=payment_gateway_Net_Banking]').addClass("disabled");
										$('[id^=payment_gateway_Net_Banking]').removeClass("active");;

									}
									if (payment_to_be_restricted_arr[p] == "EMI") {
										//credit card	//net banking	//EMI	//debit card

										$('[id^=payment_gateway_EMI]').addClass("disabled");
										$('[id^=payment_gateway_EMI]').removeClass("active");

									}
									if (payment_to_be_restricted_arr[p] == "Debit Card") {
										//credit card	//net banking	//EMI	//debit card

										$('[id^=payment_gateway_Debit_Card]').addClass("disabled");
										$('[id^=payment_gateway_Debit_Card]').removeClass("active");;

									}
									if (payment_to_be_restricted_arr[p] == "Payment Gateway") {
										//credit card	//net banking	//EMI	//debit card

										$('[id^=payment_gateway_]').addClass("disabled");
										$('[id^=payment_gateway_]').removeClass("active");;

									}
									if (payment_to_be_restricted_arr[p] == "Coupon") {

										$("#coupon").addClass("disabled");
										$("#coupon").removeClass("active");
									}
									if (payment_to_be_restricted_arr[p] == "Wallet") {
										//$( "#Wallet" ).addClass( "disabled" );
										//$( "#Wallet" ).removeClass( "active" );
									}

								}




								if (available_payment_options[0] != "Payment Gateway") {

									$scope.payment_method = available_payment_options[0].split('_').join(' ');

									$("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
									$("div.bhoechie-tab-menu>div.list-group>a").removeClass("active");
									if ($scope.wallet_amount > $scope.payment) {

										$("#Wallet").addClass("active");
										$("#Wallet").removeClass("disabled");
										$("#content_Wallet").removeClass("disabled");
										$("#content_Wallet").addClass("active");
										$scope.payment_method = "Wallet";

									} else {

										//alert("#"+available_payment_options[0]);

										if (first_active == "Wallet") {
											if ($scope.wallet_amount > $scope.payment) {
												$("div.bhoechie-tab-menu>div.list-group>a").removeClass("active");
												$("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
												$("div.bhoechie-tab>div#content_" + first_active + "").addClass("active");
												$("#" + available_payment_options[0]).addClass("active");

											} else {

												if (available_payment_options[1] != null) {

													if (available_payment_options[1] == "Payment Gateway") {

														$("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
														$("div.bhoechie-tab-menu>div.list-group>a").removeClass("active");

														//$scope.payment_method=$scope.pay_gateway_first;
														//$scope.pay_gateway_first_id=$scope.pay_gateway_first.split(' ').join('_');
														//$("div.bhoechie-tab>div#content_payment_gateway_"+$scope.pay_gateway_first_id).addClass("active");
														//$("#payment_gateway_"+$scope.pay_gateway_first_id).addClass("active");	
													}
												}
											}

										} else {

											$("div.bhoechie-tab-menu>div.list-group>a").removeClass("active");
											$("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
											$("div.bhoechie-tab>div#content_" + first_active + "").addClass("active");
											$("#" + available_payment_options[0]).addClass("active");
										}

									}

								} else {

									$("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
									$("div.bhoechie-tab-menu>div.list-group>a").removeClass("active");

									if ($scope.wallet_amount > $scope.payment) {
										$scope.payment_method = "Wallet";
										$("#Wallet").addClass("active");
										$("#Wallet").removeClass("disabled");
										$("#content_Wallet").removeClass("disabled");
										$("#content_Wallet").addClass("active");
									} else {
										$scope.payment_method = $scope.pay_gateway_first;
										$scope.pay_gateway_first_id = $scope.pay_gateway_first.split(' ').join('_');

										$("div.bhoechie-tab>div#content_payment_gateway_" + $scope.pay_gateway_first_id).addClass("active");
										$("#payment_gateway_" + $scope.pay_gateway_first_id).addClass("active");
									}
								}

								$("div.bhoechie-tab-menu>div.list-group>a").each(function () {
									if (!$(this).hasClass("disabled")) {
										idofpaymentmethod = $(this).attr("id");
										$("div.bhoechie-tab>div#content_" + idofpaymentmethod).addClass("active");
										$(this).addClass("active");
										$scope.payment_method = $(this).attr("payment_method");
										return false;
									}
								});

							}

							$scope.payment_method_fun = function (val) {
								var payment_method = val;
								//alert(payment_method);
								disableflag = "no";
								$("div.bhoechie-tab-menu>div.list-group>a").each(function () {
									if ($(this).hasClass("disabled")) {
										if ($(this).attr("payment_method") == payment_method) {
											disableflag = "yes";
										}
									}
								});
								if (disableflag == "no") {
									$scope.payment_method = payment_method;
								}
							}

							$scope.amount_fun = function (grandtotal_without_discount, grandTotal_for_discount, total_shipping_price_actual, you_saved, grandTotal_for_cash_back, total_cash_back_for_sku, total_cash_back, amount_to_pay, eligible_surprise_gift_on_invoice_uid, applies_surprise_gift_on_invoice, promotion_invoice_cashback_quote, total_price_without_shipping_price, total_shipping_price, total_coupon_used_amount, products_coupon, invoice_coupon, invoice_coupon_applied_status, inv_addon_products_amount) {


								$scope.applies_surprise_gift_on_invoice = applies_surprise_gift_on_invoice;

								$scope.promotion_invoice_cash_back = grandTotal_for_cash_back;
								$scope.promotion_invoice_free_shipping = total_shipping_price_actual;
								$scope.promotion_invoice_discount = grandTotal_for_discount;
								$scope.total_amount_cash_back = total_cash_back;
								$scope.total_amount_saved = you_saved;
								$scope.total_amount_to_pay = amount_to_pay;
								$scope.eligible_surprise_gift_on_invoice_uid = eligible_surprise_gift_on_invoice_uid;
								$scope.total_price_without_shipping_price = total_price_without_shipping_price;
								$scope.total_shipping_price = total_shipping_price;
								$scope.total_coupon_used_amount = total_coupon_used_amount;
								$scope.products_coupon = products_coupon;
								$scope.invoice_coupon = invoice_coupon;
								$scope.invoice_coupon_applied_status = invoice_coupon_applied_status;
								$scope.inv_addon_products_amount = inv_addon_products_amount;

								$scope.payment_method = $scope.payment_method;
								if (promotion_invoice_cashback_quote != null) {
									$scope.promotion_invoice_cashback_quote = promotion_invoice_cashback_quote;
								}

								$scope.wallet_amount = '<?php echo ($wallet_amount) ? $wallet_amount : 0 ?>';

								$scope.payment = amount_to_pay;

								if ($scope.wallet_amount > $scope.payment) {
									$scope.payment_method = "Wallet";
									$("#Wallet").addClass("active");
									$("#Wallet").removeClass("disabled");
									$("#content_Wallet").removeClass("disabled");
									$("#content_Wallet").addClass("active");

								} else {
									//$( "#Wallet" ).addClass( "disabled" );
									//$( "#Wallet" ).removeClass( "active" );
									$("#Wallet_content").removeClass("active");
								}

							}
							if (Scopes.get('OrderController').amount_payable != null) {
								$scope.payment = Scopes.get('OrderController').amount_payable;
							} else {
								$scope.payment = 0;
								$scope.payment_method = "";
							}
							$scope.buttonClicked = false;
							$scope.confirm_order_fun = function (payment, el) {
								var wallet_amount_insufficient_flag = "no";
								$(".bhoechie-tab-content").each(function () {
									if ($(this).hasClass("active")) {
										var wallet_amount_for_compare = "<?php echo $wallet_amount; ?>";
										if ($(this).attr("selected_payment_option_name") == "Wallet") {
											if (wallet_amount_for_compare < $scope.payment) {
												$(".bhoechie-tab-content.active .wallet_insufficient_balance_div").html("There is insufficient balance in your wallet. Please select other payment method(s)")
												wallet_amount_insufficient_flag = "yes";
												return false;
											}
											else {
												$(".bhoechie-tab-content.active .wallet_insufficient_balance_div").html("")
											}
										}
										else {
											$(".bhoechie-tab-content.active .wallet_insufficient_balance_div").html("")
										}
									}
								});
								if (wallet_amount_insufficient_flag == "yes") {
									return false;
								}
<?php
$this->session->set_userdata("cur_checkout", "checkout");
?>
									$scope.buttonClicked = true;
								///////
								//$('#mydiv_pay').show();
								$('#loading').show(); // common page loader when submit confirm order


								if ($scope.promotion_invoice_cash_back > 0 || $scope.promotion_invoice_free_shipping > 0 || $scope.promotion_invoice_discount > 0 || $scope.applies_surprise_gift_on_invoice == 1 || $scope.invoice_coupon_applied_status == 1) {
									$invoice_offer_achieved = 1;
								} else {
									$invoice_offer_achieved = 0;
								}

								order_id = document.getElementById("ORDER_ID").value;
								document.getElementById("TXN_AMOUNT").value = $scope.total_amount_to_pay;
								//test_amount=($(".cart_summary").length-1)*1;
								//document.getElementById("TXN_AMOUNT").value=test_amount;

								var FormData = { "promotion_invoice_cash_back": $scope.promotion_invoice_cash_back, "promotion_invoice_free_shipping": $scope.promotion_invoice_free_shipping, "promotion_invoice_discount": $scope.promotion_invoice_discount, "total_amount_cash_back": $scope.total_amount_cash_back, "total_amount_saved": $scope.total_amount_saved, "total_amount_to_pay": $scope.total_amount_to_pay, "payment_method": $scope.payment_method, "invoice_offer_achieved": $invoice_offer_achieved, "wallet_amount": $scope.wallet_amount, "eligible_surprise_gift_on_invoice_uid": $scope.eligible_surprise_gift_on_invoice_uid, "promotion_invoice_cashback_quote": $scope.promotion_invoice_cashback_quote, "total_price_without_shipping_price": $scope.total_price_without_shipping_price, "total_shipping_charge": $scope.total_shipping_price, "order_id": order_id, "total_coupon_used_amount": $scope.total_coupon_used_amount, "products_coupon": $scope.products_coupon, "invoice_coupon": $scope.invoice_coupon, "inv_addon_products_amount": $scope.inv_addon_products_amount };
								//this is passed to take copon data


								//console.log(FormData);
								//alert($scope.total_coupon_used_amount);
								$http({
									method: "POST",
									url: "<?php echo base_url(); ?>Account/confirm_order_process_temp",
									data: FormData,
									dataType: "JSON",
								}).success(function mySucces(result) {

									//console.log(result);
									//alert('ajax call finished ');
									//return false;

									//location.href = "<?php //echo base_url(); ?>Account/confirm_order_process/" + order_id;
									//return false;
									if ($scope.payment_method == 'Paytm') {
										$("#paymentgateway_form").attr('action', '<?php echo base_url() ?>assets/paytm/pgRedirect.php');
										$("#paymentgateway_form").submit();
									}

									if ($scope.payment_method == 'Razorpay') {

										//alert('Razorpay');
										amt = (payment * 100);
										if (amt % 1 === 0) {
											//if number is integer
											L_KEY_NUMERIC = true;
											amt_inpaise = (parseFloat(amt));
											//alert(1 + "dfdf"+amt_inpaise);
										} else {
											//if number is float
											L_KEY_NUMERIC = true;
											amt_inpaise = (parseFloat(amt).toFixed(2));
											//alert(2 + "dfdf"+amt_inpaise);
										}

										$('#merchant_total').val(amt_inpaise);

										/*var options = {
										key:            "<?php echo razor_pay_key; ?>",
										amount:         amt_inpaise,
										name:           "<?php echo $name; ?>",
										description:    "Order # <?php echo $order_id; ?>",
										netbanking:     true,
										currency:       "<?php echo $currency_code; ?>", // INR
										prefill: {
										name:       "<?php echo $card_holder_name; ?>",
										email:      "<?php echo $email; ?>",
										contact:    "<?php echo $phone; ?>"
										},
										handler: function (transaction) {
										console.log(transaction);
										$('#razorpay_payment_id').val(transaction.razorpay_payment_id);
										$("#razorpay-form").submit();
										},
										"modal": {
										"ondismiss": function(){
										location.reload()
										}
										}
										};
										*/
										/* added  new */
										$.ajax({
											'type': 'POST',
											'url': '<?php echo base_url('razorpay/create_order'); ?>',
											'data': { 'amount': amt_inpaise, 'currency': 'INR' },
											'dataType': 'json',
											success: function (response) {
												console.log(response);
												//alert(response.razorpay_order_id);
												//return false;
												//location.href = '{{url('user-payment-settings')}}';

												var options = {


													//theme:{"color" : "#F37254" }

													key: "<?php echo razor_pay_key; ?>",
													amount: amt_inpaise,
													name: "<?php echo $name; ?>",
													description: "Order # <?php echo $order_id; ?>",
													netbanking: true,
													currency: "<?php echo $currency_code; ?>", // INR
													order_id: response.razorpay_order_id,
													//image: 'https://i.imgur.com/n5tjHFD.png',
													prefill: {
														name: "<?php echo $card_holder_name; ?>",
														email: "<?php echo $email; ?>",
														contact: "<?php echo $phone; ?>"
													},
													handler: SuccessHandler,
													"modal": {
														"ondismiss": function () {
															location.reload()
														}
													}

												}

												/* var rzp = new Razorpay(options);
												rzp.open();*/

												var razorpay_pay_btn, instance;

												//function razorpaySubmit() {
												if (typeof Razorpay == 'undefined') {
													setTimeout(razorpaySubmit, 200);
													if (!razorpay_pay_btn && el) {
														razorpay_pay_btn = el;
														el.disabled = true;
														el.value = 'Please wait...';
													}
												} else {
													if (!instance) {
														instance = new Razorpay(options);
														if (razorpay_pay_btn) {
															razorpay_pay_btn.disabled = false;
															razorpay_pay_btn.value = "Pay Now";
														}
													}
													instance.open();
												}

												//e.preventDefault();
												/*document.getElementById('rzp-button1').onclick = function(e){
												}*/
											}
										});

										/* added  new */

									}
									/* commented because after integrating payment gateway starts*/
									/*
									if(result=="no_availability"){
									bootbox.alert({ 
									size: "small",
									message: 'Shipping is not available for given delivery address',
									});
									return false;
									}else if(result){
									$('#mydiv_pay').hide();
									//alert("Order has been confirmed");
									localStorage.removeItem('LocalCustomerCart');
									//document.getElementById("paymentgateway_form").submit();
									location.href="<?php echo base_url() ?>thankyou/"+result
									
									}else{
									bootbox.alert({ 
									size: "small",
									message: 'Sorry..! somthing went to wrong',
									});
									}
									*/
									/* commented because after integrating payment gateway ends*/
								}, function myError(response) {

								});
								///////
							}
						}

						function SuccessHandler(transaction) {
							//alert('success handler');
							// You can write success code here. If you want to store some data in database.
							if (transaction != null) {
								$.ajax({
									method: 'post',
									url: '<?php echo base_url('razorpay/verify_signature'); ?>',
									data: $('#validate_form').serialize() + '&razorpay_payment_id=' + transaction.razorpay_payment_id + '&razorpay_order_id=' + transaction.razorpay_order_id + '&razorpay_signature=' + transaction.razorpay_signature,
									dataType: "json",
									beforeSend: function () {
										//  $('.loader').show();
									},
									complete: function (r) {
										//alert('verified successfully');
										//alert(r);
										// console.log('finalsuccess function');
										//console.log(r);
										//return false;
										if (parseInt(r.status) == 200) {
											console.log(transaction);
											$('#razorpay_payment_id').val(transaction.razorpay_payment_id);
											$('#razorpay_order_id').val(transaction.razorpay_order_id);
											//alert(transaction.razorpay_payment_id);
											//return false;
											$("#razorpay-form").submit();
										} else {
											location.reload();
										}
										/*alert(r);
										console.log(r);
										*/
										//return false;
										//console.log('complete');
									}
								});
							}
						}

					</script>



				</div><!--tab container------>

			</div> <!---popular-tabs---->

		</div><!--page content----------->

	</div><!----container------------>
</div><!-----columns-container----------->

<!-- ./page wapper-->

<style>
	.more-less {
		float: left;
		color: #212121;
	}
</style>

<script type="text/javascript">


	$(document).ready(function () {

		function toggleIcon(e) {
			$(e.target)
				.prev('.panel-heading')
				.find(".more-less")
				.toggleClass('glyphicon-plus glyphicon-minus');
		}
		$('.panel-group').on('hidden.bs.collapse', toggleIcon);
		$('.panel-group').on('shown.bs.collapse', toggleIcon);


		activaTab();
		$("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
			e.preventDefault();

			disabled = $(this).hasClass("disabled");

			if (disabled == true) {
				alert('This payment is restricted');

				/*bootbox.alert({
				size: "small",
				message: 'This payment is restricted',
				});*/
				return false;
			}
			$(this).siblings('a.active').removeClass("active");
			$(this).addClass("active");
			var index = $(this).index();
			$("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
			$("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
		});


		$('.next-btn').click(function () {
			// $('.step li.active').next('li').removeClass('disabled');
			// $('.step li.active').next('li').find('a').attr("data-toggle","tab");
			//$('html, body').animate({scrollTop:0},700);
			var nextId = $(this).parents('.tab-panel').next().attr("id");
			$('[href=#' + nextId + ']').tab('show');
		});

		$('.prev-btn').click(function () {
			$('.step li.active').removeClass('active');
			//$('html, body').animate({scrollTop:0},700);
			var prevId = $(this).parents('.tab-panel').prev().attr("id");
			$('[href=#' + prevId + ']').tab('show');
		});


	});

	function activaTab() {

		c_id = '<?php echo $this->session->userdata("customer_id"); ?>';

		if (c_id != null && c_id != '') {
			$('.step li.active').next('li').removeClass('disabled');
			$('.step li.active').next('li').find('a').attr("data-toggle", "tab");
			$('.step a[href="#tab-2"]').tab('show');
			$('.step li').not('.active,.step li:first').addClass('disabled');
			$('.step li').not('.active,.step li:first').find('a').removeAttr("data-toggle");

		} else {

			$('.step li').not('.active').addClass('disabled');
			$('.step li').not('.active').find('a').removeAttr("data-toggle");
		}

	};


</script>