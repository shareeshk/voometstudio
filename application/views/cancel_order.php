<div class="columns-container">
    <div class="container" id="columns">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>Account/my_order">My Orders</a></li>
		  <li class="breadcrumb-item active">Cancellation of an order</li>
		</ol>
        <div class="row">
            <div class="center_column" id="center_column">
				<?php require_once 'cancel_order_part1.php'; ?>
				
				<?php 
			$inv_obj=$controller->get_details_of_products($inventory_id);
			$shipp_obj=$controller->get_shipping_address_of_order($order_id);			
			if(!empty($inv_obj)){
                            $pro_name=($inv_obj->sku_name!='') ? $inv_obj->sku_name: $inv_obj->product_name;
			?>
			<input type="hidden" id="return_product_id" return_product_id="<?php echo $inv_obj->product_id;?>">
			<input type="hidden" id="return_product_name" value="<?php  echo $pro_name;?>">
			
			 
			<div class="col-sm-4">
				<h4 class="text-uppercase margin-bottom">Item Details</h4>
				<div class="f-row">
					<img src="<?php echo base_url().$inv_obj->thumbnail;?>" class="img-responsive">
				</div>
				<div class="f-row">
					<?php  echo $pro_name;?>
					<?php
					$product_details=get_product_details($order_item['product_id']);
					if(!empty($product_details)){
						echo ($product_details->product_description!='') ? '<div class="mb-10">'.$product_details->product_description.'</div>': '';
					} 
					?>
					
				</div>
				<div class="small-text">
										
						<?php
					$inv_obj->attribute_1_value= substr($inv_obj->attribute_1_value, 0, strpos($inv_obj->attribute_1_value, ":"));
					
					if(!empty($inv_obj->attribute_1))
						echo '<div class="f-row">'.$inv_obj->attribute_1." : ".$inv_obj->attribute_1_value.'</div>';
					if(!empty($inv_obj->attribute_2))
						echo '<div class="f-row">'.$inv_obj->attribute_2." : ".$inv_obj->attribute_2_value.'</div>';
					if(!empty($inv_obj->attribute_3))
						echo '<div class="f-row">'.$inv_obj->attribute_3." : ".$inv_obj->attribute_3_value.'</div>';
					if(!empty($inv_obj->attribute_4))
						echo '<div class="f-row">'.$inv_obj->attribute_4." : ".$inv_obj->attribute_4_value.'</div>';
					?>
				
				</div>
				<div class="f-row small-text">
				Quantity : <?php echo $quantity; ?>
				</div>
										
				<?php if(!empty($shipp_obj)){ ?>
				
				<div class="f-row text-uppercase normal-text margin-top margin-bottom">
					<strong>pick up address</strong>
				</div>
				<div class="f-row small-font">
					<ul class="pull-left">
					  <li><strong class="text-uppercase small-font"><?php echo $shipp_obj->customer_name; ?></strong></li>
					  <li><?php echo $shipp_obj->address1; ?>,<?php echo $shipp_obj->address2; ?></li>
					  <li><?php echo $shipp_obj->city; ?>,<?php echo $shipp_obj->state; ?></li>
					  <li><?php echo $shipp_obj->pincode; ?>,</li>
					  <li><?php echo $shipp_obj->country; ?></li>
					  <li><?php echo "Phone: ".$shipp_obj->mobile; ?></li>
					  
					</ul>
				</div>
				
				<?php } ?>
			</div>
			
			<?php 
				
			} ?>
				
              

            		<div class="col-sm-8">
					<h4 class="text-uppercase margin-bottom">Cancellation</h4>
					
            			<form name="myForm" id="myForm" method="post" class="form-horizontal">
						
							 <?php
                                if($promotion_invoice_discount>0){

									$subtotal=$order_item["subtotal"];
									$invoice_discount=($subtotal/$total_price_without_shipping_price)*100;					
									$each_invoice_amount=$promotion_invoice_discount*$invoice_discount/100;
									//$invoice_amount=round($promotion_invoice_discount-$each_invoice_amount,2);
									$order_item_invoice_discount_value=round($each_invoice_amount,2);
									$order_item_invoice_discount_value_each=round($order_item_invoice_discount_value/$quantity,2);
									//$order_item_invoice_discount_value=round($promotion_invoice_discount/$cart_quantity,2);
									
                                ?>
								
								
									<div class="form-group">
										<label class="alert alert-warning">
										<i>There is discount offer on invoice.If You request for Cancel the <b> Discounted price will not be refunded.</b> </i>
										</label>
									</div>
							

								<?php }else{
									$order_item_invoice_discount_value=0;
									$order_item_invoice_discount_value_each=0;
								} ?>
								
						<input type="hidden" value="<?php echo $order_item_invoice_discount_value; ?>" name="order_item_invoice_discount_value" id="order_item_invoice_discount_value">
						
            			
            				<div class="form-group">
							
							
							<?php 
							$invoice_cash_back_reject='';
							if($promotion_invoice_cash_back>0 && $customer_response_for_invoice_cashback==''){
								$invoice_cash_back_reject=1;
									echo '<label class="alert alert-warning">';
									echo '<i>There is Cash back offer on invoice.If You request for Cancel the <b>Cash back </b> offer will be rejected</i>';	
									echo '</label>';
							}
							?>
							
							<input type="hidden" name="invoice_cash_back_reject" id="invoice_cash_back_reject" value="<?php echo $invoice_cash_back_reject; ?>">
							<input type="hidden" name="total_amount_cash_back" id="total_amount_cash_back" value="<?php echo $total_amount_cash_back; ?>">
							<input type="hidden" name="order_id" id="order_id" value="<?php echo $order_id; ?>">
							
							<?php 
							$invoice_surprise_gift_reject='';
							if($invoice_surprise_gift!='' && $customer_response_for_surprise_gifts==''){
								$invoice_surprise_gift_reject=1;
									echo '<label class="alert alert-warning">';
									echo '<i>There is Surprise gift offer on invoice.If You request for Cancel the <b>Surprise gift</b> offer will be rejected</i>';
									
									echo '</label>';
							}
							?>
							
							<input type="hidden" name="invoice_surprise_gift_reject" id="invoice_surprise_gift_reject" value="<?php echo $invoice_surprise_gift_reject; ?>">
							
							
							<?php 
							$order_item_promotion='';
							if($order_item['promotion_available']==1){
								$order_item_promotion=1;
									echo '<label class="alert alert-warning">';
									echo '<i>There is a promotion available for this order item.If You request for Cancel the <b>Promotion</b> offer will be rejected</i>';
									
									echo '</label>';
							}
							?>
							
							<input type="hidden" name="order_item_promotion" id="order_item_promotion" value="<?php echo $order_item_promotion; ?>">
							
							</div>
							
							<div class="form-group">
							   <label class="control-label col-md-3">Reason For Cancel</label>
							   <div class="col-md-9">
								  <select class="form-control" name="cancel_reason" id="cancel_reason" required onchange="remove_error();">
										<option value="">Please Choose</option>
									<?php foreach($cancel_reason as $reasons){?>
										<option value="<?php echo $reasons['cancel_reason_id'] ?>"><?php echo ucfirst($reasons['reason_name']); ?> </option>
									<?php }?>
								  </select>
							  </div>
							</div>
            			
						
	            			<div class="form-group">
							  <label class="control-label col-md-3">Comment</label>
							  <div class="col-md-9">
							  <textarea class="form-control" rows="3" name="cancel_reason_comment" id="comment" onkeyup="remove_error();"></textarea>
							  </div>
							</div>
						
						
<?php if($controller->get_order_payment_done($order_id)){ ?>
	            			<div class="form-group">
								<label class="control-label col-md-3">Choose refund method</label>
								<div class="col-md-9">

                                    <?php if($order_payment_type=='Razorpay'){ ?>
                                        <label class="checkbox-inline pl-0">
                                          <input type="radio" checked name="refund_method" required onchange="setRefundMethod('<?php echo $order_item_id; ?>')" value="Razorpay"> Razorpay
                                        </label>
                                    <?php }else{ ?>
                                        <label class="checkbox-inline pl-0">
                                            <input type="radio" id="wallet_radio"  name="refund_method" required onchange="setRefundMethod('<?php echo $order_item_id; ?>')" value="Wallet"> Wallet1
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="radio" name="refund_method" required onchange="setRefundMethod('<?php echo $order_item_id; ?>')" value="Bank Transfer"> Bank Transfer
                                        </label>
                                    <?php } ?>

							<label class="checkbox-inline">
								<span id="error_refund_method" class="red"></span>
							</label>
							</div>
						</div>
						
						
						

					<div class="margin-top" id="bank_account_data" style="display: none;">
						<?php
								if(count($bank_detail)!=0){
									?>
						<div class="form-group">
							
								<div class="col-sm-9 col-sm-offset-3">
									<button type="button"  data-toggle="modal" href="#all_bank_accounts" data-backdrop="static" class="button preventDflt"> Use Saved Bank Accounts</button> <button type="button" onclick="add_new_bank_data()" id="add_new_bank_details_button" class="button preventDflt" style="display:none"> Reset</button>
								</div>
							
						</div>
								<?php
								}
									?>
	<div class="modal" id="all_bank_accounts">
     <div class="modal-dialog modal-lg">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close preventDflt" style="text-align: right" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 class="modal-title"> Your Total Bank Account With Us : 
				<?php 
				
					if(!empty($bank_detail[0])){
						echo count($bank_detail[0]);
					}else{
						echo "0";
					}
					?>
			  </h4>
           </div>
           <div  class="modal-body">
					<div class="row">
					
				<?php
				
				$i=0; foreach($bank_detail[0] as $bank_data){ ?>
				<div class="col-sm-4">
						<div class="well">
						<dl>
						
										<dt><strong>Bank Name</strong></dt> 
										<dd id="bank_name_<?php echo $i ?>"><?php echo $bank_data['bank_name']?></dd>
										<dt><strong>Branch Name</strong></dt> 
										<dd id="branch_name_<?php echo $i ?>"><?php echo $bank_data['branch_name']?></dd>
										<dt><strong>City</strong></dt> 
										<dd id="city_<?php echo $i ?>"><?php echo $bank_data['city']?></dd>
										<dt><strong>IFSC Code</strong></dt> 
										<dd id="ifsc_<?php echo $i ?>"><?php echo $bank_data['ifsc_code']?></dd>
										<dt><strong>Account Number</strong></dt> 
										<dd id="account_number_<?php echo $i ?>"><?php echo $bank_data['account_number']?></dd>
										<dt><strong>Account Holder Name</strong></dt> 
										<dd id="account_holder_name_<?php echo $i ?>"><?php echo $bank_data['account_holder_name']?></dd>
										<dt><strong>Mobile Number</strong></dt> 
										<dd id="account_holder_numer_<?php echo $i ?>"><?php echo $bank_data['mobile_number']?></dd>
									</dl>
							
							
								<button type="button" class="button btn-block preventDflt" onclick="use_this_account_data(<?php echo $i ?>)">USE THIS</button>
							

					</div>
					</div>

			<?php $i++;} ?>
			
					</div>
           </div>
       
        <div class="modal-footer">
           <a href="#" data-dismiss="modal" id="close"class="btn">Close</a>
        </div>
     </div>
    </div>
  </div>							
							
						
							
								
									<div class="form-group">
									   <label class="control-label col-md-3">Bank name</label> 
										 <div class="col-md-9">
										  <input id="bank_name" name="bank_name" type="text" placeholder="Enter Bank Name"  class="form-control"> 
										  <span class="help-block"></span>  
										  </div>
									</div>
									<div class="form-group">
									  <label class="control-label col-md-3">Branch name</label>    
									  <div class="col-md-9">
									  <input id="bank_branch_name" name="bank_branch_name" type="text" placeholder="Enter Bank Branch Name"  class="form-control"> 
									  <span class="help-block"></span>   
									  </div>
									</div>
									<div class="form-group">
									  <label class="control-label col-md-3">Branch city</label>
									   <div class="col-md-9">
									  <input id="bank_city" name="bank_city" type="text" placeholder="Enter Branch City"  class="form-control"> 
									  <span class="help-block"></span>   
									  </div>
									</div>
									<div class="form-group">
										 <label class="control-label col-md-3">IFSC Code</label>
										 <div class="col-md-9">
										  <input id="bank_ifsc_code" onclick="validate_ifsc()" onkeyup="validate_ifsc()" name="bank_ifsc_code" type="text" placeholder="Enter IFSC code"  class="form-control"> 
										  <span id="errors">IFSC code must be 6 numbers</span>
										  <span class="help-block"></span> 
										 </div>
									</div>
									<div class="form-group">
									  <label class="control-label col-md-3">Account Number</label>  
									 <div class="col-md-9">
									  <input id="bank_account_number"  onfocus="validate_account()" onkeyup="validate_account()" name="bank_account_number" type="text" placeholder="Enter your Bank Account Number"  class="form-control">
									   <span id="errors1">Bank account number must be 15 numbers</span>	
									  <span class="help-block"></span> 							  
									  </div>
									</div>
									
								
								
									<div class="form-group">
									    <label class="control-label col-md-3">Confirm Number</label> 
									  <div class="col-md-9">
									  <input id="bank_account_confirm_number" onfocus="validate_confirm()" onkeyup="validate_confirm()" name="bank_account_confirm_number" type="text" placeholder="Confirm Bank Account Number"  class="form-control">
									   <span id="errors2">Bank account number must be same</span>	
									  <span class="help-block"></span>  
									  </div>
									</div>
								
								
									<div class="form-group">
									  <label class="control-label col-md-3">Account Holder</label>  
									  <div class="col-md-9">
									  <input id="account_holder_name" onkeypress="return isAlfa(event)" name="account_holder_name" type="text" placeholder="Bank Account holder Name"  class="form-control">
									  <span class="help-block"></span>  
									  </div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Phone number</label>
									  <div class="col-md-9">
									  <input id="bank_return_refund_phone_number" onkeyup="ValidateNumber(this)"  onkeypress="return isNumber(event)" name="bank_return_refund_phone_number" type="text"  placeholder="Enter Your Registered Mobile Number" class="form-control" maxlength="16">
									 <span id="errors3" style="color:red;display:none;">Enter 10digit mobile number</span>
									  <span class="help-block"></span>  
									  </div>
									</div>
									<div class="form-group">
									<?php
										$disabled='';
										$saved_bank_details=0;
										if(!empty($bank_detail[0])){

											$saved_bank_details=count($bank_detail[0]);									  
											if($saved_bank_details>=3){
												$disabled='disabled';
											}
										}
									  ?>
									  
									  <div class="col-md-9 col-md-offset-3">
									  <div class="checkbox">
									    <label for="checkboxes-0">
									      <input type="checkbox" name="save_this_bank_data" <?php echo $disabled; ?> id="save_this_bank_data"  value="1">
										   <input type="hidden" name="saved_bank_details" id="saved_bank_details"  value="<?php echo $saved_bank_details; ?>">
									      Save for later use
									    </label>
										</div>
									  </div>
									</div>	            			
								
																
						
					
					
					</div>

				
<?php }else{
	
	
}


 ?>
								
						
			
								<div class="form-group margin-top margin-bottom">
									<div class="col-sm-9 col-md-offset-3">
										<div class="checkbox">
											<label for="checkboxes-1" style="line-height: 21px;">
											  <input type="checkbox" required="" name="accept_terms_and_conditions"  value="1" onclick="remove_error();">
											  I agree to <a href="#">terms and conditions</a> &nbsp;&nbsp;<span id="error_accept_terms_and_conditions" class="red"></span>
											</label>
										</div>
									</div>
								</div>
								
								<?php 
								if(strtolower($order_item['payment_type'])=='cod'){
								?>
								<div class="form-group margin-top margin-bottom small-text">
									<div class="col-sm-12">									
										<strong>Note: </strong> There will be no refund as the order is purchased using Cash-On-Delivery.
									</div>
								</div>
								
								<?php 
								}
								?>
							
									
										<div class="form-group">
										<div class="col-sm-9">
										  <button type="button" id="cancel_order_btn" onclick="submitForm('<?php echo $order_item_id; ?>')" class="button pull-right text-uppercase preventDflt">Confirm cancellation</button>
										</div>
									</div>
								
						
						</form>	
		            </div>
				
            </div>
			
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<script>
function setRefundMethod(){
	$('#error_refund_method').html('');
	document.getElementById('bank_account_data').style.display = 'none';
	var checkedValue = null; 
	var inputElements = document.getElementsByName('refund_method');
	for(var i=0; inputElements[i]; ++i){
	      if(inputElements[i].checked){
	           checkedValue = inputElements[i].value;
	           break;
	      }
	}
	if(checkedValue=="Bank Transfer"){
		document.getElementById('bank_account_data').style.display = 'block';
		document.getElementById('bank_ifsc_code').required = true;
		document.getElementById('bank_account_number').required = true;
		document.getElementById('bank_account_confirm_number').required = true;
		document.getElementById('account_holder_name').required = true;
		document.getElementById('bank_return_refund_phone_number').required = true;
	}else{
		document.getElementById('bank_account_data').style.display = 'none';
		document.getElementById('bank_ifsc_code').required = false;
		document.getElementById('bank_account_number').required = false;
		document.getElementById('bank_account_confirm_number').required = false;
		document.getElementById('account_holder_name').required = false;
		document.getElementById('bank_return_refund_phone_number').required = false; 
	}
}
			
</script>

<script>
function ValidateNo() {
    var phoneNo = document.getElementById('bank_return_refund_phone_number');

    if (phoneNo.value == "" || phoneNo.value == null) {
		//alert("Please enter your Mobile No.");
		phoneNo.value="";
		phoneNo.focus();
		return false;
	}else if (phoneNo.value.length < 10 || phoneNo.value.length > 10) {
		return false;
	}else{
		return true;
	}

}
		
function isAlfa(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
        return false;
    }
    return true;
}
function isNumber(evt) {

    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
	
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
	
    return true;
}

function remove_error(){
	$('#comment').css({"border": "1px solid #ccc"});
	$('#cancel_reason').css({"border": "1px solid #ccc"});
	$('#error_accept_terms_and_conditions').html('');
}

function submitForm(order_item_id){
	
	err=0;
	var order_id=$('#order_id').val();
	var inputElements = document.getElementsByName('refund_method');
	checkedValue=null;
	for(var i=0; inputElements[i]; ++i){
	      if(inputElements[i].checked){
	           checkedValue = inputElements[i].value;
	           break;
	      }
	}
	
	cancel_reason=$("#cancel_reason").val();
	comment=$("#comment").val();
	
	if(cancel_reason==''){
	   $('#cancel_reason').css("border", "1px solid red");	   
	   err = 1;
	}else{			
		$('#cancel_reason').css({"border": "1px solid #ccc"});
	}
	if(comment==''){
	   $('#comment').css("border", "1px solid red");	   
	   err = 1;
	}else{			
		$('#comment').css({"border": "1px solid #ccc"});
	}
	
	ref=$('input[name=refund_method]').val();
	
	if(ref){
		if($('input[name=refund_method]').is(':checked')) {	
			 $('#error_refund_method').html('');
		}else{
			$('#error_refund_method').html('Pls choose refund method');
			err=1;
		}
	}
	
	if($('input[name=accept_terms_and_conditions]').is(':checked')) {	
		 $('#error_accept_terms_and_conditions').html('');
	}else{
		$('#error_accept_terms_and_conditions').html('Accept terms and conditions');
		err=1;
	}

	if(err==1){
		return false;
	}

	if(checkedValue=="Bank Transfer"){
	
      if(document.myForm.bank_name.value == ""){
	  
        //alert("Please enter some text first");
	
		myForm.bank_name.focus();
		return false;
    }else if(document.myForm.bank_branch_name.value == ""){
	  
        //alert("Please enter some text first");

		myForm.bank_branch_name.focus();
		return false;
    }else if(document.myForm.bank_city.value == ""){
	  
        //alert("Please enter some text first");
	
		myForm.bank_city.focus();
		return false;
	}else if(document.myForm.bank_ifsc_code.value.length!=6){
		
		myForm.bank_ifsc_code.focus();
		return false;
	}else if(document.myForm.bank_account_number.value.length!=15){
	       
		myForm.bank_account_number.focus();
		return false;
	}else if((document.myForm.bank_account_number.value)!=(document.myForm.bank_account_confirm_number.value)){
	        
		myForm.bank_account_confirm_number.focus();
		return false;
	}else if(document.myForm.account_holder_name.value == ""){
	  
        //alert("Please enter some text first");
	
		myForm.account_holder_name.focus();
		return false;
	}else if(document.myForm.bank_return_refund_phone_number.value.length !=10){
	  
        //alert("Please enter some text first");

		myForm.bank_return_refund_phone_number.focus();
		return false;
	}else{
		//document.myForm.submit();
		$("#cancel_order_btn").html('<i class="fa fa-spinner"></i> Processing ...');
		document.getElementById("cancel_order_btn").disabled=true;
		$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>Account/submit_cancel_order_item/'+order_item_id,
			data:$("#myForm").serialize(),
			success:function(data){
				//alert(data+'1');
				//return false;
				$("#cancel_order_btn").html('Confirm cancellation');
				document.getElementById("cancel_order_btn").disabled=true;//page willbe redirected
				alert('successfully submitted');
                location.href = '<?php echo base_url(); ?>Account/order_details/'+order_id;
				/*bootbox.alert({
				  size: "small",
				  message: 'successfully submited',
				  callback: function () { location.href = '<?php echo base_url(); ?>Account/order_details/'+order_id; }
				});*/
				
				//location.reload();
			}
		});


	  }
	  
	}else{

        $("#cancel_order_btn").html('<i class="fa fa-spinner"></i> Processing ...');
        document.getElementById("cancel_order_btn").disabled=true;
		$.ajax({
			type:'POST',
			url:'<?php echo base_url()?>Account/submit_cancel_order_item/'+order_item_id,
			data:$("#myForm").serialize(),
			success:function(data){
				//alert(data);
				//return false;
                $("#cancel_order_btn").html('Confirm cancellation');
                document.getElementById("cancel_order_btn").disabled=true;//page willbe redirected

                alert('successfully submited');
                location.href = '<?php echo base_url(); ?>Account/order_details/'+order_id;

				/*bootbox.alert({
				  size: "small",
				  message: 'successfully submited',
				  callback: function () { location.href = '<?php echo base_url(); ?>Account/order_details/'+order_id; }
				  
				});		
				*/
			}
		});
		//document.myForm.submit();
	}
}
	
	function validate_ifsc(){
		var value = document.getElementById('bank_ifsc_code').value;
		if (value.length != 6) {
			document.getElementById("errors").style.display = "block";
			
		}else if (value.length == 6){
	 
		document.getElementById("errors").style.display = "none";
		}
	}
	
	function displayKeyCode(evt){
		var textBox = getObject('bank_return_refund_phone_number');
		var charCode = (evt.which) ? evt.which : event.keyCode
		textBox.value = String.fromCharCode(charCode);
		if (charCode == 8){
		    alert(textBox.value);

			/*bootbox.alert({
							  size: "small",
							  message: textBox.value
							   
							});*/
		}
		
	}
	function ValidateNumber(obj) {
		var value = document.getElementById('bank_return_refund_phone_number').value;
		if (value.length != 10) {
			document.getElementById("errors3").style.display = "block";
			
		}else if (value.length == 10){
	 
		document.getElementById("errors3").style.display = "none";
		}
		val=obj.value;
		/*if(val!=""){
			if(val=="+91"){
				obj.value="";
			}
			else if(val.indexOf("+91")!== -1){}
			else if(val.indexOf("+91")== -1){ 
				obj.value="+91"+val;
			}
		}*/
	}

	function validate_account(){
		 
		var value = document.getElementById('bank_account_number').value;
		if (value.length != 15) {
			document.getElementById("errors1").style.display = "block";
			
		}else if (value.length == 15){
	 
		document.getElementById("errors1").style.display = "none";
		}
	}
	
	function validate_confirm(){
		var value = document.getElementById('bank_account_confirm_number').value;
		if ((document.myForm.bank_account_number.value)!=(document.myForm.bank_account_confirm_number.value)) {
			document.getElementById("errors2").style.display = "block";
			
		}else if ((document.myForm.bank_account_number.value)==(document.myForm.bank_account_confirm_number.value)){
	 
		document.getElementById("errors2").style.display = "none";
		}
	}
	
	function use_this_account_data(obj){
		document.getElementById('errors').style.display = 'none';
		document.getElementById('errors1').style.display = 'none';
		document.getElementById('errors2').style.display = 'none';
		document.getElementById('errors3').style.display = 'none';
		document.getElementById('add_new_bank_details_button').style.display = '';
		var bank_name=document.getElementById('bank_name_'+obj).innerHTML
		var branch_name=document.getElementById('branch_name_'+obj).innerHTML
		var city=document.getElementById('city_'+obj).innerHTML
		var ifsc=document.getElementById('ifsc_'+obj).innerHTML
		var account_number=document.getElementById('account_number_'+obj).innerHTML
		var account_holder_name=document.getElementById('account_holder_name_'+obj).innerHTML
		var account_holder_numer=document.getElementById('account_holder_numer_'+obj).innerHTML
		
		document.getElementById('bank_name').value=bank_name;
		document.getElementById('bank_branch_name').value=branch_name;
		document.getElementById('bank_city').value=city;
		document.getElementById('bank_ifsc_code').value=ifsc;
		document.getElementById('bank_account_number').value=account_number;
		document.getElementById('bank_account_confirm_number').value=account_number;
		document.getElementById('account_holder_name').value=account_holder_name;
		document.getElementById('bank_return_refund_phone_number').value=account_holder_numer;
		document.getElementById('bank_name').setAttribute('readOnly',true);
		document.getElementById('bank_branch_name').setAttribute('readOnly',true);
		document.getElementById('bank_city').setAttribute('readOnly',true);
		document.getElementById('bank_ifsc_code').setAttribute('readOnly',true);
		document.getElementById('bank_account_number').setAttribute('readOnly',true);
		document.getElementById('bank_account_confirm_number').setAttribute('readOnly',true);
		document.getElementById('account_holder_name').setAttribute('readOnly',true);
		document.getElementById('bank_return_refund_phone_number').setAttribute('readOnly',true);
		document.getElementById('save_this_bank_data').disabled=true;
		$('#all_bank_accounts').modal('hide');
	}
	function add_new_bank_data(){
		document.getElementById('bank_name').value="";
		document.getElementById('bank_branch_name').value="";
		document.getElementById('bank_city').value="";
		document.getElementById('bank_ifsc_code').value="";
		document.getElementById('bank_account_number').value="";
		document.getElementById('bank_account_confirm_number').value="";
		document.getElementById('account_holder_name').value="";
		document.getElementById('bank_return_refund_phone_number').value="";
		document.getElementById('bank_name').readOnly=false;
		document.getElementById('bank_branch_name').readOnly=false;
		document.getElementById('bank_city').readOnly=false;
		document.getElementById('bank_ifsc_code').readOnly=false;
		document.getElementById('bank_account_number').readOnly=false;
		document.getElementById('bank_account_confirm_number').readOnly=false;
		document.getElementById('account_holder_name').readOnly=false;
		document.getElementById('bank_return_refund_phone_number').readOnly=false;
		det_count=$('#saved_bank_details').val().trim();
		if(det_count<3){
			document.getElementById('save_this_bank_data').disabled=false;
		}
	}
</script>