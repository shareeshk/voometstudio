 <style>
 .thank_you{
	padding: 10px;
    line-height: 20px;
 }
 .separator_line{
	border-right: 1px solid #ccc;
	margin-left:15px;
	margin-right:20px;
}
 .error_msg{
	 margin-left: 15px;height: 271px;text-align: center; padding-top: 115px;color:red;
 }
 .price_details_thankyou + .popover {
   left: -111px !important;
}
.price_details_thankyou + .popover .arrow {
  left: 93% !important;
}
.price_details_thankyou2 + .popover {
   left: 157px !important;
}
.price_details_thankyou2 + .popover .arrow {
  left: 78% !important;
}
#center_column{
	margin-top:1em;
}
.ordrk-color a{
	color:#e46c0a;
}
 </style>
<!-- Home slideder-->


    <div class="container">
        <div class="my_orders_links">
		
        <div class="columns-container mt-50">
               <!--------------------------------->
			   <?php
				//print_r($order_summary_mail_data_arr);
			   ?>
			<div class="row" id="center_column">
            	<?php
					$grandtotal=0;
					
				?>
				
            		<div class="col-md-10 col-md-offset-1 well" style="background-color:#fff;">
            			<div class="col-sm-7">
		                	<div class="row">
								<div class="col-sm-12 thank_you">
								<h1 class="page-heading pl-0">
									Sorry your transaction could not be completed
								</h1>

								<?php if($combo_order==1){

								?>

								<p  style="text-align: justify;margin-top: 10px;">
								We are redirecting you to the <u><a href="<?php echo base_url()?>catalog_combo">Checkout Combo Pack</a></u> page.
								</p>

								<?php 
								
								}else{
									?>
								<p  style="text-align: justify;margin-top: 10px;">
								We are redirecting you to the <u><a href="<?php echo base_url()?>checkout">Checkout</a></u> page to complete the transaction.
								</p>
									<?php 
								} ?>
								
								<br>
								
								
								</div>
							</div>
		                </div>
						<div class="col-sm-5">
		                	
						</div>
						 
				
		            </div>
					
		          

            </div>
			
		</div>
			
			   <!--------------------------------->
           
        </div>
    </div>

<!-- END Home slideder-->
<script>
function toggle_content(ele){
	var cont_id=$(ele).attr("content_id");
	if($(window).width() <= 480){
		$(ele).popover('hide');
		$(cont_id).toggle();
	}
}

</script>
