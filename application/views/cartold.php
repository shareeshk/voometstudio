<style type="text/css">
.cart_summary .qty input {
    text-align: center;
    max-width: 50px;
    margin: 0 auto;
    border-radius: 0px;
    border: 1px solid #eaeaea;
}
.cart_summary .qty a {
    padding: 3px 5px 5px 5px;
    border: 1px solid #eaeaea;
    display: inline-block;
    width: auto;
    margin-top: 0px; 
}
.borderless td, .borderless th {
    border: none;
}
.overflow{
	overflow: auto;
}
.left-align-cashback{
	width: 86%;float: left;text-align: right;
}
.left-align-cashback-total{
	width: 85%;float: left;text-align: right;font-size: .9em;
}
.cart_summary td {
    vertical-align: middle !important;
    padding: 10px;
}
.cart_summary .table > tbody > tr > td, .table > tbody > tr > th, .cart_summary .table > tfoot > tr > td, .table > tfoot > tr > th, .cart_summary .table > thead > tr > td{
	 padding: 5px;
}
.table > thead > tr > th {
	 padding: 6px;
}
</style>	
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo base_url()?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Cart</span>
			
			<?php //print_r($_COOKIE); ?>
			
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
       <!-- ../ <h2 class="page-heading no-line">
            <span class="page-heading-title2">Shopping Cart Summary</span>
        </h2>

		page heading-->
		
<script type="text/javascript">

//2nd controller//
function OrderController($scope,$rootScope,Scopes,$http,$sce) {
	
	Scopes.store('OrderController', $scope);
	//$scope.searched_pincode_msg='';
	c_id='<?php echo $this->session->userdata("customer_id");?>';

	var searched_pincode='<?php echo ($this->session->userdata("searched_pincode")) ? $this->session->userdata("searched_pincode") :'';?>';
	
	//var searched_pincode=localStorage.getItem("searched_pincode");
	
	$scope.pincode=searched_pincode;
	if(searched_pincode!=null && searched_pincode!=''){
		$scope.searched_pincode=searched_pincode;		
		$scope.pincode=$scope.searched_pincode;
	}
	if(searched_pincode==''){
		$scope.searched_pincode_msg='No pincode searched';	
	}else{
		$scope.searched_pincode_msg='pincode searched';	
	}
	$scope.final_count_free_items=0;
	$scope.show_cash_back_tr=0;
	
	$scope.update_cart_controller_scopes=function (order_product_count,products_order,w=''){
		//alert('on load');
		$scope.count_of_free_items=new Array();
	
		$scope.out_of_stock_item_in_cart=[];
		$scope.applied_at_invoice_arr = [];
		$scope.order_product_count=order_product_count;
		$scope.products_order=products_order;		
		$scope.final_count_free_items=0;//newly added

		if($scope.order_product_count==0){
			$scope.show_cash_back_tr=0;
			$scope.eligible_cash_back_percentage_display=0;
			$scope.eligible_discount_percentage_display=0;
			$scope.eligible_free_shipping_display=0;
			$scope.eligible_free_surprise_gift_display=0;
			$scope.eligible_cash_back_percentage_display=0;
			$scope.discount_oninvoice=0;
			$scope.cash_back_oninvoice=0;
			$scope.free_shipping=0;
			$scope.surprise_gift=0;
			$scope.total_cash_back=0;
			$scope.show_surprise_gift_on_invoice=0;
			$scope.invoice_surprise_gift=0;
			$scope.final_count_free_items=0;
			return false;
		}
	
		//alert(w);	

		$scope.total_cash_back_for_sku=0;
		total_netprice=0;
		individual_price_without_shipping=0;
		total_shipping_price=0;
		$scope.cash_back_tr_arr=[];
		
		if(localStorage.changed_moq_with_inventory_id !=null && localStorage.changed_moq_with_inventory_id !=''){
			
			changed_inv=localStorage.changed_moq_with_inventory_id;
			changed_inv=JSON.parse(changed_inv);
			inventory_id_to_update=changed_inv.inventory_id;
		}else{
			inventory_id_to_update='';
		}

		for(j=0;j<$scope.order_product_count;j++){
		
		var val=$scope.products_order[j].attribute_1_value.split(':');
		$scope.products_order[j].attribute_1_value=val[0];
		//alert($scope.products_order[j].stock_available+"||"+parseInt($scope.products_order[j].inventory_moq));
		
		if(parseInt($scope.products_order[j].stock_available)>= parseInt($scope.products_order[j].inventory_moq)){
					
			$scope.products_order[j].item_stock_available=1;
			
			if(c_id==null || c_id==''){
				$scope.products_order[j].selling_price=$scope.products_order[j].inventory_selling_price_with_out_discount;
			}//without session
			
			
			if(products_order[j].inventory_selling_price==null){
				//when fetching the data from db				
				$scope.products_order[j].selling_price=$scope.products_order[j].selling_price;
				$scope.products_order[j].inventory_selling_price=$scope.products_order[j].selling_price;
			}else{
				
				$scope.products_order[j].selling_price=products_order[j].inventory_selling_price;
			}
			
			if(inventory_id_to_update != ''){
				if($scope.products_order[j].inventory_id==inventory_id_to_update){
					
					$scope.products_order[j].inventory_moq=changed_inv.changed_moq;
					if(changed_inv.shipping_charge_from_detail_page!=null){
					$scope.products_order[j].shipping_charge=changed_inv.shipping_charge_from_detail_page;
					//alert($scope.products_order[j].shipping_charge+"shipping charge");
					}
					/*$scope.products_order[j].inventory_weight_in_grams=changed_inv.changed_inventory_weight_in_grams;
					if(changed_inv.parcel_category_multiplier!=null){
					$scope.products_order[j].inventory_weight_in_grams=changed_inv.parcel_category_multiplier;
					}
					$scope.products_order[j].inventory_weight_in_grams=changed_inv.weight_multiplier;
					*/
					/*if(w==1){
						
					}*/
					localStorage.removeItem('changed_moq_with_inventory_id');
				}
			}
			
			//alert($scope.products_order[j].inventory_moq);
			
			if($scope.products_order[j].promotion_minimum_quantity!=''){
				
				if(parseInt($scope.products_order[j].promotion_minimum_quantity)<=parseInt($scope.products_order[j].inventory_moq)){
					 $scope.products_order[j].show_addon_items=1;
				}else{
					$scope.products_order[j].show_addon_items=0;
				}
			}else{
				$scope.products_order[j].show_addon_items=0;
			}
	
			$scope.products_order[j].final_product_individual=0;

	
		modulo=$scope.products_order[j].quantity_without_promotion;
		quotient=$scope.products_order[j].quantity_with_promotion;
				
		$scope.products_order[j].modulo=modulo;
		$scope.products_order[j].quotient=quotient;
		$scope.products_order[j].price_of_quotient_1=Math.round($scope.products_order[j].individual_price_of_product_with_promotion);	
		$scope.products_order[j].price_of_modulo_1=$scope.products_order[j].individual_price_of_product_without_promotion;

		if($scope.products_order[j].promotion_available==1){
		
		////////////
		
				if($scope.products_order[j].default_discount!=""){
				
					
			if(modulo!=0 && ($scope.products_order[j].inventory_moq>=parseInt($scope.products_order[j].promotion_minimum_quantity))){
				
				$scope.products_order[j].discount=1;//default
				$scope.products_order[j].promotion=2;
			
				
				$scope.products_order[j].price_after_default_discount=$scope.products_order[j].total_price_of_product_without_promotion;

				if(quotient!=0){
					$scope.products_order[j].price_after_promotion_discount=$scope.products_order[j].total_price_of_product_with_promotion;
				}

				if(quotient==0 && modulo!=0){
					$scope.products_order[j].promotion_quote_show=0;
				}else{
					$scope.products_order[j].promotion_quote_show=1;
				}
				if(quotient!=0 && modulo==0){
					$scope.products_order[j].discount_quote_show=0;
				}else{
					$scope.products_order[j].discount_quote_show=1;
				}
		
				if($scope.products_order[j].price_after_promotion_discount!=null){
					$scope.products_order[j].final_product_individual=parseInt($scope.products_order[j].price_after_promotion_discount)+parseInt($scope.products_order[j].price_after_default_discount);
				}else{
					$scope.products_order[j].promotion="";
					$scope.products_order[j].discount=1;
					$scope.products_order[j].final_product_individual=$scope.products_order[j].price_after_default_discount;
				}
				
			}else{		
				
				if($scope.products_order[j].inventory_moq>=parseInt($scope.products_order[j].promotion_minimum_quantity)){
					
					$scope.products_order[j].promotion_quote_show=1;
					$scope.products_order[j].discount_quote_show=0;
	
					$scope.products_order[j].price_after_promotion_discount=$scope.products_order[j].total_price_of_product_with_promotion;
					$scope.products_order[j].final_product_individual=$scope.products_order[j].price_after_promotion_discount;
						
					$scope.products_order[j].price_after_default_discount=0;
					
					$scope.products_order[j].promotion=1;
					$scope.products_order[j].discount="";
				
				}else{
		
					if(quotient==0 && modulo!=0){
						$scope.products_order[j].promotion_quote_show=0;
					}else{
						$scope.products_order[j].promotion_quote_show=1;
					}
					if(quotient!=0 && modulo==0){
						$scope.products_order[j].discount_quote_show=0;
					}else{
						$scope.products_order[j].discount_quote_show=1;
					}
			
			
					$scope.products_order[j].price_after_default_discount=$scope.products_order[j].total_price_of_product_without_promotion;
						
					$scope.products_order[j].promotion_quote_show=0;
					$scope.products_order[j].discount_quote_show=1;
	
					$scope.products_order[j].promotion="";
					$scope.products_order[j].discount=1;
					$scope.products_order[j].final_product_individual=$scope.products_order[j].price_after_default_discount;
					
				}
			
			}
	
				}//discount promotion !=0
				
		if($scope.products_order[j].default_discount==""){
			
			if(modulo!=0){
				
				$scope.products_order[j].discount=1;
				$scope.products_order[j].promotion=2;
				$scope.products_order[j].price_of_modulo_1=$scope.products_order[j].selling_price;

				$scope.products_order[j].price_after_default_discount=$scope.products_order[j].total_price_of_product_without_promotion;

				if(quotient!=0){

					$scope.products_order[j].price_of_quotient_1=Math.round($scope.products_order[j].individual_price_of_product_with_promotion);
		
					$scope.products_order[j].price_after_promotion_discount=$scope.products_order[j].total_price_of_product_with_promotion;
				}
				
				if(quotient==0 && modulo!=0){
					$scope.products_order[j].promotion_quote_show=0;
				}else{
					$scope.products_order[j].promotion_quote_show=1;
				}
				if(quotient!=0 && modulo==0){
					$scope.products_order[j].discount_quote_show=0;
				}else{
					$scope.products_order[j].discount_quote_show=1;
				}
		
				if($scope.products_order[j].price_after_promotion_discount!=null){
					$scope.products_order[j].final_product_individual=parseInt($scope.products_order[j].price_after_promotion_discount)+parseInt($scope.products_order[j].price_after_default_discount);
				}else{
				
					$scope.products_order[j].promotion="";
					$scope.products_order[j].discount=1;
					$scope.products_order[j].final_product_individual=$scope.products_order[j].price_after_default_discount;
				}

			}else{		
			
				$scope.products_order[j].promotion_quote_show=1;
				$scope.products_order[j].discount_quote_show=0;
				$scope.products_order[j].price_after_promotion_discount=$scope.products_order[j].total_price_of_product_with_promotion;
				
				$scope.products_order[j].final_product_individual=$scope.products_order[j].price_after_promotion_discount;				
				$scope.products_order[j].price_after_default_discount=0;
			    $scope.products_order[j].promotion=1;
			    $scope.products_order[j].discount="";
			
			}

		}
				
			}//promo avai 1
	

			if($scope.products_order[j].promotion_available==0){
				$scope.products_order[j].final_product_individual=$scope.products_order[j].selling_price*($scope.products_order[j].inventory_moq);
				$scope.products_order[j].discount_quote_show=0;
				$scope.products_order[j].promotion_quote_show=0;
			}
			

			if($scope.products_order[j].promotion_item!='' && $scope.products_order[j].promotion_available==1){
				
				$scope.getFreeInventory($scope.products_order[j].promotion_item,$scope.products_order[j].inventory_id,$scope.products_order[j].promotion_item_num,$scope.products_order[j].quotient,$scope.products_order[j].show_addon_items,j,$scope.products_order[j].promotion_minimum_quantity,$scope.products_order[j].inventory_moq);
			
			}else{
				
				if(document.getElementById('total_num_of_free_items')!=null){
					document.getElementById('total_num_of_free_items').innerHTML="";
				}
			}
			
			///////////////////promotion/////////////////
			
			if($scope.products_order[j].territory_duration!=-1){
				var months=new Array("January","February","March","April","May","June","July","August","Sep","October","November","December");
				var days=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Fri","Saturday");

				var d=new Date();
				duration=parseInt($scope.products_order[j].territory_duration);	
				d.setDate(d.getDate() + duration);
				$scope.products_order[j].delivery_date=days[d.getDay()]+" "+months[d.getMonth()]+" "+d.getDate()+","+d.getFullYear();	
			}else{
				$scope.products_order[j].delivery_date="Not applied";
			}
			
			if($scope.products_order[j].inventory_weight_in_grams_for_unit!=-1){
				$scope.products_order[j].inventory_weight_in_grams=parseInt($scope.products_order[j].inventory_moq)*parseInt($scope.products_order[j].inventory_weight_in_grams_for_unit);	
			}
					
			$scope.products_order[j].inventory_selling_price_without_discount=(($scope.products_order[j].selling_price)*($scope.products_order[j].inventory_moq));
						
			$scope.products_order[j].inventory_selling_price_with_discount=($scope.products_order[j].inventory_selling_price_without_discount);

			if(parseInt($scope.products_order[j].shipping_charge)!=parseInt(-1)){
				
				//alert($scope.products_order[j].final_product_individual+"final_price_ind");
				
				if($scope.products_order[j].final_product_individual!=0){
					$scope.products_order[j].subtotal=parseInt($scope.products_order[j].final_product_individual)+parseInt($scope.products_order[j].shipping_charge);
				}else{
					$scope.products_order[j].subtotal=parseInt($scope.products_order[j].inventory_selling_price_without_discount)+parseInt($scope.products_order[j].shipping_charge);
					
				}

				$scope.products_order[j].shipping_charge_msg=0;
				$scope.products_order[j].shipping_charge_view=1;
				if($scope.products_order[j].shipping_charge==0){
					$scope.products_order[j].msg_style="color:green;";
					$scope.products_order[j].delivery_service_msg='Free Delivery service';
				}else{
					$scope.products_order[j].msg_style="color:#333;";
					$scope.products_order[j].delivery_service_msg='Service is available';
				}
			}else{
				
				$scope.products_order[j].subtotal=parseInt($scope.products_order[j].inventory_selling_price_without_discount);
			
				$scope.products_order[j].shipping_charge_msg="Not applied";
				$scope.products_order[j].shipping_charge_view=0;
				$scope.products_order[j].msg_style="color:red;";
				$scope.products_order[j].delivery_service_msg='Service is not available';
			}
			
			if($scope.products_order[j].final_product_individual){
				if(parseInt($scope.products_order[j].shipping_charge)!=parseInt(-1)){
					$scope.products_order[j].subtotal=parseInt($scope.products_order[j].final_product_individual)+parseInt($scope.products_order[j].shipping_charge);
					total_shipping_price+=parseFloat($scope.products_order[j].shipping_charge);

				}else{
					$scope.products_order[j].subtotal=parseInt($scope.products_order[j].final_product_individual);
					//total_shipping_price+=parseFloat($scope.products_order[j].shipping_charge);
				}
			}
			
			//alert($scope.products_order[j].subtotal+"subtotal");
			

			if($scope.products_order[j].promotion_cashback>0){
				
				$scope.show_cash_back_tr=1;
				
				cash_back_amount=parseFloat($scope.products_order[j].cash_back_value);
				$scope.total_cash_back_for_sku+=parseFloat(cash_back_amount);
				
				$scope.cash_back_tr_arr.push({
					"promotion_cashback_percentage":$scope.products_order[j].promotion_cashback,
					"product_name":$scope.products_order[j].product_name,
					"inventory_sku":$scope.products_order[j].inventory_sku,
					"cash_back_amount":cash_back_amount,
					"sku_count":$scope.products_order[j].inventory_moq
				});

			}
			
			
		}else{
					
				$scope.products_order[j].item_stock_available=0;
				$scope.products_order[j].subtotal=0;
				
				$scope.out_of_stock_item_in_cart.push(1);
		}//checking out of stock
		
		
			total_netprice+=parseFloat($scope.products_order[j].subtotal);	

			individual_price_without_shipping+=parseInt($scope.products_order[j].final_product_individual);
			
		}//forloop
		
		
		if($scope.cash_back_tr_arr.length>0){
			$scope.show_cash_back_tr=1;
		}else{
			$scope.show_cash_back_tr=0;
		}

		//alert($scope.final_count_free_items+"fffff");
		
		if($scope.out_of_stock_item_in_cart.length>0){
			$scope.out_of_stock_item_exists=1;
			$('#proceed_to_checkout_id').hide();
		}else{	
			$scope.out_of_stock_item_exists=0;	
			$('#proceed_to_checkout_id').show();
				
		}
		
		$scope.total_price_without_shipping_price=individual_price_without_shipping;
	
			
			$scope.grandtotal_without_discount=parseInt(total_netprice);
			$scope.grandTotal=parseInt(total_netprice);
			$scope.eligible_discount_percentage_display=0;
			$scope.total_shipping_price=total_shipping_price;
			
			$scope.grandTotal_for_discount=0;
			$scope.total_shipping_price_actual=0;
			
		if(c_id!=null && c_id==''){
			
			localStorage.setItem("LocalCustomerCartLS",JSON.stringify($scope.products_order));

		/*	LocalCustomerCartLS=localStorage.LocalCustomerCartLS;
			localStorage.removeItem(LocalCustomerCartLS.show_addon_items);
			localStorage.removeItem(LocalCustomerCartLS.final_product_individual);
			localStorage.removeItem(LocalCustomerCartLS.modulo);
			localStorage.removeItem(LocalCustomerCartLS.promotion_quote_show);
			localStorage.removeItem(LocalCustomerCartLS.quotient);
			localStorage.removeItem(LocalCustomerCartLS.price_of_quotient_1);
			localStorage.removeItem(LocalCustomerCartLS.price_after_promotion_discount);
			localStorage.removeItem(LocalCustomerCartLS.price_after_default_discount);
			localStorage.removeItem(LocalCustomerCartLS.promotion);
			localStorage.removeItem(LocalCustomerCartLS.discount);
			localStorage.removeItem(LocalCustomerCartLS.discount_quote_show);*/
			
		}
		
		
	<?php 
	$price_list=array();
	if(!empty($promotions_cart)){
		?>
		
		$scope.eligible_cash_back_percentage_display=0;
		$scope.eligible_discount_percentage_display=0;
		$scope.eligible_free_shipping_display=0;
		$scope.eligible_free_surprise_gift_display=0;
		$scope.discount_oninvoice=0;
		$scope.cash_back_oninvoice=0;
		$scope.free_shipping=0;
		$scope.surprise_gift=0;
		$scope.show_surprise_gift_on_invoice=0;
		$scope.invoice_surprise_gift=0;
		
		$scope.promotions_all = [];
		
		<?php
		//$cart_promo_arr=array();
		foreach($promotions_cart as $data){ 
		
		$buy_type_arr=explode(',',$data['buy_type']);
		
		if(count($buy_type_arr)==1){
			if(preg_match('/surprise/', strtolower($data["promo_quote"]))){
				array_push($price_list,$data);
			}
			
		}
		
		?>
			//alert('<?php echo json_encode($data); ?>');
		
			$scope.promotions_cart = [];
			var promotion_id='<?php echo $data['promo_uid']; ?>';
			var promo_to_buy='<?php echo $data['to_buy']; ?>';
			var promo_to_get='<?php echo $data['to_get']; ?>';
			var promo_buy_type='<?php echo $data['buy_type']; ?>';	
			var promo_get_type='<?php echo $data['get_type']; ?>';		
			var promo_name='<?php echo $data['promo_name']; ?>';	
			var applied_at_invoice='<?php echo $data['applied_at_invoice']; ?>';
	
			
			$scope.promotion_id_arr=promotion_id.split(',');
			$scope.promo_to_buy_arr=promo_to_buy.split(',');
			$scope.promo_to_get_arr=promo_to_get.split(',');
			$scope.promo_buy_type_arr=promo_buy_type.split(',');
			$scope.promo_get_type_arr=promo_get_type.split(',');
			$scope.promo_name_arr=promo_name.split(',');

			
			
			$scope.promotions_cart.push({
            'promotion_id_arr': $scope.promotion_id_arr, 
            'promo_to_buy_arr': $scope.promo_to_buy_arr,
            'promo_to_get_arr': $scope.promo_to_get_arr,
            'promo_buy_type_arr': $scope.promo_buy_type_arr,
            'promo_get_type_arr': $scope.promo_get_type_arr,
			'promo_name_arr':$scope.promo_name_arr
			});


			for(var t=0;t<$scope.promotions_cart.length;t++){
				
				//alert($scope.promotions_all[t][u].promotion_id_arr);
				
				promo_to_buy_arr=$scope.promotions_cart[t].promo_to_buy_arr;
				promo_to_get_arr=$scope.promotions_cart[t].promo_to_get_arr;
				promo_buy_type_arr=$scope.promotions_cart[t].promo_buy_type_arr;
				promo_get_type_arr=$scope.promotions_cart[t].promo_get_type_arr;
				promo_name_arr=$scope.promotions_cart[t].promo_name_arr;
				
				for(var v=0;v<promo_to_buy_arr.length;v++){
					
					to_buy=promo_to_buy_arr[v];
					to_buy_previous=promo_to_get_arr[v-1];
					to_get=promo_to_get_arr[v];
					to_get_previous=promo_to_get_arr[v-1];
					buy_type=promo_buy_type_arr[v];
					get_type=promo_get_type_arr[v];
					promo_name=promo_name_arr[v];

					if((to_get=='') && (get_type=='')){

						if($scope.total_shipping_price==0){
							$scope.free_shipping='Search the pincode to get <strong>Free shipping</strong> .';
							$scope.explicitlyTrustedHtml_free_shipping = $sce.trustAsHtml($scope.free_shipping);
							$scope.eligible_free_shipping_display=0;
							$scope.free_shipping=1;
						}else{
						
							$scope.currency_type_free_shipping=buy_type;
							$scope.eligible_free_shipping_display=0;
							$scope.free_shipping=0;
							if(parseInt(to_buy)>parseInt(individual_price_without_shipping)){
							
							$scope.total_shipping_price_actual=0;
							$scope.free_shipping='Expend more '+parseInt(to_buy-individual_price_without_shipping)+' '+buy_type+' to get <strong>Free shipping</strong> .';
							$scope.explicitlyTrustedHtml_free_shipping = $sce.trustAsHtml($scope.free_shipping);
							$scope.eligible_free_shipping_display=0;
							$scope.free_shipping=1;
							
							}else{
								$scope.explicitlyTrustedHtml_free_shipping = $sce.trustAsHtml('');
								$scope.eligible_free_shipping_display=1;
								$scope.total_shipping_price_actual=$scope.total_shipping_price;
								$scope.free_shipping=0;
							}
						}
					}
						
						if(get_type=="discount"){
							
							$scope.discount_oninvoice=0;
							if(parseInt(to_buy)>individual_price_without_shipping){
								if(to_get_previous>0){
$scope.grandTotal_for_discount=Math.round((to_get_previous/100)*individual_price_without_shipping);
$scope.eligible_discount_percentage_display=1;
$scope.eligible_discount_percentage=to_get_previous;
$scope.currency_type_discount=buy_type;
								}else{
						$scope.eligible_discount_percentage_display=0;
						$scope.explicitlyTrustedHtml_discount = $sce.trustAsHtml('');
								}
if(individual_price_without_shipping<to_buy){
$scope.discount_oninvoice=1;	
$scope.discount_oninvoice='Expend more '+parseInt(to_buy-individual_price_without_shipping)+' '+buy_type+' to get <strong>'+to_get+'% '+get_type+' on INVOICE</strong> .';
$scope.explicitlyTrustedHtml_discount = $sce.trustAsHtml($scope.discount_oninvoice);
}else{
	$scope.discount_oninvoice=0;
	$scope.explicitlyTrustedHtml_discount = $sce.trustAsHtml("");
}	
break;																															
							}else{
									if(to_get>0){
										
										
										$scope.grandTotal_for_discount=Math.round((to_get/100)*individual_price_without_shipping)
										$scope.eligible_discount_percentage_display=1;
										$scope.eligible_discount_percentage=to_get;
										$scope.currency_type_discount=buy_type;
										

									}else{
										$scope.eligible_discount_percentage_display=0;
										
									}
					
							}
						}
						
						if(get_type=="cash back"){
							$scope.cash_back_oninvoice=0;
							if(parseInt(to_buy)>individual_price_without_shipping){
								if(to_get_previous>0){
$scope.grandTotal_for_cash_back=parseFloat((to_get_previous/100)*individual_price_without_shipping).toFixed(2);
$scope.eligible_cash_back_percentage_display=1;
$scope.eligible_cash_back_percentage=to_get_previous;
$scope.currency_type_cash_back=buy_type;
								}else{
						$scope.eligible_cash_back_percentage_display=0;
						$scope.explicitlyTrustedHtml_cash_back = $sce.trustAsHtml('');
								}
if(individual_price_without_shipping<to_buy){
$scope.cash_back_oninvoice=1;	
$scope.cash_back_oninvoice='Expend more '+parseInt(to_buy-individual_price_without_shipping)+' '+buy_type+' to get <strong>'+to_get+'% '+get_type+' on INVOICE</strong> .';
$scope.explicitlyTrustedHtml_cash_back = $sce.trustAsHtml($scope.cash_back_oninvoice);
}else{
	$scope.cash_back_oninvoice=0;
	$scope.explicitlyTrustedHtml_cash_back = $sce.trustAsHtml("");
}	
break;																															
							}else{
									if(to_get>0){
										
										
										$scope.grandTotal_for_cash_back=parseFloat((to_get/100)*individual_price_without_shipping).toFixed(2);
										$scope.eligible_cash_back_percentage_display=1;
										$scope.eligible_cash_back_percentage=to_get;
										$scope.currency_type_cash_back=buy_type;
										

									}else{
										$scope.eligible_cash_back_percentage_display=0;
										
									}
					
							}
						}
						
				}

			}
		
			<?php 
		}
		
		$m = array();
		foreach ($price_list as $key => $row) {
			$m[$key] = $row['to_buy'];
		}
        array_multisort($m, SORT_ASC, $price_list);

	}?>
		$scope.applied_at_invoice_arr=<?php echo json_encode($price_list); ?>;
		//alert(applied_at_invoice_arr);
		
		if($scope.applied_at_invoice_arr.length>0){
			
			$scope.show_surprise_gift_on_invoice=1;
			
			for(h=0;h<$scope.applied_at_invoice_arr.length;h++){
	
				if(parseInt($scope.applied_at_invoice_arr[h].to_buy)>parseInt(individual_price_without_shipping)){
					
						//for top alert	
						expand_more=parseInt($scope.applied_at_invoice_arr[h].to_buy)-parseInt(individual_price_without_shipping);
						
						$scope.invoice_surprise=' To eligible for ('+$scope.applied_at_invoice_arr[h].promo_quote+') expend more '+expand_more+' '+$scope.applied_at_invoice_arr[h].buy_type;
						$scope.explicitlyTrustedHtml_invoice_surprise_gift = $sce.trustAsHtml($scope.invoice_surprise);
						$scope.invoice_surprise_gift=1;
						
						//for bootom messsage
						
						if($scope.applied_at_invoice_arr[h-1]!=null){
							$scope.invoice_surprise_result=$scope.applied_at_invoice_arr[h-1].promo_quote;
							$scope.explicitlyTrustedHtml_invoice_surprise_gift_result = $sce.trustAsHtml($scope.invoice_surprise_result);
							$scope.invoice_surprise_gift_result=1;
						}else{
						
							$scope.explicitlyTrustedHtml_invoice_surprise_gift_result = $sce.trustAsHtml("");
							$scope.invoice_surprise_gift_result=0;
							
						}
						break;
					
				}else{
					$scope.explicitlyTrustedHtml_invoice_surprise_gift = $sce.trustAsHtml("");
					$scope.invoice_surprise_gift=0;
					
					$scope.invoice_surprise_result=$scope.applied_at_invoice_arr[h].promo_quote;
					$scope.explicitlyTrustedHtml_invoice_surprise_gift_result = $sce.trustAsHtml($scope.invoice_surprise_result);
					$scope.invoice_surprise_gift_result=1;
				}
			}
			
		}else{
			$scope.show_surprise_gift_on_invoice=0;
		}
		

		if($scope.eligible_discount_percentage_display==1 || $scope.eligible_free_shipping_display==1){
		    if($scope.total_shipping_price_actual){
		        $scope.grandTotal_after_deduction=(parseInt($scope.grandtotal_without_discount)-parseInt($scope.grandTotal_for_discount)-parseInt($scope.total_shipping_price_actual)); 
                $scope.amount_to_pay=$scope.grandTotal_after_deduction;
		    }
		    else{
		         $scope.amount_to_pay=parseInt($scope.grandtotal_without_discount)-parseInt($scope.grandTotal_for_discount);
		    }
				
		}else{
			$scope.amount_to_pay=parseInt($scope.grandTotal);
		}

		if($scope.eligible_discount_percentage_display==1 || $scope.eligible_free_shipping_display==1){

		    if(parseInt($scope.total_shipping_price_actual)){
		        $scope.you_saved=parseInt($scope.grandTotal_for_discount)+parseInt($scope.total_shipping_price_actual);
		    }else{
		        $scope.you_saved=parseInt($scope.grandTotal_for_discount)
		    }
			
		}else{
			$scope.you_saved=0;
		}

		if($scope.show_cash_back_tr==1){
					if($scope.grandTotal_for_cash_back==null){
						$scope.grandTotal_for_cash_back=0;
					}
					if($scope.total_cash_back_for_sku==null){
						$scope.total_cash_back_for_sku=0;
					}			
				$scope.total_cash_back=parseFloat($scope.grandTotal_for_cash_back)+parseFloat($scope.total_cash_back_for_sku);
				$scope.total_cash_back=$scope.total_cash_back.toFixed(2);
		}else{
			//alert($scope.grandTotal_for_cash_back);
			
			if(parseInt($scope.grandTotal_for_cash_back)>0){
	
				$scope.total_cash_back=parseInt($scope.grandTotal_for_cash_back);
			}else{
				$scope.total_cash_back=0;
			}
		}
	
	}
	
	$scope.getFreeInventory=function(obj,inv,num,quotient,addons_show,i,promotion_minimum_quantity,inventory_moq){

	$http({
				method : "POST",
				url : "<?php echo base_url();?>get_free_inventory_items_in_promotions",
				data:{'free_inventory':obj,'inv':inv},
				dataType:"JSON",
				async:false
				}).success(function mySucces(result) {
					
					//document.getElementsByClassName('extra_data')[i].innerHTML=result	
			
			if(document.getElementById('extra_data_'+i)!=null){
		
				document.getElementById('extra_data_'+i).innerHTML=result	
			}
			
			var total_num=0;
		
		        var free_nums_for_invs=num.split(',')
                var promotion_item_free=obj.split(',')
				
                for(var k=0;k<free_nums_for_invs.length;k++){

				if(document.getElementById("free_inv_nums_"+promotion_item_free[k]+"_"+inv)!=null){
					
                  		quotient_q=inventory_moq/promotion_minimum_quantity
						quotient_q=Math.floor(quotient_q)
						    document.getElementById("free_inv_nums_"+promotion_item_free[k]+"_"+inv).innerHTML=quotient_q*free_nums_for_invs[k];
                    }
                    //alert(("free_inv_nums_"+promotion_item_free[k]+"_"+inv))
                    total_num+=quotient_q*free_nums_for_invs[k];
                }
         
			 
                if(document.getElementById('total_num_of_free_items')!=null){
						if(total_num>0){
							document.getElementById('total_num_of_free_items').innerHTML=" + "+total_num+" Free Items";
                        }else{
                            document.getElementById('total_num_of_free_items').innerHTML="";
                        }  
                 }
				
				if(total_num!=null && total_num!=0){
					$scope.calculate_free_items(total_num);
				}
				
				}, function myError(response) {
					
				});
			
	}
	
	$scope.calculate_free_items=function(total_num){
			$scope.count_of_free_items.push(total_num);
			temp=0
			for(c=0;c<$scope.count_of_free_items.length;c++){
				temp+=parseInt($scope.count_of_free_items[c]);
			}
			$scope.final_count_free_items=temp;
			//alert($scope.final_count_free_items);
	}
	
	$scope.check_availability_pincodeFun=function(check){	
		//alert($scope.pincode)
		if($scope.pincode!=null){			
			//localStorage.setItem("searched_pincode",$scope.pincode);
		
			$http({
				method : "POST",
				url : "<?php echo base_url();?>check_pincode_which_has_service",
				data:{'pincode':$scope.pincode},
				dataType:"JSON",
				async:false
				}).success(function mySucces(result) {
					$scope.pincode_has_service=result;	
					
					if(result==true){						
						$("#available_pincode_status_div").show();
						$("#available_pincode_div").hide();
						var searched_pincode='<?php echo (isset($_COOKIE['searched_pincode'])) ?$_COOKIE['searched_pincode'] :'';?>';
						//alert(searched_pincode);
						$scope.get_corresponding_shipping_charge(check);
						$scope.searched_pincode_msg="This Pincode has service";
					}else{
						$("#available_pincode_status_div").hide();
						$("#available_pincode_div").show();
						$scope.searched_pincode_msg="No Service for this pincode";
						$scope.pincode_has_no_service();
					}				
				}, function myError(response) {
					
				});
			
		}else{
			alert('Please Enter pincode');
		}
		
	}
	if(c_id!='' && c_id!=null){
		
			$http({
				method : "POST",
				url : "<?php echo base_url();?>account/get_cartinfo_in_json_format",
				data:"1=2",
				dataType:"JSON",
				}).success(function mySucces(result) {
					
					$scope.products_order=result;
					$scope.order_product_count=$scope.products_order.length;
					total_netprice=0;
					if($scope.order_product_count==0){
						$scope.empty_cart_in_ordercontroller=1;
						$(".cart_summary").css({"display":"none"});
						$(".proceed_to_checkout").css({"display":"none"});
					}else{
						$scope.empty_cart_in_ordercontroller=0;
						$(".cart_summary").css({"display":""});
						$(".proceed_to_checkout").css({"display":""});
					}	
					$scope.update_cart_controller_scopes($scope.order_product_count,$scope.products_order,w=2);	
					if($scope.pincode!=null){
						$scope.check_availability_pincodeFun();
					}
					
				}, function myError(response) {
					
				});
	}else{

			if(localStorage.LocalCustomerCartLS !=null){
				
				$scope.products_order=JSON.parse(localStorage.LocalCustomerCartLS);
				
				$scope.order_product_count=$scope.products_order.length;
				Scopes.store('OrderController', $scope);
				total_netprice=0;
				if($scope.order_product_count==0){
					$scope.empty_cart_in_ordercontroller=1;
					$(".cart_summary").css({"display":"none"});
					$(".proceed_to_checkout").css({"display":"none"});
				}
			
				$scope.update_cart_controller_scopes($scope.order_product_count,$scope.products_order);
				if($scope.pincode!=null){
					$scope.check_availability_pincodeFun();
				}
			}else{
				$scope.empty_cart_in_ordercontroller=1;
				$(".cart_summary").css({"display":"none"});
				$(".proceed_to_checkout").css({"display":"none"});
			}
	}
	
	$scope.pincode_has_no_service=function(){
		
		for(v=0;v<$scope.order_product_count;v++){
			
			$scope.products_order[v].shipping_charge=-1;
			$scope.products_order[v].logistics_id=-1;
			$scope.products_order[v].logistics_parcel_category_id=-1;
			$scope.products_order[v].parcel_category_multiplier=-1;
			$scope.products_order[v].weight_multiplier=-1;
			$scope.products_order[v].default_delivery_mode_id=-1;
			$scope.products_order[v].logistics_territory_id=-1;
			$scope.products_order[v].territory_duration=-1;
			$scope.products_order[v].msg_style="color:red;";
			$scope.products_order[v].delivery_service_msg='Service Not is available';
		}

		var update_cart_info_post_data=$scope.products_order;
		if(c_id!=null && c_id!=''){
			$scope.update_and_get_cartable_info_in_json_format_multiple(update_cart_info_post_data);
		}else{
			$scope.update_cart_controller_scopes($scope.order_product_count,$scope.products_order);	
		}		
	}
	
	$scope.get_corresponding_shipping_charge=function(check){
		
		data={'pincode':$scope.pincode,'cart_data':$scope.products_order};
	
		$http({
		method : "POST",
		url : "<?php echo base_url();?>get_corresponding_shipping_charge",
		data:data,
		dataType:"JSON",
		async:false
		}).success(function mySucces(result) {
		for(u=0;u<result.length;u++){
			if(result[u].delivery_service==true){
				$scope.products_order[u].shipping_charge=result[u].shipping_charge;
				
				$scope.products_order[u].delivery_service=result[u].delivery_service;
				$scope.products_order[u].logistics_id=result[u].logistics_id;
				$scope.products_order[u].logistics_territory_id=result[u].logistics_territory_id;
				
				$scope.products_order[u].default_delivery_mode_id=result[u].default_delivery_mode_id;
				$scope.products_order[u].logistics_parcel_category_id=result[u].logistics_parcel_category_id;
				$scope.products_order[u].parcel_category_multiplier=result[u].parcel_category_multiplier;
				$scope.products_order[u].weight_multiplier=result[u].weight_multiplier;					
				$scope.products_order[u].territory_duration=result[u].territory_duration_org;	
				$scope.products_order[u].delivery_service_msg='Service is available';	
				$scope.products_order[u].msg_style="color:#333;";
				localStorage.setItem("LocalCustomerCartLS",JSON.stringify($scope.products_order))
			}
			if(result[u].delivery_service==false){
				$scope.products_order[u].shipping_charge=-1;	
				$scope.products_order[u].delivery_service=false;
				$scope.products_order[u].logistics_id=-1;
				$scope.products_order[u].logistics_territory_id=-1;
				$scope.products_order[u].default_delivery_mode_id=-1;
				$scope.products_order[u].logistics_parcel_category_id=-1;
				$scope.products_order[u].parcel_category_multiplier=-1;
				$scope.products_order[u].weight_multiplier=-1;
				$scope.products_order[u].territory_duration=-1;
				
				$scope.products_order[u].msg_style="color:red;";
				$scope.products_order[u].delivery_service_msg='Service is not available';	
			}
			if(result[u].delivery_service=="free"){
				//alert('delivery charges free');
				$scope.products_order[u].shipping_charge=0;	
				$scope.products_order[u].delivery_service="free";
				$scope.products_order[u].msg_style="color:green;";
				$scope.products_order[u].delivery_service_msg='Free Delivery service';
			}
			
				
		}			
		order_product_count=$scope.products_order.length;	
			
		var update_cart_info_post_data=$scope.products_order;
		
			if(c_id!=null && c_id!=''){
				$scope.update_and_get_cartable_info_in_json_format_multiple(update_cart_info_post_data);	
			}else{
				$scope.update_cart_controller_scopes($scope.order_product_count,$scope.products_order,1);	
			}
			/*if(check=='check'){
				location.reload();
			}*/
		}, function myError(response) {
			
		});
		
	}
	$scope.update_and_get_cartable_info_in_json_format_multiple=function(update_cart_info_post_data){
		
		$http({
				method : "POST",
				url : "<?php echo base_url();?>Account/update_and_get_cartable_info_in_json_format_multiple",
				data:update_cart_info_post_data,
				dataType:"JSON",
				}).success(function mySucces(result) {
				$scope.products_order=result;
				$scope.order_product_count=$scope.products_order.length;
				$scope.update_cart_controller_scopes($scope.order_product_count,$scope.products_order);
	
				}, function myError(response) {	
		});
	}
	
	$scope.show_availability_pincode_div_fun=function(){
		$("#available_pincode_div").css({"display":"block"});
		$("#available_pincode_status_div").hide();
	}	
	$scope.increment = function(x) {
		if (parseInt($scope.products_order[x].inventory_moq) >= parseInt($scope.products_order[x].inventory_max_oq)) {
			bootbox.alert({ 
			  size: "small",
			  message: 'This value is not allowed. Maximum order Quantity is '+$scope.products_order[x].inventory_moq,
			});
		return; }
		
		if($scope.products_order[x].inventory_moq==$scope.products_order[x].stock_available){
			alert('Sorry! There is no stock');
			return false;
		}
		$scope.products_order[x].inventory_moq++;
		$scope.update_cart_by_order_quantity(x);
	};
	  
	$scope.decrement = function(x){
		if(parseInt($scope.products_order[x].inventory_moq) <= parseInt($scope.products_order[x].inventory_moq_original)) { 
			bootbox.alert({ 
			  size: "small",
			  message: 'This value is not allowed. Minimum order Quantity is '+$scope.products_order[x].inventory_moq,
			});
			return; 
		}
		$scope.products_order[x].inventory_moq--;
		$scope.update_cart_by_order_quantity(x);
	};
	  
	$scope.keypup=function(evt,x){
		
		
		if(parseInt($scope.products_order[x].inventory_moq) >= parseInt($scope.products_order[x].inventory_moq_original) && parseInt($scope.products_order[x].inventory_moq)<=parseInt($scope.products_order[x].inventory_max_oq)){
			$scope.update_cart_by_order_quantity(x);
		}else{
			bootbox.alert({ 
			  size: "small",
			  message: 'This value is not allowed. Maximum order Quantity is '+$scope.products_order[x].inventory_max_oq,
			  //callback: function(x){ $scope.update_moq(x); }
			});
			$scope.products_order[x].inventory_moq=$scope.products_order[x].inventory_max_oq;

			$scope.update_cart_by_order_quantity(x);
		}
	}
	$scope.update_moq=function(x){
		$scope.products_order[x].inventory_moq=$scope.products_order[x].inventory_max_oq;
		return true;
	}	
	$scope.update_cart_by_order_quantity=function (x){
		
	//alert($scope.products_order[x].promotion_minimum_quantity);
	$scope.products_order[x].cash_back_amount=""

		$scope.products_order[x].inventory_weight_in_grams=parseInt($scope.products_order[x].inventory_moq)*parseInt($scope.products_order[x].inventory_weight_in_grams_for_unit);

		//////////////////////added/////////////////////
		
		if($scope.products_order[x].promotion_available==1){
				
				if($scope.products_order[x].default_discount!=""){
				
			
				modulo=$scope.products_order[x].inventory_moq%$scope.products_order[x].promotion_minimum_quantity;
						
				$scope.products_order[x].modulo=modulo;	
				
					 
					 
			if(modulo!=0 && ($scope.products_order[x].inventory_moq>=parseInt($scope.products_order[x].promotion_minimum_quantity))){
				
				   
				$scope.products_order[x].price_of_modulo_1=Math.round($scope.products_order[x].selling_price-(($scope.products_order[x].selling_price*$scope.products_order[x].default_discount)/100));
			
				$scope.products_order[x].price_after_default_discount=Math.round(modulo*$scope.products_order[x].price_of_modulo_1);
				quotient=$scope.products_order[x].inventory_moq/$scope.products_order[x].promotion_minimum_quantity;
				quotient=Math.floor(quotient);	
		
			
				if(quotient!=0){
					$scope.products_order[x].quotient=$scope.products_order[x].promotion_minimum_quantity*quotient;	
		
					$scope.products_order[x].price_of_quotient_1=Math.round($scope.products_order[x].selling_price-(($scope.products_order[x].selling_price*$scope.products_order[x].promotion_discount)/100));
					$scope.products_order[x].price_after_promotion_discount=Math.round($scope.products_order[x].promotion_minimum_quantity*quotient*$scope.products_order[x].price_of_quotient_1);
	
				}
		
				if($scope.products_order[x].price_after_promotion_discount!=null){
					$scope.products_order[x].final_product_individual=parseInt($scope.products_order[x].price_after_promotion_discount)+parseInt($scope.products_order[x].price_after_default_discount);
				}else{

					$scope.products_order[x].final_product_individual=$scope.products_order[x].price_after_default_discount;
				}
				
			}else{
			
				
				if($scope.products_order[x].inventory_moq>=parseInt($scope.products_order[x].promotion_minimum_quantity)){
					
/*$scope.products_order[x].price_of_quotient_1=$scope.products_order[x].selling_price-(($scope.products_order[x].selling_price*$scope.products_order[x].default_discount)/100);	
			$scope.products_order[x].price_after_default_discount=$scope.products_order[x].quantity_with_promotion*$scope.products_order[x].price_of_modulo_1;
            alert($scope.products_order[x].default_discount)
            alert($scope.products_order[x].price_after_default_discount)
            return false;*/		
					quotient=$scope.products_order[x].inventory_moq/$scope.products_order[x].promotion_minimum_quantity;
					quotient=Math.floor(quotient);		
					$scope.products_order[x].quotient=$scope.products_order[x].promotion_minimum_quantity*quotient;
					$scope.products_order[x].price_of_quotient_1=Math.round($scope.products_order[x].selling_price-(($scope.products_order[x].selling_price*$scope.products_order[x].promotion_discount)/100));
					$scope.products_order[x].price_after_promotion_discount=Math.round($scope.products_order[x].promotion_minimum_quantity*quotient*$scope.products_order[x].price_of_quotient_1);
					
					$scope.products_order[x].final_product_individual=parseInt($scope.products_order[x].price_after_promotion_discount);
						
					$scope.products_order[x].price_after_default_discount=0;
					
	
				
				}else{
					
					quotient=$scope.products_order[x].inventory_moq/$scope.products_order[x].promotion_minimum_quantity;
					quotient=Math.floor(quotient);	
					$scope.products_order[x].quotient=$scope.products_order[x].promotion_minimum_quantity*quotient;

					$scope.products_order[x].price_of_modulo_1=Math.round($scope.products_order[x].selling_price-(($scope.products_order[x].selling_price*$scope.products_order[x].default_discount)/100));
					
					$scope.products_order[x].price_after_default_discount=modulo*$scope.products_order[x].price_of_modulo_1;
	
					$scope.products_order[x].final_product_individual=parseInt($scope.products_order[x].price_after_default_discount);
					
				}
			
			}
	
				}//discount promotion !=0
				
		if($scope.products_order[x].default_discount==""){

			modulo=$scope.products_order[x].inventory_moq%$scope.products_order[x].promotion_minimum_quantity;
			$scope.products_order[x].modulo=modulo;		
			if(modulo!=0){
				

				$scope.products_order[x].price_of_modulo_1=$scope.products_order[x].selling_price;
					
				$scope.products_order[x].price_after_default_discount=modulo*$scope.products_order[x].price_of_modulo_1;
				quotient=$scope.products_order[x].inventory_moq/$scope.products_order[x].promotion_minimum_quantity;
				quotient=Math.floor(quotient);				
				if(quotient!=0){
					$scope.products_order[x].quotient=$scope.products_order[x].promotion_minimum_quantity*quotient;	
					$scope.products_order[j].price_of_quotient_1=Math.round($scope.products_order[x].selling_price-(($scope.products_order[x].selling_price*$scope.products_order[x].promotion_discount)/100));
		
					$scope.products_order[x].price_after_promotion_discount=Math.round($scope.products_order[x].promotion_minimum_quantity*quotient*$scope.products_order[x].price_of_quotient_1);
				}
				

				if($scope.products_order[x].price_after_promotion_discount!=null){
					$scope.products_order[x].final_product_individual=parseInt($scope.products_order[x].price_after_promotion_discount)+parseInt($scope.products_order[x].price_after_default_discount);
				}else{
	
					$scope.products_order[x].final_product_individual=parseInt($scope.products_order[x].price_after_default_discount);
				}

				
			}else{		
	
				quotient=$scope.products_order[x].inventory_moq/$scope.products_order[x].promotion_minimum_quantity;
				quotient=Math.floor(quotient);		
				$scope.products_order[x].quotient=$scope.products_order[x].promotion_minimum_quantity*quotient;
				$scope.products_order[x].price_of_quotient_1=Math.round($scope.products_order[x].selling_price-(($scope.products_order[x].selling_price*$scope.products_order[x].promotion_discount)/100));
				$scope.products_order[x].price_after_promotion_discount=Math.round($scope.products_order[x].promotion_minimum_quantity*quotient*$scope.products_order[x].price_of_quotient_1);
				
				$scope.products_order[x].final_product_individual=$scope.products_order[x].price_after_promotion_discount;				
				$scope.products_order[x].price_after_default_discount=0;

			
			}

		}

			if($scope.products_order[x].promotion_cashback>0){	
			cash_back_amount=parseFloat($scope.products_order[x].final_product_individual*($scope.products_order[x].promotion_cashback/100)).toFixed(2);
			$scope.products_order[x].cash_back_value=parseFloat(cash_back_amount)
			}else{
			
			cash_back_amount="";
			$scope.products_order[x].cash_back_value=cash_back_amount;
			}
				
			}//promo avai 1
			else{
		$scope.products_order[x].individual_price_of_product_without_promotion='';
		$scope.products_order[x].total_price_of_product_without_promotion='';
		$scope.products_order[x].total_price_of_product_with_promotion='' ;
		$scope.products_order[x].quantity_without_promotion='';
		$scope.products_order[x].quantity_with_promotion='';
			cash_back_amount="";
            $scope.products_order[x].cash_back_value=cash_back_amount;	
				
			}
		
		
		if($scope.products_order[x].price_of_modulo_1){
			$scope.products_order[x].individual_price_of_product_without_promotion=$scope.products_order[x].price_of_modulo_1;
		}else{
	$scope.products_order[x].price_of_modulo_1='';	$scope.products_order[x].individual_price_of_product_without_promotion=$scope.products_order[x].price_of_modulo_1;	
		}
		
		if($scope.products_order[x].price_of_quotient_1){
			$scope.products_order[x].individual_price_of_product_with_promotion=$scope.products_order[x].price_of_quotient_1;
		}else{
	$scope.products_order[x].price_of_quotient_1='';	$scope.products_order[x].individual_price_of_product_with_promotion=$scope.products_order[x].price_of_quotient_1;	
		}
		
	
		if($scope.products_order[x].price_after_default_discount){
					$scope.products_order[x].total_price_of_product_without_promotion=$scope.products_order[x].price_after_default_discount;
		}else{
			
			$scope.products_order[x].price_after_default_discount='';
			$scope.products_order[x].total_price_of_product_without_promotion=$scope.products_order[x].price_after_default_discount;
		}

		if($scope.products_order[x].price_after_promotion_discount){
			
			$scope.products_order[x].total_price_of_product_with_promotion=$scope.products_order[x].price_after_promotion_discount;
		}else{
			
		$scope.products_order[x].price_after_promotion_discount='';	$scope.products_order[x].total_price_of_product_with_promotion=$scope.products_order[x].price_after_promotion_discount;
		}
			
		if($scope.products_order[x].modulo){
			$scope.products_order[x].quantity_without_promotion=$scope.products_order[x].modulo;
		}else{
			$scope.products_order[x].modulo='';
		$scope.products_order[x].quantity_without_promotion=$scope.products_order[x].modulo;	
		}
		
		if($scope.products_order[x].quotient){
			$scope.products_order[x].quantity_with_promotion=$scope.products_order[x].quotient;
		}else{
			$scope.products_order[x].quotient='';
			$scope.products_order[x].quantity_with_promotion=$scope.products_order[x].quotient;	
		}
		
		
		//////////////////////added/////////////////////
		
		
		if(c_id!='' && c_id!=null){
			
			
			var update_cart_info_post_data = {'cart_id' : $scope.products_order[x].cart_id,'inventory_moq' : $scope.products_order[x].inventory_moq,'inventory_weight_in_grams': $scope.products_order[x].inventory_weight_in_grams,'logistics_id':$scope.products_order[x].logistics_id,'inventory_volume_in_mm':$scope.products_order[x].inventory_volume_in_mm,'logistics_territory_id':$scope.products_order[x].logistics_territory_id,'default_delivery_mode_id':$scope.products_order[x].default_delivery_mode_id,'parcel_category_multiplier':$scope.products_order[x].parcel_category_multiplier,'logistics_parcel_category_id':$scope.products_order[x].logistics_parcel_category_id,'territory_duration':$scope.products_order[x].territory_duration,'inventory_weight_in_grams_org':$scope.products_order[x].inventory_weight_in_grams_org,'inventory_weight_in_grams_for_unit':$scope.products_order[x].inventory_weight_in_grams_for_unit,'shipping_charge':$scope.products_order[x].shipping_charge,'individual_price_of_product_with_promotion':$scope.products_order[x].individual_price_of_product_with_promotion,'individual_price_of_product_without_promotion':$scope.products_order[x].individual_price_of_product_without_promotion,'total_price_of_product_without_promotion':$scope.products_order[x].total_price_of_product_without_promotion,'total_price_of_product_with_promotion':$scope.products_order[x].total_price_of_product_with_promotion,'quantity_without_promotion':$scope.products_order[x].quantity_without_promotion,'quantity_with_promotion':$scope.products_order[x].quantity_with_promotion,'promotion_available':$scope.products_order[x].promotion_available,'cash_back_value':$scope.products_order[x].cash_back_value};

			$http({
				method : "POST",
				url : "<?php echo base_url();?>Account/update_and_get_cartable_info_in_json_format",
				data:update_cart_info_post_data,
				dataType:"JSON",
				}).success(function mySucces(result) {		
						$scope.products_order=result;
						$scope.order_product_count=$scope.products_order.length;
						
						$scope.update_cart_controller_scopes($scope.order_product_count,$scope.products_order);
				}, function myError(response) {
					
				});
		}else{
			var update_cart_info_post_data = {'cart_id' : $scope.products_order[x].cart_id,'inventory_moq' : $scope.products_order[x].inventory_moq,'inventory_weight_in_grams': $scope.products_order[x].inventory_weight_in_grams,'logistics_id':$scope.products_order[x].logistics_id,'inventory_volume_in_mm':$scope.products_order[x].inventory_volume_in_mm,'logistics_territory_id':$scope.products_order[x].logistics_territory_id,'default_delivery_mode_id':$scope.products_order[x].default_delivery_mode_id,'parcel_category_multiplier':$scope.products_order[x].parcel_category_multiplier,'shipping_charge':$scope.products_order[x].shipping_charge,'individual_price_of_product_with_promotion':$scope.products_order[x].individual_price_of_product_with_promotion,'individual_price_of_product_without_promotion':$scope.products_order[x].individual_price_of_product_without_promotion,'total_price_of_product_without_promotion':$scope.products_order[x].total_price_of_product_without_promotion,'total_price_of_product_with_promotion':$scope.products_order[x].total_price_of_product_with_promotion,'quantity_without_promotion':$scope.products_order[x].quantity_without_promotion,'quantity_with_promotion':$scope.products_order[x].quantity_with_promotion,'promotion_available':$scope.products_order[x].promotion_available,'cash_back_value':$scope.products_order[x].cash_back_value};
    

			
			$http({
				method : "POST",
				url : "<?php echo base_url();?>Account/get_shipping_charge_without_session",
				data:update_cart_info_post_data,
				dataType:"JSON",
				}).success(function mySucces(result) {
				
				//alert(JSON.stringify(result));

				$scope.products_order[x].weight_multiplier=result.weight_multiplier;
				$scope.products_order[x].shipping_charge=result.shipping_charge;
				$scope.order_product_count=$scope.products_order.length;
				$scope.update_cart_controller_scopes($scope.order_product_count,$scope.products_order,1);
				}, function myError(response) {
					
				});
		localStorage.setItem("LocalCustomerCartLS",JSON.stringify($scope.products_order));
		}
	}
	
	$scope.removeItem = function (x) {
		if(document.getElementById('extra_data_'+x)!=null){
			document.getElementById('extra_data_'+x).innerHTML="";	
		}
	   
	if(c_id!='' && c_id!=null){
			total_netprice=0;
			
			var remove_cart_info_post_data = {'cart_id' : $scope.products_order[x].cart_id};
			$http({
				method : "POST",
				url : "<?php echo base_url();?>Account/remove_cart_info_for_session_customer",
				data:remove_cart_info_post_data,
				dataType:"JSON",
				}).success(function mySucces(result) {
					
					$scope.products_order.splice(x, 1);
					$scope.order_product_count=$scope.products_order.length;
					total_netprice=0;
					if($scope.order_product_count==0){
						$scope.empty_cart_in_ordercontroller=1;
						$(".cart_summary").css({"display":"none"});
						$(".proceed_to_checkout").css({"display":"none"});
					}else{
						$scope.empty_cart_in_ordercontroller=0;
						$(".cart_summary").css({"display":""});
						$(".proceed_to_checkout").css({"display":""});
					}
					
					$scope.update_cart_controller_scopes($scope.order_product_count,$scope.products_order);
					$cartElement=document.getElementsByClassName("cart_count");
					for(i=0;i<$cartElement.length;i++){
						$cartElement[i].innerHTML=$scope.order_product_count;
					}
					
				}, function myError(response) {
					
				});
			
		}else{
	
        $scope.errortext = "";
		$scope.products_order.splice(x, 1);
		localStorage.setItem("LocalCustomerCartLS", JSON.stringify($scope.products_order));
		$scope.order_product_count=$scope.products_order.length;		
		
		Scopes.get('CartController').cart_count=$scope.order_product_count;
		
		total_netprice=0;
		if($scope.order_product_count==0){
			$scope.empty_cart_in_ordercontroller=1;
			$(".cart_summary").css({"display":"none"});		
			$(".proceed_to_checkout").css({"display":"none"});
		}
		$scope.update_cart_controller_scopes($scope.order_product_count,$scope.products_order);
		
		}
	
    }
	if($scope.searched_pincode !=null){
		$scope.pincode=$scope.searched_pincode;
		$("#available_pincode_status_div").show();
		$("#available_pincode_div").hide();
	}else{
		$("#available_pincode_div").show();
		$("#available_pincode_status_div").hide();
		
	}
	
	$scope.proceedCheckout=function(){
		if(c_id!='' && c_id!=null){
			location.href="<?php echo base_url()?>checkout";
		}else{
			<?php
				$this->session->set_userdata("cur_page","checkout");
			?>
			location.href="<?php echo base_url()?>login";	
		}
	}
}

function isNumber(evt){
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    //if(charCode > 31 && (charCode < 48 || charCode > 57 || charCode!=8 || charCode!=46)) {
	if(charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
		<?php 
		
		//print_r($promotions_cart);
		
		($promotions_cart);?>
		<?php if(!empty($promotions_cart)){?>
		<ul class="nav nav-stacked hide" id="cart_promotion">
			<?php foreach($promotions_cart as $data){
				$offer=htmlspecialchars_decode($data['promo_quote']);
				$offer = preg_replace("/\([^)]+\)/","",$offer); 
				?>
			
			<li id="<?php echo ($data['promo_uid']);?>"><?php echo $offer;?></li>
			<?php }?>
		</ul>
		<?php } ?>

    <div class="page-content page-order" ng-controller="OrderController">
		
	
        <div class="col-sm-6 col-sm-offset-3">
		<?php if(!empty($promotions_cart)){ ?>
		<div class="alert alert-info" ng-bind-html="explicitlyTrustedHtml_discount" ng-if="discount_oninvoice" ></div>
		<div class="alert alert-success" ng-bind-html="explicitlyTrustedHtml_cash_back" ng-if="cash_back_oninvoice"></div>
		<div class="alert alert-warning" ng-bind-html="explicitlyTrustedHtml_surprise_gift" ng-if="surprise_gift"></div>
		<div class="alert alert-danger" ng-bind-html="explicitlyTrustedHtml_free_shipping" ng-if="free_shipping"></div>
		<div class="alert alert-danger" ng-bind-html="explicitlyTrustedHtml_invoice_surprise_gift" ng-if="invoice_surprise_gift"></div>
		<?php } ?>
        </div>
		<div class="popular-tabs">

		<div class="col-md-12" style="padding: 0px 0px 17px 0px;">
  
			<div class="col-md-4">Your shopping cart contains:
				<span class="">{{order_product_count}} Product(s) </span>
				<span ng-if="final_count_free_items>0"> + {{final_count_free_items}} Free Items</span>
            </div>
			
			<!---- added for pincode search---->
			<div class="product-desc col-md-8"  id="available_pincode_div" style="display:none;">	
					<form name="userForm" novalidate>
					<div class="form-group col-md-4">
									
						<input id="available_pincode" ng-minlength="6" ng-maxlength="6" ng-model="pincode" name="available_pincode" class="form-control" placeholder="Enter Pincode" required type="text">
						<p class="help-block" ng-show="userForm.available_pincode.$error.required || userForm.available_pincode.$error.number">Valid Pincode number is required
						</p>			
					
						<p class="help-block" ng-show="((userForm.available_pincode.$error.minlength ||	   userForm.available_pincode.$error.maxlength) && 		   userForm.available_pincode.$dirty)">
								Pincode should be 6 digits
						</p>

					</div>
					<div class="form-group col-md-2">
						<button class="button" style="padding:6px 15px;" ng-click="check_availability_pincodeFun('check')"> <i class="glyphicon glyphicon-map-marker" style="top:3px;"></i> Check</button>
					</div>
					</form>
								
				<div ng-if="searched_pincode_msg" class="col-md-4">{{searched_pincode_msg}}</div>
			</div>

			<div class="product-desc col-md-8" id="available_pincode_status_div" style="display:none;">
			<i class="glyphicon glyphicon-map-marker" style="top:3px;"></i>{{searched_pincode_msg}} {{pincode}} <u style="cursor:pointer;" ng-click="show_availability_pincode_div_fun()">Change</u>							
			</div>
				
		</div>
			<!---- added for pincode search---->
			
				<div id="order-detail-content" class="col-md-12 overflow">
					<table class="table table-responsive cart_summary">
						
						<tbody>

							<tr ng-repeat="(key, value) in products_order track by $index">
								<td colspan="8" style="border:none;">
							

								<table class="table table-bordered table-responsive cart_summary">
								<thead>
									<tr>
										<th class="cart_product">Product</th>
										<th>Description</th>
										<th width="10%" class="text-center">Qty</th>
										<th width="5%" ng-if="value.promotion_available==1" class="text-center">Break-up</th>
										
										<th width="20%" ng-if="value.promotion_available==1"  class="text-center">Promo</th>
										<th width="10%" class="text-center" width="10%">Price</th>
										<th width="20%" class="text-center">Shipping and delivery</th>
										<th width="10%" class="text-center">Subtotal</th>
										

									</tr>
								</thead>
							<tr>
									
								<td class="cart_product">
									<a href="<?php echo base_url();?>detail/<?php echo $controller->sample_code(); ?>{{value.inventory_id}}"><img ng-src="<?php echo base_url();?>{{value.inventory_image}}" alt="Product"></a>
									
								</td>
	
								<td class="cart_description">
									<p class="product-name"><a href="<?php echo base_url();?>detail/<?php echo $controller->sample_code(); ?>{{value.inventory_id}}">{{value.product_name}} </a></p>
									<small class="cart_ref">{{value.inventory_sku}}</small><br>
									<small><a href="#">{{value.attribute_1}} : {{value.attribute_1_value}}</a></small><br>   
									<small ng-if="value.attribute_2!=''"><a href="#">{{value.attribute_2}} : {{value.attribute_2_value}}</a></small>
								</td>								
								<td class="qty">
									<div class="row" style="display:inline-block;">
									<a class="col-md-3" href="javascript:;" ng-click="decrement($index);"><i class="fa fa-minus" ></i></a>	
									
									<input class="form-control input-sm col-md-3" ng-model="value.inventory_moq" type="text" onkeypress="return isNumber(event);" ng-keyup="keypup($event,$index)">
									<a class="col-md-3" href="javascript:;" ng-click="increment($index);" ><i class="fa fa-plus" ></i></a>
									
									
									</div>

								</td>
								<td colspan="5" class="text-center" ng-if="value.item_stock_available==0"><!---apply opacity over here--->
								<span ng-if="value.item_stock_available==0" style="color:red;">Out of stock</span>
									<span ng-if="(value.item_stock_available==0 && value.stock_available>0)">Available unit(s): {{value.stock_available}}</span>
									
								</td>
								
								<td colspan="5" ng-if="value.item_stock_available==1">
									<table class="" width="100%">
									<tr ng-if="value.promotion_available==0">
										<td width="10%">
											{{value.inventory_moq}} * {{value.selling_price}}
										</td>
										<td width="20%">
												<span ng-if="value.shipping_charge_view==1"> <i class="fa fa-inr" aria-hidden="true"></i> {{value.shipping_charge}}</span>
									<span ng-if="value.shipping_charge_view==0" ng-model="shipping_charge">Shipping Charge {{value.shipping_charge_msg}}</span>
									<span><br>Delivery By {{value.delivery_date}}</span>
									<span ng-if="value.delivery_service_msg"><br> <span style="{{value.msg_style}}">{{value.delivery_service_msg}}</span>
												</span>
										</td>
										<td width="10%" class="text-right">
										{{value.inventory_selling_price_without_discount}}
										
										</td>
									</tr>
										
									<tr ng-if="value.promotion_available==1 && value.discount_quote_show==1">
											<td width="5%">{{value.discount}}</td>
											<td width="20%"><span ng-if="value.discount_quote_show==1">	{{value.promotion_default_discount_promo}}</span></td>
											<td width="10%"><span ng-if="value.modulo!=0">{{value.modulo}} * {{value.price_of_modulo_1}}<br></span></td>
											<td width="20%" rowspan="2">
												<span ng-if="value.shipping_charge_view==1"> <i class="fa fa-inr" aria-hidden="true"></i> {{value.shipping_charge}}</span>
									<span ng-if="value.shipping_charge_view==0" ng-model="shipping_charge">Shipping Charge {{value.shipping_charge_msg}}</span>
									<span><br>Delivery By {{value.delivery_date}}</span>
									<span ng-if="value.delivery_service_msg"><br> <span style="{{value.msg_style}}">{{value.delivery_service_msg}}</span>
												</span>
											</td>
											<td width="10%" class="text-right"><span ng-if="value.price_after_default_discount>0">{{value.price_after_default_discount}}</span></td>
									</tr>
										
	
									<tr ng-if="value.promotion_available==1 && value.promotion_quote_show==1">
											<td width="5%">{{value.promotion}}</td>
											<td width="20%"><span ng-if="value.promotion_quote_show==1">	{{value.promotion_quote}}<br></span></td>
										
										
											<td width="10%"><span> {{value.quotient}} * {{value.price_of_quotient_1}}</span> 
											
											</td>
										
										
										
											<td width="20%" rowspan="2" ng-if="value.discount_quote_show==0">
											<span ng-if="value.shipping_charge_view==1"> <i class="fa fa-inr" aria-hidden="true"></i> {{value.shipping_charge}}</span>
									<span ng-if="value.shipping_charge_view==0" ng-model="shipping_charge">Shipping Charge {{value.shipping_charge_msg}}</span>
									<span><br>Delivery By {{value.delivery_date}}</span>
									<span ng-if="value.delivery_service_msg"><br> <span style="{{value.msg_style}}">{{value.delivery_service_msg}}</span>
												</span>
											</td>		
											<td width="10%" class="text-right"><span ng-if="value.price_after_promotion_discount>0"> {{value.price_after_promotion_discount}}</span>
									<span ng-if="value.promotion_available==0">
									
										{{value.inventory_selling_price_without_discount}}
									</span>
											</td>
										
										
										</tr>
	
									</table>
								</td>
								

							</tr>
									
									<tbody  class="extra_data" id="extra_data_{{$index}}" ng-if="value.show_addon_items==1">

									</tbody>
									
									<tr ng-if="value.promotion_available==1">
										<td colspan="8">
											<table class="" width="100%">
											
												<td class="text-right" colspan="8" >
													<u class="red"><a href='#' class="red btn-sm" ng-click="removeItem($index)"> Remove Item</a></u>
												
													<span class="text-right" colspan="1" ng-if="value.item_stock_available==1">
														<span>  <i class="fa fa-inr" aria-hidden="true"></i> {{value.subtotal}} </span>
													</span>
												</td>
											</table>
										</td>
									
									</tr>
									
									<tr ng-if="value.promotion_available==0">
										<td colspan="8">
											<table class="" width="100%">
												<td class="text-right" colspan="8" >
													<u class="red"><a href='#' class="red btn-sm" ng-click="removeItem($index)"> Remove Item</a></u>
												
												<span class="text-right" ng-if="value.item_stock_available==1">
												
													<span>  <i class="fa fa-inr" aria-hidden="true"></i> {{value.subtotal}}</span>
												</span>
												
												</td>
											</table>
										</td>	
									</tr>
										
										
								</table>
								
								</td>
							</tr>
							
							
						
						
						<?php if(!empty($promotions_cart)){ ?>
					<tr style="font-size:.95em;">

							<td colspan="8" style="border:none;">
								<table class="table-bordered" width="100%">

						<tfoot ng-if="out_of_stock_item_exists==0">
						
							<tr class="bg-success">

								<td>Total products price (tax incl.)
								
								<!--{{total_shipping_price}} and {{total_price_without_shipping_price}} -->
								</td>
								
								<td class="text-right">
									<span ng-if="eligible_discount_percentage_display==1">
									+ <i class="fa fa-inr" aria-hidden="true"></i> {{grandtotal_without_discount}}
									</span>
									<span ng-if="eligible_discount_percentage_display==0">
									 <i class="fa fa-inr" aria-hidden="true"></i> {{amount_to_pay}}
									</span>
								</td>	
								
								
							</tr>
							<tr class="bg-danger" ng-if="eligible_discount_percentage_display==1"> 

								 <td ><span ng-if="eligible_discount_percentage_display==1"> You are eligible for discount on invoice {{eligible_discount_percentage}}% of {{total_price_without_shipping_price}} {{currency_type_discount}}</span></td>
								 <td  class="text-right" ng-if="eligible_discount_percentage_display==1">- <i class="fa fa-inr" aria-hidden="true"></i> {{grandTotal_for_discount}}</td>
							</tr>

							<tr class="bg-danger" ng-if="eligible_free_shipping_display==1"> 

								 <td ><span ng-if="eligible_free_shipping_display==1"> You are eligible for  Free shipping on  invoice of shipping charges {{total_shipping_price}} {{currency_type_free_shipping}} </span></td>
								 <td class="text-right" ng-if="eligible_free_shipping_display==1">- <i class="fa fa-inr" aria-hidden="true"></i> {{total_shipping_price}}</td>
							</tr>
							
							
							<tr class="bg-success" ng-if="eligible_discount_percentage_display==1 || eligible_free_shipping_display==1"> 

								 <td><span ng-if="eligible_discount_percentage_display==1 || eligible_free_shipping_display==1"> To amount payable on invoice after <span ng-if="eligible_discount_percentage_display==1"> discount </span>
								 
								 <span ng-if="eligible_discount_percentage_display==1 && eligible_free_shipping_display==1">  and </span>
								 </span>
								 
								 <span ng-if="eligible_free_shipping_display==1"> after deduction shipping charges </span>

								 
								 
								 </td>
								 <td class="text-right"><i class="fa fa-inr" aria-hidden="true"></i> {{amount_to_pay}}</td>	
							</tr>
							
							<tr class="bg-danger" ng-if="eligible_discount_percentage_display==1 || eligible_free_shipping_display==1"> 

								 <td>
								 <span ng-if="eligible_discount_percentage_display==1 || eligible_free_shipping_display==1"> 	You Saved 
								 </span>
								 </td>
								 <td class="text-right"><i class="fa fa-inr" aria-hidden="true"></i> {{you_saved}}</td>	
							</tr>
							<tr ng-if="eligible_cash_back_percentage_display==1" class="bg-info"> 

									<td ><span ng-if="eligible_cash_back_percentage_display==1"> Total amount to be credited to your Wallet </span></td>
									<td  class="text-right" ng-if="eligible_cash_back_percentage_display==1">  <i class="fa fa-inr" aria-hidden="true"></i> {{total_cash_back}}</td>
									
							</tr>
							
						</tfoot>
						
						
						
						</table>
						</td>
					</tr>
	
						<?php }else{ ?>
						
						<tr ng-if="out_of_stock_item_exists==0" style="font-size:.95em;">

							<td colspan="8" style="border:none;">
								<table class="table-bordered" width="100%">
									<tbody>
									<tr>
										<td class="text-right" colspan="8">

											Total products price1 (tax incl.)<!--{{total_price_without_shipping_price}}-->
												
											<span><i class="fa fa-inr" aria-hidden="true"></i> {{amount_to_pay}}</span>
										</td>
									</tr>	
									</tbody>
								</table>												
							</td>
						</tr> 
						
						<?php } ?>
						
	<tr ng-if="out_of_stock_item_exists==0" style="font-size:.95em;">

							<td colspan="8" style="border:none;">
								<table class="table-bordered" width="100%">
	<?php if(!empty($promotions_cart)){ ?>
	<tfoot ng-if="show_surprise_gift_on_invoice==1 && out_of_stock_item_exists==0">
		<tr class="bg-warning" ng-if="invoice_surprise_gift_result">
			<td>
				<span class="left-align-cashback">Eligible for surprise gifts (<span ng-bind-html="explicitlyTrustedHtml_invoice_surprise_gift_result" ></span></span>
			<span class="text-right" style="width:30%;font-size: 1.2em;"> <small><i class="fa fa-gift" aria-hidden="true"></i> </small></span>
			
			</td>
			
		</tr>
	</tfoot>
	<?php } ?>
	
	<tfoot ng-if="out_of_stock_item_exists==0">
						
		<tr ng-if="show_cash_back_tr==1" class="bg-info">
			<td colspan="2" style="padding: 1px;">

				<div class="panel-group bg-info" style="margin:0px;">
					<div class="panel panel-default">
						<div class="panel-heading" data-toggle="collapse" href="#collapse1" style="background-color:#d9edf7;cursor:pointer;">
							<span style="font-size:.95em;">
							
							<span class="panel-title text-right">
							<i class="more-less glyphicon glyphicon-plus"></i> <span class="left-align-cashback-total">Total cash back for SKUs </span><span class="text-right" style="width:30%;font-size: .9em;"><i class="fa fa-inr" aria-hidden="true"></i>{{total_cash_back_for_sku}}</span>
							
							</span></span>
						</div>
						<div id="collapse1" class="panel-collapse collapse">
							<ul class="list-group">
								 <li class="list-group-item" ng-repeat="(key,value) in cash_back_tr_arr" style="background-color:#d9edf7;"><span class="text-left left-align-cashback">			
									 {{value.promotion_cashback_percentage}} % cash back on {{value.sku_count}} quantity of {{value.inventory_sku}} </span><span class="text-right" style="width:30%"><i class="fa fa-inr" aria-hidden="true"></i> {{value.cash_back_amount}}</span>
								</li>
							</ul>
						
						</div>
					</div>
				</div>
	  
			</td>
	</tr>					
								
								
	<tr ng-if="eligible_cash_back_percentage_display==1" class="bg-info"> 

			 <td>
			 <span ng-if="eligible_cash_back_percentage_display==1" class="left-align-cashback"> You are eligible for  cash back on  invoice {{eligible_cash_back_percentage}}% </span>
			 <span class="text-right" ng-if="eligible_cash_back_percentage_display==1" style="width:30%;font-size: .9em;"><i class="fa fa-inr" aria-hidden="true"></i> {{grandTotal_for_cash_back}}</span>
			 </td>
	</tr>
						
	</tfoot>
	
</table>
</td>
</tr>
	
					</tbody><!---common tbody---->					
					
					</table>
                <div class="cart_navigation">
                    <a class="prev-btn" href="<?php echo base_url();?>index">Continue shopping</a>
                    
						<a id="proceed_to_checkout_id" class="next-btn proceed_to_checkout" ng-click="proceedCheckout()" href="#">Proceed to checkout</a>
					
                </div>
            </div>
		</div><!---tab---->		


	</div><!--page container----------->
	
</div>
</div>


<script>
window.onload=function(){
		var inv_elems_display_elems=document.getElementsByClassName('free_inv_nums')

	for(var i=0;i<inv_elems_display_elems.length;i++){
		inv_elems_display_elems[i].innerHTML=quotient
		//alert(quotient)
	}

}

$(document).ready(function(){
	
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
});
</script>
<!-- ./page wapper-->
