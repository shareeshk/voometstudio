<style type="text/css">

.margin-b-20{
    margin-top:20px ;
    margin-bottom:20px ;
    text-transform: capitalize;
}
.highlight{
    color:red;
}
.disc-style{
    list-style: disc;
    margin-left: 20px;
    margin-bottom: 10px;
}
</style>

<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo base_url()?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Privacy Policy</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">Infomations</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                 <ul class="tree-menu">
                                    <li><span></span><a href="<?php echo base_url();?>termsandconditions">Terms of Use</a></li>
									<li class="active"><span></span><a href="<?php echo base_url();?>policy">Privacy Policy</a></li>
                                    <li><span></span><a href="<?php echo base_url();?>business_policy" >Business Policies</a></li>

									<!-- <li><span></span><a href="<?php echo base_url();?>payments">Payments</a></li>
									
									<li><span></span><a href="<?php echo base_url();?>delivery">Delivery</a></li>
									<li><span></span><a href="<?php echo base_url();?>warranty">Warranty</a></li> -->
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
                
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading text-center">
                    <span class="page-heading-title2">Privacy Policy</span>
                </h2>
                <!-- Content page -->
                <div class="content-text clearfix">    


                <p>
                <span class="highlight">Effective as of 1st January 2024</span>
                <br>
                <br>
Please read this Privacy Policy carefully. By accessing this Website (as defined below), you accept all the terms and conditions of this Privacy Policy. We will require your consent for the use and disclosure of your personal data. By submitting your personal data to us online in our website, you agree to give us consent to use and disclose your personal data in accordance with this policy. If you do not agree with the terms and conditions of this Privacy Policy, you must immediately cease using and accessing this Website and any content, services or products contained in or available from this Website for which you need to provide personal information.

                </p>

             
<h4 class="margin-b-20">
   <b>Terms & Conditions</b>
</h4>

<h4 class="margin-b-20">
   <b>1. Information Collection</b>
</h4>

<p>We may collect personal information such as your name, email address, phone number, shipping address, and payment information when you make a purchase or interact with our website.
</p>

<h4 class="margin-b-20">
   <b>2. Use of Information</b>
</h4>
<p>
We use the information collected to process transactions, communicate with you, provide customer support, improve our products and services, and personalize your experience.
</p>

<h4 class="margin-b-20">
   <b>3. Data Security
</b>
</h4>
<p>We implement appropriate security measures to protect your personal information against unauthorized access, alteration, disclosure, or destruction.
</p>

<h4 class="margin-b-20">
   <b>4. Information Sharing
</b>
</h4>

<p>We may share your personal information with third-party service providers who assist us in conducting our business operations, such as payment processors and shipping companies. We may also disclose your information in compliance with legal obligations or to protect our rights and interests.

<h4 class="margin-b-20">
   <b>5. Marketing Communication
</b>
</h4>

<p>With your consent, we may send you marketing communications about our products, promotions, and events. You have the option to opt out of receiving these communications at any time.
</p>
<h4 class="margin-b-20">
   <b>6. Cookies

</b>
</h4>

<p>We use cookies and similar technologies to enhance your browsing experience, analyze website traffic, and personalize content. You can adjust your browser settings to disable cookies, although this may affect your ability to access certain features of our website.
</p>


<h4 class="margin-b-20">
   <b>7. Third-Party Links
</b>
</h4>

<p>Our website may contain links to third-party websites. We are not responsible for the privacy practices or content of these websites. We encourage you to review the privacy policies of these third parties before providing any personal information.
</p>

<h4 class="margin-b-20">
   <b>8. Collection and Use of Personal Information 

</b>
</h4>

<p>Except as set out in this Privacy Policy, we will not disclose any personally identifiable information without your permission unless we are legally entitled or required to do so (for example, if required to do so by legal process or for the purposes of prevention of fraud or other crime) or if we believe that such action is necessary to protect and/or defend our rights, property or personal safety and those of our users/customers etc. We collect and use your personal information (including names, addresses, telephone numbers and email addresses) for the purposes of: 
</p>

<ul class="disc-style">

<li>Responding to queries or requests submitted by you</li>
<li>Administering or otherwise carrying out our obligations in relation to any service you require or agreement you have with us</li>
<li>To improve the Website (we continually strive to improve Website offerings based on any information or feedback received from you)</li>
<li>Internal record keeping; and/or</li>
<li>Informing you of any events, services, newsletters and possible job opportunities</li>
</ul>
<p>If you do not wish us to use your personal information for these purposes, you may so advise us when you submit your personal information or at any time after that (see contact information below), which option is give in the Website. Upon receiving your advice that you do not wish us to use your information for these purposes we will either not use it that way, or will discontinue such use immediately, as the case may be.</p>
<p>We control the personal information collected through this Website. We may use third parties for the fulfillment of our obligations hereunder, and if so, we will require them to comply with the requirements of this Policy.</p>

<h4 class="margin-b-20">
   <b>9. Changes to this Policy

</b>
</h4>

<p>We reserve the right to update or modify this Privacy Policy at any time. Any changes will be effective immediately upon posting the revised policy on our website.
</p>
<h4 class="margin-b-20">
   <b>10. Contact Us

</b>
</h4>

<p>If you have any questions or concerns about our Privacy Policy, please contact us at  <b><?php echo SITE_EMAIL_ENQ; ?></b>

<br>
By using our website, you consent to the terms of this Privacy Policy. If you have comments or questions about this Privacy Policy, you can reach us at voometstudio@mailinator.com
</p>


                </div>
                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<!-- ./page wapper-->
