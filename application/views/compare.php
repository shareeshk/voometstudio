
<style type="text/css">
.overflow{
	overflow: auto;
}
</style>

<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo base_url();?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Compare</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">Compare Products</span>
        </h2>
        <!-- ../page heading-->
		
		<?php
			//print_r($compare_params_arr);
			$specification_group_name_arr=array();
			
			foreach($compare_params_arr as $v){
				$inventory_id=$v;
				$get_inventory_info_arr=$controller->get_inventory_info_by_inventory_id($inventory_id);
				$get_product_info_arr=$controller->get_product_info_by_inventory_id($inventory_id);
				
				
				/////////////////
				$all_ids_arr=array('brand_id'=>$get_product_info_arr["brand_id"],'subcat_id'=>$get_product_info_arr["subcat_id"],'cat_id'=>$get_product_info_arr["cat_id"],'pcat_id'=>$get_product_info_arr["pcat_id"]);
				
				$allowed_specification_group_arr=$controller->get_specification_group_by_category($all_ids_arr);
				//print_r($allowed_specification_group_arr);
				$allowed_specification_group=$controller->get_specification_groups($allowed_specification_group_arr);
				
				
				foreach($allowed_specification_group as $allowed_specification_group_arr){
								
								$specification_values_arr=$controller->get_specification_values($allowed_specification_group_arr["specification_group_id"],$get_inventory_info_arr["id"]);
								
								foreach($specification_values_arr as $specification_values){
							
									$specification_group_name_arr[$allowed_specification_group_arr["specification_group_name"]][]=$specification_values["specification_name"];
								
								
								}
								
					}
			}
			
			foreach($specification_group_name_arr as $k => $specification_name_arr){
				$specification_group_name_arr[$k]=array_unique($specification_name_arr);
			}
			//print_r($specification_group_name_arr);
			?>
			
			
			
		 <?php
			$main_arr=array();
			foreach($compare_params_arr as $inventory_id){
				$get_inventory_info_arr=$controller->get_inventory_info_by_inventory_id($inventory_id);
                               /* echo '<pre>';
                                print_r($get_inventory_info_arr);
                                echo '</pre>';*/
				$get_product_info_arr=$controller->get_product_info_by_inventory_id($inventory_id);
				$main_arr["Product Image"][]=$get_inventory_info_arr["common_image"];
				$main_arr["Product Name"][]=$get_product_info_arr["product_name"];
				$avg_value=$controller->get_average_value_of_ratings($inventory_id);
				$main_arr["Rating"][]=$avg_value."||".$inventory_id;
				$main_arr["Price"][]=$get_inventory_info_arr["selling_price"];
				$main_arr["Description"][]=$get_product_info_arr["product_description"];
				if($get_inventory_info_arr["product_status"]=="In Stock"){
					$main_arr["Availability"][]=$get_inventory_info_arr["product_status"]." "."(".$get_inventory_info_arr['stock']." Items)";
				}
				else{
					$main_arr["Availability"][]=$get_inventory_info_arr["product_status"];
				}
				$main_arr["SKU"][]=$get_inventory_info_arr["sku_id"];
				$main_arr[$get_inventory_info_arr["attribute_2"]][]=$get_inventory_info_arr["attribute_2_value"];
				$main_arr[$get_inventory_info_arr["attribute_1"]][]=$get_inventory_info_arr["attribute_1_value"];
                                if($get_inventory_info_arr["attribute_3"]!=''){
				$main_arr[$get_inventory_info_arr["attribute_3"]][]=$get_inventory_info_arr["attribute_3_value"];
                                }
                                if($get_inventory_info_arr["attribute_4"]!=''){
				$main_arr[$get_inventory_info_arr["attribute_4"]][]=$get_inventory_info_arr["attribute_4_value"];
                                }
				$main_arr["Action"][]=$inventory_id;
			}
			//print_r($main_arr);
		 ?>
		<div class="page-content overflow">
            <table class="table table-bordered table-compare">
                <?php
					foreach($main_arr as $label => $inventory_info_arr){
						?>
						<tr>
							<td   width="25%" class="compare-label"><?php echo $label;?></td>
							<?php
								foreach($inventory_info_arr as $actual_val){
									
										
											if($label=="Product Image"){
										?>
										<td width="25%">
											<a href="#">
												<img src="<?php echo base_url().$actual_val;?>" alt="Product">
											</a>
										</td>
										<?php
											}
											else if($label=="Action"){
										?>
										<td width="25%">
											<a href="#">
											<?php
												$rand_1=$controller->sample_code();
											?>
												<button class="add-cart button button-sm" onclick="location.href='<?php echo base_url()?>detail/<?php echo $rand_1.$actual_val;?>'">Add to Cart</button>
												
											</a>
										</td>
										<?php
											}
											else if($label=="Rating"){
												$actual_val_arr=explode("||",$actual_val);
												$actual_val_rating=$actual_val_arr[0];
												$actual_val_inventory_id=$actual_val_arr[1];
											?>
											<td width="25%">
				
												<div class="product-star">
													<?php 
													$str_star='';	
													
													$total_reviews=$controller->get_total_reviews($actual_val_inventory_id);
												
													if($actual_val_rating==0.5){
														
														$str_star.='<i class="fa fa-star-half-o"></i>';	
														$str_star.='<i class="fa fa-star-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
																		
													}elseif($actual_val_rating==1){
														
														$str_star.='<i class="fa fa-star"></i>';	
														$str_star.='<i class="fa fa-star-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
														
													}elseif($actual_val_rating==1.5){
														$str_star.='<i class="fa fa-star"></i>';	
														$str_star.='<i class="fa fa-star-half-o"></i>';	
														$str_star.='<i class="fa fa-star-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
													}elseif($actual_val_rating==2){
														$str_star.='<i class="fa fa-star"></i>';	
														$str_star.='<i class="fa fa-star"></i>';	
														$str_star.='<i class="fa fa-star-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
													}elseif($actual_val_rating==2.5){
														$str_star.='<i class="fa fa-star"></i>';	
														$str_star.='<i class="fa fa-star"></i>';	
														$str_star.='<i class="fa fa-star-half-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
														
													}elseif($actual_val_rating==3){
														$str_star.='<i class="fa fa-star"></i>';	
														$str_star.='<i class="fa fa-star"></i>';	
														$str_star.='<i class="fa fa-star"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
													}elseif($actual_val_rating==3.5){
														$str_star.='<i class="fa fa-star"></i>';	
														$str_star.='<i class="fa fa-star"></i>';	
														$str_star.='<i class="fa fa-star"></i>';
														$str_star.='<i class="fa fa-star-half-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
													}elseif($actual_val_rating==4){
														$str_star.='<i class="fa fa-star"></i>';	
														$str_star.='<i class="fa fa-star"></i>';	
														$str_star.='<i class="fa fa-star"></i>';
														$str_star.='<i class="fa fa-star"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
													}elseif($actual_val_rating==4.5){
														$str_star.='<i class="fa fa-star"></i>';	
														$str_star.='<i class="fa fa-star"></i>';	
														$str_star.='<i class="fa fa-star"></i>';
														$str_star.='<i class="fa fa-star"></i>';
														$str_star.='<i class="fa fa-star-half-o"></i>';
													}elseif($actual_val_rating==5){
														$str_star.='<i class="fa fa-star"></i>';	
														$str_star.='<i class="fa fa-star"></i>';	
														$str_star.='<i class="fa fa-star"></i>';
														$str_star.='<i class="fa fa-star"></i>';
														$str_star.='<i class="fa fa-star"></i>';
													}else{
														$str_star.='<i class="fa fa-star-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
														$str_star.='<i class="fa fa-star-o"></i>';
													}
													
													echo $str_star;
													?>
													
													&nbsp;<a href="#" onclick="show_all_reviews(<?php echo $actual_val_inventory_id; ?>)"><u>View all reviews (<?php echo $total_reviews; ?>)</u></a>
												</div>
												
											</td>
											<?php
												}
											else{
												?>
												<td width="25%">
													<a href="#">
														<?php
															echo $actual_val;
														?>
													</a>
												</td>
										<?php
											}
										?>
										<?php
									
								}
							?>
							<?php
										if(count($compare_params_arr)==2){
											if($label=="Product Name"){
												$label_text="Add a product";
											}
											else{
												$label_text="";
											}
										?>
											<td width="25%" <?php if($label=="Product Image"){echo "class='well'";} ?>>
												<?php echo $label_text;?>
											</td>
										<?php
										}
										?>
						</tr>
						<?php
					}
				?>
            
			
			<?php
				$main_arr=array();
				foreach($compare_params_arr as $inventory_id){
					$get_inventory_info_arr=$controller->get_inventory_info_by_inventory_id($inventory_id);
					$get_product_info_arr=$controller->get_product_info_by_inventory_id($inventory_id);
					$all_ids_arr=array('brand_id'=>$get_product_info_arr["brand_id"],'subcat_id'=>$get_product_info_arr["subcat_id"],'cat_id'=>$get_product_info_arr["cat_id"],'pcat_id'=>$get_product_info_arr["pcat_id"]);
		
					$allowed_specification_group_arr=$controller->get_specification_group_by_category($all_ids_arr);
					//print_r($allowed_specification_group_arr);
					$allowed_specification_group=$controller->get_specification_groups($allowed_specification_group_arr);
					//print_r($allowed_specification_group);
					foreach($allowed_specification_group as $allowed_specification_group_arr){
						$specification_values_arr=$controller->get_specification_values($allowed_specification_group_arr["specification_group_id"],$get_inventory_info_arr["id"]);
						
						//$main_arr[$allowed_specification_group_arr["specification_group_name"]]=
						//$specification_group_name_arr[$allowed_specification_group_arr["specification_group_name"]]=array()
						if(array_key_exists($allowed_specification_group_arr["specification_group_name"],$specification_group_name_arr) && count($specification_group_name_arr[$allowed_specification_group_arr["specification_group_name"]])>0){
						foreach($specification_group_name_arr[$allowed_specification_group_arr["specification_group_name"]] as $actual_specification_name){
							if(count($specification_values_arr)==0){
								$main_arr[$actual_specification_name][]="-";
							}
							else{
								$count=0;
								foreach($specification_values_arr as $specification_values){
									
									if($specification_values["specification_name"]==$actual_specification_name){
										$count++;
										$specification_val=$specification_values["specification_value"];
										break;
									}
										
								}
								if($count>0){
									$main_arr[$actual_specification_name][]=$specification_val;
								}
								else{
									$main_arr[$actual_specification_name][]="-";
								}
							}
							
						}
						}
						
					}
				}
				//print_r($main_arr);
				$parent_main_arr=array();
				foreach($specification_group_name_arr as $group_name => $arr){
					foreach($main_arr as $key => $val){
						if(in_array($key,$arr)){
							$parent_main_arr[$group_name][]=array($key=>$val);
						}
					}
				}
				//print_r($parent_main_arr);
				
			?>
		 
                <?php
					foreach($parent_main_arr as $group_name => $group_name_arr){
							if(count($compare_params_arr)==2){
								$colspan=count($compare_params_arr)+2;
							}
							else{
								$colspan=count($compare_params_arr)+1;
							}
										
						?>
						<tr>
							<th colspan="<?php echo $colspan;?>">
								<?php echo $group_name;?>
							</th>
						</tr>
						<?php
					foreach($group_name_arr as $k => $main_arr){
					foreach($main_arr as $label => $inventory_info_arr){
						?>
						<tr>
							<td class="compare-label" width="25%"><?php echo $label;?></td>
							<?php
								foreach($inventory_info_arr as $actual_val){
									
										
											
												?>
												<td  width="25%">
													<a href="#">
														<?php
															echo $actual_val;
														?>
													</a>
												</td>
										
										<?php
									
								}
							?>
							<?php
										if(count($compare_params_arr)==2){
										?>
											<td  width="25%">
												
											</td>
										<?php
										}
										?>
						</tr>
						<?php
					}
					}
					?>
					 
					<?php
					}
				?>
           </table>
		
		</div>
</div>
<!-- ./page wapper-->
<script>
function show_all_reviews(inventory_id){
  rand='<?php echo $controller->sample_code(); ?>';
  location.href="<?php echo base_url(); ?>show_all_reviews/"+rand+inventory_id;
}

</script>