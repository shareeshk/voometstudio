
<script type="text/javascript" src="<?php echo base_url();?>assets/js/multiselect.js"></script>

<script  type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js" ></script>
<link href="<?php echo base_url();?>assets/css/datepicker.css" rel="stylesheet" /> 

<style type="text/css">
    .margin_top{
        margin-top: 30px;
    }
    .verify_code_form{
        margin-top: 20px;
        background-color: #eee;
        padding: 10px;
    }
    .margin{
        margin-top: 10px;
        margin-bottom: 10px;
    }
</style>

<div class="columns-container">
    <div class="container" id="columns">
        <ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Home</a></li>
		  <li class="breadcrumb-item active">Request a Quote</li>
		</ol>
        <div class="row">
            <?php 
            
            //print_r($this->session->userdata());
            
            ?>
			<?php //require_once 'leftcolum.php';?>
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
				<div class="panel panel-default">
               
                <!----------sku details------------------>
                
          
                    
    
                   
                     
        <div class="panel-body">
        
        <div class="col-sm-offset-2 col-sm-10">
        
            <h3>Request for Quotation</h3>
            
        <form id="rfq_common_form" class="form-horizontal mt-20" enctype="multipart/form-data">
					
			
			<input type="hidden" name="rq_unique_id" id="rq_unique_id" value="<?php echo $rq_unique_id;?>">
			<input type="hidden" name="source_page" id="source_page" value="<?php echo $source_page;?>">
            <input type="hidden" name="rq_customer_id" id="rq_customer_id" value="<?php echo $rq_customer_id;?>">
			<input type="hidden" name="rq_pcat_id" id="rq_pcat_id" value="<?php echo $rq_pcat_id;?>">
			<input type="hidden" name="rq_cat_id" id="rq_cat_id" value="<?php echo $rq_cat_id;?>">
			<input type="hidden" name="rq_subcat_id" id="rq_subcat_id" value="<?php echo $rq_subcat_id;?>">
			<input type="hidden" name="rq_brand_id" id="rq_brand_id" value="<?php echo $rq_brand_id;?>">
			<input type="hidden" name="rq_product_id" id="rq_product_id" value="<?php echo $rq_product_id;?>">
			<input type="hidden" name="rq_sku_ids" id="rq_sku_ids" value="<?php echo $rq_sku_ids;?>">
			<input type="hidden" name="rq_inv_id" id="rq_inv_id" value="<?php echo $rq_inv_id;?>">
			<input type="hidden" name="rq_qty" id="rq_qty" value="<?php echo $rq_qty;?>">
			<input type="hidden" name="rq_units" id="rq_units" value="<?php echo $rq_units;?>">
			<input type="hidden" name="vendor_id_list" id="vendor_id_list" value="<?php echo $vendor_id_list;?>">
			<input type="hidden" name="rq_cat_name" id="rq_cat_name" value="<?php echo $rq_cat_name;?>">
			<input type="hidden" name="rq_subcat_name" id="rq_subcat_name" value="<?php echo $rq_subcat_name;?>">
			<input type="hidden" name="rq_brand_name" id="rq_brand_name" value="<?php echo $rq_brand_name;?>">
			<input type="hidden" name="rq_product_name" id="rq_product_name" value="<?php echo $rq_product_name;?>">
           
                        <span style="display:none;">
            <div class="form-group">
                <label for="name" class="control-label col-sm-2">Postal code</label>
                <div class="col-sm-6">
                    <input type="text" onkeypress="return isNumber(event)" class="form-control" name="rq_pincode" value="<?php echo $rq_pincode;?>" id="rq_pincode" placeholder="Enter your postal code" maxlength="6">
                </div>
            </div><!-- postalcode -->
            
            <div class="form-group">
                <label for="name" class="control-label col-sm-2">Quantity</label>
                <div class="col-sm-6">
                    <input type="text" onkeypress="return isNumber(event)" class="form-control" name="rq_qty" value="<?php echo $rq_qty; ?>" >
                </div>
            </div>
            <div class="form-group">
			
                <label for="name" class="control-label col-sm-2">Units</label>
                <div class="col-sm-6">
                    <select class="form-control" name="rq_units">
                        <option value="">-Choose-</option>
                        <option value="KG" <?php if(strtolower($rq_units)=="kg" || strtolower($rq_units)=="kg" ){echo "selected";}?>>KG</option>
                        <option value="Metric tonnes" <?php if(strtolower($rq_units)=="metric tonnes" || strtolower($rq_units)=="metric tonnes"){echo "selected";}?>>Metric tonnes</option>
                        <option value="Reams" <?php if(strtolower($rq_units)=="reams" || strtolower($rq_units)=="reams"){echo "selected";}?>>Reams</option>
                    </select>
                </div>
            </div>
           
                        </span>                  
                                    
                            
                                    <div class="card row">
									  <div class="container col-sm-11 col-sm-offset-1">
										<h4 class="mt-20 text-primary">
											<?php echo $rq_cat_name;?> > 
											<?php echo $rq_subcat_name;?> > 
											<?php echo $rq_brand_name;?> > 
											<?php echo $rq_product_name;?> 
                                                                                        
                                                                                        <br>
                                                                                        <?php
																						/*
																						?>
																						<small>
                                                                                            
                                                                                            You requested for <b><?php echo $rq_qty; ?> <?php echo $rq_units; ?></b> to be delivered at postal code <b><?php echo $rq_pincode; ?></b>. <br>Please proceed to fill your details and submit the request.
                                                                                        </small>
																						 <?php
																						*/
																						?>
																						<br>
																						<p>
																							Quantity Requested : <b><?php echo $rq_qty; ?> <?php echo $rq_units; ?></b>
																						
																							at Postal Code : <b><?php echo $rq_pincode; ?> </b>
																						</p>
										</h4>
									  </div>
									</div>
                                   
                                    
                                    <div class="form-group mt-20">
                                        <label for="name" class="control-label col-sm-2"> Name</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="rq_name" value="<?php echo $rq_name;?>" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="control-label col-sm-2"> Email</label>
                                        <div class="col-sm-6">
                                            <input type="email" class="form-control" name="rq_email" value="<?php echo $rq_email;?>" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="control-label col-sm-2"> Mobile</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="rq_mobile" value="<?php echo $rq_mobile;?>" onkeypress="return isNumber(event)" maxlength="10">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="control-label col-sm-2"> Location</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="rq_location" value="" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="control-label col-sm-2"> Delivery Address</label>
                                        <div class="col-sm-6">
                                            <textarea class="form-control" name="rq_delivery_address" rows="5"><?php echo $rq_delivery_address;?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="rq_payment_terms" class="control-label col-sm-2"> Payment terms</label>
                                        <div class="col-sm-6 form-check">
                                            <input type="checkbox" class="form-check-input" name="rq_payment_terms[]" value="cash" id="check1"> 
                                            <label class="form-check-label" for="check1">
                                                Cash</label>
                                            <input type="checkbox" class="form-check-input" name="rq_payment_terms[]" value="credit" id="check1"> 
                                            <label class="form-check-label" for="check2">
                                            Credit
                                            </label>
                                        </div>
                                    </div>

                                    <!--- new fields -->
                                    
                                    <div class="form-group">
                                        <label for="name" class="control-label col-sm-2">Other Details</label>
                                        <div class="col-sm-6">
                                            <textarea class="form-control" name="rq_details"  rows="5"></textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-3 col-sm-offset-2">
                                          <button type="submit" class="button btn-block preventDflt" id="rfq_common_form_btn"> Submit </button>
                                        </div>
                                        <div class="col-sm-3">
                                          <button type="reset" class="button btn-block preventDflt"> Cancel </button>
                                        </div>
                                    
                                    </div>
									
									
									
									<div class="row" id="rfq_common_form_btn_progress_bar_div" style="display:none;">
			  <div class="col-md-12">
		<div class="form-group">
                        <div class="progress" style="height:10px !important;">
                            <div class="progress-bar progress-bar-success progress-bar-success progress-bar-striped myprogress_webinar" role="progressbar" style="width:0%;"></div>
                        </div>

                        
                    </div>
				</div>
			</div>
			
			
                                    
                                    
                                </form>
					</div>	
				</div>
    
    
                
                <!----------sku details------------------>
                
                
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

<script>
   
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && ((charCode < 48 || charCode > 57))) {
            return false;
        }
        return true;
    }
    
       
	   
$(document).ready(function(){	   
	$("#rfq_common_form").on('submit',(function(e) {
		e.preventDefault();
		
		rq_pincode=document.getElementById("rq_pincode").value;
		if(rq_pincode.length!=6){
			swal({
				title:"Info", 
				html: "Please enter 6 digit postal code", 
				type: "info",
				allowOutsideClick: false
			}).then(function () {
				document.getElementById("rq_subcat_id").value="";
				return false;
			});
			return false;
		}
		
		rq_unique_id=$("#rq_unique_id").val();
		
		$.ajax({
			url:"<?php echo base_url();?>Account/request_a_quote_common_action",
			type:'POST',
			data:new FormData(this),
			contentType: false,
			cache: false,	
			processData:false,
			beforeSend:function(){
					$("#rfq_common_form_btn").html("Processing <i class='fa fa-refresh fa-spin'></i>");
					$("#rfq_common_form_btn_progress_bar_div").css({"display":"block"});
				},
				xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    setInterval_webinar=setInterval(function(){ 
                                            $.ajax({
                                                    url:"<?php echo base_url();?>Account/request_a_quote_common_action_mail_status/"+rq_unique_id,
                                                    type: "POST",      				// Type of request to be send, called as method
                                                    data:  "1=2",
                                                    dataType:"JSON",
                                                    success:function(res_total){
                                                            vendor_email_list_sent=res_total.vendor_email_list_sent;
                                                            percentComplete=(vendor_email_list_sent/1);
                                                            percentComplete = parseInt(percentComplete * 100);
                                                            //$('.myprogress_webinar').text(percentComplete + '%');
                                                            $('.myprogress_webinar').css('width', percentComplete + '%');
                                                    }
                                            });
                                    }, 1000);
                                }
                            }, false);
                            return xhr;
                        },
			success:function(data){
                                //console.log(data);
                                //return false;
				$("#rfq_common_form_btn").html("Submit");
				if(data=="mail_sent" || data=="mail_not_sent" || data=="no_vendors"){
					swal({
						title:"Success", 
						text:"Your request is captured", 
						type: "success",
						allowOutsideClick: false
					}).then(function () {
						source_page_url="<?php echo $source_page_url; ?>";
						//alert(source_page_url)
						location.href="<?php echo base_url();?>"+source_page_url;
					});
				}
				else{
					swal("Error", "Error", "error");
				}
			}
		});
	}));
});




</script>