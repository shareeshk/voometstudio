<?php
include_once('fpdfs.php');
class FPDF_INVOICE extends FPDF
{
// Page footer
function Footer()
{
    
	// To be implemented in your own inherited class
	// custom code starts
	// Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,5,'This is a computer generated invoice. Does not need manual authentication or signature.',0,0,'C');
	$this->Ln();
	$this->Cell(0,5,'All terms and conditions applied as per company policy.',0,0,'C');
	/// customer code ends

}
}

?>