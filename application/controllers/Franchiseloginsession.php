<?php
	class Franchiseloginsession extends CI_controller{
		function __construct(){
			parent::__construct();
			$this->load->model('Franchise_frontend');			
			$this->load->library('My_PHPMailer');	
			$this->load->helper('cookie');		
			$this->load->helper('emailsmstemplate');
			$this->load->helper('general');			
		}
	
		public function index(){
			redirect("Franchiseloginsession");
			
		}
		public function email_exist($input){
				$this->load->helper("email");
				$type = "";
				if(valid_email($input)):
					$type = "email";
					endif;
					switch($type){
						case "email":
							$sql_customer ="SELECT * FROM `franchise_customer` WHERE `email` = '$input'";
							break;
					}

				$exec_customer = $this->db->query($sql_customer);
				if(($exec_customer->num_rows()==0)){
					$this->form_validation->set_message("email_exist" , "The $type you entered does not exist");	
					return FALSE;
				}
		}
		
		function checkemailverification(){
			$email=$this->input->post('email');
			$data=$this->Franchise_frontend->checkemailverification($email);
			echo $data[0]->active;
			}
		
		function check_email()
		{	
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data->email))
				{
					$email=$data->email;
					$data=$this->Franchise_frontend->verify_email($email);
				}
				else{
					return null;
				}
			}
		}
		
		function check_mobile()
		{
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data->mobile))
				{
					$mobile=$data->mobile;
					$data=$this->Franchise_frontend->verify_mobile($mobile);
				}
				else{
					return null;
				}
			}
		}
		function check_otpcode()
		{
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data->c_id))
				{
					$c_id=$data->c_id;
					$rn=$data->otp_code;
					$data=$this->Franchise_frontend->get_verif_code_db($c_id,$rn);
				}
				else{
					return null;
				}
			}
		}
		
		function add_customer(){
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data))
				{
					//[password_c] => 1
					$name=$data->name;
					$email=$data->email;
					$temppassword=$data->temppassword;
					
					$pwtemp="";
					/*for($i=0;$i<strlen($temppassword);$i++){
						$pwtemp.=chr(ord($temppassword[$i])+6);
					}*/				

					// no otp send and password update for franchise

					$mobile=$data->mobile;
					$rand=mt_rand(1000,10000);
					
					if($mobile != '' ){
						
						$mobile_stuff_arr=mobile_stuff();
						$authKey = $mobile_stuff_arr["authKey_smstemplate"];
						$mobileNumber = "+91".$mobile;
						$senderId = $mobile_stuff_arr["senderId_smstemplate"];
						$DLT_TE_ID = $mobile_stuff_arr["DLT_TE_ID"];
						

						$message = urlencode($rand." is your one time password(OTP) to process your login request.\nPacks Bazaar");
						$route = "4";
						$postData = array(
							'authkey' => $authKey,
							'mobiles' => $mobileNumber,
							'message' => $message,
							'sender' => $senderId,
							'route' => $route,
							'DLT_TE_ID' => $DLT_TE_ID
						);

						$url="https://control.msg91.com/api/sendhttp.php";
						$ch = curl_init();
						curl_setopt_array($ch, array(
							CURLOPT_URL => $url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_POST => true,
							CURLOPT_POSTFIELDS => $postData
						));
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

						//$output = curl_exec($ch);

						if(curl_errno($ch))
						{
							echo 'error:' . curl_error($ch);
						}else{
							//no error - code has been sent

							$pincode=$data->pin;
							$city=$data->city;
							$state=$data->state;
							$country=$data->country;

							$data=$this->Franchise_frontend->add_customer($name,$email,$mobile,$pwtemp,$rand,$pincode,$city,$state,$country);	
						}

						curl_close($ch);
					}//sms
					
					
				}
			}
		}
		function update_customer()
		{
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data))
				{
					//[password_c] => 1
					$c_id=$data->c_id;
					$name=$data->name;
					$email=$data->email;
					$password=$data->password;
					$pwtemp="";
					for($i=0;$i<strlen($password);$i++){
						$pwtemp.=chr(ord($password[$i])+6);
					}
					$mobile=$data->mobile;
					$rand=mt_rand(1000,10000);
					
					if($mobile != '' ){
						
						
						$mobile_stuff_arr=mobile_stuff();
						$authKey = $mobile_stuff_arr["authKey_smstemplate"];
						$mobileNumber = "+91".$mobile;
						$senderId = $mobile_stuff_arr["senderId_smstemplate"];
						$DLT_TE_ID = $mobile_stuff_arr["DLT_TE_ID"];
						

						$message = urlencode($rand." is your one time password(OTP) to process your login request.\nPacks Bazaar");
						$route = "4";
						$postData = array(
							'authkey' => $authKey,
							'mobiles' => $mobileNumber,
							'message' => $message,
							'sender' => $senderId,
							'route' => $route,
							'DLT_TE_ID' => $DLT_TE_ID
						);

						$url="https://control.msg91.com/api/sendhttp.php";
						$ch = curl_init();
						curl_setopt_array($ch, array(
							CURLOPT_URL => $url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_POST => true,
							CURLOPT_POSTFIELDS => $postData
						));
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

						$output = curl_exec($ch);

						if(curl_errno($ch))
						{
							echo 'error:' . curl_error($ch);
						}else{
							//no error - code has been sent
							$pincode=$data->pin;
							$city=$data->city;
							$state=$data->state;
							$country=$data->country;

							$data=$this->Franchise_frontend->update_new_customer($c_id,$name,$email,$mobile,$pwtemp,$rand,$pincode,$city,$state,$country);	
						}

						curl_close($ch);
					}//sms
					
				}
			}
		}
		function resend_code_to_newuser()
		{
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data))
				{
					$c_id=$data->c_id;
					$mobile=$data->val;
					$rand=rand(1000,10000);
					if($mobile != '' ){
						
						$mobile_stuff_arr=mobile_stuff();
						$authKey = $mobile_stuff_arr["authKey_smstemplate"];
						$mobileNumber = "+91".$mobile;
						$senderId = $mobile_stuff_arr["senderId_smstemplate"];
						$DLT_TE_ID = $mobile_stuff_arr["DLT_TE_ID"];
						

						$message = urlencode($rand." is your one time password(OTP) to process your login request.\nPacks Bazaar");
						$route = "4";
						$postData = array(
							'authkey' => $authKey,
							'mobiles' => $mobileNumber,
							'message' => $message,
							'sender' => $senderId,
							'route' => $route,
							'DLT_TE_ID' => $DLT_TE_ID
						);

						$url="https://control.msg91.com/api/sendhttp.php";
						$ch = curl_init();
						curl_setopt_array($ch, array(
							CURLOPT_URL => $url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_POST => true,
							CURLOPT_POSTFIELDS => $postData
						));
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

						$output = curl_exec($ch);

						if(curl_errno($ch))
						{
							echo 'error:' . curl_error($ch);
						}else{
							//no error - code has been sent
							$data=$this->Franchise_frontend->resend_code_to_newuser($c_id,$mobile,$rand);
						}

						curl_close($ch);
					}//sms
					
				}
			}
		}
		function activate_new_user()
		{
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data))
				{
					$c_id=$data->c_id;
					
					//$wallet_created=$this->Franchise_frontend->create_wallet_account($c_id);

					if(1){
						
						$result=$this->Franchise_frontend->activate_new_user($c_id);
						
						if($result){	
						
							$customer_info_arr=$this->Franchise_frontend->get_customer_info_by_cid($c_id);
							$this->session->set_userdata("customer_id",$c_id);
							$this->session->set_userdata("franchise_customer_id",$c_id);
							$this->session->set_userdata("logged_in",true);
							$this->session->set_userdata("customer_name",$customer_info_arr->name);
							$this->session->set_userdata("customer_email",$customer_info_arr->email);
							$this->session->set_userdata("customer_mobile",$customer_info_arr->mobile);
							$this->session->set_userdata("user_type","franchise_customer");
							
							$valid=true;
							echo json_encode(array(
							'data' => $valid,
							'mobileno_in_session'=>$customer_info_arr->mobile
							));
						}else{
							$valid=false;
							echo json_encode(array(
							'data' => $valid,
							));
						}
					
					}else{
						$valid=false;
						echo json_encode(array(
						'data' => $valid,
						));
					}
				
				}
			}
		}
		function customer_login()
		{
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data->password))
				{
					$un=$data->username;
					$pw=$data->password;
					$type=$data->type;
					$pwtemp="";
					for($i=0;$i<strlen($pw);$i++){
						$pwtemp.=chr(ord($pw[$i])+7);
					}
					//echo $pwtemp;exit;
					$data=$this->Franchise_frontend->customer_login($un,$pwtemp,$type);
				}
				else{
					return null;
				}
			}
		}
		function customer_check_user()
		{	
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data->val))
				{
					$val=$data->val;
					$data=$this->Franchise_frontend->customer_verify_user($val);
				}
				else{
					return null;
				}
			}
		}
		function resend_code_to_setpassword()
		{
			$email_stuff_arr=email_stuff();
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data))
				{
					$val=$data->val;
					$rand=rand(100000,999999);
					
					if(is_numeric($val))
					{
						$mobile=$val;
						if($mobile != '' ){
						
						
						$mobile_stuff_arr=mobile_stuff();
						$authKey = $mobile_stuff_arr["authKey_smstemplate"];
						$mobileNumber = "+91".$mobile;
						$senderId = $mobile_stuff_arr["senderId_smstemplate"];
						$DLT_TE_ID = $mobile_stuff_arr["DLT_TE_ID"];
						
						$message = urlencode($rand." is your one time password(OTP) to process your login request.\nPacks Bazaar");
						$route = "4";
						$postData = array(
							'authkey' => $authKey,
							'mobiles' => $mobileNumber,
							'message' => $message,
							'sender' => $senderId,
							'route' => $route,
							'DLT_TE_ID' => $DLT_TE_ID
						);

						$url="https://control.msg91.com/api/sendhttp.php";
						$ch = curl_init();
						curl_setopt_array($ch, array(
							CURLOPT_URL => $url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_POST => true,
							CURLOPT_POSTFIELDS => $postData
						));
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

						$output = curl_exec($ch);

						if(curl_errno($ch))
						{
							echo 'error:' . curl_error($ch);
						}else{
							//no error - code has been sent
							$data=$this->Franchise_frontend->resend_code_to_setpassword($val,$rand);
						}

						curl_close($ch);
						}//sms
					
						
					}else{
							
						//through Email
						if($val!=''){
							$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr>
							
							<tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi, </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">Your verification code to reset your password <b>'.$rand.'</b></p><br><p style="padding:0;margin:0;color:#565656;font-size: 15px;">Warm Regards, <br><b>Team Axonlabs</b></p><br></td></tr>';
							
							$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

							$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

							$usermessage.='</tbody> </table></tr></table></center> </body></html>';
						
							
							$mail = new PHPMailer;
							/*$mail->isSMTP();
							$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
							$mail->Port = '587';//  live - comment it 
							$mail->Auth = true;//  live - comment it 
							$mail->SMTPAuth = true;
							$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
							$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
							$mail->SMTPSecure = 'tls';*/
							$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
							$mail->FromName = $email_stuff_arr["name_emailtemplate"];
							$mail->addAddress($val);
							//$mail->AddCC("rajitkumar1974@gmail.com", "Rajit Kumar");               
							//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
							$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
							$mail->WordWrap = 50;
							$mail->isHTML(true);
							$mail->Subject = 'Reset Password';
							$mail->Body    = $usermessage;
							if($val != ''){		
									$mail->send();
									$data=$this->Franchise_frontend->resend_code_to_setpassword($val,$rand);
							}
						}
						
					}//mail

				}
			}
		}
		function resend_code_to_newpassword()
		{
			$email_stuff_arr=email_stuff();
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data))
				{
					$c_id=$data->c_id;
					$val=$data->val;
					$rand=rand(100000,999999);
					if(is_numeric($val))
					{
						$mobile=$val;
						if($mobile != '' ){
						
						$mobile_stuff_arr=mobile_stuff();
						$authKey = $mobile_stuff_arr["authKey_smstemplate"];
						$mobileNumber = "+91".$mobile;
						$senderId = $mobile_stuff_arr["senderId_smstemplate"];
						$DLT_TE_ID = $mobile_stuff_arr["DLT_TE_ID"];
						

						$message = urlencode($rand." is your one time password(OTP) to process your login request.\nPacks Bazaar");
						$route = "4";
						$postData = array(
							'authkey' => $authKey,
							'mobiles' => $mobileNumber,
							'message' => $message,
							'sender' => $senderId,
							'route' => $route,
							'DLT_TE_ID' => $DLT_TE_ID
						);

						$url="https://control.msg91.com/api/sendhttp.php";
						$ch = curl_init();
						curl_setopt_array($ch, array(
							CURLOPT_URL => $url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_POST => true,
							CURLOPT_POSTFIELDS => $postData
						));
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

						$output = curl_exec($ch);

						if(curl_errno($ch))
						{
							echo 'error:' . curl_error($ch);
						}else{
							//no error - code has been sent
							$data=$this->Franchise_frontend->resend_code_to_newpassword($c_id,$val,$rand);
						}

						curl_close($ch);
						}//sms
					
						
					}else{
						
						//through Email
						if($val!=''){
							$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr>
							
							<tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi, </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">Your verification code to reset your password <b>'.$rand.'</b></p><br><p style="padding:0;margin:0;color:#565656;font-size: 15px;">Warm Regards, <br><b>Team Axonlabs</b></p><br></td></tr>';
							
							$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';
							
							$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

							$usermessage.='</tbody> </table></tr></table></center> </body></html>';
						
							
							$mail = new PHPMailer;
							/*$mail->isSMTP();
							$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
							$mail->Port = '587';//  live - comment it 
							$mail->Auth = true;//  live - comment it 
							$mail->SMTPAuth = true;
							$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
							$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
							$mail->SMTPSecure = 'tls';*/
							$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
							$mail->FromName = $email_stuff_arr["name_emailtemplate"];
							$mail->addAddress($val);
							//$mail->AddCC("rajitkumar1974@gmail.com", "Rajit Kumar");               
							//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
							$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
							$mail->WordWrap = 50;
							$mail->isHTML(true);
							$mail->Subject = 'Reset Password';
							$mail->Body    = $usermessage;
							if($val != ''){		
									$mail->send();
									$data=$this->Franchise_frontend->resend_code_to_newpassword($c_id,$val,$rand);
							}
						}
						
					}//mail
					
					
				}
			}
		}
		function reset_password()
		{
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data))
				{
					$c_id=$data->c_id;
					$pw=$data->password;
					$pwtemp="";
					for($i=0;$i<strlen($pw);$i++){
						$pwtemp.=chr(ord($pw[$i])+7);
					}
					
					$data=$this->Franchise_frontend->reset_password($c_id,$pwtemp);
				}
			}
		}
		
		public function logout(){
		if($this->session->userdata("logged_in")){
			
			$update_logout=$this->Franchise_frontend->update_visitor_logout();
			
			$this->session->sess_destroy();
			//redirect($_SERVER["HTTP_REFERER"]);		
			redirect(base_url());
		}
		else{
			redirect(base_url());
		}
	}
        public function add_vendor(){
                if(isset($_REQUEST))
                {
                        $data = json_decode(file_get_contents("php://input"));
                        $comp_data=$data;
                        if(isset($data))
                        {
                                //[password_c] => 1
                                $name=$data->name;
                                $email=$data->email;
                                $temppassword=$data->temppassword;
                                
                                $address1=(isset($data->address1)) ? $data->address1 : '';
                                $address2=(isset($data->address2)) ? $data->address2 : '';
                                $pincode=(isset($data->pincode)) ? $data->pincode : '';

                                $pwtemp="";
                                for($i=0;$i<strlen($temppassword);$i++){
                                    $pwtemp.=chr(ord($temppassword[$i])+6);
                                }				

                                $mobile=$data->mobile;
                                //$rand=mt_rand(1000,10000);
                                $flag=$this->Franchise_frontend->add_vendor($name,$email,$mobile,$pwtemp,$data);
                                //echo $flag;
                                
                                if($flag!=''){
                                $email_stuff_arr=email_stuff();
                                $str='Thanks for registering as a vendor. We have received your request. We will get back to you soon. Please visit to your panel '.base_url()."Administrator.";
                            if($email!=''){

								$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr>
								
								<tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi, </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">'.$str.'</p><br><p style="padding:0;margin:0;color:#565656;font-size: 15px;">Warm Regards, <br><b>'.$email_stuff_arr["regardsteam_emailtemplate"].'</b></p><br></td></tr>';
									
									$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

									$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

									$usermessage.='</tbody> </table></tr></table></center> </body></html>';


                                    $mail = new PHPMailer;
                                    /*$mail->isSMTP();
                                    $mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
                                    $mail->Port = '587';//  live - comment it 
                                    $mail->Auth = true;//  live - comment it 
                                    $mail->SMTPAuth = true;
                                    $mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
                                    $mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
                                    $mail->SMTPSecure = 'tls';*/
                                    $mail->From = $email_stuff_arr["fromemail_emailtemplate"];
                                    $mail->FromName = $email_stuff_arr["name_emailtemplate"];
                                    $mail->addAddress($email);
                                    $mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
                                    $mail->WordWrap = 50;
                                    $mail->isHTML(true);
                                    $mail->Subject = 'Vendor Registration';
                                    $mail->Body    = $usermessage;
                                    if($email != ''){
                                        @$mail->send();
                                        //$flag=$this->Customer_account->sended_code_to_mail($email,$rand);
                                        //echo $flag;
                                    }
                            }
                                }
                                echo $flag;
                                
                                /*sending mail to vendor */

                        }
                }
        }
  
	public function provide_category($pcat_id)
	{
		return $this->Franchise_frontend->provide_category($pcat_id);
		
	}
	/* Franchise bank details */
	
	
	public function update_bank_details($bank_id,$order_item_id=0){
		if($this->session->userdata("logged_in")){
			$method=current_url();
			$this->session->set_userdata("cur_page",$method);
			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="account";
			$data["cur_page"]="update_bank_details";
			if($order_item_id!=0){
				$bank_account_number=$this->Customer_account->get_bank_account_number_bank_id($bank_id,'delivered');
			}
			else{
				$bank_account_number=$this->Customer_account->get_bank_account_number_bank_id($bank_id,'refund');
			}
			$bank_detail=array();
			if(!empty($bank_account_number)){
				if($order_item_id!=0){
					$bank_detail=$this->get_bank_detail_delivered($bank_account_number);
				}
				else{
					$bank_detail=$this->get_bank_detail($bank_account_number);
				}
			}
			$data['bank_detail']=$bank_detail;
			$data['order_item_id']=$order_item_id;
			$data['wallet_bank_transfer']="no";
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('customer/update_bank_details');
			$this->load->view('footer');
		}
		else{
			redirect(base_url());
		}	
	}
	
	
	public function get_bank_detail($account_number){
		$customer_id=$this->session->userdata("customer_id");
		return $this->Customer_account->get_bank_detail($account_number,$customer_id);
	}
	public function get_bank_detail_delivered($account_number){
		$customer_id=$this->session->userdata("customer_id");
		return $this->Customer_account->get_bank_detail_delivered($account_number,$customer_id);
	}
	public function update_bank_details_action(){
		if($this->session->userdata("logged_in")){
			
			$wallet_bank_transfer=$this->input->post("wallet_bank_transfer");
			$bank_id=$this->input->post("bank_id");
			$bank_name_rep=$this->input->post("bank_name_rep");
			$bank_branch_name_rep=$this->input->post("bank_branch_name_rep");
			$bank_city_rep=$this->input->post("bank_city_rep");
			$bank_ifsc_code_rep=$this->input->post("bank_ifsc_code_rep");
			$bank_account_number_rep=$this->input->post("bank_account_number_rep");
			$bank_account_confirm_number_rep=$this->input->post("bank_account_confirm_number_rep");
			$account_holder_name_rep=$this->input->post("account_holder_name_rep");
			$bank_return_refund_phone_number_rep=$this->input->post("bank_return_refund_phone_number_rep");
			$order_item_id=$this->input->post("order_item_id");
			
			$customer_id=$this->session->userdata("customer_id");
			if($wallet_bank_transfer=="no"){
				if($order_item_id!=0){
					$this->Customer_account->update_returns_desired_with_bank_details_update($order_item_id);
					$flag=$this->Customer_account->update_delivered_bank_details_action($bank_id,$bank_name_rep,$bank_branch_name_rep,$bank_city_rep,$bank_ifsc_code_rep,$bank_account_number_rep,$bank_account_confirm_number_rep,$account_holder_name_rep,$bank_return_refund_phone_number_rep);
					echo $flag;
				}
				else{
					$flag=$this->Customer_account->update_bank_details_action($bank_id,$bank_name_rep,$bank_branch_name_rep,$bank_city_rep,$bank_ifsc_code_rep,$bank_account_number_rep,$bank_account_confirm_number_rep,$account_holder_name_rep,$bank_return_refund_phone_number_rep);
					echo $flag;
				}
			}
			if($wallet_bank_transfer=="yes"){
				
					$flag=$this->Customer_account->update_wallet_bank_transfer_details_action($bank_id,$bank_name_rep,$bank_branch_name_rep,$bank_city_rep,$bank_ifsc_code_rep,$bank_account_number_rep,$bank_account_confirm_number_rep,$account_holder_name_rep,$bank_return_refund_phone_number_rep);
					echo $flag;
			}
		}
		else{
			redirect(base_url());
		}	
	}
	
	/* Franchise bank details */
	
	}
?>