<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Su_admin extends CI_Controller {
	function __construct(){
		parent::__construct();
		$data["controller"]=$this;
		$this->load->model('su_admin/Su_model');
		$this->load->helper('emailsmstemplate');
	}
	public function index(){
	    
		if($this->session->userdata("user_type")=="Su Master Country"){
			$data["controller"]=$this;
			$data["module_type"]=$this->session->userdata("module_type");
		    $this->load->view('suadmin/header',$data);
			$this->load->view('suadmin/reports');
		}
		else{
			$this->load->view('suadmin/header');
			$this->load->view('suadmin/login');
		}
	}
    public function get_all_num_data_of_all_promotion(){
        if($this->session->userdata("user_type")=="Su Master Country"){
            $this->load->model('admin/promotions_manage_data');
            $all_num_promo=array();
            $pending_promotion_num=$this->promotions_manage_data->pending_promotion_num();
            $all_num_promo['pending_promotion_num']=$pending_promotion_num;
            echo json_encode($all_num_promo);
        }
        else{
            $this->load->view('suadmin/header');
            $this->load->view('suadmin/login');
        }
    }
    public function get_quote_selector_value($quote_selector){
        $this->load->model('admin/promotions_manage_data');
        return $this->promotions_manage_data->get_quote_selector_value($quote_selector);
    }
    public function view_this_promotion($promo_uid)
    {
        if($this->session->userdata("user_type")=="Su Master Country"){
            $this->load->model('admin/promotions_manage_data');
            $data['promo_data']=$this->promotions_manage_data->get_promo_master_data($promo_uid);
            $data['controller']=$this;

			$data["module_type"]=$this->session->userdata("module_type");
			
            $this->load->view('suadmin/header',$data);
            $this->load->view('suadmin/view_this_promotions.php',$data);
        }
        else{
            $this->load->view('suadmin/header');
            $this->load->view('suadmin/login');
        }
    }
    
    public function all_pending_approval(){
        if($this->session->userdata("user_type")=="Su Master Country"){
			
			$data["controller"]=$this;
			$data["module_type"]=$this->session->userdata("module_type");
			
           $this->load->view('suadmin/header',$data);
            $this->load->view('suadmin/all_pending_approval');
        }
        else{
            $this->load->view('suadmin/header');
            $this->load->view('suadmin/login');
        }
    }
    public function pending_promotions_processing(){
      if($this->session->userdata("user_type")=="Su Master Country"){
            $this->load->model('admin/promotions_manage_data');
            $params = $columns = $totalRecords = $data = array();

            $params = $this->input->post();
            
            $totalRecords=$this->promotions_manage_data->pending_promotions_processing($params,"get_total_num_recs");
            
            $queryRecordsResult=$this->promotions_manage_data->pending_promotions_processing($params,"get_recs");
            
            $i=count($queryRecordsResult);
            $i=0;
            foreach($queryRecordsResult as $queryRecordsObj){
                if($queryRecordsObj->promo_approval==0 && $queryRecordsObj->hold_flag==0){
                    $promo_approval="Pending Approval";
                }
                if($queryRecordsObj->promo_approval==1){
                    $promo_approval="Approved";
                }
                if($queryRecordsObj->promo_approval==0 && $queryRecordsObj->hold_flag==1){
                    $promo_approval="Put On Hold";
                }
                
                if($queryRecordsObj->promo_start_date!=""){
                    $start_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_start_date));
                }
                else{
                    $start_date="";
                }
                
                if($queryRecordsObj->promo_end_date!=""){
                    $end_date=date("D,j M Y H:i", strtotime($queryRecordsObj->promo_end_date));
                }
                else{
                    $end_date="";
                }
                
                $row=array();
                $row[]=++$i;
                $row[]='<a href="'.base_url().'suadmin/Su_admin/view_this_promotion/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->id.'</a>';
                $row[]=$promo_approval;
                $row[]='<button class="btn btn-info btn-xs" promo_id='.$queryRecordsObj->promo_uid.' onclick="getAllLogOfThisPromotion(this)"> View Log</button>';
                $row[]=$this->get_promo_type_name($queryRecordsObj->promo_type);
                $row[]='<a href="'.base_url().'suadmin/Su_admin/view_this_promotion/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->promo_name.'</a>';
                $row[]='<a href="'.base_url().'suadmin/Su_admin/view_this_promotion/'.$queryRecordsObj->promo_uid.'">'.$queryRecordsObj->promo_quote.'</a>';
                $row[]=$end_date;
                $row[]=$start_date;
                
                $data[]=$row;
                $row=array();
            }
            $json_data = array(
                    "draw"            => intval( $params['draw'] ),   
                    "recordsTotal"    => intval( $totalRecords ),  
                    "recordsFiltered" => intval($totalRecords),
                    "data"            => $data   // total data array
                    );
            echo json_encode($json_data);  // send data as json format
            
                }
            else{
                $this->load->view('suadmin/header');
                $this->load->view('suadmin/login');
            }
    }
    

    public function get_promo_type_name($id){
        $this->load->model('admin/promotions_manage_data');
        return $this->promotions_manage_data->get_promo_type_name($id);
    }
    public function get_all_log_data_of_promotion(){
        if($this->session->userdata("user_type")=="Su Master Country"){
            $this->load->model('admin/promotions_manage_data');
            $promo_id=$this->input->post('promo_id');
            $getAllData=$this->promotions_manage_data->get_all_log_data_of_promotion($promo_id);
            $str='<div class="table-responsive"><table class="table table-hover table-condensed"><thead>
            <tr>
                <th width="5%">S No.</th>
                <th width="15%">Log</th>
               <th width="10%">Date Time</th>
                <th width="10%">By</th>
               
                <th width="30%">Promotion Summary</th>
                <th width="30%">Promotion Status</th>
              </tr>
            </thead>
            <tbody>';
            if(!empty($getAllData)){
                $i=1;
                foreach($getAllData as $data){
                    $str.='<tr><td>'.$i.'</td><td>'.$data['changes'].'</td><td>'.date("D,j M Y H:i", strtotime($data['timestamp'])).'</td><td>'.$data['user_id'].'</td><td><button class="accordion"></button><div class="panel">'.$data['commit'].'</div></td><td>'.$data['promo_status'].'</td></tr>';
                    $i++;
                }
            }
            else{
                $str.='<tr><td colspan="6">No Data to Show</td></tr>';
            }
            $str.='</tbody></table></div>';
            echo $str;
        }
        else{
            $this->load->view('suadmin/header');
            $this->load->view('suadmin/login');
        }
    }
    public function update_promotion_request($temp_email_num="",$promo_id="",$action=""){

           $success=0;
            if($this->input->post('temp_email_num')){
                $temp_email_num=$this->input->post('temp_email_num');
            }
           
           if($this->Su_model->check_temp_email_num($temp_email_num)){
               if($this->input->post('actions')){
                   $action=$this->input->post('actions');
                   $promo_id=$this->input->post('promo_id');
               }
                   
                   $action_num=0;
                   $action_reject=0;
                   $changes="";
                   $promo_status="";
                   $approval_sent_state=0;
                   $hold_flag=0;
        
                   if($action=='accept'){
                       $action_num=1;
                       $approval_sent_state=1;
                   }
                   if($action=='hold'){
                       $approval_sent_state=1;
                       $hold_flag=1;
                   }
                   if($action=='reject'){
                       $action_reject=1;
                   }
                   
                   $this->load->model('admin/promotions_manage_data');
                   $getpromodata=$this->promotions_manage_data->getcurrentpromodata($promo_id);

                     if(!empty($getpromodata)){
                    foreach ($getpromodata as $data){
        
        
                        if($data['promo_start_date']!=""){
                            $start_date=date("D,j M Y H:i", strtotime($data['promo_start_date']));
                        }
                        else{
                            $start_date="";
                        }
                        if($data['promo_end_date']!=""){
                            $end_date=date("D,j M Y H:i", strtotime($data['promo_end_date']));
                        }
                        else{
                            $end_date="";
                        }
                        $name=$data['su_email'];
                        $parts = explode("@", $name);
                        $username = $parts[0];
                        
                        if($action=='accept'){
                            $promo_status='<span class="change_to_log"><b>Approved</b></span>';
                           $changes='<span class="change_to_log"><span class="change_head">Approved By </span><span class="change_data">&#145;'.$name.'&#146;</span>on '.date("D,j M Y H:i").'</span>';
                       }
                       if($action=='hold'){
                            $promo_status='<span class="change_to_log"><b>Put on hold</b></span>';
                           $changes='<span class="change_to_log"><span class="change_head">On Hold </span><span class="change_data">&#145;'.$name.'&#146;</span>on '.date("D,j M Y H:i").'</span>';

                       }
                       if($action=='reject'){
                            $promo_status='<span class="change_to_log"><b>Rejected</b></span>';
                           $changes='<span class="change_to_log"><span class="change_head">Rejected by </span><span class="change_data">&#145;'.$name.'&#146;</span>on '.date("D,j M Y H:i").'</span>';

                       }
        
        
                        
                        $deal_type=$this->get_promo_type_name($data['promo_type']);
                        $final_offer_value='Deal Type: '.$deal_type.'<br>'.'Deal Name: '.$data['promo_name'].'<br>'.
                        'Deal Quote: '.$data['promo_quote'].'<br>'.'<span class="log_dates">Start Date: '.$start_date.'<br>'.
                        'End Date: '.$end_date.'</span>';
                        
                        
                    }
                }
                $update_log=$this->promotions_manage_data->update_promotion_log($promo_id,$changes,$final_offer_value,$username,$promo_status);
                $success=$this->Su_model->update_promotion_request($action_num,$action_reject,$approval_sent_state,$promo_id,$hold_flag);   
           
           echo $success;
               
           }
else{
    echo $success;
}
           
           

    }
    public function email_promotion_update($temp_email_num="",$promo_id="",$action=""){
        $data['temp_email_num']=$temp_email_num;
        $data['promo_id']=$promo_id;
        $data['action']=$action;
        $this->load->view('suadmin/email_update_promotion',$data);
    }
    public function ex(){
        if($this->session->userdata("user_type")=="Su Master Country"){
           
        }
        else{
            $this->load->view('suadmin/header');
            $this->load->view('suadmin/login');
        }
    }
	
	
	
	
	public function all_inventories(){
        if($this->session->userdata("user_type")=="Su Master Country"){
			
			$data["controller"]=$this;
			$data["module_type"]=$this->session->userdata("module_type");
			
		
           $this->load->view('suadmin/header',$data);
            $this->load->view('suadmin/all_inventories');
        }
        else{
            $this->load->view('suadmin/header');
            $this->load->view('suadmin/login');
        }
    }
	
	
	
	public function all_inventory_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Su_model->all_inventory_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Su_model->all_inventory_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				//$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->id.'">';
				$var='';
				$row[]='<img src="'.base_url().$queryRecordsObj -> image.'" width="100" alt="Image Not Found"><br>';
				$val='';
				$val.='Sku Id : '.$queryRecordsObj -> sku_id;
				
				$val.='<h4>Attributes</h4>';
				$val.=$queryRecordsObj -> attribute_1.":".$queryRecordsObj -> attribute_1_value.'<br>';

				if(!empty($queryRecordsObj -> attribute_2)){
					$val.=$queryRecordsObj -> attribute_2.":".$queryRecordsObj -> attribute_2_value.'<br>';
				}
				if(!empty($queryRecordsObj -> attribute_3)){
					$val.=$queryRecordsObj -> attribute_3.":".$queryRecordsObj -> attribute_3_value.'<br>';
				}
				if(!empty($queryRecordsObj -> attribute_4)){ 
					$val.=$queryRecordsObj -> attribute_4.":".$queryRecordsObj -> attribute_4_value.'<br>';
				}
				
				$row[]=$val;				
				$val2='';				
				$val2.='Tax:'.$queryRecordsObj -> tax_percent.'<br>';
				$val2.='Selling Price: '.curr_sym.$queryRecordsObj -> selling_price.'<br>';
				$val2.='Purchased Price: '.curr_sym.$queryRecordsObj -> purchased_price.'<br>';
				if($queryRecordsObj -> purchased_price>$queryRecordsObj -> selling_price){
					$val2.='Status : Loss ('.curr_sym.($queryRecordsObj -> purchased_price-$queryRecordsObj -> selling_price).')<br>';
				}
				else if($queryRecordsObj -> purchased_price<$queryRecordsObj -> selling_price){
					$val2.='Status : Profit ('.curr_sym.($queryRecordsObj -> selling_price-$queryRecordsObj -> purchased_price).')<br>';
				}
				else{
					$val2.='Status : Nill<br>';
				}
				$val2.='MOQ: '.$queryRecordsObj -> moq.'<br>';
				$val2.='Max OQ: '.$queryRecordsObj -> max_oq.'<br>';
				$val2.='Stock:'.$queryRecordsObj -> stock.'<br>';
				$val2.='Sold:'.$queryRecordsObj -> num_sales.'<br>';
				$val2.='Status:'.$queryRecordsObj -> product_status.'<br>';
				$row[]=$val2;	
				if($queryRecordsObj->active==1){
				$row[]='<input type="button" class="btn btn-warning btn-xs btn-block" value="Edit" onClick="editInventoryFun('.$queryRecordsObj->id.','.$queryRecordsObj->product_id.')">';
				}

				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('suadmin/header');
			$this->load->view('suadmin/login');
		}
		
	}
	
	
	
	public function edit_inventory_form($inventory_id,$product_id){
		if($this->session->userdata("logged_in")){
		$data["controller"]=$this;
		$data['id']=$inventory_id;	
		$data['product_id']=$product_id;
		$get_product_details_arr=$this->Su_model->get_product_details($product_id);
		$data['product_name']=$get_product_details_arr["product_name"];	
		
		
		$data['pcat_id']=$get_product_details_arr["pcat_id"];
		$data['cat_id']=$get_product_details_arr["cat_id"];	
		$data['subcat_id']=	$get_product_details_arr["subcat_id"];		
		$data['brand_id']=$get_product_details_arr["brand_id"];	
		$get_inventory_details_arr = $this->Su_model->get_inventory_details($inventory_id);
		$data["sku_id"]=$get_inventory_details_arr["sku_id"];
		$data['inventory'] = $this->Su_model->inventory($product_id);
		$data['parent_category'] = $this->Su_model->parent_category();
		$data['create_editview']=$this->input->post("create_editview");
		$data['attributes'] = $this->Su_model->get_all_attributes($get_product_details_arr["subcat_id"]);
		
		$data["attribute_1_value_display"]=$get_inventory_details_arr["attribute_1_value"];
		$data["attribute_2_value_display"]=$get_inventory_details_arr["attribute_2_value"];
		$data["attribute_3_value_display"]=$get_inventory_details_arr["attribute_3_value"];
		$data["attribute_4_value_display"]=$get_inventory_details_arr["attribute_4_value"];
		$data["hsncode"]=$get_inventory_details_arr["hsncode"];
		
		$vendor_id=1;
		$data['vendors']=$this->Su_model->vendors();
		$data['get_all_specification_group'] = $this->Su_model->get_all_specification_group($get_product_details_arr["pcat_id"],$get_product_details_arr["cat_id"],$get_product_details_arr['subcat_id']);
		$data['filterbox'] = $this->Su_model->get_all_filterbox($get_product_details_arr["pcat_id"],$get_product_details_arr["cat_id"],$get_product_details_arr['subcat_id']);
		$data['logistics_parcel_category']=$this->Su_model->logistics_parcel_category();
		$data['logistics_all_territory_name'] = $this->Su_model->logistics_all_territory_name();
		/////////////////////
		$inventory_data_arr=$this->get_all_inventory_data($product_id,$inventory_id);
		$data["inventory_data_arr"]=$inventory_data_arr;
		
		$get_all_specification_by_inventory_id_arr=$this->get_all_specification_by_inventory_id($inventory_id);
		$data["get_all_specification_by_inventory_id_arr"]=$get_all_specification_by_inventory_id_arr;
		
		$get_all_filter_by_inventory_id_arr=$this->get_all_filter_by_inventory_id($inventory_id);
		$data["get_all_filter_by_inventory_id_arr"]=$get_all_filter_by_inventory_id_arr;
		$data["menu_flag"]="catalog_active_links";
		
		
		$data["module_type"]=$this->session->userdata("module_type");
			
			
		//////
		$this -> load -> view('suadmin/header',$data);
		$this -> load -> view('suadmin/edit_inventory_view',$data);	
		}else{
			$this->load->view('suadmin/header');
			$this->load->view('suadmin/login');
		}	
	}
	
	public function get_all_inventory_data($product_id,$inventory_id){
		if($this->session->userdata("logged_in")){			
			$data=$this->Su_model->get_all_inventory_data($product_id,$inventory_id);
			return $data;
		}else{
			$this->load->view('suadmin/header');
			$this->load->view('suadmin/login');
		}
		
	}
	
	public function get_all_specification_by_inventory_id($inventory_id){
		if($this->session->userdata("logged_in")){
			$data=$this->Su_model->get_all_specification_by_inventory_id($inventory_id);
			return $data;
	
		}else{
			$this->load->view('suadmin/header');
			$this->load->view('suadmin/login');
		}
	}
	public function get_all_filter_by_inventory_id($inventory_id){
		if($this->session->userdata("logged_in")){

			$data=$this->Su_model->get_all_filter_by_inventory_id($inventory_id);
			return $data;
			
		}else{
			$this->load->view('suadmin/header');
			$this->load->view('suadmin/login');
		}
	}
	
	public function get_all_specification($specification_group_id){
		if($this->session->userdata("logged_in")){
			$data=$this->Su_model->get_all_specification($specification_group_id);
			return $data;
			
		}else{
			$this->load->view('suadmin/header');
			$this->load->view('suadmin/login');
		}
	}
	
	public function get_all_filter($filterbox_id){
		if($this->session->userdata("logged_in")){
			$data=$this->Su_model->get_all_filter($filterbox_id);
			return $data;
			
		}else{
			redirect(base_url());
		}
	}
	
	
	
	
	public function edit_inventory(){
		if($this->session->userdata("logged_in")){

			$this->load->library('upload');
			$files = $_FILES;
			$sku_id=addslashes($this->input->post("edit_sku_id"));
			
			$common_image='';
			$common_image_name=$files['edit_common_image']['name'];
			if($common_image_name!=""){
				$previous_image=$this->input->post("previous_image");
				if(file_exists($previous_image)) {
					unlink($previous_image);			   
				}
					
				$c_ext = pathinfo($common_image_name, PATHINFO_EXTENSION);
				$c_filen=rand(10,100000);
				
				$_FILES['edit_common_image']['name']=$c_filen.".".$c_ext;
				$_FILES['edit_common_image']['type']= $files['edit_common_image']['type'];
				$_FILES['edit_common_image']['tmp_name']= $files['edit_common_image']['tmp_name'];
				$_FILES['edit_common_image']['error']= $files['edit_common_image']['error'];
				$_FILES['edit_common_image']['size']= $files['edit_common_image']['size'];  
				$this->upload->initialize($this->set_upload_options_config($sku_id,300,366));
				
				if (!$this -> upload -> do_upload("edit_common_image")) {
					echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					$common_image = "assets/pictures/images/products/" .$sku_id."/". $upload_data['file_name'];
				}
			}else{
				$common_image=$this->input->post("previous_image");	  
			}
			
			$image_arr=[];
			$thumbnail_arr=[];
				if(!is_dir('assets/pictures/images/products/'.$sku_id)){
						mkdir('assets/pictures/images/products/'.$sku_id);
					}
			for($i=1;$i<=5;$i++){
					if($_FILES['edit_image'.$i]['name']!=''){
				
					$previous_image=$this->input->post("previous_image".$i);
					if(file_exists($previous_image)) {
						unlink($previous_image);			   
					}
					
					
					
					$filename=$files['edit_image'.$i]['name'];  
					$ext = pathinfo($filename, PATHINFO_EXTENSION);
					$filen=rand(10,100000);
					
					$_FILES['edit_image'.$i]['name']=$filen.".".$ext;
					$_FILES['edit_image'.$i]['type']= $files['edit_image'.$i]['type'];
					$_FILES['edit_image'.$i]['tmp_name']= $files['edit_image'.$i]['tmp_name'];
					$_FILES['edit_image'.$i]['error']= $files['edit_image'.$i]['error'];
					$_FILES['edit_image'.$i]['size']= $files['edit_image'.$i]['size'];  
					
					$this->upload->initialize($this->set_upload_options_config($sku_id,420,512));
					if (!$this -> upload -> do_upload("edit_image".$i)) {
						echo $this -> upload -> display_errors();exit;
					} else {
						$upload_data = $this -> upload -> data();
						$image = "assets/pictures/images/products/" .$sku_id."/". $upload_data['file_name'];
					}
				}else{
				   if($this->input->post("previous_image".$i)==""){
					   if($this->input->post("previous_image".$i."_unlink")!=""){
						   if(file_exists($this->input->post("previous_image".$i."_unlink"))){
							unlink($this->input->post("previous_image".$i."_unlink"));
						   }
					   }
					   $image="";
				   }
				   else{
					   $image=$this->input->post("previous_image".$i);
				   }
				}
				$image_arr[]=$image;
				
				
				 if($_FILES['edit_thumbnail'.$i]['name']!=''){ 
				
					$previous_thumbnail=$this->input->post("previous_thumbnail".$i);
					if(file_exists($previous_thumbnail)) {
						unlink($previous_thumbnail);
					}				
					$filename1=$files['edit_thumbnail'.$i]['name'];  
					$ext1 = pathinfo($filename1, PATHINFO_EXTENSION);
					$filen1=rand(10,100000);
					$_FILES['edit_thumbnail'.$i]['name']= $filen1.".".$ext1;
					$_FILES['edit_thumbnail'.$i]['type']= $files['edit_thumbnail'.$i]['type'];
					$_FILES['edit_thumbnail'.$i]['tmp_name']= $files['edit_thumbnail'.$i]['tmp_name'];
					$_FILES['edit_thumbnail'.$i]['error']= $files['edit_thumbnail'.$i]['error'];
					$_FILES['edit_thumbnail'.$i]['size']= $files['edit_thumbnail'.$i]['size'];
					$this->upload->initialize($this->set_upload_options_config($sku_id,100,122));
					
					if (!$this -> upload -> do_upload("edit_thumbnail".$i)) {
					echo $this -> upload -> display_errors();exit;
					} else {
						$upload_data = $this -> upload -> data();
						
						$thumbnail = "assets/pictures/images/products/" .$sku_id."/". $upload_data['file_name'];
					}
					
				}else{
					
				   if($this->input->post("previous_thumbnail".$i)==""){
					   if($this->input->post("previous_thumbnail".$i."_unlink")!=""){
						   if(file_exists($this->input->post("previous_thumbnail".$i."_unlink"))){
							unlink($this->input->post("previous_thumbnail".$i."_unlink"));
						   }
					   }
						$thumbnail="";
				   }
				   else{
					   $thumbnail=$this->input->post("previous_thumbnail".$i);
				   }
				   
				}
				$thumbnail_arr[]=$thumbnail;
				
				
				
					 if($_FILES['edit_largeimage'.$i]['name']!=''){ 
				
					$previous_largeimage=$this->input->post("previous_largeimage".$i);
					if(file_exists($previous_largeimage)) {
						unlink($previous_largeimage);
					}				
					$filename1=$files['edit_largeimage'.$i]['name'];  
					$ext1 = pathinfo($filename1, PATHINFO_EXTENSION);
					$filen1=rand(10,100000);
					$_FILES['edit_largeimage'.$i]['name']= $filen1.".".$ext1;
					$_FILES['edit_largeimage'.$i]['type']= $files['edit_largeimage'.$i]['type'];
					$_FILES['edit_largeimage'.$i]['tmp_name']= $files['edit_largeimage'.$i]['tmp_name'];
					$_FILES['edit_largeimage'.$i]['error']= $files['edit_largeimage'.$i]['error'];
					$_FILES['edit_largeimage'.$i]['size']= $files['edit_largeimage'.$i]['size'];
					$this->upload->initialize($this->set_upload_options_config($sku_id,850,1036));
					
					if (!$this -> upload -> do_upload("edit_largeimage".$i)) {
					echo $this -> upload -> display_errors();exit;
					} else {
						$upload_data = $this -> upload -> data();
						
						$largeimage = "assets/pictures/images/products/" .addslashes($this->input->post("edit_sku_id"))."/". $upload_data['file_name'];
					}
					
				}else{
					
				   if($this->input->post("previous_largeimage".$i)==""){
					   if($this->input->post("previous_largeimage".$i."_unlink")!=""){
						   if(file_exists($this->input->post("previous_largeimage".$i."_unlink"))){
							unlink($this->input->post("previous_largeimage".$i."_unlink"));
						   }
					   }
						$largeimage="";
				   }
				   else{
					   $largeimage=$this->input->post("previous_largeimage".$i);
				   }
				   
				}
				$largeimage_arr[]=$largeimage;
				
				
				
				
			}
			
			$inventory_id=$this->input->post("edit_inventory_id");
			
			$product_id=$this->input->post("edit_product_id");
			
			
			
			
			
			
			
			$base_price=$this->db->escape_str($this -> input -> post("edit_base_price"));
			$selling_price=$this->db->escape_str($this -> input -> post("edit_selling_price"));
			$purchased_price=$this->db->escape_str($this -> input -> post("edit_purchased_price"));
			//$mrp_quantity=$this->db->escape_str($this -> input -> post("edit_mrp_quantity"));
			$moq=$this->db->escape_str($this -> input -> post("edit_moq"));
			$max_oq=$this->db->escape_str($this -> input -> post("edit_max_oq"));
			$tax=$this->db->escape_str($this -> input -> post("edit_tax"));
			$stock=$this->db->escape_str($this -> input -> post("edit_stock"));
			$product_status=$this->db->escape_str($this -> input -> post("edit_product_status"));
			$product_status_view=$this->input->post("edit_product_status_view");
			$low_stock=$this->db->escape_str($this -> input -> post("edit_low_stock"));
			$cutoff_stock=$this->db->escape_str($this -> input -> post("edit_cutoff_stock"));
			//$policy_id=$this->input->post("edit_policy_id");
			
				

			
	
	

			
				
					
			
			$filter_arr=$this->input->post("edit_filter");
		
			
			
			for($k=1;$k<=5;$k++){
				if(count($image_arr)==$k){
					for($i=1;$i<=5-$k;$i++){
						$image_arr[]="";
					}
				}
			}
			
			foreach ($image_arr as $key => $value)
			{ 
				if ($value === "")
				{ 
					unset($image_arr[$key]);
					$image_arr[] = $value;
				}
			}

			// rebuild array index
			$image_arr = array_values($image_arr);
			
			///////////////////////////////////
			for($k=1;$k<=5;$k++){
				if(count($thumbnail_arr)==$k){
					for($i=1;$i<=5-$k;$i++){
						$thumbnail_arr[]="";
					}
				}
			}
			foreach ($thumbnail_arr as $key => $value)
			{ 
				if ($value === "")
				{ 
					unset($thumbnail_arr[$key]);
					$thumbnail_arr[] = $value;
				}
			}

			// rebuild array index
			$thumbnail_arr = array_values($thumbnail_arr);
			
			////////////////////////////////////////
			for($k=1;$k<=5;$k++){
				if(count($largeimage_arr)==$k){
					for($i=1;$i<=5-$k;$i++){
						$largeimage_arr[]="";
					}
				}
			}
			foreach ($largeimage_arr as $key => $value)
			{ 
				if ($value === "")
				{ 
					unset($largeimage_arr[$key]);
					$largeimage_arr[] = $value;
				}
			}

			// rebuild array index
			$largeimage_arr = array_values($largeimage_arr);

			$highlight=$this->input->post("highlight");
			$highlight=htmlentities($highlight, ENT_QUOTES);
			
			$flag=$this->Su_model->edit_inventory($inventory_id,$product_id,$common_image,$image_arr,$thumbnail_arr,$largeimage_arr,$base_price,$selling_price,$purchased_price,$moq,$max_oq,$tax,$stock,$product_status,$product_status_view,$low_stock,$cutoff_stock,$filter_arr,$highlight);
			
			if($flag==true){
				$flag_update_index=$this->update_search_index($inventory_id);
				if($flag_update_index==true){
					echo true;
				}else{
					echo 0;
				}
			}else{
				echo 0;
			}
	
		}else{
			//$this->load->view('admin/header');
			//$this->load->view('admin/login');
		}
	}
	
	public function update_search_index($inventory_id){
		include("assets/elastic/app/init_customer.php");
			
		$params = array();
		$params['index'] = 'flamingo1';
		$params['type'] = 'flamingoproducts1';
		$params['id'] = $inventory_id;
		$result = $es->get($params);
		

		$res=$this->Su_model->create_search_index($inventory_id);
		$parambody_arr=array();
		$parambody_arr=array("inventory_id"=>$inventory_id,"sku_id"=>$res->sku_id,"product_id"=>$res->product_id,"product_name"=>$res->product_name,"product_description"=>$res->product_description,"pcat_id"=>$res->pcat_id,"cat_id"=>$res->cat_id,"subcat_id"=>$res->subcat_id,"filter_brand_id"=>$res->brand_id,"image"=>$res->image,"thumbnail"=>$res->thumbnail,"tax_percent"=>$res->tax_percent,"base_price"=>$res->base_price,"selling_price"=>$res->selling_price,"cat_name"=>$res->cat_name,"subcat_name"=>$res->subcat_name,"brand_name"=>$res->brand_name,"common_image"=>$res->common_image);
		
		$query_filters=$this->Su_model->get_filter_id_new($res->inventory_id);
		
		
		foreach($query_filters as $res_filters){
			$filterbox_name=$this->Su_model->get_filterbox_name($res_filters->filter_id);
			$filterbox_name = str_replace(' ', '_', $filterbox_name);
			$parambody_arr["filter_".$filterbox_name."_id"]=$res_filters->filter_id;
		}
		
		$parambody_arr["sugg1"]=$res->sugg1;
		$parambody_arr["sugg2"]=$res->sugg2;
		$parambody_arr["generalsugg1"]=$res->generalsugg1;
		$parambody_arr["generalsugg2"]=$res->generalsugg2;
		$parambody_arr["generalsugg3"]=$res->generalsugg3;
		$parambody_arr["generalsugg4"]=$res->generalsugg4;
		$result['_source']=$parambody_arr;
		$params["body"]["doc"]=$result['_source'];
		
		$flag = $es->update($params);
		
		return $flag;
		
	}
	
	
	
	public function get_default_discount_for_inv($inventory_id,$selling_price,$product_id){
	
	//$product_id=$this->Su_model->get_product_id($inventory_id);
        $promo_array=array();
        if(!empty($product_id)){
            $a_TO_z_ids=$this->Su_model->get_a_TO_z_id($product_id);
            
            if(!empty($a_TO_z_ids)){
                foreach($a_TO_z_ids as $a_TO_z_id){
                    $brand_id=$a_TO_z_id['brand_id'];
                    $subcat_id=$a_TO_z_id['subcat_id'];
                    $cat_id=$a_TO_z_id['cat_id'];
                    $pcat_id=$a_TO_z_id['pcat_id'];
                }
                
                $product_level_promotions_uid=$this->Su_model->product_level_promotion_uid($product_id);
                $brand_level_promotions_uid=$this->Su_model->brand_level_promotion_uid($brand_id);
                $subcat_level_promotions_uid=$this->Su_model->subcat_level_promotion_uid($subcat_id);
                $cat_level_promotions_uid=$this->Su_model->cat_level_promotion_uid($cat_id);
                $pcat_level_promotions_uid=$this->Su_model->pcat_level_promotion_uid($pcat_id);
                $store_level_promotions_uid=$this->Su_model->store_level_promotion_uid();
               
                if(!empty($product_level_promotions_uid)){
                    foreach($product_level_promotions_uid as $product_level_promotion){
                        $promo_data=$this->Su_model->product_level_promotion($product_level_promotion['promo_uid'],$product_id);
                        
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
                        }
                    }
                    
                }
                
                if(!empty($brand_level_promotions_uid)){
                    
                    foreach($brand_level_promotions_uid as $brand_level_promotion){
                        $promo_data=$this->Su_model->brand_level_promotion($brand_level_promotion['promo_uid'],$brand_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
                            
                        }
                    }
                }
                if(!empty($subcat_level_promotions_uid)){
                    foreach($subcat_level_promotions_uid as $subcat_level_promotion){
                        $promo_data=$this->Su_model->subcat_level_promotion($subcat_level_promotion['promo_uid'],$subcat_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
                        }
                    }
                }
                if(!empty($cat_level_promotions_uid)){
                    foreach($cat_level_promotions_uid as $cat_level_promotion){
                        $promo_data=$this->Su_model->cat_level_promotion($cat_level_promotion['promo_uid'],$cat_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
                        }
                    }
                }
                if(!empty($pcat_level_promotions_uid)){
                    foreach($pcat_level_promotions_uid as $pcat_level_promotion){
                        $promo_data=$this->Su_model->pcat_level_promotion($pcat_level_promotion['promo_uid'],$pcat_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
                        }
                    }
                }
                if(!empty($store_level_promotions_uid)){
                    foreach($store_level_promotions_uid as $store_level_promotion){
                        $promo_data=$this->Su_model->store_level_promotion($store_level_promotion['promo_uid']);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
                        }
                    }
                }
            }
        }
        $promo_uid=$this->Su_model->get_promotions($inventory_id);
        
        if(!empty($promo_uid)){
            foreach($promo_uid as $res){
                $promo_data=$this->Su_model->get_all_promotions_data($res['promo_uid'],$inventory_id);
                if(!empty($promo_data)){
                    array_push($promo_array,$promo_data);
                    
                }
            }
        }
        $inventory_product_info_obj=$this->get_inventory_product_info_by_inventory_id($inventory_id);
        //print_r($promo_array);
		$default_price_array=array();
		if(!empty($promo_array)){
			
			foreach($promo_array as $promotion){
				foreach($promotion as $promo){
					if (preg_match_all('/\d+(?=%)/', $promo-> get_type, $match)&&($promo->to_buy==1)){
						if($promo->to_buy==1){
							$offer = $promo->promo_quote;
							$offer = preg_replace("/\([^)]+\)/","",$offer);        
							$default_price_array['current_price']= round($selling_price-round($selling_price*($match[0][0]/100)));
							$default_price_array['inventory_price']= $selling_price;
							$default_price_array['discount']= $match[0][0];
						}
					}	
				}
			}
		}
		return $default_price_array;
}

public function get_inventory_product_info_by_inventory_id($inventory_id){
		$inventory_info=$this->Su_model->get_inventory_product_info_by_inventory_id($inventory_id);
		return $inventory_info;
	}
	
	
	public function inventory_export(){

        if($this->session->userdata("logged_in")){
			$data["module_type"]=$this->session->userdata("module_type");
            $data_posted=$this->input->post();

            if($this->session->flashdata('inv_post_data')){

                $data_posted=$this->session->flashdata('inv_post_data');
                $data['import_success']=1;
                $data['import_success_msg']=$this->session->flashdata('import_msg');
                // print_r($data);exit;
            }

            if($this->input->post("export_inventory")){
                //print_r($this->input->post());
                $this->export_inventory_all($this->input->post());
            }

            $data["controller"]=$this;
            $data['product_id']=(isset($data_posted["products"])) ? $data_posted["products"] : '';
            $data['brand_id']=(isset($data_posted["brands"])) ? $data_posted["brands"]: '';
            $data['subcat_id']=(isset($data_posted["subcategories"])) ?$data_posted["subcategories"] : '';
            $data['cat_id']=(isset($data_posted["categories"])) ? $data_posted["categories"]: '';
            $data['pcat_id']=(isset($data_posted["parent_category"])) ?$data_posted["parent_category"] : '';

            $data['inventory'] = $this->Su_model->inventory($data['product_id']);
            $data['product_details'] = $this->Su_model->get_product_details($data['product_id']);
            $data['parent_category'] = $this->Su_model->parent_category();

            $vendor_id=1;
            //$data['pcat_cat_subcat_view'] = $this->Su_model->pcat_cat_subcat_view();

            $data['vendors']=$this->Su_model->vendors();
            $data['get_all_specification_group'] = $this->Su_model->get_all_specification_group($data['pcat_id'],$data['cat_id'],$data['subcat_id']);
            ////
            $data["menu_flag"]="catalog_active_links";
            $this -> load -> view('suadmin/header',$data);
            $this -> load -> view('suadmin/import_export_inventory_view',$data);

        }else{
            $this->load->view('suadmin/header');
            $this->load->view('suadmin/login');
        }
    }
	
	public function export_inventory_all($arr){
        $data_posted=$arr;
        $product_id=(isset($data_posted["products"])) ? $data_posted["products"]: '';
        $brand_id=(isset($data_posted["brands"])) ? $data_posted["brands"]: '';
        $subcat_id=(isset($data_posted["subcategories"])) ?$data_posted["subcategories"] : '';
        $cat_id=(isset($data_posted["categories"])) ? $data_posted["categories"]: '';
        $pcat_id=(isset($data_posted["parent_category"])) ?$data_posted["parent_category"] : '';

        $inventory_details = $this->Su_model->get_inventory_details_result($arr['products'],$pcat_id,$cat_id,$subcat_id,$brand_id);
        $product_details = $this->Su_model->get_product_details_result($arr['products']);

        $header_top =array("Product name","Product description","Subcategory name","Category name","Parent category name","Brand name");
        /* file name */
        $product_name=(isset($product_details['product_name'])) ? '_'.$product_details['product_name']:'';
        $product_name = str_replace(' ', '_', $product_name);
        $filename = 'product_inventory'.$product_name.'_'.date('Ymd').'.csv';
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/csv; ");
        /* get data */
        //$usersData = $this->Crud_model->getUserDetails();
        /* file creation */

        //$usersData=array(array('jon','jhon','male','jhon@gmail.com'));
        //$header = array("Username","Name","Gender","Email");

        $file = fopen('php://output','w');

        $header = array("sku_id", "attribute_1", "attribute_1_value", "attribute_2", "attribute_2_value", "attribute_3", "attribute_3_value", "attribute_4", "attribute_4_value", "tax_percent", "base_price", "selling_price", "purchased_price", "mrp_quantity", "moq", "moq_base_price", "moq_price", "moq_purchased_price", "max_oq", "num_sales", "stock", "low_stock", "cutoff_stock");

        //fputcsv($file, $header_top);
        //fputcsv($file, $product_details);
        fputcsv($file, $header);
        foreach ($inventory_details as $key=>$line){
            fputcsv($file,$line);
        }
        fclose($file);
        exit;
    }
	
	
	public function show_available_categories(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("pcat_id");
			$active=$this->input->post("active");
			if($this->input->post("with_status")){
				$with_status=$this->input->post("with_status");
			}else{
				$with_status=0;
			}
			$result_cat=$this->Su_model->show_available_categories($pcat_id,$active);
			if(count($result_cat)!=0){
				$str='';
				$str.='<option value=""></option>';	
				if($with_status==1){
					foreach($result_cat as $res){
						$active_status=($res->active==1)?" - active":" - Inactive";
						$str.='<option value="'.$res->cat_id.'-'.$res->active.'">'.$res->cat_name.$active_status.'</option>';
					}
				}else{
					foreach($result_cat as $res){
					$str.='<option value="'.$res->cat_id.'">'.$res->cat_name.'</option>';
					}
				}
				echo $str;
			}else{
				echo count($result_cat);
			}
		}else{
			//$this->load->view('admin/header');
			//$this->load->view('admin/login');
		}
	}
	
	public function show_available_subcategories(){
		if($this->session->userdata("logged_in")){
			$cat_id=$this->input->post("cat_id");
			$active=$this->input->post("active");
			if($this->input->post("with_status")){
				$with_status=$this->input->post("with_status");
			}else{
				$with_status=0;
			}
			$result_subcat=$this->Su_model->show_available_subcategories($cat_id,$active);
			if(count($result_subcat)!=0){
				$str='';
				$str.='<option value=""></option>';
				if($with_status==1){
					foreach($result_subcat as $res){
						$active_status=($res->active==1)?" - active":" - Inactive";
						$str.='<option value="'.$res->subcat_id.'-'.$res->active.'">'.$res->subcat_name.$active_status.'</option>';
					}
				}else{
					foreach($result_subcat as $res){
					$str.='<option value="'.$res->subcat_id.'">'.$res->subcat_name.'</option>';
					}
				}
				echo $str;
			}else{
				echo count($result_subcat);
			}
		}else{
			//$this->load->view('admin/header');
			//$this->load->view('admin/login');
		}
	}
	
	public function show_available_brands(){
		if($this->session->userdata("logged_in")){	
			$subcat_id=$this->input->post("subcat_id");
			$active=$this->input->post("active");
			if($this->input->post("with_status")){
				$with_status=$this->input->post("with_status");
			}else{
				$with_status=0;
			}
			$result_brand=$this->Su_model->show_available_brands($subcat_id,$active);
			if(count($result_brand)!=0){
				$str='';
				$str.='<option value=""></option>';
				if($with_status==1){
					foreach($result_brand as $res){
						$active_status=($res->active==1)?" - active":" - Inactive";
						$str.='<option value="'.$res->brand_id.'-'.$res->active.'">'.$res->brand_name.$active_status.'</option>';
					}
				}else{
					foreach($result_brand as $res){
						$str.='<option value="'.$res->brand_id.'">'.$res->brand_name.'</option>';
					}
				}
				echo $str;
			}else{
				echo count($result_brand);
			}
		}else{
			//$this->load->view('admin/header');
			//$this->load->view('admin/login');
		}
	}
	
	 public function show_available_products(){
		if($this->session->userdata("logged_in")){	
			$brand_id=$this->input->post("brand_id");
			$active=$this->input->post("active");
			if($this->input->post("with_status")){
				$with_status=$this->input->post("with_status");
			}else{
				$with_status=0;
			}
			$result_brand=$this->Su_model->show_available_products($brand_id,$active);
			print_r($result_brand);
			if(count($result_brand)!=0){
				$str='';
				$str.='<option value=""></option>';
				if($with_status==1){
					foreach($result_brand as $res){
						$active_status=($res->active==1)?" - active":" - Inactive";
						$str.='<option value="'.$res->product_id.'-'.$res->active.'">'.$res->product_name.$active_status.'</option>';
					}
				}else{
					foreach($result_brand as $res){
						$str.='<option value="'.$res->product_id.'">'.$res->product_name.'</option>';
					}
				}
				echo $str;
			}else{
				echo count($result_brand);
			}
		}else{
			//$this->load->view('admin/header');
			//$this->load->view('admin/login');
		}
	}
}


