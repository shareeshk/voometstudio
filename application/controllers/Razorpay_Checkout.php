<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require ('razorpay-php/Razorpay.php');
use Razorpay\Api\Api;
use Razorpay\Api\Errors\SignatureVerificationError;


class Razorpay_Checkout extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('admin/Model_orders');
        date_default_timezone_set('Asia/Kolkata');
    }
    public function index() {
        $this->checkout();
    }

    public function checkout() {
        $data['title']              = 'Checkout payment | Infovistar';
        $data['callback_url']       = base_url().'razorpay/callback';
        //$data['surl']               = base_url().'razorpay/success';
        //$data['furl']               = base_url().'razorpay/failed';
        //
        $data['surl']               = base_url()."Account/confirm_order_process";
        $data['furl']               = base_url()."Account/failed_order_process";
        $data['currency_code']      = 'INR';
        $this->load->view('razorpay/checkout', $data);
    }

    public function create_order(){
        $key_id         = razor_pay_key;
        $key_secret     = razor_pay_secret;
        $amount= $this->input->post('amount');
        //$amount=$request->amount;

        $api = new Api($key_id, $key_secret);
        $orderData = [
            'receipt' => 345678,
            'amount' => $amount,
            'currency' => curr_code,
            'payment' => array(
                'capture' => 'automatic',
                'capture_options' => array(
                    'automatic_expiry_period' => 12,
                    'manual_expiry_period' => 7200,
                    'refund_speed' => 'optimum'
                )
            )
        ];
        $razorpayOrder = $api->order->create($orderData);

        //print_r($razorpayOrder);
        //exit;
        $razorpayOrderId = $razorpayOrder['id'];
        //$_SESSION['razorpay_order_id'] = $razorpayOrderId;
        //Session::put('razorpay_order_id',$razorpayOrderId);
        $this->session->set_userdata("razorpay_order_id",$razorpayOrderId);
        $displayAmount = $amount = $orderData['amount'];
        $orderData["razorpay_order_id"]=$razorpayOrderId;
        echo json_encode($orderData);
    }
    public function razorpay_verify_signature()
    {
        $success = true;
        $error = "Payment Failed";
        $key_id         = razor_pay_key;
        $key_secret     = razor_pay_secret;

        $razorpay_order_id = $this->input->post('razorpay_order_id');
        $razorpay_signature = $this->input->post('razorpay_signature');
        $razorpay_payment_id = $this->input->post('razorpay_payment_id');

        if (empty($razorpay_payment_id) === false) {
            try {
                $generated_signature = hash_hmac('sha256', $razorpay_order_id . "|" . $razorpay_payment_id, $key_secret);

                if ($generated_signature == $razorpay_signature) {
                    //echo "payment success";
                }

                if ($razorpay_order_id != '' && $razorpay_signature != '' && $razorpay_payment_id = '') {
                    $api = new Api($key_id, $key_secret);
                    $attrbutes = array("razorpay_signature" => $razorpay_signature, "razorpay_payment_id" => $razorpay_payment_id, "razorpay_order_id" => $razorpay_order_id);
                    $order = $api->utility->verifyPaymentSignature($attrbutes);
                    //print_r($order);
                    //echo json_encode($order);
                }
            } catch (SignatureVerificationError $e) {
                $success = false;
                $error = 'Razorpay Error : ' . $e->getMessage();
            }
        }
        $resData=array();
        if ($success === true)
        {
            $resData['status']=$success;
            $resData['paymentID']=$_POST['razorpay_payment_id'];
            $resData['error']='';
            $html = "<p>Your payment was successful</p>
             <p>Payment ID: {$_POST['razorpay_payment_id']}</p>";
        }
        else
        {
            $resData['status']=$success;
            $resData['paymentID']=$_POST['razorpay_payment_id'];
            $resData['error']='1';
            $html = "<p>Your payment failed</p>
             <p>{$error}</p>";
        }
        echo json_encode($resData);
        //echo $html;
    }
        // initialized cURL Request
    private function curl_handler($payment_id, $amount)  {

        $url            = 'https://api.razorpay.com/v1/payments/'.$payment_id.'/capture';
       /* $key_id         = "rzp_test_sVRFAR6t11Wr91";
        $key_secret     = "3RZOUZ6V3sGEHKPts1trXFNn";*/
        $key_id         = razor_pay_key;
        $key_secret     = razor_pay_secret;

        $fields_string  = "amount=$amount";
        //cURL Request
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $key_id.':'.$key_secret);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        return $ch;
    }

    // callback method
    public function callback() {
        //print_r($this->input->post());

        $razorpay_payment_id = $this->input->post('razorpay_payment_id');
        $merchant_order_id = $this->input->post('merchant_order_id');
        $razorpay_order_id = $this->input->post('razorpay_order_id');
        $this->session->set_flashdata('razorpay_payment_id', $razorpay_payment_id);
        $this->session->set_flashdata('merchant_order_id', $merchant_order_id);//internal order id
        $this->session->set_flashdata('razorpay_order_id', $razorpay_order_id);
		
		//$this->session->set_userdata('razorpay_payment_id', $razorpay_payment_id);
       // $this->session->set_userdata('razorpay_order_id', $razorpay_order_id);
		

        if (!empty($this->input->post('razorpay_payment_id')) && !empty($this->input->post('merchant_order_id'))) {
            //success
            redirect($this->input->post('merchant_surl_id').'/'.$merchant_order_id.'/'.$razorpay_payment_id.'/'.$razorpay_order_id);
        }else{
            //fail
            redirect($this->input->post('merchant_furl_id').'/'.$merchant_order_id.'/'.$razorpay_payment_id.'/'.$razorpay_order_id);
        }
        exit;
        if (!empty($this->input->post('razorpay_payment_id')) && !empty($this->input->post('merchant_order_id'))) {


            $razorpay_payment_id = $this->input->post('razorpay_payment_id');
            $merchant_order_id = $this->input->post('merchant_order_id');

            $this->session->set_flashdata('razorpay_payment_id', $this->input->post('razorpay_payment_id'));
            $this->session->set_flashdata('merchant_order_id', $this->input->post('merchant_order_id'));
            $currency_code = 'INR';
            $amount = $this->input->post('merchant_total');
            $success = false;
            $error = '';
            try {
                $ch = $this->curl_handler($razorpay_payment_id, $amount);
                //execute post
                $result = curl_exec($ch);
                $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if ($result === false) {
                    $success = false;
                    $error = 'Curl error: '.curl_error($ch);
                } else {
                    $response_array = json_decode($result, true);
                    //Check success response
                    if ($http_status === 200 and isset($response_array['error']) === false) {
                        $success = true;
                    } else {
                        $success = false;
                        if (!empty($response_array['error']['code'])) {
                            $error = $response_array['error']['code'].':'.$response_array['error']['description'];
                        } else {
                            $error = 'RAZORPAY_ERROR:Invalid Response <br/>'.$result;
                        }
                    }
                }
                //close curl connection
                curl_close($ch);
            } catch (Exception $e) {
                $success = false;
                $error = 'Request to Razorpay Failed';
            }

           // echo $error;
           // exit;
            if ($success === true) {
                if(!empty($this->session->userdata('ci_subscription_keys'))) {
                    $this->session->unset_userdata('ci_subscription_keys');
                }
                if (!$order_info['order_status_id']) {
                    redirect($this->input->post('merchant_surl_id').'/'.$merchant_order_id);
                } else {
                    redirect($this->input->post('merchant_surl_id').'/'.$merchant_order_id);
                }

            } else {
                redirect($this->input->post('merchant_furl_id').'/'.$merchant_order_id);
            }
        } else {
            echo 'An error occured. Contact site administrator, please!';
        }
    }
    public function success() {
        $data['title'] = 'Razorpay Success | TechArise';
        echo "<h4>Your transaction is successful</h4>";
        echo "<br/>";
        echo "Transaction ID: ".$this->session->flashdata('razorpay_payment_id');
        echo "<br/>";
        echo "Order ID: ".$this->session->flashdata('merchant_order_id');
    }
    public function failed() {
        $data['title'] = 'Razorpay Failed | TechArise';
        echo "<h4>Your transaction got Failed</h4>";
        echo "<br/>";
        echo "Transaction ID: ".$this->session->flashdata('razorpay_payment_id');
        echo "<br/>";
        echo "Order ID: ".$this->session->flashdata('merchant_order_id');
    }
    public function razorpay_refund(){
        /* common for cancel,return refund */

        $key_id         = razor_pay_key;
        $key_secret     = razor_pay_secret;
        $amount=$this->input->post("amount");
        //$memo_to_buyer=$this->input->post("memo_to_buyer");
        $paymentId     = $this->input->post('paymentId');
        $comments     = $this->input->post("admin_comments");

        /*$amount=1;
        $paymentId='pay_IGlyqLcMgEsJI8';
        $comments="refund test text";*/
        //$amount=1;// for checking
        $amount=($amount*100);

        $receipt=time();
        $api = new Api($key_id, $key_secret);

        $response='';$rpnse=array();

        try {
            $res = $api->payment->fetch($paymentId)->refund(array("amount" => $amount, "speed" => "optimum", "notes" => array("notes_key_1" => $comments, "notes_key_2" => ""), "receipt" => $receipt));

            $refund_id='';
            $refund_status='';
            if(isset($res->id)){
                //print_r($res);
                $refund_id=$res->id;
                $refund_status=$res->status;
                if($res->status=='processed'){
                   // $this->Model_orders->update_refund_data($cancel_id,$refund_id,$refund_status,$order_item_id);
                }
            }

            $rpnse=array('status'=>'1','response'=>$res,'razorpay_refund_id'=>$refund_id,'razorpay_refund_status'=>$refund_status);

        }catch (Razorpay\Api\Errors\BadRequestError $ex){
            $msg  = 'Error Code: '.$ex->getCode().". Message: ".$ex->getMessage();
            $response=$msg;
            $rpnse=array('status'=>'0','response'=>$response);
        }

        echo json_encode($rpnse);
    }
}