<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Search extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Customer_blogs');
		$this->load->model('Customer_frontend');
		$this->load->model('Customer_account');
		$this->load->model('Model_search');
		$this->load->model('Front_promotions');
        $this->load->model('Front_policy');
		$this->load->helper('notifications');
		$this->load->helper('user_analytics');
		$this->load->helper('cookie');
		$this->load->library('My_PHPMailer');
		$this->load->library('email');
		$this->load->helper('emailsmstemplate');
		$this->load->helper('general');
		$this->load->helper('s3_fileupload');
		date_default_timezone_set('Asia/Kolkata');
		$this->load->helper('date');
	}
	public function angular($str)
	{
		$data["controller"]=$this;

		$this->load->view('default');
		
	}

	public function index2(){
		$data['page_name']='';
		$data['page_type']='';
		$data['page_id']='';
		
		
		$this->load->view('index1');
		//$this->load->view('footer');
	}
	public function index(){
		////read_and_update_traffic();
		//visitor_activity_analytics();	
		
		//echo $da=get_cookie('remember_me1');
		$method=current_url();
		$this->session->set_userdata("products_page",$method);
		$data["ui_arr"]=$this->get_ui_data_for_index();
		$data['page_name']='';
		$data['page_type']='';
		$data['page_id']='';
		$data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["current_controller"]="search";
		$data["controller"]=$this;
		$data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
		$data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$data["ganesh_image_yes_or_no"]=$this->Customer_frontend->ganesh_image_yes_or_noFun();

		$parent_categories=$this->Customer_frontend->parent_category();
		$data['parent_categories']=$parent_categories;

		$front_product_display=$this->Customer_frontend->front_product_display();
		$data['front_product_display']=$front_product_display;
		
		$this->load->view('header',$data);
		
		$this->load->view('index');
		$this->load->view('footer');
	}
	
	public function sortBy($field,&$array,$direction='asc')
	{
		/*usort($array, create_function('$a, $b', '
			$a = $a["' . $field . '"];
			$b = $b["' . $field . '"];

			if ($a == $b)
			{
				return 0;
			}

			return ($a ' . ($direction == 'desc' ? '>' : '<') .' $b) ? -1 : 1;
		'));*/

		return true;
	}
	
	public function sample_code()
	{
		$seed = str_split('abcdefghijklmnopqrstuvwxyz'
                     .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                     .'0123456789'); // and any other characters
				shuffle($seed); // probably optional since array_is randomized; this may be redundant
				$rand = '';
				foreach (array_rand($seed, 9) as $k) 
					$rand .= $seed[$k];	
				return $rand;
	}
	
	public function incrementalHash($len = 5){
	  $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	  $base = strlen($charset);
	  $result = '';
	  $temp=explode(' ', microtime());
	  $now = $temp[1];
	  while ($now >= $base){
		$i = $now % $base;
		$result = $charset[$i] . $result;
		$now /= $base;
	  }
	  echo substr($result, -5);
	}
	
	public function provide_first_level_menu()
	{
		$p_menu=$this->Customer_frontend->provide_parent_category_level();
		$menu1=array();
		
		foreach($p_menu as $val)
		{
			if($val->menu_level==1)
			{
				$menu1[]=array('parent_menu'=>$val->pcat_name,'type'=>'parent_cat','id'=>$val->pcat_id,'pcat_id'=>$val->pcat_id,'order'=>$val->menu_sort_order,'show_sub_menu'=>$val->show_sub_menu);
			}
		}
		
		$s_menu=$this->Customer_frontend->provide_category_level();
		
		foreach($s_menu as $value)
		{
			if($value->menu_level==1)
			{
				$menu1[]=array('parent_menu'=>$value->cat_name,'type'=>'cat','id'=>$value->cat_id,'order'=>$value->menu_sort_order,'pcat_id'=>$value->pcat_id);
			}
		}
		@$this->sortBy('order',$menu1);
		return $menu1;
	}
	
	public function provide_category($pcat_id)
	{
		return $this->Customer_frontend->provide_category($pcat_id);
		
	}
	
	public function provide_sub_category($cat_id)
	{
		$sub_cat=$this->Customer_frontend->provide_sub_category($cat_id);
		//$sub_cat=$this->Customer_frontend->provide_sub_category($cat_value->cat_id);
				
		$menu3=array();
		foreach ($sub_cat as $subcat_value)
		{
			$menu3[]=array('subcat_id'=>$subcat_value->subcat_id,'subcat_name'=>$subcat_value->subcat_name,'order'=>$subcat_value->menu_sort_order);			
		}
			@$this->sortBy('order',$menu3);
		return $menu3;

	}
	public function provide_brands($subcat_id)
	{
		return $this->Customer_frontend->provide_brands($subcat_id);
		
	}
	public function get_subcat_name($pcat,$cat,$subcat){
		if(preg_match("/[a-z]/i", $pcat) || $pcat=="" || preg_match("/[a-z]/i", $cat) || $cat=="" || preg_match("/[a-z]/i", $subcat) || $subcat==""){
			return "";
		}
		else{
			return $this->Model_search->get_subcat_name($pcat,$cat,$subcat);
		}
	}
	public function get_cat_name($pcat,$cat){
		if(preg_match("/[a-z]/i", $pcat) || $pcat=="" || preg_match("/[a-z]/i", $cat) || $cat==""){
			return "";
		}
		else{
			return $this->Model_search->get_cat_name($pcat,$cat);
		}
	}
	public function get_pcat_name($pcat){
		if(preg_match("/[a-z]/i", $pcat) || $pcat==""){
			return "";
		}
		else{
			$pcat_name=$this->Model_search->get_pcat_name($pcat);
			if(isset($pcat_name)){
				return $pcat_name;
			}
			else{
				return "";
			}
		}
	}
	public function get_parentcategory_name($pcat_id){
		$parentcategory_name=$this->Model_search->get_parentcategory_name($pcat_id);
		return $parentcategory_name;
	}
	public function search_category($pcat,$cat='',$subcat='',$jarvinai_inventory_id_list=''){

		////read_and_update_traffic();
		//visitor_activity_analytics();

		if($cat!="" && $subcat!=""){
			$category_is_exists="yes";
			$subcategory_is_exists="yes";
		}
		else if($cat!="" && $subcat==""){
			$category_is_exists="yes";
			$subcategory_is_exists="no";
		}
		else if($cat=="" && $subcat!=""){
			$category_is_exists="no";
			$subcategory_is_exists="yes";
		}
		else{
			$category_is_exists="no";
			$subcategory_is_exists="no";
		}
		if(!$this->input->post()){
			$parentcategory_name="";
			$category_name="";
			$subcategory_name="";
			if($pcat!=""){
				$pcat=substr($pcat, 9);
				if($pcat!=0){
					$parentcategory_name=$this->Model_search->get_parentcategory_name($pcat);
					$parentcategory_name=strtolower($parentcategory_name);
					$parentcategory_name = str_replace(' ', '-', $parentcategory_name);
				}
				else{
					$parentcategory_name="null";
				}
			}
			if($cat!=""){
				$cat=substr($cat, 9);
				$category_name=$this->Model_search->get_category_name($cat);
				$category_name=strtolower($category_name);
				$category_name = str_replace(' ', '-', $category_name);
			}
			if($subcat!=""){
				$subcat=substr($subcat, 9);
				$subcategory_name=$this->Model_search->get_subcategory_name($subcat);
				$subcategory_name=strtolower($subcategory_name);
				$subcategory_name = str_replace(' ', '-', $subcategory_name);
			}
			redirect(site_url("catalog/".$parentcategory_name."/".$category_name."/".$subcategory_name.'/'.$jarvinai_inventory_id_list));
		}
		else{
			
			//visitor_activity();
		$method=current_url();
		$this->session->set_userdata("products_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		if($subcat!=''){
			$subcate_id=substr($subcat, 9);
			$inv_id=$this->Model_search->get_count_of_invs($subcate_id);
			
			if(!empty($inv_id)){
				$inv_count=count($inv_id);
				if($inv_count==1){
					$rand=$this->sample_code();
					$obj=reset($inv_id);
					$invent_rand_id=$rand.$obj->id;
					redirect(site_url("detail/".$invent_rand_id));
				}
			}
		}

		$search_keywords=$this->input->post('search_keywords');
		if($search_keywords!=''){
			$search_keyword_index_page=strtolower($search_keywords);
			$flag=$this->Model_search->dump_search_keywords($search_keywords);
			//echo $flag;
		}
			$search_keyword="";$search_cat="";$page_rows="";$pagenum="";
            $sorting_by="";$cat_current="";$cat_current_id="";$type_of_filter="";
            $color_filter="";$color_prev="";$size_filter="";$size_prev="";
            $price_filter="";$price_prev="";$brand_filter="";$brand_prev="";
			//print_r($this->input->post());exit;
			if(!$this->input->post()){
				
				if($pcat!="" && $cat!="" && $subcat!=""){
				   $pcat=substr($pcat, 9);
					$cat=substr($cat, 9);
					$subcat=substr($subcat, 9);
				   $level_type="subcat_id";
				   $level_value=$pcat."_".$cat."_".$subcat;
				   $level_name=$this->get_subcat_name($pcat,$cat,$subcat);
				}
				if($pcat!="" && $cat!="" && $subcat==""){
				   $pcat=substr($pcat, 9);
					$cat=substr($cat, 9);
					
				   $level_type="cat_id";
				   $level_value=$pcat."_".$cat;
				   $level_name=$this->get_cat_name($pcat,$cat);
				}
				if($pcat!="" && $cat=="" && $subcat==""){
				   $pcat=substr($pcat, 9);
					
					
				   $level_type="pcat_id";
				   $level_value=$pcat;
				   $level_name=$this->get_pcat_name($pcat);
				}
            }
            else{
			 extract($this->input->post());
			  $pcat=substr($pcat, 9);
			$cat=substr($cat, 9);
			$subcat=substr($subcat, 9);
            }
            
            $data['p_category']=$this->provide_first_level_menu();
        $data["controller"]=$this;
		
		
        $data["current_controller"]="search";
        $data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
        $data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
        
            if($level_type=="brand_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id=$level_value_arr[3];
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$brand_id;
            }
            if($level_type=="subcat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$subcat_id;
            }
			if($level_type=="cat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$cat_id;
            }
			if($level_type=="pcat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id="";
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$pcat_id;
            }
            if($level_type=="product_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id=$level_value_arr[3];
                $product_id=$level_value_arr[4];
                $inventory_id="";
                $data['cat_current_id']=$product_id;
               
            }

			if($level_type=="inventory_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id=$level_value_arr[3];
                $product_id=$level_value_arr[4];
				$inventory_id=$level_value_arr[5];
                $data['cat_current_id']=$inventory_id;
               
            }
                $data["pcat_id"]=$pcat_id;
                $data['cat_id']=$cat_id;
                $data['subcat_id']=$subcat_id;
                $data['brand_id']=$brand_id;
                $data['product_id']=$product_id;
                $data['inventory_id']=$inventory_id;
                $data['searched_key']=$level_name;
                $data['level_type']=$level_type;
			$arr=array();
            $arr["pcat_id"]=$pcat_id;
            $arr['cat_id']=$cat_id;
            $arr['subcat_id']=$subcat_id;
            $arr['brand_id']=$brand_id;
            $arr['product_id']=$product_id;
             
            
            $data["category_tree"]=$arr;
            $data['level_value']=$level_value;
            $data['cat_current']=$level_type;
			$data["controller"]=$this;
            
            
			$data["current_controller"]="search";
			$data['search_keyword_index_page']="";
			if($this->input->post("search_keyword_index_page")!=""){
				$data['search_keyword_index_page']=$this->input->post("search_keyword_index_page");
			}
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			
			
			if($category_is_exists=="yes" && $subcategory_is_exists=="yes"){
				if($pcat!=""){
					$get_parent_cat_info_obj=$this->get_parent_cat_info($pcat);
				}
				if($cat!=""){
					$get_cat_info_obj=$this->get_cat_info($cat);
				}
				if($subcat!=""){
					$get_subcat_info_obj=$this->get_subcat_info($subcat);
				}
				
				if($pcat=="" || $cat=="" || $subcat==""){
					$this->load->view('error404',$data);
				}
				else if(preg_match("/[a-z]/i", $pcat) || preg_match("/[a-z]/i", $cat) || preg_match("/[a-z]/i", $subcat)){
					$this->load->view('error404',$data);
				}
				else if($pcat==0){
					if(empty($get_cat_info_obj) || empty($get_subcat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category',$data);
					}
				}
				else if($pcat!=0){
					if(empty($get_parent_cat_info_obj) || empty($get_cat_info_obj) || empty($get_subcat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category',$data);
					}
				}
				else{
					$this->load->view('search_category',$data);
				}
			}
			else if($category_is_exists=="yes" && $subcategory_is_exists=="no"){
				
				if($pcat!=""){
					$get_parent_cat_info_obj=$this->get_parent_cat_info($pcat);
				}
				if($cat!=""){
					$get_cat_info_obj=$this->get_cat_info($cat);
				}
				
				if($pcat=="" || $cat==""){
					$this->load->view('error404',$data);
				}
				else if(preg_match("/[a-z]/i", $pcat) || preg_match("/[a-z]/i", $cat)){
					$this->load->view('error404',$data);
				}
				else if($pcat==0){
					if(empty($get_cat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category',$data);
					}
				}
				else if($pcat!=0){
					if(empty($get_parent_cat_info_obj) || empty($get_cat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category',$data);
					}
				}
				else{
					$this->load->view('search_category',$data);
				}
			
			}
			else if($category_is_exists=="no" && $subcategory_is_exists=="no"){
				
				if($pcat!=""){
					$get_parent_cat_info_obj=$this->get_parent_cat_info($pcat);
				}
				if($pcat==""){
					$this->load->view('error404',$data);
				}
				else if(preg_match("/[a-z]/i", $pcat)){
					$this->load->view('error404',$data);
				}
				else if($pcat==0){
					$this->load->view('search_category',$data);
				}
				else if($pcat!=0){
					if(empty($get_parent_cat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category',$data);
					}
				}
				else{
					$this->load->view('search_category',$data);
				}
			
			}
			$this->load->view('footer');
		}
	}
	public function search_category_combo($pcat,$cat='',$subcat=''){
		////read_and_update_traffic();
		//visitor_activity_analytics();

		if($cat!="" && $subcat!=""){
			$category_is_exists="yes";
			$subcategory_is_exists="yes";
		}
		else if($cat!="" && $subcat==""){
			$category_is_exists="yes";
			$subcategory_is_exists="no";
		}
		else if($cat=="" && $subcat!=""){
			$category_is_exists="no";
			$subcategory_is_exists="yes";
		}
		else{
			$category_is_exists="no";
			$subcategory_is_exists="no";
		}
		if(!$this->input->post()){
			$parentcategory_name="";
			$category_name="";
			$subcategory_name="";
			if($pcat!=""){
				$pcat=substr($pcat, 9);
				if($pcat!=0){
					$parentcategory_name=$this->Model_search->get_parentcategory_name($pcat);
					$parentcategory_name=strtolower($parentcategory_name);
					$parentcategory_name = str_replace(' ', '-', $parentcategory_name);
				}
				else{
					$parentcategory_name="null";
				}
			}
			if($cat!=""){
				$cat=substr($cat, 9);
				$category_name=$this->Model_search->get_category_name($cat);
				$category_name=strtolower($category_name);
				$category_name = str_replace(' ', '-', $category_name);
			}
			if($subcat!=""){
				$subcat=substr($subcat, 9);
				$subcategory_name=$this->Model_search->get_subcategory_name($subcat);
				$subcategory_name=strtolower($subcategory_name);
				$subcategory_name = str_replace(' ', '-', $subcategory_name);
			}
			//redirect(site_url("catalog_combo/".$parentcategory_name));
			redirect(site_url("catalog_combo/".$parentcategory_name."/".$category_name."/".$subcategory_name));
		}
		else{
			
			//visitor_activity();
		$method=current_url();
		$this->session->set_userdata("products_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		
		}

		$search_keywords=$this->input->post('search_keywords');
		if($search_keywords!=''){
			$search_keyword_index_page=strtolower($search_keywords);
			$flag=$this->Model_search->dump_search_keywords($search_keywords);
			//echo $flag;
		}
			$search_keyword="";$search_cat="";$page_rows="";$pagenum="";
            $sorting_by="";$cat_current="";$cat_current_id="";$type_of_filter="";
            $color_filter="";$color_prev="";$size_filter="";$size_prev="";
            $price_filter="";$price_prev="";$brand_filter="";$brand_prev="";
			//print_r($this->input->post());exit;
			if(!$this->input->post()){
				
				if($pcat!="" && $cat!="" && $subcat!=""){
				   $pcat=substr($pcat, 9);
					$cat=substr($cat, 9);
					$subcat=substr($subcat, 9);
				   $level_type="subcat_id";
				   $level_value=$pcat."_".$cat."_".$subcat;
				   $level_name=$this->get_subcat_name($pcat,$cat,$subcat);
				}
				if($pcat!="" && $cat!="" && $subcat==""){
				   $pcat=substr($pcat, 9);
					$cat=substr($cat, 9);
					
				   $level_type="cat_id";
				   $level_value=$pcat."_".$cat;
				   $level_name=$this->get_cat_name($pcat,$cat);
				}
				if($pcat!="" && $cat=="" && $subcat==""){
				   $pcat=substr($pcat, 9);
					
					
				   $level_type="pcat_id";
				   $level_value=$pcat;
				   $level_name=$this->get_pcat_name($pcat);
				}
            }
            else{
			 	extract($this->input->post());
			  	$pcat=substr($pcat, 9);
				$cat=substr($cat, 9);
				$subcat=substr($subcat, 9);
            }
            
            $data['p_category']=$this->provide_first_level_menu();
        $data["controller"]=$this;
		
		
        $data["current_controller"]="search";
        $data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
        $data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
        
            if($level_type=="brand_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id=$level_value_arr[3];
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$brand_id;
            }
            if($level_type=="subcat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$subcat_id;
            }
			if($level_type=="cat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$cat_id;
            }
			if($level_type=="pcat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id="";
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$pcat_id;
            }
            if($level_type=="product_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id=$level_value_arr[3];
                $product_id=$level_value_arr[4];
                $inventory_id="";
                $data['cat_current_id']=$product_id;
               
            }
                $data["pcat_id"]=$pcat_id;
                $data['cat_id']=$cat_id;
                $data['subcat_id']=$subcat_id;
                $data['brand_id']=$brand_id;
                $data['product_id']=$product_id;
                $data['inventory_id']=$inventory_id;
                $data['searched_key']=$level_name;
                $data['level_type']=$level_type;
			$arr=array();
            $arr["pcat_id"]=$pcat_id;
            $arr['cat_id']=$cat_id;
            $arr['subcat_id']=$subcat_id;
            $arr['brand_id']=$brand_id;
            $arr['product_id']=$product_id;
             
            
            $data["category_tree"]=$arr;
            $data['level_value']=$level_value;
            $data['cat_current']=$level_type;
			$data["controller"]=$this;
            
            
			$data["current_controller"]="search";
			$data['search_keyword_index_page']="";
			if($this->input->post("search_keyword_index_page")!=""){
				$data['search_keyword_index_page']=$this->input->post("search_keyword_index_page");
			}
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			
			
			if($category_is_exists=="yes" && $subcategory_is_exists=="yes"){
				if($pcat!=""){
					$get_parent_cat_info_obj=$this->get_parent_cat_info($pcat);
				}
				if($cat!=""){
					$get_cat_info_obj=$this->get_cat_info($cat);
				}
				if($subcat!=""){
					$get_subcat_info_obj=$this->get_subcat_info($subcat);
				}
				
				if($pcat=="" || $cat=="" || $subcat==""){
					$this->load->view('error404',$data);
				}
				else if(preg_match("/[a-z]/i", $pcat) || preg_match("/[a-z]/i", $cat) || preg_match("/[a-z]/i", $subcat)){
					$this->load->view('error404',$data);
				}
				else if($pcat==0){
					if(empty($get_cat_info_obj) || empty($get_subcat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category_combo',$data);
					}
				}
				else if($pcat!=0){
					if(empty($get_parent_cat_info_obj) || empty($get_cat_info_obj) || empty($get_subcat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category_combo',$data);
					}
				}
				else{
					$this->load->view('search_category_combo',$data);
				}
			}
			else if($category_is_exists=="yes" && $subcategory_is_exists=="no"){
				
				if($pcat!=""){
					$get_parent_cat_info_obj=$this->get_parent_cat_info($pcat);
				}
				if($cat!=""){
					$get_cat_info_obj=$this->get_cat_info($cat);
				}
				
				if($pcat=="" || $cat==""){
					$this->load->view('error404',$data);
				}
				else if(preg_match("/[a-z]/i", $pcat) || preg_match("/[a-z]/i", $cat)){
					$this->load->view('error404',$data);
				}
				else if($pcat==0){
					if(empty($get_cat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category_combo',$data);
					}
				}
				else if($pcat!=0){
					if(empty($get_parent_cat_info_obj) || empty($get_cat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category_combo',$data);
					}
				}
				else{
					$this->load->view('search_category_combo',$data);
				}
			
			}
			else if($category_is_exists=="no" && $subcategory_is_exists=="no"){
				
				if($pcat!=""){
					$get_parent_cat_info_obj=$this->get_parent_cat_info($pcat);
				}
				if($pcat==""){
					$this->load->view('error404',$data);
				}
				else if(preg_match("/[a-z]/i", $pcat)){
					$this->load->view('error404',$data);
				}
				else if($pcat==0){
					$this->load->view('search_category_combo',$data);
				}
				else if($pcat!=0){
					if(empty($get_parent_cat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category_combo',$data);
					}
				}
				else{
					$this->load->view('search_category_combo',$data);
				}
			
			}
			$this->load->view('footer');
	}
	public function catalog($parentcategory_name='',$category_name="",$subcategory_name="",$jarvinai_inventory_id_list=""){
		////read_and_update_traffic();
		//visitor_activity_analytics();
		$pcat="";
		$cat="";
		$subcat="";
	
		if($parentcategory_name!="null" && $parentcategory_name!=''){
			$parentcategory_name = str_replace('-', ' ', $parentcategory_name);
			$pcat=$this->Model_search->get_pcat_id_by_parentcategory_name($parentcategory_name);
                        if($pcat==''){
                            $rand_temp=$this->sample_code();
                            $pcat=$rand_temp."0";
                        }else{
                            $rand_temp=$this->sample_code();
                            $pcat=$rand_temp.$pcat;
                        }
		}
		else{
			$rand_temp=$this->sample_code();
			$pcat=$rand_temp."0";
		}
	
                //echo $pcat;
		$Ncat_id='';
		
		if($category_name!=""){
			$category_name = str_replace('-', ' ', $category_name);
			$cat=$this->Model_search->get_cat_id_by_category_name($category_name,$parentcategory_name);
            $Ncat_id=$cat;
			$rand_temp=$this->sample_code();
			$cat=$rand_temp.$cat;
		}
		if($subcategory_name!="" && $Ncat_id!=""){
			$subcategory_name = str_replace('-', ' ', $subcategory_name);
			$subcat=$this->Model_search->get_subcat_id_by_subcategory_name($subcategory_name,$Ncat_id);
			$rand_temp=$this->sample_code();
			$subcat=$rand_temp.$subcat;
		}
		
		
			//visitor_activity();
		$method=current_url();
		$this->session->set_userdata("products_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data["jarvinai_inventory_id_list"]=$jarvinai_inventory_id_list;
		if(!empty($jarvinai_inventory_id_list)){
			$jarvinai_inventory_id_arr=explode("axdx",$jarvinai_inventory_id_list);
			$data["jarvinaioutput"]="jarvinaioutput";
			$data["jarvinaioutput_number_of_skus"]=count($jarvinai_inventory_id_arr);
		}
		else{
			$data["jarvinaioutput"]="";
			$data["jarvinaioutput_number_of_skus"]="1";
		}
		if($subcat!=''){
			$subcate_id=substr($subcat, 9);
			$inv_id=$this->Model_search->get_count_of_invs($subcate_id);
			
			if(!empty($inv_id)){
				$inv_count=count($inv_id);
				if($inv_count==1){
					$rand=$this->sample_code();
					$obj=reset($inv_id);
					$invent_rand_id=$rand.$obj->id;
					redirect(site_url("detail/".$invent_rand_id));
				}
			}
		}

		$search_keywords=$this->input->post('search_keywords');
		if($search_keywords!=''){
			$search_keyword_index_page=strtolower($search_keywords);
			$flag=$this->Model_search->dump_search_keywords($search_keywords);
			//echo $flag;
		}
			$search_keyword="";$search_cat="";$page_rows="";$pagenum="";
            $sorting_by="";$cat_current="";$cat_current_id="";$type_of_filter="";
            $color_filter="";$color_prev="";$size_filter="";$size_prev="";
            $price_filter="";$price_prev="";$brand_filter="";$brand_prev="";
			//print_r($this->input->post());exit;
			if(!$this->input->post()){
				if($pcat!="" && $cat!="" && $subcat!=""){
				   $pcat=substr($pcat, 9);
					$cat=substr($cat, 9);
					$subcat=substr($subcat, 9);
				   $level_type="subcat_id";
				   $level_value=$pcat."_".$cat."_".$subcat;
				   $level_name=$this->get_subcat_name($pcat,$cat,$subcat);
				}
				if($pcat!="" && $cat!="" && $subcat==""){
				   $pcat=substr($pcat, 9);
					$cat=substr($cat, 9);
					
				   $level_type="cat_id";
				   $level_value=$pcat."_".$cat;
				   $level_name=$this->get_cat_name($pcat,$cat);
				}
				if($pcat!="" && $cat=="" && $subcat==""){
				   $pcat=substr($pcat, 9);
					
					
				   $level_type="pcat_id";
				   $level_value=$pcat;
				   $level_name=$this->get_pcat_name($pcat);
				}
            }
            else{
			 extract($this->input->post());
            }
            
            $data['p_category']=$this->provide_first_level_menu();
        $data["controller"]=$this;
		
		
        $data["current_controller"]="search";
        $data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
        $data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
        
            if($level_type=="brand_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id=$level_value_arr[3];
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$brand_id;
            }
            if($level_type=="subcat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$subcat_id;
            }
			if($level_type=="cat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$cat_id;
            }
			if($level_type=="pcat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id="";
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$pcat_id;
            }
            if($level_type=="product_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id=$level_value_arr[3];
                $product_id=$level_value_arr[4];
                $inventory_id="";
                $data['cat_current_id']=$product_id;
               
            }
                $data["pcat_id"]=$pcat_id;
                $data['cat_id']=$cat_id;
                $data['subcat_id']=$subcat_id;
                $data['brand_id']=$brand_id;
                $data['product_id']=$product_id;
                $data['inventory_id']=$inventory_id;
                $data['searched_key']=$level_name;
                $data['level_type']=$level_type;
			$arr=array();
            $arr["pcat_id"]=$pcat_id;
            $arr['cat_id']=$cat_id;
            $arr['subcat_id']=$subcat_id;
            $arr['brand_id']=$brand_id;
            $arr['product_id']=$product_id;
             
            
            $data["category_tree"]=$arr;
            $data['level_value']=$level_value;
            $data['cat_current']=$level_type;
			$data["controller"]=$this;
            
            
			$data["current_controller"]="search";
			$data['search_keyword_index_page']="";
			if($this->input->post("search_keyword_index_page")!=""){
				$data['search_keyword_index_page']=$this->input->post("search_keyword_index_page");
			}
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('search_category',$data);
			$this->load->view('footer');	
	
	}


	/* new function */

	
	public function catalog_combo($parentcategory_name='',$category_name="",$subcategory_name=""){
		////read_and_update_traffic();
		//visitor_activity_analytics();
		$pcat="";
		$cat="";
		$subcat="";
	
		$adm_settings=get_admin_combo_settings();
		
		$data['adm_settings']=$adm_settings;

		if($parentcategory_name!="null" && $parentcategory_name!=''){
			$parentcategory_name = str_replace('-', ' ', $parentcategory_name);
			$pcat=$this->Model_search->get_pcat_id_by_parentcategory_name($parentcategory_name);
                        if($pcat==''){
                            $rand_temp=$this->sample_code();
                            $pcat=$rand_temp."0";
                        }else{
                            $rand_temp=$this->sample_code();
                            $pcat=$rand_temp.$pcat;
                        }
		}
		else{
			$rand_temp=$this->sample_code();
			$pcat="";
		}
	
                //echo $pcat;
		$Ncat_id='';
		
		if($category_name!=""){
			$category_name = str_replace('-', ' ', $category_name);
			$cat=$this->Model_search->get_cat_id_by_category_name($category_name,$parentcategory_name);
            $Ncat_id=$cat;
			$rand_temp=$this->sample_code();
			$cat=$rand_temp.$cat;
		}
		if($subcategory_name!="" && $Ncat_id!=""){
			$subcategory_name = str_replace('-', ' ', $subcategory_name);
			$subcat=$this->Model_search->get_subcat_id_by_subcategory_name($subcategory_name,$Ncat_id);
			$rand_temp=$this->sample_code();
			$subcat=$rand_temp.$subcat;
		}
		
		
			//visitor_activity();
		$method=current_url();
		$this->session->set_userdata("products_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		if($subcat!=''){
			$subcate_id=substr($subcat, 9);
			$inv_id=$this->Model_search->get_count_of_invs($subcate_id);
			
			if(!empty($inv_id)){
				$inv_count=count($inv_id);
				if($inv_count==1){
					$rand=$this->sample_code();
					$obj=reset($inv_id);
					$invent_rand_id=$rand.$obj->id;
					redirect(site_url("detail/".$invent_rand_id));
				}
			}
		}

		$search_keywords=$this->input->post('search_keywords');
		if($search_keywords!=''){
			$search_keyword_index_page=strtolower($search_keywords);
			$flag=$this->Model_search->dump_search_keywords($search_keywords);
			//echo $flag;
		}
			$search_keyword="";$search_cat="";$page_rows="";$pagenum="";
            $sorting_by="";$cat_current="";$cat_current_id="";$type_of_filter="";
            $color_filter="";$color_prev="";$size_filter="";$size_prev="";
            $price_filter="";$price_prev="";$brand_filter="";$brand_prev="";
			//print_r($this->input->post());exit;
			if(!$this->input->post()){



				if($pcat!="" && $cat!="" && $subcat!=""){
				   $pcat=substr($pcat, 9);
					$cat=substr($cat, 9);
					$subcat=substr($subcat, 9);
				   $level_type="subcat_id";
				   $level_value=$pcat."_".$cat."_".$subcat;
				   $level_name=$this->get_subcat_name($pcat,$cat,$subcat);
				}
				if($pcat!="" && $cat!="" && $subcat==""){
				   $pcat=substr($pcat, 9);
					$cat=substr($cat, 9);
					
				   $level_type="cat_id";
				   $level_value=$pcat."_".$cat;
				   $level_name=$this->get_cat_name($pcat,$cat);
				}
				if($pcat!="" && $cat=="" && $subcat==""){
				   $pcat=substr($pcat, 9);
					
					
				   $level_type="pcat_id";
				   $level_value=$pcat;
				   $level_name=$this->get_pcat_name($pcat);
				}

				if($pcat=="" && $cat=="" && $subcat==""){
					$level_type="all";
					$level_value="";
					$level_name="";
				}

            }
            else{
			 extract($this->input->post());
            }
            
            $data['p_category']=$this->provide_first_level_menu();
        $data["controller"]=$this;
		
		
        $data["current_controller"]="search";
        $data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
        $data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
        
            if($level_type=="brand_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id=$level_value_arr[3];
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$brand_id;
            }
            if($level_type=="subcat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$subcat_id;
            }
			if($level_type=="cat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$cat_id;
            }
			if($level_type=="pcat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id="";
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$pcat_id;
            }
            if($level_type=="product_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id=$level_value_arr[3];
                $product_id=$level_value_arr[4];
                $inventory_id="";
                $data['cat_current_id']=$product_id;
               
            }
			if($level_type=="all"){
                $level_value_arr=array();
                $pcat_id="";
                $cat_id="";
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']="";
            }
                $data["pcat_id"]=$pcat_id;
                $data['cat_id']=$cat_id;
                $data['subcat_id']=$subcat_id;
                $data['brand_id']=$brand_id;
                $data['product_id']=$product_id;
                $data['inventory_id']=$inventory_id;
                $data['searched_key']=$level_name;
                $data['level_type']=$level_type;
			$arr=array();
            $arr["pcat_id"]=$pcat_id;
            $arr['cat_id']=$cat_id;
            $arr['subcat_id']=$subcat_id;
            $arr['brand_id']=$brand_id;
            $arr['product_id']=$product_id;
             
            
            $data["category_tree"]=$arr;
            $data['level_value']=$level_value;
            $data['cat_current']=$level_type;
			$data["controller"]=$this;
            
            
			$data["current_controller"]="search";
			$data['search_keyword_index_page']="";
			if($this->input->post("search_keyword_index_page")!=""){
				$data['search_keyword_index_page']=$this->input->post("search_keyword_index_page");
			}
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('search_category_combo',$data);
			$this->load->view('footer');	
	
	}
	/* new function */
	
	
	
	public function get_parent_cat_info($pcat_id){
		$data["controller"]=$this;
		$parent_cat_infoObj=$this->Model_search->get_parent_cat_info($pcat_id);
		return $parent_cat_infoObj;
	}
	
	public function get_parent_cat_info_all_combo($pcat_id){
		$data["controller"]=$this;
		$parent_cat_infoObj=$this->Model_search->get_parent_cat_info_all_combo($pcat_id);
		return $parent_cat_infoObj;
	}
	
	public function get_cat_info($cat_id){
		$data["controller"]=$this;
		$cat_infoObj=$this->Model_search->get_cat_info($cat_id);
		return $cat_infoObj;
	}
	
	public function get_subcat_info($subcat_id){
		$data["controller"]=$this;
		$subcat_infoObj=$this->Model_search->get_subcat_info($subcat_id);
		return $subcat_infoObj;
	}
	
	public function get_brand_info($brand_id){
		$data["controller"]=$this;
		$brand_infoObj=$this->Model_search->get_brand_info($brand_id);
		return $brand_infoObj;
	}
	public function get_product_info($product_id){
		$data["controller"]=$this;
		$product_infoObj=$this->Model_search->get_product_info($product_id);
		return $product_infoObj;
	}
	public function get_inventory_info($inventory_id){
		$data["controller"]=$this;
		$inventory_infoObj=$this->Model_search->get_inventory_info($inventory_id);
		return $inventory_infoObj;
	}
	
	public function get_filter_box_info($cat_current,$cat_current_id,$category_tree){
		$data["controller"]=$this;
		$filter_box_infoRes=$this->Model_search->get_filter_box_info($cat_current,$cat_current_id,$category_tree);
		return $filter_box_infoRes;
	}
	public function get_filter_id_by_filter_name($colorFilterName){
		$filter_box_infoRes=$this->Model_search->get_filter_id_by_filter_name($colorFilterName);
		return $filter_box_infoRes;
	}
	
	public function get_filter_info($filterbox_id){
		$data["controller"]=$this;
		$filter_infoRes=$this->Model_search->get_filter_info($filterbox_id);
		return $filter_infoRes;
	}
	public function get_filter_info_by_filterboxname($filterbox_name){
		$data["controller"]=$this;
		$filter_infoRes=$this->Model_search->get_filter_info_by_filterboxname($filterbox_name);
		return $filter_infoRes;
	}
	
	public function get_filter_availability_for_products($cat_current,$cat_current_id,$filter_id,$filter_prod_arr,$filter_prod_arr_with_filter_arr,$type_filter,$filter_values){
		$data["controller"]=$this;
		$count_filter_id_for_products=$this->Model_search->get_filter_availability_for_products($cat_current,$cat_current_id,$filter_id,$filter_prod_arr,$filter_prod_arr_with_filter_arr,$type_filter,$filter_values);
		return $count_filter_id_for_products;
	}
	public function get_filter_id_constant_by_type($type_filter,$filter_prod_arr){
		$filter_id_constant_arr=$this->Model_search->get_filter_id_constant_by_type($type_filter,$filter_prod_arr);
		return $filter_id_constant_arr;
	}
	
	public function get_all_tree_of_products($p_id)
	{
		$data=$this->Customer_frontend->get_all_tree_of_products($p_id);
		
		return $data;
	}
	public function get_all_tree_of_subcat($subcat_id)
	{
		$data=$this->Customer_frontend->get_all_tree_of_subcat($subcat_id);
		
		return $data;
	}
	public function category_search($type){
		$rand_1=$this->sample_code();
		$rand_2=$this->sample_code();
		if($type=="men"){
			$this->search_category($rand_1."1",$rand_2."1");
		}
		if($type=="women"){
			$this->search_category($rand_1."1",$rand_2."2");
		}
		if($type=="kids"){
			$this->search_category($rand_1."1",$rand_2."3");
		}
	}
	
	public function search_keywords(){
		
		$term=$_GET['term'];
		$product_name_arr=array();
		$product_name_arr=$this->Model_search->search_keywords($term);
		
		sort($product_name_arr);
		echo json_encode($product_name_arr);
	}
	public function get_filterbox_units($filterbox_name){
		$filterbox_units=$this->Model_search->get_filterbox_units($filterbox_name);
		return $filterbox_units;
	}
	public function testing_search_index()
	{
		require_once("assets/elastic/app/init_customer.php");
		$query_inv=$this->Model_search->testing_search_inv();
		$combo_pack_discount=0;
		$adm_settings=get_admin_combo_settings();
		
		if(!empty($adm_settings)){
			$combo_pack_min_items=$adm_settings->adm_combo_pack_min_items;
			$combo_pack_discount=$adm_settings->adm_combo_pack_discount;
			$combo_pack_shipping_charge=$adm_settings->adm_combo_pack_shipping_charge;
			$combo_pack_status=$adm_settings->adm_combo_pack_status;
		}
		foreach($query_inv as $res){
			if($res->pcat_id!=0){
				$pcat_idObj=$this->get_parent_cat_info($res->pcat_id);
				if(empty($pcat_idObj)){
					continue;
				}
				$res->pcat_name=(isset($pcat_idObj->pcat_name)) ? $pcat_idObj->pcat_name : '';
			}
			else{
				$res->pcat_name="";
			}
			$cat_idObj=$this->get_cat_info($res->cat_id);
			
			if(empty($cat_idObj)){
				continue;
			}
			
			$res->cat_name=$cat_idObj->cat_name;
			
			$subcat_idObj=$this->get_subcat_info($res->subcat_id);
			if(empty($subcat_idObj)){
				continue;
			}
			$res->subcat_name=$subcat_idObj->subcat_name;
			
			$brand_idObj=$this->get_brand_info($res->brand_id);
			
			if(empty($brand_idObj)){
				continue;
			}
			
			
			$res->brand_name=$brand_idObj->brand_name;

			if($combo_pack_discount>0){
				$combo_sel_price=round($res->max_selling_price-($res->max_selling_price*$combo_pack_discount/100));
			}else{
				$combo_sel_price= $searchRes["_source"]["max_selling_price"];
			}

			$parambody_arr=array();
			$parambody_arr=array("inventory_id"=>$res->inventory_id,"sku_id"=>$res->sku_id,"sku_name"=>$res->sku_name,"product_id"=>$res->product_id,"product_name"=>$res->product_name,"product_description"=>$res->product_description,"pcat_id"=>$res->pcat_id,"cat_id"=>$res->cat_id,"subcat_id"=>$res->subcat_id,"filter_brand_id"=>$res->brand_id,"brand_id"=>$res->brand_id,"image"=>$res->image,"thumbnail"=>$res->thumbnail,"tax_percent"=>$res->tax_percent,"base_price"=>$res->base_price,"selling_price"=>$res->selling_price,"max_selling_price"=>$res->max_selling_price,"selling_discount"=>$res->selling_discount,"combo_sel_price"=>$combo_sel_price,"pcat_name"=>$res->pcat_name,"cat_name"=>$res->cat_name,"subcat_name"=>$res->subcat_name,"brand_name"=>$res->brand_name,"all_cat"=>0,"common_image"=>$res->common_image);
			
			
			$query_filters=$this->Model_search->get_filter_id_new($res->inventory_id);
			foreach($query_filters as $res_filters){
				$filterbox_name=$this->Model_search->get_filterbox_name($res_filters->filter_id);
				if($filterbox_name!=""){
					$filterbox_name = str_replace(' ', '_', $filterbox_name);
					
					$filterbox_type=$this->Model_search->get_filterbox_type($res_filters->filter_id);
					$filter_options=$this->Model_search->get_filter_options($res_filters->filter_id);
					$parambody_arr["filter_".$filterbox_name."_type"]=$filterbox_type;
					$parambody_arr["filter_".$filterbox_name."_filter_options"]=$filter_options;
					$parambody_arr["filter_".$filterbox_name."_id"]=$res_filters->filter_id;
				}
			}
			
			if($res->subcat_id==1){
				$parambody_arr["sugg1"]=$res->subcat_name." for men";
				$parambody_arr["sugg2"]="men's ".$res->subcat_name;
			}
			if($res->subcat_id==2){
				$parambody_arr["sugg1"]=$res->subcat_name." for women";
				$parambody_arr["sugg2"]="women's ".$res->subcat_name;
			}
			if($res->subcat_id==3){
				$parambody_arr["sugg1"]=$res->subcat_name." for kids";
				$parambody_arr["sugg2"]="kid's ".$res->subcat_name;
			}
			$parambody_arr["generalsugg1"]=$res->subcat_name." in All departments";
			$parambody_arr["generalsugg2"]=$res->subcat_name;
			$parambody_arr["generalsugg3"]=$res->cat_name;
			$parambody_arr["generalsugg4"]=$res->pcat_name;

			$params=array();
			$params["index"]="flamingo1";
			$params["type"]="flamingoproducts1";
			$params["id"]=$res->inventory_id;
			
			
			
			$params["body"]=$parambody_arr;
			
			if($res->inventory_type!=2){
				print_r($params["body"]);
				$es->index($params);
			}
			
			
			
		}
		
	}
	   public function testing_pcat_Cat(){
		exit;
        require_once("assets/elastic/app/init_customer.php");
        $query_inv=$this->Model_search->testing_pcat_Cat();
        foreach($query_inv as $res){
            $parambody_arr["pcat_name"]=$res->pcat_name;
            $parambody_arr["pcat_id"]=$res->pcat_id;
            $parambody_arr["subcat_name"]=$res->subcat_name;
            $parambody_arr["subcat_id"]=$res->subcat_id;
            $parambody_arr["cat_name"]=$res->cat_name;
            $parambody_arr["cat_type"]="flamingoCategory";

            $params=array();
            $params["index"]="flamingo1";
            $params["type"]="flamingoCategory";//
            $params["id"]=$res->subcat_id;
            $params["body"]=$parambody_arr;
            
            echo "<pre>";
            //print_r($params["body"]);
			
           $res= $es->index($params);
		   print_r( $res);
		   echo "</pre>";
        }
    }

    public function testing_cat_subCat(){
		exit;
        require_once("assets/elastic/app/init_customer.php");
        $query_inv=$this->Model_search->testing_cat_subCat();
        foreach($query_inv as $res){
            $parambody_arr["subcat_name"]=$res->subcat_name;
            $parambody_arr["subcat_id"]=$res->subcat_id;
            $parambody_arr["cat_id"]=$res->cat_id;
            $parambody_arr["pcat_id"]=$res->pcat_id;
            $parambody_arr["cat_name"]=$res->cat_name;
            //$parambody_arr["brand_name"]=$res->brand_name;
            $parambody_arr["brand_id"]=$res->brand_id;
            $parambody_arr["product_id"]=$res->product_id;
            $parambody_arr["product_name"]=$res->product_name;
            $parambody_arr["cat_type"]="flamingoSubCategory";
            
            $params=array();
            $params["index"]="flamingo1";
            $params["type"]="flamingoSubCategory";

            $params["id"]=$res->product_id;
            
            
            
            $params["body"]=$parambody_arr;
            
            echo "<pre>";
            print_r($params["body"]);
			//echo "</pre>";
            $res=$es->index($params);
			print_r($res);
			echo "</pre>";
        }
    }
	
	public function testing_search()
	{
		require_once("assets/elastic/app/init_customer.php");
		
		$params["index"]="flamingo1";
		$params["type"]="flamingoproducts1";
		$params["from"]=0;
		$params["size"]=10000;
		$res=$es->search($params);
		echo "<pre>";
		echo count($res["hits"]["hits"]);
		print_r($res["hits"]["hits"]);
		echo "</pre>";
	}
	
	public function testing_search_delete_run1(){
		$this->load->view('testing_search_delete_run1');
	}
	public function testing_search_delete()
	{
		require_once("assets/elastic/app/init_customer.php");
		
		$params["index"]="flamingo1";
		$params["type"]="flamingoproducts1";
		//$params["from"]=0;
		//$params["size"]=10000;
		$res=$es->search($params);

		foreach($res["hits"]["hits"] as $v){
			$params["index"]="flamingo1";
			$params["type"]="flamingoproducts1";
			$params["id"]=$v["_id"];
			$es->delete($params);
		}
		
		$params["index"]="flamingo1";
		$params["type"]="flamingoproducts1";
		//$params["from"]=0;
		//$params["size"]=10000;
		$res=$es->search($params);
		foreach($res["hits"]["hits"] as $v){
			$params["index"]="flamingo1";
			$params["type"]="flamingoproducts1";
			$params["id"]=$v["_id"];
			$es->delete($params);
		}
		$params["index"]="flamingo1";
		$params["type"]="flamingoproducts1";
		//$params["from"]=0;
		//$params["size"]=10000;
		$res=$es->search($params);
		foreach($res["hits"]["hits"] as $v){
			$params["index"]="flamingo1";
			$params["type"]="flamingoproducts1";
			$params["id"]=$v["_id"];
			$es->delete($params);
		}
	}
	
	public function testing_search_test()
	{
		require_once("assets/elastic/app/init_customer.php");
		
		$params["index"]="flamingo1";
		$params["type"]="flamingoproducts1";
		$params["from"]=0;
		$params["size"]=10000;
		$res=$es->search($params);
		print_r($res["hits"]["hits"]);
		exit;
		$query=$this->Model_search->testing_search();
		foreach($query as $res){
			$pcat_idObj=$this->get_parent_cat_info($res->pcat_id);
			$res->pcat_name=$pcat_idObj->pcat_name;
			
			$cat_idObj=$this->get_cat_info($res->cat_id);
			$res->cat_name=$cat_idObj->cat_name;
			
			$subcat_idObj=$this->get_subcat_info($res->subcat_id);
			$res->subcat_name=$subcat_idObj->subcat_name;
			
			$brand_idObj=$this->get_brand_info($res->brand_id);
			$res->brand_name=$brand_idObj->brand_name;
			$params["index"]="flamingo1";
			$params["type"]="flamingoproducts1";
			$params["id"]=$res->product_id;
			$params["body"]=array("product_id"=>$res->product_id,"product_name"=>$res->product_name,"product_description"=>$res->product_description,"pcat_id"=>$res->pcat_id,"cat_id"=>$res->cat_id,"subcat_id"=>$res->subcat_id,"brand_id"=>$res->brand_id,"image"=>$res->image,"thumbnail"=>$res->thumbnail,"selling_price"=>$res->selling_price,"pcat_name"=>$res->pcat_name,"cat_name"=>$res->cat_name,"subcat_name"=>$res->subcat_name,"brand_name"=>$res->brand_name);
			
		}
		
		
		$params["index"]="flamingo1";
		$params["type"]="flamingoproducts1";
			
		$res=$es->search($params);
		
		
		echo '<pre>';
		print_r($res);
		echo '</pre>';
	
	}
	
	public function testing()
	{
		require_once("assets/elastic/app/init_customer.php");
		
		$params['index'] = 'flamingo1';
		$params['type']  = 'flamingoproducts1';
		$params["body"]["query"]["filtered"]["filter"]["bool"]["must"][]["term"]["pcat_id"]=1;
		
		
			$color_arr=array(9);
			$color_filter_param_arr=array();
			foreach($color_arr as $colorId){
				$color_filter_param_arr[]["term"]["filter_color_id"]=$colorId;
			}
			$params["body"]["query"]["filtered"]["filter"]["bool"]["must"][]["bool"]["should"]=$color_filter_param_arr;
			
		
			$size_arr=array(15);
			$size_filter_param_arr=array();
			foreach($size_arr as $sizeId){
				$size_filter_param_arr[]["term"]["filter_size_id"]=$sizeId;
			}
			$params["body"]["query"]["filtered"]["filter"]["bool"]["must"][]["bool"]["should"]=$size_filter_param_arr;
	
		//print_r($params);
		$params["from"]=0;
		$params["size"]=10000;
		$query = $es->search($params);
		echo (count($query["hits"]["hits"]));
		print_r($query["hits"]["hits"]);
	}
	public function get_inventory_product_info_by_inventory_id($inventory_id){
		$inventory_info=$this->Model_search->get_inventory_product_info_by_inventory_id($inventory_id);
		return $inventory_info;
	}
	public function get_inventory_product_info_by_inventory_id_json(){
		$inv_data = json_decode(file_get_contents("php://input"));
		$inventory_id=$inv_data->inventory_id;
		$inventory_info=$this->Model_search->get_inventory_product_info_by_inventory_id($inventory_id);
		echo json_encode($inventory_info);
	}
	public function get_product_filter_attributes_1_values($product_id){
		$product_filter_attributes_1_values_res=$this->Model_search->get_product_filter_attributes_1_values($product_id);
		return $product_filter_attributes_1_values_res;
	}
	public function get_product_filter_attributes($product_id){
		$att_total_arr=$this->Model_search->get_product_filter_attributes($product_id);
		return $att_total_arr;
	}
	public function get_attribute_value_exists_or_not($att_total_key,$inventory_id){
		$status=$this->Model_search->get_attribute_value_exists_or_not($att_total_key,$inventory_id);
		return $status;
	}
	
	public function get_available_remaining_attributes($attribute_n_value,$attribute_value,$product_id,$val1=''){
		$available_remaining_attributes=$this->Model_search->get_available_remaining_attributes($attribute_n_value,$attribute_value,$product_id,$val1);
		return $available_remaining_attributes;
	}
	public function get_inventory_id_by_series_filter(){
		$inventory_id=$this->Model_search->get_inventory_id_by_series_filter($this->input->post());
		echo $inventory_id;
	}
	public function get_availability_of_colors_based_on_remaining_attributes($available_remaining_attributes_for_color_arr){
		$availability_of_colors_based_on_remaining_attributes_result_arr=$this->Model_search->get_availability_of_colors_based_on_remaining_attributes($available_remaining_attributes_for_color_arr);
		$attribute_color_value_arr=[];
		foreach($availability_of_colors_based_on_remaining_attributes_result_arr as $arr){
			$attribute_color_value_arr[]=strtolower($arr["attribute_1_value"]);
		}
		return $attribute_color_value_arr;
	}
	//added 7_8
	
	public function get_specification_group_by_category($all_ids_arr)
	{
		
		$allowed_specification_group_arr=$this->Model_search->get_specification_group_by_category($all_ids_arr);
		return $allowed_specification_group_arr;
	}
	public function get_specification_groups($allowed_specification_group_arr)
	{
		$specification_groups=$this->Model_search->get_specification_groups($allowed_specification_group_arr);
		return $specification_groups;
	}
	
	public function get_specification_values($specification_group_id,$inventory_id){
		$specification_values_arr=$this->Model_search->get_specification_values($specification_group_id,$inventory_id);
		return $specification_values_arr;
	}
	
	public function check_wishlist($inventory_id){
		
		$count_for_wishlist=$this->Model_search->check_wishlist($inventory_id);
		return $count_for_wishlist;
	}
	public function add_to_wishlist(){
		$inventory_id=$this->input->post("inventory_id");
		$count_for_wishlist=$this->check_wishlist($inventory_id);
		if($count_for_wishlist>0){
			$delete_flag=$this->Model_search->delete_wishlist($inventory_id);
			if($delete_flag){
				$arr=array("exists"=>"yes");
			}
			else{
				$arr=array("exists"=>"NA");
			}
		}
		else{
			$info_for_wishlistObj=$this->Model_search->get_info_for_wishlist($inventory_id);
			$res=$this->Model_search->add_to_wishlist($inventory_id,addslashes($info_for_wishlistObj->product_name),$info_for_wishlistObj->common_image,$info_for_wishlistObj->selling_price,$info_for_wishlistObj->product_status);
			if($res){
				$arr=array("exists"=>"no");
			}
			else{
				$arr=array("exists"=>"NA");
			}
		}
		echo json_encode($arr);
	}
		
		///adddeddd///////ratings/////
	public function get_average_value_of_ratings($inventory_id){
		$avg_value=$this->Model_search->get_average_value_of_ratings($inventory_id);
		return $avg_value;
	}
	public function number_of_ratings($inventory_id){
		$rate_number=$this->Model_search->number_of_ratings($inventory_id);
		return $rate_number;
	}
	public function number_of_reviews($inventory_id){
		$review_number=$this->Model_search->number_of_reviews($inventory_id);
		return $review_number;
	}
	public function get_data_for_reviews($inventory_id){
		$reviews=$this->Model_search->get_data_for_reviews($inventory_id);
		return $reviews;
	}
	public function write_review(){	
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="search";

		if($this->input->post("inv_id")!=''){
			if($this->input->post("rate_order")!=null && $this->input->post("rate_order")!=''){
				$data["order_item_id"]=$this->input->post("rate_order_order_item_id");
			}else{
				$data["order_item_id"]=0;
			}
		$data["inventory_id"]=$this->input->post("inv_id");
		$data["product_name"]=$this->input->post("pro_name");
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('write_review_view');
		$this->load->view('footer');
		}else{

			redirect(site_url("Search"));
		
		}
	}
	public function get_data_from_rated_inventory($inventory_id,$customer_id){
		return $this->Model_search->get_data_from_rated_inventory($inventory_id,$customer_id);
	}
	public function check_review_exists(){
		if($this->input->post("inventory_id")!=''){
			$inventory_id=$this->input->post("inventory_id");
			$customer_id=$this->input->post("customer_id");

			$order_item_arr=$this->Model_search->get_order_item_id($inventory_id,$customer_id);
			
			if(!empty($order_item_arr)){
				$order_item_id=$order_item_arr["order_item_id"];
				$exist=$this->Model_search->check_review_exists($inventory_id,$customer_id,$order_item_id);
	
			}else{
				$exist="no_access";
			}
			
			echo $exist;
		}
	}
	public function get_order_item_id($inventory_id,$customer_id){
		$order_item_arr=$this->Model_search->get_order_item_id($inventory_id,$customer_id);
		return $order_item_arr;
	}
	public function rated_inventory(){
		if($this->input->post("inventory_id")!=''){
			
		$data= array(
		'review_title' => $this->input->post('review_title'),	
		'customer_id' => $this->input->post('customer_id'),
		'customer_name' => $this->input->post('customer_name'),
		'review' => $this->input->post('review'),
		'rating' => $this->input->post('rate'),
		'inventory_id' => $this->input->post('inventory_id'),
		'order_item_id' => $this->input->post('order_item_id'),
		'purchased_timestamp' => $this->input->post('purchased_timestamp'),
		'verified_customer'=>1,
		'status'=>'2'//pending
		);
		$go_to_myorder=0;
		$inventory_id=$this->input->post("inventory_id");
		$customer_id=$this->input->post("customer_id");
		
		if($this->input->post("order_item_id")!=null){
			$data['order_item_id']=$this->input->post('order_item_id');
			$order_item_id=$data['order_item_id'];
			$go_to_myorder=1;
			$exist=$this->Model_search->check_review_exists($inventory_id,$customer_id,$order_item_id);
		}else{
			$order_item_arr=$this->Model_search->get_order_item_id($inventory_id,$customer_id);
			
			if(!empty($order_item_arr)){

				$order_item_id=$order_item_arr["order_item_id"];		
				$exist=$this->Model_search->check_review_exists($inventory_id,$customer_id,$order_item_id);
			}else{
				$exist="no_access";
			}
		}

			$rand_1=$this->sample_code();
			$invent_rand_id=$rand_1.$inventory_id;

			if($exist!="no_access"){
				
				if($exist=="yes"){
					$reviews=$this->Model_search->update_rated_inventory($data,$inventory_id,$customer_id);
				}else{
					$reviews=$this->Model_search->rated_inventory($data);
				}
			}else{
				$reviews=0;
			}

			if($reviews==true){
				$previous_page = $this->session->userdata('cur_page');
				if($go_to_myorder==1){
					echo site_url('Account/my_order');	
				}else{
					if(isset($previous_page) && $previous_page != ''){
						echo site_url("detail/".$invent_rand_id);	
					}else{
						echo site_url("detail/".$invent_rand_id);
					}
				}
				
			}else{
				echo $exist;
			}
		
		}else{
			$previous_page = $this->session->userdata('previous_page');
			if(isset($previous_page) && $previous_page != ''){
				redirect(site_url($previous_page));
			} 
		}
	}
	public function add_to_ratedorder(){
		
		if($this->input->post("inventory_id")!=''){
		$data= array(
		'review_title' => $this->input->post('review_title'),	
		'customer_id' => $this->input->post('customer_id'),
		'order_item_id' => $this->input->post('order_item_id'),
		'inventory_id' => $this->input->post('inventory_id'),
		'message' => $this->input->post('review'),
		'rating' => $this->input->post('rate'),
		'feedback_status'=>'2'//pending
		);
		
		$order_item_id=$this->input->post("order_item_id");
		$customer_id=$this->input->post("customer_id");
		
		$exist=$this->Model_search->check_review_exists_in_rateorder($order_item_id,$customer_id);
		
		
			if($exist=="yes"){
				$reviews=$this->Model_search->update_ratedorder($data,$order_item_id,$customer_id);
			}else{
				$reviews=$this->Model_search->add_to_ratedorder($data);
			}

			if($reviews==true){
				echo site_url('Account/my_order');
			}
		
		}else{
			$this->session->set_flashdata('notification',"sorry..! you can't review the product");
			echo site_url('Account/my_order');
		}
	}
	
	public function show_all_reviews($inventory_id,$pageno=1,$rating_value=0,$rating=''){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$inventory_id=substr($inventory_id, 9);
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="search";
		$data["rating_value"]=$rating_value;
		$data["inventory_id"]=$inventory_id;
		$data["product_name"]=$this->Model_search->get_product_name_by_inventory_id($inventory_id);
		$records_per_page=5;
		$data["total_reviews"]=$this->Model_search->get_total_reviews($inventory_id);
		$data["pageno"]=$pageno;
		$data["total_rate_count"]=$this->Model_search->get_total_rate_count($inventory_id);
        $data["total_review_count"]=$this->Model_search->get_total_review_count($inventory_id);
		$data["total_pages"]=$this->Model_search->total_show_all_reviews($inventory_id,$records_per_page,$rating_value);
		$data["all_reviews"]=$this->Model_search->show_all_reviews($inventory_id,$records_per_page,$pageno,$rating_value);
		$this->session->unset_userdata("cur_page");
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('reviews',$data);
		$this->load->view('footer');
	}
	public function number_ratings_one_to_five($inventory_id){
		$number_ratings_one_to_five=$this->Model_search->number_ratings_one_to_five($inventory_id);
		return $number_ratings_one_to_five;
	}
	////added for shipping charge calculation//////
	
	public function get_territory_of_pincode($pincode,$vendor_id,$logistics_id,$territory_name_id){
		$default_logistics_arr=$this->Model_search->get_default_logistics($vendor_id,$logistics_id);
		if(isset($default_logistics_arr)){
			$default_logistics_id=$default_logistics_arr["logistics_id"];
			$logistics_name=$default_logistics_arr["logistics_name"];
			$logistics_weblink=$default_logistics_arr["logistics_weblink"];
			$default_delivery_mode_arr=$this->Model_search->default_delivery_mode($logistics_id);
			
				
			/** getting info of pincode like officename and deliverytype etc starts */
			$imp_json_info_arr=$this->Model_search->get_imp_json_info_of_pincode($pincode);
			$office_name=$imp_json_info_arr["office_name"];
			$deliverytype=$imp_json_info_arr["deliverytype"];
			/** getting info of pincode like officename and deliverytype etc ends */
			
			
			
			
			$logistics_service_arr=$this->Model_search->get_logistics_service_details($vendor_id,$default_logistics_id,$pincode,$territory_name_id);
			$logistics_service_id=$logistics_service_arr["logistics_service_id"];
			$city=$logistics_service_arr["city"];
			$default_delivery_mode_id=$default_delivery_mode_arr['logistics_delivery_mode_id'];
			$default_delivery_mode=$default_delivery_mode_arr['delivery_mode'];
			
			$speed_duration_json=$default_delivery_mode_arr['speed_duration'];
		
			
			
			$shipping_detail=array('logistics_territory_id'=>$territory_name_id,'logistics'=>$default_logistics_id,'default_delivery_mode_id'=>$default_delivery_mode_id,'default_delivery_mode'=>$default_delivery_mode,'logistics_name'=>$logistics_name,'city'=>$city,'office_name'=>$office_name,'deliverytype'=>$deliverytype,"speed_duration"=>$speed_duration_json,'logistics_weblink'=>$logistics_weblink);
		}
		else{
			/* else condition newly added for packsbazaar 5/8/2021 starts */
			$shipping_detail=array('logistics_territory_id'=>$territory_name_id,'logistics'=>$logistics_id,'default_delivery_mode_id'=>0,'default_delivery_mode'=>"",'logistics_name'=>"",'city'=>"",'office_name'=>"",'deliverytype'=>"","speed_duration"=>"",'logistics_weblink'=>"");
			/* else condition newly added for packsbazaar 5/8/2021 ends */
		}
		return $shipping_detail;
	}
	public function get_parcel_category_of_inventory($inventory_id){
		$parcel_category=$this->Model_search->get_parcel_category_of_inventory($inventory_id);
	}
	public function get_default_parcel_category($logistics_id){
		$default_parcel_category=$this->Model_search->get_default_parcel_category($logistics_id);
		return $default_parcel_category;
	}
	public function get_logistics_inventory_data($inventory_id){
		$logistics_inventory=$this->Model_search->get_logistics_inventory_data($inventory_id);
		return $logistics_inventory; 
	}
	public function available_parcel_category_of_inventory($inventory_id,$parcel_cat_arr){
		$available_parcel_category=$this->Model_search->available_parcel_category_of_inventory($inventory_id,$parcel_cat_arr);
		return $available_parcel_category;
	}
	public function get_logistics_price($current_quantity,$inventory_id,$logistics_id,$inventory_weight_in_kg,$inventory_volume_in_kg,$logistics_territory_id="",$default_delivery_mode_id="",$logistics_parcel_category_id=""){
		$logistics_price_resultant_inventory_volume_in_kg_arr=$this->Model_search->get_logistics_price($current_quantity,$inventory_id,$logistics_id,$inventory_weight_in_kg,$inventory_volume_in_kg,$logistics_territory_id,$default_delivery_mode_id,$logistics_parcel_category_id);
		return $logistics_price_resultant_inventory_volume_in_kg_arr;
	}
	
	public function get_default_shipping_charge($inventory_id,$pincode,$logistics_territory_id,$logistics_id,$default_delivery_mode_id,$logistics_parcel_category_id,$logistics_price,$speed_territory_duration){
		
		//echo $logistics_price;
		
		$logistics_territory_name=$this->get_territory_name_by_territory_id($logistics_territory_id);
		
		$free_shipping_arr=$this->get_free_shipping_territories_by_inventory_id($inventory_id);
		

		$territory_charge=$logistics_price;
		$speed_territory_duration_arr=json_decode($speed_territory_duration,true);
		
		$territory_duration_org=$speed_territory_duration_arr[$logistics_territory_id];
		
		// if the number of days added to current date is sunday come then add 1 more day
		if(Date('w', strtotime('+'.$territory_duration_org.' days'))==0){
			$territory_duration_org=$territory_duration_org+1;
		}

		$territory_duration = strtotime("+".$territory_duration_org." day");
		
		if($free_shipping_arr!=false){	

			if(in_array($logistics_territory_name,$free_shipping_arr)) {			
			$shipping_charge=0;	
			}
			else{
				//$shipping_charge=0;
				$shipping_charge=$territory_charge;
				//$shipping_charge=$logistics_price;
			}
		}else{
			
			 $shipping_charge=$territory_charge;
			 //$shipping_charge=$logistics_price;
			
		}	
		$shipping_charge=round($shipping_charge);
		
		$default_shipping_detail=array('shipping_charge'=>$shipping_charge,'territory_duration'=>$territory_duration,'territory_duration_org'=>$territory_duration_org);
		return $default_shipping_detail;
	
	}
	public function check_pincode_which_has_service_for_storing_in_localstorage($saved_pincode,$vendor_id){
		$pincode=$saved_pincode;
		
		$result=$this->Model_search->get_default_logistics_id_for_all_vendors($vendor_id);
		$logistics_id_arr=array();
		foreach($result as $res){
			$logistics_id_arr[]=$res->logistics_id;
		}
		
		$arr=$this->Model_search->check_availability_pincode_for_storing_in_localstorage($logistics_id_arr,$pincode);
		return $arr;
		//$this->session->set_userdata("searched_pincode",$pincode);
		
		//setcookie('searched_pincode',$pincode);
	}
	public function check_pincode_which_has_service(){
		$pincode_check_post_data = json_decode(file_get_contents("php://input"));
		$pincode=$pincode_check_post_data->pincode;
		$vendor_id=$pincode_check_post_data->vendor_id;
		//$vendor_id=$this->Model_search->get_vendor_id();
		$result=$this->Model_search->get_default_logistics_id_for_all_vendors($vendor_id);
		$logistics_id_arr=array();
		foreach($result as $res){
			$logistics_id_arr[]=$res->logistics_id;
		}
		//print_r($logistics_id_arr);
		
		$flag=$this->Model_search->check_availability_pincode($logistics_id_arr,$pincode,$vendor_id);
		
		//$this->session->set_userdata("searched_pincode",$pincode);
		
		//setcookie('searched_pincode',$pincode);
			
		
	}
	
	public function get_corresponding_shipping_charge(){
		//update the cart table details if customer is in session
		$pincode_check_post_data = json_decode(file_get_contents("php://input"));
		$pincode=$pincode_check_post_data->pincode;
		$cart_data=$pincode_check_post_data->cart_data;

		$all_data_arr=array();

		$pincode_valid=$this->Model_search->validate_pincode($pincode);
		$valid_cart_items_count=0;
		foreach($cart_data as $cart_data_value){
			if(($cart_data_value->logistics_id)==-1){
				
				//$logistics_id=$cart_data_value->logistics_id;
				
				$current_quantity=$cart_data_value->inventory_moq;
				$inventory_id=$cart_data_value->inventory_id;
				$vendor_id=$cart_data_value->vendor_id;				
				
				/////////////////// getting logistics id and territory name id starts /////
				$result=$this->Model_search->get_default_logistics_id_for_all_vendors($vendor_id);
					$logistics_id_arr=array();
					foreach($result as $res){
						$logistics_id_arr[]=$res->logistics_id;
					}
					
					$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr=$this->Model_search->get_logistics_id_territory_name_id_by_pincode_vendor_id($logistics_id_arr,$pincode,$vendor_id);
				$logistics_id=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["logistics_id"];

				$territory_name_id=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["territory_name_id"];
				$flag=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["check_logistics_yes"];
				//////////////////  getting logistics id and territory name id ends /////////
				
				///////////////////////
				$moq_original=$cart_data_value->inventory_moq_original;
				$shipping_detail=$this->get_territory_of_pincode($pincode,$vendor_id,$logistics_id,$territory_name_id);
				
				$logistics_territory_id=$shipping_detail['logistics_territory_id'];
				$logistics_territory_name=$this->get_territory_name_by_territory_id($logistics_territory_id);
				
				$free_shipping_arr=$this->get_free_shipping_territories_by_inventory_id($inventory_id);
	
						$logistics_id=$shipping_detail['logistics'];
						//$flag=$this->Model_search->check_availability_pincode_foreach_inventory($pincode,$logistics_id);
						if($flag==true){				
						$city=$shipping_detail['city'];	
						$default_delivery_mode_id=$shipping_detail['default_delivery_mode_id'];
						$default_delivery_mode=$shipping_detail['default_delivery_mode'];
						$logistics_name=$shipping_detail['logistics_name'];
						$speed_duration=$shipping_detail['speed_duration'];
						
						$default_parcel_category=$this->get_default_parcel_category($logistics_id);
						$logistics_parcel_category_id=$default_parcel_category["logistics_parcel_category_id"];
						$parcel_type=$default_parcel_category["parcel_type"];
						$parcel_type_description=$default_parcel_category["parcel_type_description"];
						
						$logistics_inventory_obj=$this->get_logistics_inventory_data($inventory_id);
						$parcel_cat_arr=$logistics_inventory_obj->logistics_parcel_category_id;
						$available_parcel_category_of_inventory=$this->available_parcel_category_of_inventory($inventory_id,$parcel_cat_arr);
						
						//$inventory_weight_in_kg=$logistics_inventory_obj->inventory_weight_in_kg;
						$inventory_weight_in_kg=$cart_data_value->inventory_weight_in_kg;
						$inventory_weight_in_kg_for_unit=round($inventory_weight_in_kg/$moq_original);
						
						$inventory_volume_in_kg=$cart_data_value->inventory_volume_in_kg;
						
						////////// getting inventory addons weights in kg starts////////////////////////////////
						$addons_inv_list_arr=[];
						$inventory_for_addons_weight_in_kg=0;
						if($cart_data_value->addon_products_status=="1"){
							$addons_inv_list_full_arr=$cart_data_value->addon_products;
							if(gettype($addons_inv_list_full_arr)=="string"){
								$addons_inv_list_full_arr=json_decode($addons_inv_list_full_arr);
							}
							if(!empty($addons_inv_list_full_arr)){
								foreach($addons_inv_list_full_arr as $obj){
									$addons_inv_list_arr[]=$obj->inv_id;
								}
								$inventory_for_addons_weight_in_kg=$this->Model_search->get_inventory_for_addons_weight_in_kg($addons_inv_list_arr);
							}
						}
						
						$inventory_weight_in_kg=$inventory_weight_in_kg+(float)$inventory_for_addons_weight_in_kg;
						////////// getting inventory addons weights in kg ends////////////////////////////////
						
						
						
					
						
						
						//$logistics_price=$logistics_price_resultant_inventory_volume_in_kg_arr["logistics_price_with_tax"];
						
						$get_flat_shipping_available_or_not_arr=$this->get_flat_shipping_available_or_not($inventory_id);
						if($get_flat_shipping_available_or_not_arr["shipping_charge_not_applicable"]=="yes"){
							$logistics_price=$get_flat_shipping_available_or_not_arr["flat_shipping_charge"];
							// the below two conditions only for hurbz launch only local and karnataka territories enabled
							// if($logistics_territory_id==1 || $logistics_territory_id==6 || $logistics_territory_id==14){
							// 	$logistics_price=60;
							// }
							// if($logistics_territory_id==2 || $logistics_territory_id==7 || $logistics_territory_id==15){
							// 	$logistics_price=150;
							// }
						}
						else{
							
							$logistics_price_resultant_inventory_volume_in_kg_arr=$this->get_logistics_price($current_quantity,$inventory_id,$logistics_id,$inventory_weight_in_kg,$inventory_volume_in_kg,$logistics_territory_id,$default_delivery_mode_id,$logistics_parcel_category_id);
							
							$logistics_price=$logistics_price_resultant_inventory_volume_in_kg_arr["logistics_price_with_tax"];
						}
						
						
						
						
						$default_shipping_detail=$this->get_default_shipping_charge($inventory_id,$pincode,$logistics_territory_id,$logistics_id,$default_delivery_mode_id,$logistics_parcel_category_id,$logistics_price,$speed_duration);
						
					
					
					if($free_shipping_arr!=false){	

						if(in_array($logistics_territory_name,$free_shipping_arr)) {
							$default_shipping_detail['logistics_price']=0;
							$default_shipping_detail['shipping_charge']=0;
							$default_shipping_detail['delivery_service']="free";
						
						}
						else{
							$default_shipping_detail['logistics_price']=$logistics_price;
							$default_shipping_detail['shipping_charge']=$logistics_price;
						}
					}
					else{
						$default_shipping_detail['logistics_price']=$logistics_price;
						$default_shipping_detail['shipping_charge']=$logistics_price;
					}
						$default_shipping_detail['delivery_service']=$flag;
						$default_shipping_detail['logistics_id']=$logistics_id;
						$default_shipping_detail['logistics_territory_id']=$logistics_territory_id;
						
						$default_shipping_detail['default_delivery_mode_id']=$default_delivery_mode_id;
						
						$default_shipping_detail['logistics_parcel_category_id']=$logistics_parcel_category_id;
						
						
						$default_shipping_detail['logistics_price']=$logistics_price;
						$default_shipping_detail['inventory_weight_in_kg']=$inventory_weight_in_kg;
						$default_shipping_detail['inventory_weight_in_kg_for_unit']=$inventory_weight_in_kg_for_unit;
						$default_shipping_detail['inventory_volume_in_kg']=$inventory_volume_in_kg;
						}else{
								$default_shipping_detail=array('shipping_charge'=>-1,'territory_duration'=>-1,'territory_duration_org'=>-1,'delivery_service'=>$flag);
							}
	
				
			}else{
				
				$current_quantity=$cart_data_value->inventory_moq;
				
				
				$vendor_id=$cart_data_value->vendor_id;		

				/////////////////// getting logistics id and territory name id starts /////
				$result=$this->Model_search->get_default_logistics_id_for_all_vendors($vendor_id);
					$logistics_id_arr=array();
					foreach($result as $res){
						$logistics_id_arr[]=$res->logistics_id;
					}
					
					$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr=$this->Model_search->get_logistics_id_territory_name_id_by_pincode_vendor_id($logistics_id_arr,$pincode,$vendor_id);
				$logistics_id=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["logistics_id"];
				$territory_name_id=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["territory_name_id"];
				$flag=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["check_logistics_yes"];
				//////////////////  getting logistics id and territory name id ends /////////

				
				$default_parcel_category=$this->get_default_parcel_category($logistics_id);
				// only if conditon not content  newly added for packsbazaar 5/8/2021 starts 
				if(isset($default_parcel_category)){
				$logistics_parcel_category_id=$default_parcel_category["logistics_parcel_category_id"];
				$shipping_detail=$this->get_territory_of_pincode($pincode,$vendor_id,$logistics_id,$territory_name_id);
				
				$logistics_territory_id=$shipping_detail['logistics_territory_id'];
				$logistics_id=$shipping_detail['logistics'];
				$city=$shipping_detail['city'];	
						$default_delivery_mode_id=$shipping_detail['default_delivery_mode_id'];
						$default_delivery_mode=$shipping_detail['default_delivery_mode'];
						$logistics_name=$shipping_detail['logistics_name'];
						$speed_duration=$shipping_detail['speed_duration'];
						/////
				
				$logistics_territory_name=$this->get_territory_name_by_territory_id($logistics_territory_id);
				$inventory_id=$cart_data_value->inventory_id;
				$free_shipping_arr=$this->get_free_shipping_territories_by_inventory_id($inventory_id);
				}
			// only if conditon not content  newly added for packsbazaar 5/8/2021 ends 
				
				//	$flag=$this->Model_search->check_availability_pincode_foreach_inventory($pincode,$logistics_id);
					
					if($flag==true){
						$default_shipping_detail=$this->get_default_shipping_charge($cart_data_value->inventory_id,$pincode,$logistics_territory_id,$logistics_id,$default_delivery_mode_id,$logistics_parcel_category_id,$cart_data_value->logistics_price,$speed_duration);
						
						
						
						$get_flat_shipping_available_or_not_arr=$this->get_flat_shipping_available_or_not($inventory_id);
						if($get_flat_shipping_available_or_not_arr["shipping_charge_not_applicable"]=="yes"){
							$logistics_price=$get_flat_shipping_available_or_not_arr["flat_shipping_charge"];
							
							// the below two conditions only for hurbz launch only local and karnataka territories enabled
							//if($logistics_territory_id==1 || $logistics_territory_id==6 || $logistics_territory_id==14){
							//	$logistics_price=60;
							//}
							//if($logistics_territory_id==2 || $logistics_territory_id==7 || $logistics_territory_id==15){
							//	$logistics_price=150;
							//}
							
							
						}
						else{
							
							
							
							
							////////// getting inventory addons weights in kg starts////////////////////////////////
						$cart_data_value_inventory_weight_in_kg=$cart_data_value->inventory_weight_in_kg;
						$addons_inv_list_arr=[];
						$inventory_for_addons_weight_in_kg=0;
						if($cart_data_value->addon_products_status=="1"){
							$addons_inv_list_full_arr=$cart_data_value->addon_products;
							if(gettype($addons_inv_list_full_arr)=="string"){
								$addons_inv_list_full_arr=json_decode($addons_inv_list_full_arr);
							}
							if(!empty($addons_inv_list_full_arr)>0){
								foreach($addons_inv_list_full_arr as $obj){
									$addons_inv_list_arr[]=$obj->inv_id;
								}
								$inventory_for_addons_weight_in_kg=$this->Model_search->get_inventory_for_addons_weight_in_kg($addons_inv_list_arr);
							}
						}
						
						$cart_data_value_inventory_weight_in_kg=$cart_data_value_inventory_weight_in_kg+(float)$inventory_for_addons_weight_in_kg;
						////////// getting inventory addons weights in kg ends////////////////////////////////
						
						
						
						
							
							$logistics_price_resultant_inventory_volume_in_kg_arr=$this->get_logistics_price($current_quantity,$inventory_id,$logistics_id,$cart_data_value_inventory_weight_in_kg,$cart_data_value->inventory_volume_in_kg,$logistics_territory_id,$default_delivery_mode_id,$logistics_parcel_category_id);
						//$logistics_price=$logistics_price_resultant_inventory_volume_in_kg_arr["logistics_price_with_tax"];
						
						
							$logistics_price=$logistics_price_resultant_inventory_volume_in_kg_arr["logistics_price_with_tax"];
						}
						
							
						if($free_shipping_arr!=false){	

							if(in_array($logistics_territory_name,$free_shipping_arr)) {
								$default_shipping_detail['logistics_price']=0;
								$default_shipping_detail['shipping_charge']=0;
								$default_shipping_detail['delivery_service']="free";
							
							}
							else{
								$default_shipping_detail['logistics_price']=$logistics_price;
								$default_shipping_detail['shipping_charge']=$logistics_price;
							}
						}
						else{
							$default_shipping_detail['logistics_price']=$logistics_price;
							$default_shipping_detail['shipping_charge']=$logistics_price;
						}
						$default_shipping_detail['delivery_service']=$flag;
						$default_shipping_detail['logistics_id']=$logistics_id;
						$default_shipping_detail['logistics_territory_id']=$logistics_territory_id;
						
						
						
						$default_shipping_detail['default_delivery_mode_id']=$default_delivery_mode_id;
						
						$default_shipping_detail['logistics_parcel_category_id']=$logistics_parcel_category_id;

						$default_shipping_detail['inventory_weight_in_kg']=$cart_data_value->inventory_weight_in_kg;
						$default_shipping_detail['inventory_weight_in_kg_for_unit']=$cart_data_value->inventory_weight_in_kg_for_unit;
						$default_shipping_detail['inventory_volume_in_kg']=$cart_data_value->inventory_volume_in_kg;
					}else{
						$default_shipping_detail=array('shipping_charge'=>-1,'territory_duration'=>-1,'territory_duration_org'=>-1,'delivery_service'=>$flag);
					}

				
			}
            $default_shipping_detail['pincode_valid']=$pincode_valid;
			// the below if condition only for hurbz launch
			$valid_cart_items_count++;
			/*$shipping_charge_applied_index_arr=[];
			for($shipping_charge_applied_index=1;$shipping_charge_applied_index<=100;$shipping_charge_applied_index=$shipping_charge_applied_index+4){
				$shipping_charge_applied_index_arr[]=$shipping_charge_applied_index;
			}
			if(!in_array($valid_cart_items_count,$shipping_charge_applied_index_arr)){
				$default_shipping_detail['logistics_price']=0;
				$default_shipping_detail['shipping_charge']=0;
			}*/
			
			$all_data_arr[]=$default_shipping_detail;
		}

        echo json_encode($all_data_arr);

	}
	
	
	public function get_corresponding_shipping_charge_for_detail_page(){
		//update the cart table details if customer is in session
		$pincode_check_post_data = json_decode(file_get_contents("php://input"));
		
		$logistics_id=$pincode_check_post_data->logistics_id;
		$territory_name_id=$pincode_check_post_data->territory_name_id;
		
		$pincode=$pincode_check_post_data->pincode;
		$inv_value=$pincode_check_post_data->cart_data;
		
		$inventory_weight_in_kg=$inv_value->inventory_weight_in_kg;
		$all_data_arr=array();
		
		$vendor_id=$inv_value->vendor_id;
		$inventory_id=$inv_value->id;

		///echo "test";
		
		$default_parcel_category=$this->get_default_parcel_category($logistics_id);
		//$parcel_category_multiplier=$inv_value->parcel_category_multiplier;
		$logistics_parcel_category_id=$default_parcel_category["logistics_parcel_category_id"];
		//$parcel_category_multiplier=$default_parcel_category["multiplier"];	
		
		$shipping_detail=$this->get_territory_of_pincode($pincode,$vendor_id,$logistics_id,$territory_name_id);
		//print_r($shipping_detail);
		//exit;
		$city=$shipping_detail['city'];
		$office_name=$shipping_detail['office_name'];
		$terri=isset($inv_value->logistics_territory_id) ? $inv_value->logistics_territory_id:'';
		
		if($terri!=''){
			
			$logistics_territory_id=$inv_value->logistics_territory_id;
                        $logistics_id=$inv_value->logistics_id;
			$default_delivery_mode_id=($inv_value->default_delivery_mode_id == '')? $shipping_detail['default_delivery_mode_id']: $inv_value->default_delivery_mode_id;
			$speed_duration=$shipping_detail['speed_duration'];

		}else{
			$logistics_territory_id=$shipping_detail['logistics_territory_id'];
			$default_delivery_mode_id=$shipping_detail['default_delivery_mode_id'];
			$speed_duration=$shipping_detail['speed_duration'];
			$logistics_id=$shipping_detail['logistics'];
		}


        $inventory_weight_in_kg=$inv_value->inventory_weight_in_kg;
		
		//$inventory_weight_in_kg_for_unit=round($inventory_weight_in_kg/$inv_value->moq);
		$current_quantity=$inv_value->moq;
		
		$inventory_volume_in_kg=$inv_value->inventory_volume_in_kg;
		
	////////// test code starts/////////	
		/*	$get_logistics_inventory_data_obj=$this->Model_search->get_logistics_inventory_data($inventory_id);
			$inventory_volume_in_lbh_cms=$get_logistics_inventory_data_obj->inventory_volume_in_kg;
			$quantity_volumetric_breakup_point=$get_logistics_inventory_data_obj->volumetric_breakup_point;
			
			$get_logistics_by_logistics_id_obj=$this->Model_search->get_logistics_by_logistics_id($logistics_id);
			$logistics_volumetric_value=$get_logistics_by_logistics_id_obj->logistics_volumetric_value;
			$net_volumetric_volume_weight=$inventory_volume_in_lbh_cms/$logistics_volumetric_value;
			
			if($current_quantity<($quantity_volumetric_breakup_point+1)){
				$resultant_inventory_volume_in_kg=$net_volumetric_volume_weight;
			}
			else{
				$resultant_inventory_volume_in_kg=(intval(($current_quantity-1)/$quantity_volumetric_breakup_point)+1)*$net_volumetric_volume_weight;
			}
			
			*/
			
			
////////// test code ends /////////				
		$get_flat_shipping_available_or_not_arr=$this->get_flat_shipping_available_or_not($inventory_id);
		if($get_flat_shipping_available_or_not_arr["shipping_charge_not_applicable"]=="yes"){
			
			//$logistics_price_resultant_inventory_volume_in_kg_arr=$this->get_logistics_price($current_quantity,$inventory_id,$logistics_id,$inventory_weight_in_kg,$inventory_volume_in_kg,$logistics_territory_id,$default_delivery_mode_id,$logistics_parcel_category_id);
	
			//$logistics_price=$logistics_price_resultant_inventory_volume_in_kg_arr["logistics_price_with_tax"];
		
			//$resultant_inventory_volume_in_kg=$logistics_price_resultant_inventory_volume_in_kg_arr["resultant_inventory_volume_in_kg"];
			$logistics_price=$get_flat_shipping_available_or_not_arr["flat_shipping_charge"];
		}
		else{
			$logistics_price_resultant_inventory_volume_in_kg_arr=$this->get_logistics_price($current_quantity,$inventory_id,$logistics_id,$inventory_weight_in_kg,$inventory_volume_in_kg,$logistics_territory_id,$default_delivery_mode_id,$logistics_parcel_category_id);
	
			$logistics_price=$logistics_price_resultant_inventory_volume_in_kg_arr["logistics_price_with_tax"];
		
			//$resultant_inventory_volume_in_kg=$logistics_price_resultant_inventory_volume_in_kg_arr["resultant_inventory_volume_in_kg"];
		}
		
		$default_shipping_detail=$this->get_default_shipping_charge($inventory_id,$pincode,$logistics_territory_id,$logistics_id,$default_delivery_mode_id,$logistics_parcel_category_id,$logistics_price,$speed_duration);
		
		
		$free_shipping_arr=$this->get_free_shipping_territories_by_inventory_id($inventory_id);
			
			$logistics_territory_name=$this->get_territory_name_by_territory_id($logistics_territory_id);
			
			if($free_shipping_arr!=false){	

				if(in_array($logistics_territory_name,$free_shipping_arr)) {
				
				$default_shipping_detail['shipping_charge']=0;
				$default_shipping_detail['delivery_service']="free";
				
				}else{
					$default_shipping_detail['delivery_service']=true;
				}
				
			}else{
				$default_shipping_detail['delivery_service']=true;
			}
			
		$shipping_charge=$default_shipping_detail['shipping_charge'];
		$shipping_charge=round($shipping_charge);
		$delivery_service=$default_shipping_detail['delivery_service'];
		
		if($shipping_charge==0){
			$free_delivery="Free Delivery Service";
		}else{
			$free_delivery="";
		}
		$territory_duration_calculated=$default_shipping_detail['territory_duration'];
		$territory_duration=$default_shipping_detail['territory_duration_org'];
		/////////////////////////
		/*$get_flat_shipping_available_or_not_arr=$this->get_flat_shipping_available_or_not($inventory_id);
		//print_r($get_flat_shipping_available_or_not_arr);
		if($get_flat_shipping_available_or_not_arr["shipping_charge_not_applicable"]=="yes"){
			$shipping_charge=$get_flat_shipping_available_or_not_arr["flat_shipping_charge"];
			$logistics_price=$get_flat_shipping_available_or_not_arr["flat_shipping_charge"];
		}*/
		//////////////////////////
		$array_temp=array("shipping_charge"=>$shipping_charge,"free_delivery"=>$free_delivery,"territory_duration"=>$territory_duration,"inventory_weight_in_kg"=>$inventory_weight_in_kg,"city"=>$city,"office_name"=>$office_name,"logistics_territory_id"=>$logistics_territory_id,"default_delivery_mode_id"=>$default_delivery_mode_id,"logistics_id"=>$logistics_id,"logistics_price"=>$logistics_price,"delivery_service"=>$delivery_service);
		
		echo json_encode($array_temp);
	}	
	public function get_territory_name_by_territory_id($logistics_territory_id){
		$territory_name=$this->Model_search->get_territory_name_by_territory_id($logistics_territory_id);
		return $territory_name;
	}
	public function get_free_shipping_territories_by_inventory_id($inventory_id){
		$free_shipping_territories_arr=$this->Model_search->get_free_shipping_territories_by_inventory_id($inventory_id);
		
		$arr=array();
		if($free_shipping_territories_arr!=false){
			
			foreach($free_shipping_territories_arr as $free_shipping_territories_arr_value){
				$arr[]=$free_shipping_territories_arr_value["territory_name"];
			}
		return $arr;
		}else{
			return false;
		}
	}
	///////////////// for get
	public function test(){
		echo $this->get_inventory_shipping_charge("599","600001","1","10");
	}
	public function get_inventory_shipping_charge($inventory_id,$pincode,$vendor_id,$moq){
		$updated_moq=$moq*20;
		$shipping_detail=$this->get_territory_of_pincode($pincode,$vendor_id);
		
		$logistics_territory_id=$shipping_detail['logistics_territory_id'];
		$logistics_territory_name=$this->get_territory_name_by_territory_id($logistics_territory_id);
		
		$free_shipping_arr=$this->get_free_shipping_territories_by_inventory_id($inventory_id);

				$logistics_id=$shipping_detail['logistics'];
				$speed_duration=$shipping_detail['speed_duration'];
				$flag=$this->Model_search->check_availability_pincode_foreach_inventory($pincode,$logistics_id);
				if($flag==true){				
					$city=$shipping_detail['city'];	
					$default_delivery_mode_id=$shipping_detail['default_delivery_mode_id'];
					$default_delivery_mode=$shipping_detail['default_delivery_mode'];
					$logistics_name=$shipping_detail['logistics_name'];
					
					
					
					$default_parcel_category=$this->get_default_parcel_category();
					$logistics_parcel_category_id=$default_parcel_category["logistics_parcel_category_id"];
					$parcel_type=$default_parcel_category["parcel_type"];
					
					$parcel_type_description=$default_parcel_category["parcel_type_description"];
					
					$logistics_inventory_obj=$this->get_logistics_inventory_data($inventory_id);
					
					$parcel_cat_arr=$logistics_inventory_obj->logistics_parcel_category_id;
					$available_parcel_category_of_inventory=$this->available_parcel_category_of_inventory($inventory_id,$parcel_cat_arr);
					
					$inventory_weight_in_kg=$logistics_inventory_obj->inventory_weight_in_kg;
					
					
					$inventory_weight_in_kg_for_unit=round($inventory_weight_in_kg/$moq);
					$inventory_weight_in_kg=$updated_moq*$inventory_weight_in_kg_for_unit;
					$inventory_volume_in_kg=$logistics_inventory_obj->inventory_volume_in_kg;
					
					$logistics_price_resultant_inventory_volume_in_kg_arr=$this->get_logistics_price($logistics_id,$inventory_weight_in_kg,$inventory_volume_in_kg);
					$logistics_price=$logistics_price_resultant_inventory_volume_in_kg_arr["logistics_price_with_tax"];
					
					$default_shipping_detail=$this->get_default_shipping_charge($inventory_id,$pincode,$logistics_territory_id,$logistics_id,$default_delivery_mode_id,$logistics_parcel_category_id,$logistics_price,$speed_duration);
				
				
					if($free_shipping_arr!=false){	

						if(in_array($logistics_territory_name,$free_shipping_arr)) {
						
						$default_shipping_detail['shipping_charge']=0;
						$default_shipping_detail['delivery_service']="free";
						
						}
					}
					
				}else{
					$default_shipping_detail=array('shipping_charge'=>-1,'territory_duration'=>-1,'territory_duration_org'=>-1,'delivery_service'=>$flag);
				}
				return $default_shipping_detail['shipping_charge'];
	}
//////////////////////////////////////////////////////////////////    
public function detail($inventory_rid,$jarvinaioutput=""){

		//visitor_activity();
		
		//$method=$this->uri->segment(2);
		/*
		$method=current_url();
		$this->session->set_userdata("cur_page",$method);
		$data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
        $data['inventory_id']=substr($inventory_rid, 9);
        $inventory_id=substr($inventory_rid, 9);
        $data['p_category']=$this->provide_first_level_menu();
        $data["controller"]=$this;
        $data["policy_applied"]=$this->getPolicyForInventory($inventory_id);
		
        $data["all_recomendations"]=$this->getRecomendationsForInventory($inventory_id);
        $product_id=$this->Front_promotions->get_product_id($inventory_id);
        $promo_array=array();
        if(!empty($product_id)){
            $a_TO_z_ids=$this->Front_promotions->get_a_TO_z_id($product_id);
            
            if(!empty($a_TO_z_ids)){
                foreach($a_TO_z_ids as $a_TO_z_id){
                    $brand_id=$a_TO_z_id['brand_id'];
                    $subcat_id=$a_TO_z_id['subcat_id'];
                    $cat_id=$a_TO_z_id['cat_id'];
                    $pcat_id=$a_TO_z_id['pcat_id'];
                }
                
                $product_level_promotions_uid=$this->Front_promotions->product_level_promotion_uid($product_id);
                $brand_level_promotions_uid=$this->Front_promotions->brand_level_promotion_uid($brand_id);
                $subcat_level_promotions_uid=$this->Front_promotions->subcat_level_promotion_uid($subcat_id);
                $cat_level_promotions_uid=$this->Front_promotions->cat_level_promotion_uid($cat_id);
                $pcat_level_promotions_uid=$this->Front_promotions->pcat_level_promotion_uid($pcat_id);
                $store_level_promotions_uid=$this->Front_promotions->store_level_promotion_uid();
               
                if(!empty($product_level_promotions_uid)){
                    foreach($product_level_promotions_uid as $product_level_promotion){
                        $promo_data=$this->Front_promotions->product_level_promotion($product_level_promotion['promo_uid'],$product_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
                        }
                    }
                    
                }
                
                if(!empty($brand_level_promotions_uid)){
                    
                    foreach($brand_level_promotions_uid as $brand_level_promotion){
                        $promo_data=$this->Front_promotions->brand_level_promotion($brand_level_promotion['promo_uid'],$brand_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
                            
                        }
                    }
                }
                if(!empty($subcat_level_promotions_uid)){
                    foreach($subcat_level_promotions_uid as $subcat_level_promotion){
                        $promo_data=$this->Front_promotions->subcat_level_promotion($subcat_level_promotion['promo_uid'],$subcat_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
							
                        }
                    }
                }
                if(!empty($cat_level_promotions_uid)){
                    foreach($cat_level_promotions_uid as $cat_level_promotion){
                        $promo_data=$this->Front_promotions->cat_level_promotion($cat_level_promotion['promo_uid'],$cat_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
							
                        }
                    }
                }
                if(!empty($pcat_level_promotions_uid)){
                    foreach($pcat_level_promotions_uid as $pcat_level_promotion){
                        $promo_data=$this->Front_promotions->pcat_level_promotion($pcat_level_promotion['promo_uid'],$pcat_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
							
                        }
                    }
                }
                if(!empty($store_level_promotions_uid)){
					
                    foreach($store_level_promotions_uid as $store_level_promotion){
                        $promo_data=$this->Front_promotions->store_level_promotion($store_level_promotion['promo_uid']);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
                        }
                    }
                }
            }
        }
        
        
        $promo_uid=$this->Front_promotions->get_promotions($inventory_id);
        
        if(!empty($promo_uid)){
            
            foreach($promo_uid as $res){
                $promo_data=$this->Front_promotions->get_all_promotions_data($res['promo_uid'],$inventory_id);
                if(!empty($promo_data)){
                    array_push($promo_array,$promo_data);
                    
                }
            }
        }
        
        
        $data["promotions"]=$promo_array;
        $data["current_controller"]="template";
        $data["total_reviews"]=$this->Model_search->get_total_reviews($inventory_id);
        $data["total_rate_count"]=$this->Model_search->get_total_rate_count($inventory_id);
        $data["total_review_count"]=$this->Model_search->get_total_review_count($inventory_id);
        $data["number_ratings_one_to_five"]=$this->Model_search->number_ratings_one_to_five($inventory_id);
        
        $attribute_n_checked_arr=array();
        if(!$this->input->post("attribute_n_value")){
            $data["attribute_n_value"]="attribute_1_value";
        }
        else{
            $attribute_n_checked=$this->input->post("attribute_n_checked");
            $attribute_n_checked_arr=explode("_",$attribute_n_checked);
            $data["attribute_n_value"]=$this->input->post("attribute_n_value");
        }
        $data["attribute_n_checked"]=$attribute_n_checked_arr;
        
        $ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$data["ui_arr"]=$this->get_ui_data_for_index();
        $this->load->view('header',$data);
		
		$get_sku_id_product_id_Obj=$this->Model_search->get_sku_id_product_id($inventory_id);
		if(preg_match("/[a-z]/i", $inventory_id)){
			$this->load->view('error404',$data);
		}
		else if(empty($get_sku_id_product_id_Obj)){
			$this->load->view('error404',$data);
		}
		else{
			$this->load->view('detail',$data);
		}
        $this->load->view('footer');
		*/
		
		$parentcategory_name="";
		$category_name="";
		$subcategory_name="";
		$brand_name="";
		$product_name="";
		$sku_id="";
		$inventory_id=substr($inventory_rid, 9);
		$get_sku_id_product_id_Obj=$this->Model_search->get_sku_id_product_id($inventory_id);
		$sku_id=$get_sku_id_product_id_Obj->sku_id;
		$product_id=$get_sku_id_product_id_Obj->product_id;
		$get_product_stuff_by_product_id_seo_obj=$this->Model_search->get_product_stuff_by_product_id_seo($product_id);
		$product_name=$get_product_stuff_by_product_id_seo_obj->product_name;
		$pcat_id=$get_product_stuff_by_product_id_seo_obj->pcat_id;
		$cat_id=$get_product_stuff_by_product_id_seo_obj->cat_id;
		$subcat_id=$get_product_stuff_by_product_id_seo_obj->subcat_id;
		$brand_id=$get_product_stuff_by_product_id_seo_obj->brand_id;
		
		if($pcat_id!=0){
			$parentcategory_name=$this->Model_search->get_parentcategory_name($pcat_id);
			$parentcategory_name=strtolower($parentcategory_name);
			$parentcategory_name = str_replace(' ', '-', $parentcategory_name);
		}
		else{
			$parentcategory_name="null";
		}
	
		
		$category_name=$this->Model_search->get_category_name($cat_id);
		$category_name=strtolower($category_name);
		$category_name = str_replace(' ', '-', $category_name);
		
		
		$subcategory_name=$this->Model_search->get_subcategory_name($subcat_id);
		$subcategory_name=strtolower($subcategory_name);
		$subcategory_name = str_replace(' ', '-', $subcategory_name);
		
		$brand_name=$this->Model_search->get_brand_name($brand_id);
		$brand_name=strtolower($brand_name);
		$brand_name = str_replace(' ', '-', $brand_name);
		
		
		$product_name=strtolower($product_name);
		$product_name = str_replace(' ', '-', $product_name);
		
		$sku_id=strtolower($sku_id);
		$sku_id = str_replace(' ', '-', $sku_id);
	
		redirect(site_url("item/".$parentcategory_name."/".$category_name."/".$subcategory_name."/".$brand_name."/".$product_name."/".$sku_id."/".$jarvinaioutput));
  }
  
  public function item($parentcategory_name,$category_name,$subcategory_name,$brand_name,$product_name,$sku_id,$jarvinaioutput=""){
	  
	////read_and_update_traffic();
	//visitor_activity_analytics();

			$sku_id = str_replace('-', ' ', $sku_id);
			$inventory_id=$this->Model_search->get_inventory_id_by_sku_id_seo($sku_id);
			$rand_temp=$this->sample_code();
			$inventory_rid=$rand_temp.$inventory_id;
			
			
		//visitor_activity();
		
		//$method=$this->uri->segment(2);
		if($jarvinaioutput!=""){
			$data["jarvinaioutput"]=$jarvinaioutput;
			$data["jarvinaioutput_number_of_skus"]=1;
		}
		else{
			$data["jarvinaioutput"]="";
			$data["jarvinaioutput_number_of_skus"]=1;
		}
			
		$method=current_url();
		$this->session->set_userdata("cur_page",$method);
		$data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
        $data['inventory_id']=substr($inventory_rid, 9);
        $inventory_id=substr($inventory_rid, 9);
        $data['p_category']=$this->provide_first_level_menu();
        $data["controller"]=$this;
        $data["policy_applied"]=$this->getPolicyForInventory($inventory_id);
		
        $data["all_recomendations"]=$this->getRecomendationsForInventory($inventory_id);
        $product_id=$this->Front_promotions->get_product_id($inventory_id);
        $promo_array=array();$promo_uid_arr=array();
        if(!empty($product_id)){
            $a_TO_z_ids=$this->Front_promotions->get_a_TO_z_id($product_id);
            
            if(!empty($a_TO_z_ids)){
                foreach($a_TO_z_ids as $a_TO_z_id){
                    $brand_id=$a_TO_z_id['brand_id'];
                    $subcat_id=$a_TO_z_id['subcat_id'];
                    $cat_id=$a_TO_z_id['cat_id'];
                    $pcat_id=$a_TO_z_id['pcat_id'];
                }
                
                $product_level_promotions_uid=$this->Front_promotions->product_level_promotion_uid($product_id);
                $brand_level_promotions_uid=$this->Front_promotions->brand_level_promotion_uid($brand_id);
                $subcat_level_promotions_uid=$this->Front_promotions->subcat_level_promotion_uid($subcat_id);
                $cat_level_promotions_uid=$this->Front_promotions->cat_level_promotion_uid($cat_id);
                $pcat_level_promotions_uid=$this->Front_promotions->pcat_level_promotion_uid($pcat_id);
                $store_level_promotions_uid=$this->Front_promotions->store_level_promotion_uid();
               
                if(!empty($product_level_promotions_uid)){
                    foreach($product_level_promotions_uid as $product_level_promotion){
                        $promo_data=$this->Front_promotions->product_level_promotion($product_level_promotion['promo_uid'],$product_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
							$promo_uid_arr[]=$product_level_promotion['promo_uid'];
                        }
						
                    }
                    
                }
                
                if(!empty($brand_level_promotions_uid)){
                    
                    foreach($brand_level_promotions_uid as $brand_level_promotion){
                        $promo_data=$this->Front_promotions->brand_level_promotion($brand_level_promotion['promo_uid'],$brand_id);
                        if(!empty($promo_data)){
							if(!in_array($brand_level_promotion['promo_uid'],$promo_uid_arr)){
                            	array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$brand_level_promotion['promo_uid'];
							}
                            
                        }
                    }
                }
                if(!empty($subcat_level_promotions_uid)){
                    foreach($subcat_level_promotions_uid as $subcat_level_promotion){
                        $promo_data=$this->Front_promotions->subcat_level_promotion($subcat_level_promotion['promo_uid'],$subcat_id);
                        if(!empty($promo_data)){
							if(!in_array($subcat_level_promotion['promo_uid'],$promo_uid_arr)){
                            	array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$subcat_level_promotion['promo_uid'];
							}
							
                        }
                    }
                }
                if(!empty($cat_level_promotions_uid)){
                    foreach($cat_level_promotions_uid as $cat_level_promotion){
                        $promo_data=$this->Front_promotions->cat_level_promotion($cat_level_promotion['promo_uid'],$cat_id);
                        if(!empty($promo_data)){
                            if(!in_array($cat_level_promotion['promo_uid'],$promo_uid_arr)){
                            	array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$cat_level_promotion['promo_uid'];
							}
							
                        }
                    }
                }
                if(!empty($pcat_level_promotions_uid)){
                    foreach($pcat_level_promotions_uid as $pcat_level_promotion){
                        $promo_data=$this->Front_promotions->pcat_level_promotion($pcat_level_promotion['promo_uid'],$pcat_id);
                        if(!empty($promo_data)){
                            
							if(!in_array($pcat_level_promotion['promo_uid'],$promo_uid_arr)){
                            	array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$pcat_level_promotion['promo_uid'];
							}
                        }
                    }
                }
                if(!empty($store_level_promotions_uid)){
					
                    foreach($store_level_promotions_uid as $store_level_promotion){
                        $promo_data=$this->Front_promotions->store_level_promotion($store_level_promotion['promo_uid']);
                        if(!empty($promo_data)){
							if(!in_array($store_level_promotion['promo_uid'],$promo_uid_arr)){
                            	array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$store_level_promotion['promo_uid'];
							}
                        }
                    }
                }
            }
        }
        $promo_uid=$this->Front_promotions->get_promotions($inventory_id);
        
        if(!empty($promo_uid)){
            foreach($promo_uid as $res){
                $promo_data=$this->Front_promotions->get_all_promotions_data($res['promo_uid'],$inventory_id);
                if(!empty($promo_data)){
                    //array_push($promo_array,$promo_data);
                    if(!in_array($res['promo_uid'],$promo_uid_arr)){
						array_push($promo_array,$promo_data);
						$promo_uid_arr[]=$res['promo_uid'];
					}
                }
            }
        }
        
		//print_r($promo_array);

        $data["promotions"]=$promo_array;
        $data["current_controller"]="template";
        $data["total_reviews"]=$this->Model_search->get_total_reviews($inventory_id);
		
		$data["total_rate_count"]=$this->Model_search->get_total_rate_count($inventory_id);
        $data["total_review_count"]=$this->Model_search->get_total_review_count($inventory_id);
        
		$data["number_ratings_one_to_five"]=$this->Model_search->number_ratings_one_to_five($inventory_id);
        
        $attribute_n_checked_arr=array();
        if(!$this->input->post("attribute_n_value")){
            $data["attribute_n_value"]="attribute_1_value";
        }
        else{
            $attribute_n_checked=$this->input->post("attribute_n_checked");
            $attribute_n_checked_arr=explode("_",$attribute_n_checked);
            $data["attribute_n_value"]=$this->input->post("attribute_n_value");
        }
        $data["attribute_n_checked"]=$attribute_n_checked_arr;
        
        $ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$data["ui_arr"]=$this->get_ui_data_for_index();
        
		
			
			$data["positioning"]=$this->Customer_frontend->get_positioning($inventory_id);

			$this->load->view('header',$data);
			
			$get_sku_id_product_id_Obj=$this->Model_search->get_sku_id_product_id($inventory_id);
			if(preg_match("/[a-z]/i", $inventory_id)){
				$this->load->view('error404',$data);
			}
			else if(empty($get_sku_id_product_id_Obj)){
				$this->load->view('error404',$data);
			}
			else{
				$this->load->view('detail',$data);
			}
			$this->load->view('footer');
		//}
		
    }
	public function detail_old($inventory_rid){
		$method=current_url();
		$this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
	  
        $data['inventory_id']=substr($inventory_rid, 9);
        $inventory_id=substr($inventory_rid, 9);
        $data['p_category']=$this->provide_first_level_menu();
        $data["controller"]=$this;
        $data["policy_applied"]=$this->getPolicyForInventory($inventory_id);
        $data["all_recomendations"]=$this->getRecomendationsForInventory($inventory_id);
        $product_id=$this->Front_promotions->get_product_id($inventory_id);
        $promo_array=array();
        if(!empty($product_id)){
            $a_TO_z_ids=$this->Front_promotions->get_a_TO_z_id($product_id);
            
            if(!empty($a_TO_z_ids)){
                foreach($a_TO_z_ids as $a_TO_z_id){
                    $brand_id=$a_TO_z_id['brand_id'];
                    $subcat_id=$a_TO_z_id['subcat_id'];
                    $cat_id=$a_TO_z_id['cat_id'];
                    $pcat_id=$a_TO_z_id['pcat_id'];
                }
                
                $product_level_promotions_uid=$this->Front_promotions->product_level_promotion_uid($product_id);
                $brand_level_promotions_uid=$this->Front_promotions->brand_level_promotion_uid($brand_id);
                $subcat_level_promotions_uid=$this->Front_promotions->subcat_level_promotion_uid($subcat_id);
                $cat_level_promotions_uid=$this->Front_promotions->cat_level_promotion_uid($cat_id);
                $pcat_level_promotions_uid=$this->Front_promotions->pcat_level_promotion_uid($pcat_id);
                $store_level_promotions_uid=$this->Front_promotions->store_level_promotion_uid();
               
                if(!empty($product_level_promotions_uid)){
                    foreach($product_level_promotions_uid as $product_level_promotion){
                        $promo_data=$this->Front_promotions->product_level_promotion($product_level_promotion['promo_uid'],$product_id);
                        
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
                        }
                    }
                    
                }
                
                if(!empty($brand_level_promotions_uid)){
                    
                    foreach($brand_level_promotions_uid as $brand_level_promotion){
                        $promo_data=$this->Front_promotions->brand_level_promotion($brand_level_promotion['promo_uid'],$brand_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
                            
                        }
                    }
                }
                if(!empty($subcat_level_promotions_uid)){
                    foreach($subcat_level_promotions_uid as $subcat_level_promotion){
                        $promo_data=$this->Front_promotions->subcat_level_promotion($subcat_level_promotion['promo_uid'],$subcat_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
                        }
                    }
                }
                if(!empty($cat_level_promotions_uid)){
                    foreach($cat_level_promotions_uid as $cat_level_promotion){
                        $promo_data=$this->Front_promotions->cat_level_promotion($cat_level_promotion['promo_uid'],$cat_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
                        }
                    }
                }
                if(!empty($pcat_level_promotions_uid)){
                    foreach($pcat_level_promotions_uid as $pcat_level_promotion){
                        $promo_data=$this->Front_promotions->pcat_level_promotion($pcat_level_promotion['promo_uid'],$pcat_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
                        }
                    }
                }
                if(!empty($store_level_promotions_uid)){
                    foreach($store_level_promotions_uid as $store_level_promotion){
                        $promo_data=$this->Front_promotions->store_level_promotion($store_level_promotion['promo_uid']);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
                        }
                    }
                }
            }
        }
        $promo_uid=$this->Front_promotions->get_promotions($inventory_id);
        
        if(!empty($promo_uid)){
            foreach($promo_uid as $res){
                $promo_data=$this->Front_promotions->get_all_promotions_data($res['promo_uid'],$inventory_id);
                if(!empty($promo_data)){
                    array_push($promo_array,$promo_data);
                    
                }
            }
        }
        
        $data["promotions"]=$promo_array;
        $data["current_controller"]="search";
        $data["total_reviews"]=$this->Model_search->get_total_reviews($inventory_id);
        $data["number_ratings_one_to_five"]=$this->Model_search->number_ratings_one_to_five($inventory_id);
        
        $attribute_n_checked_arr=array();
        if(!$this->input->post("attribute_n_value")){
            $data["attribute_n_value"]="attribute_1_value";
        }
        else{
            $attribute_n_checked=$this->input->post("attribute_n_checked");
            $attribute_n_checked_arr=explode("_",$attribute_n_checked);
            $data["attribute_n_value"]=$this->input->post("attribute_n_value");
        }
        $data["attribute_n_checked"]=$attribute_n_checked_arr;
        
        $ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
        $this->load->view('header',$data);

        $this->load->view('detail_backup',$data);
        $this->load->view('footer');
    }

public function get_free_item_data($ids,$promo_uid,$nums){
    $item_data=$this->Front_promotions->get_free_item_data($ids);
    $strs="";

    foreach ($item_data as $data){
        $product_name=$this->get_product_name($data['product_id']);
   
        $strs.='';
		$attribute_1_value=$data["attribute_1_value"];
		$attribute_1_value_arr=explode(":",$attribute_1_value);
		$attr_value=$attribute_1_value_arr[0];
		$rand=$this->sample_code();
		$strs.='<a href="'.base_url().'detail/'.$rand.$data["id"].'" target="_blank">'.$product_name.'s - '.$attr_value.'</a><br>';
        $strs.= '<input type="hidden" free_nums="'.$nums.'" name="promo_selector_name_'.$promo_uid.'"  value="'.$product_name.'">';
        $strs.= '<input type="hidden" name="promo_selector_'.$promo_uid.'"  value="'.$ids.'">';
        
		
    }
    return $strs;
}
public function get_free_item_data_image_path_attributes($inventory_id){
	$get_free_item_data_image_path_attributes_arr=$this->Front_promotions->get_free_item_data_image_path_attributes($inventory_id);
	return $get_free_item_data_image_path_attributes_arr;
}
public function get_product_name($product_id){
    return $this->Front_promotions-> get_product_name($product_id);
}

public function get_free_inventory_items_in_promotions(){
        $free_inventory_data = json_decode(file_get_contents("php://input"));
        $free_inventory="";
        $inventory_id="";
        foreach($free_inventory_data as $k=>$invs){
            if($k=='free_inventory'){
                $free_inventory.=$invs;
            }
            if($k=='inv'){
                $inventory_id.=$invs;
            }
        }
        $inventory_arr=explode(",",$free_inventory);
            $str="";
            $k=0;
            foreach($inventory_arr as $inv_id){
                if($inv_id!=""){
                    $promo_items=$this->Front_promotions->get_all_promotion_free_item($inv_id);
                    
                    foreach($promo_items as $data){
                        $arrars=[];
                        $image=$data['image'];
                        $product_id=$data['product_id'];
                        $product_name=$this->Front_promotions->get_product_name($product_id);
                        $attribute_1=$data['attribute_1'];
                        $attribute_1_value=$data['attribute_1_value'];
                        $attribute_2=$data['attribute_2'];
                        $attribute_2_value=$data['attribute_2_value'];
                        $attribute_3=$data['attribute_3'];
                        $attribute_3_value=$data['attribute_3_value'];
                        $attribute_4=$data['attribute_4'];
                        $attribute_4_value=$data['attribute_4_value'];
                        $sku_id=$data['sku_id'];
                        $selling_price=$data['selling_price'];
                        $max_selling_price=$data['max_selling_price'];
                        /*addded*/
                        $product_id=$this->Front_promotions->get_product_id($data['id']);
					
                        if(!empty($product_id)){
                            $a_TO_z_ids=$this->Front_promotions->get_a_TO_z_id($product_id);
                        }
                        //print_r($a_TO_z_ids);

                        if(!empty($a_TO_z_ids)){
                                foreach($a_TO_z_ids as $a_TO_z_id){
                                    $brand_id=$a_TO_z_id['brand_id'];
                                    $subcat_id=$a_TO_z_id['subcat_id'];
                                    $cat_id=$a_TO_z_id['cat_id'];
                                    $pcat_id=$a_TO_z_id['pcat_id'];
                                }
                        $all_straight_discounts=$this->Front_promotions->get_all_straight_discount();

                                if(!empty($all_straight_discounts)){

                                foreach($all_straight_discounts as $discount){

									//print_r($discount);

                                        $storeLevelStraightDiscount=$this->Front_promotions->getStoreLevelStraightDiscount($discount['promo_uid']);
										//echo $discount['promo_uid'];
                                        if($storeLevelStraightDiscount){
                                                $discount_per=$discount['get_type'];

                                                if(strpos($discount_per, "%") !== false){
                                                        $discount_per=str_replace("%","",$discount_per);
                                                }
                                                $selling_price=$max_selling_price;
												//echo '||'.$selling_price.'||'.$discount_per;
												if(is_numeric($discount_per)){
                                                	$price_after_discount=$selling_price-(($discount_per/100)*$selling_price);
												}else{
													$price_after_discount=$selling_price;
												}
                                                $arrars['selling_price']=$price_after_discount;
                                        }
                                        $parentCategoryLevelStraightDiscount=$this->Front_promotions->getParentCategoryLevelStraightDiscount($discount['promo_uid'],$pcat_id);
                                        if($parentCategoryLevelStraightDiscount){
                                                $discount_per=$discount['get_type'];
                                                if(strpos($discount_per, "%") !== false){
                                                        $discount_per=str_replace("%","",$discount_per);
                                                }
                                                $selling_price=$max_selling_price;
												if(is_numeric($discount_per)){
                                                	$price_after_discount=$selling_price-(($discount_per/100)*$selling_price);
												}else{
													$price_after_discount=$selling_price;
												}
                                                $arrars['selling_price']=$price_after_discount;
                                        }
                                        $categoryLevelStraightDiscount=$this->Front_promotions->getCategoryLevelStraightDiscount($discount['promo_uid'],$cat_id);
                                        if($categoryLevelStraightDiscount){
                                                $discount_per=$discount['get_type'];
                                                if(strpos($discount_per, "%") !== false){
                                                        $discount_per=str_replace("%","",$discount_per);
                                                }
                                                $selling_price=$max_selling_price;
                                                if(is_numeric($discount_per)){
                                                	$price_after_discount=$selling_price-(($discount_per/100)*$selling_price);
												}else{
													$price_after_discount=$selling_price;
												}
                                                $arrars['selling_price']=$price_after_discount;
                                        }
                                        $subCategoryLevelStraightDiscount=$this->Front_promotions->getSubCategoryLevelStraightDiscount($discount['promo_uid'],$subcat_id);
                                        if($subCategoryLevelStraightDiscount){
                                                $discount_per=$discount['get_type'];
                                                if(strpos($discount_per, "%") !== false){
                                                        $discount_per=str_replace("%","",$discount_per);
                                                }
                                                $selling_price=$max_selling_price;
                                                if(is_numeric($discount_per)){
                                                	$price_after_discount=$selling_price-(($discount_per/100)*$selling_price);
												}else{
													$price_after_discount=$selling_price;
												}
                                                $arrars['selling_price']=$price_after_discount;
                                        }

                                        $brandLevelStraightDiscount=$this->Front_promotions->getBrandLevelStraightDiscount($discount['promo_uid'],$brand_id);
                                        if($brandLevelStraightDiscount){
                                                $discount_per=$discount['get_type'];
                                                if(strpos($discount_per, "%") !== false){
                                                        $discount_per=str_replace("%","",$discount_per);
                                                }
                                                $selling_price=$max_selling_price;
                                                if(is_numeric($discount_per)){
                                                	$price_after_discount=$selling_price-(($discount_per/100)*$selling_price);
												}else{
													$price_after_discount=$selling_price;
												}
                                                $arrars['selling_price']=$price_after_discount;
                                        }
                                        $productLevelStraightDiscount=$this->Front_promotions->getProdctLevelStraightDiscount($discount['promo_uid'],$product_id);
                                        if($productLevelStraightDiscount){
                                                $discount_per=$discount['get_type'];
                                                if(strpos($discount_per, "%") !== false){
                                                        $discount_per=str_replace("%","",$discount_per);
                                                }
                                                $selling_price=$max_selling_price;
                                                if(is_numeric($discount_per)){
                                                	$price_after_discount=$selling_price-(($discount_per/100)*$selling_price);
												}else{
													$price_after_discount=$selling_price;
												}
                                                $arrars['selling_price']=$price_after_discount;
                                        }
                                        $inventoryLevelStraightDiscount=$this->Front_promotions->getInventoryLevelStraightDiscount($discount['promo_uid'],$data['id']);
                                        if($inventoryLevelStraightDiscount){
                                                $discount_per=$discount['get_type'];
                                                if(strpos($discount_per, "%") !== false){
                                                        $discount_per=str_replace("%","",$discount_per);
                                                }
                                                $selling_price=$max_selling_price;
                                                if(is_numeric($discount_per)){
                                                	$price_after_discount=$selling_price-(($discount_per/100)*$selling_price);
												}else{
													$price_after_discount=$selling_price;
												}
                                                $arrars['selling_price']=$price_after_discount;
                                        }
                                }
                                }
                        }
                        
                        $selling_price=isset($arrars['selling_price']) ? $arrars['selling_price'] : $data['selling_price'];
                        /*addded*/
                        
                    }
                    $str.="<tr><td><img src='".base_url().$image."'></td>";
                    $str.="<td><p class='product-name'><a href='#'>".$product_name."</a></p><small class='cart_ref'>".$sku_id."</small>";
                        $attribute_1_value = substr($attribute_1_value, 0, strpos($attribute_1_value, ":"));
						/*$str.='<br><small>';
                        if(!empty($attribute_1)){
                            $str.=$attribute_1." : ".$attribute_1_value."<br>";
                        }
                        if(!empty($attribute_2)){
                            $str.=$attribute_2." : ".$attribute_2_value."<br>";
                        }
                        if(!empty($attribute_3)){
                            $str.=$attribute_3." : ".$attribute_3_value."<br>";
                        }
                        if(!empty($attribute_4)){
                            $str.=$attribute_4." : ".$attribute_4_value."<br>";
                        }
						$str.='</small>';*/
                    $str.="</td><td class='text-center'><span id='free_inv_nums_".$inv_id."_".$inventory_id."'></span></td><td colspan='5'  class='text-center'><p><b>FREE</b> - You saved <span style='color:#e46c0a'><span id='free_inv_nums_quantity_show_frontend_strike_totalprice_".$inv_id."_".$inventory_id."'></span></span> (<span id='free_inv_nums_quantity_show_frontend_strike_".$inv_id."_".$inventory_id."'></span> * ".curr_sym.$selling_price."<span id='free_inv_nums_quantity_show_frontend_strike_sellingprice_".$inv_id."_".$inventory_id."' style='display:none;'>".$selling_price."</span>)</p> </td></tr>";
                }
                $k++;
            }
        echo $str;
        
    }

///////////////////////////////////////////////////////////
    /*free inventory_in promotions*/
    /* addon products  */

	public function get_addon_products(){
		$data = json_decode(file_get_contents("php://input"));
		if(isset($data->main_inventory_id))
		{
			$inventory_id=$data->main_inventory_id;
			$addon_inventories=$data->addon_inventories;
			$inv_index=$data->inv_index;
			$addon_total_price=0;
			//echo $addon_inventories;

			$addon_products=''; $str='';
			
			if($addon_inventories!=''){
				$addon_arr=explode(',',$addon_inventories);
				$addon_arr=array_filter($addon_arr);
				$addon_inventories=implode(',',$addon_arr);
				$addon_products_arr=[];

				//print_r($addon_arr);
				
				if(!empty($addon_arr)){
					$str.='<tr><td colspan="3">';
					foreach($addon_arr as $inv_id){
						$res_data=get_all_inventory_data($inv_id);

						//print_r($res_data);

						if(!empty($res_data)){
							$addon_total_price+=($res_data->selling_price);
							$addon_products_arr[]=['inv_id'=>$inv_id,'inv_price'=>$res_data->selling_price];
							$image=$res_data->image;
							$product_name=($res_data->sku_name!='') ? $res_data->sku_name : $res_data->product_name;
							
							$str.='<div class="col-xs-5 col-sm-5 col-md-3 nopad text-center" id="addon_inv_div_'.$inventory_id.'_'.$inv_id.'">
							<label class="image-checkbox image-checkbox-checked" ng-click="remove_addon_products('.$inv_index.','.$inventory_id.','.$inv_id.')">
							<img class="img-responsive" src="'.base_url().$res_data->image.'" />';

							$str.='<span><input type="hidden" id="addon_inv_'.$inventory_id.'_'.$inv_id.'" value="'.$res_data->selling_price.'"></span>';

							$str.='<i class="fa fa-check hidden" style="line-height: 15px;"></i>';

							$str.='<h5 class="margin-top">'.$product_name.'</h5>';
							
							$str.='<p>Price :'.curr_sym.$res_data->selling_price.'</p>';

							$str.='</label>';

							$str.='</div>';
						}
					}

					$str.='</td>';
					$str.='<td colspan="3" class="text-center">
					
					'.count($addon_arr).' additional items Selected
					</td>';
					$str.='<td class="text-center">';

					$str.='<span id="addon_total_price_'.$inventory_id.'">'.curr_sym.$addon_total_price.'</span>';
					$str.='</td>';
					$str.='</tr>';
				}
				//$addon_products=json_encode($addon_products_arr);
			}

			//print json_encode($addon_products);
			echo $str;
		}
	}

    /* addon products  */

/*recomendations start*/
public function getRecomendationsForInventory($inventory_id){
     $this->load->model('front_recomendations');
     $product_id=$this->Front_promotions->get_product_id($inventory_id);
     //$recomendation_list=$this->front_recomendations->getAllRecomendationList();
     $recomendations=array();
     if(!empty($product_id)){
            $a_TO_z_ids=$this->Front_promotions->get_a_TO_z_id($product_id);
         if(!empty($a_TO_z_ids)){
                foreach($a_TO_z_ids as $a_TO_z_id){
                    $brand_id=$a_TO_z_id['brand_id'];
                    $subcat_id=$a_TO_z_id['subcat_id'];
                    $cat_id=$a_TO_z_id['cat_id'];
                    $pcat_id=$a_TO_z_id['pcat_id'];
                }
                $inventory_id;
                $product_id;
                $brand_id;
                $subcat_id;
                $cat_id;
                $pcat_id;
                $sub_category_level_recomendations=$this->front_recomendations->get_sub_category_level_recomendations($pcat_id,$cat_id,$subcat_id);
                if($sub_category_level_recomendations){
                    $recomendations=array();
                    $recomendations=$recomendations+$sub_category_level_recomendations;
                }
                $brand_level_recomendations=$this->front_recomendations->get_brand_level_recomendations($pcat_id,$cat_id,$subcat_id,$brand_id);
                if($brand_level_recomendations){
                    $recomendations=array();
                    $recomendations=$recomendations+$brand_level_recomendations;
                }
                $product_level_recomendations=$this->front_recomendations->get_product_level_recomendations($pcat_id,$cat_id,$subcat_id,$brand_id,$product_id);
                if($product_level_recomendations){
                    $recomendations=array();
                    $recomendations=$recomendations+$product_level_recomendations;
                }
                $inventory_level_recomendations=$this->front_recomendations->get_inventory_level_recomendations($pcat_id,$cat_id,$subcat_id,$brand_id,$product_id,$inventory_id);
                if($inventory_level_recomendations){
                    $recomendations=array();
                    $recomendations=$recomendations+$inventory_level_recomendations;
                }
         }
     }
	 if(!empty($recomendations)){
		 
$recomendations_data=$this->front_recomendations->getAllRecomendation_data($recomendations[0]['recomendation_uid']);

   $first_level_recomendation=array();
    $second_level_recomendation=array();
    $third_level_recomendation=array();
if($recomendations_data){
 
    foreach($recomendations_data as $recom){
        
        if($recom['recomendation_level']=='first_level'){
            array_push($first_level_recomendation,$recom);
        }
        if($recom['recomendation_level']=='second_level'){
            array_push($second_level_recomendation,$recom);
        }
        if($recom['recomendation_level']=='third_level'){
            array_push($third_level_recomendation,$recom);
        }
        
    }
}
$final_recomendation_lista='';

$final_recomendation_list_first=array();
$final_recomendation_list_second=array();
$final_recomendation_list_third=array();
$array_first_level="";
$array_second_level="";
$array_third_level="";
$i=0;$j=0;$k=0;
if(count($first_level_recomendation)>0){
    shuffle($first_level_recomendation);
    
    $r=1;
    foreach( $first_level_recomendation as $datas){
        $array_first_level.=$datas['inv_id'];
        if($r<count($first_level_recomendation)){
            $array_first_level.=',';
        }
        $r++;
    }
    if(strlen($array_first_level)>0){
        $array_first_level=explode(',',$array_first_level);
    }
}

if(count($second_level_recomendation)>0){
    shuffle($second_level_recomendation);
    
    $r=1;
    foreach( $second_level_recomendation as $datas){
        $array_second_level.=$datas['inv_id'];
        if($r<count($second_level_recomendation)){
            $array_second_level.=',';
        }
        $r++;
    }
    if(strlen($array_second_level)>0){
        $array_second_level=explode(',',$array_second_level);
    }
}

if(count($first_level_recomendation)>0){
    shuffle($third_level_recomendation);
    
    $r=1;
    foreach( $third_level_recomendation as $datas){
        $array_third_level.=$datas['inv_id'];
        if($r<count($third_level_recomendation)){
            $array_third_level.='';
        }
        $r++;
    }
    if(strlen($array_third_level)>0){
        $array_third_level=explode(',',$array_third_level);
    }
    
}
$num=0;
if(is_array($array_first_level) && count($array_first_level)>0){
    
    $array_first_level=array_chunk($array_first_level,3);

    $num=count($array_first_level);
    
}
if(is_array($array_second_level) && count($array_second_level)>0){
    $array_second_level=array_chunk($array_second_level,2);
    //print_r($array_second_level);echo "<br>";
    if(count($array_second_level)>$num){
        $num=count($array_second_level);
    }
    
}
if(is_array($array_third_level) && count($array_third_level)>0){
    $array_third_level=array_chunk($array_third_level,1);
    //print_r($array_third_level);echo "<br>";
    if(count($array_third_level)>$num){
        $num=count($array_third_level);
    }
}
$final_array=array();
for($j=0;$j<$num;$j++){
    if(array_key_exists($j,$array_first_level)){
        array_push($final_array,$array_first_level[$j]);
    }
    if(array_key_exists($j,$array_second_level)){
        array_push($final_array,$array_second_level[$j]);
    }
    if(array_key_exists($j,$array_third_level)){
        array_push($final_array,$array_third_level[$j]);
    }
}

foreach($final_array as $arrys){
    $i=0;
    foreach($arrys as $s){
        $final_recomendation_lista.=$this->getDataForInventory($s);
    }
}

$final_recomendation_lista.='';

return $final_recomendation_lista;
	 }else{
		 return false;
	 }
}

public function getDataForInventory($inv_id){
    $invs_data=$this->front_recomendations->getInventoryData($inv_id);
    $returnString='';
    if($invs_data){
        
        foreach($invs_data as $data){
			$inventory_id=$data['id'];
			$product_id=$data['product_id'];
			$selling_price=$data['selling_price']; // parsing max_selling price
			$max_selling_price=$data['max_selling_price'];
			$selling_discount=$data['selling_discount'];
                        
			$inv_discount_data=$this->get_default_discount_for_inv($inventory_id,$max_selling_price,$product_id);
			
			$productsssName=$this->getProductName($data['product_id']);
			
			$out = strlen($productsssName) > 10 ? substr($productsssName,0,10)."..." : $productsssName;
			
            $returnString.='<li>
									<div class="product-container">
                                        <div class="left-block">
                                        <a href="'.base_url().'detail/'.$this->sample_code().$data["id"].'">
                                            <img src="'.base_url().$data["common_image"].'" class="img-responsive" > 
                                        </a>
										<div class="quick-view">
                                                    <a title="Add to my wishlist" class="heart" href="#"></a>
                                                    <a title="Add to compare" class="compare" href="#"></a>
                                        </div>';
						if(!empty($inv_discount_data)){			
							$returnString.='<div class="price-percent-reduction2">-'.$inv_discount_data['discount'].'% OFF</div>';
						}			
							$returnString.='</div>
										<div class="right-block">
										<h5 class="product-name"><a href="#"><span title="'.$productsssName.'">'.$out.'</span></a></h5>
										<small>
                                                <table>';
                                                    if($data['attribute_1']!=""){
														
														if(preg_match('/:/', $data['attribute_1_value'])){
															$arr_color=explode(':',$data['attribute_1_value']);
															$value=$arr_color[0];
														}
														else{
															$value=$data['attribute_1_value'];
														}
														
                                                    $returnString.='<tr>
                                                                        <td> '.$data['attribute_1'].' </td>
                                                                        <td> <b>'.$value.'</b></td>
                                                                    </tr>';
                                                    }
                                                    if($data['attribute_2']!=""){
														if(preg_match('/:/', $data['attribute_2_value'])){
															$arr_color=explode(':',$data['attribute_2_value']);
															$value=$arr_color[0];
														}else{
															$value=$data['attribute_2_value'];
														}
                                                    $returnString.='<tr>
                                                                        <td> '.$data['attribute_2'].'</td>
                                                                        <td> <b>'.$value.'</b></td>
                                                                    </tr>';
                                                    }
                                                    if($data['attribute_3']!=""){
														if(preg_match('/:/', $data['attribute_3_value'])){
															$arr_color=explode(':',$data['attribute_3_value']);
															$value=$arr_color[0];
														}else{
															$value=$data['attribute_3_value'];
														}
														
                                                    $returnString.='<tr>
                                                                        <td> '.$data['attribute_3'].'</td>
                                                                        <td> <b>'.$value.'</b></td>
                                                                    </tr>';
                                                    }
                                                   if($data['attribute_4']!=""){
													   if(preg_match('/:/', $data['attribute_4_value'])){
															$arr_color=explode(':',$data['attribute_4_value']);
															$value=$arr_color[0];
														}else{
															$value=$data['attribute_4_value'];
														}
                                                    $returnString.='<tr>
                                                                        <td> '.$data['attribute_4'].'</td>
                                                                        <td> <b>'.$value.'</b></td>
                                                                    </tr>';
                                                   }
                                                  
                                                $returnString.=' </table>
                                            </small>';
						
                                            
$rate_number=$this->number_of_ratings($inventory_id);
$avg_value=$this->get_average_value_of_ratings($inventory_id);
$str_star='<div class="product-star has-popover">';	
		if($avg_value==0.5){
			
			$str_star.='<i class="fa fa-star-half-o"></i>';	
			$str_star.='<i class="fa fa-star-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
							
		}elseif($avg_value==1){
			
			$str_star.='<i class="fa fa-star"></i>';	
			$str_star.='<i class="fa fa-star-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
			
		}elseif($avg_value==1.5){
			$str_star.='<i class="fa fa-star"></i>';	
			$str_star.='<i class="fa fa-star-half-o"></i>';	
			$str_star.='<i class="fa fa-star-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
		}elseif($avg_value==2){
			$str_star.='<i class="fa fa-star"></i>';	
			$str_star.='<i class="fa fa-star"></i>';	
			$str_star.='<i class="fa fa-star-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
		}elseif($avg_value==2.5){
			$str_star.='<i class="fa fa-star"></i>';	
			$str_star.='<i class="fa fa-star"></i>';	
			$str_star.='<i class="fa fa-star-half-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
			
		}elseif($avg_value==3){
			$str_star.='<i class="fa fa-star"></i>';	
			$str_star.='<i class="fa fa-star"></i>';	
			$str_star.='<i class="fa fa-star"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
		}elseif($avg_value==3.5){
			$str_star.='<i class="fa fa-star"></i>';	
			$str_star.='<i class="fa fa-star"></i>';	
			$str_star.='<i class="fa fa-star"></i>';
			$str_star.='<i class="fa fa-star-half-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
		}elseif($avg_value==4){
			$str_star.='<i class="fa fa-star"></i>';	
			$str_star.='<i class="fa fa-star"></i>';	
			$str_star.='<i class="fa fa-star"></i>';
			$str_star.='<i class="fa fa-star"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
		}elseif($avg_value==4.5){
			$str_star.='<i class="fa fa-star"></i>';	
			$str_star.='<i class="fa fa-star"></i>';	
			$str_star.='<i class="fa fa-star"></i>';
			$str_star.='<i class="fa fa-star"></i>';
			$str_star.='<i class="fa fa-star-half-o"></i>';
		}elseif($avg_value==5){
			$str_star.='<i class="fa fa-star"></i>';	
			$str_star.='<i class="fa fa-star"></i>';	
			$str_star.='<i class="fa fa-star"></i>';
			$str_star.='<i class="fa fa-star"></i>';
			$str_star.='<i class="fa fa-star"></i>';
		}else{
			$str_star.='<i class="fa fa-star-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
			$str_star.='<i class="fa fa-star-o"></i>';
		}
		$str_star.='('.$rate_number.')&nbsp;';
		$str_star.='</div>';
		
							$returnString.=$str_star;	
							
							$returnString.='<div class="content_price">';
												if(!empty($inv_discount_data)){
												$returnString.='<span class="price product-price">'.curr_sym.$inv_discount_data['current_price'].'</span>
                                                <span class="price old-price">'.curr_sym.$inv_discount_data['inventory_price'].'</span>';
												
												}else{
													$returnString.='<span class="price product-price">'.curr_sym.$data['selling_price'].'</span>';
												}	
												
                            $returnString.='</div>
                                        </div>
 
                                </li>';
         }
    }
	
return $returnString;

}

public function getProductName($productId){
    return $this->front_recomendations->getProductName($productId);
}
/*recomendations ends*/
    
/*policy*/
public function getPolicyForInventory($inventory_id){
     $product_id=$this->Front_promotions->get_product_id($inventory_id);
     $policy_id_list=$this->Front_policy->getAllActivePolicyList();
	 
     $policy_array=array();
        if(!empty($product_id)){
            $a_TO_z_ids=$this->Front_promotions->get_a_TO_z_id($product_id);
            
            if(!empty($a_TO_z_ids)){
                foreach($a_TO_z_ids as $a_TO_z_id){
                    $brand_id=$a_TO_z_id['brand_id'];
                    $subcat_id=$a_TO_z_id['subcat_id'];
                    $cat_id=$a_TO_z_id['cat_id'];
                    $pcat_id=$a_TO_z_id['pcat_id'];
                }
                    $inventory_id;
                    $product_id;
                    $brand_id;
                    $subcat_id;
                    $cat_id;
                    $pcat_id;
$payment_policy=array();
                foreach($policy_id_list as $policy){
                    
      
                       /*check wheather payment policy exists or not*/
                       if($this->Front_policy->check_policy_exist($policy['id'],$policy['name'])){
                               /*Payment*/
                           $id=$policy['id'];
                           $name=$policy['name'];
                            $storeLevelPaymentPolicy=$this->Front_policy->getStoreLevelPaymentPolicy($id);
                            if(!empty($storeLevelPaymentPolicy)){
                                
                                foreach($storeLevelPaymentPolicy as $PaymentPolicy){
                                    $policy_uid=$PaymentPolicy['policy_uid'];
                                    $policy_data=$this->Front_policy->store_level_policy($id,$name,$policy_uid);
                                   $payment_policy[$policy['name']]=$policy_data;
                                }
                            }
                            $parentCategoryPaymentLevelPolicy=$this->Front_policy->getParentCategoryLevelPaymentPolicy($id,$pcat_id);
							
                            if(!empty($parentCategoryPaymentLevelPolicy)){
     
                                foreach($parentCategoryPaymentLevelPolicy as $PaymentPolicy){
									
                                    $policy_uid=$PaymentPolicy['policy_uid'];
                                    $policy_data=$this->Front_policy->parent_category_level_policy($id,$name,$policy_uid,$pcat_id);
									
                                   $payment_policy[$policy['name']]=$policy_data;
								  
                                }
                            }
                            $CategoryLevelPaymentPolicy=$this->Front_policy->getCategoryLevelPaymentPolicy($id,$cat_id);
                            if(!empty($CategoryLevelPaymentPolicy)){
 
                                foreach($CategoryLevelPaymentPolicy as $PaymentPolicy){
                                    $policy_uid=$PaymentPolicy['policy_uid'];
                                   $policy_data=$this->Front_policy->category_level_policy($id,$name,$policy_uid,$cat_id);
                                   $payment_policy[$policy['name']]=$policy_data;
                                }
                            }
                            $subCategoryLevelPaymentPolicy=$this->Front_policy->getSubCategoryLevelPaymentPolicy($id,$subcat_id);
                            if(!empty($subCategoryLevelPaymentPolicy)){

                                foreach($subCategoryLevelPaymentPolicy as $PaymentPolicy){
                                    $policy_uid=$PaymentPolicy['policy_uid'];
                                   $policy_data=$this->Front_policy->sub_category_level_policy($id,$name,$policy_uid,$subcat_id);
                                   $payment_policy[$policy['name']]=$policy_data;
                                }
                            }
                            $brandLevelPaymentPolicy=$this->Front_policy->getBrandLevelPaymentPolicy($id,$brand_id);
                            if(!empty($brandLevelPaymentPolicy)){

                                foreach($brandLevelPaymentPolicy as $PaymentPolicy){
                                    $policy_uid=$PaymentPolicy['policy_uid'];
                                   $policy_data=$this->Front_policy->brand_level_policy($id,$name,$policy_uid,$brand_id);
                                   $payment_policy[$policy['name']]=$policy_data;
                                }
                            }
                            $productLevelPaymentPolicy=$this->Front_policy->getProductLevelPaymentPolicy($id,$product_id);
                            if(!empty($productLevelPaymentPolicy)){

                                foreach($productLevelPaymentPolicy as $PaymentPolicy){
                                    $policy_uid=$PaymentPolicy['policy_uid'];
                                   $policy_data=$this->Front_policy->product_level_policy($id,$name,$policy_uid,$product_id);
                                   $payment_policy[$policy['name']]=$policy_data;
                                }
                            }
                            $invLevelPaymentPolicy=$this->Front_policy->getInvLevelPaymentPolicy($id,$inventory_id);
                            if(!empty($invLevelPaymentPolicy)){

                                foreach($invLevelPaymentPolicy as $PaymentPolicy){
                                    $policy_uid=$PaymentPolicy['policy_uid'];
                                    $policy_data=$this->Front_policy->inventory_level_policy($id,$name,$policy_uid,$inventory_id);
                                   $payment_policy[$policy['name']]=$policy_data;
                                }
                            }
                       }
                       
                   
                 
                }
                
                return $payment_policy;
                /*Replacement*/
            }
        }
}
/*policy ends*/
	public function dump_search_keywords($search_keyword_index_page){
		$search_keyword_index_page=strtolower($search_keyword_index_page);
		
		$this->Model_search->dump_search_keywords($search_keyword_index_page);
	}
	/*similar products starts*/
	public function getSimilarProducts($product_id){
		$similarProducts=$this->Front_promotions->getSimilarProducts($product_id);
		return $similarProducts;
	}
	/*simliar products ends*/

	public function add_mail_id_to_notify(){
		$inventory_id=$this->input->post("inventory_id");
		$email=$this->input->post("email");
		$sku_id=$this->input->post("sku_id");
		$customer_id=$this->input->post("customer_id");
		$res=$this->Model_search->add_mail_id_to_notify($inventory_id,$email,$sku_id,$customer_id);
		echo $res;
	}
	public function getSearchSugessations(){
		$search_cat=$this->input->post('search_cat');
		$search_keyword=$this->input->post('search_keyword');
		/// searchkeywords length make sure 3 
		if(strlen($search_keyword)>=3){
		
		require_once("assets/elastic/app/init_customer.php");
		if($search_cat!=""){
			$search_in;
			$search_in_value;
			$search_cat_arr=explode('_',$search_cat);
			$search_cat=$search_cat_arr[0];
			$search_type=$search_cat_arr[1];
			if($search_type=="pcat"){
				$search_in=$search_type;
				$search_in_value=$search_cat;
				
				//['query'=>['filtered'=>['query'=>['match_phrase_prefix'=>['_all'=>$search_keyword]],'filter'=>['bool'=>['must'=>[['term'=>['pcat_id'=>$search_in_value]]]]]]]]
	
	$params = ['index' => 'flamingo1','body'=>
	[
		'query' => [
			'bool' => [
				
				"must"=> [
					"match"=> [
					  "pcat_id"=>$search_in_value
					]
				],
				'filter' => [
					'term' => [
						'pcat_id' => $search_in_value,
					]
				],
			],
		],
	],
	
	];
	
			}else{
				$pcat_id=$search_cat_arr[0];
				$search_in_value=$search_cat_arr[1];
	
				//['query'=>['filtered'=>['query'=>['match_phrase_prefix'=>['_all'=>$search_keyword]],'filter'=>['bool'=>['must'=>[['term'=>['pcat_id'=>$pcat_id]],['term'=>['cat_id'=>$search_in_value]],]]]]]]
	
	$params = ['index' => 'flamingo1','body'=>
	[
		'query' => [
			'bool' => [
				
				"must"=> [
					"match"=> [
					  "cat_id"=>$search_in_value
					]
				],
				'filter' => [
					'term' => [
						'pcat_id' => $search_in_value,
					],
					'term' => [
						'cat_id' => $search_in_value,
					]
				],
			],
		],
	],
	
	];
	
			}
				
		}else{
			//['query'=>['filtered'=>['query'=>['match_phrase_prefix'=>['_all'=>$search_keyword]],'filter'=>['or'=>[['type'=>['value'=>'flamingoCategory']],['type'=>['value'=>'flamingoSubCategory']]]],]]]
			
			/*
			'bool' => [
				'must' => [
					[ 'match' => [ 'product_name' => $search_keyword] ],
					[ 'match' => [ 'pcat_name' => $search_keyword ] ],
					[ 'match' => [ 'cat_name' => $search_keyword ] ],
					[ 'match' => [ 'subcat_name' => $search_keyword ] ],
					[ 'match' => [ 'brand_name' => $search_keyword ] ],
	
				]
				],
			
			*/
	$params = ['index' => 'flamingo1','body'=>
	[
		'query' => [
			
				"multi_match" => [
					"fields" => ["product_name","sku_name","sku_id"],
					"query" => $search_keyword,
					"type" => "phrase_prefix"
				]
		]	
		
	],
	
	];
	
			
		}
	
	
	//print_r($params);
	$response = $es->search($params);
	$array=array();
	
	//print_r($response);
	
		$array_for_exact=array();
		$pro_name=array();
		$sku_name_arr=array();
		$sku_id_arr=array();
		//echo "<pre>";
		//print_r($response['hits']['hits']);
		//echo "</pre>";exit;
		
		foreach($response['hits']['hits'] as $hits){
			   
				//if($hits['_type']=="flamingoSubCategory"){
				//if(1){
				if($hits['_type']=="flamingoproducts1"){ // I modified
					// search whether the product_name is matched with entered characters or not
						if (stripos($hits['_source']['product_name'], $search_keyword) !== false or stripos($hits['_source']['sku_id'], $search_keyword) !== false) {
							$push= array (
								'label' => $hits['_source']['product_name'].' - <span style="color:red;">'.$hits['_source']['cat_name'].'</span>',
								
								'product_name' =>$hits['_source']['product_name'],
								
								'product_id' => $hits['_source']['pcat_id'].'_'.$hits['_source']['cat_id'].'_'.$hits['_source']['subcat_id'].'_'.$hits['_source']['brand_id'].'_'.$hits['_source']['product_id'],
								
								'label_id'   => $hits['_source']['pcat_id'].'_'.$hits['_source']['cat_id'].'_'.$hits['_source']['subcat_id'],
								'label_name'=>strtolower($hits['_source']['product_name'].'-'.$hits['_source']['cat_name']),
								'main_chain_format'=>'product_id'
								
							);
							if($hits['_source']['pcat_id']!=0){
								$pcat_idObj=$this->get_parent_cat_info($hits['_source']['pcat_id']);
								if(empty($pcat_idObj)){
									continue;
								}
							}
							$cat_idObj=$this->get_cat_info($hits['_source']['cat_id']);
							if(empty($cat_idObj)){
								continue;
							}
							
							$subcat_idObj=$this->get_subcat_info($hits['_source']['subcat_id']);
							if(empty($subcat_idObj)){
								continue;
							}
							
							$brand_idObj=$this->get_brand_info($hits['_source']['brand_id']);
							if(empty($brand_idObj)){
								continue;
							}
							
							$product_idObj=$this->get_product_info($hits['_source']['product_id']);
							if(empty($product_idObj)){
								continue;
							}
							
							
							
							array_push($array_for_exact,$push);
						}
						
						
					// search whether the sku_name is matched with entered characters or not
					if (stripos($hits['_source']['sku_name'], $search_keyword) !== false) {
						 $push= array (
								'label' => $hits['_source']['sku_name'].' - <span style="color:red;">'.$hits['_source']['subcat_name'].'</span>',
								
								'sku_name' =>$hits['_source']['sku_name'],
								
								'inventory_id' => $hits['_source']['pcat_id'].'_'.$hits['_source']['cat_id'].'_'.$hits['_source']['subcat_id'].'_'.$hits['_source']['brand_id'].'_'.$hits['_source']['product_id'].'_'.$hits['_source']['inventory_id'],
								
								'label_id'   => $hits['_source']['pcat_id'].'_'.$hits['_source']['cat_id'].'_'.$hits['_source']['subcat_id'],
								'label_name'=>strtolower($hits['_source']['sku_name'].'-'.$hits['_source']['subcat_name']),
								'main_chain_format'=>'inventory_id'
								
							);
							if($hits['_source']['pcat_id']!=0){
								$pcat_idObj=$this->get_parent_cat_info($hits['_source']['pcat_id']);
								if(empty($pcat_idObj)){
									continue;
								}
							}
							$cat_idObj=$this->get_cat_info($hits['_source']['cat_id']);
							if(empty($cat_idObj)){
								continue;
							}
							
							$subcat_idObj=$this->get_subcat_info($hits['_source']['subcat_id']);
							if(empty($subcat_idObj)){
								continue;
							}
							
							$brand_idObj=$this->get_brand_info($hits['_source']['brand_id']);
							if(empty($brand_idObj)){
								continue;
							}
							
							$product_idObj=$this->get_product_info($hits['_source']['product_id']);
							if(empty($product_idObj)){
								continue;
							}
							
							$inventory_idObj=$this->get_inventory_info($hits['_source']['inventory_id']);
							if(empty($inventory_idObj)){
								continue;
							}
							
							
							array_push($array_for_exact,$push);
						}
						
				 }
			   /* if($hits['_type']=="flamingoCategory"){
				 $push= array (
								'label' => $hits['_source']['subcat_name'].' - <span style="color:red;">'.$hits['_source']['cat_name'].'</span> <b>'.$hits['_source']['pcat_name'].'</b>',
								'subcat_name' =>$hits['_source']['subcat_name'],
								'subcat_id' => $hits['_source']['pcat_id'].'_'.$hits['_source']['cat_id'].'_'.$hits['_source']['subcat_id'],
								
								'label_id'   => $hits['_source']['pcat_id'].'_'.$hits['_source']['cat_id'].'_'.$hits['_source']['subcat_id'],
								'label_name'=>strtolower($hits['_source']['subcat_name'].'-'.$hits['_source']['cat_name'].$hits['_source']['pcat_name']),
								'main_chain_format'=>'subcat_id'
							);
							if($hits['_source']['pcat_id']!=0){
								$pcat_idObj=$this->get_parent_cat_info($hits['_source']['pcat_id']);
								if(empty($pcat_idObj)){
									continue;
								}
							}
							$cat_idObj=$this->get_cat_info($hits['_source']['cat_id']);
							if(empty($cat_idObj)){
								continue;
							}
							$subcat_idObj=$this->get_subcat_info($hits['_source']['subcat_id']);
							if(empty($subcat_idObj)){
								continue;
							}
							
							
							
				 array_push($array_for_exact,$push);
				 }*/
				 /* 12/14/2021 code for removing duplicate labels from elastic search auto complete box starts */
				//$array_for_exact_subcat_id_arr=[];
				 $array_for_exact_product_id_arr=[];
				 $array_for_exact_inventory_id_arr=[];
				 foreach($array_for_exact as $arr){
					//if($arr["main_chain_format"]=="subcat_id"){
						//$array_for_exact_subcat_id_arr[]=$arr;
					//}
					if($arr["main_chain_format"]=="product_id"){
						$array_for_exact_product_id_arr[]=$arr;
					}
					if($arr["main_chain_format"]=="inventory_id"){
						$array_for_exact_inventory_id_arr[]=$arr;
					}
				}
				//$subcat_label_id_label_name_arr=[];
				$product_label_id_label_name_arr=[];
				$array_for_exact_without_duplicates=[];
				$total_array_with_without_duplicates=[];
				$total_array_label_names_arr=[];
				/*if(!empty($array_for_exact_subcat_id_arr)){
					foreach($array_for_exact_subcat_id_arr as $arr){
						$subcat_label_id_label_name_arr[]=$arr["label_id"]." ".$arr["label_name"];
						$array_for_exact_without_duplicates[]=$arr;
					}
				}
				if(!empty($array_for_exact_product_id_arr)){
					foreach($array_for_exact_product_id_arr as $arr){
						if(!in_array($arr["label_id"]." ".$arr["label_name"],$subcat_label_id_label_name_arr)){
							$array_for_exact_without_duplicates[]=$arr;
						}
					}
				}*/
				
				if(!empty($array_for_exact_product_id_arr)){
					foreach($array_for_exact_product_id_arr as $arr){
						$product_label_id_label_name_arr[]=$arr["label_id"]." ".$arr["label_name"];
						$array_for_exact_without_duplicates[]=$arr;
					}
				}
				
				if(!empty($array_for_exact_inventory_id_arr)){
					foreach($array_for_exact_inventory_id_arr as $arr){
						if(!in_array($arr["label_id"]." ".$arr["label_name"],$product_label_id_label_name_arr)){
							$product_label_id_label_name_arr[]=$arr["label_id"]." ".$arr["label_name"];
							$array_for_exact_without_duplicates[]=$arr;
						}
					}
				}
				//print_r($array_for_exact_without_duplicates);
				
				/* 12/14/2021 code for removing duplicate labels from elastic search auto complete box ends */
				$array["exact"]=$array_for_exact_without_duplicates;
				
		}
		
	echo json_encode ($array);
	
				}
				else{
					$array["exact"]=array();
					echo json_encode ($array);
				}
	
	}
	
	
public function get_default_discount_for_inv($inventory_id,$selling_price,$product_id,$qty_type_promo=''){
	
	//$product_id=$this->Front_promotions->get_product_id($inventory_id);
        $promo_array=array();$promo_uid_arr=array();
        if(!empty($product_id)){
            $a_TO_z_ids=$this->Front_promotions->get_a_TO_z_id($product_id);
            
            if(!empty($a_TO_z_ids)){
                foreach($a_TO_z_ids as $a_TO_z_id){
                    $brand_id=$a_TO_z_id['brand_id'];
                    $subcat_id=$a_TO_z_id['subcat_id'];
                    $cat_id=$a_TO_z_id['cat_id'];
                    $pcat_id=$a_TO_z_id['pcat_id'];
                }
                
                $product_level_promotions_uid=$this->Front_promotions->product_level_promotion_uid($product_id);
                $brand_level_promotions_uid=$this->Front_promotions->brand_level_promotion_uid($brand_id);
                $subcat_level_promotions_uid=$this->Front_promotions->subcat_level_promotion_uid($subcat_id);
                $cat_level_promotions_uid=$this->Front_promotions->cat_level_promotion_uid($cat_id);
                $pcat_level_promotions_uid=$this->Front_promotions->pcat_level_promotion_uid($pcat_id);
                $store_level_promotions_uid=$this->Front_promotions->store_level_promotion_uid();
               
                if(!empty($product_level_promotions_uid)){
					foreach($product_level_promotions_uid as $product_level_promotion){
						$promo_data=$this->Front_promotions->product_level_promotion($product_level_promotion['promo_uid'],$product_id);
						if(!empty($promo_data)){
							array_push($promo_array,$promo_data);
							$promo_uid_arr[]=$product_level_promotion['promo_uid'];
						}
						
					}
					
				}
				
				if(!empty($brand_level_promotions_uid)){
					
					foreach($brand_level_promotions_uid as $brand_level_promotion){
						$promo_data=$this->Front_promotions->brand_level_promotion($brand_level_promotion['promo_uid'],$brand_id);
						if(!empty($promo_data)){
							if(!in_array($brand_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$brand_level_promotion['promo_uid'];
							}
						}
					}
				}
				if(!empty($subcat_level_promotions_uid)){
					foreach($subcat_level_promotions_uid as $subcat_level_promotion){
						$promo_data=$this->Front_promotions->subcat_level_promotion($subcat_level_promotion['promo_uid'],$subcat_id);
						if(!empty($promo_data)){
							if(!in_array($subcat_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$subcat_level_promotion['promo_uid'];
							}
							
						}
					}
				}
				if(!empty($cat_level_promotions_uid)){
					foreach($cat_level_promotions_uid as $cat_level_promotion){
						$promo_data=$this->Front_promotions->cat_level_promotion($cat_level_promotion['promo_uid'],$cat_id);
						if(!empty($promo_data)){
							if(!in_array($cat_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$cat_level_promotion['promo_uid'];
							}
						}
					}
				}
				if(!empty($pcat_level_promotions_uid)){
					foreach($pcat_level_promotions_uid as $pcat_level_promotion){
						$promo_data=$this->Front_promotions->pcat_level_promotion($pcat_level_promotion['promo_uid'],$pcat_id);
						if(!empty($promo_data)){
							if(!in_array($pcat_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$pcat_level_promotion['promo_uid'];
							}
							
						}
					}
				}
				if(!empty($store_level_promotions_uid)){
					
					foreach($store_level_promotions_uid as $store_level_promotion){
						$promo_data=$this->Front_promotions->store_level_promotion($store_level_promotion['promo_uid']);
						if(!empty($promo_data)){
							if(!in_array($store_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$store_level_promotion['promo_uid'];
							}
						}
					}
				}
            }
        }
        $promo_uid=$this->Front_promotions->get_promotions($inventory_id);
        
        if(!empty($promo_uid)){
            foreach($promo_uid as $res){
                $promo_data=$this->Front_promotions->get_all_promotions_data($res['promo_uid'],$inventory_id);
                if(!empty($promo_data)){
                    array_push($promo_array,$promo_data);
                    
                }
            }
        }
        $inventory_product_info_obj=$this->get_inventory_product_info_by_inventory_id($inventory_id);
        //print_r($promo_array);
		$default_price_array=array();

		$overall_promo_arr=array();

		if(!empty($promo_array)){
			
			foreach($promo_array as $promotion){
				foreach($promotion as $promo){
					if (preg_match_all('/\d+(?=%)/', $promo-> get_type, $match)&&($promo->to_buy>=1)){

						if($qty_type_promo=='qty_based'){

							if($promo->to_buy>1 && $promo->buy_type='Qty'){
								
								//echo 'inside';

								//print_r($promo);
								$temp=[];
								if (preg_match_all('/\d+(?=%)/', $promo->get_type, $match) && ($promo->to_buy >1)) {
									$offer = $promo->promo_quote;
									$offer = preg_replace("/\([^)]+\)/","",$offer);        
									$default_price_array['current_price']= round($selling_price-round($selling_price*($match[0][0]/100)));
									$default_price_array['inventory_price']= round($selling_price);
									$default_price_array['discount']= $match[0][0];

									/* New required data */
									$temp["promo_quote"]=$promo->promo_quote;
									$temp["promo_uid"]=$promo->promo_uid;
									$temp["promo_list_to_buy"]=$promo->to_buy;
									$temp["promo_list_to_get"]=$promo->to_get;
									$temp["promo_list_to_buy_type"]=$promo->buy_type;
									$temp["promo_list_to_get_type"]=$promo->get_type;
									$temp["promo_list_discount"]=$match[0][0];
									/* New required data */

									$overall_promo_arr[]=$temp;
								}
							}
						}else{
							/* for straight % discount -  single qty*/
							if($promo->to_buy==1){
								$offer = $promo->promo_quote;
								$offer = preg_replace("/\([^)]+\)/","",$offer);        
								$default_price_array['current_price']= round($selling_price-round($selling_price*($match[0][0]/100)));
								$default_price_array['inventory_price']= round($selling_price);
								$default_price_array['discount']= $match[0][0];
							}
						}
						
					}	
				}
			}
		}
		if($qty_type_promo=='qty_based'){
			return $overall_promo_arr;
		}else{
			return $default_price_array;
		}
		
}
	public function compare(){
			//visitor_activity();
			$method=current_url();
			$this->session->set_userdata("cur_page",$method);
			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="search";
			$data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
			$data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('compare',$data);
			$this->load->view('footer');	
	}
	public function compare_inventories(){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="search";
		$data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
		$data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
		if($this->input->post("inventory_id_list_for_compare")!=""){
		$compare_params_str=$this->input->post("inventory_id_list_for_compare");
		$compare_params_arr=explode(",",$compare_params_str);
		$data["compare_params_arr"]=$compare_params_arr;
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('compare',$data);
		$this->load->view('footer');	
		}else{
			redirect(base_url());
		}
	}
	public function get_inventory_info_by_inventory_id($inventory_id){
		$get_inventory_info_arr= $this->Model_search->get_inventory_info_by_inventory_id($inventory_id);
		return $get_inventory_info_arr;
	}
	public function get_product_info_by_inventory_id($inventory_id){
		$get_product_info_arr = $this->Model_search->get_product_info_by_inventory_id($inventory_id);
		return $get_product_info_arr;
	}
	public function get_total_reviews($inventory_id){
		return $this->Model_search->get_total_reviews($inventory_id);
	}
	
	public function add_to_compare_inventories(){
		$inventory_id_list_for_compare=$this->input->post("inventory_id_list_for_compare");
		$inventory_id_list_for_compare_arr=explode(",",$inventory_id_list_for_compare);
		
		$flag=$this->Model_search->add_to_compare_inventories($inventory_id_list_for_compare_arr);
		echo $flag;
	}
	
	public function inventory_comparision_yes_or_no($inventory_id){
		$inventory_comparision=$this->Model_search->inventory_comparision_yes_or_no($inventory_id);
		return $inventory_comparision;
	}
	public function check_expansion_is_there_or_not($att_k,$product_id){
		$common_arr=array();
		
		$attribute_id=$this->Model_search->get_attribute_id($product_id);
		if($attribute_id!=''){
			$exp_obj=$this->Model_search->get_attribute_expansion_info($att_k,$attribute_id);
			
			if(!empty($exp_obj)){
				$attribute_expansions_id=$exp_obj->attribute_expansions_id;
				$opt_val_obj=$this->Model_search->attribute_expansions_sub_options($attribute_expansions_id);
				$common_arr['values']=$opt_val_obj;
				$common_arr['titles']=$exp_obj;
				
				return $common_arr;
				
			}else{
				return 'no';
			}
		}else{
			return 'no';
		}
		
	}
	public function to_return_value($p1,$p2,$p3)
	{
		$arr=array();
		$pcat_id=substr($p1, 9);$cat_id=substr($p2, 9);$subcat_id=substr($p3, 9);
		
		if(!empty($pcat_id)){
			if(!ctype_digit($pcat_id)){
				redirect(base_url()."error_to_view");
			}
		}
		if(!empty($cat_id)){
			if(!ctype_digit($cat_id)){
				redirect(base_url()."error_to_view");
				
			}
		}
		if(!empty($subcat_id)){
			if(!ctype_digit($subcat_id)){
				redirect(base_url()."error_to_view");
			}
		}
		if($pcat_id==0){
			$arr['pcat_name']='';
		}
		if(!empty($pcat_id) && is_numeric($pcat_id) && $pcat_id!=0){

			$p_name=$this->Customer_frontend->get_name_of_parent($pcat_id);
			if(isset($p_name[0]->pcat_name)){
				$arr['pcat_name']=$p_name[0]->pcat_name;
			}else{	
				redirect(base_url()."error_to_view");
			}
			
		}
		
		if(!empty($cat_id) && is_numeric($cat_id)){
			$cat_name=$this->Customer_frontend->get_name_of_category($pcat_id,$cat_id);
			
			if(isset($cat_name[0]->cat_name)){
				$arr['cat_name']=$cat_name[0]->cat_name;
			}else{
				redirect(base_url()."error_to_view");
			}
			
		}
		
		if(!empty($subcat_id) && is_numeric($subcat_id)){
			$subcat_name=$this->Customer_frontend->get_name_of_subcategory($pcat_id,$cat_id,$subcat_id);
			
			if(isset($subcat_name[0]->subcat_name)){
				$arr['subcat_name']=$subcat_name[0]->subcat_name;
			}else{
				redirect(base_url()."error_to_view");
			}
		}
		return $arr;

	}
	
	public function add_news_letter_subscription(){		
        extract($this->input->post());	
		print_r($this->input->post());	
        $dumped=$this->Customer_frontend->add_news_letter_subscription($email_address_nl);		
        if($dumped){		
            echo 1;		
        }else{		
            echo 0;		
        }		
    }
	/*blog realted stuff*/
	public function blog_like_feed_back(){
	    extract($this->input->post());
        $added=$this->Customer_blogs->blog_like_feed_back($customer_id,$blog_id);
        if($added){
          http_response_code(200);
          echo '<i class="fa fa-thumbs-up" style="color:#ff0000"></i>';
            
        }else{
            http_response_code(400);
            echo "Oops! Some error, try after some time.";
        }
	}
    public function add_blogs_comments(){
        //print_r($this->input->post()); exit;
        extract($this->input->post());
        $added=$this->Customer_blogs->add_blogs_comments($customer_image,$customer_id,$blog_id,$customer_name,$message);
        if($added){
          http_response_code(200);
          echo 'Comments Added';
            
        }else{
            http_response_code(400);
            echo "Oops! Some error, try after some time.";
        }
    }
	public function get_blogs_of_tag($tags_in_url){
		$tag=$this->Customer_blogs->get_blog_tags_by_tags_in_url($tags_in_url);
		$tag=urldecode($tag);
        $method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
            
        $data['p_category']=$this->provide_first_level_menu();
        $data["controller"]=$this;
        $data["current_controller"]="search";
        $data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
        $data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
        $data['topic_sel']=$this->Customer_blogs->get_blog_topics();
        $data['tags']=$this->Customer_blogs->get_blog_tags();
        $data['popular_blogs']=$this->Customer_blogs->get_popular_blogs();
        $data['all_blog']=$this->Customer_blogs->get_blogs_of_tag($tags_in_url);
        $data['interest_for']=$tag;
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
        $this->load->view('blog');
        $this->load->view('footer');
    }
    
    public function get_blogs_of_topic($topic){
		$topic=urldecode($topic);
        $method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
            
        $data['p_category']=$this->provide_first_level_menu();
        $data["controller"]=$this;
        $data["current_controller"]="search";
        $data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
        $data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
        $data['topic_sel']=$this->Customer_blogs->get_blog_topics();
        $data['tags']=$this->Customer_blogs->get_blog_tags();
        $data['popular_blogs']=$this->Customer_blogs->get_popular_blogs();
        $data['all_blog']=$this->Customer_blogs->get_blogs_of_topic($topic);
        $data['interest_for']=$topic;
		
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
        $this->load->view('blog');
        $this->load->view('footer');
    }
	public function blog(){
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
            
        $data['p_category']=$this->provide_first_level_menu();
        $data["controller"]=$this;
        $data["current_controller"]="search";
        $data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
        $data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
        $data['topic_sel']=$this->Customer_blogs->get_blog_topics();
        $data['tags']=$this->Customer_blogs->get_blog_tags();
        $data['popular_blogs']=$this->Customer_blogs->get_popular_blogs();
        $data['all_blog']=$this->Customer_blogs->all_blogs();
        $data['interest_for']="";
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
        $this->load->view('blog');
        $this->load->view('footer');
	}
	public function get_blog_post($id){
        $method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
            
        $data['p_category']=$this->provide_first_level_menu();
        $data["controller"]=$this;
        $data["current_controller"]="search";
        $data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
        $data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
        $blog_data=$this->Customer_blogs->get_blog_post($id);
        //print_r($blog_data); 
        if(empty($blog_data)){
                    $this->session->set_flashdata('error','This Blog Does not Exists');
                    redirect(base_url()."blog");
                }
        $data['topic_sel']=$this->Customer_blogs->get_blog_topics();
        $data['tags']=$this->Customer_blogs->get_blog_tags();
        $data['blog_comment_data']=$this->Customer_blogs->get_blog_post_comments_live($blog_data['blog_id']);
        $data['blog_data']=$blog_data;
        $data['popular_blogs']=$this->Customer_blogs->get_popular_blogs();
        $ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
        $this->load->view('blog_post');
        $this->load->view('footer');
    }
	public function get_blog_post_preview($id,$customer_id){
		
	    $method=current_url();
        $this->session->set_userdata("cur_page",$method);
		$data['page_name']='';
		$data['page_type']='';
		$data['page_id']='';
		$data['products_obj']='';
			
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="search";
		$data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
		$data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
        $blog_data=$this->Customer_blogs->get_blog_post_preview($id,$customer_id);
	
        //print_r($blog_data); 
        if(empty($blog_data)){
                    $this->session->set_flashdata('error','This Blog Does not Exists');
                    redirect(base_url()."blog");
                }
				
        $data['topic_sel']=$this->Customer_blogs->get_blog_topics();
		
        $data['tags']=$this->Customer_blogs->get_blog_tags();
		
        $data['blog_comment_data']=$this->Customer_blogs->get_blog_post_comments_live($blog_data['blog_id']);
        $data['blog_data']=$blog_data;
		
        $data['popular_blogs']=$this->Customer_blogs->get_popular_blogs();
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('blog_post_preview');
		$this->load->view('footer');
	}
    /*blog realted stuff ends*/
	public function category($type='',$id=''){
		$category_name="";
		if($type=="cat"){
			if($id!=""){
				$cat=substr($id, 9);
				$category_name=$this->Model_search->get_category_name($cat);
				$category_name=strtolower($category_name);
				$category_name = str_replace(' ', '-', $category_name);
			}
		}
		redirect(site_url("category_details/".$type."/".$category_name));
	}
	public function category_details($type='',$category_name=''){
		$id="";
		if($type=="cat"){
			if($category_name!=""){
				$category_name = str_replace('-', ' ', $category_name);
				$parentcategory_name="";
				$cat=$this->Model_search->get_cat_id_by_category_name($category_name,$parentcategory_name);
				$rand_temp=$this->sample_code();
				$id=$rand_temp.$cat;
			}
		}
		
		//visitor_activity();
		$method=current_url();
		$this->session->set_userdata("products_page",$method);
		
		if($type=='' && $id==''){
			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
		}else{
			if($type=='cat'){
				$id=substr($id, 9);
				$data["ui_arr"]=$this->get_ui_data_for_category_index($id);
				$cat_obj=$this->Customer_frontend->get_category($id);
				if(!empty($cat_obj)){
					$data['page_name']=$cat_obj->cat_name;
					$data['page_type']='cat';
					$data['page_id']=$cat_obj->cat_id;
					$products_obj=$this->Customer_frontend->get_products_under_this_category($id);
					$data['products_obj']=$products_obj;
				}
			}
		}
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="search";
			$data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
			$data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('indexcat');
			$this->load->view('footer');
		
	}
	public function get_products_of($name,$id,$type){
		
		if($type=='cat'){
			$products_obj=$this->Customer_frontend->get_products_under_this_category($id);
		}
		if($type=='parent_cat'){
			
		}
		return $products_obj;
		
	}
	public function index1(){
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="search";
		$data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
		$data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('index');
		$this->load->view('footer');
	}
	public function login($order_id=""){

		////read_and_update_traffic();
		//visitor_activity_analytics();	

		//visitor_activity();
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="search";
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		if($order_id!=""){
			$get_order_placed_date_customer_id_arr=$this->Model_search->get_order_placed_date_customer_id($order_id);
			$order_placed_date=$get_order_placed_date_customer_id_arr["order_placed_date"];
			$order_placed_date_format=date("jS F, Y \a\\t H:i",strtotime($order_placed_date))." Hrs";
			$customer_name=$this->Model_search->get_customer_name($get_order_placed_date_customer_id_arr["customer_id"]);
			$data["customer_name"]=$customer_name;
			$data["order_placed_date_format"]=$order_placed_date_format;
		}
		$data["order_id"]=$order_id;
		
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('login');
		$this->load->view('footer');
		
	}	
	
	public function invalid_url_structure(){
			
		//visitor_activity();
		$method=current_url();
		$this->session->set_userdata("cur_page",$method);
		$data['current_page_url']=$method;
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="search";
			$this->load->model('Front_promotions');
			
			$promotions_at_cart=$this->Front_promotions->get_promotions_at_cart();
			$updated_promotion_cart=array();
			
			if($promotions_at_cart!=false){

			foreach($promotions_at_cart as $key=>$data_val){
				$arrs=[];
				
				$arrs['promo_uid']=$data_val['promo_uid'];
				$arrs['promo_type']=$data_val['promo_type'];
				$arrs['quote_selector']=$data_val['quote_selector'];
				$arrs['promo_name']=$data_val['promo_name'];
				$arrs['to_buy']=$data_val['to_buy'];
				$arrs['to_get']=$data_val['to_get'];
				$arrs['buy_type']=$data_val['buy_type'];
				$arrs['get_type']=$data_val['get_type'];
				$arrs['applied_at_invoice']=$data_val['applied_at_invoice'];
				
			
					$offer = $data_val['promo_quote'];
					if(preg_replace("/\([^)]+\)/","",$offer)){
						$offer = preg_replace("/\([^)]+\)/","",$offer); 
						$offer = preg_replace('/\s\s\s+/', ' ', $offer);
						$arrs['promo_quote']=$offer;
					}else{
						$arrs['promo_quote']=$offer;
					}
				
				
				array_push($updated_promotion_cart,$arrs);
			}
			
			}
			$data["promotions_cart"]=$updated_promotion_cart;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			
			$data["get_shipping_module_info_obj"]=$this->get_shipping_module_info();
			
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('error404');
			$this->load->view('footer');
		
	}
	public function cart(){
		////read_and_update_traffic();
		//visitor_activity_analytics();	
		
		//visitor_activity();
		$method=current_url();
		$this->session->set_userdata("cur_page",$method);
		$data['current_page_url']=$method;
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="search";
			$this->load->model('Front_promotions');
			
			$promotions_at_cart=$this->Front_promotions->get_promotions_at_cart();
			$updated_promotion_cart=array();
			
			if($promotions_at_cart!=false){

			foreach($promotions_at_cart as $key=>$data_val){
				$arrs=[];
				
				$arrs['promo_uid']=$data_val['promo_uid'];
				$arrs['promo_type']=$data_val['promo_type'];
				$arrs['quote_selector']=$data_val['quote_selector'];
				$arrs['promo_name']=$data_val['promo_name'];
				$arrs['to_buy']=$data_val['to_buy'];
				$arrs['to_get']=$data_val['to_get'];
				$arrs['buy_type']=$data_val['buy_type'];
				$arrs['get_type']=$data_val['get_type'];
				$arrs['applied_at_invoice']=$data_val['applied_at_invoice'];
				
			
					$offer = $data_val['promo_quote'];
					if(preg_replace("/\([^)]+\)/","",$offer)){
						$offer = preg_replace("/\([^)]+\)/","",$offer); 
						$offer = preg_replace('/\s\s\s+/', ' ', $offer);
						$arrs['promo_quote']=$offer;
					}else{
						$arrs['promo_quote']=$offer;
					}
				
				
				array_push($updated_promotion_cart,$arrs);
			}
			
			}
			$data["promotions_cart"]=$updated_promotion_cart;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			
			$data["get_shipping_module_info_obj"]=$this->get_shipping_module_info();
			
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('cart');
			$this->load->view('footer');
		
	}
	public function cart_old(){
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="search";
			$this->load->model('Front_promotions');
			
			$promotions_at_cart=$this->Front_promotions->get_promotions_at_cart();
			$updated_promotion_cart=array();
			
			if($promotions_at_cart!=false){

			foreach($promotions_at_cart as $key=>$data_val){
				$arrs=[];
				
				$arrs['promo_uid']=$data_val['promo_uid'];
				$arrs['promo_type']=$data_val['promo_type'];
				$arrs['quote_selector']=$data_val['quote_selector'];
				$arrs['promo_name']=$data_val['promo_name'];
				$arrs['to_buy']=$data_val['to_buy'];
				$arrs['to_get']=$data_val['to_get'];
				$arrs['buy_type']=$data_val['buy_type'];
				$arrs['get_type']=$data_val['get_type'];
				$arrs['applied_at_invoice']=$data_val['applied_at_invoice'];
				
			
					$offer = $data_val['promo_quote'];
					if(preg_replace("/\([^)]+\)/","",$offer)){
						$offer = preg_replace("/\([^)]+\)/","",$offer); 
						$offer = preg_replace('/\s\s\s+/', ' ', $offer);
						$arrs['promo_quote']=$offer;
					}else{
						$arrs['promo_quote']=$offer;
					}
				
				
				array_push($updated_promotion_cart,$arrs);
			}
			
			}
			$data["promotions_cart"]=$updated_promotion_cart;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('cart_backup');
			$this->load->view('footer');
		
	}
	
	public function checkout($flag_payment=""){

		////read_and_update_traffic();
		//visitor_activity_analytics();

		$data['checkout_type']='cart';
		$data["flag_payment"]=$flag_payment;
			$method=current_url();
			$this->session->set_userdata("cur_page",$method);
			$data['current_page_url']=$method;
			//visitor_activity();
			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
			$data["get_all_states_arr"]=$this->Customer_account->get_all_states();
            $customer_id=$this->session->userdata("customer_id");
			
			$count_of_cart=$this->Customer_account->get_count_of_cart($customer_id);
			$this->Customer_account->tracking_temp_dump_checkout_withinsession($customer_id);

			if($count_of_cart==0){
				redirect(base_url());
			}else{
				$data['p_category']=$this->provide_first_level_menu();
				$data["controller"]=$this;
				$data["current_controller"]="search";
				$this->session->unset_userdata("cur_page");
				$data["wallet_amount"]=$this->Customer_account->get_wallet_amount($customer_id); //should uncoomment for wallet payment
				//$data["wallet_amount"]=0; // as of now wallet payment is not needed
				
				$data["customer_mobile"]=$this->Customer_account->get_mobile_db($customer_id);
				$data["customer_email"]=$this->Customer_account->get_email_db($customer_id);
				
				$data["payment_options_obj"]=$this->Customer_account->get_payment_options();
				
				$this->load->model('Front_promotions');
			
			$promotions_at_cart=$this->Front_promotions->get_promotions_at_cart();
			$updated_promotion_cart=array();
			
			if($promotions_at_cart!=false){
				
			foreach($promotions_at_cart as $key=>$data_val){
				$arrs=[];
				
				$arrs['promo_uid']=$data_val['promo_uid'];
				$arrs['promo_type']=$data_val['promo_type'];
				$arrs['quote_selector']=$data_val['quote_selector'];
				$arrs['promo_name']=$data_val['promo_name'];
				$arrs['to_buy']=$data_val['to_buy'];
				$arrs['to_get']=$data_val['to_get'];
				$arrs['buy_type']=$data_val['buy_type'];
				$arrs['get_type']=$data_val['get_type'];
				$arrs['applied_at_invoice']=$data_val['applied_at_invoice'];
				
			
					$offer = $data_val['promo_quote'];
					if(preg_replace("/\([^)]+\)/","",$offer)){
						$offer = preg_replace("/\([^)]+\)/","",$offer); 
						$offer = preg_replace('/\s\s\s+/', ' ', $offer);
						$arrs['promo_quote']=$offer;
					}else{
						$arrs['promo_quote']=$offer;
					}
				
				
				array_push($updated_promotion_cart,$arrs);
			}
			}
			
			$data["promotions_cart"]=$updated_promotion_cart;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			
			$data["order_id"]=$this->Customer_frontend->get_order_id();

			$data["invoice_coupon"]=$this->Customer_account->check_coupon_available_invoice();
			/* added for razorpay */
                $data['callback_url']       = base_url().'razorpay/callback';
                $data['surl']               = base_url()."Account/confirm_order_process";
                $data['furl']               = base_url()."Account/failed_order_process";
                $data['currency_code']      = curr_code;
			/* added for razorpay */

			$this->load->view('header',$data);
			$this->load->view('checkout');
			$this->load->view('footer');
			}
		
	}
	
	public function checkout_combo($flag_payment=""){
		if($this->session->userdata("logged_in")){
		$data['checkout_type']='combo';
		////read_and_update_traffic();
		//visitor_activity_analytics();
		$data["flag_payment"]=$flag_payment;
			$method=current_url();
			$this->session->set_userdata("cur_page",$method);
			$data['current_page_url']=$method;
			//visitor_activity();
			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
			$data["get_all_states_arr"]=$this->Customer_account->get_all_states();
            $customer_id=$this->session->userdata("customer_id");
			
			$this->Customer_account->tracking_temp_dump_checkout_withinsession($customer_id);

			// if($count_of_cart==0){
			// 	redirect(base_url());
			// }else{
		if(1){
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="search";
			$this->session->unset_userdata("cur_page");
			//$data["wallet_amount"]=$this->Customer_account->get_wallet_amount($customer_id); //should uncoomment for wallet payment
			$data["wallet_amount"]=0; // as of now wallet payment is not needed
			
			$data["customer_mobile"]=$this->Customer_account->get_mobile_db($customer_id);
			$data["customer_email"]=$this->Customer_account->get_email_db($customer_id);
				
			$data["payment_options_obj"]=$this->Customer_account->get_payment_options();
			
			$updated_promotion_cart=array();
			
			$data["promotions_cart"]=$updated_promotion_cart;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$get_order_id_Combo=$this->Customer_frontend->get_order_id_for_combo();

			// echo $get_order_id_Combo;
			// exit;
			$data["order_id"]=$get_order_id_Combo;
			$data["invoice_coupon"]=$this->Customer_account->check_coupon_available_invoice();
			/* added for razorpay */
                $data['callback_url']       = base_url().'razorpay/callback';
                $data['surl']               = base_url()."Account/confirm_order_process";
                $data['furl']               = base_url()."Account/failed_order_process";
                $data['currency_code']      = curr_code;
			/* added for razorpay */

			$this->load->view('header',$data);
			$this->load->view('checkout_combo');
			$this->load->view('footer');
		}

		}else{
			$this->session->set_userdata("cur_page", base_url() . "checkout_combo");
			redirect(base_url('login'));
		}
		
	}
	
	
	public function aboutus(){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('aboutus');
			$this->load->view('footer');	
	}
	
	
	
	
	
	
	public function tandc(){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('tandc');
			$this->load->view('footer');	
	}
	public function contactus(){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('contactus');
			$this->load->view('footer');	
	}
	public function contactus_submit(){

        $formData = $this->input->post();

        $name=$formData['name'];
        $email=$formData['email'];
        $subject=$formData['subject'];
        $mobile=$formData['mobile'];
        $message=$formData['message'];
        $this->sendEmail($formData);
        $flag=$this->Model_search->save_contact($name,$email,$subject,$mobile,$message);
        return $flag;
    }
    private function sendEmail($mailData){
        // Load the email library
        $email_stuff_arr=email_stuff();
        // Mail content
        $mailContent = '
            <p><b>Name: </b>'.$mailData['name'].'</p>
            <p><b>Mobile: </b>'.$mailData['mobile'].'</p>
            <p><b>Email: </b>'.$mailData['email'].'</p>
            <p><b>Subject: </b>'.$mailData['subject'].'</p>
            <p><b>Message: </b>'.$mailData['message'].'</p>
        ';

        $msg=
        '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>Contact Request Submitted</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color:color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer"><tr><table style="max-width:500px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;"><h3><u>Customer contact Query</u></h3>'.$mailContent.'<br></td></tr>';
		
		$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

		$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

		$usermessage.='</tbody> </table></tr></table></center> </body></html>';

        //$target_email_cc1="nanthinivinothkumar0227@gmail.com";
        $target_email_cc1=SITE_ADMIN_EMAIL;
        $target_email_cc_name1="Admin";

        $mail = new PHPMailer;
        /*$mail->isSMTP();
        $mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
        $mail->Port = '587';//  live - comment it
        $mail->Auth = true;//  live - comment it
        $mail->SMTPAuth = true;
        $mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it
        $mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it
        $mail->SMTPSecure = 'tls';*/
        $mail->From = $email_stuff_arr["fromemail_emailtemplate"];
        $mail->FromName = $email_stuff_arr["name_emailtemplate"];
        //$mail->addAddress($email);
        $mail->addAddress($mailData['email']);
        $mail->AddCC($target_email_cc1, $target_email_cc_name1);
       // $mail->AddCC($target_email_cc2, $target_email_cc_name2);
        $mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
        $mail->WordWrap = 50;
        $mail->isHTML(true);
        $mail->Subject = 'Customer Contact Request from '.$mailData['name'];
        $mail->Body    = $msg;

        $flag=@$mail->send();



        $mobile_stuff_arr=mobile_stuff();
        $authKey = $mobile_stuff_arr["authKey_smstemplate"];
        $mobileNumber = "+91".$mailData['mobile'];
        $senderId = $mobile_stuff_arr["senderId_smstemplate"];

        $sms_message="Thanks for Contacting us.\n Admin will contact you shortly.";
        //$sms_message.="Name : ".$mailData['name']."\n";
        //$sms_message.="Mobile : ".$mailData['mobile']."\n";
        $message = urlencode($sms_message);
        $route = "4";
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );
        $url="https://control.msg91.com/api/sendhttp.php";
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $output = curl_exec($ch);

        if(curl_errno($ch)){
           // echo 'error:' . curl_error($ch);
        }
        curl_close($ch);


        return true;
        // Send email & return status
    }
	public function privacypolicy(){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('privacypolicy');
			$this->load->view('footer');	
	}
	
	public function termsandconditions(){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('termsandconditions');
			$this->load->view('footer');	
	}
	public function intellectualproperty(){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('intellectualproperty');
			$this->load->view('footer');	
	}
	public function sellthroughus(){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('sellthroughus');
			$this->load->view('footer');	
	}
	public function add_partner(){
		$email_stuff_arr=email_stuff();
		$details_arr=$this->input->post();
		extract($details_arr);

		$add_partner_arr=$this->Customer_frontend->add_partner($details_arr);
		$flag=$add_partner_arr["status"];

		if($flag){
			
			
				$email_s=$email_stuff_arr["fromemail_emailtemplate"];
			
						//through Email
				if($email!=''){
					$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>Thank You Note</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color:color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer"><tr><table style="max-width:500px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;"><h3><u>New Partner Query</u></h3><p>Name: <b>'.$name.'</b></p><p>Company Name: <b>'.$company_name.'</b></p><p>City: <b>'.$city.'</b></p><p>Interested In : <b>'.$interested_in.'</b></p><p>Email: <b>'.$email.'</b></p><p>Mobile : <b>'.$mobile.'</b></p><p>Comments : <b>'.$comment.'</b></p></p><br></td></tr>';
					
					$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

					$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

					$usermessage.='</tbody> </table></tr></table></center> </body></html>';
				   
					
					$mail = new PHPMailer;
					/*$mail->isSMTP();
					$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
					$mail->Port = '587';//  live - comment it 
					$mail->Auth = true;//  live - comment it 
					$mail->SMTPAuth = true;
					$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
					$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
					$mail->SMTPSecure = 'tls';*/
					$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
					$mail->FromName = $email_stuff_arr["name_emailtemplate"];
					//$mail->addAddress($email);
					$mail->addAddress($email_s);
					//$mail->AddCC("rajitkumar1974@gmail.com", "Rajit Kumar");               
					//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
					$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
					$mail->WordWrap = 50;
					$mail->isHTML(true);
					$mail->Subject = 'New Parter Query';
					$mail->Body    = $usermessage;
							
					@$mail->send();
					//$flag=$this->Customer_account->sended_code_to_mail($email,$rand);
					//echo $flag;
					
				}
			
		}
		echo json_encode($add_partner_arr);
	}
	public function error_to_view(){
			//visitor_activity();
			$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('error');
			$this->load->view('footer');	
	}
	public function quick_view(){
		$this->load->view('quick_view');
		exit;	
	}
	
	public function provide_parent_category()
	{
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category'] = $this->Customer_frontend->provide_parent_category();
		$data["controller"]=$this;
		$data["current_controller"]="search";
		$this->load->view('header',$data);
	}

	
	public function get_list_of_filters($p1,$p2,$p3)
	{

		$arr=array();
		$pcat_id=substr($p1, 9);$cat_id=substr($p2, 9);$subcat_id=substr($p3, 9);
		
		if(!empty($pcat_id)){
			if(!ctype_digit($pcat_id))
			{
				redirect(base_url()."error_to_view");
			}
		}
		if(!empty($cat_id)){
			if(!ctype_digit($cat_id))
			{
				redirect(base_url()."error_to_view");
				
			}
		}
		if(!empty($subcat_id)){
			if(!ctype_digit($subcat_id))
			{
				redirect(base_url()."error_to_view");
			}
		}
		if(!empty($pcat_id) && is_numeric($pcat_id))
		{
				$arr['pcat_id']=$pcat_id;
		}
		
		if(!empty($cat_id) && is_numeric($cat_id))
		{
				$arr['cat_id']=$cat_id;
		}
		
		if(!empty($subcat_id) && is_numeric($subcat_id))
		{	
				$arr['subcat_id']=$subcat_id;
		}
		
		$data=$this->Customer_frontend->get_list_of_filters($arr);

		usort($data, array($this, "cmp"));
		return $data;
	}
	public function get_count_of_products($f_id)
	{
		$data=$this->Customer_frontend->get_count_of_products($f_id);
		return $data;
	}
	public function get_list_of_filter_options($filter_box_id)
	{
		$data=$this->Customer_frontend->get_list_of_filter_options($filter_box_id);
		return $data;
	}
	
	public function to_return_ids($p1,$p2,$p3)
	{
		$arr=array();
		$pcat_id=substr($p1, 9);$cat_id=substr($p2, 9);$subcat_id=substr($p3, 9);
		
		if(!empty($pcat_id)){
			if(!ctype_digit($pcat_id))
			{
				redirect(base_url()."error_to_view");
			}
		}
		if(!empty($cat_id)){
			if(!ctype_digit($cat_id))
			{
				redirect(base_url()."error_to_view");
				
			}
		}
		if(!empty($subcat_id)){
			if(!ctype_digit($subcat_id))
			{
				redirect(base_url()."error_to_view");
			}
		}
		if(!empty($pcat_id) && is_numeric($pcat_id))
		{
				$arr['pcat_id']=$pcat_id;
		}
		
		if(!empty($cat_id) && is_numeric($cat_id))
		{
				$arr['cat_id']=$cat_id;
		}
		
		if(!empty($subcat_id) && is_numeric($subcat_id))
		{	
				$arr['subcat_id']=$subcat_id;	
		}
		$data=$this->to_return_products($arr);
		
		return $data;
	}
	
	public function to_return_products($arr)
	{
		$ids = implode(', ',$arr);

		$data=$this->Customer_frontend->to_return_products($arr);
		
		return $data;

	}
	
	
	
	
	public function cmp($a, $b)
	{
		return strcmp($a->sort_order, $b->sort_order);
	}
	
	// added 3_8_16///
	public function get_cart_table_details()
	{
		$data = json_decode(file_get_contents("php://input"));
		if(isset($data->customer_id))
		{
			$customer_id=$data->customer_id;
			$data_obj=$this->Customer_frontend->get_cart_table_details($customer_id);
			//print $data_obj;
			print json_encode($data_obj);
		}
	}
	
	public function get_all_addresses_customer(){
		
			$customer_id=$this->session->userdata("customer_id");
			$this->load->model("Customer_account");
			$all_addresses=$this->Customer_account->get_all_customer_addresses($customer_id);
			return $all_addresses;
		
	}
	
	
	public function set_this_address_default($id){
		$shipping_address_id=$id;
		$customer_id=$this->session->userdata("customer_id");
		$this->load->model("Customer_account");
		$updated=$this->Customer_account->set_this_address_default($shipping_address_id,$customer_id);
		echo $updated;
	}
	public function delete_this_address($id){
		$customer_address_id=$id;
		$this->load->model("Customer_account");
		$customer_id=$this->session->userdata("customer_id");
		$updated=$this->Customer_account->delete_this_address($customer_address_id,$customer_id);
		echo $updated;
	}
	public function failed($order_id){

		//$data['p_category'] = $this->Customer_frontend->provide_parent_category();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
		$data['current_page_url']=$method;
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="search";
		$data["order_id"]=$order_id;
		$this->Customer_frontend->dump_failed_order_id($order_id);
		$data["order_summary_mail_data_arr"]=$this->Customer_account->get_order_summary_mail_data($order_id);
		
		if(!empty($data["order_summary_mail_data_arr"])){
			$data["shipping_address_id"]=$this->Customer_account->get_shipping_address_id($order_id);
			$data["shipping_address_table_data_arr"]=$this->Customer_account->get_shipping_address($data["shipping_address_id"]);
			$data['invoice_offers']=$this->Customer_account->get_all_order_invoice_offers($order_id);
			$data['order_id']=$order_id;
		}
		
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$data["combo_order"]=0;
		$this->load->view('header',$data);
		
		$this->load->view('failed',$data);
		$this->load->view('footer');
	}
	public function failed_combo($order_id){

		//$data['p_category'] = $this->Customer_frontend->provide_parent_category();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
		$data['current_page_url']=$method;
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="search";
		$data["order_id"]=$order_id;
		$this->Customer_frontend->dump_failed_order_id($order_id);
		$data["order_summary_mail_data_arr"]=$this->Customer_account->get_order_summary_mail_data($order_id);
		
		if(!empty($data["order_summary_mail_data_arr"])){
			$data["shipping_address_id"]=$this->Customer_account->get_shipping_address_id($order_id);
			$data["shipping_address_table_data_arr"]=$this->Customer_account->get_shipping_address($data["shipping_address_id"]);
			$data['invoice_offers']=$this->Customer_account->get_all_order_invoice_offers($order_id);
			$data['order_id']=$order_id;
		}
		
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();

		$data["combo_order"]=1;

		$this->load->view('header',$data);
		
		$this->load->view('failed',$data);
		$this->load->view('footer');
	}
	
	/*public function thankyou_contactus(){
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
		$data['current_page_url']=$method;
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('thankyou-contactus');
			$this->load->view('footer');	
	}*/
	public function thankyou($order_id){

		//$data['p_category'] = $this->Customer_frontend->provide_parent_category();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
		$data['current_page_url']=$method;
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="search";
		$data["order_id"]=$order_id;
		$data["order_summary_mail_data_arr"]=$this->Customer_account->get_order_summary_mail_data($order_id);
		
                /*echo '<pre>';
                
                print_r($data["order_summary_mail_data_arr"]);
                echo '</pre>';*/
                
		if(!empty($data["order_summary_mail_data_arr"])){
			$data["shipping_address_id"]=$this->Customer_account->get_shipping_address_id($order_id);
			$data["shipping_address_table_data_arr"]=$this->Customer_account->get_shipping_address($data["shipping_address_id"]);
			$data['invoice_offers']=$this->Customer_account->get_all_order_invoice_offers($order_id);
			$data['order_id']=$order_id;
		}
		
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		
		$this->load->view('thankyou',$data);
		$this->load->view('footer');
	}
	public function get_order_grand_total_from_active($id){
			$grandtotal=$this->Customer_account->get_order_grand_total_from_active($id);
			$total_order_val=0;
			foreach($grandtotal as $total){
				$total_order_val+=$total['grandtotal'];
			}
			return $total_order_val;
	}
	
	public function get_free_skus_from_inventory_with_details($promo_item,$promotion_item_num){
		
		$promo_item=rtrim($promo_item,',');
		$promotion_item_num=rtrim($promotion_item_num,',');
		
		$promo_item_arr = explode(',', $promo_item);
		$promo_item_num_arr = explode(',', $promotion_item_num);
		
		$two=array_combine($promo_item_arr,$promo_item_num_arr);
		
		$prmo_item_res=$this->Customer_account->get_free_skus_from_inventory($promo_item);
		$str_entire=array();

		return $prmo_item_res;
		
	}
	public function get_order_placed_date($id){
		return $this->Customer_account->get_order_placed_date($id);
	}
	public function get_inventory_details($inventory_id){
		return $this->Customer_account->get_inventory_details($inventory_id);
	}
	public function get_ui_data_for_index(){
		$res=array();
		$bg_img=$this->Customer_frontend->bg_image_index();
		$res['bg_img']=$bg_img;
		
		$ft_img=$this->Customer_frontend->foot_image_index();
		$res['ft_img']=$ft_img;
		
		$testi=$this->Customer_frontend->testimonial_index();
		$res['testi']=$testi;
		
		$invs_our_products=$this->Customer_frontend->inv_index("Quick Buy");
		$res['invs_ourproducts']=$invs_our_products;

		$p_category= $this->Customer_frontend->provide_parent_category();
		
		$invs_most_purchased=$this->Customer_frontend->inv_index("Most Purchased");
		$res['invs_mostpurchased']=$invs_most_purchased;
	
		$invs_shop_by_parentcategory=$this->Customer_frontend->brand_index_shopbyparentcategory("Shop by Parent Category");
		$res['invs_shopbyparentcategory']=$invs_shop_by_parentcategory;
		
		$invs_shop_by_brand=$this->Customer_frontend->brand_index_shopbybrand("Shop by Brand");
		$res['invs_shopbybrand']=$invs_shop_by_brand;
		
		
		$invs_shop_by_category=$this->Customer_frontend->brand_index_shopbycategory("Shop by Category");
		$res['invs_shopbycategory']=$invs_shop_by_category;
		
		$invs_knockout_deals=$this->Customer_frontend->inv_index("Knockout Deals");
		$res['invs_knockoutdeals']=$invs_knockout_deals;
		
		$invs_trending_products=$this->Customer_frontend->inv_index("Trending Products");
		$res['invs_trendingproducts']=$invs_trending_products;
		
		$invs_deal_of_the_day=$this->Customer_frontend->inv_index("Deal Of the day");
		$res['invs_dealoftheday']=$invs_deal_of_the_day;
		
		$site_des=$this->Customer_frontend->site_description();
		$res['site_des']=$site_des;
		
		$frontend_blogs=$this->Customer_frontend->post_to_frontend_blogs();
		$res['post_to_frontend_blogs']=$frontend_blogs;
		
		return $res;
		
	}
	public function get_ui_data_for_category_index($cat_id){
		$res=array();
		$bg_img=$this->Customer_frontend->bg_image_cat_index($cat_id);
		$res['bg_img']=$bg_img;
		
		$ft_img=$this->Customer_frontend->foot_image_index();
		$res['ft_img']=$ft_img;
		
		$testi=$this->Customer_frontend->testimonial_cat_index($cat_id);
		$res['testi']=$testi;
		
		$invs=$this->Customer_frontend->inv_cat_index($cat_id);
		$res['invs']=$invs;

		return $res;
		
	}
	/* user activity */
	public function dump_section_activity(){
		//$localstorage_data = json_decode(file_get_contents("php://input"));
		$localstorage_data = (file_get_contents("php://input"));
		extract($this->input->post());
		$section_activity;
		$current_page;
		
		$data='{"'.time().'":'.$section_activity.",".'"page"'.":".'"'.$current_page.'"}';
		$userID=get_cookie('userID');
		
		$customer_id=$this->session->userdata("customer_id");
		if(isset($customer_id) && $customer_id!=''){
			$user_type=$customer_id;
		}else{
			$user_type="guest";
		}
		
		$flag=$this->Customer_frontend->update_section_activity($data,$userID,$user_type);
		echo $flag;
	}
	public function dump_btn_activity(){
		$localstorage_data = (file_get_contents("php://input"));
		extract($this->input->post());
		$btn_activity;
		
		//$data='{"'.time().'":'.$btn_activity."}";
		$data=$btn_activity;
		$userID=get_cookie('userID');
		
		$customer_id=$this->session->userdata("customer_id");
		if(isset($customer_id) && $customer_id!=''){
			$user_type=$customer_id;
		}else{
			$user_type="guest";
		}
		$flag=$this->Customer_frontend->user_btn_activity($data,$userID,$user_type);
		echo $flag;
	}
	
	public function dump_reviews_read(){
		$localstorage_data = (file_get_contents("php://input"));
		extract($this->input->post());
		$reviews;
		
		$data='{"'.time().'":'.$reviews."}";
		$userID=get_cookie('userID');
		
		$customer_id=$this->session->userdata("customer_id");
		if(isset($customer_id) && $customer_id!=''){
			$user_type=$customer_id;
		}else{
			$user_type="guest";
		}
		$flag=$this->Customer_frontend->user_reviews_read($data,$userID,$user_type);
		echo $flag;
	}
	
	public function dump_filter_checked(){
		$localstorage_data = (file_get_contents("php://input"));
		extract($this->input->post());
		$filter;
		
		$data='{"'.time().'":'.$filter."}";
		$userID=get_cookie('userID');
		
		$customer_id=$this->session->userdata("customer_id");
		if(isset($customer_id) && $customer_id!=''){
			$user_type=$customer_id;
		}else{
			$user_type="guest";
		}
		$flag=$this->Customer_frontend->user_filter_checked($data,$userID,$user_type);
		echo $flag;
	}
	/* user activity */
	public function test_sample($arr){
		$req_page='['.$arr.']';	
		echo '<pre>';
		$req_page=json_decode($req_page,true);
		echo count($req_page);
		print_r($req_page);
		echo '</pre>';
	}
	public function test_sample_2(){
		
		$sql="select * from visitor_data limit 1";
		$res=$this->db->query($sql);

		$user_behaviour=$res->row_array();
		$user_behaviour_per_page=$user_behaviour["user_behaviour"];
		$user_behaviour_request_page=$user_behaviour["request_page"];
		$user_behaviour_btn_activity=$user_behaviour["user_btn_activity"];
		$log_actitivity=$user_behaviour["log_actitivity"];
		$reviews_read=$user_behaviour["reviews_read"];
		$filter_selection=$user_behaviour["filter_selection"];
		
		$this->test_sample($user_behaviour_request_page);
		
		$user_behaviour = ltrim($user_behaviour_per_page, ', ');
		$user_behaviour='['.$user_behaviour.']';	
		echo '<pre>';
		$user_behaviour=json_decode($user_behaviour,true);
		echo count($user_behaviour);
		print_r($user_behaviour);
		echo '</pre>';
		
		$btn_activity = ltrim($user_behaviour_btn_activity, ', ');
		$btn_activity='['.$btn_activity.']';	
		echo '<pre>';
		$btn_activity=json_decode($btn_activity,true);
		echo count($btn_activity);
		print_r($btn_activity);
		echo '</pre>';
		
		$reviews_read = ltrim($reviews_read, ', ');
		$reviews_read='['.$reviews_read.']';	
		echo '<pre>';
		$reviews_read=json_decode($reviews_read,true);
		echo count($reviews_read);
		print_r($reviews_read);
		echo '</pre>';
		
		$filter_selection = ltrim($filter_selection, ', ');
		$filter_selection='['.$filter_selection.']';	
		echo '<pre>';
		$filter_selection=json_decode($filter_selection,true);
		echo count($filter_selection);
		print_r($filter_selection);
		echo '</pre>';
		
		$log_actitivity = ltrim($log_actitivity, ', ');
		$log_actitivity='['.$log_actitivity.']';	
		echo '<pre>';
		$log_actitivity=json_decode($log_actitivity,true);
		echo count($log_actitivity);
		print_r($log_actitivity);
		echo '</pre>';

		exit;	
	}
	public function test_dump(){
		if(($handle = fopen("assets/test1.csv" , "r")) !== FALSE) 
						{
							$count_log=0;
							$count=0;
							while (($data = fgetcsv($handle)) !== FALSE) 
							{
								if($data!=0){
									$count++;
									if($count!=1){
										
										///////////////////
										//generate a user_id
										$date = new DateTime();
										$mt=microtime();
										$salt="business";
										$rand=mt_rand(1, 1000000);
										$ts=$date->getTimestamp();
										$str=$salt.$rand.$ts.$mt;
										$key=md5($str);
										$user_id=$key;
										//print_r($data);
										////////////////////
										
										$bank_name=trim(addslashes($data[0]));
										$ifsc_code=trim(addslashes($data[1]));
										$branch=trim(addslashes($data[2]));
										$address=trim(addslashes($data[3]));
										$city=trim(addslashes($data[4]));
										$district=trim(addslashes($data[5]));
										$state=trim(addslashes($data[6]));
										if($bank_name=="" && $ifsc_code=="" && $branch=="" && $address=="" && $city=="" && $district=="" && $state==""){
											continue;
										}
										$sql="insert into ifsc_codes set bank_name='$bank_name',ifsc_code='$ifsc_code',branch='$branch',address='$address',city='$city',district='$district',state='$state'";
										$this->db->query($sql);
								}
							}
						}
					}
	}
	
	
	
public function policy(){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('policy');
			$this->load->view('footer');	
	}

	public function business_policy(){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('business_policy');
			$this->load->view('footer');	
	}
	
	
	public function shipping(){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('shipping');
			$this->load->view('footer');	
	}
	
	public function payments(){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('payments');
			$this->load->view('footer');	
	}
	
	public function warranty(){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('warranty');
			$this->load->view('footer');	
	}
	
	public function faq(){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('faq');
			$this->load->view('footer');	
	}
	
	
	
	public function delivery(){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('delivery');
			$this->load->view('footer');	
	}
	
	public function replacements(){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$data["returns"]="replacements";
			$this->load->view('header',$data);
			$this->load->view('returns');
			$this->load->view('footer');	
	}
	
	public function returns($flag=""){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$data["flag"]=$flag;
			$this->load->view('header',$data);
			$this->load->view('returns');
			$this->load->view('footer');	
	}
	
	public function whylivingseed($type='',$id=''){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('whylivingseed');
			$this->load->view('footer');
		
	}
	
	
	
		function enquiries_server()
		{
			if(isset($_REQUEST))
			{
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data))
				{
					$email_stuff_arr=email_stuff();
					$connect_name=$data->connect_name;
					$connect_mobile=$data->connect_mobile;
					$connect_email=$data->connect_email;
					
					$target_mobile="8050581532";
					$target_mobile_cc1="8197569620";
					$target_mobile_cc2="7899439925";
					
					$target_email="rajit@axonlabs.in";
					$target_email_name=$email_stuff_arr["name_emailtemplate"];
					$target_email_cc1=SITE_ADMIN_EMAIL;
					$target_email_cc_name1="Karan";
					$target_email_cc2="shareeshk@gmail.com";
					$target_email_cc_name2="Shareesh";
					
					/*
					$target_mobile="7899439925";
					$target_mobile_cc1="7975591327";
					$target_mobile_cc2="9844597474";
					
					$target_email="shareeshk@gmail.com";
					$target_email_name="Shareesh";
					$target_email_cc1="manjurshv@gmail.com";
					$target_email_cc_name1="Manju";
					$target_email_cc2="shareeshkumar.k@gmail.com";
					$target_email_cc_name2="Shareeshkumar";
					*/
					
					if($connect_mobile != '' ){
						
						
						$mobile_stuff_arr=mobile_stuff();
						$authKey = $mobile_stuff_arr["authKey_smstemplate"];
						$mobileNumber = "+91".$target_mobile;
						$senderId = $mobile_stuff_arr["senderId_smstemplate"];
						
						$sms_message="New TGP Enquiry\n";
						$sms_message.="Name : ".$connect_name."\n";
						$sms_message.="Mobile : ".$connect_mobile."\n";
						$message = urlencode($sms_message);
						$route = "4";
						$postData = array(
							'authkey' => $authKey,
							'mobiles' => $mobileNumber,
							'message' => $message,
							'sender' => $senderId,
							'route' => $route
						);

						$url="https://control.msg91.com/api/sendhttp.php";
						$ch = curl_init();
						curl_setopt_array($ch, array(
							CURLOPT_URL => $url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_POST => true,
							CURLOPT_POSTFIELDS => $postData
						));
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

						//$output = curl_exec($ch);

						if(curl_errno($ch))
						{
							echo 'error:' . curl_error($ch);
						}else{
							//no error - code has been sent
							$this->Model_search->enquiries_server($connect_name,$connect_mobile,$connect_email);	
						}

						curl_close($ch);
						
						//////////////////////////////////////////
						
						
						
						
						
						$mobile_stuff_arr=mobile_stuff();
						$authKey = $mobile_stuff_arr["authKey_smstemplate"];
						$mobileNumber = "+91".$target_mobile_cc1;
						$senderId = $mobile_stuff_arr["senderId_smstemplate"];
						
						$sms_message="New TGP Enquiry\n";
						$sms_message.="Name : ".$connect_name."\n";
						$sms_message.="Mobile : ".$connect_mobile."\n";
						$sms_message.="Email : ".$connect_email."\n";
						$message = urlencode($sms_message);
						$route = "4";
						$postData = array(
							'authkey' => $authKey,
							'mobiles' => $mobileNumber,
							'message' => $message,
							'sender' => $senderId,
							'route' => $route
						);

						$url="https://control.msg91.com/api/sendhttp.php";
						$ch = curl_init();
						curl_setopt_array($ch, array(
							CURLOPT_URL => $url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_POST => true,
							CURLOPT_POSTFIELDS => $postData
						));
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

						//$output = curl_exec($ch);

						if(curl_errno($ch))
						{
							echo 'error:' . curl_error($ch);
						}else{
							//no error - code has been sent
							
						}

						curl_close($ch);
					
						
						
						
						
						
						$mobile_stuff_arr=mobile_stuff();
						$authKey = $mobile_stuff_arr["authKey_smstemplate"];
						$mobileNumber = "+91".$target_mobile_cc2;
						$senderId = $mobile_stuff_arr["senderId_smstemplate"];
						
						$sms_message="New TGP Enquiry\n";
						$sms_message.="Name : ".$connect_name."\n";
						$sms_message.="Mobile : ".$connect_mobile."\n";
						$message = urlencode($sms_message);
						$route = "4";
						$postData = array(
							'authkey' => $authKey,
							'mobiles' => $mobileNumber,
							'message' => $message,
							'sender' => $senderId,
							'route' => $route
						);

						$url="https://control.msg91.com/api/sendhttp.php";
						$ch = curl_init();
						curl_setopt_array($ch, array(
							CURLOPT_URL => $url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_POST => true,
							CURLOPT_POSTFIELDS => $postData
						));
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

						$output = curl_exec($ch);

						if(curl_errno($ch))
						{
							echo 'error:' . curl_error($ch);
						}else{
							//no error - code has been sent
							
						}

						curl_close($ch);
					
						
						
						
						
					}//sms
					
					
					///////////// email starts ////
					
					
			$email_message="New TGP Enquiry<br>";
			$email_message.="Name : ".$connect_name."<br>";
			$email_message.="Mobile : ".$connect_mobile."<br>";	
			$email_message.="Email : ".$connect_email."<br>";	
								
			$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr>
			
			<tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Dear Administrator, </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">'.$email_message.'</p><br></td></tr><tr><td style="background-color:#fff;"></td></tr>';
			
			$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

			$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';
			
			$usermessage.='</tbody> </table></tr></table></center> </body></html>';
			
			
					
					$mail = new PHPMailer;
					/*$mail->isSMTP();
					$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
					$mail->Port = '587';//  live - comment it 
					$mail->Auth = true;//  live - comment it 
					$mail->SMTPAuth = true;
					$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
					$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
					$mail->SMTPSecure = 'tls';*/
					$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
					$mail->FromName = $email_stuff_arr["name_emailtemplate"];
					$mail->addAddress($target_email);
					$mail->AddCC($target_email_cc1, $target_email_cc_name1);               
					$mail->AddCC($target_email_cc2, $target_email_cc_name2);               
					//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
					$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
					$mail->WordWrap = 50;
					$mail->isHTML(true);
					$mail->Subject = 'New Enquiry';
					$mail->Body    = $usermessage;
					if($target_email != ''){		
							if($mail->send()){
								echo true;
							}
							else{
								echo false;
							}
					}
					else{
							echo false;
						}
					///// email ends ///////////
					
				}
			}
		}
		
		public function get_shipping_module_info(){
			$get_shipping_module_info_arr=$this->Model_search->get_shipping_module_info();
			return $get_shipping_module_info_arr;
		}
		
		public function test_ganesh(){
			$ganesh_text=$this->input->post("ganesh_text");
			$status=$this->Customer_frontend->test_ganesh($ganesh_text);
			echo $status;
		}
		
		
		/* I am adding from live copy of samrick starts **/
		//testing_search_delete_run1
	//testing_search
	//testing_search_index
	//testing_pcat_Cat
	//testing_cat_subCat
	
	
	

		
		
		public function thankyou_contactus($appointment_record_unique_id){

		//$data['p_category'] = $this->Customer_frontend->provide_parent_category();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
       
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="search";
		
		
		
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		
		$data["patient_name"]="Sir";
		
		$data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
		
		
		$this->load->view('header',$data);
		
		$this->load->view('thankyou-contactus',$data);
		$this->load->view('footer');
	}
	
	
	
	
	
	
	
	function webinar_registration($webinar_title)
		{
			$webinar_title=urldecode($webinar_title);
			if(isset($_REQUEST))
			{
				
				$data = json_decode(file_get_contents("php://input"));
				if(isset($data))
				{
					
					$name=($data->name);
					$email=($data->email);
					$mobile=($data->mobile);
					
					
					if(isset($data->postal_code)){
						$postal_code=$data->postal_code;
					}
					else{
						$postal_code="";
					}
					
					
					
					
					
					
					///////////////////
										//generate a user_id
										$date = new DateTime();
										$mt=microtime();
										$salt="business";
										$rand=mt_rand(1, 1000000);
										$ts=$date->getTimestamp();
										$str=$salt.$rand.$ts.$mt;
										$key=md5($str);
										$webinar_registration_unique_id=$key;
										//print_r($data);
										////////////////////
										
										
									$webinar_registration_arr=$this->Model_search->webinar_registration($webinar_title,$name,$postal_code,$email,$mobile,$webinar_registration_unique_id);
									
				
					$flag_admin_sent=false;	
					$flag_webinar_sent=false;
					
					///////////// email starts ////
					//$target_email=$email;
					
					
			//$email_message="New Enquiry<br>";
			$email_message="Webinar Title : ".$webinar_title."<br>";	
			$email_message.="Name : ".$name."<br>";
			$email_message.="Email : ".$email."<br>";	
			$email_message.="Mobile : ".$mobile."<br>";	
			
			if($postal_code!=""){
				$email_message.="Postal Code : ".$postal_code."<br>";	
			}				
			
			$email_stuff_arr=email_stuff();
			
			
			$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr>
			
			<tr> <td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">'.$email_message.'</p><br></td></tr>';

			$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

			$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';
				
			$usermessage.='</tbody> </table></tr></table></center> </body></html>';
			
					$target_email="rajit@axonlabs.in";

					$mail = new PHPMailer;
					/*$mail->isSMTP();
					$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
					$mail->Port = '587';//  live - comment it 
					$mail->Auth = true;//  live - comment it 
					$mail->SMTPAuth = true;
					$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
					$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
					$mail->SMTPSecure = 'tls';*/
					$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
					$mail->FromName = $email_stuff_arr["name_emailtemplate"];
					$mail->addAddress($target_email);              
					//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
					$mail->AddBCC("sucheta@axonlabs.in", "Sucheta");
					$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
					$mail->WordWrap = 50;
					$mail->isHTML(true);
					$mail->Subject =  'New Webinar Registration';
					$mail->Body    = $usermessage;	


					if($target_email != ''){		
							if($mail->send()){
								$flag_admin_sent=true;	
								
							}
							else{
								$flag_admin_sent=false;	
							}
					}
					else{
							$flag_admin_sent=false;	
						}
					///// email ends ///////////
					
					
					
					
					
						
					///////////// email to webinar starts ////
					
					//$target_email="shareeshk@gmail.com";
					
			
			$email_message="Dear ".$email.",<br><br>Your attendance at SAMRICK Webinar is confirmed. 
A Zoom invite shall be sent to your email ID 24 hours before the webinar followed by a reminder on WhatsApp.<br><br>See you soon at our Webinar!
<br>Thank you for trusting SAMRICK.<br><br>- Team Samrick";
			
								
			
			$email_stuff_arr=email_stuff();
			
			
			$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr>
			
			<tr> <td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">'.$email_message.'</p><br></td></tr>';
		
			
			$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

			$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';
				
			$usermessage.='</tbody> </table></tr></table></center> </body></html>';
			
			
					$mail = new PHPMailer;
					/*$mail->isSMTP();
					$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
					$mail->Port = '587';//  live - comment it 
					$mail->Auth = true;//  live - comment it 
					$mail->SMTPAuth = true;
					$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
					$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
					$mail->SMTPSecure = 'tls';*/
					$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
					$mail->FromName = $email_stuff_arr["name_emailtemplate"];
					$mail->addAddress($email);              
					$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
					$mail->AddBCC("sucheta@axonlabs.in", "Sucheta");
					$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
					$mail->WordWrap = 50;
					$mail->isHTML(true);
					$mail->Subject =  'SAMRICK - Webinar Registration Confirmation for '."'".$webinar_title."'";
					$mail->Body    = $usermessage;	

					if($mail->send()){
						$flag_webinar_sent=true;
					}
					///////////// email to webinar ends ////
					
					$webinar_registration_arr["flag_webinar_sent"]=$flag_webinar_sent;
					$webinar_registration_arr["flag_admin_sent"]=$flag_admin_sent;
					echo json_encode($webinar_registration_arr);
					
				}
			}
		}
		
		public function thankyou_webinar($webinar_registration_unique_id){

			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
			
			//$data['p_category'] = $this->Customer_frontend->provide_parent_category();
			$method=current_url();
			$this->session->set_userdata("cur_page",$method);
		   
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="search";
			
			
			
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$registered_name=$this->Model_search->get_registered_name_by_webinar_registration_unique_id($webinar_registration_unique_id);
			$data["registered_name"]=$registered_name;
			$this->load->view('header',$data);
			
			$this->load->view('thankyou-webinar',$data);
			$this->load->view('footer');
		}
		
		
		
		
		
		public function thankyou_partner($partner_unique_id){

			//$data['p_category'] = $this->Customer_frontend->provide_parent_category();
			$method=current_url();
			$this->session->set_userdata("cur_page",$method);
		   
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="search";
			
			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
		
			
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$partner_name=$this->Model_search->get_partner_name_by_partner_unique_id($partner_unique_id);
			$data["partner_name"]=$partner_name;
			$this->load->view('header',$data);
			
			$this->load->view('thankyou-partner',$data);
			$this->load->view('footer');
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		public function whyvoometstudioproducts(){
		//visitor_activity();
		$method=current_url();
        $this->session->set_userdata("cur_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
			$data["current_controller"]="search";
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('whysamrick');
			$this->load->view('footer');
		
	}
	
	public function testtt(){
		echo "xyz";
	}
		
		/* I am adding from live copy of samrick ends **/
		
		
		
		public function testcal(){
			$sql="select id,tax_percent,selling_price,moq from inventory";
			$result=$this->db->query($sql);
			$res=$result->result_array();
			foreach($res as $xarr){
				$tax_percent=$xarr["tax_percent"];
				$selling_price=$xarr["selling_price"];
				$moq=$xarr["moq"];
				$inventory_id=$xarr["id"];
				$base_price=round(($selling_price*100)/(100+$tax_percent));
				$moq_base_price=$base_price*$moq;
				$sql1="update inventory set moq_base_price='$moq_base_price',base_price='$base_price' where id='$inventory_id'";
				$result1=$this->db->query($sql1);
			}
		}
		
		public function get_brand_name($brand_id){
			$brand_name=$this->Model_search->get_brand_name($brand_id);
			return $brand_name;
		}
		
		///////// No of offers display in catalog page starts /////////////////
		
		public function get_number_of_offers($inventory_id){
			$product_id=$this->Front_promotions->get_product_id($inventory_id);
			$promo_array=array();$promo_uid_arr=array();
			if(!empty($product_id)){
				$a_TO_z_ids=$this->Front_promotions->get_a_TO_z_id($product_id);
				
				if(!empty($a_TO_z_ids)){
					foreach($a_TO_z_ids as $a_TO_z_id){
						$brand_id=$a_TO_z_id['brand_id'];
						$subcat_id=$a_TO_z_id['subcat_id'];
						$cat_id=$a_TO_z_id['cat_id'];
						$pcat_id=$a_TO_z_id['pcat_id'];
					}
					
					$product_level_promotions_uid=$this->Front_promotions->product_level_promotion_uid($product_id);
					$brand_level_promotions_uid=$this->Front_promotions->brand_level_promotion_uid($brand_id);
					$subcat_level_promotions_uid=$this->Front_promotions->subcat_level_promotion_uid($subcat_id);
					$cat_level_promotions_uid=$this->Front_promotions->cat_level_promotion_uid($cat_id);
					$pcat_level_promotions_uid=$this->Front_promotions->pcat_level_promotion_uid($pcat_id);
					$store_level_promotions_uid=$this->Front_promotions->store_level_promotion_uid();
				   
					if(!empty($product_level_promotions_uid)){
						foreach($product_level_promotions_uid as $product_level_promotion){
							$promo_data=$this->Front_promotions->product_level_promotion($product_level_promotion['promo_uid'],$product_id);
							if(!empty($promo_data)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$product_level_promotion['promo_uid'];
							}
							
						}
						
					}
					
					if(!empty($brand_level_promotions_uid)){
						
						foreach($brand_level_promotions_uid as $brand_level_promotion){
							$promo_data=$this->Front_promotions->brand_level_promotion($brand_level_promotion['promo_uid'],$brand_id);
							if(!empty($promo_data)){
								if(!in_array($brand_level_promotion['promo_uid'],$promo_uid_arr)){
									array_push($promo_array,$promo_data);
									$promo_uid_arr[]=$brand_level_promotion['promo_uid'];
								}
							}
						}
					}
					if(!empty($subcat_level_promotions_uid)){
						foreach($subcat_level_promotions_uid as $subcat_level_promotion){
							$promo_data=$this->Front_promotions->subcat_level_promotion($subcat_level_promotion['promo_uid'],$subcat_id);
							if(!empty($promo_data)){
								if(!in_array($subcat_level_promotion['promo_uid'],$promo_uid_arr)){
									array_push($promo_array,$promo_data);
									$promo_uid_arr[]=$subcat_level_promotion['promo_uid'];
								}
								
							}
						}
					}
					if(!empty($cat_level_promotions_uid)){
						foreach($cat_level_promotions_uid as $cat_level_promotion){
							$promo_data=$this->Front_promotions->cat_level_promotion($cat_level_promotion['promo_uid'],$cat_id);
							if(!empty($promo_data)){
								if(!in_array($cat_level_promotion['promo_uid'],$promo_uid_arr)){
									array_push($promo_array,$promo_data);
									$promo_uid_arr[]=$cat_level_promotion['promo_uid'];
								}
							}
						}
					}
					if(!empty($pcat_level_promotions_uid)){
						foreach($pcat_level_promotions_uid as $pcat_level_promotion){
							$promo_data=$this->Front_promotions->pcat_level_promotion($pcat_level_promotion['promo_uid'],$pcat_id);
							if(!empty($promo_data)){
								if(!in_array($pcat_level_promotion['promo_uid'],$promo_uid_arr)){
									array_push($promo_array,$promo_data);
									$promo_uid_arr[]=$pcat_level_promotion['promo_uid'];
								}
								
							}
						}
					}
					if(!empty($store_level_promotions_uid)){
						
						foreach($store_level_promotions_uid as $store_level_promotion){
							$promo_data=$this->Front_promotions->store_level_promotion($store_level_promotion['promo_uid']);
							if(!empty($promo_data)){
								if(!in_array($store_level_promotion['promo_uid'],$promo_uid_arr)){
									array_push($promo_array,$promo_data);
									$promo_uid_arr[]=$store_level_promotion['promo_uid'];
								}
							}
						}
					}
				}
			}
			$promo_uid=$this->Front_promotions->get_promotions($inventory_id);
			
			if(!empty($promo_uid)){
				foreach($promo_uid as $res){
					$promo_data=$this->Front_promotions->get_all_promotions_data($res['promo_uid'],$inventory_id);
					if(!empty($promo_data)){
						if(!in_array($res['promo_uid'],$promo_uid_arr)){
							array_push($promo_array,$promo_data);
							$promo_uid_arr[]=$res['promo_uid'];
						}
					}
				}
			}
			
			$promotions=$promo_array;
			return $promotions;
		}
		 
	
	
		///////// No of offers display in catalog page ends /////////////////
	public function get_check_availability_of_free_shipping_promotion(){
		$check_availability_of_free_shipping_promotion=$this->Front_promotions->get_check_availability_of_free_shipping_promotion();
		return $check_availability_of_free_shipping_promotion;
	}
	
	/* shopbycategory starts */
	public function get_category_name($cat_id){
		$category_name=$this->Model_search->get_category_name($cat_id);
		return $category_name;
	}
	/* shopbycategory ends */
	/* emioptions starts */
	public function get_minimum_emistartvalue(){
		$min_emistartvalue=$this->Model_search->get_minimum_emistartvalue();
		return $min_emistartvalue;
	}
	public function get_emi_options(){
		$get_emi_options_result_arr=$this->Model_search->get_emi_options();
		?>
		<div class="emioptions_popover">
		<h4 class="text-center mb-10">EMI Options
		<span onclick="closepopover_emioptionsFun()" class="pull-right" style="cursor:pointer;font-weight:bold">&times;</span>
		</h4>
		<?php
			if(!empty($get_emi_options_result_arr)){
				foreach($get_emi_options_result_arr as $emi_options_arr){
		?>
		
		<button class="emioptions_accordion"><img width="10%" src="<?php echo base_url().$emi_options_arr['image_emioptions_path']?>"> &nbsp;&nbsp;<?php echo $emi_options_arr['name']?></button>
<div class="panel">
 <h5 class="text-center mt-10">
	<b><?php echo $emi_options_arr['planname']?></b>
 </h5>
 <?php
 $emiplan_details_json=$emi_options_arr["emiplan_details_json"];
 if($emiplan_details_json!=""){
	 $emiplan_details_json_arr=json_decode($emiplan_details_json,true);
	 if(!empty($emiplan_details_json_arr)){
		 ?>
		 <table class="table">
		 <tr>
			<th>
				EMI Plan
			</th>
			<th>
				Interest(pa)
			</th>
			<th>
				Total Cost
			</th>
			<th>
				
			</th>
		 </tr>
		 <?php
		 foreach($emiplan_details_json_arr as $emailplan_str){
			 ?>
			 <tr>
			 <?php
			 $emailplan_str_arr=explode("|||||",$emailplan_str);
			  ?>
			  <td><?php echo $emailplan_str_arr[0];?></td><td><?php echo $emailplan_str_arr[1];?></td><td><?php echo curr_sym; ?> <?php echo $emailplan_str_arr[2];?></td><td><i class="fa fa-paper-plane" aria-hidden="true" style="cursor:pointer;" onclick="bankAPICallFun()"></i></td>
			  
			 </tr>
			 <?php
		 }
		 ?>
		 </table>
		 <?php
	 }
 }
 ?>
 
 
</div>
<?php
				}
			}
		?>


</div>
<script>
var acc = document.getElementsByClassName("emioptions_accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>
		<?php
	}
	/* emioptions ends */
	/* skus under category starts */
	public function get_skus_under_chain($chain_id,$chain_name){
		$count_number_of_skus=$this->Model_search->get_skus_under_chain($chain_id,$chain_name);
		return $count_number_of_skus;
	}
	/* skus under category ends */
	
	public function get_flat_shipping_available_or_not($inventory_id){
		$shipping_charge_not_applicable="";
		$flat_shipping_charge="";
		//$check_availability_of_free_shipping_promotion_arr=$this->get_check_availability_of_free_shipping_promotion();
		//if($check_availability_of_free_shipping_promotion_arr["status"]=="yes"){
			$get_logistics_inventory_data_obj=$this->Model_search->get_logistics_inventory_data($inventory_id);
			if(!empty($get_logistics_inventory_data_obj)){
				if($get_logistics_inventory_data_obj->shipping_charge_not_applicable=="yes"){
					$flat_shipping_charge=$get_logistics_inventory_data_obj->flat_shipping_charge;
					return array("shipping_charge_not_applicable"=>"yes","flat_shipping_charge"=>$flat_shipping_charge);
				}
			}
		//}
		return array("shipping_charge_not_applicable"=>"no","flat_shipping_charge"=>"");
	}
	
	public function time_elapsed_string($datetime, $full = false){
		$today_date = date("Y-m-d");
		if($today_date==$datetime){
			return 'today';
		}
		else{
		   //get main CodeIgniter object
		   $ci =& get_instance();
		   
		   //load databse library
		   $ci->load->database();
		   
		   //get data from database
		  $now = new DateTime;
			$ago = new DateTime($datetime);
			$diff = $now->diff($ago);

			$diff->w = floor($diff->d / 7);
			$diff->d -= $diff->w * 7;

			$string = array(
				'y' => 'year',
				'm' => 'month',
				'w' => 'week',
				'd' => 'day',
				'h' => 'hour',
				'i' => 'minute',
				's' => 'second',
			);
			foreach ($string as $k => &$v) {
				if ($diff->$k) {
					$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
				} else {
					unset($string[$k]);
				}
			}

			if (!$full) $string = array_slice($string, 0, 1);
			return $string ? implode(', ', $string) . ' ago' : 'just now';
		}
   }
   public function get_promotion_active_status(){
       $data = json_decode(file_get_contents("php://input"));
        if(isset($data->promo_id_selected))
        {
                $id=$data->promo_id_selected;
                $data_obj=get_promotion_active_status($id);
                //print $data_obj;
                print json_encode($data_obj);
        }
   }
   public function get_promotion_active_status_all(){
       
        $data = json_decode(file_get_contents("php://input"));
        $products_order=$data->products_order;
        //print_r($products_order);
        //exit;
        $arr=array();
        if(!empty($products_order)){
            foreach($products_order as $val){
                if(!isset($val->promotion_id_selected)){
                    $arr[]=[];
                }else{
                    $id=$val->promotion_id_selected;
                    $data_obj=get_promotion_active_status($id);
                    //print $data_obj;
                    $arr[]=$data_obj;
                    //print json_encode($data_obj);
                 }
            }
        }
        print json_encode($arr);
            
   }
   public function get_promo_availability(){
        $promo_array=array();$promo_uid_arr=array();
        $data = json_decode(file_get_contents("php://input"));
        $inventory_id=$data->inv_id;
        
        $product_id=$this->Front_promotions->get_product_id($inventory_id);
        $promo_array=array();
        if(!empty($product_id)){
            $a_TO_z_ids=$this->Front_promotions->get_a_TO_z_id($product_id);
            
            if(!empty($a_TO_z_ids)){
                foreach($a_TO_z_ids as $a_TO_z_id){
                    $brand_id=$a_TO_z_id['brand_id'];
                    $subcat_id=$a_TO_z_id['subcat_id'];
                    $cat_id=$a_TO_z_id['cat_id'];
                    $pcat_id=$a_TO_z_id['pcat_id'];
                }
                
                $product_level_promotions_uid=$this->Front_promotions->product_level_promotion_uid($product_id);
                $brand_level_promotions_uid=$this->Front_promotions->brand_level_promotion_uid($brand_id);
                $subcat_level_promotions_uid=$this->Front_promotions->subcat_level_promotion_uid($subcat_id);
                $cat_level_promotions_uid=$this->Front_promotions->cat_level_promotion_uid($cat_id);
                $pcat_level_promotions_uid=$this->Front_promotions->pcat_level_promotion_uid($pcat_id);
                $store_level_promotions_uid=$this->Front_promotions->store_level_promotion_uid();
               
                if(!empty($product_level_promotions_uid)){
					foreach($product_level_promotions_uid as $product_level_promotion){
						$promo_data=$this->Front_promotions->product_level_promotion($product_level_promotion['promo_uid'],$product_id);
						if(!empty($promo_data)){
							array_push($promo_array,$promo_data);
							$promo_uid_arr[]=$product_level_promotion['promo_uid'];
						}
					}

				}

				if(!empty($brand_level_promotions_uid)){

					foreach($brand_level_promotions_uid as $brand_level_promotion){
						$promo_data=$this->Front_promotions->brand_level_promotion($brand_level_promotion['promo_uid'],$brand_id);
						if(!empty($promo_data)){
							
							if(!in_array($brand_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$brand_level_promotion['promo_uid'];
							}
						}
					}
				}
				if(!empty($subcat_level_promotions_uid)){
					foreach($subcat_level_promotions_uid as $subcat_level_promotion){
						$promo_data=$this->Front_promotions->subcat_level_promotion($subcat_level_promotion['promo_uid'],$subcat_id);
						if(!empty($promo_data)){
							
							if(!in_array($subcat_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$subcat_level_promotion['promo_uid'];
							}
						}
					}
				}
				if(!empty($cat_level_promotions_uid)){
					foreach($cat_level_promotions_uid as $cat_level_promotion){
						$promo_data=$this->Front_promotions->cat_level_promotion($cat_level_promotion['promo_uid'],$cat_id);
						if(!empty($promo_data)){
							if(!in_array($cat_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$cat_level_promotion['promo_uid'];
							}

						}
					}
				}
				if(!empty($pcat_level_promotions_uid)){
					foreach($pcat_level_promotions_uid as $pcat_level_promotion){
						$promo_data=$this->Front_promotions->pcat_level_promotion($pcat_level_promotion['promo_uid'],$pcat_id);
						if(!empty($promo_data)){
							if(!in_array($pcat_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$pcat_level_promotion['promo_uid'];
							}
						}
					}
				}
				if(!empty($store_level_promotions_uid)){

					foreach($store_level_promotions_uid as $store_level_promotion){
						$promo_data=$this->Front_promotions->store_level_promotion($store_level_promotion['promo_uid']);
						if(!empty($promo_data)){
							if(!in_array($store_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$store_level_promotion['promo_uid'];
							}
						}
					}
				}
            }
        }
        
        $promo_uid=$this->Front_promotions->get_promotions($inventory_id);
        
        //print_r($promo_uid);
        
        if(!empty($promo_uid)){
            foreach($promo_uid as $res){
                $promo_data=$this->Front_promotions->get_all_promotions_data($res['promo_uid'],$inventory_id);
                if(!empty($promo_data)){
                    array_push($promo_array,$promo_data);
                    
                }
            }
        }
        
        //$data["promotions"]=$promo_array;
        
        print json_encode($promo_array);
   }
   public function get_promo_availability_all(){
        $promo_array=array();
        $data = json_decode(file_get_contents("php://input"));
        $products_order=$data->products_order;
        //print_r($products_order);
        //exit;
        $promo_array_all=array();$promo_uid_arr=array();
        if(!empty($products_order)){
            foreach($products_order as $val){
                $promo_array=array();
                $inventory_id=$val->inventory_id;
                $product_id=$val->product_id;
                if(!empty($product_id)){
                    $a_TO_z_ids=$this->Front_promotions->get_a_TO_z_id($product_id);

                    if(!empty($a_TO_z_ids)){
                        foreach($a_TO_z_ids as $a_TO_z_id){
                            $brand_id=$a_TO_z_id['brand_id'];
                            $subcat_id=$a_TO_z_id['subcat_id'];
                            $cat_id=$a_TO_z_id['cat_id'];
                            $pcat_id=$a_TO_z_id['pcat_id'];
                        }

                        $product_level_promotions_uid=$this->Front_promotions->product_level_promotion_uid($product_id);
                        $brand_level_promotions_uid=$this->Front_promotions->brand_level_promotion_uid($brand_id);
                        $subcat_level_promotions_uid=$this->Front_promotions->subcat_level_promotion_uid($subcat_id);
                        $cat_level_promotions_uid=$this->Front_promotions->cat_level_promotion_uid($cat_id);
                        $pcat_level_promotions_uid=$this->Front_promotions->pcat_level_promotion_uid($pcat_id);
                        $store_level_promotions_uid=$this->Front_promotions->store_level_promotion_uid();

                        if(!empty($product_level_promotions_uid)){
                            foreach($product_level_promotions_uid as $product_level_promotion){
                                $promo_data=$this->Front_promotions->product_level_promotion($product_level_promotion['promo_uid'],$product_id);
                                if(!empty($promo_data)){
                                    array_push($promo_array,$promo_data);
									$promo_uid_arr[]=$product_level_promotion['promo_uid'];
                                }
                            }

                        }

                        if(!empty($brand_level_promotions_uid)){

                            foreach($brand_level_promotions_uid as $brand_level_promotion){
                                $promo_data=$this->Front_promotions->brand_level_promotion($brand_level_promotion['promo_uid'],$brand_id);
                                if(!empty($promo_data)){
                                    
									if(!in_array($brand_level_promotion['promo_uid'],$promo_uid_arr)){
										array_push($promo_array,$promo_data);
										$promo_uid_arr[]=$brand_level_promotion['promo_uid'];
									}
                                }
                            }
                        }
                        if(!empty($subcat_level_promotions_uid)){
                            foreach($subcat_level_promotions_uid as $subcat_level_promotion){
                                $promo_data=$this->Front_promotions->subcat_level_promotion($subcat_level_promotion['promo_uid'],$subcat_id);
                                if(!empty($promo_data)){
                                    
									if(!in_array($subcat_level_promotion['promo_uid'],$promo_uid_arr)){
										array_push($promo_array,$promo_data);
										$promo_uid_arr[]=$subcat_level_promotion['promo_uid'];
									}
                                }
                            }
                        }
                        if(!empty($cat_level_promotions_uid)){
                            foreach($cat_level_promotions_uid as $cat_level_promotion){
                                $promo_data=$this->Front_promotions->cat_level_promotion($cat_level_promotion['promo_uid'],$cat_id);
                                if(!empty($promo_data)){
									if(!in_array($cat_level_promotion['promo_uid'],$promo_uid_arr)){
										array_push($promo_array,$promo_data);
										$promo_uid_arr[]=$cat_level_promotion['promo_uid'];
									}

                                }
                            }
                        }
                        if(!empty($pcat_level_promotions_uid)){
                            foreach($pcat_level_promotions_uid as $pcat_level_promotion){
                                $promo_data=$this->Front_promotions->pcat_level_promotion($pcat_level_promotion['promo_uid'],$pcat_id);
                                if(!empty($promo_data)){
                                    if(!in_array($pcat_level_promotion['promo_uid'],$promo_uid_arr)){
										array_push($promo_array,$promo_data);
										$promo_uid_arr[]=$pcat_level_promotion['promo_uid'];
									}
                                }
                            }
                        }
                        if(!empty($store_level_promotions_uid)){

                            foreach($store_level_promotions_uid as $store_level_promotion){
                                $promo_data=$this->Front_promotions->store_level_promotion($store_level_promotion['promo_uid']);
                                if(!empty($promo_data)){
                                    if(!in_array($store_level_promotion['promo_uid'],$promo_uid_arr)){
										array_push($promo_array,$promo_data);
										$promo_uid_arr[]=$store_level_promotion['promo_uid'];
									}
                                }
                            }
                        }
                    }
                }

                $promo_uid=$this->Front_promotions->get_promotions($inventory_id);

                //print_r($promo_uid);

                if(!empty($promo_uid)){
                    foreach($promo_uid as $res){
                        $promo_data=$this->Front_promotions->get_all_promotions_data($res['promo_uid'],$inventory_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);

                        }
                    }
                }
                
                $promo_array_all[]=$promo_array;
            }
        }
        
        
        //$data["promotions"]=$promo_array;
        
        //print_r($promo_array_all);
        
        print json_encode($promo_array_all);
   }
   public function seller_registration(){
       $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
        $data['p_category']=$this->provide_first_level_menu();
        $data["controller"]=$this;
        $data["current_controller"]="search";
        $ft_img=$this->Customer_frontend->foot_image_index();
        $data['ft_img']='';
        if(!empty($ft_img)){
            if(isset($ft_img->background_image)){
                $data['ft_img']=$ft_img->background_image;
            }
        }
        $data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
        $this->load->view('header',$data);
        $this->load->view('seller_registration');
        $this->load->view('footer');
	
   }
   public function get_active_products(){
        $inactive_pro=array();
        $data = json_decode(file_get_contents("php://input"));
        $products_order=$data->products_order;
        foreach($products_order as $key=>$val){
            $product_id=$val->product_id;
            $inventory_id=$val->inventory_id;
            $pro=$this->Model_search->get_active_products($product_id,$inventory_id);
            if($pro!=''){
                $inactive_pro[]=$inventory_id;
            }
        }
        print json_encode($inactive_pro);
   }
   
   
   public function get_corresponding_shipping_charge_combo(){
	//update the cart table details if customer is in session
	$pincode_check_post_data = json_decode(file_get_contents("php://input"));

	$pincode=$pincode_check_post_data->pincode;

	$cart_data=$pincode_check_post_data->combo_products;

	$all_data_arr=array();

	$pincode_valid=$this->Model_search->validate_pincode($pincode);

	
	$logistics_id='';
	$logistics_parcel_category_id='';
	$default_delivery_mode_id='';
	$logistics_territory_id='';
	$delivery_mode='';
	$parcel_type='';
	$territory_duration='';
	$logistics_name='';
	$logistics_weblink='';
	

	$overall_weight=0;

	foreach($cart_data as $key=>$cart_data_value){

		//print_r($cart_data_value);
		//exit;
//$key==1
		if(1){

			$inventory_id=$cart_data_value->inv_id;
			$inv_data=get_all_inventory_data($inventory_id);
			$current_quantity=$inv_data->moq;

			$vendor_id=$inv_data->vendor_id;//logisics vendor_id				
			
			/////////////////// getting logistics id and territory name id starts /////
			$result=$this->Model_search->get_default_logistics_id_for_all_vendors($vendor_id);
				$logistics_id_arr=array();
				foreach($result as $res){
					$logistics_id_arr[]=$res->logistics_id;
				}
				
				$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr=$this->Model_search->get_logistics_id_territory_name_id_by_pincode_vendor_id($logistics_id_arr,$pincode,$vendor_id);
			$logistics_id=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["logistics_id"];
			
			$territory_name_id=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["territory_name_id"];
			$flag=$get_logistics_id_territory_name_id_by_pincode_vendor_id_arr["check_logistics_yes"];
			//////////////////  getting logistics id and territory name id ends /////////
			
			///////////////////////
			$moq_original=$inv_data->moq;
			$shipping_detail=$this->get_territory_of_pincode($pincode,$vendor_id,$logistics_id,$territory_name_id);
			
			$logistics_territory_id=$shipping_detail['logistics_territory_id'];
			$logistics_territory_name=$this->get_territory_name_by_territory_id($logistics_territory_id);
			
			$free_shipping_arr=$this->get_free_shipping_territories_by_inventory_id($inventory_id);

					$logistics_id=$shipping_detail['logistics'];
					//$flag=$this->Model_search->check_availability_pincode_foreach_inventory($pincode,$logistics_id);
					if($flag==true){				
					$city=$shipping_detail['city'];	
					$default_delivery_mode_id=$shipping_detail['default_delivery_mode_id'];
					$default_delivery_mode=$shipping_detail['default_delivery_mode'];
					$logistics_name=$shipping_detail['logistics_name'];
					$logistics_weblink=$shipping_detail['logistics_weblink'];
					$speed_duration=$shipping_detail['speed_duration'];
					
					$default_parcel_category=$this->get_default_parcel_category($logistics_id);
					$logistics_parcel_category_id=$default_parcel_category["logistics_parcel_category_id"];
					$parcel_type=$default_parcel_category["parcel_type"];
					$parcel_type_description=$default_parcel_category["parcel_type_description"];
					
					$logistics_inventory_obj=$this->get_logistics_inventory_data($inventory_id);
					$parcel_cat_arr=$logistics_inventory_obj->logistics_parcel_category_id;
					$available_parcel_category_of_inventory=$this->available_parcel_category_of_inventory($inventory_id,$parcel_cat_arr);
					
					$inventory_weight_in_kg=$logistics_inventory_obj->inventory_weight_in_kg;

					//$inventory_weight_in_kg=$cart_data_value->inventory_weight_in_kg;
					$inventory_weight_in_kg_for_unit=round($inventory_weight_in_kg/$moq_original);
					
					$inventory_volume_in_kg=$logistics_inventory_obj->inventory_volume_in_kg;
				
					////////// getting inventory addons weights in kg starts////////////////////////////////
						
					$addons_inv_list_arr=[];
					$inventory_for_addons_weight_in_kg=0;
					
					
					$inventory_weight_in_kg=$inventory_weight_in_kg;

					////////// getting inventory addons weights in kg ends////////////////////////////////
					
					//$logistics_price=$logistics_price_resultant_inventory_volume_in_kg_arr["logistics_price_with_tax"];
					
					$get_flat_shipping_available_or_not_arr=$this->get_flat_shipping_available_or_not($inventory_id);

					if($get_flat_shipping_available_or_not_arr["shipping_charge_not_applicable"]=="yes"){
						$logistics_price=$get_flat_shipping_available_or_not_arr["flat_shipping_charge"];
					}else{
						
						$logistics_price_resultant_inventory_volume_in_kg_arr=$this->get_logistics_price($current_quantity,$inventory_id,$logistics_id,$inventory_weight_in_kg,$inventory_volume_in_kg,$logistics_territory_id,$default_delivery_mode_id,$logistics_parcel_category_id);
						
						$logistics_price=$logistics_price_resultant_inventory_volume_in_kg_arr["logistics_price_with_tax"];
					}
			
					$default_shipping_detail=$this->get_default_shipping_charge($inventory_id,$pincode,$logistics_territory_id,$logistics_id,$default_delivery_mode_id,$logistics_parcel_category_id,$logistics_price,$speed_duration);
					
				
				
				if($free_shipping_arr!=false){	

					if(in_array($logistics_territory_name,$free_shipping_arr)) {
						$default_shipping_detail['logistics_price']=0;
						$default_shipping_detail['shipping_charge']=0;
						$default_shipping_detail['delivery_service']="free";
					
					}
					else{
						$default_shipping_detail['logistics_price']=$logistics_price;
						$default_shipping_detail['shipping_charge']=$logistics_price;
					}
				}else{
					$default_shipping_detail['logistics_price']=$logistics_price;
					$default_shipping_detail['shipping_charge']=$logistics_price;
				}

					$default_shipping_detail['delivery_service']=$flag;

					$default_shipping_detail['logistics_id']=$logistics_id;
					$default_shipping_detail['logistics_territory_id']=$logistics_territory_id;
					
					$default_shipping_detail['default_delivery_mode_id']=$default_delivery_mode_id;
					
					$default_shipping_detail['logistics_parcel_category_id']=$logistics_parcel_category_id;
					
					
					$default_shipping_detail['logistics_price']=$logistics_price;
					$default_shipping_detail['inventory_weight_in_kg']=$inventory_weight_in_kg;
					$default_shipping_detail['inventory_weight_in_kg_for_unit']=$inventory_weight_in_kg_for_unit;
					$default_shipping_detail['inventory_volume_in_kg']=$inventory_volume_in_kg;
					
				}else{
					$default_shipping_detail=array('shipping_charge'=>-1,'territory_duration'=>-1,'territory_duration_org'=>-1,'delivery_service'=>$flag);
				}

				$default_delivery_mode_arr=$this->Model_search->default_delivery_mode($logistics_id);
			
				$pincode;
				$logistics_id;
				$logistics_parcel_category_id;
				$default_delivery_mode_id=$default_delivery_mode_id;
				$logistics_territory_id;
				$delivery_mode=$default_delivery_mode_arr['delivery_mode'];
				$parcel_type;
				$territory_duration=$default_shipping_detail['territory_duration_org'];
				$logistics_name;
				$logistics_weblink;

				$insert_arr=array('pincode'=>"$pincode",'logistics_id'=>"$logistics_id",'logistics_parcel_category_id'=>"$logistics_parcel_category_id",'default_delivery_mode_id'=>"$default_delivery_mode_id",'logistics_territory_id'=>"$logistics_territory_id","delivery_mode"=>"$delivery_mode",'parcel_type'=>"$parcel_type",'territory_duration'=>"$territory_duration",'logistics_name'=>"$logistics_name",'logistics_weblink'=>"$logistics_weblink");

				$flag=$this->Model_search->update_carttable_combo_dump($insert_arr);

				$default_shipping_detail['pincode_valid']=$pincode_valid;
				$all_data_arr[]=$default_shipping_detail;
		}

		
		
	}

	echo json_encode($all_data_arr);

}
public function test_social_shares(){
	$method=current_url();
	$this->session->set_userdata("cur_page",$method);
	$data['page_name']='';
	$data['page_type']='';
	$data['page_id']='';
	$data['products_obj']='';
		$data["current_controller"]="search";
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('test_social_shares');
		$this->load->view('footer');
}









	
	public function get_expertise_area_details_from_master_db($expertise_area_unique_id){
		$expertise_area_details_arr=$this->Model_search->get_expertise_area_details_from_master_db($expertise_area_unique_id);
		return $expertise_area_details_arr;
	}
	
	public function get_expertise_area_list_from_master_db(){
		$expertise_area_details_arr=$this->Model_search->get_expertise_area_list_from_master_db();
		return $expertise_area_details_arr;
	}
	
	public function request_reference_id($rq_customer_name,$rq_gsm){
		$rq_customer_name=str_replace(' ', '', strtolower($rq_customer_name));
		$cus_name=substr($rq_customer_name, 0, 4);
		$gsm=str_replace(' ', '', strtolower($rq_gsm));
		$rand=mt_rand(1000, 9999);
		$date=date('d');
		$month=date('m');
		
		return $str=$cus_name.$date.$month.$gsm.$rand;
		
	}

	
	public function request_a_quote(){
		 if($this->session->userdata("customer_id")){
			$rq_customer_id=$this->session->userdata("customer_id");
			$rq_customer_name=$this->session->userdata("customer_name");
                        
		}else{
			$rq_customer_id='Guest';
            $rq_customer_name='Guest';
		}
            $localstorage_data = json_decode($this->input->post('combo_products'));
			//print_r($localstorage_data);exit;
		$inv_id_arr=[];
		$inv_sku_id_arr=[];
		$inv_qtys_arr=[];
		if(count($localstorage_data)>0){
			foreach($localstorage_data as $dataObj){
				$inv_id_arr[]=$dataObj->inv_id;
				$inv_sku_id_arr[]=$dataObj->inv_sku_id;
				$inv_qtys_arr[]=$dataObj->inv_quantity;
			}
		}
                
	//print_r($inv_sku_id_arr);
	//exit;
			$date = new DateTime();
			$mt=microtime();
			$salt="employeespriam";
			$rand=mt_rand(1, 1000000);
			$ts=$date->getTimestamp();
			$str=$salt.$rand.$ts.$mt;
			$rq_unique_id=md5($str);
			
			
				$rq_brand_id=mt_rand(0, 1);
					
                $ref_id=$this->request_reference_id($rq_customer_name,$rq_brand_id);
                
		$flag=$this->Customer_account->request_a_quote_different_views(
														$ref_id,
														$rq_customer_id,
														$inv_id_arr,
														$inv_sku_id_arr,"",$inv_qtys_arr);
		$arr=array("rq_unique_id"=>$ref_id);
            echo json_encode($arr);
	}
	
	
	
	public function request_to_quote($rq_unique_id='') {
		    $method=current_url();
			$this->session->set_userdata("cur_page",$method);
			$data['page_name']='';
			$data['page_type']='';
			$data['page_id']='';
			$data['products_obj']='';
			$data['p_category']=$this->provide_first_level_menu();
			$data["controller"]=$this;
			$data["current_controller"]="search";
			
			$rfq_detailsObj=$this->Customer_account->get_details_from_rfq($rq_unique_id);
			
			//////////////////
			
		
		
			$data['rfq_details']=$rfq_detailsObj;
			$data["rq_unique_id"]=$rq_unique_id;
			$data["rq_customer_id"]=$rfq_detailsObj->rq_customer_id;
			$data["rq_sku_ids"]=$rfq_detailsObj->rq_sku_ids;
			$data["rq_inv_ids"]=$rfq_detailsObj->rq_inv_ids;
			$data["rq_inv_qtys"]=$rfq_detailsObj->rq_inv_qtys;
			$data["vendor_id_list"]=$rfq_detailsObj->vendor_id_list;
			
			$rq_pincode="";

			if($this->session->userdata("logged_in")){
				$customer_id=$this->session->userdata("customer_id");
				$cus_details=$this->Customer_account->get_customer_profile_info($customer_id);
				if(!empty($cus_details)){
					$rq_name=isset($cus_details['cus_del_contact_name']) ? $cus_details['cus_del_contact_name'] : '';
					$rq_email=isset($cus_details['cus_del_email']) ? $cus_details['cus_del_email'] : '';
					$rq_mobile=isset($cus_details['cus_del_mobile']) ? $cus_details['cus_del_mobile'] : '';
					$rq_delivery_address=isset($cus_details['cus_del_address']) ? $cus_details['cus_del_address'] : '';
					if($rq_pincode==''){
						$rq_pincode=isset($cus_details['cus_del_pincode']) ? $cus_details['cus_del_pincode'] : $rq_pincode;
					}
				}
				else{
					$rq_name="";
					$rq_email="";
					$rq_mobile="";
					$rq_delivery_address="";
					$rq_pincode="";
				}
			}
			else{
				$rq_name="";
				$rq_email="";
				$rq_mobile="";
				$rq_delivery_address="";
				$rq_pincode="";
			}
			$data["rq_name"]=$rq_name;
			$data["rq_email"]=$rq_email;
			$data["rq_mobile"]=$rq_mobile;
			$data["rq_delivery_address"]=$rq_delivery_address;
			$data["rq_pincode"]=$rq_pincode;
			
            $this->load->view('header',$data);
            $this->load->view('rfq_common_new');
            $this->load->view('footer');
        
    }
	
	public function show_available_active_categories(){
		
			$pcat_id=$this->input->post("pcat_id");
			$result_cat=$this->Model_search->show_available_active_categories($pcat_id);
			if(count($result_cat)!=0){
				$str='';
				$str.='<option value=""> - Choose Category - </option>';	
				foreach($result_cat as $res){
					$str.='<option value="'.$res->cat_id.'">'.$res->cat_name.'</option>';
				}
				echo $str;
			}else{
				echo count($result_cat);
			}
		
	}
	
	public function show_available_active_subcategories(){
		
			$cat_id=$this->input->post("cat_id");
			$result_subcat=$this->Model_search->show_available_active_subcategories($cat_id);
			if(count($result_subcat)!=0){
				$str='';
				$str.='<option value=""> - Choose Sub Category - </option>';
				foreach($result_subcat as $res){
					$str.='<option value="'.$res->subcat_id.'">'.$res->subcat_name.'</option>';
				}
				echo $str;
			}else{
				echo count($result_subcat);
			}
		
	}
	
	public function show_available_active_brands(){
			
			$subcat_id=$this->input->post("subcat_id");
			$result_brand=$this->Model_search->show_available_active_brands($subcat_id);
			if(count($result_brand)!=0){
				$str='';
				$str.='<option value=""> - Choose Brand - </option>';
				foreach($result_brand as $res){
					$str.='<option value="'.$res->brand_id.'">'.$res->brand_name.'</option>';
				}
				echo $str;
			}else{
				echo count($result_brand);
			}
		
	}
	
	public function show_available_active_products(){
			
			$brand_id=$this->input->post("brand_id");
			$result_brand=$this->Model_search->show_available_active_products($brand_id);
			if(count($result_brand)!=0){
				$str='';
				$str.='<option value=""> - Choose Product - </option>';
				foreach($result_brand as $res){
					$str.='<option value="'.$res->product_id.'">'.$res->product_name.'</option>';
				}
				echo $str;
			}else{
				echo count($result_brand);
			}
		
	}
	
	public function show_available_active_skus(){
			
			$product_id=$this->input->post("product_id");
			$result_product=$this->Model_search->show_available_active_skus($product_id);
			if(count($result_product)!=0){
				$str='';
				$str.='<option value=""> - Choose SKU - </option>';
				foreach($result_product as $res){
					$str.='<option value="'.$res->id.'">'.$res->sku_id.'</option>';
				}
				echo $str;
			}else{
				echo count($result_product);
			}
		
	}
	
	public function get_treeview_fun(){
		$result_catalog_tree_view=$_REQUEST["result_catalog_tree_view"];
		$result_catalog_tree_view_arr=explode(",",$result_catalog_tree_view);
		$get_all_parent_categories_arr=$this->Model_search->get_all_parent_categories();
		$parent_category_arr=[];
		foreach($get_all_parent_categories_arr as $all_parent_categories_arr){
			
			$get_categories_by_pcat_id_arr=$this->Model_search->get_categories_by_pcat_id($all_parent_categories_arr["pcat_id"]);
			$category_arr=[];
			foreach($get_categories_by_pcat_id_arr as $categories_by_pcat_id_arr){
				
				$get_sub_categories_by_cat_id_arr=$this->Model_search->get_sub_categories_by_cat_id($categories_by_pcat_id_arr["cat_id"]);
				$sub_categories_arr=[];
				foreach($get_sub_categories_by_cat_id_arr as $sub_categories_by_pcat_id_arr){
					if(in_array("subcat_id_".$sub_categories_by_pcat_id_arr["subcat_id"],$result_catalog_tree_view_arr)){
						$sub_categories_arr[]=array("id"=>"subcat_id_".$sub_categories_by_pcat_id_arr["subcat_id"],"text"=>$sub_categories_by_pcat_id_arr["subcat_name"],"checked"=>true);
					}
					else{
						$sub_categories_arr[]=array("id"=>"subcat_id_".$sub_categories_by_pcat_id_arr["subcat_id"],"text"=>$sub_categories_by_pcat_id_arr["subcat_name"]);
					}
				}
				if(in_array("cat_id_".$categories_by_pcat_id_arr["cat_id"],$result_catalog_tree_view_arr)){
					$category_arr[]=array("id"=>"cat_id_".$categories_by_pcat_id_arr["cat_id"],"text"=>$categories_by_pcat_id_arr["cat_name"],"expanded"=>true,"checked"=>true,"items"=>$sub_categories_arr);
				}
				else{
					$category_arr[]=array("id"=>"cat_id_".$categories_by_pcat_id_arr["cat_id"],"text"=>$categories_by_pcat_id_arr["cat_name"],"expanded"=>true,"items"=>$sub_categories_arr);
				}
				
			}
			if(in_array("pcat_id_".$all_parent_categories_arr["pcat_id"],$result_catalog_tree_view_arr)){
				$parent_category_arr[]=array("id"=>"pcat_id_".$all_parent_categories_arr["pcat_id"],"text"=>$all_parent_categories_arr["pcat_name"],"expanded"=>true,"checked"=>true,"items"=>$category_arr);
			}
			else{
				$parent_category_arr[]=array("id"=>"pcat_id_".$all_parent_categories_arr["pcat_id"],"text"=>$all_parent_categories_arr["pcat_name"],"expanded"=>true,"items"=>$category_arr);
			}
			
			
		}
		echo json_encode($parent_category_arr);
	}
	
	public function get_inventories_of_catalog_tree_view_by_subcat_id_fun($result_catalog_tree_view_subcat_id_arr){
		$catalog_tree_view_inventory_id_arr=[];
		$result_catalog_tree_view_inventory_id_arr=$this->Model_search->get_inventories_of_catalog_tree_view_by_subcat_id_fun($result_catalog_tree_view_subcat_id_arr);
		foreach($result_catalog_tree_view_inventory_id_arr as $arr){
			$catalog_tree_view_inventory_id_arr[]=$arr["id"];
		}
		return $catalog_tree_view_inventory_id_arr;
	}
	
	
    /** Become a reseller starts */
	public function becomeareseller(){
		$data["controller"]=$this;
		$method=current_url();
		$this->session->set_userdata("cur_page",$method);
		$data['page_name']='';
		$data['page_type']='';
		$data['page_id']='';
		$data['products_obj']='';
		
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		
		$data["current_controller"]="Search";
		$data["cur_Spage"]="cur_Spage";
		$data["cur_page"]="becomeareseller";
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		
		$this->load->view('header',$data);
		$this->load->view('becomeareseller');
		$this->load->view('footer');
	}
	
	
	function add_save_becomeareseller(){
		if(isset($_REQUEST))
		{
			$data = json_decode(file_get_contents("php://input"));
			if(isset($data))
			{
				$reseller_companyname=$this->db->escape_str($data->reseller_companyname);
				$reseller_name=$this->db->escape_str($data->reseller_name);
				$reseller_email=$this->db->escape_str($data->reseller_email);
				$reseller_mobile=$this->db->escape_str($data->reseller_mobile);
				$reseller_city=$this->db->escape_str($data->reseller_city);
				$reseller_type=$this->db->escape_str($data->reseller_type);
				
				
				
				$date = new DateTime();
				$mt=microtime();
				$salt="becomeareseller_secure_id";
				$rand=mt_rand(1, 1000000);
				$ts=$date->getTimestamp();
				$str=$salt.$rand.$ts.$mt;
				$secure_id=md5($str);	
		
					$result=$this->Customer_frontend->add_save_becomeareseller(
								$reseller_companyname,
								$reseller_name,
								$reseller_email,
								$reseller_mobile,
								$reseller_city,
								$reseller_type,
								$secure_id
							);	
				if($result){

					$email_stuff_arr=email_stuff();
					$email_s=$email_stuff_arr["fromemail_emailtemplate"];
					$toemail_emailtemplate_live_1=$email_stuff_arr["toemail_emailtemplate_live_1"];
					
				
					//through Email
			

				$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color:color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
				
				<center style="background-color:#E1E		1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody>';
				
				
				$usermessage.='<tr><td align="center" valign="top" width="500" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr>';
				
				$usermessage.='</tbody></table></td></tr><tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 14px;">';

				$usermessage.='Become A Reseller Enquiry Information :<br><br>';

				$usermessage.='<span style="font-size: 14px;"><b>Company Name : </b>'.$reseller_companyname.'<br><b>Name : </b>'.$reseller_name.'<br><b>Email : </b>'.$reseller_email.'<br><b>Mobile: </b>'.$reseller_mobile.'<br><b>City: </b>'.$reseller_city.'<br><b>Type: </b>'.$reseller_type.'</span></p><br></td></tr>';
				

				//$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

				$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

				$usermessage.='</tbody> </table></tr></table></center> </body></html>';
			   
				
				$mail = new PHPMailer;
				/*$mail->isSMTP();
				$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
				$mail->Port = '587';//  live - comment it 
				$mail->Auth = true;//  live - comment it 
				$mail->SMTPAuth = true;
				$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
				$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
				$mail->SMTPSecure = 'tls';*/
				$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
				$mail->FromName = $email_stuff_arr["name_emailtemplate"];
				$mail->addAddress($toemail_emailtemplate_live_1);              
				//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
				//$mail->AddBCC("sucheta@axonlabs.in", "Sucheta");
				$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				$mail->Subject = 'Become A Reseller';
				$mail->Body    = $usermessage;
						
				@$mail->send();
				//$flag=$this->Customer_account->sended_code_to_mail($email,$rand);
				//echo $flag;
				
			


					echo json_encode(array(
					'status' => 'yes',
					'secure_id'=>$secure_id
					));
				}
				else{
					echo json_encode(array(
					'status' => 'no'
					));
				}
				
				
			}
		}
	}
	
	
	/** Become a reseller ends */
	/** request for quotation starts **/
	
	public function search_category_requestforquotation($pcat,$cat='',$subcat=''){
		////read_and_update_traffic();
		//visitor_activity_analytics();

		if($cat!="" && $subcat!=""){
			$category_is_exists="yes";
			$subcategory_is_exists="yes";
		}
		else if($cat!="" && $subcat==""){
			$category_is_exists="yes";
			$subcategory_is_exists="no";
		}
		else if($cat=="" && $subcat!=""){
			$category_is_exists="no";
			$subcategory_is_exists="yes";
		}
		else{
			$category_is_exists="no";
			$subcategory_is_exists="no";
		}
		if(!$this->input->post()){
			$parentcategory_name="";
			$category_name="";
			$subcategory_name="";
			if($pcat!=""){
				$pcat=substr($pcat, 9);
				if($pcat!=0){
					$parentcategory_name=$this->Model_search->get_parentcategory_name($pcat);
					$parentcategory_name=strtolower($parentcategory_name);
					$parentcategory_name = str_replace(' ', '-', $parentcategory_name);
				}
				else{
					$parentcategory_name="null";
				}
			}
			if($cat!=""){
				$cat=substr($cat, 9);
				$category_name=$this->Model_search->get_category_name($cat);
				$category_name=strtolower($category_name);
				$category_name = str_replace(' ', '-', $category_name);
			}
			if($subcat!=""){
				$subcat=substr($subcat, 9);
				$subcategory_name=$this->Model_search->get_subcategory_name($subcat);
				$subcategory_name=strtolower($subcategory_name);
				$subcategory_name = str_replace(' ', '-', $subcategory_name);
			}
			//redirect(site_url("catalog_requestforquotation/".$parentcategory_name));
			redirect(site_url("catalog_requestforquotation/".$parentcategory_name."/".$category_name."/".$subcategory_name));
		}
		else{
			
			//visitor_activity();
		$method=current_url();
		$this->session->set_userdata("products_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		
		}

		$search_keywords=$this->input->post('search_keywords');
		if($search_keywords!=''){
			$search_keyword_index_page=strtolower($search_keywords);
			$flag=$this->Model_search->dump_search_keywords($search_keywords);
			//echo $flag;
		}
			$search_keyword="";$search_cat="";$page_rows="";$pagenum="";
            $sorting_by="";$cat_current="";$cat_current_id="";$type_of_filter="";
            $color_filter="";$color_prev="";$size_filter="";$size_prev="";
            $price_filter="";$price_prev="";$brand_filter="";$brand_prev="";
			//print_r($this->input->post());exit;
			if(!$this->input->post()){
				
				if($pcat!="" && $cat!="" && $subcat!=""){
				   $pcat=substr($pcat, 9);
					$cat=substr($cat, 9);
					$subcat=substr($subcat, 9);
				   $level_type="subcat_id";
				   $level_value=$pcat."_".$cat."_".$subcat;
				   $level_name=$this->get_subcat_name($pcat,$cat,$subcat);
				}
				if($pcat!="" && $cat!="" && $subcat==""){
				   $pcat=substr($pcat, 9);
					$cat=substr($cat, 9);
					
				   $level_type="cat_id";
				   $level_value=$pcat."_".$cat;
				   $level_name=$this->get_cat_name($pcat,$cat);
				}
				if($pcat!="" && $cat=="" && $subcat==""){
				   $pcat=substr($pcat, 9);
					
					
				   $level_type="pcat_id";
				   $level_value=$pcat;
				   $level_name=$this->get_pcat_name($pcat);
				}
            }
            else{
			 	extract($this->input->post());
			  	$pcat=substr($pcat, 9);
				$cat=substr($cat, 9);
				$subcat=substr($subcat, 9);
            }
            
            $data['p_category']=$this->provide_first_level_menu();
        $data["controller"]=$this;
		
		
        $data["current_controller"]="search";
        $data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
        $data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
        
            if($level_type=="brand_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id=$level_value_arr[3];
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$brand_id;
            }
            if($level_type=="subcat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$subcat_id;
            }
			if($level_type=="cat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$cat_id;
            }
			if($level_type=="pcat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id="";
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$pcat_id;
            }
            if($level_type=="product_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id=$level_value_arr[3];
                $product_id=$level_value_arr[4];
                $inventory_id="";
                $data['cat_current_id']=$product_id;
               
            }
                $data["pcat_id"]=$pcat_id;
                $data['cat_id']=$cat_id;
                $data['subcat_id']=$subcat_id;
                $data['brand_id']=$brand_id;
                $data['product_id']=$product_id;
                $data['inventory_id']=$inventory_id;
                $data['searched_key']=$level_name;
                $data['level_type']=$level_type;
			$arr=array();
            $arr["pcat_id"]=$pcat_id;
            $arr['cat_id']=$cat_id;
            $arr['subcat_id']=$subcat_id;
            $arr['brand_id']=$brand_id;
            $arr['product_id']=$product_id;
             
            
            $data["category_tree"]=$arr;
            $data['level_value']=$level_value;
            $data['cat_current']=$level_type;
			$data["controller"]=$this;
            
            
			$data["current_controller"]="search";
			$data['search_keyword_index_page']="";
			if($this->input->post("search_keyword_index_page")!=""){
				$data['search_keyword_index_page']=$this->input->post("search_keyword_index_page");
			}
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			
			
			if($category_is_exists=="yes" && $subcategory_is_exists=="yes"){
				if($pcat!=""){
					$get_parent_cat_info_obj=$this->get_parent_cat_info($pcat);
				}
				if($cat!=""){
					$get_cat_info_obj=$this->get_cat_info($cat);
				}
				if($subcat!=""){
					$get_subcat_info_obj=$this->get_subcat_info($subcat);
				}
				
				if($pcat=="" || $cat=="" || $subcat==""){
					$this->load->view('error404',$data);
				}
				else if(preg_match("/[a-z]/i", $pcat) || preg_match("/[a-z]/i", $cat) || preg_match("/[a-z]/i", $subcat)){
					$this->load->view('error404',$data);
				}
				else if($pcat==0){
					if(empty($get_cat_info_obj) || empty($get_subcat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category_requestforquotation',$data);
					}
				}
				else if($pcat!=0){
					if(empty($get_parent_cat_info_obj) || empty($get_cat_info_obj) || empty($get_subcat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category_requestforquotation',$data);
					}
				}
				else{
					$this->load->view('search_category_requestforquotation',$data);
				}
			}
			else if($category_is_exists=="yes" && $subcategory_is_exists=="no"){
				
				if($pcat!=""){
					$get_parent_cat_info_obj=$this->get_parent_cat_info($pcat);
				}
				if($cat!=""){
					$get_cat_info_obj=$this->get_cat_info($cat);
				}
				
				if($pcat=="" || $cat==""){
					$this->load->view('error404',$data);
				}
				else if(preg_match("/[a-z]/i", $pcat) || preg_match("/[a-z]/i", $cat)){
					$this->load->view('error404',$data);
				}
				else if($pcat==0){
					if(empty($get_cat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category_requestforquotation',$data);
					}
				}
				else if($pcat!=0){
					if(empty($get_parent_cat_info_obj) || empty($get_cat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category_requestforquotation',$data);
					}
				}
				else{
					$this->load->view('search_category_requestforquotation',$data);
				}
			
			}
			else if($category_is_exists=="no" && $subcategory_is_exists=="no"){
				
				if($pcat!=""){
					$get_parent_cat_info_obj=$this->get_parent_cat_info($pcat);
				}
				if($pcat==""){
					$this->load->view('error404',$data);
				}
				else if(preg_match("/[a-z]/i", $pcat)){
					$this->load->view('error404',$data);
				}
				else if($pcat==0){
					$this->load->view('search_category_requestforquotation',$data);
				}
				else if($pcat!=0){
					if(empty($get_parent_cat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category_requestforquotation',$data);
					}
				}
				else{
					$this->load->view('search_category_requestforquotation',$data);
				}
			
			}
			$this->load->view('footer');
	}
	
	
	public function catalog_requestforquotation($parentcategory_name='',$category_name="",$subcategory_name=""){
		////read_and_update_traffic();
		//visitor_activity_analytics();
		$pcat="";
		$cat="";
		$subcat="";
	
		$adm_settings=get_admin_combo_settings();
		
		$data['adm_settings']=$adm_settings;

		if($parentcategory_name!="null" && $parentcategory_name!=''){
			$parentcategory_name = str_replace('-', ' ', $parentcategory_name);
			$pcat=$this->Model_search->get_pcat_id_by_parentcategory_name($parentcategory_name);
                        if($pcat==''){
                            $rand_temp=$this->sample_code();
                            $pcat=$rand_temp."0";
                        }else{
                            $rand_temp=$this->sample_code();
                            $pcat=$rand_temp.$pcat;
                        }
		}
		else{
			$rand_temp=$this->sample_code();
			$pcat="";
		}
	
                //echo $pcat;
		$Ncat_id='';
		
		if($category_name!=""){
			$category_name = str_replace('-', ' ', $category_name);
			$cat=$this->Model_search->get_cat_id_by_category_name($category_name,$parentcategory_name);
            $Ncat_id=$cat;
			$rand_temp=$this->sample_code();
			$cat=$rand_temp.$cat;
		}
		if($subcategory_name!="" && $Ncat_id!=""){
			$subcategory_name = str_replace('-', ' ', $subcategory_name);
			$subcat=$this->Model_search->get_subcat_id_by_subcategory_name($subcategory_name,$Ncat_id);
			$rand_temp=$this->sample_code();
			$subcat=$rand_temp.$subcat;
		}
		
		
			//visitor_activity();
		$method=current_url();
		$this->session->set_userdata("products_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		if($subcat!=''){
			$subcate_id=substr($subcat, 9);
			$inv_id=$this->Model_search->get_count_of_invs($subcate_id);
			
			if(!empty($inv_id)){
				$inv_count=count($inv_id);
				if($inv_count==1){
					$rand=$this->sample_code();
					$obj=reset($inv_id);
					$invent_rand_id=$rand.$obj->id;
					redirect(site_url("detail/".$invent_rand_id));
				}
			}
		}

		$search_keywords=$this->input->post('search_keywords');
		if($search_keywords!=''){
			$search_keyword_index_page=strtolower($search_keywords);
			$flag=$this->Model_search->dump_search_keywords($search_keywords);
			//echo $flag;
		}
			$search_keyword="";$search_cat="";$page_rows="";$pagenum="";
            $sorting_by="";$cat_current="";$cat_current_id="";$type_of_filter="";
            $color_filter="";$color_prev="";$size_filter="";$size_prev="";
            $price_filter="";$price_prev="";$brand_filter="";$brand_prev="";
			//print_r($this->input->post());exit;
			if(!$this->input->post()){



				if($pcat!="" && $cat!="" && $subcat!=""){
				   $pcat=substr($pcat, 9);
					$cat=substr($cat, 9);
					$subcat=substr($subcat, 9);
				   $level_type="subcat_id";
				   $level_value=$pcat."_".$cat."_".$subcat;
				   $level_name=$this->get_subcat_name($pcat,$cat,$subcat);
				}
				if($pcat!="" && $cat!="" && $subcat==""){
				   $pcat=substr($pcat, 9);
					$cat=substr($cat, 9);
					
				   $level_type="cat_id";
				   $level_value=$pcat."_".$cat;
				   $level_name=$this->get_cat_name($pcat,$cat);
				}
				if($pcat!="" && $cat=="" && $subcat==""){
				   $pcat=substr($pcat, 9);
					
					
				   $level_type="pcat_id";
				   $level_value=$pcat;
				   $level_name=$this->get_pcat_name($pcat);
				}

				if($pcat=="" && $cat=="" && $subcat==""){
					$level_type="all";
					$level_value="";
					$level_name="";
				}

            }
            else{
			 extract($this->input->post());
            }
            
            $data['p_category']=$this->provide_first_level_menu();
        $data["controller"]=$this;
		
		
        $data["current_controller"]="search";
        $data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
        $data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
        
            if($level_type=="brand_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id=$level_value_arr[3];
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$brand_id;
            }
            if($level_type=="subcat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$subcat_id;
            }
			if($level_type=="cat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$cat_id;
            }
			if($level_type=="pcat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id="";
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$pcat_id;
            }
            if($level_type=="product_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id=$level_value_arr[3];
                $product_id=$level_value_arr[4];
                $inventory_id="";
                $data['cat_current_id']=$product_id;
               
            }
			if($level_type=="all"){
                $level_value_arr=array();
                $pcat_id="";
                $cat_id="";
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']="";
            }
                $data["pcat_id"]=$pcat_id;
                $data['cat_id']=$cat_id;
                $data['subcat_id']=$subcat_id;
                $data['brand_id']=$brand_id;
                $data['product_id']=$product_id;
                $data['inventory_id']=$inventory_id;
                $data['searched_key']=$level_name;
                $data['level_type']=$level_type;
			$arr=array();
            $arr["pcat_id"]=$pcat_id;
            $arr['cat_id']=$cat_id;
            $arr['subcat_id']=$subcat_id;
            $arr['brand_id']=$brand_id;
            $arr['product_id']=$product_id;
             
            
            $data["category_tree"]=$arr;
            $data['level_value']=$level_value;
            $data['cat_current']=$level_type;
			$data["controller"]=$this;
            
            
			$data["current_controller"]="search";
			$data['search_keyword_index_page']="";
			if($this->input->post("search_keyword_index_page")!=""){
				$data['search_keyword_index_page']=$this->input->post("search_keyword_index_page");
			}
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('search_category_requestforquotation',$data);
			$this->load->view('footer');	
	
	}
	public function get_all_brand_names(){
		$result_all_brand_names_arr=$this->Model_search->get_all_brand_names();
		return $result_all_brand_names_arr;
	}
	/** request for quotation */

	 /** Become our franchise */
	public function becomeourfranchise(){
		$data["controller"]=$this;
		$method=current_url();
		$this->session->set_userdata("cur_page",$method);
		$data['page_name']='';
		$data['page_type']='';
		$data['page_id']='';
		$data['products_obj']='';
		
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		
		$data["current_controller"]="Search";
		$data["cur_Spage"]="cur_Spage";
		$data["cur_page"]="becomeourfranchise";
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		
	

		$this->load->view('header',$data);
		$this->load->view('becomeourfranchise');
		$this->load->view('footer');
	}
	function add_save_beomeourfranchise(){
		if(isset($_REQUEST))
		{
			$data = json_decode(file_get_contents("php://input"));
			if(isset($data))
			{
				// $beomeourfranchise_companyname=$this->db->escape_str($data->beomeourfranchise_companyname);
				$beomeourfranchise_name=$this->db->escape_str($data->beomeourfranchise_name);
				$beomeourfranchise_email=$this->db->escape_str($data->email);
				$beomeourfranchise_mobile=$this->db->escape_str($data->mobile);
				
				$pincode=$this->db->escape_str($data->pin);
				$city=$this->db->escape_str($data->city);
				$state=$this->db->escape_str($data->state);
				$country=$this->db->escape_str($data->country);
				
				$date = new DateTime();
				$mt=microtime();
				$salt="franchise_secure_id";
				$rand=mt_rand(1, 1000000);
				$ts=$date->getTimestamp();
				$str=$salt.$rand.$ts.$mt;
				$secure_id=md5($str);	
		
				sample_code();
				$password=sample_code();

				$result=$this->Customer_account->add_customer_result($beomeourfranchise_name,$beomeourfranchise_email,$beomeourfranchise_mobile,$password,$rand,$pincode,$city,$state,$country);

				if($result){

					$email_stuff_arr=email_stuff();
					$email_s=$email_stuff_arr["fromemail_emailtemplate"];
					//$toemail_emailtemplate_live_1=$email_stuff_arr["toemail_emailtemplate_live_1"];
					$toemail_emailtemplate_live_1="nanthinivinothkumar0227@gmail.com";
				
					//through Email
			

				$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color:color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
				
				<center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody>';
				
				
				$usermessage.='<tr><td align="center" valign="top" width="500" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr>';
				
				$usermessage.='</tbody></table></td></tr><tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 14px;">';

				$usermessage.='Become Our Franchise Enquiry Information :<br><br>';

				$usermessage.='<span style="font-size: 14px;"><b>Name : </b>'.$beomeourfranchise_name.'<br><b>Email : </b>'.$beomeourfranchise_email.'<br><b>Mobile: </b>'.$beomeourfranchise_mobile.'<br>';
				
				// $usermessage.='<b>City: </b>'.$beomeourfranchise_city.'<br>';
				$usermessage.='<b>City: </b>'.$city.'</span></p><br></td></tr>';
				

				//$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

				$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

				$usermessage.='</tbody> </table></tr></table></center> </body></html>';
			   
				
				$mail = new PHPMailer;
				/*$mail->isSMTP();
				$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
				$mail->Port = '587';//  live - comment it 
				$mail->Auth = true;//  live - comment it 
				$mail->SMTPAuth = true;
				$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
				$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
				$mail->SMTPSecure = 'tls';*/
				$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
				$mail->FromName = $email_stuff_arr["name_emailtemplate"];
				$mail->addAddress($toemail_emailtemplate_live_1);              
				//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
				//$mail->AddBCC("sucheta@axonlabs.in", "Sucheta");
				$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				$mail->Subject = 'Become Our Franchise';
				$mail->Body    = $usermessage;
						
				@$mail->send();

				/* mail to user */

				

				$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color:color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
				
				<center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody>';
				
				
				$usermessage.='<tr><td align="center" valign="top" width="500" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr>';
				
				$usermessage.='</tbody></table></td></tr><tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 14px;">';

				$usermessage.='Thank you for showing interest with '.SITE_NAME.'. We will get back to you soon. Please visit to your panel '.base_url().'/franchise_login with username : '.$beomeourfranchise_email.' and password: '.$password.'.<br><br> Your details are follows :<br>';

				$usermessage.='<span style="font-size: 14px;"><b>Name : </b>'.$beomeourfranchise_name.'<br><b>Email : </b>'.$beomeourfranchise_email.'<br><b>Mobile: </b>'.$beomeourfranchise_mobile.'<br>';
				
				// $usermessage.='<b>City: </b>'.$beomeourfranchise_city.'<br>';
				$usermessage.='<b>City: </b>'.$city.'</span></p><br></td></tr>';
				

				//$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

				$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

				$usermessage.='</tbody> </table></tr></table></center> </body></html>';
			   
				
				$mail = new PHPMailer;
				/*$mail->isSMTP();
				$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
				$mail->Port = '587';//  live - comment it 
				$mail->Auth = true;//  live - comment it 
				$mail->SMTPAuth = true;
				$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
				$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
				$mail->SMTPSecure = 'tls';*/
				$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
				$mail->FromName = $email_stuff_arr["name_emailtemplate"];
				$mail->addAddress($toemail_emailtemplate_live_1);              
				//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
				//$mail->AddBCC("sucheta@axonlabs.in", "Sucheta");
				$mail->addReplyTo($beomeourfranchise_email, $beomeourfranchise_name);
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				$mail->Subject = 'Become Our Franchise';
				$mail->Body    = $usermessage;
						
				@$mail->send();
				/* mail to user */


				//$flag=$this->Customer_account->sended_code_to_mail($email,$rand);
				//echo $flag;
					echo json_encode(array(
					'status' => 'yes',
					'secure_id'=>$secure_id
					));
					
				}else{
					echo json_encode(array(
					'status' => 'no'
					));
				}
				
				
			}
		}
	}
	/* Franchise login */

	public function franchise_login($order_id=""){
		//visitor_activity();
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$data["current_controller"]="search";
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		if($order_id!=""){
			$get_order_placed_date_customer_id_arr=$this->Model_search->get_order_placed_date_customer_id($order_id);
			$order_placed_date=$get_order_placed_date_customer_id_arr["order_placed_date"];
			$order_placed_date_format=date("jS F, Y \a\\t H:i",strtotime($order_placed_date))." Hrs";
			$customer_name=$this->Model_search->get_customer_name($get_order_placed_date_customer_id_arr["customer_id"]);
			$data["customer_name"]=$customer_name;
			$data["order_placed_date_format"]=$order_placed_date_format;
		}
		$data["order_id"]=$order_id;
		
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('franchise_login');
		$this->load->view('footer');
		
	}

	/* Franchise login */
/** get offer message addinv starts */
public function get_offer_message_for_single_external_and_internal_taggedinv($addon_invs){
	$is_all_offerprices_zero="yes";
	foreach($addon_invs as $data_inv){
		if($data_inv->tagged_type=="internal"){
			$tagged_inventory_offerprices_arr=explode(',',$data_inv->tagged_inventory_offerprices);
			$addon_single_or_multiple_tagged_inventories_in_frontend=$data_inv->addon_single_or_multiple_tagged_inventories_in_frontend; // for internal either it should be single or multiple any one strictly
			foreach($tagged_inventory_offerprices_arr as $offerprice){
				if($offerprice==0){
					
				}
				else{
					$is_all_offerprices_zero="no";
				}
			}
		}
		
		
		
	}
	if($addon_single_or_multiple_tagged_inventories_in_frontend=="single"){
		if($is_all_offerprices_zero=="yes"){
			$message="Bingo! you can choose any ONE below product absolutely FREE";
		}
		if($is_all_offerprices_zero=="no"){
			$message="Hey! You can also choose any ONE of the featured products at a discount exclusively on Hurbz";
		}
	}
	if($addon_single_or_multiple_tagged_inventories_in_frontend=="multiple"){
		$message="Recommended products you can buy additionally";
	}
	return $message;
}
public function is_offer_discount_exists($inventory_id){
	$count_offer_discount=$this->Model_search->is_offer_discount_exists($inventory_id);
	if($count_offer_discount>0){
		return "yes";
	}
	else{
		return "no";
	}
}
/** get offer message addinv ends */	

/* delivered fast */
public function delivered_fast(){
	//////read_and_update_traffic();
	////visitor_activity();						
	//echo $da=get_cookie('remember_me1');
	$method=current_url();
	$this->session->set_userdata("products_page",$method);
	$data["ui_arr"]=$this->get_ui_data_for_index();
	$data['page_name']='';
	$data['page_type']='';
	$data['page_id']='';
	$data['products_obj']='';
	$data['p_category']=$this->provide_first_level_menu();
	$data["current_controller"]="search";
	$data["controller"]=$this;
	$data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
	$data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
	$ft_img=$this->Customer_frontend->foot_image_index();
	$data['ft_img']='';
	if(!empty($ft_img)){
		if(isset($ft_img->background_image)){
			$data['ft_img']=$ft_img->background_image;
		}
	}
	$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
	$data["ganesh_image_yes_or_no"]=$this->Customer_frontend->ganesh_image_yes_or_noFun();


	$parent_categories=$this->Customer_frontend->parent_category();
	$data['parent_categories']=$parent_categories;

	$front_product_display=$this->Customer_frontend->front_product_display();
	$data['front_product_display']=$front_product_display;
		

	$this->load->view('header',$data);
	
	$this->load->view('delivered_fast');
	$this->load->view('footer');
}
public function design($type=''){
	if($type!=''){
		$type=ucfirst(str_replace('_', ' ', $type));
	}else{
		$type="home";
	}
	
	$data["design_type"]= $type;
	//////read_and_update_traffic();
	////visitor_activity();						
	//echo $da=get_cookie('remember_me1');
	$method=current_url();
	$this->session->set_userdata("products_page",$method);
	$data["ui_arr"]=$this->get_ui_data_for_index();
	$data['page_name']='';
	$data['page_type']='';
	$data['page_id']='';
	$data['products_obj']='';
	$data['p_category']=$this->provide_first_level_menu();
	$data["current_controller"]="search";
	$data["controller"]=$this;
	$data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
	$data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
	$ft_img=$this->Customer_frontend->foot_image_index();
	$data['ft_img']='';
	if(!empty($ft_img)){
		if(isset($ft_img->background_image)){
			$data['ft_img']=$ft_img->background_image;
		}
	}
	$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
	$data["ganesh_image_yes_or_no"]=$this->Customer_frontend->ganesh_image_yes_or_noFun();
	
	$data["design_type"]=$type;
	$this->load->view('header',$data);
	//echo $type; exit;
	if(strtolower($type)=='modular kitchen'){
		$this->load->view('design_kitchen');
	}elseif(strtolower($type)=='bedroom'){
		$this->load->view('design_bedroom');
	}elseif(strtolower($type)=='office room'){
		$this->load->view('design_our_office');
	}elseif(strtolower($type)=='conference room'){
		$this->load->view('design_conference_room');
	}elseif(strtolower($type)=='workstations'){
		$this->load->view('design_workstations');
	}else{
		$this->load->view('design');
	}
	
	$this->load->view('footer');
}

/* delivered fast */
/* home design */
function add_design(){
	if(isset($_REQUEST))
	{
		$data = json_decode(file_get_contents("php://input"));
		if(isset($data))
		{
			// $beomeourfranchise_companyname=$this->db->escape_str($data->beomeourfranchise_companyname);
			$name=$this->db->escape_str($data->name);
			$email=$this->db->escape_str($data->email);
			$mobile=$this->db->escape_str($data->mobile);
			
			$pincode=$this->db->escape_str($data->pin);
			$city=$this->db->escape_str($data->city);
			$state=$this->db->escape_str($data->state);
			$country=$this->db->escape_str($data->country);
			

			if(isset($data->size_of_home)){
				$size_of_home=$this->db->escape_str($data->size_of_home);
			}else{
				$size_of_home='1';
			}
			
			if(isset($data->size_of_home)){
				$size_of_home_des=$this->db->escape_str($data->size_of_home_des);

			}else{
				$size_of_home_des='small';
			}
			if(isset($data->design_type)){
				$design_type=$this->db->escape_str($data->design_type);

			}else{
				$design_type='home';
			}

			$interior_budget=$this->db->escape_str($data->interior_budget);
			//$design_type=$this->db->escape_str($data->design_type);
			
			$result=$this->Customer_account->add_design_result($name,$email,$mobile,$pincode,$city,$state,$country,$size_of_home,$interior_budget,$design_type,$size_of_home_des);

			if($result){

				$email_stuff_arr=email_stuff();
				$email_s=$email_stuff_arr["fromemail_emailtemplate"];
				//$toemail_emailtemplate_live_1=$email_stuff_arr["toemail_emailtemplate_live_1"];
				$toemail_emailtemplate_live_1="nanthinivinothkumar0227@gmail.com";
			
				//through Email
		

			$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color:color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
			
			<center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody>';
			
			
			$usermessage.='<tr><td align="center" valign="top" width="500" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr>';
			
			$usermessage.='</tbody></table></td></tr><tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 14px;">';

			$usermessage.='<h3>'.ucfirst($design_type).' Design Request :</h3><br>';


			$usermessage.='<span style="font-size: 14px;">';

			$usermessage.='<b>Size: </b>'.$size_of_home.' BHK ('.$size_of_home_des.')<br>';
			$usermessage.='<b>Budget: </b>'.$interior_budget.'<br>';

			$usermessage.='<b>Name : </b>'.$name.'<br><b>Email : </b>'.$email.'<br><b>Mobile: </b>'.$mobile.'<br>';
			

			$usermessage.='<b>City: </b>'.$city.'<br>';
			$usermessage.='<b>State: </b>'.$state.'<br>';
			$usermessage.='<b>Country: </b>'.$country.'<br>';
			$usermessage.='<b>Pincode: </b>'.$pincode.'<br>';


			$usermessage.='</span></p><br></td></tr>';
			

			$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

			$usermessage.='</tbody> </table></tr></table></center> </body></html>';
		   
			
			$mail = new PHPMailer;
			/*$mail->isSMTP();
			$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
			$mail->Port = '587';//  live - comment it 
			$mail->Auth = true;//  live - comment it 
			$mail->SMTPAuth = true;
			$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
			$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
			$mail->SMTPSecure = 'tls';*/
			$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
			$mail->FromName = $email_stuff_arr["name_emailtemplate"];
			$mail->addAddress($toemail_emailtemplate_live_1);              
			//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
			//$mail->AddBCC("sucheta@axonlabs.in", "Sucheta");
			$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
			$mail->WordWrap = 50;
			$mail->isHTML(true);
			$mail->Subject = ucfirst($design_type).' Design Request';
			$mail->Body    = $usermessage;
					
			@$mail->send();
			//$flag=$this->Customer_account->sended_code_to_mail($email,$rand);
			//echo $flag;
				echo json_encode(array(
				'status' => 'yes'
				));
			}else{
				echo json_encode(array(
				'status' => 'no'
				));
			}
			
			
		}
	}
}
/* home design */

/* kitchen design */
function add_design_kitchen(){
	if(isset($_REQUEST))
	{
		$data = json_decode(file_get_contents("php://input"));
		if(isset($data))
		{
			// $beomeourfranchise_companyname=$this->db->escape_str($data->beomeourfranchise_companyname);
			$name=$this->db->escape_str($data->name);
			$email=$this->db->escape_str($data->email);
			$mobile=$this->db->escape_str($data->mobile);
			
			$pincode=$this->db->escape_str($data->pin);
			$city=$this->db->escape_str($data->city);
			$state=$this->db->escape_str($data->state);
			$country=$this->db->escape_str($data->country);
			

			if(isset($data->design_type)){
				$design_type=$this->db->escape_str($data->design_type);

			}else{
				$design_type='home';
			}

			$interior_budget=$this->db->escape_str($data->interior_budget);
			$kitchen_layout=(isset($data->kitchen_layout)) ? $this->db->escape_str($data->kitchen_layout) : '';
			$area_of_kitchen=(isset($data->area_of_kitchen)) ? $this->db->escape_str($data->area_of_kitchen) : '';
			$kitchen_top=(isset($data->area_of_kitchen)) ? $this->db->escape_str($data->kitchen_top) : '';
			
			$result=$this->Customer_account->add_design_kitchen_result($name,$email,$mobile,$pincode,$city,$state,$country,$interior_budget,$design_type,$kitchen_layout,$area_of_kitchen,$kitchen_top);

			if($result){

				$email_stuff_arr=email_stuff();
				$email_s=$email_stuff_arr["fromemail_emailtemplate"];
				//$toemail_emailtemplate_live_1=$email_stuff_arr["toemail_emailtemplate_live_1"];
				$toemail_emailtemplate_live_1="nanthinivinothkumar0227@gmail.com";
			
				//through Email
		

			$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color:color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
			
			<center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody>';
			
			
			$usermessage.='<tr><td align="center" valign="top" width="500" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr>';
			
			$usermessage.='</tbody></table></td></tr><tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 14px;">';

			$usermessage.='<h3>'.ucfirst($design_type).' Design Request :</h3><br>';


			$usermessage.='<span style="font-size: 14px;">';

			$usermessage.='<b>Kitchens Layout: </b>'.$kitchen_layout.'<br>';
			$usermessage.='<b>Approximate Area Of Kitchen: </b>'.$interior_budget.'<br>';
			$usermessage.='<b>Kind Of Kitchen Top: </b>'.$kitchen_top.'<br>';

			$usermessage.='<b>Name : </b>'.$name.'<br><b>Email : </b>'.$email.'<br><b>Mobile: </b>'.$mobile.'<br>';
			

			$usermessage.='<b>City: </b>'.$city.'<br>';
			$usermessage.='<b>State: </b>'.$state.'<br>';
			$usermessage.='<b>Country: </b>'.$country.'<br>';
			$usermessage.='<b>Pincode: </b>'.$pincode.'<br>';


			$usermessage.='</span></p><br></td></tr>';
			

			$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

			$usermessage.='</tbody> </table></tr></table></center> </body></html>';
		   
			
			$mail = new PHPMailer;
			/*$mail->isSMTP();
			$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
			$mail->Port = '587';//  live - comment it 
			$mail->Auth = true;//  live - comment it 
			$mail->SMTPAuth = true;
			$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
			$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
			$mail->SMTPSecure = 'tls';*/
			$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
			$mail->FromName = $email_stuff_arr["name_emailtemplate"];
			$mail->addAddress($toemail_emailtemplate_live_1);              
			//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
			//$mail->AddBCC("sucheta@axonlabs.in", "Sucheta");
			$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
			$mail->WordWrap = 50;
			$mail->isHTML(true);
			$mail->Subject = ucfirst($design_type).' Design Request';
			$mail->Body    = $usermessage;
					
			@$mail->send();
			//$flag=$this->Customer_account->sended_code_to_mail($email,$rand);
			//echo $flag;
				echo json_encode(array(
				'status' => 'yes'
				
				));
			}else{
				echo json_encode(array(
				'status' => 'no'
				));
			}
			
			
		}
	}
}

function add_design_bedroom(){
	if(isset($_REQUEST))
	{
		$data = json_decode(file_get_contents("php://input"));
		if(isset($data))
		{
			// $beomeourfranchise_companyname=$this->db->escape_str($data->beomeourfranchise_companyname);
			$name=$this->db->escape_str($data->name);
			$email=$this->db->escape_str($data->email);
			$mobile=$this->db->escape_str($data->mobile);
			
			$pincode=$this->db->escape_str($data->pin);
			$city=$this->db->escape_str($data->city);
			$state=$this->db->escape_str($data->state);
			$country=$this->db->escape_str($data->country);
			

			if(isset($data->design_type)){
				$design_type=$this->db->escape_str($data->design_type);

			}else{
				$design_type='home';
			}

			$interior_budget=$this->db->escape_str($data->interior_budget);

			if(isset($data->bedroom_layout)){
				//$bedroom_layout=$this->db->escape_str($data->bedroom_layout);
				$bedroom_layout=(array) $data->bedroom_layout;
				$bedroom_layout=array_keys($bedroom_layout);
				$bedroom_layout=(!empty($bedroom_layout)) ? implode(',',$bedroom_layout) : '';
				$bl = preg_replace('/[_]+/', ' ', ($bedroom_layout));
				$bl = preg_replace('/[,]+/', ', ', ($bl));
				$bl = ucwords($bl);
				$bedroom_layout=$this->db->escape_str($bl);

			}else{
				$bedroom_layout='';
			}
			
			//$bedroom_layout=(isset($data->bedroom_layout)) ? $this->db->escape_str(implode(',',$data->bedroom_layout)) : '';
			$bedroom_style=(isset($data->bedroom_style)) ? $this->db->escape_str($data->bedroom_style) : '';
			$area_of_bedroom=(isset($data->area_of_bedroom)) ? $this->db->escape_str($data->area_of_bedroom) : '';
			$bedroom_utility=(isset($data->bedroom_utility)) ? $this->db->escape_str($data->bedroom_utility) : '';
			
			$result=$this->Customer_account->add_design_bedroom_result($name,$email,$mobile,$pincode,$city,$state,$country,$interior_budget,$design_type,$bedroom_layout,$bedroom_style,$area_of_bedroom,$bedroom_utility);

			if($result){

				$email_stuff_arr=email_stuff();
				$email_s=$email_stuff_arr["fromemail_emailtemplate"];
				//$toemail_emailtemplate_live_1=$email_stuff_arr["toemail_emailtemplate_live_1"];
				$toemail_emailtemplate_live_1="nanthinivinothkumar0227@gmail.com";
			
				//through Email
		

			$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color:color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
			
			<center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody>';
			
			
			$usermessage.='<tr><td align="center" valign="top" width="500" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr>';
			
			$usermessage.='</tbody></table></td></tr><tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 14px;">';

			$usermessage.='<h3>'.ucfirst($design_type).' Design Request :</h3><br>';


			$usermessage.='<span style="font-size: 14px;">';

			$usermessage.='<b>Bedroom Layout: </b>'.$bedroom_layout.'<br>';
			$usermessage.='<b>Bedroom Style: </b>'.$bedroom_style.'<br>';
			$usermessage.='<b>Area Of Bedroom: </b>'.$area_of_bedroom.'<br>';
			$usermessage.='<b>Bedroom Utility: </b>'.$bedroom_utility.'<br>';

			$usermessage.='<b>Budget: </b>'.$interior_budget.'<br>';

			$usermessage.='<b>Name : </b>'.$name.'<br><b>Email : </b>'.$email.'<br><b>Mobile: </b>'.$mobile.'<br>';
			

			$usermessage.='<b>City: </b>'.$city.'<br>';
			$usermessage.='<b>State: </b>'.$state.'<br>';
			$usermessage.='<b>Country: </b>'.$country.'<br>';
			$usermessage.='<b>Pincode: </b>'.$pincode.'<br>';


			$usermessage.='</span></p><br></td></tr>';
			

			$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

			$usermessage.='</tbody> </table></tr></table></center> </body></html>';
		   
			
			$mail = new PHPMailer;
			/*$mail->isSMTP();
			$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
			$mail->Port = '587';//  live - comment it 
			$mail->Auth = true;//  live - comment it 
			$mail->SMTPAuth = true;
			$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
			$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
			$mail->SMTPSecure = 'tls';*/
			$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
			$mail->FromName = $email_stuff_arr["name_emailtemplate"];
			$mail->addAddress($toemail_emailtemplate_live_1);              
			//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
			//$mail->AddBCC("sucheta@axonlabs.in", "Sucheta");
			$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
			$mail->WordWrap = 50;
			$mail->isHTML(true);
			$mail->Subject = ucfirst($design_type).' Design Request';
			$mail->Body    = $usermessage;
					
			@$mail->send();
			//$flag=$this->Customer_account->sended_code_to_mail($email,$rand);
			//echo $flag;
				echo json_encode(array(
				'status' => 'yes'
				
				));
			}else{
				echo json_encode(array(
				'status' => 'no'
				));
			}
			
			
		}
	}
}
function add_design_conference_room(){
	if(isset($_REQUEST))
	{
		$data = json_decode(file_get_contents("php://input"));
		if(isset($data))
		{
			// $beomeourfranchise_companyname=$this->db->escape_str($data->beomeourfranchise_companyname);
			$name=$this->db->escape_str($data->name);
			$email=$this->db->escape_str($data->email);
			$mobile=$this->db->escape_str($data->mobile);
			
			$pincode=$this->db->escape_str($data->pin);
			$city=$this->db->escape_str($data->city);
			$state=$this->db->escape_str($data->state);
			$country=$this->db->escape_str($data->country);
			

			if(isset($data->design_type)){
				$design_type=$this->db->escape_str($data->design_type);

			}else{
				$design_type='home';
			}

			$interior_budget=$this->db->escape_str($data->interior_budget);

			if(isset($data->conf_room_layout)){
				//$conf_room_layout=$this->db->escape_str($data->conf_room_layout);
				$conf_room_layout=(array) $data->conf_room_layout;
				$ol=filter_array( $conf_room_layout,'selected');
				$conf_room_layout=(!empty($ol)) ? implode(',',$ol) : '';
				$conf_room_layout=$this->db->escape_str($conf_room_layout);

			}else{
				$conf_room_layout='';
			}
			
			$conf_room_style=(isset($data->conf_room_style)) ? $this->db->escape_str($data->conf_room_style) : '';
			$size_of_conf_room=(isset($data->size_of_conf_room)) ? $this->db->escape_str($data->size_of_conf_room) : '';
			$conf_room_type=(isset($data->conf_room_type)) ? $this->db->escape_str($data->conf_room_type) : '';
			
			//echo $conf_room_layout;
			//exit;

			$result=$this->Customer_account->add_design_conf_room_result($name,$email,$mobile,$pincode,$city,$state,$country,$interior_budget,$design_type,$conf_room_layout,$conf_room_style,$size_of_conf_room,$conf_room_type);

			if($result){

				$email_stuff_arr=email_stuff();
				$email_s=$email_stuff_arr["fromemail_emailtemplate"];
				//$toemail_emailtemplate_live_1=$email_stuff_arr["toemail_emailtemplate_live_1"];
				$toemail_emailtemplate_live_1="nanthinivinothkumar0227@gmail.com";
			
				//through Email
		

			$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color:color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
			
			<center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody>';
			
			
			$usermessage.='<tr><td align="center" valign="top" width="500" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr>';
			
			$usermessage.='</tbody></table></td></tr><tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 14px;">';

			$usermessage.='<h3>'.ucfirst($design_type).' Design Request :</h3><br>';


			$usermessage.='<span style="font-size: 14px;">';

			$conf_room_layout = preg_replace('/[ ,]+/', ' ', ($conf_room_layout));	

			$usermessage.='<b>Type: </b>'.$conf_room_type.'<br>';
			$usermessage.='<b>Size Of Conference Room: </b>'.$size_of_conf_room.'<br>';
			$usermessage.='<b>Fitting: </b>'.$conf_room_layout.'<br>';
			$usermessage.='<b>Style: </b>'.$conf_room_style.'<br>';
			
			$usermessage.='<b>Budget: </b>'.$interior_budget.'<br>';

			$usermessage.='<b>Name : </b>'.$name.'<br><b>Email : </b>'.$email.'<br><b>Mobile: </b>'.$mobile.'<br>';
			

			$usermessage.='<b>City: </b>'.$city.'<br>';
			$usermessage.='<b>State: </b>'.$state.'<br>';
			$usermessage.='<b>Country: </b>'.$country.'<br>';
			$usermessage.='<b>Pincode: </b>'.$pincode.'<br>';


			$usermessage.='</span></p><br></td></tr>';
			

			$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

			$usermessage.='</tbody> </table></tr></table></center> </body></html>';
		   
			
			$mail = new PHPMailer;
			/*$mail->isSMTP();
			$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
			$mail->Port = '587';//  live - comment it 
			$mail->Auth = true;//  live - comment it 
			$mail->SMTPAuth = true;
			$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
			$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
			$mail->SMTPSecure = 'tls';*/
			$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
			$mail->FromName = $email_stuff_arr["name_emailtemplate"];
			$mail->addAddress($toemail_emailtemplate_live_1);              
			//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
			//$mail->AddBCC("sucheta@axonlabs.in", "Sucheta");
			$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
			$mail->WordWrap = 50;
			$mail->isHTML(true);
			$mail->Subject = ucfirst($design_type).' Design Request';
			$mail->Body    = $usermessage;
					
			@$mail->send();
			//$flag=$this->Customer_account->sended_code_to_mail($email,$rand);
			//echo $flag;
				echo json_encode(array(
				'status' => 'yes'
				
				));
			}else{
				echo json_encode(array(
				'status' => 'no'
				));
			}
			
			
		}
	}
}
function add_design_workstations(){
	if(isset($_REQUEST))
	{
		$data = json_decode(file_get_contents("php://input"));
		if(isset($data))
		{
			// $beomeourfranchise_companyname=$this->db->escape_str($data->beomeourfranchise_companyname);
			$name=$this->db->escape_str($data->name);
			$email=$this->db->escape_str($data->email);
			$mobile=$this->db->escape_str($data->mobile);
			
			$pincode=$this->db->escape_str($data->pin);
			$city=$this->db->escape_str($data->city);
			$state=$this->db->escape_str($data->state);
			$country=$this->db->escape_str($data->country);
			

			if(isset($data->design_type)){
				$design_type=$this->db->escape_str($data->design_type);

			}else{
				$design_type='home';
			}

			$interior_budget=$this->db->escape_str($data->interior_budget);

			if(isset($data->workstations_style)){
				$workstations_style=(array) $data->workstations_style;
				$ol=filter_array($workstations_style,'selected');
				$workstations_style=(!empty($ol)) ? implode(',',$ol) : '';
				$workstations_style=$this->db->escape_str($workstations_style);

			}else{
				$workstations_style='';
			}
			
			$workstations_material=(isset($data->workstations_material)) ? $this->db->escape_str($data->workstations_material) : '';
			$size_of_workstations=(isset($data->size_of_workstations)) ? $this->db->escape_str($data->size_of_workstations) : '';
			$seating_capacity=(isset($data->seating_capacity)) ? $this->db->escape_str($data->seating_capacity) : '';
			
			//echo $workstations_style;
			//exit;

			$result=$this->Customer_account->add_design_workstations_result($name,$email,$mobile,$pincode,$city,$state,$country,$interior_budget,$design_type,$workstations_style,$workstations_material,$size_of_workstations,$seating_capacity);

			if($result){

				$email_stuff_arr=email_stuff();
				$email_s=$email_stuff_arr["fromemail_emailtemplate"];
				//$toemail_emailtemplate_live_1=$email_stuff_arr["toemail_emailtemplate_live_1"];
				$toemail_emailtemplate_live_1="nanthinivinothkumar0227@gmail.com";
			
				//through Email
		

			$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color:color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
			
			<center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody>';
			
			
			$usermessage.='<tr><td align="center" valign="top" width="500" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr>';
			
			$usermessage.='</tbody></table></td></tr><tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 14px;">';

			$usermessage.='<h3>'.ucfirst($design_type).' Design Request :</h3><br>';


			$usermessage.='<span style="font-size: 14px;">';

			//workstations_style,$workstations_style,$size_of_workstations,$seating_capacity

			$workstations_style = preg_replace('/[ ,]+/', ' ', ($workstations_style));	

			$usermessage.='<b>Seating Capacity: </b>'.$seating_capacity.'<br>';
			$usermessage.='<b>Size Of Office: </b>'.$size_of_workstations.'<br>';
			$usermessage.='<b>Style: </b>'.$workstations_style.'<br>';
			$usermessage.='<b>Material Preference: </b>'.$workstations_material.'<br>';
			
			$usermessage.='<b>Budget: </b>'.$interior_budget.'<br>';

			$usermessage.='<b>Name : </b>'.$name.'<br><b>Email : </b>'.$email.'<br><b>Mobile: </b>'.$mobile.'<br>';
			
			$usermessage.='<b>City: </b>'.$city.'<br>';
			$usermessage.='<b>State: </b>'.$state.'<br>';
			$usermessage.='<b>Country: </b>'.$country.'<br>';
			$usermessage.='<b>Pincode: </b>'.$pincode.'<br>';

			$usermessage.='</span></p><br></td></tr>';
			

			$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

			$usermessage.='</tbody> </table></tr></table></center> </body></html>';
		   
			
			$mail = new PHPMailer;
			/*$mail->isSMTP();
			$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
			$mail->Port = '587';//  live - comment it 
			$mail->Auth = true;//  live - comment it 
			$mail->SMTPAuth = true;
			$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
			$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
			$mail->SMTPSecure = 'tls';*/
			$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
			$mail->FromName = $email_stuff_arr["name_emailtemplate"];
			$mail->addAddress($toemail_emailtemplate_live_1);              
			//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
			//$mail->AddBCC("sucheta@axonlabs.in", "Sucheta");
			$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
			$mail->WordWrap = 50;
			$mail->isHTML(true);
			$mail->Subject = ucfirst($design_type).' Design Request';
			$mail->Body    = $usermessage;
					
			@$mail->send();
			//$flag=$this->Customer_account->sended_code_to_mail($email,$rand);
			//echo $flag;
				echo json_encode(array(
				'status' => 'yes'
				
				));
			}else{
				echo json_encode(array(
				'status' => 'no'
				));
			}
			
			
		}
	}
}
function add_design_our_office(){
	if(isset($_REQUEST))
	{
		$data = json_decode(file_get_contents("php://input"));
		if(isset($data))
		{
			// $beomeourfranchise_companyname=$this->db->escape_str($data->beomeourfranchise_companyname);
			$name=$this->db->escape_str($data->name);
			$email=$this->db->escape_str($data->email);
			$mobile=$this->db->escape_str($data->mobile);
			
			$pincode=$this->db->escape_str($data->pin);
			$city=$this->db->escape_str($data->city);
			$state=$this->db->escape_str($data->state);
			$country=$this->db->escape_str($data->country);
			

			if(isset($data->design_type)){
				$design_type=$this->db->escape_str($data->design_type);

			}else{
				$design_type='home';
			}

			$interior_budget=$this->db->escape_str($data->interior_budget);

			if(isset($data->office_layout)){
				
				$office_layout=(array) $data->office_layout;
				$ol=filter_array( $office_layout,'selected');
				$office_layout=(!empty($ol)) ? implode(',',$ol) : '';
				$office_layout=$this->db->escape_str($office_layout);

			}else{
				$office_layout='';
			}
			
			if(isset($data->requirement)){
				
				$requirement=(array) $data->requirement;
				$arrr=filter_array($requirement,'selected');
				$rq=(!empty($arrr)) ? implode(',',$arrr) : '';
				$requirement=$this->db->escape_str($rq);

			}else{
				$requirement='';
			}

			//echo $office_layout;
			//echo '<br>';
			//echo $requirement;
			//$bedroom_layout=(isset($data->bedroom_layout)) ? $this->db->escape_str(implode(',',$data->bedroom_layout)) : '';
			$office_type=(isset($data->office_type)) ? $this->db->escape_str($data->office_type) : '';
			$size_of_office=(isset($data->size_of_office)) ? $this->db->escape_str($data->size_of_office) : '';
			$office_style=(isset($data->office_style)) ? $this->db->escape_str($data->office_style) : '';
			
			$result=$this->Customer_account->add_design_our_office_result($name,$email,$mobile,$pincode,$city,$state,$country,$interior_budget,$design_type,$office_layout,$requirement,$office_type,$size_of_office,$office_style);

			if($result){

				$email_stuff_arr=email_stuff();
				$email_s=$email_stuff_arr["fromemail_emailtemplate"];
				//$toemail_emailtemplate_live_1=$email_stuff_arr["toemail_emailtemplate_live_1"];
				$toemail_emailtemplate_live_1="nanthinivinothkumar0227@gmail.com";
			
				//through Email
		

			$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color:color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
			
			<center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody>';
			
			
			$usermessage.='<tr><td align="center" valign="top" width="500" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr>';
			
			$usermessage.='</tbody></table></td></tr><tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 14px;">';

			$usermessage.='<h3>'.ucfirst($design_type).' Design Request :</h3><br>';


			$usermessage.='<span style="font-size: 14px;">';

			
			$office_layout = preg_replace('/[ ,]+/', '<br>', ($office_layout));	

			$usermessage.='<b>Office Layout :</b> '.$office_layout.'';
			$usermessage.='<br><b>Office Requirement :</b> '.$requirement.'';
			$usermessage.='<br><b>Office Type : </b>'.$office_type.'';
			$usermessage.='<br><b>Size Of Office : </b> '.$size_of_office.'';
			$usermessage.='<br><b>Office Style : </b>'.$office_style.'<br>';
			
			$usermessage.='<b>Budget: </b>'.$interior_budget.'<br>';

			$usermessage.='<b>Name : </b>'.$name.'<br><b>Email : </b>'.$email.'<br><b>Mobile: </b>'.$mobile.'<br>';
			

			$usermessage.='<b>City: </b>'.$city.'<br>';
			$usermessage.='<b>State: </b>'.$state.'<br>';
			$usermessage.='<b>Country: </b>'.$country.'<br>';
			$usermessage.='<b>Pincode: </b>'.$pincode.'<br>';


			$usermessage.='</span></p><br></td></tr>';
			

			$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

			$usermessage.='</tbody> </table></tr></table></center> </body></html>';
		   
			
			$mail = new PHPMailer;
			/*$mail->isSMTP();
			$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
			$mail->Port = '587';//  live - comment it 
			$mail->Auth = true;//  live - comment it 
			$mail->SMTPAuth = true;
			$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
			$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
			$mail->SMTPSecure = 'tls';*/
			$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
			$mail->FromName = $email_stuff_arr["name_emailtemplate"];
			$mail->addAddress($toemail_emailtemplate_live_1);              
			//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
			//$mail->AddBCC("sucheta@axonlabs.in", "Sucheta");
			$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
			$mail->WordWrap = 50;
			$mail->isHTML(true);
			$mail->Subject = ucfirst($design_type).' Design Request';
			$mail->Body    = $usermessage;
					
			@$mail->send();
			//$flag=$this->Customer_account->sended_code_to_mail($email,$rand);
			//echo $flag;
				echo json_encode(array(
				'status' => 'yes'
				
				));
			}else{
				echo json_encode(array(
				'status' => 'no'
				));
			}
			
			
		}
	}
}
/* kitchen design */



/*** shopbybrand starts ***/
	
	
	
	public function search_category_shopbybrand($pcat,$cat='',$subcat=''){
		////read_and_update_traffic();
		//visitor_activity_analytics();

		if($cat!="" && $subcat!=""){
			$category_is_exists="yes";
			$subcategory_is_exists="yes";
		}
		else if($cat!="" && $subcat==""){
			$category_is_exists="yes";
			$subcategory_is_exists="no";
		}
		else if($cat=="" && $subcat!=""){
			$category_is_exists="no";
			$subcategory_is_exists="yes";
		}
		else{
			$category_is_exists="no";
			$subcategory_is_exists="no";
		}
		if(!$this->input->post()){
			$parentcategory_name="";
			$category_name="";
			$subcategory_name="";
			if($pcat!=""){
				$pcat=substr($pcat, 9);
				if($pcat!=0){
					$parentcategory_name=$this->Model_search->get_parentcategory_name($pcat);
					$parentcategory_name=strtolower($parentcategory_name);
					$parentcategory_name = str_replace(' ', '-', $parentcategory_name);
				}
				else{
					$parentcategory_name="null";
				}
			}
			if($cat!=""){
				$cat=substr($cat, 9);
				$category_name=$this->Model_search->get_category_name($cat);
				$category_name=strtolower($category_name);
				$category_name = str_replace(' ', '-', $category_name);
			}
			if($subcat!=""){
				$subcat=substr($subcat, 9);
				$subcategory_name=$this->Model_search->get_subcategory_name($subcat);
				$subcategory_name=strtolower($subcategory_name);
				$subcategory_name = str_replace(' ', '-', $subcategory_name);
			}
			//redirect(site_url("catalog_shopbybrand/".$parentcategory_name));
			redirect(site_url("catalog_shopbybrand/".$parentcategory_name."/".$category_name."/".$subcategory_name));
		}
		else{
			
			//visitor_activity();
		$method=current_url();
		$this->session->set_userdata("products_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		
		}

		$search_keywords=$this->input->post('search_keywords');
		if($search_keywords!=''){
			$search_keyword_index_page=strtolower($search_keywords);
			$flag=$this->Model_search->dump_search_keywords($search_keywords);
			//echo $flag;
		}
			$search_keyword="";$search_cat="";$page_rows="";$pagenum="";
            $sorting_by="";$cat_current="";$cat_current_id="";$type_of_filter="";
            $color_filter="";$color_prev="";$size_filter="";$size_prev="";
            $price_filter="";$price_prev="";$brand_filter="";$brand_prev="";
			//print_r($this->input->post());exit;
			if(!$this->input->post()){
				
				if($pcat!="" && $cat!="" && $subcat!=""){
				   $pcat=substr($pcat, 9);
					$cat=substr($cat, 9);
					$subcat=substr($subcat, 9);
				   $level_type="subcat_id";
				   $level_value=$pcat."_".$cat."_".$subcat;
				   $level_name=$this->get_subcat_name($pcat,$cat,$subcat);
				}
				if($pcat!="" && $cat!="" && $subcat==""){
				   $pcat=substr($pcat, 9);
					$cat=substr($cat, 9);
					
				   $level_type="cat_id";
				   $level_value=$pcat."_".$cat;
				   $level_name=$this->get_cat_name($pcat,$cat);
				}
				if($pcat!="" && $cat=="" && $subcat==""){
				   $pcat=substr($pcat, 9);
					
					
				   $level_type="pcat_id";
				   $level_value=$pcat;
				   $level_name=$this->get_pcat_name($pcat);
				}
            }
            else{
			 	extract($this->input->post());
			  	$pcat=substr($pcat, 9);
				$cat=substr($cat, 9);
				$subcat=substr($subcat, 9);
            }
            
            $data['p_category']=$this->provide_first_level_menu();
        $data["controller"]=$this;
		
		
        $data["current_controller"]="search";
        $data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
        $data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
        
            if($level_type=="brand_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id=$level_value_arr[3];
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$brand_id;
            }
            if($level_type=="subcat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$subcat_id;
            }
			if($level_type=="cat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$cat_id;
            }
			if($level_type=="pcat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id="";
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$pcat_id;
            }
            if($level_type=="product_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id=$level_value_arr[3];
                $product_id=$level_value_arr[4];
                $inventory_id="";
                $data['cat_current_id']=$product_id;
               
            }
                $data["pcat_id"]=$pcat_id;
                $data['cat_id']=$cat_id;
                $data['subcat_id']=$subcat_id;
                $data['brand_id']=$brand_id;
                $data['product_id']=$product_id;
                $data['inventory_id']=$inventory_id;
                $data['searched_key']=$level_name;
                $data['level_type']=$level_type;
			$arr=array();
            $arr["pcat_id"]=$pcat_id;
            $arr['cat_id']=$cat_id;
            $arr['subcat_id']=$subcat_id;
            $arr['brand_id']=$brand_id;
            $arr['product_id']=$product_id;
             
            
            $data["category_tree"]=$arr;
            $data['level_value']=$level_value;
            $data['cat_current']=$level_type;
			$data["controller"]=$this;
            
            
			$data["current_controller"]="search";
			$data['search_keyword_index_page']="";
			if($this->input->post("search_keyword_index_page")!=""){
				$data['search_keyword_index_page']=$this->input->post("search_keyword_index_page");
			}
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			
			
			if($category_is_exists=="yes" && $subcategory_is_exists=="yes"){
				if($pcat!=""){
					$get_parent_cat_info_obj=$this->get_parent_cat_info($pcat);
				}
				if($cat!=""){
					$get_cat_info_obj=$this->get_cat_info($cat);
				}
				if($subcat!=""){
					$get_subcat_info_obj=$this->get_subcat_info($subcat);
				}
				
				if($pcat=="" || $cat=="" || $subcat==""){
					$this->load->view('error404',$data);
				}
				else if(preg_match("/[a-z]/i", $pcat) || preg_match("/[a-z]/i", $cat) || preg_match("/[a-z]/i", $subcat)){
					$this->load->view('error404',$data);
				}
				else if($pcat==0){
					if(empty($get_cat_info_obj) || empty($get_subcat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category_shopbybrand',$data);
					}
				}
				else if($pcat!=0){
					if(empty($get_parent_cat_info_obj) || empty($get_cat_info_obj) || empty($get_subcat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category_shopbybrand',$data);
					}
				}
				else{
					$this->load->view('search_category_shopbybrand',$data);
				}
			}
			else if($category_is_exists=="yes" && $subcategory_is_exists=="no"){
				
				if($pcat!=""){
					$get_parent_cat_info_obj=$this->get_parent_cat_info($pcat);
				}
				if($cat!=""){
					$get_cat_info_obj=$this->get_cat_info($cat);
				}
				
				if($pcat=="" || $cat==""){
					$this->load->view('error404',$data);
				}
				else if(preg_match("/[a-z]/i", $pcat) || preg_match("/[a-z]/i", $cat)){
					$this->load->view('error404',$data);
				}
				else if($pcat==0){
					if(empty($get_cat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category_shopbybrand',$data);
					}
				}
				else if($pcat!=0){
					if(empty($get_parent_cat_info_obj) || empty($get_cat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category_shopbybrand',$data);
					}
				}
				else{
					$this->load->view('search_category_shopbybrand',$data);
				}
			
			}
			else if($category_is_exists=="no" && $subcategory_is_exists=="no"){
				
				if($pcat!=""){
					$get_parent_cat_info_obj=$this->get_parent_cat_info($pcat);
				}
				if($pcat==""){
					$this->load->view('error404',$data);
				}
				else if(preg_match("/[a-z]/i", $pcat)){
					$this->load->view('error404',$data);
				}
				else if($pcat==0){
					$this->load->view('search_category_shopbybrand',$data);
				}
				else if($pcat!=0){
					if(empty($get_parent_cat_info_obj)){
						$this->load->view('error404',$data);
					}
					else{
						$this->load->view('search_category_shopbybrand',$data);
					}
				}
				else{
					$this->load->view('search_category_shopbybrand',$data);
				}
			
			}
			$this->load->view('footer');
	}
	
	
	public function catalog_shopbybrand($parentcategory_name='',$category_name="",$subcategory_name=""){
		////read_and_update_traffic();
		$pcat="";
		$cat="";
		$subcat="";
		
		/*** Here in $parentcategory_name place passed the shopbybrand brandname as we dont have any choice. if this string consists of string shopbybrand_ then  the control is coming from home page shopbybrand section with specific brandname otherwise it is coming from some other section starts ****/
		$shopbybrand_brandname="";
		if($parentcategory_name!=""){
			if(strpos($parentcategory_name, "shopbybrand_") !== false){
				$shopbybrand_brandname=explode("shopbybrand_",$parentcategory_name)[1];
				$parentcategory_name=''; // clearing the $parentcategory_name as the purpose of identifying brand is over.
			}
		}
		$data["shopbybrand_brandname"]=$shopbybrand_brandname;
		/*** Here in $parentcategory_name place passed the shopbybrand brandname as we dont have any choice. if this string consists of string shopbybrand_ then  the control is coming from home page shopbybrand section with specific brandname otherwise it is coming from some other section ends ****/
		
		
		$adm_settings=get_admin_combo_settings();
		
		$data['adm_settings']=$adm_settings;

		if($parentcategory_name!="null" && $parentcategory_name!=''){
			$parentcategory_name = str_replace('-', ' ', $parentcategory_name);
			$pcat=$this->Model_search->get_pcat_id_by_parentcategory_name($parentcategory_name);
			
                        if($pcat==''){
                            $rand_temp=$this->sample_code();
                            $pcat=$rand_temp."0";
                        }else{
                            $rand_temp=$this->sample_code();
                            $pcat=$rand_temp.$pcat;
                        }
		}
		else{
			$rand_temp=$this->sample_code();
			$pcat="";
			
		}
	
                //echo $pcat;
		$Ncat_id='';
		
		if($category_name!=""){
			$category_name = str_replace('-', ' ', $category_name);
			$cat=$this->Model_search->get_cat_id_by_category_name($category_name,$parentcategory_name);
            $Ncat_id=$cat;
			$rand_temp=$this->sample_code();
			$cat=$rand_temp.$cat;
		}
		if($subcategory_name!="" && $Ncat_id!=""){
			$subcategory_name = str_replace('-', ' ', $subcategory_name);
			$subcat=$this->Model_search->get_subcat_id_by_subcategory_name($subcategory_name,$Ncat_id);
			$rand_temp=$this->sample_code();
			$subcat=$rand_temp.$subcat;
		}
		
		
			//visitor_activity();
		$method=current_url();
		$this->session->set_userdata("products_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		if($subcat!=''){
			$subcate_id=substr($subcat, 9);
			$inv_id=$this->Model_search->get_count_of_invs($subcate_id);
			
			if(!empty($inv_id)){
				$inv_count=count($inv_id);
				if($inv_count==1){
					$rand=$this->sample_code();
					$obj=reset($inv_id);
					$invent_rand_id=$rand.$obj->id;
					redirect(site_url("detail/".$invent_rand_id));
				}
			}
		}

		$search_keywords=$this->input->post('search_keywords');
		if($search_keywords!=''){
			$search_keyword_index_page=strtolower($search_keywords);
			$flag=$this->Model_search->dump_search_keywords($search_keywords);
			//echo $flag;
		}
			$search_keyword="";$search_cat="";$page_rows="";$pagenum="";
            $sorting_by="";$cat_current="";$cat_current_id="";$type_of_filter="";
            $color_filter="";$color_prev="";$size_filter="";$size_prev="";
            $price_filter="";$price_prev="";$brand_filter="";$brand_prev="";
			//print_r($this->input->post());exit;
			if(!$this->input->post()){



				if($pcat!="" && $cat!="" && $subcat!=""){
				   $pcat=substr($pcat, 9);
					$cat=substr($cat, 9);
					$subcat=substr($subcat, 9);
				   $level_type="subcat_id";
				   $level_value=$pcat."_".$cat."_".$subcat;
				   $level_name=$this->get_subcat_name($pcat,$cat,$subcat);
				}
				if($pcat!="" && $cat!="" && $subcat==""){
				   $pcat=substr($pcat, 9);
					$cat=substr($cat, 9);
					
				   $level_type="cat_id";
				   $level_value=$pcat."_".$cat;
				   $level_name=$this->get_cat_name($pcat,$cat);
				}
				if($pcat!="" && $cat=="" && $subcat==""){
				   $pcat=substr($pcat, 9);
					
					
				   $level_type="pcat_id";
				   $level_value=$pcat;
				   $level_name=$this->get_pcat_name($pcat);
				}

				if($pcat=="" && $cat=="" && $subcat==""){
					$level_type="all";
					$level_value="";
					$level_name="";
				}

            }
            else{
			 extract($this->input->post());
            }
            
            $data['p_category']=$this->provide_first_level_menu();
        $data["controller"]=$this;
		
		
        $data["current_controller"]="search";
        $data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
        $data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
        
            if($level_type=="brand_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id=$level_value_arr[3];
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$brand_id;
            }
            if($level_type=="subcat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$subcat_id;
            }
			if($level_type=="cat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$cat_id;
            }
			if($level_type=="pcat_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id="";
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']=$pcat_id;
            }
            if($level_type=="product_id"){
                $level_value_arr=explode('_',$level_value);
                $pcat_id=$level_value_arr[0];
                $cat_id=$level_value_arr[1];
                $subcat_id=$level_value_arr[2];
                $brand_id=$level_value_arr[3];
                $product_id=$level_value_arr[4];
                $inventory_id="";
                $data['cat_current_id']=$product_id;
               
            }
			if($level_type=="all"){
                $level_value_arr=array();
                $pcat_id="";
                $cat_id="";
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']="";
            }
                $data["pcat_id"]=$pcat_id;
                $data['cat_id']=$cat_id;
                $data['subcat_id']=$subcat_id;
                $data['brand_id']=$brand_id;
                $data['product_id']=$product_id;
                $data['inventory_id']=$inventory_id;
                $data['searched_key']=$level_name;
                $data['level_type']=$level_type;
			$arr=array();
            $arr["pcat_id"]=$pcat_id;
            $arr['cat_id']=$cat_id;
            $arr['subcat_id']=$subcat_id;
            $arr['brand_id']=$brand_id;
            $arr['product_id']=$product_id;
             
            
            $data["category_tree"]=$arr;
            $data['level_value']=$level_value;
            $data['cat_current']=$level_type;
			$data["controller"]=$this;
            
            
			$data["current_controller"]="search";
			$data['search_keyword_index_page']="";
			if($this->input->post("search_keyword_index_page")!=""){
				$data['search_keyword_index_page']=$this->input->post("search_keyword_index_page");
			}
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			$this->load->view('header',$data);
			$this->load->view('search_category_shopbybrand',$data);
			$this->load->view('footer');	
	
	}
	
	
	/*** shopbybrand ends ***/
	
/* home design */	
public function get_total_count_skus_indexed_in_elasticsearch(){
	$result=$this->Model_search->get_total_count_skus_indexed_in_elasticsearch();
	return count($result);
}
/*** get all tagged_inventories for combo starts */
public function goto_tagged_inventory_combo(){
	
	$tagged_inventory_id_arr=[];
	$inventory_details_list_arr=[];
	$tagged_main_inventory_id_list_in=$this->input->post("tagged_main_inventory_id_list_in");
	
	if($tagged_main_inventory_id_list_in==""){
		header('Location:'.base_url()."catalog_combo");
		exit;
	}
	
	$result_arr=$this->Model_search->get_all_tagged_inventories_for_combo($tagged_main_inventory_id_list_in);
	if(!empty($result_arr)){
		foreach($result_arr as $arr){
			$tagged_inventory_id_arr=array_merge($tagged_inventory_id_arr,explode(",",$arr["tagged_inventory_ids"]));
		}
	}
	$data["tagged_inventory_id_arr"]=$tagged_inventory_id_arr;
	
	
	
		////read_and_update_traffic();
		$pcat="";
		$cat="";
		$subcat="";
	
		$adm_settings=get_admin_combo_settings();
		
		$data['adm_settings']=$adm_settings;

		$rand_temp=$this->sample_code();
		$pcat="";
	
                //echo $pcat;
		$Ncat_id='';
		
		
		
		
		
			//visitor_activity();
		$method=current_url();
		$this->session->set_userdata("products_page",$method);
        $data['page_name']='';
        $data['page_type']='';
        $data['page_id']='';
        $data['products_obj']='';
		

			$search_keyword="";$search_cat="";$page_rows="";$pagenum="";
            $sorting_by="";$cat_current="";$cat_current_id="";$type_of_filter="";
            $color_filter="";$color_prev="";$size_filter="";$size_prev="";
            $price_filter="";$price_prev="";$brand_filter="";$brand_prev="";
			//print_r($this->input->post());exit;
			//if(!$this->input->post()){


				

				if($pcat=="" && $cat=="" && $subcat==""){
					$level_type="all";
					$level_value="";
					$level_name="";
				}

            //}
            //else{
			 //extract($this->input->post());
            //}
            
            $data['p_category']=$this->provide_first_level_menu();
        $data["controller"]=$this;
		
		
        $data["current_controller"]="search";
        $data['get_all_categories'] = $this->Customer_frontend->get_all_categories();
        $data['get_all_parent_categories'] = $this->Customer_frontend->get_all_parent_categories();
        
          
			if($level_type=="all"){
                $level_value_arr=array();
                $pcat_id="";
                $cat_id="";
                $subcat_id="";
                $brand_id="";
                $product_id="";
                $inventory_id="";
                $data['cat_current_id']="";
            }
                $data["pcat_id"]=$pcat_id;
                $data['cat_id']=$cat_id;
                $data['subcat_id']=$subcat_id;
                $data['brand_id']=$brand_id;
                $data['product_id']=$product_id;
                $data['inventory_id']=$inventory_id;
                $data['searched_key']=$level_name;
                $data['level_type']=$level_type;
			$arr=array();
            $arr["pcat_id"]=$pcat_id;
            $arr['cat_id']=$cat_id;
            $arr['subcat_id']=$subcat_id;
            $arr['brand_id']=$brand_id;
            $arr['product_id']=$product_id;
             
            
            $data["category_tree"]=$arr;
            $data['level_value']=$level_value;
            $data['cat_current']=$level_type;
			$data["controller"]=$this;
            
            
			$data["current_controller"]="search";
			$data['search_keyword_index_page']="";
			
			$ft_img=$this->Customer_frontend->foot_image_index();
			$data['ft_img']='';
			if(!empty($ft_img)){
				if(isset($ft_img->background_image)){
					$data['ft_img']=$ft_img->background_image;
				}
			}
			$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
			
			$this->load->view('header',$data);		
			$this->load->view('tagged_inventory_combo');
			$this->load->view('footer');	
	
	
	
	
	
	
}

/*** get all tagged_inventories for combo ends */
public function request_a_quote_from_details($inv_sku_id,$inv_id,$qty){
	
	if($this->session->userdata("customer_id")){
	   $rq_customer_id=$this->session->userdata("customer_id");
	   $rq_customer_name=$this->session->userdata("customer_name");
				   
   }else{
	   $rq_customer_id='Guest';
	   $rq_customer_name='Guest';
   }
	   
	   $inv_id_arr[]=$inv_id;
	   $inv_sku_id_arr[]=$inv_sku_id;
	   $inv_qtys_arr[]=$qty;
   
		   
//print_r($inv_sku_id_arr);
//exit;
	   $date = new DateTime();
	   $mt=microtime();
	   $salt="employeespriam";
	   $rand=mt_rand(1, 1000000);
	   $ts=$date->getTimestamp();
	   $str=$salt.$rand.$ts.$mt;
	   $rq_unique_id=md5($str);
	   
	   
		   $rq_brand_id=mt_rand(0, 1);
			   
		   $ref_id=$this->request_reference_id($rq_customer_name,$rq_brand_id);
		   
   $flag=$this->Customer_account->request_a_quote_different_views(
												   $ref_id,
												   $rq_customer_id,
												   $inv_id_arr,
												   $inv_sku_id_arr,"from_details_page",$inv_qtys_arr);
   $this->request_to_quote($ref_id);

}
public function shipping_policy(){
	//visitor_activity();
	$method=current_url();
	$this->session->set_userdata("cur_page",$method);
	$data['page_name']='';
	$data['page_type']='';
	$data['page_id']='';
	$data['products_obj']='';
		$data["current_controller"]="search";
		$data['p_category']=$this->provide_first_level_menu();
		$data["controller"]=$this;
		$ft_img=$this->Customer_frontend->foot_image_index();
		$data['ft_img']='';
		if(!empty($ft_img)){
			if(isset($ft_img->background_image)){
				$data['ft_img']=$ft_img->background_image;
			}
		}
		$data["searched_keywords"]=$this->Customer_frontend->get_hot_searched_key_words();
		$this->load->view('header',$data);
		$this->load->view('shipping_policy');
		$this->load->view('footer');	
}
public function get_active_addons(){
	$inactive_pro=array();
	$data = json_decode(file_get_contents("php://input"));
	$add_on=$data->add_on;
	$products_order=$data->products_order;
	$add_main_ids=$data->add_main_ids;

	//print_r($add_on);

	//exit;

	$final_addon=[];$addon_total_price=0;$inventory=[];$change_is_there=0;
	foreach($add_on as $key1=>$val){

		$ind_addon=$val;
		

		//  echo '------';
		//  print_r($ind_addon);
		// echo '------';
		$main_id=$add_main_ids[$key1];
		//echo $main_id.'||';

		if(!empty($ind_addon) && $ind_addon!='[]' && $ind_addon!='"[]"'){

			$temp=array();
			if(is_array($ind_addon)){
				
			}else{
				
				$ind_addon=json_decode($ind_addon);
			}
			$ind_tot_price=0;

			//print_r($ind_addon);

			if(!empty($ind_addon)){

				foreach($ind_addon as $key2=>$ia){
					
					$inv_id=$ia->inv_id;
					

					$addon_total_price+=intval($ia->inv_price);
					$ind_tot_price+=$ia->inv_price;
					/* check if inv exists */
					$data=get_inv_info_active($inv_id,$main_id);

					if(!empty($data)){
						$inventory[]=$inv_id;
						$temp[]=$ind_addon[$key2];
					}else{
						unset($ind_addon[$key2]);
						$change_is_there++;
					}
					/* check if inv exists */
				}
			}

			$final_addon[$key1]=$temp;

		}else{
			$final_addon[$key1]=$val;//empty
		}

		//print_r($ind_addon);

		if(!empty($inventory)){
			$addon_inventories=implode(',',$inventory);
			$addon_product_status=1;
			$addon_products=json_encode($final_addon[$key1]);
			$addon_total_price=$ind_tot_price;
		}
	}
	
	//print_r($final_addon);

	print json_encode($final_addon);
}

  
public function get_baseurl(){
	$url_current_page=sprintf(
	"%s://%s/",
		isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
		$_SERVER['SERVER_NAME'],
		$_SERVER['REQUEST_URI']
	  );
	if(preg_match("/localhost/",$url_current_page)){
		return "http://localhost/voomet/";
	}
	else{
		return $url_current_page;
	}
}
public function capture_time_spent_on_page(){

	$timeSpent=$this->input->post('timeSpent');
	$previous_uri=$this->input->post('previous_uri');
	$request_uri=$this->input->post('request_uri');
	$page_url=$this->input->post('page_url');

	$previous_uri=$this->db->escape_str($previous_uri);
	$request_uri=$this->db->escape_str($request_uri);
	$page_url=$this->db->escape_str($page_url);
	// echo $timeSpent;
	// exit;
	read_and_update_traffic($page_url);//to find the view count
	visitor_activity_analytics($timeSpent,$request_uri,$previous_uri);// time spent

}

public function update_and_get_cartable_combo_info(){

	$valid_cart_items_count=0;
	$temp=0;
	$post_data = json_decode(file_get_contents("php://input"));

	//print_r($update_cart_info_post_data);
	$combo_shipping_price=$post_data->combo_shipping_price;
	$combo_grand_total=$post_data->combo_grand_total;
	$combo_products=json_encode($post_data->combo_products);

	$flag=$this->Customer_account->update_and_get_cartable_combo_info($combo_shipping_price,$combo_grand_total,$combo_products);
	return $flag;
	
}
public function get_inventory_id_by_sku_id($sku_id){
	$sku_id = str_replace('-', ' ', $sku_id);
	$inventory_id=$this->Model_search->get_inventory_id_by_sku_id($sku_id);
	if($inventory_id!=""){
		$rand_temp=$this->sample_code();
		$inventory_rid=$rand_temp.$inventory_id;
		echo json_encode(array("inventory_id"=>$inventory_rid,"response_from_fun"=>"yes"));
	}
	else{
		echo json_encode(array("response_from_fun"=>"none"));
	}
}
public function get_pcat_cat_subcat_id_by_sku_id_json(){
	$inventory_id_arr=[];
	$skuid_list=$this->input->post("skuid_list");
	$skuid_list_arr=explode("axdx",$skuid_list);
	$firstsku_id=$skuid_list_arr[0];
	$inventory_id_result_arr=$this->Model_search->get_inventory_id_list_by_sku_id_arr($skuid_list_arr);
	foreach($inventory_id_result_arr as $arr){
		$inventory_id_arr[]=$arr["inventory_id"];
	}
	$inventory_id_seperated=implode("axdx",$inventory_id_arr);
	$pcat_cat_subcat_id_by_sku_id_arr=$this->Model_search->get_pcat_cat_subcat_id_by_sku_id($firstsku_id);
	if(!empty($pcat_cat_subcat_id_by_sku_id_arr)){
		echo json_encode(array("inventory_id_seperated"=>$inventory_id_seperated,"pcat_id"=>$pcat_cat_subcat_id_by_sku_id_arr["pcat_id"],"cat_id"=>$pcat_cat_subcat_id_by_sku_id_arr["cat_id"],"subcat_id"=>$pcat_cat_subcat_id_by_sku_id_arr["subcat_id"],"response_from_fun"=>"yes"));
	}
	else{
		echo json_encode(array("response_from_fun"=>"none"));
	}
}


public function sendtestmail(){
	$mail = new PHPMailer;
	/*$mail->isSMTP();
	//$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
	//$mail->Port = '587';//  live - comment it 
	//$mail->Auth = true;//  live - comment it 
	//$mail->SMTPAuth = true;
	//$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
	//$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
	//$mail->SMTPSecure = 'tls';*/
	$mail->From = "info@voometstudio.com";
	$mail->FromName = "Axon Test";
	//$mail->addAddress($email);
	$mail->addAddress("rajitkumar1974@gmail.com");
	//$mail->AddCC("rajitkumar1974@gmail.com", "Rajit Kumar");               
	//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
	$mail->addReplyTo( "info@voometstudio.com", "Axon Test",0);
	$mail->WordWrap = 50;
	$mail->isHTML(true);
	$mail->Subject = 'New Test Query';
	$mail->Body    = "Test";
			
	if($mail->send()){
		echo "Success";
	}
	else{
		echo "Fail";
		echo $mail->ErrorInfo;
	}
}

}
?>