<?php 

error_reporting(1);
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Coupon extends CI_Controller {
    private $pagesize = 20;
    function __construct()
    {
       
        parent::__construct();
        $this->load->database();
        $this->load->model('admin/Model_coupon');
        $this->load->library('My_PHPMailer');
        $this->load->helper('emailsmstemplate');
        $this->load->helper('general_helper');
    }
    /* coupon */

public function index(){
    $this->manage_coupon();
}
public function manage_coupon(){
        //echo 'dsd';exit;
        if($this->session->userdata("logged_in")){
            $data["controller"]=$this;
            $data["menu_flag"]="coupon";	
            $this->load->view('admin/vmanage',$data);
            $this->load->view('admin/coupon/manage_coupon',$data);
        }else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
	
    public function manage_coupon_processing(){

        if($this->session->userdata("logged_in")){
                $params = $columns = $totalRecords = $data = array();
                $params = $this->input->post();
                $totalRecords=$this->Model_coupon->manage_coupon_processing($params,"get_total_num_recs");

                $queryRecordsResult=$this->Model_coupon->manage_coupon_processing($params,"get_recs");
                $snoCount=0;
                
                foreach($queryRecordsResult as $Obj){
                    $row=array();
                    $row[] = ++$snoCount;
                    $row[] = $Obj->coupon_code.'<br>';
                    $row[] = $Obj->coupon_name;
                    //$row[] = ($Obj->type_of_coupon=='1') ? "Product" : "Invoice";
                    $c_txt='';
                    if($Obj->type_of_coupon=='1'){
                        $c_txt.='<small><b>Product SKUs :</b></small>'.'<br>';
                        $sku_ids=$Obj->sku_ids;
                        $ids=explode('|',$sku_ids);
                        
                        if(!empty($ids) && $sku_ids!=''){
                            for($k=0;$k<count($ids);$k++){
                                $rand_1=sample_code();
                                $str=explode('-',$ids[$k]);
                                //print_r($str);
                                if(!empty($str)){
                                    $c_txt.='<li>
                                <a target="_blank" href="'.base_url().'detail/'.$rand_1.$str[1].'">
                                    '.$str[0].' </a>
                                </li>';
                                 }
                            }
                        }else{
                            $c_txt.='';
                        }
                    }
                    if($Obj->type_of_coupon=='2'){
                        //$c_txt.='<small><b>Invoice</b></small>'.'<br>';
                        $c_txt.='Total cart Value :'.curr_sym.$Obj->tot_cart_val;
                    }

                    $c_txt.='<br>';
                    $c_txt.='Coupon per user : '.$Obj->coupon_per_user;

                    $status_text='';$status_title='';$btn='';
                    if($Obj->status=='1'){
                        $status_text='Active';
                        $status_title='Change to Inactive';
                        $btn='success';
                        
                    }else{
                        $status_text='Inactive';
                        $status_title='Change to Active';
                        $btn='';
                    }
                        
                    $row[] = $c_txt;
                    
                    $text=($Obj->type=='1') ? "Percentage:".$Obj->value.' %' : "Flat:".curr_sym.$Obj->value;
                    
                    $row[] = $text;


                    $startdate = $Obj->end_date;
                    $expire = strtotime($startdate);
                    $today = date('Y-m-d H:i:s');
                    $today_time=strtotime($today);
                    $ex_status='';
                    if($today_time >= $expire){
                        $ex_status='<span style="color:red;">Expired</span>';
                    } else {
                        //$ex_status="Active";
                    }

                    $row[] = "<small><small>FROM </small></small>" . date('d/m/Y h:i A',strtotime($Obj->start_date)) . " <small><small>TO </small></small>" .date('d/m/Y h:i A',strtotime($Obj->end_date)).'<br>'.$ex_status;



                    $row[] = '<a href="'.base_url().'admin/Coupon/edit_coupon/'.$Obj->coupon_id.'"> Edit </a>'.'<br>'.'<button class="btn btn-sm btn-'.$btn.'" onclick="change_status('.$Obj->status.','.$Obj->coupon_id.')" title="'.$status_title.'">'.$status_text.'</button>';
                    $data[]=$row;
                }

                $json_data = array(
                    "draw"            => intval( $params['draw'] ),   
                    "recordsTotal"    => intval( $totalRecords ),  
                    "recordsFiltered" => intval($totalRecords),
                    "data"            => $data   // total data array
                    );
                echo json_encode($json_data);  // send data as json format
        }else{
                $this->load->view('admin/header');
                $this->load->view('admin/login');
        }
    }
    public function add_coupon(){
        if($this->session->userdata("logged_in")){
            $coupon_id=$this->input->get("coupon_id");
            $data["controller"]=$this;
            $data["menu_flag"]="coupon";	
            $data["coupon_data"]=$this->Model_coupon->get_all_coupon_data($coupon_id);
            $data["coupon_id"]=$coupon_id;
            $data['parent_category'] = $this->Model_coupon->parent_category();
            $this->load->view('admin/vmanage',$data);
            $this->load->view('admin/coupon/add_coupon',$data);
        }else{
                $this->load->view('admin/header');
                $this->load->view('admin/login');
        }
    }

/* coupon */
    
        /* for sku section */
   public function show_available_categories(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("pcat_id");
			$cat_id=$this->input->post("cat_id");
			$active=$this->input->post("active");
			if($this->input->post("with_status")){
				$with_status=$this->input->post("with_status");
			}else{
				$with_status=0;
			}
			$result_cat=$this->Model_coupon->show_available_categories($pcat_id,$active);
                        $selected='';
			if(count($result_cat)!=0){
				$str='';
				$str.='<option value="">-Select Category</option>';	
				if($with_status==1){
					foreach($result_cat as $res){
						$active_status=($res->active==1)?" - active":" - Inactive";
						$str.='<option value="'.$res->cat_id.'-'.$res->active.'">'.$res->cat_name.$active_status.'</option>';
					}
				}else{
					foreach($result_cat as $res){
                                            if($res->cat_id==$cat_id){
                                                $selected='selected';
                                            }else{
                                                $selected='';
                                            }
					$str.='<option value="'.$res->cat_id.'" '.$selected.'>'.$res->cat_name.'</option>';
					}
				}
				echo $str;
			}else{
				echo count($result_cat);
			}
		}else{
                        redirect(base_url());
                }
	}
   public function show_available_subcategories(){
		if($this->session->userdata("logged_in")){
			$cat_id=$this->input->post("cat_id");
			$subcat_id=$this->input->post("subcat_id");
			$active=$this->input->post("active");
			if($this->input->post("with_status")){
				$with_status=$this->input->post("with_status");
			}else{
				$with_status=0;
			}
			$result_subcat=$this->Model_coupon->show_available_subcategories($cat_id,$active);
                        $selected='';
			if(count($result_subcat)!=0){
				$str='';
				$str.='<option value="">-Select Subcategory-</option>';
				if($with_status==1){
					foreach($result_subcat as $res){
						$active_status=($res->active==1)?" - active":" - Inactive";
						$str.='<option value="'.$res->subcat_id.'-'.$res->active.'">'.$res->subcat_name.$active_status.'</option>';
					}
				}else{
					foreach($result_subcat as $res){
                                            if($res->subcat_id==$subcat_id){
                                                $selected='selected';
                                            }else{
                                                $selected='';
                                            }
                                            $str.='<option value="'.$res->subcat_id.'" '.$selected.'>'.$res->subcat_name.'</option>';
					}
				}
				echo $str;
			}else{
				echo count($result_subcat);
			}
		}else{
                        redirect(base_url());
                }
	}
   public function show_available_brands(){
		if($this->session->userdata("logged_in")){	
			$subcat_id=$this->input->post("subcat_id");
			$brand_id=$this->input->post("brand_id");
			$active=$this->input->post("active");
			if($this->input->post("with_status")){
				$with_status=$this->input->post("with_status");
			}else{
				$with_status=0;
			}
			$result_brand=$this->Model_coupon->show_available_brands($subcat_id,$active);
                        $selected='';
			if(count($result_brand)!=0){
				$str='';
				$str.='<option value="">-Select Brand-</option>';
				if($with_status==1){
					foreach($result_brand as $res){
						$active_status=($res->active==1)?" - active":" - Inactive";
						$str.='<option value="'.$res->brand_id.'-'.$res->active.'">'.$res->brand_name.$active_status.'</option>';
					}
				}else{
					foreach($result_brand as $res){
                                            if($res->brand_id==$brand_id){
                                                $selected='selected';
                                            }else{
                                                $selected='';
                                            }
                                            $str.='<option value="'.$res->brand_id.'" '.$selected.'>'.$res->brand_name.'</option>';
					}
				}
				echo $str;
			}else{
				echo count($result_brand);
			}
                }else{
                        redirect(base_url());
                }
	}
        public function show_available_products(){
		if($this->session->userdata("logged_in")){	
			$brand_id=$this->input->post("brand_id");
			$product_id=$this->input->post("product_id");
			$active=$this->input->post("active");
			if($this->input->post("with_status")){
				$with_status=$this->input->post("with_status");
			}else{
				$with_status=0;
			}
			$result_brand=$this->Model_coupon->show_available_products($brand_id,$active);
			//print_r($result_brand);
			if(count($result_brand)!=0){
				$str='';
				$str.='<option value=""></option>';
				if($with_status==1){
					foreach($result_brand as $res){
						$active_status=($res->active==1)?" - active":" - Inactive";
						$str.='<option value="'.$res->product_id.'-'.$res->active.'">'.$res->product_name.$active_status.'</option>';
					}
				}else{
                                    
					foreach($result_brand as $res){
                                            if($res->product_id==$product_id){
                                                $selected='selected';
                                            }else{
                                                $selected='';
                                            }
						$str.='<option value="'.$res->product_id.'" '.$selected.'>'.$res->product_name.'</option>';
					}
				}
				echo $str;
			}else{
				echo count($result_brand);
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
   public function show_available_skus(){
		if($this->session->userdata("logged_in")){	
			$product_id=$this->input->post("product_id");
			$sku_ids=$this->input->post("sku_ids");
			$active=$this->input->post("active");
			if($this->input->post("with_status")){
				$with_status=$this->input->post("with_status");
			}else{
				$with_status=0;
			}
			$result_sku=$this->Model_coupon->show_available_skus($product_id,$active);
			if(count($result_sku)!=0){
				$str='';
				//$str.='<option value=""></option>';
				if($with_status==1){
					foreach($result_sku as $res){
						$active_status=($res->active==1)?" - active":" - Inactive";
						$str.='<option value="'.$res->sku_id.'-'.$res->active.'">'.$res->brand_name.$active_status.'</option>';
					}
				}else{
                                        $arr=explode('|',$sku_ids);
                                        //print_r($arr);
					foreach($result_sku as $res){
                                                $val=$res->sku_id.'-'.$res->id;
                                                if(in_array($val,$arr)){
                                                    $selected='selected';
                                                }else{
                                                    $selected='';
                                                    $str.='<option value="'.$res->sku_id.'-'.$res->id.'" '.$selected.'>'.$res->sku_id.'</option>';
                                                }
						//$str.='<option value="'.$res->sku_id.'-'.$res->id.'" '.$selected.'>'.$res->sku_id.'</option>';
					}
				}
				echo $str;
			}else{
				echo count($result_sku);
			}
                }else{
                        redirect(base_url());
                }
	}
   
   /* for sku section */
        
    public function check_coupon_exists() {
        if($this->session->userdata("logged_in")){
            $coupon_code = $this->input->post('coupon_code');
            $coupon_name = $this->input->post('coupon_name');
            $coupon_id = $this->input->post('coupon_id');
            $error_msg='';
            $flag=$this->Model_coupon->check_coupon_exists($coupon_code,$coupon_name,$coupon_id);
            echo json_encode(array("flag"=>$flag,'message'=>$error_msg));
        }else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function add_coupon_submit(){
        if($this->session->userdata("logged_in")){
            $params=$this->input->post();
            $coupon_id=$this->input->post('coupon_id');
            
            $flag=$this->Model_coupon->add_or_update_coupon($coupon_id,$params);
            if($coupon_id!=''){
                $msg='Coupon updated successfully';
            }else{
                $msg='Coupon added successfully';
            }
            $this->session->set_flashdata('notification',$msg);
            redirect('admin/Coupon/manage_coupon')->withInput();
            
        }else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function edit_coupon($id){
        if($this->session->userdata("logged_in")){
            //$coupon_id=$this->input->get("coupon_id");
            $data["controller"]=$this;
            $data["menu_flag"]="coupon";	
            $data["coupon_data"]=$this->Model_coupon->get_all_coupon_data($id);
            //print_r($data["coupon_data"]);
            $data["coupon_id"]=$id;
            $data['parent_category'] = $this->Model_coupon->parent_category();
            $this->load->view('admin/vmanage',$data);
            $this->load->view('admin/coupon/add_coupon',$data);
        }else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function change_status() {
        if($this->session->userdata("logged_in")){
            
            $status = $this->input->post('status');
            $coupon_id = $this->input->post('coupon_id');
            $error_msg='';
            $flag=$this->Model_coupon->change_status($coupon_id,$status);
            if($status=='1'){
                $error_msg='Coupon deactivated successfully';
            }
            if($status=='0'){
                $error_msg='Coupon activated successfully';
            }
            
            echo json_encode(array("flag"=>1,'message'=>$error_msg));
        }else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    
}
	 
	
