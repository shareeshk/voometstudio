<?php 
error_reporting(0);
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Orders extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('My_fpdf');
		$this->load->model('admin/Model_orders');
		$this->load->model('Customer_account');
		$this->load->library('My_PHPMailer');	
		$this->load->helper('emailsmstemplate');
        $this->load->helper('general');
		$this->load->helper('s3_fileupload');
	}
	public function index(){
		if($this->session->userdata("logged_in")){
			$this->onhold_orders_processing();
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_inventory_info_by_inventory_id($inventory_id){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$inventory_obj=$this->Model_orders->get_inventory_info_by_inventory_id($inventory_id);
			return $inventory_obj;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_product_info_by_product_id($product_id){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$product_obj=$this->Model_orders->get_product_info_by_product_id($product_id);
			return $product_obj;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_subcat_info_by_product_id($product_id){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$subcat_obj=$this->Model_orders->get_subcat_info_by_product_id($product_id);
			return $subcat_obj;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function onhold_orders(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["cancel_reason"]=$this->Model_orders->get_admin_order_cancel_reasons();
			$this->Model_orders->notify_activeorders_unset();
			$data["menu_flag"]="active_order_active_links";	
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/onhold_orders',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function onhold_orders_processing(){
		
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_orders->onhold_orders_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_orders->onhold_orders_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$cancelflag=true;
				
				if($queryRecordsObj->type_of_order=="replaced"){
					$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->prev_order_item_id);
					
					if($order_replacement_decision_data_obj->customer_status=="cancel"){
						$cancelflag=false;
					}
				}
				
				if($cancelflag){
					
				$row[]="<input type='checkbox' name='onhold_orders_list' value='".$queryRecordsObj->orders_status_id."'>";
				
				$order_summary='<ul class="list-unstyled small">';

				//if($queryRecordsObj->ord_addon_products_status=='1'){
				if(0){
					$combo_prod=$this->get_addon_data($queryRecordsObj);
					$order_summary.='<li>'.$combo_prod.'</li>';
					$row[]="Combo products";
					
					
					$order_summary.="<li>SKU: ".$queryRecordsObj->sku_id."</li>";
                                if($queryRecordsObj->ord_sku_name!=''){
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
                                }else{
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
                                }
								
								
				
				}else{

					$inventory_id=$queryRecordsObj->inventory_id;
					$image=$queryRecordsObj->image;
					$img_path=get_inventory_image_path($inventory_id,$image);

					$row[]="<img src='".base_url().$img_path."'  style='width: 100px;height: 122px;' >";
				
					$order_summary.="<li>SKU: ".$queryRecordsObj->sku_id."</li>";
					if($queryRecordsObj->ord_sku_name!=''){
						$order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
					}else{
						$order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
					}

					$product_details=get_product_details($queryRecordsObj->product_id);
					if(!empty($product_details))
					$order_summary.=($product_details->product_description!='') ? "<li class='mt-10'><small>".$product_details->product_description.'</small></li>': '';
								
								
				}
				//$order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";

				$order_summary.="<li>Order Date: ".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."</li>";
				$order_summary.="<li>Order ID: ".$queryRecordsObj->order_id.' <i  style="cursor:pointer" class="fa fa-question-circle orderid_details_popover" data-trigger="focus" id="orderid_'.$queryRecordsObj->order_id."_".$queryRecordsObj->order_item_id.'" aria-hidden="true"></i>'."</li>";
				$order_summary.="<li>Order Item ID: ".$queryRecordsObj->order_item_id."</li>";
				if($queryRecordsObj->type_of_order==""){
					$order_summary.="<li>Payment Type: ".$queryRecordsObj->payment_type."</li>";
					if($queryRecordsObj->payment_type=='Razorpay'){
						$arr=razorpay_ids($queryRecordsObj->order_id);
						if(!empty($arr)){
							$order_summary.="<li>Razorpay Order ID: ".$arr['razorpayOrderId']."</li>";
							$order_summary.="<li>Razorpay Payment ID: ".$arr['razorpayPaymentId']."</li>";
						}
					}
				}
				////////////////added -replacement payment///////////////////////
				if($queryRecordsObj->type_of_order=="replaced"){

					if($order_replacement_decision_data_obj->paid_by=="customer"){
						if($order_replacement_decision_data_obj->balance_amount_paid_by=="cod"){
							$order_summary.="<li>Payment Type: COD <span class='text-info bold'>(Amount due)</span></li>";
						}else{
							$order_summary.="<li>Payment Type: ".$order_replacement_decision_data_obj->balance_amount_paid_by."</li>";
							
							if($order_replacement_decision_data_obj->payment_status=="paid"){
								
								$str= " <span class='text-info bold'>(".$order_replacement_decision_data_obj->paid_by."  paid through ".$order_replacement_decision_data_obj->balance_amount_paid_by.") </span>";
							}else{
								$str=($order_replacement_decision_data_obj->balance_amount_paid_by=="")? " Not Selected" : "<span class='text-info bold'>(".$order_replacement_decision_data_obj->paid_by." has to pay through ".$order_replacement_decision_data_obj->balance_amount_paid_by.") </span>";
							}			
							$order_summary.='<li>'.$str.'</li>';
						}
					}
				
					if($order_replacement_decision_data_obj->paid_by=="admin"){
						$order_summary.="<li>Payment Type: Null </li>";
						//$return_summary.=" (".$order_replacement_decision_data_obj->paid_by." has to pay through ".$order_replacement_decision_data_obj->refund_method.")<br>";
					}
				
				}
			////////////////added -replacement payment///////////////////////
				
				if($queryRecordsObj->type_of_order=="replaced"){
					$order_summary.="<li>[<span class='text-info bold'>Replaced Order</span>]</li>";
				}
				
				if($queryRecordsObj->prev_order_item_id!=""){
					
					$orders_status_data_obj=$this->Model_orders->get_orders_status_data($queryRecordsObj->prev_order_item_id);
					
					$replacement_desired_data_obj=$this->Model_orders->get_replacement_desired_data($queryRecordsObj->prev_order_item_id);
					
					//$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->prev_order_item_id);
					
					if($order_replacement_decision_data_obj->pickup_identifier=="after replace"){
						$order_summary.="<li>[<span class='text-info bold'>Pickup has been completed</span>]</li>";
					}
					if($order_replacement_decision_data_obj->pickup_identifier=="before replace"){
						$replacement_option_status='';
						if($orders_status_data_obj->initiate_replacement_pickup =='1' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='[Return the Item]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='1' && $orders_status_data_obj->order_replacement_pickup==1){
							$replacement_option_status='[Item Had been Collected Successfully]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='0' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='[Initiate Pickup has been rejected by admin]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='';
						}
						$order_summary.="<li>".'<span class="text-info bold">'.$replacement_option_status.'</span>'."</li>";
					}
				}
				
				/////////////////added - promotion////////////////////
				$str_promo='';	
				if($queryRecordsObj->type_of_order=="replaced"){

					$prev_order_item_id=$queryRecordsObj->prev_order_item_id;
					$purchased_order_data_obj=$this->Model_orders->get_shipping_charge_and_grand_total_from_history_orders($prev_order_item_id);
					//print_r($orders_status_data_obj);
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					
					$str_promo=$this->get_details_of_promotion_for_replaced_order($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal,$prev_order_item_id);
					
				}else{
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal);
					
				}
			   
				$order_summary.='<li>'.$str_promo.'</li>';
				$order_summary.='</ul>';
				
			
				/////////////////added - promotion////////////////////
				
			
				
				$addon_str='';

				if($queryRecordsObj->ord_addon_master_order_item_id!=''){

					$sku_id=get_main_sku($queryRecordsObj->ord_addon_master_order_item_id);
					$addon_str.='<b class="text-primary"><small>AddOn SKU (Main SKU Id:'.$sku_id.')</small></b>';
				}else{
					$addon_str.='<b class="text-primary"><small>Main SKU</small></b>';
				}

				/* get product vendor details */
				if($queryRecordsObj->product_vendor_id!=''){
					$p_v_data=get_product_vendor_details($queryRecordsObj->product_vendor_id);
					if(!empty($p_v_data)){
						$addon_str.='<br><br><div><small><u>Product Vendor</u></small></div>';
						$addon_str.='<div><b class="text-danger"><small>'.$p_v_data->name.'</small></b></div>';
						$addon_str.='<div class="text-danger"><small>'.$p_v_data->email.'</small></div>';
					}
				}
				/* get product vendor details */
				
				$row[]=$order_summary.$addon_str;

				$logi_details=$this->logistics_details_of_order($queryRecordsObj);
				$row[]=$logi_details;
				
				$order_quantity_price='<ul class="list-unstyled small">';
				
				if($queryRecordsObj->type_of_order=="replaced"){
					//$replaced_item_price_individual=$this->Model_orders->get_replaced_item_price_individual($prev_order_item_id);
					$replaced_item_price_individual=$queryRecordsObj->product_price;
					$order_quantity_price.="<li>Qty: ".$queryRecordsObj->quantity."</li>";
					
					$order_quantity_price.="<li>Price of replacement order (each): ".curr_sym." ".$replaced_item_price_individual."</li>";
					
					$order_quantity_price.="<li>Shipping charge for replacement:  ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
					$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
					
				}else{
					$order_quantity_price.="<li>Qty: ".$queryRecordsObj->quantity."</li>";
					$order_quantity_price.="<li>Total Item Price: ".curr_sym." ".$queryRecordsObj->subtotal."</li>";
					
					if($queryRecordsObj->promotion_invoice_free_shipping>0){
						$order_quantity_price.="<li>Shipping charge (<span class='text-danger'>waived off</span>): ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
						$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->subtotal."</li>";
					}else{
						$order_quantity_price.="<li>Shipping charge: ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
						$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
					}

					$order_quantity_price.='<li><a class="text-info bold" href="#" onclick="view_order_summary_(\''.$queryRecordsObj->order_id.'\')" id="view_order_summary_'.$queryRecordsObj->order_item_id.'">View Invoice Data ';
					
					if($queryRecordsObj->invoice_offer_achieved==1){
						$invoice_ofr_count=$this->get_invoice_offers_str($queryRecordsObj);
						$order_quantity_price.='<span class="badge">'.$invoice_ofr_count.'</span>';
					}
					$order_quantity_price.='</a></li>';
				}
				//echo $queryRecordsObj->order_id;
				
				$order_quantity_price.='</ul>';
				
				$row[]=$order_quantity_price;
				$customer_add='<ul class="list-unstyled small">';
				$customer_add.="<li>".$queryRecordsObj->customer_name."</li>";
				$customer_add.='<li>'.$queryRecordsObj->address1."</li>";
				$customer_add.='<li>'.$queryRecordsObj->address2."</li>";
				$customer_add.='<li>'.$queryRecordsObj->city.",".$queryRecordsObj->state."</li>";
				$customer_add.='<li>'.$queryRecordsObj->country."</li>";
				$customer_add.='<li>'.$queryRecordsObj->pincode."</li>";
				$customer_add.='<li>'.$queryRecordsObj->mobile."</li>";
				$customer_add.='</ul>';
				$row[]=$customer_add;
				$data[]=$row;
				$row=array();
				
				}
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function set_as_approved(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$list_to_be_approved=$this->input->post("list_to_be_approved");
			$send_notification=$this->input->post("send_notification");
			
			$flag=$this->Model_orders->set_as_approved($list_to_be_approved,$send_notification);
			//$flag=true;
			
			if($flag==true){
				/////////////////////////////////////////////////////////////
				$result_order_item_id_list_obj_arr=$this->Model_orders->get_order_item_id_list_by_orders_status_id($list_to_be_approved);
				
				/*foreach($result_order_item_id_list_obj_arr as $result_order_item_id_list_obj){
					$this->Model_orders->set_order_cancellation_policy($result_order_item_id_list_obj->order_item_id,$result_order_item_id_list_obj->order_id,'placed');
				}*/
				
				
				$status_view="approved_status_view";
				$flag_update=$this->Model_orders->update_notification_to_send($result_order_item_id_list_obj_arr,$send_notification,$status_view);
				
				$send_sms=$this->input->post("sms");
				$send_email=intval($this->input->post("email"));

				if($send_sms==1 || $send_email==1){
					foreach($result_order_item_id_list_obj_arr as $result_order_item_id_list_obj){
						
						
						
						$order_summary_mail_data_arr=$this->Model_orders->get_order_summary_mail_data($result_order_item_id_list_obj->order_item_id);
						foreach($order_summary_mail_data_arr as $order_summary_mail_data){
							
						$email=$this->Model_orders->get_email_db($order_summary_mail_data['customer_id']);
						$mobile=$this->Model_orders->get_mobile_db($order_summary_mail_data['customer_id']);
						
						if($mobile != '' && $send_sms==1){
							$this->send_sms_in_order_process($order_summary_mail_data,$mobile,"onhold");
						}//sms
				
						echo $email.'|1|';

						if($email!='' && $send_email==1){
							echo $email.'|2|';
							$this->send_email_in_order_process($order_summary_mail_data,$email,"onhold");
						}//mail	
		
						}
					}
				}
				/////////////////////////////////////////////////////////////
			}

			echo $flag;
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
			
		
	}
	///////////////////////
	public function approved_orders(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["cancel_reason"]=$this->Model_orders->get_admin_order_cancel_reasons();
			$data["menu_flag"]="active_order_active_links";	
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/approved_orders',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function approved_orders_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_orders->approved_orders_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_orders->approved_orders_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$cancelflag=true;
				if($queryRecordsObj->type_of_order=="replaced"){
					$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->prev_order_item_id);
					
					if($order_replacement_decision_data_obj->customer_status=="cancel"){
						$cancelflag=false;
					}
				}
				
				if($cancelflag){
				$row[]="<input type='checkbox' name='approved_orders_list' value='".$queryRecordsObj->orders_status_id."'>";
				
				$order_summary='<ul class="list-unstyled small">';

				//if($queryRecordsObj->ord_addon_products_status=='1'){
				if(0){
					$combo_prod=$this->get_addon_data($queryRecordsObj);
					$order_summary.='<li>'.$combo_prod.'</li>';
					$row[]="Combo products";
					
					$order_summary.="<li>SKU: ".$queryRecordsObj->sku_id."</li>";
                                if($queryRecordsObj->ord_sku_name!=''){
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
                                }else{
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
                                }
								
				
				}else{

					$inventory_id=$queryRecordsObj->inventory_id;
					$image=$queryRecordsObj->image;
					$img_path=get_inventory_image_path($inventory_id,$image);

					$row[]="<img src='".base_url().$img_path."'  style='width: 100px;height: 122px;' >";

				$order_summary.="<li>SKU: ".$queryRecordsObj->sku_id."</li>";
                                if($queryRecordsObj->ord_sku_name!=''){
                                    $order_summary.="<li class='text-info' bold>".$queryRecordsObj->ord_sku_name."</li>";
                                }else{
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
                                }
								$product_details=get_product_details($queryRecordsObj->product_id);
								if(!empty($product_details))
								$order_summary.=($product_details->product_description!='') ? "<li class='mt-10'><small>".$product_details->product_description.'</small></li>': '';

								
				}
				$order_summary.="<li>Order Date: ".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."</li>";
				$order_summary.="<li>Order ID: ".$queryRecordsObj->order_id.' <i  style="cursor:pointer" class="fa fa-question-circle orderid_details_popover" data-trigger="focus" id="orderid_'.$queryRecordsObj->order_id."_".$queryRecordsObj->order_item_id.'" aria-hidden="true"></i>'."</li>";
				$order_summary.="<li>Order Item ID: ".$queryRecordsObj->order_item_id."</li>";
				if($queryRecordsObj->type_of_order==""){
					$order_summary.="<li>Payment Type: ".$queryRecordsObj->payment_type."</li>";
                    if($queryRecordsObj->payment_type=='Razorpay'){
                        $arr=razorpay_ids($queryRecordsObj->order_id);
                        if(!empty($arr)){
                            $order_summary.="<li>Razorpay Order ID: ".$arr['razorpayOrderId']."</li>";
                            $order_summary.="<li>Razorpay Payment ID: ".$arr['razorpayPaymentId']."</li>";
                        }
                    }
				}
				////////////////added -replacement payment///////////////////////
				if($queryRecordsObj->type_of_order=="replaced"){

					if($order_replacement_decision_data_obj->paid_by=="customer"){
						if($order_replacement_decision_data_obj->balance_amount_paid_by=="cod"){
							$order_summary.="<li>Payment Type: COD <span class='text-info bold'>(Amount due)</span></li>";
						}else{
							$order_summary.="<li>Payment Type: ".$order_replacement_decision_data_obj->balance_amount_paid_by."</li>";
							
							if($order_replacement_decision_data_obj->payment_status=="paid"){
								
								$str= "<span class='text-info bold'>(".$order_replacement_decision_data_obj->paid_by."  paid through ".$order_replacement_decision_data_obj->balance_amount_paid_by.")</span>";
							}else{
								$str=($order_replacement_decision_data_obj->balance_amount_paid_by=="")? " Not Selected" : "<span class='text-info bold'>(".$order_replacement_decision_data_obj->paid_by." has to pay through ".$order_replacement_decision_data_obj->balance_amount_paid_by.") </span>";
							}			
							$order_summary.='<li>'.$str.'</li>';
						}
					}
				
					if($order_replacement_decision_data_obj->paid_by=="admin"){
						$order_summary.="<li>Payment Type: Null </li>";
						//$return_summary.=" (".$order_replacement_decision_data_obj->paid_by." has to pay through ".$order_replacement_decision_data_obj->refund_method.")<br>";
					}
				
				}
			////////////////added -replacement payment///////////////////////
				
				if($queryRecordsObj->type_of_order=="replaced"){
					$order_summary.="<li>[<span class='text-info bold'>Replaced Order</span>]</li>";
				}
				
				if($queryRecordsObj->prev_order_item_id!=""){
					
					$orders_status_data_obj=$this->Model_orders->get_orders_status_data($queryRecordsObj->prev_order_item_id);
					
					$replacement_desired_data_obj=$this->Model_orders->get_replacement_desired_data($queryRecordsObj->prev_order_item_id);
					
					//$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->prev_order_item_id);
					
					if($order_replacement_decision_data_obj->pickup_identifier=="after replace"){
						$order_summary.="<li>[<span class='text-info bold'>Pickup has been completed</span>]</li>";
					}
					if($order_replacement_decision_data_obj->pickup_identifier=="before replace"){
						$replacement_option_status='';
						if($orders_status_data_obj->initiate_replacement_pickup =='1' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='[Return the Item]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='1' && $orders_status_data_obj->order_replacement_pickup==1){
							$replacement_option_status='[Item Had been Collected Successfully]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='0' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='[Initiate Pickup has been rejected by admin]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='';
						}
						$order_summary.="<li><span class='text-info bold'>".$replacement_option_status."</span></li>";
					}
				}
				
				/////////////////added - promotion////////////////////
				$str_promo='';
				if($queryRecordsObj->type_of_order=="replaced"){

					$prev_order_item_id=$queryRecordsObj->prev_order_item_id;
					$purchased_order_data_obj=$this->Model_orders->get_shipping_charge_and_grand_total_from_history_orders($prev_order_item_id);
					
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion_for_replaced_order($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal,$prev_order_item_id);
					
				}else{
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal);
				}	
				$order_summary.='<li>'.$str_promo.'</li>';
				$order_summary.='</ul>';
		   
				
				/////////////////added - promotion////////////////////
				$addon_str='';

				if($queryRecordsObj->ord_addon_master_order_item_id!=''){

					$sku_id=get_main_sku($queryRecordsObj->ord_addon_master_order_item_id);
					$addon_str.='<b class="text-primary"><small>AddOn SKU (Main SKU Id:'.$sku_id.')</small></b>';
				}else{
					$addon_str.='<b class="text-primary"><small>Main SKU</small></b>';
				}

				/* get product vendor details */
				if($queryRecordsObj->product_vendor_id!=''){
					$p_v_data=get_product_vendor_details($queryRecordsObj->product_vendor_id);
					if(!empty($p_v_data)){
						$addon_str.='<br><br><div><small><u>Product Vendor</u></small></div>';
						$addon_str.='<div><b class="text-danger"><small>'.$p_v_data->name.'</small></b></div>';
						$addon_str.='<div class="text-danger"><small>'.$p_v_data->email.'</small></div>';
					}
				}
				/* get product vendor details */
				
				$row[]=$order_summary.$addon_str;
				
				//$row[]=$order_summary;
				$logi_details=$this->logistics_details_of_order($queryRecordsObj);
				$row[]=$logi_details;
				$order_quantity_price='<ul class="list-unstyled small">';
				if($queryRecordsObj->type_of_order=="replaced"){
					//$replaced_item_price_individual=$this->Model_orders->get_replaced_item_price_individual($prev_order_item_id);
					$replaced_item_price_individual=$queryRecordsObj->product_price;
					$order_quantity_price.="<li>Qty: ".$queryRecordsObj->quantity."</li>";
					
					$order_quantity_price.="<li>Price of replacement order (each):  ".curr_sym." ".$replaced_item_price_individual."</li>";
					
					$order_quantity_price.="<li>Shipping charge for replacement:  ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
					$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
					
				}else{
					$order_quantity_price.="<li>Qty:".$queryRecordsObj->quantity."</li>";
					$order_quantity_price.="<li>Total Item Price: ".$queryRecordsObj->subtotal."</li>";
					if($queryRecordsObj->promotion_invoice_free_shipping>0){
						$order_quantity_price.="<li>Shipping charge (<span class='text-danger'>waived off</span>): ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
						$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->subtotal."</li>";
					}else{
						$order_quantity_price.="<li>Shipping charge: ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
						$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
					}
					
					$order_quantity_price.='<li><a class="text-info bold" href="#" onclick="view_order_summary_(\''.$queryRecordsObj->order_id.'\')" id="view_order_summary_'.$queryRecordsObj->order_item_id.'">View Invoice Data ';
					if($queryRecordsObj->invoice_offer_achieved==1){
						$invoice_ofr_count=$this->get_invoice_offers_str($queryRecordsObj);
						$order_quantity_price.='<span class="badge text-danger">'.$invoice_ofr_count.'</span>';
					}
					$order_quantity_price.='</a>'."</li>";
					
				}
				$order_quantity_price.='</ul>';
				
				$row[]=$order_quantity_price;
				$customer_add='<ul class="list-unstyled small">';
				$customer_add.="<li>".$queryRecordsObj->customer_name."</li>";
				$customer_add.="<li>".$queryRecordsObj->address1."</li>";
				$customer_add.="<li>".$queryRecordsObj->address2."</li>";
				$customer_add.="<li>".$queryRecordsObj->city.",".$queryRecordsObj->state."</li>";
				$customer_add.="<li>".$queryRecordsObj->country."</li>";
				$customer_add.="<li>".$queryRecordsObj->pincode."</li>";
				$customer_add.="<li>".$queryRecordsObj->mobile."</li>";
				$customer_add.='</ul>';
				$row[]=$customer_add;
				$data[]=$row;
				$row=array();
				}
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function set_as_confirmed(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$list_to_be_confirmed=$this->input->post("list_to_be_confirmed");
			$send_notification=$this->input->post("send_notification");
			
			$flag=$this->Model_orders->set_as_confirmed($list_to_be_confirmed,$send_notification);
			
			if($flag==true){
				
				/////////////////////////////////////////////////////////////
				$result_order_item_id_list_obj_arr=$this->Model_orders->get_order_item_id_list_by_orders_status_id($list_to_be_confirmed);
				
				/*foreach($result_order_item_id_list_obj_arr as $result_order_item_id_list_obj){
					$this->Model_orders->set_order_cancellation_policy($result_order_item_id_list_obj->order_item_id,$result_order_item_id_list_obj->order_id,'confirmed');
				}*/
				
				
				$status_view="confirmed_status_view";
				$flag_update=$this->Model_orders->update_notification_to_send($result_order_item_id_list_obj_arr,$send_notification,$status_view);
				
				$send_sms=$this->input->post("sms");
				$send_email=intval($this->input->post("email"));
				
				if($send_sms==1 || $send_email==1){
					foreach($result_order_item_id_list_obj_arr as $result_order_item_id_list_obj){
						
						
						$order_summary_mail_data_arr=$this->Model_orders->get_order_summary_mail_data($result_order_item_id_list_obj->order_item_id);
						foreach($order_summary_mail_data_arr as $order_summary_mail_data){
							
						$email=$this->Model_orders->get_email_db($order_summary_mail_data['customer_id']);
						$mobile=$this->Model_orders->get_mobile_db($order_summary_mail_data['customer_id']);
						
						if($mobile != '' && $send_sms==1){
							$this->send_sms_in_order_process($order_summary_mail_data,$mobile,"approved");
						}//sms
				
						echo $email;

						if($email!='' && $send_email==1){
							$this->send_email_in_order_process($order_summary_mail_data,$email,"approved");
							
						}//mail	
			
			
						}
					}
				}
				/////////////////////////////////////////////////////////////
			}
			echo $flag;
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	///////////////////////
	public function confirmed_orders(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["cancel_reason"]=$this->Model_orders->get_admin_order_cancel_reasons();
			$data["menu_flag"]="active_order_active_links";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/confirmed_orders',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function confirmed_orders_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_orders->confirmed_orders_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_orders->confirmed_orders_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$cancelflag=true;
				if($queryRecordsObj->type_of_order=="replaced"){
					$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->prev_order_item_id);
					if($order_replacement_decision_data_obj->customer_status=="cancel"){
						$cancelflag=false;
					}
				}
				
				if($cancelflag){
				$row[]="<input type='checkbox' name='confirmed_orders_list' value='".$queryRecordsObj->orders_status_id."'>";
				
				$order_summary='<ul class="list-unstyled small">';

				
				//if($queryRecordsObj->ord_addon_products_status=='1'){
				if(0){
					$combo_prod=$this->get_addon_data($queryRecordsObj);
					$order_summary.='<li>'.$combo_prod.'</li>';
					$row[]="Combo products";
					
					$order_summary.="<li>SKU: ".$queryRecordsObj->sku_id."</li>";
                                if($queryRecordsObj->ord_sku_name!=''){
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
                                }else{
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
                                }
				
				}else{
					$inventory_id=$queryRecordsObj->inventory_id;
					$image=$queryRecordsObj->image;
					$img_path=get_inventory_image_path($inventory_id,$image);

					$row[]="<img src='".base_url().$img_path."'  style='width: 100px;height: 122px;' >";

					$order_summary.="<li>SKU: ".$queryRecordsObj->sku_id."</li>";
                                if($queryRecordsObj->ord_sku_name!=''){
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
                                }else{
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
                                }
								$product_details=get_product_details($queryRecordsObj->product_id);
								if(!empty($product_details))
								$order_summary.=($product_details->product_description!='') ? "<li class='mt-10'><small>".$product_details->product_description.'</small></li>': '';
								
				}
				$order_summary.="<li>Order Date: ".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."</li>";
				$order_summary.="<li>Order ID: ".$queryRecordsObj->order_id.' <i  style="cursor:pointer" class="fa fa-question-circle orderid_details_popover" data-trigger="focus" id="orderid_'.$queryRecordsObj->order_id."_".$queryRecordsObj->order_item_id.'" aria-hidden="true"></i>'."</li>";
				$order_summary.="<li>Order Item ID: ".$queryRecordsObj->order_item_id."</li>";
				
				if($queryRecordsObj->type_of_order==""){
					$order_summary.="<li>Payment Type: ".$queryRecordsObj->payment_type."</li>";
                    if($queryRecordsObj->payment_type=='Razorpay'){
                        $arr=razorpay_ids($queryRecordsObj->order_id);
                        if(!empty($arr)){
                            $order_summary.="<li>Razorpay Order ID: ".$arr['razorpayOrderId']."</li>";
                            $order_summary.="<li>Razorpay Payment ID: ".$arr['razorpayPaymentId']."</li>";
                        }
                    }
				}
				////////////////added -replacement payment///////////////////////
				if($queryRecordsObj->type_of_order=="replaced"){

					if($order_replacement_decision_data_obj->paid_by=="customer"){
						if($order_replacement_decision_data_obj->balance_amount_paid_by=="cod"){
							$order_summary.="<li>Payment Type: COD <span class='text-info bold'>(Amount due)</span></li>";
						}else{
							$order_summary.="<li>Payment Type: ".$order_replacement_decision_data_obj->balance_amount_paid_by."</li>";
							
							if($order_replacement_decision_data_obj->payment_status=="paid"){
								
								$str="<span class='text-info bold'>(".$order_replacement_decision_data_obj->paid_by."  paid through ".$order_replacement_decision_data_obj->balance_amount_paid_by.") </span>";
							}else{
								$str=($order_replacement_decision_data_obj->balance_amount_paid_by=="")? " Not Selected" : "<span class='text-info bold'>(".$order_replacement_decision_data_obj->paid_by." has to pay through ".$order_replacement_decision_data_obj->balance_amount_paid_by.") </span>";
							}			
							$order_summary.='<li>'.$str.'</li>';
						}
					}
				
					if($order_replacement_decision_data_obj->paid_by=="admin"){
						$order_summary.="<li>Payment Type: Null </li>";
						//$return_summary.=" (".$order_replacement_decision_data_obj->paid_by." has to pay through ".$order_replacement_decision_data_obj->refund_method.")<br>";
					}
				
				}
			////////////////added -replacement payment///////////////////////
				
				if($queryRecordsObj->type_of_order=="replaced"){
					$order_summary.="[<span class='text-info bold'>Replaced Order</span>]";
				}
				
				if($queryRecordsObj->prev_order_item_id!=""){
					
					$orders_status_data_obj=$this->Model_orders->get_orders_status_data($queryRecordsObj->prev_order_item_id);
					
					$replacement_desired_data_obj=$this->Model_orders->get_replacement_desired_data($queryRecordsObj->prev_order_item_id);
					
					//$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->prev_order_item_id);
					
					if($order_replacement_decision_data_obj->pickup_identifier=="after replace"){
						$order_summary.="<li>[<span class='text-info bold'>Pickup has been completed</span>]</li>";
					}
					if($order_replacement_decision_data_obj->pickup_identifier=="before replace"){
						$replacement_option_status='';
						if($orders_status_data_obj->initiate_replacement_pickup =='1' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='[Return the Item]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='1' && $orders_status_data_obj->order_replacement_pickup==1){
							$replacement_option_status='[Item Had been Collected Successfully]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='0' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='[Initiate Pickup has been rejected by admin]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='';
						}
						$order_summary.="<li>".'<span class="text-info bold">'.$replacement_option_status.'</span>'."</li>";
					}
				}
				
				/////////////////added - promotion////////////////////
				$str_promo='';
				if($queryRecordsObj->type_of_order=="replaced"){

					$prev_order_item_id=$queryRecordsObj->prev_order_item_id;
					$purchased_order_data_obj=$this->Model_orders->get_shipping_charge_and_grand_total_from_history_orders($prev_order_item_id);
					
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion_for_replaced_order($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal,$prev_order_item_id);
					
				}else{
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal);
				}
				$order_summary.='<li>'.$str_promo.'</li>';
				$order_summary.='</ul>';
				  
				
				/////////////////added - promotion////////////////////
				
				
				$addon_str='';

				if($queryRecordsObj->ord_addon_master_order_item_id!=''){

					$sku_id=get_main_sku($queryRecordsObj->ord_addon_master_order_item_id);
					$addon_str.='<b class="text-primary"><small>AddOn SKU (Main SKU Id:'.$sku_id.')</small></b>';
				}else{
					$addon_str.='<b class="text-primary"><small>Main SKU</small></b>';
				}
				/* get product vendor details */
				if($queryRecordsObj->product_vendor_id!=''){
					$p_v_data=get_product_vendor_details($queryRecordsObj->product_vendor_id);
					if(!empty($p_v_data)){
						$addon_str.='<br><br><div><small><u>Product Vendor</u></small></div>';
						$addon_str.='<div><b class="text-danger"><small>'.$p_v_data->name.'</small></b></div>';
						$addon_str.='<div class="text-danger"><small>'.$p_v_data->email.'</small></div>';
					}
				}
				/* get product vendor details */
				$row[]=$order_summary.$addon_str;
				
				$logi_details=$this->logistics_details_of_order($queryRecordsObj);
				$row[]=$logi_details;
				$order_quantity_price='<ul class="list-unstyled small">';
				if($queryRecordsObj->type_of_order=="replaced"){
					//$replaced_item_price_individual=$this->Model_orders->get_replaced_item_price_individual($prev_order_item_id);
					$replaced_item_price_individual=$queryRecordsObj->product_price;
					$order_quantity_price.="<li>Qty: ".$queryRecordsObj->quantity."</li>";
					
					$order_quantity_price.="<li>Price of replacement order (each):  ".curr_sym." ".$replaced_item_price_individual."</li>";
					
					$order_quantity_price.="<li>Shipping charge for replacement:  ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
					$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
				}else{
					$order_quantity_price.="<li>Qty:".$queryRecordsObj->quantity."</li>";
					$order_quantity_price.="<li>Total Item Price:".$queryRecordsObj->subtotal."</li>";
					if($queryRecordsObj->promotion_invoice_free_shipping>0){
						$order_quantity_price.="<li>Shipping charge (<span class='text-danger'>waived off</span>): ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
						$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->subtotal."</li>";
					}else{
						$order_quantity_price.="<li>Shipping charge: ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
						$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
					}

					$order_quantity_price.='<li><a class="text-info bold" href="#" onclick="view_order_summary_(\''.$queryRecordsObj->order_id.'\')" id="view_order_summary_'.$queryRecordsObj->order_item_id.'">View Invoice Data ';
					if($queryRecordsObj->invoice_offer_achieved==1){
						$invoice_ofr_count=$this->get_invoice_offers_str($queryRecordsObj);
						$order_quantity_price.='<span class="badge text-danger">'.$invoice_ofr_count.'</span>';
					}
					$order_quantity_price.='</a></li>';
				
				}
				$order_quantity_price.='</ul>';
				$row[]=$order_quantity_price;
				$customer_add='<ul class="list-unstyled small">';
				$customer_add.="<li>".$queryRecordsObj->customer_name."</li>";
				$customer_add.="<li>".$queryRecordsObj->address1."</li>";
				$customer_add.="<li>".$queryRecordsObj->address2."</li>";
				$customer_add.="<li>".$queryRecordsObj->city.",".$queryRecordsObj->state."</li>";
				$customer_add.="<li>".$queryRecordsObj->country."</li>";
				$customer_add.="<li>".$queryRecordsObj->pincode."</li>";
				$customer_add.="<li>".$queryRecordsObj->mobile."</li>";
				$customer_add.='</ul>';
				$row[]=$customer_add;
				
				// <button =\"javascript:void(0);\" onclick=\"generateInvoiceFun('transit','$queryRecordsObj->order_item_id','$queryRecordsObj->order_id')\" class='btn btn-primary btn-xs btn-block transit_invoice_sent_$queryRecordsObj->order_item_id'>Transit Invoice</button> 
				
				$row[]="<button =\"javascript:void(0);\" onclick=\"generateInvoiceFun('customer','$queryRecordsObj->order_item_id','$queryRecordsObj->order_id')\" class='btn btn-primary btn-xs btn-block customer_invoice_sent_$queryRecordsObj->order_item_id'>Customer Invoice</button> <button =\"javascript:void(0);\" onclick=\"generateLabelsFun('$queryRecordsObj->order_item_id','$queryRecordsObj->order_id')\" class='btn btn-primary btn-xs btn-block'>Labels</button> <button =\"javascript:void(0);\" onclick=\"generatePackinglistFun('$queryRecordsObj->order_item_id','$queryRecordsObj->order_id')\" class='btn btn-primary btn-xs btn-block'>Packing List</button> 
				
				
				<button =\"javascript:void(0);\" onclick=\"generateInvoiceLabelsPackinglistFun('$queryRecordsObj->order_item_id','$queryRecordsObj->order_id')\" class='btn btn-info btn-xs btn-block invoice_sent_$queryRecordsObj->order_item_id'>Email Docs</button>";
				
				$data[]=$row;
				$row=array();
				}
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function set_as_packed(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$list_to_be_packed=$this->input->post("list_to_be_packed");
			$send_notification=$this->input->post("send_notification");
			
			$flag=$this->Model_orders->set_as_packed($list_to_be_packed);
			if($flag==true){
				// generation of pdf of manifest starts///
				
				$tracking_id_generation=$this->Model_orders->get_tracking_id_generation();
				if($tracking_id_generation=="yes"){
					$get_info_of_products_packed_arr=$this->Model_orders->get_info_of_products_packed();
					if(count($get_info_of_products_packed_arr)>=0){
						
						foreach($get_info_of_products_packed_arr as $k => $info_of_products_packed_arr){
						
							$date = new DateTime();
							$mt=microtime();
							$salt="packing";
							$rand=mt_rand(1, 1000000);
							$ts=$date->getTimestamp();
							$str=$salt.$rand.$ts.$mt;
							$tracking_id=md5($str);
							$this->Model_orders->update_tracking_id_active_orders($info_of_products_packed_arr["order_item_id"],$tracking_id);
						}
					}
					
				}
				
				// generation of pdf of manifest ends///
				/////////////////////////////////////////////////////////////
				$result_order_item_id_list_obj_arr=$this->Model_orders->get_order_item_id_list_by_orders_status_id($list_to_be_packed);
				/*foreach($result_order_item_id_list_obj_arr as $result_order_item_id_list_obj){
					$this->Model_orders->set_order_cancellation_policy($result_order_item_id_list_obj->order_item_id,$result_order_item_id_list_obj->order_id,'packed');
				}*/
				$status_view="packed_status_view";
				$flag_update=$this->Model_orders->update_notification_to_send($result_order_item_id_list_obj_arr,$send_notification,$status_view);
				
				$send_sms=$this->input->post("sms");
				$send_email=$this->input->post("email");
				if($send_sms==1 || $send_email==1){
					foreach($result_order_item_id_list_obj_arr as $result_order_item_id_list_obj){
						
						
						$order_summary_mail_data_arr=$this->Model_orders->get_order_summary_mail_data($result_order_item_id_list_obj->order_item_id);
						foreach($order_summary_mail_data_arr as $order_summary_mail_data){
							
						$email=$this->Model_orders->get_email_db($order_summary_mail_data['customer_id']);

						$mobile=$this->Model_orders->get_mobile_db($order_summary_mail_data['customer_id']);
						
						if($mobile != '' && $send_sms==1){
							$this->send_sms_in_order_process($order_summary_mail_data,$mobile,"confirmed");
						}//sms
				
						if($email!='' && $send_email==1){
							$this->send_email_in_order_process($order_summary_mail_data,$email,"confirmed");
							
						}//mail	
			
			
						}
					}
				}
				/////////////////////////////////////////////////////////////
			}
			echo $flag;
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	//////////////////
	///////////////////////
	public function packed_orders(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["cancel_reason"]=$this->Model_orders->get_admin_order_cancel_reasons();
			$data["menu_flag"]="active_order_active_links";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/packed_orders',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function packed_orders_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_orders->packed_orders_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_orders->packed_orders_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$cancelflag=true;
				if($queryRecordsObj->type_of_order=="replaced"){
					$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->prev_order_item_id);
					
					if($order_replacement_decision_data_obj->customer_status=="cancel"){
						$cancelflag=false;
					}
					
				}
				
				if($cancelflag){
					
					$row[]="<input type='checkbox' name='packed_orders_list' value='".$queryRecordsObj->orders_status_id."'>";
					
					$order_summary='<ul class="list-unstyled small">';
					//if($queryRecordsObj->ord_addon_products_status=='1'){
				if(0){
						$combo_prod=$this->get_addon_data($queryRecordsObj);
						$order_summary.='<li>'.$combo_prod.'</li>';
						$row[]="Combo products";
							$order_summary.="<li>SKU: ".$queryRecordsObj->sku_id."</li>";
                                        if($queryRecordsObj->ord_sku_name!=''){
                                            $order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
                                        }else{
                                            $order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
                                        }
					}else{
						$inventory_id=$queryRecordsObj->inventory_id;
						$image=$queryRecordsObj->image;
						$img_path=get_inventory_image_path($inventory_id,$image);

						$row[]="<img src='".base_url().$img_path."'  style='width: 100px;height: 122px;' >";
						
						$order_summary.="<li>SKU: ".$queryRecordsObj->sku_id."</li>";
                                        if($queryRecordsObj->ord_sku_name!=''){
                                            $order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
                                        }else{
                                            $order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
                                        }
										$product_details=get_product_details($queryRecordsObj->product_id);
										if(!empty($product_details))
										$order_summary.=($product_details->product_description!='') ? "<li class='mt-10'><small>".$product_details->product_description.'</small></li>': '';
										
						}
					$order_summary.="<li>Order Date: ".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."</li>";
					$order_summary.="<li>Order ID: ".$queryRecordsObj->order_id.' <i  style="cursor:pointer" class="fa fa-question-circle orderid_details_popover" data-trigger="focus" id="orderid_'.$queryRecordsObj->order_id."_".$queryRecordsObj->order_item_id.'" aria-hidden="true"></i>'."</li>";
					$order_summary.="<li>Order Item ID: ".$queryRecordsObj->order_item_id."</li>";
					if($queryRecordsObj->type_of_order==""){
					$order_summary.="<li>Payment Type: ".$queryRecordsObj->payment_type."</li>";
                        if($queryRecordsObj->payment_type=='Razorpay'){
                            $arr=razorpay_ids($queryRecordsObj->order_id);
                            if(!empty($arr)){
                                $order_summary.="<li>Razorpay Order ID: ".$arr['razorpayOrderId']."</li>";
                                $order_summary.="<li>Razorpay Payment ID: ".$arr['razorpayPaymentId']."</li>";
                            }
                        }
				}
				////////////////added -replacement payment///////////////////////
				if($queryRecordsObj->type_of_order=="replaced"){

					if($order_replacement_decision_data_obj->paid_by=="customer"){
						if($order_replacement_decision_data_obj->balance_amount_paid_by=="cod"){
							$order_summary.="<li>Payment Type: COD <span class='text-info bold'>(Amount due)</span></li>";
						}else{
							$order_summary.="<li>Payment Type: ".$order_replacement_decision_data_obj->balance_amount_paid_by."</li>";
							
							if($order_replacement_decision_data_obj->payment_status=="paid"){
								
								$str= "<span class='text-info bold'>(".$order_replacement_decision_data_obj->paid_by."  paid through ".$order_replacement_decision_data_obj->balance_amount_paid_by.") </span>";
							}else{
								$str=($order_replacement_decision_data_obj->balance_amount_paid_by=="")? " Not Selected" : "<span class='text-info bold'>(".$order_replacement_decision_data_obj->paid_by." has to pay through ".$order_replacement_decision_data_obj->balance_amount_paid_by.") </span>";
							}			
							$order_summary.='<li>'.$str.'</li>';
						}
					}
				
					if($order_replacement_decision_data_obj->paid_by=="admin"){
						$order_summary.="<li>Payment Type: Null </li>";
						//$return_summary.=" (".$order_replacement_decision_data_obj->paid_by." has to pay through ".$order_replacement_decision_data_obj->refund_method.")<br>";
					}
				
				}
			////////////////added -replacement payment///////////////////////
					if($queryRecordsObj->type_of_order=="replaced"){
						$order_summary.="<li>[<span class='text-info bold'>Replaced Order</span>]</li>";
					}
					
				if($queryRecordsObj->prev_order_item_id!=""){
					
					$orders_status_data_obj=$this->Model_orders->get_orders_status_data($queryRecordsObj->prev_order_item_id);
					
					$replacement_desired_data_obj=$this->Model_orders->get_replacement_desired_data($queryRecordsObj->prev_order_item_id);

					
					if($order_replacement_decision_data_obj->pickup_identifier=="after replace"){
						$order_summary.="<li>[<span class='text-info bold'>Pickup has been completed</span>]</li>";
					}
					if($order_replacement_decision_data_obj->pickup_identifier=="before replace"){
						$replacement_option_status='';
						if($orders_status_data_obj->initiate_replacement_pickup =='1' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='[Return the Item]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='1' && $orders_status_data_obj->order_replacement_pickup==1){
							$replacement_option_status='[Item Had been Collected Successfully]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='0' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='[Initiate Pickup has been rejected by admin]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='';
						}
						$order_summary.="<li>".'<span class="text-info bold">'.$replacement_option_status.'</span>'."</li>";
					}
				}
				
				
				/////////////////added - promotion////////////////////
				$str_promo='';	
				if($queryRecordsObj->type_of_order=="replaced"){

					$prev_order_item_id=$queryRecordsObj->prev_order_item_id;
					$purchased_order_data_obj=$this->Model_orders->get_shipping_charge_and_grand_total_from_history_orders($prev_order_item_id);
					
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion_for_replaced_order($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal,$prev_order_item_id);
					
				}else{
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal);
				}
				$order_summary.='<li>'.$str_promo.'</li>';
				$order_summary.='</ul>';
				
		   
				
				/////////////////added - promotion////////////////////
				
				
				$addon_str='';

				if($queryRecordsObj->ord_addon_master_order_item_id!=''){

					$sku_id=get_main_sku($queryRecordsObj->ord_addon_master_order_item_id);
					$addon_str.='<b class="text-primary"><small>AddOn SKU (Main SKU Id:'.$sku_id.')</small></b>';
				}else{
					$addon_str.='<b class="text-primary"><small>Main SKU</small></b>';
				}
				/* get product vendor details */
				if($queryRecordsObj->product_vendor_id!=''){
					$p_v_data=get_product_vendor_details($queryRecordsObj->product_vendor_id);
					if(!empty($p_v_data)){
						$addon_str.='<br><br><div><small><u>Product Vendor</u></small></div>';
						$addon_str.='<div><b class="text-danger"><small>'.$p_v_data->name.'</small></b></div>';
						$addon_str.='<div class="text-danger"><small>'.$p_v_data->email.'</small></div>';
					}
				}
				/* get product vendor details */
				$row[]=$order_summary.$addon_str;
					$logi_details=$this->logistics_details_of_order($queryRecordsObj,'packed');
					$row[]=$logi_details;
					
					$order_quantity_price='<ul class="list-unstyled small">';
					
					if($queryRecordsObj->type_of_order=="replaced"){
						//$replaced_item_price_individual=$this->Model_orders->get_replaced_item_price_individual($prev_order_item_id);
						$replaced_item_price_individual=$queryRecordsObj->product_price;
						$order_quantity_price.="<li>Qty:".$queryRecordsObj->quantity."</li>";
						
						$order_quantity_price.="<li>Price of replacement order (each): ".curr_sym." ".$replaced_item_price_individual."</li>";
						
						$order_quantity_price.="<li>Shipping charge for replacement: ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
						$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
					}else{
						$order_quantity_price.="<li>Qty:".$queryRecordsObj->quantity."</li>";
						$order_quantity_price.="<li>Total Item Price:".$queryRecordsObj->subtotal."</li>";
						if($queryRecordsObj->promotion_invoice_free_shipping>0){
							$order_quantity_price.="<li>Shipping charge (<span class='text-danger'>waived off</span>): ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
							$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->subtotal."</li>";
						}else{
							$order_quantity_price.="<li>Shipping charge: ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
							$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
						}
						$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
						
						$order_quantity_price.='<li><a class="text-info bold" href="#" onclick="view_order_summary_(\''.$queryRecordsObj->order_id.'\')" id="view_order_summary_'.$queryRecordsObj->order_item_id.'">View Invoice Data ';
						if($queryRecordsObj->invoice_offer_achieved==1){
							$invoice_ofr_count=$this->get_invoice_offers_str($queryRecordsObj);
							$order_quantity_price.='<span class="badge text-danger"><i class="fa fa-gift" aria-hidden="true"></i> '.$invoice_ofr_count.'</span>';
						}
						$order_quantity_price.='</a></li>';
					}
					$order_quantity_price.='</ul>';
				
					$row[]=$order_quantity_price;
					$customer_add='<ul class="list-unstyled small">';
					$customer_add.="<li>".$queryRecordsObj->customer_name."</li>";
					$customer_add.="<li>".$queryRecordsObj->address1."</li>";
					$customer_add.="<li>".$queryRecordsObj->address2."</li>";
					$customer_add.="<li>".$queryRecordsObj->city.",".$queryRecordsObj->state."</li>";
					$customer_add.="<li>".$queryRecordsObj->country."</li>";
					$customer_add.="<li>".$queryRecordsObj->pincode."</li>";
					$customer_add.="<li>".$queryRecordsObj->mobile."</li>";
					$row[]=$customer_add;
					$data[]=$row;
					$row=array();
				}
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function set_as_shipped(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			//////// updating the tracking number starts ////////
			if($this->input->post("tracking_number")){
				$orders_status_id=$this->input->post("list_to_be_shipped");
				$tracking_number=$this->input->post("tracking_number");
				$this->Model_orders->update_tracking_number($orders_status_id,$tracking_number);
			}
			//////// updating the tracking number ends ////////
			$list_to_be_shipped=$this->input->post("list_to_be_shipped");
			$flag=$this->Model_orders->set_as_shipped($list_to_be_shipped);
			$send_notification=$this->input->post("send_notification");
			
			if($flag==true){
				
				/////////////////////////////////////////////////////////////
				$result_order_item_id_list_obj_arr=$this->Model_orders->get_order_item_id_list_by_orders_status_id($list_to_be_shipped);
				$status_view="shipped_status_view";
				$flag_update=$this->Model_orders->update_notification_to_send($result_order_item_id_list_obj_arr,$send_notification,$status_view);
				
				$send_sms=$this->input->post("sms");
				$send_email=$this->input->post("email");
				if($send_sms==1 || $send_email==1){
					foreach($result_order_item_id_list_obj_arr as $result_order_item_id_list_obj){
						
						$order_summary_mail_data_arr=$this->Model_orders->get_order_summary_mail_data($result_order_item_id_list_obj->order_item_id);
						foreach($order_summary_mail_data_arr as $order_summary_mail_data){
							
						$email=$this->Model_orders->get_email_db($order_summary_mail_data['customer_id']);
						$mobile=$this->Model_orders->get_mobile_db($order_summary_mail_data['customer_id']);
						
						if($mobile != '' && $send_sms==1){
							$this->send_sms_in_order_process($order_summary_mail_data,$mobile,"packed");
						}//sms
				
						if($email!='' && $send_email==1){
							$this->send_email_in_order_process($order_summary_mail_data,$email,"packed");
							
						}//mail	
			
			
						}
					}
				}
				/////////////////////////////////////////////////////////////
			}
			echo $flag;
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	//////////////////
	///////////////////////
	public function shipped_orders(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["cancel_reason"]=$this->Model_orders->get_admin_order_cancel_reasons();
			$data["menu_flag"]="active_order_active_links";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/shipped_orders',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function shipped_orders_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_orders->shipped_orders_processing($params,"get_total_num_recs");
			
			
			$queryRecordsResult=$this->Model_orders->shipped_orders_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				if($queryRecordsObj->type_of_order=="replaced"){
					$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->prev_order_item_id);
				}
				
				$row=array();
				$row[]="<input type='checkbox' name='shipped_orders_list' value='".$queryRecordsObj->orders_status_id."'>";
				
				$order_summary='<ul class="list-unstyled small">';

				//if($queryRecordsObj->ord_addon_products_status=='1'){
				if(0){
					$combo_prod=$this->get_addon_data($queryRecordsObj);
					$order_summary.='<li>'.$combo_prod.'</li>';
					$row[]="Combo products";
					
					$order_summary.="<li>SKU: ".$queryRecordsObj->sku_id."</li>";
                                if($queryRecordsObj->ord_sku_name!=''){
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
                                }else{
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
                                }
				
				}else{
					$inventory_id=$queryRecordsObj->inventory_id;
					$image=$queryRecordsObj->image;
					$img_path=get_inventory_image_path($inventory_id,$image);

					$row[]="<img src='".base_url().$img_path."'  style='width: 100px;height: 122px;' >";
					$order_summary.="<li>SKU: ".$queryRecordsObj->sku_id."</li>";
                                if($queryRecordsObj->ord_sku_name!=''){
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
                                }else{
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
                                }

								$product_details=get_product_details($queryRecordsObj->product_id);
								if(!empty($product_details))
								$order_summary.=($product_details->product_description!='') ? "<li class='mt-10'><small>".$product_details->product_description.'</small></li>': '';
								
				}
				$order_summary.="<li>Order Date:".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."</li>";
				$order_summary.="<li>Order ID:".$queryRecordsObj->order_id.' <i  style="cursor:pointer" class="fa fa-question-circle orderid_details_popover" data-trigger="focus" id="orderid_'.$queryRecordsObj->order_id."_".$queryRecordsObj->order_item_id.'" aria-hidden="true"></i>'."</li>";
				$order_summary.="<li>Order Item ID:".$queryRecordsObj->order_item_id."</li>";
				if($queryRecordsObj->type_of_order==""){
					$order_summary.="<li>Payment Type:".$queryRecordsObj->payment_type."</li>";
                    if($queryRecordsObj->payment_type=='Razorpay'){
                        $arr=razorpay_ids($queryRecordsObj->order_id);
                        if(!empty($arr)){
                            $order_summary.="<li>Razorpay Order ID: ".$arr['razorpayOrderId']."</li>";
                            $order_summary.="<li>Razorpay Payment ID: ".$arr['razorpayPaymentId']."</li>";
                        }
                    }
				}
				////////////////added -replacement payment///////////////////////
				if($queryRecordsObj->type_of_order=="replaced"){

					if($order_replacement_decision_data_obj->paid_by=="customer"){
						if($order_replacement_decision_data_obj->balance_amount_paid_by=="cod"){
							$order_summary.="<li>Payment Type: COD <span class='text-info bold'>(Amount due)</span></li>";
						}else{
							$order_summary.="<li>Payment Type: ".$order_replacement_decision_data_obj->balance_amount_paid_by."</li>";
							
							if($order_replacement_decision_data_obj->payment_status=="paid"){
								
								$str= "<span class='text-info bold'>(".$order_replacement_decision_data_obj->paid_by."  paid through ".$order_replacement_decision_data_obj->balance_amount_paid_by.") </span>";
							}else{
								$str=($order_replacement_decision_data_obj->balance_amount_paid_by=="")? " Not Selected" : "<span class='text-info bold'>(".$order_replacement_decision_data_obj->paid_by." has to pay through ".$order_replacement_decision_data_obj->balance_amount_paid_by.") </span>";
							}			
							$order_summary.='<li>'.$str.'</li>';
						}
					}
				
					if($order_replacement_decision_data_obj->paid_by=="admin"){
						$order_summary.="<li>Payment Type: Null </li>";
						//$return_summary.=" (".$order_replacement_decision_data_obj->paid_by." has to pay through ".$order_replacement_decision_data_obj->refund_method.")<br>";
					}
				
				}
			////////////////added -replacement payment///////////////////////
			
				if($queryRecordsObj->type_of_order=="replaced"){
					$order_summary.="<li>[<span class='text-info bold'>Replaced Order</span>]</li>";
				}
				
				if($queryRecordsObj->prev_order_item_id!=""){
					
					$orders_status_data_obj=$this->Model_orders->get_orders_status_data($queryRecordsObj->prev_order_item_id);
					
					$replacement_desired_data_obj=$this->Model_orders->get_replacement_desired_data($queryRecordsObj->prev_order_item_id);
					
					//$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->prev_order_item_id);
					
					if($order_replacement_decision_data_obj->pickup_identifier=="after replace"){
						$order_summary.="<li>[<span class='text-info bold'>Pickup has been completed</span>]</li>";
					}
					if($order_replacement_decision_data_obj->pickup_identifier=="before replace"){
						$replacement_option_status='';
						if($orders_status_data_obj->initiate_replacement_pickup =='1' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='[Return the Item]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='1' && $orders_status_data_obj->order_replacement_pickup==1){
							$replacement_option_status='[Item Had been Collected Successfully]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='0' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='[Initiate Pickup has been rejected by admin]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='';
						}
						$order_summary.="<li>".'<span class="text-info bold">'.$replacement_option_status.'</span>'."</li>";
					}
				}
				
				/////////////////added - promotion////////////////////
				$str_promo='';	
				if($queryRecordsObj->type_of_order=="replaced"){

					$prev_order_item_id=$queryRecordsObj->prev_order_item_id;
					$purchased_order_data_obj=$this->Model_orders->get_shipping_charge_and_grand_total_from_history_orders($prev_order_item_id);
					
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion_for_replaced_order($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal,$prev_order_item_id);
					
				}else{
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal);   

				}
				$order_summary.='<li>'.$str_promo.'</li>';
				$order_summary.='</ul>';
				
		   
				
				/////////////////added - promotion////////////////////
				$addon_str='';

				if($queryRecordsObj->ord_addon_master_order_item_id!=''){

					$sku_id=get_main_sku($queryRecordsObj->ord_addon_master_order_item_id);
					$addon_str.='<b class="text-primary"><small>AddOn SKU (Main SKU Id:'.$sku_id.')</small></b>';
				}else{
					$addon_str.='<b class="text-primary"><small>Main SKU</small></b>';
				}
				/* get product vendor details */
				if($queryRecordsObj->product_vendor_id!=''){
					$p_v_data=get_product_vendor_details($queryRecordsObj->product_vendor_id);
					if(!empty($p_v_data)){
						$addon_str.='<br><br><div><small><u>Product Vendor</u></small></div>';
						$addon_str.='<div><b class="text-danger"><small>'.$p_v_data->name.'</small></b></div>';
						$addon_str.='<div class="text-danger"><small>'.$p_v_data->email.'</small></div>';
					}
				}
				/* get product vendor details */
				$row[]=$order_summary.$addon_str;

				$logi_details=$this->logistics_details_of_order($queryRecordsObj);
				$row[]=$logi_details;
				$order_quantity_price='<ul class="list-unstyled small">';
				if($queryRecordsObj->type_of_order=="replaced"){
					//$replaced_item_price_individual=$this->Model_orders->get_replaced_item_price_individual($prev_order_item_id);
					$replaced_item_price_individual=$queryRecordsObj->product_price;
					$order_quantity_price.="<li>Qty:".$queryRecordsObj->quantity."</li>";
					
					$order_quantity_price.="<li>Price of replacement order (each):  ".curr_sym." ".$replaced_item_price_individual."</li>";
					
					$order_quantity_price.="<li>Shipping charge for replacement:  ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
					$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
				}else{
					$order_quantity_price.="<li>Qty:".$queryRecordsObj->quantity."</li>";
					$order_quantity_price.="<li>Total Item Price:".$queryRecordsObj->subtotal."</li>";
					if($queryRecordsObj->promotion_invoice_free_shipping>0){
						$order_quantity_price.="<li>Shipping charge (<span class='text-info bold'>waived off</span>): ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
						$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->subtotal."</li>";
					}else{
						$order_quantity_price.="<li>Shipping charge: ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
						$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
					}	
					
					$order_quantity_price.='<li><a class="text-info bold" href="#" onclick="view_order_summary_(\''.$queryRecordsObj->order_id.'\')" id="view_order_summary_'.$queryRecordsObj->order_item_id.'">View Invoice Data ';
					if($queryRecordsObj->invoice_offer_achieved==1){
						$invoice_ofr_count=$this->get_invoice_offers_str($queryRecordsObj);
						$order_quantity_price.='<span class="badge text-danger">'.$invoice_ofr_count.'</span>';
					}
					$order_quantity_price.='</a></li>';
				}
				$order_quantity_price.='</ul>';
				$row[]=$order_quantity_price;
				$customer_add='<ul class="list-unstyled small">';
				$customer_add.="<li>".$queryRecordsObj->customer_name."</li>";
				$customer_add.="<li>".$queryRecordsObj->address1."</li>";
				$customer_add.="<li>".$queryRecordsObj->address2."</li>";
				$customer_add.="<li>".$queryRecordsObj->city.",".$queryRecordsObj->state."</li>";
				$customer_add.="<li>".$queryRecordsObj->country."</li>";
				$customer_add.="<li>".$queryRecordsObj->pincode."</li>";
				$customer_add.="<li>".$queryRecordsObj->mobile."</li>";
				$customer_add.='</ul>';
				$row[]=$customer_add;
				$data[]=$row;
				$row=array();
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function set_as_delivered(){
		
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$list_to_be_delivered=$this->input->post("list_to_be_delivered");
			$order_delivered_date=$this->input->post("order_delivered_date");
			$order_delivered_time=$this->input->post("order_delivered_time");
			$send_notification=$this->input->post("send_notification");
			
			$list_to_be_delivered_arr=explode("-",$list_to_be_delivered);
			
			$list_to_be_delivered_order_item_arr=array();
			foreach($list_to_be_delivered_arr as $orders_status_id){
				$list_to_be_delivered_order_item_arr[]=$this->Model_orders->get_order_item_id_by_orders_status_id($orders_status_id);
			}
			
			foreach($list_to_be_delivered_order_item_arr as $order_item_id){
			
					$order_active_data=$this->Model_orders->get_all_order_data_from_order_item($order_item_id);/*fetch data from active order table based on order_item_id*/
					foreach($order_active_data as $data){
						$purchased_price=$data['purchased_price'];
						$prev_order_item_id=$data['prev_order_item_id'];
						$order_item_id=$data['order_item_id'];
						$customer_id=$data['customer_id'];
						$order_id=$data['order_id'];
						$shipping_address_id=$data['shipping_address_id'];
						$inventory_id=$data['inventory_id'];
						$product_id=$data['product_id'];
						$sku_id=$data['sku_id'];
						$tax_percent=$data['tax_percent'];
						$product_price=$data['product_price'];
						$shipping_charge=$data['shipping_charge'];
						$subtotal=$data['subtotal'];
						$wallet_status=$data['wallet_status'];
						$wallet_amount=$data['wallet_amount'];
						$grandtotal=$data['grandtotal'];
						$quantity=$data['quantity'];
						$timestamp=$data['timestamp'];
						$image=$data['image'];
						$random_number=$data['random_number'];
						$invoice_email=$data['invoice_email'];
						$invoice_number=$data['invoice_number'];
						$logistics_name=$data['logistics_name'];
						$logistics_weblink=$data['logistics_weblink'];
						$tracking_number=$data['tracking_number'];
						$expected_delivery_date=$data['expected_delivery_date'];
						$delivery_mode=$data['delivery_mode'];
						$parcel_category=$data['parcel_category'];

						$vendor_id=$data['vendor_id'];
						$payment_type=$data['payment_type'];
						$payment_status=$data['payment_status'];
						$return_period=$data['return_period'];
						
						$logistics_id=$data['logistics_id'];
						$logistics_delivery_mode_id=$data['logistics_delivery_mode_id'];
						$logistics_territory_id=$data['logistics_territory_id'];
						$logistics_parcel_category_id=$data['logistics_parcel_category_id'];

						$promotion_available=$data['promotion_available'];
						$promotion_residual=$data['promotion_residual'];
						$promotion_discount=$data['promotion_discount'];
						$promotion_cashback=$data['promotion_cashback'];
						$promotion_surprise_gift=$data['promotion_surprise_gift'];
						$promotion_surprise_gift_type=$data['promotion_surprise_gift_type'];
						$promotion_surprise_gift_skus=$data['promotion_surprise_gift_skus'];
						$promotion_surprise_gift_skus_nums=$data['promotion_surprise_gift_skus_nums'];
						$default_discount=$data['default_discount'];
						$promotion_item_multiplier=$data['promotion_item_multiplier'];
						$promotion_item=$data['promotion_item'];
						$promotion_item_num=$data['promotion_item_num'];
						$promotion_clubed_with_default_discount=$data['promotion_clubed_with_default_discount'];
						$promotion_id_selected=$data['promotion_id_selected'];
						$promotion_type_selected=$data['promotion_type_selected'];
						$promotion_minimum_quantity=$data['promotion_minimum_quantity'];
						$promotion_quote=$data['promotion_quote'];
						$promotion_default_discount_promo=$data['promotion_default_discount_promo'];
						$individual_price_of_product_with_promotion=$data['individual_price_of_product_with_promotion'];
						$individual_price_of_product_without_promotion=$data['individual_price_of_product_without_promotion'];
						$total_price_of_product_without_promotion=$data['total_price_of_product_without_promotion'];
						$total_price_of_product_with_promotion=$data['total_price_of_product_with_promotion'];
						$quantity_without_promotion=$data['quantity_without_promotion'];
						$quantity_with_promotion=$data['quantity_with_promotion'];
						$cash_back_value=$data['cash_back_value'];
						$razorpayOrderId=$data['razorpayOrderId'];
						$razorpayPaymentId=$data['razorpayPaymentId'];
						$ord_max_selling_price=$data['ord_max_selling_price'];
						$ord_selling_discount=$data['ord_selling_discount'];
						$ord_tax_percent_price=$data['ord_tax_percent_price'];
						$ord_taxable_price=$data['ord_taxable_price'];
						$ord_sku_name=$data['ord_sku_name'];
						
						/* coupon */

						$ord_coupon_code=$data['ord_coupon_code'];
						$ord_coupon_name=$data['ord_coupon_name'];
						$ord_coupon_type=$data['ord_coupon_type'];
						$ord_coupon_value=$data['ord_coupon_value'];
						$ord_coupon_reduce_price=$data['ord_coupon_reduce_price'];
						$ord_coupon_status=$data['ord_coupon_status'];
                                                
						/* coupon */

						/* addon products details */
						$ord_addon_products_status=$data['ord_addon_products_status'];
						$ord_addon_products=$data['ord_addon_products'];
						$ord_addon_inventories=$data['ord_addon_inventories'];
						$ord_addon_total_price=$data['ord_addon_total_price'];
						
						$ord_addon_single_or_multiple_tagged_inventories_in_frontend=$data['ord_addon_single_or_multiple_tagged_inventories_in_frontend'];
						$ord_addon_master_order_item_id=$data['ord_addon_master_order_item_id'];
						
						
						$product_vendor_id=$data['product_vendor_id'];
						/* addon products details */
						//,$ord_addon_products_status,$ord_addon_products,$ord_addon_inventories,$ord_addon_total_price

					}
					$type_of_order=$this->get_the_type_of_order_fresh_or_replaced($order_item_id);
					
					if($type_of_order==""){
						
                                            //curr_code=INR in defined in constants
						if($promotion_surprise_gift_type==curr_code){
							//$wallet_updated=$this->update_wallet_for_surprise_gift($order_item_id,$customer_id,$promotion_surprise_gift);
							$refund_wallet_comments="Surprise gift on delivery of item";
							$updated=$this->Model_orders->update_order_item_offers($order_item_id,$order_id,$customer_id,$refund_wallet_comments,$promotion_surprise_gift,$promotion_quote);
							
							//surprise gift amount transfered to the customer wallet
						}
						if($promotion_available==1 && $promotion_cashback>0){
							//&& $payment_type=="COD" //for all payment
							//$wallet_updated=$this->update_wallet_for_cashback($order_item_id,$customer_id,$cash_back_value);
							$refund_wallet_comments="Cash back amount on delivery of item";
							$updated=$this->Model_orders->update_order_item_offers($order_item_id,$order_id,$customer_id,$refund_wallet_comments,$cash_back_value,$promotion_quote);
							//cash back amount transfered to the customer wallet
						}
						
						/* move above data to completed_orders table*/
						$this->Model_orders->active_order_add_to_completed_orders($purchased_price,$order_item_id,$customer_id,$order_id,$shipping_address_id,$inventory_id,$product_id,$sku_id,$tax_percent,$product_price,$shipping_charge,$subtotal,$wallet_status,$wallet_amount,$grandtotal,$quantity,$timestamp,$image,$random_number,$invoice_email,$invoice_number,$logistics_name,$logistics_weblink,$tracking_number,$expected_delivery_date,$delivery_mode,$parcel_category,$vendor_id,$payment_type,$payment_status,$return_period,$logistics_id,$logistics_delivery_mode_id,$logistics_territory_id,$logistics_parcel_category_id,$promotion_available,$promotion_residual,$promotion_discount,$promotion_cashback,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$default_discount,$promotion_item_multiplier,$promotion_item,$promotion_item_num,$promotion_clubed_with_default_discount,$promotion_id_selected,$promotion_type_selected,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_without_promotion,$total_price_of_product_with_promotion,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$razorpayOrderId,$razorpayPaymentId,$ord_max_selling_price,$ord_selling_discount,$ord_tax_percent_price,$ord_taxable_price,$ord_sku_name,$ord_coupon_code,$ord_coupon_name,$ord_coupon_type,$ord_coupon_value,$ord_coupon_reduce_price,$ord_coupon_status,$ord_addon_products_status,$ord_addon_products,$ord_addon_inventories,$ord_addon_total_price,$ord_addon_single_or_multiple_tagged_inventories_in_frontend,$ord_addon_master_order_item_id,$product_vendor_id);
					}
					if($type_of_order=="replaced"){
						/* move above data to replaced_orders table*/
						$this->Model_orders->active_order_add_to_replaced_orders($purchased_price,$prev_order_item_id,$order_item_id,$customer_id,$order_id,$shipping_address_id,$inventory_id,$product_id,$sku_id,$tax_percent,$product_price,$shipping_charge,$subtotal,$wallet_status,$wallet_amount,$grandtotal,$quantity,$timestamp,$image,$random_number,$invoice_email,$invoice_number,$logistics_name,$logistics_weblink,$tracking_number,$expected_delivery_date,$delivery_mode,$parcel_category,$vendor_id,$payment_type,$payment_status,$return_period,$logistics_id,$logistics_delivery_mode_id,$logistics_territory_id,$logistics_parcel_category_id,$promotion_available,$promotion_residual,$promotion_discount,$promotion_cashback,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$default_discount,$promotion_item_multiplier,$promotion_item,$promotion_item_num,$promotion_clubed_with_default_discount,$promotion_id_selected,$promotion_type_selected,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_without_promotion,$total_price_of_product_with_promotion,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$razorpayOrderId,$razorpayPaymentId,$ord_max_selling_price,$ord_selling_discount,$ord_tax_percent_price,$ord_taxable_price,$ord_sku_name,$ord_coupon_code,$ord_coupon_name,$ord_coupon_type,$ord_coupon_value,$ord_coupon_reduce_price,$ord_coupon_status,$ord_addon_products_status,$ord_addon_products,$ord_addon_inventories,$ord_addon_total_price,$ord_addon_single_or_multiple_tagged_inventories_in_frontend,$ord_addon_master_order_item_id,$product_vendor_id);
					}
					
			}
			
			//completed orders exists
			
			$flag=$this->Model_orders->set_as_delivered($list_to_be_delivered,$order_delivered_date,$order_delivered_time,$order_id,$customer_id);
			
			if($flag==true){
				
				/////////////////////////////////////////////////////////////
				$result_order_item_id_list_obj_arr=$this->Model_orders->get_order_item_id_list_by_orders_status_id($list_to_be_delivered);
				$status_view="delivered_status_view";
				$flag_update=$this->Model_orders->update_notification_to_send($result_order_item_id_list_obj_arr,$send_notification,$status_view);
				
				$send_sms=$this->input->post("sms");
				$send_email=$this->input->post("email");
				
				if($send_sms==1 || $send_email==1){
					foreach($result_order_item_id_list_obj_arr as $result_order_item_id_list_obj){
						
						$order_summary_mail_data_arr=$this->Model_orders->get_order_summary_mail_data($result_order_item_id_list_obj->order_item_id);
						foreach($order_summary_mail_data_arr as $order_summary_mail_data){
							
						$email=$this->Model_orders->get_email_db($order_summary_mail_data['customer_id']);
						$mobile=$this->Model_orders->get_mobile_db($order_summary_mail_data['customer_id']);
						
						if($mobile != '' && $send_sms==1){
							$this->send_sms_in_order_process($order_summary_mail_data,$mobile,"shipped");
						}//sms
				
						if($email!='' && $send_email==1){
							$this->send_email_in_order_process($order_summary_mail_data,$email,"shipped");
							
						}//mail	

						}
					}
				}
				/////////////////////////////////////////////////////////////
				
				
			}
			
			if($flag){
				foreach($list_to_be_delivered_order_item_arr as $order_item_id){
					$this->Model_orders->remove_data_from_active_order_table($order_item_id);
				}
				
			}
			echo $flag;			
					
			////////////////////
			
			
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_the_type_of_order_fresh_or_replaced($order_item_id){
		$type_of_order=$this->Model_orders->get_the_type_of_order_fresh_or_replaced($order_item_id);
		return $type_of_order;
	}
	//////////////////
	///////////////////////
	public function delivered_orders(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["menu_flag"]="inactive_order_active_links";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/delivered_orders',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	////////////////////////edited/////////delivered Orders Processing/////////
	
	public function delivered_orders_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();		
			$totalRecords=0;	
			$queryRecordsResult=$this->Model_orders->delivered_orders_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				$order_replacement_pickup=$queryRecordsObj->order_replacement_pickup;
				$purchased_quantity=$queryRecordsObj->quantity;
				$product_price=$queryRecordsObj->product_price;
				$shipping_charge=$queryRecordsObj->shipping_charge;
				$grandTotal=$queryRecordsObj->grandtotal;
				$subtotal=$queryRecordsObj->subtotal;
				$replaced_orders_arr=$this->Model_orders->get_replaced_orders($queryRecordsObj->order_item_id);
				$flag=true;
				
				if(!empty($replaced_orders_arr)){
					if($replaced_orders_arr["quantity"]==$queryRecordsObj->quantity){
						$flag=false;//fully replaced.so it should go to returned orders
					}
				}
	
				$refunded_details_from_returned_orders_table_arr=$this->get_refunded_details_from_returned_orders_table($queryRecordsObj->order_item_id);
				
				$sub_refunded_details_from_returned_orders_table_arr=$this->get_sub_refunded_details_from_returned_orders_table($queryRecordsObj->order_item_id);
				
				$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->order_item_id);
				
				$quantity_covered=0;
				
				if(count($refunded_details_from_returned_orders_table_arr)>0){
					$quantity_covered+=$refunded_details_from_returned_orders_table_arr["quantity"];
				}
				if(count($sub_refunded_details_from_returned_orders_table_arr)>0){
					$quantity_covered+=$sub_refunded_details_from_returned_orders_table_arr["quantity"];
				}
				//|| $order_replacement_decision_data_obj->refund_status=="refunded"//removed
				if(!empty($order_replacement_decision_data_obj)){
					if($order_replacement_decision_data_obj->status=="replaced" || $order_replacement_decision_data_obj->status=="refunded" || ($order_replacement_decision_data_obj->customer_status=="cancel" && $order_replacement_pickup==1)){
						$quantity_covered+=$order_replacement_decision_data_obj->quantity;
					}
				}
				
				if($quantity_covered==$queryRecordsObj->quantity){
					$flag=false;//fully replaced.so it should go to returned orders
				}
			if($flag){
				$totalRecords++;
				$row=array();
				
				$order_summary='<ul class="list-unstyled small">';
				//if($queryRecordsObj->ord_addon_products_status=='1'){
				if(0){
					$combo_prod=$this->get_addon_data($queryRecordsObj);
					$order_summary.='<li>'.$combo_prod.'</li>';
					$row[]="Combo products";
					
					$order_summary.="<li>SKU:".$queryRecordsObj->sku_id."</li>";
                                if($queryRecordsObj->ord_sku_name!=''){
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
                                }else{
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
                                }
				
				}else{
					$inventory_id=$queryRecordsObj->inventory_id;
					$image=$queryRecordsObj->image;
					$img_path=get_inventory_image_path($inventory_id,$image);

					$row[]="<img src='".base_url().$img_path."'  style='width: 100px;height: 122px;' >";
					$order_summary.="<li>SKU:".$queryRecordsObj->sku_id."</li>";
                                if($queryRecordsObj->ord_sku_name!=''){
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
                                }else{
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
                                }

								$product_details=get_product_details($queryRecordsObj->product_id);
								if(!empty($product_details))
								$order_summary.=($product_details->product_description!='') ? "<li class='mt-10'><small>".$product_details->product_description.'</small></li>': '';

								
				}
				$order_summary.="<li>Order Date:".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."</li>";
				$order_summary.="<li>Order ID:".$queryRecordsObj->order_id.' <i  style="cursor:pointer" class="fa fa-question-circle orderid_details_popover" data-trigger="focus" id="orderid_'.$queryRecordsObj->order_id."_".$queryRecordsObj->order_item_id.'" aria-hidden="true"></i>'."</li>";
				$order_summary.="<li>Order Item ID:".$queryRecordsObj->order_item_id."</li>";

				/* razorpay details */
                if($queryRecordsObj->type_of_order==""){
                    $order_summary.="<br/><li>Payment Type: ".$queryRecordsObj->payment_type."</li>";
                    if($queryRecordsObj->payment_type=='Razorpay'){
                        $arr=razorpay_ids($queryRecordsObj->order_id);
                        if(!empty($arr)){
                            $order_summary.="<li>Razorpay Order ID: ".$arr['razorpayOrderId']."</li>";
                            $order_summary.="<li>Razorpay Payment ID: ".$arr['razorpayPaymentId']."</li><br/>";
                        }
                    }
                }
				/* razorpay details */

				if(!empty($replaced_orders_arr)){
					$prev_order_item_id=$replaced_orders_arr['prev_order_item_id'];
					
					$order_status_data_obj=$this->Model_orders->get_orders_status_data_of_replaced_item($prev_order_item_id);
					
					$str="<li><u>Replaced Order</u>".'</li>';
					$str.=" <li>Date: ".$order_status_data_obj->order_delivered_date."</li><li> Time: ".$order_status_data_obj->order_delivered_time.'</li>';
					
					if($order_status_data_obj->type_of_order=="replaced"){
						$order_summary.="<li>[This Order includes replaced Order with order item ID ".$order_status_data_obj->order_item_id."]</li>";
					}
				}else{
					
					$str="<li> Date: ".$queryRecordsObj->order_delivered_date."</li><li> Time: ".$queryRecordsObj->order_delivered_time.'</li>';
				}
				
				/////////////////added - promotion////////////////////
				$str_promo='';
				if($queryRecordsObj->type_of_order=="replaced"){

					$prev_order_item_id=$queryRecordsObj->prev_order_item_id;
					$purchased_order_data_obj=$this->Model_orders->get_shipping_charge_and_grand_total_from_history_orders($prev_order_item_id);
					
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion_for_replaced_order($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal,$prev_order_item_id);
					
				}else{
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal);
				}
				$order_summary.="<li>".$str_promo."</li>";
		   		$order_summary.='</ul>';	
				/////////////////added - promotion////////////////////
				
				$addon_str='';

				if($queryRecordsObj->ord_addon_master_order_item_id!=''){

					$sku_id=get_main_sku($queryRecordsObj->ord_addon_master_order_item_id);
					$addon_str.='<b class="text-primary"><small>AddOn SKU (Main SKU Id:'.$sku_id.')</small></b>';
				}else{
					$addon_str.='<b class="text-primary"><small>Main SKU</small></b>';
				}
				/* get product vendor details */
				if($queryRecordsObj->product_vendor_id!=''){
					$p_v_data=get_product_vendor_details($queryRecordsObj->product_vendor_id);
					if(!empty($p_v_data)){
						$addon_str.='<br><br><div><small><u>Product Vendor</u></small></div>';
						$addon_str.='<div><b class="text-danger"><small>'.$p_v_data->name.'</small></b></div>';
						$addon_str.='<div class="text-danger"><small>'.$p_v_data->email.'</small></div>';
					}
				}
				/* get product vendor details */
				$row[]=$order_summary.$addon_str;

				$gift_amount_transfer='';

				if($queryRecordsObj->type_of_order==""){
					if($queryRecordsObj->promotion_surprise_gift_type==curr_code){
						$gift_amount_transfer.='<li><span class="text-info">'.curr_sym." ".$queryRecordsObj->promotion_surprise_gift.' was transferred to wallet (surprise gift)</span></li>';
					}
				}
				
				$logi_details=$this->logistics_details_of_order_final($queryRecordsObj);
				
				$row[]='<ul class="list-unstyled small">'.$gift_amount_transfer.$str.'</ul>'.$logi_details;

				$order_details="<li>purchased Qty:".$queryRecordsObj->quantity."</li>";
				$order_details.="<li>Total Item Price:".$queryRecordsObj->subtotal."</li>";
				if($queryRecordsObj->promotion_invoice_free_shipping>0){
					$order_details.="<li>Shipping charge (<span style='color:red;'>waived off</span>): ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
					$order_details.="<li>Total: ".curr_sym." ".$queryRecordsObj->subtotal."</li>";
				}else{
					$order_details.="<li>Shipping charge: ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
					$order_details.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
				}	
				
				
//<!---------------------------------------------------------------------------------------------------------->//				
				$check_request=$this->Model_orders->check_request_is_there_for_this_order($queryRecordsObj->order_item_id);
				$str_diff='';
				if($check_request=="yes"){
					$order_item_id=$queryRecordsObj->order_item_id;
					$order_details.='<li><form target="_blank" name="return_details_'.$order_item_id.'" id="return_details_'.$order_item_id.'" method="post"><input type="hidden" value="'.$order_item_id.'" name="order_item_id"></form><a class="text-info bold" onclick="go_to_return_details('.$order_item_id.')" style="cursor:pointer;"> View Return Details </a></li>';
				
				}//check_request
//<!---------------------------------------------------------------------------------------------------------->//							
				
				
				
				if($queryRecordsObj->type_of_order==""){
					
					$order_details.='<li><a class="text-info bold" href="#" onclick="view_order_summary_(\''.$queryRecordsObj->order_id.'\')" id="view_order_summary_'.$queryRecordsObj->order_item_id.'">View Invoice Data ';
					if($queryRecordsObj->invoice_offer_achieved==1){
						$invoice_ofr_count=$this->get_invoice_offers_str($queryRecordsObj);
						$order_details.='<span class="badge" style="color: red;">'.$invoice_ofr_count.'</span>';
					}
					$order_details.='</a></li>';
					
					
					if($queryRecordsObj->promotion_invoice_cash_back>0 && $queryRecordsObj->customer_response_for_invoice_cashback=="accept" && $queryRecordsObj->status_of_refund_for_cashback=="refunded" && $queryRecordsObj->invoice_number_for_cashback!=''){
						
						$order_details.='<li><a class="text-info bold" href="#" onclick="generate_invoice_for_order_ID_offers('.$queryRecordsObj->order_id.','."'".'cashback'."'".')" id="view_order_invoice'.$queryRecordsObj->order_item_id.'">View Order Invoice';
					
						$order_details.='</a></li>';
						
					}
					
					if($queryRecordsObj->invoice_surprise_gift>0 && $queryRecordsObj->customer_response_for_surprise_gifts=="accept" && $queryRecordsObj->status_of_refund_for_surprise_gifts=="refunded" && $queryRecordsObj->invoice_number_for_surprise_gifts!=''){
						
						$order_details.='<li><a class="text-info bold" href="#" onclick="generate_invoice_for_order_ID_offers('.$queryRecordsObj->order_id.','."'".'surprise_gift'."'".')" id="view_order_invoice'.$queryRecordsObj->order_item_id.'">View Order Invoice';
					
						$order_details.='</a></li>';
						
					}
					
				}
				
				$row[]='<ul class="list-unstyled small">'.$order_details.'</ul>';
				$customer_add='<ul class="list-unstyled small">';
				$customer_add.="<li>".$queryRecordsObj->customer_name."</li>";
				$customer_add.="<li>".$queryRecordsObj->address1."</li>";
				$customer_add.="<li>".$queryRecordsObj->address2."</li>";
				$customer_add.="<li>".$queryRecordsObj->city.",".$queryRecordsObj->state."</li>";
				$customer_add.="<li>".$queryRecordsObj->country."</li>";
				$customer_add.="<li>".$queryRecordsObj->pincode."</li>";
				$customer_add.="<li>".$queryRecordsObj->mobile."</li>";
				$customer_add.='</ul>';
				$row[]=$customer_add;
				
				// <button =\"javascript:void(0);\" onclick=\"sendInvoiceFun('transit','$queryRecordsObj->order_item_id','$queryRecordsObj->order_id')\" class='btn btn-xs btn-block btn-primary transit_invoice_sent_$queryRecordsObj->order_item_id'>Transit Invoice</button> 
				
				$row[]="<button =\"javascript:void(0);\" onclick=\"sendInvoiceFun('customer','$queryRecordsObj->order_item_id','$queryRecordsObj->order_id')\" class='btn btn-xs btn-block btn-primary _customer_invoice_sent_$queryRecordsObj->order_item_id'>Customer Invoice</button>";
				$data[]=$row;
				
				
				
				
			}else{
				
				$row=array();
				}
				
			}//foreach end
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function returned_orders_processing(){
		
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();		
			$totalRecords=0;		
			$queryRecordsResult=$this->Model_orders->returned_orders_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				$order_replacement_pickup=$queryRecordsObj->order_replacement_pickup;
				$purchased_quantity=$queryRecordsObj->quantity;
				$product_price=$queryRecordsObj->product_price;
				$shipping_charge=$queryRecordsObj->shipping_charge;
				$grandTotal=$queryRecordsObj->grandtotal;
				$subtotal=$queryRecordsObj->subtotal;
				$replaced_orders_arr=$this->Model_orders->get_replaced_orders($queryRecordsObj->order_item_id);
				$flag=false;
			
				if(!empty($replaced_orders_arr)){
					if($replaced_orders_arr["quantity"]==$queryRecordsObj->quantity){
						$flag=true;//fully replaced.so it should go to returned orders	
					}
				}

				$refunded_details_from_returned_orders_table_arr=$this->get_refunded_details_from_returned_orders_table($queryRecordsObj->order_item_id);
				
				$sub_refunded_details_from_returned_orders_table_arr=$this->get_sub_refunded_details_from_returned_orders_table($queryRecordsObj->order_item_id);
				
				$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->order_item_id);
				
				
				$quantity_covered=0;
				
				if(count($refunded_details_from_returned_orders_table_arr)>0){
					$quantity_covered+=$refunded_details_from_returned_orders_table_arr["quantity"];
				}
				if(count($sub_refunded_details_from_returned_orders_table_arr)>0){
					$quantity_covered+=$sub_refunded_details_from_returned_orders_table_arr["quantity"];
				}
				if(!empty($order_replacement_decision_data_obj)){
					if($order_replacement_decision_data_obj->status=="replaced" || $order_replacement_decision_data_obj->status=="refunded" || ($order_replacement_decision_data_obj->customer_status=="cancel" && $order_replacement_pickup==1)){
						$quantity_covered+=$order_replacement_decision_data_obj->quantity;
					}
				}
				
				if($quantity_covered==$queryRecordsObj->quantity){
					$flag=true;//fully covered(quantity)
				}
				
				//echo $flag;
				
			if($flag){
				$totalRecords++;
				$row=array();
				$row[]="<img src='".base_url().$queryRecordsObj->image."' width='50' height='80'>";
				$order_summary="<ul class='list-unstyled'>";
				$order_summary.="<li>SKU:".$queryRecordsObj->sku_id."</li>";
                                if($queryRecordsObj->ord_sku_name!=''){
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
                                }else{
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
                                }

								$product_details=get_product_details($queryRecordsObj->product_id);
								if(!empty($product_details))
								$order_summary.=($product_details->product_description!='') ? "<li class='mt-10'><small>".$product_details->product_description.'</small></li>': '';
								

				$order_summary.="<li>Order Date:".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."</li>";
				$order_summary.="<li>Order ID:".$queryRecordsObj->order_id."</li>";
				$order_summary.="<li>Order Item ID:</b>".$queryRecordsObj->order_item_id."</li>";
			
				$promotion_available=$queryRecordsObj->promotion_available;
				$order_item_obj=(object) array();
				if($promotion_available==1){
					$order_item_obj=$this->Model_orders->get_details_of_order_item($queryRecordsObj->order_item_id);
				}
				if(!empty($replaced_orders_arr)){
					$prev_order_item_id=$replaced_orders_arr['prev_order_item_id'];
					
					$order_status_data_obj=$this->Model_orders->get_orders_status_data_of_replaced_item($prev_order_item_id);
					
					$str="<li><u>Replaced Order</u></li><li> Date: ".$order_status_data_obj->order_delivered_date."</li><li> Time:".$order_status_data_obj->order_delivered_time.'</li>';
					
					if($order_status_data_obj->type_of_order=="replaced"){
						$order_summary.="<li>[This Order includes replaced Order with order item ID ".$order_status_data_obj->order_item_id."]</li>";
					}
				}else{
					$str="<li>Date: ".$queryRecordsObj->order_delivered_date."</li><li> Time:".$queryRecordsObj->order_delivered_time."</li>";
				}
				
				/////////////////added - promotion////////////////////
				$str_promo='';	
				if($queryRecordsObj->type_of_order=="replaced"){

					$prev_order_item_id=$queryRecordsObj->prev_order_item_id;
					$purchased_order_data_obj=$this->Model_orders->get_shipping_charge_and_grand_total_from_history_orders($prev_order_item_id);
					
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion_for_replaced_order($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal,$prev_order_item_id);
					
				}else{
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal);
				}
				$order_summary.='</ul>';
				$order_summary.=$str_promo;
				
				/////////////////added - promotion////////////////////
				
				$addon_str='';

				if($queryRecordsObj->ord_addon_master_order_item_id!=''){

					$sku_id=get_main_sku($queryRecordsObj->ord_addon_master_order_item_id);
					$addon_str.='<b class="text-primary"><small>AddOn SKU (Main SKU Id:'.$sku_id.')</small></b>';
				}else{
					$addon_str.='<b class="text-primary"><small>Main SKU</small></b>';
				}
				/* get product vendor details */
				if($queryRecordsObj->product_vendor_id!=''){
					$p_v_data=get_product_vendor_details($queryRecordsObj->product_vendor_id);
					if(!empty($p_v_data)){
						$addon_str.='<br><br><div><small><u>Product Vendor</u></small></div>';
						$addon_str.='<div><b class="text-danger"><small>'.$p_v_data->name.'</small></b></div>';
						$addon_str.='<div class="text-danger"><small>'.$p_v_data->email.'</small></div>';
					}
				}
				/* get product vendor details */
				$row[]=$order_summary.$addon_str;

				$gift_amount_transfer='';
				
				if($queryRecordsObj->type_of_order==""){
					if($queryRecordsObj->promotion_surprise_gift_type==curr_code){
						$gift_amount_transfer.='<li><span style="color:blue;">'.curr_sym." ".$queryRecordsObj->promotion_surprise_gift.' was transferred to wallet <i>(surprise gift)</i></span></li>';
					}
				}
				
				$logi_details=$this->logistics_details_of_order_final($queryRecordsObj);
				
				$row[]='<ul class="list-unstyled small">'.$gift_amount_transfer.$str.'</ul>'.$logi_details;
			
				if($queryRecordsObj->type_of_order=="replaced"){
					$order_details="<li>Qty:".$queryRecordsObj->quantity."</li>";
					$order_details.="<li>Shipping charge for replacement: ".curr_sym." "." </b>".$queryRecordsObj->shipping_charge."</li>";
					$order_details.="<li>Total:</b> ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
				}else{
					$order_details="<li>purchased Qty:</b>".$queryRecordsObj->quantity."</li>";
					$order_details.="<li>Total Item Price:</b>".$queryRecordsObj->subtotal."</li>";
					if($queryRecordsObj->promotion_invoice_free_shipping>0){
						$order_details.="<li>Shipping charge (<span style='color:red;'>waived off</span>): ".curr_sym." </b>".$queryRecordsObj->shipping_charge."</li>";
						$order_details.="<li>Total:</b> ".curr_sym." ".$queryRecordsObj->subtotal."</li>";
					}else{
						$order_details.="<li>Shipping charge: ".curr_sym." </b>".$queryRecordsObj->shipping_charge."</li>";
						$order_details.="<li>Total:</b> ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
					}
					
					$order_details.='<li><a class="text-info bold" href="#" onclick="view_order_summary_(\''.$queryRecordsObj->order_id.'\')" id="view_order_summary_'.$queryRecordsObj->order_item_id.'">View Invoice Data ';
					if($queryRecordsObj->invoice_offer_achieved==1){
						$invoice_ofr_count=$this->get_invoice_offers_str($queryRecordsObj);
						$order_details.='<span class="badge"><i class="fa fa-gift" aria-hidden="true"></i> '.$invoice_ofr_count.'</span>';
					}
					$order_details.='</a>'."</li>";
					
					if($queryRecordsObj->promotion_invoice_cash_back>0 && $queryRecordsObj->customer_response_for_invoice_cashback=="accept" && $queryRecordsObj->status_of_refund_for_cashback=="refunded" && $queryRecordsObj->invoice_number_for_cashback!=''){
						
						$order_details.='<li><a class="text-info bold" href="#"  onclick="generate_invoice_for_order_ID_offers('.$queryRecordsObj->order_id.','."'".'cashback'."'".')" id="view_order_invoice'.$queryRecordsObj->order_item_id.'">View Order Invoice';
					
						$order_details.='</a>'."</li>";
						
					}
					
					if($queryRecordsObj->invoice_surprise_gift>0 && $queryRecordsObj->customer_response_for_surprise_gifts=="accept" && $queryRecordsObj->status_of_refund_for_surprise_gifts=="refunded" && $queryRecordsObj->invoice_number_for_surprise_gifts!=''){
						
						$order_details.='<li><a class="text-info bold" href="#"  onclick="generate_invoice_for_order_ID_offers('.$queryRecordsObj->order_id.','."'".'surprise_gift'."'".')" id="view_order_invoice'.$queryRecordsObj->order_item_id.'">View Order Invoice';
					
						$order_details.='</a>'."</li>";
						
					}
				}

				
				$check_request=$this->Model_orders->check_request_is_there_for_this_order($queryRecordsObj->order_item_id);
				
				if($check_request=="yes"){
					$order_item_id=$queryRecordsObj->order_item_id;
					$order_details.='<li><form target="_blank" name="return_details_'.$order_item_id.'" id="return_details_'.$order_item_id.'" method="post"><input type="hidden" value="'.$order_item_id.'" name="order_item_id"></form><a class="text-info bold" href="#"  onclick="go_to_return_details('.$order_item_id.')"> View Return Details </a></li>';
				
				
				}//check_request
				
				$row[]='<ul class="list-unstyled small">'.$order_details.'</ul>';
				
				$customer_add='<li>'.$queryRecordsObj->customer_name."</li>";
				$customer_add.='<li>'.$queryRecordsObj->address1."</li>";
				$customer_add.='<li>'.$queryRecordsObj->address2."</li>";
				$customer_add.='<li>'.$queryRecordsObj->city.",".$queryRecordsObj->state."</li>";
				$customer_add.='<li>'.$queryRecordsObj->country."</li>";
				$customer_add.='<li>'."Pincode:".$queryRecordsObj->pincode."</li>";
				$customer_add.='<li>'."Mobile:".$queryRecordsObj->mobile."</li>";
				
				$row[]='<ul class="list-unstyled small">'.$customer_add.'</ul>';
				$row[]="<button =\"javascript:void(0);\" onclick=\"sendInvoiceFun('$queryRecordsObj->order_item_id','$queryRecordsObj->order_id')\" class='btn btn-primary btn-xs btn-block invoice_sent_$queryRecordsObj->order_item_id'>Send Invoice</button>";
				$data[]=$row;
				
				
				
			}else{
				$row=array();
				}
			}//foreach end
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_order_details_of_refund($refunded_details_from_returned_orders_table_arr){
		
		$returned_orders=$refunded_details_from_returned_orders_table_arr;
		$first_trans='<table class="table table-bordered" cellspacing="0" width="100%">';
		$first_trans.='<thead class="bg-info"><th colspan="2"><b> First Transaction of Return Order ID :</b> '.$returned_orders["return_order_id"].' </th> </thead>';
		$first_trans.='<tbody>';
		$first_trans.="<tr><td>Quantity Refunded</td><td> ".$returned_orders['quantity']."</td></tr>";
		$first_trans.="<tr><td>Item Price Refunded</td><td> ".curr_sym." ".$returned_orders['item_with_deductions_concession']." (".$returned_orders['item_deduction_percentage']."% deducted)"."</td></tr>";
		$first_trans.="<tr><td>Shipping Price Refunded</td><td> ".curr_sym." ".$returned_orders['shipping_with_deductions_concession']." (".$returned_orders['shipping_deduction_percentage']."% deducted)"."</td></tr>";
		$first_trans.="<tr><td>Return Shipping Concession</td><td> ".curr_sym." ".$returned_orders['return_shipping_concession']."</td></tr>";
		$first_trans.="<tr><td>Other Concession</td><td> ".curr_sym." ".$returned_orders['return_other_concession']."</td></tr>";
		$first_trans.="<tr><td>Total</td><td> ".curr_sym." ".$returned_orders['total_refund_with_concession']."</td></tr>";
		$first_trans.="</tbody></table>";
		return $first_trans;
		
	}
	public function get_order_details_of_sub_refund($sub_refunded_details_arr){
		
		$sub_returned_orders=$sub_refunded_details_arr;
		$second_trans='<table class="table table-bordered" cellspacing="0" width="100%">';
		$second_trans.='<thead class="bg-info"><th colspan="2"><b>Second Transaction of same Return order</b> </th> </thead>';
		$second_trans.='<tbody>';
		$second_trans.="<tr><td>Quantity Refunded</td><td> ".$sub_refunded_details_arr['quantity']."</td></tr>";
		$second_trans.="<tr><td>Item Price Refunded</td><td> ".curr_sym." ".$sub_refunded_details_arr['item_with_deductions_concession']." (".$sub_refunded_details_arr['item_deduction_percentage']."% deducted)"."</td></tr>";
		$second_trans.="<tr><td>Shipping Price Refunded</td><td> ".curr_sym." ".$sub_refunded_details_arr['shipping_with_deductions_concession']." (".$sub_refunded_details_arr['shipping_deduction_percentage']."% deducted)"."</td></tr>";
		$second_trans.="<tr><td>Return Shipping Concession</td><td> ".curr_sym." ".$sub_refunded_details_arr['return_shipping_concession']."</td></tr>";
		$second_trans.="<tr><td>Other Concession </td><td>".curr_sym." ".$sub_refunded_details_arr['return_other_concession']."</td></tr>";
		$second_trans.="<tr><td>Total</td><td>".curr_sym." ".$sub_refunded_details_arr['total_refund_with_concession']."</td></tr>";
		$second_trans.="</tbody></table>";
		return $second_trans;
		
	}
	public function get_order_details_of_refund_after_replace_cancel($replacement_decision_obj,$replacement_obj,$order_replacement_pickup,$purchased_quantity){
		
		$refund_details='<table class="table table-bordered" cellspacing="0" width="100%">';
		$refund_details.='<thead class="bg-info"><th colspan="2"><b>Replacement is cancelled but order is refunded</b> </th> </thead>';
		$refund_details.='<tbody>';
								
		if($order_replacement_pickup==1){
			$refund_details.="<tr><td>Quantity Refunded</td><td> ".$replacement_decision_obj->quantity."</td></tr>";
		}
		
		$refund_details.="<tr><td>Purchased Item Price </td><td>  ".curr_sym." ".($replacement_decision_obj->quantity*$replacement_obj->return_value_each_inventory_id)."</td></tr>";
		
		if($replacement_obj->shipping_charge_applied_cancels_chk==1){
			$refund_details.="<tr><td>Shipping Charge for Purchased Item </td><td>  ".curr_sym." ".($replacement_obj->shipping_charge/$purchased_quantity)*$replacement_decision_obj->quantity."</td></tr>";
		}else{
			$refund_details.="<tr><td>Shipping Charge for Purchased Item </td><td> ".curr_sym."0</td></tr>";
			$replacement_obj->shipping_charge=0;
		}
		
		if($replacement_obj->paid_by=="customer" && $replacement_obj->payment_status=="paid"){
		
			$refund_details.="<tr><td>Balance Squared </td><td>  ".curr_sym." ".$replacement_decision_obj->balance_amount."</td></tr>";
			if($replacement_obj->paid_by=="customer" && $replacement_obj->payment_status=="pending"){
				$refund_details.="<tr><td>Not Paid by </td><td>".$replacement_obj->paid_by."</td></tr>";
			}
			if($replacement_obj->paid_by=="customer" && $replacement_obj->payment_status=="paid"){
				$refund_details.="<tr><td>Paid by </td><td>".$replacement_obj->paid_by." through". $replacement_obj->balance_amount_paid_by."</td></tr>";
			}
		
		}
		
		if($replacement_obj->return_shipping_concession_cancels_chk==0){
			$replacement_decision_obj->return_shipping_concession=0;
		}

		if($replacement_obj->paid_by=="admin" || $replacement_obj->paid_by==""){
			
			$refund_details.="<tr><td>Return Shipping Concession </td><td> ".$replacement_decision_obj->return_shipping_concession."</td></tr>";
			$refund_details.="<tr><td>Total Refunded Amount</td><td> ".curr_sym;
			//print_r($replacement_obj);
			$refund_details.=($replacement_decision_obj->return_shipping_concession)+($replacement_decision_obj->quantity*$replacement_obj->return_value_each_inventory_id)+($replacement_decision_obj->quantity*($replacement_obj->shipping_charge/$purchased_quantity))."</td></tr>";
			
			if($replacement_obj->refund_method==""){
				$refund_details.="<tr><td>Successfully refunded to</td><td>  Wallet</td></tr>";
			}else{
				$refund_details.="<tr><td>Successfully refunded to </td><td> ".$replacement_obj->refund_method."</td></tr>";
			}

		}
	
		if($replacement_obj->paid_by=="customer"){
			
			$refund_details.="<tr><td>Return Shipping Concession </td><td> ".curr_sym." ".$replacement_decision_obj->return_shipping_concession."</td></tr>";
			
			$refund_details.="<tr><td>Total Refunded Amount </td><td> ".curr_sym." ";
			
			if($replacement_obj->payment_status=="pending"){
				$refund_details.=($replacement_decision_obj->quantity*$replacement_obj->return_value_each_inventory_id)+(($replacement_obj->shipping_charge/$purchased_quantity)*$replacement_decision_obj->quantity)+($replacement_decision_obj->return_shipping_concession)."</td></tr>";
			}
			if($replacement_obj->payment_status=="paid"){
				if($replacement_decision_obj->pickup_identifier=="before replace"){
					$refund_details.=($replacement_decision_obj->balance_amount)+(($replacement_obj->shipping_charge/$purchased_quantity)*$replacement_decision_obj->quantity)+($replacement_decision_obj->return_shipping_concession)."</td></tr>";
				}else{
					$refund_details.=($replacement_decision_obj->balance_amount)+($replacement_decision_obj->quantity*$replacement_obj->return_value_each_inventory_id)+(($replacement_obj->shipping_charge/$purchased_quantity)*$replacement_decision_obj->quantity)+($replacement_decision_obj->return_shipping_concession)."</td></tr>";
				}
				
			}
			if($replacement_obj->refund_method==""){
				$refund_details.="<tr><td>Successfully refunded to</td><td>  Wallet</td></tr>";
			}
			else{
				$refund_details.="<tr><td>Successfully refunded to</td><td>  ".$replacement_obj->refund_method."</td></tr>";
			}

		}

			
		if($replacement_obj->paid_by=="customer" && $replacement_obj->payment_status=="pending"){
		
			$refund_details.="<tr><td>Balance Squared </td><td>  ".curr_sym." ".$replacement_decision_obj->balance_amount."</td></tr>";
			if($replacement_obj->paid_by=="customer" && $replacement_obj->payment_status=="pending"){
				$refund_details.="<tr><td>Not Paid by </td><td>".$replacement_obj->paid_by."</td></tr>";
			}
			if($replacement_obj->paid_by=="customer" && $replacement_obj->payment_status=="paid"){
				$refund_details.="<tr><td>Paid by </td><td>".$replacement_obj->paid_by." through". $replacement_obj->balance_amount_paid_by."</td></tr>";
			}
		
		}
		$refund_details.="</tbody></table>";
		return 	$refund_details;	
	}
	public function get_order_details_replacement($replacement_decision_obj,$replacement_obj,$order_replacement_pickup,$purchased_quantity,$product_price){
		
		$rep_details='<table class="table table-bordered" cellspacing="0" width="100%">';
		$rep_details.='<thead class="bg-info"><th colspan="2"><b> Return Order Id :</b> '.$replacement_decision_obj->return_order_id.' </th> </thead>';
		$rep_details.='<tbody>';
		$rep_details.="<tr><td>Quantity Replaced</td><td> ".$replacement_decision_obj->quantity."</td></tr>";
		$rep_details.="<tr><td>Item price of Replaced (each)</td><td>  ".curr_sym." ".$replacement_obj->replacement_value_each_inventory_id."</td></tr>";
		$rep_details.="<tr><td>Total Replacement Item Price</td><td> ".curr_sym." ".($replacement_decision_obj->quantity*$replacement_obj->replacement_value_each_inventory_id)."</td></tr>";

		
		$rep_details.="<tr><td>Differential Item Price </td><td>  ".curr_sym." ".($replacement_decision_obj->balance_amount_item_price)."</td></tr>";
		
		$rep_details.="<tr><td>Shipping charge for ".$replacement_decision_obj->quantity." </td><td>  ".curr_sym." ".($replacement_decision_obj->shipping_charge_replacement)."</td></tr>";
		
		if($replacement_decision_obj->balance_amount==0){
			$rep_details.="<tr><td>Balance Paid </td><td>  Nil</td></tr>";
		}else{
			$rep_details.="<tr><td>Balance Paid </td><td>  ".curr_sym." ".($replacement_decision_obj->balance_amount)."</td></tr>";
		}
		
		
		if($replacement_decision_obj->return_shipping_concession!=0){
			if($replacement_obj->paid_by=="admin" || $replacement_obj->paid_by==""){
				$rep_details.="<tr><td>Return Shipping Concession </td><td> ".$replacement_decision_obj->return_shipping_concession."</td></tr>";
				
				$rep_details.="<tr><td>Total Balance Amount </td><td>  ".curr_sym." ";
				$rep_details.=($replacement_decision_obj->return_shipping_concession)+$replacement_decision_obj->balance_amount."</td></tr>";
			}
		}
		
		if($replacement_obj->paid_by=="customer"){
				if($replacement_decision_obj->return_shipping_concession!=0){
					$rep_details.="<tr><td>Return Shipping Concession </td><td> ".$replacement_decision_obj->return_shipping_concession."</td></tr>";
				}
		}
		if($replacement_obj->paid_by=="admin"){
			$rep_details.="<tr><td>Payment Mode </td><td> ".$replacement_obj->refund_method."</td></tr>";
		}
		if($replacement_obj->paid_by=="customer"){
			$rep_details.="<tr><td>Payment Mode </td><td> ".$replacement_obj->balance_amount_paid_by."</td></tr>";
		}
		if(abs(($replacement_decision_obj->quantity*$replacement_obj->return_value_each_inventory_id)-(	$replacement_decision_obj->quantity*$replacement_obj->replacement_value_each_inventory_id))!=0){
			if($replacement_obj->paid_by!=''){
				$rep_details.="<tr><td>Paid by </td><td> ".$replacement_obj->paid_by."</td></tr>";
			}
		}
		$rep_details.="</tbody></table>";
		return $rep_details;
		
	}
	
	public function get_and_calculate_left_for_refund_and_replacement($refunded_details_from_returned_orders_table_arr,$sub_refunded_details_from_returned_orders_table_arr,$order_replacement_decision_data_obj,$product_price,$order_replacement_pickup,$shipping_charge,$grandtotal,$subtotal,$purchased_quantity,$get_replacement_data_obj,$orders,$promotion_available,$queryRecordsObj,$order_item_obj){
		$order_details='<table class="table table-bordered" cellspacing="0" width="100%">';
		$order_details.='<thead class="bg-info"><th colspan="2"><b>What is left with Customer</b>  </th> </thead>';
		$order_details.='<tbody>';
		$order_details.="<tr><td>Quantity Replaced</td><td> ".$replacement_decision_obj->quantity."</td></tr>";
		$quantity_filled=0;
		$itemprice_filled=0;
		$shippingprice_filled=0;
		$total_filled=0;
		if(count($refunded_details_from_returned_orders_table_arr)>0){
			$quantity_filled+=($refunded_details_from_returned_orders_table_arr['quantity']);
			$itemprice_filled+=($refunded_details_from_returned_orders_table_arr['item_with_deductions_concession']);
			$shippingprice_filled+=($refunded_details_from_returned_orders_table_arr['shipping_with_deductions_concession']);
			$total_filled+=($refunded_details_from_returned_orders_table_arr['item_with_deductions_concession']+$refunded_details_from_returned_orders_table_arr['shipping_with_deductions_concession']);
		}
		//echo $total_filled;
		
		if(count($sub_refunded_details_from_returned_orders_table_arr)>0){
			$quantity_filled+=($sub_refunded_details_from_returned_orders_table_arr['quantity']);
			$itemprice_filled+=($sub_refunded_details_from_returned_orders_table_arr['item_with_deductions_concession']);
			$shippingprice_filled+=($sub_refunded_details_from_returned_orders_table_arr['shipping_with_deductions_concession']);
			$total_filled+=($sub_refunded_details_from_returned_orders_table_arr['item_with_deductions_concession']+$sub_refunded_details_from_returned_orders_table_arr['shipping_with_deductions_concession']);
		}
		
		if(isset($order_replacement_decision_data_obj)){
			
			if(count($order_replacement_decision_data_obj)>0 && (($order_replacement_decision_data_obj->status=="replaced" || $order_replacement_decision_data_obj->status=="refund" || $order_replacement_decision_data_obj->status=="refunded")||$order_replacement_pickup=="1" && $order_replacement_decision_data_obj->refund_status=="refunded")){
				
				$quantity_filled+=$order_replacement_decision_data_obj->quantity;
				
				
				//requested_quantity_price should be
				if($queryRecordsObj->promotion_cashback>0){
				
					if($order_item_obj->status_of_refund=="refunded"){
						$replaced_value=($order_replacement_decision_data_obj->return_value_each_inventory_id*$order_replacement_decision_data_obj->quantity);
						$replaced_cashback=(($order_item_obj->amount_to_refund/$purchased_quantity)*$order_replacement_decision_data_obj->quantity);
						$itemprice_filled+=($replaced_value-$replaced_cashback);
						
					}else{
						//changed from  replacement_value_each_inventory_id
						$itemprice_filled+=$get_replacement_data_obj->return_value_each_inventory_id*$order_replacement_decision_data_obj->quantity;
					}
				}else{
					//changed from  replacement_value_each_inventory_id
					$itemprice_filled+=$get_replacement_data_obj->return_value_each_inventory_id*$order_replacement_decision_data_obj->quantity;
				}

				if($get_replacement_data_obj->shipping_charge_applied_cancels_chk==1){
					$shippingprice_filled+=($get_replacement_data_obj->shipping_charge/$purchased_quantity)*$order_replacement_decision_data_obj->quantity;

				}else{
					$shippingprice_filled+=0;
				}
				
				if($order_replacement_decision_data_obj->order_item_invoice_discount_value_each>0){
					$order_item_invoice_discount_value=($order_replacement_decision_data_obj->quantity*$order_replacement_decision_data_obj->order_item_invoice_discount_value_each);
					$itemprice_filled-=$order_item_invoice_discount_value;
				}
					
				$total_filled+=$itemprice_filled+$shippingprice_filled;
			}	
		}
		
		$quantity_left=$purchased_quantity-($quantity_filled);
		
		

		////////////////////replacement with cashback///////////////////
			
			if($queryRecordsObj->promotion_cashback>0){
				
				if($order_item_obj->status_of_refund=="refunded"){
					$amount_without_cashback=($queryRecordsObj->subtotal-$order_item_obj->amount_to_refund);

					$itemprice_left=($amount_without_cashback-$itemprice_filled);

					//$itemprice_left=($item_price_filled_cashb);
				}else{
					$itemprice_left=($subtotal)-($itemprice_filled);
				}
				
			}else{
				
				$itemprice_left=($subtotal)-($itemprice_filled);
			}

				
		////////////////////replacement with cashback///////////////////

			if(count($refunded_details_from_returned_orders_table_arr)>0){
				if($refunded_details_from_returned_orders_table_arr['order_item_invoice_discount_value_each']>0){
					$order_item_invoice_discount_value=($purchased_quantity*$refunded_details_from_returned_orders_table_arr['order_item_invoice_discount_value_each']);
					
					$itemprice_left=number_format($itemprice_left,2);
					$order_item_invoice_discount_value=number_format($order_item_invoice_discount_value,2);
					
					$itemprice_left=(double)str_replace(',','',$itemprice_left)-(double)str_replace(',','',$order_item_invoice_discount_value);
					
				}
			}elseif(count($order_replacement_decision_data_obj)>0){
				if($order_replacement_decision_data_obj->order_item_invoice_discount_value_each>0){
				$order_item_invoice_discount_value=($purchased_quantity*$order_replacement_decision_data_obj->order_item_invoice_discount_value_each);
				
					$itemprice_left=number_format($itemprice_left,2);
					$order_item_invoice_discount_value=number_format($order_item_invoice_discount_value,2);
					
					$itemprice_left=(double)str_replace(',','',$itemprice_left)-(double)str_replace(',','',$order_item_invoice_discount_value);
				}
			}
			
		///replaced item price

		$itemprice=($subtotal);//is subtotal
		
		/*for later refererance */
		//$shippingprice_left=($queryRecordsObj->shipping_charge)-($shippingprice_filled);

		$shippingprice_left=($shipping_charge)-$shippingprice_filled;
		if($queryRecordsObj->promotion_invoice_free_shipping>0){
			$shipping_charge=0;
			$shippingprice_left=0;
		}
		$total_left=$itemprice_left+$shippingprice_left;
	
		
		if($promotion_available==1){

			if($orders=="delivered_orders"){

				if($quantity_left>0 && $purchased_quantity!=$quantity_left){
					
					$order_details.="<tr><td>Quantity</td><td> ".abs($quantity_left)."</td></tr>";
					$order_details.="<tr><td>Item price</td><td> ".abs($itemprice_left)."</td></tr>";	
					$order_details.="<tr><td>Shipping Price</td><td> ".curr_sym." ".abs($shippingprice_left)."</td></tr>";
					$order_details.="<tr><td>Total</td><td> ".curr_sym." ".abs($total_left)."</td></tr>";
					
				}
			
			}else{
				$order_details.="<tr><td>Quantity </td><td> ".abs($quantity_left)."</td></tr>";
				$order_details.="<tr><td>Shipping Price</td><td> ".curr_sym." ".abs($shippingprice_left)."</td></tr>";
				$order_details.="<tr><td>Item price</td><td> ".abs($itemprice_left)."</td></tr>";	
				$order_details.="<tr><td>Total</td><td> ".curr_sym." ".abs($total_left)."</td></tr>";

			}
			
		}else{
			if($orders=="delivered_orders"){

				if($quantity_left>0 && $purchased_quantity!=$quantity_left){
					$order_details.="<tr><td>Quantity</td><td> ".abs($quantity_left)."</td></tr>";
					$order_details.="<tr><td>Item price</td><td> ".curr_sym." ".abs($itemprice_left)."</td></tr>";
					$order_details.="<tr><td>Shipping Price</td><td> ".curr_sym." ".abs($shippingprice_left)."</td></tr>";
					$order_details.="<tr><td>Total</td><td> ".curr_sym." ".abs($total_left)."</td></tr>";
				}
				
			}else{
				$order_details.="<tr><td>Quantity</td><td> ".abs($quantity_left)."</td></tr>";
				$order_details.="<tr><td>Item price</td><td> ".curr_sym." ".abs($itemprice_left)."</td></tr>";
				$order_details.="<tr><td>Shipping Price</td><td> ".curr_sym." ".abs($shippingprice_left)."</td></tr>";
				$order_details.="<tr><td>Total</td><td> ".curr_sym." ".abs($total_left)."</td></tr>";
			}
		}
				$order_details.="</tbody></table>";
		return $order_details;
	}
	public function get_and_calculate_left_for_replacement($order_replacement_decision_data_obj,$product_price,$shipping_charge,$purchased_quantity,$queryRecordsObj,$order_item_obj){

		$order_details='<table class="table table-bordered" cellspacing="0" width="100%">';
				$order_details.='<thead class="bg-info"><th colspan="2"><b>What is left with Customer</b>  </th> </thead>';
				$order_details.='<tbody>';
		$itemprice_filled=0;
		$quantity_left=$purchased_quantity-$order_replacement_decision_data_obj->quantity;

		if($queryRecordsObj->promotion_cashback>0){
			if($order_item_obj->status_of_refund=="refunded"){
				$amount_without_cashback=($queryRecordsObj->subtotal-$order_item_obj->amount_to_refund);
				$item_price_filled_cashb=0;
				if(count($order_replacement_decision_data_obj)>0){
					$replaced_value=($order_replacement_decision_data_obj->return_value_each_inventory_id*$order_replacement_decision_data_obj->quantity);
					$replaced_cashback=(($order_item_obj->amount_to_refund/$purchased_quantity)*$order_replacement_decision_data_obj->quantity);
					$item_price_filled_cashb=($replaced_value-$replaced_cashback);
					$itemprice_filled_left=($quantity_left*($order_item_obj->amount_to_refund/$purchased_quantity));
					}
					
					$itemprice_left=($amount_without_cashback-$item_price_filled_cashb);
					
			}else{
				$itemprice_left=($order_replacement_decision_data_obj->return_value_each_inventory_id*$quantity_left);
			}
			
		}else{
			$itemprice_left=($order_replacement_decision_data_obj->return_value_each_inventory_id*$quantity_left);
		}
		
		if($order_replacement_decision_data_obj->order_item_invoice_discount_value_each>0){
			$dis=($quantity_left*$order_replacement_decision_data_obj->order_item_invoice_discount_value_each);
			$itemprice_left-=$dis;
		}
		
		$shippingprice_left=$shipping_charge;

		$total_left=$itemprice_left+$shippingprice_left;
		$order_details.="<tr><td>Quantity</td><td> ".$quantity_left."</td></tr>";
		$order_details.="<tr><td>Item Price</td><td> ".curr_sym." ".$itemprice_left."</td></tr>";
		$order_details.="<tr><td>Shipping Price</td><td> ".curr_sym." ".$shippingprice_left."</td></tr>";
		$order_details.="<tr><td>Total</td><td> ".curr_sym." ".$total_left."</td></tr>";
		$order_details.="</tbody></table>";
		return $order_details;
	}
	public function get_and_calculate_left_for_refund($refunded_details_from_returned_orders_table_arr,$sub_refunded_details_from_returned_orders_table_arr,$product_price,$order_replacement_pickup,$shipping_charge,$grandtotal,$purchased_quantity,$promotion_available,$queryRecordsObj,$order_item_obj){
		$order_details='<table class="table table-bordered" cellspacing="0" width="100%">';
				$order_details.='<thead class="bg-info"><th colspan="2"><b>What is left with Customer</b>  </th> </thead>';
				$order_details.='<tbody>';
		$quantity_sub_refund=0;
		$total_item_price=0;
		if($queryRecordsObj->promotion_invoice_free_shipping>0){
			$shipping_charge=0;
			$shippingprice_left=0;
		}

		
		if($promotion_available==1){
				
			$total_item_price=(floatval($queryRecordsObj->total_price_of_product_with_promotion)+floatval($queryRecordsObj->total_price_of_product_without_promotion));
			
			if($refunded_details_from_returned_orders_table_arr["cash_back_value"]>0){
				
				if($order_item_obj->status_of_refund=="refunded"){
					$deducted_price=($total_item_price-$queryRecordsObj->cash_back_value);
					$total_item_price=$deducted_price;
				}
			}
			if($queryRecordsObj->promotion_invoice_discount>0){
				$order_item_invoice_discount_value=($purchased_quantity*$refunded_details_from_returned_orders_table_arr['order_item_invoice_discount_value_each']);
				
				$total_item_price-=$order_item_invoice_discount_value;
			}
			
			$total_with_shipping=(floatval($total_item_price)+floatval($refunded_details_from_returned_orders_table_arr['shipping_price_without_concession']));
			$grandtotal=$total_with_shipping;
		}

		if(count($sub_refunded_details_from_returned_orders_table_arr)>0){

			$quantity_left_sub_refund=($purchased_quantity)-($sub_refunded_details_from_returned_orders_table_arr['quantity']+$refunded_details_from_returned_orders_table_arr['quantity']);
			
			$shippingprice_left_sub_refund=($shipping_charge*$purchased_quantity)-($sub_refunded_details_from_returned_orders_table_arr['shipping_with_deductions_concession']+$refunded_details_from_returned_orders_table_arr['shipping_with_deductions_concession']);
				
			$total_left_sub_refund=($grandtotal)-($sub_refunded_details_from_returned_orders_table_arr['item_with_deductions_concession']+$sub_refunded_details_from_returned_orders_table_arr['shipping_with_deductions_concession']+$refunded_details_from_returned_orders_table_arr['item_with_deductions_concession']+$refunded_details_from_returned_orders_table_arr['shipping_with_deductions_concession']);
			
			
			$order_details.="<tr><td>Quantity : ".$quantity_left_sub_refund."</td></tr>";
			if($promotion_available==0){
				
				$itemprice_left_sub_refund=($product_price*$purchased_quantity)-($sub_refunded_details_from_returned_orders_table_arr['item_with_deductions_concession']+$refunded_details_from_returned_orders_table_arr['item_with_deductions_concession']);
				$order_details.="<tr><td>Item Price</td><td> ".curr_sym." ".$itemprice_left_sub_refund."</td></tr>";
				
				$order_details.="<tr><td>Shipping Price</td><td> ".curr_sym." ".$shippingprice_left_sub_refund."</td></tr>";
				$order_details.="<tr><td>Total</td><td> ".curr_sym." ".abs($total_left_sub_refund)."</td></tr>";
			}else{
				$deduction=floatval($sub_refunded_details_from_returned_orders_table_arr['item_with_deductions_concession']+$refunded_details_from_returned_orders_table_arr['item_with_deductions_concession']);
				$itemprice_left_sub_refund=($total_item_price-$deduction);
				if($queryRecordsObj->promotion_surprise_gift_type==curr_code){
					if($quantity_left_sub_refund==0){
						$itemprice_left_sub_refund-=intval($queryRecordsObj->promotion_surprise_gift);
					}
				}
				$order_details.="<tr><td>Item Price</td><td> ".curr_sym." ".abs(round($itemprice_left_sub_refund,2))."</td></tr>";
				$order_details.="<tr><td>Shipping Price</td><td> ".curr_sym." ".abs(round($shippingprice_left_sub_refund))."</td></tr>";
				$order_details.="<tr><td>Total</td><td> ".curr_sym." ".abs(round((round($itemprice_left_sub_refund,2)+round($shippingprice_left_sub_refund))))."</td></tr>";
			}
			
				$order_item_id=$queryRecordsObj->order_item_id;
				$promotion_item=$queryRecordsObj->promotion_item;
				$promotion_item_num=$queryRecordsObj->promotion_item_num;
				$promotion_surprise_gift_skus=$queryRecordsObj->promotion_surprise_gift_skus;
				$promotion_surprise_gift_skus_nums=$queryRecordsObj->promotion_surprise_gift_skus_nums;
				$promotion_surprise_gift=$queryRecordsObj->promotion_surprise_gift;
				$promotion_surprise_gift_type=$queryRecordsObj->promotion_surprise_gift_type;
				
				$str_promo='';
				if($promotion_item!='' || ($promotion_surprise_gift_skus!='')){
								
						
						$quantity=$quantity_left_sub_refund;
						$promotion_minimum_quantity=$queryRecordsObj->promotion_minimum_quantity;
						
						$promo_item=rtrim($promotion_item,',');
						$promotion_item_num=rtrim($promotion_item_num,',');
						
						$promo_item_arr = explode(',', $promo_item);
						$promo_item_num_arr = explode(',', $promotion_item_num);
						
						$two=array_combine($promo_item_arr,$promo_item_num_arr);
						
						$temp_quotient=intval($quantity/$promotion_minimum_quantity);
						
						//Surprise
						
						$promotion_surprise_gift_skus=rtrim($promotion_surprise_gift_skus,',');
						$promotion_surprise_gift_skus_nums=rtrim($promotion_surprise_gift_skus_nums,',');
						
						$gift_skus_arr = explode(',', $promotion_surprise_gift_skus);
						$gift_skus_nums_arr = explode(',', $promotion_surprise_gift_skus_nums);
						
						$gift_with_count=array_combine($gift_skus_arr,$gift_skus_nums_arr);
						if($promotion_item!=''){
							$free_items_arr=$this->Customer_account->get_free_skus_from_inventory($promotion_item,$promotion_item_num);
						}
						if($promotion_surprise_gift_type=='Qty'){
							$free_surprise_items=$this->Customer_account->get_free_skus_from_inventory($promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums);
						}
				}

				if($promotion_item!='' || ($promotion_surprise_gift_skus!='')){
					$str_promo.='<table class="table table-bordered" style="font-size: .8em;text-align:justify;margin-bottom: 0px;"><tbody>';
					$str_promo.='<tr><td colspan="3" class="text-center"><b>Free items for Quantity Left ('.$quantity.')</b></td></tr>';
					if($promotion_item!='' && $temp_quotient>=1){

						foreach($free_items_arr as $free_items){
							$id=$free_items->id;
							$str_promo.='<tr><td>SKU :'.$free_items->sku_id.' (free)</td><td>Quantity '.($temp_quotient*$two[$id]).'</td><td class="text-right"><del>'.curr_sym." ".(($temp_quotient*$two[$id])*$free_items->selling_price).'</del></td></tr>';
						}
					}

					if($promotion_surprise_gift_type=='Qty' && $temp_quotient>=1){

						foreach($free_surprise_items as $surprise_items){
							$id=$surprise_items->id;
							$str_promo.='<tr><td>SKU : '.$surprise_items->sku_id.' (free)</td><td>Quantity '.($gift_with_count[$id]).'</td><td class="text-right"><del>'.curr_sym." ".(($gift_with_count[$id])*$surprise_items->selling_price).'</del></td></tr>';

						}
					}
					$str_promo.='</tbody></table>';
				}
			///to show the free items left with customer
				$order_details.="<tr><td>Promotion Details</td><td>".$str_promo."</td></tr>";
			
			
		}else{
			
			
			
			if(count($refunded_details_from_returned_orders_table_arr)>0){
	
				$quantity_left=($purchased_quantity)-($refunded_details_from_returned_orders_table_arr['quantity']);
	
				$shippingprice_left=($shipping_charge*$purchased_quantity)-($refunded_details_from_returned_orders_table_arr['shipping_with_deductions_concession']);
				$total_left=($grandtotal)-($refunded_details_from_returned_orders_table_arr['item_with_deductions_concession']+$refunded_details_from_returned_orders_table_arr['shipping_with_deductions_concession']);
				
				$order_details.="<tr><td>Quantity</td><td> ".$quantity_left."</td></tr>";
				if($promotion_available==0){
					
					$itemprice_left=($product_price*$purchased_quantity)-($refunded_details_from_returned_orders_table_arr['item_with_deductions_concession']);
					
					$order_details.="<tr><td>Item Price</td><td> ".curr_sym." ".$itemprice_left."</td></tr>";
					$order_details.="<tr><td>Shipping Price</td><td> ".curr_sym." ".$shippingprice_left."</td></tr>";
					$order_details.="<tr><td>Total</td><td> ".curr_sym." ".$total_left."</td></tr>";
				}else{
					
					$itemprice_left=($total_item_price-$refunded_details_from_returned_orders_table_arr['item_with_deductions_concession']);
					
					if($queryRecordsObj->promotion_surprise_gift_type==curr_code){
						if($quantity_left==0){
							$itemprice_left-=intval($queryRecordsObj->promotion_surprise_gift);
						}
					}
					$order_details.="<tr><td>Item Price</td><td> ".curr_sym." ".$itemprice_left."</td></tr>";
					$order_details.="<tr><td>Shipping Price</td><td> ".curr_sym." ".round($shippingprice_left)."</td></tr>";
					$order_details.="<tr><td>Total</td><td> ".curr_sym." ".abs(round($itemprice_left,2)+round($shippingprice_left))."</td></tr>";
				}

				
				$order_item_id=$queryRecordsObj->order_item_id;
				$promotion_item=$queryRecordsObj->promotion_item;
				$promotion_item_num=$queryRecordsObj->promotion_item_num;
				$promotion_surprise_gift_skus=$queryRecordsObj->promotion_surprise_gift_skus;
				$promotion_surprise_gift_skus_nums=$queryRecordsObj->promotion_surprise_gift_skus_nums;
				$promotion_surprise_gift=$queryRecordsObj->promotion_surprise_gift;
				$promotion_surprise_gift_type=$queryRecordsObj->promotion_surprise_gift_type;
				
				$str_promo='';
				if($promotion_item!='' || ($promotion_surprise_gift_skus!='')){
								
						//$quantity=$queryRecordsObj->quantity;
						$quantity=$quantity_left;
						$promotion_minimum_quantity=$queryRecordsObj->promotion_minimum_quantity;
						
						$promo_item=rtrim($promotion_item,',');
						$promotion_item_num=rtrim($promotion_item_num,',');
						
						$promo_item_arr = explode(',', $promo_item);
						$promo_item_num_arr = explode(',', $promotion_item_num);
						
						$two=array_combine($promo_item_arr,$promo_item_num_arr);
						
						$temp_quotient=intval($quantity/$promotion_minimum_quantity);
						
						//Surprise
						
						$promotion_surprise_gift_skus=rtrim($promotion_surprise_gift_skus,',');
						$promotion_surprise_gift_skus_nums=rtrim($promotion_surprise_gift_skus_nums,',');
						
						$gift_skus_arr = explode(',', $promotion_surprise_gift_skus);
						$gift_skus_nums_arr = explode(',', $promotion_surprise_gift_skus_nums);
						
						$gift_with_count=array_combine($gift_skus_arr,$gift_skus_nums_arr);
						if($promotion_item!=''){
							$free_items_arr=$this->Customer_account->get_free_skus_from_inventory($promotion_item,$promotion_item_num);
						}
						if($promotion_surprise_gift_type=='Qty'){
							$free_surprise_items=$this->Customer_account->get_free_skus_from_inventory($promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums);
						}
				}

				if($promotion_item!='' || ($promotion_surprise_gift_skus!='')){
					$str_promo.='<table class="table table-bordered" style="font-size: .8em;text-align:justify;margin-bottom: 0px;"><tbody>';
					$str_promo.='<tr><td colspan="3" class="text-center"><b>Free items for Quantity Left ('.$quantity.')</b></td></tr>';
					if($promotion_item!='' && $temp_quotient>=1){

						foreach($free_items_arr as $free_items){
							$id=$free_items->id;
							$str_promo.='<tr><td>SKU :'.$free_items->sku_id.' (free)</td><td>Quantity '.($temp_quotient*$two[$id]).'</td><td class="text-right"><del>'.curr_sym." ".(($temp_quotient*$two[$id])*$free_items->selling_price).'</del></td></tr>';
						}
					}

					if($promotion_surprise_gift_type=='Qty' && $temp_quotient>=1){

						foreach($free_surprise_items as $surprise_items){
							$id=$surprise_items->id;
							$str_promo.='<tr><td>SKU : '.$surprise_items->sku_id.' (free)</td><td>Quantity '.($gift_with_count[$id]).'</td><td class="text-right"><del>'.curr_sym." ".(($gift_with_count[$id])*$surprise_items->selling_price).'</del></td></tr>';

						}
					}
					$str_promo.='</tbody></table>';
				}
			///to show the free items left with customer
				$order_details.="<tr><td>Promotion details</td><td>".$str_promo."</td></tr>";
			}
		}
	
			
		$order_details.="</tbody></table>";
		return $order_details;
	}
	////////////////////////edited/////////delivered Orders Processing/////////
	
	
	public function get_refunded_details_from_returned_orders_table($order_item_id){
		$refunded_details_from_returned_orders_table_arr=$this->Model_orders->get_refunded_details_from_returned_orders_table($order_item_id);
		return $refunded_details_from_returned_orders_table_arr;
	}
	
	public function get_sub_refunded_details_from_returned_orders_table($order_item_id){
		$sub_refunded_details_from_returned_orders_table_arr=$this->Model_orders->get_sub_refunded_details_from_returned_orders_table($order_item_id);
		return $sub_refunded_details_from_returned_orders_table_arr;
	}
	
	
	//////////////////
	
	public function set_as_cancelled(){
		
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$list_to_be_cancelled=$this->input->post("list_to_be_cancelled");
			
			$cancel_reason_id=$this->input->post("cancel_reason");
			$reason_for_cancel_order=$this->input->post("reason_for_cancel_order");
			$update_inventory_stock=$this->input->post("update_inventory_stock");
			$send_notification=$this->input->post("send_notification");
			
			$list_to_be_cancelled_arr=explode("-",$list_to_be_cancelled);
			$list_to_be_cancelled_order_item_arr=array();
			foreach($list_to_be_cancelled_arr as $orders_status_id){
				$list_to_be_cancelled_order_item_arr[]=$this->Model_orders->get_order_item_id_by_orders_status_id($orders_status_id);
			}
			//exit;
			foreach($list_to_be_cancelled_order_item_arr as $order_item_id){
			
			/*first add order item to cancels*/
			
			$user_type=$this->session->userdata("user_type");
			
			$refund_method="";
			$refund_bank_id="";
			$add_cancel_order=$this->Model_orders->add_cancel_orders_to_cancels_table($order_item_id,$cancel_reason_id,$reason_for_cancel_order,$update_inventory_stock,"admin",$refund_method,$refund_bank_id);
			
			if($update_inventory_stock=="yes"){
				
				$cancel_reas=$this->Model_orders->get_cancel_reason_to_check($cancel_reason_id);
				if($cancel_reas!="No Inventory"){
					$reset_stock=1;
				}else{
					$reset_stock=0;
				}
			}else{
				$reset_stock=0;
			}
						
			if($add_cancel_order){ /*if cancels table is added*/
						$order_active_data=$this->Model_orders->get_all_order_data_from_order_item($order_item_id);/*fetch data from active order table based on order_item_id*/
						$status_view="cancelled_status_view";
						$flag_update=$this->Model_orders->update_notification_to_send_cancel($order_active_data,$send_notification,$status_view);
						
					foreach($order_active_data as $data){
						$purchased_price=$data['purchased_price'];
						$prev_order_item_id=$data['prev_order_item_id'];
						$order_item_id=$data['order_item_id'];
						$customer_id=$data['customer_id'];
						$order_id=$data['order_id'];
						$shipping_address_id=$data['shipping_address_id'];
						$inventory_id=$data['inventory_id'];
						$product_id=$data['product_id'];
						$sku_id=$data['sku_id'];
						$tax_percent=$data['tax_percent'];
						$product_price=$data['product_price'];
						$shipping_charge=$data['shipping_charge'];
						$subtotal=$data['subtotal'];
						$wallet_status=$data['wallet_status'];
						$wallet_amount=$data['wallet_amount'];
						$grandtotal=$data['grandtotal'];
						$quantity=$data['quantity'];
						$timestamp=$data['timestamp'];
						$image=$data['image'];
						$random_number=$data['random_number'];
						$invoice_email=$data['invoice_email'];
						$invoice_number=$data['invoice_number'];
						$logistics_name=$data['logistics_name'];
						$logistics_weblink=$data['logistics_weblink'];
						$tracking_number=$data['tracking_number'];
						$expected_delivery_date=$data['expected_delivery_date'];
						$delivery_mode=$data['delivery_mode'];
						$parcel_category=$data['parcel_category'];

						$vendor_id=$data['vendor_id'];
						$payment_type=$data['payment_type'];
						$payment_status=$data['payment_status'];
						$return_period=$data['return_period'];
						
						$logistics_id=$data['logistics_id'];
						$logistics_delivery_mode_id=$data['logistics_delivery_mode_id'];
						$logistics_territory_id=$data['logistics_territory_id'];
						$logistics_parcel_category_id=$data['logistics_parcel_category_id'];

						$promotion_available=$data['promotion_available'];
						$promotion_residual=$data['promotion_residual'];
						$promotion_discount=$data['promotion_discount'];
						$promotion_cashback=$data['promotion_cashback'];
						$promotion_surprise_gift=$data['promotion_surprise_gift'];
						$promotion_surprise_gift_type=$data['promotion_surprise_gift_type'];
						$promotion_surprise_gift_skus=$data['promotion_surprise_gift_skus'];
						$promotion_surprise_gift_skus_nums=$data['promotion_surprise_gift_skus_nums'];
						$default_discount=$data['default_discount'];
						$promotion_item_multiplier=$data['promotion_item_multiplier'];
						$promotion_item=$data['promotion_item'];
						$promotion_item_num=$data['promotion_item_num'];
						$promotion_clubed_with_default_discount=$data['promotion_clubed_with_default_discount'];
						$promotion_id_selected=$data['promotion_id_selected'];
						$promotion_type_selected=$data['promotion_type_selected'];
						$promotion_minimum_quantity=$data['promotion_minimum_quantity'];
						$promotion_quote=$data['promotion_quote'];
						$promotion_default_discount_promo=$data['promotion_default_discount_promo'];
						$individual_price_of_product_with_promotion=$data['individual_price_of_product_with_promotion'];
						$individual_price_of_product_without_promotion=$data['individual_price_of_product_without_promotion'];
						$total_price_of_product_without_promotion=$data['total_price_of_product_without_promotion'];
						$total_price_of_product_with_promotion=$data['total_price_of_product_with_promotion'];
						$quantity_without_promotion=$data['quantity_without_promotion'];
						$quantity_with_promotion=$data['quantity_with_promotion'];
						$cash_back_value=$data['cash_back_value'];
                        $razorpayOrderId=$data['razorpayOrderId'];
                        $razorpayPaymentId=$data['razorpayPaymentId'];
						//invoice details
						$promotion_invoice_cash_back=$data['promotion_invoice_cash_back'];
						$invoice_surprise_gift=$data['invoice_surprise_gift'];
						$total_price_without_shipping_price=$data['total_price_without_shipping_price'];
						$promotion_invoice_discount=$data['promotion_invoice_discount'];
                                                $ord_max_selling_price=$data['ord_max_selling_price'];
						$ord_selling_discount=$data['ord_selling_discount'];
						$ord_tax_percent_price=$data['ord_tax_percent_price'];
						$ord_taxable_price=$data['ord_taxable_price'];
						$ord_sku_name=$data['ord_sku_name'];
                                                
						/* coupon */

						$ord_coupon_code=$data['ord_coupon_code'];
						$ord_coupon_name=$data['ord_coupon_name'];
						$ord_coupon_type=$data['ord_coupon_type'];
						$ord_coupon_value=$data['ord_coupon_value'];
						$ord_coupon_reduce_price=$data['ord_coupon_reduce_price'];
						$ord_coupon_status=$data['ord_coupon_status'];
                                                
						/* coupon */

						/* addon products details */
						$ord_addon_products_status=$data['ord_addon_products_status'];
						$ord_addon_products=$data['ord_addon_products'];
						$ord_addon_inventories=$data['ord_addon_inventories'];
						$ord_addon_total_price=$data['ord_addon_total_price'];
						
						$ord_addon_single_or_multiple_tagged_inventories_in_frontend=$data['ord_addon_single_or_multiple_tagged_inventories_in_frontend'];
						$ord_addon_master_order_item_id=$data['ord_addon_master_order_item_id'];
						
						
						
						$product_vendor_id=$data['product_vendor_id'];
						/* addon products details */
						//,$ord_addon_products_status,$ord_addon_products,$ord_addon_inventories,$ord_addon_total_price
						
					}
					
						if($promotion_invoice_discount>0){
							
							$invoice_discount=($subtotal/$total_price_without_shipping_price)*100;					
							$each_invoice_amount=$promotion_invoice_discount*$invoice_discount/100;
							//$invoice_amount=round($promotion_invoice_discount-$each_invoice_amount,2);
							$order_item_invoice_discount_value=round($each_invoice_amount,2);
							$order_item_invoice_discount_value_each=round($order_item_invoice_discount_value/$quantity,2);
						}else{
							$order_item_invoice_discount_value=0;
							$order_item_invoice_discount_value_each=0;
						}
					
					/* move above data to cancelled_orders table*/
					
					if($reset_stock==1){
						
						$reset_stock_flag=$this->Model_orders->add_stocks_of_inventory($inventory_id,$quantity,$promotion_available,$promotion_item,$promotion_item_num,$promotion_minimum_quantity,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$ord_addon_products_status,$ord_addon_inventories);
					}
					
					$add_to_cancel_orders=$this->Model_orders->active_order_add_to_cancel_orders($purchased_price,$prev_order_item_id,$order_item_id,$customer_id,$order_id,$shipping_address_id,$inventory_id,$product_id,$sku_id,$tax_percent,$product_price,$shipping_charge,$subtotal,$wallet_status,$wallet_amount,$grandtotal,$quantity,$timestamp,$image,$random_number,$invoice_email,$invoice_number,$logistics_name,$logistics_weblink,$tracking_number,$expected_delivery_date,$delivery_mode,$parcel_category,$vendor_id,$payment_type,$payment_status,$return_period,$logistics_id,$logistics_delivery_mode_id,$logistics_territory_id,$logistics_parcel_category_id,$promotion_available,$promotion_residual,$promotion_discount,$promotion_cashback,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$default_discount,$promotion_item_multiplier,$promotion_item,$promotion_item_num,$promotion_clubed_with_default_discount,$promotion_id_selected,$promotion_type_selected,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_without_promotion,$total_price_of_product_with_promotion,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$order_item_invoice_discount_value,$razorpayOrderId,$razorpayPaymentId,$ord_max_selling_price,$ord_selling_discount,$ord_tax_percent_price,$ord_taxable_price,$ord_sku_name,$ord_coupon_code,$ord_coupon_name,$ord_coupon_type,$ord_coupon_value,$ord_coupon_reduce_price,$ord_coupon_status,$ord_addon_products_status,$ord_addon_products,$ord_addon_inventories,$ord_addon_total_price,$ord_addon_single_or_multiple_tagged_inventories_in_frontend,$ord_addon_master_order_item_id,$product_vendor_id);
					
					
					if($add_to_cancel_orders){
						$status="reject";
						if($promotion_invoice_cash_back>0){	
							$flag_cashback=$this->Model_orders->update_customer_response_for_invoice_cashback($order_id,$status,$customer_id,$total_amount_cash_back);		
						}
						if($invoice_surprise_gift!=''){
							$flag_sg=$this->Model_orders->update_customer_status_surprise_gifts_on_invoice($order_id,$status,$customer_id);
						}
	
						$cancel_id=$this->Model_orders->get_cancel_id_of_this_transaction($order_item_id);/*get cancel_id from cancels table*/
						
						$wallet_exists=$this->Model_orders->get_wallet_exists_for_customer($customer_id);
						/*add this transaction to refund table*/
						$return_id="";/*this is cancel order case*/
						$refund_method="";
						
						$update_order_item_id_to_cancel=$this->Model_orders->update_order_item_id_to_cancel($order_item_id);/*changing order_item to cancelled*/

						if($update_order_item_id_to_cancel){
							$flag=$this->Model_orders->remove_data_from_active_order_table($order_item_id);
						}
					}
				}
			}
			
			////////////////////
			$flag=$this->Model_orders->set_as_cancelled($list_to_be_cancelled,$reason_for_cancel_order);
			
			
			if($flag==true){
				/////////////////////////////////////////////////////////////
			
				$send_sms=$this->input->post("sms");
				$send_email=$this->input->post("email");
				if($send_sms==1 || $send_email==1){
					foreach($list_to_be_cancelled_order_item_arr as $order_item_id){
						
						$order_summary_mail_data_arr=$this->Model_orders->get_order_summary_mail_data($order_item_id);
						
						foreach($order_summary_mail_data_arr as $order_summary_mail_data){
							
						$email=$this->Model_orders->get_email_db($order_summary_mail_data['customer_id']);
						$mobile=$this->Model_orders->get_mobile_db($order_summary_mail_data['customer_id']);
						
						if($mobile != '' && $send_sms==1){
							$this->send_sms_in_order_process($order_summary_mail_data,$mobile,"cancelled");
						}//sms
				
						if($email!='' && $send_email==1){
							
							$this->send_email_in_order_process($order_summary_mail_data,$email,"cancelled");
						}//mail	
		
						}
					}
				}
				/////////////////////////////////////////////////////////////
			}
			echo $flag;
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	/////////////////////////
	///////////////////////
	public function cancelled_orders(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$this->Model_orders->notify_cancel_orders_unset();
			//$data["menu_flag"]="cancels_active_links";
			$data["menu_flag"]="active_order_active_links";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/cancelled_orders',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function cancelled_orders_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_orders->cancelled_orders_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_orders->cancelled_orders_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				
				$order_summary='<ul class="list-unstyled small">';
				//if($queryRecordsObj->ord_addon_products_status=='1'){
				if(0){
					$combo_prod=$this->get_addon_data($queryRecordsObj);
					$order_summary.='<li>'.$combo_prod.'</li>';
					$row[]="Combo products";
					$order_summary.="<li>SKU: ".$queryRecordsObj->sku_id."</li>";
                                if($queryRecordsObj->ord_sku_name!=''){
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
                                }else{
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
                                }
				}else{
					$inventory_id=$queryRecordsObj->inventory_id;
					$image=$queryRecordsObj->image;
					$img_path=get_inventory_image_path($inventory_id,$image);

					$row[]="<img src='".base_url().$img_path."'  style='width: 100px;height: 122px;' >";
						$order_summary.="<li>SKU: ".$queryRecordsObj->sku_id."</li>";
                                if($queryRecordsObj->ord_sku_name!=''){
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
                                }else{
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
                                }
								$product_details=get_product_details($queryRecordsObj->product_id);
								if(!empty($product_details))
								$order_summary.=($product_details->product_description!='') ? "<li class='mt-10'><small>".$product_details->product_description.'</small></li>': '';
								
				}
				$order_summary.="<li>Order Date: ".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."</li>";
				$order_summary.="<li>Order ID: ".$queryRecordsObj->order_id.' <i  style="cursor:pointer" class="fa fa-question-circle orderid_details_popover" data-trigger="focus" id="orderid_'.$queryRecordsObj->order_id."_".$queryRecordsObj->order_item_id.'" aria-hidden="true"></i>'."</li>";
				$order_summary.="<li>Order Item ID: ".$queryRecordsObj->order_item_id."</li>";
				if($queryRecordsObj->type_of_order=="replaced"){
					$order_summary.="<li>[<span class='text-info bold'>Replaced Order</span>]</li>";
				}
				
				$prev_order_item_id=$queryRecordsObj->prev_order_item_id;
				
				if($queryRecordsObj->prev_order_item_id!=""){
					
					$orders_status_data_obj=$this->Model_orders->get_orders_status_data($queryRecordsObj->prev_order_item_id);
					
					$replacement_desired_data_obj=$this->Model_orders->get_replacement_desired_data($queryRecordsObj->prev_order_item_id);
					
					$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->prev_order_item_id);
					
					if($order_replacement_decision_data_obj->pickup_identifier=="after replace"){
						$order_summary.="<li>[<span class='text-info bold'>Pickup has been completed</span>]</li>";
					}
					if($order_replacement_decision_data_obj->pickup_identifier=="before replace"){
						$replacement_option_status='';
						if($orders_status_data_obj->initiate_replacement_pickup =='1' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='[Return the Item]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='1' && $orders_status_data_obj->order_replacement_pickup==1){
							$replacement_option_status='[Item Had been Collected Successfully]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='0' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='[Initiate Pickup has been rejected by admin]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='';
						}
						$order_summary.="<li>".'<span class="text-info bold">'.$replacement_option_status.'</span>'."</li>";
					}
				}
				
				
				
				/////////////////added - promotion////////////////////
					
				if($queryRecordsObj->type_of_order=="replaced"){
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion_for_replaced_order($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal,$prev_order_item_id);
			   
				}else{
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal);
				}
				$order_summary.='</ul>';
				$order_summary.=$str_promo;
		   
				
				/////////////////added - promotion////////////////////
				
				$addon_str='';

				if($queryRecordsObj->ord_addon_master_order_item_id!=''){

					$sku_id=get_main_sku($queryRecordsObj->ord_addon_master_order_item_id);
					$addon_str.='<b class="text-primary"><small>AddOn SKU (Main SKU Id:'.$sku_id.')</small></b>';
				}else{
					$addon_str.='<b class="text-primary"><small>Main SKU</small></b>';
				}
				/* get product vendor details */
				if($queryRecordsObj->product_vendor_id!=''){
					$p_v_data=get_product_vendor_details($queryRecordsObj->product_vendor_id);
					if(!empty($p_v_data)){
						$addon_str.='<br><br><div><small><u>Product Vendor</u></small></div>';
						$addon_str.='<div><b class="text-danger"><small>'.$p_v_data->name.'</small></b></div>';
						$addon_str.='<div class="text-danger"><small>'.$p_v_data->email.'</small></div>';
					}
				}
				/* get product vendor details */
				$row[]=$order_summary.$addon_str;

				$status_of_transaction=$this->get_status_of_transaction($queryRecordsObj->order_item_id);
				$cancels_tableObj=$this->Model_orders->get_data_cancels_table($queryRecordsObj->order_item_id);
				$cancellation_details="";
				$cancellation_details.='<ul class="list-unstyled small">';
				$cancellation_details.="<li>Cancelled By: ".$cancels_tableObj->cancelled_by."</li>";
				
				if(strlen($cancels_tableObj->cancel_reason_comments)>200){
					$cancellation_details.='<li>Comments:<div class="f-row margin-top more">'.str_split($cancels_tableObj->cancel_reason_comments,200)[0].'<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' .substr($cancels_tableObj->cancel_reason_comments,201). '</span>&nbsp;&nbsp;<a href="#" class="morelink" onclick="morelinkfun(this)">more</a></span></div></li>';
				}else{
					$cancellation_details.='<li>Comments:<div class="f-row margin-top more">'.$cancels_tableObj->cancel_reason_comments.'</div></li>';
				}
				
				if($status_of_transaction->refund_type==""){
					$payment_type_from_cancelled_orders=$this->get_cancelled_order_customer_payment_type($queryRecordsObj->order_item_id);
					if(strtolower($payment_type_from_cancelled_orders)=='cod'){
						$cancellation_details.="<li>Customer Payment Method:COD</li>";
					}else{
						$cancellation_details.="<li>Refund Method: Not Yet Given</li>";
					}
				}
				else{
					$cancellation_details.="<li>Refund Method: ".$status_of_transaction->refund_type.'</li>';

                    if($status_of_transaction->refund_type=='Razorpay'){
                        $arr=razorpay_ids($queryRecordsObj->order_id);
                        if(!empty($arr)){
                            $cancellation_details.="<li>Razorpay Order ID: ".$arr['razorpayOrderId']."</li>";
                            $cancellation_details.="<li>Razorpay Payment ID: ".$arr['razorpayPaymentId']."</li>";
                        }
                    }
				}
				$cancellation_details.='</ul>';
				$row[]=$cancellation_details;
				$order_quantity_price='<ul class="list-unstyled small">';
				
				if($queryRecordsObj->type_of_order=="replaced"){
					//$replaced_item_price_individual=$this->Model_orders->get_replaced_item_price_individual($prev_order_item_id);

					$replaced_item_price_individual=$queryRecordsObj->product_price;					
					$order_quantity_price.="<li>Qty:".$queryRecordsObj->quantity."</li>";				
					$order_quantity_price.="<li>Price of replacement order (each): ".curr_sym." ".$replaced_item_price_individual."</li>";
					$order_quantity_price.="<li>Shipping charge for replacement:  ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
					$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
				}else{
					$order_quantity_price.="<li>Qty:".$queryRecordsObj->quantity."</li>";
					$order_quantity_price.="<li>Total Item Price:</b>".$queryRecordsObj->subtotal."</li>";
					if($queryRecordsObj->promotion_invoice_free_shipping>0){
						$order_quantity_price.="<li>Shipping charge (<span style='color:red;'>waived off</span>): ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
						$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->subtotal."</li>";
					}else{
						$order_quantity_price.="<li>Shipping charge: ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
						$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
					}	
					
					$order_quantity_price.='<li><a class="text-info bold" href="#" onclick="view_order_summary_(\''.$queryRecordsObj->order_id.'\')" id="view_order_summary_'.$queryRecordsObj->order_item_id.'">View Invoice Data ';
					if($queryRecordsObj->invoice_offer_achieved==1){
						$invoice_ofr_count=$this->get_invoice_offers_str($queryRecordsObj);
						$order_quantity_price.='<span class="badge" style="color: red;"> '.$invoice_ofr_count.'</span>';
					}
					$order_quantity_price.='</a></li>';
				}

				$order_quantity_price.='</ul>';
				$row[]=$order_quantity_price;
				$customer_add='<ul class="list-unstyled small">';
				$customer_add.="<li>".$queryRecordsObj->customer_name."</li>";
				$customer_add.="<li>".$queryRecordsObj->address1."</li>";
				$customer_add.="<li>".$queryRecordsObj->address2."</li>";
				$customer_add.="<li>".$queryRecordsObj->city.",".$queryRecordsObj->state."</li>";
				$customer_add.="<li>".$queryRecordsObj->country."</li>";
				$customer_add.="<li>".$queryRecordsObj->pincode."</li>";
				$customer_add.="<li>".$queryRecordsObj->mobile."</li>";
				$customer_add.='</ul>';
				$row[]=$customer_add;
				$actions="";
				
				$actions.='<ul class="list-unstyled small">';
                $payment_type_from_cancelled_orders=$this->get_cancelled_order_customer_payment_type($queryRecordsObj->order_item_id);
				if($status_of_transaction->refund_type==""){

					if(strtolower($payment_type_from_cancelled_orders)=='cod'){
						$actions.="<li>No refund is needed. Customer didn't pay money for the purchase because the payment type is COD</li>";
						$actions.="<li>No details</li>";
					}else{
						$actions.='<li>Awaiting</li>';
						$actions.='<li><a class="text-info bold" href="#" onclick="open_refund_type_modal('.$queryRecordsObj->order_item_id.')">Transfer to Wallet</a></li>';
						$actions.="<li>No details</li>";
					}
				}else{

					if($status_of_transaction->status==""){
						$actions.="<li>No details</li>";
						$actions.='<li><a class="text-info bold" href="#" onclick="open_cancel_refund_issue_fun('.$queryRecordsObj->order_item_id.')">Issue Refund</a></li>';
					}
					if($status_of_transaction->status=="refund"){
						$actions.="<li>No details</li>";
						$actions.='<li>Processed</li>';
					}
					if($status_of_transaction->status=="refunded"){
						
						$actions.='<li>Refund Completed</li>';
						
						$actions.="<li>Transaction ID:".$queryRecordsObj->transaction_id."</li>";
						$actions.="<li>Transaction Date:".date("D j M, Y",strtotime($queryRecordsObj->transaction_date))."</li>";
						$actions.="<li>Transaction Time:".$queryRecordsObj->transaction_time."</li>";
					}
					if($status_of_transaction->status=="not refunded"){
					
						
						if(strlen($queryRecordsObj->refund_fail_comments)>200){
							$actions.='<li>Comments:<div class="f-row margin-top more">'.str_split($queryRecordsObj->refund_fail_comments,200)[0].'<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' .substr($queryRecordsObj->refund_fail_comments,201). '</span>&nbsp;&nbsp;<a href="#" class="morelink" onclick="morelinkfun(this)">more</a></span></div></li>';
						}else{
							$actions.='<li>Comments:<div class="f-row margin-top more">'.$queryRecordsObj->refund_fail_comments.'</div></li>';
						}
						
						$actions.="<li>Reg Bank Details</li>";
						if($queryRecordsObj->allow_update_bank_details=="updated"){
							$actions.="<li>Bank details has been updated by customer</li>";
						}
						if($queryRecordsObj->allow_update_bank_details=="yes"){
							$actions.="<li>Bank details need to be update by customer</li>";
						}
						if($queryRecordsObj->allow_update_bank_details=="no"){
							$actions.="<li>Bank details no need to update by customer</li>";
						}
						$actions.='<li><a class="text-info bold" href="#" onclick="resend_cancel_refund_fun('.$queryRecordsObj->order_item_id.')">Resend</a></li>';
						$actions.='<li><a class="text-info bold" href="#" onclick="open_refund_type_modal('.$queryRecordsObj->order_item_id.')">Transfer to Wallet</a></li>';
					}
				}
				
				$actions.='</ul>';
				
				$row[]=$actions;
				$data[]=$row;
				$row=array();
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function cancelled_orders_inactive(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$this->Model_orders->notify_cancel_orders_unset();
			$data["menu_flag"]="inactive_order_active_links";
			//$data["menu_flag"]="cancels_inactive_links";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/cancelled_orders_inactive',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function cancelled_orders_inactive_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_orders->cancelled_orders_inactive_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_orders->cancelled_orders_inactive_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				
				$order_summary='<ul class="list-unstyled small">';
				//if($queryRecordsObj->ord_addon_products_status=='1'){
				if(0){
					$combo_prod=$this->get_addon_data($queryRecordsObj);
					$order_summary.='<li>'.$combo_prod.'</li>';
					$row[]="Combo products";
					$order_summary.="<li>SKU:".$queryRecordsObj->sku_id."</li>";
					if($queryRecordsObj->ord_sku_name!=''){
						$order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
					}else{
						$order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
					}
				}else{
					$inventory_id=$queryRecordsObj->inventory_id;
					$image=$queryRecordsObj->image;
					$img_path=get_inventory_image_path($inventory_id,$image);

					$row[]="<img src='".base_url().$img_path."'  style='width: 100px;height: 122px;' >";
					$order_summary.="<li>SKU:".$queryRecordsObj->sku_id."</li>";
					if($queryRecordsObj->ord_sku_name!=''){
						$order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
					}else{
						$order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
					}
					$product_details=get_product_details($queryRecordsObj->product_id);
					if(!empty($product_details))
					$order_summary.=($product_details->product_description!='') ? "<li class='mt-10'><small>".$product_details->product_description.'</small></li>': '';
					
				}

				$order_summary.="<li>Order Date:".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."</li>";
				$order_summary.="<li>Order ID:".$queryRecordsObj->order_id."</li>";
				$order_summary.="<li>Order Item ID:".$queryRecordsObj->order_item_id."</li>";
				if($queryRecordsObj->type_of_order=="replaced"){
					$order_summary.="<li>[<span class='text-info bold'>Replaced Order</span>]</li>";
				}
				
				$prev_order_item_id=$queryRecordsObj->prev_order_item_id;
				
				if($queryRecordsObj->prev_order_item_id!=""){
					
					$orders_status_data_obj=$this->Model_orders->get_orders_status_data($queryRecordsObj->prev_order_item_id);
					
					$replacement_desired_data_obj=$this->Model_orders->get_replacement_desired_data($queryRecordsObj->prev_order_item_id);
					
					$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->prev_order_item_id);
					
					if($order_replacement_decision_data_obj->pickup_identifier=="after replace"){
						$order_summary.="<li>[<span class='text-info bold'>Pickup has been completed</span>]</li>";
					}
					if($order_replacement_decision_data_obj->pickup_identifier=="before replace"){
						$replacement_option_status='';
						if($orders_status_data_obj->initiate_replacement_pickup =='1' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='[Return the Item]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='1' && $orders_status_data_obj->order_replacement_pickup==1){
							$replacement_option_status='[Item Had been Collected Successfully]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='0' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='[Initiate Pickup has been rejected by admin]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='';
						}
						$order_summary.="<li>".'<span class="text-info bold">'.$replacement_option_status.'</span>'."</li>";
					}
				}
				
				
				
				/////////////////added - promotion////////////////////
					
				if($queryRecordsObj->type_of_order=="replaced"){
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion_for_replaced_order($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal,$prev_order_item_id);
			   
				}else{
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal);
				}
				$order_summary.='</ul>';
				$order_summary.=$str_promo;
		   
				
				/////////////////added - promotion////////////////////
				
				$row[]=$order_summary;
				$status_of_transaction=$this->get_status_of_transaction($queryRecordsObj->order_item_id);
				$cancels_tableObj=$this->Model_orders->get_data_cancels_table($queryRecordsObj->order_item_id);
				$cancellation_details="";
				$cancellation_details.='<ul class="list-unstyled small">';
				$cancellation_details.="<li>Cancelled By:".$cancels_tableObj->cancelled_by."</li>";
				
				if(strlen($cancels_tableObj->cancel_reason_comments)>200){
					$cancellation_details.='<li>Comments:<div class="f-row margin-top more">'.str_split($cancels_tableObj->cancel_reason_comments,200)[0].'<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' .substr($cancels_tableObj->cancel_reason_comments,201). '</span>&nbsp;&nbsp;<a href="#" class="morelink" onclick="morelinkfun(this)">more</a></span></div></li>';
				}else{
					$cancellation_details.='<li>Comments:<div class="f-row margin-top more">'.$cancels_tableObj->cancel_reason_comments.'</div></li>';
				}
				
				
				if($status_of_transaction->refund_type==""){
					$payment_type_from_cancelled_orders=$this->get_cancelled_order_customer_payment_type($queryRecordsObj->order_item_id);
					if(strtolower($payment_type_from_cancelled_orders)=='cod'){
						$cancellation_details.="<li>Customer Payment Method: COD</li>";
					}else{
						$cancellation_details.="<li>Refund Method: Not Yet Given</li>";
					}
				}
				else{
					$cancellation_details.="<li>Refund Method: ".$status_of_transaction->refund_type."</li>";
				}
				$cancellation_details.='</ul>';
				$row[]=$cancellation_details;
				$order_quantity_price='<ul class="list-unstyled small">';
				if($queryRecordsObj->type_of_order=="replaced"){
					//$replaced_item_price_individual=$this->Model_orders->get_replaced_item_price_individual($prev_order_item_id);

					$replaced_item_price_individual=$queryRecordsObj->product_price;					
					$order_quantity_price.="<li>Qty:".$queryRecordsObj->quantity."</li>";				
					$order_quantity_price.="<li>Price of replacement order (each):  ".curr_sym." ".$replaced_item_price_individual."</li>";
					$order_quantity_price.="<li>Shipping charge for replacement:  ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
					$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
				}else{
					$order_quantity_price.="<li>Qty:".$queryRecordsObj->quantity."</li>";
					$order_quantity_price.="<li>Total Item Price:</b>".$queryRecordsObj->subtotal."</li>";
					if($queryRecordsObj->promotion_invoice_free_shipping>0){
						$order_quantity_price.="<li>Shipping charge (<span style='color:red;'>waived off</span>): ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
						$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->subtotal."</li>";
					}else{
						$order_quantity_price.="<li>Shipping charge: ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
						$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
					}	
					
					$order_quantity_price.='<li><a class="text-info bold" href="#" onclick="view_order_summary_(\''.$queryRecordsObj->order_id.'\')" id="view_order_summary_'.$queryRecordsObj->order_item_id.'">View Invoice Data ';
					if($queryRecordsObj->invoice_offer_achieved==1){
						$invoice_ofr_count=$this->get_invoice_offers_str($queryRecordsObj);
						$order_quantity_price.='<span class="badge" style="color:red;">'.$invoice_ofr_count.'</span>';
					}
					$order_quantity_price.='</a></li>';
				}
					$order_quantity_price.='</ul>';
				
				$row[]=$order_quantity_price;
				$customer_add='<ul class="list-unstyled small">';
				$customer_add.="<li>".$queryRecordsObj->customer_name."</li>";
				$customer_add.="<li>".$queryRecordsObj->address1."</li>";
				$customer_add.="<li>".$queryRecordsObj->address2."</li>";
				$customer_add.="<li>".$queryRecordsObj->city.",".$queryRecordsObj->state."</li>";
				$customer_add.="<li>".$queryRecordsObj->country."</li>";
				$customer_add.="<li>".$queryRecordsObj->pincode."</li>";
				$customer_add.="<li>".$queryRecordsObj->mobile."</li>";
				$customer_add.='</ul>';
				$row[]=$customer_add;
				
				$actions="";
				
				$actions.='<ul class="list-unstyled small">';
				
				if($status_of_transaction->refund_type==""){
					$payment_type_from_cancelled_orders=$this->get_cancelled_order_customer_payment_type($queryRecordsObj->order_item_id);
					if(strtolower($payment_type_from_cancelled_orders)=='cod'){
						$actions.="<li>No refund is needed. Customer didn't pay money for the purchase because the payment type is COD</li>";
						$actions.="<li>No details</li>";
					}else{
						$actions.='<li>Awaiting</li>';
						$actions.="<li>No details</li>";
					}
				}else{
					if($status_of_transaction->status==""){
						$actions.="<li>No details</li>";
						$actions.='<li><a class="text-info bold" href="#" onclick="open_cancel_refund_issue_fun('.$queryRecordsObj->order_item_id.')">Issue Refund</a></li>';
					}
					if($status_of_transaction->status=="refund"){
						$actions.="<li>No details</li>";
						$actions.='<li>Processed</li>';
					}
					if($status_of_transaction->status=="refunded"){
						
						$actions.='<li>Refund Completed</li>';
						$actions.="<li>Transaction ID:".$queryRecordsObj->transaction_id."</li>";
						$actions.="<li>Transaction Date:".date("D j M, Y",strtotime($queryRecordsObj->transaction_date))."</li>";
						$actions.="<li>Transaction Time:".$queryRecordsObj->transaction_time."</li>";
						
					}
				}
				$actions.='</ul>';

				$row[]=$actions;	
				$data[]=$row;
				$row=array();
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_status_of_transaction($order_item_id){
		$refund_type=$this->Model_orders->get_status_of_transaction($order_item_id);
		return $refund_type;
	}
	public function getIndianCurrency(float $number)
{
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred','thousand','lakh', 'crore');
    while( $i < $digits_length ) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
        } else $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    $paise = ($decimal > 0) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
	$paise="";//I did 6/11/2021
    $inwords=($Rupees ? $Rupees . 'rupees ' : '') . $paise;
	$inwords = preg_replace('/\s+/', ' ', $inwords);
	return ucfirst($inwords)." only";
}
	public function generate_invoice_pdf(){
		if($this->session->userdata("logged_in")){
			$email_stuff_arr=email_stuff();
			$generalstuff_arr=generalstuff();
			$company_gstin=$generalstuff_arr["company_gstin"];
			$data["controller"]=$this;
			extract($this->input->post());
			//echo $order_item_id;
			
			if(isset($order)){
				if($order=="confirmed"){
					$active_orders_obj=$this->Model_orders->get_data_from_active_orders($order_item_id);	
				}
			}else{
				$order="delivered";
				$active_orders_obj=$this->Model_orders->get_data_from_history_orders($order_item_id);	
			}
			
			$type_of_order=$this->Model_orders->get_type_of_order_item($order_item_id);
			
			$invoice_data_obj=$this->Model_orders->get_data_from_invoice_offers($active_orders_obj->order_id);
			$order_item_obj=$this->Model_orders->get_details_of_order_item($active_orders_obj->order_item_id);
			$order_status_data_obj=$this->Model_orders->get_details_orders_status($active_orders_obj->order_item_id);
			$shipping_add_obj=$this->Model_orders->get_shipping_add_info_by_shipping_address_id($active_orders_obj->shipping_address_id);		
			
			$logistics_local_state_name=$this->Model_orders->get_make_as_local_state_by_logistics_id($active_orders_obj->logistics_id);
			if(trim(strtolower($shipping_add_obj->state))==trim(strtolower($logistics_local_state_name))){
				$is_local_state="yes";
			}
			else{
				$is_local_state="no";
			}
			$pdf = new FPDF_INVOICE('P','mm','A5');
			$pdf->AddPage();
			//$pdf->Ln();
			$pdf->SetFont('Arial','B',12);
			$pdf->SetTextColor(0,0,0);
			if($type_of_copy=="transit"){
				$pdf->Cell(55,5,"TRANSIT COPY",0,0,'L',0);
				$pdf->SetFont('Arial','',12);
				$pdf->Cell(33,5,"Order ID:",0,0,'R',0);
				$pdf->Cell(40,5,$order_id,0,0,'R',0);
			}
			if($type_of_copy=="customer"){
				$pdf->Cell(55,5,"CUSTOMER COPY",0,0,'L',0);
				$pdf->SetFont('Arial','',12);
				$pdf->Cell(35,5,"Order ID:",0,0,'R',0);
				$pdf->Cell(40,5,$order_id,0,0,'R',0);
			}
			if($type_of_copy=="revised"){
				$pdf->Cell(60,5,"REVISED COPY",0,0,'L',0);
				$pdf->SetFont('Arial','',12);
				$pdf->Cell(35,5,"Order ID:",0,0,'R',0);
				$pdf->Cell(35,5,$order_id,0,0,'R',0);
			}
			$pdf->SetTextColor(0,0,0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',12);
			
			//$pdf->Ln();
			$pdf->Ln();
			$logopath=base_url().$email_stuff_arr["pdflogo_emailtemplate"];
			$pdf->Image($logopath,10,18);
			
			
			$pdf->SetFont('Arial','',15);
			$pdf->Cell(130,10,'INVOICE',0,0,'R',0);
			$pdf->Ln();
			if($company_gstin!=""){
				$pdf->SetFont('Arial','',10);
				$pdf->SetTextColor(0,0,0);
				$pdf->Cell(130,5,'GST : '.$company_gstin,0,0,'L',0);
				$pdf->SetTextColor(0,0,0);
				$pdf->Ln();
			}
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,2,'',0,0,'R',0);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Ln();
			// $pdf->SetFont('Arial','i',10);
			// $pdf->Cell(60,5,'Sold By:',0,0,'L',0);
			// $pdf->Ln();
			
			
			// $pdf->SetFont('Arial','B',10);
			// $pdf->Cell(130,5,'MAGNUM LABEL',0,0,'L',0);
			// $pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_street1"]." ".$email_stuff_arr["company_address_street2"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_city_state"]." India.",0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_customer_care_stuff"],0,0,'L',0);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->setFillColor(0,0,0);
			$pdf->SetX(0);
			$pdf->Cell(150,0.5,'',0,0,'R',1);
			$pdf->setFillColor(255,255,255);
			$pdf->Ln();
			//$pdf->Cell(130,7,'',0,0,'L',0);
			//$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->SetTextColor(0,0,0);


			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(13,5,'Bill To:',0,0,'L',0);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(67,5,$shipping_add_obj->customer_name,0,0,'L',0);
			$pdf->SetTextColor(0,0,0);
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(25,5,'Order Date:',0,0,'L',0);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(25,5,date("D, j M, Y",strtotime($active_orders_obj->timestamp)),0,0,'L',0);
			$pdf->Ln();
			
			
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(30,5,'Customer GSTIN:',0,0,'L',0);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(50,5,$shipping_add_obj->customer_gstin,0,0,'L',0);
            $pdf->SetFont('Arial','B',10);
            $pdf->Cell(25,5,'Invoice Date:',0,0,'L',0);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(25,5,date("D, j M, Y"),0,0,'L',0);
			$pdf->Ln();

			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(30,5,'Billing Address:',0,0,'L',0);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(50,5,'',0,0,'L',0);
			$pdf->SetTextColor(0,0,0);
            $pdf->SetFont('Arial','B',10);
            $pdf->Cell(25,5,'Invoice No:',0,0,'L',0);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(25,5,$active_orders_obj->invoice_number,0,0,'L',0);
			$pdf->Ln();

			/*$shipping_add_obj->address1="No.38 & 39, Bhanu Nursing Home Road, Bommanahalli, Hosur Main Road,";
			$pdf->SetFont('Arial','i',8);
			$pdf->Cell(130,4,$shipping_add_obj->address1,0,0,'L',0);
			$pdf->Ln();*/
			
			
			$pdf->SetFont('Arial','i',8);
			//$pdf->Cell(130,4,$shipping_add_obj->address2." ".$shipping_add_obj->city." ".$shipping_add_obj->state.", ".$shipping_add_obj->country." ".$shipping_add_obj->pincode,0,0,'L',0);
			$pdf->MultiCell(130,4,$shipping_add_obj->address1.", ".$shipping_add_obj->address2." ".$shipping_add_obj->city." ".$shipping_add_obj->state.", ".$shipping_add_obj->country." ".$shipping_add_obj->pincode,0,'L',0,0);
			
			//$pdf->Ln();
			
			
			$pdf->SetTextColor(0,0,0);
			$pdf->SetFont('Arial','i',8);
            $pdf->Cell(15,5,'Contact No:',0,0,'L',0);
            $pdf->SetFont('Arial','i',8);
            $pdf->Cell(35,5,"+91 ".wordwrap($shipping_add_obj->mobile , 5 , ' ' , true ),0,0,'L',0);
			$pdf->Ln();
			
			//$pdf->Ln();


			$pdf->SetLineWidth(0.1);
			$pdf->SetDrawColor(0,0,0);
			//$pdf->SetFillColor(0,0,0);
			//$pdf->SetTextColor(255,255,255);
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(50,8,'Items','TLB',0,'L',true); 
			$pdf->Cell(20,8,'HSN Code','TB',0,'L',true); 
			$pdf->Cell(20,8,'Qty','TB',0,'C',true);
			//uni=True
            
			$pdf->Cell(20,8,'Per Unit ('.curr_txt.')','TB',0,'L',true);
			$pdf->Cell(20,8,'Total ('.curr_txt.') ','TRB',0,'R',true);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10,True);
			$pdf->SetFillColor(255,255,255);
			$pdf->SetTextColor(0,0,0);
			
			//$pdf->SetDrawColor(213,213,213);
			//$pdf->SetLineWidth(0.5);
			
			
			$total_quantity=0;
			$total_without_tax=0;
			$total_tax_percent=0;
			$total_tax_amount=0;
			$total_with_tax=0;
			
			//$pdf->SetFont('Arial','',10);
			$product_info_obj=$this->get_product_info_by_product_id($active_orders_obj->product_id);
			$inventory_info_obj=$this->get_inventory_info_by_inventory_id($active_orders_obj->inventory_id);
			$hsncode=($inventory_info_obj->hsncode) ? $inventory_info_obj->hsncode : '';
			$subcat_info_obj=$this->get_subcat_info_by_product_id($active_orders_obj->product_id);
			//$pdf->Cell(30,6,$subcat_info_obj->subcat_name,'L',0,'L',0); 
			
			$pro_name=($inventory_info_obj->sku_name!='') ? $inventory_info_obj->sku_name :$product_info_obj->product_name ;
			$productfullinfo="";
			$no_of_lines_product_desc=1;
			$product_desc=$productfullinfo."".$pro_name;
			$line_product_desc=array();
			$line_product_desc[$no_of_lines_product_desc]=substr($product_desc,0,60);
			if(strlen(substr($product_desc,60,60))!=0){
				$no_of_lines_product_desc=2;
				$line_product_desc[$no_of_lines_product_desc]=substr($product_desc,60,60);
			}
			if(strlen(substr($product_desc,120,60))!=0){
				$no_of_lines_product_desc=3;
				$line_product_desc[$no_of_lines_product_desc]=substr($product_desc,120,60);
			}
			for($i=1;$i<=count($line_product_desc);$i++){
				$pdf->SetFont('Arial','',9);
				
			//////////////////////////////////////////
			$inventory_dimentions="";
			$color_attribute_is_there="no";
			if(strtolower($inventory_info_obj->attribute_1)=="color" || strtolower($inventory_info_obj->attribute_2)=="color" || strtolower($inventory_info_obj->attribute_3)=="color" || strtolower($inventory_info_obj->attribute_4)=="color"){
				$color_attribute_is_there="yes";
				if(strtolower($inventory_info_obj->attribute_1)=="color"){
					$inventory_dimentions.=" - ".explode(":",$inventory_info_obj->attribute_1_value)[0];
				}
				else if(strtolower($inventory_info_obj->attribute_2)=="color"){
					$inventory_dimentions.=" - ".explode(":",$inventory_info_obj->attribute_2_value)[0];
				}
				else if(strtolower($inventory_info_obj->attribute_3)=="color"){
					$inventory_dimentions.=" - ".explode(":",$inventory_info_obj->attribute_3_value)[0];
				}
				else if(strtolower($inventory_info_obj->attribute_4)=="color"){
					$inventory_dimentions.=" - ".explode(":",$inventory_info_obj->attribute_4_value)[0];
				}
			}
			else{
			/////////////////////////////////	
				
				$noncolor_attribute_first_is_there="no";
				$noncolor_attribute_second_is_there="no";
				if(strtolower($inventory_info_obj->attribute_1)!="color"){
					$noncolor_attribute_first_is_there="yes";
					$inventory_dimentions.=" - ".$inventory_info_obj->attribute_1_value;
				}
				if(strtolower($inventory_info_obj->attribute_2)!="color"){
					if($noncolor_attribute_first_is_there=="yes"){
						$noncolor_attribute_second_is_there="yes";
						if($inventory_info_obj->attribute_2_value!=""){
							$inventory_dimentions.=" (".$inventory_info_obj->attribute_2_value.")";
						}
					}
					else{
						$noncolor_attribute_first_is_there="yes";
						$inventory_dimentions.=" - ".$get_free_item_data_image_path_attributes_arr["attribute_2_value"];
					}
				}
				if(strtolower($inventory_info_obj->attribute_3)!="color"){
					if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
						if($noncolor_attribute_first_is_there=="yes"){
							$noncolor_attribute_second_is_there="yes";
							if($inventory_info_obj->attribute_3_value!=""){
								$inventory_dimentions.=" (".$inventory_info_obj->attribute_3_value.")";
							}
						}
						else{
							$noncolor_attribute_first_is_there="yes";
							$inventory_dimentions.=" - ".$inventory_info_obj->attribute_3_value;
						}
					}
				}
				if(strtolower($inventory_info_obj->attribute_4)!="color"){
					if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
						if($noncolor_attribute_first_is_there=="yes"){
							$noncolor_attribute_second_is_there="yes";
							if($inventory_info_obj->attribute_4_value!=""){
								$inventory_dimentions.=" (".$inventory_info_obj->attribute_4_value.")";
							}
						}
						else{
							$noncolor_attribute_first_is_there="yes";
							$inventory_dimentions.=" - ".$inventory_info_obj->attribute_4_value;
						}
					}
				}
			
			//////////////////////////////
			
			}
			$color_attribute_is_there="no";
			$noncolor_attribute_first_is_there="no";
			$noncolor_attribute_second_is_there="no";
			///////////////////////////////////////////
			if($inventory_info_obj->sku_name==''){
				$product_dimension_sku=$line_product_desc[$i]." ".$inventory_dimentions;
			}
			else{
				$product_dimension_sku=$line_product_desc[$i];
			}
			$more_than_25_chars="no";
			if(strlen($product_dimension_sku)>=25){
				//$pdf->SetFont('Arial','B',5);
				$more_than_25_chars="yes";
			}
			else{
				//$pdf->SetFont('Arial','',9);
			}
			
			//$pdf->Cell(50,8,'Micro Flute Box - Brown (250L X 165W X 65H mm)','BL',0,'L',0);

			//if($active_orders_obj->ord_addon_products_status!='1'){
			if(1){

			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(15,5,"SKU ID : ",'L',0,'L',0);
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(115,5,$inventory_info_obj->sku_id,'R',0,'L',0);
			$pdf->Ln();
			
			if($more_than_25_chars!="yes"){
				if($inventory_info_obj->sku_name==''){
					$pdf->Cell(50,5,$line_product_desc[$i]." ".$inventory_dimentions,'BL',0,'L',0);
				}
				else{
					$pdf->Cell(50,5,$line_product_desc[$i],'BL',0,'L',0);
				}
				$pdf->Cell(20,5,$hsncode,'B',0,'L',0);
			}else{
				$pdf->SetFont('Arial','',8);
				$pdf->Cell(50,5,$line_product_desc[$i],'L',0,'L',0);
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(20,5,$hsncode,0,0,'L',0);
			}
			if($i==1){

			$pdf->SetFont('Arial','',9);
			if($more_than_25_chars=="yes"){
				$pdf->Cell(20,5,$active_orders_obj->quantity,0,0,'C',0);
			}
			else{
				$pdf->Cell(20,5,$active_orders_obj->quantity,'B',0,'C',0);
			}

			$selling_price_without_tax=round(($active_orders_obj->subtotal*100/(100+$active_orders_obj->tax_percent)),2);
			$tax_percent=round($active_orders_obj->tax_percent,2);
			$tax_amount=round((($selling_price_without_tax*$tax_percent)/100),2);
			$selling_price_with_tax=round(($active_orders_obj->subtotal),2);
			
			$g_tot=round(($active_orders_obj->grandtotal),2);
			$total_quantity+=$active_orders_obj->quantity;
			$total_without_tax+=$selling_price_without_tax;
			$total_tax_amount+=$tax_amount;
			$total_with_tax+=$selling_price_with_tax;
			
			
			if($active_orders_obj->promotion_id_selected!=""){
				
				/////// 2/202/2022 changes starts ////////////////
			$individual_price_of_product_with_promotion=round($active_orders_obj->individual_price_of_product_with_promotion,2);
			
			$subtotal=round($active_orders_obj->subtotal,2);

			//$subtotal=($subtotal-$active_orders_obj->ord_addon_total_price);
			
			/////// 2/202/2022 changes ends ////////////////
			
			
				if($more_than_25_chars=="yes"){
					$pdf->Cell(20,5,number_format($individual_price_of_product_with_promotion,2,'.',''),0,0,'L',0);
					//$pdf->Cell(20,6,$tax_percent,'L',0,'L',0);
					//$pdf->Cell(20,6,$tax_amount,'L',0,'L',0);
					$pdf->Cell(20,5,number_format($subtotal,2,'.',''),'R',0,'R',0);
				}
				else{
					$pdf->Cell(20,5,number_format($individual_price_of_product_with_promotion,2,'.',''),'B',0,'L',0);
					//$pdf->Cell(20,6,$tax_percent,'L',0,'L',0);
					//$pdf->Cell(20,6,$tax_amount,'L',0,'L',0);
					$pdf->Cell(20,5,number_format($subtotal,2,'.',''),'RB',0,'R',0);
				}
				if($more_than_25_chars=="yes"){
					$pdf->Ln();
					if($inventory_info_obj->sku_name==""){
						$pdf->Cell(130,5,$inventory_dimentions,'RBL',0,'L',0);
					}
					else{
						$pdf->Cell(130,5,'','RBL',0,'L',0);
					}
				}
			}else{
				/////// 2/202/2022 changes starts ////////////////
			$individual_price_of_product=round($active_orders_obj->product_price,2);

			$subtotal=round($active_orders_obj->subtotal,2);
			//$subtotal=($subtotal-$active_orders_obj->ord_addon_total_price);

			/////// 2/202/2022 changes ends ////////////////

				if($more_than_25_chars=="yes"){
					$pdf->Cell(20,5,number_format($individual_price_of_product,2,'.',''),0,0,'L',0);
					//$pdf->Cell(20,6,$tax_percent,'L',0,'L',0);
					//$pdf->Cell(20,6,$tax_amount,'L',0,'L',0);
					$pdf->Cell(20,5,number_format($subtotal,2,'.',''),'R',0,'R',0);
				}
				else{
					$pdf->Cell(20,5,number_format($individual_price_of_product,2,'.',''),'B',0,'L',0);
					//$pdf->Cell(20,6,$tax_percent,'L',0,'L',0);
					//$pdf->Cell(20,6,$tax_amount,'L',0,'L',0);
					$pdf->Cell(20,5,number_format($subtotal,2,'.',''),'RB',0,'R',0);
				}
				if($more_than_25_chars=="yes"){
					$pdf->Ln();
					if($inventory_info_obj->sku_name==""){
						$pdf->Cell(130,5,$inventory_dimentions,'RBL',0,'L',0);
					}
					else{
						$pdf->Cell(130,5,'','RBL',0,'L',0);
					}
				}
				
				$pdf->Ln();

				
				
						$taxable_price_display=$active_orders_obj->ord_taxable_price;
						$CGST=$active_orders_obj->CGST;
						$IGST=$active_orders_obj->IGST;
						$SGST=$active_orders_obj->SGST;
		
						if($active_orders_obj->ord_addon_master_order_item_id!=''){
						}else{
						
						$pdf->SetFont('Arial','',9);
						$pdf->Cell(50,5,'','L',0,'L',0); 
						$pdf->Cell(20,5,'',0,0,'L',0);
						$pdf->Cell(20,5,'',0,0,'L',0);
						$pdf->Cell(20,5,'Taxable Value',0,0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						$pdf->Cell(20,5,number_format($taxable_price_display*$active_orders_obj->quantity,2,'.',''),'R',0,'R',0);
						$pdf->Ln();	
						$pdf->Cell(50,5,'','L',0,'L',0); 
						$pdf->Cell(20,5,'',0,0,'L',0);
						$pdf->Cell(20,5,'',0,0,'L',0);
						$pdf->SetFont('Arial','',7);
						$pdf->Cell(20,5,'CGST ('.$active_orders_obj->CGST.'%)',0,0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						$pdf->Cell(20,5,number_format(($CGST*$taxable_price_display*$active_orders_obj->quantity)/100,2,'.',''),'R',0,'R',0);
						$pdf->Ln();	
						$pdf->Cell(50,5,'','L',0,'L',0); 
						$pdf->Cell(20,5,'',0,0,'L',0);
						$pdf->Cell(20,5,'',0,0,'L',0);
						$pdf->Cell(20,5,'IGST ('.$active_orders_obj->IGST.'%)',0,0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						$pdf->Cell(20,5,number_format(($IGST*$taxable_price_display*$active_orders_obj->quantity)/100,2,'.',''),'R',0,'R',0);
						
						
						$pdf->Ln();	
						$pdf->Cell(50,5,'','L',0,'L',0); 
						$pdf->Cell(20,5,'',0,0,'L',0);
						$pdf->Cell(20,5,'',0,0,'L',0);
						$pdf->Cell(20,5,'SGST ('.$active_orders_obj->SGST.'%)',0,0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						$pdf->Cell(20,5,number_format(($SGST*$taxable_price_display*$active_orders_obj->quantity)/100,2,'.',''),'R',0,'R',0);
						
						
							
						$pdf->SetFont('Arial','',9);

					}//addon
						
			}
			
			
			}else{
					$pdf->Cell(20,5,'',0,0,'L',0); 
					$pdf->Cell(20,5,'',0,0,'L',0); 
					//$pdf->Cell(20,6,'','L',0,'L',0); 
					//$pdf->Cell(20,6,'','L',0,'L',0); 
					$pdf->Cell(20,5,'',0,0,'L',0); 
				}

				if($active_orders_obj->ord_addon_master_order_item_id!=''){
					/* no line for addon sku */
				}else{
					$pdf->Ln();
					/* main ku*/
				}

			


			} // cpmbo product condition

			}
           
             
			if($active_orders_obj->promotion_quote!="" && $type_of_order==""){
				if($active_orders_obj->promotion_quote!=$active_orders_obj->promotion_default_discount_promo){
					$promo_text=$active_orders_obj->promotion_quote;
					$promo_text = preg_replace('/\s\s+/', ' ', $promo_text);
					$promo_text =stripslashes($promo_text);
					$promo_text =iconv('UTF-8', 'windows-1252', $promo_text);
					$pdf->SetFont('Arial','I',9);
					if($active_orders_obj->promotion_default_discount_promo!="" && $active_orders_obj->default_discount>0){
						
						$pdf->Cell(50,8,"Promo Applied: ".$promo_text,'BL',0,'L',0); 
						$pdf->Cell(20,8,'','B',0,'L',0);
						$pdf->Cell(20,8,'','B',0,'L',0);
						$pdf->Cell(20,8,'','B',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						$pdf->Cell(20,8,'','RB',0,'R',0);
						
						
					
					
					}else{
						
						$pdf->Cell(50,8,"Promo Applied: ".$promo_text,'BL',0,'L',0); 
						$pdf->Cell(20,8,'','B',0,'L',0);
						$pdf->Cell(20,8,'','B',0,'L',0);
						$pdf->Cell(20,8,'','B',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						$pdf->Cell(20,8,'','RB',0,'R',0);
						
						
						
						
					}
					$pdf->Ln();
				}
			}
                        
                        

			if($active_orders_obj->promotion_default_discount_promo!="" && $active_orders_obj->default_discount>0 && $type_of_order==""){
				$promo_text=$active_orders_obj->promotion_default_discount_promo;
				$promo_text = preg_replace('/\s\s+/', ' ', $promo_text);
				$promo_text =stripslashes($promo_text);
				$promo_text =iconv('UTF-8', 'windows-1252', $promo_text);
				$pdf->SetFont('Arial','I',9);
						
				
				
				/////// 2/202/2022 changes starts ////////////////
				$original_mrp=round($active_orders_obj->ord_max_selling_price,2);
				$promotion_discount_display=$active_orders_obj->promotion_discount;
				$taxable_price_display=$active_orders_obj->ord_taxable_price;
				$CGST=$active_orders_obj->CGST;
				$IGST=$active_orders_obj->IGST;
				$SGST=$active_orders_obj->SGST;
			/////// 2/202/2022 changes ends ////////////////
				
						$pdf->Cell(50,5,"Promo Applied: ".$promo_text,'L',0,'L',0); 
						$pdf->Cell(20,5,'','',0,'L',0);
						$pdf->Cell(20,5,'','',0,'L',0);
						$pdf->Cell(20,5,'','',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						$pdf->Cell(20,5,'','R',0,'R',0);
						/////// 2/202/2022 changes starts ////////////////
						$pdf->Ln();
						
						$pdf->Cell(50,5,$promotion_discount_display."% off on MRP of Rs ".number_format($original_mrp,2,'.',''),'BL',0,'L',0); 
						$pdf->Cell(20,5,'','B',0,'L',0);
						$pdf->Cell(20,5,'','B',0,'L',0);
						$pdf->Cell(20,5,'','B',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						$pdf->Cell(20,5,'','RB',0,'R',0);
			/////// 2/202/2022 changes ends ////////////////
			
						$pdf->Ln();
				
						$pdf->SetFont('Arial','',9);
						$pdf->Cell(50,5,'','L',0,'L',0); 
						$pdf->Cell(20,5,'',0,0,'L',0);
						$pdf->Cell(20,5,'',0,0,'L',0);
						$pdf->Cell(20,5,'Taxable Value',0,0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						$pdf->Cell(20,5,number_format($taxable_price_display*$active_orders_obj->quantity,2,'.',''),'R',0,'R',0);
						$pdf->Ln();	
						$pdf->Cell(50,5,'','L',0,'L',0); 
						$pdf->Cell(20,5,'',0,0,'L',0);
						$pdf->Cell(20,5,'',0,0,'L',0);
						$pdf->SetFont('Arial','',7);
						$pdf->Cell(20,5,'CGST ('.$active_orders_obj->CGST.'%)',0,0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						$pdf->Cell(20,5,number_format(($CGST*$taxable_price_display*$active_orders_obj->quantity)/100,2,'.',''),'R',0,'R',0);
						$pdf->Ln();	
						$pdf->Cell(50,5,'','L',0,'L',0); 
						$pdf->Cell(20,5,'',0,0,'L',0);
						$pdf->Cell(20,5,'',0,0,'L',0);
						$pdf->Cell(20,5,'IGST ('.$active_orders_obj->IGST.'%)',0,0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						$pdf->Cell(20,5,number_format(($IGST*$taxable_price_display*$active_orders_obj->quantity)/100,2,'.',''),'R',0,'R',0);
						$pdf->Ln();	
						$pdf->Cell(50,5,'','L',0,'L',0); 
						$pdf->Cell(20,5,'',0,0,'L',0);
						$pdf->Cell(20,5,'',0,0,'L',0);
						$pdf->Cell(20,5,'SGST ('.$active_orders_obj->SGST.'%)',0,0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						$pdf->Cell(20,5,number_format(($SGST*$taxable_price_display*$active_orders_obj->quantity)/100,2,'.',''),'R',0,'R',0);
						$pdf->Ln();	
						$pdf->SetFont('Arial','',9);
			}
             
			

			/* addon sections  */
			//if($active_orders_obj->ord_addon_products_status=='1'){
			if(0){
				
				$arr=json_decode($active_orders_obj->ord_addon_products);
				foreach($arr as $val){
					$name=$val->inv_name;
					$image=$val->inv_image;
					$sku_id=$val->inv_sku_id;
					$price=round($val->inv_price,2);

					$pdf->SetFont('Arial','B',9);
					$pdf->Cell(15,5,"SKU ID : ",'L',0,'L',0);
					$pdf->SetFont('Arial','',9);
					$pdf->Cell(65,5,$sku_id,'',0,'L',0);
					$pdf->Cell(10,5,'1','',0,'L',0);
					$pdf->Cell(20,5,number_format($price,2),'',0,'L',0);
					$pdf->Cell(20,5,number_format($price,2),'R',0,'R',0);
					$pdf->Ln();

					$pdf->SetFont('Arial','',9);
					$pdf->Cell(130,5,$val->inv_name,'RLB',0,'L',0);
					$pdf->Ln();

				}
			}
			
		/* addon sections  */


			$pdf->SetFont('Arial','',9);
			
			$pdf->Cell(50,8,'','BL',0,'L',0); 
			$pdf->Cell(20,8,'','B',0,'L',0); 
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(20,8,'','B',0,'C',0);

			
			$shipping_charge_without_tax=round(((($active_orders_obj->shipping_charge)*100)/(100+$active_orders_obj->tax_percent)),2);
			$shipping_tax_percent=round($active_orders_obj->tax_percent,2);
			$shipping_tax_amount=round((($shipping_charge_without_tax*$tax_percent)/100),2);
			$shipping_charge_with_tax=round(($active_orders_obj->shipping_charge),2);
			
			if($invoice_data_obj->promotion_invoice_free_shipping>0 && $type_of_order==""){
				$shipping_charge_without_tax=0;
				$shipping_tax_percent=0;
				$shipping_tax_amount=0;
				$shipping_charge_with_tax=0;
				
				$pdf->Cell(20,8,"Shipping Charge",'B',0,'L',0);
				//$pdf->Cell(20,6,0,'TL',0,'L',0);
				//$pdf->Cell(20,6,0,'TL',0,'L',0);
				$pdf->Cell(20,8,"0.00",'RB',0,'R',0);
				
				
			
			
			
				$pdf->Ln();
				
				$promo_text="Shipping charge waived off";
				$pdf->SetFont('Arial','I',9);				
						
						$pdf->Cell(50,8,$promo_text,'BL',0,'L',0);
						$pdf->Cell(20,8,'','B',0,'L',0);						
						$pdf->Cell(20,8,'','B',0,'L',0);
						$pdf->Cell(20,8,'','B',0,'L',0);
					//	$pdf->Cell(20,6,'','TBL',0,'L',0);
					//	$pdf->Cell(20,6,'','TBL',0,'L',0);
						$pdf->Cell(20,8,'','RB',0,'L',0);
						
						
			
			
				$pdf->Ln();
				
			}else{
				$pdf->Cell(20,8, 'Shipping Charge','B',0,'L',0);
				//$pdf->Cell(20,6,$shipping_tax_percent,'TL',0,'L',0);
				//$pdf->Cell(20,6,$shipping_tax_amount,'TL',0,'L',0);
				$pdf->Cell(20,8,number_format($active_orders_obj->shipping_charge,2,'.',''),'RB',0,'R',0);
				
				
				
			
			
				$pdf->Ln();
			}
			
			$total_without_tax+=$shipping_charge_without_tax;
			$total_tax_amount+=$shipping_tax_amount;
			$total_with_tax+=$shipping_charge_with_tax;
			
			$total_without_tax=round($total_without_tax,2);
			$total_tax_amount=round($total_tax_amount,2);
			$total_with_tax=round($total_with_tax,2);
			
			///////////////////
			$total_tax_value=round(($total_without_tax*$tax_percent)/100,2);
			$total_invoice_value=round(($total_without_tax+$total_tax_value),2);
			///////////////////
                        
                        
                        
/* sku coupon */
                        if($active_orders_obj->ord_coupon_status=='1'){
                            
                            $sku_coupontext='';
                            $sku_coupontext.=($active_orders_obj->ord_coupon_type=='2') ? curr_txt : '';
                            $sku_coupontext.=round($active_orders_obj->ord_coupon_value);
                            $sku_coupontext.=($active_orders_obj->ord_coupon_type=='1') ? '%' : 'Flat';
                            $sku_coupontext.='';
                            
                            $pdf->SetFont('Arial','',9);
                            $pdf->Cell(50,8,"Coupon Applied: ".$sku_coupontext,'BL',0,'L',0); 
                            $pdf->Cell(20,8,'','B',0,'L',0);
                            $pdf->Cell(20,8,'','B',0,'L',0);
                            $pdf->Cell(20,8,'','B',0,'L',0);
                            //$pdf->Cell(20,6,'','TBL',0,'L',0);
                            //$pdf->Cell(20,6,'','TBL',0,'L',0);
                            $pdf->Cell(20,8,$active_orders_obj->ord_coupon_reduce_price,'RB',0,'R',0);
                            $pdf->Ln();	
                            
                            $total_invoice_value=round(($total_invoice_value-$active_orders_obj->ord_coupon_reduce_price));
                        }
                        
                        /* sku coupon */
                        
				$each_invoice_amount=0;	
				
				if($invoice_data_obj->promotion_invoice_discount>0 && $type_of_order==''){
					$invoice_discount=($selling_price_with_tax/$invoice_data_obj->total_price_without_shipping_price)*100;
					
					$each_invoice_amount=$invoice_data_obj->promotion_invoice_discount*$invoice_discount/100;
					/*
					$pdf->SetFont('Arial','B',9);
					$pdf->Cell(90,6,'Invoice Discount','TBL',0,'L',0); 
					$pdf->Cell(20,6,'','TB',0,'L',0);
					$pdf->Cell(40,6,'','TB',0,'L',0);
					//$pdf->Cell(20,6,'','TBL',0,'L',0);
					//$pdf->Cell(20,6,'','TBL',0,'L',0);
					$pdf->Cell(40,6,$each_invoice_amount,'TRBL',0,'L',0);

					$pdf->Ln();
					*/
					//$total_with_tax=round($total_with_tax-$each_invoice_amount,2);
					$total_invoice_value=round(($total_invoice_value-$each_invoice_amount));
					
				}
			/*$pdf->SetDrawColor(0,0,0);
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(70,8,'','TLB',0,'R',0);
			$pdf->Cell(20,8,'','TB',0,'R',0); 
			$pdf->Cell(40,8,'Sub Total: '.number_format($total_without_tax,2,'.',''),'TRB',0,'R',0);
			$pdf->Ln();
			//$pdf->SetDrawColor(213,213,213);
			if($is_local_state=="no"){
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(70,8,'','TLB',0,'L',0);
				$pdf->Cell(20,8,'','B',0,'R',0); 
				$pdf->Cell(40,8,'IGST '.$tax_percent.'%: '.number_format($total_tax_value,2,'.',''),'TRB',0,'R',0);
				$pdf->Ln();
			}
			else{
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(70,8,'','TLB',0,'L',0);
				$pdf->Cell(20,8,'','B',0,'R',0); 
				$pdf->Cell(40,8,'CGST '.round(($tax_percent/2),2).'%: '.number_format(round($total_tax_value/2,2),2,'.',''),'TRB',0,'R',0);
				$pdf->Ln();
				
				
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(70,8,'','TLB',0,'L',0);
				$pdf->Cell(20,8,'','B',0,'R',0); 
				$pdf->Cell(40,8,'SGST '.round(($tax_percent/2),2).'%: '.number_format(round($total_tax_value/2,2),2,'.',''),'TRB',0,'R',0);
				$pdf->Ln();
			}*/
			
			/* invoice coupon applied with shipping price without any tax deduction */
                        if($invoice_data_obj->invoice_coupon_status=='1' && $type_of_order==''){
                            
                                $inc_txt.='(';
                                $inc_txt.=($invoice_data_obj->invoice_coupon_type=='2') ? curr_txt : ''.' ';
                                $inc_txt.=round($invoice_data_obj->invoice_coupon_value);
                                $inc_txt.=($invoice_data_obj->invoice_coupon_type=='1') ? '%' : 'Flat' ;
                                $inc_txt.=')';

                                $g_t=($invoice_data_obj->total_price_without_shipping_price+$invoice_data_obj->total_shipping_charge);//actual grand total
                                $sku_percent=(($g_tot/$g_t)*100);
                                //$invoice_data_obj->invoice_coupon_reduce_price
                                
                                $each_invoice_coupon_discount=round($invoice_data_obj->invoice_coupon_reduce_price*($sku_percent/100),2);
                                
                                $ne=round((($invoice_data_obj->invoice_coupon_reduce_price/$g_t)*$g_tot),2);
                                
                                // '||'.$g_tot.'||'.$g_t.'||'.$sku_percent.'||'.$ne.'||';
                                
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(70,8,'','TLB',0,'L',0);
				$pdf->Cell(20,8,'','B',0,'R',0); 
				$pdf->Cell(40,8,'Invoice Coupon '.$inc_txt.':'.number_format($each_invoice_coupon_discount,2,'.',''),'TRB',0,'R',0);
				$pdf->Ln();
                                        
                                $total_invoice_value=round(($total_invoice_value-$each_invoice_coupon_discount));//total_coupon_used_amount can be reduced if sku coupon applied
			}
                        
                        if($invoice_data_obj->promotion_invoice_discount>0 && $type_of_order==''){
				
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(70,8,'','TLB',0,'L',0);
				$pdf->Cell(20,8,'','B',0,'R',0); 
				$pdf->Cell(40,8,'Invoice Value: '.number_format(round(($total_without_tax+$total_tax_value),2),2,'.',''),'TRB',0,'R',0);
				$pdf->Ln();
			//number_format(xx,2,'.','');
			
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(70,8,'','TLB',0,'L',0);
				$pdf->Cell(20,8,'','B',0,'R',0); 
				$pdf->Cell(40,8,'Invoice Discount: '.number_format($each_invoice_amount,2,'.',''),'TRB',0,'R',0);
				$pdf->Ln();
			}
			$pdf->SetDrawColor(0,0,0);
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(90,8,'','TLB',0,'L',0);
			$pdf->Cell(18,8,'Grand Total:','TB',0,'R',0); 
			$pdf->SetFont('Arial','',9);

			//if($active_orders_obj->ord_addon_products_status=='1'){
			if(0){
				$total_invoice_value_format=round($active_orders_obj->ord_addon_total_price);
			}else{
				$total_invoice_value_format=round($total_invoice_value);
			}
			
			$pdf->Cell(22,8,curr_txt."".number_format($total_invoice_value_format,2),'TRB',0,'R',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,8,'Total invoice value including GST (in words) : ','TRL',0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(130,8,$this->getIndianCurrency($total_invoice_value_format,2),'RBL',0,'C',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,8,'Thank you for shopping with '.SITE_NAME,0,0,'C',0);
			//$pdf->SetDrawColor(213,213,213);
			/*
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(100,10,'','',0,'L',0);
			$pdf->Cell(50,10,'Amount Due ('.curr_txt.'):','B',0,'R',0);
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(40,10,number_format($total_invoice_value,2,'.',''),'B',0,'L',0);
			$pdf->Ln();
			*/
			
			$pdf->SetFont('Arial','B',9);
			/*$pdf->Cell(190,7,'Notes','',0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(190,7,'1) Prices are Ex works,Bangalore. Transport charges extra.','',0,'L',0);
			$pdf->Ln();
			$pdf->Cell(190,7,'2) Payment 100% on 30 days from date of invoice can be remitted to our bank a/c.','',0,'L',0);
			$pdf->Ln();
			$pdf->Cell(190,7,'3) IDFC Bank A/c no. 10028187211','',0,'L',0);
			$pdf->Ln();
			$pdf->Cell(190,7,'4) IFSC Code : IDFB0080151','',0,'L',0);
			$pdf->Ln();*/
			
			
			
			

			//////////////// second page starts//////////////////////////
			$quantity_sub_refund=0;
			$order_return_decision_quantity=0;
			$replaced_quantity=0;
				
			$active_orders_obj=$this->Model_orders->get_data_from_history_orders($order_item_id);
			
			$second_page_flag=false;
			$purchased_quantity=$active_orders_obj->quantity;
			
			$order_replacement_decision_data_obj=$this->Model_orders->get_order_replacement_decision_data($order_item_id);
			$replacement_desired_data_obj=$this->Model_orders->get_replacement_desired_data($order_item_id);
			$orders_status_data_for_second_page=$this->Model_orders->get_orders_status_data($order_item_id);
			$order_return_decision_data_obj=$this->Model_orders->get_order_return_decision_data($order_item_id);
			$sub_refunded_details_from_returned_orders_table_arr=$this->get_sub_refunded_details_from_returned_orders_table($order_item_id);

			if(!empty($replacement_desired_data_obj) && !empty($order_replacement_decision_data_obj)){
				if($replacement_desired_data_obj->paid_by=="customer" && $replacement_desired_data_obj->payment_status=="paid" && $order_replacement_decision_data_obj->pickup_identifier=="before replace" && ($replacement_desired_data_obj->order_replacement_decision_customer_status=="cancel" || $order_replacement_decision_data_obj->refund_status=="refunded")){
					$second_page_flag=false;
					$replaced_quantity=0;
				}
				if($order_replacement_decision_data_obj->pickup_identifier=="before replace" && ($order_replacement_decision_data_obj->status=="replaced" || $order_replacement_decision_data_obj->status=="refunded")){
					$second_page_flag=true;
					$replaced_quantity=$order_replacement_decision_data_obj->quantity;
				}
				if($order_replacement_decision_data_obj->pickup_identifier=="after replace" && ($order_replacement_decision_data_obj->status=="replaced" || $order_replacement_decision_data_obj->status=="refunded")){
					$second_page_flag=true;
					$replaced_quantity=$order_replacement_decision_data_obj->quantity;
				}
				if(($replacement_desired_data_obj->order_replacement_decision_customer_status=="cancel" || $order_replacement_decision_data_obj->refund_status=="refunded") && $order_replacement_decision_data_obj->pickup_identifier=="after replace" && $orders_status_data_for_second_page->order_replacement_pickup==1){
					$second_page_flag=true;
					$replaced_quantity=$order_replacement_decision_data_obj->quantity;
				}
				if(($replacement_desired_data_obj->order_replacement_decision_customer_status=="cancel" || $order_replacement_decision_data_obj->refund_status=="refunded") && $order_replacement_decision_data_obj->pickup_identifier=="after replace" && $orders_status_data_for_second_page->order_replacement_pickup==0){
					$second_page_flag=false;
					$replaced_quantity=0;
				}
				if(($replacement_desired_data_obj->order_replacement_decision_customer_status=="cancel" || $order_replacement_decision_data_obj->refund_status=="refunded") && $order_replacement_decision_data_obj->pickup_identifier=="before replace" && ($replacement_desired_data_obj->payment_status=="pending" || $replacement_desired_data_obj->payment_status=="")){
					$second_page_flag=false;
					$replaced_quantity=0;
				}
			}
			
			
			if(!empty($order_return_decision_data_obj)){
				if($order_return_decision_data_obj->status=="refunded"){
					$second_page_flag=true;
					$order_return_decision_quantity=$order_return_decision_data_obj->quantity;
				}
				else{
					$order_return_decision_quantity=0;
				}
				$quantity_sub_refund=0;
				if(!empty($sub_refunded_details_from_returned_orders_table_arr)){
				$second_page_flag=true;
				$quantity_sub_refund+=$sub_refunded_details_from_returned_orders_table_arr["quantity"];
				}
			}
			
			$availability_of_order_item_id_in_refund_and_replacement=$this->get_availability_of_order_item_id_in_refund_and_replacement($order_item_id);

				if($availability_of_order_item_id_in_refund_and_replacement=="yes"){
					$active_orders_obj->quantity=($active_orders_obj->quantity)-($replaced_quantity+($order_return_decision_quantity)+($quantity_sub_refund));
				}
				else{
					if(!empty($replacement_desired_data_obj) && !empty($order_replacement_decision_data_obj)){
					$active_orders_obj->quantity=($active_orders_obj->quantity)-($replaced_quantity);

						//replacement
						
					}
					if(!empty($order_return_decision_data_obj) || !empty($sub_refunded_details_from_returned_orders_table_arr)){
					$active_orders_obj->quantity=($active_orders_obj->quantity)-(($order_return_decision_quantity)+($quantity_sub_refund));
					}
				}
			
			
				 
					if($second_page_flag){
					// if(1){
					 
			
				$pdf->AddPage();
			$pdf->Ln();
			//$pdf->Ln();
			$logopath=base_url().$email_stuff_arr["pdflogo_emailtemplate"];
			$pdf->Image($logopath,10,15);
			
			
			$pdf->SetFont('Arial','',15);
			$pdf->Cell(130,10,'INVOICE',0,0,'R',0);
			$pdf->Ln();
			if($company_gstin!=""){
				$pdf->SetFont('Arial','',10);
				$pdf->SetTextColor(0,0,0);
				$pdf->Cell(130,5,'GST : '.$company_gstin,0,0,'L',0);
				$pdf->SetTextColor(0,0,0);
				$pdf->Ln();
			}
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,2,'',0,0,'R',0);
			$pdf->Ln();
			$pdf->Cell(60,5,'',0,0,'L',0);//Fully owned brand by
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(43,5,"Invoice Number:",0,0,'R',0);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(27,5,$active_orders_obj->invoice_number,0,0,'R',0);
			$pdf->Ln();
			// $pdf->SetFont('Arial','B',10);
			// $pdf->Cell(130,5,'MAGNUM LABEL',0,0,'L',0);
			// $pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_street1"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_street2"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_city_state"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,'India.',0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_customer_care_number"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_customer_care_email"],0,0,'L',0);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->setFillColor(0,0,0);
			$pdf->SetX(0);
			$pdf->Cell(150,0.5,'',0,0,'R',1);
			$pdf->setFillColor(255,255,255);
			$pdf->Ln();
			$pdf->Cell(130,7,'',0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->SetTextColor(0,0,0);


                        $pdf->SetFont('Arial','B',10);
                        $pdf->Cell(25,5,'Billing Name:',0,0,'L',0);
                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(55,5,$shipping_add_obj->customer_name,0,0,'L',0);
                        $pdf->SetTextColor(0,0,0);
                        $pdf->SetFont('Arial','B',10);
                        $pdf->Cell(25,5,'Order Date:',0,0,'L',0);
                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(25,5,date("D, j M, Y",strtotime($active_orders_obj->timestamp)),0,0,'L',0);
                        $pdf->Ln();

                        $pdf->SetFont('Arial','B',10);
                        $pdf->Cell(30,5,'Customer GSTIN:',0,0,'L',0);
                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(50,5,$shipping_add_obj->customer_gstin,0,0,'L',0);
                        $pdf->SetFont('Arial','B',10);
                        $pdf->Cell(25,5,'Invoice Date:',0,0,'L',0);
                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(25,5,date("D, j M, Y"),0,0,'L',0);
                        $pdf->Ln();

                        $pdf->SetFont('Arial','B',10);
                        $pdf->Cell(30,5,'Billing Address:',0,0,'L',0);
                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(50,5,'',0,0,'L',0);
                        $pdf->SetTextColor(0,0,0);
                        $pdf->SetFont('Arial','B',10);
                        $pdf->Cell(25,5,'Contact No:',0,0,'L',0);
                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(25,5,"+91 ".wordwrap($shipping_add_obj->mobile , 5 , ' ' , true ),0,0,'L',0);
                        $pdf->Ln();

                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(80,4,$shipping_add_obj->address1,0,0,'L',0);
                        $pdf->SetTextColor(0,0,0);
                        $pdf->Ln();

                        $pdf->SetFont('Arial','',10);
                        $pdf->MultiCell(80,4,$shipping_add_obj->address2,0,'L',0,0);

                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(80,4,$shipping_add_obj->city,0,0,'L',0);
                        $pdf->SetTextColor(0,0,0);
                        $pdf->SetFont('Arial','B',10);
                        $pdf->Cell(25,4,'',0,0,'L',0);
                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(25,4,'',0,0,'L',0);
                        $pdf->Ln();

                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(80,4,$shipping_add_obj->state.", ".$shipping_add_obj->country." ".$shipping_add_obj->pincode,0,0,'L',0);
                        $pdf->SetTextColor(0,0,0);
                        $pdf->Ln();
                        $pdf->Ln();

                        $pdf->SetLineWidth(0.1);
			$pdf->SetDrawColor(0,0,0);
			//$pdf->SetFillColor(0,0,0);
			//$pdf->SetTextColor(255,255,255);
			$pdf->SetFont('Arial','B',9);

			$pdf->Cell(50,8,'Items','TLB',0,'L',true);
			$pdf->Cell(20,8,'HSN Code','TB',0,'L',true);
			$pdf->Cell(20,8,'Quantity','TB',0,'L',true);
			$pdf->Cell(20,8,'Price ('.curr_txt.')','TB',0,'L',true);
			$pdf->Cell(20,8,'Amt. ('.curr_txt.') ','TRB',0,'L',true);
			$pdf->Ln();
				$pdf->SetFillColor(255,255,255);
			$pdf->SetTextColor(0,0,0);

			//$pdf->SetDrawColor(213,213,213);
			//$pdf->SetLineWidth(0.5);
			
				$total_quantity=0;
				$total_without_tax=0;
				$total_tax_percent=0;
				$total_tax_amount=0;
				$total_with_tax=0;

				$product_info_obj=$this->get_product_info_by_product_id($active_orders_obj->product_id);
                                $inventory_info_obj=$this->get_inventory_info_by_inventory_id($active_orders_obj->inventory_id);
				//$subcat_info_obj=$this->get_subcat_info_by_product_id($active_orders_obj->product_id);
				//$pdf->Cell(30,6,$subcat_info_obj->subcat_name,0,0,'L',0); 
				
				$pro_name=($inventory_info_obj->sku_name!='') ? $inventory_info_obj->sku_name :$product_info_obj->product_name ;
				$no_of_lines_product_desc_sp=1;
			$product_desc_sp=$productfullinfo."".$pro_name;
						$line_product_desc_sp=array();
			$line_product_desc_sp[$no_of_lines_product_desc_sp]=substr($product_desc_sp,0,50);
			if(strlen(substr($product_desc_sp,50,50))!=0){
				$no_of_lines_product_desc_sp=2;
				$line_product_desc_sp[$no_of_lines_product_desc_sp]=substr($product_desc_sp,50,50);
			}
			if(strlen(substr($product_desc_sp,100,50))!=0){
				$no_of_lines_product_desc_sp=3;
				$line_product_desc_sp[$no_of_lines_product_desc_sp]=substr($product_desc_sp,100,50);
			}
			for($i=1;$i<=count($line_product_desc_sp);$i++){
				$pdf->SetFont('Arial','B',9);
				
				
					//////////////////////////////////////////
					$inventory_info_obj=$this->get_inventory_info_by_inventory_id($active_orders_obj->inventory_id);
			$inventory_dimentions="";
			$color_attribute_is_there="no";
			if(strtolower($inventory_info_obj->attribute_1)=="color" || strtolower($inventory_info_obj->attribute_2)=="color" || strtolower($inventory_info_obj->attribute_3)=="color" || strtolower($inventory_info_obj->attribute_4)=="color"){
				$color_attribute_is_there="yes";
				if(strtolower($inventory_info_obj->attribute_1)=="color"){
					$inventory_dimentions.=" - ".explode(":",$inventory_info_obj->attribute_1_value)[0];
				}
				else if(strtolower($inventory_info_obj->attribute_2)=="color"){
					$inventory_dimentions.=" - ".explode(":",$inventory_info_obj->attribute_2_value)[0];
				}
				else if(strtolower($inventory_info_obj->attribute_3)=="color"){
					$inventory_dimentions.=" - ".explode(":",$inventory_info_obj->attribute_3_value)[0];
				}
				else if(strtolower($inventory_info_obj->attribute_4)=="color"){
					$inventory_dimentions.=" - ".explode(":",$inventory_info_obj->attribute_4_value)[0];
				}
			}
			else{
			/////////////////////////////////	
				
				$noncolor_attribute_first_is_there="no";
				$noncolor_attribute_second_is_there="no";
				if(strtolower($inventory_info_obj->attribute_1)!="color"){
					$noncolor_attribute_first_is_there="yes";
					$inventory_dimentions.=" - ".$inventory_info_obj->attribute_1_value;
				}
				if(strtolower($inventory_info_obj->attribute_2)!="color"){
					if($noncolor_attribute_first_is_there=="yes"){
						$noncolor_attribute_second_is_there="yes";
						if($inventory_info_obj->attribute_2_value!=""){
							$inventory_dimentions.=" (".$inventory_info_obj->attribute_2_value.")";
						}
					}
					else{
						$noncolor_attribute_first_is_there="yes";
						$inventory_dimentions.=" - ".$get_free_item_data_image_path_attributes_arr["attribute_2_value"];
					}
				}
				if(strtolower($inventory_info_obj->attribute_3)!="color"){
					if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
						if($noncolor_attribute_first_is_there=="yes"){
							$noncolor_attribute_second_is_there="yes";
							if($inventory_info_obj->attribute_3_value!=""){
								$inventory_dimentions.=" (".$inventory_info_obj->attribute_3_value.")";
							}
						}
						else{
							$noncolor_attribute_first_is_there="yes";
							$inventory_dimentions.=" - ".$inventory_info_obj->attribute_3_value;
						}
					}
				}
				if(strtolower($inventory_info_obj->attribute_4)!="color"){
					if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
						if($noncolor_attribute_first_is_there=="yes"){
							$noncolor_attribute_second_is_there="yes";
							if($inventory_info_obj->attribute_4_value!=""){
								$inventory_dimentions.=" (".$inventory_info_obj->attribute_4_value.")";
							}
						}
						else{
							$noncolor_attribute_first_is_there="yes";
							$inventory_dimentions.=" - ".$inventory_info_obj->attribute_4_value;
						}
					}
				}
			
			//////////////////////////////
			
			}
			$color_attribute_is_there="no";
			$noncolor_attribute_first_is_there="no";
			$noncolor_attribute_second_is_there="no";
			
			///////////////////////////////////////////
			
			$product_dimension_sku=$line_product_desc_sp[$i]." ".$inventory_dimentions;
			$more_than_25_chars="no";
			if(strlen($product_dimension_sku)>=25){
				//$pdf->SetFont('Arial','B',5);
				$more_than_25_chars="yes";
			}
			else{
				//$pdf->SetFont('Arial','',9);
				$more_than_25_chars="no";
			}
			//$pdf->Cell(50,8,'Micro Flute Box - Brown (250L X 165W X 65H mm)','BL',0,'L',0);
			
			
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(15,5,"SKU ID : ",'L',0,'L',0);
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(115,5,$inventory_info_obj->sku_id,'R',0,'L',0);
			$pdf->Ln();
			
			
			
			if($more_than_25_chars!="yes"){
				$pdf->Cell(50,5,$line_product_desc_sp[$i]." ".$inventory_dimentions,'BL',0,'L',0);
				$pdf->Cell(20,5,'','B',0,'L',0);
			}
			else{
				$pdf->Cell(50,5,$line_product_desc_sp[$i],'L',0,'L',0);
				$pdf->Cell(20,5,'',0,0,'L',0);
			}
				if($i==1){

			$pdf->SetFont('Arial','',9);
			if($more_than_25_chars=="yes"){
				$pdf->Cell(20,5,$active_orders_obj->quantity,0,0,'C',0);
			}
			else{
				$pdf->Cell(20,5,$active_orders_obj->quantity,'B',0,'C',0);
			}
			$item_charge_replacement=0;
				$refunded_details_from_returned_orders_table_arr=$this->get_refunded_details_from_returned_orders_table($active_orders_obj->order_item_id);
				$sub_refunded_details_from_returned_orders_table_arr=$this->get_sub_refunded_details_from_returned_orders_table($active_orders_obj->order_item_id);
				
				if(!empty($refunded_details_from_returned_orders_table_arr)){
					
					$item_price_deducton_each=(($refunded_details_from_returned_orders_table_arr['item_price_without_concession']*$refunded_details_from_returned_orders_table_arr['item_deduction_percentage'])/100);
					$item_charge_replacement+=$item_price_deducton_each;
					////addded//////

				}
		
				if(!empty($sub_refunded_details_from_returned_orders_table_arr)){

					$item_price_deducton_each=$sub_refunded_details_from_returned_orders_table_arr['deducted_item_price'];
					$item_charge_replacement-=$item_price_deducton_each;
				}
				
				if($active_orders_obj->promotion_available==1){
					if($active_orders_obj->individual_price_of_product_without_promotion!='' && $active_orders_obj->individual_price_of_product_without_promotion!=0){
						$active_orders_obj->product_price=$active_orders_obj->individual_price_of_product_without_promotion;
					}else{
						$active_orders_obj->product_price=($active_orders_obj->subtotal/$purchased_quantity);
					}
					
					if(($active_orders_obj->default_discount>0 && $active_orders_obj->promotion_discount>0)){
						$active_orders_obj->product_price=($active_orders_obj->subtotal/$purchased_quantity);
					}
				}
				
				//replacement cash back////////
				$individual_cashback=0;
				if($active_orders_obj->promotion_cashback>0){
					
					$itemprice=(($active_orders_obj->quantity*$active_orders_obj->product_price)+$item_charge_replacement);
					
					
					if($order_item_obj->status_of_refund=="refunded"){
						$amount_without_cashback=($active_orders_obj->subtotal-$order_item_obj->amount_to_refund);
						$item_price_filled_cashb=0;
						
						if(!empty($order_replacement_decision_data_obj)){
							
							$replaced_value=($order_replacement_decision_data_obj->return_value_each_inventory_id*$order_replacement_decision_data_obj->quantity);
							$individual_cashback=($order_item_obj->amount_to_refund/$purchased_quantity);
	
							
						}else{
							
							if(!empty($refunded_details_from_returned_orders_table_arr)){
								$individual_cashback=($order_item_obj->amount_to_refund/$purchased_quantity);
							}

						}
						
					}else{
						
						$itemprice=(($active_orders_obj->quantity*$active_orders_obj->product_price)+$item_charge_replacement);
					}
					
				}else{
					
					$itemprice=(($active_orders_obj->quantity*$active_orders_obj->product_price)+$item_charge_replacement);
				}
				
					$selling_price_without_tax=round((($itemprice*100)/(100+$active_orders_obj->tax_percent)),2);

					$tax_percent=round($active_orders_obj->tax_percent,2);
					
					$tax_amount=round((($selling_price_without_tax*$tax_percent)/100),2);
					
					$selling_price_with_tax=round(($itemprice),2);
					
					if($availability_of_order_item_id_in_refund_and_replacement=="yes"){

							////added////
							if(!empty($refunded_details_from_returned_orders_table_arr)){
								if($refunded_details_from_returned_orders_table_arr['order_item_invoice_discount_value_each']>0){
									$order_item_invoice_discount_value=($refunded_details_from_returned_orders_table_arr['quantity']*$refunded_details_from_returned_orders_table_arr['order_item_invoice_discount_value_each']);
									
								}
							}
							////added////
					}
				$total_quantity+=$active_orders_obj->quantity;
				$total_without_tax+=$selling_price_without_tax;
				$total_tax_amount+=$tax_amount;
				$total_with_tax+=$selling_price_with_tax;
				if($more_than_25_chars=="yes"){
					$pdf->Cell(20,5,number_format($selling_price_without_tax,2,'.',''),0,0,'L',0);
					//$pdf->Cell(20,6,$tax_percent,'L',0,'L',0);
					//$pdf->Cell(20,6,$tax_amount,'L',0,'L',0);
					$pdf->Cell(20,5,number_format($selling_price_without_tax,2,'.',''),'R',0,'R',0);
				}
				else{
					$pdf->Cell(20,5,number_format($selling_price_without_tax,2,'.',''),'B',0,'L',0);
					//$pdf->Cell(20,6,$tax_percent,'L',0,'L',0);
					//$pdf->Cell(20,6,$tax_amount,'L',0,'L',0);
					$pdf->Cell(20,5,number_format($selling_price_without_tax,2,'.',''),'RB',0,'R',0);
				}
				if($more_than_25_chars=="yes"){
					$pdf->Ln();
					$pdf->Cell(130,5,$inventory_dimentions,'RBL',0,'L',0);
				}
			
			}else{
					$pdf->Cell(20,5,'',0,0,'L',0); 
					$pdf->Cell(20,5,'',0,0,'L',0); 
					//$pdf->Cell(20,6,'','L',0,'L',0); 
					//$pdf->Cell(20,6,'','L',0,'L',0); 
					$pdf->Cell(20,5,'','RB',0,'R',0); 
				}
			$pdf->Ln();
			}
				
			
				
				if($active_orders_obj->promotion_quote!="" && $type_of_order==""){
					if(($active_orders_obj->promotion_quote!=$active_orders_obj->promotion_default_discount_promo) && ($active_orders_obj->quantity>=$active_orders_obj->promotion_minimum_quantity)){
						$promo_text=$active_orders_obj->promotion_quote;
						$promo_text = preg_replace('/\s\s+/', ' ', $promo_text);
						$promo_text =stripslashes($promo_text);
						$promo_text =iconv('UTF-8', 'windows-1252', $promo_text);
						$pdf->SetFont('Arial','I',9);
						if($active_orders_obj->promotion_default_discount_promo!="" && $active_orders_obj->default_discount>0){
				
							$pdf->Cell(50,8,$promo_text,'BL',0,'L',0); 
							$pdf->Cell(20,8,'','B',0,'L',0);
						$pdf->Cell(20,8,'','B',0,'C',0);
						$pdf->Cell(20,8,'','B',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						$pdf->Cell(20,8,'','RB',0,'R',0);
						}else{
							$pdf->Cell(50,8,$promo_text,'BL',0,'L',0); 
							$pdf->Cell(20,8,'','B',0,'L',0);
						$pdf->Cell(20,8,'','B',0,'C',0);
						$pdf->Cell(20,8,'','B',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						$pdf->Cell(20,8,'','RB',0,'R',0);
						}
						$pdf->Ln();
					}
				}

				if($active_orders_obj->promotion_default_discount_promo!="" && $active_orders_obj->default_discount>0 && $type_of_order==""){
					$promo_text=$active_orders_obj->promotion_default_discount_promo;
					$promo_text = preg_replace('/\s\s+/', ' ', $promo_text);
					$promo_text =stripslashes($promo_text);
					$promo_text =iconv('UTF-8', 'windows-1252', $promo_text);
					$pdf->SetFont('Arial','',9);
					$pdf->Cell(50,8,$promo_text,'BL',0,'L',0); 
					$pdf->Cell(20,8,'','B',0,'L',0);
						$pdf->Cell(20,8,'','B',0,'C',0);
						$pdf->Cell(20,8,'','B',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						//$pdf->Cell(20,6,'','TBL',0,'L',0);
						$pdf->Cell(20,8,'','RB',0,'R',0);
					$pdf->Ln();
				}
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(50,8,'Shipping Charge','BL',0,'L',0); 
			$pdf->Cell(20,8,'','B',0,'L',0);
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(20,8,$active_orders_obj->quantity,'B',0,'C',0);
				
				$shipping_charge_replacement=0;
				
				
				if(!empty($refunded_details_from_returned_orders_table_arr)){
					$shipping_charge_replacement+=$refunded_details_from_returned_orders_table_arr['shipping_with_deductions_concession'];
				}

				if(!empty($sub_refunded_details_from_returned_orders_table_arr)){
					$shipping_charge_replacement+=$sub_refunded_details_from_returned_orders_table_arr['shipping_with_deductions_concession'];
				}
				
				if(!empty($replacement_desired_data_obj) && !empty($order_replacement_decision_data_obj)){
					
					if($replacement_desired_data_obj->shipping_charge_applied_cancels_chk==1){
						 $shipping_charge_replacement+=($active_orders_obj->shipping_charge/$purchased_quantity)*$order_replacement_decision_data_obj->quantity;
					}
					else{
						$shipping_charge_replacement+=0;
					}
				}
				
				$shipping_charge_without_tax=round(((($active_orders_obj->shipping_charge-$shipping_charge_replacement)*100)/(100+$active_orders_obj->tax_percent)),2);
				$shipping_tax_percent=round($active_orders_obj->tax_percent,2);
				$shipping_tax_amount=round((($shipping_charge_without_tax*$tax_percent)/100),2);
				$shipping_charge_with_tax=round(($active_orders_obj->shipping_charge-$shipping_charge_replacement),2);

				if($invoice_data_obj->promotion_invoice_free_shipping>0 && $type_of_order==""){
					$shipping_charge_without_tax=0;
					$shipping_tax_percent=0;
					$shipping_tax_amount=0;
					$shipping_charge_with_tax=0;
					
					$pdf->Cell(20,8,"Shipping Charge",'B',0,'L',0);
					//$pdf->Cell(20,6,0,'TL',0,'L',0);
					//$pdf->Cell(20,6,0,'TL',0,'L',0);
					$pdf->Cell(20,8,"0.00",'RB',0,'R',0);
					$pdf->Ln();
					
					$promo_text="Shipping charge waived off";
					$pdf->SetFont('Arial','I',9);				
					$pdf->Cell(50,8,$promo_text,'BL',0,'L',0); 
					$pdf->Cell(20,8,'','B',0,'L',0);
					$pdf->Cell(20,8,'','B',0,'C',0);
					$pdf->Cell(20,8,'','B',0,'L',0);
					//$pdf->Cell(20,6,'','TBL',0,'L',0);
					//$pdf->Cell(20,6,'','TBL',0,'L',0);
					$pdf->Cell(20,8,'','RB',0,'R',0);
					$pdf->Ln();
				}else{
					$pdf->Cell(20,8,number_format($shipping_charge_without_tax,2,'.',''),'B',0,'L',0);
					//$pdf->Cell(20,6,$shipping_tax_percent,'TL',0,'L',0);
					//$pdf->Cell(20,6,$shipping_tax_amount,'TL',0,'L',0);
					$pdf->Cell(20,8,number_format($shipping_charge_without_tax,2,'.',''),'RB',0,'R',0);
					$pdf->Ln();
				}

				$total_without_tax+=$shipping_charge_without_tax;
				$total_tax_amount+=$shipping_tax_amount;
				$total_with_tax+=$shipping_charge_with_tax;
				
				$total_without_tax=round($total_without_tax,2);
				$total_tax_amount=round($total_tax_amount,2);
				$total_with_tax=round($total_with_tax,2);
				

			///////////////////
			$total_tax_value=round(($total_without_tax*$tax_percent)/100,2);
			$total_invoice_value=round(($total_without_tax+$total_tax_value),2);
			///////////////////
				$in_dis=0;		
				if($invoice_data_obj->promotion_invoice_discount>0 && $type_of_order==''){
					//only replacement
					
						if(!empty($refunded_details_from_returned_orders_table_arr) ){
							$refunded_discount=($refunded_details_from_returned_orders_table_arr['quantity']*$refunded_details_from_returned_orders_table_arr['order_item_invoice_discount_value_each']);

							if($total_quantity>0){
								$in_dis=($total_quantity*$refunded_details_from_returned_orders_table_arr['order_item_invoice_discount_value_each']);
							}
						}elseif(!empty($order_replacement_decision_data_obj->order_item_invoice_discount_value_each)){
							$refunded_discount=($order_replacement_decision_data_obj->quantity*$order_replacement_decision_data_obj->order_item_invoice_discount_value_each);

							if($total_quantity>0){
								$in_dis=($total_quantity*$order_replacement_decision_data_obj->order_item_invoice_discount_value_each);
							}
						}
					
					/*
					$pdf->SetFont('Arial','B',9);
					$pdf->Cell(170,6,"Invoice Discount ",'TL',0,'L',0); 
					$pdf->Cell(20,6,$in_dis,'TRL',0,'L',0);
					$pdf->Ln();
					*/
					//$total_with_tax=round($total_with_tax-$in_dis,2);
					$total_invoice_value=round($total_invoice_value-$in_dis,2);
				}
				
				
				if($individual_cashback>0){
					if($total_quantity>0){
						$in_dis_individual_cashback=($total_quantity*$individual_cashback);
					}
					/*
					$pdf->SetFont('Arial','B',9);
					$pdf->Cell(170,6,"Invoice Cashback ",'TL',0,'L',0); 
					$pdf->Cell(20,6,$in_dis_individual_cashback,'TRL',0,'L',0);
					$pdf->Ln();
					*/
					//$total_with_tax=round($total_with_tax-$in_dis_individual_cashback,2);
					$total_invoice_value=round($total_invoice_value-$in_dis_individual_cashback);
					
				}
				
				
				
				
				
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(70,8,'','TLB',0,'R',0);
			$pdf->Cell(20,8,'','B',0,'R',0); 
			$pdf->Cell(40,8,'Sub Total: '.number_format($total_without_tax,2,'.',''),'TRB',0,'R',0);
			$pdf->Ln();
			
			if($is_local_state=="no"){
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(70,8,'','TLB',0,'L',0);
				$pdf->Cell(20,8,'','B',0,'R',0); 
				$pdf->Cell(40,8,'IGST '.$tax_percent.'%: '.number_format($total_tax_value,2,'.',''),'TRB',0,'R',0);
				$pdf->Ln();
			}
			else{
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(70,8,'','TLB',0,'L',0);
				$pdf->Cell(20,8,'','B',0,'R',0); 
				$pdf->Cell(40,8,'CGST: '.round(($tax_percent/2),2).'%: '.number_format(round($total_tax_value/2,2),2,'.',''),'TRB',0,'R',0);
				$pdf->Ln();
				
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(70,8,'','TLB',0,'L',0);
				$pdf->Cell(20,8,'','B',0,'R',0); 
				$pdf->Cell(40,8,'SGST: '.round(($tax_percent/2),2).'%: '.number_format(round($total_tax_value/2,2),2,'.',''),'TRB',0,'R',0);
				$pdf->Ln();
			}
				
			
			
			
			
		
			
			if($invoice_data_obj->promotion_invoice_discount>0 && $type_of_order==''){
				//only replacement
				
					if(!empty($refunded_details_from_returned_orders_table_arr) ){
						$refunded_discount=($refunded_details_from_returned_orders_table_arr['quantity']*$refunded_details_from_returned_orders_table_arr['order_item_invoice_discount_value_each']);

						if($total_quantity>0){
							$in_dis=($total_quantity*$refunded_details_from_returned_orders_table_arr['order_item_invoice_discount_value_each']);
						}
					}elseif(!empty($order_replacement_decision_data_obj->order_item_invoice_discount_value_each)){
						$refunded_discount=($order_replacement_decision_data_obj->quantity*$order_replacement_decision_data_obj->order_item_invoice_discount_value_each);

						if($total_quantity>0){
							$in_dis=($total_quantity*$order_replacement_decision_data_obj->order_item_invoice_discount_value_each);
						}
					}
				
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(70,8,'','TLB',0,'L',0);
				$pdf->Cell(20,8,'','B',0,'R',0); 
				$pdf->Cell(40,8,'Invoice Value: '.number_format(round(($total_without_tax+$total_tax_value),2),2,'.',''),'TRB',0,'R',0);
				$pdf->Ln();
			
				
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(70,8,'','TLB',0,'L',0);
				$pdf->Cell(20,8,'','B',0,'R',0); 
				$pdf->Cell(40,8,'Invoice Discount: '.number_format($in_dis,2,'.',''),'TRB',0,'R',0);
				$pdf->Ln();
			
			
			}
			
			
			if($individual_cashback>0){
				if($total_quantity>0){
					$in_dis_individual_cashback=($total_quantity*$individual_cashback);
				}
				$pdf->SetFont('Arial','B',9);
				$pdf->Cell(70,8,'','TLB',0,'L',0);
				$pdf->Cell(20,8,"",'B',0,'L',0); 
				$pdf->Cell(40,8,'Invoice Cashback: '.$in_dis_individual_cashback,'TRB',0,'R',0);
				$pdf->Ln();
				
			}
			$pdf->SetDrawColor(0,0,0);
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(90,8,'','TLB',0,'L',0);
			$pdf->Cell(18,8,'Total Amount: ','TB',0,'R',0); 
			$pdf->SetFont('Arial','',9);
			$total_invoice_value_format=round($total_invoice_value);
			$pdf->Cell(22,8,curr_sym." ".number_format($total_invoice_value_format,2),'TRB',0,'R',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,8,'Total invoice value including GST (in words) : ','TRL',0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(130,8,$this->getIndianCurrency($total_invoice_value_format,2),'RBL',0,'C',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,8,'Thank you for shopping with '.SITE_NAME,0,0,'C',0);
			//$pdf->SetDrawColor(213,213,213);
			
			
			$pdf->SetFont('Arial','B',9);
			/*$pdf->Cell(190,7,'Notes','',0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(190,7,'1) Prices are Ex works,Bangalore. Transport charges extra.','',0,'L',0);
			$pdf->Ln();
			$pdf->Cell(190,7,'2) Payment 100% on 30 days from date of invoice can be remitted to our bank a/c.','',0,'L',0);
			$pdf->Ln();
			$pdf->Cell(190,7,'3) IDFC Bank A/c no. 10028187211','',0,'L',0);
			$pdf->Ln();
			$pdf->Cell(190,7,'4) IFSC Code : IDFB0080151','',0,'L',0);
			$pdf->Ln();
			*/
			
			
			
			
				
			}
			
			
			
			//third page//
			
			$order_item_cashback=0;
			$order_item_status_of_refund='';
			if($active_orders_obj->promotion_cashback>0 && $type_of_order=='' && $order=="delivered"){
				$order_item_obj=$this->Model_orders->get_details_of_order_item($active_orders_obj->order_item_id);
				$order_item_status_of_refund=$order_item_obj->status_of_refund;
			}
			if($active_orders_obj->cash_back_value>0 &&  $active_orders_obj->cash_back_value!='' && $type_of_order=='' && $order=="delivered" && $order_item_status_of_refund=="refunded"){
				//if(1){
				
				$order_item_cashback=$active_orders_obj->cash_back_value;
				
			$pdf->AddPage();
			$pdf->Ln();
			//$pdf->Ln();
			
			//////////////////////////
			$logopath=base_url().$email_stuff_arr["pdflogo_emailtemplate"];
			$pdf->Image($logopath,10,15);
			
			
			$pdf->SetFont('Arial','',15);
			$pdf->Cell(130,10,'INVOICE',0,0,'R',0);
			$pdf->Ln();
			if($company_gstin!=""){
				$pdf->SetFont('Arial','',10);
				$pdf->SetTextColor(0,0,0);
				$pdf->Cell(130,5,'GST : '.$company_gstin,0,0,'L',0);
				$pdf->SetTextColor(0,0,0);
				$pdf->Ln();
			}
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,2,'',0,0,'R',0);
			$pdf->Ln();
			$pdf->Cell(60,5,'',0,0,'L',0);//Fully owned brand by
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(43,5,"Invoice Number:",0,0,'R',0);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(27,5,$active_orders_obj->invoice_number,0,0,'R',0);
			$pdf->Ln();
			// $pdf->SetFont('Arial','B',10);
			// $pdf->Cell(130,5,'MAGNUM LABEL',0,0,'L',0);
			// $pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_street1"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_street2"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_city_state"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,'India.',0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_customer_care_number"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_customer_care_email"],0,0,'L',0);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->setFillColor(0,0,0);
			$pdf->SetX(0);
			$pdf->Cell(150,0.5,'',0,0,'R',1);
			$pdf->setFillColor(255,255,255);
			$pdf->Ln();
			$pdf->Cell(130,7,'',0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->SetTextColor(0,0,0);
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(25,5,'Billing Name:',0,0,'L',0);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(55,5,$shipping_add_obj->customer_name,0,0,'L',0);
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(25,5,'Order Date:',0,0,'L',0);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(25,5,date("D, j M, Y",strtotime($active_orders_obj->timestamp)),0,0,'L',0);
                $pdf->Ln();

                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(30,5,'Customer GSTIN:',0,0,'L',0);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(50,5,$shipping_add_obj->customer_gstin,0,0,'L',0);
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(25,5,'Invoice Date:',0,0,'L',0);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(25,5,date("D, j M, Y"),0,0,'L',0);
                $pdf->Ln();

                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(30,5,'Billing Address:',0,0,'L',0);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(50,5,'',0,0,'L',0);
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(25,5,'Contact No:',0,0,'L',0);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(25,5,"+91 ".wordwrap($shipping_add_obj->mobile , 5 , ' ' , true ),0,0,'L',0);
                $pdf->Ln();

                $pdf->SetFont('Arial','',10);
                $pdf->Cell(80,4,$shipping_add_obj->address1,0,0,'L',0);
                $pdf->SetTextColor(0,0,0);
                $pdf->Ln();

                $pdf->SetFont('Arial','',10);
                $pdf->MultiCell(80,4,$shipping_add_obj->address2,0,'L',0,0);

                $pdf->SetFont('Arial','',10);
                $pdf->Cell(80,4,$shipping_add_obj->city,0,0,'L',0);
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(25,4,'',0,0,'L',0);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(25,4,'',0,0,'L',0);
                $pdf->Ln();

                $pdf->SetFont('Arial','',10);
                $pdf->Cell(80,4,$shipping_add_obj->state.", ".$shipping_add_obj->country." ".$shipping_add_obj->pincode,0,0,'L',0);
                $pdf->SetTextColor(0,0,0);
                $pdf->Ln();
                $pdf->Ln();

			$pdf->SetLineWidth(0.1);
			$pdf->SetDrawColor(0,0,0);
			//$pdf->SetFillColor(0,0,0);
			//$pdf->SetTextColor(255,255,255);
			$pdf->SetFont('Arial','B',9);
			///////////////////////////////
				$pdf->Cell(70,8,'Promotion','TLB',0,'L',true);
				$pdf->Cell(20,8,'Price ('.curr_txt.') ','TB',0,'L',true);
				//$pdf->Cell(20,6,'Tax',1,0,'L',true);
				//$pdf->Cell(20,6,'Tax',1,0,'L',true);
				$pdf->Cell(40,8,'Total ('.curr_txt.') ','TRB',0,'L',true);
				$pdf->Ln();
				$pdf->SetFillColor(255,255,255);
				$pdf->SetTextColor(0,0,0);

				//$pdf->SetDrawColor(213,213,213);
				//$pdf->SetLineWidth(0.5);
				
				$total_quantity=0;
				$total_without_tax=0;
				$total_tax_percent=0;
				$total_tax_amount=0;
				$total_with_tax=0;
				
				$promo_text='';
				if($active_orders_obj->promotion_quote!=""){
					if(($active_orders_obj->promotion_quote!=$active_orders_obj->promotion_default_discount_promo)){
						$promo_text=$active_orders_obj->promotion_quote;
						$promo_text = preg_replace('/\s\s+/', ' ', $promo_text);
						$promo_text =stripslashes($promo_text);
						$promo_text =iconv('UTF-8', 'windows-1252', $promo_text);
					}
				}
				
				
				$pdf->SetFont('Arial','B',10);
				$pdf->Cell(70,10,$promo_text,'TLB',0,'L',0); 
				$pdf->SetFont('Arial','',10);			
				$pdf->Cell(20,10,$active_orders_obj->cash_back_value,'B',0,'L',0);
				//$pdf->Cell(20,6,"NA",'L',0,'L',0);
				//$pdf->Cell(20,6,"NA",'L',0,'L',0);
				$pdf->Cell(40,10,$active_orders_obj->cash_back_value,'TRB',0,'R',0);
				$pdf->Ln();
				//$pdf->SetFont('Arial','B',10);
				$pdf->Cell(90,10,'','TLB',0,'R',0); 
				$pdf->Cell(40,10,' Total: '.curr_sym.$active_orders_obj->cash_back_value,'TRB',0,'R',0);
				$pdf->Ln();
				
				
			
			$pdf->SetFont('Arial','B',9);
			/*	$pdf->Cell(190,7,'Notes','',0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(190,7,'1) Prices are Ex works,Bangalore. Transport charges extra.','',0,'L',0);
			$pdf->Ln();
			$pdf->Cell(190,7,'2) Payment 100% on 30 days from date of invoice can be remitted to our bank a/c.','',0,'L',0);
			$pdf->Ln();
			$pdf->Cell(190,7,'3) IDFC Bank A/c no. 10028187211','',0,'L',0);
			$pdf->Ln();
			$pdf->Cell(190,7,'4) IFSC Code : IDFB0080151','',0,'L',0);
			$pdf->Ln();
			*/
				
				

					//////////////////////////////////////////////////////
					
					
				}//sku cash back
				
				//surprise gift curr_code
				
				if($active_orders_obj->promotion_surprise_gift_type!='' && ($active_orders_obj->promotion_surprise_gift_type==curr_code)  && $type_of_order=='' && $order=="delivered" && $order_item_obj->status_of_refund=="refunded"){
				//if(1){
					
					$promo_text='';
					if($active_orders_obj->promotion_quote!=""){
						if(($active_orders_obj->promotion_quote!=$active_orders_obj->promotion_default_discount_promo)){
							$promo_text=$active_orders_obj->promotion_quote;
							$promo_text = preg_replace('/\s\s+/', ' ', $promo_text);
							$promo_text =stripslashes($promo_text);
							$promo_text =iconv('UTF-8', 'windows-1252', $promo_text);
						}
					}
					
					$pdf->AddPage();
			$pdf->Ln();
			//$pdf->Ln();
			/////////////////////
			$logopath=base_url().$email_stuff_arr["pdflogo_emailtemplate"];
			$pdf->Image($logopath,10,15);
			
			
			$pdf->SetFont('Arial','',15);
			$pdf->Cell(130,10,'INVOICE',0,0,'R',0);
			$pdf->Ln();
			if($company_gstin!=""){
				$pdf->SetFont('Arial','',10);
				$pdf->SetTextColor(0,0,0);
				$pdf->Cell(130,5,'GST : '.$company_gstin,0,0,'L',0);
				$pdf->SetTextColor(0,0,0);
				$pdf->Ln();
			}
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,2,'',0,0,'R',0);
			$pdf->Ln();
			$pdf->Cell(60,5,'',0,0,'L',0);//Fully owned brand by
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(43,5,"Invoice Number:",0,0,'R',0);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(27,5,$active_orders_obj->invoice_number,0,0,'R',0);
			$pdf->Ln();
			// $pdf->SetFont('Arial','B',10);
			// $pdf->Cell(130,5,'MAGNUM LABEL',0,0,'L',0);
			// $pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_street1"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_street2"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_city_state"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,'India.',0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_customer_care_number"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_customer_care_email"],0,0,'L',0);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->setFillColor(0,0,0);
			$pdf->SetX(0);
			$pdf->Cell(150,0.5,'',0,0,'R',1);
			$pdf->setFillColor(255,255,255);
			$pdf->Ln();
			$pdf->Cell(130,7,'',0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->SetTextColor(0,0,0);


                    $pdf->SetFont('Arial','B',10);
                    $pdf->Cell(25,5,'Billing Name:',0,0,'L',0);
                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(55,5,$shipping_add_obj->customer_name,0,0,'L',0);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Arial','B',10);
                    $pdf->Cell(25,5,'Order Date:',0,0,'L',0);
                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(25,5,date("D, j M, Y",strtotime($active_orders_obj->timestamp)),0,0,'L',0);
                    $pdf->Ln();

                    $pdf->SetFont('Arial','B',10);
                    $pdf->Cell(30,5,'Customer GSTIN:',0,0,'L',0);
                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(50,5,$shipping_add_obj->customer_gstin,0,0,'L',0);
                    $pdf->SetFont('Arial','B',10);
                    $pdf->Cell(25,5,'Invoice Date:',0,0,'L',0);
                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(25,5,date("D, j M, Y"),0,0,'L',0);
                    $pdf->Ln();

                    $pdf->SetFont('Arial','B',10);
                    $pdf->Cell(30,5,'Billing Address:',0,0,'L',0);
                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(50,5,'',0,0,'L',0);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Arial','B',10);
                    $pdf->Cell(25,5,'Contact No:',0,0,'L',0);
                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(25,5,"+91 ".wordwrap($shipping_add_obj->mobile , 5 , ' ' , true ),0,0,'L',0);
                    $pdf->Ln();

                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(80,4,$shipping_add_obj->address1,0,0,'L',0);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->Ln();

                    $pdf->SetFont('Arial','',10);
                    $pdf->MultiCell(80,4,$shipping_add_obj->address2,0,'L',0,0);

                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(80,4,$shipping_add_obj->city,0,0,'L',0);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Arial','B',10);
                    $pdf->Cell(25,4,'',0,0,'L',0);
                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(25,4,'',0,0,'L',0);
                    $pdf->Ln();

                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(80,4,$shipping_add_obj->state.", ".$shipping_add_obj->country." ".$shipping_add_obj->pincode,0,0,'L',0);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->Ln();
                    $pdf->Ln();


                    $pdf->SetLineWidth(0.1);
			$pdf->SetDrawColor(0,0,0);
			//$pdf->SetFillColor(0,0,0);
			//$pdf->SetTextColor(255,255,255);
			$pdf->SetFont('Arial','B',9);

			///////////////////////////////////////
				$pdf->Cell(70,8,'Promotion','TLB',0,'L',true);
				$pdf->Cell(20,8,'Price ('.curr_txt.') ','TB',0,'L',true);
				$pdf->Cell(40,8,'Total ('.curr_txt.') ','TRB',0,'L',true);
				$pdf->Ln();
				$pdf->SetFillColor(255,255,255);
			$pdf->SetTextColor(0,0,0);

			//$pdf->SetDrawColor(213,213,213);
			//$pdf->SetLineWidth(0.5);
			
				$price=0;
					
					
					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(70,10,$promo_text,'TLB',0,'L',0); 
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(20,10,$active_orders_obj->promotion_surprise_gift,'B',0,'L',0);
					$pdf->Cell(40,10,$active_orders_obj->promotion_surprise_gift,'TRB',0,'R',0);
					$pdf->Ln();
					//$pdf->SetFont('Arial','B',10);
					$pdf->Cell(90,10,'','TLB',0,'R',0); 
					$pdf->Cell(40,10,' Total: '.curr_sym.$active_orders_obj->promotion_surprise_gift,'TRB',0,'R',0);
					
					$pdf->Ln();
				
				$pdf->SetFont('Arial','B',9);
		/*	$pdf->Cell(190,7,'Notes','',0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(190,7,'1) Prices are Ex works,Bangalore. Transport charges extra.','',0,'L',0);
			$pdf->Ln();
			$pdf->Cell(190,7,'2) Payment 100% on 30 days from date of invoice can be remitted to our bank a/c.','',0,'L',0);
			$pdf->Ln();
			$pdf->Cell(190,7,'3) IDFC Bank A/c no. 10028187211','',0,'L',0);
			$pdf->Ln();
			$pdf->Cell(190,7,'4) IFSC Code : IDFB0080151','',0,'L',0);
			$pdf->Ln();
			
			*/
				
				
			
				}//surprise gift curr_code

				$filename="assets/pictures/invoices/Invoice_".$order_id.$order_item_id."_".$type_of_copy.".pdf";
				$pdf->Output($filename,'F');
				if($sendInvoice=="yes"){
					$this->sendInvoice($order_id,$order_item_id,$shipping_add_obj,$type_of_copy);
				}
				if($sendInvoice=="no"){
					echo true;
				}
			/////////////// second page starts ///////////////////////////
			
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	
	
	
	}
	
	public function sendInvoice($order_id,$order_item_id,$shipping_add_obj,$type_of_copy){
		$email_stuff_arr=email_stuff();
		$updateinfo = array("invoice_email" => 1);
		$this -> db -> where("order_item_id", $order_item_id);
		$this -> db -> update("active_orders", $updateinfo);
		
		$customer_info_obj=$this->Model_orders->get_customer_info_by_customer_id($shipping_add_obj->customer_id);
		
		$email=$customer_info_obj->email;
		$customer_name=$shipping_add_obj->customer_name;
		$email_body="We have attached your invoices should you need them in the future.";
		
		$invoicecopy='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"> <head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> <meta name="format-detection" content="telephone=no"/> <title>'.$email_stuff_arr["name_emailtemplate"].'</title> <style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style> </head> <body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"> <center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"> <tr> <table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"> <tbody> <tr> <td> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tbody> <tr> <td align="center" valign="top" width="500" style="padding:10px"> <table border="0" cellspacing="0" width="100%"> <tbody> <tr> <td valign="top"  align="center" style="padding:10px;"><img src="'.base_url.$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody> </table> </td></tr></tbody> </table> </td></tr>
		
		
		<tr> <td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi '.$customer_name.', </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">'.$email_body.'</p><br><p style="padding:0;margin:0;color:#565656;font-size: 15px;">Warm Regards, <br/><b>'.$email_stuff_arr["actualnameofcompany_emailtemplate"].'</b></p><br></td></tr>';
		
		
		$invoicecopy.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

		$invoicecopy.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';
			
		$invoicecopy.='</tbody> </table></tr></table></center> </body></html>';
		
		$mail = new PHPMailer;
		/*$mail->isSMTP();
		$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
		$mail->Port = '587';//  live - comment it 
		$mail->Auth = true;//  live - comment it 
		$mail->SMTPAuth = true;
		$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
		$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
		$mail->SMTPSecure = 'tls';*/
		$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
		$mail->FromName = $email_stuff_arr["name_emailtemplate"];
		$mail->addAddress($email, $customer_name);
		//$mail->AddCC("rajitkumar1974@gmail.com", "Rajit Kumar");               
		//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
		$mail->addReplyTo($email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
		$mail->WordWrap = 50;
		$mail->isHTML(true);
		$filepath="assets/pictures/invoices/Invoice_".$order_id.$order_item_id."_".$type_of_copy.".pdf";
	
		$mail->Subject = 'Invoice copy for '.$email_stuff_arr["name_emailtemplate"].' '.$order_id;
		$mail->Body    = $invoicecopy;
		$mail->AddAttachment($filepath);
		if($email != '')
		{		
			if(!$mail->send()){ echo "Not send ".$mail->ErrorInfo." ".$email;}
			else{ echo true;}
		}
	
	}
	
	public function generate_label_pdf(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			extract($this->input->post());
			//echo $order_item_id;
			$active_orders_obj=$this->Model_orders->get_data_from_active_orders($order_item_id);	
			$shipping_add_obj=$this->Model_orders->get_shipping_add_info_by_shipping_address_id($active_orders_obj->shipping_address_id);
			
			$vendor_obj=$this->Model_orders->get_vendor_info_by_vendor_id($active_orders_obj->vendor_id);
			$pdf = new FPDF('P','mm','A5');
			$email_stuff_arr=email_stuff();
			$pdf->AddPage();
			$pdf->SetFont('Arial','B',12);
			$logopath=base_url().$email_stuff_arr["pdflogo_emailtemplate"];
			$pdf->Image($logopath,5,7);
			$pdf->Cell(65,15,'',0,0,'L',0);
			$pdf->Cell(65,15,'',1,0,'L',0);
			$pdf->ln();
			$pdf->SetDrawColor(0, 162, 232);
			$pdf->SetLineWidth(0.1);
			$pdf->Cell(130,5,'','B',0,'L',0);
			$pdf->ln();
			
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(130,10,'Speed Post Advance Customer A/C No. : 3000040947',0,0,'L',0);
			$pdf->ln();

            $pdf->SetFont('Arial','',10);
			$pdf->Cell(50,8,'',0,0,'R',0);
			$pdf->Cell(60,8,'TO: '.$shipping_add_obj->customer_name,0,0,'L',0); 
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(50,6,'',0,0,'R',0);
			$pdf->Cell(60,6,'Address:',0,0,'L',0); 
			$pdf->Ln();
            $pdf->SetFont('Arial','',10);
			$pdf->Cell(50,6,'',0,0,'R',0);
            $pdf->SetFont('Arial','',10);
			$pdf->Cell(60,6,$shipping_add_obj->address1,0,0,'L',0); 
			$pdf->Ln();
            $pdf->SetFont('Arial','',10);
			$pdf->Cell(50,6,'',0,0,'R',0);
			//$pdf->Cell(60,6,$shipping_add_obj->address2,0,0,'L',0);
            $pdf->SetFont('Arial','',10);
            $pdf->MultiCell(90,6,$shipping_add_obj->address2,0,'L',0,0);

            //$pdf->Ln();
			$pdf->Cell(50,6,'',0,0,'R',0);
            $pdf->SetFont('Arial','',10);
			$pdf->Cell(60,6,"City: ".$shipping_add_obj->city,0,0,'L',0);
			$pdf->Ln();
			$pdf->Cell(50,6,'',0,0,'R',0);
            $pdf->SetFont('Arial','',10);
			$pdf->Cell(60,6,"State: ".$shipping_add_obj->state.', '.$shipping_add_obj->country,0,0,'L',0);
			$pdf->Ln();
			$pdf->Cell(50,6,'',0,0,'R',0);
            $pdf->SetFont('Arial','',10);
			$pdf->Cell(60,6,'Pincode: '.$shipping_add_obj->pincode,0,0,'L',0);
			$pdf->Ln();
			$pdf->Cell(50,6,'',0,0,'R',0);
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(15,6,'Mobile: ',0,0,'L',0); 
			$pdf->Cell(45,6,$shipping_add_obj->mobile,0,0,'L',0);
			//$pdf->Ln();
			$pdf->Ln();
			$pdf->Cell(130,10,'Reason for non delivery:',0,0,'L',0); 
			$pdf->Ln();
			$pdf->Cell(130,25,'',1,0,'L',0);
			$pdf->ln();
			
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,8,'FROM:',0,0,'L',0); 
			$pdf->ln();
            $pdf->SetFont('Arial','',10);
			$pdf->Cell(130,6,$vendor_obj->name,0,0,'L',0); 
			$pdf->Ln();
            $pdf->SetFont('Arial','',10);
			
            $pdf->SetFont('Arial','',10);
			$pdf->Cell(130,6,$vendor_obj->address1,0,0,'L',0); 
			$pdf->Ln();
            $pdf->SetFont('Arial','',10);
			$pdf->Cell(130,6,$vendor_obj->address2,0,0,'L',0); 
			$pdf->Ln();
            $pdf->SetFont('Arial','',10);
			$pdf->Cell(130,6,'Pincode: '.$vendor_obj->pincode,0,0,'L',0); 
			$pdf->Ln();
            $pdf->SetFont('Arial','',10);
			if($vendor_obj->landline!="" && $vendor_obj->landline!=$vendor_obj->mobile){
				$pdf->Cell(130,6,'Landline: '.$vendor_obj->landline,0,0,'L',0);
				$pdf->Ln();
			}
            $pdf->SetFont('Arial','',10);
			$pdf->Cell(130,6,'Mobile: '.$vendor_obj->mobile,0,0,'L',0);
			$pdf->Ln();
			$pdf->Cell(130,6,$vendor_obj->email,0,0,'L',0);
			$pdf->Ln();
			$pdf->Cell(130,6,'Order Number: '.$active_orders_obj->order_id,0,0,'L',0);
			$pdf->Ln();
			
			$filename="assets/pictures/labels/label_".$order_id.$order_item_id.".pdf";
			$pdf->Output($filename,'F');
			
			///////
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	/////////////////////////////////////////////////////
	public function cancels_issue_refund($order_item_id){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["menu_flag"]="";
			//$data["type_of_refund"]=$this->Model_orders->returns_issue_refund();
			$data["order_item_id"]=$order_item_id;
			$data["cancels_table_data"]=$this->Model_orders->get_data_cancels_table($order_item_id);
			$data["razorpay_paymentID"]=$this->Model_orders->get_razorpay_paymentID($order_item_id);

			//$data["refund_table_data"]=$this->Model_orders->get_data_refund_table($data["cancels_table_data"]->cancel_id);
			$data["refund_table_data"]=$data["cancels_table_data"];
			
			$data["cancelled_orders_table_data"]=$this->Model_orders->get_data_cancelled_orders_table($order_item_id);
			
			
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/cancels_issue_refund',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_bank_details($id){
		$bank_details=$this->Model_orders->get_bank_details($id);
		$ret_bank_details="";
		if(!empty($bank_details)){
			foreach($bank_details as $details){
				$ret_bank_details.='<ul class="list-unstyled small"><li>Bank Details</li><li>Bank Name : '.$details['bank_name'].'</li><li>Branch Name : '.$details['branch_name'].'</li><li>City : '.$details['city'].'</li><li>IFSC Code : '.$details['ifsc_code'].'</li><li> Account Number : '.$details['account_number'].'</li><li> Account Holder Name : '.$details['account_holder_name'].'</li><li> Mobile Number : '.$details['mobile_number'].'</li></ul>';
			}
		}
		return $ret_bank_details;
	}
	
	public function cancels_issue_full_refund(){
		$order_item_id=$this->input->post("order_item_id");
		$quantity_refunded=$this->input->post("quantity_refunded");
		
		$cancel_id=$this->input->post("cancel_id");
		$total_refund_without_concession=$this->input->post("total_refund_without_concession");
		
		
		$item_price_without_concession=$this->input->post("item_price_without_concession");
		$item_deduction_percentage=$this->input->post("item_deduction_percentage");
		$shipping_price_without_concession=$this->input->post("shipping_price_without_concession");
		$shipping_deduction_percentage=$this->input->post("shipping_deduction_percentage");
		$item_with_deductions_concession=$this->input->post("item_with_deductions_concession");
		$shipping_with_deductions_concession=$this->input->post("shipping_with_deductions_concession");
		
		
		$memo_to_buyer=$this->input->post("memo_to_buyer");
		$memo_to_seller=$this->input->post("memo_to_seller");
		$refund_method=$this->input->post("refund_method");
		$refund_bank_id=$this->input->post("refund_bank_id");
		$customer_id=$this->input->post("customer_id");

		$razorpay_refund_id=$this->input->post("razorpay_refund_id");
		$razorpay_refund_status=$this->input->post("razorpay_refund_status");
		$razorpay_paymentid=$this->input->post("paymentId");

		$flag=$this->Model_orders->cancels_issue_full_refund($order_item_id,$cancel_id,$total_refund_without_concession,$item_price_without_concession,$item_deduction_percentage,$shipping_price_without_concession,$shipping_deduction_percentage,$item_with_deductions_concession,$shipping_with_deductions_concession,$memo_to_buyer,$memo_to_seller,$refund_method,$refund_bank_id,$customer_id,$quantity_refunded,$razorpay_refund_id,$razorpay_refund_status,$razorpay_paymentid);
		echo $flag;
	}
	
	public function cancels_issue_partial_refund(){
		$order_item_id=$this->input->post("order_item_id");
		$quantity_refunded=$this->input->post("quantity_refunded");
		$cancel_id=$this->input->post("cancel_id");
		$item_price_without_concession=$this->input->post("order_amount_product");
		$item_deduction_percentage=$this->input->post("deductions_product");
		$shipping_price_without_concession=$this->input->post("order_amount_shipping");
		$shipping_deduction_percentage=$this->input->post("deductions_shipping");
		$total_refund_without_concession=$this->input->post("total_product_price_with_shipping");
		
		$item_with_deductions_concession=$this->input->post("amount_to_refund_product");
		$shipping_with_deductions_concession=$this->input->post("amount_to_refund_shipping");
		
		$total_refund_with_concession=$this->input->post("total_amount_to_refund");
		
		$refund_method=$this->input->post("refund_method");
		$refund_bank_id=$this->input->post("refund_bank_id");
		$customer_id=$this->input->post("customer_id");

        $razorpay_refund_id=$this->input->post("razorpay_refund_id");
        $razorpay_refund_status=$this->input->post("razorpay_refund_status");
        $razorpay_paymentid=$this->input->post("paymentId");

		$memo_to_buyer_partial_refund=$this->input->post("memo_to_buyer_partial_refund");
		$memo_to_seller_partial_refund=$this->input->post("memo_to_seller_partial_refund");
		$flag=$this->Model_orders->cancels_issue_partial_refund($order_item_id,$cancel_id,$item_price_without_concession,$item_deduction_percentage,$shipping_price_without_concession,$shipping_deduction_percentage,$total_refund_without_concession,$item_with_deductions_concession,$shipping_with_deductions_concession,$total_refund_with_concession,$refund_method,$refund_bank_id,$customer_id,$memo_to_buyer_partial_refund,$memo_to_seller_partial_refund,$quantity_refunded,$razorpay_refund_id,$razorpay_refund_status,$razorpay_paymentid);
		echo $flag;
	}
	
	public function cancels_refund_request_orders_accounts_incoming(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$this->Model_orders->notify_cancel_account_unset();
			$data["menu_flag"]="accounts_cancel_active_links";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/cancels_refund_request_orders_accounts_incoming',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function cancels_refund_request_orders_accounts_incoming_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_orders->cancels_refund_request_orders_accounts_incoming_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_orders->cancels_refund_request_orders_accounts_incoming_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$inventory_id=$queryRecordsObj->inventory_id;
				$image=$queryRecordsObj->image;
				$img_path=get_inventory_image_path($inventory_id,$image);

				$row[]="<img src='".base_url().$img_path."'  style='width: 100px;height: 122px;' >";
				$order_summary='<ul class="list-unstyled">';
				$order_summary.="<li>SKU:".$queryRecordsObj->sku_id."</li>";
                                if($queryRecordsObj->ord_sku_name!=''){
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
                                }else{
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
                                }
								$product_details=get_product_details($queryRecordsObj->product_id);
								if(!empty($product_details))
								$order_summary.=($product_details->product_description!='') ? "<li class='mt-10'><small>".$product_details->product_description.'</small></li>': '';
								

				$order_summary.="<li>Order Date:".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."</li>";
				$order_summary.="<li>Order ID:".$queryRecordsObj->order_id."</li>";
				$order_summary.="<li>Order Item ID:".$queryRecordsObj->order_item_id."</li>";
				$order_summary.='</ul>';
				$row[]=$order_summary;
				
				$cancels_tableObj=$this->Model_orders->get_data_cancels_table($queryRecordsObj->order_item_id);
				$cancellation_details='<ul class="list-unstyled">';
				$cancellation_details.="<li>Cancelled By:".$cancels_tableObj->cancelled_by."</li>";
				
				if(strlen($cancels_tableObj->cancel_reason_comments)>200){
					$cancellation_details.='<li>Comments:<div class="f-row margin-top more">'.str_split($cancels_tableObj->cancel_reason_comments,200)[0].'<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' .substr($cancels_tableObj->cancel_reason_comments,201). '</span>&nbsp;&nbsp;<a href="#" class="morelink" onclick="morelinkfun(this)">more</a></span></div></li>';
				}else{
					$cancellation_details.='<li>Comments:<div class="f-row margin-top more">'.$cancels_tableObj->cancel_reason_comments.'</div></li>';
				}
				
				$cancellation_details.="<li>".$queryRecordsObj->type_of_transaction."</li>";
				$cancellation_details.='</ul>';
				$row[]=$cancellation_details;
				
				///
				$refund_details_obj=$this->get_cancel_refund_details($queryRecordsObj->order_item_id);
				$refund_details='<ul class="list-unstyled">';
				$refund_details.="<li>Item Price:"." ".curr_sym." ".$refund_details_obj->item_with_deductions_concession."</li>";
				$refund_details.="<li>Shipping Price:"." ".curr_sym." ".$refund_details_obj->shipping_with_deductions_concession."</li>";
				
				$refund_details.="<li>Total Refund With Concession:"." ".curr_sym." ".$refund_details_obj->total_refund_with_concession."</li>";
				$refund_details.="<li>Refund Method:".$refund_details_obj->refund_method."</li>";
				$refund_details.='</ul>';
				$row[]=$refund_details;
				/////////////////
				$customer_add='<ul class="list-unstyled">';
				$customer_add.="<li class='text-info bold'>".$queryRecordsObj->customer_name."</li>";
				$customer_add.="<li>".$queryRecordsObj->address1."</li>";
				$customer_add.="<li>".$queryRecordsObj->address2."</li>";
				$customer_add.="<li>".$queryRecordsObj->city.",".$queryRecordsObj->state."</li>";
				$customer_add.="<li>".$queryRecordsObj->country."</li>";
				$customer_add.="<li>".$queryRecordsObj->pincode."</li>";
				$customer_add.="<li>".$queryRecordsObj->mobile."</li>";
				$customer_add.='</ul>';


                $customer_add.='<span id="razorpay_form_'.$queryRecordsObj->order_item_id.'" style="display:none;">
                                <input name="order_item_id" value="'.$queryRecordsObj->order_item_id.'" type="hidden">
                                <input name="request_type" value="refund" type="hidden">
                                <input name="amount" type="hidden" value="'.$refund_details_obj->total_refund_with_concession.'">
                                <input name="return_order_id" type="hidden" value="'.$queryRecordsObj->return_order_id.'">
				                <input name="paymentId" type="hidden" value="'.$queryRecordsObj->razorpayPaymentId.'">
                                <input type="hidden" class="razorpay_refund_id_'.$queryRecordsObj->order_item_id.'" name="razorpay_refund_id" value="">
                                <input type="hidden" class="razorpay_refund_status_'.$queryRecordsObj->order_item_id.'" name="razorpay_refund_status" value="">
                                <input type="hidden" name="order_status_id" value="'.$queryRecordsObj->orders_status_id.'">
           
                                <div class="form-group">
                                <label class="col-md-6">Razorpay Payment ID</label>
                                    <div class="col-md-6">
                                        '.$queryRecordsObj->razorpayPaymentId.'
                                    </div>
                                </div>
                                <div class="form-group">
                                <label class="col-md-6">Amount to be refunded:</label>
                                    <div class="col-md-6">
                                        '.curr_sym.$refund_details_obj->total_refund_with_concession.'
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="comments" placeholder="Enter Comments" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input type="submit" class="btn btn-success btn-xs btn-block req_btn_'.$queryRecordsObj->order_item_id.'" value="Submit">
                                    </div>
                                </div>
                                </span>
                                ';

                $row[]=$customer_add;
				
				/////////////////
				//////
				$actions="";
				$cancels_table_obj=$this->Model_orders->get_data_cancels_table($queryRecordsObj->order_item_id);
				//$refund_table_obj=$this->Model_orders->get_data_refund_table($cancels_table_obj->cancel_id);
				$refund_table_obj=$cancels_table_obj;
				if($refund_table_obj->refund_type=="Bank Transfer"){
					$actions.='<input type="button" value="Success" class="btn btn-success btn-xs btn-block" data-toggle="modal"  onclick="open_cancel_refund_success_fun('.$queryRecordsObj->order_item_id.')">';
				
					$actions.='<input type="button" value="Fail" class="btn btn-danger btn-xs btn-block" data-toggle="modal"  onclick="open_cancel_refund_failure_fun('.$queryRecordsObj->order_item_id.')">';
				}
				if($refund_table_obj->refund_type=="Wallet"){
					$actions.='<input type="button" value="Transfer to wallet" class="btn btn-info btn-xs btn-block" data-toggle="modal"  onclick="open_cancel_refund_wallet_fun('.$queryRecordsObj->order_item_id.')">';
				}

                if($refund_table_obj->refund_type=="Razorpay"){
                    $actions.='<input type="button" value="Refund request to Razorpay" class="btn btn-success btn-xs btn-block" data-toggle="modal"  onclick="open_cancel_refund_success_fun_razorpay('.$queryRecordsObj->order_item_id.')">';

                    $actions.='<input type="button" value="Fail" class="btn btn-danger btn-xs btn-block" data-toggle="modal"  onclick="open_cancel_refund_failure_fun('.$queryRecordsObj->order_item_id.')">';
                }

				$actions.='<input type="button" value="Print" class="btn btn-info btn-xs btn-block" onclick="printcontent('.$queryRecordsObj->order_item_id.')">';
				$row[]=$actions;
				//////////////////////
				$data[]=$row;
				$row=array();
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function cancels_refund_request_orders_accounts_incoming_success(){
		$order_item_id=$this->input->post("order_item_id");
		$cancels_refund_request_orders_accounts_incoming_obj=$this->Model_orders->cancels_refund_request_orders_accounts_incoming($order_item_id);
		
		?>
		 <input type="hidden" class="form-control" name="order_item_id" value="<?php echo $cancels_refund_request_orders_accounts_incoming_obj->order_item_id; ?>">
		 
            
						<div class="form-group">
							
							<div class="col-md-12">
								<input type="text" class="form-control" name="transaction_id" placeholder="Enter Transaction ID" required />
							</div>
						</div>
						<div class="form-group">
							
							<div class="col-md-12">
								<input type="text" class="form-control" name="transaction_date" id="transaction_date" placeholder="Transaction Date" required />
							</div>
						</div>
						<div class="form-group">
							
							<div class="col-md-12">
								<input id="transaction_time" name="transaction_time" type="text" class="time form-control" placeholder="Transaction Time" required />
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<input type="submit" class="btn btn-success btn-xs btn-block" value="Submit">
							</div>
						</div>
            
					 
				  <script>
				  $(document).ready(function(){
						
						$('#transaction_date').bootstrapMaterialDatePicker
						({
							time: false,
							clearButton: true
						});

						$('#transaction_time').bootstrapMaterialDatePicker
						({
							date: false,
							shortTime: true,
							use24hours: false,
							clearButton: true,
							format: 'hh:mm A'
						});

						$.material.init()
					});
				  </script>
                  
		<?php
	}
	
	public function cancels_refund_request_orders_accounts_incoming_failure(){
		$order_item_id=$this->input->post("order_item_id");
		$cancels_refund_request_orders_accounts_incoming_obj=$this->Model_orders->cancels_refund_request_orders_accounts_incoming($order_item_id);
		
		?>
		 <input type="hidden" class="form-control" name="order_item_id" value="<?php echo $cancels_refund_request_orders_accounts_incoming_obj->order_item_id; ?>">
		 
           
						<div class="form-group">
							<div class="col-md-12">
								<textarea class="form-control" name="refund_fail_comments" required placeholder="Write comments"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<input type="checkbox" name="allow_update_bank_details" value="allow_update_bank_details"> Allow customer to update bank details
							</div>
						</div>
					 <div class="form-group">
							<div class="col-md-12">
								<input type="submit" class="btn btn-danger btn-xs btn-block" value="Submit">
							</div>
						</div>
		<?php
	}
	
	public function cancels_refund_request_orders_accounts_incoming_wallet(){
		$order_item_id=$this->input->post("order_item_id");
		$cancels_refund_request_orders_accounts_incoming_obj=$this->Model_orders->cancels_refund_request_orders_accounts_incoming($order_item_id);
		
		?>
		 
		 <input type="hidden" class="form-control" name="order_item_id" value="<?php echo $cancels_refund_request_orders_accounts_incoming_obj->order_item_id; ?>">
		 

						<div class="form-group">
							<div class="col-md-12">Total Amount need to be refund: 
								<?php echo "".curr_sym." ".$refund_details_obj=$this->get_cancel_refund_details($cancels_refund_request_orders_accounts_incoming_obj->order_item_id)->total_refund_with_concession;?>
								<input type="hidden" name="total_amount_need_to_be_refund" value="<?php echo $refund_details_obj=$this->get_cancel_refund_details($cancels_refund_request_orders_accounts_incoming_obj->order_item_id)->total_refund_with_concession;?>">
								<input type="hidden" name="customer_id" value="<?php echo $cancels_refund_request_orders_accounts_incoming_obj->customer_id;?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<input type="checkbox" name="allow_customer_to_transfer" value="allow_customer_to_transfer">Allow customer to transfer to bank
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<textarea class="form-control" name="refund_wallet_comments" required placeholder="Write comments"></textarea>
							</div>
						</div>
					<div class="form-group">
							<div class="col-md-12">
								<input type="submit" class="btn btn-info btn-xs btn-block" value="Submit">
							</div>
						</div>
		<?php
	}
	
	public function cancels_refund_request_orders_accounts_incoming_printcontent(){
		$order_item_id=$this->input->post("order_item_id");
		$cancels_refund_request_orders_accounts_incoming_obj=$this->Model_orders->cancels_refund_request_orders_accounts_incoming($order_item_id);
		
		?>
		<ul class="list-unstyled">
		<li>Cancel ID: <?php echo $cancels_refund_request_orders_accounts_incoming_obj->cancel_id;?></li>
	<li>Refund Details:</li>
	</ul>
	<?php
		$refund_details_obj=$this->get_cancel_refund_details($cancels_refund_request_orders_accounts_incoming_obj->order_item_id);
		
		$refund_details="<ul class='list-unstyled'>";
		$refund_details.="<li>Item Price:"." ".curr_sym." ".$refund_details_obj->item_with_deductions_concession."</li>";
		$refund_details.="<li>Shipping Price:"." ".curr_sym." ".$refund_details_obj->shipping_with_deductions_concession."</li>";
		$refund_details.="<li>Total Refund With Concession:"." ".curr_sym." ".$refund_details_obj->total_refund_with_concession."</li>";
		$refund_details.="<li>Refund Method: ".$refund_details_obj->refund_method."</li>";
		$refund_details.="</ul>";
		echo $refund_details;
		$refund_bank_details=$this->get_bank_details($refund_details_obj->refund_bank_id);
		echo $refund_bank_details;
	?>
		<?php
	}
	
	
	
	public function get_cancel_refund_details($order_item_id){
		$refund_details_obj=$this->Model_orders->get_cancel_refund_details($order_item_id);
		return $refund_details_obj;
	}
	
	public function cancel_refund_success(){
		$order_item_id=$this->input->post("order_item_id");
		$transaction_id=$this->input->post("transaction_id");
		$transaction_date=$this->input->post("transaction_date");
		$transaction_time=$this->input->post("transaction_time");
		$flag=$this->Model_orders->cancel_refund_success($order_item_id,$transaction_id,$transaction_date,$transaction_time);
		echo $flag;
	}
    public function cancel_refund_success_razopay(){
        $order_item_id=$this->input->post("order_item_id");
        //$transaction_id=$this->input->post("transaction_id");
        $transaction_id=$this->input->post("razorpay_refund_id");
        $razorpay_refund_id=$this->input->post("razorpay_refund_id");
        $razorpay_refund_status=$this->input->post("razorpay_refund_status");
        $paymentId=$this->input->post("paymentId");
        $comments=$this->input->post("comments");

        $transaction_date=date("Y-m-d");
        $transaction_time=date("h:i A");
        $flag=$this->Model_orders->cancel_refund_success($order_item_id,$transaction_id,$transaction_date,$transaction_time,$razorpay_refund_id,$razorpay_refund_status);
        echo $flag;
    }
	
	public function cancel_refund_failure(){
		$order_item_id=$this->input->post("order_item_id");
		$refund_fail_comments=$this->input->post("refund_fail_comments");
		$allow_update_bank_details=$this->input->post("allow_update_bank_details");
		
		$flag=$this->Model_orders->cancel_refund_failure($order_item_id,$refund_fail_comments,$allow_update_bank_details);
		echo $flag;
	}
	
	public function cancel_refund_wallet(){
		$order_item_id=$this->input->post("order_item_id");
		$customer_id=$this->input->post("customer_id");
		$credit_bank_transfer=$this->input->post("allow_customer_to_transfer");
		$refund_wallet_comments=$this->input->post("refund_wallet_comments");
		$total_amount_need_to_be_refund=$this->input->post("total_amount_need_to_be_refund");
		///////////////////////
		$this->Model_orders->activate_wallet_customer($customer_id);
		/////
		/*get customer wallet id*/
		$wallet_summary=$this->Model_orders->get_customer_wallet_summary($customer_id);
		foreach($wallet_summary as $summary){
			$wallet_id=$summary['wallet_id'];
			$amount=$summary['wallet_amount'];
		}
		
		$transaction_details=$refund_wallet_comments;
		$debit="";
		$credit=$total_amount_need_to_be_refund;
		$amount=$credit+$amount;
		$wallet_transaction_id=$this->Model_orders->update_customer_wallet($wallet_id,$customer_id,$transaction_details,$debit,$credit,$amount,$credit_bank_transfer);
		$transaction_date=date("Y-m-d");
		$transaction_time=date("h:i A");
		
		$flag=$this->Model_orders->cancel_refund_success($order_item_id,$wallet_transaction_id,$transaction_date,$transaction_time);
		echo $flag;
		////////////////
	}
	
	public function cancels_refund_request_orders_refund_success(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["menu_flag"]="cancels_active_links";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/cancels_refund_request_orders_refund_success',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function cancels_refund_request_orders_refund_success_processing(){

		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_orders->cancels_refund_request_orders_refund_success_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_orders->cancels_refund_request_orders_refund_success_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]="<img src='".base_url().$queryRecordsObj->image."' width='50' height='80'>";
				$order_summary="<b>SKU:</b>".$queryRecordsObj->sku_id."<br>";
                                if($queryRecordsObj->ord_sku_name!=''){
                                    $order_summary.="<p class='text-info bold'>".$queryRecordsObj->ord_sku_name."</p>";
                                }else{
                                    $order_summary.="<p class='text-info bold'>".$queryRecordsObj->product_name."</p>";
                                }
								$product_details=get_product_details($queryRecordsObj->product_id);
								if(!empty($product_details))
								$order_summary.=($product_details->product_description!='') ? "<li class='mt-10'><small>".$product_details->product_description.'</small></li>': '';
								

				$order_summary.="<b>Order Date:</b>".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."<br>";
				$order_summary.="<b>Order ID:</b>".$queryRecordsObj->order_id."<br>";
				$order_summary.="<b>Order Item ID:</b>".$queryRecordsObj->order_item_id."<br>";
				$row[]=$order_summary;
				$row[]="Cancelled";
				
				$cancels_tableObj=$this->Model_orders->get_data_cancels_table($queryRecordsObj->order_item_id);
				$cancellation_details="";
				$cancellation_details.="<b>Cancelled By:</b>".$cancels_tableObj->cancelled_by."<br>";
				$cancellation_details.="<b>Comments:</b><br>";
				$cancellation_details.=$cancels_tableObj->cancel_reason_comments;
				$row[]=$cancellation_details;
				
				///
				$refund_details_obj=$this->get_cancel_refund_details($queryRecordsObj->order_item_id);
				$refund_details="";
				$refund_details.="<b>Item Price:</b>"." ".curr_sym." ".$refund_details_obj->item_with_deductions_concession."<br>";
				$refund_details.="<b>Shipping Price:</b>"." ".curr_sym." ".$refund_details_obj->shipping_with_deductions_concession."<br>";
				
				$refund_details.="<b>Total Refund With Concession:</b>"." ".curr_sym." ".$refund_details_obj->total_refund_with_concession."<br>";
				$refund_details.="<b>Refund Method:</b>".$refund_details_obj->refund_method."<br>";
				if($refund_details_obj->refund_method=="Bank Transfer"){
					$refund_details.="<b>Bank Details:</b>".$this->get_bank_details($refund_details_obj->refund_bank_id)."<br>";
				}
				$row[]=$refund_details;
				/////////////////
				
				$customer_add="<b>".$queryRecordsObj->customer_name."</b><br>";
				$customer_add.=$queryRecordsObj->address1."<br>";
				$customer_add.=$queryRecordsObj->address2."<br>";
				$customer_add.=$queryRecordsObj->city.",".$queryRecordsObj->state."<br>";
				$customer_add.=$queryRecordsObj->country."<br>";
				$customer_add.="Pincode:".$queryRecordsObj->pincode."<br>";
				$customer_add.="Mobile:".$queryRecordsObj->mobile."<br>";
				$row[]=$customer_add;
				
				/////////////////
				////
				$transaction_details="";
				$transaction_details.="<b>Transaction ID:</b><br>".$queryRecordsObj->transaction_id."<br>";
				$transaction_details.="<b>Transaction Date:</b><br>".date("D j M, Y",strtotime($queryRecordsObj->transaction_date))."<br>";
				$transaction_details.="<b>Transaction Time:</b><br>".$queryRecordsObj->transaction_time."<br>";
				$row[]=$transaction_details;
				////
				//////////////////////
				$data[]=$row;
				$row=array();
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function cancels_refund_request_orders_accounts_success(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["menu_flag"]="accounts_cancel_active_links";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/cancels_refund_request_orders_accounts_success',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function cancels_refund_request_orders_accounts_success_printcontent(){
		$order_item_id=$this->input->post("order_item_id");
		$cancels_refund_request_orders_accounts_success_obj=$this->Model_orders->cancels_refund_request_orders_accounts_success($order_item_id);
		?>
		<ul class="list-unstyled">
		<li>Cancel ID: <?php echo $cancels_refund_request_orders_accounts_success_obj->cancel_id;?></li>
	<li>Refund Details:</li>
	</ul>
	<?php
		$refund_details_obj=$this->get_cancel_refund_details($cancels_refund_request_orders_accounts_success_obj->order_item_id);
		
		$refund_details="<ul class='list-unstyled'>";
		$refund_details.="<li>Item Price:"." ".curr_sym." ".$refund_details_obj->item_with_deductions_concession."</li>";
		$refund_details.="<li>Shipping Price:"." ".curr_sym." ".$refund_details_obj->shipping_with_deductions_concession."</li>";
		$refund_details.="<li>Total Refund With Concession:"." ".curr_sym." ".$refund_details_obj->total_refund_with_concession."</li>";
		$refund_details.="<li>Refund Method: ".$refund_details_obj->refund_method."</li>";
		$refund_details.="</ul>";
		echo $refund_details;
		$refund_bank_details=$this->get_bank_details($refund_details_obj->refund_bank_id);
		echo $refund_bank_details;
	?>
		<?php
	}
	
	public function cancels_refund_request_orders_accounts_success_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_orders->cancels_refund_request_orders_accounts_success_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_orders->cancels_refund_request_orders_accounts_success_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				
				$order_summary='<ul class="list-unstyled small">';
				//if($queryRecordsObj->ord_addon_products_status=='1'){
				if(0){
					$combo_prod=$this->get_addon_data($queryRecordsObj);
					$order_summary.='<li>'.$combo_prod.'</li>';
					$row[]="Combo products";
					$order_summary.="<li>SKU:".$queryRecordsObj->sku_id."</li>";
					if($queryRecordsObj->ord_sku_name!=''){
						$order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
					}else{
						$order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
					}
				}else{
					$inventory_id=$queryRecordsObj->inventory_id;
					$image=$queryRecordsObj->image;
					$img_path=get_inventory_image_path($inventory_id,$image);

					$row[]="<img src='".base_url().$img_path."'  style='width: 100px;height: 122px;' >";
					$order_summary.="<li>SKU:".$queryRecordsObj->sku_id."</li>";
					if($queryRecordsObj->ord_sku_name!=''){
						$order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
					}else{
						$order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
					}
					$product_details=get_product_details($queryRecordsObj->product_id);
					if(!empty($product_details))
					$order_summary.=($product_details->product_description!='') ? "<li class='mt-10'><small>".$product_details->product_description.'</small></li>': '';
					
				}

				$order_summary.="<li>Order Date:".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."</li>";
				$order_summary.="<li>Order ID:".$queryRecordsObj->order_id."</li>";
				$order_summary.="<li>Order Item ID:".$queryRecordsObj->order_item_id."</li>";
				$order_summary.='</ul>';
				$row[]=$order_summary;
				
				$cancels_tableObj=$this->Model_orders->get_data_cancels_table($queryRecordsObj->order_item_id);
				$cancellation_details='<ul class="list-unstyled small">';
				$cancellation_details.="<li>Cancelled By:".$cancels_tableObj->cancelled_by."</li>";
				
				if(strlen($cancels_tableObj->cancel_reason_comments)>200){
					$cancellation_details.='<li>Comments:<div class="f-row margin-top more">'.str_split($cancels_tableObj->cancel_reason_comments,200)[0].'<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' .substr($cancels_tableObj->cancel_reason_comments,201). '</span>&nbsp;&nbsp;<a href="#" class="morelink" onclick="morelinkfun(this)">more</a></span></div></li>';
				}else{
					$cancellation_details.='<li>Comments:<div class="f-row margin-top more">'.$cancels_tableObj->cancel_reason_comments.'</div></li>';
				}
				
				$cancellation_details.='</ul>';
				$row[]=$cancellation_details;

				$refund_details_obj=$this->get_cancel_refund_details($queryRecordsObj->order_item_id);
				$refund_details='<ul class="list-unstyled small">';
				$refund_details.="<li>Item Price:"." ".curr_sym." ".$refund_details_obj->item_with_deductions_concession."</li>";
				$refund_details.="<li>Shipping Price:"." ".curr_sym." ".$refund_details_obj->shipping_with_deductions_concession."</li>";
				$refund_details.="<li>Total Refund With Concession:"." ".curr_sym." ".$refund_details_obj->total_refund_with_concession."</li>";
				$refund_details.="<li>Refund Method:".$refund_details_obj->refund_method."</li>";
				$refund_details.='</ul>';
				
				
				$row[]=$refund_details;
				/////////////////
				$customer_add='<ul class="list-unstyled small">';
				$customer_add.="<li class='text-info bold'>".$queryRecordsObj->customer_name."</li>";
				$customer_add.="<li>".$queryRecordsObj->address1."</li>";
				$customer_add.="<li>".$queryRecordsObj->address2."</li>";
				$customer_add.="<li>".$queryRecordsObj->city.",".$queryRecordsObj->state."</li>";
				$customer_add.="<li>".$queryRecordsObj->country."</li>";
				$customer_add.="<li>".$queryRecordsObj->pincode."</li>";
				$customer_add.="<li>".$queryRecordsObj->mobile."</li>";
				$customer_add.='</ul>';
				$row[]=$customer_add;
				
				/////////////////
				////
				$transaction_details='<ul class="list-unstyled small">';
				if($refund_details_obj->refund_method=="Bank Transfer" || $refund_details_obj->refund_method=="Razorpay" ){
                    if( $refund_details_obj->refund_method=="Razorpay"){
                        $transaction_details.="<li>Razorpay Refund ID:".$queryRecordsObj->transaction_id."</li>";
                        $transaction_details.="<li>Razorpay Refund Status:".$queryRecordsObj->razorpay_refund_status."</li>";
                    }else{
                        $transaction_details.="<li>Transaction ID:".$queryRecordsObj->transaction_id."</li>";
                    }
				$transaction_details.="<li>Transaction Date:".date("D j M, Y",strtotime($queryRecordsObj->transaction_date))."</li>";
				$transaction_details.="<li>Transaction Time:".$queryRecordsObj->transaction_time."</li>";
				}else{
				$transaction_details.="<li>No transactions details for wallet method</li>";	
				}
				$transaction_details.='<li><input type="button" value="Print Refund & Bank details" class="btn btn-success btn-xs btn-block" onclick="printcontent('.$queryRecordsObj->order_item_id.')"></li>';
				$transaction_details.='</ul>';
				$row[]=$transaction_details;
				////
				//////////////////////
				$data[]=$row;
				$row=array();
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	
	
	public function resend_cancel_refund(){
		if($this->session->userdata("logged_in")){
			$order_item_id=$this->input->post("order_item_id");
			$flag=$this->Model_orders->resend_cancel_refund($order_item_id);
			echo $flag;
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function cancel_transfer_to_wallet(){
		if($this->session->userdata("logged_in")){
			$order_item_id=$this->input->post("order_item_id");
			
			
			$flag=$this->Model_orders->cancel_transfer_to_wallet($order_item_id);
			echo $flag;
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function cancels_refund_request_orders_accounts_failure(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["menu_flag"]="accounts_cancel_active_links";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/cancels_refund_request_orders_accounts_failure',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function cancels_refund_request_orders_accounts_failure_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_orders->cancels_refund_request_orders_accounts_failure_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_orders->cancels_refund_request_orders_accounts_failure_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				
				$order_summary='<ul class="list-unstyled small">';
				//if($queryRecordsObj->ord_addon_products_status=='1'){
				if(0){
					$combo_prod=$this->get_addon_data($queryRecordsObj);
					$order_summary.='<li>'.$combo_prod.'</li>';
					$row[]="Combo products";
					$order_summary.="<li>SKU:".$queryRecordsObj->sku_id."</li>";
							if($queryRecordsObj->ord_sku_name!=''){
								$order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
							}else{
								$order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
							}
				}else{
					$inventory_id=$queryRecordsObj->inventory_id;
					$image=$queryRecordsObj->image;
					$img_path=get_inventory_image_path($inventory_id,$image);

					$row[]="<img src='".base_url().$img_path."'  style='width: 100px;height: 122px;' >";
						$order_summary.="<li>SKU:".$queryRecordsObj->sku_id."</li>";
							if($queryRecordsObj->ord_sku_name!=''){
								$order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
							}else{
								$order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
							}
							$product_details=get_product_details($queryRecordsObj->product_id);
							if(!empty($product_details))
							$order_summary.=($product_details->product_description!='') ? "<li class='mt-10'><small>".$product_details->product_description.'</small></li>': '';
							
				}
				$order_summary.="<li>Order Date:".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."</li>";
				$order_summary.="<li>Order ID:".$queryRecordsObj->order_id."</li>";
				$order_summary.="<li>Order Item ID:</b>".$queryRecordsObj->order_item_id."</li>";
				$order_summary.='</ul>';
				$row[]=$order_summary;

				
				$cancels_tableObj=$this->Model_orders->get_data_cancels_table($queryRecordsObj->order_item_id);
				$cancellation_details='<ul class="list-unstyled small">';
				$cancellation_details.="<li>Cancelled By:".$cancels_tableObj->cancelled_by."</li>";
				
				if(strlen($cancels_tableObj->cancel_reason_comments)>200){
					$cancellation_details.='<li>Comments:<div class="f-row margin-top more">'.str_split($cancels_tableObj->cancel_reason_comments,200)[0].'<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' .substr($cancels_tableObj->cancel_reason_comments,201). '</span>&nbsp;&nbsp;<a href="#" class="morelink" onclick="morelinkfun(this)">more</a></span></div></li>';
				}else{
					$cancellation_details.='<li>Comments:<div class="f-row margin-top more">'.$cancels_tableObj->cancel_reason_comments.'</div></li>';
				}
				
				$cancellation_details.='</ul>';
				$row[]=$cancellation_details;
				
				$refund_details_obj=$this->get_cancel_refund_details($queryRecordsObj->order_item_id);
				$refund_details='<ul class="list-unstyled small">';
				$refund_details.="<li>Item Price:"." ".curr_sym." ".$refund_details_obj->item_with_deductions_concession."</li>";
				$refund_details.="<li>Shipping Price:"." ".curr_sym." ".$refund_details_obj->shipping_with_deductions_concession."</li>";
				$refund_details.="<li>Total Refund With Concession:"." ".curr_sym." ".$refund_details_obj->total_refund_with_concession."</li>";
				$refund_details.="<li>Refund Method:".$refund_details_obj->refund_method."</li>";
				$refund_details.='</ul>';
				$row[]=$refund_details;
				/////////////////
				$customer_add='<ul class="list-unstyled small">';
				$customer_add.="<li class='text-info bold'>".$queryRecordsObj->customer_name."</li>";
				$customer_add.="<li>".$queryRecordsObj->address1."</li>";
				$customer_add.="<li>".$queryRecordsObj->address2."</li>";
				$customer_add.="<li>".$queryRecordsObj->city.",".$queryRecordsObj->state."</li>";
				$customer_add.="<li>".$queryRecordsObj->country."</li>";
				$customer_add.="<li>".$queryRecordsObj->pincode."</li>";
				$customer_add.="<li>".$queryRecordsObj->mobile."</li>";
				$customer_add.='</ul>';
				$row[]=$customer_add;
				////
				$failure_comments="";
				$failure_comments.="<p class='small'>".$queryRecordsObj->refund_fail_comments."</p>";
				$row[]=$failure_comments;
				/////////////////
				////
				
				//////////////////////
				$data[]=$row;
				$row=array();
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function send_sms_in_order_process($order_summary_mail_data,$mobile,$current_flag){
		
		$mobile_stuff_arr=mobile_stuff();
		$authKey = $mobile_stuff_arr["authKey_smstemplate"];
		$mobileNumber = "+91".$mobile;
		$senderId = $mobile_stuff_arr["senderId_smstemplate"];
					
			$message="";
			if($current_flag=="onhold"){
				$message.= urlencode("We have verified and placed your order $order_summary_mail_data[order_id] amounting to ".curr_sym."$order_summary_mail_data[grandtotal] and it is being processed.");
			}
			if($current_flag=="approved"){
				$message.= urlencode("Your order $order_summary_mail_data[order_id] amounting to ".curr_sym."$order_summary_mail_data[grandtotal] had been confirmed and it is being processed.");
			}
			if($current_flag=="confirmed"){
				$message.= urlencode("Your order $order_summary_mail_data[order_id] amounting to ".$current_flag."$order_summary_mail_data[grandtotal] is ready to be shipped.");
			}
			if($current_flag=="packed"){
				$message.= urlencode("Your order had been shipped from our Warehouse. The Tracking number is $order_summary_mail_data[tracking_number] and the link to follow up is $order_summary_mail_data[logistics_weblink]");
			}
			if($current_flag=="shipped"){
				$message.='We are happy to inform you that we have successfully delivered your order. Thank you for shopping!';
			}
			if($current_flag=="cancelled"){
				$message.='Your order had been cancelled. Thank you for shopping!';
			}
			if($current_flag=="undelivered"){
				$message.='Sorry! We are not able to deliver your order. Thank you for shopping!';
			}
			
			
			$route = "4";
			$postData = array(
				'authkey' => $authKey,
				'mobiles' => $mobileNumber,
				'message' => $message,
				'sender' => $senderId,
				'route' => $route
			);

			$url="https://control.msg91.com/api/sendhttp.php";
			$ch = curl_init();
			curl_setopt_array($ch, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $postData
			));
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

			$output = curl_exec($ch);

			if(curl_errno($ch)){
				echo 'error:' . curl_error($ch);
			}
			
			curl_close($ch);
			
	}
	
	public function send_email_in_order_process($order_summary_mail_data,$email,$current_flag){
		    include("assets/s3_fileupload/app/start.php");
			$email_stuff_arr=email_stuff();
			$order_id=$order_summary_mail_data["order_id"];
			$shipping_address_id=$this->Model_orders->get_shipping_address_id($order_summary_mail_data["order_item_id"]);
			$shipping_address_table_data_arr=$this->Model_orders->get_shipping_address($shipping_address_id);
			$orders_status_data=$this->Model_orders->get_orders_status_data($order_summary_mail_data["order_item_id"]);
			foreach($shipping_address_table_data_arr as $shipping_address_table_data){
				$shipping_address="";
				if($current_flag!="cancelled"){
					$shipping_address.='<table style="color:#565656;line-height:15px;font-size:15px;background-color:#fff;"><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;padding-bottom:15px"><b>Items will be shipped to:</b><br></td></tr><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;"><b>'.$shipping_address_table_data['customer_name'].'</b></td></tr><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;"> <span>Mob : </span>'.$shipping_address_table_data['mobile'].'</td></tr><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;">'.$shipping_address_table_data['address1'].',</td></tr><tr><td colspan="2" style="color: #565656;line-height: 22px;font-size: 15px;">'.$shipping_address_table_data['address2'].',</td></tr><tr><td colspan="2" style="color: #565656;line-height:15px;font-size: 15px;">'.$shipping_address_table_data['city'].', '.$shipping_address_table_data['pincode'].'</td></tr><tr><td colspan="2" style="color: #565656;line-height: 15px;font-size: 15px;">'.$shipping_address_table_data['state'].', '.$shipping_address_table_data['country'].'</td></tr></table>';
				}
				$shipping_address.='<p style="padding:0;margin:0;color:#565656;font-size: 15px;"><br/>Warm Regards, <br><b>'.$email_stuff_arr["regardsteam_emailtemplate"].'</b></p><br>';
			}
				
			$order_details='';
			$order_details.='<table>';
			$order_details.='<tr>';
			$order_details.='<th width="20%">Image</th>';
			$order_details.='<th width="40%">Product</th>';
			$order_details.='<th width="10%">Item Price</th>';
			$order_details.='<th width="10%">Qty</th>';
			$order_details.='<th width="20%">Subtotal</th>';
			$order_details.='</tr>';
				

				/* addon section */
				/*
				if($order_summary_mail_data['ord_addon_products_status']=='1'){
					$order_details.='<tr>';
					$order_details.='<td>';
						$arr=json_decode($order_summary_mail_data['ord_addon_products']);
						$count=count($arr);
						if($order_summary_mail_data['ord_addon_inventories']!=''){
							//$order_details.='<tr><th align="center" colspan="5" style="text-align: center;text-decoration: underline;">
							 //Combo Products - Included</th></tr>';
							$i=1;
							foreach($arr as $val){
								
								$name=$val->inv_name;
								$image=$val->inv_image;
								$sku_id=$val->inv_sku_id;
								$price=$val->inv_price;
								$order_details.='<tr>
								<td align="center"><img src="'.$image.'" style="width:85px;"></td>
								<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">'.$name.'<br>SKU id : '.$sku_id.'</p></td>
								<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">'.curr_sym.$price.'</td>
								<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">1</td>
								<td  width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">'.curr_sym.$price.'
								</td></tr>';
							}
							
					}
					$order_details.='</td>';
					$order_details.='</tr>';
			
				}else{ */

			$order_details.='<tr>';
			$order_details.='<td align="center">';
			$order_details.="<img src='".base_url().$order_summary_mail_data['thumbnail']."'>";
			$order_details.='</td>';
			$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
			$order_details.=$order_summary_mail_data['sku_name']."<br>";

			if($order_summary_mail_data['sku_name']==''){
				$order_details.=$order_summary_mail_data['product_name']."<br>";
			}
			
			$product_details=get_product_details($order_summary_mail_data['product_id']);
			if(!empty($product_details))
				$order_details.=($product_details->product_description!='') ? "".$product_details->product_description.'<br>': '';
			
			
			if(!empty($order_summary_mail_data['attribute_1'])){
				$order_details.=$order_summary_mail_data['attribute_1']." : ".$order_summary_mail_data['attribute_1_value'].'<br>';
			}
			if(!empty($order_summary_mail_data['attribute_2'])){
				$order_details.=$order_summary_mail_data['attribute_2']." : ".$order_summary_mail_data['attribute_2_value'].'<br>';
			}
			if(!empty($order_summary_mail_data['attribute_3'])){
				$order_details.=$order_summary_mail_data['attribute_3']." : ".$order_summary_mail_data['attribute_3_value'].'<br>';
			}
			if(!empty($order_summary_mail_data['attribute_4'])){
				$order_details.=$order_summary_mail_data['attribute_4']." : ".$order_summary_mail_data['attribute_4_value'];
			}
			if(!empty($order_summary_mail_data['sku_id'])){
				$order_details.="SKU : ".$order_summary_mail_data['sku_id'];
			}
			
			
			
			$order_details.='</p></td>';
			$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
			$order_details.=curr_sym.$order_summary_mail_data['product_price'];
			$order_details.='</td>';
			$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
			$order_details.=$order_summary_mail_data['quantity'];
			$order_details.='</p></td>';
			$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
			$order_details.=curr_sym.$order_summary_mail_data['subtotal'];
			$order_details.='</td>';
			$order_details.='</tr>';

		//}
			//////////////////////////////////////////////////
			
			////////////// addon single type starts ///
			
			//if($order_summary_mail_data['ord_addon_single_or_multiple_tagged_inventories_in_frontend']=="single" && $order_summary_mail_data['ord_addon_products_status']=="1"){
			if(0){
					// [{"inv_id":"2","inv_price":2000,"inv_mrp_price":3500,"inv_sku_id":"WPNC3","inv_image":"assets/pictures/images/products/WPNC3/86424.jpg","inv_name":"PNC5","inv_ship_price":0}]
					$ord_addon_products_json=$order_summary_mail_data["ord_addon_products"];
					if($ord_addon_products_json!=""){
						$ord_addon_products_json_arr=json_decode($ord_addon_products_json,true);
						
						$addon_inventory_id=$ord_addon_products_json_arr[0]["inv_id"];
						$addon_inventory_price=$ord_addon_products_json_arr[0]["inv_price"];
						$addon_inventory_mrp_price=$ord_addon_products_json_arr[0]["inv_mrp_price"];
						$addon_sku_id=$ord_addon_products_json_arr[0]["inv_sku_id"];
						$addon_inv_image=$ord_addon_products_json_arr[0]["inv_image"];
						$addon_inv_name=$ord_addon_products_json_arr[0]["inv_name"];
						$tagged_type=$ord_addon_products_json_arr[0]["tagged_type"];
						$addon_ship_price=$ord_addon_products_json_arr[0]["inv_ship_price"];
						$addon_inventory_info_obj=$this->Model_orders->get_inventory_info_by_inventory_id($addon_inventory_id);
						
						
						$order_details.='<tr style="background-color:#FFFFE0">';
						$order_details.='<td align="center">';
						$order_details.="<img src='".base_url().$addon_inv_image."'>";
						$order_details.='</td>';
						$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
						$order_details.="<b>Addon For ".$order_summary_mail_data['product_name']."</b><br>".$addon_inv_name."<br>";
						if($tagged_type=="internal"){
							if(!empty($addon_inventory_info_obj->attribute_1)){
								$order_details.=$addon_inventory_info_obj->attribute_1." : ".$addon_inventory_info_obj->attribute_1_value.'<br>';
							}
							if(!empty($addon_inventory_info_obj->attribute_2)){
								$order_details.=$addon_inventory_info_obj->attribute_2." : ".$addon_inventory_info_obj->attribute_2_value.'<br>';
							}
							if(!empty($addon_inventory_info_obj->attribute_3)){
								$order_details.=$addon_inventory_info_obj->attribute_3." : ".$addon_inventory_info_obj->attribute_3_value.'<br>';
							}
							if(!empty($addon_inventory_info_obj->attribute_4)){
								$order_details.=$addon_inventory_info_obj->attribute_4." : ".$addon_inventory_info_obj->attribute_4_value.'<br>';
							}
						}
						if(!empty($addon_sku_id)){
							$order_details.="SKU : ".$addon_sku_id;
						}
						
						$order_details.='</p>';
						$order_details.='</td>';
						$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
						$order_details.='<del>'.curr_sym.''.round($addon_inventory_mrp_price).'</del> ';
						$order_details.=curr_sym.''.round($addon_inventory_price);
						$order_details.='</td>';
						$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
						$order_details.=1;
						$order_details.='</p></td>';
						$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
						//$order_details.=curr_sym.''.$order_summary_mail_data['subtotal'];
						// $order_details.=curr_sym.''.round($order_summary_mail_data['quantity']*$order_summary_mail_data['ord_max_selling_price']);
						$order_details.='';
						$order_details.='</td>';
						$order_details.='</tr>';
						
						
					}
				}
				
				////////////// addon single type ends ///
			
			//// stright n% discount and another promo radio both is there then starts 
				if($order_summary_mail_data['promotion_default_discount_promo']!="" && $order_summary_mail_data['promotion_quote']!="" && $order_summary_mail_data['default_discount']>0 && $order_summary_mail_data['promotion_quote']!=$order_summary_mail_data['promotion_default_discount_promo']){
					$order_details.='<tr>';
					$order_details.='<td align="center"></td>';
					$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
					
					$order_details.="Base Discount : <br>".trim($order_summary_mail_data['promotion_default_discount_promo']);
					
					$order_details.='</td>';
					$order_details.='<td valign="top" style="padding:12px 10px 0 10px;margin:0;text-align:center;"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
					$product_price_after_default_discount=round($order_summary_mail_data['ord_max_selling_price']-(($order_summary_mail_data['ord_max_selling_price']*$order_summary_mail_data['default_discount'])/100));
					
					$order_details.=curr_sym." ".$product_price_after_default_discount." <del>".curr_sym." ".$order_summary_mail_data['ord_max_selling_price']."</del>";
					$order_details.='</td>';
					$order_details.='<td colspan="2"></td>';
					$order_details.='</tr>';
				}
				//// stright n% discount and another promo radio both is there then starts 
				
				
				
				// Straight 5% discount on SKU’s starts 
				if(intval($order_summary_mail_data['quantity_with_promotion'])>0){
					if(intval($order_summary_mail_data['promotion_discount'])>0 && (intval($order_summary_mail_data['promotion_discount'])==intval($order_summary_mail_data['default_discount']))){
						
						$promotion_straight_discount_str='Discount @ ('.$order_summary_mail_data['promotion_discount'].'%)';
						$eligible_straight_quantity_str='Eligible Quantity : '.$order_summary_mail_data['quantity_with_promotion'];
						$applied_straight_offer_str='Applied Offer : '.$order_summary_mail_data['promotion_quote'];
						$promo_default_straight_discount_price=floatval($order_summary_mail_data['ord_max_selling_price']-$order_summary_mail_data['individual_price_of_product_with_promotion']);
						$promo_straight_discount_offer_calculation_display=$order_summary_mail_data['quantity_with_promotion'].' *  '.curr_sym." ".$promo_default_straight_discount_price;
						$promo_straight_discount_offer_calculation=$order_summary_mail_data['quantity_with_promotion']*$promo_default_straight_discount_price;
						$order_details.='<tr>';
						$order_details.='<td>';
						$order_details.='</td>';
						$order_details.='<td colspan="2" style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.=$promotion_straight_discount_str;
							$order_details.=" ".$eligible_straight_quantity_str."<br>";
							$order_details.=$applied_straight_offer_str;
						$order_details.='</td>';
						$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.=$promo_straight_discount_offer_calculation_display;
						$order_details.='</td>';
						$order_details.='<td  valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.=' '.curr_sym.' '.$promo_straight_discount_offer_calculation;
						$order_details.='</td>';
						$order_details.='</tr>';
					
					}
				}
				// Straight 5% discount on SKU’s ends
				/// buy 5 sku and 6% discount table starts 
				if(intval($order_summary_mail_data['quantity_with_promotion'])>0){
					if(intval($order_summary_mail_data['promotion_discount'])>0 && (intval($order_summary_mail_data['promotion_discount'])!=intval($order_summary_mail_data['default_discount']))){
						$promotion_discount_str='Promotion Discount @ ('.$order_summary_mail_data['promotion_discount'].'%)';
						$eligible_quantity_str='Eligible Quantity : '.$order_summary_mail_data['quantity_with_promotion'];
						$applied_offer_str='Applied Offer : '.$order_summary_mail_data['promotion_quote'];
						$promo_discount_price=floatval($order_summary_mail_data['ord_max_selling_price']-$order_summary_mail_data['individual_price_of_product_with_promotion']);
						$promo_discount_offer_calculation_display=$order_summary_mail_data['quantity_with_promotion'].' * '.curr_sym." ".$promo_discount_price;
						$promo_discount_offer_calculation=$order_summary_mail_data['quantity_with_promotion']*$promo_discount_price;
						$order_details.='<tr>';
						$order_details.='<td>';
						$order_details.='</td>';
						$order_details.='<td colspan="2" style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.=$promotion_discount_str;
							$order_details.=" ".$eligible_quantity_str."<br>";
							$order_details.=$applied_offer_str;
						$order_details.='</td>';
						$order_details.='<td width="20%" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.=$promo_discount_offer_calculation_display;
						$order_details.='</td>';
						$order_details.='<td  valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.=' '.curr_sym.' '.$promo_discount_offer_calculation;
						$order_details.='</td>';
						$order_details.='</tr>';
					}
				}
				/// buy 5 sku and 6% discount table ends
				/// Free item starts 
				if($order_summary_mail_data['promotion_item']!=""){
					$free_items_arr=$this->get_free_skus_from_inventory_with_details($order_summary_mail_data['promotion_item'],$order_summary_mail_data['promotion_item_num']);
					foreach($free_items_arr as $free_itemsObj){
						$order_details.='<tr>';
						
						$order_details.='<td align="center">';
						$order_details.="<img src='".base_url().$free_itemsObj->image."'  style='width:85px;'>";
						$order_details.='</td>';
						
						$order_details.='<td style="padding:12px 15px 0 10px;margin:0;vertical-align:top;min-width:100px"><p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
                                                if($free_itemsObj->sku_name!=''){
                                                    $order_details.=$free_itemsObj->sku_name."<br>";
                                                }else{
                                                    $order_details.=$free_itemsObj->product_name."<br>";
                                                }
						
						if(!empty($free_itemsObj->attribute_1)){
							$order_details.=$free_itemsObj->attribute_1." : ".$free_itemsObj->attribute_1_value.'<br>';
						}
						if(!empty($free_itemsObj->attribute_2)){
							$order_details.=$free_itemsObj->attribute_2." : ".$free_itemsObj->attribute_2_value.'<br>';
						}
						if(!empty($free_itemsObj->attribute_3)){
							$order_details.=$free_itemsObj->attribute_3." : ".$free_itemsObj->attribute_3_value.'<br>';
						}
						if(!empty($free_itemsObj->attribute_4)){
							$order_details.=$free_itemsObj->attribute_4." : ".$free_itemsObj->attribute_4_value.'<br>';
						}
						if(!empty($free_itemsObj->sku_id)){
							$order_details.="SKU : ".$free_itemsObj->sku_id;
						}
						$order_details.='</p>';
						$order_details.='</td>';
						$order_details.='<td colspan="3" valign="top" align="center" style="padding:12px 10px 0 10px;margin:0;text-align:center"> <p style="white-space:nowrap;padding:0;margin:0;color:#848484;font-size:12px">';
							$order_details.='Free - You saved <del>'.curr_sym." ".$order_summary_mail_data['promotion_item_num']*$order_summary_mail_data['promotion_item_multiplier']*$free_itemsObj->selling_price.'</del> ('.$order_summary_mail_data['promotion_item_num']*$order_summary_mail_data['promotion_item_multiplier'].' * '.curr_sym.' '.$free_itemsObj->selling_price.')';
							$order_details.='<br>';
							$order_details.='(Offer Applied : '.$order_summary_mail_data['promotion_quote'].')';
						$order_details.='</td>';
						$order_details.='</tr>';
					}
				}
				/// Free item ends 
				$invoice_offers=$this->Customer_account->get_all_order_invoice_offers($order_id);
				$shipping_charge_waived="no";
				if(!empty($invoice_offers)){
					foreach($invoice_offers as $data){
						$promotion_invoice_free_shipping=$data["promotion_invoice_free_shipping"];
						$invoice_offer_achieved=$data["invoice_offer_achieved"];
						if($promotion_invoice_free_shipping>0 && $invoice_offer_achieved==1){
							$shipping_charge_waived="yes";
						}else{
							$shipping_charge_waived="no";
						}
					}
				}
				if($shipping_charge_waived=="yes"){
					$shipping_charge_modified=0;
					$subtotal_modified=$order_summary_mail_data['subtotal'];
				}
				else{
					$shipping_charge_modified=$order_summary_mail_data['shipping_charge'];
					$subtotal_modified=$order_summary_mail_data['grandtotal'];
				}
			
			/////////////////////////////////////////////////////////////////
			
			
		$order_details.='<tr>';
		$order_details.='<td colspan=5 align="center">';
		$order_details.='<p style="margin:10px 0 0 0;padding:5px;background-color:#f9f9f9;border:1px solid #eee;color:#666;font-size:13px;text-align:center">(+) Delivery: '.curr_sym.' '.($shipping_charge_modified);
		$order_details.='</p></td>';
		$order_details.='</tr>';
			
			
			
			if($current_flag!="shipped" && $current_flag!="cancelled"){
			$order_details.='<tr>';
			$order_details.='<td colspan=5 align="center" style="padding:0 0 17px 0">';
			$order_details.='<p style="padding:4px 5px;background-color:#fffed5;border:1px solid #f9e2b2;color:#565656;margin:10px 0 0 0;text-align:center;font-size:12px">Delivery scheduled by '.date("D j M, Y",strtotime($order_summary_mail_data['expected_delivery_date']));
			$order_details.='</p></td>';
			$order_details.='</tr>';
			}
			if($current_flag=="shipped"){
			$order_details.='<tr>';
			$order_details.='<td colspan=5 align="center" style="padding:0 0 17px 0">';
			$order_details.='<p style="padding:4px 5px;background-color:#fffed5;border:1px solid #f9e2b2;color:#565656;margin:10px 0 0 0;text-align:center;font-size:12px">Delivered on '.date("D j M, Y",strtotime($orders_status_data->order_delivered_date)).' '.$orders_status_data->order_delivered_time;
			$order_details.='</p></td>';
			$order_details.='</tr>';
			}
			
			$order_details.='<tr>';
				$order_details.='<td colspan=5 style="border-top: 2px solid #565656;border-bottom: 1px solid #e6e6e6; padding:10px 20px 10px 20px;;margin: 0;background-color: #f9f9f9;">';
				$order_details.='<p style="padding:0;margin:0;text-align:right;color:#004b9b;line-height:22px;white-space:nowrap;font-size:18px">Total : <span style="font-size:18;color:#004b9b;">'.curr_sym.' '.$subtotal_modified;
				$order_details.='</span></p></td>';
				$order_details.='</tr>';
			
			
			$order_details.='</table>';
			
			
			
			
			
			
			
			
			$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color:color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:10px"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr><tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi '.$shipping_address_table_data['customer_name'].', </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">';
			
			if($current_flag=="onhold"){
				$usermessage.='Your order has been placed.';
			}
			if($current_flag=="approved"){
				$usermessage.='Your order has been confirmed.';
			}
			if($current_flag=="confirmed"){
				$usermessage.='Your order has been packed.';
			}
			if($current_flag=="packed"){
				$usermessage.='Your order has been shipped. Your Tracking number is '.$order_summary_mail_data['tracking_number'].' and the link to follow up is <a href="'.$order_summary_mail_data['logistics_weblink'].'" target="_blank">Track Your Shipment</a>';
			}
			if($current_flag=="shipped"){
				$usermessage.='Your order has been delivered successfully.';
			}
			if($current_flag=="cancelled"){
				$get_cancel_reason_stuff_arr=$this->Model_orders->get_cancel_reason_stuff($order_summary_mail_data["order_item_id"]);
				$usermessage.='Your below order has been cancelled by product administrator of '.$email_stuff_arr["name_emailtemplate"].'<br>';
				$reason_cancel_id=$get_cancel_reason_stuff_arr["reason_cancel_id"];
				$cancel_reason_comments=$get_cancel_reason_stuff_arr["cancel_reason_comments"];
				$cancel_reason=$this->Model_orders->get_cancel_reason_to_check($reason_cancel_id);
				$usermessage.="<b>Reason For Cancellation</b> : ".$cancel_reason." (".$cancel_reason_comments.")<br><br>";
				$usermessage.="We further humbly note to you that we will work towards a tighter control on our side so that such incidences are not repeated again. Thank you sincerely for your cooperation.";
				
			}
			if($current_flag=="undelivered"){
				$usermessage.='Your order is undelivered';
			}
			$usermessage.='</p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">';
			
			if($current_flag=="onhold"){
				$usermessage.='We will send you another email once your order is confirmed.';
			}
			if($current_flag=="approved"){
				$usermessage.='We will send you another email once your order is packed.';
			}
			if($current_flag=="confirmed"){
				$usermessage.='We will send you another email once your order is shipped.';
			}
			if($current_flag=="packed"){
				$usermessage.='We will send you another email once your order is delivered.';
			}
			if($current_flag=="shipped"){
				$usermessage.='Thank you for shopping with <a href="'.SITE_URL.'" target="_blank">'.SITE_NAME.'</a>';
				$rating_mail='<td valign="top" align="center" width="300" style="background-color:#f9f9f9">
                                                <br>
                                                <table width="100%" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        
														
														<tr> <td><table style="max-width:600px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6" cellpadding="0" cellspacing="0" width="100%">
<tbody> <tr> <td style="border-top:#e6e6e6 solid 1px;border-bottom:#e6e6e6 solid 1px;background-color:#f9f9f9;display:block;margin:0 auto;clear:both;padding:15px" align="left" bgcolor="" valign="top"> <table style="background-color:#f8f8f8;border:solid 1px #ccc;margin:0;padding:0" cellpadding="0" cellspacing="0" width="100%"> <tbody><tr> <td style="padding:10px;margin:0" align="left" valign="top"> <p style="margin:0;font-size:13px;padding:0"><strong>Kindly
rate our service as per the below
scale.</strong></p><br><p style="margin:0;font-size:12px;padding:0 10px;color:#666"></p><table style="background-color:#f8f8f8" cellpadding="0" cellspacing="0" width="100%"> <tbody> <tr> <td colspan="3" style="padding:8px 10px;margin:0" align="left" valign="top" width="50%"> <p style="margin:0;font-size:12px;padding:0;color:#666">1
- Very Unhappy</p></td><td colspan="6" style="padding:8px 10px;margin:0" align="right" valign="top" width="50%"> <p style="margin:0;font-size:12px;padding:0;color:#666">5
- Very happy</p></td></tr><tr><td align="center" valign="top" width="20%"><a style="background-color:#ffffff;text-decoration:none;display:block;line-height:20px;min-height:20px;width:20px;border:solid 1px #254061" href="'.base_url().'Account/rateorderbymail/'.$order_summary_mail_data["order_item_id"].'/1" target="_blank"><span style="font-size:12px;color:#444">1</span></a>
</td><td align="center" valign="top" width="20%">
<a style="background-color:#ffffff;text-decoration:none;display:block;line-height:20px;min-height:20px;width:20px;border:solid 1px #254061" href="'.base_url().'Account/rateorderbymail/'.$order_summary_mail_data["order_item_id"].'/2" target="_blank"><span style="font-size:12px;color:#444">2</span></a>
</td><td align="center" valign="top" width="20%">
<a style="background-color:#ffffff;text-decoration:none;display:block;line-height:20px;min-height:20px;width:20px;border:solid 1px #254061" href="'.base_url().'Account/rateorderbymail/'.$order_summary_mail_data["order_item_id"].'/3" target="_blank"><span style="font-size:12px;color:#444">3</span></a>
</td><td align="center" valign="top" width="20%">
<a style="background-color:#ffffff;text-decoration:none;display:block;line-height:20px;min-height:20px;width:20px;border:solid 1px #254061" href="'.base_url().'Account/rateorderbymail/'.$order_summary_mail_data["order_item_id"].'/4" target="_blank"><span style="font-size:12px;color:#444">4</span></a>
</td><td align="center" valign="top" width="20%">
<a style="background-color:#ffffff;text-decoration:none;display:block;line-height:20px;min-height:20px;width:20px;border:solid 1px #254061" href="'.base_url().'Account/rateorderbymail/'.$order_summary_mail_data["order_item_id"].'/5" target="_blank"><span style="font-size:12px;color:#444">5</span></a>
</td><td align="center" valign="top" width="20%"></td></tr></tbody></table><br><p style="margin:0;padding:0;color:#666;font-size:11px">
Your response will be recorded and you will be
redirected to our feedback form.
</p></td></tr></tbody></table> </td></tr></tbody>
</table></td></tr>





                                                    </tbody>
                                                </table>
                                            </td> ';
			}
			if($current_flag=="cancelled"){
				//$usermessage.='Thank you for shopping!';
			}
			if($current_flag=="undelivered"){
				$usermessage.='Sorry! We are not able to deliver your order.';
			}
			if($current_flag!="shipped"){
				$rating_mail="";
			}
			
			$get_order_placed_date_customer_id_arr=$this->Model_orders->get_order_placed_date_customer_id($order_summary_mail_data['order_id']);
			$order_placed_date=$get_order_placed_date_customer_id_arr["order_placed_date"];
			$order_placed_date_format=date("jS F, Y \a\\t H:i",strtotime($order_placed_date))." Hrs";
			if($current_flag=="shipped"){
				$rating_mail1="";
				$usermessage.='</p> <table align="center"><tr>'.$rating_mail.'</tr></table>  </td></tr><tr><td style="background-color:#ffffff;display:block;margin:0 auto;clear:both;padding:20px 20px 0 20px" align="left" valign="top">	<table border="0" width="100%"><tbody><tr><td colspan="2" style="padding:10px;background-color:#004b9b"><span style="color:#fff;"><b> Below is the summary of your order ID '.$order_summary_mail_data['order_id'].' dated '.$order_placed_date_format.'</b></span></td></tr><tr><td style="background-color:#ffffff;display:block;margin:0 auto;clear:both;padding:20px 20px 0 20px" align="left" valign="top">'.$order_details.'</td></tr></tbody></table></td></tr>
				
				
				
				<tr><td style="background-color:#fff;><table width="100%"><tbody><tr><td valign="top" align="left" style="background-color:#ffffff;color:#565656;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;padding:20px 20px 0 20px" bgcolor="#ffffff"></td></tr></tbody></table></td></tr>';
				
				if($current_flag!="cancelled"){
					$usermessage.='<tr><td valign="top" align="left" style="margin: 0 auto;clear:both;padding: 20px 20px 0 20px;background-color:#fff;">'.$shipping_address.'</td></tr>';
				}
				
				$usermessage.='<tr><td><table width="100%" cellspacing="0" cellpadding="0" style="max-width:802px;"> <tbody><tr>'.$rating_mail1.'<td valign="top" align="center" width="300" style="background-color:#f9f9f9"> <br> <table width="100%" cellspacing="0" cellpadding="0"> <tbody><tr> <td valign="top" align="left" style="padding:0 10px 15px 20px;margin:0"> <p style="padding:0;margin:0 0 7px 0;font-size:16px;color:#565656">Any Questions?</p><p style="padding:0;margin:0;font-size:11px;color:#565656;line-height:20px">Get in touch with our customer care team at +91 80505 04950</p> </td> </tr> </tbody></table> </td> </tr> </tbody></table></td></tr>';
				
				$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

				$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

				$usermessage.='</tbody> </table></tr></table></center> </body></html>';
			}
			else{
				if($current_flag!="cancelled"){
					$usermessage.=' Meanwhile, you can check the status of your order on '.$email_stuff_arr["name_emailtemplate"].'</p><br><p style="text-align:center;padding:0;margin:0" align="center"> <a style="width:200px;margin:0px auto;background-color:#ff8c00;text-align:center;border:#ff8c00 solid 1px;padding:8px 0;text-decoration:none;border-radius:2px;display:block;color:#fff;font-size:13px" align="center" href="'.base_url().'Account/order_details_for_mail/'.$order_summary_mail_data['order_id'].'" target="_blank"> <span style="color:#ffffff;font-size:13px;background-color:#ff8c00">TRACK ORDER</span> </a> </p>';
				}
				$usermessage.=' </td></tr><tr><td style="background-color:#ffffff;display:block;margin:0 auto;clear:both;padding:20px 20px 0 20px" align="left" valign="top">	<table border="0" width="100%"><tbody><tr><td colspan="2" style="padding:10px;background-color:#004b9b"><span style="color:#fff;"><b> Below is the summary of your order ID '.$order_summary_mail_data['order_id'].' dated '.$order_placed_date_format.'</b></span></td></tr><tr><td style="background-color:#ffffff;display:block;margin:0 auto;clear:both;padding:20px 20px 0 20px" align="left" valign="top">'.$order_details.'</td></tr></tbody></table></td></tr>';
				
				
				
				$usermessage.='<tr><td style="background-color:#fff;><table width="100%"><tbody><tr><td valign="top" align="left" style="background-color:#ffffff;color:#565656;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;padding:20px 20px 0 20px" bgcolor="#ffffff"></td></tr></tbody></table></td></tr>';
				
				if($current_flag!='cancelled'){
					$usermessage.='<tr><td valign="top" align="left" style="margin: 0 auto;clear:both;padding: 20px 20px 0 20px;background-color:#fff;">'.$shipping_address.'</td></tr>';
				}
				
				$usermessage.='<tr><td><table width="100%" cellspacing="0" cellpadding="0" style="max-width:802px;"> <tbody><tr>'.$rating_mail.'<td valign="top" align="center" width="300" style="background-color:#f9f9f9"> <br> <table width="100%" cellspacing="0" cellpadding="0"> <tbody><tr> <td valign="top" align="left" style="padding:0 10px 15px 20px;margin:0"> <p style="padding:0;margin:0 0 7px 0;font-size:16px;color:#565656">Any Questions?</p><p style="padding:0;margin:0;font-size:11px;color:#565656;line-height:20px">Get in touch with our customer care team at +91 80505 04950</p> </td> </tr> </tbody></table> </td> </tr> </tbody></table></td></tr>';
				
				$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

				$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

				$usermessage.='</tbody> </table></tr></table></center> </body></html>';
			}
			
			$mail = new PHPMailer;
			/*$mail->isSMTP();
			$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
			$mail->Port = '587';//  live - comment it 
			$mail->Auth = true;//  live - comment it 
			$mail->SMTPAuth = true;
			$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
			$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
			$mail->SMTPSecure = 'tls';*/
			$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
			$mail->FromName = $email_stuff_arr["name_emailtemplate"];

			//$email='nanthinivinothkumar0227@gmail.com';
			//$email='shareeshk@gmail.com';
			$mail->addAddress($email);
			//if($current_flag=="approved"){
				$current_base_url=base_url();
				
			//}
			//$mail->AddCC("rajitkumar1974@gmail.com", "Rajit Kumar");               
			//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
			$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
			$mail->WordWrap = 50;
			$mail->isHTML(true);
			if($current_flag=="onhold"){
				$sub="Order Placed";
			}
			if($current_flag=="approved"){
				$sub="Order Confirmation";
			}
			if($current_flag=="confirmed"){
				$sub="Order Packed";
			}
			if($current_flag=="packed"){
				$sub="Order Shipped";
			}
			if($current_flag=="shipped"){
				$sub="Order Delivered";
			}
			if($current_flag=="cancelled"){
				$sub="Note: Order Cancelled";
			}
			if($current_flag=="undelivered"){
				$sub="Order Undelivered";
			}

			$sub=$sub.' - Order ID : '.$order_id;

			$mail->Subject = $sub;
			$mail->Body    = $usermessage;
			
			if(!$mail->send()){
				echo 'mail is not sent';
			}
			
			//echo true;
	//}
}
	
	
	public function get_replacement_desired_data($order_item_id){
		if($this->session->userdata("logged_in")){
			$replacement_desired_data_obj=$this->Model_orders->get_replacement_desired_data($order_item_id);
			return $replacement_desired_data_obj;
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function returns_replacement_request_orders_accounts_success(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$returns_replacement_request_orders_accounts_success_obj=$this->Model_orders->returns_replacement_request_orders_accounts_success();
			return $returns_replacement_request_orders_accounts_success_obj;
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_order_replacement_decision_data($order_item_id){
		if($this->session->userdata("logged_in")){
			$order_replacement_decision_data_obj=$this->Model_orders->get_order_replacement_decision_data($order_item_id);
			return $order_replacement_decision_data_obj;
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_availability_of_order_item_id_in_refund_and_replacement($order_item_id){
		if($this->session->userdata("logged_in")){
			$availability_of_order_item_id_in_refund_and_replacement=$this->Model_orders->get_availability_of_order_item_id_in_refund_and_replacement($order_item_id);
			return $availability_of_order_item_id_in_refund_and_replacement;
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}

	public function returned_orders(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["menu_flag"]="inactive_order_active_links";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/returned_orders',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	//////////////////
	
	public function set_as_cancelled_for_shipped(){
		
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$list_to_be_cancelled=$this->input->post("list_to_be_cancelled");
			$cancel_reason_id=$this->input->post("cancel_reason");			
			$cancel_reason=$this->Model_orders->get_cancel_reason($cancel_reason_id);			
			$reason_for_cancel_order=$this->input->post("reason_for_cancel_order");
			$update_inventory_stock=$this->input->post("update_inventory_stock");
			$send_notification=$this->input->post("send_notification");
			
			$list_to_be_cancelled_arr=explode("-",$list_to_be_cancelled);
			$list_to_be_cancelled_order_item_arr=array();
			$list_to_be_cancelled_prev_order_item_arr=array();
			
			foreach($list_to_be_cancelled_arr as $orders_status_id){
				$list_to_be_cancelled_order_item_arr[]=$this->Model_orders->get_order_item_id_by_orders_status_id($orders_status_id);
				$prev_order_item_id=$this->Model_orders->get_prev_order_item_id_by_orders_status_id($orders_status_id);
				if($prev_order_item_id!=""){
					$list_to_be_cancelled_prev_order_item_arr[]=$prev_order_item_id;
				}
			}

			foreach($list_to_be_cancelled_order_item_arr as $order_item_id){
			
				$type_of_order=$this->Model_orders->get_the_type_of_order_fresh_or_replaced($order_item_id);
			
				if($type_of_order!="replaced"){
					
					$user_type=$this->session->userdata("user_type");		
					$refund_method="";
					$refund_bank_id="";
					$add_cancel_order=$this->Model_orders->add_cancel_orders_to_cancels_table($order_item_id,$cancel_reason_id,$reason_for_cancel_order,$update_inventory_stock,"admin",$refund_method,$refund_bank_id);
					
					if($update_inventory_stock=="yes"){
				
						$cancel_reas=$this->Model_orders->get_cancel_reason_to_check($cancel_reason_id);
						if($cancel_reas!="No Inventory"){
							$reset_stock=1;
						}else{
							$reset_stock=0;
						}
					}else{
						$reset_stock=0;
					}
					
					if($add_cancel_order){ 
						$order_active_data=$this->Model_orders->get_all_order_data_from_order_item($order_item_id);
						$status_view="cancelled_status_view";
						$flag_update=$this->Model_orders->update_notification_to_send_cancel($order_active_data,$send_notification,$status_view);
						
						foreach($order_active_data as $data){
							$purchased_price=$data['purchased_price'];
							$prev_order_item_id=$data['prev_order_item_id'];
							$order_item_id=$data['order_item_id'];
							$customer_id=$data['customer_id'];
							$order_id=$data['order_id'];
							$shipping_address_id=$data['shipping_address_id'];
							$inventory_id=$data['inventory_id'];
							$product_id=$data['product_id'];
							$sku_id=$data['sku_id'];
							$tax_percent=$data['tax_percent'];
							$product_price=$data['product_price'];
							$shipping_charge=$data['shipping_charge'];
							$subtotal=$data['subtotal'];
							$wallet_status=$data['wallet_status'];
							$wallet_amount=$data['wallet_amount'];
							$grandtotal=$data['grandtotal'];
							$quantity=$data['quantity'];
							$timestamp=$data['timestamp'];
							$image=$data['image'];
							$random_number=$data['random_number'];
							$invoice_email=$data['invoice_email'];
							$invoice_number=$data['invoice_number'];
							$logistics_name=$data['logistics_name'];
							$logistics_weblink=$data['logistics_weblink'];
							$tracking_number=$data['tracking_number'];
							$expected_delivery_date=$data['expected_delivery_date'];
							$delivery_mode=$data['delivery_mode'];
							
							$parcel_category=$data['parcel_category'];

							$vendor_id=$data['vendor_id'];
							$payment_type=$data['payment_type'];
							$payment_status=$data['payment_status'];
							$return_period=$data['return_period'];
							
							$logistics_id=$data['logistics_id'];
							$logistics_delivery_mode_id=$data['logistics_delivery_mode_id'];
							$logistics_territory_id=$data['logistics_territory_id'];
							$logistics_parcel_category_id=$data['logistics_parcel_category_id'];

							$promotion_available=$data['promotion_available'];
							$promotion_residual=$data['promotion_residual'];
							$promotion_discount=$data['promotion_discount'];
							$promotion_cashback=$data['promotion_cashback'];
							$promotion_surprise_gift=$data['promotion_surprise_gift'];
							$promotion_surprise_gift_type=$data['promotion_surprise_gift_type'];
							$promotion_surprise_gift_skus=$data['promotion_surprise_gift_skus'];
							$promotion_surprise_gift_skus_nums=$data['promotion_surprise_gift_skus_nums'];
							$default_discount=$data['default_discount'];
							$promotion_item_multiplier=$data['promotion_item_multiplier'];
							$promotion_item=$data['promotion_item'];
							$promotion_item_num=$data['promotion_item_num'];
							$promotion_clubed_with_default_discount=$data['promotion_clubed_with_default_discount'];
							$promotion_id_selected=$data['promotion_id_selected'];
							$promotion_type_selected=$data['promotion_type_selected'];
							$promotion_minimum_quantity=$data['promotion_minimum_quantity'];
							$promotion_quote=$data['promotion_quote'];
							$promotion_default_discount_promo=$data['promotion_default_discount_promo'];
							$individual_price_of_product_with_promotion=$data['individual_price_of_product_with_promotion'];
							$individual_price_of_product_without_promotion=$data['individual_price_of_product_without_promotion'];
							$total_price_of_product_without_promotion=$data['total_price_of_product_without_promotion'];
							$total_price_of_product_with_promotion=$data['total_price_of_product_with_promotion'];
							$quantity_without_promotion=$data['quantity_without_promotion'];
							$quantity_with_promotion=$data['quantity_with_promotion'];
							$cash_back_value=$data['cash_back_value'];
                            $razorpayOrderId=$data['razorpayOrderId'];
                            $razorpayPaymentId=$data['razorpayPaymentId'];
							
							//invoice details
							$promotion_invoice_cash_back=$data['promotion_invoice_cash_back'];
							$invoice_surprise_gift=$data['invoice_surprise_gift'];
							$total_price_without_shipping_price=$data['total_price_without_shipping_price'];
							$promotion_invoice_discount=$data['promotion_invoice_discount'];
                                                        $ord_max_selling_price=$data['ord_max_selling_price'];
                                                        $ord_selling_discount=$data['ord_selling_discount'];
                                                        $ord_tax_percent_price=$data['ord_tax_percent_price'];
                                                        $ord_taxable_price=$data['ord_taxable_price'];
                                                        $ord_sku_name=$data['ord_sku_name'];


							/* coupon */

							$ord_coupon_code=$data['ord_coupon_code'];
							$ord_coupon_name=$data['ord_coupon_name'];
							$ord_coupon_type=$data['ord_coupon_type'];
							$ord_coupon_value=$data['ord_coupon_value'];
							$ord_coupon_reduce_price=$data['ord_coupon_reduce_price'];
							$ord_coupon_status=$data['ord_coupon_status'];

							/* coupon */
							/* addon products details */
							$ord_addon_products_status=$data['ord_addon_products_status'];
							$ord_addon_products=$data['ord_addon_products'];
							$ord_addon_inventories=$data['ord_addon_inventories'];
							$ord_addon_total_price=$data['ord_addon_total_price'];
							
							$ord_addon_single_or_multiple_tagged_inventories_in_frontend=$data['ord_addon_single_or_multiple_tagged_inventories_in_frontend'];
							$ord_addon_master_order_item_id=$data['ord_addon_master_order_item_id'];
						
						
							
							$product_vendor_id=$data['product_vendor_id'];
							/* addon products details */
							//,$ord_addon_products_status,$ord_addon_products,$ord_addon_inventories,$ord_addon_total_price
						}
						// move above data to cancelled_orders table
						if($promotion_invoice_discount>0){
							
							$invoice_discount=($subtotal/$total_price_without_shipping_price)*100;					
							$each_invoice_amount=$promotion_invoice_discount*$invoice_discount/100;
							//$invoice_amount=round($promotion_invoice_discount-$each_invoice_amount,2);
							$order_item_invoice_discount_value=round($each_invoice_amount,2);
							$order_item_invoice_discount_value_each=round($order_item_invoice_discount_value/$quantity,2);
						}else{
							$order_item_invoice_discount_value=0;
							$order_item_invoice_discount_value_each=0;
						}
						
						if($reset_stock==1){
			
							$reset_stock_flag=$this->Model_orders->add_stocks_of_inventory($inventory_id,$quantity,$promotion_available,$promotion_item,$promotion_item_num,$promotion_minimum_quantity,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$ord_addon_products_status,$ord_addon_inventories);
						}
						

						$add_to_cancel_orders=$this->Model_orders->active_order_add_to_cancel_orders($purchased_price,$prev_order_item_id,$order_item_id,$customer_id,$order_id,$shipping_address_id,$inventory_id,$product_id,$sku_id,$tax_percent,$product_price,$shipping_charge,$subtotal,$wallet_status,$wallet_amount,$grandtotal,$quantity,$timestamp,$image,$random_number,$invoice_email,$invoice_number,$logistics_name,$logistics_weblink,$tracking_number,$expected_delivery_date,$delivery_mode,$parcel_category,$vendor_id,$payment_type,$payment_status,$return_period,$logistics_id,$logistics_delivery_mode_id,$logistics_territory_id,$logistics_parcel_category_id,$promotion_available,$promotion_residual,$promotion_discount,$promotion_cashback,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$default_discount,$promotion_item_multiplier,$promotion_item,$promotion_item_num,$promotion_clubed_with_default_discount,$promotion_id_selected,$promotion_type_selected,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_without_promotion,$total_price_of_product_with_promotion,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$order_item_invoice_discount_value,$razorpayOrderId,$razorpayPaymentId,$ord_max_selling_price,$ord_selling_discount,$ord_tax_percent_price,$ord_taxable_price,$ord_sku_name,$ord_coupon_code,$ord_coupon_name,$ord_coupon_type,$ord_coupon_value,$ord_coupon_reduce_price,$ord_coupon_status,$ord_addon_products_status,$ord_addon_products,$ord_addon_inventories,$ord_addon_total_price,$ord_addon_single_or_multiple_tagged_inventories_in_frontend,$ord_addon_master_order_item_id,$product_vendor_id);

						if($add_to_cancel_orders){
							
							$status="reject";
							if($promotion_invoice_cash_back>0){	
								$flag_cashback=$this->Model_orders->update_customer_response_for_invoice_cashback($order_id,$status,$customer_id,$total_amount_cash_back);		
							}
							if($invoice_surprise_gift!=''){
								$flag_sg=$this->Model_orders->update_customer_status_surprise_gifts_on_invoice($order_id,$status,$customer_id);
							}
							
							
							$cancel_id=$this->Model_orders->get_cancel_id_of_this_transaction($order_item_id);
							
							$wallet_exists=$this->Model_orders->get_wallet_exists_for_customer($customer_id);
							//add this transaction to refund table
							$return_id="";//this is cancel order case
							$refund_method="";
							
							$update_order_item_id_to_cancel=$this->Model_orders->update_order_item_id_to_cancel($order_item_id);//changing order_item to cancelled

						
							if($update_order_item_id_to_cancel){
								$flag=$this->Model_orders->remove_data_from_active_order_table($order_item_id);
								//redirect(base_url()."Account/my_order");
								echo $flag;
							}
						}
					}
				}else{
					
						//replaced order
						
					 	$order_active_data=$this->Model_orders->get_all_order_data_from_order_item($order_item_id);
						foreach($order_active_data as $data){
							$purchased_price=$data['purchased_price'];
							$prev_order_item_id=$data['prev_order_item_id'];
							$order_item_id=$data['order_item_id'];
							$customer_id=$data['customer_id'];
							$order_id=$data['order_id'];
							$shipping_address_id=$data['shipping_address_id'];
							$inventory_id=$data['inventory_id'];
							$product_id=$data['product_id'];
							$sku_id=$data['sku_id'];
							$tax_percent=$data['tax_percent'];
							$product_price=$data['product_price'];
							$shipping_charge=$data['shipping_charge'];
							$subtotal=$data['subtotal'];
							$wallet_status=$data['wallet_status'];
							$wallet_amount=$data['wallet_amount'];
							$grandtotal=$data['grandtotal'];
							$quantity=$data['quantity'];
							$timestamp=$data['timestamp'];
							$image=$data['image'];
							$random_number=$data['random_number'];
							$invoice_email=$data['invoice_email'];
							$invoice_number=$data['invoice_number'];
							$logistics_name=$data['logistics_name'];
							$logistics_weblink=$data['logistics_weblink'];
							$tracking_number=$data['tracking_number'];
							$expected_delivery_date=$data['expected_delivery_date'];
							$delivery_mode=$data['delivery_mode'];
							$parcel_category=$data['parcel_category'];

							$vendor_id=$data['vendor_id'];
							$payment_type=$data['payment_type'];
							$payment_status=$data['payment_status'];
							$return_period=$data['return_period'];
							
							$logistics_id=$data['logistics_id'];
							$logistics_delivery_mode_id=$data['logistics_delivery_mode_id'];
							$logistics_territory_id=$data['logistics_territory_id'];
							$logistics_parcel_category_id=$data['logistics_parcel_category_id'];

							$promotion_available=$data['promotion_available'];
							$promotion_residual=$data['promotion_residual'];
							$promotion_discount=$data['promotion_discount'];
							$promotion_cashback=$data['promotion_cashback'];
							$promotion_surprise_gift=$data['promotion_surprise_gift'];
							$promotion_surprise_gift_type=$data['promotion_surprise_gift_type'];
							$promotion_surprise_gift_skus=$data['promotion_surprise_gift_skus'];
							$promotion_surprise_gift_skus_nums=$data['promotion_surprise_gift_skus_nums'];
							$default_discount=$data['default_discount'];
							$promotion_item_multiplier=$data['promotion_item_multiplier'];
							$promotion_item=$data['promotion_item'];
							$promotion_item_num=$data['promotion_item_num'];
							$promotion_clubed_with_default_discount=$data['promotion_clubed_with_default_discount'];
							$promotion_id_selected=$data['promotion_id_selected'];
							$promotion_type_selected=$data['promotion_type_selected'];
							$promotion_minimum_quantity=$data['promotion_minimum_quantity'];
							$promotion_quote=$data['promotion_quote'];
							$promotion_default_discount_promo=$data['promotion_default_discount_promo'];
							$individual_price_of_product_with_promotion=$data['individual_price_of_product_with_promotion'];
							$individual_price_of_product_without_promotion=$data['individual_price_of_product_without_promotion'];
							$total_price_of_product_without_promotion=$data['total_price_of_product_without_promotion'];
							$total_price_of_product_with_promotion=$data['total_price_of_product_with_promotion'];
							$quantity_without_promotion=$data['quantity_without_promotion'];
							$quantity_with_promotion=$data['quantity_with_promotion'];
							$cash_back_value=$data['cash_back_value'];
                                                        $razorpayOrderId=$data['razorpayOrderId'];
                                                        $razorpayPaymentId=$data['razorpayPaymentId'];
							//invoice details
							$promotion_invoice_cash_back=$data['promotion_invoice_cash_back'];
							$invoice_surprise_gift=$data['invoice_surprise_gift'];
							$total_price_without_shipping_price=$data['total_price_without_shipping_price'];
							$promotion_invoice_discount=$data['promotion_invoice_discount'];
                                                        $ord_max_selling_price=$data['ord_max_selling_price'];
                                                        $ord_selling_discount=$data['ord_selling_discount'];
                                                        $ord_tax_percent_price=$data['ord_tax_percent_price'];
                                                        $ord_taxable_price=$data['ord_taxable_price'];
                                                        $ord_sku_name=$data['ord_sku_name'];

														
							/* coupon */

							$ord_coupon_code=$data['ord_coupon_code'];
							$ord_coupon_name=$data['ord_coupon_name'];
							$ord_coupon_type=$data['ord_coupon_type'];
							$ord_coupon_value=$data['ord_coupon_value'];
							$ord_coupon_reduce_price=$data['ord_coupon_reduce_price'];
							$ord_coupon_status=$data['ord_coupon_status'];

							/* coupon */
							/* addon products details */
							$ord_addon_products_status=$data['ord_addon_products_status'];
							$ord_addon_products=$data['ord_addon_products'];
							$ord_addon_inventories=$data['ord_addon_inventories'];
							$ord_addon_total_price=$data['ord_addon_total_price'];
							
							
							$ord_addon_single_or_multiple_tagged_inventories_in_frontend=$data['ord_addon_single_or_multiple_tagged_inventories_in_frontend'];
							$ord_addon_master_order_item_id=$data['ord_addon_master_order_item_id'];
							
							
							
							$product_vendor_id=$data['product_vendor_id'];
							/* addon products details */
						}
						
						$order_item_invoice_discount_value=0;
						
						$add_to_cancel_orders=$this->Model_orders->active_order_add_to_cancel_orders($purchased_price,$prev_order_item_id,$order_item_id,$customer_id,$order_id,$shipping_address_id,$inventory_id,$product_id,$sku_id,$tax_percent,$product_price,$shipping_charge,$subtotal,$wallet_status,$wallet_amount,$grandtotal,$quantity,$timestamp,$image,$random_number,$invoice_email,$invoice_number,$logistics_name,$logistics_weblink,$tracking_number,$expected_delivery_date,$delivery_mode,$parcel_category,$vendor_id,$payment_type,$payment_status,$return_period,$logistics_id,$logistics_delivery_mode_id,$logistics_territory_id,$logistics_parcel_category_id,$promotion_available,$promotion_residual,$promotion_discount,$promotion_cashback,$promotion_surprise_gift,$promotion_surprise_gift_type,$promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums,$default_discount,$promotion_item_multiplier,$promotion_item,$promotion_item_num,$promotion_clubed_with_default_discount,$promotion_id_selected,$promotion_type_selected,$promotion_minimum_quantity,$promotion_quote,$promotion_default_discount_promo,$individual_price_of_product_with_promotion,$individual_price_of_product_without_promotion,$total_price_of_product_without_promotion,$total_price_of_product_with_promotion,$quantity_without_promotion,$quantity_with_promotion,$cash_back_value,$order_item_invoice_discount_value,$razorpayOrderId,$razorpayPaymentId,$ord_max_selling_price,$ord_selling_discount,$ord_tax_percent_price,$ord_taxable_price,$ord_sku_name,$ord_coupon_code,$ord_coupon_name,$ord_coupon_type,$ord_coupon_value,$ord_coupon_reduce_price,$ord_coupon_status,$ord_addon_products_status,$ord_addon_products,$ord_addon_inventories,$ord_addon_total_price,$ord_addon_single_or_multiple_tagged_inventories_in_frontend,$ord_addon_master_order_item_id,$product_vendor_id);
						if($add_to_cancel_orders){

							$wallet_exists=$this->Model_orders->get_wallet_exists_for_customer($customer_id);
							
							
							$update_order_item_id_to_cancel=$this->Model_orders->update_order_item_id_to_cancel($order_item_id);//changing order_item to cancelledactive_order_add_to_replaced_orders
							if($update_order_item_id_to_cancel){
								$flag=$this->Model_orders->remove_data_from_active_order_table($order_item_id);
								echo $flag;
							}
						}
				}
			}

			$flag=$this->Model_orders->set_as_cancelled($list_to_be_cancelled,$reason_for_cancel_order);
			
			if(count($list_to_be_cancelled_prev_order_item_arr)>0){
				$flag=$this->Model_orders->set_as_cancelled_for_shipped($list_to_be_cancelled_prev_order_item_arr,$reason_for_cancel_order,$cancel_reason);
			}
			
			if($flag==true){
				/////////////////////////////////////////////////////////////
				$send_sms=$this->input->post("sms");
				$send_email=$this->input->post("email");
				if($send_sms==1 || $send_email==1){
					foreach($list_to_be_cancelled_order_item_arr as $order_item_id){
						
						$order_summary_mail_data_arr=$this->Model_orders->get_order_summary_mail_data($order_item_id);
						foreach($order_summary_mail_data_arr as $order_summary_mail_data){
							
						$email=$this->Model_orders->get_email_db($order_summary_mail_data['customer_id']);
						$mobile=$this->Model_orders->get_mobile_db($order_summary_mail_data['customer_id']);
						
						if($mobile != '' && $send_sms==1){
							$this->send_sms_in_order_process($order_summary_mail_data,$mobile,"undelivered");
						}//sms
				
						if($email!='' && $send_email==1){
							$this->send_email_in_order_process($order_summary_mail_data,$email,"undelivered");
						}//mail	
		
						}
					}
				}
				/////////////////////////////////////////////////////////////
			}
			echo $flag;
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	////////////////////////
	public function cannot_cancel_the_replace_order(){
		
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$list_to_be_cancelled=$this->input->post("list_to_be_cancelled");

			$list_to_be_cancelled_arr=explode("-",$list_to_be_cancelled);
			$list_to_be_cancelled_prev_order_item_arr=array();
			
			foreach($list_to_be_cancelled_arr as $orders_status_id){
				$prev_order_item_id=$this->Model_orders->get_prev_order_item_id_by_orders_status_id($orders_status_id);
				if($prev_order_item_id!=""){
					$list_to_be_cancelled_prev_order_item_arr[]=$prev_order_item_id;
				}
			}

			if(count($list_to_be_cancelled_prev_order_item_arr)>0){
				echo "no_access";
			}else{
				echo "access";
			}

		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_cancelled_order_customer_payment_type($order_item_id){
		$data["controller"]=$this;
		$payment_type=$this->Customer_account->get_cancelled_order_customer_payment_type($order_item_id);
		return $payment_type;
	}
	public function get_data_from_invoice_offers(){
		
		$order_id=$this->input->post("order_id");
		
		if($this->input->post("cancelled_order")){
			$cancelled_order=1;
		}else{
			$cancelled_order=0;
		}
		
		$res_obj=$this->Model_orders->get_data_from_invoice_offers($order_id);
		
		$str_os="";
		if(!empty($res_obj)){
			$inc_txt='';
			$str_os.='<table class="table">';
			//$str_os.='<thead><tr><th>Name</th><th>Value</th></tr></thead>';	
				
			$str_os.='<tr><td>Promotion Invoice Cash back</td><td class="text-right">'.curr_sym.$res_obj->promotion_invoice_cash_back.'</td></tr>';
			$str_os.='<tr><td>promotion invoice free shipping</td><td class="text-right" style="color:red;">- '.curr_sym.$res_obj->promotion_invoice_free_shipping.'</td></tr>';
			$str_os.='<tr><td>Promotion invoice discount</td><td class="text-right" style="color:red;">- '.curr_sym.$res_obj->promotion_invoice_discount.'</td></tr>';
                        if($res_obj->invoice_coupon_status=='1'){
                            $inc_txt.='(';
                            $inc_txt.=($res_obj->invoice_coupon_type=='2') ? curr_sym : ''.' ';
                            $inc_txt.=round($res_obj->invoice_coupon_value);
                            $inc_txt.=($res_obj->invoice_coupon_type=='1') ? '%' : 'Flat' ;
                            $inc_txt.=')';
                            
                            $str_os.='<tr><td>Invoice coupon '.$inc_txt.'</td><td class="text-right" style="color:red;">- '.curr_sym.round($res_obj->invoice_coupon_reduce_price).'</td></tr>';
                        }
			$str_os.='<tr><td>Total amount cash back</td><td class="text-right">'.curr_sym.$res_obj->total_amount_cash_back.'</td></tr>';
			$str_os.='<tr><td>Total coupon amount</td><td class="text-right">'.curr_sym.round($res_obj->total_coupon_used_amount).'</td></tr>';
			$str_os.='<tr><td>Total amount saved</td><td class="text-right"> '.curr_sym.$res_obj->total_amount_saved.'</td></tr>';
			$str_os.='<tr><td>Total amount </td><td class="text-right"><b> '.curr_sym.$res_obj->total_amount_to_pay.'</b></td></tr>';
			$str_os.='<tr><td><i>Payment Method</i></td><td class="text-right"> <i>'.$res_obj->payment_method.'</i></td></tr>';
			
			//$str_os.='<tr><td></td><td>'.$res_obj->invoice_offer_achieved.'</td></tr>';
			
			$invoice_surprise_gift_promo_quote=$res_obj->invoice_surprise_gift_promo_quote;
			$invoice_surprise_gift=$res_obj->invoice_surprise_gift;
			$invoice_surprise_gift_type=$res_obj->invoice_surprise_gift_type;
			$invoice_surprise_gift_skus=$res_obj->invoice_surprise_gift_skus;
			$invoice_surprise_gift_skus_nums=$res_obj->invoice_surprise_gift_skus_nums;
			
			$promotion_invoice_cash_back=$res_obj->promotion_invoice_cash_back;
			$promotion_invoice_free_shipping=$res_obj->promotion_invoice_free_shipping;
			$promotion_invoice_discount=$res_obj->promotion_invoice_discount;
			$customer_response_for_surprise_gifts=$res_obj->customer_response_for_surprise_gifts;
			
			$order_id=$res_obj->order_id;
			$invoice_offer_achieved=$res_obj->invoice_offer_achieved;
			$str_invoice='';
			if($res_obj->invoice_offer_achieved==1){

				$str_invoice='';
				if($invoice_surprise_gift!=''){
					
					$str_invoice.='<tr><td colspan="2"> <small><b>Promo quote : </b>'.$invoice_surprise_gift_promo_quote.'</small></td></tr>';
					
					if($invoice_surprise_gift_type=='Qty'){
					$free_items_status_arr=$this->Model_orders->get_status_of_free_items($res_obj->id);	
					//print_r($free_items_status_arr);
					$array_delivered=$free_items_status_arr["array_delivered"];
					$array_shipped=$free_items_status_arr["array_shipped"];
					$array_comments=$free_items_status_arr["array_comments"];
					$array_undelivered=$free_items_status_arr["array_undelivered"];
					
					$str_invoice.='<tr class="bg-warning"><td colspan="2">';
												
												
												
						$str_invoice.='<div class="">';
							$str_invoice.='<div class="">';
							
								$str_invoice.='<div class="" data-toggle="collapse" href="#collapse'.$order_id.'" style="cursor:pointer;">';

										$str_invoice.='<u class="bold text-success">Free Surprise Gifts</u>';
								$str_invoice.='</div>';
								
								$str_invoice.='<div id="collapse'.$order_id.'" class="panel-collapse collapse">';
									
									
								$invoice_surprise_gift_skus=rtrim($invoice_surprise_gift_skus,',');
								$invoice_surprise_gift_skus_nums=rtrim($invoice_surprise_gift_skus_nums,',');
								
								$invoice_surprise_gift_skus_arr = explode(',', $invoice_surprise_gift_skus);
								$invoice_surprise_gift_skus_nums_arr = explode(',', $invoice_surprise_gift_skus_nums);
								
								$two=array_combine($invoice_surprise_gift_skus_arr,$invoice_surprise_gift_skus_nums_arr);	
								
								if($invoice_surprise_gift_skus!=''){
									
									$invoice_free_items_arr=$this->Customer_account->get_free_skus_from_inventory($invoice_surprise_gift_skus);
									$j=0;
									foreach($invoice_free_items_arr as $invoice_free_items){
											
									$str_invoice.='<div class="col-md-12 free_items_style">';
									
										$str_invoice.='<div class="col-md-2">';					
										$str_invoice.='<img src="'.base_url().$invoice_free_items->image.'" alt="FREE PRODUCTS" style="width: 50%;">';
										$str_invoice.='</div>';
										
										$str_invoice.='<div class="col-md-4">';
											$str_invoice.='<small>';
                                                                                        if($invoice_free_items->sku_name!=''){
                                                                                            $str_invoice.=$invoice_free_items->sku_name."<br>";
                                                                                        }else{
                                                                                            $str_invoice.=$invoice_free_items->product_name."<br>";
                                                                                        }
											//$str_invoice.=$invoice_free_items->product_name;
											//$str_invoice.='<br>';
										
											
											if(!empty($invoice_free_items->attribute_1))
												$invoice_free_items->attribute_1_value= substr($invoice_free_items->attribute_1_value, 0, strpos($invoice_free_items->attribute_1_value, ":"));
												$str_invoice.=$invoice_free_items->attribute_1." : ".$invoice_free_items->attribute_1_value.'<br>';
											if(!empty($invoice_free_items->attribute_2))
												$str_invoice.=$invoice_free_items->attribute_2." : ".$invoice_free_items->attribute_2_value.'<br>';
											if(!empty($invoice_free_items->attribute_3))
												$str_invoice.=$invoice_free_items->attribute_3." : ".$invoice_free_items->attribute_3_value.'<br>';
											if(!empty($invoice_free_items->attribute_4))
												$str_invoice.=$invoice_free_items->attribute_4." : ".$invoice_free_items->attribute_4_value.'<br>';
											
											$str_invoice.='</small>';
										$str_invoice.='</div>';
										
										$str_invoice.='<div class="col-md-2">';
											$str_invoice.='<small>';
											$str_invoice.='Quantity :'; 
											$id=$invoice_free_items->id;
											$str_invoice.=($two[$id]);
											$str_invoice.='</small>';
										$str_invoice.='</div>';
										
										$str_invoice.='<div class="col-md-4">';
											$str_invoice.='<small>';
											if($array_undelivered[$j]==1){
												$str_invoice.='<span style="color:red;">Un Delivered</span>';
													$str_invoice.='<br>';
											}else{
												
												if($array_delivered[$j]==1){
													$str_invoice.='<span style="color:blue;">Delivered</span>';
													$str_invoice.='<br>';
												}else{										
													if($array_delivered[$j]==0 && $array_shipped[$j]==1){
														$str_invoice.='<span style="color:blue;">Shipped</span>';
														$str_invoice.='<br>';
													}elseif($array_delivered[$j]==0 && $array_shipped[$j]==0){
														$str_invoice.='<span style="color:blue;">pending</span>';
														$str_invoice.='<br>';
													}
												}
											}
											if(!empty($array_comments[$j])){
												$str_invoice.='(admin comments : '.$array_comments[$j].')';
											}
											
											$str_invoice.='</small>';
										$str_invoice.='</div>';
									
									$str_invoice.='</div>';
									
									$j++;
									
									}
								}
						
								$str_invoice.='</div>';
							$str_invoice.='</div>';
						$str_invoice.='</div>';
						
						$str_invoice.='</td></tr>';
												
						if($cancelled_order==0){

							if($customer_response_for_surprise_gifts=='reject'){
								$str_invoice.='<tr class="bg-warning"><td colspan="2"><div class="col-md-12 bg-warning text-center" style="">
									<small><span style="color:red;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> <del>Customer rejected the surprise gifts </del></i></b></span></small>
									</div></td></tr>';
							}
							if($customer_response_for_surprise_gifts=='accept'){
								$str_invoice.='<tr class="bg-warning"><td colspan="2"><div class="col-md-12 bg-warning text-center" style="">
									<small><span style="color:green;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> Customer accepted the surprise gifts.</i></b></span></small>
									</div></td></tr>';
							}
							if($customer_response_for_surprise_gifts==''){
								$str_invoice.='<tr class="bg-warning"><td colspan="2"><div class="col-md-12 bg-warning text-center" style="">
									<small><span style="color:blue;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> Waiting for Customer response to deliver surprise gifts</i></b></span></small>
									</div></td></tr>';
							}
						}						
					}
					
					if($invoice_surprise_gift_type==curr_code){
						$credited_status=0;
						$debited_status=0;
						
						if($cancelled_order==0){
							$credited_status=$this->Model_orders->check_amount_paid_or_not($order_id,$invoice_surprise_gift);
						}
						if($cancelled_order==1){
							//cancelled order
							$debited_status=$this->Model_orders->check_amount_debit_or_not($order_id,$invoice_surprise_gift);
						}
						
						if($credited_status>0){
							$str_invoice.='<tr class="bg-warning"><td colspan="2"><div class="col-md-12 bg-warning text-center" style="">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> '.curr_sym.$invoice_surprise_gift.' was credited to customer wallet (surprise gift)</i></b></span>
							</div></td></tr>';
							
						}
						if($debited_status>0){
						
							$str_invoice.='<tr class="bg-warning"><td colspan="2"><div class="col-md-12 bg-warning text-center" style="">
							<span style="color:#d65f08;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> '.curr_sym.$invoice_surprise_gift.' was debited from customer wallet (surprise gift)</i></b></span>
							</div></td></tr>';
						}
						if($cancelled_order==0){
							
							if($customer_response_for_surprise_gifts=='reject'){
								$str_invoice.='<tr class="bg-warning"><td colspan="2"><div class="col-md-12 bg-warning text-center" style="">
									<small><span style="color:red;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> <del>Customer rejected the surprise gifts </del></i></b></span></small>
									</div></td></tr>';
							}
							if($customer_response_for_surprise_gifts=='accept'){
								$str_invoice.='<tr class="bg-warning"><td colspan="2"><div class="col-md-12 bg-warning text-center" style="">
									<small><span style="color:green;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> Customer accepted the surprise gifts</i></b></span></small>
									</div></td></tr>';
							}
							if($customer_response_for_surprise_gifts==''){
								$str_invoice.='<tr class="bg-warning"><td colspan="2"><div class="col-md-12 bg-warning text-center" style="">
									<small><span style="color:blue;"><b><i> <i class="fa fa-gift" aria-hidden="true"></i> Waiting for Customer response to credit the amount</i></b></span></small>
									</div></td></tr>';
							}
						}

					}
											
				}
				
				
			}//invoice offer achived
			
			$str_os.=$str_invoice;
			$str_os.='</tbody></table>';
		}
		
		echo $str_os;
	}
	
	public function get_details_of_promotion($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal){

			$str_promo='';
                        $discount_saved=0;
                        $promo_dis=0;
                        $total_promo_discount_price=0;
                        $grand_total_default_discount_price=0;
			$str_promo_quote='<ul class="list-unstyled">';
			$promotion_available=$queryRecordsObj->promotion_available;
			
			if($promotion_available==1){
				$quantity_without_promotion=$queryRecordsObj->quantity_without_promotion;
				$quantity_with_promotion=$queryRecordsObj->quantity_with_promotion;
				$promotion_quote=$queryRecordsObj->promotion_quote;
				$promotion_default_discount_promo=$queryRecordsObj->promotion_default_discount_promo;
				
				$str_promo_quote.='<li class="text-info">Promotion Applies</li>';
				if(!empty($quantity_without_promotion)){
					$str_promo_quote.='<li>'.$promotion_default_discount_promo.'</li>';
				}
				if(!empty($quantity_with_promotion)){
					$str_promo_quote.='<li>'.$promotion_quote.'</li>';
				}
                                
                                //&& $queryRecordsObj->promotion_quote!=$queryRecordsObj->promotion_default_discount_promo
                                if($queryRecordsObj->promotion_default_discount_promo!="" && $queryRecordsObj->default_discount>0){
                                    /* straight promotion */
                                }else{
                                    /* when promotion not applied*/
                                    if($queryRecordsObj->ord_selling_discount>0){
                                        $str_promo_quote.='<li class="text-info">Default discount Applies</li>';
                                        $str_promo_quote.='<li> Default discount :'.round($queryRecordsObj->ord_selling_discount).'% OFF</li>';
                                    }
                                }
			}else{
                            if($queryRecordsObj->ord_selling_discount>0){
                                $str_promo_quote.='<li class="text-info">Default discount Applies</li>';
                                $str_promo_quote.='<li> Default discount :'.round($queryRecordsObj->ord_selling_discount).'% OFF</li>';
                            }
                        }
                        
                        
			$str_promo_quote.='</ul>';
			/////////////////////////////////////////
			$str_promo.=$str_promo_quote;
			if($queryRecordsObj->promotion_surprise_gift_type!=''){ 

				$promotion_surprise_gift_type=$queryRecordsObj->promotion_surprise_gift_type;
				
				$str_promo.='<div id="app_promotion_'.$queryRecordsObj->order_item_id.'" class="" >';

				$str_promo.='<div class="promo_view" id="app_accordion_'.$queryRecordsObj->order_item_id.'">';
				$str_promo.='<div class="">';

				$str_promo.='<div class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#app_accordion_'.$queryRecordsObj->order_item_id.'" href="#app_collapseOne_'.$queryRecordsObj->order_item_id.'" style="cursor:pointer;">';

				//$str_promo.='<div class=" panel-title" >';

				//$str_promo.='<i class="more-less glyphicon glyphicon-plus"></i>';
				$str_promo.='<u class="bold text-success">Surprise gift details</u>';

				//$str_promo.='</div>';

				$str_promo.='</div>';
				$str_promo.='<div id="app_collapseOne_'.$queryRecordsObj->order_item_id.'" class="panel-collapse collapse"> ';
				$str_promo.='<div class="panel-body">';
				
				$str_promo.='<table class="table" style="font-size: .8em;text-align:justify">';

				$promotion_surprise_gift_skus=$queryRecordsObj->promotion_surprise_gift_skus;
				$promotion_surprise_gift_skus_nums=$queryRecordsObj->promotion_surprise_gift_skus_nums;
				$promotion_surprise_gift_skus=rtrim($promotion_surprise_gift_skus,',');
				$promotion_surprise_gift_skus_nums=rtrim($promotion_surprise_gift_skus_nums,',');
					if($promotion_surprise_gift_type=='Qty'){
						//$queryRecordsObj->promotion_surprise_gift_type;
						$str_promo.='<h5 class="text-center"> Gift Type - Free quantity</h5>';	
						$str_promo.='<thead><tr><th width="25%" style="text-align: left">surprise gift skus</th><th width="25%" style="text-align: left">Qty</th></tr></thead>';
						$promo_gift_arr = explode(',', $promotion_surprise_gift_skus);
						$promo_gift_num_arr = explode(',', $promotion_surprise_gift_skus_nums);
						
						$sku_id_arr=$this->Model_orders->get_sku_ids($promotion_surprise_gift_skus);
						
						$two=array_combine($promo_gift_arr,$promo_gift_num_arr);
						foreach($two as $inv_id=>$stock_count){
							//get SKU id for inventory_id
							$str_promo.='<tr><td>'.$sku_id_arr[$inv_id].'</td><td>'.$stock_count.'</td></tr>';
						}
					}
					if($promotion_surprise_gift_type==curr_code){
						$str_promo.='<h5 class="text-center"> Gift Type - Amount</h5>';
						$str_promo.='<thead><tr><th width="25%" style="text-align: left">Amount</th><th width="25%" style="text-align: left">Currency</th></tr></thead>';
						
						$str_promo.='<tr><td>'.$queryRecordsObj->promotion_surprise_gift.'</td><td>'.$promotion_surprise_gift_type.'</td></tr>';
					}

				$str_promo.='</table>';

				$str_promo.='</div>';

				$str_promo.='</div>';

				$str_promo.='</div>';
				$str_promo.='</div>';
				
				$str_promo.="</div>";

			}
			
			/////////////////////////////////////////
			
				$str_promo.='<div id="promotion_'.$queryRecordsObj->order_item_id.'" class="" >';

				$str_promo.='<div class="promo_view" id="accordion_'.$queryRecordsObj->order_item_id.'">';
				$str_promo.='<div class="">';

				$str_promo.='<div class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_'.$queryRecordsObj->order_item_id.'" href="#collapseOne_'.$queryRecordsObj->order_item_id.'" style="cursor:pointer;">';

				//$str_promo.='<div class=" panel-title" >';

				//$str_promo.='<i class="more-less glyphicon glyphicon-plus"></i>';
				$str_promo.='<u class="bold text-info">Details</u>';

				//$str_promo.='</div>';

				$str_promo.='</div>';
				$str_promo.='<div id="collapseOne_'.$queryRecordsObj->order_item_id.'" class="panel-collapse collapse"> ';
				$str_promo.='<div class="panel-body">';


///////////////////////new stucture//////////////
	$promotion_invoice_free_shipping=$queryRecordsObj->promotion_invoice_free_shipping;
	$order_item_id=$queryRecordsObj->order_item_id;
	$promotion_item=$queryRecordsObj->promotion_item;
	$promotion_item_num=$queryRecordsObj->promotion_item_num;
	$promotion_surprise_gift_skus=$queryRecordsObj->promotion_surprise_gift_skus;
	$promotion_surprise_gift_skus_nums=$queryRecordsObj->promotion_surprise_gift_skus_nums;
	$promotion_surprise_gift=$queryRecordsObj->promotion_surprise_gift;
	$promotion_surprise_gift_type=$queryRecordsObj->promotion_surprise_gift_type;
	
		if($promotion_item!='' || ($promotion_surprise_gift_skus!='')){
						
				$quantity=$queryRecordsObj->quantity;
				$promotion_minimum_quantity=$queryRecordsObj->promotion_minimum_quantity;
				
				$promo_item=rtrim($promotion_item,',');
				$promotion_item_num=rtrim($promotion_item_num,',');
				
				$promo_item_arr = explode(',', $promo_item);
				$promo_item_num_arr = explode(',', $promotion_item_num);
				
				$two=array_combine($promo_item_arr,$promo_item_num_arr);
				
				$temp_quotient=intval($quantity/$promotion_minimum_quantity);
				
				//Surprise
				
				$promotion_surprise_gift_skus=rtrim($promotion_surprise_gift_skus,',');
				$promotion_surprise_gift_skus_nums=rtrim($promotion_surprise_gift_skus_nums,',');
				
				$gift_skus_arr = explode(',', $promotion_surprise_gift_skus);
				$gift_skus_nums_arr = explode(',', $promotion_surprise_gift_skus_nums);
				
				$gift_with_count=array_combine($gift_skus_arr,$gift_skus_nums_arr);
				if($promotion_item!=''){
					$free_items_arr=$this->Customer_account->get_free_skus_from_inventory($promotion_item,$promotion_item_num);
				}
				if($promotion_surprise_gift_type=='Qty'){
					$free_surprise_items=$this->Customer_account->get_free_skus_from_inventory($promotion_surprise_gift_skus,$promotion_surprise_gift_skus_nums);
				}
		}
		
		//$str_promo='';
		$str_promo.='<table class="table table-bordered" style="font-size: .8em;text-align:justify;margin-bottom: 0px;">';
                                                 
		$str_promo.='<thead><tr><th style="padding:0px"></th><th width="25%" style="padding:0px"></th><th style="padding:0px" width="25%" style="text-align: right"></th></tr></thead>';

		$total_product_price_normal=($queryRecordsObj->quantity*round($queryRecordsObj->ord_max_selling_price));
		
		$shipping_price_normal=$queryRecordsObj->shipping_charge;

		$total_amount_normal=($shipping_price_normal+$total_product_price_normal);

		$str_promo.='<tr><td>Base product price</td><td>'.$queryRecordsObj->quantity.' <i class="fa fa-times" aria-hidden="true"></i> '.round($queryRecordsObj->ord_max_selling_price).'</td><td class="text-right">'.curr_sym.$total_product_price_normal.'</td></tr>';
		
		$str_promo.='<tr><td colspan="2">Shipping Charge for '.$queryRecordsObj->quantity.'unit(s)</td><td class="text-right">'.curr_sym.$shipping_price_normal.'</td></tr>';
		//if($queryRecordsObj->ord_addon_single_or_multiple_tagged_inventories_in_frontend=="single"){
		if(0){
			$addon_products_arr=json_decode($queryRecordsObj->ord_addon_products,true);
			$addon_inv_mrp_price=$addon_products_arr[0]["inv_mrp_price"];
			$addon_inv_price=$addon_products_arr[0]["inv_price"];
			$addon_inv_sku_id=$addon_products_arr[0]["inv_sku_id"];
			$str_promo.='<tr><td colspan="2">Addon SKU : '.$addon_inv_sku_id.'</td><td class="text-right"><del>'.curr_sym.$addon_inv_mrp_price.'</del> '.curr_sym.$addon_inv_price.'</td></tr>';
			$total_amount_normal+=$addon_inv_price;
		}
		
		$str_promo.='<tr><td colspan="2">Total value </td><td class="text-right">'.curr_sym.$total_amount_normal.'</td></tr>';
		    if($queryRecordsObj->promotion_available==1){    
			
				if(intval($queryRecordsObj->quantity_without_promotion)>0 && $queryRecordsObj->default_discount>0){ 
				
					$default_discount_price=floatval($queryRecordsObj->ord_max_selling_price-$queryRecordsObj->individual_price_of_product_without_promotion);
					$total_default_discount_price=($queryRecordsObj->quantity_without_promotion*$default_discount_price);
                                        $grand_total_default_discount_price+=intval($total_default_discount_price);
					$total_item_price_with_base_price=($queryRecordsObj->product_price*$queryRecordsObj->quantity_without_promotion);
							
					$str_promo.='<tr><td> Discount @ ('.$queryRecordsObj->default_discount.'%)<br>Eligible Quantity : '.$queryRecordsObj->quantity_without_promotion.'<br>For promotion <br><small style="font-size: .9em;color: blue">'.$queryRecordsObj->promotion_default_discount_promo.'</small></td><td>'.$queryRecordsObj->quantity_without_promotion.' <i class="fa fa-times" aria-hidden="true"></i> '.$default_discount_price.'</td><td class="text-right" style="color:red"> '.curr_sym.$total_default_discount_price.'</td></tr>';
	
				}
                                                
                if(intval($queryRecordsObj->quantity_with_promotion)>0){ 
                                                 
					if(intval($queryRecordsObj->promotion_discount)>0&& (intval($queryRecordsObj->promotion_discount)==intval($queryRecordsObj->default_discount))){
				
						$default_discount_price=floatval($queryRecordsObj->ord_max_selling_price-$queryRecordsObj->individual_price_of_product_with_promotion);
			
						$total_default_discount_price=($queryRecordsObj->quantity_with_promotion*$default_discount_price);
                                                $grand_total_default_discount_price+=intval($total_default_discount_price);
						$total_item_price_with_base_price=($queryRecordsObj->product_price*$queryRecordsObj->quantity_with_promotion);
									
						$str_promo.='<tr><td> Discount @ ('. $queryRecordsObj->promotion_discount.'%) <br>Eligible Quantity : '.$queryRecordsObj->quantity_with_promotion.'<br>For promotion<br><small style="font-size: .9em;color: blue">'.$queryRecordsObj->promotion_quote.'</small></td><td>'.$queryRecordsObj->quantity_with_promotion.' <i class="fa fa-times" aria-hidden="true"></i> '.$default_discount_price.'</td><td class="text-right" style="color:red"> '.curr_sym. $total_default_discount_price.'</td></tr>';
					 
					}
                                                 
					if(intval($queryRecordsObj->promotion_discount)>0 && (intval($queryRecordsObj->promotion_discount)!=intval($queryRecordsObj->default_discount))){

						$promo_discount_price=floatval($queryRecordsObj->ord_max_selling_price-$queryRecordsObj->individual_price_of_product_with_promotion);
														
						$total_promo_discount_price=($queryRecordsObj->quantity_with_promotion*$promo_discount_price);
						$total_item_price_with_base_price=($queryRecordsObj->product_price*$queryRecordsObj->quantity_with_promotion);
									
						$str_promo.='<tr><td>Promotion Discount @ ('.$queryRecordsObj->promotion_discount.'%)<br>Eligible Quantity : '.$queryRecordsObj->quantity_with_promotion.'<br>For Promotion<br><small style="font-size: .9em;color: blue">'.$queryRecordsObj->promotion_quote.'</small></td><td>'.$queryRecordsObj->quantity_with_promotion.' <i class="fa fa-times" aria-hidden="true"></i> '.$promo_discount_price.'</td><td class="text-right" style="color:red;">'.curr_sym. $total_promo_discount_price.'</td></tr>';
					}   
                                                    
					if(intval($queryRecordsObj->promotion_discount)==0 && $queryRecordsObj->promotion_default_discount_promo=="Price for remaining"){ 
						  
						if(intval($queryRecordsObj->default_discount>0)){ 
						  
							$str_promo.='<tr><td>Standard MRP @ ('.$queryRecordsObj->product_price.') <br>Eligible Quantity : '.$queryRecordsObj->quantity_with_promotion.'<br>For Promotion<br><small style="font-size: .9em;color: blue">'.$queryRecordsObj->promotion_quote.'</small></td><td>'.$queryRecordsObj->quantity_with_promotion.' <i class="fa fa-times" aria-hidden="true"></i> '.$queryRecordsObj->product_price.'</td><td class="text-right">'.curr_sym.$queryRecordsObj->total_price_of_product_with_promotion.'</td></tr>';
							 
						}else{					
							$str_promo.='<tr><td colspan="3">For Promotion<br><small style="font-size: .9em;color: blue">'.$queryRecordsObj->promotion_quote.'</small></td></tr>'; 
						} 
					}                          
				}                             
												
				if($promotion_item!='' || ($promotion_surprise_gift_skus!='')){
					if($promotion_item!=''){

						foreach($free_items_arr as $free_items){
							$id=$free_items->id;
							$str_promo.='<tr><td>SKU :'.$free_items->sku_id.'(free)</td><td>Quantity '.($temp_quotient*$two[$id]).'</td><td class="text-right"><del>'.curr_sym.(($temp_quotient*$two[$id])*$free_items->selling_price).'</del></td></tr>';
						}
					}

					if($promotion_surprise_gift_type=='Qty'){

						foreach($free_surprise_items as $surprise_items){
							$id=$surprise_items->id;
							$str_promo.='<tr><td>SKU : '.$surprise_items->sku_id.'(free)</td><td>Quantity '.($gift_with_count[$id]).'</td><td class="text-right"><del>'.curr_sym.(($gift_with_count[$id])*$surprise_items->selling_price).'</del></td></tr>';

						}
					}
				}
                                //&& $queryRecordsObj->promotion_quote==$queryRecordsObj->promotion_default_discount_promo
                                if($queryRecordsObj->promotion_default_discount_promo!="" && $queryRecordsObj->default_discount>0 ){
                                     /* straight discount */

                                 }else{
                                     if($queryRecordsObj->ord_selling_discount>0){

                                        $str_promo.='<tr><td> Discount @ ('. round($queryRecordsObj->ord_selling_discount).'%) <br>Eligible Quantity : '.$queryRecordsObj->quantity.'</td><td >'.$queryRecordsObj->quantity.' <i class="fa fa-times" aria-hidden="true"></i> '.curr_sym.''.round(($queryRecordsObj->ord_max_selling_price-$queryRecordsObj->product_price)).'</td><td class="text-right">'.curr_sym.''.round($queryRecordsObj->quantity*($queryRecordsObj->ord_max_selling_price-$queryRecordsObj->product_price)).'</td></tr>';
                                        $discount_saved=round($queryRecordsObj->quantity*($queryRecordsObj->ord_max_selling_price-$queryRecordsObj->product_price));
                                     }

                                 }
                                     
								
			}//promo available ;
                        
                        else{
                            if($queryRecordsObj->ord_selling_discount>0){
                                	$str_promo.='<tr><td> Discount @ ('. round($queryRecordsObj->ord_selling_discount).'%) <br>Eligible Quantity : '.$queryRecordsObj->quantity.'</td><td >'.$queryRecordsObj->quantity.' <i class="fa fa-times" aria-hidden="true"></i> '.curr_sym.''.round(($queryRecordsObj->ord_max_selling_price-$queryRecordsObj->product_price)).'</td><td class="text-right">'.curr_sym.''.round($queryRecordsObj->quantity*($queryRecordsObj->ord_max_selling_price-$queryRecordsObj->product_price)).'</td></tr>';
                                        $discount_saved=round($queryRecordsObj->quantity*($queryRecordsObj->ord_max_selling_price-$queryRecordsObj->product_price));
                            }
                        }
                                              
			$str_promo.='<tr>';

			if($promotion_invoice_free_shipping>0){ 

				$str_promo.='<td>Shipping Charge ( <span style="color:red">waived off</span> )</td>';
				$str_promo.='<td>'.$queryRecordsObj->quantity.'units(s)</td>
				<td class="text-right" style="color:red">'.curr_sym.$queryRecordsObj->shipping_charge.'</td>';
			} 
			$str_promo.='</tr>';

			if($queryRecordsObj->cash_back_value!="" && $queryRecordsObj->cash_back_value!=0){
				
				$str_promo.='<tr>';
				$str_promo.='<td>Cashback value of '.$queryRecordsObj->promotion_cashback.'% <br><em>(credited to wallet) </em></td>
				<td>'.$queryRecordsObj->quantity.' <i class="fa fa-times" aria-hidden="true"></i>  '. round($queryRecordsObj->cash_back_value/$queryRecordsObj->quantity).'</td>
				<td class="text-right" >'.curr_sym.$queryRecordsObj->cash_back_value.'</td>';
				$str_promo.='</tr>';
			}
                        
                        
                          /*sku coupon */                
                        $sku_coupon_used=0;
                        if($queryRecordsObj->ord_coupon_status=='1'){ 

                            $sku_coupon_used=round($queryRecordsObj->ord_coupon_reduce_price);

                            $str_promo.='<tr><td colspan="2">Coupon applied on SKU <b>(';
                            $str_promo.=($queryRecordsObj->ord_coupon_type=='2') ? curr_sym : '';
                            $str_promo.=round($queryRecordsObj->ord_coupon_value);
                            $str_promo.=($queryRecordsObj->ord_coupon_type=='1') ? '%' : 'Flat';
                            $str_promo.=')</b>';

                            $str_promo.='</td><td class="text-right">'.curr_sym;
                            $str_promo.=round($queryRecordsObj->ord_coupon_reduce_price);
                            $str_promo.='</td></tr>';
                        }
                                               
                        /*sku coupon */
                        
						/* addon section */
						/*if($queryRecordsObj->ord_addon_products_status=='1'){
							$total_amount_normal+=round($queryRecordsObj->ord_addon_total_price);
							$addon=json_decode($queryRecordsObj->ord_addon_products);
							//print_r($addon);
							$addon_count=count($addon);
							
							$str_promo.='<tr>';
							$str_promo.='<td>Additional products</td>';
							$str_promo.='<td>'.$addon_count.' items'.'</td>';
							$str_promo.='<td class="text-right" >'.curr_sym. round($queryRecordsObj->ord_addon_total_price).'</td>';
							$str_promo.='</tr>';
							
						}*/
						/* addon section */

                        /*Minus text*/
                        $minustext='';
                                                
                        if($discount_saved>0 || $sku_coupon_used){
                            $minustext='(';
                            $minustext.=curr_sym.$total_amount_normal;
                            if(intval($discount_saved)>0){
                                $minustext.='-'.curr_sym.$discount_saved;
                            }
                            if($total_promo_discount_price>0){
                                $minustext.='-'.curr_sym.$total_promo_discount_price;
                                $promo_dis+=round($total_promo_discount_price);
                            }
                            if(intval($grand_total_default_discount_price)>0){
                                $minustext.='-'.curr_sym.$grand_total_default_discount_price;
                                $promo_dis+=round($grand_total_default_discount_price);
                            }
                            if($sku_coupon_used>0){
                                $minustext.='-'.curr_sym.$sku_coupon_used;
                            }
                            $minustext.=')'; 
                        }
                        /*Minus text*/
                        
			if($promotion_invoice_free_shipping>0){
			  
				$str_promo.='<tr><td colspan="2"><em><b>Amount to pay</b></em>'.$minustext.'</td><td class="text-right"><b>'.curr_sym.($queryRecordsObj->subtotal-$sku_coupon_used).'</b></td></tr>';
			}else{ 
				$str_promo.='<tr><td colspan="2"><em><b>Amount to pay </b></em>'.$minustext.'</td><td class="text-right"><b>'.curr_sym. ($queryRecordsObj->grandtotal-$sku_coupon_used).'</b></td></tr>';
			} 
   
			if($total_amount_normal>$queryRecordsObj->grandtotal){				
				$you_saved=($total_amount_normal-$queryRecordsObj->grandtotal);
				if($promotion_invoice_free_shipping>0){
					$you_saved+=$shipping_price_normal;
				}
				if(intval($sku_coupon_used)>0){
					$you_saved+=$sku_coupon_used;
				}
				$str_promo.='<tr><td colspan="2"><em><b>Customer Saved</b></em></td><td class="text-right" style="color:green"><b>'.curr_sym. round($you_saved).'</b></td></tr>';
			}
						
			$str_promo.='</table>';

			


///////////////////////new stucture//////////////

				$str_promo.='</div>';

				$str_promo.='</div>';

				$str_promo.='</div>';
				$str_promo.='</div>';
				
				$str_promo.="</div>";

				return $str_promo;
	
		}
	public function get_addon_data($queryRecordsObj){
		$str='';
		/* addon section  */
		//if($queryRecordsObj->ord_addon_products_status=='1'){
				if(0){
			$arr=json_decode($queryRecordsObj->ord_addon_products);
			$count=count($arr);
			if($queryRecordsObj->ord_addon_inventories!=''){
				//$str.='<h6 class="margin-top"><i><b>Combo products</b></i></h6>';
				$str.='<table class="table table-bordered" style="font-size: .8em;text-align:justify;margin-bottom: 0px;">';
				$str.='<thead><th width="50%">Image</th><th width="50%">Details</th></thead>';

				$i=1;
				foreach($arr as $val){
					
					$inv_id=$val->inv_id;
					$name=$val->inv_name;
					$image=$val->inv_image;
					$sku_id=$val->inv_sku_id;
					$price=$val->inv_price;
					$img_path=get_inventory_image_path_combo($inv_id,$image);
					$str.='<tr><th><img src="'.base_url().$img_path.'" style="width:40%;height:50%;"></th><th>
					Name : '.$name.'<br>SKU id : '.$sku_id.'<br>Price : '.curr_sym.$price.'
					</th></tr>';
				}
				$str.='</table>';
		}
	}

	/* addon section  */

	return $str;
	}
	public function get_details_of_promotion_for_replaced_order($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal,$prev_order_item_id){

			$str_promo='';
			$str_promo_quote='';
			$promotion_available=$queryRecordsObj->promotion_available;
			$order_item_id=$queryRecordsObj->order_item_id;
			$str_promo.='<div id="promotion_'.$order_item_id.'" class="" >';

			$str_promo.='<div class="promo_view" id="accordion_'.$order_item_id.'">';
			$str_promo.='<div class="">';

			$str_promo.='<div class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_'.$order_item_id.'" href="#collapseOne_'.$order_item_id.'" style="cursor:pointer;">';

			//$str_promo.='<div class=" panel-title" >';

			//$str_promo.='<i class="more-less glyphicon glyphicon-plus"></i>';
			$str_promo.='<u class="bold text-info">Details</u>';

			//$str_promo.='</div>';

			$str_promo.='</div>';
			$str_promo.='<div id="collapseOne_'.$order_item_id.'" class="panel-collapse collapse"> ';
			$str_promo.='<div class="panel-body">';

			$str_promo.='<table class="table table-bordered" style="font-size: .8em;text-align:justify;margin-bottom: 0px;">';
													 
			$str_promo.='<thead><tr><th style="padding:0px"></th><th width="25%" style="padding:0px"></th><th style="padding:0px" width="25%" style="text-align: right"></th></tr></thead>';

			$total_product_price_normal=($queryRecordsObj->quantity*$queryRecordsObj->product_price);
			$shipping_price_normal=$queryRecordsObj->shipping_charge;

			$total_amount_normal=($shipping_price_normal+$total_product_price_normal);

			$str_promo.='<tr><td>Base product price</td><td>'.$queryRecordsObj->quantity.' <i class="fa fa-times" aria-hidden="true"></i> '.$queryRecordsObj->product_price.'</td><td class="text-right">'.curr_sym.$total_product_price_normal.'</td></tr>';
			
			$str_promo.='<tr><td colspan="2">Shipping Charge for '.$queryRecordsObj->quantity.'unit(s)</td><td class="text-right">'.curr_sym.$shipping_price_normal.'</td></tr>';
			
			$str_promo.='<tr><td colspan="2">Total value </td><td class="text-right">'.curr_sym.$total_amount_normal.'</td></tr>';
			$str_promo.='</table>';


			$str_promo.='</div>';

			$str_promo.='</div>';

			$str_promo.='</div>';
			$str_promo.='</div>';
			
			$str_promo.="</div>";

			return $str_promo;
	}
	/*low stock notification*/
	public function low_stock_notification(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["menu_flag"]="notification_active_links";	
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/low_stock_notification_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function low_stock_notification_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_orders->low_stock_notification_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_orders->low_stock_notification_processing($params,"get_recs");
			$i=1;
			foreach($queryRecordsResult as $queryRecordsObj){
				$row[]=$i;
				
				$attr='<ul class="list-unstyled small">';
				$attr.='<li>SKU ID: '.$queryRecordsObj->sku_id.'</li>';
				$attr.='<li>Attributes</li>';
				$attr.="<li>".$queryRecordsObj->attribute_1." : ".$queryRecordsObj->attribute_1_value.'</li>';
				if(!empty($queryRecordsObj->attribute_2))
				$attr.="<li>".$queryRecordsObj->attribute_2." : ".$queryRecordsObj->attribute_2_value.'</li>';
				if(!empty($queryRecordsObj->attribute_3))
				$attr.="<li>".$queryRecordsObj->attribute_3." : ".$queryRecordsObj->attribute_3_value.'</li>';
				if(!empty($queryRecordsObj->attribute_4))
				$attr.="<li>".$queryRecordsObj->attribute_4." : ".$queryRecordsObj->attribute_4_value.'</li>';
				$attr.='</ul>';
				$row[]=$attr;
				$catdet ='<ul class="list-unstyled small">';
				$catdet.='<li>SKU Name: '.$queryRecordsObj->sku_name.'</li>';
				$catdet.='<li>Product Name: '.$queryRecordsObj->product_name.'</li>';
				$catdet.='<li>Brand: '.$queryRecordsObj->brand_name.'</li>';
				$catdet.='<li>Subcategory: '.$queryRecordsObj->subcat_name.'</li>';
				$catdet.='<li>Category: '.$queryRecordsObj->cat_name.'</li>';
				if($queryRecordsObj->pcat_name!=""){
				$catdet.='<li>Parent Category: '.$queryRecordsObj->pcat_name.'</li>';
				}
				$catdet.='</ul>';
				$row[]=$catdet;
				
				$row[]=$queryRecordsObj->stock;
				$row[]=$queryRecordsObj->low_stock;
				$row[]=$queryRecordsObj->cutoff_stock;
				
				
				$row[]='<button class="btn btn-info btn-xs btn-block" onclick="update_stock_of_inventory('.$queryRecordsObj->id.','."'".$queryRecordsObj->sku_id."'".','.$queryRecordsObj->stock.')">Update Stock</button>';

				$data[]=$row;
				$row=array();
				$i++;
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function update_inventory_stock(){
		$inventory_id=$this->input->post("inventory_id");
		$sku_id=$this->input->post("SKUID_in_update_stock");
		$stock=$this->input->post("stock");
		$result=$this->Model_orders->update_inventory_stock($inventory_id,$stock);
		
		$stock_available_status=$this->Model_orders->get_stock_available_or_not_by_inventory($sku_id,$inventory_id);
		$out_of_stock_switch_status=$this->Model_orders->get_out_of_stock_switch();
		if($out_of_stock_switch_status=="yes"){
		if($stock_available_status=="yes"){
			
				
				
			
						$email_stuff_arr=email_stuff();
						
						$email_obj=$this->Model_orders->get_list_of_emails($sku_id,$inventory_id);
						if(!empty($email_obj)){
						foreach($email_obj as $email){	
							$to_email_addresses=$email->email;
							if($to_email_addresses!=""){
								$to_name=$this->Model_orders->get_customer_name_by_emailid($to_email_addresses);
								if($to_name==""){
									$email_name=$to_email_addresses;
								}
								else{
									$email_name=$to_name;
								}
								$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr>
								
								<tr> <td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi '.$email_name.', </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">Stock is now available for this SKU ID <b>'.$sku_id.'</b>. You can now purchase it online, before stock last. Hurry up.</p><br><p style="text-align:center;padding:0;margin:0" align="center"> <a style="width:200px;margin:0px auto;background-color:#ff8c00;text-align:center;border:#ff8c00 solid 1px;padding:8px 0;text-decoration:none;border-radius:2px;display:block;color:#fff;font-size:13px" align="center" href="'.base_url().'detail/56axcdo62'.$inventory_id.'" target="_blank"> <span style="color:#ffffff;font-size:13px;background-color:#ff8c00">Buy Now</span> </a> </p><br></td></tr><tr> <td> <table width="100%" cellspacing="0" cellpadding="0" style="max-width:802px;"> <tbody> <tr> <td valign="top" align="center" width="300" style="background-color:#f9f9f9"> <br><table width="100%" cellspacing="0" cellpadding="0"> <tbody> <tr> <td valign="top" align="left" style="padding:0 10px 15px 20px;margin:0"> <p style="padding:0;margin:0 0 7px 0;font-size:16px;color:#565656">Any Questions?</p><p style="padding:0;margin:0;font-size:11px;color:#565656;line-height:20px">Write to us at '.$email_stuff_arr["fromemail_emailtemplate"].' </p></td></tr></tbody> </table> </td></tr></tbody> </table> </td></tr>';
								
								
								$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';

								$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

								$usermessage.='</tbody> </table></tr></table></center> </body></html>';
							
								
								$mail = new PHPMailer;
								/*$mail->isSMTP();
								$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
								$mail->Port = '587';//  live - comment it 
								$mail->Auth = true;//  live - comment it 
								$mail->SMTPAuth = true;
								$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
								$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
								$mail->SMTPSecure = 'tls';*/
								$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
								$mail->FromName = $email_stuff_arr["name_emailtemplate"];
								$mail->addAddress($to_email_addresses);
								$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
								$mail->WordWrap = 50;
								$mail->isHTML(true);
								$mail->Subject = 'Stock Notification from '.$email_stuff_arr["name_emailtemplate"];
								$mail->Body    = $usermessage;
										
										$sent=$mail->send();
										if($sent==true){
											$res=$this->Model_orders->update_mailsent_in_request_for_out_of_stock($inventory_id,$sku_id);
											//////
											$mail_count=$this->mail_notification_btn_show_or_hide($inventory_id);
											$mobile_count=$this->mobile_notification_btn_show_or_hide($inventory_id);
											if($mail_count=="show" or $mobile_count=="show"){
												
											}
											else{
												$rn=rand(1,50000);
												$update_notification_group_id=$this->Model_orders->update_notification_group_id($inventory_id,$rn);
												if($update_notification_group_id){
													$this->Model_orders->completed_notification_group_id($inventory_id,$rn);
												}
											}
											///////
											echo $res;
										}else{
											echo "not sent";
										}
							}
								
						}
						}
						else{
							echo true;
						}
					
					
					
			
		}
		}
		else{
			echo $result;
		}
	}
	public function request_for_out_of_stock(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["menu_flag"]="notification_active_links";	
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/request_for_out_of_stock_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function mail_notification_btn_show_or_hide($inventory_id){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$count=$this->Model_orders->mail_notification_btn_show_or_hide($inventory_id);
			if($count>0){
				return "show";
			}
			else{
				return "hide";
			}
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function mobile_notification_btn_show_or_hide($inventory_id){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$count=$this->Model_orders->mobile_notification_btn_show_or_hide($inventory_id);
			if($count>0){
				return "show";
			}
			else{
				return "hide";
			}
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function request_for_out_of_stock_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_orders->request_for_out_of_stock_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_orders->request_for_out_of_stock_processing($params,"get_recs");
			$i=1;
			foreach($queryRecordsResult as $queryRecordsObj){
				$row[]=$i;
				$attr='<ul class="list-unstyled small">';
				$attr.='<li>SKU ID: '.$queryRecordsObj->sku_id.'</li>';
				$attr.='<li>Attributes</li>';
				$attr.='<li>'.$queryRecordsObj->attribute_1." : ".$queryRecordsObj->attribute_1_value.'</li>';
				if(!empty($queryRecordsObj->attribute_2))
				$attr.='<li>'.$queryRecordsObj->attribute_2." : ".$queryRecordsObj->attribute_2_value.'</li>';
				if(!empty($queryRecordsObj->attribute_3))
				$attr.='<li>'.$queryRecordsObj->attribute_3." : ".$queryRecordsObj->attribute_3_value.'</li>';
				if(!empty($queryRecordsObj->attribute_4))
				$attr.='<li>'.$queryRecordsObj->attribute_4." : ".$queryRecordsObj->attribute_4_value.'</li>';
				$attr.='</ul>';
				$row[]=$attr;
				
				
				$str='';

				$str.='<ul class="list-unstyled small">';
				$str.='<li>Sku name: '.$queryRecordsObj->sku_name.'</li>';
				$str.='<li>Product: '.$queryRecordsObj->product_name.'</li>';
				$str.='<li>Brand: '.$queryRecordsObj->brand_name.'</li>';
				$str.='<li>Subcategory: '.$queryRecordsObj->subcat_name.'</li>';
				$str.='<li>Category: '.$queryRecordsObj->cat_name.'</li>';
				if($queryRecordsObj->pcat_name!=""){
				$str.='<li>Parent Category: '.$queryRecordsObj->pcat_name.'</li>';
				}
				$str.='</ul>';

				$row[]=$str;
				$stock='';
				$stock.='<ul class="list-unstyled small">';
				$stock.="<li>Count of Requests: ".$queryRecordsObj->request_count."</li>";
				if($queryRecordsObj->stock<$queryRecordsObj->low_stock){
					$stock.='<li>Available Stock: <span class="text-danger bold">'.$queryRecordsObj->stock.'<span></li>';
				}else{
					$stock.='<li>Available Stock: '.$queryRecordsObj->stock.'</li>';
				}
				$stock.='</ul>';
				$row[]=$stock;
				$sku_id=$queryRecordsObj->sku_id; $inventory_id=$queryRecordsObj->id;
				$email_obj=$this->Model_orders->get_list_of_emails($sku_id,$inventory_id);
				
				
				$email_str='';
				if(!empty($email_obj)){
					
					$email_str.='<div id="email_promotion_'.$queryRecordsObj->id.'" class="" >';

					$email_str.='<div class="promo_view" id="email_accordion_'.$queryRecordsObj->id.'">';
					$email_str.='<div class="">';

					$email_str.='<div class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#email_accordion_'.$queryRecordsObj->id.'" href="#email_collapseOne_'.$queryRecordsObj->id.'" style="cursor:pointer;">';

					$email_str.='<u class="bold text-success">Email Addresses</u>';


					$email_str.='</div>';
					$email_str.='<div id="email_collapseOne_'.$queryRecordsObj->id.'" class="panel-collapse collapse"> ';
					$email_str.='<div class="panel-body small">';
					$email_str_obj='';
						
						foreach($email_obj as $email){	
							$email_str_obj.=$email->email.', ';	
						}
						
					$email_str.=rtrim($email_str_obj,', ');

					$email_str.='</div>';

					$email_str.='</div>';

					$email_str.='</div>';
					$email_str.='</div>';
					
					$email_str.="</div>";
					
				
				
				
				if($queryRecordsObj->stock<$queryRecordsObj->low_stock){
					$email_str.='<span style="color:red;">Out of Stock</span>';
				}else{
					$mail_btn_show_or_hide=$this->mail_notification_btn_show_or_hide($inventory_id);
					if($mail_btn_show_or_hide=="show"){
						$email_str.='<button class="btn btn-success btn-xs" onclick="send_mail_to_notify('.$queryRecordsObj->id.','."'".$queryRecordsObj->sku_id."'".')">Send Mail to Notify</button>';
					}else{
						//$queryRecordsObj->mail_sent==1;
						$time = strtotime($queryRecordsObj->mail_sent_timestamp);
						$myFormatForView = date("d-m-Y g:i A", $time);
						
						$mail_sent='<span class="text-success">Mail Sent</span><br>';
						$mail_sent.='<small><span style=""> at <b>'.$myFormatForView.'</b></span></small><br>';
						$email_str.=$mail_sent;
					}
				}
				}else{
					$email_str.='<p class="lead">No E-mail requests</p>';
				}
				$row[]=$email_str;
				$mobile_obj=$this->Model_orders->get_list_of_mobiles($sku_id,$inventory_id);
				$mobile_str='';
				if(!empty($mobile_obj)){
					
					$mobile_str.='<div id="mobile_promotion_'.$queryRecordsObj->id.'" class="" >';

					$mobile_str.='<div class="promo_view" id="mobile_accordion_'.$queryRecordsObj->id.'">';
					$mobile_str.='<div class="">';

					$mobile_str.='<div class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#mobile_accordion_'.$queryRecordsObj->id.'" href="#mobile_collapseOne_'.$queryRecordsObj->id.'" style="cursor:pointer;">';

					$mobile_str.='<u class="bold text-success">Mobile Numbers</u>';


					$mobile_str.='</div>';
					$mobile_str.='<div id="mobile_collapseOne_'.$queryRecordsObj->id.'" class="panel-collapse collapse"> ';
					$mobile_str.='<div class="panel-body"><small>';
					$mobile_str_obj='';
						
						foreach($mobile_obj as $mobile){	
							$mobile_str_obj.=$mobile->mobile.',';	
						}
						
					$mobile_str.=trim($mobile_str_obj,',');
					$mobile_str.='</small></div>';

					$mobile_str.='</div>';

					$mobile_str.='</div>';
					$mobile_str.='</div>';
					
					$mobile_str.="</div>";
					
				
				if($queryRecordsObj->stock<$queryRecordsObj->low_stock){
					$mobile_str.='<span style="color:red;">Out of Stock</span>';
				}else{
					$mobile_btn_show_or_hide=$this->mobile_notification_btn_show_or_hide($inventory_id);
					if($mobile_btn_show_or_hide=="show"){
						$mobile_str.='<button class="btn btn-success btn-xs" onclick="send_message_to_notify('.$queryRecordsObj->id.','."'".$queryRecordsObj->sku_id."'".')">Send Mail to Notify</button>';
					}else{
						//$queryRecordsObj->mail_sent==1;
						$mobiletime = strtotime($queryRecordsObj->mobile_sent_timestamp);
						$myFormatFormobileView = date("d-m-Y g:i A", $mobiletime);
						
						$mobile_sent='<span class="text-success">Mail Sent</span><br>';
						$mobile_sent.='<small><span style=""> at <b>'.$myFormatFormobileView.'</b></span></small><br>';
						$mobile_str.=$mobile_sent;
					}
				}
				}else{
					$mobile_str.='<p class="lead">No SMS requests</p>';
				}
				$row[]=$mobile_str;
				$data[]=$row;
				$row=array();
				$i++;
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function send_mail_to_notify(){
		$email_stuff_arr=email_stuff();
		$inventory_id=$this->input->post("inventory_id");
		$sku_id=$this->input->post("sku_id");
		$email_obj=$this->Model_orders->get_list_of_emails($sku_id,$inventory_id);
		
		foreach($email_obj as $email){	
			$to_email_addresses=$email->email;
			if($to_email_addresses!=""){
			
				$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr>
				
				
				<tr> <td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi Customer, </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">Stock is now available for this SKU ID <b>'.$sku_id.'</b>. You can now purchase it online, before stock last. Hurry up.</p><br></td></tr><tr> <td> <table width="100%" cellspacing="0" cellpadding="0" style="max-width:802px;"> <tbody> <tr> <td valign="top" align="center" width="300" style="background-color:#f9f9f9"> <br><table width="100%" cellspacing="0" cellpadding="0"> <tbody> <tr> <td valign="top" align="left" style="padding:0 10px 15px 20px;margin:0"> <p style="padding:0;margin:0 0 7px 0;font-size:16px;color:#565656">Any Questions?</p><p style="padding:0;margin:0;font-size:11px;color:#565656;line-height:20px">Write to us at '.$email_stuff_arr["fromemail_emailtemplate"].' </p></td></tr></tbody> </table> </td></tr></tbody> </table> </td></tr><tr> <td> <table border="0" cellpadding="0" cellspacing="0" width="100%" class="flexibleContainer" style="background-color:#2e2e2e;"> <tr> <td align="center" valign="top" width="500" class="flexibleContainerCell" style="padding: 0px;"> <table border="0" cellspacing="0" width="100%"> <tr> <td valign="top" bgcolor="#2e2e2e"> <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#fff;text-align:center;line-height:140%;"> <p>'.$email_stuff_arr["footercopyrights_emailtemplate"].'</p></div></td></tr></table> </td></tr></table> </td></tr></tbody> </table> </tr></table> </center></body></html>';
			
				
				$mail = new PHPMailer;
				/*$mail->isSMTP();
				$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
				$mail->Port = '587';//  live - comment it 
				$mail->Auth = true;//  live - comment it 
				$mail->SMTPAuth = true;
				$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
				$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
				$mail->SMTPSecure = 'tls';*/
				$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
				$mail->FromName = $email_stuff_arr["name_emailtemplate"];
				$mail->addAddress($to_email_addresses);
				$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				$mail->Subject = 'Stock Notification from '.$email_stuff_arr["name_emailtemplate"];
				$mail->Body    = $usermessage;
						
						$sent=$mail->send();
						if($sent==true){
							$res=$this->Model_orders->update_mailsent_in_request_for_out_of_stock($inventory_id,$sku_id);
							//////
							echo $mail_count=$this->mail_notification_btn_show_or_hide($inventory_id);
							echo $mobile_count=$this->mobile_notification_btn_show_or_hide($inventory_id);
							if($mail_count=="show" or $mobile_count=="show"){
								
							}
							else{
								$rn=rand(1,50000);
								$update_notification_group_id=$this->Model_orders->update_notification_group_id($inventory_id,$rn);
								if($update_notification_group_id){
									$this->Model_orders->completed_notification_group_id($inventory_id,$rn);
								}
							}
							///////
							echo $res;
						}else{
							echo "not sent";
						}
			}
				
		}
	}
	public function send_message_to_notify(){
		$inventory_id=$this->input->post("inventory_id");
		$sku_id=$this->input->post("sku_id");
		$mobile_obj=$this->Model_orders->get_list_of_mobiles($sku_id,$inventory_id);
		foreach($mobile_obj as $mobile){	
		$to_mobile_numbers=$mobile->mobile;	
		if($to_mobile_numbers!=''){
			
			
			$mobile_stuff_arr=mobile_stuff();
						$authKey = $mobile_stuff_arr["authKey_smstemplate"];
						$mobileNumber = "+91".$to_mobile_numbers;
						$senderId = $mobile_stuff_arr["senderId_smstemplate"];
						
						
			
			
			$message = urlencode("A product you requested for notification with SKU ID $sku_id is now available for purchase.");
			$route = "4";
			$postData = array(
				'authkey' => $authKey,
				'mobiles' => $mobileNumber,
				'message' => $message,
				'sender' => $senderId,
				'route' => $route
			);

			$url="https://control.msg91.com/api/sendhttp.php";
			$ch = curl_init();
			curl_setopt_array($ch, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $postData
			));
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

			$output = curl_exec($ch);
			
			if(curl_errno($ch)){
				echo 'error:' . curl_error($ch);
			}else{
						
						if($output==true){
							$res=$this->Model_orders->update_mobilesent_in_request_for_out_of_stock($inventory_id,$sku_id);
							
							//////
							$mail_count=$this->mail_notification_btn_show_or_hide($inventory_id);
							$mobile_count=$this->mobile_notification_btn_show_or_hide($inventory_id);
							if($mail_count=="show" or $mobile_count=="show"){
								
							}
							else{
								$rn=rand(1,50000);
								$update_notification_group_id=$this->Model_orders->update_notification_group_id($inventory_id,$rn);
								if($update_notification_group_id){
									$this->Model_orders->completed_notification_group_id($inventory_id,$rn);
								}
							}
							///////
							
							
							echo $res;
						}else{
							echo "not sent";
						}
				
			}
			
			curl_close($ch);
			
					
		}
		}
	}
	public function update_wallet_for_surprise_gift($order_item_id,$customer_id,$total_amount_need_to_be_refund){
		
		$credit_bank_transfer=1;
		$refund_wallet_comments="surprise gift on delivery of item";
		$this->Model_orders->activate_wallet_customer($customer_id);
		/////
		/*get customer wallet id*/

		$flag=$this->Model_orders->update_order_item_offers($order_item_id,$order_id,$customer_id,$refund_wallet_comments,$total_amount_need_to_be_refund);
		
		return $flag;
		////////////////
	}
	public function update_wallet_for_cashback($order_item_id,$customer_id,$total_amount_need_to_be_refund){
		
		$credit_bank_transfer=1;
		$refund_wallet_comments="Cash back amount when item is delivered";
		$this->Model_orders->activate_wallet_customer($customer_id);
		
		/*get customer wallet id*/
		$wallet_summary=$this->Model_orders->get_customer_wallet_summary($customer_id);
		foreach($wallet_summary as $summary){
			$wallet_id=$summary['wallet_id'];
			$amount=$summary['wallet_amount'];
		}
		
		$transaction_details=$refund_wallet_comments;
		$debit="";
		$credit=$total_amount_need_to_be_refund;
		$amount=$credit+$amount;
		$flag=$this->Model_orders->update_wallet($wallet_id,$customer_id,$transaction_details,$debit,$credit,$amount,$credit_bank_transfer);
		
		return $flag;
		////////////////
	}
	public function get_invoice_offers_str($obj){
		
		$invoice_surprise_gift_promo_quote=$obj->invoice_surprise_gift_promo_quote;
		$invoice_surprise_gift=$obj->invoice_surprise_gift;
		$invoice_surprise_gift_type=$obj->invoice_surprise_gift_type;
		$invoice_surprise_gift_skus=$obj->invoice_surprise_gift_skus;
		$invoice_surprise_gift_skus_nums=$obj->invoice_surprise_gift_skus_nums;
		
		$promotion_invoice_cash_back=$obj->promotion_invoice_cash_back;
		$promotion_invoice_free_shipping=$obj->promotion_invoice_free_shipping;
		$promotion_invoice_discount=$obj->promotion_invoice_discount;
		$invoice_coupon_status=$obj->invoice_coupon_status;
		
		$order_id=$obj->order_id;
		$invoice_offer_achieved=$obj->invoice_offer_achieved;
		
		$w=0;
		if($invoice_offer_achieved==1){
		
			if(!empty($invoice_surprise_gift) && $invoice_surprise_gift!=0){
				$w++;
			}
			if(!empty($promotion_invoice_cash_back) && $promotion_invoice_cash_back!=0 ){
				$w++;
			}
			if(!empty($promotion_invoice_free_shipping) && $promotion_invoice_free_shipping!=0){
				$w++;
			}
			if(!empty($promotion_invoice_discount) && $promotion_invoice_discount!=0){
				$w++;
			}
			if(!empty($invoice_coupon_status) && $invoice_coupon_status!=0){
				$w++;
			}
		}
		return $w;
	}
	public function invoices_free_items_status(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$this->Model_orders->notify_free_items_unset_invoice();
			$data["menu_flag"]="invoices_free_items_status";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/invoices_free_items_status_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	public function invoices_free_items_status_processing(){
		
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_orders->invoices_free_items_status_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_orders->invoices_free_items_status_processing($params,"get_recs");
			
			//print_r($queryRecordsResult);
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]="<input type='checkbox' name='free_orders_list' value='".$queryRecordsObj->invoices_free_items_status_id."'>";
				if($queryRecordsObj->sku_name!=''){
                                    $row[]=$queryRecordsObj->sku_name;	
                                }else{
                                    $row[]=$queryRecordsObj->product_name;	
                                }
				$row[]=$queryRecordsObj->order_id;		
				$row[]=$queryRecordsObj->invoices_offers_id;		
				$row[]=$queryRecordsObj->sku_id;	
				$btn='';
				//print_r($queryRecordsObj);
				
				if(($queryRecordsObj->item_delivered==0) && ($queryRecordsObj->customer_response_for_surprise_gifts!='' && $queryRecordsObj->customer_response_for_surprise_gifts!='reject')){
					$btn='<button id="update_status_'.$queryRecordsObj->invoices_free_items_status_id.'" class="btn btn-primary btn-xs btn-block" onclick="open_modal_to_update_status_fun('.$queryRecordsObj->invoices_free_items_status_id.','."'".$queryRecordsObj->order_id."'".','.$queryRecordsObj->item_shipped.','.$queryRecordsObj->item_delivered.','.$queryRecordsObj->item_undelivered.')" >Update Status</button>';
				}
				
				
				if(($queryRecordsObj->item_delivered==1) && ($queryRecordsObj->customer_response_for_surprise_gifts!='')){
					$btn='<button id="update_status_'.$queryRecordsObj->invoices_free_items_status_id.'" class="btn btn-success btn-xs btn-block" >Delivered</button>';
					if($queryRecordsObj->invoice_number==''){
						$btn='<button id="generate_free_item_invoice_'.$queryRecordsObj->invoices_free_items_status_id.'" class="btn btn-success btn-xs btn-block" onclick="generate_free_item_invoice('.$queryRecordsObj->invoices_free_items_status_id.','."'".'generate'."'".','."'".$queryRecordsObj->order_id."'".')" >Generate Invoice</button>';
					}else{
						$btn='<button id="generate_free_item_invoice_'.$queryRecordsObj->invoices_free_items_status_id.'" class="btn btn-success btn-xs btn-block" onclick="generate_free_item_invoice('.$queryRecordsObj->invoices_free_items_status_id.','."'".'view'."'".','."'".$queryRecordsObj->order_id."'".')" >View Invoice</button>';
					}
					
				}
				if(($queryRecordsObj->item_undelivered==1) && ($queryRecordsObj->customer_response_for_surprise_gifts!='')){
					$btn='<button id="update_status_'.$queryRecordsObj->invoices_free_items_status_id.'" class="btn btn-danger btn-xs btn-block" >Not delivered </button>';	
				}
			
				$row[]=$btn.'<button id="view_details_'.$queryRecordsObj->invoices_free_items_status_id.'" class="btn btn-warning btn-xs btn-block" onclick="view_details_fun('.$queryRecordsObj->invoices_free_items_status_id.','."'".$queryRecordsObj->order_id."'".')" >View details</button>';		
				$data[]=$row;
				$row=array();	
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_data_from_free_items_status(){
		$order_id=$this->input->post("order_id");
		$invoices_free_items_status_id=$this->input->post("invoices_free_items_status_id");
		$resObj=$this->Model_orders->get_data_from_free_items_status($order_id,$invoices_free_items_status_id);
		
		$str='';
		if(!empty($resObj)){
			
			$str='';
			$str.='<table class="table bordered" style="font-size: .9em;text-align:justify">';
			
			if($resObj->customer_response_for_surprise_gifts=='')
			$str.='<thead><th colspan="2"><h5 class="alert alert-warning text-center">customer response for surprise gifts - " <b>Pending</b> " </h5></th></thead>';
			if($resObj->customer_response_for_surprise_gifts!='')
			$str.='<thead><th colspan="2" class="alert alert-warning text-center"><h5>Customer response for surprise gifts - " <b>'.$resObj->customer_response_for_surprise_gifts.'</b> " </h5>';
			if($resObj->item_shipped==1){
				$str.='<h5>Item was " <b>Shipped</b> " </h5>';
			}
			if($resObj->item_delivered==1){
				$str.='<h5>Item was " <b>Delivered</b> "</h5>';
			}
			if($resObj->item_undelivered==1){
				$str.='<h5>Item was " <b>Not Delivered</b> "</h5>';
			}
			if($resObj->admin_comments!=''){
				
				if(strlen($resObj->admin_comments)>200){
					$str.='<h5>Admin Comments : <div class="f-row margin-top more">'.str_split($resObj->admin_comments,200)[0].'<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' .substr($resObj->admin_comments,201). '</span>&nbsp;&nbsp;<a href="#" class="morelink" onclick="morelinkfun(this)">more</a></span></div></h5>';
				}else{
					$str.='<h5>Admin Comments : <div class="f-row margin-top more">'.$resObj->admin_comments.'</div></h5>';
				}
				
			}
			$str.='</th></thead>';
		
			$str.='<thead><th colspan="2"><h5 class="alert alert-info text-center">Promo Detais</h5></th></thead>';
			
			$str.='<tr><td>Invoice Offer Id</td><td>'.$resObj->invoices_offers_id.'</td></tr>';
			$str.='<tr><td>order id</td><td>'.$resObj->order_id.'</td></tr>';
			$str.='<tr><td>Promo quote</td><td>'.$resObj->invoice_surprise_gift_promo_quote.'</td></tr>';

			$str.='<thead><th colspan="2"><h5 class="alert alert-info text-center">Item details</h5></th></thead>';
			$str.='<tr><td>SKU ID</td><td>'.$resObj->sku_id.'</td></tr>';
			$str.='<tr><td>Quatity</td><td>'.$resObj->quantity.'</td></tr>';
			$str.='<tr><td>SKU Name</td><td>'.$resObj->sku_name.'</td></tr>';
			$str.='<tr><td>Product Name</td><td>'.$resObj->product_name.'</td></tr>';
			$str.='<tr><td>Product Image</td><td><img src="'.base_url().$resObj->image.'"  style="width:70px;height:100px;"><div style="float:left;width:150px;">';
			$resObj->attribute_1_value = substr($resObj->attribute_1_value, 0, strpos($resObj->attribute_1_value, ":"));
			if(!empty($resObj->attribute_1))	
				$str.=$resObj->attribute_1." : ".$resObj->attribute_1_value.'<br>';
			if(!empty($resObj->attribute_2))
				$str.=$resObj->attribute_2." : ".$resObj->attribute_2_value.'<br>';
			if(!empty($resObj->attribute_3))
				$str.=$resObj->attribute_3." : ".$resObj->attribute_3_value.'<br>';
			if(!empty($resObj->attribute_4))
				$str.=$resObj->attribute_4." : ".$resObj->attribute_4_value.'<br>';
			$str.='</div></td></tr>';
			
			$str_speed_duration="";
			$speed_territory_duration_arr=json_decode($resObj->speed_duration,true);
			foreach($speed_territory_duration_arr as $territory_name_id => $duration){
				$get_logistics_all_territory_name_data_arr=$this->Model_orders->get_logistics_all_territory_name_data($territory_name_id);
				$str_speed_duration.=$get_logistics_all_territory_name_data_arr["territory_name"]." = ".$duration."<br>";
			}
			
			
			$str.='<thead><th colspan="2"><h5 class="alert alert-info text-center">Logistics Details</h5></th></thead>';
			$str.='<tr><td>Vendor name</td><td>'.$resObj->name.'</td></tr>';
			$str.='<tr><td>Logistics Name</td><td>'.$resObj->logistics_name.'</td></tr>';
			$str.='<tr><td>Logistics weblink</td><td>'.$resObj->logistics_weblink.'</td></tr>';
			$str.='<tr><td>Logistics parcel type</td><td>'.$resObj->parcel_type.'</td></tr>';
			$str.='<tr><td>Parcel type description</td><td>'.$resObj->parcel_type_description.'</td></tr>';
			$str.='<tr><td>Logistics Delivery Mode</td><td>'.$resObj->delivery_mode.'</td></tr>';
			$str.='<tr><td>Logistics Delivery speed duration(Days)</td><td>'.$str_speed_duration.'</td></tr>';
			$str.='<thead><th colspan="2"><h5 class="alert alert-info text-center">Shipping Details</h5></th></thead>';
			$str.='<tr><td>Customer Name</td><td>'.$resObj->customer_name.'</td></tr>';
			$str.='<tr><td>Address Line 1</td><td>'.$resObj->address1.'</td></tr>';
			$str.='<tr><td>Address Line 2</td><td>'.$resObj->address2.'</td></tr>';
			$str.='<tr><td>City</td><td>'.$resObj->city.'</td></tr>';
			$str.='<tr><td>State</td><td>'.$resObj->state.'</td></tr>';
			$str.='<tr><td>Country</td><td>'.$resObj->country.' </td></tr>';
			$str.='<tr><td>Pincode</td><td>'.$resObj->pincode.' </td></tr>';
			$str.='<tr><td>Mobile</td><td>'.$resObj->mobile.' </td></tr>';
			$str.='<tr><td>Email</td><td>'.$resObj->email.' </td></tr>';
			
			
			$str.='</table>';
			
		}
		echo $str;
		
	}
	public function update_free_item_status_on_invoice(){
		$order_id=$this->input->post("order_id");
		$invoices_free_items_status_id=$this->input->post("invoices_free_items_status_id");
		$status=$this->input->post("status");
		$item_delivered_date=$this->input->post("item_delivered_date");
		$item_delivered_time=$this->input->post("item_delivered_time");
		$admin_comments=addslashes($this->input->post("admin_comments"));
		
		$update_stock=$this->input->post("update_stock");
		
		if(isset($update_stock)){
			if($update_stock=="yes" && $status=="undelivered"){
				$obj=$this->Model_orders->get_all_order_invoice_offers_obj($order_id);
				if(!empty($obj)){
					$invoice_surprise_gift_skus=$obj->invoice_surprise_gift_skus;
					$invoice_surprise_gift_skus_nums=$obj->invoice_surprise_gift_skus_nums;
					$flag2=$this->Model_orders->update_stock_invoice_surprise_gifts_on_accept($invoice_surprise_gift_skus,$invoice_surprise_gift_skus_nums);
				}
				
			}
		}
		
		$flag=$this->Model_orders->update_free_item_status_on_invoice($order_id,$invoices_free_items_status_id,$status,$item_delivered_date,$item_delivered_time,$admin_comments);
		
		echo $flag;
	}
	function htmlallentities($str){
		  $res = '';
		  $strlen = strlen($str);
		  for($i=0; $i<$strlen; $i++){
			$byte = ord($str[$i]);
			if($byte < 128) // 1-byte char
			  $res .= $str[$i];
			elseif($byte < 192); // invalid utf8
			elseif($byte < 224) // 2-byte char
			  $res .= '&#'.((63&$byte)*64 + (63&ord($str[++$i]))).';';
			elseif($byte < 240) // 3-byte char
			  $res .= '&#'.((15&$byte)*4096 + (63&ord($str[++$i]))*64 + (63&ord($str[++$i]))).';';
			elseif($byte < 248) // 4-byte char
			  $res .= '&#'.((15&$byte)*262144 + (63&ord($str[++$i]))*4096 + (63&ord($str[++$i]))*64 + (63&ord($str[++$i]))).';';
		  }
		  return $res;
	}
	public function generate_free_item_invoice(){
		$email_stuff_arr=email_stuff();
		$invoices_free_items_status_id=$this->input->post('invoices_free_items_status_id');
		$order_id=$this->input->post('order_id');	
		$order_obj=$this->Model_orders->get_data_from_free_items_status($order_id,$invoices_free_items_status_id);
		
		//print_r($order_obj);
		
		$rand=mt_rand(1,1000000);
		$invoice_number='#BLR_'.date("ym".date("d")).$rand;
		
		$pdf = new FPDF();
		$pdf->AddPage();
			$pdf->Ln();
			$pdf->Ln();
			$logopath=base_url().$email_stuff_arr["pdflogo_emailtemplate"];
			$pdf->Image($logopath,10,6,30);
			$pdf->SetFont('Arial','B',14);
			$pdf->Cell(100);
			$pdf->Cell(90,6,'Tax Invoice / Cash Memo',0,'R');
			$pdf->Ln(30);
			/*$pdf->SetLineWidth(0.1);
			$pdf->SetDrawColor(175,175,175);
			$pdf->Cell(190,7,'','T',0,'L',0);
			$pdf->Ln(0);*/
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(30,6,'Invoice No.: ','TBL',0,'L',0);
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(40,6,$order_obj->invoice_number,'TB',0,'L',0);
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(20,6,'Order ID:','TBL',0,'L',0); 
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(40,6,$order_id,'TBR',0,'L',0);
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(20,6,'PAN/TAN:','TBL',0,'L',0); 
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(40,6,'AAWFP0549D','TBR',0,'L',0);
			$pdf->Ln(6);
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(30,6,'Invoice Date: ','BL',0,'L',0);
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(40,6,date("D j M, Y"),'B',0,'L',0); 
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(20,6,'Order Date:','BL',0,'L',0); 
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(40,6,date("D j M ,Y",strtotime($order_obj->timestamp)),'B',0,'L',0); 
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(20,6,'GSTIN:','BL',0,'L',0); 
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(40,6,'AAWFP0549D','BR',0,'L',0);
			$pdf->Ln(6);
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(50,6,'Sold By ',0,0,'L',0);
			$pdf->Cell(70,6,'Billing Address',0,0,'L',0); 
			$pdf->Cell(70,6,'Shipping Address',0,0,'L',0);
			$pdf->Ln(5);
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(50,6,$email_stuff_arr["name_emailtemplate"],0,0,'L',0);
			$pdf->Cell(70,6,$order_obj->customer_name,0,0,'L',0); 
			$pdf->Cell(70,6,$order_obj->customer_name,0,0,'L',0); 
			$pdf->Ln(5);
			$pdf->SetFont('Arial','',9);
			$line_company_address_arr=array("1"=>"Plot no 37, 2nd Floor","2"=>"Kempegowda Service Road","3"=>"Domlur 1st Stage","4"=>"Domlur","5"=>"Bangalore - 560071");
			$line_company_address_status_arr=array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"");
			$line_company_add1=array("1"=>"Plot no 37, 2nd Floor","2"=>"","3"=>"");
			//////////////////////
			$no_of_lines_billing_count=0;
			$no_of_lines_billing=1;
			$billing_add1=$order_obj->address1;
			$line_billing_add1=array("1"=>"","2"=>"","3"=>"");
			//////////////////// billing address1 starts //////
			$billing_add1_arr=explode(" ",$billing_add1);
			$billing_add1_str_len=0;
			$no_of_lines_billing_flag="no";
			foreach($billing_add1_arr as $k_billing => $billing_add1_str){
				$billing_add1_str_len+=strlen($billing_add1_str);
				$billing_add1_str_len+=1;
				if($billing_add1_str_len<=40){
					$line_billing_add1[$no_of_lines_billing].=$billing_add1_str." ";
					$no_of_lines_billing_flag="yes";
				}
			}
			if($no_of_lines_billing_flag=="yes"){
				$no_of_lines_billing_count++;
			}
			
			$billing_add1_str_len=0;
			$no_of_lines_billing=2;
			$no_of_lines_billing_flag="no";
			foreach($billing_add1_arr as $k_billing => $billing_add1_str){
				$billing_add1_str_len+=strlen($billing_add1_str);
				$billing_add1_str_len+=1;
				if($billing_add1_str_len>40 && $billing_add1_str_len<=80){
					$line_billing_add1[$no_of_lines_billing].=$billing_add1_str." ";
					$no_of_lines_billing_flag="yes";
				}
			}
			if($no_of_lines_billing_flag=="yes"){
				$no_of_lines_billing_count++;
			}
			
			$billing_add1_str_len=0;
			$no_of_lines_billing=3;
			$no_of_lines_billing_flag="no";
			foreach($billing_add1_arr as $k_billing => $billing_add1_str){
				$billing_add1_str_len+=strlen($billing_add1_str);
				$billing_add1_str_len+=1;
				if($billing_add1_str_len>80){
					$line_billing_add1[$no_of_lines_billing].=$billing_add1_str." ";
					$no_of_lines_billing_flag="yes";
				}
			}
			if($no_of_lines_billing_flag=="yes"){
				$no_of_lines_billing_count++;
			}
			//////////////////// billing address1 ends //////
			
			
			//////////////////// shipping address1 starts //////
			$no_of_lines_shipping_count=0;
			$no_of_lines_shipping=1;
			$shipping_add1=$order_obj->address1;
			$line_shipping_add1=array("1"=>"","2"=>"","3"=>"");
			$shipping_add1_arr=explode(" ",$shipping_add1);
			$shipping_add1_str_len=0;
			$no_of_lines_shipping_flag="no";
			foreach($shipping_add1_arr as $k_shipping => $shipping_add1_str){
				$shipping_add1_str_len+=strlen($shipping_add1_str);
				$shipping_add1_str_len+=1;
				if($shipping_add1_str_len<=40){
					$line_shipping_add1[$no_of_lines_shipping].=$shipping_add1_str." ";
					$no_of_lines_shipping_flag="yes";
				}
			}
			if($no_of_lines_shipping_flag=="yes"){
				$no_of_lines_shipping_count++;
			}
			
			$shipping_add1_str_len=0;
			$no_of_lines_shipping=2;
			$no_of_lines_shipping_flag="no";
			foreach($shipping_add1_arr as $k_shipping => $shipping_add1_str){
				$shipping_add1_str_len+=strlen($shipping_add1_str);
				$shipping_add1_str_len+=1;
				if($shipping_add1_str_len>40 && $shipping_add1_str_len<=80){
					$line_shipping_add1[$no_of_lines_shipping].=$shipping_add1_str." ";
					$no_of_lines_shipping_flag="yes";
				}
			}
			if($no_of_lines_shipping_flag=="yes"){
				$no_of_lines_shipping_count++;
			}
			
			
			$shipping_add1_str_len=0;
			$no_of_lines_shipping=3;
			$no_of_lines_shipping_flag="no";
			foreach($shipping_add1_arr as $k_shipping => $shipping_add1_str){
				$shipping_add1_str_len+=strlen($shipping_add1_str);
				$shipping_add1_str_len+=1;
				if($shipping_add1_str_len>80){
					$line_shipping_add1[$no_of_lines_shipping].=$shipping_add1_str." ";
					$no_of_lines_shipping_flag="yes";
				}
			}
			if($no_of_lines_shipping_flag=="yes"){
				$no_of_lines_shipping_count++;
			}
			//////////////////// shipping address1 ends //////
			$no_of_lines=max((int)$no_of_lines_billing_count,(int)$no_of_lines_shipping_count);
			/////////////////////
			for($i=1;$i<=$no_of_lines;$i++){
				$line_company_address_flag="no";
				foreach($line_company_address_arr as $kk_company_add => $v){ 
					if($line_company_address_status_arr[$kk_company_add]==""){
						$pdf->Cell(50,6,$line_company_address_arr[$kk_company_add],0,0,'L',0); 
						$line_company_address_status_arr[$kk_company_add]="yes";
						$line_company_address_flag="yes";
						break;
					}
				}
				if($line_company_address_flag=="no"){
					$pdf->Cell(50,6,"",0,0,'L',0); 
				}
				$pdf->Cell(70,6,trim($line_billing_add1[$i]),0,0,'L',0); 
				$pdf->Cell(70,6,$line_shipping_add1[$i],0,0,'L',0);
				$pdf->Ln(5);
			}
			$pdf->SetFont('Arial','',9);
			
			$line_company_add2=array("1"=>"Kempegowda Service Road","2"=>"","3"=>"");
			
			//////////////////// billing address2 starts //////
			$no_of_lines_billing_add2_count=0;
			$no_of_lines_billing_add2=1;
			$billing_add2=$order_obj->address2;
			$line_billing_add2=array("1"=>"","2"=>"","3"=>"");
			$billing_add2_arr=explode(" ",$billing_add2);
			$billing_add2_str_len=0;
			$no_of_lines_billing_add2_flag="no";
			foreach($billing_add2_arr as $k_billing => $billing_add2_str){
				$billing_add2_str_len+=strlen($billing_add2_str);
				$billing_add2_str_len+=1;
				if($billing_add2_str_len<=40){
					$line_billing_add2[$no_of_lines_billing_add2].=$billing_add2_str." ";
					$no_of_lines_billing_add2_flag="yes";
				}
			}
			if($no_of_lines_billing_add2_flag=="yes"){
				$no_of_lines_billing_add2_count++;
			}
			
			$billing_add2_str_len=0;
			$no_of_lines_billing_add2=2;
			$no_of_lines_billing_add2_flag="no";
			foreach($billing_add2_arr as $k_billing => $billing_add2_str){
				$billing_add2_str_len+=strlen($billing_add2_str);
				$billing_add2_str_len+=1;
				if($billing_add2_str_len>40 && $billing_add2_str_len<=80){
					$line_billing_add2[$no_of_lines_billing_add2].=$billing_add2_str." ";
					$no_of_lines_billing_add2_flag="yes";
				}
			}
			if($no_of_lines_billing_add2_flag=="yes"){
				$no_of_lines_billing_add2_count++;
			}
			
			$billing_add2_str_len=0;
			$no_of_lines_billing_add2=3;
			$no_of_lines_billing_add2_flag="no";
			foreach($billing_add2_arr as $k_billing => $billing_add2_str){
				$billing_add2_str_len+=strlen($billing_add2_str);
				$billing_add2_str_len+=1;
				if($billing_add2_str_len>80){
					$line_billing_add2[$no_of_lines_billing_add2].=$billing_add2_str." ";
					$no_of_lines_billing_add2_flag="yes";
				}
			}
			if($no_of_lines_billing_add2_flag=="yes"){
				$no_of_lines_billing_add2_count++;
			}
			//////////////////// billing address2 ends //////
			//////////////////// shipping address2 starts //////
			$no_of_lines_shipping_add2_count=0;
			$no_of_lines_shipping_add2=1;
			$shipping_add2=$order_obj->address2;
			$line_shipping_add2=array("1"=>"","2"=>"","3"=>"");
			$shipping_add2_arr=explode(" ",$shipping_add2);
			$shipping_add2_str_len=0;
			$no_of_lines_shipping_add2_flag="no";
			foreach($shipping_add2_arr as $k_shipping => $shipping_add2_str){
				$shipping_add2_str_len+=strlen($shipping_add2_str);
				$shipping_add2_str_len+=1;
				if($shipping_add2_str_len<=40){
					$line_shipping_add2[$no_of_lines_shipping_add2].=$shipping_add2_str." ";
					$no_of_lines_shipping_add2_flag="yes";
				}
			}
			if($no_of_lines_shipping_add2_flag=="yes"){
				$no_of_lines_shipping_add2_count++;
			}
			
			$shipping_add2_str_len=0;
			$no_of_lines_shipping_add2=2;
			$no_of_lines_shipping_add2_flag="no";
			foreach($shipping_add2_arr as $k_shipping => $shipping_add2_str){
				$shipping_add2_str_len+=strlen($shipping_add2_str);
				$shipping_add2_str_len+=1;
				if($shipping_add2_str_len>40 && $shipping_add2_str_len<=80){
					$line_shipping_add2[$no_of_lines_shipping_add2].=$shipping_add2_str." ";
					$no_of_lines_shipping_add2_flag="yes";
				}
			}
			if($no_of_lines_shipping_add2_flag=="yes"){
				$no_of_lines_shipping_add2_count++;
			}
			
			
			$shipping_add2_str_len=0;
			$no_of_lines_shipping_add2=3;
			$no_of_lines_shipping_add2_flag="no";
			foreach($shipping_add2_arr as $k_shipping => $shipping_add2_str){
				$shipping_add2_str_len+=strlen($shipping_add2_str);
				$shipping_add2_str_len+=1;
				if($shipping_add2_str_len>80){
					$line_shipping_add2[$no_of_lines_shipping_add2].=$shipping_add2_str." ";
					$no_of_lines_shipping_add2_flag="yes";
				}
			}
			if($no_of_lines_shipping_add2_flag=="yes"){
				$no_of_lines_shipping_add2_count++;
			}
			//////////////////// shipping address2 ends //////
			$no_of_lines_add2=max((int)$no_of_lines_billing_add2_count,(int)$no_of_lines_shipping_add2_count);
			/////////////////////
			for($i=1;$i<=$no_of_lines_add2;$i++){
				$line_company_address_flag="no";
				foreach($line_company_address_arr as $kk_company_add => $v){ 
					if($line_company_address_status_arr[$kk_company_add]==""){
						$pdf->Cell(50,6,$line_company_address_arr[$kk_company_add],0,0,'L',0); 
						$line_company_address_status_arr[$kk_company_add]="yes";
						$line_company_address_flag="yes";
						break;
					}
				}
				if($line_company_address_flag=="no"){
					$pdf->Cell(50,6,"",0,0,'L',0); 
				}
				$pdf->Cell(70,6,$line_billing_add2[$i],0,0,'L',0); 
				$pdf->Cell(70,6,$line_shipping_add2[$i],0,0,'L',0);
				$pdf->Ln(5);
			}
			if($order_obj->city!=""){
				$billing_city=$order_obj->city." - ";
			}else{
				$billing_city="";
			}
			if($order_obj->city!=""){
				$shipping_city=$order_obj->city." - ";
			}else{
				$shipping_city="";
			}
			if($order_obj->state!=""){
				$billing_state=$order_obj->state.", ";
			}else{
				$billing_state="";
			}
			if($order_obj->country!=""){
				$billing_country=$order_obj->country;
			}else{
				$billing_country="";
			}
			if($order_obj->state!=""){
				$shipping_state=$order_obj->state.", ";
			}else{
				$shipping_state="";
			}
			if($order_obj->country!=""){
				$shipping_country=$order_obj->country;
			}else{
				$shipping_country="";
			}
			$pdf->SetFont('Arial','',9);
			$line_company_address_flag="no";
			foreach($line_company_address_arr as $kk_company_add => $v){ 
				if($line_company_address_status_arr[$kk_company_add]==""){
					$pdf->Cell(50,6,$line_company_address_arr[$kk_company_add],0,0,'L',0); 
					$line_company_address_status_arr[$kk_company_add]="yes";
					$line_company_address_flag="yes";
					break;
				}
			}
			if($line_company_address_flag=="no"){
				$pdf->Cell(50,6,"",0,0,'L',0); 
			}
				
				
			$pdf->Cell(70,6,trim($billing_city."".$order_obj->pincode),0,0,'L',0); 
			$pdf->Cell(70,6,trim($shipping_city."".$order_obj->pincode),0,0,'L',0); 
			$pdf->Ln(5);
			$pdf->SetFont('Arial','',9);
			$line_company_address_flag="no";
				foreach($line_company_address_arr as $kk_company_add => $v){ 
					if($line_company_address_status_arr[$kk_company_add]==""){
						$pdf->Cell(50,6,$line_company_address_arr[$kk_company_add],0,0,'L',0); 
						$line_company_address_status_arr[$kk_company_add]="yes";
						$line_company_address_flag="yes";
						break;
					}
				}
				if($line_company_address_flag=="no"){
					$pdf->Cell(50,6,"",0,0,'L',0); 
				}
			
			
			$pdf->Cell(70,6,trim($billing_state."".$billing_country),0,0,'L',0); 
			$pdf->Cell(70,6,trim($shipping_state."".$shipping_country),0,0,'L',0);
			$pdf->Ln(5);
			$pdf->SetFont('Arial','',9);
			$line_company_address_flag="no";
				foreach($line_company_address_arr as $kk_company_add => $v){ 
					if($line_company_address_status_arr[$kk_company_add]==""){
						$pdf->Cell(50,6,$line_company_address_arr[$kk_company_add],0,0,'L',0); 
						$line_company_address_status_arr[$kk_company_add]="yes";
						$line_company_address_flag="yes";
						break;
					}
				}
				if($line_company_address_flag=="no"){
					$pdf->Cell(50,6,"",0,0,'L',0); 
				}
			$pdf->Cell(70,6,'Mobile Number: '.$order_obj->mobile,0,0,'L',0); 
			$pdf->Cell(70,6,'Mobile Number: '.$order_obj->mobile,0,0,'L',0);
			$pdf->Ln(8);
			
			$pdf->SetLineWidth(0.1);
			$pdf->SetDrawColor(0,0,0);
			$pdf->SetFillColor(175,175,175);
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(90,6,'Product',1,0,'L',true); 
			$pdf->Cell(20,6,'Qty',1,0,'L',true);
			$pdf->Cell(20,6,'Price',1,0,'L',true);
			$pdf->Cell(20,6,'Tax',1,0,'L',true);
			$pdf->Cell(20,6,'Tax',1,0,'L',true);
			$pdf->Cell(20,6,'Total',1,0,'L',true);
			$pdf->Ln();
		
		$price=0;
		
                $pro_name=($order_obj->sku_name!='') ? $order_obj->sku_name:$order_obj->product_name;
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(90,6,$pro_name.' - '.$order_obj->sku_id,'L',0,'L',0); 
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(20,6,$order_obj->quantity,'L',0,'L',0); 
		$pdf->Cell(20,6,$price,'L',0,'L',0);
		$pdf->Cell(20,6,"NA",'L',0,'L',0);
		$pdf->Cell(20,6,"NA",'L',0,'L',0);
		$pdf->Cell(20,6,$price,'RL',0,'L',0);
		$pdf->Ln();
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(170,6,' Total:','TBL',0,'R',0); 
		$pdf->Cell(20,6,$price,'TRBL',0,'L',0);
		$pdf->Ln();
		
		$filename="assets/pictures/invoices/Orders/Invoice_Free_".$order_obj->invoices_free_items_status_id."_".$order_id.".pdf";
		$pdf->Output($filename,'F');
		
		$flag=$this->Model_orders->update_invoice_numbers_in_invoices_free_items_status_id($invoices_free_items_status_id,$invoice_number);
		
		
		echo $flag;

	}
	
	public function return_details_of_order_item(){
		if($this->session->userdata("logged_in")){	
			$order_item_id=$this->input->post("order_item_id");			
			$data["controller"]=$this;	
			$data["order_item_id"]=$order_item_id;		
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/return_details_of_order_item',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_return_details_of_order_item($order_item_id){
		
			$queryRecordsResult=$this->Model_orders->return_details_of_order_item($order_item_id);
			$order_details='';
			foreach($queryRecordsResult as $queryRecordsObj){
				
				$order_replacement_pickup=$queryRecordsObj->order_replacement_pickup;
				$purchased_quantity=$queryRecordsObj->quantity;
				$product_price=$queryRecordsObj->product_price;
				$shipping_charge=$queryRecordsObj->shipping_charge;
				$grandTotal=$queryRecordsObj->grandtotal;
				$subtotal=$queryRecordsObj->subtotal;
				
				$replaced_orders_arr=$this->Model_orders->get_replaced_orders($queryRecordsObj->order_item_id);
				$flag=true;
				
				if(!empty($replaced_orders_arr)){
					if($replaced_orders_arr["quantity"]==$queryRecordsObj->quantity){
						$flag=false;//fully replaced.so it should go to returned orders
					}
				}
	
				$refunded_details_from_returned_orders_table_arr=$this->get_refunded_details_from_returned_orders_table($queryRecordsObj->order_item_id);
				
				$sub_refunded_details_from_returned_orders_table_arr=$this->get_sub_refunded_details_from_returned_orders_table($queryRecordsObj->order_item_id);
				
				$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->order_item_id);
		//<!---------------------------------------------------------------------------------------------------------->//				
				$check_request=$this->Model_orders->check_request_is_there_for_this_order($queryRecordsObj->order_item_id);
				$str_diff='';
				
				if($check_request=="yes"){
					
				////////////for refund (only)////// formation of string////starts//////
				
				$refunded_details_from_returned_orders_table_arr=$this->get_refunded_details_from_returned_orders_table($queryRecordsObj->order_item_id);
				
				if(!empty($refunded_details_from_returned_orders_table_arr)){
					$order_details.=$this->get_order_details_of_refund($refunded_details_from_returned_orders_table_arr);
					$str_diff="ref";
				}
				
				$sub_refunded_details_arr=$this->get_sub_refunded_details_from_returned_orders_table($queryRecordsObj->order_item_id);
				
				
				if(!empty($sub_refunded_details_arr)){
					$order_details.=$this->get_order_details_of_sub_refund($sub_refunded_details_arr);
					$str_diff="ref";
				}
				
				//replacement cancel refund_status is refunded... it comes under refund//
				
				$get_replacement_data_obj=$this->Model_orders->returns_replacement_request_orders_accounts_success($queryRecordsObj->order_item_id);
						
				if(!empty($get_replacement_data_obj)){
					
					if($get_replacement_data_obj->refund_status=="refunded"){
									
						if(!empty($order_replacement_decision_data_obj)){
							$order_details.=$this->get_order_details_of_refund_after_replace_cancel($order_replacement_decision_data_obj,$get_replacement_data_obj,$order_replacement_pickup,$purchased_quantity);
						
						}
						
					}
					$str_diff="rep";
				}
				
				////////////for refund (only)////// formation of string////ends////////
				
				////////////for replacement (only)////// formation of string////starts///
				
				if(!empty($get_replacement_data_obj)){
					
					if($get_replacement_data_obj->refund_status==""){
									
						$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($get_replacement_data_obj->order_item_id);
						if(!empty($order_replacement_decision_data_obj)){
							$order_details.=$this->get_order_details_replacement($order_replacement_decision_data_obj,$get_replacement_data_obj,$order_replacement_pickup,$purchased_quantity,$product_price);
						}
					}
					$str_diff="rep";
				}
				
				$promotion_available=$queryRecordsObj->promotion_available;
				$both_refund_and_replacement=$this->get_availability_of_order_item_id_in_refund_and_replacement($queryRecordsObj->order_item_id);
				$order_item_obj=(object) array();
				if($promotion_available==1){
					$order_item_obj=$this->Model_orders->get_details_of_order_item($queryRecordsObj->order_item_id);
				}
				
				if($both_refund_and_replacement=="yes"){
					
					$order_details.=$this->get_and_calculate_left_for_refund_and_replacement($refunded_details_from_returned_orders_table_arr,$sub_refunded_details_from_returned_orders_table_arr,$order_replacement_decision_data_obj,$product_price,$order_replacement_pickup,$shipping_charge,$grandTotal,$subtotal,$purchased_quantity,$get_replacement_data_obj,"delivered_orders",$promotion_available,$queryRecordsObj,$order_item_obj);
				
				}else{
					if($str_diff=="rep"){
						$order_details.=$this->get_and_calculate_left_for_replacement($order_replacement_decision_data_obj,$product_price,$shipping_charge,$purchased_quantity,$queryRecordsObj,$order_item_obj);
					}
					if($str_diff=="ref"){
						$order_return_decision_obj=$this->Model_orders->get_order_return_decision_data($queryRecordsObj->order_item_id,$order_item_obj);
						$shipping_charge=$order_return_decision_obj->shipping_charge;
						$order_details.=$this->get_and_calculate_left_for_refund($refunded_details_from_returned_orders_table_arr,$sub_refunded_details_from_returned_orders_table_arr,$product_price,$order_replacement_pickup,$shipping_charge,$grandTotal,$purchased_quantity,$promotion_available,$queryRecordsObj,$order_item_obj);
					}
				}
				
				////////////for replacement (only)////// formation of string////ends///
				
//<!---------------------------------------------------------------------------------------------------------->//	
				echo $order_details;
				
				}
			}				

	}
	
	public function download_logistics_manifest(){
		if($this->session->userdata("logged_in")){	
			// generation of pdf of manifest starts///
			$get_info_of_products_packed_arr=$this->Model_orders->get_info_of_products_packed();
			if(count($get_info_of_products_packed_arr)==0){
				echo "no";
				exit;
			}
			$tracking_id_generation=$this->Model_orders->get_tracking_id_generation();
			if(count($get_info_of_products_packed_arr)>=0){
				$pdf = new FPDF();
				$pdf->AddPage();

				$pdf->SetFont('Arial','B',16);
				$pdf->Ln();
				
				$pdf->Ln();
				$pdf->Cell(160,6,'Logistics Manifest',0,0,'C',0); 
				$pdf->Ln();
				$pdf->SetFont('Arial','i',5);
				$pdf->Cell(160,6,'Generated On '.date("D j M,Y h:i A"),0,0,'C',0); 
				$pdf->Ln();
				$pdf->Ln();
				$pdf->SetFont('Arial','B',10);
				$pdf->Cell(60,6,' ',0,0,'C',0); 
				$pdf->Cell(100,6,'Total products to dispatch:'.count($get_info_of_products_packed_arr),0,0,'R',0); 
				$pdf->Ln();
				
				
				
				$pdf->SetFont('Arial','B',10);
				$pdf->Cell(10,6,'SNo',1,0,'L',0); 
				$pdf->Cell(70,6,'Tracking Id',1,0,'L',0); 
				$pdf->Cell(40,6,'Order Item Id',1,0,'L',0);
				$pdf->Cell(40,6,'Order Id',1,0,'L',0);
				$pdf->Ln();
				
				foreach($get_info_of_products_packed_arr as $k => $info_of_products_packed_arr){
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(10,6,$k+1,1,0,'L',0); 
					$pdf->Cell(70,6,$info_of_products_packed_arr["tracking_number"],1,0,'L',0); 
					$pdf->Cell(40,6,$info_of_products_packed_arr["order_item_id"],1,0,'L',0);
					$pdf->Cell(40,6,$info_of_products_packed_arr["order_id"],1,0,'L',0);
					$pdf->Ln();
				}
				$filename="assets/pictures/manifest/manifest.pdf";
				if(file_exists($filename)){
					unlink($filename);
				}
				$pdf->Output($filename,'F');
			}
			
			// generation of pdf of manifest ends///
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_tracking_number_availability(){
		if($this->session->userdata("logged_in")){	
			$orders_status_id=$this->input->post("orders_status_id");
			$tracking_number=$this->Model_orders->get_tracking_number_availability($orders_status_id);
			if($tracking_number==""){
				echo "no";
			}
			else{
				echo "yes";
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function update_tracking_number(){
		if($this->session->userdata("logged_in")){	
			$orders_status_id=$this->input->post("orders_status_id");
			$tracking_numer=$this->input->post("tracking_numer");
			$flag=$this->Model_orders->update_tracking_number($orders_status_id,$tracking_numer);
			echo $flag;
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function update_cancel_table_refund_type(){
		
		$order_item_id=$this->input->post('order_item_id');
		$refund_type_comments=$this->input->post('refund_type_comments');

		$refund_method="Wallet";
		$refund_bank_id="";
		$flag=$this->Model_orders->update_cancel_table_refund_type($refund_method,$refund_bank_id,$order_item_id,$refund_type_comments);
		
		echo $flag;
	}
	public function logistics_details_of_order($obj,$type_of_order=""){
		$str='';
		$str.='<ul class="list-unstyled small">';

		//$str.='<thead><tr><th colspan="2">Logistics Details</th></thead>';

		$str.= '<li class="bold text-uppercase">'.$obj->logistics_name.'</li>';
		$str.='<li><a class="text-info underline" href="#">'.$obj->logistics_weblink.'</a></li>';
		
		$str.='<li>';
		if($obj->tracking_number==""){
			$str.='Tracking Number: <span class="text-danger">Not generated</span>';
		}else{
			$str.='Tracking Number: '.$obj->tracking_number;
		}
		$str.='</li>';
		$str.='<li>Expected delivery date: '.$obj->expected_delivery_date.'</li>';
		$str.='<li>';
		
		if($obj->exact_delivery_date==""){
			$str.='Exact delivery date: <span class="text-danger">Not updated</span> <a href="#" class="text-info bold" onclick="open_update_expected_delivery_date_modal('.$obj->order_item_id.','."'".$obj->mobile."'".')">(Update)</a>';
		}else{
			$str.='Exact delivery date: '.date("D j M Y",strtotime($obj->exact_delivery_date)).' <a href="#" class="text-info bold" onclick="open_update_expected_delivery_date_modal('.$obj->order_item_id.','."'".$obj->mobile."'".')">(Update)</a>';
		}
		
		$str.='</li>';
		$str.='<li>Delivery mode: '.$obj->delivery_mode.'</li>';
		$str.='<li>Parcel type: '.$obj->parcel_category.'</li>';
		if($type_of_order=="packed"){
			$str.='<a href="#" class="text-info bold" onclick="update_logisticsFun('.$obj->order_item_id.')">(Logistics Update)</a>';
		}
		$str.='</ul>';		
		
		return $str;
	}
	public function logistics_details_of_order_final($obj){
		$str='';
		$str.='<ul class="list-unstyled small">';

		//$str.='<thead><tr><th colspan="2">Logistics Details</th></thead>';

		$str.= '<li class="bold text-uppercase">'.$obj->logistics_name.'</li>';
		$str.='<li><a class="text-info underline" href="#">'.$obj->logistics_weblink.'</a></li>';
		
		$str.='<li>';
		if($obj->tracking_number==""){
			$str.='Tracking Number: <span class="text-danger">Not generated</span>';
		}else{
			$str.='Tracking Number: '.$obj->tracking_number;
		}
		$str.='</li>';

		$str.='<li>Delivery mode: '.$obj->delivery_mode.'</li>';
		$str.='<li>Parcel type: '.$obj->parcel_category.'</li>';
		$str.='</ul>';		
		
		return $str;
	}
	public function update_change_logistics(){
		$order_item_id=$this->input->post("order_item_id");
		$logistics_name=$this->input->post("logistics_name");
		$tracking_number=$this->input->post("tracking_number");
		if($this->input->post("use_same_tracking_number")){
			$use_same_tracking_number=1;
		}
		else{
			$use_same_tracking_number=0;
		}
		$logistics_weblink=$this->input->post("logistics_weblink");
		$delivery_mode=$this->input->post("delivery_mode");
		$flag=$this->Model_orders->update_change_logistics($order_item_id,$logistics_name,$tracking_number,$logistics_weblink,$delivery_mode,$use_same_tracking_number);	
		echo $flag;
	}
	public function update_exact_date_to_empty(){
		$order_item_id=$this->input->post('exact_order_item_id');
		$flag=$this->Model_orders->update_exact_date_to_empty($order_item_id);	
		echo $flag;
	}
	public function update_exact_date(){
		$order_item_id=$this->input->post('order_item_id');
		$buyer_mobilenumber=$this->input->post('exact_buyer_mobilenumber');
		$exact_delivery_date=$this->input->post('exact_delivery_date');
		$flag=$this->Model_orders->update_exact_date($order_item_id,$exact_delivery_date);		
		
		if($flag==true){
		
			$mobile_stuff_arr=mobile_stuff();
			$authKey = $mobile_stuff_arr["authKey_smstemplate"];
			$mobileNumber = "+91".$buyer_mobilenumber;
			$senderId = $mobile_stuff_arr["senderId_smstemplate"];
			$exact_delivery_date_format=date("D j M, Y",strtotime($exact_delivery_date));
			$history_orders_obj=$this->Model_orders->get_data_from_history_orders($order_item_id);	
			$sku_id=$history_orders_obj->sku_id;
			
			$message="Due to some circumstances your order with Item Code : ".$sku_id." is rescheduled with a new delivery date ".$exact_delivery_date_format." ";
			
			
			
			
			$route = "4";
			$postData = array(
				'authkey' => $authKey,
				'mobiles' => $mobileNumber,
				'message' => $message,
				'sender' => $senderId,
				'route' => $route
			);

			$url="https://control.msg91.com/api/sendhttp.php";
			$ch = curl_init();
			curl_setopt_array($ch, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $postData
			));
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

			$output = curl_exec($ch);

			if(curl_errno($ch)){
				echo 'error:' . curl_error($ch);
			}
			
			curl_close($ch);
			
		}	
		echo $flag;
	}
	public function completed_request_for_out_of_stock(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["menu_flag"]="notification_active_links";	
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/completed_request_for_out_of_stock_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function completed_request_for_out_of_stock_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_orders->completed_request_for_out_of_stock_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_orders->completed_request_for_out_of_stock_processing($params,"get_recs");
			$i=1;
			foreach($queryRecordsResult as $queryRecordsObj){
				
				$row[]=$i;
				$attr='<ul class="list-unstyled small">';
				$attr.='<li>SKU ID: '.$queryRecordsObj->sku_id.'</li>';
				$attr.='<li>Attributes</li>';
				$attr.='<li>'.$queryRecordsObj->attribute_1." : ".$queryRecordsObj->attribute_1_value.'</li>';
				if(!empty($queryRecordsObj->attribute_2))
				$attr.='<li>'.$queryRecordsObj->attribute_2." : ".$queryRecordsObj->attribute_2_value.'</li>';
				if(!empty($queryRecordsObj->attribute_3))
				$attr.='<li>'.$queryRecordsObj->attribute_3." : ".$queryRecordsObj->attribute_3_value.'</li>';
				if(!empty($queryRecordsObj->attribute_4))
				$attr.='<li>'.$queryRecordsObj->attribute_4." : ".$queryRecordsObj->attribute_4_value.'</li>';
				$attr.='</ul>';
				$row[]=$attr;
				
				
				$str='';

				$str.='<ul class="list-unstyled small">';
				$str.='<li>SKU: '.$queryRecordsObj->sku_name.'</li>';
				$str.='<li>Product: '.$queryRecordsObj->product_name.'</li>';
				$str.='<li>Brand: '.$queryRecordsObj->brand_name.'</li>';
				$str.='<li>Subcategory: '.$queryRecordsObj->subcat_name.'</li>';
				$str.='<li>Category: '.$queryRecordsObj->cat_name.'</li>';
				if($queryRecordsObj->pcat_name!=""){
				$str.='<li>Parent Category: '.$queryRecordsObj->pcat_name.'</li>';
				}
				$str.='</ul>';

				$row[]=$str;
				$stock='';
				$stock.='<ul class="list-unstyled small">';
				$stock.="<li>Count of Requests: ".$queryRecordsObj->request_count."</li>";
				if($queryRecordsObj->stock<$queryRecordsObj->low_stock){
					$stock.='<li>Available Stock: <span class="text-danger bold">'.$queryRecordsObj->stock.'<span></li>';
				}else{
					$stock.='<li>Available Stock: '.$queryRecordsObj->stock.'</li>';
				}
				$stock.='</ul>';
				$row[]=$stock;
				$sku_id=$queryRecordsObj->sku_id; $inventory_id=$queryRecordsObj->id;
				$email_obj=$this->Model_orders->get_list_of_emails($sku_id,$inventory_id);
				
				
				$email_str='';
				if(!empty($email_obj)){
					
					$email_str.='<div id="email_promotion_'.$queryRecordsObj->id.'" class="" >';

					$email_str.='<div class="promo_view" id="email_accordion_'.$queryRecordsObj->id.'">';
					$email_str.='<div class="">';

					$email_str.='<div class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#email_accordion_'.$queryRecordsObj->id.'" href="#email_collapseOne_'.$queryRecordsObj->id.'" style="cursor:pointer;">';

					$email_str.='<u class="bold text-success">Email Addresses</u>';


					$email_str.='</div>';
					$email_str.='<div id="email_collapseOne_'.$queryRecordsObj->id.'" class="panel-collapse collapse"> ';
					$email_str.='<div class="panel-body small">';
					$email_str_obj='';
						
						foreach($email_obj as $email){	
							$email_str_obj.=$email->email.', ';	
						}
						
					$email_str.=rtrim($email_str_obj,', ');

					$email_str.='</div>';

					$email_str.='</div>';

					$email_str.='</div>';
					$email_str.='</div>';
					
					$email_str.="</div>";
					
				
				
				
				if($queryRecordsObj->stock<$queryRecordsObj->low_stock){
					$email_str.='<span style="color:red;">Out of Stock</span>';
				}else{
					$mail_btn_show_or_hide=$this->mail_notification_btn_show_or_hide($inventory_id);
					if($mail_btn_show_or_hide=="show"){
						$email_str.='<button class="btn btn-success btn-xs" onclick="send_mail_to_notify('.$queryRecordsObj->id.','."'".$queryRecordsObj->sku_id."'".')">Send Mail to Notify</button>';
					}else{
						//$queryRecordsObj->mail_sent==1;
						$time = strtotime($queryRecordsObj->mail_sent_timestamp);
						$myFormatForView = date("d-m-Y g:i A", $time);
						
						$mail_sent='<span class="text-success">Mail Sent</span><br>';
						$mail_sent.='<small><span style=""> at <b>'.$myFormatForView.'</b></span></small><br>';
						$email_str.=$mail_sent;
					}
				}
				}else{
					$email_str.='<p class="lead">No E-mail requests</p>';
				}
				$row[]=$email_str;
				$mobile_obj=$this->Model_orders->get_list_of_mobiles($sku_id,$inventory_id);
				$mobile_str='';
				if(!empty($mobile_obj)){
					
					$mobile_str.='<div id="mobile_promotion_'.$queryRecordsObj->id.'" class="" >';

					$mobile_str.='<div class="promo_view" id="mobile_accordion_'.$queryRecordsObj->id.'">';
					$mobile_str.='<div class="">';

					$mobile_str.='<div class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#mobile_accordion_'.$queryRecordsObj->id.'" href="#mobile_collapseOne_'.$queryRecordsObj->id.'" style="cursor:pointer;">';

					$mobile_str.='<u class="bold text-success">Mobile Numbers</u>';


					$mobile_str.='</div>';
					$mobile_str.='<div id="mobile_collapseOne_'.$queryRecordsObj->id.'" class="panel-collapse collapse"> ';
					$mobile_str.='<div class="panel-body"><small>';
					$mobile_str_obj='';
						
						foreach($mobile_obj as $mobile){	
							$mobile_str_obj.=$mobile->mobile.',';	
						}
						
					$mobile_str.=trim($mobile_str_obj,',');
					$mobile_str.='</small></div>';

					$mobile_str.='</div>';

					$mobile_str.='</div>';
					$mobile_str.='</div>';
					
					$mobile_str.="</div>";
					
				
				if($queryRecordsObj->stock<$queryRecordsObj->low_stock){
					$mobile_str.='<span style="color:red;">Out of Stock</span>';
				}else{
					$mobile_btn_show_or_hide=$this->mobile_notification_btn_show_or_hide($inventory_id);
					if($mobile_btn_show_or_hide=="show"){
						$mobile_str.='<button class="btn btn-success btn-xs" onclick="send_message_to_notify('.$queryRecordsObj->id.','."'".$queryRecordsObj->sku_id."'".')">Send Mail to Notify</button>';
					}else{
						//$queryRecordsObj->mail_sent==1;
						$mobiletime = strtotime($queryRecordsObj->mobile_sent_timestamp);
						$myFormatFormobileView = date("d-m-Y g:i A", $mobiletime);
						
						$mobile_sent='<span class="text-success">Mail Sent</span><br>';
						$mobile_sent.='<small><span style=""> at <b>'.$myFormatFormobileView.'</b></span></small><br>';
						$mobile_str.=$mobile_sent;
					}
				}
				}else{
					$mobile_str.='<p class="lead">No SMS requests</p>';
				}
				$row[]=$mobile_str;
				$data[]=$row;
				$row=array();
				$i++;
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	
	public function delivered_orders_inventory_wise($inventory_id){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["filter_inventory_id"]=$inventory_id;
			$data["get_inventory_info_by_inventory_id_obj"]=$this->Model_orders->get_inventory_chain_by_inventory_id($inventory_id);
			$data["menu_flag"]="inactive_order_active_links";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/delivered_orders_inventory_wise',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function delivered_orders_inventory_wise_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();		
			$totalRecords=0;	
			$queryRecordsResult=$this->Model_orders->delivered_orders_inventory_wise_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				$order_replacement_pickup=$queryRecordsObj->order_replacement_pickup;
				$purchased_quantity=$queryRecordsObj->quantity;
				$product_price=$queryRecordsObj->product_price;
				$shipping_charge=$queryRecordsObj->shipping_charge;
				$grandTotal=$queryRecordsObj->grandtotal;
				$subtotal=$queryRecordsObj->subtotal;
				$replaced_orders_arr=$this->Model_orders->get_replaced_orders($queryRecordsObj->order_item_id);
				$flag=true;
				
				if(!empty($replaced_orders_arr)){
					if($replaced_orders_arr["quantity"]==$queryRecordsObj->quantity){
						$flag=false;//fully replaced.so it should go to returned orders
					}
				}
	
				$refunded_details_from_returned_orders_table_arr=$this->get_refunded_details_from_returned_orders_table($queryRecordsObj->order_item_id);
				
				$sub_refunded_details_from_returned_orders_table_arr=$this->get_sub_refunded_details_from_returned_orders_table($queryRecordsObj->order_item_id);
				
				$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->order_item_id);
				
				$quantity_covered=0;
				
				if(count($refunded_details_from_returned_orders_table_arr)>0){
					$quantity_covered+=$refunded_details_from_returned_orders_table_arr["quantity"];
				}
				if(count($sub_refunded_details_from_returned_orders_table_arr)>0){
					$quantity_covered+=$sub_refunded_details_from_returned_orders_table_arr["quantity"];
				}
				//|| $order_replacement_decision_data_obj->refund_status=="refunded"//removed
				if(!empty($order_replacement_decision_data_obj)){
					if($order_replacement_decision_data_obj->status=="replaced" || $order_replacement_decision_data_obj->status=="refunded" || ($order_replacement_decision_data_obj->customer_status=="cancel" && $order_replacement_pickup==1)){
						$quantity_covered+=$order_replacement_decision_data_obj->quantity;
					}
				}
				
				if($quantity_covered==$queryRecordsObj->quantity){
					$flag=false;//fully replaced.so it should go to returned orders
				}
			if($flag){
				$totalRecords++;
				$row=array();
				
				$order_summary='<ul class="list-unstyled small">';

				//if($queryRecordsObj->ord_addon_products_status=='1'){
				if(0){
					$combo_prod=$this->get_addon_data($queryRecordsObj);
					$order_summary.='<li>'.$combo_prod.'</li>';
					$row[]="Combo products";
					$order_summary.="<li>SKU:".$queryRecordsObj->sku_id."</li>";
					if($queryRecordsObj->ord_sku_name!=''){
										$order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
									}else{
										$order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
									}
				}else{

					$inventory_id=$queryRecordsObj->inventory_id;
					$image=$queryRecordsObj->image;
					$img_path=get_inventory_image_path($inventory_id,$image);

					$row[]="<img src='".base_url().$img_path."'  style='width: 100px;height: 122px;' >";

					$order_summary.="<li>SKU:".$queryRecordsObj->sku_id."</li>";
					if($queryRecordsObj->ord_sku_name!=''){
										$order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
									}else{
										$order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
									}
									$product_details=get_product_details($queryRecordsObj->product_id);
									if(!empty($product_details))
									$order_summary.=($product_details->product_description!='') ? "<li class='mt-10'><small>".$product_details->product_description.'</small></li>': '';
									
					}
				

				$order_summary.="<li>Order Date:".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."</li>";
				$order_summary.="<li>Order ID:".$queryRecordsObj->order_id."</li>";
				$order_summary.="<li>Order Item ID:".$queryRecordsObj->order_item_id."</li>";
				
				if(!empty($replaced_orders_arr)){
					$prev_order_item_id=$replaced_orders_arr['prev_order_item_id'];
					
					$order_status_data_obj=$this->Model_orders->get_orders_status_data_of_replaced_item($prev_order_item_id);
					
					$str="<li><u>Replaced Order</u>".'</li>';
					$str.=" <li>Date: ".$order_status_data_obj->order_delivered_date."</li><li> Time: ".$order_status_data_obj->order_delivered_time.'</li>';
					
					if($order_status_data_obj->type_of_order=="replaced"){
						$order_summary.="<li>[This Order includes replaced Order with order item ID ".$order_status_data_obj->order_item_id."]</li>";
					}
				}else{
					
					$str="<li> Date: ".$queryRecordsObj->order_delivered_date."</li><li> Time: ".$queryRecordsObj->order_delivered_time.'</li>';
				}
				
				/////////////////added - promotion////////////////////
				$str_promo='';
				if($queryRecordsObj->type_of_order=="replaced"){

					$prev_order_item_id=$queryRecordsObj->prev_order_item_id;
					$purchased_order_data_obj=$this->Model_orders->get_shipping_charge_and_grand_total_from_history_orders($prev_order_item_id);
					
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion_for_replaced_order($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal,$prev_order_item_id);
					
				}else{
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal);
				}
				$order_summary.="<li>".$str_promo."</li>";
		   		$order_summary.='</ul>';	
				/////////////////added - promotion////////////////////
				
				$row[]=$order_summary;
				$gift_amount_transfer='';

				if($queryRecordsObj->type_of_order==""){
					if($queryRecordsObj->promotion_surprise_gift_type==curr_code){
						$gift_amount_transfer.='<li><span class="text-info"> '.curr_sym.$queryRecordsObj->promotion_surprise_gift.' was transferred to wallet (surprise gift)</span></li>';
					}
				}
				
				$logi_details=$this->logistics_details_of_order_final($queryRecordsObj);
				
				$row[]='<ul class="list-unstyled small">'.$gift_amount_transfer.$str.'</ul>'.$logi_details;

				$order_details="<li>purchased Qty:".$queryRecordsObj->quantity."</li>";
				$order_details.="<li>Total Item Price:".$queryRecordsObj->subtotal."</li>";
				if($queryRecordsObj->promotion_invoice_free_shipping>0){
					$order_details.="<li>Shipping charge (<span style='color:red;'>waived off</span>): ".curr_sym.$queryRecordsObj->shipping_charge."</li>";
					$order_details.="<li>Total: ".curr_sym.$queryRecordsObj->subtotal."</li>";
				}else{
					$order_details.="<li>Shipping charge: ".curr_sym.$queryRecordsObj->shipping_charge."</li>";
					$order_details.="<li>Total: ".curr_sym.$queryRecordsObj->grandtotal."</li>";
				}	
				
				
//<!---------------------------------------------------------------------------------------------------------->//				
				$check_request=$this->Model_orders->check_request_is_there_for_this_order($queryRecordsObj->order_item_id);
				$str_diff='';
				if($check_request=="yes"){
					$order_item_id=$queryRecordsObj->order_item_id;
					$order_details.='<li><form target="_blank" name="return_details_'.$order_item_id.'" id="return_details_'.$order_item_id.'" method="post"><input type="hidden" value="'.$order_item_id.'" name="order_item_id"></form><a class="text-info bold" onclick="go_to_return_details('.$order_item_id.')" style="cursor:pointer;"> View Return Details </a></li>';
				
				}//check_request
//<!---------------------------------------------------------------------------------------------------------->//							
				
				
				
				if($queryRecordsObj->type_of_order==""){
					
					$order_details.='<li><a class="text-info bold" href="#" onclick="view_order_summary_(\''.$queryRecordsObj->order_id.'\')" id="view_order_summary_'.$queryRecordsObj->order_item_id.'">View Invoice Data ';
					if($queryRecordsObj->invoice_offer_achieved==1){
						$invoice_ofr_count=$this->get_invoice_offers_str($queryRecordsObj);
						$order_details.='<span class="badge" style="color: red;">'.$invoice_ofr_count.'</span>';
					}
					$order_details.='</a></li>';
					
					
					if($queryRecordsObj->promotion_invoice_cash_back>0 && $queryRecordsObj->customer_response_for_invoice_cashback=="accept" && $queryRecordsObj->status_of_refund_for_cashback=="refunded" && $queryRecordsObj->invoice_number_for_cashback!=''){
						
						$order_details.='<li><a class="text-info bold" href="#" onclick="generate_invoice_for_order_ID_offers('.$queryRecordsObj->order_id.','."'".'cashback'."'".')" id="view_order_invoice'.$queryRecordsObj->order_item_id.'">View Order Invoice';
					
						$order_details.='</a></li>';
						
					}
					
					if($queryRecordsObj->invoice_surprise_gift>0 && $queryRecordsObj->customer_response_for_surprise_gifts=="accept" && $queryRecordsObj->status_of_refund_for_surprise_gifts=="refunded" && $queryRecordsObj->invoice_number_for_surprise_gifts!=''){
						
						$order_details.='<li><a class="text-info bold" href="#" onclick="generate_invoice_for_order_ID_offers('.$queryRecordsObj->order_id.','."'".'surprise_gift'."'".')" id="view_order_invoice'.$queryRecordsObj->order_item_id.'">View Order Invoice';
					
						$order_details.='</a></li>';
						
					}
					
				}
				
				$row[]='<ul class="list-unstyled small">'.$order_details.'</ul>';
				$customer_add='<ul class="list-unstyled small">';
				$customer_add.="<li>".$queryRecordsObj->customer_name."</li>";
				$customer_add.="<li>".$queryRecordsObj->address1."</li>";
				$customer_add.="<li>".$queryRecordsObj->address2."</li>";
				$customer_add.="<li>".$queryRecordsObj->city.",".$queryRecordsObj->state."</li>";
				$customer_add.="<li>".$queryRecordsObj->country."</li>";
				$customer_add.="<li>".$queryRecordsObj->pincode."</li>";
				$customer_add.="<li>".$queryRecordsObj->mobile."</li>";
				$customer_add.='</ul>';
				$row[]=$customer_add;
				$row[]="";
				$data[]=$row;
				
				
				
				
			}else{
				
				$row=array();
				}
				
			}//foreach end
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function generate_packinglist_pdf(){
		
		if($this->session->userdata("logged_in")){
			
			
			$email_stuff_arr=email_stuff();
			$generalstuff_arr=generalstuff();
			$company_gstin=$generalstuff_arr["company_gstin"];
			$data["controller"]=$this;
			extract($this->input->post());
			//echo $order_item_id;
			
			if(isset($order)){
				if($order=="confirmed"){
					$active_orders_obj=$this->Model_orders->get_data_from_active_orders($order_item_id);	
				}
			}else{
				$order="delivered";
				$active_orders_obj=$this->Model_orders->get_data_from_history_orders($order_item_id);	
			}
			
			$type_of_order=$this->Model_orders->get_type_of_order_item($order_item_id);
			
			$invoice_data_obj=$this->Model_orders->get_data_from_invoice_offers($active_orders_obj->order_id);
			$order_item_obj=$this->Model_orders->get_details_of_order_item($active_orders_obj->order_item_id);
			$order_status_data_obj=$this->Model_orders->get_details_orders_status($active_orders_obj->order_item_id);
			$shipping_add_obj=$this->Model_orders->get_shipping_add_info_by_shipping_address_id($active_orders_obj->shipping_address_id);		
			
			$logistics_local_state_name=$this->Model_orders->get_make_as_local_state_by_logistics_id($active_orders_obj->logistics_id);
			if(trim(strtolower($shipping_add_obj->state))==trim(strtolower($logistics_local_state_name))){
				$is_local_state="yes";
			}
			else{
				$is_local_state="no";
			}
			$pdf = new FPDF('P','mm','A5');
			
			$pdf->AddPage();
			//$pdf->Ln();
			$pdf->SetFont('Arial','B',12);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(60,5,"PACKING LIST",0,0,'L',0);
			$pdf->Cell(33,5,"",0,0,'R',0);
			$pdf->Cell(37,5,'',0,0,'R',0);
			$pdf->SetTextColor(0,0,0);
			$pdf->Ln();
			
			
			//$pdf->Ln();
			$pdf->Ln();
			$logopath=base_url().$email_stuff_arr["pdflogo_emailtemplate"];
			$pdf->Image($logopath,10,15);
			
			
			$pdf->SetFont('Arial','',15);
			$pdf->Cell(130,10,'INVOICE',0,0,'R',0);
			$pdf->Ln();
			if($company_gstin!=""){
				$pdf->SetFont('Arial','',10);
				$pdf->SetTextColor(0,0,0);
				$pdf->Cell(130,5,'GST : '.$company_gstin,0,0,'L',0);
				$pdf->SetTextColor(0,0,0);
				$pdf->Ln();
			}
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,2,'',0,0,'R',0);
			$pdf->Ln();
			$pdf->Cell(60,5,'',0,0,'L',0);//Fully owned brand by
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(43,5,"Invoice Number:",0,0,'R',0);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(27,5,$active_orders_obj->invoice_number,0,0,'R',0);
			$pdf->Ln();
			// $pdf->SetFont('Arial','B',10);
			// $pdf->Cell(130,5,'MAGNUM LABEL',0,0,'L',0);
			// $pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_street1"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_street2"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_city_state"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,'India.',0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_customer_care_number"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_customer_care_email"],0,0,'L',0);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->setFillColor(0,0,0);
			$pdf->SetX(0);
			$pdf->Cell(150,0.5,'',0,0,'R',1);
			$pdf->setFillColor(255,255,255);
			$pdf->Ln();

			$pdf->SetFont('Arial','',10);
			$pdf->SetTextColor(0,0,0);


            $pdf->SetFont('Arial','B',10);
            $pdf->Cell(25,5,'Billing Name:',0,0,'L',0);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(55,5,$shipping_add_obj->customer_name,0,0,'L',0);
            $pdf->SetTextColor(0,0,0);
            $pdf->SetFont('Arial','B',10);
            $pdf->Cell(25,5,'Order Date:',0,0,'L',0);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(25,5,date("D, j M, Y",strtotime($active_orders_obj->timestamp)),0,0,'L',0);
            $pdf->Ln();

            $pdf->SetFont('Arial','B',10);
            $pdf->Cell(30,5,'Customer GSTIN:',0,0,'L',0);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(50,5,$shipping_add_obj->customer_gstin,0,0,'L',0);
            $pdf->SetFont('Arial','B',10);
            $pdf->Cell(25,5,'Invoice Date:',0,0,'L',0);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(25,5,date("D, j M, Y"),0,0,'L',0);
            $pdf->Ln();

            $pdf->SetFont('Arial','B',10);
            $pdf->Cell(30,5,'Billing Address:',0,0,'L',0);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(50,5,'',0,0,'L',0);
            $pdf->SetTextColor(0,0,0);
            $pdf->SetFont('Arial','B',10);
            $pdf->Cell(25,5,'Contact No:',0,0,'L',0);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(25,5,"+91 ".wordwrap($shipping_add_obj->mobile , 5 , ' ' , true ),0,0,'L',0);
            $pdf->Ln();

            $pdf->SetFont('Arial','',10);
            $pdf->Cell(80,4,$shipping_add_obj->address1,0,0,'L',0);
            $pdf->SetTextColor(0,0,0);
            $pdf->Ln();

            $pdf->SetFont('Arial','',10);
            $pdf->MultiCell(80,4,$shipping_add_obj->address2,0,'L',0,0);

            $pdf->SetFont('Arial','',10);
            $pdf->Cell(80,4,$shipping_add_obj->city,0,0,'L',0);
            $pdf->SetTextColor(0,0,0);
            $pdf->SetFont('Arial','B',10);
            $pdf->Cell(25,4,'',0,0,'L',0);
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(25,4,'',0,0,'L',0);
            $pdf->Ln();

            $pdf->SetFont('Arial','',10);
            $pdf->Cell(80,4,$shipping_add_obj->state.", ".$shipping_add_obj->country." ".$shipping_add_obj->pincode,0,0,'L',0);
            $pdf->SetTextColor(0,0,0);
            $pdf->Ln();
           // $pdf->Ln();

			$pdf->SetLineWidth(0.1);
			$pdf->SetDrawColor(0,0,0);
			//$pdf->SetFillColor(0,0,0);
			//$pdf->SetTextColor(255,255,255);
			$pdf->SetFont('Arial','B',9);

			$pdf->Cell(80,8,'Items','TLB',0,'L',true);
			$pdf->Cell(20,8,'HSN Code','TB',0,'L',true);
			$pdf->Cell(30,8,'Quantity','TRB',0,'C',true);

			$pdf->Ln();

			$pdf->SetFillColor(255,255,255);
			$pdf->SetTextColor(0,0,0);

			//$pdf->SetDrawColor(213,213,213);
			//$pdf->SetLineWidth(0.5);
			
			
			$total_quantity=0;
			$total_without_tax=0;
			$total_tax_percent=0;
			$total_tax_amount=0;
			$total_with_tax=0;
			
			//$pdf->SetFont('Arial','',10);
			$product_info_obj=$this->get_product_info_by_product_id($active_orders_obj->product_id);
			$subcat_info_obj=$this->get_subcat_info_by_product_id($active_orders_obj->product_id);
			//$pdf->Cell(30,6,$subcat_info_obj->subcat_name,'L',0,'L',0); 
			
			
			$no_of_lines_product_desc=1;
			$productfullinfo="";
                        
                        if($active_orders_obj->ord_sku_name!=''){
                            $pro_name=$active_orders_obj->ord_sku_name;
                        }else{
                            $pro_name=$product_info_obj->product_name;
                        }
                        
			$product_desc=$productfullinfo."".$pro_name;
						$line_product_desc=array();
			$line_product_desc[$no_of_lines_product_desc]=substr($product_desc,0,50);
			if(strlen(substr($product_desc,50,50))!=0){
				$no_of_lines_product_desc=2;
				$line_product_desc[$no_of_lines_product_desc]=substr($product_desc,50,50);
			}
			if(strlen(substr($product_desc,100,50))!=0){
				$no_of_lines_product_desc=3;
				$line_product_desc[$no_of_lines_product_desc]=substr($product_desc,100,50);
			}
			for($i=1;$i<=count($line_product_desc);$i++){
				$pdf->SetFont('Arial','',9);
				
				//////////////////////////////////////////
				$inventory_info_obj=$this->get_inventory_info_by_inventory_id($active_orders_obj->inventory_id);
			$inventory_dimentions="";
			$color_attribute_is_there="no";
			if(strtolower($inventory_info_obj->attribute_1)=="color" || strtolower($inventory_info_obj->attribute_2)=="color" || strtolower($inventory_info_obj->attribute_3)=="color" || strtolower($inventory_info_obj->attribute_4)=="color"){
				$color_attribute_is_there="yes";
				if(strtolower($inventory_info_obj->attribute_1)=="color"){
					$inventory_dimentions.=" - ".explode(":",$inventory_info_obj->attribute_1_value)[0];
				}
				else if(strtolower($inventory_info_obj->attribute_2)=="color"){
					$inventory_dimentions.=" - ".explode(":",$inventory_info_obj->attribute_2_value)[0];
				}
				else if(strtolower($inventory_info_obj->attribute_3)=="color"){
					$inventory_dimentions.=" - ".explode(":",$inventory_info_obj->attribute_3_value)[0];
				}
				else if(strtolower($inventory_info_obj->attribute_4)=="color"){
					$inventory_dimentions.=" - ".explode(":",$inventory_info_obj->attribute_4_value)[0];
				}
			}
			else{
			/////////////////////////////////	
				
				$noncolor_attribute_first_is_there="no";
				$noncolor_attribute_second_is_there="no";
				if(strtolower($inventory_info_obj->attribute_1)!="color"){
					$noncolor_attribute_first_is_there="yes";
					$inventory_dimentions.=" - ".$inventory_info_obj->attribute_1_value;
				}
				if(strtolower($inventory_info_obj->attribute_2)!="color"){
					if($noncolor_attribute_first_is_there=="yes"){
						$noncolor_attribute_second_is_there="yes";
						if($inventory_info_obj->attribute_2_value!=""){
							$inventory_dimentions.=" (".$inventory_info_obj->attribute_2_value.")";
						}
					}
					else{
						$noncolor_attribute_first_is_there="yes";
						$inventory_dimentions.=" - ".$get_free_item_data_image_path_attributes_arr["attribute_2_value"];
					}
				}
				if(strtolower($inventory_info_obj->attribute_3)!="color"){
					if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
						if($noncolor_attribute_first_is_there=="yes"){
							$noncolor_attribute_second_is_there="yes";
							if($inventory_info_obj->attribute_3_value!=""){
								$inventory_dimentions.=" (".$inventory_info_obj->attribute_3_value.")";
							}
						}
						else{
							$noncolor_attribute_first_is_there="yes";
							$inventory_dimentions.=" - ".$inventory_info_obj->attribute_3_value;
						}
					}
				}
				if(strtolower($inventory_info_obj->attribute_4)!="color"){
					if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
						if($noncolor_attribute_first_is_there=="yes"){
							$noncolor_attribute_second_is_there="yes";
							if($inventory_info_obj->attribute_4_value!=""){
								$inventory_dimentions.=" (".$inventory_info_obj->attribute_4_value.")";
							}
						}
						else{
							$noncolor_attribute_first_is_there="yes";
							$inventory_dimentions.=" - ".$inventory_info_obj->attribute_4_value;
						}
					}
				}
			
			//////////////////////////////
			
			}
			$color_attribute_is_there="no";
			$noncolor_attribute_first_is_there="no";
			$noncolor_attribute_second_is_there="no";
			
			///////////////////////////////////////////
			
			//if($active_orders_obj->ord_addon_products_status!='1'){
			if(1){

			$pdf->Cell(80,8,$line_product_desc[$i]." ".$inventory_dimentions,'TLB',0,'L',0);
			$pdf->Cell(20,8,'','TB',0,'R',0); 
				if($i==1){

			$pdf->SetFont('Arial','',9);
			$pdf->Cell(30,8,$active_orders_obj->quantity,'TRB',0,'C',0);

			$selling_price_without_tax=round(($active_orders_obj->subtotal*100/(100+$active_orders_obj->tax_percent)),2);
			$tax_percent=round($active_orders_obj->tax_percent,2);
			$tax_amount=round((($selling_price_without_tax*$tax_percent)/100),2);
			$selling_price_with_tax=round(($active_orders_obj->subtotal),2);
			
			$total_quantity+=$active_orders_obj->quantity;
			$total_without_tax+=$selling_price_without_tax;
			$total_tax_amount+=$tax_amount;
			$total_with_tax+=$selling_price_with_tax;
			
			
			}else{
					
					
				}
			$pdf->Ln();

			} //combo products condition
			
			}
			if($active_orders_obj->promotion_quote!="" && $type_of_order==""){
				if($active_orders_obj->promotion_quote!=$active_orders_obj->promotion_default_discount_promo){
					$promo_text=$active_orders_obj->promotion_quote;
					$promo_text = preg_replace('/\s\s+/', ' ', $promo_text);
					$promo_text =stripslashes($promo_text);
					$promo_text =iconv('UTF-8', 'windows-1252', $promo_text);
					$pdf->SetFont('Arial','I',9);
					if($active_orders_obj->promotion_default_discount_promo!="" && $active_orders_obj->default_discount>0){
						
						$pdf->Cell(80,8,$promo_text,'TLB',0,'L',0); 
						$pdf->Cell(20,8,'','TB',0,'R',0); 
						$pdf->Cell(30,8,'','TRB',0,'L',0);
						
						
					
					
					}else{
						
						$pdf->Cell(80,8,$promo_text,'TLB',0,'L',0);
						$pdf->Cell(20,8,'','TB',0,'R',0); 						
						$pdf->Cell(30,8,'','TRB',0,'L',0);
						
						
						
						
					}
					$pdf->Ln();
				}
			}

		

			if($active_orders_obj->promotion_default_discount_promo!="" && $active_orders_obj->default_discount>0 && $type_of_order==""){
				$promo_text=$active_orders_obj->promotion_default_discount_promo;
				$promo_text = preg_replace('/\s\s+/', ' ', $promo_text);
				$promo_text =stripslashes($promo_text);
				$promo_text =iconv('UTF-8', 'windows-1252', $promo_text);
				$pdf->SetFont('Arial','I',9);
						
						$pdf->Cell(80,8,$promo_text,'TLB',0,'L',0); 
						$pdf->Cell(20,8,'','TB',0,'R',0); 
						$pdf->Cell(30,8,'','TRB',0,'L',0);
						
						
			
			
				$pdf->Ln();
			}

			/* addon sections  */
			if($active_orders_obj->ord_addon_products_status=='1'){
				
				$arr=json_decode($active_orders_obj->ord_addon_products);
				foreach($arr as $val){
					$name=$val->inv_name;
					$image=$val->inv_image;
					$sku_id=$val->inv_sku_id;
					$price=round($val->inv_price,2);
					$pdf->SetFont('Arial','B',9);
					$pdf->Cell(15,5,"SKU ID : ",'L',0,'L',0);
					$pdf->SetFont('Arial','',9);
					$pdf->Cell(85,5,$sku_id,'',0,'L',0);
					$pdf->Cell(30,5,'1','R',0,'C',0);
					$pdf->Ln();

					$pdf->SetFont('Arial','',9);
					$pdf->Cell(130,5,$val->inv_name,'RLB',0,'L',0);
					$pdf->Ln();
				}
			}
			
		/* addon sections  */


			$pdf->SetFont('Arial','',9);
		

			
			$shipping_charge_without_tax=round(((($active_orders_obj->shipping_charge)*100)/(100+$active_orders_obj->tax_percent)),2);
			$shipping_tax_percent=round($active_orders_obj->tax_percent,2);
			$shipping_tax_amount=round((($shipping_charge_without_tax*$tax_percent)/100),2);
			$shipping_charge_with_tax=round(($active_orders_obj->shipping_charge),2);
			
			if($invoice_data_obj->promotion_invoice_free_shipping>0 && $type_of_order==""){
				$shipping_charge_without_tax=0;
				$shipping_tax_percent=0;
				$shipping_tax_amount=0;
				$shipping_charge_with_tax=0;
				
				
				$pdf->Ln();
				
				$promo_text="Shipping charge waived off";
				$pdf->SetFont('Arial','I',9);				
						
						$pdf->Cell(80,8,$promo_text,'TLB',0,'L',0); 
						$pdf->Cell(20,8,'','TB',0,'R',0); 
						$pdf->Cell(30,8,'','TRB',0,'L',0);
						
						
						
			
			
				$pdf->Ln();
				
			}else{
				
				
				
				
			
			
				$pdf->Ln();
			}
			
			$total_without_tax+=$shipping_charge_without_tax;
			$total_tax_amount+=$shipping_tax_amount;
			$total_with_tax+=$shipping_charge_with_tax;
			
			$total_without_tax=round($total_without_tax,2);
			$total_tax_amount=round($total_tax_amount,2);
			$total_with_tax=round($total_with_tax,2);
			
			///////////////////
			$total_tax_value=round(($total_without_tax*$tax_percent)/100,2);
			$total_invoice_value=round(($total_without_tax+$total_tax_value),2);
			///////////////////
				$each_invoice_amount=0;	
				
				if($invoice_data_obj->promotion_invoice_discount>0 && $type_of_order==''){
					$invoice_discount=($selling_price_with_tax/$invoice_data_obj->total_price_without_shipping_price)*100;
					
					$each_invoice_amount=$invoice_data_obj->promotion_invoice_discount*$invoice_discount/100;
					
					$total_invoice_value=round(($total_invoice_value-$each_invoice_amount),2);
					
				}
	
			
			
			
			
			
		
			
			$pdf->SetFont('Arial','B',9);
	
                       

			//////////////// second page starts//////////////////////////
			$quantity_sub_refund=0;
			$order_return_decision_quantity=0;
			$replaced_quantity=0;
				
			$active_orders_obj=$this->Model_orders->get_data_from_history_orders($order_item_id);
			
			$second_page_flag=false;
			$purchased_quantity=$active_orders_obj->quantity;
			
			$order_replacement_decision_data_obj=$this->Model_orders->get_order_replacement_decision_data($order_item_id);
			$replacement_desired_data_obj=$this->Model_orders->get_replacement_desired_data($order_item_id);
			$orders_status_data_for_second_page=$this->Model_orders->get_orders_status_data($order_item_id);
			$order_return_decision_data_obj=$this->Model_orders->get_order_return_decision_data($order_item_id);
			$sub_refunded_details_from_returned_orders_table_arr=$this->get_sub_refunded_details_from_returned_orders_table($order_item_id);

			if(!empty($replacement_desired_data_obj) && !empty($order_replacement_decision_data_obj)){
				if($replacement_desired_data_obj->paid_by=="customer" && $replacement_desired_data_obj->payment_status=="paid" && $order_replacement_decision_data_obj->pickup_identifier=="before replace" && ($replacement_desired_data_obj->order_replacement_decision_customer_status=="cancel" || $order_replacement_decision_data_obj->refund_status=="refunded")){
					$second_page_flag=false;
					$replaced_quantity=0;
				}
				if($order_replacement_decision_data_obj->pickup_identifier=="before replace" && ($order_replacement_decision_data_obj->status=="replaced" || $order_replacement_decision_data_obj->status=="refunded")){
					$second_page_flag=true;
					$replaced_quantity=$order_replacement_decision_data_obj->quantity;
				}
				if($order_replacement_decision_data_obj->pickup_identifier=="after replace" && ($order_replacement_decision_data_obj->status=="replaced" || $order_replacement_decision_data_obj->status=="refunded")){
					$second_page_flag=true;
					$replaced_quantity=$order_replacement_decision_data_obj->quantity;
				}
				if(($replacement_desired_data_obj->order_replacement_decision_customer_status=="cancel" || $order_replacement_decision_data_obj->refund_status=="refunded") && $order_replacement_decision_data_obj->pickup_identifier=="after replace" && $orders_status_data_for_second_page->order_replacement_pickup==1){
					$second_page_flag=true;
					$replaced_quantity=$order_replacement_decision_data_obj->quantity;
				}
				if(($replacement_desired_data_obj->order_replacement_decision_customer_status=="cancel" || $order_replacement_decision_data_obj->refund_status=="refunded") && $order_replacement_decision_data_obj->pickup_identifier=="after replace" && $orders_status_data_for_second_page->order_replacement_pickup==0){
					$second_page_flag=false;
					$replaced_quantity=0;
				}
				if(($replacement_desired_data_obj->order_replacement_decision_customer_status=="cancel" || $order_replacement_decision_data_obj->refund_status=="refunded") && $order_replacement_decision_data_obj->pickup_identifier=="before replace" && ($replacement_desired_data_obj->payment_status=="pending" || $replacement_desired_data_obj->payment_status=="")){
					$second_page_flag=false;
					$replaced_quantity=0;
				}
			}
			
			
			if(!empty($order_return_decision_data_obj)){
				if($order_return_decision_data_obj->status=="refunded"){
					$second_page_flag=true;
					$order_return_decision_quantity=$order_return_decision_data_obj->quantity;
				}
				else{
					$order_return_decision_quantity=0;
				}
				$quantity_sub_refund=0;
				if(!empty($sub_refunded_details_from_returned_orders_table_arr)){
				$second_page_flag=true;
				$quantity_sub_refund+=$sub_refunded_details_from_returned_orders_table_arr["quantity"];
				}
			}
			
			$availability_of_order_item_id_in_refund_and_replacement=$this->get_availability_of_order_item_id_in_refund_and_replacement($order_item_id);

				if($availability_of_order_item_id_in_refund_and_replacement=="yes"){
					$active_orders_obj->quantity=($active_orders_obj->quantity)-($replaced_quantity+($order_return_decision_quantity)+($quantity_sub_refund));
				}
				else{
					if(!empty($replacement_desired_data_obj) && !empty($order_replacement_decision_data_obj)){
					$active_orders_obj->quantity=($active_orders_obj->quantity)-($replaced_quantity);

						//replacement
						
					}
					if(!empty($order_return_decision_data_obj) || !empty($sub_refunded_details_from_returned_orders_table_arr)){
					$active_orders_obj->quantity=($active_orders_obj->quantity)-(($order_return_decision_quantity)+($quantity_sub_refund));
					}
				}
		
				 
					if($second_page_flag){
					// if(1){
					 
			
				$pdf->AddPage();
			//$pdf->Ln();
			$pdf->SetFont('Arial','B',12);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(60,5,"PACKING LIST",0,0,'L',0);
			$pdf->Cell(33,5,"",0,0,'R',0);
			$pdf->Cell(37,5,"",0,0,'R',0);
			$pdf->SetTextColor(0,0,0);
			$pdf->Ln();
			
			
			//$pdf->Ln();
			$pdf->Ln();
			$logopath=base_url().$email_stuff_arr["pdflogo_emailtemplate"];
			$pdf->Image($logopath,10,15);
			
			
			$pdf->SetFont('Arial','',15);
			$pdf->Cell(130,10,'INVOICE',0,0,'R',0);
			$pdf->Ln();
			if($company_gstin!=""){
				$pdf->SetFont('Arial','',10);
				$pdf->SetTextColor(0,0,0);
				$pdf->Cell(130,5,'GST : '.$company_gstin,0,0,'L',0);
				$pdf->SetTextColor(0,0,0);
				$pdf->Ln();
			}
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,2,'',0,0,'R',0);
			$pdf->Ln();
			$pdf->Cell(60,5,'',0,0,'L',0);//Fully owned brand by
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(43,5,"Invoice Number:",0,0,'R',0);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(27,5,$active_orders_obj->invoice_number,0,0,'R',0);
			$pdf->Ln();
			// $pdf->SetFont('Arial','B',10);
			// $pdf->Cell(130,5,'MAGNUM LABEL',0,0,'L',0);
			// $pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_street1"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_street2"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_city_state"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,'India.',0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_customer_care_number"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_customer_care_email"],0,0,'L',0);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->setFillColor(0,0,0);
			$pdf->SetX(0);
			$pdf->Cell(150,0.5,'',0,0,'R',1);
			$pdf->setFillColor(255,255,255);
			$pdf->Ln();
			$pdf->Cell(130,7,'',0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->SetTextColor(0,0,0);


                        $pdf->SetFont('Arial','B',10);
                        $pdf->Cell(25,5,'Billing Name:',0,0,'L',0);
                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(55,5,$shipping_add_obj->customer_name,0,0,'L',0);
                        $pdf->SetTextColor(0,0,0);
                        $pdf->SetFont('Arial','B',10);
                        $pdf->Cell(25,5,'Order Date:',0,0,'L',0);
                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(25,5,date("D, j M, Y",strtotime($active_orders_obj->timestamp)),0,0,'L',0);
                        $pdf->Ln();

                        $pdf->SetFont('Arial','B',10);
                        $pdf->Cell(30,5,'Customer GSTIN:',0,0,'L',0);
                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(50,5,$shipping_add_obj->customer_gstin,0,0,'L',0);
                        $pdf->SetFont('Arial','B',10);
                        $pdf->Cell(25,5,'Invoice Date:',0,0,'L',0);
                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(25,5,date("D, j M, Y"),0,0,'L',0);
                        $pdf->Ln();

                        $pdf->SetFont('Arial','B',10);
                        $pdf->Cell(30,5,'Billing Address:',0,0,'L',0);
                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(50,5,'',0,0,'L',0);
                        $pdf->SetTextColor(0,0,0);
                        $pdf->SetFont('Arial','B',10);
                        $pdf->Cell(25,5,'Contact No:',0,0,'L',0);
                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(25,5,"+91 ".wordwrap($shipping_add_obj->mobile , 5 , ' ' , true ),0,0,'L',0);
                        $pdf->Ln();

                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(80,4,$shipping_add_obj->address1,0,0,'L',0);
                        $pdf->SetTextColor(0,0,0);
                        $pdf->Ln();

                        $pdf->SetFont('Arial','',10);
                        $pdf->MultiCell(80,4,$shipping_add_obj->address2,0,'L',0,0);

                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(80,4,$shipping_add_obj->city,0,0,'L',0);
                        $pdf->SetTextColor(0,0,0);
                        $pdf->SetFont('Arial','B',10);
                        $pdf->Cell(25,4,'',0,0,'L',0);
                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(25,4,'',0,0,'L',0);
                        $pdf->Ln();

                        $pdf->SetFont('Arial','',10);
                        $pdf->Cell(80,4,$shipping_add_obj->state.", ".$shipping_add_obj->country." ".$shipping_add_obj->pincode,0,0,'L',0);
                        $pdf->SetTextColor(0,0,0);
                        $pdf->Ln();
                        $pdf->Ln();


                        $pdf->SetLineWidth(0.1);
			$pdf->SetDrawColor(0,0,0);
			//$pdf->SetFillColor(0,0,0);
			//$pdf->SetTextColor(255,255,255);
			$pdf->SetFont('Arial','B',9);
			
			
				$pdf->Cell(80,8,'Items','TLB',0,'L',true); 
				$pdf->Cell(20,8,'HSN Code','TB',0,'L',true); 
			$pdf->Cell(30,8,'Quantity','TRB',0,'C',true);
			
			$pdf->Ln();
				$pdf->SetFillColor(255,255,255);
			$pdf->SetTextColor(0,0,0);
			
			//$pdf->SetDrawColor(213,213,213);
			//$pdf->SetLineWidth(0.5);
			
				$total_quantity=0;
				$total_without_tax=0;
				$total_tax_percent=0;
				$total_tax_amount=0;
				$total_with_tax=0;

				$product_info_obj=$this->get_product_info_by_product_id($active_orders_obj->product_id);
				//$subcat_info_obj=$this->get_subcat_info_by_product_id($active_orders_obj->product_id);
				//$pdf->Cell(30,6,$subcat_info_obj->subcat_name,0,0,'L',0); 
				
				
				$no_of_lines_product_desc_sp=1;
                                if($active_orders_obj->ord_sku_name!=''){
                                    $pro_name=$active_orders_obj->ord_sku_name;
                                }else{
                                    $pro_name=$product_info_obj->product_name;
                                }
			$product_desc_sp=$productfullinfo."".$pro_name;
						$line_product_desc_sp=array();
			$line_product_desc_sp[$no_of_lines_product_desc_sp]=substr($product_desc_sp,0,50);
			if(strlen(substr($product_desc_sp,50,50))!=0){
				$no_of_lines_product_desc_sp=2;
				$line_product_desc_sp[$no_of_lines_product_desc_sp]=substr($product_desc_sp,50,50);
			}
			if(strlen(substr($product_desc_sp,100,50))!=0){
				$no_of_lines_product_desc_sp=3;
				$line_product_desc_sp[$no_of_lines_product_desc_sp]=substr($product_desc_sp,100,50);
			}
			for($i=1;$i<=count($line_product_desc_sp);$i++){
				$pdf->SetFont('Arial','B',9);
				
				
				//////////////////////////////////////////
				$inventory_info_obj=$this->get_inventory_info_by_inventory_id($active_orders_obj->inventory_id);
			$inventory_dimentions="";
			$color_attribute_is_there="no";
			if(strtolower($inventory_info_obj->attribute_1)=="color" || strtolower($inventory_info_obj->attribute_2)=="color" || strtolower($inventory_info_obj->attribute_3)=="color" || strtolower($inventory_info_obj->attribute_4)=="color"){
				$color_attribute_is_there="yes";
				if(strtolower($inventory_info_obj->attribute_1)=="color"){
					$inventory_dimentions.=" - ".explode(":",$inventory_info_obj->attribute_1_value)[0];
				}
				else if(strtolower($inventory_info_obj->attribute_2)=="color"){
					$inventory_dimentions.=" - ".explode(":",$inventory_info_obj->attribute_2_value)[0];
				}
				else if(strtolower($inventory_info_obj->attribute_3)=="color"){
					$inventory_dimentions.=" - ".explode(":",$inventory_info_obj->attribute_3_value)[0];
				}
				else if(strtolower($inventory_info_obj->attribute_4)=="color"){
					$inventory_dimentions.=" - ".explode(":",$inventory_info_obj->attribute_4_value)[0];
				}
			}
			else{
			/////////////////////////////////	
				
				$noncolor_attribute_first_is_there="no";
				$noncolor_attribute_second_is_there="no";
				if(strtolower($inventory_info_obj->attribute_1)!="color"){
					$noncolor_attribute_first_is_there="yes";
					$inventory_dimentions.=" - ".$inventory_info_obj->attribute_1_value;
				}
				if(strtolower($inventory_info_obj->attribute_2)!="color"){
					if($noncolor_attribute_first_is_there=="yes"){
						$noncolor_attribute_second_is_there="yes";
						if($inventory_info_obj->attribute_2_value!=""){
							$inventory_dimentions.=" (".$inventory_info_obj->attribute_2_value.")";
						}
					}
					else{
						$noncolor_attribute_first_is_there="yes";
						$inventory_dimentions.=" - ".$get_free_item_data_image_path_attributes_arr["attribute_2_value"];
					}
				}
				if(strtolower($inventory_info_obj->attribute_3)!="color"){
					if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
						if($noncolor_attribute_first_is_there=="yes"){
							$noncolor_attribute_second_is_there="yes";
							if($inventory_info_obj->attribute_3_value!=""){
								$inventory_dimentions.=" (".$inventory_info_obj->attribute_3_value.")";
							}
						}
						else{
							$noncolor_attribute_first_is_there="yes";
							$inventory_dimentions.=" - ".$inventory_info_obj->attribute_3_value;
						}
					}
				}
				if(strtolower($inventory_info_obj->attribute_4)!="color"){
					if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
						if($noncolor_attribute_first_is_there=="yes"){
							$noncolor_attribute_second_is_there="yes";
							if($inventory_info_obj->attribute_4_value!=""){
								$inventory_dimentions.=" (".$inventory_info_obj->attribute_4_value.")";
							}
						}
						else{
							$noncolor_attribute_first_is_there="yes";
							$inventory_dimentions.=" - ".$inventory_info_obj->attribute_4_value;
						}
					}
				}
			
			//////////////////////////////
			
			}
			$color_attribute_is_there="no";
			$noncolor_attribute_first_is_there="no";
			$noncolor_attribute_second_is_there="no";
			///////////////////////////////////////////
			
			
			
			$pdf->Cell(80,8,$line_product_desc_sp[$i]." ".$inventory_dimentions,'TLB',0,'L',0);
			$pdf->Cell(20,8,'','TB',0,'R',0);
				if($i==1){

			$pdf->SetFont('Arial','',9);
			$pdf->Cell(30,8,$active_orders_obj->quantity,'TRB',0,'C',0);
			$item_charge_replacement=0;
				$refunded_details_from_returned_orders_table_arr=$this->get_refunded_details_from_returned_orders_table($active_orders_obj->order_item_id);
				$sub_refunded_details_from_returned_orders_table_arr=$this->get_sub_refunded_details_from_returned_orders_table($active_orders_obj->order_item_id);
				
				if(!empty($refunded_details_from_returned_orders_table_arr)){
					
					$item_price_deducton_each=(($refunded_details_from_returned_orders_table_arr['item_price_without_concession']*$refunded_details_from_returned_orders_table_arr['item_deduction_percentage'])/100);
					$item_charge_replacement+=$item_price_deducton_each;
					////addded//////

				}
		
				if(!empty($sub_refunded_details_from_returned_orders_table_arr)){

					$item_price_deducton_each=$sub_refunded_details_from_returned_orders_table_arr['deducted_item_price'];
					$item_charge_replacement-=$item_price_deducton_each;
				}
				
				if($active_orders_obj->promotion_available==1){
					if($active_orders_obj->individual_price_of_product_without_promotion!='' && $active_orders_obj->individual_price_of_product_without_promotion!=0){
						$active_orders_obj->product_price=$active_orders_obj->individual_price_of_product_without_promotion;
					}else{
						$active_orders_obj->product_price=($active_orders_obj->subtotal/$purchased_quantity);
					}
					
					if(($active_orders_obj->default_discount>0 && $active_orders_obj->promotion_discount>0)){
						$active_orders_obj->product_price=($active_orders_obj->subtotal/$purchased_quantity);
					}
				}
				
				//replacement cash back////////
				$individual_cashback=0;
				if($active_orders_obj->promotion_cashback>0){
					
					$itemprice=(($active_orders_obj->quantity*$active_orders_obj->product_price)+$item_charge_replacement);
					
					
					if($order_item_obj->status_of_refund=="refunded"){
						$amount_without_cashback=($active_orders_obj->subtotal-$order_item_obj->amount_to_refund);
						$item_price_filled_cashb=0;
						
						if(!empty($order_replacement_decision_data_obj)){
							
							$replaced_value=($order_replacement_decision_data_obj->return_value_each_inventory_id*$order_replacement_decision_data_obj->quantity);
							$individual_cashback=($order_item_obj->amount_to_refund/$purchased_quantity);
	
							
						}else{
							
							if(!empty($refunded_details_from_returned_orders_table_arr)){
								$individual_cashback=($order_item_obj->amount_to_refund/$purchased_quantity);
							}

						}
						
					}else{
						
						$itemprice=(($active_orders_obj->quantity*$active_orders_obj->product_price)+$item_charge_replacement);
					}
					
				}else{
					
					$itemprice=(($active_orders_obj->quantity*$active_orders_obj->product_price)+$item_charge_replacement);
				}
				
					$selling_price_without_tax=round((($itemprice*100)/(100+$active_orders_obj->tax_percent)),2);

					$tax_percent=round($active_orders_obj->tax_percent,2);
					
					$tax_amount=round((($selling_price_without_tax*$tax_percent)/100),2);
					
					$selling_price_with_tax=round(($itemprice),2);
					
					if($availability_of_order_item_id_in_refund_and_replacement=="yes"){

							////added////
							if(!empty($refunded_details_from_returned_orders_table_arr)){
								if($refunded_details_from_returned_orders_table_arr['order_item_invoice_discount_value_each']>0){
									$order_item_invoice_discount_value=($refunded_details_from_returned_orders_table_arr['quantity']*$refunded_details_from_returned_orders_table_arr['order_item_invoice_discount_value_each']);
									
								}
							}
							////added////
					}
				$total_quantity+=$active_orders_obj->quantity;
				$total_without_tax+=$selling_price_without_tax;
				$total_tax_amount+=$tax_amount;
				$total_with_tax+=$selling_price_with_tax;
				
				
			
			}else{
					$pdf->Cell(30,8,'','TRB',0,'L',0); 
					
				}
			$pdf->Ln();
			}
				
			
				
				if($active_orders_obj->promotion_quote!="" && $type_of_order==""){
					if(($active_orders_obj->promotion_quote!=$active_orders_obj->promotion_default_discount_promo) && ($active_orders_obj->quantity>=$active_orders_obj->promotion_minimum_quantity)){
						$promo_text=$active_orders_obj->promotion_quote;
						$promo_text = preg_replace('/\s\s+/', ' ', $promo_text);
						$promo_text =stripslashes($promo_text);
						$promo_text =iconv('UTF-8', 'windows-1252', $promo_text);
						$pdf->SetFont('Arial','I',9);
						if($active_orders_obj->promotion_default_discount_promo!="" && $active_orders_obj->default_discount>0){
				
							$pdf->Cell(80,8,$promo_text,'TLB',0,'L',0); 
							$pdf->Cell(20,8,'','TB',0,'R',0); 
						$pdf->Cell(30,8,'','TRB',0,'L',0);
						
						}else{
							$pdf->Cell(80,8,$promo_text,'TLB',0,'L',0); 
							$pdf->Cell(20,8,'','TB',0,'R',0); 
						$pdf->Cell(30,8,'','TRB',0,'L',0);
						
						}
						$pdf->Ln();
					}
				}

				if($active_orders_obj->promotion_default_discount_promo!="" && $active_orders_obj->default_discount>0 && $type_of_order==""){
					$promo_text=$active_orders_obj->promotion_default_discount_promo;
					$promo_text = preg_replace('/\s\s+/', ' ', $promo_text);
					$promo_text =stripslashes($promo_text);
					$promo_text =iconv('UTF-8', 'windows-1252', $promo_text);
					$pdf->SetFont('Arial','',9);
					$pdf->Cell(80,8,$promo_text,'TLB',0,'L',0); 
					$pdf->Cell(20,8,'','TB',0,'R',0); 
						$pdf->Cell(30,8,'','TRB',0,'L',0);
						
					$pdf->Ln();
				}
			$pdf->SetFont('Arial','',9);
			
				
				$shipping_charge_replacement=0;
				
				
				if(!empty($refunded_details_from_returned_orders_table_arr)){
					$shipping_charge_replacement+=$refunded_details_from_returned_orders_table_arr['shipping_with_deductions_concession'];
				}

				if(!empty($sub_refunded_details_from_returned_orders_table_arr)){
					$shipping_charge_replacement+=$sub_refunded_details_from_returned_orders_table_arr['shipping_with_deductions_concession'];
				}
				
				if(!empty($replacement_desired_data_obj) && !empty($order_replacement_decision_data_obj)){
					
					if($replacement_desired_data_obj->shipping_charge_applied_cancels_chk==1){
						 $shipping_charge_replacement+=($active_orders_obj->shipping_charge/$purchased_quantity)*$order_replacement_decision_data_obj->quantity;
					}
					else{
						$shipping_charge_replacement+=0;
					}
				}
				
				$shipping_charge_without_tax=round(((($active_orders_obj->shipping_charge-$shipping_charge_replacement)*100)/(100+$active_orders_obj->tax_percent)),2);
				$shipping_tax_percent=round($active_orders_obj->tax_percent,2);
				$shipping_tax_amount=round((($shipping_charge_without_tax*$tax_percent)/100),2);
				$shipping_charge_with_tax=round(($active_orders_obj->shipping_charge-$shipping_charge_replacement),2);

				if($invoice_data_obj->promotion_invoice_free_shipping>0 && $type_of_order==""){
					$shipping_charge_without_tax=0;
					$shipping_tax_percent=0;
					$shipping_tax_amount=0;
					$shipping_charge_with_tax=0;
					
					
					$pdf->Ln();
					
					$promo_text="Shipping charge waived off";
					$pdf->SetFont('Arial','I',9);				
					$pdf->Cell(80,8,$promo_text,'TLB',0,'L',0);
					$pdf->Cell(20,8,'','TB',0,'R',0); 					
					$pdf->Cell(30,8,'','TRB',0,'L',0);
					
					$pdf->Ln();
				}else{
					
					$pdf->Ln();
				}

				$total_without_tax+=$shipping_charge_without_tax;
				$total_tax_amount+=$shipping_tax_amount;
				$total_with_tax+=$shipping_charge_with_tax;
				
				$total_without_tax=round($total_without_tax,2);
				$total_tax_amount=round($total_tax_amount,2);
				$total_with_tax=round($total_with_tax,2);
				

				/*
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(90,6,'Total','TBL',0,'L',0); 
				$pdf->Cell(20,6,$total_quantity,'TBL',0,'L',0);
				$pdf->Cell(40,6,$total_without_tax,'TBL',0,'L',0);
				//$pdf->Cell(20,6,'','TBL',0,'L',0);
				//$pdf->Cell(20,6,$total_tax_amount,'TBL',0,'L',0);
				$pdf->Cell(40,6,$total_without_tax,'TRBL',0,'L',0);
				$pdf->Ln();
			*/
			///////////////////
			$total_tax_value=round(($total_without_tax*$tax_percent)/100,2);
			$total_invoice_value=round(($total_without_tax+$total_tax_value),2);
			///////////////////
				$in_dis=0;		
				if($invoice_data_obj->promotion_invoice_discount>0 && $type_of_order==''){
					//only replacement
					
						if(!empty($refunded_details_from_returned_orders_table_arr) ){
							$refunded_discount=($refunded_details_from_returned_orders_table_arr['quantity']*$refunded_details_from_returned_orders_table_arr['order_item_invoice_discount_value_each']);

							if($total_quantity>0){
								$in_dis=($total_quantity*$refunded_details_from_returned_orders_table_arr['order_item_invoice_discount_value_each']);
							}
						}elseif(!empty($order_replacement_decision_data_obj->order_item_invoice_discount_value_each)){
							$refunded_discount=($order_replacement_decision_data_obj->quantity*$order_replacement_decision_data_obj->order_item_invoice_discount_value_each);

							if($total_quantity>0){
								$in_dis=($total_quantity*$order_replacement_decision_data_obj->order_item_invoice_discount_value_each);
							}
						}
					
					/*
					$pdf->SetFont('Arial','B',9);
					$pdf->Cell(170,6,"Invoice Discount ",'TL',0,'L',0); 
					$pdf->Cell(20,6,$in_dis,'TRL',0,'L',0);
					$pdf->Ln();
					*/
					//$total_with_tax=round($total_with_tax-$in_dis,2);
					$total_invoice_value=round($total_invoice_value-$in_dis,2);
				}
				
				
				if($individual_cashback>0){
					if($total_quantity>0){
						$in_dis_individual_cashback=($total_quantity*$individual_cashback);
					}
					/*
					$pdf->SetFont('Arial','B',9);
					$pdf->Cell(170,6,"Invoice Cashback ",'TL',0,'L',0); 
					$pdf->Cell(20,6,$in_dis_individual_cashback,'TRL',0,'L',0);
					$pdf->Ln();
					*/
					//$total_with_tax=round($total_with_tax-$in_dis_individual_cashback,2);
					$total_invoice_value=round($total_invoice_value-$in_dis_individual_cashback,2);
					
				}
				
				
			
			
			if($invoice_data_obj->promotion_invoice_discount>0 && $type_of_order==''){
				//only replacement
				
					if(!empty($refunded_details_from_returned_orders_table_arr) ){
						$refunded_discount=($refunded_details_from_returned_orders_table_arr['quantity']*$refunded_details_from_returned_orders_table_arr['order_item_invoice_discount_value_each']);

						if($total_quantity>0){
							$in_dis=($total_quantity*$refunded_details_from_returned_orders_table_arr['order_item_invoice_discount_value_each']);
						}
					}elseif(!empty($order_replacement_decision_data_obj->order_item_invoice_discount_value_each)){
						$refunded_discount=($order_replacement_decision_data_obj->quantity*$order_replacement_decision_data_obj->order_item_invoice_discount_value_each);

						if($total_quantity>0){
							$in_dis=($total_quantity*$order_replacement_decision_data_obj->order_item_invoice_discount_value_each);
						}
					}
				
			
			
			
			}
			
			
			
			
			
			$pdf->SetFont('Arial','B',9);
			
			
			
				
			}
			
			
			
			//third page//
			
			$order_item_cashback=0;
			$order_item_status_of_refund='';
			if($active_orders_obj->promotion_cashback>0 && $type_of_order=='' && $order=="delivered"){
				$order_item_obj=$this->Model_orders->get_details_of_order_item($active_orders_obj->order_item_id);
				$order_item_status_of_refund=$order_item_obj->status_of_refund;
			}
			if($active_orders_obj->cash_back_value>0 &&  $active_orders_obj->cash_back_value!='' && $type_of_order=='' && $order=="delivered" && $order_item_status_of_refund=="refunded"){
				//if(1){
				
				$order_item_cashback=$active_orders_obj->cash_back_value;
				
			$pdf->AddPage();
			//$pdf->Ln();
			$pdf->SetFont('Arial','B',12);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(60,5,"PACKING LIST",0,0,'L',0);
			$pdf->Cell(33,5,"",0,0,'R',0);
			$pdf->Cell(37,5,'',0,0,'R',0);
			$pdf->SetTextColor(0,0,0);
			$pdf->Ln();
			
			
			//$pdf->Ln();
			$pdf->Ln();
			$logopath=base_url().$email_stuff_arr["pdflogo_emailtemplate"];
			$pdf->Image($logopath,10,15);
			
			
			$pdf->SetFont('Arial','',15);
			$pdf->Cell(130,10,'INVOICE',0,0,'R',0);
			$pdf->Ln();
			if($company_gstin!=""){
				$pdf->SetFont('Arial','',10);
				$pdf->SetTextColor(0,0,0);
				$pdf->Cell(130,5,'GST : '.$company_gstin,0,0,'L',0);
				$pdf->SetTextColor(0,0,0);
				$pdf->Ln();
			}
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,2,'',0,0,'R',0);
			$pdf->Ln();
			$pdf->Cell(60,5,'',0,0,'L',0);//Fully owned brand by
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(43,5,"Invoice Number:",0,0,'R',0);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(27,5,$active_orders_obj->invoice_number,0,0,'R',0);
			$pdf->Ln();
			// $pdf->SetFont('Arial','B',10);
			// $pdf->Cell(130,5,'MAGNUM LABEL',0,0,'L',0);
			// $pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_street1"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_street2"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_city_state"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,'India.',0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_customer_care_number"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_customer_care_email"],0,0,'L',0);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->setFillColor(0,0,0);
			$pdf->SetX(0);
			$pdf->Cell(150,0.5,'',0,0,'R',1);
			$pdf->setFillColor(255,255,255);
			$pdf->Ln();
			$pdf->Cell(130,7,'',0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->SetTextColor(0,0,0);


                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(25,5,'Billing Name:',0,0,'L',0);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(55,5,$shipping_add_obj->customer_name,0,0,'L',0);
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(25,5,'Order Date:',0,0,'L',0);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(25,5,date("D, j M, Y",strtotime($active_orders_obj->timestamp)),0,0,'L',0);
                $pdf->Ln();

                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(30,5,'Customer GSTIN:',0,0,'L',0);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(50,5,$shipping_add_obj->customer_gstin,0,0,'L',0);
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(25,5,'Invoice Date:',0,0,'L',0);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(25,5,date("D, j M, Y"),0,0,'L',0);
                $pdf->Ln();

                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(30,5,'Billing Address:',0,0,'L',0);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(50,5,'',0,0,'L',0);
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(25,5,'Contact No:',0,0,'L',0);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(25,5,"+91 ".wordwrap($shipping_add_obj->mobile , 5 , ' ' , true ),0,0,'L',0);
                $pdf->Ln();

                $pdf->SetFont('Arial','',10);
                $pdf->Cell(80,4,$shipping_add_obj->address1,0,0,'L',0);
                $pdf->SetTextColor(0,0,0);
                $pdf->Ln();

                $pdf->SetFont('Arial','',10);
                $pdf->MultiCell(80,4,$shipping_add_obj->address2,0,'L',0,0);

                $pdf->SetFont('Arial','',10);
                $pdf->Cell(80,4,$shipping_add_obj->city,0,0,'L',0);
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(25,4,'',0,0,'L',0);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(25,4,'',0,0,'L',0);
                $pdf->Ln();

                $pdf->SetFont('Arial','',10);
                $pdf->Cell(80,4,$shipping_add_obj->state.", ".$shipping_add_obj->country." ".$shipping_add_obj->pincode,0,0,'L',0);
                $pdf->SetTextColor(0,0,0);
                $pdf->Ln();
                $pdf->Ln();

                $pdf->SetLineWidth(0.1);
			$pdf->SetDrawColor(0,0,0);
			//$pdf->SetFillColor(0,0,0);
			//$pdf->SetTextColor(255,255,255);
			$pdf->SetFont('Arial','B',9);
			
				$pdf->Cell(130,8,'Promotion',1,0,'L',true); 
				
				$pdf->Ln();
				$pdf->SetFillColor(255,255,255);
				$pdf->SetTextColor(0,0,0);
				
				//$pdf->SetDrawColor(213,213,213);
				//$pdf->SetLineWidth(0.5);
				
				$total_quantity=0;
				$total_without_tax=0;
				$total_tax_percent=0;
				$total_tax_amount=0;
				$total_with_tax=0;
				
				$promo_text='';
				if($active_orders_obj->promotion_quote!=""){
					if(($active_orders_obj->promotion_quote!=$active_orders_obj->promotion_default_discount_promo)){
						$promo_text=$active_orders_obj->promotion_quote;
						$promo_text = preg_replace('/\s\s+/', ' ', $promo_text);
						$promo_text =stripslashes($promo_text);
						$promo_text =iconv('UTF-8', 'windows-1252', $promo_text);
					}
				}
				
				
				$pdf->SetFont('Arial','B',10);
				$pdf->Cell(130,8,$promo_text,1,0,'L',0); 
				$pdf->SetFont('Arial','',10);			
				
				$pdf->Ln();
				
				
				
			
			$pdf->SetFont('Arial','B',9);
		
				
				

					//////////////////////////////////////////////////////
					
					
				}//sku cash back
				
				//surprise gift code
				
				if($active_orders_obj->promotion_surprise_gift_type!='' && ($active_orders_obj->promotion_surprise_gift_type==curr_code)  && $type_of_order=='' && $order=="delivered" && $order_item_obj->status_of_refund=="refunded"){
				//if(1){
					
					$promo_text='';
					if($active_orders_obj->promotion_quote!=""){
						if(($active_orders_obj->promotion_quote!=$active_orders_obj->promotion_default_discount_promo)){
							$promo_text=$active_orders_obj->promotion_quote;
							$promo_text = preg_replace('/\s\s+/', ' ', $promo_text);
							$promo_text =stripslashes($promo_text);
							$promo_text =iconv('UTF-8', 'windows-1252', $promo_text);
						}
					}
					
					$pdf->AddPage();
			//$pdf->Ln();
			$pdf->SetFont('Arial','B',12);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(60,5,"PACKING LIST",0,0,'L',0);
			$pdf->Cell(33,5,"",0,0,'R',0);
			$pdf->Cell(37,5,'',0,0,'R',0);
			$pdf->SetTextColor(0,0,0);
			$pdf->Ln();
			
			
			//$pdf->Ln();
			$pdf->Ln();
			$logopath=base_url().$email_stuff_arr["pdflogo_emailtemplate"];
			$pdf->Image($logopath,10,15);
			
			
			$pdf->SetFont('Arial','',15);
			$pdf->Cell(130,10,'INVOICE',0,0,'R',0);
			$pdf->Ln();
			if($company_gstin!=""){
				$pdf->SetFont('Arial','',10);
				$pdf->SetTextColor(0,0,0);
				$pdf->Cell(130,5,'GST : '.$company_gstin,0,0,'L',0);
				$pdf->SetTextColor(0,0,0);
				$pdf->Ln();
			}
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,2,'',0,0,'R',0);
			$pdf->Ln();
			$pdf->Cell(60,5,'',0,0,'L',0);//Fully owned brand by
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(43,5,"Invoice Number:",0,0,'R',0);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(27,5,$active_orders_obj->invoice_number,0,0,'R',0);
			$pdf->Ln();
			// $pdf->SetFont('Arial','B',10);
			// $pdf->Cell(130,5,'MAGNUM LABEL',0,0,'L',0);
			// $pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_street1"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_street2"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_address_city_state"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(130,4,'India.',0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_customer_care_number"],0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(130,4,$email_stuff_arr["company_customer_care_email"],0,0,'L',0);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->setFillColor(0,0,0);
			$pdf->SetX(0);
			$pdf->Cell(150,0.5,'',0,0,'R',1);
			$pdf->setFillColor(255,255,255);
			$pdf->Ln();
			$pdf->Cell(130,7,'',0,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',10);
			$pdf->SetTextColor(0,0,0);


                    $pdf->SetFont('Arial','B',10);
                    $pdf->Cell(25,5,'Billing Name:',0,0,'L',0);
                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(55,5,$shipping_add_obj->customer_name,0,0,'L',0);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Arial','B',10);
                    $pdf->Cell(25,5,'Order Date:',0,0,'L',0);
                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(25,5,date("D, j M, Y",strtotime($active_orders_obj->timestamp)),0,0,'L',0);
                    $pdf->Ln();

                    $pdf->SetFont('Arial','B',10);
                    $pdf->Cell(30,5,'Customer GSTIN:',0,0,'L',0);
                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(50,5,$shipping_add_obj->customer_gstin,0,0,'L',0);
                    $pdf->SetFont('Arial','B',10);
                    $pdf->Cell(25,5,'Invoice Date:',0,0,'L',0);
                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(25,5,date("D, j M, Y"),0,0,'L',0);
                    $pdf->Ln();

                    $pdf->SetFont('Arial','B',10);
                    $pdf->Cell(30,5,'Billing Address:',0,0,'L',0);
                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(50,5,'',0,0,'L',0);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Arial','B',10);
                    $pdf->Cell(25,5,'Contact No:',0,0,'L',0);
                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(25,5,"+91 ".wordwrap($shipping_add_obj->mobile , 5 , ' ' , true ),0,0,'L',0);
                    $pdf->Ln();

                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(80,4,$shipping_add_obj->address1,0,0,'L',0);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->Ln();

                    $pdf->SetFont('Arial','',10);
                    $pdf->MultiCell(80,4,$shipping_add_obj->address2,0,'L',0,0);

                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(80,4,$shipping_add_obj->city,0,0,'L',0);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Arial','B',10);
                    $pdf->Cell(25,4,'',0,0,'L',0);
                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(25,4,'',0,0,'L',0);
                    $pdf->Ln();

                    $pdf->SetFont('Arial','',10);
                    $pdf->Cell(80,4,$shipping_add_obj->state.", ".$shipping_add_obj->country." ".$shipping_add_obj->pincode,0,0,'L',0);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->Ln();
                    $pdf->Ln();


                    $pdf->SetLineWidth(0.1);
			$pdf->SetDrawColor(0,0,0);
			//$pdf->SetFillColor(0,0,0);
			//$pdf->SetTextColor(255,255,255);
			$pdf->SetFont('Arial','B',9);
			
				$pdf->Cell(130,8,'Promotion',1,0,'L',true); 
				
				$pdf->Ln();
				$pdf->SetFillColor(255,255,255);
			$pdf->SetTextColor(0,0,0);
			
			//$pdf->SetDrawColor(213,213,213);
			//$pdf->SetLineWidth(0.5);
			
				$price=0;
					
					
					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(130,8,$promo_text,1,0,'L',0); 
					$pdf->SetFont('Arial','',10);
				
					$pdf->Ln();
					
				
				$pdf->SetFont('Arial','B',9);
		
				
			
				}//surprise gift code

				$filename="assets/pictures/packinglist/packinglist_".$order_id.$order_item_id.".pdf";
				$pdf->Output($filename,'F');
				
			/////////////// second page starts ///////////////////////////
			if(isset($sendInvoiceLabelsPackinglist_email)){
				if($sendInvoiceLabelsPackinglist_email=="yes"){
					$email_stuff_arr=email_stuff();
					$current_base_url=base_url();
					if((preg_match(SITE_TEXT,$current_base_url))){
						$toemail_emailtemplate_live_1=$email_stuff_arr["toemail_emailtemplate_live_1"];
						$toemail_emailtemplate_live_2=$email_stuff_arr["toemail_emailtemplate_live_2"];
					}
					else{
						$toemail_emailtemplate_live_1=$email_stuff_arr["toemail_emailtemplate_dev_1"];
						$toemail_emailtemplate_live_2=$email_stuff_arr["toemail_emailtemplate_dev_2"];
					}
					$mail = new PHPMailer;
					/*$mail->isSMTP();
					$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
					$mail->Port = '587';//  live - comment it 
					$mail->Auth = true;//  live - comment it 
					$mail->SMTPAuth = true;
					$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
					$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
					$mail->SMTPSecure = 'tls';*/
					$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
					$mail->FromName = $email_stuff_arr["name_emailtemplate"];
					$mail->addAddress($toemail_emailtemplate_live_1);
					$mail->addAddress($toemail_emailtemplate_live_2);
					//$mail->AddCC("rajitkumar1974@gmail.com", "Rajit Kumar");               
					//$mail->AddBCC("rajit@axonlabs.in", "Rajit Kumar");
					$mail->addReplyTo($email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
					$mail->WordWrap = 50;
					$mail->isHTML(true);
					//$filepath1="assets/pictures/invoices/Invoice_".$order_id.$order_item_id."_transit.pdf";
					$filepath2="assets/pictures/invoices/Invoice_".$order_id.$order_item_id."_customer.pdf";
					$filepath3="assets/pictures/labels/label_".$order_id.$order_item_id.".pdf";
					$filepath4="assets/pictures/packinglist/packinglist_".$order_id.$order_item_id.".pdf";
				
					$mail->Subject = 'Documents - Order ID: '.$order_id;
					$mail->Body    = "<span style='color:#ff3300'>Attached are the Copies of Customer Invoice, Labels and Packing List for Order ID: ".$order_id."</span><br><br>- Flamingo Admin Team";
					//$mail->AddAttachment($filepath1);
					$mail->AddAttachment($filepath2);
					$mail->AddAttachment($filepath3);
					$mail->AddAttachment($filepath4);
					if($toemail_emailtemplate_live_1 != '')
					{		
						if(!$mail->send()){ echo "Not send ".$mail->ErrorInfo." ".$toemail_emailtemplate_live_1;}
						else{ echo true;}
					}
				}
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	
	}
	/// shipping charges starts 
	public function shipped_charges(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["menu_flag"]="inactive_order_active_links";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/shipped_charges',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	
	public function shipping_charges_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();		
			$totalRecords=0;	
			$total_shipping_charges=0;
			$queryRecordsResult=$this->Model_orders->shipping_charges_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				$order_replacement_pickup=$queryRecordsObj->order_replacement_pickup;
				$purchased_quantity=$queryRecordsObj->quantity;
				$product_price=$queryRecordsObj->product_price;
				$shipping_charge=$queryRecordsObj->shipping_charge;
				$grandTotal=$queryRecordsObj->grandtotal;
				$subtotal=$queryRecordsObj->subtotal;
				$replaced_orders_arr=$this->Model_orders->get_replaced_orders($queryRecordsObj->order_item_id);
				$flag=true;
				
				if(!empty($replaced_orders_arr)){
					if($replaced_orders_arr["quantity"]==$queryRecordsObj->quantity){
						$flag=false;//fully replaced.so it should go to returned orders
					}
				}
	
				$refunded_details_from_returned_orders_table_arr=$this->get_refunded_details_from_returned_orders_table($queryRecordsObj->order_item_id);
				
				$sub_refunded_details_from_returned_orders_table_arr=$this->get_sub_refunded_details_from_returned_orders_table($queryRecordsObj->order_item_id);
				
				$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->order_item_id);
				
				$quantity_covered=0;
				
				if(count($refunded_details_from_returned_orders_table_arr)>0){
					$quantity_covered+=$refunded_details_from_returned_orders_table_arr["quantity"];
				}
				if(count($sub_refunded_details_from_returned_orders_table_arr)>0){
					$quantity_covered+=$sub_refunded_details_from_returned_orders_table_arr["quantity"];
				}
				//|| $order_replacement_decision_data_obj->refund_status=="refunded"//removed
				if(!empty($order_replacement_decision_data_obj)){
					if($order_replacement_decision_data_obj->status=="replaced" || $order_replacement_decision_data_obj->status=="refunded" || ($order_replacement_decision_data_obj->customer_status=="cancel" && $order_replacement_pickup==1)){
						$quantity_covered+=$order_replacement_decision_data_obj->quantity;
					}
				}
				
				if($quantity_covered==$queryRecordsObj->quantity){
					$flag=false;//fully replaced.so it should go to returned orders
				}
			if($flag){
				$totalRecords++;
				$row=array();
				
				$order_summary='<ul class="list-unstyled small">';

				//if($queryRecordsObj->ord_addon_products_status=='1'){
				if(0){
					$combo_prod=$this->get_addon_data($queryRecordsObj);
					$order_summary.='<li>'.$combo_prod.'</li>';
					$row[]="Combo products";
					$order_summary.="<li>SKU:".$queryRecordsObj->sku_id."</li>";
					$order_summary.="<li class='text-info'>".$queryRecordsObj->sku_name."</li>";
					$order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
				}else{
					$inventory_id=$queryRecordsObj->inventory_id;
					$image=$queryRecordsObj->image;
					$img_path=get_inventory_image_path($inventory_id,$image);

					$row[]="<img src='".base_url().$img_path."'  style='width: 100px;height: 122px;' >";

					$order_summary.="<li>SKU:".$queryRecordsObj->sku_id."</li>";
					$order_summary.="<li class='text-info'>".$queryRecordsObj->sku_name."</li>";
					$order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";

				}
				$order_summary.="<li>Order Date:".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."</li>";
				$order_summary.="<li>Order ID:".$queryRecordsObj->order_id."</li>";
				$order_summary.="<li>Order Item ID:".$queryRecordsObj->order_item_id."</li>";
				
				if(!empty($replaced_orders_arr)){
					$prev_order_item_id=$replaced_orders_arr['prev_order_item_id'];
					
					$order_status_data_obj=$this->Model_orders->get_orders_status_data_of_replaced_item($prev_order_item_id);
					
					$str="<li><u>Replaced Order</u>".'</li>';
					$str.=" <li>Date: ".$order_status_data_obj->order_delivered_date."</li><li> Time: ".$order_status_data_obj->order_delivered_time.'</li>';
					
					if($order_status_data_obj->type_of_order=="replaced"){
						$order_summary.="<li>[This Order includes replaced Order with order item ID ".$order_status_data_obj->order_item_id."]</li>";
					}
				}else{
					
					$str="<li> Date: ".$queryRecordsObj->order_delivered_date."</li><li> Time: ".$queryRecordsObj->order_delivered_time.'</li>';
				}
				
				/////////////////added - promotion////////////////////
				$str_promo='';
				if($queryRecordsObj->type_of_order=="replaced"){

					$prev_order_item_id=$queryRecordsObj->prev_order_item_id;
					$purchased_order_data_obj=$this->Model_orders->get_shipping_charge_and_grand_total_from_history_orders($prev_order_item_id);
					
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion_for_replaced_order($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal,$prev_order_item_id);
					
				}else{
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal);
				}
				$order_summary.="<li>".$str_promo."</li>";
		   		$order_summary.='</ul>';	
				/////////////////added - promotion////////////////////
				
				$row[]=$order_summary;
				$gift_amount_transfer='';

				if($queryRecordsObj->type_of_order==""){
					if($queryRecordsObj->promotion_surprise_gift_type==curr_code){
						$gift_amount_transfer.='<li><span class="text-info"> '.curr_sym.$queryRecordsObj->promotion_surprise_gift.' was transferred to wallet (surprise gift)</span></li>';
					}
				}
				
				$logi_details=$this->logistics_details_of_order_final($queryRecordsObj);
				
				$row[]='<ul class="list-unstyled small">'.$gift_amount_transfer.$str.'</ul>'.$logi_details;

				$order_details="<li>purchased Qty:".$queryRecordsObj->quantity."</li>";
				$order_details.="<li>Total Item Price:".$queryRecordsObj->subtotal."</li>";
				if($queryRecordsObj->promotion_invoice_free_shipping>0){
					$order_details.="<li>Shipping charge (<span style='color:red;'>waived off</span>): ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
					$order_details.="<li>Total: ".curr_sym." ".$queryRecordsObj->subtotal."</li>";
				}else{
					$order_details.="<li>Shipping charge: ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
					$order_details.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
				}	
				
				$total_shipping_charges+=$queryRecordsObj->shipping_charge;
//<!---------------------------------------------------------------------------------------------------------->//				
				$check_request=$this->Model_orders->check_request_is_there_for_this_order($queryRecordsObj->order_item_id);
				$str_diff='';
				if($check_request=="yes"){
					$order_item_id=$queryRecordsObj->order_item_id;
					$order_details.='<li><form target="_blank" name="return_details_'.$order_item_id.'" id="return_details_'.$order_item_id.'" method="post"><input type="hidden" value="'.$order_item_id.'" name="order_item_id"></form><a class="text-info bold" onclick="go_to_return_details('.$order_item_id.')" style="cursor:pointer;"> View Return Details </a></li>';
				
				}//check_request
//<!---------------------------------------------------------------------------------------------------------->//							
				
				
				
				if($queryRecordsObj->type_of_order==""){
					
					$order_details.='<li><a class="text-info bold" href="#" onclick="view_order_summary_(\''.$queryRecordsObj->order_id.'\')" id="view_order_summary_'.$queryRecordsObj->order_item_id.'">View Invoice Data ';
					if($queryRecordsObj->invoice_offer_achieved==1){
						$invoice_ofr_count=$this->get_invoice_offers_str($queryRecordsObj);
						$order_details.='<span class="badge" style="color: red;">'.$invoice_ofr_count.'</span>';
					}
					$order_details.='</a></li>';
					
					
					if($queryRecordsObj->promotion_invoice_cash_back>0 && $queryRecordsObj->customer_response_for_invoice_cashback=="accept" && $queryRecordsObj->status_of_refund_for_cashback=="refunded" && $queryRecordsObj->invoice_number_for_cashback!=''){
						
						$order_details.='<li><a class="text-info bold" href="#" onclick="generate_invoice_for_order_ID_offers('.$queryRecordsObj->order_id.','."'".'cashback'."'".')" id="view_order_invoice'.$queryRecordsObj->order_item_id.'">View Order Invoice';
					
						$order_details.='</a></li>';
						
					}
					
					if($queryRecordsObj->invoice_surprise_gift>0 && $queryRecordsObj->customer_response_for_surprise_gifts=="accept" && $queryRecordsObj->status_of_refund_for_surprise_gifts=="refunded" && $queryRecordsObj->invoice_number_for_surprise_gifts!=''){
						
						$order_details.='<li><a class="text-info bold" href="#" onclick="generate_invoice_for_order_ID_offers('.$queryRecordsObj->order_id.','."'".'surprise_gift'."'".')" id="view_order_invoice'.$queryRecordsObj->order_item_id.'">View Order Invoice';
					
						$order_details.='</a></li>';
						
					}
					
				}
				
				$row[]='<ul class="list-unstyled small">'.$order_details.'</ul>';
				$customer_add='<ul class="list-unstyled small">';
				$customer_add.="<li>".$queryRecordsObj->customer_name."</li>";
				$customer_add.="<li>".$queryRecordsObj->address1."</li>";
				$customer_add.="<li>".$queryRecordsObj->address2."</li>";
				$customer_add.="<li>".$queryRecordsObj->city.",".$queryRecordsObj->state."</li>";
				$customer_add.="<li>".$queryRecordsObj->country."</li>";
				$customer_add.="<li>".$queryRecordsObj->pincode."</li>";
				$customer_add.="<li>".$queryRecordsObj->mobile."</li>";
				$customer_add.='</ul>';
				$row[]=$customer_add;
				$row[]="<button =\"javascript:void(0);\" onclick=\"sendInvoiceFun('$queryRecordsObj->order_item_id','$queryRecordsObj->order_id')\" class='btn btn-xs btn-block btn-primary invoice_sent_$queryRecordsObj->order_item_id'>Send Invoice</button>";
				$data[]=$row;
				
				
				
				
			}else{
				
				$row=array();
				}
				
			}//foreach end
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"total_shipping_charges" => $total_shipping_charges,
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	/// shipping charges ends 
	
	public function get_inventory_details($inventory_id){
		return $this->Model_orders->get_inventory_details($inventory_id);
	}
	public function get_free_skus_from_inventory_with_details($promo_item,$promotion_item_num){
		
		$promo_item=rtrim($promo_item,',');
		$promotion_item_num=rtrim($promotion_item_num,',');
		
		$promo_item_arr = explode(',', $promo_item);
		$promo_item_num_arr = explode(',', $promotion_item_num);
		
		$two=array_combine($promo_item_arr,$promo_item_num_arr);
		
		$prmo_item_res=$this->Model_orders->get_free_skus_from_inventory($promo_item);
		$str_entire=array();

		return $prmo_item_res;
		
	}
	public function get_order_summary_mail_data_from_history_table(){
	
		if($this->session->userdata("logged_in")){
			$order_id=$this->input->post("order_id");
			$order_item_id_for_popover=$this->input->post("order_item_id");
			$order_summary_mail_data_arr=$this->Model_orders->get_order_summary_mail_data_from_history_table($order_id);
			$invoice_offers=$this->Model_orders->get_all_order_invoice_offers($order_id);
			
			$you_saved_fin=0;
			if(!empty($invoice_offers)){
										
			   foreach($invoice_offers as $data){
					$promotion_invoice_cash_back=$data["promotion_invoice_cash_back"];
					$promotion_invoice_free_shipping=$data["promotion_invoice_free_shipping"];
					$promotion_invoice_discount=$data["promotion_invoice_discount"];
					$total_amount_cash_back=$data["total_amount_cash_back"];
					$total_amount_saved=$data["total_amount_saved"];
					$total_amount_to_pay=$data["total_amount_to_pay"];
					$payment_method=$data["payment_method"];
					$invoice_offer_achieved=$data["invoice_offer_achieved"];
					
					if($promotion_invoice_free_shipping>0 && $invoice_offer_achieved==1){
						$shipping_charge_waived="yes";
					}else{
						$shipping_charge_waived="no";
					}
			   }

			}

			?>
			<script>
			function closepopoverFun(){
				$("#orderid_<?php echo $order_id;?>_<?php echo $order_item_id_for_popover; ?>").popover("hide");
			}
			</script>
			<div class="row margin-top margin-bottom">
						<div class="col-md-12">
            		<div class="panel panel-default">
							
					<div class="panel-heading" role="tab">		
						<div class="row panel-title normal-text">
							<div class="col-md-12 text-center">
								<div class="f-row text-capitalize">
									<b>Order Details</b>
									<span onclick="closepopoverFun()" class="pull-right" style="cursor:pointer;font-weight:bold">&times;</span>
								</div>			
							</div>

							
							
							
						</div>

					</div>
											
					<div class="panel-body">
						<ul class="best-sell list-unstyled">

							<?php
								foreach($order_summary_mail_data_arr as $order_summary_mail_data){
									
							?>
							<li>
								<div class="row" style="margin-bottom:1rem;border-bottom:1px solid #e1e1e1">
								<div class="col-md-7 products-block">
										<div class="row">
												
											<div class="col-md-5 products-block">
												<div class="products-block-left">
													
														<img src="<?php echo base_url().$order_summary_mail_data['image'];?>" alt="SPECIAL PRODUCTS">
													
												</div>		
											</div>
											<div class="col-md-7 products-block">
												<div class="f-row product-name ordrk-color">
													<?php echo $order_summary_mail_data['product_name'];?>
												</div>
												<div class="f-row small-text">
													<?php
												$inv_obj=$this->get_inventory_details($order_summary_mail_data['inventory_id']);
												
												//$inv_obj->attribute_1_value= substr($inv_obj->attribute_1_value, 0, strpos($inv_obj->attribute_1_value, ":"));
												if(!empty($inv_obj->attribute_1))
													echo $inv_obj->attribute_1." : ".$inv_obj->attribute_1_value.'<br>';
												if(!empty($inv_obj->attribute_2))
													echo $inv_obj->attribute_2." : ".$inv_obj->attribute_2_value.'<br>';
												if(!empty($inv_obj->attribute_3))
													echo $inv_obj->attribute_3." : ".$inv_obj->attribute_3_value.'<br>';
												if(!empty($inv_obj->attribute_4))
													echo $inv_obj->attribute_4." : ".$inv_obj->attribute_4_value.'<br>';
												?>
													
													
												</div>
													
												<div class="f-row small-text">SKU :<b> <?php echo $order_summary_mail_data['sku_id']; ?></b></div>
												<div class="f-row small-text">Qty:<b> <?php echo $order_summary_mail_data['quantity']; ?></b></div>
												<div class="f-row small-text">Order Item Id:<b> <?php echo $order_summary_mail_data['order_item_id']; ?></b></div>
												
			<?php									
//if($order_summary_mail_data["ord_addon_single_or_multiple_tagged_inventories_in_frontend"]=="single"){
if(0){
			$addon_products_arr=json_decode($order_summary_mail_data["ord_addon_products"],true);
			$addon_inv_mrp_price=$addon_products_arr[0]["inv_mrp_price"];
			$addon_inv_price=$addon_products_arr[0]["inv_price"];
			$addon_inv_sku_id=$addon_products_arr[0]["inv_sku_id"];
			?>
			<div class="f-row small-text">
			<br>
			Including Addon SKU : <?php echo $addon_inv_sku_id; ?> <br><b> <del><?php echo curr_sym.$addon_inv_mrp_price; ?></del> <br><?php echo curr_sym.$addon_inv_price; ?></b></div>
			<?php
		}
?>

		
												
												
												
												
												
												<!--<div class="f-row small-text">Seller:<b> Agent or Flamingo</b></div>-->
											</div>
									
										</div>
								</div>								
									
<?php
	//print_r($order_summary_mail_data);
	
	$order_item=$order_summary_mail_data;			
	$order_item_id=$order_item["order_item_id"];
	$promotion_item=$order_item["promotion_item"];
	$promotion_item_num=$order_item["promotion_item_num"];
	
	//no surprise gift--(surprise gift will come when order item is delivered)
	
	if($promotion_item!=''){						
		$quantity=$order_item['quantity'];
		$promotion_minimum_quantity=$order_item['promotion_minimum_quantity'];
		
		$promo_item=rtrim($promotion_item,',');
		$promotion_item_num=rtrim($promotion_item_num,',');
		
		$promo_item_arr = explode(',', $promo_item);
		$promo_item_num_arr = explode(',', $promotion_item_num);
		
		$two=array_combine($promo_item_arr,$promo_item_num_arr);
		
		$temp_quotient=intval($quantity/$promotion_minimum_quantity);
		if($promotion_item!=''){
			$free_items_arr=$this->get_free_skus_from_inventory_with_details($promotion_item,$promotion_item_num);
		}			
	}
?>
<!-----------free items------->
<div class="col-md-5 text-right">
										<input type="hidden" id="price_paid" price_paid="<?php echo $order_item['product_price'];?>">
 


										<p class="product-price"><b><?php echo curr_sym;?>
										
										<?php if($promotion_invoice_free_shipping>0){
											echo $order_item['subtotal'];
										}else{
											echo $order_item['grandtotal'];
										}
										?>
										</b>
                                                   
									<?php
/*
?>
									<a data-trigger="click" data-placement="bottom" data-toggle="popover" data-html="true" id="price_details_1_<?php echo $order_item_id;?>" content_id="#content_price_details_1_<?php echo $order_item_id; ?>" class="price_details_thankyou cursor-pointer" onclick="toggle_content(this)">[<i class="fa fa-question" aria-hidden="true" ></i>]</a> 
			   
									<div id="content_price_details_1_<?php echo $order_item_id; ?>" style="display:none;">
                                                <table class="table table-bordered" style="font-size:0.8em;text-align:justify;margin-bottom: 0px;">
                                                    <thead><tr><th style="padding:0px"></th><th width="25%" style="padding:0px"></th><th style="padding:0px" width="25%" style="text-align: right"></th></tr></thead>
                                            
<?php
											
													$total_product_price_normal=($order_item["quantity"]*$order_item["product_price"]);
													$shipping_price_normal=$order_item["shipping_charge"];
													

													$total_amount_normal=($shipping_price_normal+$total_product_price_normal);
													
	
	?>
													
													<tr><td>Base product price</td><td><?php echo $order_item['quantity']; ?> x <?php echo $order_item['product_price']?></td><td class="text-right">&#8377; <?php echo $total_product_price_normal; ?></td></tr>
													
													<tr><td colspan="2">Shipping charge for <?php echo $order_item['quantity']; ?> unit(s)</td><td class="text-right">&#8377; <?php echo $shipping_price_normal; ?></td></tr>
													
													<tr><td colspan="2">Total Value </td><td class="text-right">&#8377; <?php echo $total_amount_normal; ?></td></tr>											
                                                
                                        <?php if($order_item['promotion_available']==1){ ?>   
										
                                                <?php if(intval($order_item['quantity_without_promotion'])>0 && $order_item['default_discount']>0){ ?>
												
												<?php
												
												$default_discount_price=floatval($order_item['product_price']-$order_item['individual_price_of_product_without_promotion']);
		// $default_discount_price=(($order_item['product_price']*$order_item['default_discount'])/100);
		$total_default_discount_price=($order_item['quantity_without_promotion']*$default_discount_price);
		$total_item_price_with_base_price=($order_item['product_price']*$order_item['quantity_without_promotion']);
		
												?>
                                                <tr><td> Discount @ (<?php echo $order_item['default_discount'] ?>%)<br>Eligible Quantity : <?php echo $order_item['quantity_without_promotion'] ?><br>Applied Offer <br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_default_discount_promo']; ?></small></td><td><?php echo $order_item['quantity_without_promotion'] ?> <i class="fa fa-times" aria-hidden="true"></i> <?php echo  $default_discount_price ?></td><td class="text-right" style="color:red"> &#8377; <?php echo $total_default_discount_price;?> </td></tr>
												
												
                                                <?php }?>
                                                
                                                <?php if(intval($order_item['quantity_with_promotion'])>0){ ?>
                                                 
                                                 <?php if(intval($order_item['promotion_discount'])>0&& (intval($order_item['promotion_discount'])==intval($order_item['default_discount']))){?>
												 
												 <?php
												 
												$default_discount_price=floatval($order_item['product_price']-$order_item['individual_price_of_product_with_promotion']);
		// $default_discount_price=(($order_item['product_price']*$order_item['promotion_discount'])/100);
		$total_default_discount_price=($order_item['quantity_with_promotion']*$default_discount_price);
		$total_item_price_with_base_price=($order_item['product_price']*$order_item['quantity_with_promotion']);
		
												 ?>
                                                     <tr><td> Discount @ (<?php echo $order_item['promotion_discount'] ?>%) <br>Eligible Quantity : <?php echo $order_item['quantity_with_promotion'] ?><br>Applied Offer<br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_quote']; ?></small></td><td><?php echo $order_item['quantity_with_promotion'] ?> <i class="fa fa-times" aria-hidden="true"></i> <?php echo  $default_discount_price ?></td><td class="text-right" style="color:red"> &#8377; <?php echo $total_default_discount_price;?> </td></tr>
													 
                                                 <?php }?>
                                                 
                                                 <?php if(intval($order_item['promotion_discount'])>0 && (intval($order_item['promotion_discount'])!=intval($order_item['default_discount']))){?>
												 
												 <?php
												 
												 $promo_discount_price=floatval($order_item['product_price']-$order_item['individual_price_of_product_with_promotion']);
												
		//$promo_discount_price=(($order_item['product_price']*$order_item['promotion_discount'])/100);
		$total_promo_discount_price=($order_item['quantity_with_promotion']*$promo_discount_price);
		$total_item_price_with_base_price=($order_item['product_price']*$order_item['quantity_with_promotion']);
		
												 ?>
                                                      <tr><td>Promotion Discount @ (<?php echo $order_item['promotion_discount'] ?>%)<br>Eligible Quantity : <?php echo $order_item['quantity_with_promotion'] ?><br>Applied Offer<br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_quote']; ?></small></td><td><?php echo $order_item['quantity_with_promotion'] ?> <i class="fa fa-times" aria-hidden="true"></i> <?php echo $promo_discount_price; ?></td><td class="text-right" style="color:red;">&#8377; <?php echo $total_promo_discount_price;?> </td></tr>
                                                 <?php }?>   
                                                    
                                                  <?php if(intval($order_item['promotion_discount'])==0 && $order_item['promotion_default_discount_promo']=="Price for remaining"){?>
                                                      <?php if(intval($order_item['default_discount']>0)){ ?>
												  
                                                     <tr><td>Standard MRP @ (<?php echo $order_item['product_price'] ?>) <br>Eligible Quantity : <?php echo $order_item['quantity_with_promotion'] ?><br>Applied Offer<br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_quote']; ?></small></td><td><?php echo $order_item['quantity_with_promotion'] ?> <i class="fa fa-times" aria-hidden="true"></i> <?php echo $order_item['product_price'] ?></td><td class="text-right">&#8377; <?php echo $order_item["total_price_of_product_with_promotion"];?> </td></tr>
													 
												  <?php }else{
													  ?>
													   <tr><td colspan="3">Applied Offer<br><small style="font-size: .9em;color: blue"><?php echo $order_item['promotion_quote']; ?></small></td></tr>
													  <?php
												  } ?>
                                                 <?php }?>  
                                                    
                                                
                                                <?php }?>
                                                
                                                <?php if(intval($order_item['quantity_without_promotion'])>0 && $order_item['default_discount']==0){ ?>
                                                 <!-- no quote and no default promotion---->

                                                <?php }?>
                                                
                                              <?php } ?>
                        <?php
					
							if($promotion_item!=''){

								foreach($free_items_arr as $free_items){
									?>
									<tr><td><?php echo "SKU : ".$free_items->sku_id."(free)"; ?></td><td><?php $id=$free_items->id;
										echo "Qty ".($temp_quotient*$two[$id]); ?></td><td class="text-right"><del><?php echo "&#8377; ".$free_items->selling_price; ?></del></td></tr>
									<?php
								}
							}

							?>                            
                                              
                                              <tr>
                                                <?php 
												
												if($promotion_invoice_free_shipping>0){   ?>
			
													<td>Shipping charge 
													( <span style="color:red">waived off</span> )
													</td>
                                                <td><?php echo $order_item['quantity'] ?> units(s)</td>
                                                <td class="text-right" style="color:red">
												
												&#8377; <?php echo $order_item["shipping_charge"]; ?>
                                                
                                                </td>
													<?php
												}?>

  
                                                </tr>
                                                
                                                <?php
                                                if($order_item["cash_back_value"]!="" && $order_item["cash_back_value"]!=0){                                          ?>
                                               <tr>
                                                 
                                                <td>Cashback value of <?php echo $order_item['promotion_cashback'] ?> % <br><em>(credited to wallet) </em></td>
                                                <td><?php echo $order_item['quantity'] ?> <i class="fa fa-times" aria-hidden="true"></i>  <?php echo round($order_item["cash_back_value"]/$order_item['quantity'])?></td>
                                                <td class="text-right" >&#8377; <?php echo $order_item["cash_back_value"]; ?></td>
                                                </tr>
                                                
                                                <?php 
												
												}
												?>
                                                
                                                <?php if($promotion_invoice_free_shipping>0){ ?>
                                                  
												  <tr><td colspan="2"><em><b>You Pay </b></em></td><td class="text-right"><b>&#8377; <?php echo $order_item["subtotal"];?></b></td></tr>
												  
                                                <?php }else{ ?>
                                                    <tr><td colspan="2"><em><b>You Pay </b></em></td><td class="text-right"><b>&#8377; <?php echo $order_item["grandtotal"];?></b></td></tr>
                                               <?php } ?>
                                                

                                               
											<?php
											if($total_amount_normal>$order_item["grandtotal"]){
												
											$you_saved=($total_amount_normal-$order_item["grandtotal"]);
											if($promotion_invoice_free_shipping>0){
												$you_saved+=$shipping_price_normal;
											}
											?>
											
											<tr><td colspan="2"><em><b>You Saved</b></em></td><td class="text-right" style="color:green"><b>&#8377; <?php echo $you_saved;?></b></td></tr>
											<?php
											$you_saved_fin+=$you_saved;;
											}
											?>
											</table>
                                                
                                    </div>
<?php
*/
?>

<?php

$total_product_price_normal=($order_item["quantity"]*round($order_item["ord_max_selling_price"]));
$shipping_price_normal=$order_item["shipping_charge"];


$total_amount_normal=($shipping_price_normal+$total_product_price_normal);

if($total_amount_normal>$order_item["grandtotal"]){
												
	$you_saved=($total_amount_normal-$order_item["grandtotal"]);
	if($promotion_invoice_free_shipping>0){
		$you_saved+=$shipping_price_normal;
	}
	$you_saved_fin+=$you_saved;;
}
											
	?>										

                                            
											               
                                            <!--<small>(<i>Inclusive of shipping charges</i>)</small>-->
											
		<?php 
			if($order_item['promotion_available']==1){ 
			if($order_item['total_price_of_product_with_promotion']!="" || $order_item['total_price_of_product_without_promotion']!=""){                        
					$r=0;
					if($order_item['promotion_quote']!=""){
						$r=1;
					}	
					if($order_item['promotion_default_discount_promo']!="" && $order_item['default_discount']>0 && $order_item['promotion_quote']!=$order_item['promotion_default_discount_promo']){
										
						$r+=1;
					}					
					
					
					?>                         
					
					<?php
					/*
					?>
					<a data-trigger="click" data-placement="bottom" data-toggle="popover" data-html="true" id="price_details_2_<?php echo $order_item_id; ?>" class="price_details_thankyou2 cursor-pointer" content_id="#content_price_details_2_<?php echo $order_item_id; ?>" onclick="toggle_content(this)">


					
					Offers : <?php echo $r; ?></a>
					<?php
					*/
					?>
					
					<div id="content_price_details_2_<?php echo $order_item_id; ?>" style="display:block;">
						<table class="table table-bordered" style="font-size: .8em;text-align:justify;margin-bottom: 0px;">
						<?php
						$r=0;
						if($order_item['promotion_quote']!=""){
							$r=1;
							?>				
							<tr><td class="text-center"><span style="color:#0000ff">Offer <?php echo $r;?> : </span><?php 
							$promoquote_display_checktotalcostincludingshippingcharge=$order_item['promotion_quote'];?>
							<?php
							if($order_item['promotion_item']!=""){
								if(isset($free_items_arr)){
									
									foreach($free_items_arr as $freeitemObj){
										
										//$promoquote_display_checktotalcostincludingshippingcharge.=" of ";
										
										// $promoquote_display_checktotalcostincludingshippingcharge.=$freeitemObj->product_name;
		
			
		$color_attribute_is_there="no";
			if(strtolower($freeitemObj->attribute_1)=="color" || strtolower($freeitemObj->attribute_2)=="color" || strtolower($freeitemObj->attribute_3)=="color" || strtolower($freeitemObj->attribute_4)=="color"){
					$color_attribute_is_there="yes";
				if(strtolower($freeitemObj->attribute_1)!="color"){
					$promoquote_display_checktotalcostincludingshippingcharge.="<font class='text-warning'> (".$freeitemObj->attribute_1_value.")</font>";
				}
				else if(strtolower($freeitemObj->attribute_2)!="color"){
					$promoquote_display_checktotalcostincludingshippingcharge.="<font class='text-warning'> (".$freeitemObj->attribute_2_value.")</font>";
				}
				else if(strtolower($freeitemObj->attribute_3)!="color"){
					$promoquote_display_checktotalcostincludingshippingcharge.="<font class='text-warning'> (".$freeitemObj->attribute_3_value.")</font>";
				}
				else if(strtolower($freeitemObj->attribute_4)!="color"){
					$promoquote_display_checktotalcostincludingshippingcharge.="<font class='text-warning'> (".$freeitemObj->attribute_4_value.")</font>";
				}
			}
			else{

						/////////////////////////////////	
							
							$noncolor_attribute_first_is_there="no";
							$noncolor_attribute_second_is_there="no";
							if(strtolower($freeitemObj->attribute_1)!="color"){
								$noncolor_attribute_first_is_there="yes";
								$promoquote_display_checktotalcostincludingshippingcharge.=" - ".$freeitemObj->attribute_1_value;
							}
							if(strtolower($freeitemObj->attribute_2)!="color"){
								if($noncolor_attribute_first_is_there=="yes"){
									$noncolor_attribute_second_is_there="yes";
									if($freeitemObj->attribute_2_value!=""){
										$promoquote_display_checktotalcostincludingshippingcharge.="<font class='text-warning'> (".$freeitemObj->attribute_2_value.")</font>";
									}
								}
								else{
									$noncolor_attribute_first_is_there="yes";
									$promoquote_display_checktotalcostincludingshippingcharge.=" - ".$get_free_item_data_image_path_attributes_arr["attribute_2_value"];
								}
							}
							if(strtolower($freeitemObj->attribute_3)!="color"){
								if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
									if($noncolor_attribute_first_is_there=="yes"){
										$noncolor_attribute_second_is_there="yes";
										if($freeitemObj->attribute_3_value!=""){
											$promoquote_display_checktotalcostincludingshippingcharge.="<font class='text-warning'> (".$freeitemObj->attribute_3_value.")</font>";
										}
									}
									else{
										$noncolor_attribute_first_is_there="yes";
										$promoquote_display_checktotalcostincludingshippingcharge.=" - ".$freeitemObj->attribute_3_value;
									}
								}
							}
							if(strtolower($freeitemObj->attribute_4)!="color"){
								if($noncolor_attribute_first_is_there=="no" || $noncolor_attribute_second_is_there=="no"){
									if($noncolor_attribute_first_is_there=="yes"){
										$noncolor_attribute_second_is_there="yes";
										if($freeitemObj->attribute_4_value!=""){
											$promoquote_display_checktotalcostincludingshippingcharge.="<font class='text-warning'> (".$freeitemObj->attribute_4_value.")</font>";
										}
									}
									else{
										$noncolor_attribute_first_is_there="yes";
										$promoquote_display_checktotalcostincludingshippingcharge.=" - ".$freeitemObj->attribute_4_value;
									}
								}
							}
						
						//////////////////////////////
						
						
			}
			$color_attribute_is_there="no";
			$noncolor_attribute_first_is_there="no";
			$noncolor_attribute_second_is_there="no";	
			$promoquote_display_checktotalcostincludingshippingcharge.=" <img src='".base_url().$freeitemObj->image."' width='40%'>";
			
		
		
		
									}
								}
						}
								echo $promoquote_display_checktotalcostincludingshippingcharge."<br>";
							?>		
							</td></tr>
					   <?php
						} 
						if($order_item['promotion_default_discount_promo']!="" && $order_item['default_discount']>0 && $order_item['promotion_quote']!=$order_item['promotion_default_discount_promo']){
						$r+=1;
						?>
						   <tr><td class="text-center"><span style="color:#0000ff">Offer <?php echo $r;?> : </span><?php echo $order_item['promotion_default_discount_promo'] ?></td></tr>
					   <?php  
						}
					   ?>
						</table>
					</div>
												
                <?php 
			}
			}
			 ?>
											
	</p>										

										</div>
									
<!-----promo_popover------------------>												
<!----------------------------promo popover---------------------------------------------->
											

									
									</div><!--whole--->
																		
									
									</li>
									
									
									
<script>

$(document).ready(function(){
	var order_item_id='<?php echo $order_item_id;?>';
	
	$("#price_details_1_"+order_item_id).popover({
		html: true, 
		content: function() {
				if($(window).width() > 480){
					$('#content_price_details_1_'+order_item_id).hide();
					return $('#content_price_details_1_'+order_item_id).html();	
				}
			}
	});

	$("#price_details_2_"+order_item_id).popover({
		html: true, 
		content: function() {	 
				if($(window).width() > 480){
					$('#content_price_details_2_'+order_item_id).hide();
					return $('#content_price_details_2_'+order_item_id).html();
				}
			}
	});
	
});
</script>
									
									
									
									<?php
									}
									?>									

								</ul>
								
								
							</div>
							
							
							
							<div class="panel-footer">
								<div class="row">
										<div class="col-md-6 text-right"><small><b><?php 
										if($total_amount_saved>0){
											echo "Customer's Total Saving is ".curr_sym." ".$total_amount_saved;
										}elseif($you_saved_fin>0){
											echo "Customer's Total Saving is ".curr_sym." ".$you_saved_fin;
										}else{
											echo "";
										}

										?> </b> </small></div><div class="col-md-6 text-right" style="color:#004b9b;font-size:1.2em;"><b>Total Order Value<span class="order_grand_total"> <?php echo curr_sym;?>  <?php echo $total_amount_to_pay?></b></span></div>
									
								</div>
							</div>	
							
							


            		</div>
				</div>
			
		</div>
			
			<?php
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	
}

public function out_of_stock_switch_fun(){
	$out_of_stock_switch_status=$this->input->post("out_of_stock_switch_status");
	$flag=$this->Model_orders->out_of_stock_switch_fun($out_of_stock_switch_status);
	echo $flag;
}
public function get_out_of_stock_switch(){
	$status=$this->Model_orders->get_out_of_stock_switch();
	return $status;
}

//// failed orders starts //


public function failed_orders(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["cancel_reason"]=$this->Model_orders->get_admin_order_cancel_reasons();
			$this->Model_orders->notify_activeorders_unset();
			$data["menu_flag"]="failed_order_active_links";	
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/failed_orders',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function failed_orders_processing(){
		
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_orders->failed_orders_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_orders->failed_orders_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$cancelflag=true;
				
				if($queryRecordsObj->type_of_order=="replaced"){
					$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->prev_order_item_id);
					
					if($order_replacement_decision_data_obj->customer_status=="cancel"){
						$cancelflag=false;
					}
				}
				
				if($cancelflag){
					
				//$row[]="<input type='checkbox' name='onhold_orders_list' value='".$queryRecordsObj->orders_status_id."'>";
				
				$order_summary='<ul class="list-unstyled small">';

				//$queryRecordsObj->ord_addon_products_status=='1'
				if(0){
					$combo_prod=$this->get_addon_data($queryRecordsObj);
					$order_summary.='<li>'.$combo_prod.'</li>';
					$row[]="Combo products";
					
					
					$order_summary.="<li>SKU: ".$queryRecordsObj->sku_id."</li>";
                                if($queryRecordsObj->ord_sku_name!=''){
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
                                }else{
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
                                }
								
								
				
				}else{

					$inventory_id=$queryRecordsObj->inventory_id;
					$image=$queryRecordsObj->image;
					$img_path=get_inventory_image_path($inventory_id,$image);

					$row[]="<img src='".base_url().$img_path."'  style='width: 100px;height: 122px;' >";
				
					$order_summary.="<li>SKU: ".$queryRecordsObj->sku_id."</li>";
                                if($queryRecordsObj->ord_sku_name!=''){
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->ord_sku_name."</li>";
                                }else{
                                    $order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";
                                }

								$product_details=get_product_details($queryRecordsObj->product_id);
								if(!empty($product_details))
								$order_summary.=($product_details->product_description!='') ? "<li class='mt-10'><small>".$product_details->product_description.'</small></li>': '';
								
								
				}
				//$order_summary.="<li class='text-info bold'>".$queryRecordsObj->product_name."</li>";

				$order_summary.="<li>Order Date: ".date("D j M, Y",strtotime($queryRecordsObj->timestamp))."</li>";
				$order_summary.="<li>Order ID: ".$queryRecordsObj->order_id.' <i  style="cursor:pointer" class="fa fa-question-circle orderid_details_popover" data-trigger="focus" id="orderid_'.$queryRecordsObj->order_id."_".$queryRecordsObj->order_item_id.'" aria-hidden="true"></i>'."</li>";
				$order_summary.="<li>Order Item ID: ".$queryRecordsObj->order_item_id."</li>";
				if($queryRecordsObj->type_of_order==""){
					$order_summary.="<li>Payment Type: ".$queryRecordsObj->payment_type."</li>";
					if($queryRecordsObj->payment_type=='Razorpay'){
                                            $arr=razorpay_ids($queryRecordsObj->order_id);
                                            if(!empty($arr)){
                                                $order_summary.="<li>Razorpay Order ID: ".$arr['razorpayOrderId']."</li>";
                                                $order_summary.="<li>Razorpay Payment ID: ".$arr['razorpayPaymentId']."</li>";
                                            }
                                        }
				}
				////////////////added -replacement payment///////////////////////
				if($queryRecordsObj->type_of_order=="replaced"){

					if($order_replacement_decision_data_obj->paid_by=="customer"){
						if($order_replacement_decision_data_obj->balance_amount_paid_by=="cod"){
							$order_summary.="<li>Payment Type: COD <span class='text-info bold'>(Amount due)</span></li>";
						}else{
							$order_summary.="<li>Payment Type: ".$order_replacement_decision_data_obj->balance_amount_paid_by."</li>";
							
							if($order_replacement_decision_data_obj->payment_status=="paid"){
								
								$str= " <span class='text-info bold'>(".$order_replacement_decision_data_obj->paid_by."  paid through ".$order_replacement_decision_data_obj->balance_amount_paid_by.") </span>";
							}else{
								$str=($order_replacement_decision_data_obj->balance_amount_paid_by=="")? " Not Selected" : "<span class='text-info bold'>(".$order_replacement_decision_data_obj->paid_by." has to pay through ".$order_replacement_decision_data_obj->balance_amount_paid_by.") </span>";
							}			
							$order_summary.='<li>'.$str.'</li>';
						}
					}
				
					if($order_replacement_decision_data_obj->paid_by=="admin"){
						$order_summary.="<li>Payment Type: Null </li>";
						//$return_summary.=" (".$order_replacement_decision_data_obj->paid_by." has to pay through ".$order_replacement_decision_data_obj->refund_method.")<br>";
					}
				
				}
			////////////////added -replacement payment///////////////////////
				
				if($queryRecordsObj->type_of_order=="replaced"){
					$order_summary.="<li>[<span class='text-info bold'>Replaced Order</span>]</li>";
				}
				
				if($queryRecordsObj->prev_order_item_id!=""){
					
					$orders_status_data_obj=$this->Model_orders->get_orders_status_data($queryRecordsObj->prev_order_item_id);
					
					$replacement_desired_data_obj=$this->Model_orders->get_replacement_desired_data($queryRecordsObj->prev_order_item_id);
					
					//$order_replacement_decision_data_obj=$this->get_order_replacement_decision_data($queryRecordsObj->prev_order_item_id);
					
					if($order_replacement_decision_data_obj->pickup_identifier=="after replace"){
						$order_summary.="<li>[<span class='text-info bold'>Pickup has been completed</span>]</li>";
					}
					if($order_replacement_decision_data_obj->pickup_identifier=="before replace"){
						$replacement_option_status='';
						if($orders_status_data_obj->initiate_replacement_pickup =='1' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='[Return the Item]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='1' && $orders_status_data_obj->order_replacement_pickup==1){
							$replacement_option_status='[Item Had been Collected Successfully]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='0' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='[Initiate Pickup has been rejected by admin]';
						}
						if($orders_status_data_obj->initiate_replacement_pickup=='' && $orders_status_data_obj->order_replacement_pickup==0){
							$replacement_option_status='';
						}
						$order_summary.="<li>".'<span class="text-info bold">'.$replacement_option_status.'</span>'."</li>";
					}
				}
				
				/////////////////added - promotion////////////////////
				$str_promo='';	
				if($queryRecordsObj->type_of_order=="replaced"){

					$prev_order_item_id=$queryRecordsObj->prev_order_item_id;
					$purchased_order_data_obj=$this->Model_orders->get_shipping_charge_and_grand_total_from_history_orders($prev_order_item_id);
					//print_r($orders_status_data_obj);
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					
					$str_promo=$this->get_details_of_promotion_for_replaced_order($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal,$prev_order_item_id);
					
				}else{
					$purchased_quantity_initial=$queryRecordsObj->quantity;
					$shipping_charge=$queryRecordsObj->shipping_charge;
					$grandtotal=$queryRecordsObj->grandtotal;
					$str_promo=$this->get_details_of_promotion($queryRecordsObj,$purchased_quantity_initial,$shipping_charge,$grandtotal);
				}
			   
				$order_summary.='<li>'.$str_promo.'</li>';
				$order_summary.='</ul>';
				
			
				/////////////////added - promotion////////////////////
				
			
				
				$row[]=$order_summary;

				//$logi_details=$this->logistics_details_of_order($queryRecordsObj);
				//$row[]=$logi_details;
				
				$order_quantity_price='<ul class="list-unstyled small">';
				
				if($queryRecordsObj->type_of_order=="replaced"){
					//$replaced_item_price_individual=$this->Model_orders->get_replaced_item_price_individual($prev_order_item_id);
					$replaced_item_price_individual=$queryRecordsObj->product_price;
					$order_quantity_price.="<li>Qty: ".$queryRecordsObj->quantity."</li>";
					
					$order_quantity_price.="<li>Price of replacement order (each): ".curr_sym." ".$replaced_item_price_individual."</li>";
					
					$order_quantity_price.="<li>Shipping charge for replacement:  ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
					$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
					
				}else{
					$order_quantity_price.="<li>Qty: ".$queryRecordsObj->quantity."</li>";
					$order_quantity_price.="<li>Total Item Price: ".curr_sym." ".$queryRecordsObj->subtotal."</li>";
					
					if($queryRecordsObj->promotion_invoice_free_shipping>0){
						$order_quantity_price.="<li>Shipping charge (<span class='text-danger'>waived off</span>): ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
						$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->subtotal."</li>";
					}else{
						$order_quantity_price.="<li>Shipping charge: ".curr_sym." ".$queryRecordsObj->shipping_charge."</li>";
						$order_quantity_price.="<li>Total: ".curr_sym." ".$queryRecordsObj->grandtotal."</li>";
					}

					//$order_quantity_price.='<li><a class="text-info bold" href="#" onclick="view_order_summary_(\''.$queryRecordsObj->order_id.'\')" id="view_order_summary_'.$queryRecordsObj->order_item_id.'">View Invoice Data ';
					
					//if($queryRecordsObj->invoice_offer_achieved==1){
						//$invoice_ofr_count=$this->get_invoice_offers_str($queryRecordsObj);
						//$order_quantity_price.='<span class="badge">'.$invoice_ofr_count.'</span>';
					//}
					//$order_quantity_price.='</a></li>';
				}
				//echo $queryRecordsObj->order_id;
				
				$order_quantity_price.='</ul>';
				
				$row[]=$order_quantity_price;
				$customer_add='<ul class="list-unstyled small">';
				$customer_add.="<li>".$queryRecordsObj->customer_name."</li>";
				$customer_add.='<li>'.$queryRecordsObj->address1."</li>";
				$customer_add.='<li>'.$queryRecordsObj->address2."</li>";
				$customer_add.='<li>'.$queryRecordsObj->city.",".$queryRecordsObj->state."</li>";
				$customer_add.='<li>'.$queryRecordsObj->country."</li>";
				$customer_add.='<li>'.$queryRecordsObj->pincode."</li>";
				$customer_add.='<li>'.$queryRecordsObj->mobile."</li>";
				$customer_add.='</ul>';
				$row[]=$customer_add;
				$data[]=$row;
				$row=array();
				
				}
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	
//// failed orders ends ////
public function get_baseurl(){
		$url_current_page=sprintf(
		"%s://%s/",
			isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
			$_SERVER['SERVER_NAME'],
			$_SERVER['REQUEST_URI']
		  );
		if(preg_match("/localhost/",$url_current_page)){
			return "http://localhost/hurbz/";
		}
		else{
			return $url_current_page;
		}
	}


}
