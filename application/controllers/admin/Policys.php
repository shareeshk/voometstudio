<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Policys extends CI_Controller {
        private $pagesize = 20;
	function __construct()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model('admin/Policys_manage_data');
        $this->load->library('My_PHPMailer');
		$this->load->helper('emailsmstemplate');
	}
	public function index(){
        if($this->session->userdata("logged_in")){
            $data['policy_list']=$this->Policys_manage_data->get_all_policy_list();
			$data["menu_flag"]="";
            $this->load->view('admin/vmanage',$data);
            $this->load->view('admin/policy/policy_list',$data);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    
    public function get_all_existing_policy(){
        if($this->session->userdata("logged_in")){
            $policy_type_list="";
            $policy_type_id=$this->input->post('policy_list_id');
            $policy_type=$this->get_policy_type($policy_type_id);
            if($policy_type=='Payment'){
                $policy_type_list=$this->Policys_manage_data->get_all_payment_related_policy();
            }
            
            echo json_encode($policy_type_list);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function get_policy_type($policy_type_id){
        return $this->Policys_manage_data->get_policy_type($policy_type_id);
    }
	public function create_payment_policy($id){
        if($this->session->userdata("logged_in")){
            $this->load->model('admin/promotions_manage_data');
            $data['parent_category']=$this->promotions_manage_data->get_all_parent_category();
            $data['payment_policy_attributes']=$this->Policys_manage_data->get_all_payment_policy_attributes($id);
            $data['policy_type']=$id;
            $this->load->view('admin/vmanage');
            $this->load->view('admin/policy/create_payment_policy',$data);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function add_payment_restrictions(){
        if($this->session->userdata("logged_in")){
            $products_tagged_for_purchasing="";
            $parent_category="";
            $category="";
            $sub_category="";
            $attributes="";
            $attributes_selector="";
            $payment_method="";
            $cod_restrictions="";
            $max_val="";
            $min_val="";
            $message="";
            $currency_type="";
            $override_previous_policy="";
            $policy_type_name="";
            $name_of_products_tagged_for_purchasing="";
            //print_r($this->input->post());exit;
            extract($this->input->post());
            $date = new DateTime();
            $mt=microtime();
            $salt="promo_id";
            $rand=mt_rand(1, 1000000);
            $ts=$date->getTimestamp();
            $str=$salt.$rand.$ts.$mt;
            $policy_uid=md5($str);
            if($override_previous_policy!="0"){
                $this->delete_policy_internal($override_previous_policy,$policy_type_name);
            }
            
            $payment_method=implode(',', $payment_method);
            $payment_method;
            $this->Policys_manage_data->details_for_policy_payment_restriction($policy_uid,$message,$max_val,$min_val,$currency_type,$payment_method,$name_of_products_tagged_for_purchasing,$cod_restrictions);
            $added=$this->Policys_manage_data->add_items_tagged_for_policy_restriction($policy_type_id,$policy_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector);
            if($added){
                $this->session->set_flashdata('notification','Policy Added');
                redirect(base_url().'admin/Policys');
            }
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function payment_processing(){
      if($this->session->userdata("logged_in")){
        $params = $columns = $totalRecords = $data = array();
    
        $params = $this->input->post();
        
        $totalRecords=$this->Policys_manage_data->Payment_processing($params,"get_total_num_recs");
        
        $queryRecordsResult=$this->Policys_manage_data->Payment_processing($params,"get_recs");
        
        $i=count($queryRecordsResult);
        $i=0;
        foreach($queryRecordsResult as $queryRecordsObj){
            if($queryRecordsObj->active==0){
                $status="Not Active";
            }
            if($queryRecordsObj->active==1){
                $status="Active";
            }
            
            if($queryRecordsObj->timestamp!=""){
                $date_since=date("D,j M Y H:i", strtotime($queryRecordsObj->timestamp));
            }
            else{
                $date_since="";
            }

            $restriction="";
            if($queryRecordsObj->max_value!=0){
                $restriction="(Restriction Above ".$queryRecordsObj->max_value." ".$queryRecordsObj->currency_type.")";
            }else{
                $restriction=" (Full Restrictions) ";
            }
            
            $row=array();
            $row[]=++$i;
            $row[]=$queryRecordsObj->attributes;
            $row[]='<a href="'.base_url().'admin/Policys/getDetailofPaymentPolicy/'.$queryRecordsObj->payment_policy_uid.'">'.$queryRecordsObj->item_level.' '.$restriction.'</a>';
            $row[]='<button class="btn- btn-danger btn-xs" policy_type="Payment" policy_uid="'.$queryRecordsObj->payment_policy_uid.'" onclick="delete_policy(this)"><i class="fa fa-trash-o"></i></button>';
            $row[]=$date_since;
            
            $data[]=$row;
            $row=array();
        }
        $json_data = array(
                "draw"            => intval( $params['draw'] ),   
                "recordsTotal"    => intval( $totalRecords ),  
                "recordsFiltered" => intval($totalRecords),
                "data"            => $data   // total data array
                );
        echo json_encode($json_data);  // send data as json format
    
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }

    public function getDetailofPaymentPolicy($id){
        if($this->session->userdata("logged_in")){
            $data['policy_data']=$this->Policys_manage_data->getDataOfParticularPaymentPolicy($id);
            $this->load->model('admin/promotions_manage_data');
            $data['parent_category']=$this->promotions_manage_data->get_all_parent_category();
            
            $data['policy_items']=$this->Policys_manage_data->getItemsOfParticularPolicy($id);
			if(isset($data['policy_items'][0]['policy_type'])){
				$data['payment_policy_attributes']=$this->Policys_manage_data->get_all_payment_policy_attributes($data['policy_items'][0]['policy_type']);
			}
			else{
				$data['payment_policy_attributes']="";
			}
			if(isset($data['policy_items'][0]['policy_type'])){
				$data['policy_type']=$data['policy_items'][0]['policy_type'];
			}
			else{
				$data['policy_type']="";
			}
            $data['policy_uid']=$id;
            $this->load->view('admin/vmanage');
            $this->load->view('admin/policy/update_payment_policy',$data);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    
    public function update_payment_restrictions(){
        $products_tagged_for_purchasing="";
            $parent_category="";
            $category="";
            $sub_category="";
            $attributes="";
            $attributes_selector="";
            $payment_method="";
            $cod_restrictions="";
            $max_val="";
            $min_val="";
            $message="";
            $currency_type="";
            $policy_uid="";
            $policy_type_id="";
            
            $override_previous_policy="";
            $policy_type_name="";
            
            $name_of_products_tagged_for_purchasing="";
            $policy_list_modified="";
            //print_r($this->input->post());exit;
            extract($this->input->post());

            if($override_previous_policy!="0"){
                $this->delete_policy_internal($override_previous_policy,$policy_type_name);
            }
            $payment_method=implode(',', $payment_method);
            $payment_method;
            $added=$this->Policys_manage_data->update_for_policy_payment_restriction($policy_uid,$message,$max_val,$min_val,$currency_type,$payment_method,$name_of_products_tagged_for_purchasing,$cod_restrictions);
            if($policy_list_modified==1){
                $added=$this->Policys_manage_data->update_items_tagged_for_policy_restriction($policy_type_id,$policy_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector);
            }
            
            if($added){
                $this->session->set_flashdata('notification','Policy Updated');
                redirect(base_url().'admin/Policys');
            }
        
    }

    public function delete_policy_internal($policy_list_id,$policy_type){
        if($this->session->userdata("logged_in")){
            if($policy_type=="payment"){
                $deleted=$this->Policys_manage_data->delete_policy_payment($policy_list_id);
            }
            if($policy_type=="replacement"){
                $deleted=$this->Policys_manage_data->delete_replacement_payment($policy_list_id);
            }
            if($policy_type=="refund"){
                $deleted=$this->Policys_manage_data->delete_refund_payment($policy_list_id);
            }
            if($policy_type=="cancellation"){
                $deleted=$this->Policys_manage_data->delete_cancellation_payment($policy_list_id);
            }
            if($policy_type=="exchange"){
                $deleted=$this->Policys_manage_data->delete_exchange_payment($policy_list_id);
            }
            if($policy_type=="shipping_charge_for_replacement"){
                $deleted=$this->Policys_manage_data->delete_shipping_charge_for_replacement($policy_list_id);
            }
            if($deleted){
                return true;
            }
        }
        else{ 
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function delete_policy(){
        $policy_list_id=$this->input->post('policy_list_id');
        $policy_type=$this->input->post('policy_type');
        if(strtolower($policy_type)=="payment"){
            $deleted=$this->Policys_manage_data->delete_policy_payment($policy_list_id);
        }
        if(strtolower($policy_type)=="replacement"){
            $deleted=$this->Policys_manage_data->delete_replacement_payment($policy_list_id);
        }
        if(strtolower($policy_type)=="refund"){
            $deleted=$this->Policys_manage_data->delete_refund_payment($policy_list_id);
        }
        if(strtolower($policy_type)=="cancellation"){
            $deleted=$this->Policys_manage_data->delete_cancellation_payment($policy_list_id);
        }
        if(strtolower($policy_type)=="exchange"){
            $deleted=$this->Policys_manage_data->delete_exchange_payment($policy_list_id);
        }
        if(strtolower($policy_type)=="shipping_charge_for_replacement"){
                $deleted=$this->Policys_manage_data->delete_shipping_charge_for_replacement($policy_list_id);
            }
        if($deleted){
            echo 1;
        }
    }
    
    public function create_replacement_policy($id){
        if($this->session->userdata("logged_in")){
            $this->load->model('admin/promotions_manage_data');
            $data['parent_category']=$this->promotions_manage_data->get_all_parent_category();
            //$data['payment_replacement_attributes']=$this->Policys_manage_data->get_all_payment_policy_attributes($id);
            $data['policy_type']=$id;
            $this->load->view('admin/vmanage');
            $this->load->view('admin/policy/create_replacement_policy',$data);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function add_replacement_restrictions(){
        if($this->session->userdata("logged_in")){
            $products_tagged_for_purchasing="";
            $parent_category="";
            $category="";
            $sub_category="";
            $attributes="";
            $attributes_selector="";
            $replacement_restrict="";
            $new_days_for_replacement="";
            $message="";
            $name_of_products_tagged_for_purchasing="";
            $override_previous_policy="";
            $policy_type_name="";
            //print_r($this->input->post());exit;
            extract($this->input->post());
            
            if($override_previous_policy!="0"){
                $this->delete_policy_internal($override_previous_policy,$policy_type_name);
            }
            $date = new DateTime();
            $mt=microtime();
            $salt="promo_id";
            $rand=mt_rand(1, 1000000);
            $ts=$date->getTimestamp();
            $str=$salt.$rand.$ts.$mt;
            $policy_uid=md5($str);
            $this->Policys_manage_data->details_for_policy_replacement_restriction($policy_uid,$message,$replacement_restrict,$new_days_for_replacement,$name_of_products_tagged_for_purchasing,$cod_restrictions);
            $added=$this->Policys_manage_data->add_items_tagged_for_policy_restriction($policy_type_id,$policy_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector);
            if($added){
                $this->session->set_flashdata('notification','Policy Added');
                redirect(base_url().'admin/Policys');
            }
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function replacement_processing(){
      if($this->session->userdata("logged_in")){
        $params = $columns = $totalRecords = $data = array();
    
        $params = $this->input->post();
        
        $totalRecords=$this->Policys_manage_data->Replacement_processing($params,"get_total_num_recs");
        
        $queryRecordsResult=$this->Policys_manage_data->Replacement_processing($params,"get_recs");
        
        $i=count($queryRecordsResult);
        $i=0;
        foreach($queryRecordsResult as $queryRecordsObj){
            if($queryRecordsObj->active==0){
                $status="Not Active";
            }
            if($queryRecordsObj->active==1){
                $status="Active";
            }
            
            if($queryRecordsObj->timestamp!=""){
                $date_since=date("D,j M Y H:i", strtotime($queryRecordsObj->timestamp));
            }
            else{
                $date_since="";
            }

            $restriction="";
            if($queryRecordsObj->replacement_restrict=="new_day_limit"){
                $restriction_type="Limit On Days";
                $restriction="(New Limit of ".$queryRecordsObj->new_days_for_replacement." day(s))";
            }else{
                $restriction_type="No Replacement Policy";
                $restriction="";
            }
            
            $row=array();
            $row[]=++$i;
            $row[]=$restriction_type;
            $row[]='<a href="'.base_url().'admin/Policys/getDetailofReplacementPolicy/'.$queryRecordsObj->replacement_policy_uid.'">'.$queryRecordsObj->item_level.' '.$restriction.'</a>';
            $row[]='<button class="btn- btn-danger btn-xs" policy_type="Replacement" policy_uid="'.$queryRecordsObj->replacement_policy_uid.'" onclick="delete_policy(this)"><i class="fa fa-trash-o"></i></button>';
            $row[]=$date_since;
            
            $data[]=$row;
            $row=array();
        }
        $json_data = array(
                "draw"            => intval( $params['draw'] ),   
                "recordsTotal"    => intval( $totalRecords ),  
                "recordsFiltered" => intval($totalRecords),
                "data"            => $data   // total data array
                );
        echo json_encode($json_data);  // send data as json format
    
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }

    public function getDetailofReplacementPolicy($id){
        if($this->session->userdata("logged_in")){
            $data['policy_data']=$this->Policys_manage_data->getDataOfParticularReplacementPolicy($id);
            $this->load->model('admin/promotions_manage_data');
            $data['parent_category']=$this->promotions_manage_data->get_all_parent_category();
            
            $data['policy_items']=$this->Policys_manage_data->getItemsOfParticularPolicy($id);
            //$data['payment_policy_attributes']=$this->Policys_manage_data->get_all_payment_policy_attributes($data['policy_items'][0]['policy_type']);
            $data['policy_type']=$data['policy_items'][0]['policy_type'];
            $data['policy_uid']=$id;
            $this->load->view('admin/vmanage');
            $this->load->view('admin/policy/update_replacement_policy',$data);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    
    public function update_replacement_restrictions(){
           $products_tagged_for_purchasing="";
            $parent_category="";
            $category="";
            $sub_category="";
            $attributes="";
            $attributes_selector="";
            $replacement_restrict="";
            $new_days_for_replacement="";
            $message="";
            $name_of_products_tagged_for_purchasing="";
            $policy_uid="";
            $policy_type_id="";
            $name_of_products_tagged_for_purchasing="";
            $policy_list_modified="";
            $override_previous_policy="";
            $policy_type_name="";
            //print_r($this->input->post());exit;
            extract($this->input->post());
            
            if($override_previous_policy!="0"){
                $this->delete_policy_internal($override_previous_policy,$policy_type_name);
            }
            $added=$this->Policys_manage_data->update_for_policy_replacement_restriction($policy_uid,$message,$replacement_restrict,$new_days_for_replacement,$name_of_products_tagged_for_purchasing);
            if($policy_list_modified==1){
                $added=$this->Policys_manage_data->update_items_tagged_for_policy_restriction($policy_type_id,$policy_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector);
            }
            
            if($added){
                $this->session->set_flashdata('notification','Policy Updated');
                redirect(base_url().'admin/Policys');
            }
        
    }

    public function create_refund_policy($id){
        if($this->session->userdata("logged_in")){
            $this->load->model('admin/promotions_manage_data');
            $data['parent_category']=$this->promotions_manage_data->get_all_parent_category();
            $data['payment_refund_attributes']=$this->Policys_manage_data->get_all_payment_policy_attributes($id);
            $data['policy_type']=$id;
            $this->load->view('admin/vmanage');
            $this->load->view('admin/policy/create_Refund_policy',$data);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function add_refund_restrictions(){
        if($this->session->userdata("logged_in")){
            $products_tagged_for_purchasing="";
            $parent_category="";
            $category="";
            $sub_category="";
            $attributes="";
            $attributes_selector="";
            $refund_restrict="";
            $new_days_for_refund="";
            $message="";
            $policy_type_id="";
            $name_of_products_tagged_for_purchasing="";
            $override_previous_policy="";
            $policy_type_name="";
            //print_r($this->input->post());exit;
            extract($this->input->post());
            
            if($override_previous_policy!="0"){
                $this->delete_policy_internal($override_previous_policy,$policy_type_name);
            }
            $refund_method=implode(',', $refund_method);

            $date = new DateTime();
            $mt=microtime();
            $salt="promo_id";
            $rand=mt_rand(1, 1000000);
            $ts=$date->getTimestamp();
            $str=$salt.$rand.$ts.$mt;
            $policy_uid=md5($str);
            $this->Policys_manage_data->details_for_policy_refund_restriction($policy_uid,$message,$refund_restrict,$new_days_for_refund,$name_of_products_tagged_for_purchasing,$refund_method);
            $added=$this->Policys_manage_data->add_items_tagged_for_policy_restriction($policy_type_id,$policy_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector);
            if($added){
                $this->session->set_flashdata('notification','Policy Added');
                redirect(base_url().'admin/Policys');
            }
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function refund_processing(){
      if($this->session->userdata("logged_in")){
        $params = $columns = $totalRecords = $data = array();
    
        $params = $this->input->post();
        
        $totalRecords=$this->Policys_manage_data->Refund_processing($params,"get_total_num_recs");
        
        $queryRecordsResult=$this->Policys_manage_data->Refund_processing($params,"get_recs");
        
        $i=count($queryRecordsResult);
        $i=0;
        foreach($queryRecordsResult as $queryRecordsObj){
            if($queryRecordsObj->active==0){
                $status="Not Active";
            }
            if($queryRecordsObj->active==1){
                $status="Active";
            }
            
            if($queryRecordsObj->timestamp!=""){
                $date_since=date("D,j M Y H:i", strtotime($queryRecordsObj->timestamp));
            }
            else{
                $date_since="";
            }

            $restriction="";
            if($queryRecordsObj->refund_restrict=="new_day_limit"){
                $restriction_type="Refund Timline";
                $restriction="{New Limit of ".$queryRecordsObj->new_days_for_refund." day(s)}";
            }else{
                $restriction_type="No Refund Policy";
                $restriction="";
            }
            
            $row=array();
            $row[]=++$i;
            $row[]=$restriction_type.' '.$restriction;
            $row[]='<a href="'.base_url().'admin/Policys/getDetailofRefundPolicy/'.$queryRecordsObj->refund_policy_uid.'">'.$queryRecordsObj->item_level.'</a>';
            $row[]='<button class="btn- btn-danger btn-xs" policy_type="Refund" policy_uid="'.$queryRecordsObj->refund_policy_uid.'" onclick="delete_policy(this)"><i class="fa fa-trash-o"></i></button>';
            $row[]=$date_since;
            
            $data[]=$row;
            $row=array();
        }
        $json_data = array(
                "draw"            => intval( $params['draw'] ),   
                "recordsTotal"    => intval( $totalRecords ),  
                "recordsFiltered" => intval($totalRecords),
                "data"            => $data   // total data array
                );
        echo json_encode($json_data);  // send data as json format
    
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }

    public function getDetailofRefundPolicy($id){
        if($this->session->userdata("logged_in")){
            $data['policy_data']=$this->Policys_manage_data->getDataOfParticularRefundPolicy($id);
            $this->load->model('admin/promotions_manage_data');
            $data['parent_category']=$this->promotions_manage_data->get_all_parent_category();
            
            $data['policy_items']=$this->Policys_manage_data->getItemsOfParticularPolicy($id);
			if(isset($data['policy_items'][0]['policy_type'])){
				$data['payment_refund_attributes']=$this->Policys_manage_data->get_all_payment_policy_attributes($data['policy_items'][0]['policy_type']);
			}
			else{
				$data['payment_refund_attributes']="";
			}
			if(isset($data['policy_items'][0]['policy_type'])){
				$data['policy_type']=$data['policy_items'][0]['policy_type'];
			}
			else{
				$data['policy_type']="";
			}
            $data['policy_uid']=$id;
            $this->load->view('admin/vmanage');
            $this->load->view('admin/policy/update_refund_policy',$data);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    
    public function update_refund_restrictions(){
            $products_tagged_for_purchasing="";
            $parent_category="";
            $category="";
            $sub_category="";
            $attributes="";
            $attributes_selector="";
            $refund_restrict="";
            $new_days_for_refund="";
            $message="";
            $policy_type_id="";
            $name_of_products_tagged_for_purchasing="";
            $refund_method="";
            $override_previous_policy="";
            $policy_type_name="";
            //print_r($this->input->post());exit;
            extract($this->input->post());
            
            if($override_previous_policy!="0"){
                $this->delete_policy_internal($override_previous_policy,$policy_type_name);
            }
            
            
            if($refund_method!=""){
                $refund_method=implode(',', $refund_method);
            }
            $added=$this->Policys_manage_data->update_for_policy_refund_restriction($policy_uid,$message,$refund_restrict,$new_days_for_refund,$name_of_products_tagged_for_purchasing,$refund_method);
            if($policy_list_modified==1){
                $added=$this->Policys_manage_data->update_items_tagged_for_policy_restriction($policy_type_id,$policy_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector);
            }
            
            if($added){
                $this->session->set_flashdata('notification','Policy Updated');
                redirect(base_url().'admin/Policys');
            }
        
    }
 
    public function create_cancellation_policy($id){
        if($this->session->userdata("logged_in")){
            $this->load->model('admin/promotions_manage_data');
            $data['parent_category']=$this->promotions_manage_data->get_all_parent_category();
            //$data['payment_replacement_attributes']=$this->Policys_manage_data->get_all_payment_policy_attributes($id);
            $data['policy_type']=$id;
            $this->load->view('admin/vmanage');
            $this->load->view('admin/policy/create_cancellation_policy',$data);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
        public function add_cancellation_restrictions(){
        if($this->session->userdata("logged_in")){
            $products_tagged_for_purchasing="";
            $parent_category="";
            $category="";
            $sub_category="";
            $attributes="";
            $attributes_selector="";
            $cancellation_restrict="";
            $new_days_for_cancellation="";
            $message="";
            $name_of_products_tagged_for_purchasing="";
            $new_hour_limit="";
            $override_previous_policy="";
            $policy_type_name="";
            
            //print_r($this->input->post());exit;
            extract($this->input->post());
            $date = new DateTime();
            $mt=microtime();
            $salt="promo_id";
            $rand=mt_rand(1, 1000000);
            $ts=$date->getTimestamp();
            $str=$salt.$rand.$ts.$mt;
            $policy_uid=md5($str);
            
            if($override_previous_policy!="0"){
                $this->delete_policy_internal($override_previous_policy,$policy_type_name);
            }
            
            $this->Policys_manage_data->details_for_policy_cancellation_restriction($policy_uid,$message,$cancellation_restrict,$new_days_for_cancellation,$name_of_products_tagged_for_purchasing,$new_hour_limit,$select_order_status_type);
            $added=$this->Policys_manage_data->add_items_tagged_for_policy_restriction($policy_type_id,$policy_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector);
            if($added){
                $this->session->set_flashdata('notification','Policy Added');
                redirect(base_url().'admin/Policys');
            }
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function cancellation_processing(){
      if($this->session->userdata("logged_in")){
        $params = $columns = $totalRecords = $data = array();
    
        $params = $this->input->post();
        
        $totalRecords=$this->Policys_manage_data->Cancellation_processing($params,"get_total_num_recs");
        
        $queryRecordsResult=$this->Policys_manage_data->Cancellation_processing($params,"get_recs");
        
        $i=count($queryRecordsResult);
        $i=0;
        foreach($queryRecordsResult as $queryRecordsObj){
            if($queryRecordsObj->active==0){
                $status="Not Active";
            }
            if($queryRecordsObj->active==1){
                $status="Active";
            }
            
            if($queryRecordsObj->timestamp!=""){
                $date_since=date("D,j M Y H:i", strtotime($queryRecordsObj->timestamp));
            }
            else{
                $date_since="";
            }
			$restriction_type="";
            $restriction="";
            if($queryRecordsObj->cancellation_restrict=="new_day_limit"){
				if($queryRecordsObj->order_status_type!=""){
					$restriction_type.="Limit On Order Status";
					$restriction.="(".ucwords($queryRecordsObj->order_status_type).")<br>";
				}
                if($queryRecordsObj->new_hour_limit!=""){
                    $restriction_type.="Limit On Hours";
                    $restriction.="(New Limit of ".$queryRecordsObj->new_hour_limit." hours(s))";
                }
                if($queryRecordsObj->new_days_for_cancellation!=""){
                    $restriction_type.="Limit On Days";
                    $restriction.="(New Limit of ".$queryRecordsObj->new_days_for_cancellation." day(s))";
                }
                
            }else{
                $restriction_type="No Cancellation Policy";
                $restriction="";
            }
            
            $row=array();
            $row[]=++$i;
            $row[]=$restriction_type.' '.$restriction;
            $row[]='<a href="'.base_url().'admin/Policys/getDetailofCancellationPolicy/'.$queryRecordsObj->cancellation_policy_uid.'">'.$queryRecordsObj->item_level.'</a>';
            $row[]='<button class="btn- btn-danger btn-xs" policy_type="Cancellation" policy_uid="'.$queryRecordsObj->cancellation_policy_uid.'" onclick="delete_policy(this)"><i class="fa fa-trash-o"></i></button>';
            $row[]=$date_since;
            
            $data[]=$row;
            $row=array();
        }
        $json_data = array(
                "draw"            => intval( $params['draw'] ),   
                "recordsTotal"    => intval( $totalRecords ),  
                "recordsFiltered" => intval($totalRecords),
                "data"            => $data   // total data array
                );
        echo json_encode($json_data);  // send data as json format
    
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }

    public function getDetailofCancellationPolicy($id){
        if($this->session->userdata("logged_in")){
            $data['policy_data']=$this->Policys_manage_data->getDataOfParticularCancellationPolicy($id);
            $this->load->model('admin/promotions_manage_data');
            $data['parent_category']=$this->promotions_manage_data->get_all_parent_category();
            
            $data['policy_items']=$this->Policys_manage_data->getItemsOfParticularPolicy($id);
            //$data['payment_policy_attributes']=$this->Policys_manage_data->get_all_payment_policy_attributes($data['policy_items'][0]['policy_type']);
			if(isset($data['policy_type'])){
            $data['policy_type']=$data['policy_items'][0]['policy_type'];
			}else{
				$data['policy_type']="";
			}
            $data['policy_uid']=$id;
            $this->load->view('admin/vmanage');
            $this->load->view('admin/policy/update_cancellation_policy',$data);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    
    public function update_cancellation_restrictions(){
           $products_tagged_for_purchasing="";
            $parent_category="";
            $category="";
            $sub_category="";
            $attributes="";
            $attributes_selector="";
            $cancellation_restrict="";
            $new_days_for_cancellation="";
            $message="";
            $name_of_products_tagged_for_purchasing="";
            $policy_uid="";
            $policy_type_id="";
            $name_of_products_tagged_for_purchasing="";
            $policy_list_modified="";
            $new_hour_limit="";
            $override_previous_policy="";
            $policy_type_name="";
            //print_r($this->input->post());exit;
            extract($this->input->post());
            
            if($override_previous_policy!="0"){
                $this->delete_policy_internal($override_previous_policy,$policy_type_name);
            }
            
            $added=$this->Policys_manage_data->update_for_policy_cancellation_restriction($policy_uid,$message,$cancellation_restrict,$new_days_for_cancellation,$new_hour_limit,$name_of_products_tagged_for_purchasing,$select_order_status_type);
            if($policy_list_modified==1){
                $added=$this->Policys_manage_data->update_items_tagged_for_policy_restriction($policy_type_id,$policy_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector);
            }
            
            if($added){
                $this->session->set_flashdata('notification','Policy Updated');
                redirect(base_url().'admin/Policys');
            }
        
    }

    

    
    public function create_shipping_charge_for_replacement_policy($id){
        if($this->session->userdata("logged_in")){
            $this->load->model('admin/promotions_manage_data');
            $data['parent_category']=$this->promotions_manage_data->get_all_parent_category();
            $data['payment_refund_attributes']=$this->Policys_manage_data->get_all_payment_policy_attributes($id);
            $data['policy_type']=$id;
            $this->load->view('admin/vmanage');
            $this->load->view('admin/policy/create_shipping_charge_for_replacement_policy',$data);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function add_shipping_charge_for_replacement_restrictions(){
        if($this->session->userdata("logged_in")){
            $products_tagged_for_purchasing="";
            $parent_category="";
            $category="";
            $sub_category="";
            $attributes="";
            $attributes_selector="";
            $refund_restrict="";
            $new_days_for_refund="";
            $message="";
            $policy_type_id="";
            $name_of_products_tagged_for_purchasing="";
            $override_previous_policy="";
            $policy_type_name="";
            //print_r($this->input->post());exit;
            extract($this->input->post());
             
            if($override_previous_policy!="0"){
                echo $policy_type_name;
                if($this->delete_policy_internal($override_previous_policy,$policy_type_name)){
  
                }
            }

            if($replacement_perentage_shipping_charge_by_admin!=""){
                $replacement_perentage_shipping_charge_by_customer=intval(100-$replacement_perentage_shipping_charge_by_admin);
            }
            if($replacement_perentage_shipping_charge_by_customer!=""){
                $replacement_perentage_shipping_charge_by_admin=intval(100-$replacement_perentage_shipping_charge_by_customer);
            }
            $date = new DateTime();
            $mt=microtime();
            $salt="promo_id";
            $rand=mt_rand(1, 1000000);
            $ts=$date->getTimestamp();
            $str=$salt.$rand.$ts.$mt;
            $policy_uid=md5($str);
            $this->Policys_manage_data->details_for_policy_shipping_charge_for_replacement_restriction($policy_uid,$replacement_perentage_shipping_charge_by_customer,$shipping_charge_on,$replacement_perentage_shipping_charge_by_admin,$name_of_products_tagged_for_purchasing);
            $added=$this->Policys_manage_data->add_items_tagged_for_policy_restriction($policy_type_id,$policy_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector);
            if($added){
                $this->session->set_flashdata('notification','Policy Added');
                redirect(base_url().'admin/Policys');
            }
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function shipping_charge_for_replacement_processing(){
      if($this->session->userdata("logged_in")){
        $params = $columns = $totalRecords = $data = array();
    
        $params = $this->input->post();
        
        $totalRecords=$this->Policys_manage_data->shipping_charge_for_replacement_processing($params,"get_total_num_recs");
        
        $queryRecordsResult=$this->Policys_manage_data->shipping_charge_for_replacement_processing($params,"get_recs");
        
        $i=count($queryRecordsResult);
        $i=0;
        foreach($queryRecordsResult as $queryRecordsObj){
            if($queryRecordsObj->active==0){
                $status="Not Active";
            }
            if($queryRecordsObj->active==1){
                $status="Active";
            }
            
            if($queryRecordsObj->timestamp!=""){
                $date_since=date("D,j M Y H:i", strtotime($queryRecordsObj->timestamp));
            }
            else{
                $date_since="";
            }

            
            $row=array();
            $row[]=++$i;
            $row[]="Percentage paid by customer (".$queryRecordsObj->percent_paid_by_customer.")<br> Percentage paid by Admin(".$queryRecordsObj->percent_paid_by_admin.")";
            $row[]='<a href="'.base_url().'admin/Policys/getDetailof_shipping_charge_for_replacement/'.$queryRecordsObj->shipping_charge_on_replacement_policy_uid.'">'.$queryRecordsObj->item_level.'</a>';
            
            $row[]='<button class="btn- btn-danger btn-xs" policy_type="shipping_charge_for_replacement" policy_uid="'.$queryRecordsObj->shipping_charge_on_replacement_policy_uid.'" onclick="delete_policy(this)"><i class="fa fa-trash-o"></i></button>';
            $row[]=$date_since;
            
            $data[]=$row;
            $row=array();
        }
        $json_data = array(
                "draw"            => intval( $params['draw'] ),   
                "recordsTotal"    => intval( $totalRecords ),  
                "recordsFiltered" => intval($totalRecords),
                "data"            => $data   // total data array
                );
        echo json_encode($json_data);  // send data as json format
    
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }

    public function getDetailof_shipping_charge_for_replacement($id){
        if($this->session->userdata("logged_in")){
            $data['policy_data']=$this->Policys_manage_data->getDataOfParticularshipping_charge_for_replacementPolicy($id);
            $this->load->model('admin/promotions_manage_data');
            $data['parent_category']=$this->promotions_manage_data->get_all_parent_category();
            
            $data['policy_items']=$this->Policys_manage_data->getItemsOfParticularPolicy($id);
			if(isset($data['policy_items'][0]['policy_type']))
            $data['payment_refund_attributes']=$this->Policys_manage_data->get_all_payment_policy_attributes($data['policy_items'][0]['policy_type']);
			if(isset($data['policy_items'][0]['policy_type']))
            $data['policy_type']=$data['policy_items'][0]['policy_type'];
            $data['policy_uid']=$id;
            $this->load->view('admin/vmanage');
            $this->load->view('admin/policy/update_shipping_charge_for_replacement_policy',$data);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    
    public function update_shipping_charge_on_replacement(){
            $products_tagged_for_purchasing="";
            $parent_category="";
            $category="";
            $sub_category="";
            $attributes="";
            $attributes_selector="";
            $refund_restrict="";
            $new_days_for_refund="";
            $message="";
            $policy_type_id="";
            $name_of_products_tagged_for_purchasing="";
            $refund_method="";
            $override_previous_policy="";
            $policy_type_name="";
            extract($this->input->post());
            if($replacement_perentage_shipping_charge_by_admin!=""){
                $replacement_perentage_shipping_charge_by_customer=intval(100-$replacement_perentage_shipping_charge_by_admin);
            }
            if($replacement_perentage_shipping_charge_by_customer!=""){
                $replacement_perentage_shipping_charge_by_admin=intval(100-$replacement_perentage_shipping_charge_by_customer);
            }
            if($override_previous_policy!="0"){
                if($this->delete_policy_internal($override_previous_policy,$policy_type_name)){
                }
            }
            
            $added=$this->Policys_manage_data->update_for_policy_shipping_charge_for_replacement_restriction($policy_uid,$replacement_perentage_shipping_charge_by_customer,$shipping_charge_on,$replacement_perentage_shipping_charge_by_admin,$name_of_products_tagged_for_purchasing);
            if($policy_list_modified==1){
                $added=$this->Policys_manage_data->update_items_tagged_for_policy_restriction($policy_type_id,$policy_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector);
            }
            
            if($added){
                $this->session->set_flashdata('notification','Policy Updated');
                redirect(base_url().'admin/Policys');
            }

        
    }
    
    
    
    
    public function promotion_processing(){
        
    } 
    public function create_Exchange_policy($id){
        if($this->session->userdata("logged_in")){
            $this->load->model('admin/promotions_manage_data');
            $data['parent_category']=$this->promotions_manage_data->get_all_parent_category();
            //$data['payment_replacement_attributes']=$this->Policys_manage_data->get_all_payment_policy_attributes($id);
            $data['policy_type']=$id;
            $this->load->view('admin/vmanage');
            $this->load->view('admin/policy/create_exchange_policy',$data);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    
    public function add_exchange_policy(){
        if($this->session->userdata("logged_in")){
            $products_tagged_for_purchasing="";
            $parent_category="";
            $category="";
            $sub_category="";
            $attributes="";
            $attributes_selector="";
            
            $name_of_products_tagged_for_purchasing="";
            $policy_type_id="";
            $message_ui="";
            $exchange_value_min="";
            $exchange_value_max="";
            $exchange_value_currency="";
            $message_t_c="";
            $content="";
            $location="";
            $shipping="";
            $message_disclaimer="";
            $exchange_start_date="";
            $exchange_end_date="";
            
            $override_previous_policy="";
            $policy_type_name="";
            //print_r($this->input->post());exit;
            extract($this->input->post());
            
            if($override_previous_policy!="0"){
                $this->delete_policy_internal($override_previous_policy,$policy_type_name);
            }
            
            $date = new DateTime();
            $mt=microtime();
            $salt="promo_id";
            $rand=mt_rand(1, 1000000);
            $ts=$date->getTimestamp();
            $str=$salt.$rand.$ts.$mt;
            $policy_uid=md5($str);
            
            
            $this->Policys_manage_data->details_for_policy_exchange($policy_uid,$message_ui,$exchange_value_min,$exchange_value_max,$name_of_products_tagged_for_purchasing,$exchange_value_currency,$message_t_c,$content,$location,$shipping,$message_disclaimer,$exchange_start_date,$exchange_end_date);
            $added=$this->Policys_manage_data->add_items_tagged_for_policy_restriction($policy_type_id,$policy_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector);
            if($added){
                $this->session->set_flashdata('notification','SKUs Added. Proceed to Step 2');
                redirect(base_url().'admin/Policys/getDetailofExchangePolicy/'.$policy_uid);
            }
         }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    public function exchange_processing(){
    if($this->session->userdata("logged_in")){
        $params = $columns = $totalRecords = $data = array();
    
        $params = $this->input->post();
        
        $totalRecords=$this->Policys_manage_data->Exchange_processing($params,"get_total_num_recs");
        
        $queryRecordsResult=$this->Policys_manage_data->Exchange_processing($params,"get_recs");
        
        $i=count($queryRecordsResult);
        $i=0;
        foreach($queryRecordsResult as $queryRecordsObj){
            if($queryRecordsObj->active==0){
                $status="Not Active";
            }
            if($queryRecordsObj->active==1){
                $status="Active";
            }
            if($queryRecordsObj->exchange_value_currency!=""){
                $ex_curr_val=$queryRecordsObj->exchange_value_currency;
                //$exchange_parameters.='<br>Currency Type: '.. ' ';
            }
            $exchange_parameters="";
            if($queryRecordsObj->exchange_value_min!=""){
                $ex_min_val=$queryRecordsObj->exchange_value_min;
                $exchange_parameters.='<small>Min Exchange value: '.$ex_min_val .' '.$ex_curr_val;
            }
            if($queryRecordsObj->exchange_value_max!=""){
                $ex_max_val=$queryRecordsObj->exchange_value_max;
                $exchange_parameters.='<br> Max Exchange value: '.$ex_max_val .' '.$ex_curr_val;
            }
               
            if($queryRecordsObj->location!=""){
                $location=$queryRecordsObj->location;
                $exchange_parameters.='<br> Location: '.$location;
            }
            if($queryRecordsObj->exchange_start_date!=""){
                $exchange_parameters.='<br> Start Date: '.date("D,j M Y H:i", strtotime($queryRecordsObj->exchange_start_date));
            }
            if($queryRecordsObj->exchange_end_date!=""){
                $exchange_parameters.='<br> End Date: '.date("D,j M Y H:i", strtotime($queryRecordsObj->exchange_end_date)). ' </small>';
            }
            if($queryRecordsObj->timestamp!=""){
                $date_since=date("D,j M Y H:i", strtotime($queryRecordsObj->timestamp));
            }
            else{
                $date_since="";
            }
            $row=array();
            $row[]=++$i;
            $row[]=$exchange_parameters;
            $row[]='<a href="'.base_url().'admin/Policys/getDetailofExchangePolicy/'.$queryRecordsObj->exchange_policy_uid.'">'.$queryRecordsObj->item_level.'</a>';
            $row[]='<button class="btn- btn-danger btn-xs" policy_type="Exchange" policy_uid="'.$queryRecordsObj->exchange_policy_uid.'" onclick="delete_policy(this)"><i class="fa fa-trash-o"></i></button>';
            $row[]=$date_since;
            
            $data[]=$row;
            $row=array();
        }
        $json_data = array(
                "draw"            => intval( $params['draw'] ),   
                "recordsTotal"    => intval( $totalRecords ),  
                "recordsFiltered" => intval($totalRecords),
                "data"            => $data   // total data array
                );
        echo json_encode($json_data);  // send data as json format
    
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }

    public function getDetailofExchangePolicy($id){
        if($this->session->userdata("logged_in")){
            $data['policy_data']=$this->Policys_manage_data->getDataOfParticularExchangePolicy($id);
            $this->load->model('admin/promotions_manage_data');
            $data['parent_category']=$this->promotions_manage_data->get_all_parent_category();
            
            $data['policy_items']=$this->Policys_manage_data->getItemsOfParticularPolicy($id);
            //$data['payment_policy_attributes']=$this->Policys_manage_data->get_all_payment_policy_attributes($data['policy_items'][0]['policy_type']);
			if(isset($data['policy_type'])){
            $data['policy_type']=$data['policy_items'][0]['policy_type'];
			}else{
				$data['policy_type']='';
			}
            $data['policy_uid']=$id;
            $this->load->view('admin/vmanage');
            $this->load->view('admin/policy/update_exchange_policy',$data);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
    
    public function update_exchange_restrictions(){
            $products_tagged_for_purchasing="";
            $parent_category="";
            $category="";
            $sub_category="";
            $attributes="";
            $attributes_selector="";
            
            $name_of_products_tagged_for_purchasing="";
            $policy_type_id="";
            $message_ui="";
            $exchange_value_min="";
            $exchange_value_max="";
            $exchange_value_currency="";
            $message_t_c="";
            $content="";
            $location="";
            $shipping="";
            $message_disclaimer="";
            $exchange_start_date="";
            $exchange_end_date="";
        
        
            $policy_uid="";
            $policy_type_id="";
            $policy_list_modified="";

            $override_previous_policy="";
            $policy_type_name="";
            
            
			/////////////added//////////////////

			//print_r($_FILES);
			//print_r($this->input->post());exit;
			
            extract($this->input->post());
			
			$this->load->library('upload');
			$files = $_FILES;
			$img_name=$_FILES['image']['name'];
			if($img_name!=''){
        
				$filename=$files['image']['name'];  
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				$filen=rand(10,100000);
				
				$_FILES['image']['name']=$filen.".".$ext;
				$_FILES['image']['type']= $files['image']['type'];
				$_FILES['image']['tmp_name']= $files['image']['tmp_name'];
				$_FILES['image']['error']= $files['image']['error'];
				$_FILES['image']['size']= $files['image']['size'];  
				
				
				$this->upload->initialize($this->set_upload_options());

				if (!$this -> upload -> do_upload("image")) {
				echo "something went wrong, display errors" . $this -> upload -> display_errors();
				} else {
					$upload_data = $this -> upload -> data();
					$image = "assets/pictures/images/products/" . $upload_data['file_name'];
				}
			}

			if($cpt=0){
				$image="";
			}
			if(isset($pincodes_applicable)){
				$pincodes_applicable=1;
			}else{
				$pincodes_applicable=0;
			}
			
			/////////////added//////////////////
			
            if($override_previous_policy!="0"){
                $this->delete_policy_internal($override_previous_policy,$policy_type_name);
            }
            $added=$this->Policys_manage_data->update_for_policy_exchange_restriction($policy_uid,$message_ui,$exchange_value_min,$exchange_value_max,$name_of_products_tagged_for_purchasing,$exchange_value_currency,$message_t_c,$content,$location,$shipping,$message_disclaimer,$exchange_start_date,$exchange_end_date,$image,$pickup_charge,$pincodes_applicable);
			
            if($policy_list_modified==1){
                $added=$this->Policys_manage_data->update_items_tagged_for_policy_restriction($policy_type_id,$policy_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector);
            }
            
            if($added){
                $this->session->set_flashdata('notification','Policy Updated');
                redirect(base_url().'admin/Policys');
            }
        
    } 
	private function set_upload_options(){   
		//upload an image options
		$config = array();
		$config['upload_path'] = './assets/pictures/images/products/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '2048';
		$config['max_width'] = '1024';
		$config['max_height'] = '800';
		$config['overwrite']     = FALSE;

		return $config;
	}	
    public function getExisting_policy(){
        if($this->session->userdata("logged_in")){
            $selected_attr=$this->input->post('selected_attr');
            $selected_level=$this->input->post('selected_level');
            $policy_type_id=$this->input->post('policy_type_id');
            $policy_type_name=$this->input->post('policy_type_name');
            $products_tagged_for_purchasing="";
            $sql_check=$this->Policys_manage_data->getExisting_policy($selected_attr,$selected_level,$policy_type_id);
                if($sql_check!=""){
                    echo $sql_check;
                }
                else{
                    echo 0;
                }
            }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
}
	 
	
