<?php
class Adminlog extends CI_controller{
    function __construct(){
		parent::__construct();
		$this->load->model('admin/Admin');	
		$this->load->helper('emailsmstemplate');		
	}
	public function index(){
		if($this->session->userdata("logged_in")){
			redirect(base_url()."admin/Admin_manage");
			
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
   
	public function admin_check_user(){	
		if(isset($_REQUEST)){
			$data = json_decode(file_get_contents("php://input"));
			if(isset($data->val)){
				$email=$data->val;
				$data=$this->Admin->admin_verify_user($email);
			}
			else{
				return null;
			}
		}
	}
		
	public function admin_login(){
		if(isset($_REQUEST)){
			$data = json_decode(file_get_contents("php://input"));
			if(isset($data->password)){
				$un=$data->username;
				$pw=$data->password;
				$data=$this->Admin->admin_login($un,$pw);
			}
			else{
				return null;
			}
		}
	}
}
?>