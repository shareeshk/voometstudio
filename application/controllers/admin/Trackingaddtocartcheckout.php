<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Trackingaddtocartcheckout extends CI_Controller {
	function __construct()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model('admin/Model_trackingaddtocartcheckout');
        $this->load->library('My_PHPMailer');
		$this->load->helper('emailsmstemplate');
		$this->load->helper('general');
	}
	public function index(){
        if($this->session->userdata("logged_in")){
			$data["menu_flag"]="";
			$data["total_records_in_tracking_temp"]=$this->Model_trackingaddtocartcheckout->total_records_in_tracking_temp();
            $this->load->view('admin/vmanage',$data);
            $this->load->view('admin/trackingaddtocartcheckout',$data);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
	
	public function trackingaddtocartcheckout_processing(){
        if($this->session->userdata("logged_in")){
            $params = $columns = $totalRecords = $data = array();
            
            $params = $this->input->post();
            
			//$totalRecords=$this->Model_trackingaddtocartcheckout->trackingaddtocartcheckout_processing($params,"get_total_num_recs_initial");
			
            $totalRecords=$this->Model_trackingaddtocartcheckout->trackingaddtocartcheckout_processing($params,"get_total_num_recs");
            
            $queryRecordsResult=$this->Model_trackingaddtocartcheckout->trackingaddtocartcheckout_processing($params,"get_recs");
            
            $i=1;
            foreach($queryRecordsResult as $queryRecordsObj){
                $row=array();
                $row[]=$i;
                $row[]=$queryRecordsObj->inventory_sku;
                $row[]=$queryRecordsObj->inventory_moq;
                $row[]=$queryRecordsObj->customer_name;
				$row[]=$queryRecordsObj->page_name;
                $row[]=$queryRecordsObj->tracking_temp_timestamp_format;
                $data[]=$row;
                $row=array();
                $i++;
            }
            $json_data = array(
                    "draw"            => intval( $params['draw'] ),  
					//"recordsTotalInitial"    => intval( $totalRecordsInitial ),  
                    "recordsTotal"    => intval( $totalRecords ),  
                    "recordsFiltered" => intval($totalRecords),
                    "data"            => $data   // total data array
                    );
            echo json_encode($json_data);  // send data as json format
            
        }else{
            $this->logged_out();
        }   
    }
	
    
}
	 
	
