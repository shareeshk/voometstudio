<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Complaint_manage extends CI_Controller {
        private $pagesize = 20;
	function __construct()
	{
		parent::__construct();
        $this->load->database();
	//	$this->load->model('custmodel_manage',"m");
		$this->load->model('admin/Complaint_manage_data');
		$this->load->helper('emailsmstemplate');
	}
	public function index()
	{
		
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["menu_flag"]="";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/complaint_history',$data);
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function all_complaint_processing(){
	$params = $columns = $totalRecords = $data = array();

	$params = $this->input->post();
	
	$totalRecords=$this->Complaint_manage_data->all_complaint_processing($params,"get_total_num_recs");
	
	$queryRecordsResult=$this->Complaint_manage_data->all_complaint_processing($params,"get_recs");
	
	$i=count($queryRecordsResult);
	foreach($queryRecordsResult as $queryRecordsObj){
		$row=array();
		
		$row[]=date("D, jS M'y",strtotime($queryRecordsObj->timestamp));
		$row[]='<a href="'.base_url().'admin/Complaint_manage/complaint_thread/'.$queryRecordsObj->complaint_thread_id.'">'.$queryRecordsObj->subjects.'</a>';
		$row[]='<a href="'.base_url().'admin/Complaint_manage/complaint_thread/'.$queryRecordsObj->complaint_thread_id.'">'.$queryRecordsObj->id.'</a>';
		$row[]=ucfirst($queryRecordsObj->regarding);
		$row[]=ucfirst($queryRecordsObj->severity);
		$row[]=$queryRecordsObj->close_case_text;
		$data[]=$row;
		$row=array();
	}
	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);
	echo json_encode($json_data);  // send data as json format
	
}

/*function for complaint_thread start*/

public function complaint_thread($id){
	if($this->session->userdata("logged_in")){
		$data["controller"]=$this;
		$data["case_id"]=$id;
		$complaint_thread_id=$id;
		$data["controller"]=$this;
		$data["master_complaint_data"]=$this->Complaint_manage_data->get_master_data_of_complaints($complaint_thread_id);
		$this->load->view('admin/vmanage');
		$this->load->view('admin/complaint_history_thread',$data);
	}
	else{
		redirect(base_url());
	}
}

public function get_complaint_case_present_status($id){
	if($id==0){
		return "Pending Admin Action";
	}
	if($id==1){
		return "Pending Customer Action";
	}
	if($id==2){
		return "Resolved By Customer";
	}
	if($id==3){
		return "Resolved By Admin";
	}
	if($id==4){
		return "Disputed";
	}
}

public function get_complaint_case_user_fullname($id){
	return $this->Complaint_manage_data->get_complaint_case_user_fullname($id);
}

public function change_complaint_case_status(){
if($this->session->userdata("logged_in")){
	$case_id=$this->input->post('case_id');
	$action=$this->input->post('action');
	$success=$this->Complaint_manage_data->change_complaint_case_status($case_id,$action);
	if($success){
		echo "1";
	}
	else{
		echo "0";
	}
	}
	else{
		redirect(base_url());
	}	
}

public function get_all_country_code(){
	$this->load->model('customer_account');
	echo json_encode($this->customer_account->get_all_country_code());
}


public function add_conversation_to_complaint(){
	extract($_POST);

	//generate a $thread_id
	$date = new DateTime();
	$mt=microtime();
	$salt="thread_id";
	$rand=mt_rand(1, 1000000);
	$ts=$date->getTimestamp();
	$str=$salt.$rand.$ts.$mt;
	$thread_id=md5($str);
	$complaint_thread_id=$case_id;
	$user_type=$this->session->userdata('user_type');
	$customer_id="";

	if(!empty($_FILES["filesToUpload"]["name"][0])){
			if (!file_exists($folder="assets/complaints_attachment")) {
							$mask=umask(0);
							mkdir($folder,0777);
							umask($mask);
						}
			
					if (!file_exists($folder=$folder.'/'.$complaint_thread_id)) {
							$mask=umask(0);
							mkdir($folder,0777);
							umask($mask);
						}
					if (!file_exists($folder=$folder.'/'.$thread_id)) {
							$mask=umask(0);
							mkdir($folder,0777);
							umask($mask);
						}
					
					for($i=0;$i<count($_FILES["filesToUpload"]["name"]);$i++){
						$mask=umask(0);
						mkdir($folder.'/'.$i,0777);
						umask($mask);
						$sourcePath=$_FILES["filesToUpload"]["tmp_name"][$i];
						$targetPath=$folder.'/'.$i."/".$_FILES["filesToUpload"]["name"][$i];
						$file_path=$targetPath;
						if(move_uploaded_file($sourcePath,$targetPath)){						
							$this->Complaint_manage_data->save_complaint_attachment($file_path,$thread_id,$complaint_thread_id,$customer_id);
						}
					}
				}
	$action=1;
		$this->Complaint_manage_data->change_complaint_case_status($complaint_thread_id,$action);
		$this->Complaint_manage_data->add_thread_to_complaints($complaint_thread_id,$thread_id,$user_type,$customer_id,$discription,$contact_method,$country_code,$phone_number,$ext_number);
}

public function get_all_thread_complaint(){
	$complaint_id=$this->input->post('complaint_id');
	$all_complaint_data=$this->Complaint_manage_data->get_all_thread_complaint($complaint_id);
	if(!empty($all_complaint_data)){
		$threadscontainer="";
		foreach($all_complaint_data as $complaint_data){
			
			$timestamp = $complaint_data['timestamp'];
			$splitTimeStamp = explode(" ",$timestamp);
			$date = $splitTimeStamp[0];
			$time = $splitTimeStamp[1];
			$date = date('M d, Y',strtotime($timestamp));
			$time = date('H:i A',strtotime($timestamp));
			
			$threadscontainer.='<div class="row">';
			
			
			if($complaint_data['created_by']!="customer"){
				
				
				$threadscontainer.='<div class="col-sm-4 text-right">
		            				<p><b>'.ucfirst($complaint_data['created_by']).'</b></p>
		            				<p>'.$date.'</p><p>'.$time.'</p>';
					if($complaint_data['contact_method']=="web"){
						 $threadscontainer.='<p><i class="fa fa-desktop fa-2x" aria-hidden="true"></i></p>';
					}
					if($complaint_data['contact_method']=="phone"){
						$threadscontainer.='<p><i class="fa fa-phone fa-2x" aria-hidden="true"></i></p>
											<p>Phone No. :+'.$complaint_data['phone_country'].' '.$complaint_data['phone_number'].'</p>';
							if(!empty($complaint_data['phone_extension'])){
								$threadscontainer.='<p>'.$complaint_data['phone_extension'].'</p>';
							}
											
						
						
					}
		          
		            $threadscontainer.='</div>';
				$checked1="";$checked2="";$checked3="";$checked4="";$checked5="";
				$choiceVal="";
				if($complaint_data['feed_back_rating']==1){
					$checked1="checked";
					$choiceVal="Poor";
				}
				if($complaint_data['feed_back_rating']==2){
					$checked2="checked";
					$choiceVal="Below Average";
				}
				if($complaint_data['feed_back_rating']==3){
					$checked3="checked";
					$choiceVal="Average";
				}
				if($complaint_data['feed_back_rating']==4){
					$checked4="checked";
					$choiceVal="Very Good";
				}
				if($complaint_data['feed_back_rating']==5){
					$checked5="checked";
					$choiceVal="Excellent";
				}
				
				$threadscontainer.='<div class="col-sm-8 well" style="background-color: #eee;">';
				
				if(strlen($complaint_data['descriptions'])>200){
					$threadscontainer.='<div class="col-sm-12">Descriptions:<div class="f-row margin-top more">'.str_split($complaint_data['descriptions'],200)[0].'<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' .substr($complaint_data['descriptions'],201). '</span>&nbsp;&nbsp;<a href="#" class="morelink" onclick="morelinkfun(this)">more</a></span></div></div>';
				}else{
					$threadscontainer.='<div>Descriptions:<div class="f-row margin-top more">'.$complaint_data['descriptions'].'</div></div>';
				}
				
				$threadscontainer.='<div class="col-sm-12">'.$this->get_all_attachment_of_thread_id($complaint_data['thread_id']).'</div>
	            			<div class="row">
	            			<div class="col-sm-12">
	            			<br>	
	            			<p>Thank you for using Business Division</p>
	            			<br>
	            			<p>Best regards,</p>
	            			<br>
	            			<p>Admin.</p>
	            			<p>Flamingo</p>
	            			<p>We value your feedback. Please rate my response using the link below.</p>
	            			<br>
	            			<div class="elem"></div>
	            			</div>	
	            			</div>
	            			<div class="row">
	            				<br><br>
	            				<div class="col-sm-12 well">
	            					<p>Was this response helpful? Click here to rate:</p>
	            					<div class="col-sm-6 star-rating">
									  <input disabled type="radio" '.$checked1.'  thread_id="'.$complaint_data['thread_id'].'" name="'.$complaint_data['thread_id'].'" humaVal="Poor" value="1"><i></i>
									  <input disabled type="radio" '.$checked2.'  thread_id="'.$complaint_data['thread_id'].'" name="'.$complaint_data['thread_id'].'" humaVal="Below Average" value="2"><i></i>
									  <input disabled type="radio" '.$checked3.'  thread_id="'.$complaint_data['thread_id'].'" name="'.$complaint_data['thread_id'].'" humaVal="Average" value="3"><i></i>
									  <input disabled type="radio" '.$checked4.'  thread_id="'.$complaint_data['thread_id'].'" name="'.$complaint_data['thread_id'].'" humaVal="Very Good " value="4"><i></i>
									  <input disabled type="radio" '.$checked5.'  thread_id="'.$complaint_data['thread_id'].'" name="'.$complaint_data['thread_id'].'" humaVal="Excellent" value="5"><i></i>
									</div>
									<div class="col-sm-6 choice" id="selectedStarVal">'.$choiceVal.'</div>
	            				</div>	
	            			</div>	
            			</div>';
				
			}
			else{
				$threadscontainer.='<div class="col-sm-4 text-right">
		            				<p><b>'.$this->get_complaint_case_user_fullname($complaint_data['user_id']).'</b></p>
		            				<p>'.$date.'</p><p>'.$time.' +0530</p>';
					if($complaint_data['contact_method']=="web"){
						 $threadscontainer.='<p><i class="fa fa-desktop fa-2x" aria-hidden="true"></i></p>';
					}
					else{
						$threadscontainer.='<p><i class="fa fa-phone fa-2x" aria-hidden="true"></i></p>
											<p>Phone No. :+'.$complaint_data['phone_country'].'-'.$complaint_data['phone_number'];
							if(!empty($complaint_data['phone_extension'])){
								$threadscontainer.='-'.$complaint_data['phone_extension'];
							}
											
					$threadscontainer.='</p>';
						
					}
		          
		            $threadscontainer.='</div>';
				$threadscontainer.='<div class="col-sm-8 well" style="background-color: #eee;">';
				$complaint_data['descriptions']=stripslashes($complaint_data['descriptions']);
				
				if(strlen($complaint_data['descriptions'])>200){
					$threadscontainer.='<div class="col-sm-12">Descriptions:<div class="f-row margin-top more">'.str_split($complaint_data['descriptions'],200)[0].'<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' .substr($complaint_data['descriptions'],201). '</span>&nbsp;&nbsp;<a href="#" class="morelink" onclick="morelinkfun(this)">more</a></span></div></div>';
				}else{
					$threadscontainer.='<div>Descriptions:<div class="f-row margin-top more">'.$complaint_data['descriptions'].'</div></div>';
				}
				
				$threadscontainer.='<div class="col-sm-12">'.$this->get_all_attachment_of_thread_id($complaint_data['thread_id']).'</div></div>';
			}					
							
			
			$threadscontainer.='</div><hr>';
		}	
		
		echo $threadscontainer;	
	}
	else{
		echo "No Data To Show";
	}
}

public function get_all_attachment_of_thread_id($id){
	$all_attachments=$this->Complaint_manage_data->get_all_attachment_of_thread_id($id);
	
	$str="";
	if(!empty($all_attachments)){
		$str.='<hr>';
		$str.='<p>Attachments</p>';
		foreach($all_attachments as $data){
			$str.='<p><a href="'.base_url().$data['file_path'].'" download>'.basename($data['file_path']).'</a></p>';
		}
	}
	return $str;
}
		
}
	 
	
