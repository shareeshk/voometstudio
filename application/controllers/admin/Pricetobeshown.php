<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pricetobeshown extends CI_Controller {
	function __construct()
	{
		parent::__construct();
        $this->load->database();
		$this->load->model('admin/Model_pricetobeshown');
        $this->load->library('My_PHPMailer');
		$this->load->helper('emailsmstemplate');
		$this->load->helper('general');
	}
	public function index(){
        if($this->session->userdata("logged_in")){
			$data["get_pricetobeshown"]=get_pricetobeshown();
			$data["menu_flag"]="";
            $this->load->view('admin/vmanage',$data);
            $this->load->view('admin/pricetobeshown',$data);
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
	public function add_pricetobeshown(){
		$pricetobeshown=$_REQUEST["price_to_be_shown"];
		$flag=$this->Model_pricetobeshown->add_pricetobeshown($pricetobeshown);
		echo $flag;
	}
    
}
	 
	
