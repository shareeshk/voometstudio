<?php 

class Ratemanaging extends CI_Controller{
	
	function __construct(){
		parent::__construct(); 
		$this->load->model('admin/Model_rate');
		$this->load->helper('emailsmstemplate');
	}
	
	public function rated_inventory(){
		if($this->session->userdata("logged_in")){
			
			$inventory_id=$this->input->post("inventory");
			$data['product_id']=$this->input->post("products");
			$data['brand_id']=$this->input->post("brands");
			$data['subcat_id']=$this->input->post("subcategories");
			$data['cat_id']=$this->input->post("categories");
			$data['pcat_id']=$this->input->post("parent_category");
			$data['inventory_id']=$inventory_id;
			$data["controller"]=$this;
			$data["menu_flag"]="rated_inventory";
			
			if(!empty($inventory_id)){
			
				$this->load->view('admin/vmanage',$data);
				$this->load->view('admin/ratings_and_reviews_admin',$data);
			}else{
				$this->rated_inventory_filter();		
			}
		}else{
			$data['title']= 'Home';
			$this->load->view('admin/login',$data);
		}
	}
	
	public function rated_inventory_processing(){
		
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_rate->rated_inventory_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_rate->rated_inventory_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$flag=$queryRecordsObj->status;
				$row=array();
				$row[]='<input type="checkbox" name="selected_list_'.$flag.'" value="'.$queryRecordsObj->id.'">';
				$row[]='<a href="'.base_url().'admin/Manage_cust/view/'.$queryRecordsObj->customer_id.'">'.$queryRecordsObj->customer_name.'</a>';
				$row[]=$queryRecordsObj->product_name;
				$expression = $queryRecordsObj->rating;
				$rate='';
				switch ($expression) {
					case '1':
						$rate.='<img src="' . base_url().'assets/smileys_email/0.png" height="30px" weight="30px">';
						break;
					case '2':
						$rate.='<img src="' . base_url().'assets/smileys_email/1.png" height="30px" weight="30px">';
						break;
					case '3':
						$rate.='<img src="' . base_url().'assets/smileys_email/2.png" height="30px" weight="30px">';
						break;
					case '4':
						$rate.='<img src="' . base_url().'assets/smileys_email/3.png" height="30px" weight="30px">';
						break;
					case '5':
						$rate.='<img src="' . base_url().'assets/smileys_email/4.png" height="30px" weight="30px">';
						break;
					default:
						
				}
				switch ($expression) {
					case '1':
							$rate.='<span style="color:red">&#9733;</span>&#9734;&#9734;&#9734;&#9734';
						break;
					case '2':						
							$rate.='<span style="color:red">&#9733;&#9733;</span>&#9734;&#9734;&#9734';
						break;
					case '3':			
							$rate.='<span style="color:red">&#9733;&#9733;&#9733;</span>&#9734;&#9734';
						
						break;
					case '4':
							$rate.='<span style="color:red">&#9733;&#9733;&#9733;&#9733;</span>&#9734';			
						break;
					case '5':						
							$rate.='<span style="color:red">&#9733;&#9733;&#9733;&#9733;&#9733;</span>';						
						break;
					default:
						
				}
				$row[]=$rate;
				$row[]=$queryRecordsObj->review;
				$row[]=$queryRecordsObj->posteddate;		

				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data   // total data array
						);
			echo json_encode($json_data);  // send data as json format
		
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	
	}

	function allow_rated_inventory(){
		$selected_list=$this->input->post("selected_list");
		$flag=$this->Model_rate->allow_rated_inventory($selected_list);	
		echo $flag;
	}
	function notallow_rated_inventory(){
		$selected_list=$this->input->post("selected_list");
		$flag=$this->Model_rate->notallow_rated_inventory($selected_list);
		echo $flag;
	}
	public function rated_inventory_filter(){
		
		if($this->session->userdata("logged_in")){		
			
			$data['product_id']=$this->input->post("product_id");
			$data['brand_id']=$this->input->post("brand_id");
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['inventory_id']=$this->input->post("inventory_id");
			
			$data['parent_category'] = $this->Model_rate->parent_category();
			$data["menu_flag"]="rated_inventory";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/ratings_and_reviews_filter',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_categories_for_shop_by_parentcategory(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("pcat_id");
			$result_cat=$this->Model_rate->show_available_categories($pcat_id);
			if(count($result_cat)!=0){
				?>
				<?php
				foreach($result_cat as $res){
				?>
					<option value="<?php echo $res->cat_id;?>"><?php echo $res->cat_name;?></option>
					<?php
				}
			}
			else{
				echo count($result_cat);
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_categories(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("pcat_id");
			$result_cat=$this->Model_rate->show_available_categories($pcat_id);
			if(count($result_cat)!=0){
				?>
				<option value="">Select Category</option>
				<?php
				foreach($result_cat as $res){
				?>
					<option value="<?php echo $res->cat_id;?>"><?php echo $res->cat_name;?></option>
					<?php
				}
			}
			else{
				echo count($result_cat);
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_subcategories(){
		if($this->session->userdata("logged_in")){
			$cat_id=$this->input->post("cat_id");
			$result_subcat=$this->Model_rate->show_available_subcategories($cat_id);
			if(count($result_subcat)!=0){
				?>
				<option value="">Select Sub Category</option>
				<?php
				foreach($result_subcat as $res){
				?>
					<option value="<?php echo $res->subcat_id;?>"><?php echo $res->subcat_name;?></option>
					<?php
				}
			}
			else{
				echo count($result_subcat);
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_brands(){
		if($this->session->userdata("logged_in")){	
			$subcat_id=$this->input->post("subcat_id");
			$result_brand=$this->Model_rate->show_available_brands($subcat_id);
			if(count($result_brand)!=0){
				?>
				<option value="">Select Brand</option>
				<?php
				foreach($result_brand as $res){
				?>
					<option value="<?php echo $res->brand_id;?>"><?php echo $res->brand_name;?></option>
					<?php
				}
			}
			else{
				echo count($result_brand);
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_products(){
		if($this->session->userdata("logged_in")){	
			$brand_id=$this->input->post("brand_id");
			$result_product=$this->Model_rate->show_available_products($brand_id);
			if(count($result_product)!=0){
				?>
				<option value="">Select Product</option>
				<?php
				foreach($result_product as $res){
				?>
					<option value="<?php echo $res->product_id;?>"><?php echo $res->product_name;?></option>
					<?php
				}
			}
			else{
				echo count($result_product);
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_inventory(){
		if($this->session->userdata("logged_in")){	
			$product_id=$this->input->post("product_id");
			$result_inventory=$this->Model_rate->show_available_inventory($product_id);
			if(count($result_inventory)!=0){
				?>
				<option value="">Select Inventory</option>
				<?php
				foreach($result_inventory as $res){
				?>
					<option value="<?php echo $res->id;?>"><?php echo $res->sku_id;?></option>
					<?php
				}
			}
			else{
				echo count($result_inventory);
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function ratings_for_service(){
		if($this->session->userdata("logged_in")){	
			
			$inventory_id=$this->input->post('inventory_id');
			$inventory_id=$this->input->post("inventory");
			$data['product_id']=$this->input->post("products");
			$data['brand_id']=$this->input->post("brands");
			$data['subcat_id']=$this->input->post("subcategories");
			$data['cat_id']=$this->input->post("categories");
			$data['pcat_id']=$this->input->post("parent_category");
			
			$data['rating']=$this->input->post("rating");
			$data['from_date']=$this->input->post("from_date");
			$data['to_date']=$this->input->post("to_date");
			$data['delivered_date_from']=$this->input->post("delivered_date_from");
			$data['delivered_date_to']=$this->input->post("delivered_date_to");		
			
			$data['inventory_id']=$inventory_id;
			
			$data["controller"]=$this;
			$data['parent_category'] = $this->Model_rate->parent_category();
			$data["menu_flag"]="ratings_for_service";
			$this->load->view('admin/vmanage',$data);

			if(!empty($data['subcat_id'])){
				$this->load->view('admin/vmanage',$data);
				$this->load->view('admin/rateorder_view',$data);
			}else{
				$this->rateorder_filter();		
			}
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function rateorder_filter(){
		
		if($this->session->userdata("logged_in")){		
			
			$data['product_id']=$this->input->post("product_id");
			$data['brand_id']=$this->input->post("brand_id");
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['rating']=$this->input->post("rating");
			$data['from_date']=$this->input->post("from_date");
			$data['to_date']=$this->input->post("to_date");
			$data['delivered_date_from']=$this->input->post("delivered_date_from");
			$data['delivered_date_to']=$this->input->post("delivered_date_to");	
			
			$data['inventory_id']=$this->input->post("inventory_id");
			
			$data['parent_category'] = $this->Model_rate->parent_category();
			$data["menu_flag"]="ratings_for_service";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/rateorder_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function ratings_for_service_processing(){
		if($this->session->userdata("logged_in")){			
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_rate->ratings_for_service_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_rate->ratings_for_service_processing($params,"get_recs");
	
			foreach($queryRecordsResult as $queryRecordsObj){
					
				$row=array();
				$row[]='<a href="'.base_url().'admin/Manage_cust/view/'.$queryRecordsObj->customer_id.'">'.$queryRecordsObj->original_customer.'</a><br>Shipping Customer:&nbsp;'.$queryRecordsObj->customer_name;
				$pro_details='';
				$pro_details.='Order Item Id:&nbsp;'.$queryRecordsObj->order_item_id.'<br>';	
				$pro_details.='Sku Id:&nbsp;'.$queryRecordsObj->sku_id.'<br>';
				$pro_details.='Product Name:&nbsp;'.$queryRecordsObj->product_name.'<br>';
				//$pro_details.=$queryRecordsObj->inventory_id;
				$row[]=$pro_details;
				
				$dt = new DateTime($queryRecordsObj->order_delivered_timestamp);

				$date = $dt->format('d-m-Y');
				$time = $dt->format('H:i:s');

				$row[]=$date. ' | '. $time;

				$expression = $queryRecordsObj->rating;
				$rate='';
				switch ($expression) {
					case '1':
						$rate.='<img src="' . base_url().'assets/smileys_email/0.png" height="30px" weight="30px">';
						break;
					case '2':
						$rate.='<img src="' . base_url().'assets/smileys_email/1.png" height="30px" weight="30px">';
						break;
					case '3':
						$rate.='<img src="' . base_url().'assets/smileys_email/2.png" height="30px" weight="30px">';
						break;
					case '4':
						$rate.='<img src="' . base_url().'assets/smileys_email/3.png" height="30px" weight="30px">';
						break;
					case '5':
						$rate.='<img src="' . base_url().'assets/smileys_email/4.png" height="30px" weight="30px">';
						break;
					default:
						
				}
				switch ($expression) {
					case '1':
							$rate.='<span style="color:red">&#9733;</span>&#9734;&#9734;&#9734;&#9734';
						break;
					case '2':						
							$rate.='<span style="color:red">&#9733;&#9733;</span>&#9734;&#9734;&#9734';
						break;
					case '3':			
							$rate.='<span style="color:red">&#9733;&#9733;&#9733;</span>&#9734;&#9734';
						
						break;
					case '4':
							$rate.='<span style="color:red">&#9733;&#9733;&#9733;&#9733;</span>&#9734';			
						break;
					case '5':						
							$rate.='<span style="color:red">&#9733;&#9733;&#9733;&#9733;&#9733;</span>';						
						break;
					default:
						
				}
				$row[]=$rate;
				
				$logist_details='';
				$logist_details.='<span> Vendor Name:&nbsp; '.$queryRecordsObj->name.'</span><br>';
				$logist_details.='<span> Logistics Name:&nbsp; '.$queryRecordsObj->logistics_name.'</span>';
				$row[]=$logist_details;	
				$message=addslashes($queryRecordsObj->message);
				$actions="<input type=\"button\" value=\"Show Survey Result\" class=\"btn btn-md btn-info\" data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"showSurveyFun('$queryRecordsObj->rateorder_id','$queryRecordsObj->inventory_id','$message')\">";	
				$row[]=$actions;	
	
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data   // total data array
						);
			echo json_encode($json_data);  // send data as json format
				
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_survey_result(){
			if($this->session->userdata("logged_in")){		
				$data["controller"]=$this;
				$rateorder_id=$this->input->post("rateorder_id");
				$inventory_id=$this->input->post("inventory_id");
				$str='';
				$show_survey_resultObj=$this->Model_rate->show_survey_result($rateorder_id);
				$show_rateorder_resultObj=$this->Model_rate->show_rateorder_result($rateorder_id);
				if(!empty($show_survey_resultObj) || !empty($show_rateorder_resultObj)){
				
				$str.='<table class="table table-responsive table-hover table-bordered">';
				if(!empty($show_survey_resultObj)){
				$str.='<tr><th>Question</th><th>Response</th></tr>';
				
					foreach($show_survey_resultObj as $survey_resultObj){
						
							$str.='<tr><td>'.$survey_resultObj->question.'</td><td>';
									
							$rating_level=$survey_resultObj->rating_level;
							switch ($rating_level) {
							case '1':
								 $str.='Very Poor';
								break;
							case '2':
								$str.=' Poor';
								break;
							case '3':
								$str.='Average';
								break;
							case '4':
								$str.= 'Good';
								break;
							case '5':
								$str.='Very Good';
								break;
							default:
								
						}
						
					$str.='</td></tr>';
				
					}
				}
				
				$str.='</table><div class="col-md-12"><h4>Message</h4>';			 
				
				$msg=$this->Model_rate->get_msg_from_rateorder($rateorder_id);
				if($msg==""){
					$msg="None";
				}
				$str.='<p>'.$msg.'</p>';
				
				$str.='</div>';
				
				}
				else{
					
					$str.='<div class="col-md-12"><table><tr><td>&nbsp;&nbsp;&nbsp;</td></tr><tr><td>Service feedback not yet completed.</td></tr></table></div>';
				}	
				echo $str;
			}else{
				
				$this->load->view('admin/header');
				$this->load->view('admin/login');
			}
		}
	
}

?>