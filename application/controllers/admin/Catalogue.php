<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Catalogue extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('admin/Model_catalogue');
		$this->load->model('admin/Inventory');
		$this->load->model('Front_promotions');
		$this->load->library('My_fpdf');
		$this->load->helper('emailsmstemplate');
		$this->load->helper('file');
		$this->load->helper('general');
        // Load form validation library
	}
	public function index(){
		if($this->session->userdata("logged_in")){
			$this->parent_category();	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	
	public function parent_category(){
		if($this->session->userdata("logged_in")){
			$data["menu_flag"]="catalog_active_links";
			
			$data['parent_catagories'] = $this->Model_catalogue->parent_category();		
			
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/parent_category_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function archived_parent_category(){
		if($this->session->userdata("logged_in")){
			$data["menu_flag"]="catalog_active_links_archived";
			$data['parent_catagories'] = $this->Model_catalogue->archived_parent_category();		
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_parent_category_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function add_parent_category(){
		if($this->session->userdata("logged_in")){
			$pcat_name=$this->db->escape_str($this -> input -> post("pcat_name"));
			$menu_level=$this->input->post("menu_level");
			$show_sub_menu=$this->input->post("show_sub_menu");
			$menu_sort_order=$this->input->post("menu_sort_order");
			$view=$this->input->post("view");
			$combo_pack_status=$this->input->post("view");
			$flag=$this->Model_catalogue->check_duplication_name($pcat_name);
			if($flag==true){
				echo "exist";
			}else{
				$update_flag=$this->Model_catalogue->add_parent_category($pcat_name,$menu_level,$show_sub_menu,$menu_sort_order,$view,$combo_pack_status);
				if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_sort_order(){
		if($this->session->userdata("logged_in")){
			$get_all_parent_category_arr=$this->Model_catalogue->get_all_parent_category();
				$pcat_id_arr=array();
				foreach($get_all_parent_category_arr as $all_parent_category_arr){
					$pcat_id_arr[]=$all_parent_category_arr["menu_sort_order"];
				}
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				
				if(count($miss_arr)>0){
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<?php
				foreach($miss_arr as $k => $v){
					if($k==count($miss_arr)+1){
				?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
					else{
						?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
				}
				}
				else{
			
				?>
					<option value="" selected></option>
					<option value="0">0</option>
					<option value="<?php echo (max($pcat_id_arr)+1);?>"><?php echo (max($pcat_id_arr)+1);?></option>
				<?php
				
				}
				}else{
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<option value="1">1</option>
				<?php
				}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_parent_category_form(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("pcat_id");
			$data['parent_catagories'] = $this->Model_catalogue->parent_category();
			$data["get_parent_category_data"]=$this->Model_catalogue->get_parent_category_data($pcat_id);
			$data["get_sort_order_options_for_p_cat"]=$this->get_sort_order_options_for_p_cat($data["get_parent_category_data"]["menu_level"],$pcat_id);
			$data["pcat_id"]=$pcat_id;
			$show_sort_order_arr=$this->Model_catalogue->show_edit_sort_order($data["get_parent_category_data"]["menu_sort_order"]);
			$data["show_sort_order_arr"]=$show_sort_order_arr;
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_parent_category',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_sort_order_options_for_p_cat($menu_level,$pcat_id){
		if($this->session->userdata("logged_in")){
			$get_edit_all_parent_category_arr=$this->Model_catalogue->get_edit_all_parent_category($menu_level,$pcat_id);
				$pcat_id_arr=array();
				foreach($get_edit_all_parent_category_arr as $all_parent_category_arr){
					$pcat_id_arr[]=$all_parent_category_arr["menu_sort_order"];
				}
				
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				//$miss_arr=array_diff($base_arr,$pcat_id_arr);
				if(count($miss_arr)>0){
					$parent_category_arr["key"]="Sort Order Suggestion";
					$parent_category_arr["value"]=implode(",",$miss_arr);
				}
				else{
					$parent_category_arr["key"]="Sort Order Suggestion";
					$parent_category_arr["value"]=max($pcat_id_arr)+1;
				}
				}else{
					$parent_category_arr["key"]="";
					$parent_category_arr["value"]="No Parent Category";
				}
				//echo json_encode($parent_category_arr);
				return $parent_category_arr;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_parent_category(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("pcat_id");
			$pcat_name=$this->db->escape_str($this -> input -> post("edit_pcat_name"));
			$menu_level=$this->input->post("edit_menu_level");
			$show_sub_menu=$this->input->post("edit_show_sub_menu");
			$menu_sort_order=$this->input->post("edit_menu_sort_order");
			$menu_sort_order_default=$this->input->post("edit_menu_sort_order_default_value");
			$default_edit_pcat_name=$this->input->post("default_edit_pcat_name");
			$view=$this->input->post("edit_view");
			$combo_pack_status=$this->input->post("combo_pack_status");
			
			$type=$this->input->post("type");
			$show_sort_order=$this->input->post("show_sort_order");
			$common_cat_id=$this->input->post("common_cat_id");
			$flag_edit="";
			if($default_edit_pcat_name!=$pcat_name){
			$flag_edit=$this->Model_catalogue->check_duplication_name($pcat_name);	
			}
			if($flag_edit==true || $flag_edit!=""){
				echo "exist";
			}else{
			$flag=$this->Model_catalogue->edit_parent_category($pcat_id,$pcat_name,$menu_level,$show_sub_menu,$menu_sort_order,$view,$type,$show_sort_order,$common_cat_id,$combo_pack_status);
			if($flag==true){
				$flag_update_pcate_name=$this->update_pcatname_search_index($pcat_id,$pcat_name);
				if($flag_update_pcate_name==true){
					echo true;
				}else{
					echo 0;
				}
			}else{
				echo 0;
			}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function delete_parent_category_selected(){
		if($this->session->userdata("logged_in")){	
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_parent_category_selected($selected_list);
			if($flag==true){
				$delete_flag=$this->delete_search_index_by_pcat_selected_list($selected_list);
				if($delete_flag==true){
					echo true;
				}else{
					echo 0;
				}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function delete_parent_category_no_data_selected(){
		if($this->session->userdata("logged_in")){	
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue->delete_parent_category_no_data_selected($selected_list);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function update_parent_category_selected(){
		if($this->session->userdata("logged_in")){	
			$selected_list=$this->input->post("selected_list");
			$active=$this->input->post("active");
			$flag=$this->Model_catalogue->update_parent_category_selected($selected_list,$active);
			if($flag==true){
				if($active==0){
					$search_flag=$this->delete_search_index_by_pcat_selected_entire_list($selected_list);
				}else{
					$search_flag=$this->restore_search_index_by_pcat_selected($selected_list);
				}
				if($search_flag==true){
					echo true;
				}else{
					echo 0;
				}
			}else{
				echo 0;
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function category(){
		if($this->session->userdata("logged_in")){
			$data['pcat_id']=$this->input->post("pcat_id");
			
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data['create_editview']=$this->input->post("create_editview");
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/category_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function category_filter(){
		
		if($this->session->userdata("logged_in")){		
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();	
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/category_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function show_cat_sort_order(){
		if($this->session->userdata("logged_in")){
			$menu_sort_order=$this->input->post("menu_sort_order");
			$menu_level=$this->input->post("menu_level");
			$pcat_id=$this->input->post("pcat_id");
			$get_cat_all_parent_category_arr=$this->Model_catalogue->get_cat_all_parent_category($menu_level,$pcat_id);
				$pcat_id_arr=array();
				foreach($get_cat_all_parent_category_arr as $all_parent_category_arr){
					$pcat_id_arr[]=$all_parent_category_arr["menu_sort_order"];
				}
				
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				
				if(count($miss_arr)>0){
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<?php
				foreach($miss_arr as $k => $v){
					
					
						?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					
				}
				}
				else{
			
				?>
					<option value="" selected></option>
					<option value="0">0</option>
					<option value="<?php echo (max($pcat_id_arr)+1);?>"><?php echo (max($pcat_id_arr)+1);?></option>
				<?php
				
				}
				}else{
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<option value="1">1</option>
				<?php
				}
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function archived_category(){
		if($this->session->userdata("logged_in")){
			$data['active']=$this->input->post("active");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_category_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function archived_category_filter(){
		
		if($this->session->userdata("logged_in")){		
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_category_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_category_form(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$pcat_id=$this->input->post("pcat_id");
			$cat_id=$this->input->post("cat_id");
			$data['parent_catagories'] = $this->Model_catalogue->parent_category();
			$data['create_editview']=$this->input->post("create_editview");
			$data["get_category_data"]=$this->Model_catalogue->get_category_data($pcat_id,$cat_id);
			$get_category_data=$data["get_category_data"];
			$data["get_sort_order_options_for_cat"]=$this->get_sort_order_options_for_cat($data["get_category_data"]["menu_level"],$pcat_id);
			
			$data["show_sort_order_arr"]=$this->Model_catalogue->show_cat_edit_sort_order($data["get_category_data"]["menu_sort_order"],$data["get_category_data"]["menu_level"],$data["get_category_data"]["pcat_id"]);
			$show_sort_order_arr=$data["show_sort_order_arr"];
			$data["menu_flag"]="catalog_active_links";
			$data["pcat_id"]=$pcat_id;
			$data["cat_id"]=$cat_id;
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_category_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_sort_order_options_for_cat($menu_level,$pcat_id){
		if($this->session->userdata("logged_in")){
			$get_edit_all_parent_category_arr=$this->Model_catalogue->get_edit_all_parent_category($menu_level,$pcat_id);
				$pcat_id_arr=array();
				foreach($get_edit_all_parent_category_arr as $all_parent_category_arr){
					$pcat_id_arr[]=$all_parent_category_arr["menu_sort_order"];
				}
				
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				if(count($miss_arr)>0){
					$category_arr["key"]="Sort Order Suggestion";
					$category_arr["value"]=implode(",",$miss_arr);
				}
				else{
					$category_arr["key"]="Sort Order Suggestion";
					$category_arr["value"]=max($pcat_id_arr)+1;
				}
				}else{
					$category_arr["key"]="";
					$category_arr["value"]="No Category";
				}
				return $category_arr;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function add_category(){
		if($this->session->userdata("logged_in")){
			$cat_name=$this->db->escape_str($this -> input -> post("cat_name"));
			$pcat_id=$this->input->post("parent_category");
			$menu_level=$this->input->post("menu_level");
			$menu_sort_order=$this->input->post("menu_sort_order");
			$view=$this->input->post("view");
			$flag=$this->Model_catalogue->check_duplication_name_category($cat_name,$pcat_id,$menu_level);
			if($flag==true){
				echo "exist";
			}else{
				$update_flag=$this->Model_catalogue->add_category($cat_name,$pcat_id,$menu_level,$menu_sort_order,$view);
				if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}
			

		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_category(){
		if($this->session->userdata("logged_in")){
			$cat_id=$this->input->post("edit_cat_id");
			$cat_name=$this->db->escape_str($this -> input -> post("edit_cat_name"));
			$cat_name_default=$this->db->escape_str($this -> input -> post("edit_cat_name_default_value"));
			$pcat_id=$this->input->post("edit_parent_category");
			$menu_level=$this->input->post("edit_menu_level");
			$menu_sort_order=$this->input->post("edit_menu_sort_order");
			$menu_sort_order_default=$this->input->post("edit_menu_sort_order_default");
			$view=$this->input->post("edit_view");
			$type=$this->input->post("type");
			$common_cat_id=$this->input->post("common_cat_id");
			$flag_check="";
			if($cat_name_default!=$cat_name){
			$flag_check=$this->Model_catalogue->check_duplication_name_category($cat_name,$pcat_id,$menu_level);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
				$flag=$this->Model_catalogue->edit_category($cat_id,$cat_name,$pcat_id,$menu_level,$menu_sort_order,$view,$type,$common_cat_id);
			if($flag==true){
				$update_flag=$this->update_catname_search_index($cat_id,$cat_name);
				if($update_flag==true){
					echo true;
				}else{
					echo 0;
				}
			}else{
				echo 0;
			}
			}

		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function update_category_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$active=$this->input->post("active");
			$flag=$this -> Model_catalogue->update_category_selected($selected_list,$active);
			if($flag==true){
				if($active==0){
					$search_flag=$this->delete_search_index_by_cat_id_selected($selected_list);
				}else{
					$search_flag=$this->restore_search_index_by_cat_id_selected($selected_list);
				}
				if($search_flag==true){
					echo true;
				}else{
					echo 0;
				}
			}else{
				echo 0;
			}
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	public function delete_category_when_no_data(){
		
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this->Model_catalogue->delete_category_when_no_data($selected_list);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	public function subcategory() {
		
		if($this->session->userdata("logged_in")){
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['create_editview']=$this->input->post("create_editview");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/subcategory_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function subcategory_filter(){
		
		if($this->session->userdata("logged_in")){	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/subcategory_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_subcategory_form(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$pcat_id=$this->input->post("pcat_id");
			$cat_id=$this->input->post("cat_id");
			$subcat_id=$this->input->post("subcat_id");
			//$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_catagories'] = $this->Model_catalogue->parent_category();
			$data['create_editview']=$this->input->post("create_editview");
			$data["get_subcategory_data"]=$this->Model_catalogue->get_subcategory_data($pcat_id,$cat_id,$subcat_id);
			$get_subcategory_data=$data["get_subcategory_data"];
			$data["get_sort_order_options_for_sub_cat"]=$this->get_sort_order_options_for_sub_cat($pcat_id,$cat_id);
			
			$data["subcategory_sort_order_arr"]=$this->Model_catalogue->show_subcategory_edit_sort_order($data["get_subcategory_data"]["menu_sort_order"],$data["get_subcategory_data"]["pcat_id"],$data["get_subcategory_data"]["cat_id"]);
			
			$subcategory_sort_order_arr=$data["subcategory_sort_order_arr"];
			
			$data["menu_flag"]="catalog_active_links";
			$data["pcat_id"]=$pcat_id;
			$data["cat_id"]=$cat_id;
			$data["subcat_id"]=$subcat_id;
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_subcategory_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_sort_order_options_for_sub_cat($pcat_id,$cat_id){
		if($this->session->userdata("logged_in")){
			$get_all_sub_category_arr=$this->Model_catalogue->get_all_sub_category($pcat_id,$cat_id);
				$subcat_id_arr=array();
				foreach($get_all_sub_category_arr as $sub_category_arr){
					$subcat_id_arr[]=$sub_category_arr["menu_sort_order"];
				}
				
				if(count($subcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($subcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				
				$miss_arr=array_diff($base_arr,$subcat_id_arr);
					if(!empty($base_arr)){
						$max_val_in_base_arr = max($base_arr);
						if(in_array($max_val_in_base_arr,$subcat_id_arr)){
							$last_sort_sug=$max_val_in_base_arr+1;
							$miss_arr[]=$last_sort_sug;
						}
						if(count($miss_arr)>0){
							$subcategory_arr["key"]="Sort Order Suggestion";
							$subcategory_arr["value"]=implode(",",$miss_arr);
						}
						else{
							$subcategory_arr["key"]="Sort Order Suggestion";
							$subcategory_arr["value"]=max($subcat_id_arr)+1;
						}
					}
					else{
						$subcategory_arr["key"]="";
						$subcategory_arr["value"]="No Subcategory";
					}
				}else{
					$subcategory_arr["key"]="";
					$subcategory_arr["value"]="No Subcategory";
				}
				return $subcategory_arr;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function archived_subcategory() {
		
		if($this->session->userdata("logged_in")){
			$data['active']=$this->input->post("active");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_subcategory_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function archived_subcategory_filter(){
		
		if($this->session->userdata("logged_in")){	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_subcategory_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_categories(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("pcat_id");
			$active=$this->input->post("active");
			if($this->input->post("with_status")){
				$with_status=$this->input->post("with_status");
			}else{
				$with_status=0;
			}
			$result_cat=$this->Model_catalogue->show_available_categories($pcat_id,$active);
			if(count($result_cat)!=0){
				$str='';
				$str.='<option value=""></option>';	
				if($with_status==1){
					foreach($result_cat as $res){
						$active_status=($res->active==1)?" - active":" - Inactive";
						$str.='<option value="'.$res->cat_id.'-'.$res->active.'">'.$res->cat_name.$active_status.'</option>';
					}
				}else{
					foreach($result_cat as $res){
					$str.='<option value="'.$res->cat_id.'">'.$res->cat_name.'</option>';
					}
				}
				echo $str;
			}else{
				echo count($result_cat);
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_subcategories(){
		if($this->session->userdata("logged_in")){
			$cat_id=$this->input->post("cat_id");
			$active=$this->input->post("active");
			if($this->input->post("with_status")){
				$with_status=$this->input->post("with_status");
			}else{
				$with_status=0;
			}
			$result_subcat=$this->Model_catalogue->show_available_subcategories($cat_id,$active);
			if(count($result_subcat)!=0){
				$str='';
				$str.='<option value=""></option>';
				if($with_status==1){
					foreach($result_subcat as $res){
						$active_status=($res->active==1)?" - active":" - Inactive";
						$str.='<option value="'.$res->subcat_id.'-'.$res->active.'">'.$res->subcat_name.$active_status.'</option>';
					}
				}else{
					foreach($result_subcat as $res){
					$str.='<option value="'.$res->subcat_id.'">'.$res->subcat_name.'</option>';
					}
				}
				echo $str;
			}else{
				echo count($result_subcat);
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_subcategories_for_filterbox(){
		if($this->session->userdata("logged_in")){
			$cat_id=$this->input->post("cat_id");
			$active=$this->input->post("active");
			if($this->input->post("with_status")){
				$with_status=$this->input->post("with_status");
			}else{
				$with_status=0;
			}
			if($cat_id==""){
				echo "0";
				exit;
			}
			$result_subcat=$this->Model_catalogue->show_available_subcategories_for_filterbox($cat_id,$active);
			if($cat_id!=""){
				if(count($result_subcat)==0){
					echo "1";
					exit;
				}
			}
			if(count($result_subcat)!=0){
				$str='';
				$str.='<option value=""></option>';
				if($with_status==1){
					foreach($result_subcat as $res){
						$active_status=($res->active==1)?" - active":" - Inactive";
						$str.='<option value="'.$res->subcat_id.'-'.$res->active.'">'.$res->subcat_name.$active_status.'</option>';
					}
				}else{
					foreach($result_subcat as $res){
					$str.='<option value="'.$res->subcat_id.'">'.$res->subcat_name.'</option>';
					}
				}
				echo $str;
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_brands(){
		if($this->session->userdata("logged_in")){	
			$subcat_id=$this->input->post("subcat_id");
			$active=$this->input->post("active");
			if($this->input->post("with_status")){
				$with_status=$this->input->post("with_status");
			}else{
				$with_status=0;
			}
			$result_brand=$this->Model_catalogue->show_available_brands($subcat_id,$active);
			if(count($result_brand)!=0){
				$str='';
				$str.='<option value=""></option>';
				if($with_status==1){
					foreach($result_brand as $res){
						$active_status=($res->active==1)?" - active":" - Inactive";
						$str.='<option value="'.$res->brand_id.'-'.$res->active.'">'.$res->brand_name.$active_status.'</option>';
					}
				}else{
					foreach($result_brand as $res){
						$str.='<option value="'.$res->brand_id.'">'.$res->brand_name.'</option>';
					}
				}
				echo $str;
			}else{
				echo count($result_brand);
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_products(){
		if($this->session->userdata("logged_in")){	
			$brand_id=$this->input->post("brand_id");
			$active=$this->input->post("active");
			if($this->input->post("with_status")){
				$with_status=$this->input->post("with_status");
			}else{
				$with_status=0;
			}
			$result_brand=$this->Model_catalogue->show_available_products($brand_id,$active);
			if(count($result_brand)!=0){
				$str='';
				$str.='<option value=""></option>';
				if($with_status==1){
					foreach($result_brand as $res){
						$active_status=($res->active==1)?" - active":" - Inactive";
						$str.='<option value="'.$res->product_id.'-'.$res->active.'">'.$res->product_name.$active_status.'</option>';
					}
				}else{
					foreach($result_brand as $res){
						$str.='<option value="'.$res->product_id.'">'.$res->product_name.'</option>';
					}
				}
				echo $str;
			}else{
				echo count($result_brand);
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function show_available_filterbox(){
		if($this->session->userdata("logged_in")){	
			
			$arr=array();
			if($this->input->post("flagtype")==1){
				$arr["type"]="pcat";
				$arr["pcat_id"]=$this->input->post("pcat_id");
				
			}
			if($this->input->post("pcat_id")==0 && $this->input->post("flagtype")==1){
				$arr["type"]="pcat";
				$arr["pcat_id"]=$this->input->post("pcat_id");
				
			}
			if($this->input->post("flagtype")==2){
				$arr["type"]="cat";
				$arr["pcat_id"]=$this->input->post("pcat_id");
				$arr["cat_id"]=$this->input->post("cat_id");
			}
			if($this->input->post("flagtype")==3){
				$arr["type"]="subcat";
				$arr["pcat_id"]=$this->input->post("pcat_id");
				$arr["cat_id"]=$this->input->post("cat_id");
				$arr["subcat_id"]=$this->input->post("subcat_id");
				
			}

			$active=$this->input->post("active");
			if($this->input->post("with_status")){
				$with_status=$this->input->post("with_status");
			}else{
				$with_status=0;
			}
			$result_filterbox=$this->Model_catalogue->show_available_filterbox($arr,$active);
			if(count($result_filterbox)!=0){
				$str='';
				$str.='<option value=""></option>';
				if($with_status==1){
					foreach($result_filterbox as $res){
						$active_status=($res->active==1)?" - active":" - Inactive";
						$str.='<option  value="'.$res->filterbox_id.'-'.$res->active.'">'.$res->filterbox_name.$active_status.'</option>';
					}
				}else{
					foreach($result_filterbox as $res){
						$str.='<option filterboxname="'.$res->filterbox_name.'" value="'.$res->filterbox_id.'">'.$res->filterbox_name.'</option>';
					}
				}
				echo $str;
			}else{
				echo count($result_filterbox);
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_specification_group(){
		if($this->session->userdata("logged_in")){	
			
			$arr=array();

			
			if($this->input->post("flagtype")==1){
				$arr["type"]="pcat";
				$arr["pcat_id"]=$this->input->post("pcat_id");
				
			}
			if($this->input->post("flagtype")==2){
				$arr["type"]="cat";
				$arr["pcat_id"]=$this->input->post("pcat_id");
				$arr["cat_id"]=$this->input->post("cat_id");
			}
			if($this->input->post("flagtype")==3){
				$arr["type"]="subcat";
				$arr["pcat_id"]=$this->input->post("pcat_id");
				$arr["cat_id"]=$this->input->post("cat_id");
				$arr["subcat_id"]=$this->input->post("subcat_id");
				
			}
			$active=$this->input->post("active");
			if($this->input->post("with_status")){
				$with_status=$this->input->post("with_status");
			}else{
				$with_status=0;
			}
			$result_specification_group=$this->Model_catalogue->show_available_specification_group($arr,$active);
			if(count($result_specification_group)!=0){
				$str='';				
				$str.='<option value=""></option>';		
				if($with_status==1){
					foreach($result_specification_group as $res){
						$active_status=($res->active==1)?" - active":" - Inactive";
						$str.='<option value="'.$res->specification_group_id.'-'.$res->active.'">'.$res->specification_group_name.$active_status.'</option>';
					}
				}else{
					foreach($result_specification_group as $res){
						$str.='<option value="'.$res->specification_group_id.'">'.$res->specification_group_name.'</option>';
					}
				}
				echo $str;	
			}else{
				echo count($result_specification_group);
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_cities(){
		if($this->session->userdata("logged_in")){	
			$logistics_id=$this->input->post("logistics_id");
			$result_pin=$this->Model_catalogue->show_available_cities($logistics_id);
			if(count($result_pin)!=0){
				$str='';	
				$str.='<option value=""></option>';			
				foreach($result_pin as $res){	
					$str.='<option value="'.$res->pin."_".strtolower($res->city).'">'.$res->pin.'-'.$res->city.'</option>';
				}
				echo $str;
			}else{
				echo count($result_pin);
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_logistics_services(){
		if($this->session->userdata("logged_in")){	
			$logistics_id=$this->input->post("logistics_id");
			$result_pin=$this->Model_catalogue->show_available_cities($logistics_id);
			if(count($result_pin)!=0){
				$str='';
				$str.='<option value=""></option>';
				foreach($result_pin as $res){
					$str.='<option value="'.$res->logistics_service_id.'">'.$res->pin.'-'.$res->city.'</option>';
				}
				echo $str;				
			}else{
				echo count($result_pin);
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function show_available_logistics_territory(){
		
		if($this->session->userdata("logged_in")){	
			$logistics_service_id=$this->input->post("logistics_service_id");
			$result_terri=$this->Model_catalogue->show_available_logistics_territory($logistics_service_id);
			if(count($result_terri)!=0){				
				$str='<option value=""></option>';			
				foreach($result_terri as $res){		
					$str.='<option value="'.$res->logistics_territory_id.'">'.$res->territory_name.'</option>';
				}
				echo $str;
			}else{
				echo count($result_terri);
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_reasons(){
		
		if($this->session->userdata("logged_in")){	
			$subcat_id=$this->input->post("subcat_id");
			$result_reason=$this->Model_catalogue->show_available_reasons($subcat_id);
			if(count($result_reason)!=0){				
				$str='<option value=""></option>';			
				foreach($result_reason as $res){		
					$str.='<option value="'.$res->reason_id.'">'.$res->reason_name.'</option>';
				}
				echo $str;
			}else{
				echo count($result_reason);
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function add_subcategory(){
		if($this->session->userdata("logged_in")){
			$subcat_name=$this->db->escape_str($this -> input -> post("subcat_name"));
			$cat_id=$this->input->post("categories");
			$pcat_id=$this->input->post("parent_category");
			$subcat_comparision=$this->input->post("subcat_comparision");
			$menu_sort_order=$this->input->post("menu_sort_order");
			$view=$this->input->post("view");
			$flag=$this->Model_catalogue->check_duplication_name_sub_category($subcat_name,$pcat_id,$cat_id);
			if($flag==true){
				echo "exist";
			}else{
				$update_flag=$this->Model_catalogue->add_subcategory($subcat_name,$cat_id,$pcat_id,$menu_sort_order,$view,$subcat_comparision);
				if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}
			

		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function edit_subcategory(){
		if($this->session->userdata("logged_in")){
			$subcat_id=$this->input->post("edit_subcat_id");
			$cat_id=$this->input->post("edit_categories");
			$subcat_name=$this->db->escape_str($this -> input -> post("edit_subcat_name"));
			$subcat_name_default=$this->db->escape_str($this -> input -> post("edit_subcat_name_default"));
			$pcat_id=$this->input->post("edit_parent_category");
			$subcat_comparision=$this->input->post("edit_subcat_comparision");
			$menu_sort_order=$this->input->post("edit_menu_sort_order");
			$view=$this->input->post("edit_view");
			$common_subcat=$this->input->post("common_subcat");
			$menu_sort_order_default=$this->input->post("edit_menu_sort_order_default");
			$flag_check="";
			if($subcat_name_default!=$subcat_name){
			$flag_check=$this->Model_catalogue->check_duplication_name_sub_category($subcat_name,$pcat_id,$cat_id);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
				$flag=$this->Model_catalogue->edit_subcategory($subcat_id,$cat_id,$subcat_name,$pcat_id,$menu_sort_order,$view,$common_subcat,$subcat_comparision);
			if($flag==true){
				$update_flag=$this->update_subcatname_search_index($subcat_id,$subcat_name);
				if($update_flag==true){
					echo true;
				}else{
					echo 0;
				}
			}else{
				echo 0;
			}
			}
			

		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function update_subcategory_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$active=$this->input->post("active");
			$flag=$this -> Model_catalogue-> update_subcategory_selected($selected_list,$active);
			if($flag==true){
				if($active==0){
					$search_flag=$this->delete_search_index_by_subcat_id_selected($selected_list);
				}else{
					$search_flag=$this->restore_search_index_by_subcat_id_selected($selected_list);
				}
				if($search_flag==true){
					echo true;
				}else{
					echo 0;
				}
			}else{
				echo 0;
			}

		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	public function delete_subcategory_when_no_data(){
		
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this->Model_catalogue->delete_subcategory_when_no_data($selected_list);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	public function brands(){
		
		if($this->session->userdata("logged_in")){
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['brands'] = $this->Model_catalogue->brands();
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data['create_editview']=$this->input->post("create_editview");
			$data["menu_flag"]="catalog_active_links";
			$data["vendors_details"]=$this->Model_catalogue->vendors();
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/brand_view',$data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function brands_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['brand_id']=$this->input->post("brand_id");	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/brand_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_brand_form(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$pcat_id=$this->input->post("pcat_id");
			$cat_id=$this->input->post("cat_id");
			$subcat_id=$this->input->post("subcat_id");
			$brand_id=$this->input->post("brand_id");
			$data['brands'] = $this->Model_catalogue->brands();
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_catagories'] = $this->Model_catalogue->parent_category();
			$data['create_editview']=$this->input->post("create_editview");
			$data["get_brand_data"]=$this->get_brand_data($pcat_id,$cat_id,$subcat_id,$brand_id);
			$get_brand_data=$data["get_brand_data"];
			$data["menu_flag"]="catalog_active_links";
			$data["pcat_id"]=$pcat_id;
			$data["cat_id"]=$cat_id;
			$data["subcat_id"]=$subcat_id;
			$data["brand_id"]=$brand_id;
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_brand_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function archived_brands(){		
		if($this->session->userdata("logged_in")){
			$data['active']=$this->input->post("active");
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['brands'] = $this->Model_catalogue->brands();
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_brand_view',$data);		
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function archived_brands_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['brand_id']=$this->input->post("brand_id");	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_brand_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function add_brand(){
		if($this->session->userdata("logged_in")){	
			$brand_name=$this->db->escape_str($this -> input -> post("brand_name"));
			$cat_id=$this->input->post("categories");
			$subcat_id=$this->input->post("subcategories");
			$pcat_id=$this->input->post("parent_category");
			$view=$this->input->post("view");
			
			$flag=$this->Model_catalogue->check_duplication_name_brand($brand_name,$subcat_id,$pcat_id,$cat_id);
			if($flag==true){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->add_brand($brand_name,$subcat_id,$cat_id,$pcat_id,$view);
			if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_brand_data($pcat_id,$cat_id,$subcat_id,$brand_id){
		if($this->session->userdata("logged_in")){
			$brand_arr=$this->Model_catalogue->get_brand_data($pcat_id,$cat_id,$subcat_id,$brand_id);
			return $brand_arr;	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_brand(){
		if($this->session->userdata("logged_in")){
			$brand_id=$this->input->post("edit_brand_id");
			$brand_name=$this->db->escape_str($this -> input -> post("edit_brand_name"));
			$brand_name_default=$this->db->escape_str($this -> input -> post("edit_brand_name_default"));
			$subcat_id=$this->input->post("edit_subcategories");
			$cat_id=$this->input->post("edit_categories");
			$pcat_id=$this->input->post("edit_parent_category");
			$view=$this->input->post("edit_view");
			$flag_check="";
			if($brand_name_default!=$brand_name){
			$flag_check=$this->Model_catalogue->check_duplication_name_brand($brand_name,$subcat_id,$pcat_id,$cat_id);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
			$flag=$this->Model_catalogue->edit_brand($brand_id,$brand_name,$subcat_id,$cat_id,$pcat_id,$view);
			
			if($flag==true){
				$updated_flag=$this->update_brandname_search_index($brand_id,$brand_name,$subcat_id,$cat_id,$pcat_id);
			
				if($updated_flag==true){
					echo true;
				}else{
					echo 0;
				}
			}else{
				echo 0;
			}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function update_brand_selected(){
		
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$active=$this->input->post("active");
			$flag=$this->Model_catalogue->update_brand_selected($selected_list,$active);
			if($flag==true){
				if($active==0){
					$search_flag=$this->delete_search_index_by_brand_id_selected($selected_list);	
				}else{
					$search_flag=$this->restore_search_index_by_brand_id_selected($selected_list);	
				}
				if($search_flag==true){
					echo true;
				}else{
					echo 0;
				}
			}else{
				echo 0;
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	public function delete_brand_when_no_data(){		
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this->Model_catalogue->delete_brand_when_no_data($selected_list);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	public function attribute(){	
	if($this->session->userdata("logged_in")){
		$data['subcat_id']=$this->input->post("subcat_id");
		$data['cat_id']=$this->input->post("cat_id");
		$data['pcat_id']=$this->input->post("pcat_id");
		$data['attributes'] = $this->Model_catalogue->attribute();
		$data['brands'] = $this->Model_catalogue->brands();
		$data['subcategories'] = $this->Model_catalogue->subcategories();
		$data['categories'] = $this->Model_catalogue->category();
		$data['parent_cat_check'] = $this->Model_catalogue->parent_category_attribute_check();
		$data['parent_category'] = $this->Model_catalogue->parent_category();
		$data['create_editview']=$this->input->post("create_editview");
		$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/attribute_view',$data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function attribute_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['attribute_id']=$this->input->post("attribute_id");	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/attribute_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_attribute_form(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$catalog=$this->input->post("catalog");
			$catalog_id=$this->input->post("catalog_id");
			$attribute_id=$this->input->post("attribute_id");
			$pcat_id=$this->input->post("pcat_id");
			$cat_id=$this->input->post("cat_id");
			$subcat_id=$this->input->post("subcat_id");
			
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_catagories'] = $this->Model_catalogue->parent_category();
			$data['create_editview']=$this->input->post("create_editview");
			$attr_arr=$this->Model_catalogue->get_attribute_data($catalog,$catalog_id,$attribute_id);
			$data["attr_arr"]=$attr_arr;
			$data["catalog"]=$catalog;
			$data["catalog_id"]=$catalog_id;
			$data["attribute_id"]=$attribute_id;
			$data["pcat_id"]=$pcat_id;
			$data["cat_id"]=$cat_id;
			$data["subcat_id"]=$subcat_id;
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_attribute_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function archived_attribute() {		
		if($this->session->userdata("logged_in")){
			$data['active']=$this->input->post("active");
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['attributes'] = $this->Model_catalogue->attribute();
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_attribute_view',$data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function archived_attribute_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['attribute_id']=$this->input->post("attribute_id");	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_attribute_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function add_attribute(){
	
		if($this->session->userdata("logged_in")){
			$attribute1_name=$this->db->escape_str($this -> input -> post("attribute1_name"));
			$attribute1_option=$this->input->post("attribute1_option");
			$attribute1_option=rtrim($attribute1_option, ",");
			$attribute11_name=$this->db->escape_str($this -> input -> post("attribute11_name"));
			$attribute11_option=$this->input->post("attribute11_option");
			$attribute11_option=rtrim($attribute11_option, ",");
			$attribute2_name=$this->db->escape_str($this -> input -> post("attribute2_name"));
			$attribute2_option=$this->input->post("attribute2_option");
			$attribute2_option=rtrim($attribute2_option, ",");
			$attribute3_name=$this->db->escape_str($this -> input -> post("attribute3_name"));
			$attribute3_option=$this->input->post("attribute3_option");
			$attribute3_option=rtrim($attribute3_option, ",");
			$attribute4_name=$this->db->escape_str($this -> input -> post("attribute4_name"));
			$attribute4_option=$this->input->post("attribute4_option");
			$attribute4_option=rtrim($attribute4_option, ",");
			$cat_id=$this->input->post("categories");
			$subcat_id=$this->input->post("subcategories");
			$pcat_id=$this->input->post("parent_category");
			$view=$this->input->post("view");
			$value=$this->input->post("value");
			$flag=$this->Model_catalogue->add_attribute($attribute1_name,$attribute1_option,$attribute11_name,$attribute11_option,$attribute2_name,$attribute2_option,$attribute3_name,$attribute3_option,$attribute4_name,$attribute4_option,$subcat_id,$cat_id,$pcat_id,$view,$value);
	
			echo $flag;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function add_attribute_check(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("pcat_id");
			$flag=$this->Model_catalogue->add_attribute_check($pcat_id);
			echo json_encode($flag);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function add_attribute_subcheck(){
		if($this->session->userdata("logged_in")){
			$cat_id=$this->input->post("cat_id");
			$flag=$this->Model_catalogue->add_attribute_subcheck($cat_id);
			echo json_encode($flag);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function get_attribute_data($catalog_id,$catalog,$attribute_id){
		if($this->session->userdata("logged_in")){			
			
			$attr_arr=$this->Model_catalogue->get_attribute_data($catalog,$catalog_id,$attribute_id);
			return $attr_arr;	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_attribute(){
		if($this->session->userdata("logged_in")){	
			$attribute_id=$this->input->post("edit_attribute_id");
			$attribute1_name=$this->input->post("edit_attribute1_name");
			$attribute1_option=$this->input->post("edit_attribute1_option");
			$attribute1_option=rtrim($attribute1_option, ",");
			$attribute1_name1=$this->input->post("edit_attribute1_name1");
			$attribute1_option1=$this->input->post("edit_attribute1_option1");
			$attribute1_option1=rtrim($attribute1_option1, ",");
			$attribute2_name=$this->input->post("edit_attribute2_name");
			$attribute2_option=$this->input->post("edit_attribute2_option");
			$attribute2_option=rtrim($attribute2_option, ",");
			$attribute3_name=$this->input->post("edit_attribute3_name");
			$attribute3_option=$this->input->post("edit_attribute3_option");
			$attribute3_option=rtrim($attribute3_option, ",");
			$attribute4_name=$this->input->post("edit_attribute4_name");
			$attribute4_option=$this->input->post("edit_attribute4_option");
			$attribute4_option=rtrim($attribute4_option, ",");
			$subcat_id=$this->input->post("edit_subcategories");
			$cat_id=$this->input->post("edit_categories");
			$pcat_id=$this->input->post("edit_parent_category");
			$view=$this->input->post("edit_view");
			$value=$this->input->post("value");
			
			$flag=$this->Model_catalogue->edit_attribute($attribute_id,$attribute1_name,$attribute1_option,$attribute1_name1,$attribute1_option1,$attribute2_name,$attribute2_option,$attribute3_name,$attribute3_option,$attribute4_name,$attribute4_option,$subcat_id,$cat_id,$pcat_id,$view,$value);
			
			echo $flag;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function update_attribute_selected(){
		
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$active=$this->input->post("active");
			$flag=$this->Model_catalogue->update_attribute_selected($selected_list,$active);
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	public function delete_attribute_when_no_data(){
		
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this->Model_catalogue->delete_attribute_when_no_data($selected_list);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	public function filterbox() {
		
		if($this->session->userdata("logged_in")){
			$data['filterbox_id']=$this->input->post("filterbox_id");	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['filterbox'] = $this->Model_catalogue->filterbox();
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data['create_editview']=$this->input->post("create_editview");
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/filterbox_view',$data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function filterbox_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['filterbox_id']=$this->input->post("filterbox_id");	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/filterbox_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function archived_filterbox(){
		if($this->session->userdata("logged_in")){
			$data['active']=$this->input->post("active");
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['filterbox'] = $this->Model_catalogue->filterbox();
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_filterbox_view',$data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function archived_filterbox_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['filterbox_id']=$this->input->post("filterbox_id");	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_filterbox_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function add_filterbox(){
	
		if($this->session->userdata("logged_in")){
			$filterbox_name=$this->input->post("filterbox_name");
			$filterbox_units=$this->input->post("filterbox_units");
			$type=$this->input->post("filterbox_type");
			$sort_order=$this->input->post("sort_order");
			$cat_id=$this->input->post("categories");
			$subcat_id=$this->input->post("subcategories");
			$pcat_id=$this->input->post("parent_category");
			$pcat_view = ($this->input->post("pcat_view")) ? 1 : 0;
			$cat_view = ($this->input->post("cat_view")) ? 1 : 0;
			$subcat_view = ($this->input->post("subcat_view")) ? 1 : 0;
			$view_filterbox=$this->input->post("view");
			
			$update_flag=$this->Model_catalogue->add_filterbox($filterbox_name,$filterbox_units,$type,$sort_order,$subcat_id,$cat_id,$pcat_id,$view_filterbox,$pcat_view,$cat_view,$subcat_view);
			if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			

		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_filterbox_form(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			
			$pcat_id=$this->input->post("pcat_id");
			$cat_id=$this->input->post("cat_id");
			$subcat_id=$this->input->post("subcat_id");
			$filterbox_id=$this->input->post("filterbox_id");
			$data['filterbox'] = $this->Model_catalogue->filterbox();
			$data['subcategories'] = $this->Model_catalogue->show_available_subcategories($cat_id,1);
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_catagories'] = $this->Model_catalogue->parent_category();
			$data['create_editview']=$this->input->post("create_editview");
			$result_type='';
			if($pcat_id!=0 && $cat_id==0 && $subcat_id==0){
			$result_type="pcat_view";
			}
			if($pcat_id!=0 && $cat_id!=0 && $subcat_id==0){
			$result_type="cat_view";
			}
			if($pcat_id!=0 && $cat_id!=0 && $subcat_id!=0){
			$result_type="subcat_view";
			}
			if($pcat_id==0 && $cat_id==0 && $subcat_id==0){
			$result_type="pcat_view";
			}
			if($pcat_id==0 && $cat_id!=0 && $subcat_id==0){
			$result_type="cat_view";
			}
			if($pcat_id==0 && $cat_id!=0 && $subcat_id!=0){
			$result_type="subcat_view";
			}
			$data["get_filterbox_data"]=$this->Model_catalogue->get_filterbox_data($filterbox_id,$cat_id,$subcat_id,$pcat_id);
			$get_filterbox_data=$data["get_filterbox_data"];

			$data["get_sort_order_options_for_filterbox"]=$this->get_sort_order_options_for_filterbox($pcat_id,$cat_id);
			
			$data["filterbox_sort_order_arr"]=$this->Model_catalogue->show_filterbox_edit_sort_order($data["get_filterbox_data"]["sort_order"],$data["get_filterbox_data"]["pcat_id"],$data["get_filterbox_data"]["cat_id"],$data["get_filterbox_data"]["subcat_id"],$result_type);
			$filterbox_sort_order_arr=$data["filterbox_sort_order_arr"];
			
			$data["pcat_id"]=$pcat_id;
			$data["cat_id"]=$cat_id;
			$data["subcat_id"]=$subcat_id;
			$data["filterbox_id"]=$filterbox_id;
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_filterbox_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_sort_order_options_for_filterbox($pcat_id,$cat_id)
	{
		if($this->session->userdata("logged_in")){
			
			$get_filterbox_pcat_arr=$this->Model_catalogue->sort_order_filterbox_pcat($pcat_id,$cat_id);
				$pcat_id_arr=array();
				foreach($get_filterbox_pcat_arr as $filterbox_parent_category_arr){
					$pcat_id_arr[]=$filterbox_parent_category_arr["sort_order"];
				}
				
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				if(count($miss_arr)>0){
					$filterbox_arr["key"]="Sort Order Suggestion";
					$filterbox_arr["value"]=implode(",",$miss_arr);
				}
				else{
					$filterbox_arr["key"]="Sort Order Suggestion";
					$filterbox_arr["value"]=max($pcat_id_arr)+1;
				}
				}else{
					$filterbox_arr["key"]="";
					$filterbox_arr["value"]="This is the first filterbox";
				}
			return $filterbox_arr;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_filterbox_sort_order(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("pcat_id");
			$cat_id=$this->input->post("cat_id");
			$get_filterbox_pcat_arr=$this->Model_catalogue->sort_order_filterbox_pcat($pcat_id,$cat_id);
			if($get_filterbox_pcat_arr!= ""){
				$pcat_id_arr=array();
				foreach($get_filterbox_pcat_arr as $filterbox_pcat_arr){
					$pcat_id_arr[]=$filterbox_pcat_arr["sort_order"];
				}
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				
				if(count($miss_arr)>0){
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<?php
				foreach($miss_arr as $k => $v){
					if($k==count($miss_arr)+1){
				?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
					else{
						?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
				}
				}
				else{
			
				?>
					<option value="" selected></option>
					<option value="0">0</option>
					<option value="<?php echo (max($pcat_id_arr)+1);?>"><?php echo (max($pcat_id_arr)+1);?></option>
				<?php
				
				}
				}
			}else{
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<option value="1">1</option>
				<?php
				}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_filterbox()
	{
		if($this->session->userdata("logged_in")){
			
			$filterbox_id=$this->input->post("edit_filterbox_id");		
			$filterbox_name=$this->input->post("edit_filterbox_name");
			$filterbox_units=$this->input->post("edit_filterbox_units");
			$filterbox_name_default=$this->input->post("edit_filterbox_name_default");
			$filterbox_units_default=$this->input->post("edit_filterbox_units_default");
			$type=$this->input->post("edit_filterbox_type");
			$sort_order=$this->input->post("edit_sort_order");			
			$subcat_id=$this->input->post("edit_subcategories");
			$cat_id=$this->input->post("edit_categories");
			$pcat_id=$this->input->post("edit_parent_category");
			$pcat_view = ($this->input->post("edit_pcat_view")) ? 1 : 0;
			$cat_view = ($this->input->post("edit_cat_view")) ? 1 : 0;
			$subcat_view = ($this->input->post("edit_subcat_view")) ? 1 : 0;
			$view_filterbox=$this->input->post("edit_view");
			$common_filterbox=$this->input->post("common_filterbox");
			$sort_order_default=$this->input->post("edit_sort_order_default");
			
			$flag=$this->Model_catalogue->edit_filterbox($filterbox_id,$filterbox_name,$filterbox_units,$type,$sort_order,$subcat_id,$cat_id,$pcat_id,$view_filterbox,$pcat_view,$cat_view,$subcat_view,$common_filterbox);
			
			if($flag==true){
				$update_search=$this->update_filterbox_name_search_index($filterbox_id,$filterbox_name);
	
				if($update_search==true){
					echo true;
				}else{
					echo 0;
				}
			}else{
				echo 0;
			}
			

		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function update_filterbox_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$active=$this->input->post("active");
			$flag_update=$this->update_search_index_by_filterbox_id_selected($selected_list);			
			if($flag_update==true){
				$flag=$this->Model_catalogue->update_filterbox_selected($selected_list,$active);
				if($flag==true){
					echo true;
				}else{
					echo true;
				}
			}else{
				echo 0;
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function delete_filterbox_when_no_data(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this->Model_catalogue->delete_filterbox_when_no_data($selected_list);
			echo $flag;	
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function filter() {
		if($this->session->userdata("logged_in")){
			$data['filterbox_id']=$this->input->post("filterbox_id");
			$data['filter_id']=$this->input->post("filter_id");
			
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['filterbox'] = $this->Model_catalogue->filterbox();
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['create_editview']=$this->input->post("create_editview");
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/filter_view',$data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function filter_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['filterbox_id']=$this->input->post("filterbox_id");
			$data['filter_id']=$this->input->post("filter_id");
			
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data["menu_flag"]="catalog_active_links";
			$data['parent_category'] = $this->Model_catalogue->parent_category();	
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/filter_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function archived_filter() {
		if($this->session->userdata("logged_in")){
			$data['active']=$this->input->post("active");
			$data['filterbox_id']=$this->input->post("filterbox_id");
			$data['filter_id']=$this->input->post("filter_id");
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['filterbox'] = $this->Model_catalogue->filterbox();
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_filter_view',$data);		
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function archived_filter_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['filterbox_id']=$this->input->post("filterbox_id");
			$data['filter_id']=$this->input->post("filter_id");
			
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_filter_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function add_filter(){
	
		if($this->session->userdata("logged_in")){
			$filter_options=$this->input->post("filter_options");
			$filterbox_id=$this->input->post("filterbox");
			$sort_order=$this->input->post("sort_order");
			$cat_id=$this->input->post("categories");
			$subcat_id=$this->input->post("subcategories");
			$pcat_id=$this->input->post("parent_category");
			$view_filter=$this->input->post("view");
			$flag=$this->Model_catalogue->check_duplication_name_filter($filter_options,$filterbox_id);
			if($flag==true){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->add_filter($filter_options,$filterbox_id,$sort_order,$subcat_id,$cat_id,$pcat_id,$view_filter);
				if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_filter_form(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$pcat_id=$this->input->post("pcat_id");
			$cat_id=$this->input->post("cat_id");
			$subcat_id=$this->input->post("subcat_id");
			$filter_id=$this->input->post("filter_id");
			$filterbox_id=$this->input->post("filterbox_id");
			$data['filterbox'] = $this->Model_catalogue->filterbox();
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_catagories'] = $this->Model_catalogue->parent_category();
			$data['create_editview']=$this->input->post("create_editview");
			$data["get_filter_data"]=$this->Model_catalogue->get_filter_data($filter_id,$cat_id,$subcat_id,$pcat_id);
			$get_filter_data=$data["get_filter_data"];
			$data["get_sort_order_options_for_filter"]=$this->get_sort_order_options_for_filter($filterbox_id);
			$data["subcategory_sort_order_arr"]=$this->Model_catalogue->show_filter_edit_sort_order($data["get_filter_data"]["filter_sort_order"],$data["get_filter_data"]["filterbox_id"]);
			$subcategory_sort_order_arr=$data["subcategory_sort_order_arr"];
			$data["pcat_id"]=$pcat_id;
			$data["cat_id"]=$cat_id;
			$data["subcat_id"]=$subcat_id;
			$data["filterbox_id"]=$filterbox_id;
			$data["filter_id"]=$filter_id;
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_sort_order_options_for_filter($filterbox_id)
	{
		if($this->session->userdata("logged_in")){
			$get_all_sub_category_arr=$this->Model_catalogue->get_all_filter_sort_order($filterbox_id);
				$filter_id_arr=array();
				foreach($get_all_sub_category_arr as $sub_category_arr){
					$filter_id_arr[]=$sub_category_arr["sort_order"];
				}
				
				if(count($filter_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($filter_id_arr);$i++){
					$base_arr[]=$i;
				}
				
				$miss_arr=array_diff($base_arr,$filter_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$filter_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				if(count($miss_arr)>0){
					$filter_arr["key"]="Sort Order Suggestion";
					$filter_arr["value"]=implode(",",$miss_arr);
				}
				else{
					$filter_arr["key"]="Sort Order Suggestion";
					$filter_arr["value"]=max($filter_id_arr)+1;
				}
				}else{
					$filter_arr["key"]="";
					$filter_arr["value"]="No filter";
				}
			return $filter_arr;

			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_filter()
	{
		if($this->session->userdata("logged_in")){
			
			$filter_id=$this->input->post("edit_filter_id");
			$filter_options=$this->input->post("edit_filter_options");
			$filter_options_default=$this->input->post("edit_filter_options_default");
			$filterbox_id=$this->input->post("edit_filterbox");
			$sort_order=$this->input->post("edit_sort_order");
			$cat_id=$this->input->post("edit_categories");
			$subcat_id=$this->input->post("edit_subcategories");
			$pcat_id=$this->input->post("edit_parent_category");
			$view_filter=$this->input->post("edit_view");
			$common_filter=$this->input->post("common_filter");
			$sort_order_default=$this->input->post("edit_sort_order_default");
			$flag_check="";
			if($filter_options_default!=$filter_options){
			$flag_check=$this->Model_catalogue->check_duplication_name_filter($filter_options,$filterbox_id);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
			$flag=$this->Model_catalogue->edit_filter($filter_id,$filter_options,$filterbox_id,$sort_order,$subcat_id,$cat_id,$pcat_id,$view_filter,$common_filter);
			echo $flag;
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function update_filter_selected(){
		if($this->session->userdata("logged_in")){		
			$selected_list=$this->input->post("selected_list");
			$active=$this->input->post("active");
			$flag_delete=$this->update_search_index_by_filter_id_selected($selected_list);	
			if($flag_delete==true){	
				$flag=$this ->Model_catalogue->update_filter_selected($selected_list,$active);
				if($flag==true){
					echo true;	
				}else{
					echo 0;
				}
			}else{
				echo 0;
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function delete_filter_when_no_data(){
		if($this->session->userdata("logged_in")){			
			$selected_list=$this->input->post("selected_list");
			$flag=$this->Model_catalogue->delete_filter_when_no_data($selected_list);
			echo $flag;
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function specification_group() {
		if($this->session->userdata("logged_in")){
			$data['filterbox_id']=$this->input->post("filterbox_id");	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['specification_group'] = $this->Model_catalogue->specification_group();
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data['create_editview']=$this->input->post("create_editview");
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/specification_group_view',$data);		
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function specification_group_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['specification_group_id']=$this->input->post("specification_group_id");	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/specification_group_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function archived_specification_group(){
		if($this->session->userdata("logged_in")){
			$data['active']=$this->input->post("active");
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['specification_group'] = $this->Model_catalogue->specification_group();
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_specification_group_view',$data);	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function archived_specification_group_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['specification_group_id']=$this->input->post("specification_group_id");	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_specification_group_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function add_specification_group(){
	
		if($this->session->userdata("logged_in")){
			$specification_group_name=$this->db->escape_str($this->input->post("specification_group_name"));
			$sort_order=$this->input->post("sort_order");
			$cat_id=$this->input->post("categories");
			$subcat_id=$this->input->post("subcategories");
			$pcat_id=$this->input->post("parent_category");
			$pcat_view=$this->input->post("pcat_view");
			$cat_view=$this->input->post("cat_view");
			$subcat_view=$this->input->post("subcat_view");
			if(isset($pcat_view)){$pcat_view=1;}else{$pcat_view=0;}
			if(isset($cat_view)){$cat_view=1;}else{$cat_view=0;}
			if(isset($subcat_view)){$subcat_view=1;}else{$subcat_view=0;}
			
			$view_specification_group=$this->input->post("view");
			
			$update_flag=$this->Model_catalogue->add_specification_group($specification_group_name,$sort_order,$subcat_id,$cat_id,$pcat_id,$view_specification_group,$pcat_view,$cat_view,$subcat_view);
			if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			
	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_specification_group_form(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$pcat_id=$this->input->post("pcat_id");
			$cat_id=$this->input->post("cat_id");
			$subcat_id=$this->input->post("subcat_id");
			$specification_group_id=$this->input->post("specification_group_id");
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_catagories'] = $this->Model_catalogue->parent_category();
			$data['create_editview']=$this->input->post("create_editview");
			$result_type='';
			if($pcat_id!=0 && $cat_id==0 && $subcat_id==0){
			$result_type="pcat_view";
			}
			if($pcat_id!=0 && $cat_id!=0 && $subcat_id==0){
			$result_type="cat_view";
			}
			if($pcat_id!=0 && $cat_id!=0 && $subcat_id!=0){
			$result_type="subcat_view";
			}
			$data["get_specification_group_data"]=$this->Model_catalogue->get_specification_group_data($pcat_id,$cat_id,$subcat_id,$specification_group_id);
			$get_specification_group_data=$data["get_specification_group_data"];
			
			$data["get_sort_order_options_for_specification_group"]=$this->get_sort_order_options_for_specification_group($pcat_id,$cat_id,$subcat_id);
			
			$data["filterbox_sort_order_arr"]=$this->Model_catalogue->show_specification_group_edit_sort_order($data["get_specification_group_data"]["sort_order"],$data["get_specification_group_data"]["pcat_id"],$data["get_specification_group_data"]["cat_id"],$data["get_specification_group_data"]["subcat_id"],$result_type);
			$filterbox_sort_order_arr=$data["filterbox_sort_order_arr"];
			$data["pcat_id"]=$pcat_id;
			$data["cat_id"]=$cat_id;
			$data["subcat_id"]=$subcat_id;
			$data["specification_group_id"]=$specification_group_id;
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_specification_group_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_sort_order_options_for_specification_group($pcat_id,$cat_id,$subcat_id){
		if($this->session->userdata("logged_in")){	
			$get_specification_group_pcat_arr=$this->Model_catalogue->sort_order_specification_pcat($pcat_id,$cat_id,$subcat_id);
				$pcat_id_arr=array();				
				if($get_specification_group_pcat_arr!=false){
					foreach($get_specification_group_pcat_arr as $specification_group_parent_category_arr){
						$pcat_id_arr[]=$specification_group_parent_category_arr["sort_order"];
					}
				}
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				if(count($miss_arr)>0){
					$specification_group_arr["key"]="Sort Order Suggestion";
					$specification_group_arr["value"]=implode(",",$miss_arr);
				}
				else{
					$specification_group_arr["key"]="Sort Order Suggestion";
					$specification_group_arr["value"]=max($pcat_id_arr)+1;
				}
				}else{
					$specification_group_arr["key"]="";
					$specification_group_arr["value"]="This is the first specification group";
				}
			return $specification_group_arr;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_specification_group()
	{
		if($this->session->userdata("logged_in")){
			
			$specification_group_id=$this->input->post("edit_specification_group_id");		
			$specification_group_name=$this->db->escape_str($this->input->post("edit_specification_group_name"));
			$specification_group_name_default=$this->input->post("edit_specification_group_name_default");
			$sort_order=$this->input->post("edit_sort_order");			
			$subcat_id=$this->input->post("edit_subcategories");
			$cat_id=$this->input->post("edit_categories");
			$pcat_id=$this->input->post("edit_parent_category");
			$pcat_view=$this->input->post("edit_pcat_view");
			$cat_view=$this->input->post("edit_cat_view");
			$subcat_view=$this->input->post("edit_subcat_view");
			$view_specification_group=$this->input->post("edit_view");
			$common_specification_group=$this->input->post("common_specification_group");
			$sort_order_default=$this->input->post("edit_sort_order_default");
			
			$flag=$this->Model_catalogue->edit_specification_group($specification_group_id,$specification_group_name,$sort_order,$subcat_id,$cat_id,$pcat_id,$view_specification_group,$pcat_view,$cat_view,$subcat_view,$common_specification_group);
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function update_specification_group_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$active=$this->input->post("active");
			$flag=$this -> Model_catalogue->update_specification_group_selected($selected_list,$active);
			if($flag==true){	
				echo true;
			}else{
				echo 0;
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function delete_specification_group_when_no_data(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_specification_group_when_no_data($selected_list);
			echo $flag;
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function specification(){
		if($this->session->userdata("logged_in")){
			$data['specification_group_id']=$this->input->post("specification_group_id");	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['specification_group'] = $this->Model_catalogue->specification_group();
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['create_editview']=$this->input->post("create_editview");
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/specification_view',$data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function specification_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['specification_group_id']=$this->input->post("specification_group_id");	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/specification_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function archived_specification(){
		if($this->session->userdata("logged_in")){
			$data['active']=$this->input->post("active");
			$data['specification_group_id']=$this->input->post("specification_group_id");	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_specification_view',$data);	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function archived_specification_filter(){
		if($this->session->userdata("logged_in")){
			echo $data['specification_group_id']=$this->input->post("specification_group_id");	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_specification_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function add_specification(){
	
		if($this->session->userdata("logged_in")){
			$specification_name=$this->db->escape_str($this->input->post("specification_name"));
			$specification_value=$this->db->escape_str($this->input->post("specification_value"));
			$specification_group_id=$this->input->post("specification_group");
			$sort_order=$this->input->post("sort_order");
			$cat_id=$this->input->post("categories");
			$subcat_id=$this->input->post("subcategories");
			$pcat_id=$this->input->post("parent_category");
			$view_specification=$this->input->post("view");
			$view_comparision=$this->input->post("view");
			$flag=$this->Model_catalogue->check_duplication_name_specification($specification_name,$specification_group_id);
			if($flag==true){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->add_specification($specification_name,$specification_value,$specification_group_id,$sort_order,$subcat_id,$cat_id,$pcat_id,$view_specification,$view_comparision);
				if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_specification_form(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$pcat_id=$this->input->post("pcat_id");
			$cat_id=$this->input->post("cat_id");
			$subcat_id=$this->input->post("subcat_id");
			$specification_id=$this->input->post("specification_id");
			$specification_group_id=$this->input->post("specification_group_id");
			$data['specification_group'] = $this->Model_catalogue->specification_group();
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_catagories'] = $this->Model_catalogue->parent_category();
			$data['create_editview']=$this->input->post("create_editview");
			$data["get_specification_data"]=$this->Model_catalogue->get_specification_data($specification_id,$cat_id,$subcat_id,$pcat_id);
			$get_specification_data=$data["get_specification_data"];
			$data["get_sort_order_options_for_specification"]=$this->get_sort_order_options_for_specification($specification_group_id);
			$data["specification_sort_order_arr"]=$this->Model_catalogue->show_specification_edit_sort_order($data["get_specification_data"]["specification_sort_order"],$data["get_specification_data"]["specification_group_id"]);
			$specification_sort_order_arr=$data["specification_sort_order_arr"];
			$data["pcat_id"]=$pcat_id;
			$data["cat_id"]=$cat_id;
			$data["subcat_id"]=$subcat_id;
			$data["specification_group_id"]=$specification_group_id;
			$data["specification_id"]=$specification_id;
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_specification_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_sort_order_options_for_specification($specification_group_id)
	{
		if($this->session->userdata("logged_in")){
			$get_all_sub_category_arr=$this->Model_catalogue->get_all_specification_sort_order($specification_group_id);
				$filter_id_arr=array();
				
				foreach($get_all_sub_category_arr as $sub_category_arr){
					$filter_id_arr[]=$sub_category_arr["sort_order"];
				}
				
				if(count($filter_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($filter_id_arr);$i++){
					$base_arr[]=$i;
				}
				
				$miss_arr=array_diff($base_arr,$filter_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$filter_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				if(count($miss_arr)>0){
					$specification_arr["key"]="Sort Order Suggestion";
					$specification_arr["value"]=implode(",",$miss_arr);
				}
				else{
					$specification_arr["key"]="Sort Order Suggestion";
					$specification_arr["value"]=max($filter_id_arr)+1;
				}
				}else{
					$specification_arr["key"]="";
					$specification_arr["value"]="No specification";
				}
			return $specification_arr;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_specification()
	{
		if($this->session->userdata("logged_in")){
			
			$specification_id=$this->input->post("edit_specification_id");
			$specification_name=$this->db->escape_str($this->input->post("edit_specification_name"));
			$specification_name_default=$this->input->post("edit_specification_name_default");
			$specification_value=$this->input->post("edit_specification_value");
			$specification_group_id=$this->input->post("edit_specification_group");
			$sort_order=$this->input->post("edit_sort_order");
			$cat_id=$this->input->post("edit_categories");
			$subcat_id=$this->input->post("edit_subcategories");
			$pcat_id=$this->input->post("edit_parent_category");
			$view_specification=$this->input->post("edit_view");
			$common_specification=$this->input->post("common_specification");
			$sort_order_default=$this->input->post("edit_sort_order_default");
			$flag_check="";
			if($specification_name_default!=$specification_name){
			$flag_check=$this->Model_catalogue->check_duplication_name_specification($specification_name,$specification_group_id);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
			$flag=$this->Model_catalogue->edit_specification($specification_id,$specification_name,$specification_value,$specification_group_id,$sort_order,$subcat_id,$cat_id,$pcat_id,$view_specification,$common_specification);
			echo $flag;
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function update_specification_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$active=$this->input->post("active");
			$flag=$this -> Model_catalogue-> update_specification_selected($selected_list,$active);
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function delete_specification_selected_when_no_data(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_specification_selected_when_no_data($selected_list);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function products(){
		if($this->session->userdata("logged_in")){
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['subcat_id']=$this->input->post("subcat_id");				
			$data['brand_id']=$this->input->post("brand_id");
			
			$data['products'] = $this->Model_catalogue->products();
			$data['brands'] = $this->Model_catalogue->brands();
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data['create_editview']=$this->input->post("create_editview");
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/products_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function products_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['product_id']=$this->input->post("product_id");
			$data['brand_id']=$this->input->post("brand_id");	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/products_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_product_form(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$pcat_id=$this->input->post("pcat_id");
			$cat_id=$this->input->post("cat_id");
			$subcat_id=$this->input->post("subcat_id");
			$brand_id=$this->input->post("brand_id");
			$product_id=$this->input->post("product_id");
			$data['products'] = $this->Model_catalogue->products();
			$data['brands'] = $this->Model_catalogue->brands();
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_catagories'] = $this->Model_catalogue->parent_category();
			$data['create_editview']=$this->input->post("create_editview");
			$data["get_products_data"]=$this->get_products_data($pcat_id,$product_id,$cat_id,$subcat_id,$brand_id);
			$get_products_data=$data["get_products_data"];
			$data["pcat_id"]=$pcat_id;
			$data["cat_id"]=$cat_id;
			$data["subcat_id"]=$subcat_id;
			$data["brand_id"]=$brand_id;			
			$data["product_id"]=$product_id;
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_products_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function archived_products(){
		if($this->session->userdata("logged_in")){
			$data['active']=$this->input->post("active");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['subcat_id']=$this->input->post("subcat_id");				
			$data['brand_id']=$this->input->post("brand_id");
			$data['brand_active']=$this->input->post("brand_active");
			$data['product_active']=$this->input->post("product_active");
			
			$data['products'] = $this->Model_catalogue->products();
			$data['brands'] = $this->Model_catalogue->brands();
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_products_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function archived_products_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['product_id']=$this->input->post("product_id");
			$data['brand_id']=$this->input->post("brand_id");	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data["menu_flag"]="catalog_active_links_archived";
			$data['parent_category'] = $this->Model_catalogue->parent_category();	
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_products_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function add_products(){
	
		if($this->session->userdata("logged_in")){
			$product_name=$this->db->escape_str($this -> input -> post("product_name"));
			$product_description=$this->db->escape_str($this -> input -> post("product_description"));
			$sugg1=$this->db->escape_str($this -> input -> post("sugg1"));
			$sugg2=$this->db->escape_str($this -> input -> post("sugg2"));
			$generalsugg1=$this->db->escape_str($this -> input -> post("generalsugg1"));
			$generalsugg2=$this->db->escape_str($this -> input -> post("generalsugg2"));
			$generalsugg3=$this->db->escape_str($this -> input -> post("generalsugg3"));
			$generalsugg4=$this->db->escape_str($this -> input -> post("generalsugg4"));
			$cat_id=$this->input->post("categories");
			$subcat_id=$this->input->post("subcategories");
			$pcat_id=$this->input->post("parent_category");
			$brand_id=$this->input->post("brands");
			$view=$this->input->post("view");
			$flag=$this->Model_catalogue->check_duplication_name_product($product_name,$pcat_id,$cat_id,$subcat_id,$brand_id);
			if($flag==true){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->add_products($product_name,$product_description,$sugg1,$sugg2,$generalsugg1,$generalsugg2,$generalsugg3,$generalsugg4,$brand_id,$subcat_id,$cat_id,$pcat_id,$view);
				if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}
	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_products_data($pcat_id,$product_id,$cat_id,$subcat_id,$brand_id){
		if($this->session->userdata("logged_in")){
			$products_arr=$this->Model_catalogue->get_products_data($pcat_id,$product_id,$cat_id,$subcat_id,$brand_id);
			return $products_arr;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_products(){
		if($this->session->userdata("logged_in")){
			
			$product_id=$this->input->post("edit_product_id");
			$product_name=$this->db->escape_str($this -> input -> post("edit_product_name"));
			$product_name_default=$this->db->escape_str($this -> input -> post("edit_product_name_default"));
			$product_description=addslashes($this -> input -> post("edit_product_description"));
			$sugg1=$this->db->escape_str($this -> input -> post("edit_sugg1"));
			$sugg2=$this->db->escape_str($this -> input -> post("edit_sugg2"));
			$generalsugg1=$this->db->escape_str($this -> input -> post("edit_generalsugg1"));
			$generalsugg2=$this->db->escape_str($this -> input -> post("edit_generalsugg2"));
			$generalsugg3=$this->db->escape_str($this -> input -> post("edit_generalsugg3"));
			$generalsugg4=$this->db->escape_str($this -> input -> post("edit_generalsugg4"));
			$cat_id=$this->input->post("edit_categories");
			$subcat_id=$this->input->post("edit_subcategories");
			$pcat_id=$this->input->post("edit_parent_category");
			$brand_id=$this->input->post("edit_brands");
			$view=$this->input->post("edit_view");
			$flag_check="";
			if($product_name_default!=$product_name){
			$flag_check=$this->Model_catalogue->check_duplication_name_product($product_name,$pcat_id,$cat_id,$subcat_id,$brand_id);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
			$flag=$this->Model_catalogue->edit_products($product_id,$product_name,$product_description,$sugg1,$sugg2,$generalsugg1,$generalsugg2,$generalsugg3,$generalsugg4,$brand_id,$subcat_id,$cat_id,$pcat_id,$view);
			if($flag==true){
				$updated_flag=$this->update_product_search_index($product_id,$product_name,$sugg1,$sugg2,$generalsugg1,$generalsugg2,$generalsugg3,$generalsugg4,$brand_id,$subcat_id,$cat_id,$pcat_id);
			
				if($updated_flag==true){
					echo true;
				}else{
					echo 0;
				}
			}else{
				echo 0;
			}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function update_products_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$active=$this->input->post("active");
			$flag=$this -> Model_catalogue->update_products_selected($selected_list,$active);
			if($flag==true){
				if($active==0){
					$search_flag=$this->delete_search_index_by_product_id_selected($selected_list);
				}else{
					$search_flag=$this->restore_search_index_by_product_id_selected($selected_list);
				}	
				if($search_flag==true){
					echo true;
				}else{
					echo 0;
				}
			}else{
				echo 0;
			}	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function delete_products_when_no_data(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this ->Model_catalogue->delete_products_when_no_data($selected_list);
			if($flag==true){
				echo true;		
			}else{
				echo 0;
			}	
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	
	public function inventory_filter(){
		
		if($this->session->userdata("logged_in")){		
			$data['product_id']=$this->input->post("product_id");
			$data['brand_id']=$this->input->post("brand_id");
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['attributes'] = $this->Model_catalogue->get_all_attributes($data['subcat_id']);
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/inventory_pro_filter',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_filters_by_subcat(){
		$subcat_id=$this->input->post("subcat_id");
		$attributes = $this->Model_catalogue->get_all_attributes($subcat_id);
		?>
		
		<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>

		<div class="col-md-10 text-center">	
			<h4>Filter By Attributes</h4>
		</div>
		  <?php
		  foreach($attributes as $attributes_value)
		  {
		  ?>
	<div class="row">
		<div class="col-md-10">  
		<div class="form-group label-floating">
		
		<input type="hidden" name="search_attribute_1" value="<?php echo $attributes_value->attribute1_name;?>">
			
			<?php $attribute_values=explode(',',$attributes_value->attribute1_option); ?>
			<label class="control-label">-Select <?php echo $attributes_value->attribute1_name;?>-</label>
            <select name="search_attribute_1_value" id="search_attribute_1_value" class="form-control" onchange="drawtable(this)">
				<option value=""></option>
				<?php
					foreach($attribute_values as $val)
					{
						echo '<option value="'.$val.'">'.$val.'</option>';
					}
				?>
			</select>
			
        </div>
		</div>
    </div> 
		  <?php if(!empty($attributes_value->attribute2_name)){ ?>
	<div class="row">
		<div class="col-md-10">   
		<div class="form-group label-floating">
		<label class="control-label">-Select <?php echo $attributes_value->attribute2_name;?>-</label>
			<input type="hidden" name="search_attribute_2" value="<?php echo $attributes_value->attribute2_name;?>">
			
			<?php $attribute_values=explode(',',$attributes_value->attribute2_option); ?>
            <select name="search_attribute_2_value" id="search_attribute_2_value" class="form-control" onchange="drawtable(this)">
				<option value=""></option>
				<?php
					foreach($attribute_values as $val)
					{
						echo '<option value="'.$val.'">'.$val.'</option>';
					}
				?>
			</select>
			
        </div>
		</div>
	</div>
		 <?php }else{ ?>	 
		  
			  <input type="hidden" name="search_attribute_2" value="">
			  <input  type="hidden" name="search_attribute_2_value" id="search_attribute_2_value"  value="">
			   <?php
		  } ?>
		  
		   <?php if(!empty($attributes_value->attribute3_name)){ ?>
	<div class="row">
		<div class="col-md-10"> 	   
		<div class="form-group label-floating">
		<label class="control-label">-Select <?php echo $attributes_value->attribute3_name;?>-</label>
			<input type="hidden" name="search_attribute_3" id="search_attribute_3" value="<?php echo $attributes_value->attribute3_name;?>">

			<?php $attribute_values=explode(',',$attributes_value->attribute3_option); ?>
            <select name="search_attribute_3_value" id="search_attribute_3_value" class="form-control" onchange="drawtable(this)">
				<option value=""></option>
				<?php
					foreach($attribute_values as $val)
					{
						echo '<option value="'.$val.'">'.$val.'</option>';
					}
				?>
			</select>
			
        </div>
		</div>
    </div>
		  <?php }else{ ?>	 
		  
			<input type="hidden" name="search_attribute_3" value="">
			<input type="hidden" name="search_attribute_3_value" id="search_attribute_3_value" value="">
			   <?php
		  } ?>
		  
		  <?php if(!empty($attributes_value->attribute4_name)){ ?>
	<div class="row">
		<div class="col-md-10"> 	  
		<div class="form-group label-floating">
		<label class="control-label">-Select <?php echo $attributes_value->attribute4_name;?>-</label>
			<input type="hidden" name="search_attribute_4" value="<?php echo $attributes_value->attribute4_name;?>">
			<?php $attribute_values=explode(',',$attributes_value->attribute4_option); ?>
            <select name="search_attribute_4_value" id="search_attribute_4_value" class="form-control" onchange="drawtable(this)">
				<option value=""></option>
				<?php
					foreach($attribute_values as $val)
					{
						echo '<option value="'.$val.'">'.$val.'</option>';
					}
				?>
			</select>
        </div>
		</div>
    </div>
			<?php }else{ ?>	 
		  
			<input type="hidden" name="search_attribute_4"  value="">
			<input type="hidden" name="search_attribute_4_value" id="search_attribute_4_value"  value="">
			   <?php
			} 
		  
			} ?>
	<div class="row">
		<div class="col-md-10"> 	  
		<div class="form-group label-floating">
				<label class="control-label">-Select Stock Status-</label>
				<select name="search_product_status" id="search_product_status" class="form-control search_select" onchange="drawtable(this)">
						<option value="" selected></option>
						<option value="In Stock">In Stock</option>
						<option value="Out Of Stock">Out Of Stock</option>
						<option value="Imported Stock">Imported Stock</option>
				</select>
		</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10"> 
		<div class="form-group label-floating">
		<label class="control-label">-Select Inventory appearance-</label>		
				<select name="search_view_product_status" id="search_view_product_status" class="form-control search_select" onchange="drawtable(this)">
						<option value="" selected></option>
						<option value="1">Show</option>
						<option value="0">hide</option>
				</select>
		</div>
		</div>
	</div>
	
		<?php
	}
	
	public function edit_inventory_form($inventory_id,$product_id){
		if($this->session->userdata("logged_in")){
		$data["controller"]=$this;
		$data['id']=$inventory_id;	
		$data['product_id']=$product_id;
		$get_product_details_arr=$this->Model_catalogue->get_product_details($product_id);
		
		$data['pcat_id']=$get_product_details_arr["pcat_id"];
		$data['cat_id']=$get_product_details_arr["cat_id"];	
		$data['subcat_id']=	$get_product_details_arr["subcat_id"];		
		$data['brand_id']=$get_product_details_arr["brand_id"];	
		$inventory_positioning = $this->Model_catalogue->get_inventory_positioning($inventory_id);
		$data["inventory_positioning"]=$inventory_positioning;
		$get_inventory_details_arr = $this->Model_catalogue->get_inventory_details($inventory_id);

		$data["cat_structure"]=$this->Model_catalogue->get_all_tree_data_by_product($data['product_id']);
		

		
		$inventory_type=$get_inventory_details_arr["inventory_type"];
		$data['inventory_type']=$inventory_type;

		$data["sku_id"]=$get_inventory_details_arr["sku_id"];
		$data['inventory'] = $this->Model_catalogue->inventory($product_id);
		$data['parent_category'] = $this->Model_catalogue->parent_category();
		$data['create_editview']=$this->input->post("create_editview");
		$data['attributes'] = $this->Model_catalogue->get_all_attributes($get_product_details_arr["subcat_id"]);
		
		$vendor_id=1;
		$data['vendors']=$this->Model_catalogue->vendors();
		$data['get_all_specification_group'] = $this->Model_catalogue->get_all_specification_group($get_product_details_arr["pcat_id"],$get_product_details_arr["cat_id"],$get_product_details_arr['subcat_id']);
		$data['filterbox'] = $this->Model_catalogue->get_all_filterbox($get_product_details_arr["pcat_id"],$get_product_details_arr["cat_id"],$get_product_details_arr['subcat_id']);
		$data['logistics_parcel_category']=$this->Model_catalogue->logistics_parcel_category();
		$data['logistics_all_territory_name'] = $this->Model_catalogue->logistics_all_territory_name();
		/////////////////////
		$inventory_data_arr=$this->get_all_inventory_data($product_id,$inventory_id);

		//print_r($inventory_data_arr);
		//exit;

		$data["inventory_data_arr"]=$inventory_data_arr;
		
		$get_all_specification_by_inventory_id_arr=$this->get_all_specification_by_inventory_id($inventory_id);
		$data["get_all_specification_by_inventory_id_arr"]=$get_all_specification_by_inventory_id_arr;
		
		$get_all_filter_by_inventory_id_arr=$this->get_all_filter_by_inventory_id($inventory_id);
		$data["get_all_filter_by_inventory_id_arr"]=$get_all_filter_by_inventory_id_arr;
		$data["menu_flag"]="catalog_active_links";
		
		
		

		/* finding promotions */

		
			/* for Promotions display */
			$inventory_id=$inventory_id;
			$product_id=$product_id;
        
			if($inventory_type!='2'){
				$promo_array=$this->get_inv_promo_details($inventory_id,$product_id);
			}else{
				$promo_array=[];
			}
        
		/* promo str */
		$promo_str=''; $qty_arr=array();$active_qty_promo_exist=0;
		//print_r($promo_array);

		if(!empty($promo_array)){
			foreach($promo_array as $promo_arr){
				if(!empty($promo_arr)){
					
					foreach($promo_arr as $promo){
						$promo_str.=$promo->promo_quote.'<br>';
						$promo_str.="Period :".date('d M, Y H:i:s',strtotime($promo->promo_start_date)).' to '.date('d M, Y H:i:s',strtotime($promo->promo_end_date)).'<br>';
						$st=($promo->promo_active=='1') ? 'Active'  : 'In Active';
						if($promo->promo_active=='1' && $promo->buy_type=='Qty' && (strpos($promo->get_type, '%') !== false)){
							$active_qty_promo_exist++;
							$qty_arr[]=$promo->to_buy;
						}
						$promo_str.='Status:'.$st.'<br>';
						$promo_str.='Expired Status : '.date_status($promo->promo_start_date,$promo->promo_end_date);
						$promo_str.='<hr>';
					}
				}

			}
		}

		$active_qty_promo_exist;
		$qty_arr;
		
		$promo_qty_min = (!empty($qty_arr)) ? min($qty_arr) : '';

		if($promo_str!=''){
			$promo_str='<b><u>Promo Details</u></b>'.'<br>'.$promo_str.'';
		}else{
			$promo_str='No Promotions applied';
		}
		/* promo str */


		$data["active_qty_promo_exist"]=$active_qty_promo_exist;
		$data["promo_qty_arr"]=$qty_arr;
		$data["promo_qty_min"]=$promo_qty_min;

		/* finding promotions */

		//////
		$this -> load -> view('admin/vmanage',$data);
		if($inventory_type!='2'){
			$this -> load -> view('admin/edit_inventory_view',$data);	
		}else{
			$this -> load -> view('admin/edit_inventory_view_addon',$data);	
		}	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_inv_promo_details($inventory_id,$product_id){
		$promo_array=array();$promo_uid_arr=array();
        if(!empty($product_id)){
            $a_TO_z_ids=$this->Front_promotions->get_a_TO_z_id($product_id);
            
            if(!empty($a_TO_z_ids)){
                foreach($a_TO_z_ids as $a_TO_z_id){
                    $brand_id=$a_TO_z_id['brand_id'];
                    $subcat_id=$a_TO_z_id['subcat_id'];
                    $cat_id=$a_TO_z_id['cat_id'];
                    $pcat_id=$a_TO_z_id['pcat_id'];
                }
                
                $product_level_promotions_uid=$this->Front_promotions->product_level_promotion_uid($product_id);
                $brand_level_promotions_uid=$this->Front_promotions->brand_level_promotion_uid($brand_id);
                $subcat_level_promotions_uid=$this->Front_promotions->subcat_level_promotion_uid($subcat_id);
                $cat_level_promotions_uid=$this->Front_promotions->cat_level_promotion_uid($cat_id);
                $pcat_level_promotions_uid=$this->Front_promotions->pcat_level_promotion_uid($pcat_id);
                $store_level_promotions_uid=$this->Front_promotions->store_level_promotion_uid();
               
                if(!empty($product_level_promotions_uid)){
                    foreach($product_level_promotions_uid as $product_level_promotion){
                        $promo_data=$this->Front_promotions->product_level_promotion($product_level_promotion['promo_uid'],$product_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
							$promo_uid_arr[]=$product_level_promotion['promo_uid'];
                        }
                    }
                    
                }
                
                if(!empty($brand_level_promotions_uid)){
                    
                    foreach($brand_level_promotions_uid as $brand_level_promotion){
                        $promo_data=$this->Front_promotions->brand_level_promotion($brand_level_promotion['promo_uid'],$brand_id);
                        if(!empty($promo_data)){
							if(!in_array($brand_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$brand_level_promotion['promo_uid'];
							}
						}
                    }
                }
                if(!empty($subcat_level_promotions_uid)){
                    foreach($subcat_level_promotions_uid as $subcat_level_promotion){
                        $promo_data=$this->Front_promotions->subcat_level_promotion($subcat_level_promotion['promo_uid'],$subcat_id);
						if(!empty($promo_data)){
							if(!in_array($subcat_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$subcat_level_promotion['promo_uid'];
							}
							
						}
                    }
                }
                if(!empty($cat_level_promotions_uid)){
                    foreach($cat_level_promotions_uid as $cat_level_promotion){
                        $promo_data=$this->Front_promotions->cat_level_promotion($cat_level_promotion['promo_uid'],$cat_id);
                        if(!empty($promo_data)){
							if(!in_array($cat_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$cat_level_promotion['promo_uid'];
							}
						}
                    }
                }
                if(!empty($pcat_level_promotions_uid)){
                    foreach($pcat_level_promotions_uid as $pcat_level_promotion){
                        $promo_data=$this->Front_promotions->pcat_level_promotion($pcat_level_promotion['promo_uid'],$pcat_id);
						if(!empty($promo_data)){
							if(!in_array($pcat_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$pcat_level_promotion['promo_uid'];
							}
							
						}
                    }
                }
                if(!empty($store_level_promotions_uid)){
					
                    foreach($store_level_promotions_uid as $store_level_promotion){
                        $promo_data=$this->Front_promotions->store_level_promotion($store_level_promotion['promo_uid']);
						if(!empty($promo_data)){
							if(!in_array($store_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$store_level_promotion['promo_uid'];
							}
						}
                    }
                }
            }
        }
        $promo_uid=$this->Front_promotions->get_promotions($inventory_id);
        
        if(!empty($promo_uid)){
            foreach($promo_uid as $res){
                $promo_data=$this->Front_promotions->get_all_promotions_data($res['promo_uid'],$inventory_id);
                if(!empty($promo_data)){
					if(!in_array($res['promo_uid'],$promo_uid_arr)){
						array_push($promo_array,$promo_data);
						$promo_uid_arr[]=$res['promo_uid'];
					}
				}
            }
        }
        return $promo_array;
	}
	
	public function inventory(){
		
		if($this->session->userdata("logged_in")){

            $data_posted=$this->input->post();

            if($this->session->flashdata('inv_post_data')){

                $data_posted=$this->session->flashdata('inv_post_data');
                $data['import_success']=1;
                $data['import_success_msg']=$this->session->flashdata('import_msg');
               // print_r($data);exit;
            }

            if($this->input->post("export_inventory")){
                //print_r($this->input->post());
                $this->export_inventory($this->input->post());
            }

			$data["controller"]=$this;
			$data['product_id']=$data_posted["products"];
			$data['brand_id']=$data_posted["brands"];
			$data['subcat_id']=$data_posted["subcategories"];
			$data['cat_id']=$data_posted["categories"];
			$data['pcat_id']=$data_posted["parent_category"];
			$data['inventory_type']=$data_posted["inventory_type"];
			$brand_data=$this->Model_catalogue->get_brand_data($data['pcat_id'],$data['cat_id'],$data['subcat_id'],$data['brand_id']);
			$data['brand_data']=$brand_data;

			$data['inventory'] = $this->Model_catalogue->inventory($data['product_id']);
			$data['product_details'] = $this->Model_catalogue->get_product_details($data['product_id']);
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data['attributes'] = $this->Model_catalogue->get_all_attributes($data['subcat_id']);
			$vendor_id=1;
			//$data['pcat_cat_subcat_view'] = $this->Model_catalogue->pcat_cat_subcat_view();
			$data['vendors']=$this->Model_catalogue->vendors();
			$data['get_all_specification_group'] = $this->Model_catalogue->get_all_specification_group($data['pcat_id'],$data['cat_id'],$data['subcat_id']);
			$data['filterbox'] = $this->Model_catalogue->get_all_filterbox($data['pcat_id'],$data['cat_id'],$data['subcat_id']);
			$data['logistics_parcel_category']=$this->Model_catalogue->logistics_parcel_category();
			$data['logistics_all_territory_name'] = $this->Model_catalogue->logistics_all_territory_name();
			$data['create_editview']=$data_posted["create_editview"];
			////
			if(isset($data_posted["search_attribute_1_value"])){
				$data['search_attribute_1_value']=$data_posted["search_attribute_1_value"];
			}
			else{
				$data['search_attribute_1_value']='';
			}
			if(isset($data_posted["search_attribute_2_value"])){
				$data['search_attribute_2_value']=$data_posted["search_attribute_2_value"];
			}
			else{
				$data['search_attribute_2_value']='';
			}
			if(isset($data_posted["search_attribute_3_value"])){
				$data['search_attribute_3_value']=$data_posted["search_attribute_3_value"];
			}
			else{
				$data['search_attribute_3_value']='';
			}
			if(isset($data_posted["search_attribute_4_value"])){
				$data['search_attribute_4_value']=$data_posted["search_attribute_4_value"];
			}
			else{
				$data['search_attribute_4_value']='';
			}
			if(isset($data_posted["search_product_status"])){
				$data['search_product_status']=$data_posted["search_product_status"];
			}
			else{
				$data['search_product_status']='';
			}
			if(isset($data_posted["search_view_product_status"])){
				$data['search_view_product_status']=$data_posted["search_view_product_status"];
			}
			else{
				$data['search_view_product_status']='';
			}
			$data["menu_flag"]="catalog_active_links";
			

			////

			$data["cat_structure"]=$this->Model_catalogue->get_all_tree_data_by_product($data['product_id']);
		
			$this -> load -> view('admin/vmanage',$data);
			if($data_posted["create_editview"]=='create' && $data_posted["inventory_type"]=='2'){
				/* for addon setup */
				$this -> load -> view('admin/inventory_view_addon',$data);
			}else{
				$this -> load -> view('admin/inventory_view',$data);
			}
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function export_inventory($arr){

        $inventory_details = $this->Model_catalogue->get_inventory_details_result($arr['products']);
        $product_details = $this->Model_catalogue->get_product_details_result($arr['products']);

        $header_top =array("Product name","Product description","Subcategory name","Category name","Parent category name","Brand name");
        /* file name */
        $product_name=($product_details['product_name']) ? '_'.$product_details['product_name']:'';
        $product_name = str_replace(' ', '_', $product_name);
        $filename = 'product_inventory'.$product_name.'_'.date('Ymd').'.csv';
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/csv; ");
        /* get data */
        //$usersData = $this->Crud_model->getUserDetails();
        /* file creation */

        //$usersData=array(array('jon','jhon','male','jhon@gmail.com'));
        //$header = array("Username","Name","Gender","Email");

        $file = fopen('php://output','w');

        $header = array("sku_id", "attribute_1", "attribute_1_value", "attribute_2", "attribute_2_value", "attribute_3", "attribute_3_value", "attribute_4", "attribute_4_value", "tax_percent", "base_price", "selling_price", "purchased_price", "mrp_quantity", "moq", "moq_base_price", "moq_price", "moq_purchased_price", "max_oq", "num_sales", "stock", "low_stock", "cutoff_stock");

        //fputcsv($file, $header_top);
        //fputcsv($file, $product_details);
        fputcsv($file, $header);
        foreach ($inventory_details as $key=>$line){
            fputcsv($file,$line);
        }
        fclose($file);
        exit;
    }

    public function export_inventory_all($arr){
        $data_posted=$arr;
        $product_id=(isset($data_posted["products"])) ? $data_posted["products"]: '';
        $brand_id=(isset($data_posted["brands"])) ? $data_posted["brands"]: '';
        $subcat_id=(isset($data_posted["subcategories"])) ?$data_posted["subcategories"] : '';
        $cat_id=(isset($data_posted["categories"])) ? $data_posted["categories"]: '';
        $pcat_id=(isset($data_posted["parent_category"])) ?$data_posted["parent_category"] : '';

        $inventory_details = $this->Model_catalogue->get_inventory_details_result($product_id,$pcat_id,$cat_id,$subcat_id,$brand_id);
        $product_details = $this->Model_catalogue->get_product_details_result($product_id);

        $header_top =array("Product name","Product description","Subcategory name","Category name","Parent category name","Brand name");
        /* file name */
        $product_name=(isset($product_details['product_name'])) ? '_'.$product_details['product_name']:'';
        $product_name = str_replace(' ', '_', $product_name);
        $filename = 'product_inventory'.$product_name.'_'.date('Ymd').'.csv';
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/csv; ");
        /* get data */
        //$usersData = $this->Crud_model->getUserDetails();
        /* file creation */

        //$usersData=array(array('jon','jhon','male','jhon@gmail.com'));
        //$header = array("Username","Name","Gender","Email");

        $file = fopen('php://output','w');

        $header = array("sku_id", "attribute_1", "attribute_1_value", "attribute_2", "attribute_2_value", "attribute_3", "attribute_3_value", "attribute_4", "attribute_4_value", "tax_percent", "base_price", "selling_price", "purchased_price", "mrp_quantity", "moq", "moq_base_price", "moq_price", "moq_purchased_price", "max_oq", "num_sales", "stock", "low_stock", "cutoff_stock");

        fputcsv($file, $header_top);
        fputcsv($file, $product_details);
        fputcsv($file, $header);
        foreach ($inventory_details as $key=>$line){
            fputcsv($file,$line);
        }
        fclose($file);
        exit;
    }
    public function inventory_import(){
        $data = array();
        $memData = array();

        // If import request is submitted
        $msg='';
        if($this->input->post('importSubmit')){
            // Form field validation rules

            // Validate submitted form data
            if(1){
                $insertCount = $updateCount = $rowCount = $notAddCount = 0;

                // If file uploaded
                if(is_uploaded_file($_FILES['file']['tmp_name'])){
                    // Load CSV reader library
                    $this->load->library('CSVReader');

                    // Parse data from CSV file
                    $csvData = $this->csvreader->parse_csv($_FILES['file']['tmp_name']);

                    // Insert/update CSV data into database

                    if(!empty($csvData)){
                        foreach($csvData as $row){
                            //print_r($row);
                            //exit;
                            $rowCount++;

                            /*(`id`, `product_id`, `sku_id`, `attribute_1`, `attribute_1_value`, `attribute_2`, `attribute_2_value`, `attribute_3`, `attribute_3_value`, `attribute_4`, `attribute_4_value`, `image`, `thumbnail`, `largeimage`, `tax_percent`, `base_price`, `selling_price`, `purchased_price`, `mrp_quantity`, `moq`, `moq_base_price`, `moq_price`, `moq_purchased_price`, `max_oq`, `num_sales`, `stock`, `low_stock`, `cutoff_stock`, `product_status`, `product_status_view`, `vendor_id`, `vendor_removed`, `policy_id`, `default_delivery_date`, `return_period`, `purchased`, `active`, `timestamp`, `image2`, `thumbnail2`, `largeimage2`, `image3`, `thumbnail3`, `largeimage3`, `image4`, `thumbnail4`, `largeimage4`, `image5`, `thumbnail5`, `largeimage5`, `common_image`, `highlight`)*/

                            // Prepare data for DB insertion
                            $invData=array();

                            /*$invData = array(
                                'tax_percent' => $row['tax_percent'],
                                'base_price' => $row['base_price'],
                                'hsncode' => (isset($row['hsncode'])) ? $row['hsncode'] :'',
                                'selling_price' => $row['selling_price'],
                                'purchased_price' => $row['purchased_price'],
                                'mrp_quantity' => $row['mrp_quantity'],
                                'moq' => $row['moq'],
                                'moq_base_price' => $row['moq_base_price'],
                                'moq_price' => $row['moq_price'],
                                'moq_purchased_price' => $row['moq_purchased_price'],
                                'max_oq' => $row['max_oq'],

                            );*/

                            $tax_percent=(isset($row['tax_percent']))  ? $row['tax_percent']: '';
                            $base_price=(isset($row['base_price'])) ? $row['base_price']: '';
                            $hsncode=(isset($row['hsncode'])) ? $row['hsncode'] : '';
                            $selling_price=(isset($row['selling_price'])) ? $row['selling_price']:  '';
                            $purchased_price=(isset($row['purchased_price'])) ? $row['purchased_price']:  '';
                            $moq=(isset($row['moq'])) ? $row['moq']:  '';

                            if($tax_percent!=''){
                                $invData['tax_percent']=$tax_percent;
                            }
                            if($base_price!=''){
                                $invData['base_price']=$base_price;
                            }
                            if($hsncode!=''){
                                $invData['hsncode']=$hsncode;
                            }
                            if($selling_price!=''){
                                $invData['selling_price']=$selling_price;
                            }
                            if($purchased_price!=''){
                                $invData['purchased_price']=$purchased_price;
                            }
							$invData['moq']=$moq;

                            /*price calculation*/
                            $moq_base_price=$base_price;
                            $moq_price=$selling_price;
                            if($moq!='') {
                                $moq_base_price = $moq * $base_price;
                                $moq_price = $moq * $selling_price;
                            }
                            $invData['moq_base_price']=$moq_base_price;
                            $invData['moq_price']=$moq_price;

                            if(trim($purchased_price)==""){
                                $moq_purchased_price="";
                            }
                            else{
                                $moq_purchased_price=$moq*$purchased_price;
                            }
                            $invData['moq_purchased_price']=$moq_purchased_price;


                            // Check whether email already exists in the database
                            $con = array(
                                'where' => array(
                                    'sku_id' => $row['sku_id']
                                ),
                                'returnType' => 'count'
                            );
                            $prevCount = $this->Inventory->getRows($con);

                            if($prevCount > 0){
                                // Update member data
                                $condition = array('sku_id' => $row['sku_id']);
                                $update = $this->Inventory->update($invData, $condition);

                                if($update){
                                    $updateCount++;
                                    $inventory_id=$this->Inventory->get_inventory_id_by_sku($row['sku_id']);
                                    if($inventory_id!=''){
                                        $flag_update_index=$this->update_search_index($inventory_id);
                                        if($flag_update_index==true){
                                            //echo true;
                                        }else{
                                            //echo 0;
                                        }
                                    }
                                }
                            }else{
                                // Insert member data
                                /*  $insert = $this->Model_catalogue->insert($invData);

                                  if($insert){
                                      $insertCount++;
                                  }
                                */
                            }
                        }

                        // Status message with imported data count
                        $notAddCount = ($rowCount - ($insertCount + $updateCount));
                        $msg = 'Inventory imported successfully. Total Rows ('.$rowCount.') | Updated ('.$updateCount.') ';//| Inserted ('.$insertCount.') //| Not Inserted ('.$notAddCount.')'
                        //$this->session->set_userdata('success_msg', $successMsg);
                    }
                }else{
                    $msg= 'Error on file upload, please try again.';
                }
            }else{
                //$this->session->set_userdata('error_msg', 'Invalid file, please select only CSV file.');
                $msg='Invalid file, please select only CSV file.';
            }
        }
        $data = $this->input->post();
// i store data to flashdata
        $this->session->set_flashdata('inv_post_data',$data);
        $this->session->set_flashdata('import_msg',$msg);
// after storing i redirect it to the controller
        redirect('admin/Catalogue/inventory_import_export')->withInput();
    }

    /*
     * Callback function to check file value and type during validation
     */
    public function file_check($str){
        $err='';
        $allowed_mime_types = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != ""){
            $mime = get_mime_by_extension($_FILES['file']['name']);
            $fileAr = explode('.', $_FILES['file']['name']);
            $ext = end($fileAr);
            if(($ext == 'csv') && in_array($mime, $allowed_mime_types)){
                $err=0;
                //return true;
            }else{
               $err='Please select only CSV file to upload.';
                //return false;
            }
        }else{
            $err= 'Please select a CSV file to upload.';
            //return false;
        }
        return $err;
    }

    public function inventory_import_export(){

        if($this->session->userdata("logged_in")){

            $data_posted=$this->input->post();

            if($this->session->flashdata('inv_post_data')){

                $data_posted=$this->session->flashdata('inv_post_data');
                $data['import_success']=1;
                $data['import_success_msg']=$this->session->flashdata('import_msg');
                // print_r($data);exit;
            }

            if($this->input->post("export_inventory")){
                //print_r($this->input->post());
                //$this->export_inventory_all($this->input->post());
                $this->export_inventory_all_new($this->input->post());
            }

            $data["controller"]=$this;
            $data['product_id']=(isset($data_posted["products"])) ? $data_posted["products"] : '';
            $data['brand_id']=(isset($data_posted["brands"])) ? $data_posted["brands"]: '';
            $data['subcat_id']=(isset($data_posted["subcategories"])) ?$data_posted["subcategories"] : '';
            $data['cat_id']=(isset($data_posted["categories"])) ? $data_posted["categories"]: '';
            $data['pcat_id']=(isset($data_posted["parent_category"])) ?$data_posted["parent_category"] : '';

            $data['inventory'] = $this->Model_catalogue->inventory($data['product_id']);
            $data['product_details'] = $this->Model_catalogue->get_product_details($data['product_id']);
            $data['parent_category'] = $this->Model_catalogue->parent_category();

            $vendor_id=1;
            //$data['pcat_cat_subcat_view'] = $this->Model_catalogue->pcat_cat_subcat_view();

            $data['vendors']=$this->Model_catalogue->vendors();
            $data['get_all_specification_group'] = $this->Model_catalogue->get_all_specification_group($data['pcat_id'],$data['cat_id'],$data['subcat_id']);
            ////
            $data["menu_flag"]="catalog_active_links";
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/import_export_inventory_view',$data);

        }else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
	public function archived_inventory(){
		
		if($this->session->userdata("logged_in")){
			$data['active']=$this->input->post("active");
			$data["controller"]=$this;
			$data['product_id']=$this->input->post("product_id");
			$data['brand_id']=$this->input->post("brand_id");
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['inventory'] = $this->Model_catalogue->inventory($data['product_id']);	
			$data['product_active']=$this->input->post("product_active");	
			$data['brand_active']=$this->input->post("brand_active");	
			$data['product_details'] = $this->Model_catalogue->get_product_details($data['product_id']);
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data['attributes'] = $this->Model_catalogue->get_all_attributes($data['subcat_id']);

			$data['search_attribute_1_value']=$this->input->post("search_attribute_1_value");
			$data['search_attribute_2_value']=$this->input->post("search_attribute_2_value");
			$data['search_attribute_3_value']=$this->input->post("search_attribute_3_value");
			$data['search_attribute_4_value']=$this->input->post("search_attribute_4_value");
			$data['search_product_status']=$this->input->post("search_product_status");
			$data['search_view_product_status']=$this->input->post("search_view_product_status");
			$data["menu_flag"]="catalog_active_links_archived";
			////
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_inventory_view',$data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function archived_inventory_filter(){
		
		if($this->session->userdata("logged_in")){		
			
			$data['product_id']=$this->input->post("product_id");
			$data['brand_id']=$this->input->post("brand_id");
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="catalog_active_links_archived";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_inventory_pro_filter',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_all_specification($specification_group_id){
		if($this->session->userdata("logged_in")){
			$data=$this->Model_catalogue->get_all_specification($specification_group_id);
			return $data;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_all_specification_ids(){
		if($this->session->userdata("logged_in")){
			$subcat_id=$this->input->post("subcat_id");
			$data=$this->Model_catalogue->get_all_specification_ids($subcat_id);
			echo json_encode($data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_all_filterbox_ids(){
		if($this->session->userdata("logged_in")){
			$subcat_id=$this->input->post("subcat_id");
			$data=$this->Model_catalogue->get_all_filterbox_ids($subcat_id);
			echo json_encode($data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_all_filter($filterbox_id){
		if($this->session->userdata("logged_in")){
			$data=$this->Model_catalogue->get_all_filter($filterbox_id);
			return $data;
			
		}else{
			redirect(base_url());
		}
	}
	public function add_inventory(){
		
		if($this->session->userdata("logged_in")){
			
			$this->load->library('upload');
			$files = $_FILES;
			
			
			$sku_id=addslashes($this->input->post("sku_id"));
			
			if(!is_dir('assets/pictures/images/products/'.$sku_id)){
					mkdir('assets/pictures/images/products/'.$sku_id);
				}
				

			$common_image='';
			$common_image_name=$files['common_image']['name'];
			if($common_image_name!=""){
				$c_ext = pathinfo($common_image_name, PATHINFO_EXTENSION);
				$c_filen=rand(10,100000);
				
				$_FILES['common_image']['name']=$c_filen.".".$c_ext;
				$_FILES['common_image']['type']= $files['common_image']['type'];
				$_FILES['common_image']['tmp_name']= $files['common_image']['tmp_name'];
				$_FILES['common_image']['error']= $files['common_image']['error'];
				$_FILES['common_image']['size']= $files['common_image']['size'];  
				$this->upload->initialize($this->set_upload_options_config($sku_id,300,366));
				
				if (!$this -> upload -> do_upload("common_image")) {
					echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					$common_image = "assets/pictures/images/products/" .$sku_id."/". $upload_data['file_name'];
				}
			}
			
			
			$image_arr=[];
			$thumbnail_arr=[];
			$largeimage_arr=[];
			
			for($i=1; $i<=6; $i++){

				$filename=$files['image'.$i]['name']; 
				if($filename==""){
					continue;
				}				
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				$filen=rand(10,100000);
				
				$_FILES['image'.$i]['name']=$filen.".".$ext;
				$_FILES['image'.$i]['type']= $files['image'.$i]['type'];
				$_FILES['image'.$i]['tmp_name']= $files['image'.$i]['tmp_name'];
				$_FILES['image'.$i]['error']= $files['image'.$i]['error'];
				$_FILES['image'.$i]['size']= $files['image'.$i]['size'];  
				$this->upload->initialize($this->set_upload_options_config($sku_id,420,512));
				//////////////////////////////
				$filename1=$files['thumbnail'.$i]['name'];  
				$ext1 = pathinfo($filename1, PATHINFO_EXTENSION);
				$filen1=rand(10,100000);
				$_FILES['thumbnail'.$i]['name']= $filen1.".".$ext1;
				$_FILES['thumbnail'.$i]['type']= $files['thumbnail'.$i]['type'];
				$_FILES['thumbnail'.$i]['tmp_name']= $files['thumbnail'.$i]['tmp_name'];
				$_FILES['thumbnail'.$i]['error']= $files['thumbnail'.$i]['error'];
				$_FILES['thumbnail'.$i]['size']= $files['thumbnail'.$i]['size'];
				$this->upload->initialize($this->set_upload_options_config($sku_id,100,122));
				////////////////////
				$filename2=$files['largeimage'.$i]['name'];  
				$ext1 = pathinfo($filename2, PATHINFO_EXTENSION);
				$filen1=rand(10,100000);
				$_FILES['largeimage'.$i]['name']= $filen1.".".$ext1;
				$_FILES['largeimage'.$i]['type']= $files['largeimage'.$i]['type'];
				$_FILES['largeimage'.$i]['tmp_name']= $files['largeimage'.$i]['tmp_name'];
				$_FILES['largeimage'.$i]['error']= $files['largeimage'.$i]['error'];
				$_FILES['largeimage'.$i]['size']= $files['largeimage'.$i]['size'];
				$this->upload->initialize($this->set_upload_options_config($sku_id,850,1036));
				//////////////////////////
				

				if (!$this -> upload -> do_upload("image".$i)) {
				echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					$image = "assets/pictures/images/products/" .$sku_id."/".$upload_data['file_name'];
					$image_arr[]=$image;
				}
				/////
				if (!$this -> upload -> do_upload("thumbnail".$i)) {
				echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					$thumbnail = "assets/pictures/images/products/" .$sku_id."/". $upload_data['file_name'];
					$thumbnail_arr[]=$thumbnail;
				}
				////
				if (!$this -> upload -> do_upload("largeimage".$i)) {
				echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					$largeimage = "assets/pictures/images/products/" .$sku_id."/". $upload_data['file_name'];
					$largeimage_arr[]=$largeimage;
				}
				
			}


			$downloads_success_arr=[];
			$downloads_error_arr=[];
			for($ii=1; $ii<=6; $ii++){
				if(isset($_FILES["file_".$ii."_upload"])){
					if($_FILES["file_".$ii."_upload"]["error"]==0){
						if (!file_exists($folder="assets/pictures/downloads")) {
							$mask=umask(0);
							mkdir($folder,0777);
							umask($mask);
						}
				
						if (!file_exists($folder=$folder.'/'.$sku_id)) {
							$mask=umask(0);
							mkdir($folder,0777);
							umask($mask);
						}
						
						$date = new DateTime();
						$mt=microtime();
						$salt="catalog";
						$rand=mt_rand(1, 1000000);
						$ts=$date->getTimestamp();
						$str=$salt.$rand.$ts.$mt;
						$complaint_thread_id=md5($str);
						
						if (!file_exists($folder=$folder.'/'.$complaint_thread_id)) {
							$mask=umask(0);
							mkdir($folder,0777);
							umask($mask);
						}
						
						$sourcePath=$_FILES["file_".$ii."_upload"]["tmp_name"];
						$targetPath=$folder."/".$_FILES["file_".$ii."_upload"]["name"];
						if(move_uploaded_file($sourcePath,$targetPath)){						
							$downloads_success_arr["file_".$ii."_upload"]=$targetPath;
						}
						else{
							$downloads_error_arr[]="File ".$ii;
						}
					}
				}
			}
			
			if(!empty($downloads_error_arr)){
				echo 0;
				exit;
			}
			
			
			$inventory_type=$this->input->post("inventory_type");
			$sku_display_name_on_button_1=$this->db->escape_str($this -> input -> post("sku_display_name_on_button_1"));
			$sku_display_name_on_button_2=$this->db->escape_str($this -> input -> post("sku_display_name_on_button_2"));
			
			$product_id=$this->input->post("product_id");
			$sku_id=$this->db->escape_str($this -> input -> post("sku_id"));
			$attribute_1=$this->db->escape_str($this -> input -> post("attribute_1"));
			$attribute_1_value=$this->db->escape_str($this -> input -> post("attribute_1_value"));
			$attribute_2=$this->db->escape_str($this -> input -> post("attribute_2"));
			$attribute_2_value=$this->db->escape_str($this -> input -> post("attribute_2_value"));
			$attribute_3=$this->db->escape_str($this -> input -> post("attribute_3"));
			$attribute_3_value=$this->db->escape_str($this -> input -> post("attribute_3_value"));
			$attribute_4=$this->db->escape_str($this -> input -> post("attribute_4"));
			$attribute_4_value=$this->db->escape_str($this -> input -> post("attribute_4_value"));
			$vendor_id=$this->db->escape_str($this -> input -> post("vendors"));
			$base_price=$this->db->escape_str($this -> input -> post("taxable_price"));//custom
			$purchased_price=$this->db->escape_str($this -> input -> post("taxable_price"));//custom
			$selling_price=$this->db->escape_str($this -> input -> post("selling_price"));
			$cost_price=$this->db->escape_str($this -> input -> post("cost_price"));
			//$mrp_quantity=$this->db->escape_str($this -> input -> post("mrp_quantity"));
			$moq=$this->db->escape_str($this -> input -> post("moq"));
			$max_oq=$this->db->escape_str($this -> input -> post("max_oq"));
			$tax=$this->db->escape_str($this -> input -> post("tax"));
			$stock=$this->db->escape_str($this -> input -> post("stock"));
			$add_to_cart=$this->db->escape_str($this -> input -> post("add_to_cart"));
			$request_for_quotation=$this->db->escape_str($this -> input -> post("request_for_quotation"));
			$product_status=$this->db->escape_str($this -> input -> post("product_status"));
			$product_status_view=$this->input->post("product_status_view");
			$low_stock=$this->db->escape_str($this -> input -> post("low_stock"));
			$cutoff_stock=$this->db->escape_str($this -> input -> post("cutoff_stock"));
			//$policy_id=$this->input->post("policy_id");	
			
			if($this->input->post("logistics_parcel_category_id") != null){
			$logistics_parcel_category_id=implode(',',$this->input->post("logistics_parcel_category_id"));}else{
				$logistics_parcel_category_id='';
			}			
			if($this->input->post("logistics_territory_id") != null){
			$logistics_territory_id=implode(',',$this->input->post("logistics_territory_id"));}else{
				$logistics_territory_id='';
			}
			
			$inventory_weight_in_kg=$this->input->post("inventory_weight_in_kg");
			$inventory_volume_in_kg=$this->input->post("inventory_volume_in_kg");
			$inventory_addon_weight_in_kg=$this->input->post("inventory_addon_weight_in_kg");

			if($this->input->post("shipping_charge_not_applicable")){
				$shipping_charge_not_applicable="yes";
				$flat_shipping_charge=$this->input->post("flat_shipping_charge");
				$inventory_weight_in_kg=0;
				$inventory_volume_in_kg=0;
				$inventory_addon_weight_in_kg=0;
			}
			else{
				$shipping_charge_not_applicable="no";
				$flat_shipping_charge="";
			}
	
	
	
			//$volumetric_breakup_point=$this->input->post("volumetric_breakup_point");
			$inventory_length=$this->input->post("inventory_length");
			$inventory_breadth=$this->input->post("inventory_breadth");
			$inventory_height=$this->input->post("inventory_height");
			
			$specification_textarea_value_arr=$this->input->post("specification_textarea_value");				
			$specification_select_value_arr=$this->input->post("specification_select_value");
			
			$filter_arr=$this->input->post("filter");	
			for($k=1;$k<=6;$k++){
				if(count($image_arr)==$k){
					for($i=1;$i<=6-$k;$i++){
						$image_arr[]="";
					}
				}
			}
			for($k=1;$k<=6;$k++){
				if(count($thumbnail_arr)==$k){
					for($i=1;$i<=6-$k;$i++){
						$thumbnail_arr[]="";
					}
				}
			}
			for($k=1;$k<=6;$k++){
				if(count($largeimage_arr)==$k){
					for($i=1;$i<=6-$k;$i++){
						$largeimage_arr[]="";
					}
				}
			}
			
			$highlight=$this->input->post("highlight");
			$highlight=htmlentities($highlight, ENT_QUOTES);

			$highlight_faq=$this->input->post("highlight_faq");
			$highlight_faq=htmlentities($highlight_faq, ENT_QUOTES);
			
			$highlight_whatsinpack=$this->input->post("highlight_whatsinpack");
			$highlight_whatsinpack=htmlentities($highlight_whatsinpack, ENT_QUOTES);
			
			$highlight_aboutthebrand=$this->input->post("highlight_aboutthebrand");
			$highlight_aboutthebrand=htmlentities($highlight_aboutthebrand, ENT_QUOTES);
			
			$file_1_name=$this->db->escape_str($this -> input -> post("file_1_name"));
			$file_2_name=$this->db->escape_str($this -> input -> post("file_2_name"));
			$file_3_name=$this->db->escape_str($this -> input -> post("file_3_name"));
			$file_4_name=$this->db->escape_str($this -> input -> post("file_4_name"));
			$file_5_name=$this->db->escape_str($this -> input -> post("file_5_name"));
			$file_6_name=$this->db->escape_str($this -> input -> post("file_6_name"));
			
			
			$prod_tech_spec_name_1=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_1"));
			$prod_tech_spec_desc_1=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_1"));
			$prod_tech_spec_name_2=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_2"));
			$prod_tech_spec_desc_2=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_2"));
			$prod_tech_spec_name_3=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_3"));
			$prod_tech_spec_desc_3=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_3"));
			$prod_tech_spec_name_4=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_4"));
			$prod_tech_spec_desc_4=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_4"));
			$prod_tech_spec_name_5=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_5"));
			$prod_tech_spec_desc_5=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_5"));
			$prod_tech_spec_name_6=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_6"));
			$prod_tech_spec_desc_6=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_6"));
			$prod_tech_spec_name_7=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_7"));
			$prod_tech_spec_desc_7=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_7"));
			$prod_tech_spec_name_8=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_8"));
			$prod_tech_spec_desc_8=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_8"));
			$prod_tech_spec_name_9=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_9"));
			$prod_tech_spec_desc_9=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_9"));
			$prod_tech_spec_name_10=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_10"));
			$prod_tech_spec_desc_10=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_10"));
			
			
			$testimonial_name_1=$this->db->escape_str($this -> input -> post("testimonial_name_1"));
			$testimonial_desc_1=$this->db->escape_str($this -> input -> post("testimonial_desc_1"));
			$testimonial_name_2=$this->db->escape_str($this -> input -> post("testimonial_name_2"));
			$testimonial_desc_2=$this->db->escape_str($this -> input -> post("testimonial_desc_2"));
			$testimonial_name_3=$this->db->escape_str($this -> input -> post("testimonial_name_3"));
			$testimonial_desc_3=$this->db->escape_str($this -> input -> post("testimonial_desc_3"));
			
			$warranty=$this->db->escape_str($this -> input -> post("warranty"));
			$shipswithin=$this->db->escape_str($this -> input -> post("shipswithin"));


			
			
                        /* added fields */
                        $selling_discount=$this->input->post("selling_discount");
                        $max_selling_price=$this->input->post("max_selling_price");
                        $tax_percent_price=$this->input->post("tax_percent_price");
                        $taxable_price=$this->input->post("taxable_price");
                        $CGST=$this->input->post("CGST");
                        $IGST=$this->input->post("IGST");
                        $SGST=$this->input->post("SGST");
                        $sku_name=addslashes($this->input->post("sku_name"));
                        
			/* added fields */

			if($this->input->post("under_pcat_frontend")){
				$under_pcat_frontend=$this->input->post("under_pcat_frontend");
				if(!empty($under_pcat_frontend)){
					$under_pcat_frontend=implode(',',$under_pcat_frontend);
				}
			}else{
				$under_pcat_frontend='';
			}
			$brouchure=$this->input->post("brouchure");
			
			if($this->input->post("emi_options")){
				$emi_options=$this->input->post("emi_options");
			}
			else{
				$emi_options="";
			}

			$price_validity=$this->input->post("price_validity");
			$material=$this->input->post("material");
			$inventory_unit=$this->input->post("inventory_unit");
			/* brouchure update */

			$date = new DateTime();
			$mt=microtime();
			$salt="brouchure";
			$rand=mt_rand(1, 1000000);
			$ts=$date->getTimestamp();
			$str=$salt.$rand.$ts.$mt;
			$key=md5($str);
			$unique_id=$key;


			if(!empty($_FILES)){
				if (!file_exists($folder="product_docs")) {
					$mask=umask(0);
					mkdir($folder,0777);
					umask($mask);
				}

				//find extention
				if(isset($_FILES["brouchure"]["name"])){
					$filename=$_FILES["brouchure"]["name"];
					$ext = pathinfo($filename, PATHINFO_EXTENSION);

					//echo $ext;

					if(move_uploaded_file($_FILES["brouchure"]["tmp_name"],$folder."/".$unique_id.'.'.$ext)){
						
						// if(file_exists($old_brouchure)){
						// 	unlink($old_brouchure);
						// }
						$brouchure=$folder."/".$unique_id.'.'.$ext;
						//fileupload_s3($ff);	
					}else{

						// $arr["status"]=false;
						// $arr["unique_id"]=$unique_id;
						// $arr["file_uploaded"]="no";
						$brouchure='';
					}
				}else{
					$brouchure='';
				}
			}
			/* brouchure update */

			
			$position_text=$this->input->post("position_text");
			$position_uhave=$this->input->post("position_uhave");
			$position_others_have=$this->input->post("position_others_have");     
			$positioning_uhave_title=$this->input->post("positioning_uhave_title");
			$positioning_others_have_title=$this->input->post("positioning_others_have_title");
                
			

			$flag_arr=$this->Model_catalogue->add_inventory($product_id,$sku_id,$sku_display_name_on_button_1,$sku_display_name_on_button_2,$attribute_1,$attribute_1_value,$attribute_2,$attribute_2_value,$attribute_3,$attribute_3_value,$attribute_4,$attribute_4_value,$common_image,$image_arr,$thumbnail_arr,$largeimage_arr,$vendor_id,$base_price,$selling_price,$cost_price,$purchased_price,$moq,$max_oq,$logistics_parcel_category_id,$inventory_weight_in_kg,$inventory_volume_in_kg,$shipping_charge_not_applicable,$flat_shipping_charge,$inventory_length,$inventory_breadth,$inventory_height,$logistics_territory_id,$tax,$stock,$add_to_cart,$request_for_quotation,$product_status,$product_status_view,$low_stock,$cutoff_stock,$specification_textarea_value_arr,$specification_select_value_arr,$filter_arr,$highlight,$selling_discount,$max_selling_price,$tax_percent_price,$taxable_price,$CGST,$IGST,$SGST,$sku_name,$highlight_faq,$highlight_whatsinpack,$highlight_aboutthebrand,$file_1_name,$file_2_name,$file_3_name,$file_4_name,$file_5_name,$file_6_name,$downloads_success_arr,$prod_tech_spec_name_1,$prod_tech_spec_desc_1,$prod_tech_spec_name_2,$prod_tech_spec_desc_2,$prod_tech_spec_name_3,$prod_tech_spec_desc_3,$prod_tech_spec_name_4,$prod_tech_spec_desc_4,$prod_tech_spec_name_5,$prod_tech_spec_desc_5,$prod_tech_spec_name_6,$prod_tech_spec_desc_6,$prod_tech_spec_name_7,$prod_tech_spec_desc_7,$prod_tech_spec_name_8,$prod_tech_spec_desc_8,$prod_tech_spec_name_9,$prod_tech_spec_desc_9,$prod_tech_spec_name_10,$prod_tech_spec_desc_10,$testimonial_name_1,$testimonial_desc_1,$testimonial_name_2,$testimonial_desc_2,$testimonial_name_3,$testimonial_desc_3,$warranty,$shipswithin,$under_pcat_frontend,$brouchure,$emi_options,$price_validity,$material,$inventory_unit,$position_text,$position_uhave,$position_others_have,$positioning_uhave_title,$positioning_others_have_title,$inventory_addon_weight_in_kg,$inventory_type);
			
			if($flag_arr['res']==true){
				if($inventory_type!='2'){
					$inventory_id=$flag_arr['inventory_id'];
					$index_flag=$this->create_search_index($inventory_id);
					if($index_flag==true){
						echo true;
					}else{
						echo 0;
					}
				}else{
					echo true;
				}
				
			}else{
				echo 0;
			}
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
		public function edit_inventory(){
		if($this->session->userdata("logged_in")){

			$this->load->library('upload');
			$files = $_FILES;
			$sku_id=addslashes($this->input->post("edit_sku_id"));
			$inventory_type=$this->input->post("inventory_type");
			
			$common_image='';
			$common_image_name=$files['edit_common_image']['name'];
			if($common_image_name!=""){
				$previous_image=$this->input->post("previous_image");
				if(file_exists($previous_image)) {
					unlink($previous_image);			   
				}
					
				$c_ext = pathinfo($common_image_name, PATHINFO_EXTENSION);
				$c_filen=rand(10,100000);
				
				$_FILES['edit_common_image']['name']=$c_filen.".".$c_ext;
				$_FILES['edit_common_image']['type']= $files['edit_common_image']['type'];
				$_FILES['edit_common_image']['tmp_name']= $files['edit_common_image']['tmp_name'];
				$_FILES['edit_common_image']['error']= $files['edit_common_image']['error'];
				$_FILES['edit_common_image']['size']= $files['edit_common_image']['size'];  
				$this->upload->initialize($this->set_upload_options_config($sku_id,300,366));
				
				if (!$this -> upload -> do_upload("edit_common_image")) {
					echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					$common_image = "assets/pictures/images/products/" .$sku_id."/". $upload_data['file_name'];
				}
			}else{
				$common_image=$this->input->post("previous_image");	  
			}
			
			$image_arr=[];
			$thumbnail_arr=[];
				if(!is_dir('assets/pictures/images/products/'.$sku_id)){
						mkdir('assets/pictures/images/products/'.$sku_id);
					}
			for($i=1;$i<=6;$i++){
				if(isset($_FILES['edit_image'.$i]['name'])){
					if($_FILES['edit_image'.$i]['name']!=''){
				
					$previous_image=$this->input->post("previous_image".$i);
					if(file_exists($previous_image)) {
						unlink($previous_image);			   
					}
					
					
					
					$filename=$files['edit_image'.$i]['name'];  
					$ext = pathinfo($filename, PATHINFO_EXTENSION);
					$filen=rand(10,100000);
					
					$_FILES['edit_image'.$i]['name']=$filen.".".$ext;
					$_FILES['edit_image'.$i]['type']= $files['edit_image'.$i]['type'];
					$_FILES['edit_image'.$i]['tmp_name']= $files['edit_image'.$i]['tmp_name'];
					$_FILES['edit_image'.$i]['error']= $files['edit_image'.$i]['error'];
					$_FILES['edit_image'.$i]['size']= $files['edit_image'.$i]['size'];  
					
					$this->upload->initialize($this->set_upload_options_config($sku_id,420,512));
					if (!$this -> upload -> do_upload("edit_image".$i)) {
						echo $this -> upload -> display_errors();exit;
					} else {
						$upload_data = $this -> upload -> data();
						$image = "assets/pictures/images/products/" .$sku_id."/". $upload_data['file_name'];
					}
				

				}else{
				   if($this->input->post("previous_image".$i)==""){
					   if($this->input->post("previous_image".$i."_unlink")!=""){
						   if(file_exists($this->input->post("previous_image".$i."_unlink"))){
							unlink($this->input->post("previous_image".$i."_unlink"));
						   }
					   }
					   $image="";
				   }
				   else{
					   $image=$this->input->post("previous_image".$i);
				   }
				}
				$image_arr[]=$image;
				
				
				 if($_FILES['edit_thumbnail'.$i]['name']!=''){ 
				
					$previous_thumbnail=$this->input->post("previous_thumbnail".$i);
					if(file_exists($previous_thumbnail)) {
						unlink($previous_thumbnail);
					}				
					$filename1=$files['edit_thumbnail'.$i]['name'];  
					$ext1 = pathinfo($filename1, PATHINFO_EXTENSION);
					$filen1=rand(10,100000);
					$_FILES['edit_thumbnail'.$i]['name']= $filen1.".".$ext1;
					$_FILES['edit_thumbnail'.$i]['type']= $files['edit_thumbnail'.$i]['type'];
					$_FILES['edit_thumbnail'.$i]['tmp_name']= $files['edit_thumbnail'.$i]['tmp_name'];
					$_FILES['edit_thumbnail'.$i]['error']= $files['edit_thumbnail'.$i]['error'];
					$_FILES['edit_thumbnail'.$i]['size']= $files['edit_thumbnail'.$i]['size'];
					$this->upload->initialize($this->set_upload_options_config($sku_id,100,122));
					
					if (!$this -> upload -> do_upload("edit_thumbnail".$i)) {
					echo $this -> upload -> display_errors();exit;
					} else {
						$upload_data = $this -> upload -> data();
						
						$thumbnail = "assets/pictures/images/products/" .$sku_id."/". $upload_data['file_name'];
					}
					
				}else{
					
				   if($this->input->post("previous_thumbnail".$i)==""){
					   if($this->input->post("previous_thumbnail".$i."_unlink")!=""){
						   if(file_exists($this->input->post("previous_thumbnail".$i."_unlink"))){
							unlink($this->input->post("previous_thumbnail".$i."_unlink"));
						   }
					   }
						$thumbnail="";
				   }
				   else{
					   $thumbnail=$this->input->post("previous_thumbnail".$i);
				   }
				   
				}
				$thumbnail_arr[]=$thumbnail;
				
				
				
					 if($_FILES['edit_largeimage'.$i]['name']!=''){ 
				
					$previous_largeimage=$this->input->post("previous_largeimage".$i);
					if(file_exists($previous_largeimage)) {
						unlink($previous_largeimage);
					}				
					$filename1=$files['edit_largeimage'.$i]['name'];  
					$ext1 = pathinfo($filename1, PATHINFO_EXTENSION);
					$filen1=rand(10,100000);
					$_FILES['edit_largeimage'.$i]['name']= $filen1.".".$ext1;
					$_FILES['edit_largeimage'.$i]['type']= $files['edit_largeimage'.$i]['type'];
					$_FILES['edit_largeimage'.$i]['tmp_name']= $files['edit_largeimage'.$i]['tmp_name'];
					$_FILES['edit_largeimage'.$i]['error']= $files['edit_largeimage'.$i]['error'];
					$_FILES['edit_largeimage'.$i]['size']= $files['edit_largeimage'.$i]['size'];
					$this->upload->initialize($this->set_upload_options_config($sku_id,850,1036));
					
					if (!$this -> upload -> do_upload("edit_largeimage".$i)) {
					echo $this -> upload -> display_errors();exit;
					} else {
						$upload_data = $this -> upload -> data();
						
						$largeimage = "assets/pictures/images/products/" .addslashes($this->input->post("edit_sku_id"))."/". $upload_data['file_name'];
					}
					
				}else{
					
				   if($this->input->post("previous_largeimage".$i)==""){
					   if($this->input->post("previous_largeimage".$i."_unlink")!=""){
						   if(file_exists($this->input->post("previous_largeimage".$i."_unlink"))){
							unlink($this->input->post("previous_largeimage".$i."_unlink"));
						   }
					   }
						$largeimage="";
				   }
				   else{
					   $largeimage=$this->input->post("previous_largeimage".$i);
				   }
				   
				}
				$largeimage_arr[]=$largeimage;
				
				
			}//if
				
			}
			
			
			
			
			
			$delete_file_1_upload=$this->db->escape_str($this -> input -> post("delete_file_1_upload"));
			$delete_file_2_upload=$this->db->escape_str($this -> input -> post("delete_file_2_upload"));
			$delete_file_3_upload=$this->db->escape_str($this -> input -> post("delete_file_3_upload"));
			$delete_file_4_upload=$this->db->escape_str($this -> input -> post("delete_file_4_upload"));
			$delete_file_5_upload=$this->db->escape_str($this -> input -> post("delete_file_5_upload"));
			$delete_file_6_upload=$this->db->escape_str($this -> input -> post("delete_file_6_upload"));
			
			$inventory_id=$this->input->post("edit_inventory_id");
			$sku_id=$this->db->escape_str($this -> input -> post("edit_sku_id"));
			$update_delete_file_n_upload_arr=[];
			$update_delete_file_n_name_arr=[];
			if($delete_file_1_upload!=""){
				$update_delete_file_n_upload_arr[]="file_1_upload";
				$update_delete_file_n_name_arr[]="file_1_name";
			}
			if($delete_file_2_upload!=""){
				$update_delete_file_n_upload_arr[]="file_2_upload";
				$update_delete_file_n_name_arr[]="file_2_name";
			}
			if($delete_file_3_upload!=""){
				$update_delete_file_n_upload_arr[]="file_3_upload";
				$update_delete_file_n_name_arr[]="file_3_name";
			}
			if($delete_file_4_upload!=""){
				$update_delete_file_n_upload_arr[]="file_4_upload";
				$update_delete_file_n_name_arr[]="file_4_name";
			}
			if($delete_file_5_upload!=""){
				$update_delete_file_n_upload_arr[]="file_5_upload";
				$update_delete_file_n_name_arr[]="file_5_name";
				
			}
			if($delete_file_6_upload!=""){
				$update_delete_file_n_upload_arr[]="file_6_upload";
				$update_delete_file_n_name_arr[]="file_6_name";
			}
			if(!empty($update_delete_file_n_upload_arr)){
				$this->Model_catalogue->update_delete_file_n_upload($update_delete_file_n_upload_arr,$update_delete_file_n_name_arr,$inventory_id,$sku_id);
			}
			
			
			
			$downloads_success_arr=[];
			$downloads_error_arr=[];
			for($ii=1; $ii<=6; $ii++){
				if(isset($_FILES["file_".$ii."_upload"])){

				if($_FILES["file_".$ii."_upload"]["error"]==0){
					if (!file_exists($folder="assets/pictures/downloads")) {
						$mask=umask(0);
						mkdir($folder,0777);
						umask($mask);
					}
			
					if (!file_exists($folder=$folder.'/'.$sku_id)) {
						$mask=umask(0);
						mkdir($folder,0777);
						umask($mask);
					}
					
					$date = new DateTime();
					$mt=microtime();
					$salt="catalog";
					$rand=mt_rand(1, 1000000);
					$ts=$date->getTimestamp();
					$str=$salt.$rand.$ts.$mt;
					$complaint_thread_id=md5($str);
					
					if (!file_exists($folder=$folder.'/'.$complaint_thread_id)) {
						$mask=umask(0);
						mkdir($folder,0777);
						umask($mask);
					}
					
					$sourcePath=$_FILES["file_".$ii."_upload"]["tmp_name"];
					$targetPath=$folder."/".$_FILES["file_".$ii."_upload"]["name"];
					if(move_uploaded_file($sourcePath,$targetPath)){						
						$downloads_success_arr["file_".$ii."_upload"]=$targetPath;
					}
					else{
						$downloads_error_arr[]="File ".$ii;
					}
				}
			}//if
			}
			
			if(!empty($downloads_error_arr)){
				echo 0;
				exit;
			}
			
			
			
			
			$inventory_id=$this->input->post("edit_inventory_id");
			$sku_display_name_on_button_1=$this->db->escape_str($this -> input -> post("sku_display_name_on_button_1"));
			$sku_display_name_on_button_2=$this->db->escape_str($this -> input -> post("sku_display_name_on_button_2"));
			$product_id=$this->input->post("edit_product_id");
			$sku_id=$this->db->escape_str($this -> input -> post("edit_sku_id"));
			$attribute_1=$this->db->escape_str($this -> input -> post("edit_attribute_1"));
			$attribute_1_value=$this->db->escape_str($this -> input -> post("edit_attribute_1_value"));
			$attribute_2=$this->db->escape_str($this -> input -> post("edit_attribute_2"));
			$attribute_2_value=$this->db->escape_str($this -> input -> post("edit_attribute_2_value"));
			$attribute_3=$this->db->escape_str($this -> input -> post("edit_attribute_3"));
			$attribute_3_value=$this->db->escape_str($this -> input -> post("edit_attribute_3_value"));
			$attribute_4=$this->db->escape_str($this -> input -> post("edit_attribute_4"));
			$attribute_4_value=$this->db->escape_str($this -> input -> post("edit_attribute_4_value"));
			$vendor_id=$this->db->escape_str($this -> input -> post("edit_vendors"));
			$base_price=$this->db->escape_str($this -> input -> post("taxable_price"));//custom
			$purchased_price=$this->db->escape_str($this -> input -> post("taxable_price"));//custom
            $selling_price=$this->db->escape_str($this -> input -> post("edit_selling_price"));
			$cost_price=$this->db->escape_str($this -> input -> post("edit_cost_price"));
			//$mrp_quantity=$this->db->escape_str($this -> input -> post("edit_mrp_quantity"));
			$moq=$this->db->escape_str($this -> input -> post("edit_moq"));
			$max_oq=$this->db->escape_str($this -> input -> post("edit_max_oq"));
			$tax=$this->db->escape_str($this -> input -> post("edit_tax"));
			$stock=$this->db->escape_str($this -> input -> post("edit_stock"));
			$edit_add_to_cart=$this->db->escape_str($this -> input -> post("edit_add_to_cart"));
			$edit_request_for_quotation=$this->db->escape_str($this -> input -> post("edit_request_for_quotation"));
			$product_status=$this->db->escape_str($this -> input -> post("edit_product_status"));
			$product_status_view=$this->input->post("edit_product_status_view");
			$low_stock=$this->db->escape_str($this -> input -> post("edit_low_stock"));
			$cutoff_stock=$this->db->escape_str($this -> input -> post("edit_cutoff_stock"));
			//$policy_id=$this->input->post("edit_policy_id");
			
			if($this->input->post("edit_logistics_parcel_category_id") != null){
			$logistics_parcel_category_id=implode(',',$this->input->post("edit_logistics_parcel_category_id"));}else{
				$logistics_parcel_category_id='';
			}
			if($this->input->post("edit_logistics_territory_id") != null){
				$logistics_territory_id=implode(',',$this->input->post("edit_logistics_territory_id"));}else{
				$logistics_territory_id='';
			}
			
			$inventory_weight_in_kg=$this->input->post("edit_inventory_weight_in_kg");
			$inventory_volume_in_kg=$this->input->post("edit_inventory_volume_in_kg");	
			$inventory_addon_weight_in_kg=$this->input->post("edit_inventory_addon_weight_in_kg");

			if($this->input->post("edit_shipping_charge_not_applicable")){
				$shipping_charge_not_applicable="yes";
				$flat_shipping_charge=$this->input->post("edit_flat_shipping_charge");
				$inventory_weight_in_kg=0;
				$inventory_volume_in_kg=0;
				$inventory_addon_weight_in_kg=0;
			}
			else{
				$shipping_charge_not_applicable="no";
				$flat_shipping_charge="";
			}
	
	

			
			//$volumetric_breakup_point=$this->input->post("edit_volumetric_breakup_point");	
			$inventory_length=$this->input->post("edit_inventory_length");	
			$inventory_breadth=$this->input->post("edit_inventory_breadth");	
			$inventory_height=$this->input->post("edit_inventory_height");				
			$specification_textarea_value_arr=$this->input->post("edit_specification_textarea_value");	
			$specification_select_value_arr=$this->input->post("edit_specification_select_value");
			$filter_arr=$this->input->post("edit_filter");
			$inventory_specification_id_arr=$this->input->post("edit_inventory_specification_id");			
			$product_filter_id_arr=$this->input->post("edit_product_filter_id");
			
			/* added fields */
                        $selling_discount=$this->input->post("edit_selling_discount");
                        $max_selling_price=$this->input->post("edit_max_selling_price");
                        $tax_percent_price=$this->input->post("tax_percent_price");
                        $taxable_price=$this->input->post("taxable_price");
                        $CGST=$this->input->post("CGST");
                        $IGST=$this->input->post("IGST");
                        $SGST=$this->input->post("SGST");
                        $sku_name=$this->input->post("sku_name");
			/* added fields */
			for($k=1;$k<=6;$k++){
				if(count($image_arr)==$k){
					for($i=1;$i<=6-$k;$i++){
						$image_arr[]="";
					}
				}
			}
			
			foreach ($image_arr as $key => $value)
			{ 
				if ($value === "")
				{ 
					unset($image_arr[$key]);
					$image_arr[] = $value;
				}
			}

			// rebuild array index
			$image_arr = array_values($image_arr);
			
			///////////////////////////////////
			for($k=1;$k<=6;$k++){
				if(count($thumbnail_arr)==$k){
					for($i=1;$i<=6-$k;$i++){
						$thumbnail_arr[]="";
					}
				}
			}
			foreach ($thumbnail_arr as $key => $value)
			{ 
				if ($value === "")
				{ 
					unset($thumbnail_arr[$key]);
					$thumbnail_arr[] = $value;
				}
			}

			// rebuild array index
			$thumbnail_arr = array_values($thumbnail_arr);
			
			////////////////////////////////////////
			for($k=1;$k<=6;$k++){
				if(count($largeimage_arr)==$k){
					for($i=1;$i<=6-$k;$i++){
						$largeimage_arr[]="";
					}
				}
			}
			foreach ($largeimage_arr as $key => $value)
			{ 
				if ($value === "")
				{ 
					unset($largeimage_arr[$key]);
					$largeimage_arr[] = $value;
				}
			}

			// rebuild array index
			$largeimage_arr = array_values($largeimage_arr);

			$highlight=$this->input->post("highlight");
			$highlight=htmlentities($highlight, ENT_QUOTES);
			
			$highlight_faq=$this->input->post("highlight_faq");
			$highlight_faq=htmlentities($highlight_faq, ENT_QUOTES);
			
			$highlight_whatsinpack=$this->input->post("highlight_whatsinpack");
			$highlight_whatsinpack=htmlentities($highlight_whatsinpack, ENT_QUOTES);
			
			$highlight_aboutthebrand=$this->input->post("highlight_aboutthebrand");
			$highlight_aboutthebrand=htmlentities($highlight_aboutthebrand, ENT_QUOTES);
			
			
			$file_1_name=$this->db->escape_str($this -> input -> post("file_1_name"));
			$file_2_name=$this->db->escape_str($this -> input -> post("file_2_name"));
			$file_3_name=$this->db->escape_str($this -> input -> post("file_3_name"));
			$file_4_name=$this->db->escape_str($this -> input -> post("file_4_name"));
			$file_5_name=$this->db->escape_str($this -> input -> post("file_5_name"));
			$file_6_name=$this->db->escape_str($this -> input -> post("file_6_name"));
			
			
			
			$prod_tech_spec_name_1=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_1"));
			$prod_tech_spec_desc_1=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_1"));
			$prod_tech_spec_name_2=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_2"));
			$prod_tech_spec_desc_2=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_2"));
			$prod_tech_spec_name_3=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_3"));
			$prod_tech_spec_desc_3=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_3"));
			$prod_tech_spec_name_4=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_4"));
			$prod_tech_spec_desc_4=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_4"));
			$prod_tech_spec_name_5=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_5"));
			$prod_tech_spec_desc_5=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_5"));
			$prod_tech_spec_name_6=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_6"));
			$prod_tech_spec_desc_6=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_6"));
			$prod_tech_spec_name_7=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_7"));
			$prod_tech_spec_desc_7=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_7"));
			$prod_tech_spec_name_8=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_8"));
			$prod_tech_spec_desc_8=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_8"));
			$prod_tech_spec_name_9=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_9"));
			$prod_tech_spec_desc_9=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_9"));
			$prod_tech_spec_name_10=$this->db->escape_str($this -> input -> post("prod_tech_spec_name_10"));
			$prod_tech_spec_desc_10=$this->db->escape_str($this -> input -> post("prod_tech_spec_desc_10"));
			
			$testimonial_name_1=$this->db->escape_str($this -> input -> post("testimonial_name_1"));
			$testimonial_desc_1=$this->db->escape_str($this -> input -> post("testimonial_desc_1"));
			$testimonial_name_2=$this->db->escape_str($this -> input -> post("testimonial_name_2"));
			$testimonial_desc_2=$this->db->escape_str($this -> input -> post("testimonial_desc_2"));
			$testimonial_name_3=$this->db->escape_str($this -> input -> post("testimonial_name_3"));
			$testimonial_desc_3=$this->db->escape_str($this -> input -> post("testimonial_desc_3"));
			
			$warranty=$this->db->escape_str($this -> input -> post("warranty"));
			$shipswithin=$this->db->escape_str($this -> input -> post("shipswithin"));
			
			

			

			
			
			/* added fields */

			if($this->input->post("under_pcat_frontend")){
				$under_pcat_frontend=$this->input->post("under_pcat_frontend");
				if(!empty($under_pcat_frontend)){
						$under_pcat_frontend=implode(',',$under_pcat_frontend);
					}
			}else{
				$under_pcat_frontend='';
			}
			$brouchure=$this->input->post("brouchure");
			$old_brouchure=$this->input->post("old_brouchure");
			if($this->input->post("emi_options")){
				$emi_options=$this->input->post("emi_options");
			}
			else{
				$emi_options="";
			}
			$price_validity=$this->input->post("price_validity");
			$material=$this->input->post("material");
			$inventory_unit=$this->input->post("inventory_unit");
			/* brouchure update */

			$date = new DateTime();
			$mt=microtime();
			$salt="brouchure";
			$rand=mt_rand(1, 1000000);
			$ts=$date->getTimestamp();
			$str=$salt.$rand.$ts.$mt;
			$key=md5($str);
			$unique_id=$key;


			if(!empty($_FILES)){

				if(isset($_FILES["brouchure"])){
				if (!file_exists($folder="product_docs")) {
					$mask=umask(0);
					mkdir($folder,0777);
					umask($mask);
				}

				//find extention
				$filename=$_FILES["brouchure"]["name"];
				$ext = pathinfo($filename, PATHINFO_EXTENSION);

				//echo $old_brouchure;

				//exit;

				if(move_uploaded_file($_FILES["brouchure"]["tmp_name"],$folder."/".$unique_id.'.'.$ext)){
					
					if(file_exists($old_brouchure)){
						unlink($old_brouchure);
					}
					$brouchure=$folder."/".$unique_id.'.'.$ext;
					//fileupload_s3($ff);	
				}else{

					$brouchure=$old_brouchure;
				}
			}//if
			}
			/* brouchure update */

			$position_text=$this->input->post("position_text");
			$position_uhave=$this->input->post("position_uhave");
			$position_others_have=$this->input->post("position_others_have");
			$position_id=$this->input->post("position_id");
			$positioning_uhave_title=$this->input->post("positioning_uhave_title");
			$positioning_others_have_title=$this->input->post("positioning_others_have_title");
                
		
				$flag=$this->Model_catalogue->edit_inventory($inventory_id,$product_id,$sku_display_name_on_button_1,$sku_display_name_on_button_2,$sku_id,$attribute_1,$attribute_1_value,$attribute_2,$attribute_2_value,$attribute_3,$attribute_3_value,$attribute_4,$attribute_4_value,$common_image,$image_arr,$thumbnail_arr,$largeimage_arr,$vendor_id,$base_price,$selling_price,$cost_price,$purchased_price,$moq,$max_oq,$logistics_parcel_category_id,$inventory_weight_in_kg,$inventory_volume_in_kg,$shipping_charge_not_applicable,$flat_shipping_charge,$inventory_length,$inventory_breadth,$inventory_height,$logistics_territory_id,$tax,$stock,$edit_add_to_cart,$edit_request_for_quotation,$product_status,$product_status_view,$low_stock,$cutoff_stock,$specification_textarea_value_arr,$specification_select_value_arr,$filter_arr,$inventory_specification_id_arr,$product_filter_id_arr,$highlight,$selling_discount,$max_selling_price,$tax_percent_price,$taxable_price,$CGST,$IGST,$SGST,$sku_name,$highlight_faq,$highlight_whatsinpack,$highlight_aboutthebrand,$file_1_name,$file_2_name,$file_3_name,$file_4_name,$file_5_name,$file_6_name,$downloads_success_arr,$prod_tech_spec_name_1,$prod_tech_spec_desc_1,$prod_tech_spec_name_2,$prod_tech_spec_desc_2,$prod_tech_spec_name_3,$prod_tech_spec_desc_3,$prod_tech_spec_name_4,$prod_tech_spec_desc_4,$prod_tech_spec_name_5,$prod_tech_spec_desc_5,$prod_tech_spec_name_6,$prod_tech_spec_desc_6,$prod_tech_spec_name_7,$prod_tech_spec_desc_7,$prod_tech_spec_name_8,$prod_tech_spec_desc_8,$prod_tech_spec_name_9,$prod_tech_spec_desc_9,$prod_tech_spec_name_10,$prod_tech_spec_desc_10,$testimonial_name_1,$testimonial_desc_1,$testimonial_name_2,$testimonial_desc_2,$testimonial_name_3,$testimonial_desc_3,$warranty,$shipswithin,$under_pcat_frontend,$brouchure,$emi_options,$price_validity,$material,$inventory_unit,$position_text,$position_uhave,$position_others_have,$position_id,$positioning_uhave_title,$positioning_others_have_title,$inventory_addon_weight_in_kg,$inventory_type);
			
			if($flag==true){
				if($inventory_type!='2'){

					if($product_status=='Discontinued'){
						$flag_update_index=$this->delete_search_index($inventory_id);
					}else{
						$flag_update_index=$this->update_search_index($inventory_id);
					}

					if($flag_update_index==true){
						echo true;
					}else{
						echo 0;
					}
				}else{
					echo true;
				}
			}else{
				echo 0;
			}
	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	private function set_upload_options_config($sku_id,$width,$height){
		$config = array();
		$config['upload_path'] = './assets/pictures/images/products/'.$sku_id;
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '1048576';
		$config['max_width'] = $width;
		$config['max_height'] = $height;
		$config['overwrite']     = FALSE;

		return $config;
	}

	private function vendor_set_upload_options_config($sku_id,$width,$height){
		$config = array();
		$config['upload_path'] = './assets/pictures/images/vendor_products/'.$sku_id;
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '1048576';
		$config['max_width'] = $width;
		$config['max_height'] = $height;
		$config['overwrite']     = FALSE;

		return $config;
	}
	private function set_upload_options_external_inventory_config($sku_id,$width,$height){
		$config = array();
		$config['upload_path'] = './assets/pictures/images/external_products/'.$sku_id;
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '1048576';
		$config['max_width'] = $width;
		$config['max_height'] = $height;
		$config['overwrite']     = FALSE;

		return $config;
	}
	private function set_upload_options(){   
		//upload an image options
		$config = array();
		$config['upload_path'] = './assets/pictures/images/products/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '2048';
		$config['max_width'] = '1024';
		$config['max_height'] = '800';
		$config['overwrite']     = FALSE;

		return $config;
	}
	public function get_all_inventory_data($product_id,$inventory_id){
		if($this->session->userdata("logged_in")){			
			$data=$this->Model_catalogue->get_all_inventory_data($product_id,$inventory_id);
			return $data;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function get_all_filter_by_inventory_id($inventory_id){
		if($this->session->userdata("logged_in")){

			$data=$this->Model_catalogue->get_all_filter_by_inventory_id($inventory_id);
			return $data;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}

	public function get_all_specification_by_inventory_id($inventory_id){
		if($this->session->userdata("logged_in")){
			$data=$this->Model_catalogue->get_all_specification_by_inventory_id($inventory_id);
			return $data;
	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function create_search_index_inventory_id_list($selected_list){
		if($this->session->userdata("logged_in")){
			$flag_arr=array();
			$inventory_id_arr=explode(",",$selected_list);
			foreach($inventory_id_arr as $inventory_id){
				$flag_arr[]=$this->create_search_index($inventory_id);
			}
			if(count($flag_arr)==count($inventory_id_arr)){
				return true;
			}
			else{
				return false;
			}
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function update_inventory_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$active=$this->input->post("active");
			$flag=$this->Model_catalogue->update_inventory_selected($selected_list,$active);
			if($flag==true){
				if($active==0){
					$flag_delete_index=$this->delete_search_index_selected($selected_list);
					if($flag_delete_index==true){
						echo true;
					}else{
						echo 0;
					}
				}
				else{
					$flag_index=$this->create_search_index_inventory_id_list($selected_list);
					if($flag_index==true){
						echo true;
					}else{
						echo 0;
					}
				}
				
			}else{
				echo 0;
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function delete_inventory_when_no_info(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_inventory_when_no_info($selected_list);
			if($flag===true){
				$flag_delete_index=$this->delete_search_index_selected($selected_list);
				echo true;	
			}
			else if($flag===false){
				echo false;	
			}
			else{
				echo 2;	
			}	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
			
	}
	public function deactivate_external_inventory(){
		if($this->session->userdata("logged_in")){
			$external_inventory_id=$this->input->post("external_inventory_id");
			$flag=$this -> Model_catalogue-> deactivate_external_inventory($external_inventory_id);
			if($flag===true){
				echo true;	
			}
			else{
				echo false;	
			}	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
			
	}
	public function activate_external_inventory(){
		if($this->session->userdata("logged_in")){
			$external_inventory_id=$this->input->post("external_inventory_id");
			$flag=$this -> Model_catalogue-> activate_external_inventory($external_inventory_id);
			if($flag===true){
				echo true;	
			}
			else{
				echo false;	
			}	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
			
	}
	public function product_vendors(){		
		if($this->session->userdata("logged_in")){
			
			$data['vendors'] = $this->Model_catalogue->product_vendors();
			$data["menu_flag"]="product_vendors_view";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/product_vendors_view',$data);			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function vendors(){		
		if($this->session->userdata("logged_in")){
			
			$data['vendors'] = $this->Model_catalogue->vendors();
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/vendors_view',$data);			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function edit_vendors_form($vendor_id=''){
		if($this->session->userdata("logged_in")){
			
			if($vendor_id!=''){
				//vendor login
			}else{
				$vendor_id=$this->input->post("vendor_id");
			}
			$data['vendors'] = $this->Model_catalogue->vendors();
			$data["get_vendors_data"]=$this->get_product_vendors_data($vendor_id);
			$data["vendor_id"]=$vendor_id;
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_vendors_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}

	public function vendors_change_passowrd($vendor_id=''){
		if($this->session->userdata("logged_in")){
			
			if($vendor_id!=''){
				//vendor login
			}else{
				$vendor_id=$this->input->post("vendor_id");
			}
			
			$data["get_vendors_data"]=$this->get_product_vendors_data($vendor_id);
			$data["vendor_id"]=$vendor_id;
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/vendors_change_password',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function vendor_check_current_password(){
		$current_password=md5($this->input->post('current_password'));
		
		$vendor_id=$this->session->userdata("user_id");
		
		$verify=$this->Model_catalogue->vendor_check_current_password($current_password,$vendor_id);
		if($verify){
			echo "1";
		}
		else{
			echo "0";
		}
	}
	public function vendor_update_password(){
		extract($this->input->post());
		$password=md5($new_password);
		$user_type=$this->session->userdata("user_type");
		
		if($user_type=='vendor'){
			$vendor_id=$this->session->userdata("user_id");
		
			$updated=$this->Model_catalogue->vendor_update_password($password,$vendor_id,$new_password);
			if($updated){
				$this->session->set_flashdata('notification','Password is updated successfully');
				redirect(base_url()."admin/Catalogue/vendors_change_passowrd/".$vendor_id);
			}else{
				redirect(base_url()."admin/Catalogue/vendors_change_passowrd/".$vendor_id);
			}
		}
		
	}
	public function archived_vendors(){		
		if($this->session->userdata("logged_in")){				
			$data['vendors'] = $this->Model_catalogue->archived_vendors();
			$data["menu_flag"]="catalog_active_links_archived";	
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/archived_vendors_view',$data);			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function add_vendors(){		
		if($this->session->userdata("logged_in")){
			$name=$this->db->escape_str($this -> input -> post("name"));
			$email=$this->db->escape_str($this -> input -> post("email"));
			$mobile=$this->db->escape_str($this -> input -> post("mobile"));
			$landline=$this->db->escape_str($this -> input -> post("landline"));
			$address1=$this->db->escape_str($this -> input -> post("address1"));
			$address2=$this->db->escape_str($this -> input -> post("address2"));
                        $pincode=$this->db->escape_str($this -> input -> post("pincode"));
                        
			$flag=$this->Model_catalogue->check_duplication_name_vendor($email,$mobile,$landline,"");
			if($flag==true){
				echo "exist";
			}else{
                            $password=rand(10000000,99999999);
                            $pass=md5($password);
                            
                            $image="assets/pictures/cust_uploads/user.jpg";
                            
                            $update_flag=$this->Model_catalogue->add_vendors($name,$email,$mobile,$landline,$address1,$address2,$pincode,$password,$image);
                            if($update_flag==true){
                                    /*sending mail to vendor */
                                    
                                    $email_stuff_arr=email_stuff();
                                    $str='Your vendor account is ready. Now you are a registered Vendor. Your password is <b>'.$password.'</b>. Please login to your panel '.base_url()."Administrator.";//activation link has to be sent

				if($email!=''){
					
					$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr>
					
					<tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi, </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">'.$str.'</p><br><p style="padding:0;margin:0;color:#565656;font-size: 15px;">Warm Regards, <br><b>'.$email_stuff_arr["regardsteam_emailtemplate"].'</b></p><br></td></tr>';

					$usermessage.='<tr><td style="background-color:#e1e1e1;">'.$email_stuff_arr["footer_txt"].'</td></tr>';
					
					$usermessage.='<tr><td>'.$email_stuff_arr["footer_copiright_content"].'</td></tr>';

					$usermessage.='</tbody> </table></tr></table></center> </body></html>';
				
					
					$mail = new PHPMailer;
					/*$mail->isSMTP();
					$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
					$mail->Port = '587';//  live - comment it 
					$mail->Auth = true;//  live - comment it 
					$mail->SMTPAuth = true;
					$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
					$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
					$mail->SMTPSecure = 'tls';*/
					$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
					$mail->FromName = $email_stuff_arr["name_emailtemplate"];
					$mail->addAddress($email);
					$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
					$mail->WordWrap = 50;
					$mail->isHTML(true);
					$mail->Subject = 'Vendor Registration';
					$mail->Body    = $usermessage;
					if($email != ''){
                                            @$mail->send();
                                            //$flag=$this->Customer_account->sended_code_to_mail($email,$rand);
                                            //echo $flag;
					}
				}
                                
                                echo "yes";
                                    
                                    /*sending mail to vendor */
                            }else{
                                    echo "no";
                            }
			}
	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_vendors_data($vendor_id){
		if($this->session->userdata("logged_in")){
			$vendor_arr=$this->Model_catalogue->get_vendors_data($vendor_id);
			return $vendor_arr;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_product_vendors_data($vendor_id){
		if($this->session->userdata("logged_in")){
			$vendor_arr=$this->Model_catalogue->get_product_vendors_data($vendor_id);
			return $vendor_arr;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_vendors(){
		
		if($this->session->userdata("logged_in")){
			$vendor_id=$this->input->post("edit_vendor_id");
			$name=$this->db->escape_str($this -> input -> post("edit_name"));
			$email=$this->db->escape_str($this -> input -> post("edit_email"));
			$mobile=$this->db->escape_str($this -> input -> post("edit_mobile"));
			$landline=$this->db->escape_str($this -> input -> post("edit_landline"));
			
			$email_default=$this->db->escape_str($this -> input -> post("edit_email_default"));
			$mobile_default=$this->db->escape_str($this -> input -> post("edit_mobile_default"));
			$landline_default=$this->db->escape_str($this -> input -> post("edit_landline_default"));
			
			$address1=$this->db->escape_str($this -> input -> post("edit_address1"));
			$address2=$this->db->escape_str($this -> input -> post("edit_address2"));
			$pincode=$this->db->escape_str($this -> input -> post("edit_pincode"));



			$flag_check="";

			if($email_default!=$email || $mobile_default!=$mobile || $landline_default!=$landline){
			$flag_check=$this->Model_catalogue->check_duplication_name_vendor($email,$mobile,$landline,$vendor_id);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{

				$post_data_arr=$this -> input -> post();

			$update_flag=$this->Model_catalogue->edit_vendors($vendor_id,$name,$email,$mobile,$landline,$address1,$address2,$pincode,$post_data_arr);
			
				if($update_flag==true){
					echo true;
				}else{
					echo 0;
				}
			}
		}else{
			$this->load->view('admin/header');

			$this->load->view('admin/login');
		}
	}	
	public function update_vendors_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$active=$this->input->post("active");
			$flag=$this -> Model_catalogue-> update_vendors_selected($selected_list,$active);		
			if($flag==true){
				$selected_inv=$this->Model_catalogue->get_vendor_removed_inventories($selected_list);
				if($selected_inv!=''){
					$flag_deleted=$this->delete_search_index_selected($selected_inv);
					if($flag_deleted==true){
						echo true;
					}else{
						echo 0;
					}	
				}else{
					echo true;
				}	
			}else{
				echo 0;
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
			
	}
	public function logistics(){
		
		if($this->session->userdata("logged_in")){
			$data['vendor_id']=$this->input->post("vendor_id");
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data['vendors'] = $this->Model_catalogue->vendors();
			$data['create_editview']=$this->input->post("create_editview");
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_view',$data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function logistics_filter(){
		
		if($this->session->userdata("logged_in")){		
			$data['logistics_id']=$this->input->post("logistics_id");
			$data['vendor_id']=$this->input->post("vendor_id");
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data['vendors'] = $this->Model_catalogue->vendors();
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_sort_order_options_for_logistics($vendor_id){
		if($this->session->userdata("logged_in")){
			$get_all_logistics_arr=$this->Model_catalogue->get_all_logistics($vendor_id);
			
				$subcat_id_arr=array();
				foreach($get_all_logistics_arr as $logistics_arr){
					$subcat_id_arr[]=$logistics_arr["logistics_priority"];
				}
				
				if(count($subcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($subcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				
				$miss_arr=array_diff($base_arr,$subcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				
				if(in_array($max_val_in_base_arr,$subcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				if(count($miss_arr)>0){
					$subcategory_arr["key"]="Sort Order Suggestion";
					$subcategory_arr["value"]=implode(",",$miss_arr);
				}
				else{
					$subcategory_arr["key"]="Sort Order Suggestion";
					$subcategory_arr["value"]=max($subcat_id_arr)+1;
				}
				}else{
					$subcategory_arr["key"]="";
					$subcategory_arr["value"]="No Subcategory";
				}
				return $subcategory_arr;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_logistics_form(){
		
		if($this->session->userdata("logged_in")){
			
			$logistics_id=$this->input->post("logistics_id");
			$data["controller"]=$this;
			$data['vendor_id']=$this->input->post("vendor_id");
			$data['logistics_id']=$this->input->post("logistics_id");
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data['vendors'] = $this->Model_catalogue->vendors();
			$data['create_editview']=$this->input->post("create_editview");
			$data["get_logistics_data"]=$this->get_logistics_data($logistics_id);
			$get_logistics_data=$data["get_logistics_data"];
			$data["logistics_id"]=$logistics_id;
			
			$data["get_sort_order_options_for_logistics"]=$this->get_sort_order_options_for_logistics($data['vendor_id']);
			
			$data["logistics_sort_order_arr"]=$this->Model_catalogue->show_logistics_edit_sort_order($data["get_logistics_data"]["logistics_priority"],$data["get_logistics_data"]["vendor_id"]);
			
			$logistics_sort_order_arr=$data["logistics_sort_order_arr"];
			
			
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_logistics_view',$data);
			
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
		
	}
	public function add_logistics() {	
		if($this->session->userdata("logged_in")){
			$logistics_name=$this->db->escape_str($this -> input -> post("logistics_name"));
			$logistics_weblink=$this->db->escape_str($this -> input -> post("logistics_weblink"));
			$vendor_id=$this->input->post("vendors");
			$tracking_id_generation=$this->input->post("tracking_id_generation");
			$logistics_priority=$this->input->post("logistics_priority");
			$default_logistics=$this->input->post("view");	
			$logistics_volumetric_value=$this->input->post("logistics_volumetric_value");
			$logistics_gst=$this->input->post("logistics_gst");
			$logistics_fuelsurcharge=$this->input->post("logistics_fuelsurcharge");
			if(trim($logistics_gst)==""){
				$logistics_gst=0;
			}
			if(trim($logistics_fuelsurcharge)==""){
				$logistics_fuelsurcharge=0;
			}
			
			$flag=$this->Model_catalogue->add_logistics($logistics_name,$logistics_weblink,$vendor_id,$tracking_id_generation,$logistics_priority,$default_logistics,$logistics_volumetric_value,$logistics_gst,$logistics_fuelsurcharge);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function get_logistics_data($logistics_id){
		if($this->session->userdata("logged_in")){
			$logistics_arr=$this->Model_catalogue->get_logistics_data($logistics_id);
			return $logistics_arr;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_logistics(){		
		if($this->session->userdata("logged_in")){
			$logistics_id=$this->input->post("edit_logistics_id");
			$logistics_name=$this->db->escape_str($this -> input -> post("edit_logistics_name"));
			$logistics_weblink=$this->db->escape_str($this -> input -> post("edit_logistics_weblink"));
			$vendor_id=$this->input->post("edit_vendors");
			
			$tracking_id_generation=$this->input->post("edit_tracking_id_generation");
			
			$default_logistics=$this->input->post("edit_view");
			$logistics_priority=$this->input->post("edit_logistics_priority");
			$common_logistics=$this->input->post("common_logistics");
			$logistics_priority_default=$this->input->post("edit_logistics_priority_default");
			$logistics_volumetric_value=$this->input->post("edit_logistics_volumetric_value");
			
			$logistics_gst=$this->input->post("edit_logistics_gst");
			$logistics_fuelsurcharge=$this->input->post("edit_logistics_fuelsurcharge");
			if(trim($logistics_gst)==""){
				$logistics_gst=0;
			}
			if(trim($logistics_fuelsurcharge)==""){
				$logistics_fuelsurcharge=0;
			}
			
			
			
			$flag=$this->Model_catalogue->edit_logistics($logistics_id,$logistics_name,$logistics_weblink,$vendor_id,$tracking_id_generation,$logistics_priority,$common_logistics,$logistics_priority_default,$default_logistics,$logistics_volumetric_value,$logistics_gst,$logistics_fuelsurcharge);
			
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}	
	public function delete_logistics_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_logistics_selected($selected_list);			
			echo $flag;	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function logistics_service(){
		
		if($this->session->userdata("logged_in")){
			$data['vendor_id']=$this->input->post("vendor_id");
			$data['logistics_id']=$this->input->post("logistics_id");
			$data['vendors'] = $this->Model_catalogue->vendors();	
			$data['logistics_service'] = $this->Model_catalogue->logistics_service();
			$data['logistics_all_pincodes']= $this->Model_catalogue->logistics_all_pincodes();
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data['create_editview']=$this->input->post("create_editview");
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_service_view',$data);	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function logistics_service_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['vendor_id']=$this->input->post("vendors");
			$data['logistics_id']=$this->input->post("logistics");
			$data['logistics_service_id']=$this->input->post("logistics_service");
			$data['vendors'] = $this->Model_catalogue->vendors();	
			$data['logistics_service'] = $this->Model_catalogue->logistics_service();
			$data['logistics_all_pincodes']= $this->Model_catalogue->logistics_all_pincodes();
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_service_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_logistics_service_form(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$logistics_service_id=$this->input->post("logistics_service_id");
			$logistics_id=$this->input->post("logistics_id");
			$data['vendor_id']=$this->input->post("vendors");
			$data['logistics_id']=$this->input->post("logistics");
			$data['logistics_service_id']=$this->input->post("logistics_service");
			$data['territory_name']=$this->input->post("territory_name");
			$data['vendors'] = $this->Model_catalogue->vendors();	
			$data['logistics_service'] = $this->Model_catalogue->logistics_service();
			$data['logistics_all_pincodes']= $this->Model_catalogue->logistics_all_pincodes();
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data['create_editview']=$this->input->post("create_editview");
			$data["get_logistics_service_data"]=$this->get_logistics_service_data($logistics_service_id);
			$get_logistics_service_data=$data["get_logistics_service_data"];
			
			$data["logistics_service_id"]=$logistics_service_id;
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_logistics_service_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function check_break_point_is_there_or_not_logistics_weight(){
		if($this->session->userdata("logged_in")){
			
			$vendor_id=$this->input->post("vendor_id");
			$logistics_id=$this->input->post("logistics_id");
			$logistics_territory_id=$this->input->post("logistics_territory_id");
			$logistics_delivery_mode_id=$this->input->post("logistics_delivery_mode_id");
			$logistics_parcel_category_id=$this->input->post("logistics_parcel_category_id");
			$check_break_point_is_there_or_not_logistics_weight_status=$this->Model_catalogue->check_break_point_is_there_or_not_logistics_weight($vendor_id,$logistics_id,$logistics_territory_id,$logistics_delivery_mode_id,$logistics_parcel_category_id);
			echo $check_break_point_is_there_or_not_logistics_weight_status;
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_logistics(){
		if($this->session->userdata("logged_in")){
			$vendor_id=$this->input->post("vendor_id");
			$result_logi=$this->Model_catalogue->show_available_logistics($vendor_id);
			if(count($result_logi)!=0){
				$str='';
				$str.='<option value=""></option>';			
				foreach($result_logi as $res){				
					$str.='<option value="'.$res->logistics_id.'">'.$res->logistics_name.'</option>';			
				}
				echo $str;
			}
			else{
				echo count($result_logi);
			}
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function add_logistics_service() {
		
		if($this->session->userdata("logged_in")){
			$city_org=$this->db->escape_str($this -> input -> post("city"));
			$city=strtolower($city_org);
			$pin_org=$this->db->escape_str($this -> input -> post("pin"));
			$pin_arr=explode('_',$pin_org);
			$pin=$pin_arr[0];
			$pincode=implode('|',$this->input->post("to_select_list"));
			$logistics_id=$this->input->post("logistics");
			$flag=$this->Model_catalogue->add_logistics_service($city,$pin,$pincode,$logistics_id);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function get_available_logistics_pincodes(){
		if($this->session->userdata("logged_in")){
			$pin=$this->input->post("pin");
			$pincodes=$this->Model_catalogue->get_available_logistics_pincodes($pin);
			echo json_encode($pincodes);		
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_logistics_service_data($logistics_service_id){
		if($this->session->userdata("logged_in")){
			//$logistics_service_id=$this->input->post("logistics_service_id");
			$logistics_service_arr=$this->Model_catalogue->get_logistics_service_data($logistics_service_id);
			return $logistics_service_arr;	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_logistics_service(){		
		if($this->session->userdata("logged_in")){
			$logistics_service_id=$this->input->post("edit_logistics_service_id");
			$city_org=$this->db->escape_str($this -> input -> post("edit_city"));
			$city=strtolower($city_org);
			$pin_org=$this->db->escape_str($this -> input -> post("edit_pin"));
			$pin_arr=explode('_',$pin_org);
			$pin=$pin_arr[0];
			$pincode=implode('|',$this->input->post("edit_to_select_list"));
			$logistics_id=$this->input->post("edit_logistics");
			$flag=$this->Model_catalogue->edit_logistics_service($logistics_service_id,$city,$pin,$pincode,$logistics_id);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}		
	public function delete_logistics_service_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_logistics_service_selected($selected_list);	
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
			
	}
	public function logistics_territory(){
		
		if($this->session->userdata("logged_in")){
			$data['vendor_id']=$this->input->post("vendor_id");
			$data['logistics_id']=$this->input->post("logistics_id");
			$data['logistics_service_id']=$this->input->post("logistics_service");
			$data['territory_name']=$this->input->post("territory_name");
			$logistics_territory_id=$this->input->post("logistics_territory_id");
			$data['logistics_territory'] = $this->Model_catalogue->logistics_territory();	
			$data['logistics_service'] = $this->Model_catalogue->logistics_service();
			$data['vendors'] = $this->Model_catalogue->vendors();
			$data['logistics_all_territory_name'] = $this->Model_catalogue->logistics_all_territory_name();
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data['create_editview']=$this->input->post("create_editview");
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_territory_view',$data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function logistics_territory_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['vendor_id']=$this->input->post("vendors");
			$data['logistics_id']=$this->input->post("logistics");
			$data['logistics_service_id']=$this->input->post("logistics_service");
			$data['territory_name']=$this->input->post("territory_name");
			$data['logistics_territory'] = $this->Model_catalogue->logistics_territory();	
			$data['logistics_service'] = $this->Model_catalogue->logistics_service();
			$data['vendors'] = $this->Model_catalogue->vendors();
			$data['logistics_all_territory_name'] = $this->Model_catalogue->logistics_all_territory_name();
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_territory_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_logistics_territory_form(){
		if($this->session->userdata("logged_in")){
			$logistics_territory_id=$this->input->post("logistics_territory_id");
			$data["controller"]=$this;
			$data['vendor_id']=$this->input->post("vendors");
			$data['logistics_id']=$this->input->post("logistics");
			$data['logistics_service_id']=$this->input->post("logistics_service");
			$data['territory_name']=$this->input->post("territory_name");
			$data['logistics_territory'] = $this->Model_catalogue->logistics_territory();	
			$data['logistics_service'] = $this->Model_catalogue->logistics_service();
			$data['vendors'] = $this->Model_catalogue->vendors();
			$data['logistics_all_territory_name'] = $this->Model_catalogue->logistics_all_territory_name();
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data['create_editview']=$this->input->post("create_editview");
			$data["get_logistics_territory_data"]=$this->get_logistics_territory_data($logistics_territory_id);
			$get_logistics_territory_data=$data["get_logistics_territory_data"];
			$data["logistics_territory_id"]=$logistics_territory_id;
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_logistics_territory_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function add_logistics_territory(){
		
		if($this->session->userdata("logged_in")){		
			$vendor_id=$this->input->post("vendors");
			$logistics_service_id=$this->input->post("logistics_service");
			$territory_name=$this->db->escape_str($this -> input -> post("territory_name"));
			$flag=$this->Model_catalogue->add_logistics_territory($vendor_id,$logistics_service_id,$territory_name);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function get_logistics_territory_data($logistics_territory_id){
		if($this->session->userdata("logged_in")){
			//$logistics_territory_id=$this->input->post("logistics_territory_id");
			$logistics_territory_arr=$this->Model_catalogue->get_logistics_territory_data($logistics_territory_id);
			return $logistics_territory_arr;	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_logistics_territory(){		
		if($this->session->userdata("logged_in")){
			$logistics_territory_id=$this->input->post("edit_logistics_territory_id");
			$vendor_id=$this->input->post("edit_vendors");
			$logistics_service_id=$this->input->post("edit_logistics_service");
			$territory_name=$this->db->escape_str($this -> input -> post("edit_territory_name"));
			
			$flag=$this->Model_catalogue->edit_logistics_territory($logistics_territory_id,$vendor_id,$logistics_service_id,$territory_name);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}		
	public function delete_logistics_territory_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_logistics_territory_selected($selected_list);	
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	
	public function logistics_delivery_mode_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['vendors'] = $this->Model_catalogue->vendors();	
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_delivery_mode_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function logistics_delivery_mode(){
		
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$logistics_id=$this->input->post("logistics_id");
			$vendor_id=$this->input->post("vendor_id");
			$data['create_editview']=$this->input->post("create_editview");
			$data['logistics_delivery_mode'] = $this->Model_catalogue->logistics_delivery_mode_by_logistics_id($vendor_id,$logistics_id);
			$data["menu_flag"]="shipping_active_links";
			$data['vendors'] = $this->Model_catalogue->vendors();
			
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_delivery_mode_view',$data);	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function edit_logistics_delivery_mode_form(){
		if($this->session->userdata("logged_in")){
			$logistics_id=$this->input->post("logistics_id");
			$vendor_id=$this->input->post("vendor_id");
			$logistics_delivery_mode_id=$this->input->post("logistics_delivery_mode_id");
			$data["controller"]=$this;
			
			
			$data['logistics_delivery_mode'] = $this->Model_catalogue->logistics_delivery_mode_by_logistics_id($vendor_id,$logistics_id);
			$data["get_logistics_delivery_mode_data"]=$this->get_logistics_delivery_mode_data($logistics_delivery_mode_id);
			$get_logistics_delivery_mode_data=$data["get_logistics_delivery_mode_data"];
			
			$speed_territory_duration_json=$get_logistics_delivery_mode_data["speed_duration"];
			$speed_territory_duration_arr=json_decode($speed_territory_duration_json,true);
			$result_logi=$this->Model_catalogue->show_available_logistics_territories_for_delivery_modes($logistics_id);
			$speed_territory_duration_table='';
			if(count($result_logi)!=0){
				$speed_territory_duration_table.='<table class="table"><caption>Speed Duration(Days)</caption>';
				foreach($result_logi as $res){
					$speed_territory_duration_table.='<tr>';
					$speed_territory_duration_table.='<td>';
						$speed_territory_duration_table.=ucwords($res->territory_name);
					$speed_territory_duration_table.='</td>';
					$speed_territory_duration_table.='<td>';
						$speed_territory_duration_table.='<input onkeypress="return isNumber(event);"  required type="text" name="edit_speed_territory_duration['.$res->territory_name_id.']" class="form-control" value="'.$speed_territory_duration_arr[$res->territory_name_id].'">';
					$speed_territory_duration_table.='</td>';
					$speed_territory_duration_table.='</tr>';
				}
				$speed_territory_duration_table.='</table>';
			}
			
			$data["speed_territory_duration_table"]=$speed_territory_duration_table;
			$data["logistics_delivery_mode_id"]=$logistics_delivery_mode_id;
			$data['vendors'] = $this->Model_catalogue->vendors();
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_logistics_delivery_mode_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function add_logistics_delivery_mode(){
		
		if($this->session->userdata("logged_in")){
			
			$vendor_id=$this->db->escape_str($this -> input -> post("vendors"));
			$logistics_id=$this->db->escape_str($this -> input -> post("logistics"));
			$delivery_mode=trim($this->db->escape_str($this -> input -> post("delivery_mode")));
			$speed_territory_duration=json_encode($this->db->escape_str($this -> input -> post("speed_territory_duration")));
			$default_logistics=$this->input->post("default_logistics");
			$flag=$this->Model_catalogue->check_duplication_name_delivery_mode($delivery_mode,$logistics_id,$vendor_id);
			if($flag==true){
				echo "exist";
			}else{
				
			$update_flag=$this->Model_catalogue->add_logistics_delivery_mode($vendor_id,$logistics_id,$delivery_mode,$speed_territory_duration,$default_logistics);
				if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function get_logistics_delivery_mode_data($logistics_delivery_mode_id){
		if($this->session->userdata("logged_in")){
			$logistics_delivery_mode_arr=$this->Model_catalogue->get_logistics_delivery_mode_data($logistics_delivery_mode_id);
			return $logistics_delivery_mode_arr;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_logistics_delivery_mode(){		
		if($this->session->userdata("logged_in")){
			$logistics_delivery_mode_id=$this->input->post("edit_logistics_delivery_mode_id");
			$delivery_mode=$this->db->escape_str($this -> input -> post("edit_delivery_mode"));
			$delivery_mode_default=$this->db->escape_str($this -> input -> post("edit_delivery_mode_default"));
			$speed_territory_duration=json_encode($this->db->escape_str($this -> input -> post("edit_speed_territory_duration")));
			$default_logistics=$this->input->post("edit_default_logistics");
			$flag_check="";
			if($delivery_mode_default!=$delivery_mode){
			$flag_check=$this->Model_catalogue->check_duplication_name_delivery_mode_edit($delivery_mode,$logistics_delivery_mode_id);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->edit_logistics_delivery_mode($logistics_delivery_mode_id,$delivery_mode,$speed_territory_duration,$default_logistics);
				if($update_flag==true){
					echo true;
				}else{
					echo 0;
				}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}		
	public function delete_logistics_delivery_mode_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_logistics_delivery_mode_selected($selected_list);
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
		
	
	public function logistics_weight(){
		
		if($this->session->userdata("logged_in")){
			$data['vendor_id']=$this->input->post("vendor_id");
			$data['logistics_id']=$this->input->post("logistics_id");
			$data['logistics_territory_id']=$this->input->post("logistics_territory_id");
			$data['logistics_delivery_mode_id']=$this->input->post("delivery_mode");
			$data['logistics_parcel_category_id']=$this->input->post("parcel_type");
			$data['vendors'] = $this->Model_catalogue->vendors();	
			$data['logistics_weight'] = $this->Model_catalogue->logistics_weight();	
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data['create_editview']=$this->input->post("create_editview");
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_weight_view',$data);		
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function logistics_weight_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['vendor_id']=$this->input->post("vendors");
			$data['logistics_id']=$this->input->post("logistics");
			$data['logistics_service_id']=$this->input->post("logistics_service");
			$data['territory_name']=$this->input->post("territory_name");
			//$data['logistics_territory'] = $this->Model_catalogue->logistics_territory();	
			$data['logistics_service'] = $this->Model_catalogue->logistics_service();
			$data['vendors'] = $this->Model_catalogue->vendors();
			$data['logistics_all_territory_name'] = $this->Model_catalogue->logistics_all_territory_name();
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_weight_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_logistics_weight_form(){
		if($this->session->userdata("logged_in")){
			
			$logistics_delivery_mode_id=$this->input->post("logistics_delivery_mode_id");
			$logistics_parcel_category_id=$this->input->post("logistics_parcel_category_id");
			$logistics_id=$this->input->post("logistics_id");
			$weight_in_kg=$this->input->post("weight_in_kg");
			
			$data['logistics_id']=$logistics_id;
			$data['weight_in_kg']=$weight_in_kg;
			
			$data["logistics_delivery_mode_id"]=$logistics_delivery_mode_id;
			$get_logistics_delivery_mode_by_logistics_delivery_mode_id_arr=$this->Model_catalogue->get_logistics_delivery_mode_by_logistics_delivery_mode_id($logistics_delivery_mode_id);
			$data["delivery_mode"]=$get_logistics_delivery_mode_by_logistics_delivery_mode_id_arr["delivery_mode"];
			$data["logistics_parcel_category_id"]=$logistics_parcel_category_id;
			$get_logistics_parcel_category_by_logistics_parcel_category_id_arr=$this->Model_catalogue->get_logistics_parcel_category_by_logistics_parcel_category_id($logistics_parcel_category_id);
			$data["parcel_type"]=$get_logistics_parcel_category_by_logistics_parcel_category_id_arr["parcel_type"];
			//print_r($this->input->post());
			//exit;
			
			$logistics_weight_id=$this->input->post("logistics_weight_id");
			
			
			
			
			$data["controller"]=$this;
			
			$data['vendor_id']=$this->input->post("vendors");
			//$data['logistics_id']=$this->input->post("logistics");
			$data['logistics_service_id']=$this->input->post("logistics_service");
			$data['territory_name']=$this->input->post("territory_name");
			$data['logistics_territory_id']=$this->input->post("logistics_territory_id");
			
			$data['logistics_territory'] = $this->Model_catalogue->logistics_territory_by_logistics_id($data['logistics_id']);
			
			$data['vendors'] = $this->Model_catalogue->vendors();	
			$data['logistics_weight'] = $this->Model_catalogue->logistics_weight();	
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data['create_editview']=$this->input->post("create_editview");
			
			$data["get_logistics_weight_data"]=$this->Model_catalogue->get_logistics_weight_single($data['logistics_territory_id'],$logistics_id,$logistics_delivery_mode_id,$logistics_parcel_category_id,$weight_in_kg);
			
			$data["volumetric_data"]=$this->Model_catalogue->get_all_volumetric_weights($data['logistics_territory_id'],$logistics_delivery_mode_id,$logistics_parcel_category_id,$weight_in_kg,$logistics_id,1);
			
			//print_r($data["volumetric_data"]);
			
			$get_logistics_weight_data=$data["get_logistics_weight_data"];
			
			$data["logistics_weight_id"]=$logistics_weight_id;
			
			$data["menu_flag"]="shipping_active_links";
			
			$data["check_break_point_is_there_or_not_logistics_weight_edit_status"]=$this->Model_catalogue->check_break_point_is_there_or_not_logistics_weight_edit($data['vendor_id'],$logistics_id,$data['logistics_territory_id'],$logistics_delivery_mode_id,$logistics_parcel_category_id,$logistics_weight_id);
			
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_logistics_weight_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function add_logistics_weight(){
		
		if($this->session->userdata("logged_in")){
			$common=array();
			
			$logistics_id=$this->input->post("logistics");
			$territory_name_id=$this->input->post("logistics_territory");
			$logistics_delivery_mode_id=$this->input->post("delivery_mode");
			$logistics_parcel_category_id=$this->input->post("parcel_type");
			$weight_in_kg=$this->db->escape_str($this -> input -> post("weight_in_kg"));
			$lesser_v=$this->input->post("lesser_v");
			$lesser_m=$this->input->post("lesser_m");
			$additional_weight_in_kg=$this->input->post("additional_weight_in_kg");
			if($additional_weight_in_kg=="no"){
				$do_u_want_to_roundup="";
				$additional_weight_value_in_kg="";
				$do_u_want_to_include_breakupweight="";
			}
			if($additional_weight_in_kg=="yes"){
				$additional_weight_value_in_kg=$this->input->post("additional_weight_value_in_kg");
				$do_u_want_to_roundup=$this->input->post("do_u_want_to_roundup");
				if($do_u_want_to_roundup=="yes"){
					$do_u_want_to_include_breakupweight=$this->input->post("do_u_want_to_include_breakupweight");
				}
				else{
					$do_u_want_to_include_breakupweight="";
				}
			}
				
			/*
			$v_from_arr=$this->input->post("v_from");
			$v_to_arr=$this->input->post("v_to");
			$v_range=array();
			for($i=0;$i<count($v_from_arr);$i++){
				$v_range[]=trim($v_from_arr[$i]).'-'.trim($v_to_arr[$i]);
			}
			$v_m_arr=$this->input->post("v_m");
			$greater_v=$this->input->post("greater_v");
			$greater_m=$this->input->post("greater_m");
			*/
			$v_range=array();
			$v_m_arr=array();
			$greater_v="";
			$greater_m="";
			
			$flag=$this->Model_catalogue->add_logistics_weight($logistics_id,$territory_name_id,$logistics_delivery_mode_id,$logistics_parcel_category_id,$weight_in_kg,$lesser_v,$lesser_m,$v_range,$v_m_arr,$greater_v,$greater_m,$additional_weight_in_kg,$additional_weight_value_in_kg,$do_u_want_to_roundup,$do_u_want_to_include_breakupweight);
			
			echo $flag;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_logistics_weight_data($logistics_weight_id){
		if($this->session->userdata("logged_in")){
			$logistics_weight_arr=$this->Model_catalogue->get_logistics_weight_data($logistics_weight_id);
			return $logistics_weight_arr;	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_logistics_weight(){		
		if($this->session->userdata("logged_in")){
			
			//print_r($this->input->post());

			$logistics_weight_id=$this->input->post("edit_logistics_weight_id");
			$logistics_id=$this->input->post("edit_logistics");
			$territory_name_id=$this->input->post("edit_logistics_territory");
			
			$lesser_v=$this->input->post("lesser_v");
			$lesser_m=$this->input->post("lesser_m");
			$lesser_id=$this->input->post("lesser_id");
			
			$additional_weight_in_kg=$this->input->post("additional_weight_in_kg");
			if($additional_weight_in_kg=="no"){
				$additional_weight_value_in_kg="";
				$do_u_want_to_roundup="";
				$do_u_want_to_include_breakupweight="";
			}
			if($additional_weight_in_kg=="yes"){
				$additional_weight_value_in_kg=$this->input->post("additional_weight_value_in_kg");
				$do_u_want_to_roundup=$this->input->post("do_u_want_to_roundup");
				if($do_u_want_to_roundup=="yes"){
					$do_u_want_to_include_breakupweight=$this->input->post("do_u_want_to_include_breakupweight");
				}
				else{
					$do_u_want_to_include_breakupweight="";
				}
			}
			
			/*
			$v_from_arr=$this->input->post("v_from");
			$v_to_arr=$this->input->post("v_to");
			$v_range=array();
			for($i=0;$i<count($v_from_arr);$i++){
				$v_range[]=trim($v_from_arr[$i]).'-'.trim($v_to_arr[$i]);
			}
			$v_m_arr=$this->input->post("v_m");
			$v_ids_arr=$this->input->post("v_ids");
			$original_ids=$this->input->post("original_ids");
			$diff=array();
			if(count($original_ids)>count($v_ids_arr)){
				$diff=array_diff($original_ids,$v_ids_arr);
			}

			$greater_v=$this->input->post("greater_v");
			$greater_m=$this->input->post("greater_m");
			$greater_id=$this->input->post("greater_id");
			*/
			$v_range=array();
			$v_m_arr=array();
			$v_ids_arr=array();
			$greater_v="";
			$greater_m="";
			$greater_id="";
			$diff=array();
			
			$weight_in_kg=$this->db->escape_str($this -> input -> post("edit_weight_in_kg"));
			//$volume_in_kg=$this->db->escape_str($this -> input -> post("edit_volume_in_kg"));
			
			$logistics_price=$this->db->escape_str($this -> input -> post("edit_logistics_price"));
			$flag=$this->Model_catalogue->edit_logistics_weight($logistics_id,$territory_name_id,$weight_in_kg,$lesser_v,$lesser_m,$lesser_id,$v_range,$v_m_arr,$v_ids_arr,$greater_v,$greater_m,$greater_id,$diff,$additional_weight_in_kg,$additional_weight_value_in_kg,$do_u_want_to_roundup,$do_u_want_to_include_breakupweight);
			
			echo $flag;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}		
	public function delete_logistics_weight_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_logistics_weight_selected($selected_list);
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	public function logistics_parcel_category_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['vendors'] = $this->Model_catalogue->vendors();	
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_parcel_category_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function logistics_parcel_category(){
		
		if($this->session->userdata("logged_in")){
			$logistics_id=$this->input->post("logistics_id");
			$vendor_id=$this->input->post("vendor_id");
			$data['create_editview']=$this->input->post("create_editview");
			$data['logistics_parcel_category'] = $this->Model_catalogue->logistics_parcel_category_by_logistics_id($vendor_id,$logistics_id);
			$data['vendors'] = $this->Model_catalogue->vendors();
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_parcel_category_view',$data);		
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function edit_logistics_parcel_category_form(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$logistics_parcel_category_id=$this->input->post("logistics_parcel_category_id");
			$data['vendor_id']=$this->input->post("vendor_id");
			$data['logistics_id']=$this->input->post("logistics_id");
			$data['logistics_service_id']=$this->input->post("logistics_service");
			$data['territory_name']=$this->input->post("territory_name");
			$data["get_logistics_parcel_category_data"]=$this->get_logistics_parcel_category_data($logistics_parcel_category_id);
			$get_logistics_parcel_category_data=$data["get_logistics_parcel_category_data"];
			$data["logistics_parcel_category_id"]=$logistics_parcel_category_id;
			$data['vendors'] = $this->Model_catalogue->vendors();
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_logistics_parcel_category_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function add_logistics_parcel_category(){
		
		if($this->session->userdata("logged_in")){
			$vendor_id=$this->db->escape_str($this -> input -> post("vendors"));
			$logistics_id=$this->db->escape_str($this -> input -> post("logistics"));
			$parcel_type_org=$this->db->escape_str($this -> input -> post("parcel_type"));
			$parcel_type=strtolower($parcel_type_org);
			$parcel_type_description=$this->db->escape_str($this -> input -> post("parcel_type_description"));
			
			$default_logistics=$this->input->post("default_logistics");
			$flag=$this->Model_catalogue->check_duplication_name_parcel_category($parcel_type,$vendor_id,$logistics_id);
			if($flag==true){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->add_logistics_parcel_category($vendor_id,$logistics_id,$parcel_type,$parcel_type_description,$default_logistics);
				if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}
	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function get_logistics_parcel_category_data($logistics_parcel_category_id){
		if($this->session->userdata("logged_in")){
			$logistics_parcel_category_arr=$this->Model_catalogue->get_logistics_parcel_category_data($logistics_parcel_category_id);
			return $logistics_parcel_category_arr;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_logistics_parcel_category(){		
		if($this->session->userdata("logged_in")){
			$vendor_id=$this->input->post("edit_vendors");
			$logistics_id=$this->input->post("edit_logistics");
			$logistics_parcel_category_id=$this->input->post("edit_logistics_parcel_category_id");
			$parcel_type_org=$this->db->escape_str($this -> input -> post("edit_parcel_type"));
			$parcel_type=strtolower($parcel_type_org);
			$parcel_type_org_default=$this->db->escape_str($this -> input -> post("edit_parcel_type_default"));
			$parcel_type_default=strtolower($parcel_type_org_default);
			
			$parcel_type_description=$this->db->escape_str($this -> input -> post("edit_parcel_type_description"));
			$default_logistics=$this->input->post("edit_default_logistics");
			$flag_check="";
			if($parcel_type_default!=$parcel_type){
			$flag_check=$this->Model_catalogue->check_duplication_name_parcel_category_edit($parcel_type,$logistics_parcel_category_id);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->edit_logistics_parcel_category($vendor_id,$logistics_id,$logistics_parcel_category_id,$parcel_type,$parcel_type_description,$default_logistics);
				if($update_flag==true){
					echo true;
				}else{
					echo 0;
				}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}		
	public function delete_logistics_parcel_category_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_logistics_parcel_category_selected($selected_list);
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	public function logistics_all_pincodes(){
		if($this->session->userdata("logged_in")){
			$data['logistics_all_pincodes']= $this->Model_catalogue->logistics_all_pincodes();
			$data['logistics_all_states'] = $this->Model_catalogue->logistics_all_states();
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_all_pincodes_view',$data);	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function edit_logistics_all_pincodes_form(){
		if($this->session->userdata("logged_in")){
			$pincode_id=$this->input->post("pincode_id");
			$data["controller"]=$this;
			$data['vendor_id']=$this->input->post("vendors");
			$data['logistics_id']=$this->input->post("logistics");
			$data['logistics_service_id']=$this->input->post("logistics_service");
			$data['territory_name']=$this->input->post("territory_name");
			$data['logistics_all_states'] = $this->Model_catalogue->logistics_all_states();
			$data['logistics_all_pincodes'] = $this->Model_catalogue->logistics_all_pincodes();
			$data["get_logistics_all_pincodes_data"]=$this->get_logistics_all_pincodes_data($pincode_id);
			$get_logistics_all_pincodes_data=$data["get_logistics_all_pincodes_data"];
			$data["pincode_id"]=$pincode_id;
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_logistics_all_pincodes_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function add_logistics_all_pincodes(){
		
		if($this->session->userdata("logged_in")){
			$state_id=$this->db->escape_str($this -> input -> post("state"));
			$city_org=$this->db->escape_str($this -> input -> post("city"));
			$city=strtolower($city_org);
			$pin=$this->db->escape_str($this -> input -> post("pin"));
			$pincode_from=$this->input->post("pincode_from");
			$pincode_to=$this->input->post("pincode_to");
			$distinct=$this->input->post("distinct_pincodes");
			
			$len=count($pincode_from);
			$form_pincode='';
			$val='';$pincodes_range='';
			$k=1;
			for($i=0;$i<$len;$i++){
			
				for($j=$pincode_from[$i];$j<=$pincode_to[$i];$j++){
				
					if($k==$len){
						if($j==$pincode_to[$i]){
							$val.=$j;
						}else{
							$val.=$j.'|';
						}	
					}else{
					$val.=$j.'|';
					}
				}
				if($k==$len){
					$pincodes_range.=$pincode_from[$i].'-'.$pincode_to[$i];
				}else{
					$pincodes_range.=$pincode_from[$i].'-'.$pincode_to[$i].'|';
				}
				$k++;
			}
			
			if($distinct!=''){	
				$distinct_pincodes=array_filter($distinct);
				$distinct_pincodes=implode('|',$distinct_pincodes);
				$pincodes=$val.'|'.$distinct_pincodes;
			}else{
				$distinct_pincodes=$distinct;
				$pincodes=$val;
			}
			$flag=$this->Model_catalogue->check_duplication_name_all_pincodes($city);
			if($flag==true){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->add_logistics_all_pincodes($state_id,$city,$pin,$pincodes,$pincodes_range,$distinct_pincodes);
				if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}
	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function get_logistics_all_pincodes_data($pincode_id){
		if($this->session->userdata("logged_in")){
			$logistics_all_pincodes_arr=$this->Model_catalogue->get_logistics_all_pincodes_data($pincode_id);
			return $logistics_all_pincodes_arr;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_logistics_all_pincodes(){		
		if($this->session->userdata("logged_in")){
			$pincode_id=$this->input->post("edit_pincode_id");
			$state_id=$this->db->escape_str($this -> input -> post("edit_state"));
			
			$city_org=$this->db->escape_str($this -> input -> post("edit_city"));
			$city=strtolower($city_org);
			$city_org_default=$this->db->escape_str($this -> input -> post("edit_city_default"));
			$city_default=strtolower($city_org_default);
			
			$pin=$this->db->escape_str($this -> input -> post("edit_pin"));
			$pincode_from=$this->input->post("edit_pincode_from");
			$pincode_to=$this->input->post("edit_pincode_to");
			
			$distinct_pincodes=array_filter($this->input->post("edit_distinct_pincodes"));
			$len=count($pincode_from);
			$form_pincode='';
			
			$val='';$pincodes_range='';
			$k=1;
			for($i=0;$i<$len;$i++){
			
				for($j=$pincode_from[$i];$j<=$pincode_to[$i];$j++){
					if($k==$len){
						if($j==$pincode_to[$i]){
							$val.=$j;
						}else{
							$val.=$j.'|';
						}
						
					}else{
						$val.=$j.'|';
					}
				}
				if($k==$len){
					$pincodes_range.=$pincode_from[$i].'-'.$pincode_to[$i];
				}else{
					$pincodes_range.=$pincode_from[$i].'-'.$pincode_to[$i].'|';
				}
				$k++;
			}
			
			$distinct_pincodes=implode('|',$distinct_pincodes);
			$pincodes=$val.'|'.$distinct_pincodes;
			
			$flag_check="";
			if($city_default!=$city){
			$flag_check=$this->Model_catalogue->check_duplication_name_all_pincodes($city);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->edit_logistics_all_pincodes($state_id,$pincode_id,$city,$pin,$pincodes,$pincodes_range,$distinct_pincodes);
				if($update_flag==true){
					echo true;
				}else{
					echo 0;
				}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function delete_logistics_all_pincodes_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_logistics_all_pincodes_selected($selected_list);
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	public function logistics_all_territory_name_filter() {
		
		if($this->session->userdata("logged_in")){
			$data['vendors'] = $this->Model_catalogue->vendors();	
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_all_territory_name_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function logistics_all_territory_name() {	
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$logistics_id=$this->input->post("logistics_id");
			$vendor_id=$this->input->post("vendor_id");
			$data['create_editview']=$this->input->post("create_editview");
			$data['vendors'] = $this->Model_catalogue->vendors();
			$data['logistics_all_territory_name'] = $this->Model_catalogue->logistics_all_territory_name_by_logistics_id($vendor_id,$logistics_id);
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_all_territory_name_view',$data);	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function edit_logistics_all_territory_name_form(){
		if($this->session->userdata("logged_in")){
			$logistics_id=$this->input->post("logistics_id");
			$check_make_as_local_logistics_all_territory_name_arr=$this->Model_catalogue->check_make_as_local_logistics_all_territory_name($logistics_id);
			if(!empty($check_make_as_local_logistics_all_territory_name_arr)){
				$data["check_make_as_local_logistics_all_territory_name_status"]="yes";
			}
			else{
				$data["check_make_as_local_logistics_all_territory_name_status"]="no";
			}
			$vendor_id=$this->input->post("vendor_id");
			$territory_name_id=$this->input->post("territory_name_id");
			$data["controller"]=$this;
			$data['territory_name_id']=$this->input->post("territory_name_id");
			$data['logistics_all_territory_name'] = $this->Model_catalogue->logistics_all_territory_name_by_logistics_id($vendor_id,$logistics_id);
			$data['vendors'] = $this->Model_catalogue->vendors();
			$data["get_logistics_all_territory_name_data"]=$this->get_logistics_all_territory_name_data($territory_name_id);
			$get_logistics_all_territory_name_data=$data["get_logistics_all_territory_name_data"];
			$get_logistics_data=$this->get_logistics_data($get_logistics_all_territory_name_data["logistics_id"]);
			$data["vendor_id_in_db"]=$get_logistics_data["vendor_id"];
			$data["logistics_id_in_db"]=$get_logistics_data["logistics_id"];
			$data["logistics_name_in_db"]=$get_logistics_data["logistics_name"];		
			$data["territory_name_id"]=$territory_name_id;
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_logistics_all_territory_name_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function check_make_as_local_logistics_all_territory_name(){
		$logistics_id=$this->input->post("logistics_id");
		
		$row=$this->Model_catalogue->check_make_as_local_logistics_all_territory_name($logistics_id);
		if(!empty($row)){
			echo "yes";
		}
		else{
			echo "no";
		}
	}
	public function add_logistics_all_territory_name() {
		
		if($this->session->userdata("logged_in")){
			
			$make_as_local=$this->input->post("make_as_local");
			$logistics_id=$this->input->post("logistics");
			$territory_name_org=$this->db->escape_str($this -> input -> post("territory_name"));
			$territory_name=strtolower($territory_name_org);			
			$flag=$this->Model_catalogue->add_logistics_all_territory_name($logistics_id,$territory_name,$make_as_local);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_logistics_all_territory_name_data($territory_name_id)
	{
		if($this->session->userdata("logged_in")){
			$logistics_all_territory_name_arr=$this->Model_catalogue->get_logistics_all_territory_name_data($territory_name_id);
			return $logistics_all_territory_name_arr;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_logistics_all_territory_name()
	{		
		if($this->session->userdata("logged_in")){
			if($this->input->post("edit_make_as_local")){
				$make_as_local=$this->input->post("edit_make_as_local");
				$make_as_local_skip="no";
			}
			else{
				$make_as_local="";
				$make_as_local_skip="yes";
			}
			$territory_name_id=$this->input->post("edit_territory_name_id");
			$territory_name_org=$this->db->escape_str($this -> input -> post("edit_territory_name"));
			$territory_name=strtolower($territory_name_org);	
			$flag=$this->Model_catalogue->edit_logistics_all_territory_name($territory_name_id,$territory_name,$make_as_local,$make_as_local_skip);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}		
	public function delete_logistics_all_territory_name_selected() {
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_logistics_all_territory_name_selected($selected_list);
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	public function create_search_index($inventory_id){
		
		include("assets/elastic/app/init_customer.php");
		$res=$this->Model_catalogue->create_search_index($inventory_id);
		if($res->inventory_type==2){// if it is addon then dont elastic search
			return true;
		}
		
		/*** combo price updates starts ***/
		$combo_pack_discount=0;
		$adm_settings=get_admin_combo_settings();
		
		if(!empty($adm_settings)){
			$combo_pack_min_items=$adm_settings->adm_combo_pack_min_items;
			$combo_pack_discount=$adm_settings->adm_combo_pack_discount;
			$combo_pack_shipping_charge=$adm_settings->adm_combo_pack_shipping_charge;
			$combo_pack_status=$adm_settings->adm_combo_pack_status;
		}
		
		 if($combo_pack_discount>0){
				$combo_sel_price=round($res->max_selling_price-($res->max_selling_price*$combo_pack_discount/100));
			}else{
				$combo_sel_price= $searchRes["_source"]["max_selling_price"];
			}
			/*** combo price updates ends ***/
			

		$parambody_arr=array();
		$pcat_id_find=$res->pcat_id;
		if($pcat_id_find==0){
			$pcat_name='';
		}else{
			$pcat_name=$this->Model_catalogue->get_parent_category_name_by_pcat_id($pcat_id_find);
		}
		$parambody_arr=array("inventory_id"=>$inventory_id,"sku_id"=>$res->sku_id,"sku_name"=>$res->sku_name,"product_id"=>$res->product_id,"product_name"=>$res->product_name,"product_description"=>$res->product_description,"pcat_id"=>$res->pcat_id,"cat_id"=>$res->cat_id,"subcat_id"=>$res->subcat_id,"filter_brand_id"=>$res->brand_id,"image"=>$res->image,"thumbnail"=>$res->thumbnail,"tax_percent"=>$res->tax_percent,"base_price"=>$res->base_price,"selling_price"=>$res->selling_price,"combo_sel_price"=>$combo_sel_price,"max_selling_price"=>$res->max_selling_price,"selling_discount"=>$res->selling_discount,"cat_name"=>$res->cat_name,"subcat_name"=>$res->subcat_name,"brand_name"=>$res->brand_name,"pcat_name"=>$pcat_name,"common_image"=>$res->common_image,'product_status'=>$res->product_status,'stock'=>$res->stock);
		
		$query_filters=$this->Model_catalogue->get_filter_id_new($res->inventory_id);
		foreach($query_filters as $res_filters){
			$filterbox_name=$this->Model_catalogue->get_filterbox_name($res_filters->filter_id);
			$filterbox_name = str_replace(' ', '_', $filterbox_name);
			
			$filterbox_type=$this->Model_catalogue->get_filterbox_type($res_filters->filter_id);
			$filter_options=$this->Model_catalogue->get_filter_options($res_filters->filter_id);
			$parambody_arr["filter_".$filterbox_name."_type"]=$filterbox_type;
			$parambody_arr["filter_".$filterbox_name."_filter_options"]=$filter_options;
			$parambody_arr["filter_".$filterbox_name."_id"]=$res_filters->filter_id;
		}
		
		$parambody_arr["sugg1"]=$res->sugg1;
		$parambody_arr["sugg2"]=$res->sugg2;
		$parambody_arr["generalsugg1"]=$res->generalsugg1;
		$parambody_arr["generalsugg2"]=$res->generalsugg2;
		$parambody_arr["generalsugg3"]=$res->generalsugg3;
		$parambody_arr["generalsugg4"]=$res->generalsugg4;
		$params=array();
		$params["index"]="flamingo1";
		$params["type"]="flamingoproducts1";
		$params["id"]=$res->inventory_id;		
		$params["body"]=$parambody_arr;
		$flag=$es->index($params);
		return $flag;
	}
	public function update_search_index($inventory_id){
		include("assets/elastic/app/init_customer.php");
			
		$params = array();
		$params['index'] = 'flamingo1';
		$params['type'] = 'flamingoproducts1';
		$params['id'] = $inventory_id;

		try{
			$result = $es->get($params);
		}catch(\Exception $e){
			$flag = $this->create_search_index($inventory_id);
			return $flag;
		}
		
		

		$res=$this->Model_catalogue->create_search_index($inventory_id);
		
		/*** combo price updates starts ***/
		$combo_pack_discount=0;
		$adm_settings=get_admin_combo_settings();
		
		if(!empty($adm_settings)){
			$combo_pack_min_items=$adm_settings->adm_combo_pack_min_items;
			$combo_pack_discount=$adm_settings->adm_combo_pack_discount;
			$combo_pack_shipping_charge=$adm_settings->adm_combo_pack_shipping_charge;
			$combo_pack_status=$adm_settings->adm_combo_pack_status;
		}
		
		 if($combo_pack_discount>0){
				$combo_sel_price=round($res->max_selling_price-($res->max_selling_price*$combo_pack_discount/100));
			}else{
				$combo_sel_price= $searchRes["_source"]["max_selling_price"];
			}
			/*** combo price updates ends ***/
			
			
			
			

		$parambody_arr=array();
		$parambody_arr=array("inventory_id"=>$inventory_id,"sku_id"=>$res->sku_id,"sku_name"=>$res->sku_name,"product_id"=>$res->product_id,"product_name"=>$res->product_name,"product_description"=>$res->product_description,"pcat_id"=>$res->pcat_id,"cat_id"=>$res->cat_id,"subcat_id"=>$res->subcat_id,"filter_brand_id"=>$res->brand_id,"image"=>$res->image,"thumbnail"=>$res->thumbnail,"tax_percent"=>$res->tax_percent,"base_price"=>$res->base_price,"selling_price"=>$res->selling_price,"combo_sel_price"=>$combo_sel_price,"max_selling_price"=>$res->max_selling_price,"selling_discount"=>$res->selling_discount,"cat_name"=>$res->cat_name,"subcat_name"=>$res->subcat_name,"brand_name"=>$res->brand_name,"common_image"=>$res->common_image,'product_status'=>$res->product_status,'stock'=>$res->stock);
		
	
		$query_filters=$this->Model_catalogue->get_filter_id_new($res->inventory_id);
		
		
		foreach($query_filters as $res_filters){
			$filterbox_name=$this->Model_catalogue->get_filterbox_name($res_filters->filter_id);
			$filterbox_name = str_replace(' ', '_', $filterbox_name);
			
			$filterbox_type=$this->Model_catalogue->get_filterbox_type($res_filters->filter_id);
			$filter_options=$this->Model_catalogue->get_filter_options($res_filters->filter_id);
			$parambody_arr["filter_".$filterbox_name."_type"]=$filterbox_type;
			$parambody_arr["filter_".$filterbox_name."_filter_options"]=$filter_options;
			$parambody_arr["filter_".$filterbox_name."_id"]=$res_filters->filter_id;
		}
		
		$parambody_arr["sugg1"]=$res->sugg1;
		$parambody_arr["sugg2"]=$res->sugg2;
		$parambody_arr["generalsugg1"]=$res->generalsugg1;
		$parambody_arr["generalsugg2"]=$res->generalsugg2;
		$parambody_arr["generalsugg3"]=$res->generalsugg3;
		$parambody_arr["generalsugg4"]=$res->generalsugg4;
		$result['_source']=$parambody_arr;
		
		$params["body"]["doc"]=$result['_source'];
	
		$flag = $es->update($params);
		
		
		
		
		//echo "|||".$flag."||";
	// print_r($flag);
	// 	print_r($params);
	// 	exit;
		return $flag;
		
	}
	public function delete_search_index($inventory_id){
		include("assets/elastic/app/init_customer.php");		
		$params = array();
		$params["index"]="flamingo1";
		$params["type"]="flamingoproducts1";
		$params["id"]=$inventory_id;
		try{
			$result = $es->get($params);
			if(!empty($result)){
				$flag=$es->delete($params);
			}
		}catch(\Exception $e){
			return true;
		}
		
		$e=1;
		if($flag!=true){
			$e++;
		}	
		if($e==0){
			return true;
		}else{
			return false;
		}
	}
	public function delete_search_index_selected($selected_list){
		include("assets/elastic/app/init_customer.php");
		$inventory_arr=explode(',',$selected_list);
		$e=0;
		foreach($inventory_arr as $inventory_id){
			$res_inv=get_all_inventory_data($inventory_id);

			$addon=0;
			//print_r($res_inv);

			if(!empty($res_inv)){
				if(isset($res_inv->inventory_type)=='2'){
					$addon=1;
				}
			}

			if($addon==0){

				/* except addon products */
				$params = array();
				$params["index"]="flamingo1";
				$params["type"]="flamingoproducts1";
				$params["id"]=$inventory_id;
				$flag=$es->delete($params);
				$e=0;
				if($flag!=true){
					$e++;
				}
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}
		
	}
	public function get_inventory_exists_in_elasticsearch($inventory_id){
		$is_available="no";
		include("assets/elastic/app/init_customer.php");
		
		$params["index"]="flamingo1";
		$params["type"]="flamingoproducts1";
		$params["from"]=0;
		$params["size"]=10000;
		$res=$es->search($params);
		foreach($res["hits"]["hits"] as $varr){
			if($varr["_id"]==$inventory_id){
				$is_available="yes";
			}
		}
		return $is_available;
	}
	
	
	public function update_pcatname_search_index($pcat_id,$pcat_name){
		include("assets/elastic/app/init_customer.php");

		$query_inv=$this->Model_catalogue->get_all_inventoy_by_pcat_id_selected_list($pcat_id);
		$e=0;
		foreach($query_inv as $res){
			$params = array();
			$params['index'] = 'flamingo1';
			$params['type'] = 'flamingoproducts1';
			$params['id'] = $res->inventory_id;
			$is_available=$this->get_inventory_exists_in_elasticsearch($res->inventory_id);
			if($is_available=="yes"){
				$result = $es->get($params);
				$result['_source']['pcat_name'] = $pcat_name;
				$params["body"]["doc"]=$result['_source'];
				$flag = $es->update($params);
				if($flag!=true){
					$e++;
				}
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}
	}
	public function update_catname_search_index($cat_id,$cat_name){
		include("assets/elastic/app/init_customer.php");

		$query_inv=$this->Model_catalogue->get_all_inventoy_by_cat_id_selected($cat_id);
		$e=0;
		foreach($query_inv as $res){
			$params = array();
			$params['index'] = 'flamingo1';
			$params['type'] = 'flamingoproducts1';
			$params['id'] = $res->inventory_id;
			$is_available=$this->get_inventory_exists_in_elasticsearch($res->inventory_id);
			if($is_available=="yes"){
				$result = $es->get($params);
				$result['_source']['cat_name'] = $cat_name;
				$result['_source']['pcat_id'] = $res->pcat_id;
				$result['_source']['cat_id'] = $cat_id;
				$params["body"]["doc"]=$result['_source'];
				$flag = $es->update($params);
				if($flag!=true){
					$e++;
				}
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}
	}
	public function update_subcatname_search_index($subcat_id,$subcat_name){
		include("assets/elastic/app/init_customer.php");
		
		$query_inv=$this->Model_catalogue->get_all_inventoy_by_subcat_id_selected($subcat_id);
		$e=0;
		foreach($query_inv as $res){
			$params = array();
			$params['index'] = 'flamingo1';
			$params['type'] = 'flamingoproducts1';
			$params['id'] = $res->inventory_id;
			$is_available=$this->get_inventory_exists_in_elasticsearch($res->inventory_id);
			if($is_available=="yes"){
				$result = $es->get($params);
				$result['_source']['subcat_name'] = $subcat_name;
				$result['_source']['cat_name'] = $res->cat_name;
				//$result['_source']['pcat_name'] = $res->pcat_name;
				$result['_source']['pcat_id'] = $res->pcat_id;
				$result['_source']['cat_id'] = $res->cat_id;
				$result['_source']['subcat_id'] = $subcat_id;
				$params["body"]["doc"]=$result['_source'];
				$flag = $es->update($params);
				if($flag!=true){
					$e++;
				}
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}
	}
	public function update_brandname_search_index($brand_id,$brand_name,$subcat_id,$cat_id,$pcat_id){
		include("assets/elastic/app/init_customer.php");
		
		$query_inv=$this->Model_catalogue->get_all_inventoy_by_brand_id_selected($brand_id);
		$e=0;
		foreach($query_inv as $res){
			$params = array();
			$params['index'] = 'flamingo1';
			$params['type'] = 'flamingoproducts1';
			$params['id'] = $res->inventory_id;
			$is_available=$this->get_inventory_exists_in_elasticsearch($res->inventory_id);
			if($is_available=="yes"){
				$result = $es->get($params);
				$result['_source']['brand_name'] = $res->brand_name;
				$result['_source']['brand_id'] = $res->brand_id;
				$result['_source']['subcat_name'] = $res->subcat_name;
				$result['_source']['cat_name'] = $res->cat_name;
				//$result['_source']['pcat_name'] = $res->pcat_name;
				$result['_source']['pcat_id'] = $pcat_id;
				$result['_source']['cat_id'] = $cat_id;
				$result['_source']['subcat_id'] = $subcat_id;
				$params["body"]["doc"]=$result['_source'];
				$flag = $es->update($params);
				if($flag!=true){
					$e++;
				}
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}
	}
	public function update_product_search_index($product_id,$product_name,$sugg1,$sugg2,$generalsugg1,$generalsugg2,$generalsugg3,$generalsugg4,$brand_id,$subcat_id,$cat_id,$pcat_id){
		require_once("assets/elastic/app/init_customer.php");

		$query_inv=$this->Model_catalogue->get_all_inventoy_by_product_id_selected($product_id);
		$e=0;
		foreach($query_inv as $res){
			$params = array();
			$params['index'] = 'flamingo1';
			$params['type'] = 'flamingoproducts1';
			$params['id'] = $res->inventory_id;
			$result = $es->get($params);
			$result['_source']['product_name'] =$product_name;
			$result['_source']['cat_name'] = $res->cat_name;
			$result['_source']['subcat_name'] = $res->subcat_name;
			$result['_source']['brand_name'] = $res->brand_name;
			//$result['_source']['pcat_name'] = $res->pcat_name;
			$result['_source']['brand_id'] = $brand_id;
			$result['_source']['subcat_id'] = $subcat_id;
			$result['_source']['cat_id'] = $cat_id;
			$result['_source']['pcat_id'] = $pcat_id;
			
			$result['_source']['sugg1'] = $sugg1;
			$result['_source']['sugg2'] = $sugg2;
			$result['_source']['generalsugg1'] = $generalsugg1;
			$result['_source']['generalsugg2'] = $generalsugg2;
			$result['_source']['generalsugg3'] = $generalsugg3;
			$result['_source']['generalsugg4'] = $generalsugg4;
			$params["body"]["doc"]=$result['_source'];
			$flag = $es->update($params);
			if($flag!=true){
				$e++;
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}
	}
	public function update_filterbox_name_search_index($filterbox_id,$filterbox_name){
		require_once("assets/elastic/app/init_customer.php");
		$query_inv=$this->Model_catalogue->get_all_inventoy_by_filterbox_id($filterbox_id);
		$e=0;
		foreach($query_inv as $res){
			$inventory_id=$res->inventory_id;
			$flag_delete=$this->delete_search_index($inventory_id);	
			if($flag_delete==true){
				$flag_create=$this->create_search_index($inventory_id);
				if($flag_create!=true){
					$e++;
				}
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}
	}
	
	public function update_search_index_by_filterbox_id_selected($selected_list){
		require_once("assets/elastic/app/init_customer.php");
		$queryRecordsObj=$this->Model_catalogue->get_inventory_by_filterbox_id_selected($selected_list);
		$e=0;
		foreach($queryRecordsObj as $res){
			$inventory_id=$res->inventory_id;

			$flag=$this->delete_search_index($inventory_id);
			if($flag==true){

			$params = array();
			$params['index'] = 'flamingo1';
			$params['type'] = 'flamingoproducts1';
			$params['id'] = $inventory_id;
			$res=$this->Model_catalogue->create_search_index($inventory_id);
			$parambody_arr=array();
			$parambody_arr=array("inventory_id"=>$inventory_id,"sku_id"=>$res->sku_id,"sku_name"=>$res->sku_name,"product_id"=>$res->product_id,"product_name"=>$res->product_name,"product_description"=>$res->product_description,"pcat_id"=>$res->pcat_id,"cat_id"=>$res->cat_id,"subcat_id"=>$res->subcat_id,"filter_brand_id"=>$res->brand_id,"image"=>$res->image,"thumbnail"=>$res->thumbnail,"selling_price"=>$res->selling_price,"pcat_name"=>$res->pcat_name,"cat_name"=>$res->cat_name,"subcat_name"=>$res->subcat_name,"brand_name"=>$res->brand_name,"common_image"=>$res->common_image);
			
			$query_filters=$this->Model_catalogue->get_filter_id_new_except_filter_id($res->inventory_id,$filter_id);
		
			foreach($query_filters as $res_filters){
				$filterbox_name=$this->Model_catalogue->get_filterbox_name($res_filters->filter_id);
				$filterbox_name = str_replace(' ', '_', $filterbox_name);
				
				$filterbox_type=$this->Model_catalogue->get_filterbox_type($res_filters->filter_id);
				$filter_options=$this->Model_catalogue->get_filter_options($res_filters->filter_id);
				$parambody_arr["filter_".$filterbox_name."_type"]=$filterbox_type;
				$parambody_arr["filter_".$filterbox_name."_filter_options"]=$filter_options;
				$parambody_arr["filter_".$filterbox_name."_id"]=$res_filters->filter_id;
			}
			
			$parambody_arr["sugg1"]=$res->sugg1;
			$parambody_arr["sugg2"]=$res->sugg2;	
			$parambody_arr["generalsugg1"]=$res->generalsugg1;
			$parambody_arr["generalsugg2"]=$res->generalsugg2;
			$parambody_arr["generalsugg3"]=$res->generalsugg3;
			$parambody_arr["generalsugg4"]=$res->generalsugg4;
			
			$params = array();
			$params['index'] = 'flamingo1';
			$params['type'] = 'flamingoproducts1';
			$params['id'] = $inventory_id;
			$params["body"]=$parambody_arr;
			
			$flag=$es->index($params);
				if($flag!=true){
					$e++;
				}
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}
	}
	public function update_search_index_by_filter_id_selected($selected_list){
		require_once("assets/elastic/app/init_customer.php");
		$queryRecordsObj=$this->Model_catalogue->get_inventory_by_filter_id_selected($selected_list);
		$e=0;
		foreach($queryRecordsObj as $res){
			$inventory_id=$res->inventory_id;

			$flag=$this->delete_search_index($inventory_id);
			if($flag==true){

			$params = array();
			$params['index'] = 'flamingo1';
			$params['type'] = 'flamingoproducts1';
			$params['id'] = $inventory_id;
		
			$res=$this->Model_catalogue->create_search_index($inventory_id);
			$parambody_arr=array();
			$parambody_arr=array("inventory_id"=>$inventory_id,"sku_id"=>$res->sku_id,"sku_name"=>$res->sku_name,"product_id"=>$res->product_id,"product_name"=>$res->product_name,"product_description"=>$res->product_description,"pcat_id"=>$res->pcat_id,"cat_id"=>$res->cat_id,"subcat_id"=>$res->subcat_id,"filter_brand_id"=>$res->brand_id,"image"=>$res->image,"thumbnail"=>$res->thumbnail,"selling_price"=>$res->selling_price,"pcat_name"=>$res->pcat_name,"cat_name"=>$res->cat_name,"subcat_name"=>$res->subcat_name,"brand_name"=>$res->brand_name,"common_image"=>$res->common_image);
			
			$query_filters=$this->Model_catalogue->get_filter_id_new_except_filter_id($res->inventory_id,$filter_id);
		
			foreach($query_filters as $res_filters){
				$filterbox_name=$this->Model_catalogue->get_filterbox_name($res_filters->filter_id);
				$filterbox_name = str_replace(' ', '_', $filterbox_name);
				
				$filterbox_type=$this->Model_catalogue->get_filterbox_type($res_filters->filter_id);
				$filter_options=$this->Model_catalogue->get_filter_options($res_filters->filter_id);
				$parambody_arr["filter_".$filterbox_name."_type"]=$filterbox_type;
				$parambody_arr["filter_".$filterbox_name."_filter_options"]=$filter_options;
				$parambody_arr["filter_".$filterbox_name."_id"]=$res_filters->filter_id;
			}
			
			$parambody_arr["sugg1"]=$res->sugg1;
			$parambody_arr["sugg2"]=$res->sugg2;
			
			$parambody_arr["generalsugg1"]=$res->generalsugg1;
			$parambody_arr["generalsugg2"]=$res->generalsugg2;
			$parambody_arr["generalsugg3"]=$res->generalsugg3;
			$parambody_arr["generalsugg4"]=$res->generalsugg4;
			
			$params = array();
			$params['index'] = 'flamingo1';
			$params['type'] = 'flamingoproducts1';
			$params['id'] = $inventory_id;
			$params["body"]=$parambody_arr;
			
			$flag=$es->index($params);
				if($flag!=true){
					$e++;
				}
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}
	}
	public function delete_search_index_by_pcat_selected_list($selected_list){
		require_once("assets/elastic/app/init_customer.php");
		$pcat_arr=explode(',',$selected_list);
	
		$query_inv=$this->Model_catalogue->get_all_inventoy_by_pcat_id_selected_list($selected_list);
		$e=0;			
		foreach($query_inv as $res){
			$params = array();
			$params['index'] = 'flamingo1';
			$params['type'] = 'flamingoproducts1';
			$params['id'] = $res->inventory_id;
			$result = $es->get($params);
			$result['_source']['pcat_id']=0;
			$result['_source']['pcat_name']='';
			$params["body"]["doc"]=$result['_source'];
			$flag = $es->update($params);
			if($flag!=true){
				$e++;
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}
	}
	public function delete_search_index_by_pcat_selected_entire_list($selected_list){
		require_once("assets/elastic/app/init_customer.php");
		$pcat_arr=explode(',',$selected_list);
	
		$query_inv=$this->Model_catalogue->get_all_inventoy_by_pcat_id_selected_list($selected_list);
		$e=0;			
		foreach($query_inv as $res){
			$params = array();
			$params['index'] = 'flamingo1';
			$params['type'] = 'flamingoproducts1';
			$params['id'] = $res->inventory_id;
			$flag = $es->delete($params);
			if($flag!=true){
				$e++;
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}
	}
	public function restore_search_index_by_pcat_selected($selected_list){
		require_once("assets/elastic/app/init_customer.php");
		$query_inv=$this->Model_catalogue->get_all_inventoy_by_pcat_id_selected_list($selected_list);
		$e=0;
		foreach($query_inv as $res){
			$inventory_id=$res->inventory_id;
			$flag = $this->create_search_index($inventory_id);
			if($flag!=true){
				$e++;
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}
		
	}
	public function delete_search_index_by_cat_id_selected($selected_list){
		require_once("assets/elastic/app/init_customer.php");
		
		$cat_arr=explode(',',$selected_list);
		$query_inv=$this->Model_catalogue->get_all_inventoy_by_cat_id_selected($selected_list);
		$e=0;
	
		foreach($query_inv as $res){
			$params = array();
			$params['index'] = 'flamingo1';
			$params['type'] = 'flamingoproducts1';
			$params['id'] = $res->inventory_id;
			$flag = $es->delete($params);
			if($flag!=true){
				$e++;
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}
		
	}
	public function restore_search_index_by_cat_id_selected($selected_list){
		require_once("assets/elastic/app/init_customer.php");
		$query_inv=$this->Model_catalogue->get_all_inventoy_by_cat_id_selected($selected_list);
		$e=0;
		foreach($query_inv as $res){
			$inventory_id=$res->inventory_id;
			$flag = $this->create_search_index($inventory_id);
			if($flag!=true){
				$e++;
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}	
	}
	public function delete_search_index_by_subcat_id_selected($selected_list){
		require_once("assets/elastic/app/init_customer.php");
		$subcat_arr=explode(',',$selected_list);
		$e=0;
		$query_inv=$this->Model_catalogue->get_all_inventoy_by_subcat_id_selected($selected_list);
		foreach($query_inv as $res){
			$params = array();
			$params['index'] = 'flamingo1';
			$params['type'] = 'flamingoproducts1';
			$params['id'] = $res->inventory_id;
			$flag = $es->delete($params);
			if($flag!=true){
				$e++;
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}
	}
	public function restore_search_index_by_subcat_id_selected($selected_list){
		require_once("assets/elastic/app/init_customer.php");
		$query_inv=$this->Model_catalogue->get_all_inventoy_by_subcat_id_selected($selected_list);
		$e=0;
		foreach($query_inv as $res){
			$inventory_id=$res->inventory_id;
			$flag = $this->create_search_index($inventory_id);
			if($flag!=true){
				$e++;
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}	
	}
	public function delete_search_index_by_brand_id_selected($selected_list){
		require_once("assets/elastic/app/init_customer.php");
		$brand_arr=explode(',',$selected_list);
		$e=0;
		$query_inv=$this->Model_catalogue->get_all_inventoy_by_brand_id_selected($selected_list);
		foreach($query_inv as $res){
			$params = array();
			$params['index'] = 'flamingo1';
			$params['type'] = 'flamingoproducts1';
			$params['id'] = $res->inventory_id;
			$flag = $es->delete($params);
			if($flag!=true){
				$e++;
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}
	}
	public function restore_search_index_by_brand_id_selected($selected_list){
		require_once("assets/elastic/app/init_customer.php");
		$query_inv=$this->Model_catalogue->get_all_inventoy_by_brand_id_selected($selected_list);
		$e=0;
		foreach($query_inv as $res){
			$inventory_id=$res->inventory_id;
			$flag = $this->create_search_index($inventory_id);
			if($flag!=true){
				$e++;
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}	
	}
	public function delete_search_index_by_product_id_selected($selected_list){
		require_once("assets/elastic/app/init_customer.php");

		$query_inv=$this->Model_catalogue->get_all_inventoy_by_product_id_selected($selected_list);
		$e=0;
		foreach($query_inv as $res){
			$params = array();
			$params['index'] = 'flamingo1';
			$params['type'] = 'flamingoproducts1';
			$params['id'] = $res->inventory_id;
			$flag = $es->delete($params);
			if($flag!=true){
				$e++;
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}
	}
	public function restore_search_index_by_product_id_selected($selected_list){
		require_once("assets/elastic/app/init_customer.php");
		$query_inv=$this->Model_catalogue->get_all_inventoy_by_product_id_selected($selected_list);
		$e=0;
		foreach($query_inv as $res){
			$inventory_id=$res->inventory_id;
			$flag = $this->create_search_index($inventory_id);
			if($flag!=true){
				$e++;
			}
		}
		if($e==0){
			return true;
		}else{
			return false;
		}	
	}
	public function category_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->category_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->category_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->cat_id.'">';
				$row[]=$queryRecordsObj->cat_name;
				if(isset($queryRecordsObj->pcat_name)){
					$row[]=$queryRecordsObj->pcat_name;
				}else{
					$row[]='No Parent Category';
				}	
				$str='';
				$str.= "Menu Level :". $queryRecordsObj -> menu_level.'<br>';
				$str.="Menu Sort Order :".  $queryRecordsObj -> menu_sort_order.'<br>';
				$str.="View :".  $queryRecordsObj -> view; 
				$row[]=$str;
				$row[]=$queryRecordsObj -> timestamp;
				if($queryRecordsObj->active){
				$row[]='<form action="'.base_url().'admin/Catalogue/edit_category_form" method="post">	<input type="hidden" value="'.$queryRecordsObj -> pcat_id.'" name="pcat_id"><input type="hidden" value="'.$queryRecordsObj -> cat_id.'" name="cat_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';
				}
				
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function subcategory_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->subcategory_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->subcategory_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->subcat_id.'">';
				$row[]=$queryRecordsObj->subcat_name;
				$row[]=$queryRecordsObj->cat_name;
				if(isset($queryRecordsObj->pcat_name)){
					$row[]=$queryRecordsObj->pcat_name;
				}else{
					$row[]='No Parent Category';
				}
				
				$str='';
				$str.="Menu Sort Order :".$queryRecordsObj -> menu_sort_order.'<br>';
				$str.="View :".$queryRecordsObj -> view; 
				$row[]=$str;
				$row[]=$queryRecordsObj -> timestamp;
				if($queryRecordsObj->active){
				$row[]='<form action="'.base_url().'admin/Catalogue/edit_subcategory_form" method="post">	<input type="hidden" value="'.$queryRecordsObj -> pcat_id.'" name="pcat_id"><input type="hidden" value="'.$queryRecordsObj -> cat_id.'" name="cat_id"><input type="hidden" value="'.$queryRecordsObj -> subcat_id.'" name="subcat_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';
				}
				
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function brands_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->brands_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->brands_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->brand_id.'">';
				$row[]='Brand Name: '.$queryRecordsObj->brand_name;
				$row[]=$queryRecordsObj->subcat_name;
				$row[]=$queryRecordsObj->cat_name;
				if(isset($queryRecordsObj->pcat_name)){
					$row[]=$queryRecordsObj->pcat_name;
				}else{
					$row[]='No Parent Category';
				}
				$str='';
				$str.="View :".  $queryRecordsObj -> view; 
				$row[]=$str;

				
				$row[]=$queryRecordsObj -> timestamp;
				$user_type=$this->session->userdata("user_type");
				$link='';
				
				//$row[]=$link;

				if($queryRecordsObj->active){
				$row[]='<form action="'.base_url().'admin/Catalogue/edit_brand_form" method="post">	<input type="hidden" value="'.$queryRecordsObj -> pcat_id.'" name="pcat_id"><input type="hidden" value="'.$queryRecordsObj -> cat_id.'" name="cat_id"><input type="hidden" value="'.$queryRecordsObj -> subcat_id.'" name="subcat_id"><input type="hidden" value="'.$queryRecordsObj -> brand_id.'" name="brand_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';
				}
				
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function attribute_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->attribute_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->attribute_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj-> 	attribute_id.'">';
				$str='';
				
				if(!empty($queryRecordsObj -> attribute1_name)){
				if(preg_match("/:/",$queryRecordsObj -> attribute1_option)){
						$attribute1_option=substr($queryRecordsObj -> attribute1_option,strrpos($queryRecordsObj -> attribute1_option,":"));
						$attribute1_option=substr($queryRecordsObj -> attribute1_option,0,strrpos($queryRecordsObj -> attribute1_option,":")).str_replace(":","(",$attribute1_option).")";
					}
					else{
						$attribute1_option=$queryRecordsObj -> attribute1_option;
					}
				$str.="1. ". $queryRecordsObj -> attribute1_name.' : '.$attribute1_option.'<br>';	
				}
				if(!empty($queryRecordsObj -> attribute2_name)){
				if(preg_match("/:/",$queryRecordsObj -> attribute2_option)){
						$attribute2_option=substr($queryRecordsObj -> attribute2_option,strrpos($queryRecordsObj -> attribute2_option,":"));
						$attribute2_option=substr($queryRecordsObj -> attribute2_option,0,strrpos($queryRecordsObj -> attribute2_option,":")).str_replace(":","(",$attribute2_option).")";
					}
					else{
						$attribute2_option=$queryRecordsObj -> attribute2_option;
					}
				$str.="2. ". $queryRecordsObj -> attribute2_name.' : '.$attribute2_option.'<br>';
				}
				if(!empty($queryRecordsObj -> attribute3_name)){
				if(preg_match("/:/",$queryRecordsObj -> attribute3_option)){
						$attribute3_option=substr($queryRecordsObj -> attribute3_option,strrpos($queryRecordsObj -> attribute3_option,":"));
						$attribute3_option=substr($queryRecordsObj -> attribute3_option,0,strrpos($queryRecordsObj -> attribute3_option,":")).str_replace(":","(",$attribute3_option).")";
					}
					else{
						$attribute3_option=$queryRecordsObj -> attribute3_option;
					}
				$str.="3. ". $queryRecordsObj -> attribute3_name.' : '.$attribute3_option.'<br>';
				}
				if(!empty($queryRecordsObj -> attribute4_name)){
				if(preg_match("/:/",$queryRecordsObj -> attribute4_option)){
						$attribute4_option=substr($queryRecordsObj -> attribute4_option,strrpos($queryRecordsObj -> attribute4_option,":"));
						$attribute4_option=substr($queryRecordsObj -> attribute4_option,0,strrpos($queryRecordsObj -> attribute4_option,":")).str_replace(":","(",$attribute4_option).")";
					}
					else{
						$attribute4_option=$queryRecordsObj -> attribute4_option;
					}
				$str.="4. ". $queryRecordsObj -> attribute4_name.' : '.$attribute4_option.'<br>';
				}
				
				$var='';
				if($queryRecordsObj->subcat_id!=0){
					$var.=$queryRecordsObj->subcat_name.' , ';
				}
				if($queryRecordsObj->cat_id!=0){
					$var.=$queryRecordsObj->cat_name;
				}
				$row[]=$var;
				$row[]=$str;
				$row[]="View :".  $queryRecordsObj -> view; 
				$row[]=$queryRecordsObj -> timestamp;
				if($queryRecordsObj->active){
				$row[]='<form action="'.base_url().'admin/Catalogue/edit_attribute_form" method="post"><input type="hidden" value="'.$queryRecordsObj -> catalog.'" name="catalog"><input type="hidden" value="'.$queryRecordsObj -> catalog_id.'" name="catalog_id">	<input type="hidden" value="'.$queryRecordsObj -> pcat_id.'" name="pcat_id"><input type="hidden" value="'.$queryRecordsObj -> cat_id.'" name="cat_id"><input type="hidden" value="'.$queryRecordsObj -> subcat_id.'" name="subcat_id"><input type="hidden" value="'.$queryRecordsObj -> attribute_id.'" name="attribute_id"><input type="submit" class="btn btn-info btn-xs btn-block" value="Edit an Attribute" ></form><form target="_blank"  action="'.base_url().'admin/Catalogue/attrib_expansions" method="post"><input type="hidden" value="'.$queryRecordsObj -> attribute_id.'" name="attribute_id"><input type="submit" value="Attribute Expansions" name="attrib_expansion" class="btn btn-success btn-xs btn-block"></form>';
				}
				
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function filterbox_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->filterbox_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->filterbox_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->filterbox_id.'">';
				$row[]=$queryRecordsObj->filterbox_name;
				$row[]=$queryRecordsObj->type;
				$row[]=$queryRecordsObj->sort_order;
				$catalog=$this->Model_catalogue->get_catalog_structure($queryRecordsObj->pcat_id,$queryRecordsObj->cat_id,$queryRecordsObj->subcat_id);
				$row[]=$catalog;				
				$row[]=$queryRecordsObj->view_filterbox;
				$row[]=$queryRecordsObj -> timestamp;
				if($queryRecordsObj->active){
				$row[]='<form action="'.base_url().'admin/Catalogue/edit_filterbox_form" method="post">	<input type="hidden" value="'.$queryRecordsObj -> pcat_id.'" name="pcat_id"><input type="hidden" value="'.$queryRecordsObj -> cat_id.'" name="cat_id"><input type="hidden" value="'.$queryRecordsObj -> subcat_id.'" name="subcat_id"><input type="hidden" value="'.$queryRecordsObj -> filterbox_id.'" name="filterbox_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';
				}
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function filter_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->filter_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->filter_processing($params,"get_recs");
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->filter_id.'">';
				$row[]=$queryRecordsObj->filter_options;
				$row[]=$queryRecordsObj->filterbox_name;				
				$catalog=$this->Model_catalogue->get_catalog_structure($queryRecordsObj->pcat_id,$queryRecordsObj->cat_id,$queryRecordsObj->subcat_id);
				$row[]=$catalog;
				$row[]=$queryRecordsObj->filter_sort_order;
				$row[]=$queryRecordsObj->view_filter;
				$row[]=$queryRecordsObj -> timestamp;
				
				if($queryRecordsObj->active){
				$row[]='<form action="'.base_url().'admin/Catalogue/edit_filter_form" method="post">	<input type="hidden" value="'.$queryRecordsObj -> pcat_id.'" name="pcat_id"><input type="hidden" value="'.$queryRecordsObj -> cat_id.'" name="cat_id"><input type="hidden" value="'.$queryRecordsObj -> subcat_id.'" name="subcat_id"><input type="hidden" value="'.$queryRecordsObj -> filter_id.'" name="filter_id"><input type="hidden" value="'.$queryRecordsObj -> filterbox_id.'" name="filterbox_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';
				}

				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function specification_group_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->specification_group_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->specification_group_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj-> 	specification_group_id.'">';
				$row[]=$queryRecordsObj->specification_group_name;
				$row[]=$queryRecordsObj->sort_order;
				$catalog=$this->Model_catalogue->get_catalog_structure($queryRecordsObj->pcat_id,$queryRecordsObj->cat_id,$queryRecordsObj->subcat_id);
				$row[]=$catalog;				
				$row[]=$queryRecordsObj->view_specification_group;
				$row[]=$queryRecordsObj -> timestamp;
				if($queryRecordsObj->active){
				$row[]='<form action="'.base_url().'admin/Catalogue/edit_specification_group_form" method="post">	<input type="hidden" value="'.$queryRecordsObj -> pcat_id.'" name="pcat_id"><input type="hidden" value="'.$queryRecordsObj -> cat_id.'" name="cat_id"><input type="hidden" value="'.$queryRecordsObj -> subcat_id.'" name="subcat_id"><input type="hidden" value="'.$queryRecordsObj -> specification_group_id.'" name="specification_group_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';
				}
				
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function specification_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->specification_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->specification_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj-> 	specification_id.'">';
				$row[]=$queryRecordsObj->specification_name;
				$row[]=$queryRecordsObj->specification_value;				
				$catalog=$this->Model_catalogue->get_catalog_structure($queryRecordsObj->pcat_id,$queryRecordsObj->cat_id,$queryRecordsObj->subcat_id);
				$row[]=$queryRecordsObj->specification_group_name;
				$row[]=$catalog;
				$row[]=$queryRecordsObj->specification_sort_order;
				$row[]=$queryRecordsObj->view_specification;
				$row[]=$queryRecordsObj -> timestamp;
				
				if($queryRecordsObj->active){
				$row[]='<form action="'.base_url().'admin/Catalogue/edit_specification_form" method="post">	<input type="hidden" value="'.$queryRecordsObj -> pcat_id.'" name="pcat_id"><input type="hidden" value="'.$queryRecordsObj -> cat_id.'" name="cat_id"><input type="hidden" value="'.$queryRecordsObj -> subcat_id.'" name="subcat_id"><input type="hidden" value="'.$queryRecordsObj -> specification_group_id.'" name="specification_group_id"><input type="hidden" value="'.$queryRecordsObj -> specification_id.'" name="specification_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';
				}

				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function products_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->products_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->products_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->product_id.'">';
				$var='';
				
				$row[]=$queryRecordsObj ->product_name;
				if($queryRecordsObj->brand_id!=0){
					$var.=$queryRecordsObj->brand_name.' , ';
				}
				if($queryRecordsObj->subcat_id!=0){
					$var.=$queryRecordsObj->subcat_name.' , ';
				}
				if($queryRecordsObj->cat_id!=0){
					$var.=$queryRecordsObj->cat_name.' , ';
				}
				if($queryRecordsObj->pcat_id!=0){
					$var.=$queryRecordsObj->pcat_name;
				}	
				$row[]=$var;				
				$row[]=$queryRecordsObj -> timestamp;

				if($queryRecordsObj->active){
				$row[]='<form action="'.base_url().'admin/Catalogue/edit_product_form" method="post">	<input type="hidden" value="'.$queryRecordsObj -> pcat_id.'" name="pcat_id"><input type="hidden" value="'.$queryRecordsObj -> cat_id.'" name="cat_id"><input type="hidden" value="'.$queryRecordsObj -> subcat_id.'" name="subcat_id"><input type="hidden" value="'.$queryRecordsObj -> brand_id.'" name="brand_id"><input type="hidden" value="'.$queryRecordsObj -> product_id.'" name="product_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';
				}
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function inventory_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->inventory_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->inventory_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->id.'">';
				$var='';
				$row[]='<img src="'.base_url().$queryRecordsObj -> image.'" width="100" alt="Image Not Found"><br>';
				$val='';
				$val.='Sku Id : '.$queryRecordsObj -> sku_id;
				$val.='<br>Sku Name : '.$queryRecordsObj -> sku_name;
				
				$val.='<br><small>Catalogue Structure : '.$queryRecordsObj -> pcat_name.' > '.$queryRecordsObj -> cat_name.' > '.$queryRecordsObj -> subcat_name.' > '.$queryRecordsObj -> brand_name.'</small>';
				$inv_type='';
				$inventory_type=$queryRecordsObj ->inventory_type;
				if($inventory_type=='1'){
					$inv_type="Only Main";
				}
				if($inventory_type=='2'){
					$inv_type="Only Addon";
				}
				if($inventory_type=='3'){
					$inv_type="Both Main and Addon";
				}

				$val.='<br><small class="text-info"><b>Inventory Type : '.$inv_type.'</b></small>';

				$val.='<h4>Attributes</h4>';
				$val.=$queryRecordsObj -> attribute_1.":".$queryRecordsObj -> attribute_1_value.'<br>';

				if(!empty($queryRecordsObj -> attribute_2)){
					$val.=$queryRecordsObj -> attribute_2.":".$queryRecordsObj -> attribute_2_value.'<br>';
				}
				if(!empty($queryRecordsObj -> attribute_3)){
					$val.=$queryRecordsObj -> attribute_3.":".$queryRecordsObj -> attribute_3_value.'<br>';
				}
				if(!empty($queryRecordsObj -> attribute_4)){ 
					$val.=$queryRecordsObj -> attribute_4.":".$queryRecordsObj -> attribute_4_value.'<br>';
				}
				
				$row[]=$val;				
				$val2='';				
				$val2.='Tax:'.$queryRecordsObj -> tax_percent.'<br>';
				$val2.='Web Selling Price: '.curr_sym.$queryRecordsObj -> selling_price.'<br>';
				$val2.='Max Selling Price: '.curr_sym.$queryRecordsObj -> max_selling_price.'<br>';
				$val2.='Selling Discount %: '.$queryRecordsObj -> selling_discount.'%<br>';
                                
				$val2.='Purchased Price: '.curr_sym.$queryRecordsObj -> purchased_price.'<br>';

				if($inv_type!='2' && $queryRecordsObj -> purchased_price!='' ){
					$val2.='Status : Profit ('.curr_sym.($queryRecordsObj -> selling_price-$queryRecordsObj -> purchased_price).')<br>';
				
					if($queryRecordsObj -> purchased_price>$queryRecordsObj -> selling_price){
						$val2.='Status : Loss ('.curr_sym.($queryRecordsObj -> purchased_price-$queryRecordsObj -> selling_price).')<br>';
					}
					else if($queryRecordsObj -> purchased_price<$queryRecordsObj -> selling_price){
						$val2.='Status : Profit ('.curr_sym.($queryRecordsObj -> selling_price-$queryRecordsObj -> purchased_price).')<br>';
					}
					else{
						$val2.='Status : Nill<br>';
					}
				}
				$val2.='MOQ: '.$queryRecordsObj -> moq.'<br>';
				$val2.='Max OQ: '.$queryRecordsObj -> max_oq.'<br>';
				$val2.='Stock:'.$queryRecordsObj -> stock.'<br>';
				$val2.='Sold:'.$queryRecordsObj -> num_sales.'<br>';
				$val2.='Status:'.$queryRecordsObj -> product_status.'<br>';
				
				$txt='';$str='';
				if($queryRecordsObj->publish_status=='0'){
					$txt.='<span style="color:blue"> Yet to process </span>';
				}else if($queryRecordsObj->publish_status=='1'){
					$txt.='<span style="color:green"> <b>Publish </b></span>'.'<br> <small> On '.date('d/m/Y H:i:s',strtotime($queryRecordsObj->status_on)).'</small>';
				}else if($queryRecordsObj->publish_status=='2'){
					$txt.='<span style="color:red"><b>Unpublish </b></span>'.'<br> <small> On '.date('d/m/Y H:i:s',strtotime($queryRecordsObj->status_on)).'</small>';;
				}else if($queryRecordsObj->publish_status=='3'){
					
					$txt.='<span style="color:red"><b>Rejected </b></span>'.'<br> <small> On '.date('d/m/Y H:i:s',strtotime($queryRecordsObj->status_on)).'</small>';

					$txt.='<br><u>Rejected comments</u> : <br>';
					$txt.=''.$queryRecordsObj->rejection_comments;

				}
	
				$str.="<br>Status :<br>".  $txt.'<br>';

				//print_r($queryRecordsObj);

				$row[]=$val2.$str;	

				/* promo details */

				/* finding promotions */

		
			/* for Promotions display */
			$inventory_id=$queryRecordsObj->id;
			$product_id=$queryRecordsObj->product_id;
        
		$promo_array=$this->get_inv_promo_details($inventory_id,$product_id);
        
		/* promo str */
		$promo_str=''; $qty_arr=array();$active_qty_promo_exist=0;
		//print_r($promo_array);

		if(!empty($promo_array)){
			foreach($promo_array as $promo_arr){
				if(!empty($promo_arr)){
					
					foreach($promo_arr as $promo){
						$promo_str.=$promo->promo_quote.'<br>';
						$promo_str.="Period :".date('d M, Y H:i:s',strtotime($promo->promo_start_date)).' to '.date('d M, Y H:i:s',strtotime($promo->promo_end_date)).'<br>';
						$st=($promo->promo_active=='1') ? 'Active'  : 'In Active';
						if($promo->promo_active=='1' && $promo->buy_type=='Qty' && (strpos($promo->get_type, '%') !== false)){
							$active_qty_promo_exist++;
							$qty_arr[]=$promo->to_buy;
						}
						$promo_str.='Status:'.$st.'<br>';
						$promo_str.='Expired Status : '.date_status($promo->promo_start_date,$promo->promo_end_date);
						$promo_str.='<hr>';
					}
				}

			}
		}

		$active_qty_promo_exist;
		$qty_arr;
		
		$promo_qty_min = (!empty($qty_arr)) ? min($qty_arr) : '';

		if($promo_str!=''){
			$promo_str='<b><u>Promo Details</u></b>'.'<br>'.$promo_str.'';
		}else{
			$promo_str='No Promotions applied';
		}

		$row[]=$promo_str;

		/* promo str */

				/* promo details */

				$user_type=$this->session->userdata("user_type");
				$link='';
				if($user_type!='vendor'){
					// $link.='<br><select class="form-control" name="change_status_'.$queryRecordsObj->vendor_id.'" id="change_status_'.$queryRecordsObj->vendor_id.'" onchange="change_status('.$queryRecordsObj->vendor_id.','.$queryRecordsObj->id.',this.value)"> ';
	
					// if($queryRecordsObj->publish_status!='1' || $queryRecordsObj->publish_status!='2'){
					// 	$link.='<option value="">Change status</option>';
					// }
					// if($queryRecordsObj->publish_status!='1'){
					// 	$link.='<option value="1">Publish</option>';
					// }
					// if($queryRecordsObj->publish_status!='2'){
					// 	$link.='<option value="2">Unpublish</option>';
					// }
					
					// $link.='</select>';

					if($inventory_type!='2'){
						$link.='<button class="btn btn-warning" onclick="view_status_modal('.$queryRecordsObj->vendor_id.','.$queryRecordsObj->id.','.$queryRecordsObj->publish_status.')">Update status</button>';
					}

				}

				if($queryRecordsObj->active==1){
					if($inventory_type!='2'){
						$link.='<input type="button" class="btn btn-warning btn-xs btn-block" value="Edit" onClick="editInventoryFun('.$queryRecordsObj->id.','.$queryRecordsObj->product_id.')">'.'<input type="button" class="btn btn-warning btn-xs btn-block" value="View Orders" onClick="view_delivered_orders_by_inventory_id('.$queryRecordsObj->id.')">';
					}
				}
				$row[]=$link;

				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function logistics_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->logistics_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->logistics_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->logistics_id.'">';
				$row[]=$queryRecordsObj->logistics_name;
				$row[]=$queryRecordsObj->logistics_weblink;
				$row[]=$queryRecordsObj->name;
				$row[]=$queryRecordsObj->logistics_priority;
				$row[]=$queryRecordsObj -> timestamp;

				$row[]='<form action="'.base_url().'admin/Catalogue/edit_logistics_form" method="post">	<input type="hidden" value="'.$queryRecordsObj -> logistics_id.'" name="logistics_id"><input type="hidden" value="'.$queryRecordsObj -> vendor_id.'" name="vendor_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function logistics_service_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->logistics_service_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->logistics_service_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->logistics_service_id.'">';
				$row[]=$queryRecordsObj->logistics_name;
				$row[]=$queryRecordsObj->city.'('.$queryRecordsObj->pin.')';
				$row[]="<div style='word-wrap: break-word;word-break: break-all;white-space: normal;'>".$queryRecordsObj->pincode."</div>";				
				$row[]=$queryRecordsObj -> timestamp;
				
				$row[]='<form action="'.base_url().'admin/Catalogue/edit_logistics_service_form" method="post">	<input type="hidden" value="'.$queryRecordsObj -> logistics_service_id.'" name="logistics_service_id"><input type="hidden" value="'.$queryRecordsObj -> logistics_id.'" name="logistics_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';

				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function logistics_territory_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->logistics_territory_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->logistics_territory_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->logistics_territory_id.'">';
				$row[]=$queryRecordsObj->logistics_name;
				$row[]=$queryRecordsObj->city;
				$row[]=$queryRecordsObj->name;
				$row[]=$queryRecordsObj->territory_name;				
				$row[]=$queryRecordsObj -> timestamp;
				
				$row[]='<form action="'.base_url().'admin/Catalogue/edit_logistics_territory_form" method="post">	<input type="hidden" value="'.$queryRecordsObj -> logistics_territory_id.'" name="logistics_territory_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function logistics_weight_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->logistics_weight_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->logistics_weight_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$get_logistics_all_territory_name_data_arr=$this->Model_catalogue->get_logistics_all_territory_name_data($queryRecordsObj->territory_name_id);
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->logistics_weight_id.'">';
				$row[]=$queryRecordsObj->logistics_name;
				$row[]=$get_logistics_all_territory_name_data_arr["territory_name"];
				$row[]=$queryRecordsObj->weight_in_kg;
				
				$res_obj=$this->Model_catalogue->get_all_volumetric_weights($queryRecordsObj->territory_name_id,$queryRecordsObj->logistics_delivery_mode_id,$queryRecordsObj->logistics_parcel_category_id,$queryRecordsObj->weight_in_kg,$queryRecordsObj->logistics_id,1);
				$input_arr=$res_obj;
				$temp_iphan_arr=[];
				$temp_greater_arr=[];
				$output_arr=[];
				foreach($input_arr as $k => $arr){
					if(preg_match("/-/",$arr["volume_in_kg"])){
						$temp_iphan_arr[$k]=explode("-",$arr["volume_in_kg"])[0];
					}
					if(preg_match("/>/",$arr["volume_in_kg"])){
						$temp_greater_arr[$k]=explode(">",$arr["volume_in_kg"])[0];
					}
				}
				asort($temp_iphan_arr);
			
				if(!empty($temp_iphan_arr)){	
					foreach($temp_iphan_arr as $k => $v){
						$output_arr[]=$input_arr[$k];
					}
				}
				if(!empty($temp_greater_arr)){
					foreach($temp_greater_arr as $k => $v){
						$output_arr[]=$input_arr[$k];
					}
				}
				$volumetric_data=$output_arr;
				
				$str='';
				if(!empty($volumetric_data)){
					$str.='<table class="table small">';
					$str.='<thead><th>Volume(kg)</th><th>Charge</th></thead>';
					
					foreach($volumetric_data as $res){
						$volume_in_kg=$res["volume_in_kg"];
						if(strpos($volume_in_kg, '>') !== false) {
							$volume_in_kg=str_replace('>', 'Above ', $volume_in_kg);
						}
						$str.='<tr><td>'.$volume_in_kg.'</td><td>'.$res["logistics_price"].'</td></tr>';
					}
					$str.='</table>';
				}
				$row[]=$str;	
				//$row[]=$queryRecordsObj->volume_in_kg;
				//$row[]=$queryRecordsObj->logistics_price;				
							
				$row[]=$queryRecordsObj -> timestamp;
				
				$row[]='<form action="'.base_url().'admin/Catalogue/edit_logistics_weight_form" method="post">	<input type="hidden" value="'.$queryRecordsObj -> logistics_id.'" name="logistics_id"><input type="hidden" value="'.$queryRecordsObj -> territory_name_id.'" name="logistics_territory_id"><input type="hidden" value="'.$queryRecordsObj -> weight_in_kg.'" name="weight_in_kg"><input type="hidden" value="'.$queryRecordsObj -> logistics_delivery_mode_id.'" name="logistics_delivery_mode_id"><input type="hidden" value="'.$queryRecordsObj -> logistics_parcel_category_id.'" name="logistics_parcel_category_id"><input type="hidden" value="'.$queryRecordsObj -> logistics_weight_id.'" name="logistics_weight_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';
				
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	////ash integrated///////////
	public function reasons(){
		
		if($this->session->userdata("logged_in")){
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['subcat_id']=$this->input->post("subcat_id");	
			$data['reasons'] = $this->Model_catalogue->reasons();
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data['create_editview']=$this->input->post("create_editview");
			$data["menu_flag"]="reasons_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/reasons_view',$data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function reasons_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['reason_id']=$this->input->post("reason_id");	
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="reasons_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/reasons_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function add_reason(){
	
		if($this->session->userdata("logged_in")){
			$reason_name=$this->db->escape_str($this -> input -> post("reason_name"));
			$cat_id=$this->input->post("categories");
			$subcat_id=$this->input->post("subcategories");
			$pcat_id=$this->input->post("parent_category");
			$menu_sort_order=$this->input->post("menu_sort_order");
			$view=$this->input->post("view");
			$flag=$this->Model_catalogue->check_duplication_name_reasons($reason_name,$pcat_id);
			if($flag==true){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->add_reason($reason_name,$subcat_id,$cat_id,$pcat_id,$menu_sort_order,$view);
				if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_reason(){
		if($this->session->userdata("logged_in")){
			$reason_id=$this->input->post("edit_reason_id");
			$reason_name=$this->db->escape_str($this -> input -> post("edit_reason_name"));
			$reason_name_default=$this->db->escape_str($this -> input -> post("edit_reason_name_default"));
			$subcat_id=$this->input->post("edit_subcategories");
			$cat_id=$this->input->post("edit_categories");
			$pcat_id=$this->input->post("edit_parent_category");
			$menu_sort_order=$this->input->post("edit_menu_sort_order");
			$view=$this->input->post("edit_view");
			$common_reason_return=$this->input->post("common_reason_return");
			$menu_sort_order_default=$this->input->post("edit_menu_sort_order_default");
			$flag_check="";
			if($reason_name_default!=$reason_name){
			$flag_check=$this->Model_catalogue->check_duplication_name_reasons($reason_name,$pcat_id);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
			$flag=$this->Model_catalogue->edit_reason($reason_id,$reason_name,$subcat_id,$cat_id,$pcat_id,$menu_sort_order,$view,$common_reason_return);
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}
			}

		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function delete_reason($reason_id){	
		if($this->session->userdata("logged_in")){
			$flag=$this -> Model_catalogue-> delete_reason($reason_id);
			if($flag==true){
				echo true;
			}else{
				echo false;
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	public function edit_reasons_form(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$pcat_id=$this->input->post("pcat_id");
			$cat_id=$this->input->post("cat_id");
			$subcat_id=$this->input->post("subcat_id");
			$reason_id=$this->input->post("reason_id");
			$data['brands'] = $this->Model_catalogue->brands();
			$data['subcategories'] = $this->Model_catalogue->subcategories();
			$data['categories'] = $this->Model_catalogue->category();
			$data['parent_catagories'] = $this->Model_catalogue->parent_category();
			$data['create_editview']=$this->input->post("create_editview");
			$data["get_reason_data"]=$this->Model_catalogue->get_reason_data($pcat_id,$cat_id,$subcat_id,$reason_id);
			$get_reason_data=$data["get_reason_data"];
			$data["get_sort_order_options_for_reason_return"]=$this->get_sort_order_options_for_reason_return($pcat_id,$cat_id,$subcat_id);
			$data["subcategory_sort_order_arr"]=$this->Model_catalogue->show_reason_return_edit_sort_order($data["get_reason_data"]["sort_order"],$data["get_reason_data"]["pcat_id"],$data["get_reason_data"]["cat_id"],$data["get_reason_data"]["subcat_id"]);
			$subcategory_sort_order_arr=$data["subcategory_sort_order_arr"];
			$data["pcat_id"]=$pcat_id;
			$data["cat_id"]=$cat_id;
			$data["subcat_id"]=$subcat_id;
			$data["reason_id"]=$reason_id;
			$data["menu_flag"]="reasons_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_reasons_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_sort_order_options_for_reason_return($pcat_id,$cat_id,$subcat_id){
		if($this->session->userdata("logged_in")){
			//$reason_arr=$this->Model_catalogue->get_reason_data($pcat_id,$cat_id,$subcat_id,$reason_id);
			$get_all_sub_category_arr=$this->Model_catalogue->get_all_reason_return_sort($pcat_id,$cat_id,$subcat_id);
				$subcat_id_arr=array();
				foreach($get_all_sub_category_arr as $sub_category_arr){
					$subcat_id_arr[]=$sub_category_arr["sort_order"];
				}
				
				if(count($subcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				$max_val_in_base_arr=0;
				for($i=1;$i<=max($subcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				
				$miss_arr=array_diff($base_arr,$subcat_id_arr);
				if(!empty($base_arr)) {	
                    $max_val_in_base_arr = max($base_arr);	
                }
				if(in_array($max_val_in_base_arr,$subcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				if(count($miss_arr)>0){
					$reason_arr["key"]="Sort Order Suggestion";
					$reason_arr["value"]=implode(",",$miss_arr);
				}
				else{
					$reason_arr["key"]="Sort Order Suggestion";
					$reason_arr["value"]=max($subcat_id_arr)+1;
				}
				}else{
					$reason_arr["key"]="";
					$reason_arr["value"]="No Reason Return";
				}
			return $reason_arr;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function reasons_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->reasons_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->reasons_processing($params,"get_recs");
			$i=1;
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]=$i;
				$row[]=$queryRecordsObj->reason_name;
				$row[]=$queryRecordsObj->subcat_name;
				$row[]=$queryRecordsObj->cat_name;
				$row[]=$queryRecordsObj->pcat_name;
				$str='';
				$str.="View :".  $queryRecordsObj -> view; 
				$row[]=$str;
				$row[]=$queryRecordsObj -> timestamp;
								
				$row[]='<form action="'.base_url().'admin/Catalogue/edit_reasons_form" method="post">	<input type="hidden" value="'.$queryRecordsObj -> pcat_id.'" name="pcat_id"><input type="hidden" value="'.$queryRecordsObj -> cat_id.'" name="cat_id"><input type="hidden" value="'.$queryRecordsObj -> subcat_id.'" name="subcat_id"><input type="hidden" value="'.$queryRecordsObj -> reason_id.'" name="reason_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ><input type="button" class="btn btn-danger btn-xs btn-block" value="Delete" onClick="deleteReasonFun('.$queryRecordsObj -> reason_id.')"></form>';
				

				$data[]=$row;
				$i++;
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function subreason(){
		
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;	
			$reason_id=$this->input->post("reason_id");
			$data['reason_id']=$reason_id;	
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$subcat_id=$this->input->post("subcat_id");	
			$data['subcat_id']=	$subcat_id;	
			$data['reason_id']=$this->input->post("reason_id");		
			$data['subreason'] = $this->Model_catalogue->subreason($reason_id);
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data['reason_return'] = $this->Model_catalogue->reason_return();
			$data['create_editview']=$this->input->post("create_editview");
			$data["menu_flag"]="reasons_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/subreason_view',$data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function subreasons_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['reason_id']=$this->input->post("reason_id");			
			$data['subcat_id']=$this->input->post("subcat_id");
			$data['cat_id']=$this->input->post("cat_id");
			$data['pcat_id']=$this->input->post("pcat_id");
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data["menu_flag"]="reasons_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/subreasons_filter',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function add_subreason(){
		if($this->session->userdata("logged_in")){
			$sub_issue_name=$this->db->escape_str($this -> input -> post("sub_issue_name"));
			$reason_id=$this->input->post("reason_id");
			$menu_sort_order=$this->input->post("menu_sort_order");
			$view=$this->input->post("view");
			$flag=$this->Model_catalogue->check_duplication_name_subreasons($sub_issue_name,$reason_id);
			if($flag==true){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->add_subreason($sub_issue_name,$reason_id,$menu_sort_order,$view);
				if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function subreasons_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->subreasons_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->subreasons_processing($params,"get_recs");
			$i=1;
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]=$i;
				$row[]=$queryRecordsObj->sub_issue_name;	
				$row[]=$queryRecordsObj->sort_order;
				$row[]=$queryRecordsObj->view;
				$row[]=$queryRecordsObj -> timestamp;
				
				$row[]='<form action="'.base_url().'admin/Catalogue/edit_subreasons_form" method="post">	<input type="hidden" value="'.$queryRecordsObj -> sub_issue_id.'" name="sub_issue_id"><input type="hidden" value="'.$queryRecordsObj -> reason_id.'" name="reason_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ><input type="button" class="btn btn-danger btn-xs btn-block" value="Delete" onClick="deletesubReasonFun('.$queryRecordsObj -> sub_issue_id.')"></form>';

				$data[]=$row;
				$i++;
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function edit_subreasons_form(){
		if($this->session->userdata("logged_in")){
		$sub_issue_id=$this->input->post("sub_issue_id");
		$reason_id=$this->input->post("reason_id");
		$data["controller"]=$this;
		$data['sub_issue_id']=$sub_issue_id;	
		$data['reason_id']=$reason_id;
		$get_reasons_details_arr=$this->Model_catalogue->get_reasons_details($reason_id);
		$data['pcat_id']=$get_reasons_details_arr["pcat_id"];
		$data['cat_id']=$get_reasons_details_arr["cat_id"];	
		$data['subcat_id']=	$get_reasons_details_arr["subcat_id"];		
		$data['parent_category'] = $this->Model_catalogue->parent_category();
		$data['reason_return'] = $this->Model_catalogue->reason_return();
		$data['create_editview']=$this->input->post("create_editview");
		$data["get_subreason_data"]=$this->Model_catalogue->get_subreason_data($sub_issue_id);
		$get_subreason_data=$data["get_subreason_data"];
		$data["subcategory_sort_order_arr"]=$this->Model_catalogue->show_subreason_edit_sort_order($data["get_subreason_data"]["sort_order"],$reason_id);
		$subcategory_sort_order_arr=$data["subcategory_sort_order_arr"];
		
		$data["get_sort_order_options_for_subreason"]=$this->get_sort_order_options_for_subreason($reason_id);
		$data["menu_flag"]="reasons_active_links";
		$this -> load -> view('admin/vmanage',$data);
		$this -> load -> view('admin/edit_subreason_view',$data);	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_sort_order_options_for_subreason($reason_id){
		if($this->session->userdata("logged_in")){
			$get_all_sub_category_arr=$this->Model_catalogue->get_all_subreason_sort($reason_id);
				$subcat_id_arr=array();
				foreach($get_all_sub_category_arr as $sub_category_arr){
					$subcat_id_arr[]=$sub_category_arr["sort_order"];
				}
				
				if(count($subcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($subcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				
				$miss_arr=array_diff($base_arr,$subcat_id_arr);
				if(count($base_arr)>0){
					$max_val_in_base_arr = max($base_arr);
				}
				else{
					$max_val_in_base_arr=0;
				}
				if(in_array($max_val_in_base_arr,$subcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				if(count($miss_arr)>0){
					$reason_arr["key"]="Sort Order Suggestion";
					$reason_arr["value"]=implode(",",$miss_arr);
				}
				else{
					$reason_arr["key"]="Sort Order Suggestion";
					$reason_arr["value"]=max($subcat_id_arr)+1;
				}
				}else{
					$reason_arr["key"]="";
					$reason_arr["value"]="No Reason Return";
				}
			return $reason_arr;			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_subreason(){
		if($this->session->userdata("logged_in")){		
			$reason_id=$this->input->post("edit_reason_id");
			$edit_sub_issue_id=$this->input->post("edit_sub_issue_id");
			$edit_subreason_name=$this->db->escape_str($this -> input -> post("edit_subreason_name"));
			$subreason_name_default=$this->db->escape_str($this -> input -> post("edit_subreason_name_default"));
			$menu_sort_order=$this->input->post("edit_menu_sort_order");
			$edit_view=$this->input->post("edit_view");
			$common_subreason=$this->input->post("common_subreason");
			$menu_sort_order_default=$this->input->post("edit_menu_sort_order_default");
			$flag_check="";
			if($subreason_name_default!=$edit_subreason_name){
			$flag_check=$this->Model_catalogue->check_duplication_name_subreasons($edit_subreason_name,$reason_id);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
			$flag=$this->Model_catalogue->edit_subreason($edit_subreason_name,$edit_view,$reason_id,$edit_sub_issue_id,$menu_sort_order,$common_subreason);
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}
			}

		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function delete_subreason($sub_issue_id){
		
		if($this->session->userdata("logged_in")){
			$flag=$this -> Model_catalogue-> delete_subreason($sub_issue_id);
			if($flag==true){
				echo true;
			}else{
				echo false;
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	
	////ash integrated///////////
	
	public function reason_cancel(){
		if($this->session->userdata("logged_in")){
			$data['reason_cancel'] = $this->Model_catalogue->reason_cancel();
			$data["menu_flag"]="reasons_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/reason_cancel_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function add_reason_cancel(){
		if($this->session->userdata("logged_in")){
			$reason_name=$this->db->escape_str($this -> input -> post("reason_name"));
			$menu_sort_order=$this->input->post("menu_sort_order");
			$flag=$this->Model_catalogue->check_duplication_name_customer_cancel_reason($reason_name);
			if($flag==true){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->add_reason_cancel($reason_name,$menu_sort_order);
				if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}

		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_reason_cancel_form(){
		if($this->session->userdata("logged_in")){
			$cancel_reason_id=$this->input->post("cancel_reason_id");
			$data['reason_cancel'] = $this->Model_catalogue->reason_cancel();	
			$data["get_reason_cancel_data"]=$this->Model_catalogue->get_reason_cancel_data($cancel_reason_id);
			$get_reason_cancel_data=$data["get_reason_cancel_data"];
			$data["cancel_reason_id"]=$cancel_reason_id;
			$data["get_sort_order_options_for_reason_cancel"]=$this->get_sort_order_options_for_reason_cancel();
			$data["reason_cancel_sort_order_arr"]=$this->Model_catalogue->show_reason_cancel_edit_sort_order($data["get_reason_cancel_data"]["sort_order"]);
			$reason_cancel_sort_order_arr=$data["reason_cancel_sort_order_arr"];
			$data["menu_flag"]="reasons_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_reason_cancel_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_sort_order_options_for_reason_cancel(){
		if($this->session->userdata("logged_in")){
			$get_all_reason_cancel_arr=$this->Model_catalogue->get_all_reason_cancel_sort();
				$subcat_id_arr=array();
				foreach($get_all_reason_cancel_arr as $reason_cancel_sort_arr){
					$subcat_id_arr[]=$reason_cancel_sort_arr["sort_order"];
				}
				
				if(count($subcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($subcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				
				$miss_arr=array_diff($base_arr,$subcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$subcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				if(count($miss_arr)>0){
					$reason_cancel_arr["key"]="Sort Order Suggestion";
					$reason_cancel_arr["value"]=implode(",",$miss_arr);
				}
				else{
					$reason_cancel_arr["key"]="Sort Order Suggestion";
					$reason_cancel_arr["value"]=max($subcat_id_arr)+1;
				}
				}else{
					$reason_cancel_arr["key"]="";
					$reason_cancel_arr["value"]="No Reason cancel";
				}
			return $reason_cancel_arr;	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function edit_reason_cancel(){
		if($this->session->userdata("logged_in")){
			$cancel_reason_id=$this->input->post("cancel_reason_id");
			$reason_name=$this->db->escape_str($this -> input -> post("edit_reason_name"));
			$reason_name_default=$this->db->escape_str($this -> input -> post("edit_reason_name_default"));
			$menu_sort_order=$this->input->post("edit_menu_sort_order");
			$common_reason_cancel=$this->input->post("common_reason_cancel");
			$menu_sort_order_default=$this->input->post("edit_menu_sort_order_default");
			$flag_check="";
			if($reason_name_default!=$reason_name){
			$flag_check=$this->Model_catalogue->check_duplication_name_customer_cancel_reason($reason_name);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
			$flag=$this->Model_catalogue->edit_reason_cancel($cancel_reason_id,$reason_name,$menu_sort_order,$common_reason_cancel);
			if($flag==true){
				echo $flag;
			}else{
				echo 0;
			}
			}

		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function delete_reason_cancel_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_reason_cancel_selected($selected_list);			
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function admin_cancel_reason(){
		if($this->session->userdata("logged_in")){
			$data['admin_cancel_reason'] = $this->Model_catalogue->admin_cancel_reason();
			$data["menu_flag"]="reasons_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/admin_cancel_reason_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function add_admin_cancel_reason(){
		if($this->session->userdata("logged_in")){
			$cancel_reason=$this->db->escape_str($this -> input -> post("cancel_reason"));
			$menu_sort_order=$this->input->post("menu_sort_order");
			$flag=$this->Model_catalogue->check_duplication_name_admin_cancel_reason($cancel_reason);
			if($flag==true){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->add_admin_cancel_reason($cancel_reason,$menu_sort_order);
				if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_admin_cancel_reason_form(){
		if($this->session->userdata("logged_in")){
			$cancel_reason_id=$this->input->post("cancel_reason_id");
			$data['admin_cancel_reason'] = $this->Model_catalogue->admin_cancel_reason();		
			$data["get_admin_cancel_reason_data"]=$this->Model_catalogue->get_admin_cancel_reason_data($cancel_reason_id);
			$get_admin_cancel_reason_data=$data["get_admin_cancel_reason_data"];
			$data["cancel_reason_id"]=$cancel_reason_id;
			$data["get_sort_order_options_for_admin_cancel"]=$this->get_sort_order_options_for_admin_cancel();
			$data["reason_cancel_sort_order_arr"]=$this->Model_catalogue->show_admin_cancel_reason_edit_sort_order($data["get_admin_cancel_reason_data"]["sort_order"]);
			$reason_cancel_sort_order_arr=$data["reason_cancel_sort_order_arr"];
			$data["menu_flag"]="reasons_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_admin_cancel_reason_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_sort_order_options_for_admin_cancel(){
		if($this->session->userdata("logged_in")){
			$get_all_reason_cancel_arr=$this->Model_catalogue->get_all_admin_cancel_reason_sort();
				$subcat_id_arr=array();
				foreach($get_all_reason_cancel_arr as $reason_cancel_sort_arr){
					$subcat_id_arr[]=$reason_cancel_sort_arr["sort_order"];
				}
				
				if(count($subcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($subcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				
				$miss_arr=array_diff($base_arr,$subcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$subcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				if(count($miss_arr)>0){
					$reason_cancel_arr["key"]="Sort Order Suggestion";
					$reason_cancel_arr["value"]=implode(",",$miss_arr);
				}
				else{
					$reason_cancel_arr["key"]="Sort Order Suggestion";
					$reason_cancel_arr["value"]=max($subcat_id_arr)+1;
				}
				}else{
					$reason_cancel_arr["key"]="";
					$reason_cancel_arr["value"]="No Reason cancel";
				}
			return $reason_cancel_arr;	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function edit_admin_cancel_reason(){
		if($this->session->userdata("logged_in")){
			$cancel_reason_id=$this->input->post("cancel_reason_id");
			$reason_name=$this->db->escape_str($this -> input -> post("edit_cancel_reason"));
			$cancel_reason_default=$this->db->escape_str($this -> input -> post("edit_cancel_reason_default"));
			$menu_sort_order=$this->input->post("edit_menu_sort_order");
			$common_admin_reason_cancel=$this->input->post("common_admin_reason_cancel");
			$menu_sort_order_default=$this->input->post("edit_menu_sort_order_default");
			$flag_check="";
			if($cancel_reason_default!=$reason_name){
			$flag_check=$this->Model_catalogue->check_duplication_name_admin_cancel_reason($reason_name);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
			$flag=$this->Model_catalogue->edit_admin_cancel_reason($cancel_reason_id,$reason_name,$menu_sort_order,$common_admin_reason_cancel);
			if($flag==true){
				echo $flag;
			}else{
				echo 0;
			}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function delete_admin_cancel_reason_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_admin_cancel_reason_selected($selected_list);			
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function admin_replacement_reason(){
		if($this->session->userdata("logged_in")){
			$data['admin_replacement_reason'] = $this->Model_catalogue->admin_replacement_reason();
			$data["menu_flag"]="reasons_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/admin_replacement_reason_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function edit_admin_replacement_reason_form(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("id");
			$data['admin_replacement_reason'] = $this->Model_catalogue->admin_replacement_reason();			
			$data["get_admin_replacement_reason_data"]=$this->get_admin_replacement_reason_data($id);
			$get_admin_replacement_reason_data=$data["get_admin_replacement_reason_data"];
			$data["id"]=$id;
			$data["menu_flag"]="reasons_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_admin_replacement_reason_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function add_admin_replacement_reason(){
		if($this->session->userdata("logged_in")){
			$return_options=$this->db->escape_str($this -> input -> post("return_options"));
			$decision_return=$this->input->post("decision_return");
			$pickup_identifier=$this->input->post("pickup_identifier");
			$flag=$this->Model_catalogue->check_duplication_name_admin_replacement_reason($return_options);
			if($flag==true){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->add_admin_replacement_reason($return_options,$decision_return,$pickup_identifier);
				if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_admin_replacement_reason_data($id){
		if($this->session->userdata("logged_in")){
			//$id=$this->input->post("id");
			$replace_reason_arr=$this->Model_catalogue->get_admin_replacement_reason_data($id);
			return $replace_reason_arr;	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function edit_admin_replacement_reason(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("id");
			$edit_return_options=$this->db->escape_str($this -> input -> post("edit_return_options"));
			$return_options_default=$this->db->escape_str($this -> input -> post("edit_return_options_default"));
			$edit_decision_return=$this->input->post("edit_decision_return");
			$edit_pickup_identifier=$this->input->post("edit_pickup_identifier");
			$flag_check="";
			if($return_options_default!=$edit_return_options){
			$flag_check=$this->Model_catalogue->check_duplication_name_admin_replacement_reason($edit_return_options);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->edit_admin_replacement_reason($id,$edit_return_options,$edit_decision_return,$edit_pickup_identifier);
				if($update_flag==true){
					echo true;
				}else{
					echo 0;
				}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function delete_admin_replacement_reason_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_admin_replacement_reason_selected($selected_list);			
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function admin_return_reason(){
		if($this->session->userdata("logged_in")){
			$data['admin_return_reason'] = $this->Model_catalogue->admin_return_reason();
			$data["menu_flag"]="reasons_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/admin_return_reason_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function edit_admin_return_reason_form(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("id");
			$data['admin_return_reason'] = $this->Model_catalogue->admin_return_reason();		
			$data["get_admin_return_reason_data"]=$this->get_admin_return_reason_data($id);
			$get_admin_return_reason_data=$data["get_admin_return_reason_data"];
			$data["id"]=$id;
			$data["menu_flag"]="reasons_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_admin_return_reason_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function add_admin_return_reason(){
		if($this->session->userdata("logged_in")){
			$return_options=$this->db->escape_str($this -> input -> post("return_options"));
			$decision_return=$this->input->post("decision_return");
			$pickup_identifier=$this->input->post("pickup_identifier");
			$flag=$this->Model_catalogue->check_duplication_name_admin_return_reason($return_options);
			if($flag==true){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->add_admin_return_reason($return_options,$decision_return,$pickup_identifier);
				if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_admin_return_reason_data($id){
		if($this->session->userdata("logged_in")){
			//$id=$this->input->post("id");
			$return_reason_arr=$this->Model_catalogue->get_admin_return_reason_data($id);
			return $return_reason_arr;	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function edit_admin_return_reason(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("id");
			$edit_return_options=$this->db->escape_str($this -> input -> post("edit_return_options"));
			$return_options_default=$this->db->escape_str($this -> input -> post("edit_return_options_default"));
			$edit_decision_return=$this->input->post("edit_decision_return");
			$edit_pickup_identifier=$this->input->post("edit_pickup_identifier");
			$flag_check="";
			if($return_options_default!=$edit_return_options){
			$flag_check=$this->Model_catalogue->check_duplication_name_admin_return_reason($edit_return_options);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->edit_admin_return_reason($id,$edit_return_options,$edit_decision_return,$edit_pickup_identifier);
				if($update_flag==true){
					echo true;
				}else{
					echo 0;
				}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function delete_admin_return_reason_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_admin_return_reason_selected($selected_list);			
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function customer_deactivate_reason(){
		if($this->session->userdata("logged_in")){
			$data['customer_deactivate_reason'] = $this->Model_catalogue->customer_deactivate_reason();	$data["menu_flag"]="reasons_active_links";	
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/customer_deactivate_reason_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function add_deactivate_reason(){
		if($this->session->userdata("logged_in")){
			$deactivate_reason=$this->db->escape_str($this -> input -> post("deactivate_reason"));
			$menu_sort_order=$this->input->post("menu_sort_order");
			$view=$this->input->post("view");
			$flag=$this->Model_catalogue->check_duplication_name_customer_deactivate($deactivate_reason);
			if($flag==true){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->add_deactivate_reason($deactivate_reason,$menu_sort_order,$view);
				if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}

		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_customer_deactivate_reason_form(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("id");
			$data['customer_deactivate_reason'] = $this->Model_catalogue->customer_deactivate_reason();		
			$data["get_customer_deactivate_reason_data"]=$this->Model_catalogue->get_customer_deactivate_reason_data($id);
			$get_customer_deactivate_reason_data=$data["get_customer_deactivate_reason_data"];
			$data["get_sort_order_options_for_customer_deactivate"]=$this->get_sort_order_options_for_customer_deactivate();
			$data["reason_cancel_sort_order_arr"]=$this->Model_catalogue->show_customer_deactivate_edit_sort_order($data["get_customer_deactivate_reason_data"]["sort_order"]);
			$reason_cancel_sort_order_arr=$data["reason_cancel_sort_order_arr"];
			$data["id"]=$id;
			$data["menu_flag"]="reasons_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_customer_deactivate_reason_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_sort_order_options_for_customer_deactivate(){
		if($this->session->userdata("logged_in")){
			$get_all_reason_cancel_arr=$this->Model_catalogue->get_all_customer_deactivate_sort();
				$subcat_id_arr=array();
				foreach($get_all_reason_cancel_arr as $reason_cancel_sort_arr){
					$subcat_id_arr[]=$reason_cancel_sort_arr["sort_order"];
				}
				
				if(count($subcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($subcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				
				$miss_arr=array_diff($base_arr,$subcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$subcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				if(count($miss_arr)>0){
					$deactivate_reason_arr["key"]="Sort Order Suggestion";
					$deactivate_reason_arr["value"]=implode(",",$miss_arr);
				}
				else{
					$deactivate_reason_arr["key"]="Sort Order Suggestion";
					$deactivate_reason_arr["value"]=max($subcat_id_arr)+1;
				}
				}else{
					$deactivate_reason_arr["key"]="";
					$deactivate_reason_arr["value"]="No Customer Deactivate Reason";
				}
			return $deactivate_reason_arr;	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function edit_deactivate_reason(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("id");
			$edit_deactivate_reason=$this->db->escape_str($this -> input -> post("edit_customer_deactivate_reason"));
			$customer_deactivate_reason_default=$this->db->escape_str($this -> input -> post("edit_customer_deactivate_reason_default"));
			$menu_sort_order=$this->input->post("edit_menu_sort_order");
			$view=$this->input->post("edit_view");
			$common_customer_deactivate=$this->input->post("common_customer_deactivate");
			$menu_sort_order_default=$this->input->post("edit_menu_sort_order_default");
			$flag_check="";
			if($customer_deactivate_reason_default!=$edit_deactivate_reason){
			$flag_check=$this->Model_catalogue->check_duplication_name_customer_deactivate($edit_deactivate_reason);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
			$flag=$this->Model_catalogue->edit_deactivate_reason($id,$edit_deactivate_reason,$menu_sort_order,$view,$common_customer_deactivate);
			if($flag==true){
				echo $flag;
			}else{
				echo 0;
			}
			}

		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function delete_deactivate_reason_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_deactivate_reason_selected($selected_list);			
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function payment_options(){
		if($this->session->userdata("logged_in")){
			$data['payment_options'] = $this->Model_catalogue->payment_options();
			$data["menu_flag"]="reasons_active_links";	
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/payment_options_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function add_payment_option(){
		if($this->session->userdata("logged_in")){
			$payment_option=$this->db->escape_str($this -> input -> post("payment_option"));
			$menu_sort_order=$this->input->post("menu_sort_order");
			$view=$this->input->post("view");
			$flag=$this->Model_catalogue->check_duplication_name_payment_options($payment_option);
			if($flag==true){
				echo "exist";
			}else{
			$update_flag=$this->Model_catalogue->add_payment_options($payment_option,$menu_sort_order,$view);
				if($update_flag==true){
					echo "yes";
				}else{
					echo "no";
				}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_payment_options_form(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("id");
			$data['payment_options'] = $this->Model_catalogue->payment_options();	
			$data["get_payment_options_data"]=$this->Model_catalogue->get_payment_options_data($id);
			$data["id"]=$id;
			$data["get_sort_order_options_for_payment_options"]=$this->get_sort_order_options_for_payment_options();
			$data["payment_options_sort_order_arr"]=$this->Model_catalogue->show_payment_options_edit_sort_order($data["get_payment_options_data"]["sort_order"]);
			$payment_options_sort_order_arr=$data["payment_options_sort_order_arr"];
			$data["menu_flag"]="reasons_active_links";	
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_payment_options_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function get_sort_order_options_for_payment_options(){
		if($this->session->userdata("logged_in")){
			$get_all_reason_cancel_arr=$this->Model_catalogue->get_all_payment_options();
				$subcat_id_arr=array();
				foreach($get_all_reason_cancel_arr as $reason_cancel_sort_arr){
					$subcat_id_arr[]=$reason_cancel_sort_arr["sort_order"];
				}
				
				if(count($subcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($subcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				
				$miss_arr=array_diff($base_arr,$subcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$subcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				if(count($miss_arr)>0){
					$payment_options_arr["key"]="Sort Order Suggestion";
					$payment_options_arr["value"]=implode(",",$miss_arr);
				}
				else{
					$payment_options_arr["key"]="Sort Order Suggestion";
					$payment_options_arr["value"]=max($subcat_id_arr)+1;
				}
				}else{
					$payment_options_arr["key"]="";
					$payment_options_arr["value"]="No Payment Options";
				}
			return $payment_options_arr ;	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function edit_payment_options(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("id");
			$edit_payment_option_name=$this->db->escape_str($this -> input -> post("edit_payment_option_name"));
			$edit_payment_option_name_default=$this->db->escape_str($this -> input -> post("edit_payment_option_name_default"));
			$menu_sort_order=$this->input->post("edit_menu_sort_order");
			$view=$this->input->post("edit_view");
			$common_payment_options=$this->input->post("common_payment_options");
			$menu_sort_order_default=$this->input->post("edit_menu_sort_order_default");
			$flag_check="";
			if($edit_payment_option_name_default!=$edit_payment_option_name){
			$flag_check=$this->Model_catalogue->check_duplication_name_payment_options($edit_payment_option_name);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
			$flag=$this->Model_catalogue->edit_payment_options($id,$edit_payment_option_name,$menu_sort_order,$view,$common_payment_options);
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}
			}

		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_sort_order_payment_options(){
		if($this->session->userdata("logged_in")){
			$get_all_payment_options_arr=$this->Model_catalogue->get_all_payment_options();
				$pcat_id_arr=array();
				foreach($get_all_payment_options_arr as $all_payment_options_arr){
					$pcat_id_arr[]=$all_payment_options_arr["sort_order"];
				}
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				
				if(count($miss_arr)>0){
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<?php
				foreach($miss_arr as $k => $v){
					if($k==count($miss_arr)+1){
				?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
					else{
						?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
				}
				}
				else{
			
				?>
					<option value="" selected></option>
					<option value="0">0</option>
					<option value="<?php echo (max($pcat_id_arr)+1);?>"><?php echo (max($pcat_id_arr)+1);?></option>
				<?php
				
				}
				}else{
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<option value="1">1</option>
				<?php
				}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function delete_payment_options_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_payment_options_selected($selected_list);			
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function parent_category_count(){
		if($this->session->userdata("logged_in")){
			$count=$this->Model_catalogue->parent_category_count();
			echo $count;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function archived_parent_category_count(){
		if($this->session->userdata("logged_in")){
			$count=$this->Model_catalogue->archived_parent_category_count();
			echo $count;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function vendors_count(){
		if($this->session->userdata("logged_in")){
			$count=$this->Model_catalogue->vendors_count();
			echo $count;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function archived_vendor_count(){
		if($this->session->userdata("logged_in")){
			$count=$this->Model_catalogue->archived_vendor_count();
			echo $count;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function logistics_pincode_count(){
		if($this->session->userdata("logged_in")){
			$count=$this->Model_catalogue->logistics_pincode_count();
			echo $count;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}

	public function show_edit_sort_order(){
		if($this->session->userdata("logged_in")){
			$edit_menu_sort_order=$this->input->post("edit_menu_sort_order");
			$show_sort_order_arr=$this->Model_catalogue->show_edit_sort_order($edit_menu_sort_order);
			?>
			<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
			<div class="form-group label-floating">
			<label class="control-label">-Select Options-</label>
			<select class="form-control" id="show_available_common_cat" onchange="changeEventHandler(event)">
			<option selected value=""></option>
			<?php
			
			foreach($show_sort_order_arr as $sort_order_arr){
				
				?>
				
					<option value="<?php echo $sort_order_arr["type"]."_".$sort_order_arr["common_cat_id"];?>"><?php echo $sort_order_arr["common_cat_name"]."-".$sort_order_arr["type"]."(".$sort_order_arr["menu_sort_order"].")";?></option>
					<?php
			}
			?>
			
			</select>
			</div>
				<?php
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}	
	
	public function show_subcategory_sort_order(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("parent_category");
			$cat_id=$this->input->post("categories");
			$get_all_sub_category_arr=$this->Model_catalogue->get_all_sub_category($pcat_id,$cat_id);
				$pcat_id_arr=array();
				foreach($get_all_sub_category_arr as $all_sub_category_arr){
					$pcat_id_arr[]=$all_sub_category_arr["menu_sort_order"];
				}
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				
				if(count($miss_arr)>0){
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<?php
				foreach($miss_arr as $k => $v){
					if($k==count($miss_arr)+1){
				?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
					else{
						?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
				}
				}
				else{
			
				?>
					<option value="" selected></option>
					<option value="0">0</option>
					<option value="<?php echo (max($pcat_id_arr)+1);?>"><?php echo (max($pcat_id_arr)+1);?></option>
				<?php
				
				}
				}else{
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<option value="1">1</option>
				<?php
				}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}

	public function get_edit_parent_category($edit_menu_level,$pcat_id,$menu_level_default_value,$pcat_id_default){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$get_edit_parent_category_dropdown="";
			if($edit_menu_level==1 && $pcat_id==""){
				
					$get_edit_parent_category_dropdown.='<option value="0"></option>';
				
			}
			
			if($edit_menu_level==2 && $pcat_id=="" && $edit_menu_level!=$menu_level_default_value){
				$get_all_parent_category_arr=$this->Model_catalogue->parent_category();
				
				
				$get_edit_parent_category_dropdown.='<option value="">-Select Parent Category-</option>';
				
				foreach($get_all_parent_category_arr as $parent_category_arr){
					$pcat_id_assign=$parent_category_arr->pcat_id;
					$pcat_name_assign=$parent_category_arr->pcat_name;
					$get_edit_parent_category_dropdown.="<option value='$pcat_id_assign'>$pcat_name_assign</option>";
					
				}
				
			}
			if($edit_menu_level==2 && $pcat_id!=""){
				$get_selected_parent_category_arr=$this->Model_catalogue->get_parent_category_data($pcat_id);
					$pcat_id_assign=$get_selected_parent_category_arr['pcat_id'];
					$pcat_name_assign=$get_selected_parent_category_arr['pcat_name'];
					$get_edit_parent_category_dropdown.="<option value='$pcat_id_assign'>$pcat_name_assign</option>";
					
					
					$edit_cat_parent_category_arr=$this->Model_catalogue->parent_category_edit_cat($pcat_id);
				foreach($edit_cat_parent_category_arr as $parent_cat_arr){
					$pcat_id_assign=$parent_cat_arr->pcat_id;
					$pcat_name_assign=$parent_cat_arr->pcat_name;
					$get_edit_parent_category_dropdown.="<option value='$pcat_id_assign'>$pcat_name_assign</option>";
					
				}
			}
			if($edit_menu_level==2 && $edit_menu_level==$menu_level_default_value && $pcat_id==""){
				$get_selected_parent_category_arr_def=$this->Model_catalogue->get_parent_category_data($pcat_id_default);
				$pcat_id_assign=$get_selected_parent_category_arr_def['pcat_id'];
				$pcat_name_assign=$get_selected_parent_category_arr_def["pcat_name"];
				
					$get_edit_parent_category_dropdown.="<option value='$pcat_id_assign'>$pcat_name_assign</option>";
					
					
					$edit_cat_parent_category_arr=$this->Model_catalogue->parent_category_edit_cat($pcat_id_default);
				foreach($edit_cat_parent_category_arr as $parent_cat_arr){
					$pcat_id_assign=$parent_cat_arr->pcat_id;
					$pcat_name_assign=$parent_cat_arr->pcat_name;
					$get_edit_parent_category_dropdown.="<option value='$pcat_id_assign'>$pcat_name_assign</option>";
					
				}
			}
			return $get_edit_parent_category_dropdown;
			
		
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function get_edit_parent_category_ajax(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$edit_menu_level=$this->input->post("menu_level");
			$pcat_id=$this->input->post("pcat_id");
			$menu_level_default_value=$this->input->post("menu_level_default_value");
			$pcat_id_default=$this->input->post("edit_parent_category_default_value");
			$edit_menu_sort_order_in_default=$this->input->post("edit_menu_sort_order_arr_in_default");
			$get_edit_parent_category_dropdown="";
			if($edit_menu_level==1 && $pcat_id==""){
				
					$get_edit_parent_category_dropdown.='<option value="0"></option>';
				
			}
			
			if($edit_menu_level==2 && $pcat_id=="" && $edit_menu_level!=$menu_level_default_value){
				$get_all_parent_category_arr=$this->Model_catalogue->parent_category();
				
				
				$get_edit_parent_category_dropdown.='<option value="">-Select Parent Category-</option>';
				
				foreach($get_all_parent_category_arr as $parent_category_arr){
					$pcat_id_assign=$parent_category_arr->pcat_id;
					$pcat_name_assign=$parent_category_arr->pcat_name;
					$get_edit_parent_category_dropdown.="<option value='$pcat_id_assign'>$pcat_name_assign</option>";
					
				}
				
			}
			if($edit_menu_level==2 && $pcat_id!=""){
				$get_selected_parent_category_arr=$this->Model_catalogue->get_parent_category_data($pcat_id);
					$pcat_id_assign=$get_selected_parent_category_arr['pcat_id'];
					$pcat_name_assign=$get_selected_parent_category_arr['pcat_name'];
					$get_edit_parent_category_dropdown.="<option value='$pcat_id_assign'>$pcat_name_assign</option>";
					
					
					$edit_cat_parent_category_arr=$this->Model_catalogue->parent_category_edit_cat($pcat_id);
				foreach($edit_cat_parent_category_arr as $parent_cat_arr){
					$pcat_id_assign=$parent_cat_arr->pcat_id;
					$pcat_name_assign=$parent_cat_arr->pcat_name;
					$get_edit_parent_category_dropdown.="<option value='$pcat_id_assign'>$pcat_name_assign</option>";
					
				}
			}
			if($edit_menu_level==2 && $edit_menu_level==$menu_level_default_value && $pcat_id==""){
				$get_selected_parent_category_arr_def=$this->Model_catalogue->get_parent_category_data($pcat_id_default);
				$pcat_id_assign=$get_selected_parent_category_arr_def['pcat_id'];
				$pcat_name_assign=$get_selected_parent_category_arr_def["pcat_name"];
				
					$get_edit_parent_category_dropdown.="<option value='$pcat_id_assign'>$pcat_name_assign</option>";
					
					
					$edit_cat_parent_category_arr=$this->Model_catalogue->parent_category_edit_cat($pcat_id_default);
				foreach($edit_cat_parent_category_arr as $parent_cat_arr){
					$pcat_id_assign=$parent_cat_arr->pcat_id;
					$pcat_name_assign=$parent_cat_arr->pcat_name;
					$get_edit_parent_category_dropdown.="<option value='$pcat_id_assign'>$pcat_name_assign</option>";
					
				}
			}
		
			$get_sort_order_dropdown="";
			if(($edit_menu_level==1 && $pcat_id=="") || ($edit_menu_level==2 && $pcat_id!="") || ($edit_menu_level==2 && $edit_menu_level==$menu_level_default_value)){
				if($edit_menu_level==2 && $edit_menu_level==$menu_level_default_value && $pcat_id==""){
					$pcat_id=$pcat_id_default;
				}
			 $get_edit_all_parent_category_arr=$this->Model_catalogue->get_edit_all_parent_category($edit_menu_level,$pcat_id);
				$pcat_id_arr=array();
				foreach($get_edit_all_parent_category_arr as $all_parent_category_arr){
					$pcat_id_arr[]=$all_parent_category_arr["menu_sort_order"];
				}
				
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				
				if(count($miss_arr)>0){
				?>
				<?php
					$get_sort_order_dropdown.='<option value="" selected>';
					if($edit_menu_sort_order_in_default!=0){
					$get_sort_order_dropdown.='<option value="0">0</option>';
					}
				?>
				
				
				<?php
				foreach($miss_arr as $k => $v){
					if($k==count($miss_arr)+1){
				
					$get_sort_order_dropdown.='<option value="'.$v.'">'.$v.'</option>';
				
					}
					else{
						
					$get_sort_order_dropdown.='<option value="'.$v.'">'.$v.'</option>';
				
					}
				}
				}
				else{
			
				
					$get_sort_order_dropdown.='<option value="" selected></option>';
					if($edit_menu_sort_order_in_default!=0){
					$get_sort_order_dropdown.='<option value="0">0</option>';
					}
					$get_sort_order_dropdown.='<option value="'.(max($pcat_id_arr)+1).'">'.(max($pcat_id_arr)+1).'</option>';
				
				
				}
				}else{
				
				$get_sort_order_dropdown.='<option value="" selected></option>';
				if($edit_menu_sort_order_in_default!=0){
					$get_sort_order_dropdown.='<option value="0">0</option>';
				}
					$get_sort_order_dropdown.='<option value="1">1</option>';
				
				}
				}
				echo json_encode(array($get_edit_parent_category_dropdown,$get_sort_order_dropdown));
		
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function get_create_parent_category(){
		if($this->session->userdata("logged_in")){
			$menu_level=$this->input->post("menu_level");
			if($menu_level==1){
				?>
					<option value="0"></option>
				<?php
			}
			if($menu_level==2 || $menu_level==""){
				$get_all_parent_category_arr=$this->Model_catalogue->parent_category();
				?>
				<option value=""></option>
				<?php
				foreach($get_all_parent_category_arr as $parent_category_arr){
					?>
					<option value="<?php echo $parent_category_arr->pcat_id;?>"><?php echo $parent_category_arr->pcat_name;?></option>
					<?php
				}
				
			}
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function check_if_available_subcategories(){
		if($this->session->userdata("logged_in")){
			$cat_id=$this->input->post("cat_id");
			$flag=$this->Model_catalogue->check_if_subcategories_availablity($cat_id);
			if($flag==true){
				echo 1;
			}else{
				echo 0;
			}
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}

	
	public function check_filterbox_visibility(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("pcat_id");
			$flag=$this->Model_catalogue->check_filterbox_visibility($pcat_id);
			if($flag==true){
				echo 1;
			}else{
				echo 0;
			}
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function check_filterbox_visibility_cat(){
		if($this->session->userdata("logged_in")){
			$cat_id=$this->input->post("cat_id");
			
			$flag=$this->Model_catalogue->check_filterbox_visibility($cat_id);
			if($flag==true){
				echo 1;
			}else{
				echo 0;
			}
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function show_filter_sort_order(){
		if($this->session->userdata("logged_in")){
			
			$filterbox_id=$this->input->post("filterbox_id");
			$get_all_sub_category_arr=$this->Model_catalogue->get_all_filter_sort_order($filterbox_id);
				$pcat_id_arr=array();
				foreach($get_all_sub_category_arr as $all_sub_category_arr){
					$pcat_id_arr[]=$all_sub_category_arr["sort_order"];
				}
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				
				if(count($miss_arr)>0){
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<?php
				foreach($miss_arr as $k => $v){
					if($k==count($miss_arr)+1){
				?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
					else{
						?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
				}
				}
				else{
			
				?>
					<option value="" selected></option>
					<option value="0">0</option>
					<option value="<?php echo (max($pcat_id_arr)+1);?>"><?php echo (max($pcat_id_arr)+1);?></option>
				<?php
				
				}
				}else{
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<option value="1">1</option>
				<?php
				}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}

	public function show_specification_sort_order(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("pcat_id");
			$cat_id=$this->input->post("cat_id");
			$subcat_id=$this->input->post("subcat_id");
			$get_specification_pcat_arr=$this->Model_catalogue->sort_order_specification_pcat($pcat_id,$cat_id,$subcat_id);
			if($get_specification_pcat_arr!= ""){
				$pcat_id_arr=array();
				foreach($get_specification_pcat_arr as $specification_pcat_arr){
					$pcat_id_arr[]=$specification_pcat_arr["sort_order"];
				}
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				
				if(count($miss_arr)>0){
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<?php
				foreach($miss_arr as $k => $v){
					if($k==count($miss_arr)+1){
				?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
					else{
						?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
				}
				}
				else{
			
				?>
					<option value="" selected></option>
					<option value="0">0</option>
					<option value="<?php echo (max($pcat_id_arr)+1);?>"><?php echo (max($pcat_id_arr)+1);?></option>
				<?php
				
				}
				}
			}else{
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<option value="1">1</option>
				<?php
				}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}

	public function show_specifi_sort_order(){
		if($this->session->userdata("logged_in")){
			
			$specification_group_id=$this->input->post("specification_group_id");
			$get_all_sub_category_arr=$this->Model_catalogue->get_all_specification_sort_order($specification_group_id);
				$pcat_id_arr=array();
				foreach($get_all_sub_category_arr as $all_sub_category_arr){
					$pcat_id_arr[]=$all_sub_category_arr["sort_order"];
				}
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				
				if(count($miss_arr)>0){
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<?php
				foreach($miss_arr as $k => $v){
					if($k==count($miss_arr)+1){
				?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
					else{
						?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
				}
				}
				else{
			
				?>
					<option value="" selected></option>
					<option value="0">0</option>
					<option value="<?php echo (max($pcat_id_arr)+1);?>"><?php echo (max($pcat_id_arr)+1);?></option>
				<?php
				
				}
				}else{
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<option value="1">1</option>
				<?php
				}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}

	public function check_specification_group_visibility(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("pcat_id");
			$flag=$this->Model_catalogue->check_specification_group_visibility($pcat_id);
			if($flag==true){
				echo 1;
			}else{
				echo 0;
			}
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function check_specification_group_visibility_cat(){
		if($this->session->userdata("logged_in")){
			$cat_id=$this->input->post("cat_id");
			$flag=$this->Model_catalogue->check_specification_group_visibility_cat($cat_id);
			if($flag==true){
				echo 1;
			}else{
				echo 0;
			}
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function show_reason_cancel_sort_order(){
		if($this->session->userdata("logged_in")){
			
			$get_all_sub_category_arr=$this->Model_catalogue->get_all_reason_cancel_sort();
				$pcat_id_arr=array();
				foreach($get_all_sub_category_arr as $all_sub_category_arr){
					$pcat_id_arr[]=$all_sub_category_arr["sort_order"];
				}
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				
				if(count($miss_arr)>0){
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<?php
				foreach($miss_arr as $k => $v){
					if($k==count($miss_arr)+1){
				?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
					else{
						?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
				}
				}
				else{
			
				?>
					<option value="" selected></option>
					<option value="0">0</option>
					<option value="<?php echo (max($pcat_id_arr)+1);?>"><?php echo (max($pcat_id_arr)+1);?></option>
				<?php
				
				}
				}else{
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<option value="1">1</option>
				<?php
				}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function show_reason_cancel_edit_sort_order(){
		if($this->session->userdata("logged_in")){
			
			$edit_menu_sort_order=$this->input->post("edit_menu_sort_order");
			$reason_cancel_sort_order_arr=$this->Model_catalogue->show_reason_cancel_edit_sort_order($edit_menu_sort_order);
			
			?>
			
			<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
			<div class="form-group label-floating">
			<label class="control-label">-Select Options-</label>
			<select class="form-control" id="show_available_common_reason_cancel">
			<option selected value=""></option>
			<?php
			
			foreach($reason_cancel_sort_order_arr as $reason_cancel_order_arr){
				
				?>
				
					<option value="<?php echo $reason_cancel_order_arr["cancel_reason_id"];?>"><?php echo $reason_cancel_order_arr["reason_name"];?></option>
					<?php
			}
			?>
			
			</select>
			</div>
				<?php
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function show_admin_cancel_reason_sort_order(){
		if($this->session->userdata("logged_in")){
			
			$get_all_sub_category_arr=$this->Model_catalogue->get_all_admin_cancel_reason_sort();
				$pcat_id_arr=array();
				foreach($get_all_sub_category_arr as $all_sub_category_arr){
					$pcat_id_arr[]=$all_sub_category_arr["sort_order"];
				}
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				
				if(count($miss_arr)>0){
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<?php
				foreach($miss_arr as $k => $v){
					if($k==count($miss_arr)+1){
				?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
					else{
						?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
				}
				}
				else{
			
				?>
					<option value="" selected></option>
					<option value="0">0</option>
					<option value="<?php echo (max($pcat_id_arr)+1);?>"><?php echo (max($pcat_id_arr)+1);?></option>
				<?php
				
				}
				}else{
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<option value="1">1</option>
				<?php
				}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function show_admin_cancel_reason_edit_sort_order(){
		if($this->session->userdata("logged_in")){
			
			$edit_menu_sort_order=$this->input->post("edit_menu_sort_order");
			$reason_cancel_sort_order_arr=$this->Model_catalogue->show_admin_cancel_reason_edit_sort_order($edit_menu_sort_order);
			
			?>
			<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
			<div class="form-group label-floating">
			<label class="control-label">-Select Options-</label>
			<select class="form-control" id="show_available_common_admin_cancel_reason">
			<option selected value=""></option>
			<?php
			
			foreach($reason_cancel_sort_order_arr as $reason_cancel_order_arr){
				
				?>
				
					<option value="<?php echo $reason_cancel_order_arr["cancel_reason_id"];?>"><?php echo $reason_cancel_order_arr["cancel_reason"];?></option>
					<?php
			}
			?>
			
			</select>
			</div>
				<?php
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_customer_deactivate_sort_order(){
		if($this->session->userdata("logged_in")){
			
			$get_all_sub_category_arr=$this->Model_catalogue->get_all_customer_deactivate_sort();
				$pcat_id_arr=array();
				foreach($get_all_sub_category_arr as $all_sub_category_arr){
					$pcat_id_arr[]=$all_sub_category_arr["sort_order"];
				}
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				
				if(count($miss_arr)>0){
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<?php
				foreach($miss_arr as $k => $v){
					if($k==count($miss_arr)+1){
				?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
					else{
						?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
				}
				}
				else{
			
				?>
					<option value="" selected></option>
					<option value="0">0</option>
					<option value="<?php echo (max($pcat_id_arr)+1);?>"><?php echo (max($pcat_id_arr)+1);?></option>
				<?php
				
				}
				}else{
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<option value="1">1</option>
				<?php
				}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_customer_deactivate_edit_sort_order(){
		if($this->session->userdata("logged_in")){
			
			$edit_menu_sort_order=$this->input->post("edit_menu_sort_order");
			$reason_cancel_sort_order_arr=$this->Model_catalogue->show_customer_deactivate_edit_sort_order($edit_menu_sort_order);
			
			?>
			<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
			<div class="form-group label-floating">
			<label class="control-label">-Select Options-</label>
			<select class="form-control" id="show_available_common_customer_deactivate">
			<option selected value=""></option>
			<?php
			
			foreach($reason_cancel_sort_order_arr as $reason_cancel_order_arr){
				
				?>
				
					<option value="<?php echo $reason_cancel_order_arr["id"];?>"><?php echo $reason_cancel_order_arr["deactivate_reason"];?></option>
					<?php
			}
			?>
			
			</select>
			</div>
				<?php
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function show_reason_return_sort_order(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("parent_category");
			$cat_id=$this->input->post("categories");
			$subcat_id=$this->input->post("subcategories");
			$get_all_sub_category_arr=$this->Model_catalogue->get_all_reason_return_sort($pcat_id,$cat_id,$subcat_id);
				$pcat_id_arr=array();
				foreach($get_all_sub_category_arr as $all_sub_category_arr){
					$pcat_id_arr[]=$all_sub_category_arr["sort_order"];
				}
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				
				if(count($miss_arr)>0){
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<?php
				foreach($miss_arr as $k => $v){
					if($k==count($miss_arr)+1){
				?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
					else{
						?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
				}
				}
				else{
			
				?>
					<option value="" selected></option>
					<option value="0">0</option>
					<option value="<?php echo (max($pcat_id_arr)+1);?>"><?php echo (max($pcat_id_arr)+1);?></option>
				<?php
				
				}
				}else{
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<option value="1">1</option>
				<?php
				}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function show_reason_return_edit_sort_order(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("edit_parent_category");	
			$cat_id=$this->input->post("edit_categories");
			$subcat_id=$this->input->post("edit_subcategories");			
			$edit_menu_sort_order=$this->input->post("edit_menu_sort_order");
			$subcategory_sort_order_arr=$this->Model_catalogue->show_reason_return_edit_sort_order($edit_menu_sort_order,$pcat_id,$cat_id,$subcat_id);
			
			?>
			<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
			<div class="form-group label-floating">
			<label class="control-label">-Select Options-</label>
			<select class="form-control" id="show_available_common_reason_return">
			<option selected value=""></option>
			<?php
			
			foreach($subcategory_sort_order_arr as $subcategory_order_arr){
				
				?>
				
					<option value="<?php echo $subcategory_order_arr["reason_id"];?>"><?php echo $subcategory_order_arr["reason_name"];?></option>
					<?php
			}
			?>
			
			</select>
			</div>
				<?php
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function show_subreason_sort_order(){
		if($this->session->userdata("logged_in")){
			$reason_id=$this->input->post("reason_id");

			$get_all_sub_category_arr=$this->Model_catalogue->get_all_subreason_sort($reason_id);
				$pcat_id_arr=array();
				foreach($get_all_sub_category_arr as $all_sub_category_arr){
					$pcat_id_arr[]=$all_sub_category_arr["sort_order"];
				}
				if(count($pcat_id_arr) > 0){
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($pcat_id_arr);$i++){
					$base_arr[]=$i;
				}
				$miss_arr=array_diff($base_arr,$pcat_id_arr);
				$max_val_in_base_arr = max($base_arr);
				if(in_array($max_val_in_base_arr,$pcat_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				
				if(count($miss_arr)>0){
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<?php
				foreach($miss_arr as $k => $v){
					if($k==count($miss_arr)+1){
				?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
					else{
						?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
				}
				}
				else{
			
				?>
					<option value="" selected></option>
					<option value="0">0</option>
					<option value="<?php echo (max($pcat_id_arr)+1);?>"><?php echo (max($pcat_id_arr)+1);?></option>
				<?php
				
				}
				}else{
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<option value="1">1</option>
				<?php
				}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function show_subreason_edit_sort_order(){
		if($this->session->userdata("logged_in")){
			$reason_id=$this->input->post("reason_id");	
			$edit_menu_sort_order=$this->input->post("edit_menu_sort_order");
			$subcategory_sort_order_arr=$this->Model_catalogue->show_subreason_edit_sort_order($edit_menu_sort_order,$reason_id);
			
			?>
			<script type="text/javascript" src="<?php echo base_url();?>assets/js/material-bootstrap-wizard.js"></script>
			<div class="form-group label-floating">
			<label class="control-label">-Select Options-</label>
			<select class="form-control" id="show_available_common_subreason">
			<option selected value=""></option>
			<?php
			
			foreach($subcategory_sort_order_arr as $subcategory_order_arr){
				
				?>
				
					<option value="<?php echo $subcategory_order_arr["sub_issue_id"];?>"><?php echo $subcategory_order_arr["sub_issue_name"];?></option>
					<?php
			}
			?>
			
			</select>
			</div>
				<?php
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	
	
	public function attrib_expansion_form1(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$attribute_id=$this->input->post("attribute_id");
			$get_attribute_info_arr=$this->Model_catalogue->get_attribute_info($attribute_id);
			$data["attribute_id"]=$attribute_id;
			$data["get_attribute_info_arr"]=$get_attribute_info_arr;
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/attrib_expansion_form1',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function get_attribute_value(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$attribute_id=$this->input->post("attribute_id");
			$attribute_name=$this->input->post("attribute_name");
			$attribute_value=$this->Model_catalogue->get_attribute_value($attribute_id,$attribute_name);
			echo $attribute_value;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function attrib_expansion_form1_action(){	
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$attribute_id=$this->input->post("attribute_id");
			$attribute_name=$this->input->post("attribute_name");
			$attribute_value=$this->input->post("attribute_value");
			$attribute_expansion_name=$this->input->post("attribute_expansion_name");
			$attribute_expansion_value=$this->input->post("attribute_expansion_value");
			$attribute_sub_options=$this->input->post("attribute_sub_options");
			$flag=$this->Model_catalogue->attrib_expansion_form1_action($attribute_id,$attribute_name,$attribute_value,$attribute_expansion_name,$attribute_expansion_value,$attribute_sub_options);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function attrib_expansion_form2(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$attribute_id=$this->input->post("attribute_id");
			$get_attribute_sub_options_arr=$this->Model_catalogue->get_attribute_sub_options($attribute_id);
			$get_attribute_sub_options_comma=$get_attribute_sub_options_arr["attribute_sub_options"];
			$data["attribute_id"]=$attribute_id;
			$data["attribute_expansions_id"]=$get_attribute_sub_options_arr["attribute_expansions_id"];
			$data["get_attribute_sub_options_comma"]=$get_attribute_sub_options_comma;
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/attrib_expansion_form2',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function attrib_expansion_form2_action(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$attribute_id=$this->input->post("attribute_id");
			$attribute_expansions_id=$this->input->post("attribute_expansions_id");
			$attribute_sub_options=$this->input->post("attribute_sub_options");
			$flag=$this->Model_catalogue->attrib_expansion_form2_action($attribute_id,$attribute_expansions_id,$attribute_sub_options);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function attrib_expansions(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["menu_flag"]="catalog_active_links";
			
			$attribute_id=$this->input->post("attribute_id");
			$data["attribute_id"]=$attribute_id;
			$data["get_attrib_expansions_count"]=$this->Model_catalogue->get_attrib_expansions_count($attribute_id);
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/attribute_expansions',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function attribute_expansion_processing(){
		
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->attribute_expansion_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->attribute_expansion_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				 $row[]=$queryRecordsObj -> attribute_value;
				 $row[]=$queryRecordsObj -> attribute_expansion_name;
				 $row[]=$queryRecordsObj -> attribute_expansion_value;
				 $row[]=$queryRecordsObj -> attribute_sub_options;
				 $row[]=date("D j M, Y",strtotime($queryRecordsObj -> timestamp));
				$row[]='<form   action="'.base_url().'admin/Catalogue/edit_attrib_expansion_form1" method="post"><input type="hidden" value="'.$queryRecordsObj -> attribute_id.'" name="attribute_id"><input type="submit" value="View / Edit" name="attrib_expansion" class="btn btn-warning btn-xs btn-block"></form><input type="button" value="Delete" name="delete_attrib_expansion" class="btn btn-danger btn-xs btn-block" onclick="delete_attrib_expansionsFun()">';
				
				
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		
		
	}
	
	public function edit_attrib_expansion_form1(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$attribute_id=$this->input->post("attribute_id");
			$data["attribute_id"]=$attribute_id;
			$data["get_attribute_info_arr"]=$this->Model_catalogue->get_attribute_info($attribute_id);
			$data["get_attribute_expansion_info_arr"]=$this->Model_catalogue->get_attribute_expansion_info($attribute_id);
			$data["menu_flag"]="catalog_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_attrib_expansion_form1',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function edit_attrib_expansion_form1_action(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$attribute_id=$this->input->post("attribute_id");
			$attribute_name=$this->input->post("attribute_name");
			$attribute_value=$this->input->post("attribute_value");
			$attribute_expansion_name=$this->input->post("attribute_expansion_name");
			$attribute_expansion_value=$this->input->post("attribute_expansion_value");
			$attribute_sub_options=$this->input->post("attribute_sub_options");
			$flag=$this->Model_catalogue->edit_attrib_expansion_form1_action($attribute_id,$attribute_name,$attribute_value,$attribute_expansion_name,$attribute_expansion_value,$attribute_sub_options);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function edit_attrib_expansion_form2(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["menu_flag"]="catalog_active_links";
			$attribute_id=$this->input->post("attribute_id");
			$get_attribute_sub_options_arr=$this->Model_catalogue->get_attribute_sub_options($attribute_id);
			$get_attribute_sub_options_comma=$get_attribute_sub_options_arr["attribute_sub_options"];
			$data["attribute_id"]=$attribute_id;
			$data["attribute_expansions_id"]=$get_attribute_sub_options_arr["attribute_expansions_id"];
			$data["get_attribute_sub_options_comma"]=$get_attribute_sub_options_comma;
			
			$data["get_attribute_expansions_sub_options_arr"]=$this->Model_catalogue->attribute_expansions_sub_options($get_attribute_sub_options_arr["attribute_expansions_id"]);
			
			
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_attrib_expansion_form2',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function edit_attrib_expansion_form2_action(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$attribute_id=$this->input->post("attribute_id");
			$attribute_expansions_id=$this->input->post("attribute_expansions_id");
			$attribute_sub_options=$this->input->post("attribute_sub_options");
			$flag=$this->Model_catalogue->edit_attrib_expansion_form2_action($attribute_id,$attribute_expansions_id,$attribute_sub_options);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function delete_attrib_expansions(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$attribute_id=$this->input->post("attribute_id");
			
			$flag=$this->Model_catalogue->delete_attrib_expansions($attribute_id);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function test(){
		$email_stuff_arr=email_stuff();
			$pdf = new FPDF();

			$pdf->AddPage();

			$pdf->SetFont('Arial','B',16);
			$pdf->Ln();
			

			$pdf->Ln();
			$pdf->Cell(160,6,'Manifest',0,0,'C',0); 
			$pdf->Ln();
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(60,6,' ',0,0,'C',0); 
			$pdf->Cell(100,6,'Total products to dispatch:'."1111111",0,0,'R',0); 
			$pdf->Ln();
			$pdf->Cell(160,6,'Sold By:'.$email_stuff_arr["name_emailtemplate"],0,0,'L',0); 
			$pdf->Ln();
			$pdf->SetFont('Arial','I',9);
			$pdf->Cell(160,6,"Address:Plot no 139, 2nd 'D' Cross Road, Domlur IInd Stage, Bangalore, Karnataka, India - 560100",'B',0,'L',0); 
			$pdf->Ln();
			
			
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(20,6,'SNo',0,0,'L',0); 
			$pdf->Cell(60,6,'Tracking Id',0,0,'L',0); 
			$pdf->Cell(40,6,'Order Item Id',0,0,'L',0);
			$pdf->Cell(40,6,'Order Id',0,0,'L',0);
			$pdf->Ln();
			
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(20,6,'1',0,0,'L',0); 
			$pdf->Cell(60,6,'xxxxxxxx',0,0,'L',0); 
			$pdf->Cell(40,6,'1',0,0,'L',0);
			$pdf->Cell(40,6,'abcd',0,0,'L',0);
			$pdf->Ln();
			
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(160,3,'','B',0,'L',0); 
			$pdf->Ln();
			$filename="assets/getttttt.pdf";
			$pdf->Output($filename,'F');
	}
	public function get_brand_name($brand_id){
		$brand_name=$this->Model_catalogue->get_brand_name($brand_id);
		return $brand_name;
	}
	
	public function logistics_territory_states_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['vendor_id']=$this->input->post("vendors");
			$data['logistics_id']=$this->input->post("logistics");
			$data['vendors'] = $this->Model_catalogue->vendors();	
			$data['logistics_all_pincodes']= $this->Model_catalogue->logistics_all_pincodes();
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_territory_states_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function logistics_territory_states(){
		
		if($this->session->userdata("logged_in")){
			$data['vendor_id']=$this->input->post("vendor_id");
			$data['logistics_id']=$this->input->post("logistics_id");
			$data['territory_name_id']=$this->input->post("territory_name_id");
			$data['vendors'] = $this->Model_catalogue->vendors();	
			$data['logistics_service'] = $this->Model_catalogue->logistics_service();
			$data['logistics'] = $this->Model_catalogue->logistics();
			
			$data['create_editview']=$this->input->post("create_editview");
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_territory_states_view',$data);	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function show_available_logistics_territories(){
		if($this->session->userdata("logged_in")){
			$logistics_id=$this->input->post("logistics_id");
			$result_logi=$this->Model_catalogue->show_available_logistics_territories($logistics_id);
			if(count($result_logi)!=0){
				$str='';
				$str.='<option value=""></option>';			
				foreach($result_logi as $res){				
					$str.='<option value="'.$res->state_id.'">'.$res->state_name.' - '.$res->circle_id.'</option>';						
				}
				echo $str;
			}
			else{
				echo count($result_logi);
			}
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function show_available_logistics_territories_for_delivery_modes(){
		if($this->session->userdata("logged_in")){
			$logistics_id=$this->input->post("logistics_id");
			$result_logi=$this->Model_catalogue->show_available_logistics_territories_for_delivery_modes($logistics_id);
			if(count($result_logi)!=0){
				$str='';
				/*$str.='<option value=""></option>';			
				foreach($result_logi as $res){				
					$str.='<option value="'.$res->territory_name_id.'">'.$res->territory_name.'</option>';			
				}
				echo $str;*/
				$str.='<table class="table"><caption>Speed Duration(Days)</caption>';
				foreach($result_logi as $res){
					$str.='<tr>';
					$str.='<td>';
						$str.=ucwords($res->territory_name);
					$str.='</td>';
					$str.='<td>';
						$str.='<input onkeypress="return isNumber(event);"  required type="text" name="speed_territory_duration['.$res->territory_name_id.']" class="form-control">';
					$str.='</td>';
					$str.='</tr>';
				}
				$str.='</table>';
				echo $str;
			}
			else{
				echo count($result_logi);
			}
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function show_available_logistics_territories_states_cities(){
		if($this->session->userdata("logged_in")){
			$logistics_id=$this->input->post("logistics_id");
			$result_logi=$this->Model_catalogue->show_available_logistics_territories_states_cities($logistics_id);
			if(count($result_logi)!=0){
				$str='';
				$str.='<option value=""></option>';			
				foreach($result_logi as $res){				
					$str.='<option value="'.$res->territory_name_id.'">'.$res->territory_name.'</option>';			
				}
				echo $str;
			}
			else{
				echo count($result_logi);
			}
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function show_available_logistics_territories_all(){
		if($this->session->userdata("logged_in")){
			$logistics_id=$this->input->post("logistics_id");
			$result_logi=$this->Model_catalogue->show_available_logistics_territories_all($logistics_id);
			if(count($result_logi)!=0){
				$str='';
				$str.='<option value=""></option>';			
				foreach($result_logi as $res){				
					$str.='<option value="'.$res->territory_name_id.'">'.ucwords($res->territory_name).'</option>';			
				}
				echo $str;
			}
			else{
				echo count($result_logi);
			}
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function show_available_logistics_delivery_modes(){
		if($this->session->userdata("logged_in")){
			$logistics_id=$this->input->post("logistics_id");
			$result_logi=$this->Model_catalogue->show_available_logistics_delivery_modes($logistics_id);
			if(count($result_logi)!=0){
				$str='';
				$str.='<option value=""></option>';			
				foreach($result_logi as $res){				
					$str.='<option value="'.$res->logistics_delivery_mode_id.'">'.ucwords($res->delivery_mode).'</option>';			
				}
				echo $str;
			}
			else{
				echo count($result_logi);
			}
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function show_available_logistics_parcel_categories(){
		if($this->session->userdata("logged_in")){
			$logistics_id=$this->input->post("logistics_id");
			$result_logi=$this->Model_catalogue->show_available_logistics_parcel_categories($logistics_id);
			if(count($result_logi)!=0){
				$str='';
				$str.='<option value=""></option>';			
				foreach($result_logi as $res){				
					$str.='<option value="'.$res->logistics_parcel_category_id.'">'.ucwords($res->parcel_type).'</option>';			
				}
				echo $str;
			}
			else{
				echo count($result_logi);
			}
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function check_make_as_local_territory(){
		if($this->session->userdata("logged_in")){
			$logistics_id=$this->input->post("logistics_id");
			$territory_name_id=$this->input->post("territory_name_id");
			$check_make_as_local_territory_arr=$this->Model_catalogue->check_make_as_local_territory($logistics_id,$territory_name_id);
			if(!empty($check_make_as_local_territory_arr)){
				echo "yes";
			}
			else{
				echo "no";
			}
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function show_available_logistics_territories_states(){
		if($this->session->userdata("logged_in")){
			$logistics_id=$this->input->post("logistics_id");
			$territory_name_id=$this->input->post("territory_name_id");
			$result_logi=$this->Model_catalogue->show_available_logistics_territories_states($logistics_id,$territory_name_id);
			if(count($result_logi)!=0){
				$str='';
				foreach($result_logi as $res){				
					$str.='<option value="'.$res->state_id.'">'.$res->state_name.'</option>';			
				}
				echo $str;
			}
			else{
				echo count($result_logi);
			}
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function logistics_pincodes_by_state_ids(){
		if($this->session->userdata("logged_in")){
			$to_select_list_states_in=$this->input->post("to_select_list_states_in");
			$result_logi=$this->Model_catalogue->logistics_pincodes_by_state_ids($to_select_list_states_in);
			if(count($result_logi)!=0){
				$str='';
				foreach($result_logi as $res){				
					$str.='<option value="'.$res->pin.'">'.$res->city." - ".$res->pin.'</option>';			
				}
				echo $str;
			}
			else{
				echo count($result_logi);
			}
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function add_logistics_territory_states() {
		
		if($this->session->userdata("logged_in")){
			$logistics_id=$this->db->escape_str($this -> input -> post("logistics"));
			$territory_name_id=$this->db->escape_str($this -> input -> post("logistics_territories"));
			$to_select_list_states=$this->db->escape_str($this -> input -> post("to_select_list"));
			
			$check_logistics_territory_arr=$this->Model_catalogue->check_logistics_territory($logistics_id,$territory_name_id);
			if(!empty($check_logistics_territory_arr)){
				echo "exists";
				exit;
			}
			
			$flag=$this->Model_catalogue->add_logistics_territory_states($logistics_id,$territory_name_id,$to_select_list_states);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function logistics_territory_states_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->logistics_territory_states_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->logistics_territory_states_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->logistics_territory_state_id.'">';
				$get_vendors_data_arr=$this->Model_catalogue->get_vendors_data($queryRecordsObj->vendor_id);
				$get_logistics_all_territory_name_data_arr=$this->Model_catalogue->get_logistics_all_territory_name_data($queryRecordsObj->territory_name_id);
				$row[]=$get_vendors_data_arr["name"];
				$row[]=$queryRecordsObj->logistics_name;
				//$row[]=$get_logistics_all_territory_name_data_arr["territory_name"];

				$state_id_list=$queryRecordsObj->state_id_list;
				$state_str='';
				if($state_id_list!=''){
					$state_id_list_arr=explode('|',$state_id_list);
					if(!empty($state_id_list_arr)){
						$state_id_list=implode(',',$state_id_list_arr);
						$state_obj=$this->Model_catalogue->get_state_list($state_id_list);
						if(!empty($state_obj)){
							foreach($state_obj as $st){
								$state_str.="".$st->state_name.' - '.$st->circle_id.'<br>';
							}
						}
					}

				}

				$row[]=$get_logistics_all_territory_name_data_arr["territory_name"].'<br> <h4>States</h4>'.$state_str;

				$row[]=$queryRecordsObj -> timestamp;
				
				$row[]='<form action="'.base_url().'admin/Catalogue/edit_logistics_territory_states_form" method="post">	<input type="hidden" value="'.$queryRecordsObj -> logistics_territory_state_id.'" name="logistics_territory_state_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';

				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function edit_logistics_territory_states_form(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$logistics_territory_state_id=$this->input->post("logistics_territory_state_id");
			$data["logistics_territory_state_id"]=$logistics_territory_state_id;
			$data['get_logistics_territory_states'] = $this->Model_catalogue->get_logistics_territory_states($logistics_territory_state_id);
			$data["logistics_all_statesObj"]=$this->Model_catalogue->logistics_all_states_except_some_territory_states($logistics_territory_state_id);
			$data["get_statelistObj"]=$this->Model_catalogue->get_statelist($data['get_logistics_territory_states']["state_id_list"]);
			
			$get_logistics_all_territory_name_data_arr=$this->Model_catalogue->get_logistics_all_territory_name_data($data['get_logistics_territory_states']["territory_name_id"]);
			$data["territory_name"]=$get_logistics_all_territory_name_data_arr["territory_name"];
			
			$data["get_statelistObj"]=$this->Model_catalogue->get_statelist($data['get_logistics_territory_states']["state_id_list"]);
			
			
			$data['vendors'] = $this->Model_catalogue->vendors();
			$data['create_editview']=$this->input->post("create_editview");
			
			$data["make_as_local_territory"]=$this->check_make_as_local_or_not_by_logistics_territory_state_id($logistics_territory_state_id);
			
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_logistics_territory_states_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_statelist($statelist){
		$get_statelistObj = $this->Model_catalogue->get_statelist($statelist);
		return $get_statelistObj;
	}
	public function check_make_as_local_or_not_by_logistics_territory_state_id($logistics_territory_state_id){
		$get_logistics_territory_states_arr=$this->Model_catalogue->get_logistics_territory_states($logistics_territory_state_id);
		$logistics_id=$get_logistics_territory_states_arr["logistics_id"];
		$territory_name_id=$get_logistics_territory_states_arr["territory_name_id"];
		$get_logistics_all_territory_name_data_arr=$this->get_logistics_all_territory_name_data($territory_name_id);
		if($get_logistics_all_territory_name_data_arr["make_as_local"]=='1'){
			return "yes";
		}
		else{
			return "no";
		}
	}
	public function edit_logistics_territory_states() {
		
		if($this->session->userdata("logged_in")){
			$logistics_territory_state_id=$this->db->escape_str($this -> input -> post("edit_logistics_territory_state_id"));
			$to_select_list_states=$this->db->escape_str($this -> input -> post("to_select_list"));
			
			$flag=$this->Model_catalogue->edit_logistics_territory_states($logistics_territory_state_id,$to_select_list_states);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function delete_logistics_territory_states_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_logistics_territory_states_selected($selected_list);	
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
			
	}
	
	
	public function delete_logistics_territory_states_cities_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_catalogue-> delete_logistics_territory_states_cities_selected($selected_list);	
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
			
	}
	
	public function logistics_territory_states_cities_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['vendor_id']=$this->input->post("vendors");
			$data['logistics_id']=$this->input->post("logistics");
			$data['vendors'] = $this->Model_catalogue->vendors();	
			$data['logistics_all_pincodes']= $this->Model_catalogue->logistics_all_pincodes();
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_territory_states_cities_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function logistics_territory_states_cities(){
		
		if($this->session->userdata("logged_in")){
			$data['vendor_id']=$this->input->post("vendor_id");
			$data['logistics_id']=$this->input->post("logistics_id");
			$data['territory_name_id']=$this->input->post("territory_name_id");
			$data['state_id']=$this->input->post("state_id");
			$data['vendors'] = $this->Model_catalogue->vendors();	
			
			$data['create_editview']=$this->input->post("create_editview");
			$data["menu_flag"]="shipping_active_links";

			/* add divisions */

			$div_obj=$this->Model_catalogue->get_all_divisions($data['state_id'],$data['territory_name_id']); // from logistics_all_pincodes

			//echo '<pre>';

			$divisions=array();

			if(!empty($div_obj)){
				foreach($div_obj as $div){
					$json_arr=json_decode($div->imp_info_json);
					if(!empty($json_arr)){
						//print_r($json_arr);
						
						if(!empty($json_arr)){
							foreach($json_arr as $js){
								//print_r($js);
								$Division_ID=$js->Division_ID;
								$Division_Name=$js->Division_Name;
								$temp['Division_ID']=$Division_ID;
								$temp['Division_Name']=$Division_Name;

								$key = array_search($Division_ID, array_column($divisions, 'Division_ID'));
								//echo 'ddd'.$key.'dd';
								if($key !== false){
									
								}else{
									$divisions[]=$temp;
								}
							}
							
						}
					}

				}
			}

			//print_r($divisions);
			//exit;

			$data["all_divisions"]=$divisions;
			/* add divisions */

			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_territory_states_cities_view',$data);	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function show_available_logistics_territories_states_by_territory_all(){
		if($this->session->userdata("logged_in")){
			$territory_name_id=$this->input->post("territory_name_id");
			$logistics_id=$this->input->post("logistics_id");
			
		
			$show_available_logistics_territories_states_by_territory_arr=$this->Model_catalogue->show_available_logistics_territories_states_by_territory_all($territory_name_id,$logistics_id);
			$state_id_list_arr=array();
			$str="";
			if(!empty($show_available_logistics_territories_states_by_territory_arr)){
				$state_id_list=$show_available_logistics_territories_states_by_territory_arr["state_id_list"];
				$get_statelistObj=$this->Model_catalogue->get_statelist($state_id_list);
				$str.='<option value=""></option>';
				foreach($get_statelistObj as $obj){
					$str.='<option value="'.$obj->state_id.'">'.$obj->state_name.'</option>';
				}
				echo $str;
			}
			else{
				echo "0";
			}
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function show_available_logistics_territories_states_by_territory(){
		if($this->session->userdata("logged_in")){
			$territory_name_id=$this->input->post("territory_name_id");
			$logistics_id=$this->input->post("logistics_id");
			
			$state_id_logistics_territory_states_cities_table_arr=array();
			$get_logistics_territory_states_cities_by_logistics_id_territory_name_id_arr=$this->Model_catalogue->get_logistics_territory_states_cities_by_logistics_id_territory_name_id($territory_name_id,$logistics_id);
			foreach($get_logistics_territory_states_cities_by_logistics_id_territory_name_id_arr as $arr){
				$state_id_logistics_territory_states_cities_table_arr[]=$arr["state_id"];
			}
		
		
			$show_available_logistics_territories_states_by_territory_arr=$this->Model_catalogue->show_available_logistics_territories_states_by_territory($territory_name_id,$logistics_id);
			$state_id_list_arr=array();
			$str="";
			if(!empty($show_available_logistics_territories_states_by_territory_arr)){
				$state_id_list=$show_available_logistics_territories_states_by_territory_arr["state_id_list"];
				$get_statelistObj=$this->Model_catalogue->get_statelist($state_id_list);
				$str.='<option value=""></option>';
				foreach($get_statelistObj as $obj){
					if(!in_array($obj->state_id,$state_id_logistics_territory_states_cities_table_arr)){
						//$str.='<option value="'.$obj->state_id.'">'.$obj->state_name.'</option>';
						$str.='<option value="'.$obj->state_id.'">'.$obj->state_name.' - '.$obj->circle_id.'</option>';
					}
				}
				echo $str;
			}
			else{
				echo "0";
			}
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}

	public function show_available_logistics_territories_states_divisions_by_state(){
		if($this->session->userdata("logged_in")){
			$state_id=$this->input->post("state_id");
			$logistics_id=$this->input->post("logistics_id");
			$territory_name_id=$this->input->post("territory_name_id");
			
			/* add divisions */

			$div_obj=$this->Model_catalogue->get_all_divisions($state_id,$territory_name_id); // from logistics_all_pincodes

			//echo '<pre>';

			$divisions=array();

			if(!empty($div_obj)){
				foreach($div_obj as $div){
					$json_arr=json_decode($div->imp_info_json);
					if(!empty($json_arr)){
						//print_r($json_arr);
						
						if(!empty($json_arr)){
							foreach($json_arr as $js){
								//print_r($js);
								$Division_ID=$js->Division_ID;
								$Division_Name=$js->Division_Name;
								$temp['Division_ID']=$Division_ID;
								$temp['Division_Name']=$Division_Name;

								$key = array_search($Division_ID, array_column($divisions, 'Division_ID'));
								//echo 'ddd'.$key.'dd';
								if($key !== false){
									
								}else{
									$divisions[]=$temp;
								}
							}
							
						}
					}

				}
			}

			//print_r($divisions);
			//exit;
	
			$str="";
			if(!empty($divisions)){
				foreach($divisions as $ar){
					$str.='<option value="'.$ar["Division_ID"].'">'.$ar["Division_Name"]." - ".$ar["Division_ID"].'</option>';
				}
				echo $str;
			}
			else{
				echo "0";
			}
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_logistics_territories_states_cities_by_state(){
		if($this->session->userdata("logged_in")){
			$state_id=$this->input->post("state_id");
			$logistics_id=$this->input->post("logistics_id");
			$territory_name_id=$this->input->post("territory_name_id");
			
			$show_available_logistics_territories_states_cities_by_state_arr=$this->Model_catalogue->show_available_logistics_territories_states_cities_by_state($state_id,$logistics_id,$territory_name_id);		
			$str="";
			if(!empty($show_available_logistics_territories_states_cities_by_state_arr)){
				foreach($show_available_logistics_territories_states_cities_by_state_arr as $arr){
					$str.='<option value="'.$arr["pincode_id"].'">'.$arr["city"]." - ".$arr["pin"].'</option>';
				}
				echo $str;
			}
			else{
				echo "0";
			}
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function add_logistics_territory_states_cities() {
		
		if($this->session->userdata("logged_in")){
			$logistics_id=$this->db->escape_str($this -> input -> post("logistics"));
			$territory_name_id=$this->db->escape_str($this -> input -> post("logistics_territories"));
			$state_id=$this->db->escape_str($this -> input -> post("logistics_states"));
			$to_select_list_cities=$this->db->escape_str($this -> input -> post("to_select_list"));
			
			$check_logistics_territory_state_arr=$this->Model_catalogue->check_logistics_territory_state($logistics_id,$territory_name_id,$state_id);
			if(!empty($check_logistics_territory_state_arr)){
				echo "exists";
				exit;
			}
			
			$flag=$this->Model_catalogue->add_logistics_territory_states_cities($logistics_id,$territory_name_id,$state_id,$to_select_list_cities);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function logistics_territory_states_cities_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->logistics_territory_states_cities_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->logistics_territory_states_cities_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->logistics_territory_state_city_id.'">';
				
				$get_vendors_data_arr=$this->Model_catalogue->get_vendors_data($queryRecordsObj->vendor_id);
				$get_logistics_all_territory_name_data_arr=$this->Model_catalogue->get_logistics_all_territory_name_data($queryRecordsObj->territory_name_id);
				$get_state_name_arr=$this->Model_catalogue->get_state_name($queryRecordsObj->state_id);
				
				$row[]=$get_vendors_data_arr["name"];
				$row[]=$queryRecordsObj->logistics_name;
				$row[]=$get_logistics_all_territory_name_data_arr["territory_name"];
				$row[]=$get_state_name_arr["state_name"].'<br><h4>Divisions</h4>'.
				str_replace("|"," ",$queryRecordsObj->division_id_list);
				$row[]=$queryRecordsObj -> timestamp;
				
				$row[]='<form action="'.base_url().'admin/Catalogue/edit_logistics_territory_states_cities_form" method="post">	<input type="hidden" value="'.$queryRecordsObj -> logistics_territory_state_city_id.'" name="logistics_territory_state_city_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';

				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function edit_logistics_territory_states_cities_form(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$logistics_territory_state_city_id=$this->input->post("logistics_territory_state_city_id");
			$data["logistics_territory_state_city_id"]=$logistics_territory_state_city_id;
			$data['get_logistics_territory_states_cities'] = $this->Model_catalogue->get_logistics_territory_states_cities($logistics_territory_state_city_id);
			
			//$data["logistics_cities_by_statesObj"]=$this->Model_catalogue->logistics_cities_by_state_id_except_some_territory_cities($data['get_logistics_territory_states_cities']["state_id"],$logistics_territory_state_city_id);
			
			$state_id=$data['get_logistics_territory_states_cities']["state_id"];

			$data["logistics_cities_by_statesObj"]=$this->Model_catalogue->logistics_cities_by_state_id_except_some_territory_cities($state_id,$logistics_territory_state_city_id);

			$data["get_citylistObj"]=$this->Model_catalogue->get_citylist($data['get_logistics_territory_states_cities']["city_id_list"]);

			$territory_name_id=$data['get_logistics_territory_states_cities']["territory_name_id"];
			$get_logistics_all_territory_name_data_arr=$this->Model_catalogue->get_logistics_all_territory_name_data($territory_name_id);

			$data["territory_name"]=$get_logistics_all_territory_name_data_arr["territory_name"];
			
			$get_state_name_arr=$this->Model_catalogue->get_state_name($data['get_logistics_territory_states_cities']["state_id"]);
			$data["state_name"]=$get_state_name_arr["state_name"];
			
			$data['vendors'] = $this->Model_catalogue->vendors();


			$data["menu_flag"]="shipping_active_links";
			/* add divisions */

			$div_obj=$this->Model_catalogue->get_all_divisions($state_id,$territory_name_id); // from logistics_all_pincodes

			//echo '<pre>';

			$divisions=array();

			if(!empty($div_obj)){
				foreach($div_obj as $div){
					$json_arr=json_decode($div->imp_info_json);
					if(!empty($json_arr)){
						//print_r($json_arr);
						
						if(!empty($json_arr)){
							foreach($json_arr as $js){
								//print_r($js);
								$Division_ID=$js->Division_ID;
								$Division_Name=$js->Division_Name;
								$temp['Division_ID']=$Division_ID;
								$temp['Division_Name']=$Division_Name;

								$key = array_search($Division_ID, array_column($divisions, 'Division_ID'));
								//echo 'ddd'.$key.'dd';
								if($key !== false){
									
								}else{
									$divisions[]=$temp;
								}
							}
							
						}
					}

				}
			}

			//print_r($divisions);
			//exit;

			$data["all_divisions"]=$divisions;
			/* add divisions */

			$data['create_editview']=$this->input->post("create_editview");
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_logistics_territory_states_cities_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function edit_logistics_territory_states_cities() {
		
		if($this->session->userdata("logged_in")){
			$edit_logistics_territory_state_city_id=$this->db->escape_str($this -> input -> post("edit_logistics_territory_state_city_id"));
			$to_select_list_cities=$this->db->escape_str($this -> input -> post("to_select_list"));
			$select_list_division=$this -> input -> post("select_list_division");
		
			$flag=$this->Model_catalogue->edit_logistics_territory_states_cities($edit_logistics_territory_state_city_id,$to_select_list_cities,$select_list_division);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function logistics_territory_states_cities_pincodes_filter(){
		
		if($this->session->userdata("logged_in")){
			$data['vendor_id']=$this->input->post("vendors");
			$data['logistics_id']=$this->input->post("logistics");
			$data['vendors'] = $this->Model_catalogue->vendors();	
			$data['logistics_all_pincodes']= $this->Model_catalogue->logistics_all_pincodes();
			$data['logistics'] = $this->Model_catalogue->logistics();
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_territory_states_cities_pincodes_filter_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function show_available_logistics_territories_states_cities_by_state_id(){
		if($this->session->userdata("logged_in")){
			$territory_name_id=$this->input->post("territory_name_id");
			$logistics_id=$this->input->post("logistics_id");
			$state_id=$this->input->post("state_id");
			
			$show_available_logistics_territories_states_cities_by_state_id_arr=$this->Model_catalogue->show_available_logistics_territories_states_cities_by_state_id($territory_name_id,$logistics_id,$state_id);
			$state_id_list_arr=array();
			$str="";
			if(!empty($show_available_logistics_territories_states_cities_by_state_id_arr)){
				$city_id_list=$show_available_logistics_territories_states_cities_by_state_id_arr["city_id_list"];
				$get_citylistObj=$this->Model_catalogue->get_citylist($city_id_list);
				$str.='<option value=""></option>';
				foreach($get_citylistObj as $obj){
					$str.='<option value="'.$obj->pincode_id.'">'.$obj->city.' - '.$obj->pin.'</option>';
				}
				echo $str;
			}
			else{
				echo "0";
			}
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function logistics_territory_states_cities_pincodes(){
		
		if($this->session->userdata("logged_in")){
			$data['vendor_id']=$this->input->post("vendor_id");
			$data['logistics_id']=$this->input->post("logistics_id");
			$data['territory_name_id']=$this->input->post("territory_name_id");
			$data['state_id']=$this->input->post("state_id");
			$data['city_pin']=$this->input->post("city_pin");
			$data['vendors'] = $this->Model_catalogue->vendors();	
			
			$data['create_editview']=$this->input->post("create_editview");
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/logistics_territory_states_cities_pincodes_view',$data);	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function logistics_territory_states_cities_pincodes_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->logistics_territory_states_cities_pincodes_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->logistics_territory_states_cities_pincodes_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->logistics_service_id.'">';
				
				$get_vendors_data_arr=$this->Model_catalogue->get_vendors_data($queryRecordsObj->vendor_id);
				$get_logistics_all_territory_name_data_arr=$this->Model_catalogue->get_logistics_all_territory_name_data($queryRecordsObj->territory_name_id);
				$get_state_name_arr=$this->Model_catalogue->get_state_name($queryRecordsObj->state_id);
				
				$row[]=$get_vendors_data_arr["name"];
				$row[]=$queryRecordsObj->logistics_name;
				$row[]=$get_logistics_all_territory_name_data_arr["territory_name"];
				$row[]=$get_state_name_arr["state_name"];
				$row[]=$queryRecordsObj -> timestamp;
				
				$row[]='<form action="'.base_url().'admin/Catalogue/edit_logistics_territory_states_cities_pincodes_form" method="post">	<input type="hidden" value="'.$queryRecordsObj -> logistics_service_id.'" name="logistics_service_id"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';

				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function edit_logistics_territory_states_cities_pincodes_form(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$logistics_service_id=$this->input->post("logistics_service_id");
			$data["logistics_service_id"]=$logistics_service_id;
			$data['get_logistics_territory_states_cities_pincodes'] = $this->Model_catalogue->get_logistics_territory_states_cities_pincodes($logistics_service_id);
			$data["logistics_cities_pincodes_by_pinObj"]=$this->Model_catalogue->logistics_cities_pincodes_by_city_pin($data['get_logistics_territory_states_cities_pincodes']["pin"]);
			$data["get_citylistObj"]=$this->Model_catalogue->get_pincodelist($logistics_service_id);
			
			$get_logistics_all_territory_name_data_arr=$this->Model_catalogue->get_logistics_all_territory_name_data($data['get_logistics_territory_states_cities_pincodes']["territory_name_id"]);
			$data["territory_name"]=$get_logistics_all_territory_name_data_arr["territory_name"];
			
			$get_state_name_arr=$this->Model_catalogue->get_state_name($data['get_logistics_territory_states_cities_pincodes']["state_id"]);
			$data["state_name"]=$get_state_name_arr["state_name"];
			
			$get_city_name_arr=$this->Model_catalogue->get_city_name($data['get_logistics_territory_states_cities_pincodes']["pin"]);
			$data["city_name"]=$get_city_name_arr["city"];
			
			$data['vendors'] = $this->Model_catalogue->vendors();
			$data['create_editview']=$this->input->post("create_editview");
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_logistics_territory_states_cities_pincodes_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function edit_logistics_territory_states_cities_pincodes() {
		
		if($this->session->userdata("logged_in")){
			$edit_logistics_service_id=$this->db->escape_str($this -> input -> post("edit_logistics_service_id"));
			$to_select_list_pincodes=$this->db->escape_str($this -> input -> post("to_select_list"));
			
		
			$flag=$this->Model_catalogue->edit_logistics_territory_states_cities_pincodes($edit_logistics_service_id,$to_select_list_pincodes);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	
	
	public function show_available_logistics_territories_for_weight(){
		if($this->session->userdata("logged_in")){
			$logistics_id=$this->input->post("logistics_id");
			$result_logi=$this->Model_catalogue->show_available_logistics_territories_for_weight($logistics_id);
			if(count($result_logi)!=0){
				$str='';
				$str.='<option value=""></option>';			
				foreach($result_logi as $res){				
					$str.='<option value="'.$res->territory_name_id.'">'.$res->territory_name.'</option>';			
				}
				echo $str;
			}
			else{
				echo count($result_logi);
			}
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function show_logistics_sort_order(){
		if($this->session->userdata("logged_in")){
			$vendor_id=$this->input->post("vendors");
			$get_all_logistics_arr=$this->Model_catalogue->get_all_logistics($vendor_id);
				$logistics_id_arr=array();
				foreach($get_all_logistics_arr as $all_logistics_arr){
					$logistics_id_arr[]=$all_logistics_arr["logistics_priority"];
				}
				
				if(!empty($logistics_id_arr)){
				
				$miss_arr=array();
				$base_arr=array();
				
				for($i=1;$i<=max($logistics_id_arr);$i++){
					$base_arr[]=$i;
				}
				
				$miss_arr=array_diff($base_arr,$logistics_id_arr);
				if(max($logistics_id_arr)==0){
					$max_val_in_base_arr = 0;
				}
				else{
					$max_val_in_base_arr = max($base_arr);
				}
				if(in_array($max_val_in_base_arr,$logistics_id_arr)){
					$last_sort_sug=$max_val_in_base_arr+1;
					$miss_arr[]=$last_sort_sug;
				}
				
				if(count($miss_arr)>0){
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				
				<?php
				foreach($miss_arr as $k => $v){
					if($k==count($miss_arr)+1){
				?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
					else{
						?>
					<option value="<?php echo $v;?>"><?php echo $v;?></option>
				<?php
					}
				}
				}
				else{
			
				?>
					<option value="" selected></option>
					<option value="0">0</option>
					<option value="<?php echo (max($logistics_id_arr)+1);?>"><?php echo (max($logistics_id_arr)+1);?></option>
				<?php
				
				}
				}else{
				?>
				<option value="" selected></option>
				<option value="0">0</option>
				<option value="1">1</option>
				<?php
				}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function check_selection_in_all_parcel_categories(){
		if($this->session->userdata("logged_in")){
			$vendor_id=$this->db->escape_str($this -> input -> post("vendor_id"));

			//$vendor_id='1';
			$logistics_parcel_category_in=$this->db->escape_str($this -> input -> post("logistics_parcel_category_in"));

			$logistics_parcel_category_arr=explode("-",$logistics_parcel_category_in);

			$get_all_logistics_arr=$this->Model_catalogue->get_all_logistics($vendor_id);
			$logistics_id_orginal_arr=array();
			$logistics_id_in_parcel_category_arr=array();
			foreach($get_all_logistics_arr as $row){
				$logistics_id_orginal_arr[]=$row["logistics_id"];
			}
			//print_r($logistics_parcel_category_arr);
			foreach($logistics_parcel_category_arr as $logistics_parcel_category_id){

				$get_logistics_parcel_category_by_logistics_parcel_category_id_arr=$this->Model_catalogue->get_logistics_parcel_category_by_logistics_parcel_category_id($logistics_parcel_category_id);
				if(!empty($get_logistics_parcel_category_by_logistics_parcel_category_id_arr)){
					$logistics_id_in_parcel_category_arr[]=$get_logistics_parcel_category_by_logistics_parcel_category_id_arr["logistics_id"];
				}
			}
			//print_r($logistics_id_in_parcel_category_arr);
			$logistics_id_orginal_arr=array_unique($logistics_id_orginal_arr);
			$logistics_id_in_parcel_category_arr=array_unique($logistics_id_in_parcel_category_arr);

			sort($logistics_id_orginal_arr);
			sort($logistics_id_in_parcel_category_arr);

			//print_r($logistics_id_orginal_arr);
			//print_r($logistics_id_in_parcel_category_arr);
			
			if(count(array_diff($logistics_id_orginal_arr,$logistics_id_in_parcel_category_arr))==0 && count(array_diff($logistics_id_in_parcel_category_arr,$logistics_id_orginal_arr))==0){
				echo "yes";
			}
			else{
				echo "no";
			}

			//echo 'yes';

		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	 /* added */
	 public function inventory_all_unpublish($status=''){
		
		if($this->session->userdata("logged_in")){

		$data_posted=$this->input->post();

		if($this->session->flashdata('inv_post_data')){

			$data_posted=$this->session->flashdata('inv_post_data');
			$data['import_success']=1;
			$data['import_success_msg']=$this->session->flashdata('import_msg');
		   // print_r($data);exit;
		}
		$data['status']=$status;
		if($this->input->post("export_inventory")){
			//print_r($this->input->post());
			$this->export_inventory_all($this->input->post());
		}

		$data["controller"]=$this;
		$data['product_id']='';
		$data['brand_id']='';
		$data['subcat_id']='';
		$data['cat_id']='';
		$data['pcat_id']='';
					
		$data['create_editview']='view';
		
		$data['search_attribute_1_value']='';
		$data['search_attribute_2_value']='';
		$data['search_attribute_3_value']='';
		$data['search_attribute_4_value']='';
		$data['search_product_status']='';
		$data['search_view_product_status']='';

		$data["menu_flag"]="catalog_active_links";
		$user_type=$this->session->userdata("user_type");
		
		$str='';$arr=array();
		if($user_type=='vendor'){
			$vendor_id=$this->session->userdata("user_id");
			$v_data=get_vendor_details($vendor_id);
			$arr['total_available_sku']=0;
			$arr['total_selected_sku']=$v_data->invs_count;
		}
		
		$data['str']=$str;
		$data['total_sku_count_data']=$arr;
		
		$this -> load -> view('admin/vmanage',$data);
		$this -> load -> view('admin/inventory_view_all',$data);
		
	}else{
		$this->load->view('admin/header');
		$this->load->view('admin/login');
	}	
}
	/* added */
	public function inventory_all($status=''){
		
		if($this->session->userdata("logged_in")){

		$data_posted=$this->input->post();

		if($this->session->flashdata('inv_post_data')){

			$data_posted=$this->session->flashdata('inv_post_data');
			$data['import_success']=1;
			$data['import_success_msg']=$this->session->flashdata('import_msg');
		   // print_r($data);exit;
		}

		if($this->input->post("export_inventory")){
			//print_r($this->input->post());
			$this->export_inventory_all($this->input->post());
		}
		
		if($this->input->post("export_inventory_selected")){
			$selected_list=$this->input->post("export_inventory_selected");
			$arr=array();
			$this->export_inventory_all_new(array(),$selected_list);
			print_r($selected_list);
			exit;
			
		}
		$data['status']=$status;
		if($this->input->post("export_inventory")){
			//print_r($this->input->post());
			$this->export_inventory_all($this->input->post());
		}
		$data["controller"]=$this;
		$data['product_id']='';
		$data['brand_id']='';
		$data['subcat_id']='';
		$data['cat_id']='';
		$data['pcat_id']='';
					
		$data['create_editview']='view';
		
		$data['search_attribute_1_value']='';
		$data['search_attribute_2_value']='';
		$data['search_attribute_3_value']='';
		$data['search_attribute_4_value']='';
		$data['search_product_status']='';
		$data['search_view_product_status']='';

		$data["menu_flag"]="catalog_active_links";
		$user_type=$this->session->userdata("user_type");
		
		$str='';$arr=array();
		
		$data['str']=$str;
		$data['total_sku_count_data']=$arr;
		$this -> load -> view('admin/vmanage',$data);
		$this -> load -> view('admin/inventory_view_all',$data);
		
	}else{
		$this->load->view('admin/header');
		$this->load->view('admin/login');
	}	
}
public function inventory_processing_all(){
	if($this->session->userdata("logged_in")){
				
		$user_type=$this->session->userdata("user_type");
		$a_id=$this->session->userdata("user_id");
		/* admin settings */
		
		$adm_settings=$this->Model_catalogue->get_admin_settings();
		$max_addon_count=10;
		if(!empty($adm_settings)){
			$max_addon_count=$adm_settings->adm_addon_product_allowed;
		}

		/* admin settings */

		/* count change  */
		$str='';$arr=array();
		if($user_type=='vendor'){
			$vendor_id=$this->session->userdata("user_id");
			$v_data=get_vendor_details($vendor_id);
			
			if(!empty($v_data)){
				$arr['total_available_sku']=0;
				$arr['total_selected_sku']=$v_data->invs_count;
			}
		}
		//$str='';
					
		$data['str']=$str;
		/* count change  */
				
				
		$params = $columns = $totalRecords = $data = array();
		$params = $this->input->post();
		$totalRecords=$this->Model_catalogue->inventory_processing_all($params,"get_total_num_recs");	
		$queryRecordsResult=$this->Model_catalogue->inventory_processing_all($params,"get_recs");
		$sno=0;
		foreach($queryRecordsResult as $queryRecordsObj){
			$row=array();
			
			//$row[]=;
			//$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->id.'">  '.++$sno;
			
			if($user_type!='vendor' ){
				$row[]=++$sno;
			}
			if($user_type=='vendor' ){
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->id.'">';
			}
					
			$var='';
			$row[]='<img src="'.base_url().$queryRecordsObj -> image.'" width="100" alt="Image Not Found"><br>';
			$val='';
			$val.='Product Name : '.$queryRecordsObj -> product_name;
			

			$val.='<br>Custom SKU Name : '.$queryRecordsObj -> sku_name;
			$val.='<br>SKU ID : '.$queryRecordsObj -> sku_id;
			$val.='<br><small>Catalogue Structure : '.$queryRecordsObj -> pcat_name.' > '.$queryRecordsObj -> cat_name.' > '.$queryRecordsObj -> subcat_name.' > '.$queryRecordsObj -> brand_name.'</small>';

			
			$inv_type='';
			if($queryRecordsObj ->inventory_type=='1'){
				$inv_type="Only Main";
			}
			if($queryRecordsObj ->inventory_type=='2'){
				$inv_type="Only Addon";
			}
			if($queryRecordsObj ->inventory_type=='3'){
				$inv_type="Both Main and Addon";
			}

			$val.='<br><small class="text-info"><b>Inventory Type : '.$inv_type.'</b></small>';
			

			$val.='<h4>Attributes</h4>';
			$val.=$queryRecordsObj -> attribute_1.":".$queryRecordsObj -> attribute_1_value.'<br>';

			if(!empty($queryRecordsObj -> attribute_2)){
				$val.=$queryRecordsObj -> attribute_2.":".$queryRecordsObj -> attribute_2_value.'<br>';
			}
			if(!empty($queryRecordsObj -> attribute_3)){
				$val.=$queryRecordsObj -> attribute_3.":".$queryRecordsObj -> attribute_3_value.'<br>';
			}
			if(!empty($queryRecordsObj -> attribute_4)){ 
				$val.=$queryRecordsObj -> attribute_4.":".$queryRecordsObj -> attribute_4_value.'<br>';
			}
			
			$select=0;
			$row[]=$val;				

			if(1){
				$val2='';	$txt='';$str='';			
				$val2.='Tax:'.$queryRecordsObj -> tax_percent.'<br>';
				$val2.='Web Selling Price: '.curr_sym.$queryRecordsObj -> selling_price.'<br>';
				$val2.='Max Selling Price: '.curr_sym.$queryRecordsObj -> max_selling_price.'<br>';
				$val2.='Selling Discount %: '.$queryRecordsObj -> selling_discount.'%<br>';

				$val2.='Purchased Price: '.curr_sym.$queryRecordsObj -> purchased_price.'<br>';
				if($queryRecordsObj -> purchased_price>$queryRecordsObj -> selling_price){
						$val2.='Status : Loss ('.curr_sym.($queryRecordsObj -> purchased_price-$queryRecordsObj -> selling_price).')<br>';
				}
				else if($queryRecordsObj -> purchased_price<$queryRecordsObj -> selling_price){
						$val2.='Status : Profit ('.curr_sym.($queryRecordsObj -> selling_price-$queryRecordsObj -> purchased_price).')<br>';
				}
				else{
						$val2.='Status : Nill<br>';
				}
				$val2.='MOQ: '.$queryRecordsObj -> moq.'<br>';
				$val2.='Max OQ: '.$queryRecordsObj -> max_oq.'<br>';
				$val2.='Stock:'.$queryRecordsObj -> stock.'<br>';
				$val2.='Sold:'.$queryRecordsObj -> num_sales.'<br>';
				$val2.='Status:'.$queryRecordsObj -> product_status.'<br>';
				
				if($queryRecordsObj->publish_status=='0'){
					$txt.='<span style="color:blue"> Yet to process </span>';
				}else if($queryRecordsObj->publish_status=='1'){
					$txt.='<span style="color:green"> <b>Publish </b></span>'.'<br> <small> On '.date('d/m/Y H:i:s',strtotime($queryRecordsObj->status_on)).'</small>';
				}else if($queryRecordsObj->publish_status=='2'){
					$txt.='<span style="color:red"><b>Unpublish </b></span>'.'<br> <small> On '.date('d/m/Y H:i:s',strtotime($queryRecordsObj->status_on)).'</small>';;
				}else if($queryRecordsObj->publish_status=='3'){
					$txt.='<span style="color:red"><b>Rejected </b></span>'.'<br> <small> On '.date('d/m/Y H:i:s',strtotime($queryRecordsObj->status_on)).'</small>';

					$txt.='<br><u><small>Rejected comments</small></u> : <br>';
					$txt.=''.$queryRecordsObj->rejection_comments;
				}
				$str.="<br>Status :<br>".  $txt.'<br>';
				//print_r($queryRecordsObj);
			}
			
			$row[]=$val2.$str;	
			
			$link='';$actions='';

			$count=0;$count_txt='';
			$res=get_inventory_tagged_products($queryRecordsObj ->id);
			
			if(!empty($res)){

				$addon_txt='';
				
				foreach($res as $val){
					$tagged_inventory_ids=$val->tagged_inventory_ids;
					$arr=explode(',',$tagged_inventory_ids);
					$arr=array_filter($arr);
					//print_r($arr);
					$count+=count($arr);
					
				}
			}
			if($count>0){
				$count_txt.=' <span class="badge">'.$count.'</span> ';
			}

			/* If it is a addon inventory - no need to show the button */
			if($queryRecordsObj ->inventory_type!='2'){

				$actions.='<a href="'.base_url().'admin/Catalogue/add_tagged_invs/'.$queryRecordsObj->id.'" class="btn btn-info btn-sm">'.$count_txt.' Tag Other Inventories </a><br>';

				
				/* input to update addon max -item */

				if($count>0){
					$selection_type=get_addon_selection_limit($queryRecordsObj->id);
					$actions.='<button class="btn btn-success btn-sm" type="button" onclick="show_addon_product_count('.$queryRecordsObj->id.')"><span class="badge">'.$queryRecordsObj->addon_product_allowed.'</span> Define Addon Selection Limit</button>';

					$actions.=	'
					<div class="addon_form" id="addon_'.$queryRecordsObj->id.'" style="display:none;">
						
						<div class="col-md-10">	
							<div class="form-group label-floating">
							<label class="control-label">Addon Selection Type</label>
							
							<select name="addon_selection_type_'.$queryRecordsObj->id.'" id="addon_selection_type_'.$queryRecordsObj->id.'" class="form-control" required onchange="change_limit('.$queryRecordsObj->id.')">
							<option value="">Addon Selection Type</option>
							<option value="single" '.(($selection_type=='single') ? 'selected' : '') .'>Single</option>
							<option value="multiple"  '.(($selection_type=='multiple') ? 'selected' : '') .'>Multiple</option>
							</select>
								
							</div>
						</div>

						<div class="col-md-10">	
							<div class="form-group label-floating">
							<label class="control-label">Addon Selection Limit</label>
							<input type="text" min="1" max="'.$max_addon_count.'" name="addon_product_allowed_'.$queryRecordsObj->id.'" id="addon_product_allowed_'.$queryRecordsObj->id.'" class="form-control" value="'.$queryRecordsObj->addon_product_allowed.'" onKeyPress="return isNumber(event)">
								
							</div>
						</div>

						<div class="col-md-10">	
							<div class="form-group label-floating">
							<button class="btn btn-success btn-sm" type="button" onclick="addon_product_allowed('.$queryRecordsObj->id.')">submit</button>
							</div>
						</div>

					</div>

					';
				}
				/* input to update addon max -item */

			}
			/* If it is a addon inventory - no need to show the button */

			
				$actions.='<button class="btn btn-warning btn-sm" onclick="export_csv_single('.$queryRecordsObj->id.')">Download CSV</button><br>';
				$actions.='<a href="'.base_url().'admin/Catalogue/add_tagged_invs_combo/'.$queryRecordsObj->id.'" class="btn btn-info btn-sm">'.$count_txt.' Tag Inventories For Combo </a><br>';
			

			/* for Promotions display */
			$inventory_id=$queryRecordsObj->id;
			$product_id=$queryRecordsObj->product_id;
        $promo_array=array();$promo_uid_arr=array();
        if(!empty($product_id)){
            $a_TO_z_ids=$this->Front_promotions->get_a_TO_z_id($product_id);
            
            if(!empty($a_TO_z_ids)){
                foreach($a_TO_z_ids as $a_TO_z_id){
                    $brand_id=$a_TO_z_id['brand_id'];
                    $subcat_id=$a_TO_z_id['subcat_id'];
                    $cat_id=$a_TO_z_id['cat_id'];
                    $pcat_id=$a_TO_z_id['pcat_id'];
                }
                
                $product_level_promotions_uid=$this->Front_promotions->product_level_promotion_uid($product_id);
                $brand_level_promotions_uid=$this->Front_promotions->brand_level_promotion_uid($brand_id);
                $subcat_level_promotions_uid=$this->Front_promotions->subcat_level_promotion_uid($subcat_id);
                $cat_level_promotions_uid=$this->Front_promotions->cat_level_promotion_uid($cat_id);
                $pcat_level_promotions_uid=$this->Front_promotions->pcat_level_promotion_uid($pcat_id);
                $store_level_promotions_uid=$this->Front_promotions->store_level_promotion_uid();
               
                if(!empty($product_level_promotions_uid)){
                    foreach($product_level_promotions_uid as $product_level_promotion){
                        $promo_data=$this->Front_promotions->product_level_promotion($product_level_promotion['promo_uid'],$product_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
							$promo_uid_arr[]=$product_level_promotion['promo_uid'];
                        }
                    }
                    
                }
                
                if(!empty($brand_level_promotions_uid)){
                    
                    foreach($brand_level_promotions_uid as $brand_level_promotion){
                        $promo_data=$this->Front_promotions->brand_level_promotion($brand_level_promotion['promo_uid'],$brand_id);
                        if(!empty($promo_data)){
							if(!in_array($brand_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$brand_level_promotion['promo_uid'];
							}
						}
                    }
                }
                if(!empty($subcat_level_promotions_uid)){
                    foreach($subcat_level_promotions_uid as $subcat_level_promotion){
                        $promo_data=$this->Front_promotions->subcat_level_promotion($subcat_level_promotion['promo_uid'],$subcat_id);
						if(!empty($promo_data)){
							if(!in_array($subcat_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$subcat_level_promotion['promo_uid'];
							}
							
						}
                    }
                }
                if(!empty($cat_level_promotions_uid)){
                    foreach($cat_level_promotions_uid as $cat_level_promotion){
                        $promo_data=$this->Front_promotions->cat_level_promotion($cat_level_promotion['promo_uid'],$cat_id);
                        if(!empty($promo_data)){
							if(!in_array($cat_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$cat_level_promotion['promo_uid'];
							}
						}
                    }
                }
                if(!empty($pcat_level_promotions_uid)){
                    foreach($pcat_level_promotions_uid as $pcat_level_promotion){
                        $promo_data=$this->Front_promotions->pcat_level_promotion($pcat_level_promotion['promo_uid'],$pcat_id);
						if(!empty($promo_data)){
							if(!in_array($pcat_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$pcat_level_promotion['promo_uid'];
							}
							
						}
                    }
                }
                if(!empty($store_level_promotions_uid)){
					
                    foreach($store_level_promotions_uid as $store_level_promotion){
                        $promo_data=$this->Front_promotions->store_level_promotion($store_level_promotion['promo_uid']);
						if(!empty($promo_data)){
							if(!in_array($store_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$store_level_promotion['promo_uid'];
							}
						}
                    }
                }
            }
        }
        $promo_uid=$this->Front_promotions->get_promotions($inventory_id);
        
        if(!empty($promo_uid)){
            foreach($promo_uid as $res){
                $promo_data=$this->Front_promotions->get_all_promotions_data($res['promo_uid'],$inventory_id);
                if(!empty($promo_data)){
					if(!in_array($res['promo_uid'],$promo_uid_arr)){
						array_push($promo_array,$promo_data);
						$promo_uid_arr[]=$res['promo_uid'];
					}
				}
            }
        }
        
        $promo_array;

		/* promo str */
		$promo_str='';
		//print_r($promo_array);
		if(!empty($promo_array)){
			foreach($promo_array as $promo_arr){
				if(!empty($promo_arr)){
					
					foreach($promo_arr as $promo){
						$promo_str.=$promo->promo_quote.'<br>';
						$promo_str.="Period :".date('d M, Y H:i:s',strtotime($promo->promo_start_date)).' to '.date('d M, Y H:i:s',strtotime($promo->promo_end_date)).'<br>';
						$st=($promo->promo_active=='1') ? 'Active'  : 'In Active';
						$promo_str.='Status:'.$st.'<br>';
						$promo_str.='Expired Status : '.date_status($promo->promo_start_date,$promo->promo_end_date);
						$promo_str.='<hr>';
					}
				}

			}
		}

		if($promo_str!=''){
			$promo_str='<b><u>Promo Details</u></b>'.'<br>'.$promo_str.'';
		}else{
			$promo_str='No Promotions applied';
		}
		/* promo str */

		$row[]=$promo_str;

		/* for Promotions display */			


		$user_type=$this->session->userdata("user_type");
				//$link='';
				if($user_type!='vendor'){
					
					/*$link.='<br><select class="form-control" name="change_status_'.$queryRecordsObj->vendor_id.'" id="change_status_'.$queryRecordsObj->vendor_id.'" onchange="change_status('.$queryRecordsObj->vendor_id.','.$queryRecordsObj->id.',this.value)"> ';
	
					if($queryRecordsObj->publish_status!='1' || $queryRecordsObj->publish_status!='2'){
						$link.='<option value="">Change status</option>';
					}
					if($queryRecordsObj->publish_status!='1'){
						$link.='<option value="1">Publish</option>';
					}
					if($queryRecordsObj->publish_status!='2'){
						$link.='<option value="2">Unpublish</option>';
					}
					
					$link.='</select>';*/

					$link.='<button class="btn btn-warning" onclick="view_status_modal('.$queryRecordsObj->vendor_id.','.$queryRecordsObj->id.','.$queryRecordsObj->publish_status.')">Update status</button>';


				}
			
			$row[]=$link.$actions;
					
			$data[]=$row;	
		}
		$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data,
					"tot_sku_count_data"=>$arr
					);
		echo json_encode($json_data);
		
	}else{
		$this->load->view('admin/header');
		$this->load->view('admin/login');
	}
	
}
public function add_tagged_invs($inv_id){
	if($this->session->userdata("logged_in")){
		if($inv_id!=''){
			$res=get_all_inventory_data($inv_id);
		}else{
			redirect(base_url('admin/Catalogue/inventory_all'));
		}


		$adm_settings=$this->Model_catalogue->get_admin_settings();
		$max_addon_count=10;
		if(!empty($adm_settings)){
			$max_addon_count=$adm_settings->adm_addon_product_allowed;
		}

		$data['inv_obj']=$res;

		$res_obj=get_inventory_tagged_products($inv_id);
		
		/* already tagged inventories */

		$count=0;$count_txt='';
		$res=get_inventory_tagged_products($inv_id);
		
		if(!empty($res)){

			$addon_txt='';
			
			foreach($res as $val){
				$tagged_inventory_ids=$val->tagged_inventory_ids;
				$arr=explode(',',$tagged_inventory_ids);
				$arr=array_filter($arr);
				//print_r($arr);
				$count+=count($arr);
				
			}
		}
		
		$data['tagged_invs_count']=$count;
			
		/* already tagged inventories */
		
		$data['max_addon_count'] = $max_addon_count;

		$data['parent_category'] = $this->Model_catalogue->parent_category();
		$data["menu_flag"]="catalog_active_links";
		$data["tagged_invs"]=$res_obj;
		$data["inv_id"]=$inv_id;
		
		$this -> load -> view('admin/vmanage',$data);
		$this -> load -> view('admin/tagged_invs/add_tagged_invs',$data);
		
	}else{
		$this->load->view('admin/header');
		$this->load->view('admin/login');
	}
}


public function show_available_skus(){
	if($this->session->userdata("logged_in")){	
		$product_id=$this->input->post("product_id");
		$sku_ids=$this->input->post("sku_ids");
		$active=$this->input->post("active");
		$inv_id=$this->input->post("inv_id");
		if($this->input->post("with_status")){
			$with_status=$this->input->post("with_status");
		}else{
			$with_status=0;
		}
		$result_sku=$this->Model_catalogue->show_available_skus_by_product_id($product_id,$active);

		$arr=$this->Model_catalogue->get_already_selected_skus($product_id,$inv_id);

		if(count($result_sku)!=0){
			$str='';
			//$str.='<option value=""></option>';
			if($with_status==1){
				foreach($result_sku as $res){
					$active_status=($res->active==1)?" - active":" - Inactive";

					$str.='<option value="'.$res->sku_id.'-'.$res->active.'">'.$res->sku_id.$active_status.'</option>';
				}
			}else{
									//$arr=explode($sku_ids,'|');
				foreach($result_sku as $res){
					if($res->id!=$inv_id){

						if(!in_array($res->id,$arr)){
							$str.='<option value="'.$res->id.'">'.$res->sku_id. '  - '.$res->attribute_1_value.' , '.$res->attribute_2_value .'</option>';
						}
						
					}	
				}
			}
			echo $str;
		}else{
			echo count($result_sku);
		}
			}else{
					redirect(base_url());
			}
}
public function show_available_skus_by_subcat(){
	if($this->session->userdata("logged_in")){	
		$subcat_id=$this->input->post("subcat_id");
		$sku_ids=$this->input->post("pr_sku_ids");
		$active=$this->input->post("active");
		if($this->input->post("with_status")){
			$with_status=$this->input->post("with_status");
		}else{
			$with_status=0;
		}
		$result_sku=$this->Model_catalogue->show_available_skus_by_subcat($subcat_id,$active);
		if(count($result_sku)>0){
			$str='';
			foreach($result_sku as $res){ 
				$str.='<option value="'.$res->id.'">'.$res->sku_id. '  - '.$res->attribute_1_value.' , '.$res->attribute_2_value .'</option>';
			}
			echo $str;
		}else{
			//echo count($result_sku);
		}
			}else{
					redirect(base_url());
			}
}
public function add_tagged_invs_form_submit(){
	if($this->session->userdata("logged_in")){
			$data_posted=$this->input->post();
			//print_r($data_posted);exit;
			$inv_id=$this->input->post('tagged_main_inventory_id');
			$flag=$this->Model_catalogue->add_tagged_invs($data_posted);
			
			if($flag){
				if($inv_id!=''){
					$msg='SKUs are tagged successfully';
				}else{
					$msg='SKUs are tagged successfully';
				}
				$this->session->set_flashdata('notification',$msg);
				
				redirect(base_url("admin/Catalogue/add_tagged_invs/".$inv_id));
			}

	}else{
		redirect(base_url('Administrator'));
	}
}
public function add_tagged_external_invs_form_submit(){
	if($this->session->userdata("logged_in")){
			$data_posted=$this->input->post();
			//print_r($data_posted);exit;
			$inv_id=$this->input->post('tagged_main_inventory_id');
			$flag=$this->Model_catalogue->add_tagged_external_invs($data_posted);
			
			if($flag){
				if($inv_id!=''){
					$msg='SKUs are tagged successfully';
				}else{
					$msg='SKUs are tagged successfully';
				}
				$this->session->set_flashdata('notification',$msg);
				
				redirect(base_url("admin/Catalogue/add_tagged_invs/".$inv_id));
			}

	}else{
		redirect(base_url('Administrator'));
	}
}
public function delete_tagged_invs() {
	if($this->session->userdata("logged_in")){
		
		$tagged_id = $this->input->post('tagged_id');
		$tagged_type = $this->input->post('tagged_type');
		$error_msg='';
		$flag=$this->Model_catalogue->delete_tagged_invs($tagged_id,$tagged_type);
		if($flag){
			$error_msg='Tagged Inventory Deleted Successfully';
		}
		
		echo json_encode(array("flag"=>1,'message'=>$error_msg));
	}else{
		$this->load->view('admin/header');
		$this->load->view('admin/login');
	}
}
public function admin_settings(){
	if($this->session->userdata("logged_in")){
		
		$adm_settings=$this->Model_catalogue->get_admin_settings();
		$marquee=$this->Model_catalogue->get_marquee();
		$data["menu_flag"]="admin_settings";
		$data["adm_settings"]=$adm_settings;
		$data["marquee"]=$marquee;
		
		$this -> load -> view('admin/vmanage',$data);
		$this -> load -> view('admin/admin_settings',$data);

	}else{
		redirect(base_url('Administrator'));
	}
}
public function admin_settings_submit(){
	if($this->session->userdata("logged_in")){
		
		$post_arr=$this->input->post();

		$flag=$this->Model_catalogue->admin_settings_submit($post_arr);

		if($flag==true){
			redirect(base_url('admin/Catalogue/admin_settings'));
		}

	}else{
		redirect(base_url('Administrator'));
	}
}

public function export_inventory_all_new($arr,$selected_list=''){
	$data_posted=$arr;
	$product_id=(isset($data_posted["products"])) ? $data_posted["products"]: '';
	$brand_id=(isset($data_posted["brands"])) ? $data_posted["brands"]: '';
	$subcat_id=(isset($data_posted["subcategories"])) ?$data_posted["subcategories"] : '';
	$cat_id=(isset($data_posted["categories"])) ? $data_posted["categories"]: '';
	$pcat_id=(isset($data_posted["parent_category"])) ?$data_posted["parent_category"] : '';

	if($selected_list==''){
		$inventory_details = $this->Model_catalogue->get_inventory_details_result_new($product_id,$pcat_id,$cat_id,$subcat_id,$brand_id);
	}else{
		$inventory_details = $this->Model_catalogue->get_inventory_details_result_selected($selected_list);
	}

	$header_top =array("Product name","Subcategory name","Category name","Parent category name","Brand name");
	/* file name */
	
	$t=time();

	$filename = 'product_inventory'.$t.'_'.date('Ymd').'.csv';
	header("Content-Description: File Transfer");
	header("Content-Disposition: attachment; filename=$filename");
	header("Content-Type: application/csv; ");
	/* get data */
	//$usersData = $this->Crud_model->getUserDetails();
	/* file creation */

	//$usersData=array(array('jon','jhon','male','jhon@gmail.com'));
	//$header = array("Username","Name","Gender","Email");

	$file = fopen('php://output','w');

	$header = array("sku_id", "attribute_1", "attribute_1_value", "attribute_2", "attribute_2_value","attribute_3", "attribute_3_value", "attribute_4", "attribute_4_value", "tax_percent", "base_price", "selling_price","selling_discount","CGST","IGST","SGST", "purchased_price", "mrp_quantity", "moq", "moq_base_price", "moq_price", "moq_purchased_price", "max_oq", "num_sales", "stock", "low_stock", "cutoff_stock","Promotion Details","product_description");

	$prod_arr=array();
	$header_written_csv="no";
	foreach ($inventory_details as $key=>$line){
		
			
			$product_id=$line['product_id'];
			$id=$line['id'];
			
			if($product_id!=''){

				//print_r($product_id);
				//echo $product_id.'||';

				$product_details = $this->Model_catalogue->get_product_details_result($product_id);
				$product_name=(isset($product_details['product_name'])) ? '_'.$product_details['product_name']:'';
				$product_name = str_replace(' ', '_', $product_name);

				if(!in_array($product_id,$prod_arr)){
					$prod_arr[]=$product_id;
					if($header_written_csv=="no"){
						fputcsv($file, array_merge($header_top,$header));
						$header_written_csv="yes";
					}
				}



				/* for Promotions display */
			$inventory_id=$id;
			$product_id=$product_id;
        $promo_array=array();$promo_uid_arr=array();
        if(!empty($product_id)){
            $a_TO_z_ids=$this->Front_promotions->get_a_TO_z_id($product_id);
            
            if(!empty($a_TO_z_ids)){
                foreach($a_TO_z_ids as $a_TO_z_id){
                    $brand_id=$a_TO_z_id['brand_id'];
                    $subcat_id=$a_TO_z_id['subcat_id'];
                    $cat_id=$a_TO_z_id['cat_id'];
                    $pcat_id=$a_TO_z_id['pcat_id'];
                }
                
                $product_level_promotions_uid=$this->Front_promotions->product_level_promotion_uid($product_id);
                $brand_level_promotions_uid=$this->Front_promotions->brand_level_promotion_uid($brand_id);
                $subcat_level_promotions_uid=$this->Front_promotions->subcat_level_promotion_uid($subcat_id);
                $cat_level_promotions_uid=$this->Front_promotions->cat_level_promotion_uid($cat_id);
                $pcat_level_promotions_uid=$this->Front_promotions->pcat_level_promotion_uid($pcat_id);
                $store_level_promotions_uid=$this->Front_promotions->store_level_promotion_uid();
               
                if(!empty($product_level_promotions_uid)){
                    foreach($product_level_promotions_uid as $product_level_promotion){
                        $promo_data=$this->Front_promotions->product_level_promotion($product_level_promotion['promo_uid'],$product_id);
                        if(!empty($promo_data)){
                            array_push($promo_array,$promo_data);
							$promo_uid_arr[]=$product_level_promotion['promo_uid'];
                        }
                    }
                    
                }
                
                if(!empty($brand_level_promotions_uid)){
                    
                    foreach($brand_level_promotions_uid as $brand_level_promotion){
                        $promo_data=$this->Front_promotions->brand_level_promotion($brand_level_promotion['promo_uid'],$brand_id);
                        if(!empty($promo_data)){
							if(!in_array($brand_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$brand_level_promotion['promo_uid'];
							}
						}
                    }
                }
                if(!empty($subcat_level_promotions_uid)){
                    foreach($subcat_level_promotions_uid as $subcat_level_promotion){
                        $promo_data=$this->Front_promotions->subcat_level_promotion($subcat_level_promotion['promo_uid'],$subcat_id);
						if(!empty($promo_data)){
							if(!in_array($subcat_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$subcat_level_promotion['promo_uid'];
							}
							
						}
                    }
                }
                if(!empty($cat_level_promotions_uid)){
                    foreach($cat_level_promotions_uid as $cat_level_promotion){
                        $promo_data=$this->Front_promotions->cat_level_promotion($cat_level_promotion['promo_uid'],$cat_id);
                        if(!empty($promo_data)){
							if(!in_array($cat_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$cat_level_promotion['promo_uid'];
							}
						}
                    }
                }
                if(!empty($pcat_level_promotions_uid)){
                    foreach($pcat_level_promotions_uid as $pcat_level_promotion){
                        $promo_data=$this->Front_promotions->pcat_level_promotion($pcat_level_promotion['promo_uid'],$pcat_id);
						if(!empty($promo_data)){
							if(!in_array($pcat_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$pcat_level_promotion['promo_uid'];
							}
							
						}
                    }
                }
                if(!empty($store_level_promotions_uid)){
					
                    foreach($store_level_promotions_uid as $store_level_promotion){
                        $promo_data=$this->Front_promotions->store_level_promotion($store_level_promotion['promo_uid']);
						if(!empty($promo_data)){
							if(!in_array($store_level_promotion['promo_uid'],$promo_uid_arr)){
								array_push($promo_array,$promo_data);
								$promo_uid_arr[]=$store_level_promotion['promo_uid'];
							}
						}
                    }
                }
            }
        }
        $promo_uid=$this->Front_promotions->get_promotions($inventory_id);
        
        if(!empty($promo_uid)){
            foreach($promo_uid as $res){
                $promo_data=$this->Front_promotions->get_all_promotions_data($res['promo_uid'],$inventory_id);
                if(!empty($promo_data)){
					if(!in_array($res['promo_uid'],$promo_uid_arr)){
						array_push($promo_array,$promo_data);
						$promo_uid_arr[]=$res['promo_uid'];
					}
				}
            }
        }
        
        $promo_array;

		/* promo str */
		$promo_str='';
		//print_r($promo_array);
		if(!empty($promo_array)){
			foreach($promo_array as $promo_arr){
				if(!empty($promo_arr)){
					
					foreach($promo_arr as $promo){
						$promo_str.=$promo->promo_quote."\n";
						$promo_str.="Period : ".date('d M, Y H:i:s',strtotime($promo->promo_start_date)).' to '.date('d M, Y H:i:s',strtotime($promo->promo_end_date))."\n";
						$st=($promo->promo_active=='1') ? 'Active'  : 'In Active';
						$promo_str.='Status : '.$st."\n";
						$promo_str.='Expired Status : '.date_status($promo->promo_start_date,$promo->promo_end_date);
						$promo_str.="\n";
					}
				}

			}
		}

		if($promo_str!=''){
			$promo_str=$promo_str.'';
		}else{
			$promo_str='No Promotions applied';
		}
		$line['promotion_details']=$promo_str;
		/* promo str */

				unset($line['id']);
				unset($line['product_id']);
				$product_description_temp_arr=[];
				$product_description_temp_arr["product_description"]=$product_details['product_description'];
				unset($product_details['product_description']);
				fputcsv($file, array_merge($product_details,$line,$product_description_temp_arr));
				//fputcsv($file,$line);
			}	
			
		
	}
	fclose($file);
	exit;
}


	/* added */
	
	public function get_inventory_list_details(){
		$inventory_list_id_str=$this->input->post("inventory_list_id_str");
		$inventory_list_id_str=str_replace("-",",",$inventory_list_id_str);
		$result_sku=$this->Model_catalogue->get_inventory_list_details($inventory_list_id_str);
		echo json_encode($result_sku);
	}
	public function get_inventory_list_details_single_all(){
		$inventory_list_id_str=$this->input->post("inventory_list_id_str");
		$inventory_list_id_str=str_replace("-",",",$inventory_list_id_str);
		$result_sku=$this->Model_catalogue->get_inventory_list_details_single_all($inventory_list_id_str);
		echo json_encode($result_sku);
	}
	public function get_external_inventory_list_details_single_all(){
		$inventory_list_id_str=$this->input->post("inventory_list_id_str");
		$inventory_list_id_str=str_replace("-",",",$inventory_list_id_str);
		$result_sku=$this->Model_catalogue->get_external_inventory_list_details_single_all($inventory_list_id_str);
		echo json_encode($result_sku);
	}
	
	/*** show all category chain etc starts */
	
	public function show_all_categories(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("pcat_id");
			$result_cat=$this->Model_catalogue->show_all_categories($pcat_id);
			if(count($result_cat)!=0){
				$str='';
				$str.='<option value=""></option>';	
				foreach($result_cat as $res){
					$active_status=($res->active==1)?" - active":" - Inactive";
					$str.='<option value="'.$res->cat_id.'">'.$res->cat_name.$active_status.'</option>';
				}
				echo $str;
			}else{
				echo count($result_cat);
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_all_subcategories(){
		if($this->session->userdata("logged_in")){
			$cat_id=$this->input->post("cat_id");
			$result_subcat=$this->Model_catalogue->show_all_subcategories($cat_id);
			if(count($result_subcat)!=0){
				$str='';
				$str.='<option value=""></option>';
				foreach($result_subcat as $res){
					$active_status=($res->active==1)?" - active":" - Inactive";
					$str.='<option value="'.$res->subcat_id.'">'.$res->subcat_name.$active_status.'</option>';
				}
				echo $str;
			}else{
				echo count($result_subcat);
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_all_brands(){
		if($this->session->userdata("logged_in")){	
			$subcat_id=$this->input->post("subcat_id");
			if($this->input->post("with_status")){
				$with_status=$this->input->post("with_status");
			}else{
				$with_status=0;
			}
			$result_brand=$this->Model_catalogue->show_all_brands($subcat_id);
			if(count($result_brand)!=0){
				$str='';
				$str.='<option value=""></option>';
				foreach($result_brand as $res){
					$active_status=($res->active==1)?" - active":" - Inactive";
					$str.='<option value="'.$res->brand_id.'">'.$res->brand_name.$active_status.'</option>';
				}
				echo $str;
			}else{
				echo count($result_brand);
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_all_products(){
			
		if($this->session->userdata("logged_in")){	
		    
			$brand_id=$this->input->post("brand_id");
			$result_brand=$this->Model_catalogue->show_all_products($brand_id);
			if(count($result_brand)!=0){
				$str='';
				$str.='<option value=""></option>';
				foreach($result_brand as $res){
					$active_status=($res->active==1)?" - active":" - Inactive";
					$str.='<option value="'.$res->product_id.'">'.$res->product_name.$active_status.'</option>';
				}
				echo $str;
			}else{
				echo count($result_brand);
			}
		}else{
			
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_all_skus(){
	if($this->session->userdata("logged_in")){	
		$product_id=$this->input->post("product_id");
		$sku_ids=$this->input->post("sku_ids");
		$inv_id=$this->input->post("inv_id");
		
		$inv_type=$this->input->post("inv_type");
		
		$result_sku=$this->Model_catalogue->show_all_skus_by_product_id($product_id,$inv_type);

		$arr=$this->Model_catalogue->get_already_selected_skus($product_id,$inv_id,$sku_ids,$inv_type);

		if(count($result_sku)!=0){
			$str='';
			//$str.='<option value=""></option>';
			foreach($result_sku as $res){
				if($res->id!=$inv_id){

					if(!in_array($res->id,$arr)){
						$active_status=($res->active==1)?" - active":" - Inactive";
						$str.='<option value="'.$res->id.'">'.$res->sku_id.$active_status.'</option>';
					}
					
				}	
			}
			echo $str;
		}else{
			echo count($result_sku);
		}
			}else{
				redirect(base_url());
			}
}
	/*** show all category chain etc ends */
	

	public function product_vendors_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->product_vendors_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->product_vendors_processing($params,"get_recs");
			$sno=0;
						$user_type=$this->session->userdata("user_type");
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				// $row[]='<input type="checkbox" name="common_checkbox" id="common_checkbox" onclick="selectAllFun(this)">';
				$row[]=++$sno;
				$txt='';
				$txt.='<b>'.ucfirst($queryRecordsObj->name).'</b>';
				$txt.='<br>'.$queryRecordsObj->email;
				$txt.='<br>'.$queryRecordsObj->mobile;

				if($queryRecordsObj->whatsapp_number!=''){
					$txt.='<br><i class="fa fa-whatsapp" aria-hidden="true"></i> '.$queryRecordsObj->whatsapp_number;
				}
			
				$txt.='<br><br> <b>Address</b>';
				$txt.='<br>'.$queryRecordsObj->address1;$txt.=','.$queryRecordsObj->address2;
				$txt.='<br>'.$queryRecordsObj->city;
				$txt.='<br>'.$queryRecordsObj->state;
				$txt.='<br>'.$queryRecordsObj->country;
				$txt.='<br>'.$queryRecordsObj->pincode;
			
				$row[]=$txt;
										
				$str='';
									
				$txt='';
				if($queryRecordsObj->approval_status=='0'){
					$txt.='<span style="color:blue"> Yet to process </span>';
				}else if($queryRecordsObj->approval_status=='1'){
					$txt.='<span style="color:green"> <b>Approved </b></span>'.'<br> <small> On '.date('d/m/Y H:i:s',strtotime($queryRecordsObj->status_updated_on)).'</small>';
				}else if($queryRecordsObj->approval_status=='2'){
					$txt.='On Hold'.'<br> <small> On '.date('d/m/Y H:i:s',strtotime($queryRecordsObj->status_updated_on)).'</small>';;
				}else if($queryRecordsObj->approval_status=='3'){
					$txt.='<span style="color:red"> <b>Rejected</b></span>'.'<br> <small> On '.date('d/m/Y H:i:s',strtotime($queryRecordsObj->status_updated_on)).'</small>';;
				}
	
		
				if($queryRecordsObj->about_furniture_operations!=''){
					$about_furniture_operations=$queryRecordsObj->about_furniture_operations;
					$about_furniture_operations_arr=($about_furniture_operations!='') ? explode(',',$about_furniture_operations) : '';
								
					$str.='<br><small><b>Furniture operations</b></small> : ';
					foreach($about_furniture_operations_arr as $val){
						$val = str_replace(' ', '_', $val);
						$str.=ucfirst($val).',';
					}
				}

				if($queryRecordsObj->website_url!='')
				$str.='<br><small><b>website url</b></small> : '.$queryRecordsObj->website_url;

				if($queryRecordsObj->instagram_url!='')
				$str.='<br><small><b>instagram url</b></small> : '.$queryRecordsObj->instagram_url;

				if($queryRecordsObj->facebook_url!='')
				$str.='<br><small><b>Facebook url</b></small> : '.$queryRecordsObj->facebook_url;
				
				if($queryRecordsObj->size_of_team!='')
				$str.='<br><small><b>Size of team</b></small> : '.$queryRecordsObj->size_of_team;

				if($queryRecordsObj->kind_of_furniture!=''){
					$kind_of_furniture=$queryRecordsObj->kind_of_furniture;
					$kind_of_furniture_arr=($kind_of_furniture!='') ? explode(',',$kind_of_furniture) : '';
								
					$str.='<br><small><b>Kind of furniture</b></small> : ';
					foreach($kind_of_furniture_arr as $val){
						$val = str_replace('_', ' ', $val);
						$str.=ucfirst($val).',';
					}
				}
				//if($queryRecordsObj->kind_of_furniture!='')
				//$str.='<br><small><b>Kind of furniture</b></small> : '.$queryRecordsObj->kind_of_furniture;
				
				if($queryRecordsObj->years_of_business!='')
				$str.='<br><small><b>Year of business</b></small> : '.$queryRecordsObj->years_of_business;

				if($queryRecordsObj->shipping_capabilities!='')
				$str.='<br><small><b>Shipping Capabilities</b></small> : '.$queryRecordsObj->shipping_capabilities;

				if($queryRecordsObj->shipping_capabilities_description!=''){
					$str.='<br><small><b>Shipping Capabilities Description</b></small> : '.$queryRecordsObj->shipping_capabilities_description;
				}

				$str.="<br>Approved Status :".  $txt.'<br>';
				

	
				$row[]=$str;
				$row[]=date('d-m-Y H:i:s',strtotime($queryRecordsObj -> timestamp));
				$link='';       
				if($user_type!='vendor'){
					$link='<select class="form-control" name="change_status_'.$queryRecordsObj->vendor_id.'" id="change_status_'.$queryRecordsObj->vendor_id.'" onchange="change_status('.$queryRecordsObj->vendor_id.',this.value)"> ';
	
					if($queryRecordsObj->approval_status!='1' || $queryRecordsObj->approval_status!='2' || $queryRecordsObj->approval_status!='3'){
						$link.='<option value="">Change status</option>';
					}
					if($queryRecordsObj->approval_status!='1'){
						$link.='<option value="1">Approve</option>';
					}
					if($queryRecordsObj->approval_status!='2'){
						$link.='<option value="2">On hold</option>';
					}
					if($queryRecordsObj->approval_status!='3'){
						$link.='<option value="3">Reject</option>';
					}
					$link.='</select>';
				}
						
				if($queryRecordsObj->active){
					$link.='<br>';
					if($user_type!='vendor' && $user_type!='stockist'){
						/*$link.='<form action="'.base_url().'admin/Catalogue/edit_vendors_form" method="post">'
							. '<input type="hidden" value="'.$queryRecordsObj ->vendor_id.'" name="vendor_id">'
							. '<input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';*/
					}
				}
				
				//$link.='<br><a class="btn btn-warning btn-xs btn-block"  href="'.base_url().'admin/Catalogue/edit_vendors_form/'.$queryRecordsObj -> vendor_id.'" method="post">Edit</a>';

				$row[]=$link;
				
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function vendors_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->vendors_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->vendors_processing($params,"get_recs");
			$sno=0;
						$user_type=$this->session->userdata("user_type");
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				// $row[]='<input type="checkbox" name="common_checkbox" id="common_checkbox" onclick="selectAllFun(this)">';
				$row[]=++$sno;
				$txt='';
				$txt.='<b>'.ucfirst($queryRecordsObj->name).'</b>';
				$txt.='<br>'.$queryRecordsObj->email;
				$txt.='<br>'.$queryRecordsObj->mobile;

			
				$row[]=$txt;
										
				$str='';
							
	
				$row[]=$str;
				$row[]=date('d-m-Y H:i:s',strtotime($queryRecordsObj -> timestamp));
				$link='';       
				$row[]=$link;
				
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function vendor_change_status() {
			if($this->session->userdata("logged_in")){
	
				$status = $this->input->post('status');
				$vendor_id = $this->input->post('vendor_id');
				
				$error_msg='';
				$flag=$this->Model_catalogue->vendor_change_status($vendor_id,$status);
				
				if($flag){
					/* send mail to vendor */
					$data=get_vendor_details($vendor_id);
					
					if(!empty($data)){
						
						$email=$data->email;//vendor email
						$name=$data->name;//vendor email
						
						
						if($email!=''){
										$email_stuff_arr=email_stuff();
										$sub='';
										$msg='';
										if($status=='1'){
											$sub='Profile Approved Successfully';
											$msg.='Thanks for registering with us. Your profile has been <span style="color:green;"><b>Approved<b></span> successfully. ';
										}
										if($status=='2'){
											$sub='Profile is Onhold for Approval';
											$msg.='Thanks for registering with us. Your profile is  <b>Onhold</b>. For further details please contact the Admin. ';
										}
										if($status=='3'){
											$sub='Profile has been Rejected';
											$msg.='Your profile verification has been <span style="color:red;"><b>Rejected</b></span> by the Admin. For further details please contact the Admin. ';
										}
						
					$arr=array();
										$arr['name']=$name ;
										$arr['email']=$email;
										$arr['sub']=$sub;
										$arr['msg']=$msg;
										
										$usermessage=$this->load->view('template/notification_vendor_PROFILE',$arr,true);
	
										
					$mail = new PHPMailer;
					/*$mail->isSMTP();
					$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
					$mail->Port = '587';//  live - comment it 
					$mail->Auth = true;//  live - comment it 
					$mail->SMTPAuth = true;
					$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
					$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
					$mail->SMTPSecure = 'tls';*/
					$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
					$mail->FromName = $email_stuff_arr["name_emailtemplate"];
					$mail->addAddress($email);
					$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
					$mail->WordWrap = 50;
					$mail->isHTML(true);
					$mail->Subject = $sub;
					$mail->Body    = $usermessage;
					if($email != ''){
											@$mail->send();
											
					}
				}
					/* send mail to vendor */
					}
				}
				
				if($status!=''){
					$error_msg='Status updated successfully';
				}
	
				echo json_encode(array("flag"=>1,'message'=>$error_msg));
			}else{
				$this->load->view('admin/header');
				$this->load->view('admin/login');
			}
		}
		public function brands_change_status() {
			if($this->session->userdata("logged_in")){
	
				$status = $this->input->post('status');
				$brand_id = $this->input->post('brand_id');
				$vendor_id = $this->input->post('vendor_id');
				$user_type=$this->session->userdata("user_type");
		
				$error_msg='';
				$flag=$this->Model_catalogue->brands_change_status($brand_id,$vendor_id,$status);
				
				if($status=='3'){
					$fl=$this->update_search_index_brand($brand_id);
				}
	
				if($flag){
					/* send mail to vendor */
					$data=get_vendor_details($vendor_id);
					
					if(!empty($data)){
						
						$email=$data->email;//vendor email
						$name=$data->name;//vendor email
						
						
						if($email!=''){
										$email_stuff_arr=email_stuff();
										$sub='';
										$msg='';
										if($status=='1'){
											$sub='Your Brand is Approved Successfully';
											$msg.='Thanks for adding Brand with us. Your brand has been <span style="color:green;"><b>Approved<b></span> successfully. ';
										}
										if($status=='2'){
											$sub='Your Brand is Onhold for Approval';
											$msg.='Thanks for adding Brand with us. Your profile is  <b>Onhold</b>. For further details please contact the Admin. ';
										}
										if($status=='3'){
											$sub='Your Brand has been Rejected';
											$msg.='Your Brand verification has been <span style="color:red;"><b>Rejected</b></span> by the Admin. For further details please contact the Admin. ';
										}
						
					$arr=array();
										$arr['name']=$name ;
										$arr['email']=$email;
										$arr['sub']=$sub;
										$arr['msg']=$msg;
										
										$usermessage=$this->load->view('template/notification_vendor_PROFILE',$arr,true);
	
										
					$mail = new PHPMailer;
					/*$mail->isSMTP();
					$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
					$mail->Port = '587';//  live - comment it 
					$mail->Auth = true;//  live - comment it 
					$mail->SMTPAuth = true;
					$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
					$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
					$mail->SMTPSecure = 'tls';*/
					$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
					$mail->FromName = $email_stuff_arr["name_emailtemplate"];
					$mail->addAddress($email);
					$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
					$mail->WordWrap = 50;
					$mail->isHTML(true);
					$mail->Subject = $sub;
					$mail->Body    = $usermessage;
					if($email != ''){
											@$mail->send();
											
					}
				}
					/* send mail to vendor */
					}
				}
				
				if($status!=''){
					$error_msg='Status updated successfully';
				}
	
				echo json_encode(array("flag"=>1,'message'=>$error_msg));
			}else{
				$this->load->view('admin/header');
				$this->load->view('admin/login');
			}
		}
		public function inventory_change_status() {
			if($this->session->userdata("logged_in")){
	
				$status = $this->input->post('status');
				$inventory_id = $this->input->post('inventory_id');
				$vendor_id = $this->input->post('vendor_id');
				$comments = $this->input->post('comments');
				$user_type=$this->session->userdata("user_type");
		
				$error_msg='';
				$flag=$this->Model_catalogue->inventory_change_status($inventory_id,$vendor_id,$status,$comments );
				
				$this->update_search_index($inventory_id);
				
				if($flag){
					/* send mail to vendor */
					$data=get_vendor_details($vendor_id);
					//$data_inv=get
					
					if(!empty($data)){
						
						$email=$data->email;//vendor email
						$name=$data->name;//vendor email
						
						
						if($email!=''){
										$email_stuff_arr=email_stuff();
										$sub='';
										$msg='';
										if($status=='1'){
											$sub='SKU is Approved Successfully';
											$msg.='Thanks for adding SKU with us. Your SKU has been <span style="color:green;"><b>Approved<b></span> successfully. ';
										}
										if($status=='2'){
											$sub='SKU is Onhold for Approval';
											$msg.='Thanks for adding Brand with us. Your profile is  <b>Onhold</b>. For further details please contact the Admin. ';
										}
										if($status=='3'){
											$sub='SKU has been Rejected';
											$msg.='Your SKU verification has been <span style="color:red;"><b>Rejected</b></span> by the Admin. For further details please contact the Admin. ';
										}
						
					$arr=array();
										$arr['name']=$name ;
										$arr['email']=$email;
										$arr['sub']=$sub;
										$arr['msg']=$msg;
										
										$usermessage=$this->load->view('template/notification_vendor_PROFILE',$arr,true);
	
										
					$mail = new PHPMailer;
					/*$mail->isSMTP();
					$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
					$mail->Port = '587';//  live - comment it 
					$mail->Auth = true;//  live - comment it 
					$mail->SMTPAuth = true;
					$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
					$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
					$mail->SMTPSecure = 'tls';*/
					$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
					$mail->FromName = $email_stuff_arr["name_emailtemplate"];
					$mail->addAddress($email);
					$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
					$mail->WordWrap = 50;
					$mail->isHTML(true);
					$mail->Subject = $sub;
					$mail->Body    = $usermessage;
					if($email != ''){
											@$mail->send();
											
					}
				}
					/* send mail to vendor */
					}
				}
				
				if($status!=''){
					$error_msg='Status updated successfully';
				}
	
				echo json_encode(array("flag"=>1,'message'=>$error_msg));
			}else{
				$this->load->view('admin/header');
				$this->load->view('admin/login');
			}
		}

		public function add_vendor_catalog(){
		
			if($this->session->userdata("logged_in")){
				
				$this->load->library('upload');
				$files = $_FILES;
				
				/* brouchure update */
	
				$date = new DateTime();
				$mt=microtime();
				$salt="brouchure";
				$rand=mt_rand(1, 1000000);
				$ts=$date->getTimestamp();
				$str=$salt.$rand.$ts.$mt;
				$key=md5($str);
				$unique_id=$key;
	
	
				if(!empty($_FILES)){
					if (!file_exists($folder="product_docs")) {
						$mask=umask(0);
						mkdir($folder,0777);
						umask($mask);
					}
	
					//find extention
					$filename=$_FILES["brouchure"]["name"];
					$ext = pathinfo($filename, PATHINFO_EXTENSION);
	
					//echo $ext;
	
					if(move_uploaded_file($_FILES["brouchure"]["tmp_name"],$folder."/".$unique_id.'.'.$ext)){
						
						// if(file_exists($old_brouchure)){
						// 	unlink($old_brouchure);
						// }
						$brouchure=$folder."/".$unique_id.'.'.$ext;
						//fileupload_s3($ff);	
					}else{
	
						// $arr["status"]=false;
						// $arr["unique_id"]=$unique_id;
						// $arr["file_uploaded"]="no";
						$brouchure='';
					}
				}
				/* brouchure update */
				
				
				$sku_id=addslashes($this->input->post("sku_id"));
				
				if(!is_dir('assets/pictures/images/vendor_products/'.$sku_id)){
					mkdir('assets/pictures/images/vendor_products/'.$sku_id);
				}
				
				//print_r($files);

	
				$common_image='';
				$common_image_name=$files['common_image']['name'];
				$previous_image=$this->input->post("previous_image");
				if($common_image_name!=""){
					
					if(file_exists($previous_image)) {
						unlink($previous_image);			   
					}
					$c_ext = pathinfo($common_image_name, PATHINFO_EXTENSION);
					$c_filen=rand(10,100000);
					
					$_FILES['common_image']['name']=$c_filen.".".$c_ext;
					$_FILES['common_image']['type']= $files['common_image']['type'];
					$_FILES['common_image']['tmp_name']= $files['common_image']['tmp_name'];
					$_FILES['common_image']['error']= $files['common_image']['error'];
					$_FILES['common_image']['size']= $files['common_image']['size'];  
					$this->upload->initialize($this->vendor_set_upload_options_config($sku_id,300,366));
					
					if (!$this -> upload -> do_upload("common_image")) {
						echo $this -> upload -> display_errors();//exit;
					} else {
						$upload_data = $this -> upload -> data();
						$common_image = "assets/pictures/images/vendor_products/" .$sku_id."/". $upload_data['file_name'];
					}
				}else{
					
					$common_image=$this->input->post("previous_image");	  
					
				}
				
				
				$image_arr=[];
				$thumbnail_arr=[];
				$largeimage_arr=[];
				
				for($i=1; $i<=6; $i++){
	
					
					$filename=$files['image'.$i]['name']; 
					if($filename==""){
						continue;
					}			
						
					$ext = pathinfo($filename, PATHINFO_EXTENSION);
					$filen=rand(10,100000);
					$previous_image=$this->input->post("previous_image".$i);
					if($_FILES['image'.$i]['name']!=''){
						$_FILES['image'.$i]['name']=$filen.".".$ext;
						$_FILES['image'.$i]['type']= $files['image'.$i]['type'];
						$_FILES['image'.$i]['tmp_name']= $files['image'.$i]['tmp_name'];
						$_FILES['image'.$i]['error']= $files['image'.$i]['error'];
						$_FILES['image'.$i]['size']= $files['image'.$i]['size'];  
						$this->upload->initialize($this->vendor_set_upload_options_config($sku_id,420,512));

						if (!$this -> upload -> do_upload("image".$i)) {
							echo $this -> upload -> display_errors();//exit;
						} else {
							$upload_data = $this -> upload -> data();
							$image = "assets/pictures/images/vendor_products/" .$sku_id."/".$upload_data['file_name'];
							$image_arr[]=$image;
						}
					}else{
						if($this->input->post("previous_image".$i)==""){
							if($this->input->post("previous_image".$i."_unlink")!=""){
								if(file_exists($this->input->post("previous_image".$i."_unlink"))){
								 unlink($this->input->post("previous_image".$i."_unlink"));
								}
							}
							$image="";
						}
						else{
							$image=$this->input->post("previous_image".$i);
						}
						$image_arr[]=$image;
					 }
					 
					/////


					//////////////////////////////
					$filename1=$files['thumbnail'.$i]['name']; 
					if($filename1!=''){ 
					$ext1 = pathinfo($filename1, PATHINFO_EXTENSION);
					$filen1=rand(10,100000);
					$_FILES['thumbnail'.$i]['name']= $filen1.".".$ext1;
					$_FILES['thumbnail'.$i]['type']= $files['thumbnail'.$i]['type'];
					$_FILES['thumbnail'.$i]['tmp_name']= $files['thumbnail'.$i]['tmp_name'];
					$_FILES['thumbnail'.$i]['error']= $files['thumbnail'.$i]['error'];
					$_FILES['thumbnail'.$i]['size']= $files['thumbnail'.$i]['size'];
					$this->upload->initialize($this->vendor_set_upload_options_config($sku_id,100,122));

					if (!$this -> upload -> do_upload("thumbnail".$i)) {
						echo $this -> upload -> display_errors();//exit;
					} else {
						$upload_data = $this -> upload -> data();
						$thumbnail = "assets/pictures/images/vendor_products/" .$sku_id."/". $upload_data['file_name'];
						$thumbnail_arr[]=$thumbnail;
					}
				}else{
					
					if($this->input->post("previous_thumbnail".$i)==""){
						if($this->input->post("previous_thumbnail".$i."_unlink")!=""){
							if(file_exists($this->input->post("previous_thumbnail".$i."_unlink"))){
							 unlink($this->input->post("previous_thumbnail".$i."_unlink"));
							}
						}
						 $thumbnail="";
					}
					else{
						$thumbnail=$this->input->post("previous_thumbnail".$i);
					}
					$thumbnail_arr[]=$thumbnail;
				 }
				 
					////
					////////////////////



					$filename2=$files['largeimage'.$i]['name'];  

					if($filename2!=''){
					$ext1 = pathinfo($filename2, PATHINFO_EXTENSION);
					$filen1=rand(10,100000);
					$_FILES['largeimage'.$i]['name']= $filen1.".".$ext1;
					$_FILES['largeimage'.$i]['type']= $files['largeimage'.$i]['type'];
					$_FILES['largeimage'.$i]['tmp_name']= $files['largeimage'.$i]['tmp_name'];
					$_FILES['largeimage'.$i]['error']= $files['largeimage'.$i]['error'];
					$_FILES['largeimage'.$i]['size']= $files['largeimage'.$i]['size'];
					$this->upload->initialize($this->vendor_set_upload_options_config($sku_id,850,1036));
					//////////////////////////
					
					if (!$this -> upload -> do_upload("largeimage".$i)) {
						echo $this -> upload -> display_errors();//exit;
					} else {
						$upload_data = $this -> upload -> data();
						$largeimage = "assets/pictures/images/vendor_products/" .$sku_id."/". $upload_data['file_name'];
						$largeimage_arr[]=$largeimage;
					}
				}else{
					
					if($this->input->post("previous_largeimage".$i)==""){
						if($this->input->post("previous_largeimage".$i."_unlink")!=""){
							if(file_exists($this->input->post("previous_largeimage".$i."_unlink"))){
							 unlink($this->input->post("previous_largeimage".$i."_unlink"));
							}
						}
						 $largeimage="";
					}
					else{
						$largeimage=$this->input->post("previous_largeimage".$i);
					}
					$largeimage_arr[]=$largeimage;
				 }
				 

				}//forloop
				
				$sku_id=$this->db->escape_str($this -> input -> post("sku_id"));
				
				$vendor_id=$this->session->userdata("user_id");
				$offer_price=$this->db->escape_str($this -> input -> post("offer_price"));//custom
				
				$moq=$this->db->escape_str($this -> input -> post("moq"));
				$max_oq=$this->db->escape_str($this -> input -> post("max_oq"));
				
				$stock=$this->db->escape_str($this -> input -> post("stock"));		
			
				$weight_per_unit=$this->input->post("weight_per_unit");
				
				$highlight=$this->input->post("highlight");
				$shipswithin=$this->db->escape_str($this -> input -> post("shipswithin"));
	
				$sku_name=addslashes($this->input->post("sku_name"));
				$product_status=addslashes($this->input->post("product_status"));
							
				/* added fields */
	
	
				$under_pcat_frontend=$this->input->post("under_pcat_frontend");
				if(!empty($under_pcat_frontend)){
					$under_pcat_frontend=implode(',',$under_pcat_frontend);
				}
				//$brouchure=$this->input->post("brouchure");
				$emi_options=$this->input->post("emi_options");
				$price_validity=$this->input->post("price_validity");
				$material=$this->input->post("material");
				
	
				// print_r($image_arr);
				// print_r($thumbnail_arr);
				// print_r($largeimage_arr);

						// $image_arr=['','','','','',''];	
						// $thumbnail_arr=['','','','','',''];	
						// $largeimage_arr=['','','','','',''];	

						$action=$this->input->post("action");
						
						if($action=='edit'){
							
							$vendor_catalog_id=	$this->input->post("vendor_catalog_id");
						}else{
							$vendor_catalog_id='';
						}

						$category_id=$this->input->post("category_id");
						$admin_margin_percentage=$this->input->post("admin_margin_percentage");
						$admin_margin_value=$this->input->post("admin_margin_value");


						$manufacture_days_per_piece=$this->input->post("manufacture_days_per_piece");
						$sku_available_date=$this->input->post("sku_available_date");
						$discontinued_date=$this->input->post("discontinued_date");
						$discontinuation_comments=$this->input->post("discontinuation_comments");

						$weight_per_unit=$this->input->post("weight_per_unit");
						$dimension_of_furniture=$this->input->post("dimension_of_furniture");
						
						$inventory_unit=$this->input->post("inventory_unit");
						$inventory_length=$this->input->post("inventory_length");
						$inventory_height=$this->input->post("inventory_height");
						$inventory_breadth=$this->input->post("inventory_breadth");
						$old_product_status =$this->input->post("old_product_status");


				$flag=$this->Model_catalogue->add_vendor_catalog($sku_id,$common_image,$image_arr,$thumbnail_arr,$largeimage_arr,$vendor_id,$offer_price,$moq,$max_oq,$weight_per_unit,$stock,$product_status,$highlight,$sku_name,$shipswithin,$under_pcat_frontend,$brouchure,$emi_options,$price_validity,$material,$vendor_catalog_id,$category_id,$admin_margin_percentage,$admin_margin_value,$manufacture_days_per_piece,$sku_available_date,$discontinuation_comments,$discontinued_date,$dimension_of_furniture,$inventory_unit,$inventory_length,$inventory_height,$inventory_breadth,$old_product_status);
				
				if($flag==true){

					$fin_flag=true;
					if($product_status!='' && $product_status!=$old_product_status && $old_product_status!='' && $old_product_status!=null){
						if($vendor_catalog_id!=''){
							$res_data=get_tagged_info($vendor_catalog_id);
							$inv_id=$res_data->id;
							
							if($product_status=='Discontinued'){
								$index_flag=$this->delete_search_index($inv_id);
							}else{
								$index_flag=$this->update_search_index($inv_id);
							}
							

							if($index_flag==true){
								$fin_flag=true;
							}else{
								$fin_flag=0;
							}
						}

					}
					

					/* Send Notification Mail to admin */
						/* send mail to vendor */
		$data=get_product_vendor_details($vendor_id);
				//$data_inv=get
		
		if(!empty($data)){
			
			$email=$data->email;//vendor email
			$name=$data->name;//vendor email
			$mobile=$data->mobile;//vendor email
			
			$ventor_details='';

			$ventor_details='The details are given below, you can also check in the admin panel';

			$ventor_details.='<table>';
			$ventor_details.='<thead colspan="3"><h3>Vendor Details</h3></thead>';
			
			$ventor_details.='<tbody>';

			$ventor_details.='<tr><td><b>Name</b></td><td>:</td><td>'.$name.'</td></tr>';
			$ventor_details.='<tr><td><b>Email</b></td><td>:</td><td>'.$email.'</td></tr>';
			$ventor_details.='<tr><td><b>Mobile</b></td><td>:</td><td>'.$mobile.'</td></tr>';
			
			$ventor_details.='</tbody>';

			$ventor_details.='<thead colspan="3"><h3>SKU Details</h3></thead>';
			
			$ventor_details.='<tbody>';

			$ventor_details.='<tr><td><b>SKU ID</b></td><td>:</td><td>'.$sku_id.'</td></tr>';
			$ventor_details.='<tr><td><b>SKU Name</b></td><td>:</td><td>'.$sku_name.'</td></tr>';
			
			$ventor_details.='</tbody>';


			
			$ventor_details.='</table>';
			
			$adm_email=get_email_of_admin();

			if($adm_email!=''){
				$email_stuff_arr=email_stuff();
				$sub='';
				$msg='';
				
				$sub='New SKU is added by a Vendor';
				$msg.=$ventor_details;
						
					
				$arr=array();
				$arr['name']="Admin" ;
				$arr['email']=$adm_email;//not used
				$arr['sub']=$sub;
				$arr['msg']=$msg;
				
				$usermessage=$this->load->view('template/notification_vendor_PROFILE',$arr,true);

									
				$mail = new PHPMailer;
				/*$mail->isSMTP();
				$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
				$mail->Port = '587';//  live - comment it 
				$mail->Auth = true;//  live - comment it 
				$mail->SMTPAuth = true;
				$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
				$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
				$mail->SMTPSecure = 'tls';*/
				$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
				$mail->FromName = $email_stuff_arr["name_emailtemplate"];
				//$adm_email
				$mail->addAddress('nanthinivinothkumar0227@gmail.com');
				$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				$mail->Subject = $sub;
				$mail->Body    = $usermessage;
				if($adm_email != ''){
										@$mail->send();
										
				}
			}
		}// vendor data
				/* send mail to vendor */

					/* Send Notification Mail to admin */

					echo $fin_flag;

					
					
				}else{
					echo 0;
				}
				
			}else{
				$this->load->view('admin/header');
				$this->load->view('admin/login');
			}
			
		}


	public function vendor_inventory_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_catalogue->vendor_inventory_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_catalogue->vendor_inventory_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->vendor_catalog_id.'">';
				$var='';
				$row[]='<img src="'.base_url().$queryRecordsObj -> inv_image.'" width="100" alt="Image Not Found"><br>';

				$admin_user_type=$this->session->userdata("user_type");
				
				if($admin_user_type!='vendor'){

					$v_details='';
					$v_details.='Vendor Name : '.$queryRecordsObj -> name;
					$v_details.='<br>Email : '.$queryRecordsObj -> email;
					$v_details.='<br>Mobile : '.$queryRecordsObj -> mobile;
					$v_details.='<br>Address';
					$v_details.='<br>'.$queryRecordsObj -> city.','.$queryRecordsObj -> state.',<br>'.$queryRecordsObj -> pincode;
					$row[]=$v_details;
				}

				$val='';
				$val.='SKU Id : '.$queryRecordsObj -> sku_id;
				$val.='<br>SKU Name : '.$queryRecordsObj -> sku_name;
				
				$val.='<br><br>Category name : '.$queryRecordsObj -> catagory_name;
				//$val.='<br>SKU Name : '.$queryRecordsObj -> sku_name;
				$val.='<br>Material Type of SKU: '.ucfirst($queryRecordsObj -> material);
				$val.='<br>Selling Price For End Customer (Incl. All Taxes) (Rs.): '.$queryRecordsObj -> offer_price;
				$val.='<br>'.SITE_NAME.' Margin Percentage(%):'.$queryRecordsObj -> admin_margin_percentage.' %';
				
				$val.='<br>Actual Margin (Rs.): '.$queryRecordsObj -> admin_margin_value;
				
				$val.='<br>Price Validity : '.$queryRecordsObj -> price_validity;
				$val.='<br>Weight Per Unit : '.$queryRecordsObj -> weight_per_unit;
				$val.='<br>Dimension Of Furniture (L X W X H) : '.$queryRecordsObj -> dimension_of_furniture;
				$val.='<br>Shipped In XX Number Of Days : '.$queryRecordsObj -> shipswithin . ' days';
				$val.='<br>EMI Offer : '.$queryRecordsObj -> emi_options;
				
				if($queryRecordsObj->brouchure!=''){
				$val.='<br><u class="text-info"><a href="'.base_url().$queryRecordsObj->brouchure.'" target="_blank"><i class="fa fa-paperclip" aria-hidden="true"></i> Brouchure</a></u>';
				}

				$row[]=$val;				
				$val2='';				
				
				$val2.='MOQ: '.$queryRecordsObj -> moq.'<br>';
				$val2.='Max OQ: '.$queryRecordsObj -> max_oq.'<br>';
				//$val2.='Current Stock:'.$queryRecordsObj -> stock.'<br>';
				
				$val2.='Stock Status:<b>'.$queryRecordsObj -> product_status.'</b><br>';

				if($queryRecordsObj -> product_status=='In Stock'){
					$val2.='Current Stock:<b>'.$queryRecordsObj -> stock.'</b><br>';

				}elseif($queryRecordsObj -> product_status=='Made To Order'){
					$val2.='Days To Manufacture One Piece:<b>'.$queryRecordsObj -> manufacture_days_per_piece.'</b><br>';
					
				}elseif($queryRecordsObj -> product_status=='Out Of Stock'){
					$val2.='SKU Available Date:<b>'.$queryRecordsObj -> sku_available_date.'</b><br>';
					
				}elseif($queryRecordsObj -> product_status=='Discontinued'){
					$val2.='Discontinued Date:<br>'.$queryRecordsObj -> discontinued_date.'<br>';
					$val2.='Discontinuation Comments:<br>'.$queryRecordsObj -> discontinuation_comments.'<br><br>';
				}

				$val2.='Updated On :'. date('d/m/Y H:i:s',strtotime($queryRecordsObj -> inv_timestamp)).'<br>';
				
				$txt='';$str='';
				if($queryRecordsObj->inv_approval_status=='0' || $queryRecordsObj->inv_approval_status==''){
					$txt.='<span style="color:blue"> Yet to process </span>';
				}else if($queryRecordsObj->inv_approval_status=='1'){
					$txt.='<span style="color:green"> <b>Approved </b></span>'.'<br> <small> On '.date('d/m/Y H:i:s',strtotime($queryRecordsObj->status_on)).'</small>';
				}else if($queryRecordsObj->inv_approval_status=='2'){
					$txt.='<span style="color:red"><b>On Hold </b></span>'.'<br> <small> On '.date('d/m/Y H:i:s',strtotime($queryRecordsObj->status_on)).'</small>';;
				}else if($queryRecordsObj->inv_approval_status=='3'){
					
					$txt.='<span style="color:red"><b>Rejected </b></span>'.'<br> <small> On '.date('d/m/Y H:i:s',strtotime($queryRecordsObj->status_on)).'</small>';

					$txt.='<br><u>Rejected comments</u> : <br>';
					$txt.=''.$queryRecordsObj->rejection_comments;

				}
	
				$str.="<br>Admin Remarks :<br>".  $txt.'<br>';

				//print_r($queryRecordsObj);

				$row[]=$val2.$str;	

				$user_type=$this->session->userdata("user_type");
				$link='';
				if($user_type!='vendor'){

					if($queryRecordsObj->notification_read_status=='0'){
						$link.='<button data-defaultText="Newly added" style="background:red" title="Delete Notification" id="badge_'.$queryRecordsObj->vendor_catalog_id.'" class="badge notification_btn" onclick="delete_notification('.$queryRecordsObj->vendor_catalog_id.')">Newly added</button><br>';
					}


					$link.='<button class="btn btn-warning" onclick="view_status_modal('.$queryRecordsObj->vendor_id.','.$queryRecordsObj->vendor_catalog_id.','.$queryRecordsObj->inv_approval_status.')">Update status</button>';
					
					$tag_inv=get_tagged_info($queryRecordsObj->vendor_catalog_id);

					if(!empty($tag_inv)){
						$inventory_id=$tag_inv->id;
						$product_id=$tag_inv->product_id;
						$link.='<a class="btn btn-success" href="'.base_url().'admin/Catalogue/edit_inventory_form/'.$inventory_id.'/'.$product_id.'"> Go To Tagged Inventory</a>';

					}else{
						if($queryRecordsObj->inv_approval_status=='1'){
							$link.='<button class="btn btn-info" onclick="view_cat_modal('.$queryRecordsObj->vendor_id.','.$queryRecordsObj->vendor_catalog_id.','.$queryRecordsObj->category_id.','."'".$queryRecordsObj -> sku_id."'".')">Tag to Vommet Inventory</button>';
						}
					}


				}

				if($user_type=='vendor'){
				$link.='<input type="button" class="btn btn-warning btn-xs btn-block" value="Edit" onClick="editInventoryFun('.$queryRecordsObj->vendor_catalog_id.')">';
				}
				$row[]=$link;

				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function vendor_inventory(){
		
		if($this->session->userdata("logged_in")){

            $data_posted=$this->input->post();
			$data["controller"]=$this;			
			$data["menu_flag"]="vendor_inventory";	
			$data['parent_category'] = $this->Model_catalogue->parent_category();
			
			$data["vendors"]=$this->Model_catalogue->get_vendors_data_obj();
			$data["vendors_city_based"]=$this->Model_catalogue->get_vendors_data_obj_city_based();

			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/vendor_inventory_view',$data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}	
	
	public function edit_vendor_catalog_inventory($vendor_catalog_id){
		if($this->session->userdata("logged_in")){
			
			$vendor_id=$this->session->userdata("user_id");
			
			$data["vendor_id"]=$vendor_id;
			$data["menu_flag"]="edit_vendor_catalog";

			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$data['inventories'] = $this->Model_catalogue->get_data_of_catalog_vendor($vendor_catalog_id);


			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_vendor_catalog_inventory',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function create_vendor_catalog_inventory($vendor_id=''){
		if($this->session->userdata("logged_in")){
			
			if($vendor_id!=''){
				//vendor login
			}else{
				$vendor_id=$this->session->userdata("user_id");
			}
			
			$data["vendor_id"]=$vendor_id;
			$data["menu_flag"]="create_vendor_catalog";

			$data['parent_category'] = $this->Model_catalogue->parent_category();
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/vendor_catalog_inventory',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function delete_catalog_notification(){
		if($this->session->userdata("logged_in")){

			$id = $this->input->post('id');
			
			$flag=$this->Model_catalogue->delete_catalog_notification($id);
			echo json_encode(array("flag"=>1,'message'=>"Notification is deleted Successfully"));
		}
	}
	public function vendor_inventory_change_status() {
		if($this->session->userdata("logged_in")){

			$status = $this->input->post('status');
			$inventory_id = $this->input->post('inventory_id');
			$vendor_id = $this->input->post('vendor_id');
			$comments = $this->input->post('comments');
			$user_type=$this->session->userdata("user_type");
	
			$error_msg='';
			$flag=$this->Model_catalogue->vendor_inventory_change_status($inventory_id,$vendor_id,$status,$comments );
			
			//$this->update_search_index($inventory_id);
			
			if($flag){
				/* send mail to vendor */
				$data=get_vendor_details($vendor_id);
				//$data_inv=get
				
				if(!empty($data)){
					
					$email=$data->email;//vendor email
					$name=$data->name;//vendor email
					
					
					if($email!=''){
									$email_stuff_arr=email_stuff();
									$sub='';
									$msg='';
									if($status=='1'){
										$sub='SKU is Approved Successfully';
										$msg.='Thanks for adding SKU with us. Your SKU has been <span style="color:green;"><b>Approved<b></span> successfully. ';
									}
									if($status=='2'){
										$sub='SKU is Onhold for Approval';
										$msg.='Thanks for adding Brand with us. Your profile is  <b>Onhold</b>. For further details please contact the Admin. ';
									}
									if($status=='3'){
										$sub='SKU has been Rejected';
										$msg.='Your SKU verification has been <span style="color:red;"><b>Rejected</b></span> by the Admin. For further details please contact the Admin. ';
									}
					
				$arr=array();
									$arr['name']=$name ;
									$arr['email']=$email;
									$arr['sub']=$sub;
									$arr['msg']=$msg;
									
									$usermessage=$this->load->view('template/notification_vendor_PROFILE',$arr,true);

									
				$mail = new PHPMailer;
				/*$mail->isSMTP();
				$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
				$mail->Port = '587';//  live - comment it 
				$mail->Auth = true;//  live - comment it 
				$mail->SMTPAuth = true;
				$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
				$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
				$mail->SMTPSecure = 'tls';*/
				$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
				$mail->FromName = $email_stuff_arr["name_emailtemplate"];
				$mail->addAddress($email);
				$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				$mail->Subject = $sub;
				$mail->Body    = $usermessage;
				if($email != ''){
										@$mail->send();
										
				}
			}
				/* send mail to vendor */
				}
			}
			
			if($status!=''){
				$error_msg='Status updated successfully';
			}

			echo json_encode(array("flag"=>1,'message'=>$error_msg));
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function tag_vendor_inventory() {
		if($this->session->userdata("logged_in")){

			$post_arr = $this->input->post();
			//$vendor_id = $this->input->post('vendor_id');
			
			$error_msg='';
			$inv_arr=$this->Model_catalogue->tag_vendor_inventory($post_arr);
			
			$inv_id='';
			$product_id='';
			if(!empty($inv_arr)){
				$inv_id=$inv_arr['inv_id'];
				$product_id=$inv_arr['product_id'];
				$msg='Vendor Inventory is added successfully. You can edit for further details';

				$index_flag=$this->create_search_index($inv_id);
				if($index_flag==true){
					//echo true;
				}else{
					//echo 0;
				}
				
			}else{
				$msg="Error. Please try again";
			}

			echo json_encode(array("flag"=>1,'message'=>$msg,'inv_id'=>$inv_id,'p_id'=>$product_id));
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function add_tagged_invs_combo($inv_id){
	if($this->session->userdata("logged_in")){
		if($inv_id!=''){
			$res=get_all_inventory_data($inv_id);
		}else{
			redirect(base_url('admin/Catalogue/inventory_all'));
		}
		$data['inv_obj']=$res;

		$res_obj=get_inventory_tagged_products_combo($inv_id);
		
		$data['parent_category'] = $this->Model_catalogue->parent_category();
		$data["menu_flag"]="catalog_active_links";
		$data["tagged_invs"]=$res_obj;
		$data["inv_id"]=$inv_id;
		
		$this -> load -> view('admin/vmanage',$data);
		$this -> load -> view('admin/tagged_invs/add_tagged_invs_combo',$data);
		
	}else{
		$this->load->view('admin/header');
		$this->load->view('admin/login');
	}
}

public function add_tagged_invs_combo_form_submit(){
	if($this->session->userdata("logged_in")){
			$data_posted=$this->input->post();
			//print_r($data_posted);exit;
			$inv_id=$this->input->post('tagged_main_inventory_id');
			$flag=$this->Model_catalogue->add_tagged_invs_combo($data_posted);
			
			if($flag){
				if($inv_id!=''){
					$msg='SKUs are tagged successfully';
				}else{
					$msg='SKUs are tagged successfully';
				}
				$this->session->set_flashdata('notification',$msg);
				
				redirect(base_url("admin/Catalogue/add_tagged_invs_combo/".$inv_id));
			}

	}else{
		redirect(base_url('Administrator'));
	}
}
public function delete_tagged_invs_combo() {
	if($this->session->userdata("logged_in")){
		$tagged_id = $this->input->post('tagged_id');
		$error_msg='';
		$flag=$this->Model_catalogue->delete_tagged_invs_combo($tagged_id);
		if($flag){
			$error_msg='Tagged Inventory Deleted Successfully';
		}
		echo json_encode(array("flag"=>1,'message'=>$error_msg));
	}else{
		$this->load->view('admin/header');
		$this->load->view('admin/login');
	}
}
public function update_addon_product_allowed(){

	if($this->session->userdata("logged_in")){

		$id=$this->db->escape_str($this -> input -> post("id"));
		$addon_product_allowed=$this->db->escape_str($this -> input -> post("addon_product_allowed"));
		$addon_selection_type=$this->db->escape_str($this -> input -> post("addon_selection_type"));

		$flag=$this->Model_catalogue->update_addon_product_allowed($id,$addon_product_allowed,$addon_selection_type);
		
		if($flag==true){
			echo true;
		}else{
			echo 0;
		}

	}else{
		$this->load->view('admin/header');
		$this->load->view('admin/login');
	}

}
	
}
