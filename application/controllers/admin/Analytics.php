<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Analytics extends CI_Controller {
    private $pagesize = 20;
    function __construct() {
        parent::__construct();
        $this -> load -> model('admin/Analytics_model');
		$this->load->helper('emailsmstemplate');
		$this->load->helper('general');
    }

    public function index() {
        if ($this -> session -> userdata("logged_in")) {
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/analytics/index.php');
        } else {
            redirect(base_url());
        }
    }
	public function orders() {
        if($this -> session -> userdata("logged_in")){
            $data['menu_flag']='analytics_orders';
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/analytics/orders.php');
        }else{
            redirect(base_url());
        }
    }
	
	 public function returns() {

        if($this -> session -> userdata("logged_in")){
            $data['menu_flag']='analytics_returns';
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/analytics/returns.php');
        }else{
            redirect(base_url());
        }
    }
	
	 public function promotions() {

        if($this -> session -> userdata("logged_in")){
            $data['menu_flag']='analytics_promotions';
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/analytics/promotions.php');
        }else{
            redirect(base_url());
        }
    }
	
	 public function customers() {

        if($this -> session -> userdata("logged_in")){
            $data['menu_flag']='analytics_customers';
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/analytics/customers.php');
        }else{
            redirect(base_url());
        }
    }
	
	 public function inventory() {

        if($this -> session -> userdata("logged_in")){
            $data['menu_flag']='analytics_inventory';
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/analytics/inventory.php');
        }else{
            redirect(base_url());
        }
    }
	
	 public function visitor_clicks() {

        if($this -> session -> userdata("logged_in")){
            $data['menu_flag']='analytics_visitor_clicks';
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/analytics/visitor_clicks.php');
        }else{
            redirect(base_url('Administrator'));
        }
    }
	public function get_businessscore() {

        if($this -> session -> userdata("logged_in")){
            $this -> load -> view('admin/vmanage');
            $this -> load -> view('admin/analytics/get_businessscore.php');
        }else{
            redirect(base_url());
        }
    }

    public function getOrderReport() {
      
         $report_type = $this -> input -> post('report_type');
        
        $order = [];
        $from_date = $this -> input -> post('from_date');
        $to_date = $this -> input -> post('to_date');
        $limit_result_val = $this -> input -> post('limit_result_val');
        $select_levelTxt = $this -> input -> post('select_levelTxt');
        if ($limit_result_val == "none") {
            $limit_result_val = "";
        }
        $select_level = $this -> input -> post('select_level');
        $particular_year = $this -> input -> post('particular_year');

        $limit_result_order_val = $this -> input -> post('limit_result_order_val');
        if ($limit_result_order_val == "none") {
            $limit_result_order_val = "";
        }
        if ($report_type == "ordersStages") {
            $orderss = $this -> Analytics_model -> ordersStages($from_date, $to_date);
            $order_array=array();
            if($from_date=="" && $to_date==""){
                $order_array['timeRange']=" for Today";
            }
        
        
            if($from_date!="" && $to_date!=""){
                $order_array['timeRange']=" from ".date("D,j M Y ", strtotime($from_date))." to ".date("D,j M Y ", strtotime($to_date));
            }
            if($from_date==$to_date &&($from_date!="" && $to_date!="")){
                $order_array['timeRange']=" on ".date("D,j M Y", strtotime($from_date));
            }
            if($from_date=="" && $to_date!=""){
                $order_array['timeRange']=" before ".date("D,j M Y ", strtotime($to_date));
            }
            if($from_date!="" && $to_date==""){
                $order_array['timeRange']=" from ".date("D,j M Y", strtotime($from_date))." till today";
            }
            $order_array['data']=$orderss;
            $order[]=$order_array;
            
            
        }
        if ($report_type == "dailyPurchases") {
            $orderss = $this -> Analytics_model -> get_daily_purchases($from_date, $to_date);
            $order_array=array();
            if($from_date=="" && $to_date==""){
                $order_array['timeRange']=" for Entire history";
            }
        
        
            if($from_date!="" && $to_date!=""){
                $order_array['timeRange']=" from ".date("D,j M Y ", strtotime($from_date))." to ".date("D,j M Y ", strtotime($to_date));
            }
            if($from_date==$to_date &&($from_date!="" && $to_date!="")){
                $order_array['timeRange']=" on ".date("D,j M Y", strtotime($from_date));
            }
            if($from_date=="" && $to_date!=""){
                $order_array['timeRange']=" before ".date("D,j M Y ", strtotime($to_date));
            }
            if($from_date!="" && $to_date==""){
                $order_array['timeRange']=" from ".date("D,j M Y", strtotime($from_date))." till today";
            }
            $order_array['datass']=$orderss;
            $order[]=$order_array;
            
        }
        /*
		cancelledOrders old coding
		if ($report_type == "cancelledOrders") {
            $order = $this -> Analytics_model -> getCancelledOrders($from_date, $to_date);
        }*/
		
		if ($report_type == "cancelledOrdersNew") {
            $orderss = $this -> Analytics_model -> cancelledOrdersNewFun($from_date, $to_date);
            $order_array=array();
            if($from_date=="" && $to_date==""){
                $order_array['timeRange']=" for Today";
            }
        
        
            if($from_date!="" && $to_date!=""){
                $order_array['timeRange']=" from ".date("D,j M Y ", strtotime($from_date))." to ".date("D,j M Y ", strtotime($to_date));
            }
            if($from_date==$to_date &&($from_date!="" && $to_date!="")){
                $order_array['timeRange']=" on ".date("D,j M Y", strtotime($from_date));
            }
            if($from_date=="" && $to_date!=""){
                $order_array['timeRange']=" before ".date("D,j M Y ", strtotime($to_date));
            }
            if($from_date!="" && $to_date==""){
                $order_array['timeRange']=" from ".date("D,j M Y", strtotime($from_date))." till today";
            }
            $order_array['data']=$orderss;
            $order[]=$order_array;
            
            
        }
		
        if ($report_type == "dailyPurchasesComplete") {
            $orderss = $this -> Analytics_model -> get_daily_purchases_complete($from_date, $to_date);
            $order_array=array();
            if($from_date=="" && $to_date==""){
                $order_array['timeRange']=" for Entire history";
            }
        
        
            if($from_date!="" && $to_date!=""){
                $order_array['timeRange']=" from ".date("D,j M Y ", strtotime($from_date))." to ".date("D,j M Y ", strtotime($to_date));
            }
            if($from_date==$to_date &&($from_date!="" && $to_date!="")){
                $order_array['timeRange']=" on ".date("D,j M Y", strtotime($from_date));
            }
            if($from_date=="" && $to_date!=""){
                $order_array['timeRange']=" before ".date("D,j M Y ", strtotime($to_date));
            }
            if($from_date!="" && $to_date==""){
                $order_array['timeRange']=" from ".date("D,j M Y", strtotime($from_date))." till today";
            }
            $order_array['datass']=$orderss;
            $order[]=$order_array;
            
        }
        if ($report_type == "paymentMethods") {
            $order_arr = $this -> Analytics_model -> getPaymentMethodAnalytics($from_date, $to_date);
            $arrars_credit = [];
            $arrars_credit['name']="Credit Card";
            $arrars_credit['y']=0;
            $arrars_wallet = [];
            $arrars_wallet['name']="Wallet";
            $arrars_wallet['y']=0;
            $arrars_debit = [];
            $arrars_debit['name']="Debit Card";
            $arrars_debit['y']=0;
            $arrars_net = [];
            $arrars_net['name']="Net Banking";
            $arrars_net['y']=0;
            $arrars_coupon = [];
            $arrars_coupon['name']="Coupons";
            $arrars_coupon['y']=0;
            $arrars_emi = [];
            $arrars_emi['name']="EMI";
            $arrars_emi['y']=0;
            $arrars_cod = [];
            $arrars_cod['name']="COD";
            $arrars_cod['y']=0;
			$arrars_paytm = [];
            $arrars_paytm['name']="Paytm";
            $arrars_paytm['y']=0;
            $totalvalue=0;
            if(!empty($order_arr)){
                foreach($order_arr as $data){
                
                    if(strtolower($data['name'])=="credit card"){
                        $arrars_credit['y'] = $arrars_credit['y']+intval($data['total']);
                        $totalvalue+=intval($data['total']);
                    }
                    if(strtolower($data['name'])=="wallet"){
                        $arrars_wallet['y'] = $arrars_wallet['y']+intval($data['total']);
                        $totalvalue+=intval($data['total']);
                    }
                    if(strtolower($data['name'])=="debit card"){
                        $arrars_debit['y'] = $arrars_debit['y']+intval($data['total']);
                        $totalvalue+=intval($data['total']);
                    }
                    if(strtolower($data['name'])=="net banking"){
                        $arrars_net['y'] = $arrars_net['y']+intval($data['total']);
                        $totalvalue+=intval($data['total']);
                    }
                    if(strtolower($data['name'])=="coupon"){
                        $arrars_coupon['y'] = $arrars_coupon['y'] + intval($data['total']);
                        $totalvalue+=intval($data['total']);
                    }
                    if(strtolower($data['name'])=="emi"){
                        $arrars_emi['y'] =$arrars_emi['y']+intval($data['total']);
                        $totalvalue+=intval($data['total']);
                    }
                    if(strtolower($data['name'])=="cod"){
                        $arrars_cod['y'] =$arrars_cod['y']+ intval($data['total']);
                        $totalvalue+=intval($data['total']);
                    }
					if(strtolower($data['name'])=="paytm"){
                        $arrars_paytm['y'] =$arrars_paytm['y']+ intval($data['total']);
                        $totalvalue+=intval($data['total']);
                    }
                }
            }
            
        $orderss=array();
        array_push($orderss,$arrars_credit,$arrars_wallet,$arrars_debit,$arrars_net,$arrars_coupon,$arrars_emi,$arrars_cod,$arrars_paytm);
        $order_array=array();
            if($from_date=="" && $to_date==""){
                $order_array['timeRange']="Entire history";
            }
        
        
            if($from_date!="" && $to_date!=""){
                $order_array['timeRange']=" from ".date("D,j M Y ", strtotime($from_date))." to ".date("D,j M Y ", strtotime($to_date));
            }
            if($from_date==$to_date &&($from_date!="" && $to_date!="")){
                $order_array['timeRange']=" on ".date("D,j M Y", strtotime($from_date));
            }
            if($from_date=="" && $to_date!=""){
                $order_array['timeRange']=" before ".date("D,j M Y ", strtotime($to_date));
            }
            if($from_date!="" && $to_date==""){
                $order_array['timeRange']=" from ".date("D,j M Y", strtotime($from_date))." till today";
            }
            $order_array['data']=$orderss;
            $order_array['total_value']=$totalvalue;
            $order[]=$order_array;
        }

        
        if ($report_type == "purchaseTrend") {
            $order_arr = $this -> Analytics_model -> get_purchaseTrend_by_hour($from_date, $to_date);
            
            $hr_arr = array();
            if(!empty($order_arr)){
                foreach ($order_arr as $arr) {
                    if (in_array($arr["h"], array_keys($hr_arr))) {
                        $hr_arr[$arr["h"]] = $hr_arr[$arr["h"]] + $arr["activecount"];
                    } else {
                        $hr_arr[$arr["h"]] = $arr["activecount"];
                    }
                }
            }

            for ($x = 0; $x < 24; $x++) {
                $array_twentyFour[$x] = 0;
            }
            foreach ($array_twentyFour as $h => $v) {
                if (!in_array($h, array_keys($hr_arr))) {
                    $hr_arr[$h] = 0;
                }
            }

            $order_a = $hr_arr;
            ksort($order_a);
            $order_array=array();
            if($from_date=="" && $to_date==""){
                $order_array['timeRange']="Entire history";
            }
            
            
            if($from_date!="" && $to_date!=""){
                $order_array['timeRange']=" from ".date("D,j M Y ", strtotime($from_date))." to ".date("D,j M Y ", strtotime($to_date));
            }
            if($from_date==$to_date &&($from_date!="" && $to_date!="")){
                $order_array['timeRange']=" on ".date("D,j M Y", strtotime($from_date));
            }
            if($from_date=="" && $to_date!=""){
                $order_array['timeRange']=" before ".date("D,j M Y ", strtotime($to_date));
            }
            if($from_date!="" && $to_date==""){
                $order_array['timeRange']=" from ".date("D,j M Y", strtotime($from_date))." till today";
            }
            
            $order_array['ordersData']=$order_a;
            $order[]=$order_array;
        }
        echo json_encode($order);

    }

    public function returnsAnalytics() {
        $report_type = $this -> input -> post('report_type');
        $returns_report = [];
		
        $from_date = $this -> input -> post('from_date');
        $to_date = $this -> input -> post('to_date');
        $limit_result_val = $this -> input -> post('limit_result_val');
        $select_levelTxt = $this -> input -> post('select_levelTxt');
        if ($limit_result_val == "none") {
            $limit_result_val = "";
        }
        $select_level = $this -> input -> post('select_level');
        $particular_year = $this -> input -> post('particular_year');
        $limit_result_order_val = $this -> input -> post('limit_result_order_val');
        if ($limit_result_order_val == "none") {
            $limit_result_order_val = "";
        }
		
        if ($report_type == "replacement_cases") {
            $returns_report_arr = [];
            $replacementRaised_arr = $this -> Analytics_model -> getAllNewReplacementRequest($from_date, $to_date, $particular_year);

            $replacementRaised = array();
            foreach ($replacementRaised_arr as $cust) {
                $arrars = [];

                if ($cust['month'] == "Jan") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "0";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Feb") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "1";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Mar") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "2";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Apr") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "3";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "May") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "4";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Jun") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "5";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Jul") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "6";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Aug") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "7";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Sep") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "8";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Oct") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "9";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Nov") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "10";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Dec") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "11";
                    $arrars['d'] = $cust['d'];
                }
                array_push($replacementRaised, $arrars);
            }
            $m = array();
            foreach ($replacementRaised as $key => $row) {
                $m[$key] = $row['m'];
            }

            array_multisort($m, SORT_ASC, $replacementRaised);
            $returns_report_arr['replacementRaised'] = $replacementRaised;

            $replacementClosedByCustomerItself_arr = $this -> Analytics_model -> getAllReplacementsInitialClosedByCustomer($from_date, $to_date, $particular_year);

            $replacementClosedByCustomerItself = array();
            foreach ($replacementClosedByCustomerItself_arr as $cust) {
                $arrars = [];

                if ($cust['month'] == "Jan") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "0";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Feb") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "1";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Mar") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "2";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Apr") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "3";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "May") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "4";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Jun") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "5";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Jul") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "6";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Aug") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "7";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Sep") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "8";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Oct") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "9";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Nov") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "10";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Dec") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "11";
                    $arrars['d'] = $cust['d'];
                }
                array_push($replacementClosedByCustomerItself, $arrars);
            }
            $m = array();
            foreach ($replacementClosedByCustomerItself as $key => $row) {
                $m[$key] = $row['m'];
            }

            array_multisort($m, SORT_ASC, $replacementClosedByCustomerItself);
            $returns_report_arr['replacementClosedByCustomerItself'] = $replacementClosedByCustomerItself;

            $replacementClosedByAdminOrCustomer_arr = $this -> Analytics_model -> getAllReplacementsLaterClosedByCustomerOrAdmin($from_date, $to_date, $particular_year);
            $replacementClosedByAdminOrCustomer = array();
            foreach ($replacementClosedByAdminOrCustomer_arr as $cust) {
                $arrars = [];

                if ($cust['month'] == "Jan") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "0";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Feb") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "1";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Mar") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "2";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Apr") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "3";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "May") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "4";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Jun") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "5";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Jul") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "6";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Aug") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "7";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Sep") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "8";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Oct") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "9";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Nov") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "10";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Dec") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "11";
                    $arrars['d'] = $cust['d'];
                }
                array_push($replacementClosedByAdminOrCustomer, $arrars);
            }
            $m = array();
            foreach ($replacementClosedByAdminOrCustomer as $key => $row) {
                $m[$key] = $row['m'];
            }

            array_multisort($m, SORT_ASC, $replacementClosedByAdminOrCustomer);

            $returns_report_arr['replacementClosedByAdminOrCustomer'] = $replacementClosedByAdminOrCustomer;

            $getAllReplacementsRemainPending_arr = $this -> Analytics_model -> getAllReplacementsRemainPending($from_date, $to_date, $particular_year);
            //print_r($getAllReplacementsRemainPending_arr);
            $getAllReplacementsRemainPending = array();
            $arrarsCounts = 0;
            $cc = 0;
            foreach ($getAllReplacementsRemainPending_arr as $cust) {
                $arrars = [];

                if ($cust['month'] == "Jan") {

                    $cc += intval($cust['counts']);
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "0";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Feb") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "1";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Mar") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "2";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Apr") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "3";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "May") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "4";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Jun") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "5";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Jul") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "6";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Aug") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "7";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Sep") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "8";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Oct") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "9";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Nov") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "10";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Dec") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "11";
                    $arrars['d'] = $cust['d'];
                }

                array_push($getAllReplacementsRemainPending, $arrars);
            }
            $m = array();
            foreach ($getAllReplacementsRemainPending as $key => $row) {
                $m[$key] = $row['m'];
            }

            array_multisort($m, SORT_ASC, $getAllReplacementsRemainPending);

            $returns_report_arr['RemainPending'] = $getAllReplacementsRemainPending;

            $returns_report[] = $returns_report_arr;
        }
        /*refund start*/
        if ($report_type == "refund_cases") {
            $returns_report_arr = [];
			
            $refundRaised_arr = $this -> Analytics_model -> getAllNewRefundRequest($from_date, $to_date, $particular_year);

            $refundRaised = array();
            foreach ($refundRaised_arr as $cust) {
                $arrars = [];

                if ($cust['month'] == "Jan") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "0";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Feb") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "1";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Mar") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "2";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Apr") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "3";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "May") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "4";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Jun") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "5";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Jul") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "6";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Aug") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "7";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Sep") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "8";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Oct") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "9";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Nov") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "10";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Dec") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "11";
                    $arrars['d'] = $cust['d'];
                }
                array_push($refundRaised, $arrars);
            }
            $m = array();
            foreach ($refundRaised as $key => $row) {
                $m[$key] = $row['m'];
            }

            array_multisort($m, SORT_ASC, $refundRaised);
            $returns_report_arr['refundRaised'] = $refundRaised;

            $refundClosedByCustomerItself_arr = $this -> Analytics_model -> getAllRefundInitialClosedByCustomer($from_date, $to_date, $particular_year);

            $refundClosedByCustomerItself = array();
            foreach ($refundClosedByCustomerItself_arr as $cust) {
                $arrars = [];

                if ($cust['month'] == "Jan") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "0";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Feb") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "1";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Mar") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "2";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Apr") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "3";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "May") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "4";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Jun") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "5";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Jul") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "6";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Aug") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "7";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Sep") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "8";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Oct") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "9";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Nov") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "10";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Dec") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "11";
                    $arrars['d'] = $cust['d'];
                }
                array_push($refundClosedByCustomerItself, $arrars);
            }
            $m = array();
            foreach ($refundClosedByCustomerItself as $key => $row) {
                $m[$key] = $row['m'];
            }

            array_multisort($m, SORT_ASC, $refundClosedByCustomerItself);
            $returns_report_arr['refundClosedByCustomerItself'] = $refundClosedByCustomerItself;

            $refundClosedByAdminOrCustomer_arr = $this -> Analytics_model -> getAllRefundLaterClosedByCustomerOrAdmin($from_date, $to_date, $particular_year);
            $refundClosedByAdminOrCustomer = array();
            foreach ($refundClosedByAdminOrCustomer_arr as $cust) {
                $arrars = [];

                if ($cust['month'] == "Jan") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "0";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Feb") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "1";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Mar") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "2";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Apr") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "3";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "May") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "4";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Jun") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "5";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Jul") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "6";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Aug") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "7";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Sep") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "8";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Oct") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "9";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Nov") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "10";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Dec") {
                    if ($cust['counts'] == 0) {
                        $arrars['counts'] = null;
                    } else {
                        $arrars['counts'] = $cust['counts'];
                    }
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "11";
                    $arrars['d'] = $cust['d'];
                }
                array_push($refundClosedByAdminOrCustomer, $arrars);
            }
            $m = array();
            foreach ($refundClosedByAdminOrCustomer as $key => $row) {
                $m[$key] = $row['m'];
            }

            array_multisort($m, SORT_ASC, $refundClosedByAdminOrCustomer);

            $returns_report_arr['refundClosedByAdminOrCustomer'] = $refundClosedByAdminOrCustomer;

            $getAllRefundRemainPending_arr = $this -> Analytics_model -> getAllRefundRemainPending($from_date, $to_date, $particular_year);
            //print_r($getAllReplacementsRemainPending_arr);
            $getAllRefundRemainPending = array();
            $arrarsCounts = 0;
            $cc = 0;
            foreach ($getAllRefundRemainPending_arr as $cust) {
                $arrars = [];

                if ($cust['month'] == "Jan") {

                    $cc += intval($cust['counts']);
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "0";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Feb") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "1";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Mar") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "2";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Apr") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "3";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "May") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "4";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Jun") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "5";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Jul") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "6";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Aug") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "7";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Sep") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "8";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Oct") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "9";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Nov") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "10";
                    $arrars['d'] = $cust['d'];
                }
                if ($cust['month'] == "Dec") {
                    $cc += intval($cust['counts']);
                    $arrars['counts'] = intval($arrarsCounts) + intval($cc);
                    //$arrars['counts']=$cust['counts'];
                    $arrars['y'] = $cust['y'];
                    $arrars['m'] = "11";
                    $arrars['d'] = $cust['d'];
                }

                array_push($getAllRefundRemainPending, $arrars);
            }
            $m = array();
            foreach ($getAllRefundRemainPending as $key => $row) {
                $m[$key] = $row['m'];
            }

            array_multisort($m, SORT_ASC, $getAllRefundRemainPending);

            $returns_report_arr['RemainPending'] = $getAllRefundRemainPending;

            $returns_report[] = $returns_report_arr;
        }

        echo json_encode($returns_report);
    }


    public function getCustomerReport() {

        $report_type = $this -> input -> post('report_type');
        $from_date = $this -> input -> post('from_date');
        $to_date = $this -> input -> post('to_date');
        $limit_result_val = $this -> input -> post('limit_result_val');
        $particular_year = $this -> input -> post('particular_year');
        $customer = "";
        if ($report_type == "customer_reg") {

            if ($particular_year != "") {
                $customers = $this -> Analytics_model -> getCustomerRegistrationsAnalytics($from_date, $to_date, $limit_result_val, $particular_year);
                $customer = array();
                foreach ($customers as $cust) {
                    $arrars = [];

                    if ($cust['month'] == "Jan") {
                        if ($cust['count'] == 0) {
                            $arrars['count'] = null;
                        } else {
                            $arrars['count'] = $cust['count'];
                        }
                        $arrars['y'] = $cust['y'];
                        $arrars['m'] = "0";
                        $arrars['d'] = $cust['d'];
                    }
                    if ($cust['month'] == "Feb") {
                        if ($cust['count'] == 0) {
                            $arrars['count'] = null;
                        } else {
                            $arrars['count'] = $cust['count'];
                        }
                        $arrars['y'] = $cust['y'];
                        $arrars['m'] = "1";
                        $arrars['d'] = $cust['d'];
                    }
                    if ($cust['month'] == "Mar") {
                        if ($cust['count'] == 0) {
                            $arrars['count'] = null;
                        } else {
                            $arrars['count'] = $cust['count'];
                        }
                        $arrars['y'] = $cust['y'];
                        $arrars['m'] = "2";
                        $arrars['d'] = $cust['d'];
                    }
                    if ($cust['month'] == "Apr") {
                        if ($cust['count'] == 0) {
                            $arrars['count'] = null;
                        } else {
                            $arrars['count'] = $cust['count'];
                        }
                        $arrars['y'] = $cust['y'];
                        $arrars['m'] = "3";
                        $arrars['d'] = $cust['d'];
                    }
                    if ($cust['month'] == "May") {
                        if ($cust['count'] == 0) {
                            $arrars['count'] = null;
                        } else {
                            $arrars['count'] = $cust['count'];
                        }
                        $arrars['y'] = $cust['y'];
                        $arrars['m'] = "4";
                        $arrars['d'] = $cust['d'];
                    }
                    if ($cust['month'] == "Jun") {
                        if ($cust['count'] == 0) {
                            $arrars['count'] = null;
                        } else {
                            $arrars['count'] = $cust['count'];
                        }
                        $arrars['y'] = $cust['y'];
                        $arrars['m'] = "5";
                        $arrars['d'] = $cust['d'];
                    }
                    if ($cust['month'] == "Jul") {
                        if ($cust['count'] == 0) {
                            $arrars['count'] = null;
                        } else {
                            $arrars['count'] = $cust['count'];
                        }
                        $arrars['y'] = $cust['y'];
                        $arrars['m'] = "6";
                        $arrars['d'] = $cust['d'];
                    }
                    if ($cust['month'] == "Aug") {
                        if ($cust['count'] == 0) {
                            $arrars['count'] = null;
                        } else {
                            $arrars['count'] = $cust['count'];
                        }
                        $arrars['y'] = $cust['y'];
                        $arrars['m'] = "7";
                        $arrars['d'] = $cust['d'];
                    }
                    if ($cust['month'] == "Sep") {
                        if ($cust['count'] == 0) {
                            $arrars['count'] = null;
                        } else {
                            $arrars['count'] = $cust['count'];
                        }
                        $arrars['y'] = $cust['y'];
                        $arrars['m'] = "8";
                        $arrars['d'] = $cust['d'];
                    }
                    if ($cust['month'] == "Oct") {
                        if ($cust['count'] == 0) {
                            $arrars['count'] = null;
                        } else {
                            $arrars['count'] = $cust['count'];
                        }
                        $arrars['y'] = $cust['y'];
                        $arrars['m'] = "9";
                        $arrars['d'] = $cust['d'];
                    }
                    if ($cust['month'] == "Nov") {
                        if ($cust['count'] == 0) {
                            $arrars['count'] = null;
                        } else {
                            $arrars['count'] = $cust['count'];
                        }
                        $arrars['y'] = $cust['y'];
                        $arrars['m'] = "10";
                        $arrars['d'] = $cust['d'];
                    }
                    if ($cust['month'] == "Dec") {
                        if ($cust['count'] == 0) {
                            $arrars['count'] = null;
                        } else {
                            $arrars['count'] = $cust['count'];
                        }
                        $arrars['y'] = $cust['y'];
                        $arrars['m'] = "11";
                        $arrars['d'] = $cust['d'];
                    }
                    array_push($customer, $arrars);
                }
                $m = array();
                foreach ($customer as $key => $row) {
                    $m[$key] = $row['m'];
                }

                array_multisort($m, SORT_ASC, $customer);

            } else {
                $customer = $this -> Analytics_model -> getCustomerRegistrationsAnalytics($from_date, $to_date, $limit_result_val, $particular_year);
            }

        }
        if ($report_type == "customer_age") {
            $customer = $this -> Analytics_model -> getCustomerAgeAnalytics($from_date, $to_date, $limit_result_val);
        }
        if ($report_type == "customer_gen") {
            $customer_arr = $this -> Analytics_model -> getCustomerGenderAnalytics($from_date, $to_date, $limit_result_val);
            $customer = array();
            foreach ($customer_arr as $data) {
                $male_arr = array();
                if ($data['Male']) {
                    $male_arr['name'] = "Male";
                    $male_arr['y'] = intval($data['Male']);
                }
                $female_arr = array();
                if ($data['Female']) {
                    $female_arr['name'] = "Female";
                    $female_arr['y'] = intval($data['Female']);
                }
                array_push($customer, $male_arr);
                array_push($customer, $female_arr);
            }
        }
        if ($report_type == "customer_price_segmentation") {
            $customer = $this -> Analytics_model -> getCustomerPriceSegmentationAnalytics($from_date, $to_date, $limit_result_val);
        }
        /*{
         name: 'Microsoft Internet Explorer',
         y: 56.33
         }, {
         name: 'Chrome',
         y: 24.03,
         },*/
        echo json_encode($customer);
    }

    public function getPromotionReport() {
        $report_type = $this -> input -> post('report_type');
        $promotions_report = [];
        $from_date = $this -> input -> post('from_date');
        $to_date = $this -> input -> post('to_date');
        $limit_result_val = $this -> input -> post('limit_result_val');
        $select_levelTxt = $this -> input -> post('select_levelTxt');
        if ($limit_result_val == "none") {
            $limit_result_val = "";
        }
        $select_level = $this -> input -> post('select_level');
        $particular_year = $this -> input -> post('particular_year');
        $limit_result_order_val = $this -> input -> post('limit_result_order_val');
        if ($limit_result_order_val == "none") {
            $limit_result_order_val = "";
        }
        if ($report_type == "number_of_promotions") {
            $promotions_report = $this -> Analytics_model -> getNumberOfPromotions($from_date, $to_date, $limit_result_val, $limit_result_order_val, $particular_year);
        }

        if ($report_type == "promotions_per_month") {
            $drill_down = array();
            $series_arr_data = array();
            $common_arr = array();
            $promotions_reports = $this -> Analytics_model -> getPromotionsPerMonth($from_date, $to_date, $limit_result_val, $limit_result_order_val, $particular_year);
            $product_name = "";
            $series_arr_data = array();
            $common_arr = array();
            if (!empty($promotions_reports)) {
                foreach ($promotions_reports as $datas) {
                    $drillDown = [];

                    $data = [];
                    $data['name'] = $datas['m'];
                    $data['y'] = $datas['count'];
                    $data['drilldown'] = $datas['m'];
                    array_push($series_arr_data, $data);
                    $promotions_arr = $this -> Analytics_model -> get_all_promotion_name_by_year_month($from_date, $to_date, $limit_result_val, $datas['m'], $datas['y']);
                    $drillDown['name'] = $datas['m'];
                    $drillDown['id'] = $datas['m'];
                    $temp = array();
                    if (!empty($promotions_arr)) {
                        foreach ($promotions_arr as $dat) {

                            $drill_data = [];
                            $drill_data['id'] = $datas['m'];
                            $drill_data['name'] = html_entity_decode($dat['promo_name'],ENT_QUOTES);
                            $drill_data['y'] = $this -> Analytics_model -> get_number_of_invoices_made_with_promo_uid($dat['promo_uid']);
                            array_push($temp, $drill_data);

                        }
                    }
                    $drillDown['data'] = $temp;
                    array_push($common_arr, $drillDown);
                }

            }

            $series = [];
            $series['id'] = "Promotions Per Month";
            $series['name'] = "Promotions Per Month";
            $series['colorByPoint'] = "true";
            $series['data'] = $series_arr_data;

            $drill_down["series"] = $series;
            $drill_down["drill"] = $common_arr;
            $drill_down["name_chart"] = "Promotion Created Respective Month";
            $promotions_report[] = $drill_down;

        }
        if ($report_type == "popular_promotions") {
            $promotions_report = $this -> Analytics_model -> getNumberOfPopularPromotions($from_date, $to_date, $limit_result_val, $limit_result_order_val, $particular_year);

        }

        echo json_encode($promotions_report);

    }

    public function getProductReport() {

        $report_type = $this -> input -> post('report_type');
        $product = [];
        $from_date = $this -> input -> post('from_date');
        $to_date = $this -> input -> post('to_date');
        $limit_result_val = $this -> input -> post('limit_result_val');
        $select_levelTxt = $this -> input -> post('select_levelTxt');
        if ($limit_result_val == "none") {
            $limit_result_val = "";
        }
        $select_level = $this -> input -> post('select_level');
        $particular_year = $this -> input -> post('particular_year');

        $limit_result_order_val = $this -> input -> post('limit_result_order_val');
        if ($limit_result_order_val == "none") {
            $limit_result_order_val = "";
        }
        if ($report_type == "searched_keyword") {
			
            $product = $this -> Analytics_model -> get_searched_keywords_with_counts($from_date, $to_date, $limit_result_val, $limit_result_order_val, $particular_year);
		
			
        }
        if ($report_type == "most_purchased") {
            $product = $this -> Analytics_model -> getallPurchasedProductsForAnalytics($from_date, $to_date, $limit_result_val, $limit_result_order_val, $select_level, $particular_year);
        }

        if ($report_type == "ratings") {
            $product_arr['select_level'] = $select_levelTxt;
            /*$product_arr['data'] = $this -> Analytics_model -> getallRatingReportsForAnalytics($from_date, $to_date, $limit_result_val, $limit_result_order_val, $select_level, $particular_year);
            $product[] = $product_arr;*/
            
            
            
         $drill_down=array();
         $series_arr_data=array();
         $common_arr=array();
         $rating_arr=$this -> Analytics_model -> getallRatingReportsForAnalytics($from_date, $to_date, $limit_result_val, $limit_result_order_val, $select_level, $particular_year);
         //print_r($product_arr);exit;

         $product_name="";
         $series_arr_data=array();
         $common_arr=array();
         if(!empty($rating_arr)){
            foreach($rating_arr as $datas){
             $drillDown=[];
    
             $data=[];
             $data['name']=$datas['rating']." star ";
             $data['y']=$datas['Count'];
             $data['drilldown']=" Rated as ".$datas['rating'];
             array_push($series_arr_data,$data);
             $rating_arr_data=$this->Analytics_model->get_all_products_arr_by_rating($from_date,$to_date,$limit_result_val,$datas['rating']);
             $drillDown['name']=" Rated as ".$datas['rating'];
             $drillDown['id']=" Rated as ".$datas['rating'];
             $temp=array();
             if(!empty($rating_arr_data)){
                 foreach($rating_arr_data as $dat){
        
                 $drill_data=[];
                 $drill_data['id']=$datas['rating'];
                 $drill_data['name']=$dat['name'];
                 $drill_data['drilldown']=$dat['name'];
                 $drill_data['y']=intval($dat['counts']);
                 //$subcategory_arr=$this->Analytics_model->get_all_sub_category_arr_by_cat($dat['cat_id']);
                 array_push($temp,$drill_data);
        
                 }
             }
             $drillDown['data']=$temp;
             array_push($common_arr,$drillDown);
             }
         }
         
         $series=[];
         $series['id']="Ratings Data";
         $series['name']="Ratings Data";
         $series['colorByPoint']="true";
         $series['data']=$series_arr_data;
         $drill_down['select_level'] = $select_levelTxt;

         $drill_down["series"]=$series;
         $drill_down["drill"]=$common_arr;
         $drill_down["name_chart"]="Rating Data";
         $product[]=$drill_down;
            
        }

        /* if($report_type=="all_Inventory"){
         $drill_down=array();
         $series_arr_data=array();
         $common_arr=array();
         $product_arr=$this->Analytics_model->get_all_inventory_added($from_date,$to_date,$limit_result_val,$select_level,$particular_year);
         //print_r($product_arr);exit;

         $product_name="";
         $series_arr_data=array();
         $common_arr=array();
         foreach($product_arr as $datas){
         $drillDown=[];

         $data=[];
         $data['name']=$datas['pcat_name'];
         $data['y']=$datas['counts'];
         $data['drilldown']=$datas['pcat_name'];
         array_push($series_arr_data,$data);
         $category_arr=$this->Analytics_model->get_all_category_arr_by_pcat($from_date,$to_date,$limit_result_val,$datas['pcat_name']);
         $drillDown['name']=$datas['pcat_name'];
         $drillDown['id']=$datas['pcat_name'];
         $temp=array();
         foreach($category_arr as $dat){

         $drill_data=[];
         $drill_data['id']=$datas['pcat_name'];
         $drill_data['name']=$dat['cat_name'];
         $drill_data['drilldown']=$dat['cat_name'];
         $drill_data['y']=$this->Analytics_model->getNumOfCategoriesInSubCat($dat['cat_id']);
         $subcategory_arr=$this->Analytics_model->get_all_sub_category_arr_by_cat($dat['cat_id']);
         array_push($temp,$drill_data);

         }
         $drillDown['data']=$temp;
         array_push($common_arr,$drillDown);
         }
         $series=[];
         $series['id']="Inventory";
         $series['name']="Inventory";
         $series['colorByPoint']="true";
         $series['data']=$series_arr_data;

         $drill_down["series"]=$series;
         $drill_down["drill"]=$common_arr;
         $drill_down["name_chart"]="Inventory Data";
         $product[]=$drill_down;
         }
         */
        if ($report_type == "parent_category") {
            $drill_down = array();
            $series_arr_data = array();
            $common_arr = array();
            $product_arr = $this -> Analytics_model -> get_all_inventory_added($from_date, $to_date, $limit_result_val, $select_level, $particular_year);
            $product_name = "";
            $series_arr_data = array();
            $common_arr = array();
            if (!empty($product_arr)) {
                foreach ($product_arr as $datas) {
                    $drillDown = [];

                    $data = [];
                    $data['name'] = html_entity_decode($datas['pcat_name'],ENT_QUOTES);
                    $data['y'] = $datas['counts'];
                    $data['drilldown'] = html_entity_decode($datas['pcat_name'],ENT_QUOTES);
                    array_push($series_arr_data, $data);
                    $category_arr = $this -> Analytics_model -> get_all_category_arr_by_pcat($from_date, $to_date, $limit_result_val, $datas['pcat_name']);
                    $drillDown['name'] = $datas['pcat_name'];
                    $drillDown['id'] = $datas['pcat_name'];
                    $temp = array();
                    foreach ($category_arr as $dat) {

                        $drill_data = [];
                        $drill_data['id'] = html_entity_decode($datas['pcat_name'],ENT_QUOTES);
                        $drill_data['name'] = html_entity_decode($dat['cat_name'],ENT_QUOTES);
                        $drill_data['drilldown'] = html_entity_decode($dat['cat_name'],ENT_QUOTES);
                        $drill_data['y'] = $this -> Analytics_model -> getNumOfCategoriesInSubCat($dat['cat_id']);
                        $subcategory_arr = $this -> Analytics_model -> get_all_sub_category_arr_by_cat($dat['cat_id']);
                        array_push($temp, $drill_data);

                    }
                    $drillDown['data'] = $temp;
                    array_push($common_arr, $drillDown);
                }

            }

            $series = [];
            $series['id'] = "Top Category";
            $series['name'] = "Top Category";
            $series['colorByPoint'] = "true";
            $series['data'] = $series_arr_data;

            $drill_down["series"] = $series;
            $drill_down["drill"] = $common_arr;
            $drill_down["name_chart"] = "Top Category";
            $product[] = $drill_down;

        }
        if ($report_type == "category") {
            $drill_down = array();
            $series_arr_data = array();
            $common_arr = array();
            $category_arr = $this -> Analytics_model -> get_all_category_arr($from_date, $to_date, $limit_result_val, $select_level, $particular_year);
            $series_arr_data = array();
            if (!empty($category_arr)) {
                foreach ($category_arr as $datas) {
                    $drillDown = [];
                    $data = [];
                    $data['name'] = html_entity_decode($datas['cat_name'],ENT_QUOTES);
                    $data['y'] = $datas['counts'];
                    $data['drilldown'] = html_entity_decode($datas['cat_name'],ENT_QUOTES);
                    array_push($series_arr_data, $data);
                    $subcategory_arr = $this -> Analytics_model -> get_all_sub_category_arr_by_cat($datas['cat_id']);
                    $drillDown['name'] = html_entity_decode($datas['cat_name'],ENT_QUOTES);
                    $drillDown['id'] = html_entity_decode($datas['cat_name'],ENT_QUOTES);
                    $temp = array();
                    foreach ($subcategory_arr as $dat) {

                        $drill_data = [];
                        $drill_data['id'] = html_entity_decode($datas['cat_name'],ENT_QUOTES);
                        $drill_data['name'] = html_entity_decode($dat['subcat_name'],ENT_QUOTES);
                        $drill_data['drilldown'] = html_entity_decode($dat['subcat_name'],ENT_QUOTES);
                        $drill_data['y'] = $this -> Analytics_model -> getNumOfSubCatInBrands($dat['subcat_id']);
                        //$subcategory_arr=$this->Analytics_model->get_all_sub_category_arr_by_cat($dat['cat_id']);
                        array_push($temp, $drill_data);

                    }
                    $drillDown['data'] = $temp;
                    array_push($common_arr, $drillDown);
                }
            }

            $series = [];
            $series['id'] = "Category";
            $series['name'] = "Category";
            $series['colorByPoint'] = "true";
            $series['data'] = $series_arr_data;

            $drill_down["series"] = $series;
            $drill_down["drill"] = $common_arr;
            $drill_down["name_chart"] = "Category";
            $product[] = $drill_down;
        }
        if ($report_type == "sub_category") {
            $drill_down = array();
            $series_arr_data = array();
            $common_arr = array();
            $sub_category_arr = $this -> Analytics_model -> get_all_sub_category_arr($from_date, $to_date, $limit_result_val, $select_level, $particular_year);

            if (!empty($sub_category_arr)) {
                foreach ($sub_category_arr as $datas) {
                    $drillDown = [];

                    $data = [];
                    $data['name'] = html_entity_decode($datas['subcat_name'],ENT_QUOTES);
                    $data['y'] = $datas['counts'];
                    $data['drilldown'] = html_entity_decode($datas['subcat_name'],ENT_QUOTES);
                    array_push($series_arr_data, $data);
                    $brand_arr = $this -> Analytics_model -> get_all_barnds_arr_by_subcat($datas['subcat_id']);
                    $drillDown['name'] = html_entity_decode($datas['subcat_name'],ENT_QUOTES);
                    $drillDown['id'] = html_entity_decode($datas['subcat_name'],ENT_QUOTES);
                    $temp = array();
                    foreach ($brand_arr as $dat) {

                        $drill_data = [];
                        $drill_data['id'] = html_entity_decode($datas['subcat_name'],ENT_QUOTES);
                        $drill_data['name'] = html_entity_decode($dat['brand_name'],ENT_QUOTES);
                        $drill_data['drilldown'] = html_entity_decode($dat['brand_name'],ENT_QUOTES);
                        $drill_data['y'] = $this -> Analytics_model -> getNumOfBrandsInProducts($dat['brand_id']);
                        //$subcategory_arr=$this->Analytics_model->get_all_sub_category_arr_by_cat($dat['cat_id']);
                        array_push($temp, $drill_data);

                    }
                    $drillDown['data'] = $temp;
                    array_push($common_arr, $drillDown);
                }
            }
            $series = [];
            $series['id'] = "SubCategory";
            $series['name'] = "Sub Category";
            $series['colorByPoint'] = "true";
            $series['data'] = $series_arr_data;

            $drill_down["series"] = $series;
            $drill_down["drill"] = $common_arr;
            $drill_down["name_chart"] = "Sub Category";
            $product[] = $drill_down;

        }
        if ($report_type == "brand") {
            $drill_down = array();
            $series_arr_data = array();
            $common_arr = array();
            $brands_arr = $this -> Analytics_model -> get_all_brands_arr($from_date, $to_date, $limit_result_val, $select_level, $particular_year);
            if (!empty($brands_arr)) {
                foreach ($brands_arr as $datas) {
                    $drillDown = [];

                    $data = [];
                    $data['name'] = html_entity_decode($datas['brand_name'],ENT_QUOTES);
                    $data['y'] = html_entity_decode($datas['counts'],ENT_QUOTES);
                    $data['drilldown'] = html_entity_decode($datas['brand_name'],ENT_QUOTES);
                    array_push($series_arr_data, $data);
                    $products_arr_by = $this -> Analytics_model -> get_all_products_arr_by_brand($datas['brand_id']);
                    $drillDown['name'] = html_entity_decode($datas['brand_name'],ENT_QUOTES);
                    $drillDown['id'] = html_entity_decode($datas['brand_name'],ENT_QUOTES);
                    $temp = array();
                    if (!empty($products_arr_by)) {
                        foreach ($products_arr_by as $dat) {

                            $drill_data = [];
                            $drill_data['id'] = html_entity_decode($datas['brand_name'],ENT_QUOTES);
                            $drill_data['name'] = html_entity_decode($dat['product_name'],ENT_QUOTES);
                            $drill_data['drilldown'] = html_entity_decode($dat['product_name'],ENT_QUOTES);
                            $drill_data['y'] = $this -> Analytics_model -> getNumOfProductsInInventory($dat['product_id']);
                            //$subcategory_arr=$this->Analytics_model->get_all_sub_category_arr_by_cat($dat['cat_id']);
                            array_push($temp, $drill_data);

                        }
                    }
                    $drillDown['data'] = $temp;
                    if (!empty($drillDown)) {
                        array_push($common_arr, $drillDown);
                    }

                }
            }
            $series = [];
            $series['id'] = "Brand";
            $series['name'] = "Brand";
            $series['colorByPoint'] = "true";
            $series['data'] = $series_arr_data;

            $drill_down["series"] = $series;
            $drill_down["drill"] = $common_arr;
            $drill_down["name_chart"] = "Brand";
            $product[] = $drill_down;
        }
        if ($report_type == "products") {
            $drill_down = array();
            $series_arr_data = array();
            $common_arr = array();
            $products_arr = $this -> Analytics_model -> get_all_products_arr($from_date, $to_date, $limit_result_val, $select_level, $particular_year);
            if (!empty($products_arr)) {
                foreach ($products_arr as $datas) {
                    $drillDown = [];

                    $data = [];
                    $data['name'] = html_entity_decode($datas['product_name'],ENT_QUOTES);
                    $data['y'] = $datas['counts'];
                    $data['drilldown'] = html_entity_decode($datas['product_name'],ENT_QUOTES);
                    array_push($series_arr_data, $data);
                    $inventory_arr_by = $this -> Analytics_model -> get_all_inventory_arr_by_product($datas['product_id']);
                    $drillDown['name'] = html_entity_decode($datas['product_name'],ENT_QUOTES);
                    $drillDown['id'] = html_entity_decode($datas['product_name'],ENT_QUOTES);
                    $temp = array();
                    if (!empty($inventory_arr_by)) {
                        foreach ($inventory_arr_by as $dat) {

                            $drill_data = [];
                            $drill_data['id'] = html_entity_decode($datas['product_name'],ENT_QUOTES);
                            $drill_data['name'] = html_entity_decode($dat['sku_id'],ENT_QUOTES);
                            $drill_data['drilldown'] = html_entity_decode($dat['sku_id'],ENT_QUOTES);
                            $drill_data['y'] = intval($this -> Analytics_model -> getNumOfSkusInInventory($dat['sku_id']));
                            //$subcategory_arr=$this->Analytics_model->get_all_sub_category_arr_by_cat($dat['cat_id']);
                            array_push($temp, $drill_data);

                        }
                    }
                    $drillDown['data'] = $temp;
                    if (!empty($drillDown)) {
                        array_push($common_arr, $drillDown);
                    }

                }
            }

            $series = [];
            $series['id'] = "Products";
            $series['name'] = "Products";
            $series['colorByPoint'] = "true";
            $series['data'] = $series_arr_data;

            $drill_down["series"] = $series;
            $drill_down["drill"] = $common_arr;
            $drill_down["name_chart"] = "Products";
            $product[] = $drill_down;
        }
        echo json_encode($product);
    }

    public function getRelatedSelectionOptions() {
        if ($this -> session -> userdata("logged_in")) {
            $report_type = $this -> input -> post('report_type');
            $options = "";
            $result=array();
            if ($report_type == "parent_category") {
                $options = $this -> Analytics_model -> getAllOptionsForParentCategory();
               if(!empty($options)){
                   foreach($options as $data){
                       $temp=[];
                      $temp['id']= $data['id'];
                      $temp['name']= html_entity_decode($data['name'],ENT_QUOTES);
                      array_push($result,$temp);
                   }
               }
                echo json_encode($result);
                exit ;
            }
            if ($report_type == "category") {
                $options = $this -> Analytics_model -> getAllOptionsForCategory();
                  if(!empty($options)){
                   foreach($options as $data){
                       $temp=[];
                      $temp['id']= $data['id'];
                      $temp['name']= html_entity_decode($data['name'],ENT_QUOTES);
                      array_push($result,$temp);
                   }
                 }
                echo json_encode($result);
                exit ;
            }
            if ($report_type == "sub_category") {
                $options = $this -> Analytics_model -> getAllOptionsForSubCategory();
                if(!empty($options)){
                   foreach($options as $data){
                       $temp=[];
                      $temp['id']= $data['id'];
                      $temp['name']= html_entity_decode($data['name'],ENT_QUOTES);
                      array_push($result,$temp);
                   }
                 }
                echo json_encode($result);
                exit ;
            }
            if ($report_type == "brand") {
                $options = $this -> Analytics_model -> getAllOptionsForBrands();
                if(!empty($options)){
                   foreach($options as $data){
                       $temp=[];
                      $temp['id']= $data['id'];
                      $temp['name']= html_entity_decode($data['name'],ENT_QUOTES);
                      array_push($result,$temp);
                   }
                 }
                echo json_encode($result);
                exit ;
            }
            if ($report_type == "products") {
                $options = $this -> Analytics_model -> getAllOptionsForProducts();
				if(!empty($options)){
                   foreach($options as $data){
                       $temp=[];
                      $temp['id']= $data['id'];
                      $temp['name']= html_entity_decode($data['name'],ENT_QUOTES);
                      array_push($result,$temp);
                   }
                 }
                echo json_encode($result);
                exit ;
            }
            if ($report_type == "most_purchased") {
                $options = $this -> Analytics_model -> getAllOptionsForParentCategory();
                if(!empty($options)){
                   foreach($options as $data){
                       $temp=[];
                      $temp['id']= $data['id'];
                      $temp['name']= html_entity_decode($data['name'],ENT_QUOTES);
                      array_push($result,$temp);
                   }
                 }
                echo json_encode($result);
                exit ;
            }
            /*if($report_type=="products"){
             $options=$this->Analytics_model->getAllOptionsForProducts();
             }*/
            //echo json_encode($options);
			echo json_encode($result);
                exit ;

        } else {
            redirect(base_url());
        }
    }
	public function getPageViewedDetails(){
		if ($this -> session -> userdata("logged_in")) {
            $report_type = $this -> input -> post('report_type');
			$report_type_graph_arr[$report_type]=array();
			$report_type_sku_arr[$report_type]=array();
			$report_type_user_arr[$report_type]=array();
	
			
			$from_date = $this -> input -> post('from_date');
			$to_date = $this -> input -> post('to_date');
			$particular_year = $this -> input -> post('particular_year');
			$limit_result_order_val = $this -> input -> post('limit_result_order_val');
			$limit_result_val = $this -> input -> post('limit_result_val');
			
			$home_page_arr=array();
			$cat_page_arr=array();
			$final_arr=array();
				
            $obj=$this->Analytics_model->get_visitor_data($from_date,$to_date,$particular_year,$limit_result_order_val,$limit_result_val);
				
				
				/*echo '<pre>';
				print_r($obj);
				echo '</pre>';
                */
				
				foreach($obj as $data_all){
				//print_r($data_all);
					$visitor_id=$data_all->visitor_id;
					$user_type=$data_all->user_type;
					$ip=$data_all->IP;
					$geo_data=json_decode($data_all->geo_data);

					/*$country_name=$geo_data->country_name;
					$region_name=$geo_data->region_name;
					$city=$geo_data->city;
				*/
					if($report_type =='home'|| $report_type =='category' || $report_type =='product'){
//print_r($data_all);
						$user_behaviour=$data_all->user_behaviour;
						
						$user_behaviour = ltrim($user_behaviour, ', ');
						$user_behaviour='['.$user_behaviour.']';	
						
						$user_behaviour=json_decode($user_behaviour,true);
						//echo count($user_behaviour);
						//print_r($user_behaviour);
					
						foreach($user_behaviour as $beh => $data){
							
							foreach($data as $key => $val){
								if($key=="page" && $report_type !='button'){
									
									//if((strpos($val, '/search') !== false) && (strpos($val, '/category/cat/') === false) && ($report_type == "home")){
									if(($report_type == "home") && !(strpos($val, '/item/') !== false)){
										//echo $val."<br>";
										//print_r($data);
										$home_page=array();	
										$home_page['visitor_id']=$visitor_id;
										$home_page['user_type']=$user_type;
										$home_page['geo_data']=$geo_data;
										$home_page['page']=$val;
										$home_page['data']=$data;
										$final_arr[]=$home_page;
									
										
									}
									if((strpos($val, '/category/cat/') !== false) && ($report_type == "category")){
										
										$cat_page=array();	
										$cat_page['visitor_id']=$visitor_id;
										$cat_page['user_type']=$user_type;
										$cat_page['geo_data']=$geo_data;
										$cat_page['page']=$val;
										$cat_page['data']=$data;
										$final_arr[]=$cat_page;
									}
									if((strpos($val, '/item/') !== false) && ($report_type == "product")){
										$p_page=array();	
										$p_page['visitor_id']=$visitor_id;
										$p_page['user_type']=$user_type;
										$p_page['geo_data']=$geo_data;
										$p_page['page']=$val;
										$p_page['data']=$data;
										$final_arr[]=$p_page;
										
									}
								}
							}
							
						}
					
					}
					
					//btn activity
					if($report_type =='button'){
						$user_behaviour_btn_activity=$data_all->user_btn_activity;
						$btn_activity = ltrim($user_behaviour_btn_activity, ', ');
						$btn_activity='['.$btn_activity.']';	
						$btn_activity=json_decode($btn_activity,true);
			
						foreach($btn_activity as $beh => $data){
	
							//print_r($val);
							$btn=array();	
							$btn['visitor_id']=$visitor_id;
							$btn['user_type']=$user_type;
							$btn['geo_data']=$geo_data;
							$btn['page']='';
							$btn['data']=$data;
							$final_arr[]=$btn;

						}
					}
	
					if($report_type =='reviews'){
						$reviews_read=$data_all->reviews_read;
						$reviews_read = ltrim($reviews_read, ', ');
						$reviews_read='['.$reviews_read.']';
						$reviews=json_decode($reviews_read,true);
						foreach($reviews as $beh => $data){
							
							foreach($data as $key => $val){
								//print_r($val);
								
								$rw=array();	
								$rw['visitor_id']=$visitor_id;
								$rw['user_type']=$user_type;
								$rw['geo_data']=$geo_data;
								$rw['page']='';
								$rw['data']=$val;
								$final_arr[]=$rw;								
								
							}
						}
						
					}
				}
				
                
                /*echo $report_type;

				echo '<pre>';
				print_r($final_arr);
				echo '</pre>';*/
				
				//exit;
				
				 //date("F j, Y, g:i a", $time);
				$str='';
			
				$str.='<h6> <b>Page - </b> '.$report_type.' ';
				//$str.=' <b>Visited - </b> '.count($final_arr);
				$str.='</h6>';
				
				if($report_type=='button'){
					
					
							
							
					
					$str.='<table class="table table-bordered">';
					$str.='<thead><th>Visitor</th><th>Geo Details</th><th>Button</th><th>Details</th><th>Time</th></thead>';
					foreach ($final_arr as $page){
						$geo_str='';
						foreach($page as $key=>$p_data){
							
							if($key=='geo_data'){
								$geo_data=$p_data;
								$geo_str.='<small>';
								$geo_str.='IP :'.$geo_data->ip.'<br>';
								$geo_str.='City :'.$geo_data->city.'<br>';
								$geo_str.='Region :'.$geo_data->region_name.'<br>';
								$geo_str.='Country :'.$geo_data->country_name.'<br>';
								$geo_str.='</small>';
							}
							if($key=="visitor_id"){
								$visitor='<b>ID : </b> '.$p_data;
							}
							if($key=="user_type"){
								if($p_data!="guest"){				
									$u='<a href="'.base_url().'admin/Manage_cust/view/'.$p_data.'" target="_blank"><u>'.$p_data.'</u></a>';
									$type_of_user_button=$p_data;
								}else{
									$u=''.$p_data;
									$type_of_user_button=$p_data;
								}
								$user_type=$u;
							}
					
							if(is_array($p_data)){
								
								$milliseconds =  $p_data["time"];
								$timestamp = $milliseconds/1000;
								if($from_date!=""){
									if (($from_date <= date("Y-m-d", $timestamp)) && ($to_date >= date("Y-m-d", $timestamp))){
										$time=date("F j, Y, g:i a", $timestamp);
										$section = str_replace('_', ' ', $p_data["btn"]);
										$dif_sec='0';
										
										$inv_id=$p_data["inventory_id"];
										$inv_obj=$this->Analytics_model->get_sku($inv_id);
										$product_name=$this->Analytics_model->get_product_name($inv_obj->product_id);
										$str.='<tr><td>'.$visitor.'<br>'.$user_type.'</td><td>'.$geo_str.'</td><td>'.$section.'</td><td>'.$inv_obj->sku_id.'</td><td>'.$time.'</td></tr>';
										$exit_time=$milliseconds;
										
										$report_type_graph_arr[$report_type][]=array("sku_id"=>$inv_obj->sku_id,"user"=>$type_of_user_button,"button_name"=>$section,"timestamp"=>$timestamp);
										
										$report_type_sku_arr[$report_type]["sku_id_list"][]=$inv_obj->sku_id." - ".$product_name;
										
										/////////////////////
										
									///////////////////
									if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]=array();
										$report_type_sku_arr[$report_type]["sku_id_list"][]=$inv_obj->sku_id." - ".$product_name;
									}
									if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"]=array();
									}
									if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"]=array();
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type_users"]=array();
									}
									if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["total_sections"])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["total_sections"]=array();
									}
									if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["guest_sections"])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["guest_sections"]=array();
									}
									if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["users_sections"])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["users_sections"]=array();
									}
								
											/////////////////
										
							////////////////////////////////////////////////			
										
								if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"][$page['visitor_id']])){
									$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"][$page['visitor_id']]+=1;
								}
								else{
									$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"][$page['visitor_id']]=1;
								}
								if($page["user_type"]!="guest"){
									if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]]+=1;
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type_users"][]=$page["user_type"];
									}
									else{
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]]=1;
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type_users"][]=$page["user_type"];
										
										
									}
								}
								if($page["user_type"]=="guest"){
									if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"]["guest"])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]]+=1;
									}
									else{
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]]=1;
									}
								}
							
							
							////////////////////
			
									}
								}
								else{
									$time=date("F j, Y, g:i a", $timestamp);
									$section = str_replace('_', ' ', $p_data["btn"]);
									$dif_sec='0';
									
									$inv_id=$p_data["inventory_id"];
									$inv_obj=$this->Analytics_model->get_sku($inv_id);
									$product_name=$this->Analytics_model->get_product_name($inv_obj->product_id);
									$str.='<tr><td>'.$visitor.'<br>'.$user_type.'</td><td>'.$geo_str.'</td><td>'.$section.'</td><td>'.$inv_obj->sku_id.'</td><td>'.$time.'</td></tr>';
									$exit_time=$milliseconds;
									
									$report_type_graph_arr[$report_type][]=array("sku_id"=>$inv_obj->sku_id,"user"=>$type_of_user_button,"button_name"=>$section,"timestamp"=>$timestamp);
									
								
									
									$report_type_sku_arr[$report_type]["sku_id_list"][]=$inv_obj->sku_id." - ".$product_name;
									
									///////////////////
									if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]=array();
										$report_type_sku_arr[$report_type]["sku_id_list"][]=$inv_obj->sku_id." - ".$product_name;
									}
									if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"]=array();
									}
									if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"]=array();
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type_users"]=array();
									}
									if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["total_sections"])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["total_sections"]=array();
									}
									if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["guest_sections"])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["guest_sections"]=array();
									}
									if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["users_sections"])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["users_sections"]=array();
									}
								
											/////////////////
											
											
										////////////////////////////////////////////////			
										
								if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"][$page['visitor_id']])){
									$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"][$page['visitor_id']]+=1;
								}
								else{
									$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"][$page['visitor_id']]=1;
								}
								if($page["user_type"]!="guest"){
									if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]]+=1;
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type_users"][]=$page["user_type"];
									}
									else{
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]]=1;
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type_users"][]=$page["user_type"];
										
										
									}
								}
								if($page["user_type"]=="guest"){
									if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"]["guest"])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]]+=1;
									}
									else{
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]]=1;
									}
								}
							
							
							////////////////////
							/////
							///////////////////////////////
							if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["total_sections"][$section])){
								$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["total_sections"][$section]+=$dif_sec;
							}
							else{
								$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["total_sections"][$section]=0;
							}
							if($page["user_type"]=="guest"){
								if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["guest_sections"][$section])){
									$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["guest_sections"][$section]+=$dif_sec;
								}
								else{
									$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["guest_sections"][$section]=0;
								}
							}
							if($page["user_type"]!="guest"){
								if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["users_sections"][$section])){
									$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["users_sections"][$section]+=$dif_sec;
								}
								else{
									$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["users_sections"][$section]=0;
								}
							}
														////////////////
							////////////////////////////////
							
							
											
									
								}
								
							}
								
							
						}
					}
					
					$str.='</table>';
				}
			
				if($report_type=='reviews'){
					
					$str.='<table class="table table-bordered">';
					//$str.='<thead><th>Visitor</th><th>Geo Details</th><th>Detail</th><th>Reviews</th><th>Time</th><th>Stay time(sec)</th></thead>';
					$str.='<thead><th>Visitor</th><th>Geo Details</th><th>Detail</th><th>Reviews</th><th>Time</th></thead>';
					foreach ($final_arr as $page){
						$geo_str='';
						foreach($page as $key=>$p_data){
							
							if($key=='geo_data'){
								$geo_data=$p_data;
								$geo_str.='<small>';
								$geo_str.='IP :'.$geo_data->ip.'<br>';
								$geo_str.='City :'.$geo_data->city.'<br>';
								$geo_str.='Region :'.$geo_data->region_name.'<br>';
								$geo_str.='Country :'.$geo_data->country_name.'<br>';
								$geo_str.='</small>';
							}
							if($key=="visitor_id"){
								$visitor='<b>ID : </b> '.$p_data;
							}
							if($key=="user_type"){
								if($p_data!="guest"){				
									$u='<a href="'.base_url().'admin/Manage_cust/view/'.$p_data.'" target="_blank"><u>'.$p_data.'</u></a>';
								}else{
									$u=''.$p_data;
								}
								$user_type=$u;
							}
							$total_time=0;
							$exit_time='';
							$flag_totaltime_spent="no";
							if(is_array($p_data)){
								
								foreach($p_data as $k=>$p_dat){
									$milliseconds=$p_dat['start'];
									$timestamp = $milliseconds/1000;
			//////////////////////////////////////////////////////////////////////////
									
									if($from_date!=""){
										if (($from_date <= date("Y-m-d", $timestamp)) && ($to_date >= date("Y-m-d", $timestamp))){
											$time=date("F j, Y, g:i a", $timestamp);
											$inv_id=$p_dat["inventory_id"];
											$inv_obj=$this->Analytics_model->get_sku($inv_id);
											$dif_sec='0';
											if(isset($exit_time) && $exit_time!=''){
												$dif_sec=($milliseconds-$exit_time)/1000;
												$total_time+=$dif_sec;
											}
			
											$exit_time=$milliseconds;
													
											$re_arr=$p_dat["reviews"];
											$re_str='<table class="table small">';
											$re_str.='<thead><th>ID</th><th>Time</th></thead>';
											foreach($re_arr as $v_arr){
												$re_str.='<tr>';
												$re_str.='<td>'.$v_arr['review_id'].'</td>';
												$m=$v_arr['time'];
												$timestamp_r = $m/1000;
												$time_r=date("F j, Y, g:i a", $timestamp_r);
												
												$re_str.='<td>'.$time_r.'</td>';
												$re_str.='</tr>';
											}
											$re_str.='</table>';
											$flag_totaltime_spent="yes";
											//$str.='<tr><td>'.$visitor.'<br>'.$user_type.'</td><td>'.$geo_str.'</td><td>'.$inv_obj->sku_id.'</td><td>'.$re_str.'</td><td>'.$time.'</td><td>'.$dif_sec.'</td></tr>';
											$str.='<tr><td>'.$visitor.'<br>'.$user_type.'</td><td>'.$geo_str.'</td><td>'.$inv_obj->sku_id.'</td><td>'.$re_str.'</td><td>'.$time.'</td></tr>';
											//print_r($p_dat["reviews"]);
											
											////////////////////
											///////////////////
											$product_name=$this->Analytics_model->get_product_name($inv_obj->product_id);
											
											$report_type_sku_arr[$report_type]["sku_id_list"][]=$inv_obj->sku_id." - ".$product_name;
											
											
											if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name])){
												$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]=array();
												$report_type_sku_arr[$report_type]["sku_id_list"][]=$inv_obj->sku_id." - ".$product_name;
											}
											if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"])){
												$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"]=array();
											}
											if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"])){
												$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"]=array();
												$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type_users"]=array();
											}
											if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["total_sections"])){
												$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["total_sections"]=array();
											}
											if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["guest_sections"])){
												$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["guest_sections"]=array();
											}
											if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["users_sections"])){
												$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["users_sections"]=array();
											}
								
											/////////////////
											//////////////////////
											
											
												
										////////////////////////////////////////////////			
										
								if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"][$page['visitor_id']])){
									$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"][$page['visitor_id']]+=1;
								}
								else{
									$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"][$page['visitor_id']]=1;
								}
								if($page["user_type"]!="guest"){
									if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]]+=1;
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type_users"][]=$page["user_type"];
									}
									else{
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]]=1;
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type_users"][]=$page["user_type"];
										
										
									}
								}
								if($page["user_type"]=="guest"){
									if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"]["guest"])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]]+=1;
									}
									else{
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]]=1;
									}
								}
							
							
							////////////////////
						
							
							
							
							
										}
									}
									else{
										$time=date("F j, Y, g:i a", $timestamp);
										$inv_id=$p_dat["inventory_id"];
										$inv_obj=$this->Analytics_model->get_sku($inv_id);
										$dif_sec='0';
										if(isset($exit_time) && $exit_time!=''){
											$dif_sec=($milliseconds-$exit_time)/1000;
											$total_time+=$dif_sec;
										}
		
										$exit_time=$milliseconds;
												
										$re_arr=$p_dat["reviews"];
										$re_str='<table class="table small">';
										$re_str.='<thead><th>ID</th><th>Time</th></thead>';
										foreach($re_arr as $v_arr){
											$re_str.='<tr>';
											$re_str.='<td>'.$v_arr['review_id'].'</td>';
											$m=$v_arr['time'];
											$timestamp_r = $m/1000;
											$time_r=date("F j, Y, g:i a", $timestamp_r);
											
											$re_str.='<td>'.$time_r.'</td>';
											$re_str.='</tr>';
										}
										$re_str.='</table>';
										
										//$str.='<tr><td>'.$visitor.'<br>'.$user_type.'</td><td>'.$geo_str.'</td><td>'.$inv_obj->sku_id.'</td><td>'.$re_str.'</td><td>'.$time.'</td><td>'.$dif_sec.'</td></tr>';
										$str.='<tr><td>'.$visitor.'<br>'.$user_type.'</td><td>'.$geo_str.'</td><td>'.$inv_obj->sku_id.'</td><td>'.$re_str.'</td><td>'.$time.'</td></tr>';
										
										$flag_totaltime_spent="yes";
										
										//print_r($p_dat["reviews"]);
										
										/////////////////////////////////////////
										///////////////////
											$product_name=$this->Analytics_model->get_product_name($inv_obj->product_id);
											
											$report_type_sku_arr[$report_type]["sku_id_list"][]=$inv_obj->sku_id." - ".$product_name;
											
											
											if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name])){
												$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]=array();
												$report_type_sku_arr[$report_type]["sku_id_list"][]=$inv_obj->sku_id." - ".$product_name;
											}
											if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"])){
												$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"]=array();
											}
											if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"])){
												$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"]=array();
												$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type_users"]=array();
											}
											if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["total_sections"])){
												$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["total_sections"]=array();
											}
											if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["guest_sections"])){
												$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["guest_sections"]=array();
											}
											if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["users_sections"])){
												$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["users_sections"]=array();
											}
								
											/////////////////
										////////////////////////////////////////
										
										
											
										////////////////////////////////////////////////			
										
								if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"][$page['visitor_id']])){
									$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"][$page['visitor_id']]+=1;
								}
								else{
									$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["visitor_id"][$page['visitor_id']]=1;
								}
								if($page["user_type"]!="guest"){
									if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]]+=1;
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type_users"][]=$page["user_type"];
									}
									else{
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]]=1;
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type_users"][]=$page["user_type"];
										
										
									}
								}
								if($page["user_type"]=="guest"){
									if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"]["guest"])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]]+=1;
									}
									else{
										$report_type_graph_arr[$report_type][$inv_obj->sku_id." - ".$product_name]["user_type"][$page["user_type"]]=1;
									}
								}
							
							
							////////////////////
							/////
							
							
							
							
							
									}
									
									
			//////////////////////////////////////////////////////////////////////////
									
								}
								if($flag_totaltime_spent=="yes"){
									//$str.='<tr class="text-danger"><td colspan="5">Total time spent</td><td>'.$total_time.'</td></tr>';
								}
								$flag_totaltime_spent="no";
							}

						}
					}
					$str.='</table>';
				}
				
				if($report_type!='button' && $report_type!='reviews'){
					
				foreach ($final_arr as $key_final => $page){
					
					$geo_str='';
					$str.='<div class="row">';
					$str.='<div class="col-md-6">';
						if($report_type == "category"){
							$p_arr=(explode("/",$page['page']));
							$cat_id_str=end($p_arr);
							$cat_id=substr($cat_id_str, 9);
							$cat_name=$this->Analytics_model->get_cat_name($cat_id);
							$str.='<h6> <b>Category </b> '.$cat_name.'</h6>';
							
							///////////////////
							if(!isset($report_type_graph_arr[$report_type][$cat_name])){
								$report_type_graph_arr[$report_type][$cat_name]=array();
								if($from_date==""){
									$report_type_sku_arr[$report_type]["sku_id_list"][]=$cat_name;
								}
							}
							if(!isset($report_type_graph_arr[$report_type][$cat_name]["visitor_id"])){
								$report_type_graph_arr[$report_type][$cat_name]["visitor_id"]=array();
							}
							if(!isset($report_type_graph_arr[$report_type][$cat_name]["user_type"])){
								$report_type_graph_arr[$report_type][$cat_name]["user_type"]=array();
								$report_type_graph_arr[$report_type][$cat_name]["user_type_users"]=array();
							}
							if(!isset($report_type_graph_arr[$report_type][$cat_name]["total_sections"])){
								$report_type_graph_arr[$report_type][$cat_name]["total_sections"]=array();
							}
							if(!isset($report_type_graph_arr[$report_type][$cat_name]["guest_sections"])){
								$report_type_graph_arr[$report_type][$cat_name]["guest_sections"]=array();
							}
							if(!isset($report_type_graph_arr[$report_type][$cat_name]["users_sections"])){
								$report_type_graph_arr[$report_type][$cat_name]["users_sections"]=array();
							}
							/////////////////
							
						}
						if($report_type == "product"){
							$p_arr=(explode("/",$page['page']));
							$sku_id=end($p_arr);
							
							//$inv_id=substr($inv_id_str, 9);
							$inv_obj=$this->Analytics_model->get_sku_by_sku_id($sku_id);
							if(isset($inv_obj->sku_id)){
								$str.='<h6> <b>Inventory </b> '.$inv_obj->sku_id.'</h6>';
								
								///////////////////
								if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id])){
									$report_type_graph_arr[$report_type][$inv_obj->sku_id]=array();
									if($from_date==""){
										$report_type_sku_arr[$report_type]["sku_id_list"][]=$inv_obj->sku_id;
									}
								}
								if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id]["visitor_id"])){
									$report_type_graph_arr[$report_type][$inv_obj->sku_id]["visitor_id"]=array();
								}
								if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id]["user_type"])){
									$report_type_graph_arr[$report_type][$inv_obj->sku_id]["user_type"]=array();
									$report_type_graph_arr[$report_type][$inv_obj->sku_id]["user_type_users"]=array();
								}
								if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id]["total_sections"])){
									$report_type_graph_arr[$report_type][$inv_obj->sku_id]["total_sections"]=array();
								}
								if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id]["guest_sections"])){
									$report_type_graph_arr[$report_type][$inv_obj->sku_id]["guest_sections"]=array();
								}
								if(!isset($report_type_graph_arr[$report_type][$inv_obj->sku_id]["users_sections"])){
									$report_type_graph_arr[$report_type][$inv_obj->sku_id]["users_sections"]=array();
								}
							}
							/////////////////
						}
						
						if($report_type == "home"){
							///////////////////
							if(!isset($report_type_graph_arr[$report_type])){
								$report_type_graph_arr[$report_type]=array();
							}
							if(!isset($report_type_graph_arr[$report_type]["visitor_id"])){
								$report_type_graph_arr[$report_type]["visitor_id"]=array();
							}
							if(!isset($report_type_graph_arr[$report_type]["user_type"])){
								$report_type_graph_arr[$report_type]["user_type"]=array();
								$report_type_graph_arr[$report_type]["user_type_users"]=array();
							}
							if(!isset($report_type_graph_arr[$report_type]["total_sections"])){
								$report_type_graph_arr[$report_type]["total_sections"]=array();
							}
							if(!isset($report_type_graph_arr[$report_type]["guest_sections"])){
								$report_type_graph_arr[$report_type]["guest_sections"]=array();
							}
							if(!isset($report_type_graph_arr[$report_type]["users_sections"])){
								$report_type_graph_arr[$report_type]["users_sections"]=array();
							}
							/////////////////
						}
						
						if($report_type == "product"){
							$str.='<h6> <b>visitor id </b> '.$page['visitor_id'].'</h6>';
							if(isset($inv_obj->sku_id)){
								if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id]["visitor_id"][$page['visitor_id']])){
									$report_type_graph_arr[$report_type][$inv_obj->sku_id]["visitor_id"][$page['visitor_id']]+=1;
								}
								else{
									$report_type_graph_arr[$report_type][$inv_obj->sku_id]["visitor_id"][$page['visitor_id']]=1;
								}
								if($page["user_type"]!="guest"){				
									$str.='<h6> <b>user Type</b> <a href="'.base_url().'admin/Manage_cust/view/'.$page["user_type"].'" target="_blank"><u>'.$page["user_type"].'</u></a></h6>';
									if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id]["user_type"][$page["user_type"]])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id]["user_type"][$page["user_type"]]+=1;
										$report_type_graph_arr[$report_type][$inv_obj->sku_id]["user_type_users"][]=$page["user_type"];
									}
									else{
										$report_type_graph_arr[$report_type][$inv_obj->sku_id]["user_type"][$page["user_type"]]=1;
										$report_type_graph_arr[$report_type][$inv_obj->sku_id]["user_type_users"][]=$page["user_type"];
										
										
									}
								}
								if($page["user_type"]=="guest"){
									$str.='<h6> <b>user Type</b> '.$page["user_type"].'</h6>';
									if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id]["user_type"]["guest"])){
										$report_type_graph_arr[$report_type][$inv_obj->sku_id]["user_type"][$page["user_type"]]+=1;
									}
									else{
										$report_type_graph_arr[$report_type][$inv_obj->sku_id]["user_type"][$page["user_type"]]=1;
									}
								}
							}
							
						}
						// other than product
						else if($report_type == "home"){
							
							$str.='<h6> <b>visitor id </b> '.$page['visitor_id'].'</h6>';
							if(isset($report_type_graph_arr[$report_type]["visitor_id"][$page['visitor_id']])){
								$report_type_graph_arr[$report_type]["visitor_id"][$page['visitor_id']]+=1;
							}
							else{
								$report_type_graph_arr[$report_type]["visitor_id"][$page['visitor_id']]=1;
							}
							if($page["user_type"]!="guest"){				
								$str.='<h6> <b>user Type</b> <a href="'.base_url().'admin/Manage_cust/view/'.$page["user_type"].'" target="_blank"><u>'.$page["user_type"].'</u></a></h6>';
								if(isset($report_type_graph_arr[$report_type]["user_type"][$page["user_type"]])){
									$report_type_graph_arr[$report_type]["user_type"][$page["user_type"]]+=1;
									$report_type_graph_arr[$report_type]["user_type_users"][]=$page["user_type"];
								}
								else{
									$report_type_graph_arr[$report_type]["user_type"][$page["user_type"]]=1;
									$report_type_graph_arr[$report_type]["user_type_users"][]=$page["user_type"];
									
									
								}
							}
							if($page["user_type"]=="guest"){
								$str.='<h6> <b>user Type</b> '.$page["user_type"].'</h6>';
								if(isset($report_type_graph_arr[$report_type]["user_type"]["guest"])){
									$report_type_graph_arr[$report_type]["user_type"][$page["user_type"]]+=1;
								}
								else{
									$report_type_graph_arr[$report_type]["user_type"][$page["user_type"]]=1;
								}
							}
						}
						
						if($report_type == "category"){
							$str.='<h6> <b>visitor id </b> '.$page['visitor_id'].'</h6>';
							if(isset($report_type_graph_arr[$report_type][$cat_name]["visitor_id"][$page['visitor_id']])){
								$report_type_graph_arr[$report_type][$cat_name]["visitor_id"][$page['visitor_id']]+=1;
							}
							else{
								$report_type_graph_arr[$report_type][$cat_name]["visitor_id"][$page['visitor_id']]=1;
							}
							if($page["user_type"]!="guest"){			
								$str.='<h6> <b>user Type</b> <a href="'.base_url().'admin/Manage_cust/view/'.$page["user_type"].'" target="_blank"><u>'.$page["user_type"].'</u></a></h6>';
								if(isset($report_type_graph_arr[$report_type][$cat_name]["user_type"][$page["user_type"]])){
									$report_type_graph_arr[$report_type][$cat_name]["user_type"][$page["user_type"]]+=1;
									$report_type_graph_arr[$report_type][$cat_name]["user_type_users"][]=$page["user_type"];
								}
								else{
									$report_type_graph_arr[$report_type][$cat_name]["user_type"][$page["user_type"]]=1;
									$report_type_graph_arr[$report_type][$cat_name]["user_type_users"][]=$page["user_type"];
									
									
								}
							}
							if($page["user_type"]=="guest"){
								$str.='<h6> <b>user Type</b> '.$page["user_type"].'</h6>';
								if(isset($report_type_graph_arr[$report_type][$cat_name]["user_type"]["guest"])){
									$report_type_graph_arr[$report_type][$cat_name]["user_type"][$page["user_type"]]+=1;
								}
								else{
									$report_type_graph_arr[$report_type][$cat_name]["user_type"][$page["user_type"]]=1;
								}
							}
						}
						/*else{
							$str.='<h6> <b>visitor id </b> '.$page['visitor_id'].'</h6>';
							if($page["user_type"]!="guest"){				
								$str.='<h6> <b>user Type</b> <a href="'.base_url().'admin/Manage_cust/view/'.$page["user_type"].'" target="_blank"><u>'.$page["user_type"].'</u></a></h6>';
							}
							if($page["user_type"]=="guest"){
								$str.='<h6> <b>user Type</b> '.$page["user_type"].'</h6>';
							}
						}*/
						
					$str.='</div>';	
					$str.='<div class="col-md-6 text-right">';
					//print_r($page["geo_data"]);
					 
						if($page["geo_data"]){
							$geo_data=$page["geo_data"];
							if(is_object($geo_data)) {
							    if(isset($geo_data->ip)) {
                                    $geo_str .= $geo_data->ip . ' : <b>IP</b><br>';
                                    $geo_str .= $geo_data->city . ' : <b>City</b><br>';
                                    $geo_str .= $geo_data->region_name . ' : <b>Region</b><br>';
                                    $geo_str .= $geo_data->country_name . ' : <b>Country</b><br>';
                                }
                            }
						}
						$str.=$geo_str;
					$str.='</div>';	
					$str.='</div>';	
					
					if(is_array($page)){
						
						foreach($page as $p_data){
							
							if(is_array($p_data)){
								
								foreach($p_data as $p_dat){
									
									$str.='<table class="table table-bordered">';						
								
									if(is_array($p_dat)){
										$total_time=0;
										$exit_time='';

										$str.='<thead><th>Section</th><th>ViewTime</th><th>Stay time(sec)</th></thead>';
										
										foreach($p_dat as $t_dat){
											
											$milliseconds =  $t_dat["time"];
											$timestamp = $milliseconds/1000;
											$time=date("F j, Y, g:i a", $timestamp);
											
											if($from_date!=""){
												if($report_type=="product"){
													if (($from_date <= date("Y-m-d", $timestamp)) && ($to_date >= date("Y-m-d", $timestamp))){
														
														$section = str_replace('_', ' ', $t_dat["id"]);
														$dif_sec='0';
														if(isset($exit_time) && $exit_time!=''){
															$dif_sec=($milliseconds-$exit_time)/1000;
															$total_time+=$dif_sec;
														}
														$exit_time=$milliseconds;
														//////////////
														if(isset($inv_obj->sku_id)){
															if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id]["total_sections"][$section])){
																$report_type_graph_arr[$report_type][$inv_obj->sku_id]["total_sections"][$section]+=$dif_sec;
															}
															else{
																$report_type_graph_arr[$report_type][$inv_obj->sku_id]["total_sections"][$section]=0;
															}
															if($page["user_type"]=="guest"){
																if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id]["guest_sections"][$section])){
																	$report_type_graph_arr[$report_type][$inv_obj->sku_id]["guest_sections"][$section]+=$dif_sec;
																}
																else{
																	$report_type_graph_arr[$report_type][$inv_obj->sku_id]["guest_sections"][$section]=0;
																}
															}
															if($page["user_type"]!="guest"){
																if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id]["users_sections"][$section])){
																	$report_type_graph_arr[$report_type][$inv_obj->sku_id]["users_sections"][$section]+=$dif_sec;
																}
																else{
																	$report_type_graph_arr[$report_type][$inv_obj->sku_id]["users_sections"][$section]=0;
																}
															}
															
														}
														////////////////
														
														$str.='<tr><td>'.$section.'</td><td>'.$time.'</td><td>'.$dif_sec.'</td></tr>';
														/////////
														$report_type_sku_arr[$report_type]["sku_id_list"][]=$inv_obj->sku_id;
														///////////////
													}
												}
												else if($report_type=="home"){
													if (($from_date <= date("Y-m-d", $timestamp)) && ($to_date >= date("Y-m-d", $timestamp))){
														
														$section = str_replace('_', ' ', $t_dat["id"]);
														$dif_sec='0';
														if(isset($exit_time) && $exit_time!=''){
															$dif_sec=($milliseconds-$exit_time)/1000;
															$total_time+=$dif_sec;
														}
														$exit_time=$milliseconds;
														//////////////
														if(isset($report_type_graph_arr[$report_type]["total_sections"][$section])){
															$report_type_graph_arr[$report_type]["total_sections"][$section]+=$dif_sec;
														}
														else{
															$report_type_graph_arr[$report_type]["total_sections"][$section]=0;
														}
														if($page["user_type"]=="guest"){
															if(isset($report_type_graph_arr[$report_type]["guest_sections"][$section])){
																$report_type_graph_arr[$report_type]["guest_sections"][$section]+=$dif_sec;
															}
															else{
																$report_type_graph_arr[$report_type]["guest_sections"][$section]=0;
															}
														}
														if($page["user_type"]!="guest"){
															if(isset($report_type_graph_arr[$report_type]["users_sections"][$section])){
																$report_type_graph_arr[$report_type]["users_sections"][$section]+=$dif_sec;
															}
															else{
																$report_type_graph_arr[$report_type]["users_sections"][$section]=0;
															}
														}
														////////////////
														
														$str.='<tr><td>'.$section.'</td><td>'.$time.'</td><td>'.$dif_sec.'</td></tr>';
													}
												}
												else if($report_type=="category"){
													if (($from_date <= date("Y-m-d", $timestamp)) && ($to_date >= date("Y-m-d", $timestamp))){
														
														$section = str_replace('_', ' ', $t_dat["id"]);
														$dif_sec='0';
														if(isset($exit_time) && $exit_time!=''){
															$dif_sec=($milliseconds-$exit_time)/1000;
															$total_time+=$dif_sec;
														}
														$exit_time=$milliseconds;
														//////////////
														if(isset($report_type_graph_arr[$report_type][$cat_name]["total_sections"][$section])){
															$report_type_graph_arr[$report_type][$cat_name]["total_sections"][$section]+=$dif_sec;
														}
														else{
															$report_type_graph_arr[$report_type][$cat_name]["total_sections"][$section]=0;
														}
														if($page["user_type"]=="guest"){
															if(isset($report_type_graph_arr[$report_type][$cat_name]["guest_sections"][$section])){
																$report_type_graph_arr[$report_type][$cat_name]["guest_sections"][$section]+=$dif_sec;
															}
															else{
																$report_type_graph_arr[$report_type][$cat_name]["guest_sections"][$section]=0;
															}
														}
														if($page["user_type"]!="guest"){
															if(isset($report_type_graph_arr[$report_type][$cat_name]["users_sections"][$section])){
																$report_type_graph_arr[$report_type][$cat_name]["users_sections"][$section]+=$dif_sec;
															}
															else{
																$report_type_graph_arr[$report_type][$cat_name]["users_sections"][$section]=0;
															}
														}
														////////////////
														
														$str.='<tr><td>'.$section.'</td><td>'.$time.'</td><td>'.$dif_sec.'</td></tr>';
														
														
														////
														
														$report_type_sku_arr[$report_type]["sku_id_list"][]=$cat_name;
							
														
														/////
													}
												}
												
											}
											else{
													$section = str_replace('_', ' ', $t_dat["id"]);
													$dif_sec='0';
													if(isset($exit_time) && $exit_time!=''){
														$dif_sec=($milliseconds-$exit_time)/1000;
														$total_time+=$dif_sec;
													}
													$exit_time=$milliseconds;
													//////////////
													if($report_type=="product"){
														if(isset($inv_obj->sku_id)){
															if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id]["total_sections"][$section])){
																$report_type_graph_arr[$report_type][$inv_obj->sku_id]["total_sections"][$section]+=$dif_sec;
															}
															else{
																$report_type_graph_arr[$report_type][$inv_obj->sku_id]["total_sections"][$section]=0;
															}
															if($page["user_type"]=="guest"){
																if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id]["guest_sections"][$section])){
																	$report_type_graph_arr[$report_type][$inv_obj->sku_id]["guest_sections"][$section]+=$dif_sec;
																}
																else{
																	$report_type_graph_arr[$report_type][$inv_obj->sku_id]["guest_sections"][$section]=0;
																}
															}
															if($page["user_type"]!="guest"){
																if(isset($report_type_graph_arr[$report_type][$inv_obj->sku_id]["users_sections"][$section])){
																	$report_type_graph_arr[$report_type][$inv_obj->sku_id]["users_sections"][$section]+=$dif_sec;
																}
																else{
																	$report_type_graph_arr[$report_type][$inv_obj->sku_id]["users_sections"][$section]=0;
																}
															}
														}
														
														$str.='<tr><td>'.$section.'</td><td>'.$time.'</td><td>'.$dif_sec.'</td></tr>';
													}
													else if($report_type=="home"){
														if(isset($report_type_graph_arr[$report_type]["total_sections"][$section])){
															$report_type_graph_arr[$report_type]["total_sections"][$section]+=$dif_sec;
														}
														else{
															$report_type_graph_arr[$report_type]["total_sections"][$section]=0;
														}
														if($page["user_type"]=="guest"){
															if(isset($report_type_graph_arr[$report_type]["guest_sections"][$section])){
																$report_type_graph_arr[$report_type]["guest_sections"][$section]+=$dif_sec;
															}
															else{
																$report_type_graph_arr[$report_type]["guest_sections"][$section]=0;
															}
														}
														if($page["user_type"]!="guest"){
															if(isset($report_type_graph_arr[$report_type]["users_sections"][$section])){
																$report_type_graph_arr[$report_type]["users_sections"][$section]+=$dif_sec;
															}
															else{
																$report_type_graph_arr[$report_type]["users_sections"][$section]=0;
															}
														}
														
														$str.='<tr><td>'.$section.'</td><td>'.$time.'</td><td>'.$dif_sec.'</td></tr>';
													}
													else if($report_type=="category"){
														if(isset($report_type_graph_arr[$report_type][$cat_name]["total_sections"][$section])){
															$report_type_graph_arr[$report_type][$cat_name]["total_sections"][$section]+=$dif_sec;
														}
														else{
															$report_type_graph_arr[$report_type][$cat_name]["total_sections"][$section]=0;
														}
														if($page["user_type"]=="guest"){
															if(isset($report_type_graph_arr[$report_type][$cat_name]["guest_sections"][$section])){
																$report_type_graph_arr[$report_type][$cat_name]["guest_sections"][$section]+=$dif_sec;
															}
															else{
																$report_type_graph_arr[$report_type][$cat_name]["guest_sections"][$section]=0;
															}
														}
														if($page["user_type"]!="guest"){
															if(isset($report_type_graph_arr[$report_type][$cat_name]["users_sections"][$section])){
																$report_type_graph_arr[$report_type][$cat_name]["users_sections"][$section]+=$dif_sec;
															}
															else{
																$report_type_graph_arr[$report_type][$cat_name]["users_sections"][$section]=0;
															}
														}
														
														$str.='<tr><td>'.$section.'</td><td>'.$time.'</td><td>'.$dif_sec.'</td></tr>';
													}
													else{
														
														$str.='<tr><td>'.$section.'</td><td>'.$time.'</td><td>'.$dif_sec.'</td></tr>';
													}
													////////////////
													
											}
										}
										
										
											$str.='<tfoot class="text-danger"><td colspan="2" >Total time spent</td><td>'.$total_time.'</td></tfoot>';
										
										
									}
									
									$str.='</table>';
								}
								
								
							
							}
							
						}
					}
					
				}
				
				}//user behaviour on section
				/*echo "<pre>";
				print_r($report_type_graph_arr);
				echo "</pre>";*/
				//$arr=array("first"=>1,"second"=>2);
				if($from_date!=""){
					$from_date_format=date("D j M,Y",strtotime($from_date));
				}
				else{
					$from_date_format="";
				}
				if($to_date!=""){
					$to_date_format=date("D j M,Y",strtotime($to_date));
				}
				else{
					$to_date_format="";
				}
				
				if(isset($report_type_sku_arr[$report_type]["sku_id_list"])){
					$report_type_sku_arr[$report_type]["sku_id_list"]=array_unique($report_type_sku_arr[$report_type]["sku_id_list"]);
				}
				
				if(isset($report_type_graph_arr[$report_type]["total_sections"])){
					//print_r($report_type_graph_arr[$report_type]["user_type_users"]);
					if(empty($report_type_graph_arr[$report_type]["total_sections"])){
						$report_type_graph_arr[$report_type]["user_type"]["guest"]=0;
						$str="No Data Found";
					}
				}
				if($report_type=="category" || $report_type=="button"){
					if(empty($report_type_sku_arr[$report_type])){
						$str="No Data Found";
					}
				}
				
				
				echo json_encode(array("table_format"=>$str,"graph_format"=>$report_type_graph_arr,"sku_list"=>$report_type_sku_arr,"from_date"=>$from_date_format,"to_date"=>$to_date_format));
				//echo $str;
				exit;
			
		}else{		
            redirect(base_url());
        }
	}
	
	public function get_graph_by_sku_product(){
		$type_menu=$this->input->post("type_menu");
		$graph_format_json=$this->input->post("graph_format_json");
		$skuId=$this->input->post("skuId");
		$graph_format_arr=json_decode($graph_format_json,true);
		$sku_info_arr=$graph_format_arr[$type_menu][$skuId];
		if(isset($sku_info_arr["user_type"]["guest"])){
			$no_of_guests=$sku_info_arr["user_type"]["guest"];
		}
		else{
			$no_of_guests=0;
		}
		if(isset($sku_info_arr["user_type_users"])){
			$user_type_users=$sku_info_arr["user_type_users"];
			$no_of_unique_users=count(array_count_values($user_type_users));
			$no_of_repeat_users=0;
			foreach(array_count_values($user_type_users) as $k => $v){
				if($v>1){
					$no_of_repeat_users++;
				}
			}
		}
		else{
			$no_of_unique_users=0;
			$no_of_repeat_users=0;
		}
		$users=array();
		$users_graph=array();
		$users["name"]="Guests";
		$users["y"]=$no_of_guests;
		$users_graph[]=$users;
		$users["name"]="Unique Users";
		$users["y"]=$no_of_unique_users;
		$users_graph[]=$users;
		$users["name"]="Repeat Users";
		$users["y"]=$no_of_repeat_users;
		$users_graph[]=$users;
		////////////////////////
		$total_sections=array();
		$total_sections_graphs=array();
		$total_sections_arr=$sku_info_arr["total_sections"];
		if(!empty($total_sections_arr)){
			foreach($total_sections_arr as $section => $timespent){
				$total_sections["name"]=$section;
				$total_sections["y"]=$timespent;
				$total_sections_graphs[]=$total_sections;
			}
		}
		
		$guest_sections=array();
		$guest_sections_graphs=array();
		$guest_sections_arr=$sku_info_arr["guest_sections"];
		if(!empty($guest_sections_arr)){
			foreach($guest_sections_arr as $section => $timespent){
				$guest_sections["name"]=$section;
				$guest_sections["y"]=$timespent;
				$guest_sections_graphs[]=$guest_sections;
			}
		}
		
		$users_sections=array();
		$users_sections_graphs=array();
		$users_sections_arr=$sku_info_arr["users_sections"];
		if(!empty($users_sections_arr)){
			foreach($users_sections_arr as $section => $timespent){
				$users_sections["name"]=$section;
				$users_sections["y"]=$timespent;
				$users_sections_graphs[]=$users_sections;
			}
		}
		echo json_encode(array("users_graph"=>$users_graph,"total_sections_graphs"=>$total_sections_graphs,"guest_sections_graphs"=>$guest_sections_graphs,"users_sections_graphs"=>$users_sections_graphs));
	}
	
	public function get_graph_home(){
		$graph_format_json=$this->input->post("graph_format_json");
		$graph_format_arr=json_decode($graph_format_json,true);
		$sku_info_arr=$graph_format_arr["home"];
		if(isset($sku_info_arr["user_type"]["guest"])){
			$no_of_guests=$sku_info_arr["user_type"]["guest"];
		}
		else{
			$no_of_guests=0;
		}
		if(isset($sku_info_arr["user_type_users"])){
			$user_type_users=$sku_info_arr["user_type_users"];
			$no_of_unique_users=count(array_count_values($user_type_users));
			$no_of_repeat_users=0;
			foreach(array_count_values($user_type_users) as $k => $v){
				if($v>1){
					$no_of_repeat_users++;
				}
			}
		}
		else{
			$no_of_unique_users=0;
			$no_of_repeat_users=0;
		}
		$users=array();
		$users_graph=array();
		$users["name"]="Guests";
		$users["y"]=$no_of_guests;
		$users_graph[]=$users;
		$users["name"]="Unique Users";
		$users["y"]=$no_of_unique_users;
		$users_graph[]=$users;
		$users["name"]="Repeat Users";
		$users["y"]=$no_of_repeat_users;
		$users_graph[]=$users;
		////////////////////////
		$total_sections=array();
		$total_sections_graphs=array();
		if(isset($sku_info_arr["total_sections"])){
			$total_sections_arr=$sku_info_arr["total_sections"];
		}
		else{
			$total_sections_arr=array();
		}
		if(!empty($total_sections_arr)){
			foreach($total_sections_arr as $section => $timespent){
				$total_sections["name"]=$section;
				$total_sections["y"]=$timespent;
				$total_sections_graphs[]=$total_sections;
			}
		}
		
		$guest_sections=array();
		$guest_sections_graphs=array();
		if(isset($sku_info_arr["guest_sections"])){
			$guest_sections_arr=$sku_info_arr["guest_sections"];
		}
		else{
			$guest_sections_arr=array();
		}
		if(!empty($guest_sections_arr)){
			foreach($guest_sections_arr as $section => $timespent){
				$guest_sections["name"]=$section;
				$guest_sections["y"]=$timespent;
				$guest_sections_graphs[]=$guest_sections;
			}
		}
		
		$users_sections=array();
		$users_sections_graphs=array();
		if(isset($sku_info_arr["users_sections"])){
			$users_sections_arr=$sku_info_arr["users_sections"];
		}
		else{
			$users_sections_arr=array();
		}
		if(!empty($users_sections_arr)){
			foreach($users_sections_arr as $section => $timespent){
				$users_sections["name"]=$section;
				$users_sections["y"]=$timespent;
				$users_sections_graphs[]=$users_sections;
			}
		}
		echo json_encode(array("users_graph"=>$users_graph,"total_sections_graphs"=>$total_sections_graphs,"guest_sections_graphs"=>$guest_sections_graphs,"users_sections_graphs"=>$users_sections_graphs));
	}
	
	
	public function get_graph_categories(){
		$type_menu=$this->input->post("type_menu");
		$graph_format_json=$this->input->post("graph_format_json");
		$skuId=$this->input->post("skuId");
		$graph_format_arr=json_decode($graph_format_json,true);
		$sku_info_arr=$graph_format_arr[$type_menu][$skuId];
		
		if(isset($sku_info_arr["user_type"]["guest"])){
			$no_of_guests=$sku_info_arr["user_type"]["guest"];
		}
		else{
			$no_of_guests=0;
		}
		if(isset($sku_info_arr["user_type_users"])){
			$user_type_users=$sku_info_arr["user_type_users"];
			$no_of_unique_users=count(array_count_values($user_type_users));
			$no_of_repeat_users=0;
			foreach(array_count_values($user_type_users) as $k => $v){
				if($v>1){
					$no_of_repeat_users++;
				}
			}
		}
		else{
			$no_of_unique_users=0;
			$no_of_repeat_users=0;
		}
		$users=array();
		$users_graph=array();
		$users["name"]="Guests";
		$users["y"]=$no_of_guests;
		$users_graph[]=$users;
		$users["name"]="Unique Users";
		$users["y"]=$no_of_unique_users;
		$users_graph[]=$users;
		$users["name"]="Repeat Users";
		$users["y"]=$no_of_repeat_users;
		$users_graph[]=$users;
		////////////////////////
		$total_sections=array();
		$total_sections_graphs=array();
		$total_sections_arr=$sku_info_arr["total_sections"];
		if(!empty($total_sections_arr)){
			foreach($total_sections_arr as $section => $timespent){
				$total_sections["name"]=$section;
				$total_sections["y"]=$timespent;
				$total_sections_graphs[]=$total_sections;
			}
		}
		
		$guest_sections=array();
		$guest_sections_graphs=array();
		$guest_sections_arr=$sku_info_arr["guest_sections"];
		if(!empty($guest_sections_arr)){
			foreach($guest_sections_arr as $section => $timespent){
				$guest_sections["name"]=$section;
				$guest_sections["y"]=$timespent;
				$guest_sections_graphs[]=$guest_sections;
			}
		}
		
		$users_sections=array();
		$users_sections_graphs=array();
		$users_sections_arr=$sku_info_arr["users_sections"];
		if(!empty($users_sections_arr)){
			foreach($users_sections_arr as $section => $timespent){
				$users_sections["name"]=$section;
				$users_sections["y"]=$timespent;
				$users_sections_graphs[]=$users_sections;
			}
		}
		echo json_encode(array("users_graph"=>$users_graph,"total_sections_graphs"=>$total_sections_graphs,"guest_sections_graphs"=>$guest_sections_graphs,"users_sections_graphs"=>$users_sections_graphs));
	}
	
	public function get_graph_by_sku_button(){
		$type_menu=$this->input->post("type_menu");
		$graph_format_json=$this->input->post("graph_format_json");
		$skuId=$this->input->post("skuId");
		$graph_format_arr=json_decode($graph_format_json,true);
		$sku_info_arr=$graph_format_arr[$type_menu][$skuId];
		
		if(isset($sku_info_arr["user_type"]["guest"])){
			$no_of_guests=$sku_info_arr["user_type"]["guest"];
		}
		else{
			$no_of_guests=0;
		}
		if(isset($sku_info_arr["user_type_users"])){
			$user_type_users=$sku_info_arr["user_type_users"];
			$no_of_unique_users=count(array_count_values($user_type_users));
			$no_of_repeat_users=0;
			foreach(array_count_values($user_type_users) as $k => $v){
				if($v>1){
					$no_of_repeat_users++;
				}
			}
		}
		else{
			$no_of_unique_users=0;
			$no_of_repeat_users=0;
		}
		$users=array();
		$users_graph=array();
		$users["name"]="Guests";
		$users["y"]=$no_of_guests;
		$users_graph[]=$users;
		$users["name"]="Unique Users";
		$users["y"]=$no_of_unique_users;
		$users_graph[]=$users;
		$users["name"]="Repeat Users";
		$users["y"]=$no_of_repeat_users;
		$users_graph[]=$users;
		////////////////////////
		$total_sections=array();
		$total_sections_graphs=array();
		$total_sections_arr=$sku_info_arr["total_sections"];
		if(!empty($total_sections_arr)){
			foreach($total_sections_arr as $section => $timespent){
				$total_sections["name"]=$section;
				$total_sections["y"]=$timespent;
				$total_sections_graphs[]=$total_sections;
			}
		}
		
		$guest_sections=array();
		$guest_sections_graphs=array();
		$guest_sections_arr=$sku_info_arr["guest_sections"];
		if(!empty($guest_sections_arr)){
			foreach($guest_sections_arr as $section => $timespent){
				$guest_sections["name"]=$section;
				$guest_sections["y"]=$timespent;
				$guest_sections_graphs[]=$guest_sections;
			}
		}
		
		$users_sections=array();
		$users_sections_graphs=array();
		$users_sections_arr=$sku_info_arr["users_sections"];
		if(!empty($users_sections_arr)){
			foreach($users_sections_arr as $section => $timespent){
				$users_sections["name"]=$section;
				$users_sections["y"]=$timespent;
				$users_sections_graphs[]=$users_sections;
			}
		}
		echo json_encode(array("users_graph"=>$users_graph,"total_sections_graphs"=>$total_sections_graphs,"guest_sections_graphs"=>$guest_sections_graphs,"users_sections_graphs"=>$users_sections_graphs));
	}
	
	public function get_graph_by_sku_reviews(){
		$type_menu=$this->input->post("type_menu");
		$graph_format_json=$this->input->post("graph_format_json");
		$skuId=$this->input->post("skuId");
		$graph_format_arr=json_decode($graph_format_json,true);
		$sku_info_arr=$graph_format_arr[$type_menu][$skuId];
		
		if(isset($sku_info_arr["user_type"]["guest"])){
			$no_of_guests=$sku_info_arr["user_type"]["guest"];
		}
		else{
			$no_of_guests=0;
		}
		if(isset($sku_info_arr["user_type_users"])){
			$user_type_users=$sku_info_arr["user_type_users"];
			$no_of_unique_users=count(array_count_values($user_type_users));
			$no_of_repeat_users=0;
			foreach(array_count_values($user_type_users) as $k => $v){
				if($v>1){
					$no_of_repeat_users++;
				}
			}
		}
		else{
			$no_of_unique_users=0;
			$no_of_repeat_users=0;
		}
		$users=array();
		$users_graph=array();
		$users["name"]="Guests";
		$users["y"]=$no_of_guests;
		$users_graph[]=$users;
		$users["name"]="Unique Users";
		$users["y"]=$no_of_unique_users;
		$users_graph[]=$users;
		$users["name"]="Repeat Users";
		$users["y"]=$no_of_repeat_users;
		$users_graph[]=$users;
		////////////////////////
		$total_sections=array();
		$total_sections_graphs=array();
		$total_sections_arr=$sku_info_arr["total_sections"];
		if(!empty($total_sections_arr)){
			foreach($total_sections_arr as $section => $timespent){
				$total_sections["name"]=$section;
				$total_sections["y"]=$timespent;
				$total_sections_graphs[]=$total_sections;
			}
		}
		
		$guest_sections=array();
		$guest_sections_graphs=array();
		$guest_sections_arr=$sku_info_arr["guest_sections"];
		if(!empty($guest_sections_arr)){
			foreach($guest_sections_arr as $section => $timespent){
				$guest_sections["name"]=$section;
				$guest_sections["y"]=$timespent;
				$guest_sections_graphs[]=$guest_sections;
			}
		}
		
		$users_sections=array();
		$users_sections_graphs=array();
		$users_sections_arr=$sku_info_arr["users_sections"];
		if(!empty($users_sections_arr)){
			foreach($users_sections_arr as $section => $timespent){
				$users_sections["name"]=$section;
				$users_sections["y"]=$timespent;
				$users_sections_graphs[]=$users_sections;
			}
		}
		echo json_encode(array("users_graph"=>$users_graph,"total_sections_graphs"=>$total_sections_graphs,"guest_sections_graphs"=>$guest_sections_graphs,"users_sections_graphs"=>$users_sections_graphs));
	}
//////////////////////////////////////////////////////


 /* Added newly */
    public function vendor_customers() {

        if($this -> session -> userdata("logged_in")){
            $data['menu_flag']='vendor_analytics_customers';
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/analytics/vendor/vendor_customers.php');
        }else{
            redirect(base_url('Administrator'));
        }
    }
    public function vendor_abandoned_cart() {

        if($this -> session -> userdata("logged_in")){
            $data['menu_flag']='vendor_abandoned_cart_analytics';
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/analytics/vendor/vendor_customers.php');
        }else{
            redirect(base_url('Administrator'));
        }
    }
    public function vendor_visitors_traffic() {

        if($this -> session -> userdata("logged_in")){
            $data['menu_flag']='vendor_visitors_traffic';
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/analytics/vendor/vendor_customers.php');
        }else{
            redirect(base_url('Administrator'));
        }
    }
    public function vendor_inventory_view() {

        if($this -> session -> userdata("logged_in")){
            $data['menu_flag']='vendor_inventory_view';
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/analytics/vendor/vendor_customers.php');
        }else{
            redirect(base_url('Administrator'));
        }
    }
    public function getCustomerReportVendor() {

        $report_type = $this -> input -> post('report_type');
        $from_date = $this -> input -> post('from_date');
        $to_date = $this -> input -> post('to_date');
        $limit_result_val = $this -> input -> post('limit_result_val');
        $particular_year = $this -> input -> post('particular_year');
        
        $customer = "";
        
        /* newly added */

        if($report_type == "vendor_customer_gender"){
            $customer_arr = $this -> Analytics_model -> vendor_getCustomerGenderAnalytics($from_date, $to_date, $limit_result_val);
            $customer = array();
            foreach ($customer_arr as $data) {
                $male_arr = array();
                if ($data['Male']) {
                    $male_arr['name'] = "Male";
                    $male_arr['y'] = intval($data['Male']);
                }
                $female_arr = array();
                if ($data['Female']) {
                    $female_arr['name'] = "Female";
                    $female_arr['y'] = intval($data['Female']);
                }
                $Others_arr = array();
                if ($data['Others']!='') {
                    $female_arr['name'] = "Others";
                    $female_arr['y'] = intval($data['Others']);
                }
                array_push($customer, $male_arr);
                array_push($customer, $female_arr);
                array_push($customer, $Others_arr);
            }
        }
        
        if ($report_type == "vendor_customer_age") {
            $customer = $this -> Analytics_model -> vendor_getCustomerAgeAnalytics($from_date, $to_date, $limit_result_val);
        }
        if ($report_type == "vendor_customer_reg") {
            $customer = $this -> Analytics_model -> getCustomerRegistrationsAnalytics($from_date, $to_date, $limit_result_val, $particular_year);
        }
        
        if ($report_type == "vendor_customer_revenue" || $report_type == "vendor_customer_revenue_gender") {
            
            $customer_arr_total = $this -> Analytics_model -> vendor_revenue('', '','');

            $customer_arr = $this -> Analytics_model -> vendor_revenue($from_date, $to_date);
            /* all newly registered within the date  */
            $customer_arr_male = $this -> Analytics_model -> vendor_revenue($from_date, $to_date,'male');
            $customer_arr_female = $this -> Analytics_model -> vendor_revenue($from_date, $to_date,'female');
            $customer_arr_others = $this -> Analytics_model -> vendor_revenue($from_date, $to_date,'others');

            /* all existing registered within the date  */

            /*print_r($customer_arr_total);
            print_r($customer_arr);
            print_r($customer_arr_male);
            print_r($customer_arr_female);*/

            if(!empty($customer_arr_total)){
                $total_revenue=$customer_arr_total['total_revenue'];

                $total_revenue_new_cus=isset($customer_arr['total_revenue']) ? $customer_arr['total_revenue'] : 0;
                $total_revenue_new_cus_male=isset($customer_arr_male['total_revenue']) ? $customer_arr_male['total_revenue']: 0;
                $total_revenue_new_cus_female=isset($customer_arr_female['total_revenue']) ? $customer_arr_female['total_revenue'] : 0;
                $total_revenue_new_cus_others=isset($customer_arr_others['total_revenue']) ? $customer_arr_others['total_revenue'] : 0;

                $total_revenue_new_cus_percentage=round(($total_revenue_new_cus/$total_revenue)*100,2);
                $total_revenue_new_cus_male_percentage=round(($total_revenue_new_cus_male/$total_revenue)*100,2);
                $total_revenue_new_cus_female_percentage=round(($total_revenue_new_cus_female/$total_revenue)*100,2);
                $total_revenue_new_cus_others_percentage=round(($total_revenue_new_cus_others/$total_revenue)*100,2);

                $total_revenue_exist_cus_percentage=(100-$total_revenue_new_cus_percentage);

                $cus=array(
                    'total_revenue'=>$total_revenue,
                    'total_revenue_new_cus'=>$total_revenue_new_cus,
                    'total_revenue_new_cus_male'=>$total_revenue_new_cus_male,
                    'total_revenue_new_cus_female'=>$total_revenue_new_cus_female,
                    'total_revenue_new_cus_percentage'=>$total_revenue_new_cus_percentage,
                    'total_revenue_new_cus_male_percentage'=>$total_revenue_new_cus_male_percentage,
                    'total_revenue_new_cus_female_percentage'=>$total_revenue_new_cus_female_percentage,
                    'total_revenue_new_cus_others_percentage'=>$total_revenue_new_cus_others_percentage,
                    'total_revenue_exist_cus_percentage'=>$total_revenue_exist_cus_percentage,
                );
                //print_r($cus);

                $totl_cus=get_total_customers();//12

                $customer_arr_not_buy_all = $this -> Analytics_model -> vendor_revenue('', '','','not_buy');
                //print_r($customer_arr_not_buy_all);

                $customer_arr_not_buy_new = $this -> Analytics_model -> vendor_revenue($from_date, $to_date,'','not_buy_new');
                
                
                $customer=array();

                if($report_type == "vendor_customer_revenue"){
                   
                    //$customer=array($cus['total_revenue_exist_cus_percentage'],$cus['total_revenue_new_cus_percentage']);
                    //echo '||';

                    $new_did_not_buy=(isset($customer_arr_not_buy_new['total_custmers'])) ? $customer_arr_not_buy_new['total_custmers'] : 0;
                    $customer_arr_not_buy_all_val=(isset($customer_arr_not_buy_all['total_custmers'])) ? $customer_arr_not_buy_all['total_custmers'] : 0;
                    
                    $exist_did_not_buy=($customer_arr_not_buy_all_val-$new_did_not_buy);//9

                    $new_did_not_buy_percentage=round((($new_did_not_buy/$totl_cus)*100),2);
                    $exist_did_not_buy_percentage=round((($exist_did_not_buy/$totl_cus)*100),2);


                    $new_revenue=(isset($cus['total_revenue_new_cus_percentage'])) ? $cus['total_revenue_new_cus_percentage'] : 0;
                    $exist_revenue=(isset($cus['total_revenue_exist_cus_percentage'])) ? $cus['total_revenue_exist_cus_percentage'] : 0;

                    //$exist_did_not_buy_percentage
                    
                    //$arr1=array('name'=>'Did not Buy','data'=>array($new_did_not_buy_percentage,$exist_did_not_buy_percentage));

                    //$arr2=array('name'=>'Revenue','data'=>array($new_revenue,$exist_revenue));

                    $arr1=array('name'=>'New Customers','data'=>array($new_did_not_buy_percentage,$new_revenue));

                    $arr2=array('name'=>'Exist Customers','data'=>array($exist_did_not_buy_percentage,$exist_revenue));



                    array_push($customer, $arr1);
                    array_push($customer, $arr2);
                    //print_r($customer);

                }
                if($report_type == "vendor_customer_revenue_gender"){
                    $male_arr = array();
                    if (1) {
                        $male_arr['name'] = "Male";
                        $male_arr['y'] = intval($cus['total_revenue_new_cus_male_percentage']);
                    }
                    $female_arr = array();
                    if (1) {
                        $female_arr['name'] = "Female";
                        $female_arr['y'] = intval($cus['total_revenue_new_cus_female_percentage']);
                    }
                    $others_arr=array();
                    if (1) {
                        $others_arr['name'] = "NA";
                        $others_arr['y'] = intval($cus['total_revenue_new_cus_others_percentage']);
                    }
                    array_push($customer, $male_arr);
                    array_push($customer, $female_arr);
                    array_push($customer, $others_arr);
                }

                //$customer=$cus;
            }
			else{
				 if($report_type == "vendor_customer_revenue"){
					$customer=array();
					$arr1=array('name'=>'New Customers','data'=>array(0,0));
					$arr2=array('name'=>'Exist Customers','data'=>array(0,0));
					array_push($customer, $arr1);
					array_push($customer, $arr2);
				 }
				 if($report_type == "vendor_customer_revenue_gender"){
					$customer=array();
                    $male_arr = array();
                    if (1) {
                        $male_arr['name'] = "Male";
                        $male_arr['y'] = 0;
                    }
                    $female_arr = array();
                    if (1) {
                        $female_arr['name'] = "Female";
                        $female_arr['y'] = 0;
                    }
                    $others_arr=array();
                    if (1) {
                        $others_arr['name'] = "NA";
                        $others_arr['y'] = 0;
                    }
                    array_push($customer, $male_arr);
                    array_push($customer, $female_arr);
                    array_push($customer, $others_arr);
                }
			}

        }

        if($report_type == "vendor_customer_subscribed"){
            $customer_arr = $this -> Analytics_model -> vendor_getCustomerSubscribedAnalytics($from_date, $to_date);
            $customer = array();
                if(!empty($customer_arr)){
                
                    $registered=(intval($customer_arr['total_subscriber'])-intval($customer_arr['anonymus_customer']));

                    $customer=array(intval($customer_arr['total_subscriber']),$registered,intval($customer_arr['anonymus_customer']));

                }
            
        }
        if($report_type == "vendor_customer_sustainability"){
                

                $customer_arr = $this -> Analytics_model -> vendor_getCustomerSustainabilityAnalytics($from_date, $to_date,0,1);

                $all_dates=array();

                $customer = array();
                $onetime_purchase=(isset($customer_arr['purchase_count'])) ? intval($customer_arr['purchase_count']) : 0;

                $from_date=$to_date;
                $date = strtotime($to_date);
                $date = strtotime("-7 day", $date);
                $to_date_7=date('Y-m-d',$date);
                $to_date=$to_date_7;

                $all_dates[]=[$from_date, $to_date];

                $customer_arr = $this -> Analytics_model -> vendor_getCustomerSustainabilityAnalytics($to_date, $from_date,1,7);
                $less_7=(isset($customer_arr['purchase_count'])) ? intval($customer_arr['purchase_count']) : 0;

                $date = strtotime($to_date);
                $date = strtotime("-30 day", $date);
                $to_date_30=date('Y-m-d',$date);
                $from_date=date('Y-m-d', strtotime("$to_date -1 day"));
                $to_date=$to_date_30;
                $all_dates[]=[$from_date, $to_date];

                $customer_arr = $this -> Analytics_model -> vendor_getCustomerSustainabilityAnalytics($to_date, $from_date,7,30);
                $less_30=(isset($customer_arr['purchase_count'])) ? intval($customer_arr['purchase_count']) : 0;
                
                $date = strtotime($to_date);
                $date = strtotime("-60 day", $date);
                $to_date_60=date('Y-m-d',$date);
                $from_date=date('Y-m-d', strtotime("$to_date -1 day"));
                $to_date=$to_date_60;
                $all_dates[]=[$from_date, $to_date];

                $customer_arr = $this -> Analytics_model -> vendor_getCustomerSustainabilityAnalytics($to_date, $from_date,30,60);
                $less_60=(isset($customer_arr['purchase_count'])) ? intval($customer_arr['purchase_count']): 0;

                $date = strtotime($to_date);
                $date = strtotime("-90 day", $date);
                $to_date_90=date('Y-m-d',$date);
                $from_date=date('Y-m-d', strtotime("$to_date -1 day"));
                $to_date=$to_date_90;
                $all_dates[]=[$from_date, $to_date];

                $customer_arr = $this -> Analytics_model -> vendor_getCustomerSustainabilityAnalytics($to_date, $from_date,60,90);
                $less_90=(isset($customer_arr['purchase_count'])) ? intval($customer_arr['purchase_count']) : 0;

                $date = strtotime($to_date);
                $date = strtotime("-1 day", $date);
                $to_date_90_greater=date('Y-m-d',$date);
                $from_date=date('Y-m-d', strtotime("$to_date -1 day"));
                $to_date=$to_date_90_greater;
                $all_dates[]=[$from_date, $to_date];

                $customer_arr = $this -> Analytics_model -> vendor_getCustomerSustainabilityAnalytics($to_date, $from_date,90,1000);
                $more_90=(isset($customer_arr['purchase_count'])) ? intval($customer_arr['purchase_count']) : 0;

                $customer=array($onetime_purchase,$less_7,$less_30,$less_60,$less_90,$more_90);

                //print_r($all_dates);
             
        }

        if($report_type == "vendor_customer_life_cycle"){
			
			 $arr1=[];
                $arr2=[];
                $arr3=[];
                $arr4=[];
				
				
            //$from_date = '2000-01-25';
            //$to_date = '2010-02-20';
            
            $ts1 = strtotime($from_date);
            $ts2 = strtotime($to_date);
            
            $year1 = date('Y', $ts1);
            $year2 = date('Y', $ts2);
            
            $month1 = date('m', $ts1);
            $month2 = date('m', $ts2);
            
            $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

            //echo $month=$diff;

            $no_of_visit=$this->Analytics_model ->customer_number_of_visit($from_date,$to_date);

            //print_r($no_of_visit);
            $arr_val=[];
            if(!empty($no_of_visit)){
                foreach($no_of_visit as $arr){

                    $no_of_visit_site=$arr['visit_count'];

                    $cus_id=$arr['customer_id'];

                    $cus_values=$this->Analytics_model->customer_values($cus_id);
                    if(!empty($cus_values)){
                        foreach($cus_values as $arr){
                            $un_purchased=$arr['cart_price'];
                            $purchased=$arr['purchased_value'];

                            $per_vist_value=($purchased/$no_of_visit_site);
                            $per_visit_cart=($un_purchased/$no_of_visit_site);
                            $visit_per_month=$no_of_visit_site;
                            $yrs=($limit_result_val!='') ? ($limit_result_val*12) : 12; // else 1 yr

                            $v1=($per_vist_value*$visit_per_month*$yrs);
                            $v2=($per_visit_cart*$visit_per_month*$yrs*0.25);

                            $v=($v1+$v2);

                            $arr_val[]=$v;
                        }
                    }
                }
            }

            //print_r($arr_val);

            if(!empty($arr_val)){
                $arr1=[];
                $arr2=[];
                $arr3=[];
                $arr4=[];

                foreach ($arr_val as $element) {
                    //echo $element.'||';
                    if($element<=50000) {
                        $arr1[]= $element;
                    }

                    if($element>50000 && $element<=100000){
                        $arr2[]= $element;
                    }
                    if($element>100000 && $element<=1500000){
                        $arr3[]= $element;
                    }
                    if($element>1500000){
                        $arr4[]= $element;
                    }
                }

            }

           $customer=array(count($arr1),count($arr2),count($arr3),count($arr4));

        }
        if($report_type =='vendor_abandoned_cart_weekly'){
            $start = new DateTime($from_date);
            $end = new DateTime($to_date);
            $interval = new DateInterval('P1D');
            $dateRange = new DatePeriod($start, $interval, $end);

            $weekNumber = 1;
            $weeks = array();
            foreach ($dateRange as $date) {
                $weeks[$weekNumber][] = $date->format('Y-m-d');
                if ($date->format('w') == 6) {
                    $weekNumber++;
                }
            }

            $ranges = array_map(function($week) {
                return array(array_shift($week) 
                     , array_pop($week)); 
                    },
            $weeks);
            
            //print_r($ranges);

            $x_axis_arr=[];
            foreach($ranges as $ranges_arr){
                $x_axis_arr[]=date("D j M Y",strtotime($ranges_arr[0]))." - ".date("D j M Y",strtotime($ranges_arr[1]));
            }


            $res=[];
            foreach($ranges as $arr){
                $abandoned_cart_value=$this->Analytics_model ->abandoned_cart_value( $arr[0], $arr[1]);
                if(!empty($abandoned_cart_value)){
                    $cart_value=isset($abandoned_cart_value['cart_price']) ? intval($abandoned_cart_value['cart_price']) : 0;
                    $res[]=$cart_value;
                }
            }
            $customer=array('x_axis_arr'=>$x_axis_arr,'res'=>$res);

        }
        if($report_type=='vendor_abandoned_cart_clearance'){
            $abandoned_cart_value=$this->Analytics_model ->abandoned_cart_value($from_date, $to_date);
            $customer=array();
            if(!empty($abandoned_cart_value)){
                $cart_value=isset($abandoned_cart_value['cart_price']) ? intval($abandoned_cart_value['cart_price']) : 0;
                
                $customer=array($cart_value,80,30);
            }
        }
        if($report_type=='visitor_traffic'){
            $res=array(); $customer=array(); $total_new_visitors=0; $total_repeated_visitors=0;
            $res_arr=$this->Analytics_model->get_visitors_traffic_data($from_date,$to_date);
            if(!empty($res_arr)){
                foreach($res_arr as $num){
                    $res[]=$num['number_of_visit'];
                    if($num['number_of_visit']==1){
                        $total_new_visitors++;
                    }
                    if($num['number_of_visit']>1){
                        $total_repeated_visitors++;
                    }
                }
                $total_vistors=($total_new_visitors+$total_repeated_visitors);

                $customer=[
                    array("name"=>"Total New Visitors","y"=>$total_new_visitors),
                    array("name"=>"Total Repeat Visitors","y"=>$total_repeated_visitors)
                ];
                
            }
        }

        if($report_type=="total_visitor_pagewise"){
            $dates_arr=displayDates($from_date,$to_date);
            $page=$this -> input -> post('page');

            if($page==""){
                $page=base_url();
            }else{
                $page=base_url().$page;
            }

            //echo $page;

            //print_r($dates_arr);
            $user=array();$guest=array();
            $category_arr=array();
            
            if(!empty($dates_arr)){
                foreach($dates_arr as $mY=>$dates){
                    if(is_dir("analytics/".$mY)){
                        foreach($dates as $d){
                            $name="analytics/".$mY."/".$d.".txt";
                            
                            if(file_exists($name)){
                                $file = fopen($name,"r");
                                $json_data_arr = json_decode(file_get_contents($name), true);
                                if(!empty($json_data_arr)){
                                    
                                    //array_sum(array_column( $json_data_arr, 'view_count')); 
                                    $sum_guest = 0; $sum_user=0;
                                    foreach ($json_data_arr as $arr) {
                                        if(isset($arr['user_type']) && isset($arr['page_url'])){
                                            
                                            $page_url=json_encode($arr['page_url'], JSON_UNESCAPED_SLASHES);
                                            $page_url=trim($page_url, '"');
                                            
                                            if($page_url==$page){
                                                if($arr['user_type']=='guest'){
                                                    $sum_guest += $arr['view_count'];
                                                }else{
                                                    $sum_user += $arr['view_count'];
                                                }
                                            }
                                        }
                                    
                                    }

                                    $guest[]=$sum_guest;
                                    $user[]=$sum_user;
                                    $category_arr[]=date('d M',strtotime($d));
                                }
                            }
                        }

                    } 
                }
            }

            $customer=array('guest'=>$guest,'user'=>$user,'category'=>$category_arr,'page'=>$page);

        }

        if($report_type=="total_visitor_pagewise_top" || $report_type=="total_visitor_pagewise_bottom" ){
            $dates_arr=displayDates($from_date,$to_date);
            
            //print_r($dates_arr);
            $user=array();$guest=array();
            $category_arr=array();
            $pages_count_user=array();
            $pages_count_guest=array();
            if(!empty($dates_arr)){
                foreach($dates_arr as $mY=>$dates){
                    if(is_dir("analytics/".$mY)){
                        foreach($dates as $d){
                            $name="analytics/".$mY."/".$d.".txt";
                            
                            if(file_exists($name)){
                                $file = fopen($name,"r");
                                $json_data_arr = json_decode(file_get_contents($name), true);
                                if(!empty($json_data_arr)){
                                    
                                    //array_sum(array_column( $json_data_arr, 'view_count')); 
                                    $sum_guest = 0; $sum_user=0;
                                    foreach ($json_data_arr as $arr) {
                                        if(isset($arr['user_type']) && isset($arr['page_url'])){
                                            
                                            $page_url=json_encode($arr['page_url'], JSON_UNESCAPED_SLASHES);
                                            $page_url=trim($page_url, '"');
                                            
                                            $view_count=$arr['view_count'];

                                            if(1){
                                                if($arr['user_type']=='guest'){
                                                    $sum_guest += $arr['view_count'];
                                                    $pages_count_guest[$page_url]=(isset($pages_count_guest[$page_url])) ? ($pages_count_guest[$page_url]+$view_count):$view_count;
                                                }else{
                                                    $sum_user += $arr['view_count'];
                                                    $pages_count_user[$page_url]=(isset($pages_count_user[$page_url])) ? ($pages_count_user[$page_url]+$view_count):$view_count;
                                                }
                                            }

                                            
                                        }
                                    
                                    }

                                    $guest[]=$sum_guest;
                                    $user[]=$sum_user;
                                    $category_arr[]=date('d M',strtotime($d));
                                }
                            }
                        }

                    } 
                }
            }
            $user_arr=array();$guest_arr=array();

            if(!empty($pages_count_user)){
                if($report_type=="total_visitor_pagewise_top"){
                    arsort($pages_count_user) ;
                }else{
                    asort($pages_count_user) ;
                }

                $output = array_slice($pages_count_user, 0, $limit_result_val); 
                //print_r($output);
                $cus=[];
                $cat=array_keys($output);
                $val=array_values($output);

                
                for($i=0;$i< $limit_result_val;$i++){
                    if(isset($cat[$i])){
                        $user_arr[]=array('name'=>$cat[$i],'y'=>$val[$i]);
                    }else{
                        break;
                    }
                }
                //$user_arr=$val;
            }
            if(!empty($pages_count_guest)){
                //arsort($pages_count_guest) ;

                if($report_type=="total_visitor_pagewise_top"){
                    arsort($pages_count_guest) ;//desending
                }else{
                    asort($pages_count_guest) ;//ascending
                }

                $output = array_slice($pages_count_guest, 0, 10); 
                //print_r($output);
                $cus=[];
                $cat=array_keys($output);
                $val=array_values($output);

                for($i=0;$i< $limit_result_val;$i++){
                    if(isset($cat[$i])){
                        $guest_arr[]=array('name'=>$cat[$i],'y'=>$val[$i]);
                    }
                }
                //$guest_arr=$val;

            }

            //$customer=array('guest'=>$guest,'user'=>$user,'category'=>$category_arr,'page'=>$page);
            $customer=array('guest'=>$guest_arr,'user'=>$user_arr);

        }

        if($report_type=="total_visitor"){
            
            $dates_arr=displayDates($from_date,$to_date);
            //print_r($dates_arr);
            $user=array();$guest=array();
            $category_arr=array();
            
            if(!empty($dates_arr)){
                foreach($dates_arr as $mY=>$dates){
                    if(is_dir("analytics/".$mY)){
                        foreach($dates as $d){
                            $name="analytics/".$mY."/".$d.".txt";
                            
                            if(file_exists($name)){
                                $file = fopen($name,"r");
                                $json_data_arr = json_decode(file_get_contents($name), true);
                                if(!empty($json_data_arr)){
                                    
                                    //array_sum(array_column( $json_data_arr, 'view_count')); 
                                    $sum_guest = 0; $sum_user=0;
                                    foreach ($json_data_arr as $arr) {
                                        if(isset($arr['user_type'])){
                                            if($arr['user_type']=='guest'){
                                                $sum_guest += $arr['view_count'];
                                            }else{
                                                $sum_user += $arr['view_count'];
                                            }
                                        }
                                    
                                    }

                                    $guest[]=$sum_guest;
                                    $user[]=$sum_user;
                                    $category_arr[]=date('d M',strtotime($d));
                                }
                            }
                        }

                    } 
                }
            }

            $customer=array('guest'=>$guest,'user'=>$user,'category'=>$category_arr);
        }

        if($report_type=='vistors_request_pages'){
            $req_page_arr=array();$whole=[];
            $res_arr=$this->Analytics_model->get_visitor_request_page($from_date,$to_date);
            if(!empty($res_arr)){
                foreach($res_arr as $res){
                    $req_page='['.$res['request_page'].']';
                    $req_page_arr[]=json_decode($req_page,true);
                }
            
                foreach ($req_page_arr as $key => $contact) {
                    foreach($contact as $res){
                        if(!empty($res)){
                            $whole[$res['request_uri']]=(isset($whole[$res['request_uri']])) ? ($whole[$res['request_uri']]+1): 1;
                        }
                    }
                }
            }
            if(!empty($whole)){
                arsort($whole) ;

                $output = array_slice($whole, 0, $limit_result_val); 
                //print_r($output);
                $cus=[];
                $cat=array_keys($output);
                $val=array_values($output);

                for($i=0;$i<$limit_result_val;$i++){
                    if(isset($cat[$i])){
                        $customer[]=array('name'=>$cat[$i],'y'=>$val[$i]);
                    }else{
                        break;
                    }
                }
            }

        }

        if($report_type=='visitor_top_viewed' || $report_type=='vistors_bottom_viewed'){
            $req_page_arr=array();$whole=[];$customer_prev=array();
            $sku_arr=array();
            /* from txt file */
            if(1){
            
                $dates_arr=displayDates($from_date,$to_date);
                //print_r($dates_arr);
                $user=array();$guest=array();
                $category_arr=array();
                
                if(!empty($dates_arr)){
                    foreach($dates_arr as $mY=>$dates){
                        if(is_dir("analytics/".$mY)){
                            foreach($dates as $d){
                                $name="analytics/".$mY."/".$d.".txt";
                                
                                if(file_exists($name)){
                                    $file = fopen($name,"r");
                                    $json_data_arr = json_decode(file_get_contents($name), true);
                                    if(!empty($json_data_arr)){
                                        
                                        //array_sum(array_column( $json_data_arr, 'view_count')); 
                                        $sum_guest = 0; $sum_user=0;
                                        foreach ($json_data_arr as $arr) {
                                            if(isset($arr['page_url'])){
                                                $url= $arr['page_url'];
                                                if((preg_match("/item/",$arr['page_url']))){
                                                    $values = parse_url($url);

                                                    //print_r($values);
                                                    $host = explode('/',$values["path"]);

                                                    if(is_array($host)){
                                                        $sku=strtoupper(end($host));
                                                    }else{
                                                        $sku='';
                                                    }
                                                    if($sku!=''){
                                                        //echo "sku ".$sku.intval($arr['view_count']);
                                                        if(array_key_exists($sku,$sku_arr)){
                                                            $sku_arr[$sku]=($sku_arr[$sku]+intval($arr['view_count']));
                                                        }else{
                                                            $sku_arr[$sku]=intval($arr['view_count']);
                                                        }
                                                    }
                                                }
                                            }
                                        
                                        }
    
                                       
                                    }
                                }
                            }
    
                        } 
                    }
                }
    
                //print_r($sku_arr);
                //exit;

                //$customer=array('guest'=>$guest,'user'=>$user,'category'=>$category_arr);
            }
            /* from txt file */


            $table_str='';
            
            //print_r($sku_arr);

            //$res_arr=$this->Analytics_model->get_top_viewed_pages($from_date,$to_date,$limit_result_val);
            $view_sum=array();
            if(!empty($sku_arr)){
                
                $table_str.='<table class="table table-bordered table-responsive">';
                $table_str.='<thead><th>Tree</th><th>Image</th><th>View Count</th></thead>';


                $temp_arr='';
                foreach($sku_arr as $key=>$res){
                    $customer_prev[]=array('name'=>$key,'y'=>intval($res));
                    $view_sum[]=intval($res);
                }
                $temp_arr=$customer_prev;
                
                sort($temp_arr);

                rsort($customer_prev);

                $temp_asc = $temp_arr;
                $temp_desc = $customer_prev;
 
                
                $sum=array_sum($view_sum);

                $sku_arr=array_slice($sku_arr, 0, $limit_result_val);

                foreach($sku_arr as $sku_id=>$count){
                    
                    $per=($sum>0) ? round(($count/$sum*100),1): 0;

                    $res=get_all_inventory_data_by_sku_id($sku_id);
                    if(!empty($res)){
                        $table_str.='<tr><td>'.$res["cat_name"].' > '.$res["subcat_name"].' > '.$res["brand_name"].' > '.$res["product_name"].' > '.$res['sku_id'].'</td>'.'<td><img src="'.base_url().$res["image"].'" width="100" alt="Image Not Found"></td>'.'<td>'.$count.' ('. $per.'%)</td></tr>';
                    }
            
                }

                $table_str.='</table>';
            }

            if($report_type=='visitor_top_viewed'){
                //echo 'inside';
                $temp_asc=array_slice($temp_asc, 0, $limit_result_val);
                $customer=array('res'=>$temp_asc,'tabl'=> $table_str);
            }

            if($report_type=='vistors_bottom_viewed'){
                $customer_prev=array_slice($customer_prev, 0, $limit_result_val);
                $customer=array('res'=>$customer_prev,'tabl'=> $table_str);
            }
            

        }

        if(0){

            $req_page_arr=array();$whole=[];$customer_prev=array();
            $res_arr=$this->Analytics_model->get_bottom_viewed_pages($from_date,$to_date,$limit_result_val);
            $table_str='';
            if(!empty($res_arr)){
                $table_str.='<table class="table table-bordered table-responsive">';
                $table_str.='<thead><th>Tree</th><th>Image</th><th>View Count</th></thead>';

                foreach($res_arr as $res){

                    $customer_prev[]=array('name'=>$res['sku_id'],'y'=>intval($res['view_count']));
                    $view_sum[]=intval($res['view_count']);
                   
                }
                $sum=array_sum($view_sum);

                foreach($res_arr as $res){
                    $per=($sum>0) ? round(($res["view_count"]/$sum)*100,1): 0;
                    $table_str.='<tr><td>'.$res["cat_name"].' > '.$res["subcat_name"].' > '.$res["brand_name"].' > '.$res["product_name"].' > '.$res['sku_id'].'</td>'.'<td><img src="'.base_url().$res["image"].'" width="100" alt="Image Not Found"></td>'.'<td>'.$res["view_count"].' ('.$per.'%)</td></tr>';
                }
            }

            $customer=array('res'=>$customer_prev,'tabl'=> $table_str);
        }
		
		
		
		
		
		
		
		
		
		

        /* newly added */

        echo json_encode($customer);
    }

	public function conversation_chain_view() {

        if($this -> session -> userdata("logged_in")){
            $data['menu_flag']='conversation_chain_view';
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/analytics/vendor/vendor_customers.php');
        }else{
            redirect(base_url('Administrator'));
        }
    }
    /* Added newly */
    public function vendor_dashboard() {
        if($this -> session -> userdata("logged_in")){
            $data['menu_flag']='vendor_dashboard';

            /* Profit */

            $profits=$this->Analytics_model->profit_for_today();

            $data['profits_today']=$profits['total_revenue_today']; 
            $data['total_revenue_yesterday']=$profits['total_revenue_yesterday'];
            $profit_value=$profits['total_revenue'];
            $total_sales=$profits['total_sales'];


            $total_sales_today=$profits['total_sales_today'];
            $total_sales_yesterday=$profits['total_sales_yesterday'];

            $diff=($profits['total_revenue_today']-$profits['total_revenue_yesterday']);

            /* profit */
            //print_r($profits);

            $a=$profits['total_revenue_today'];
            $b=$profits['total_revenue_yesterday'];

            //$a=160;//today
            //$b=80; //yesterday

            $diff=($a-$b);

            //echo "difference from yesterday = ".$diff;
            //echo '<br>';

            $profit_status= '';
            $c=0;
            if($diff>0){

                $c=(($a-$b)/$a * 100); //increase percentage
                //echo "percentage increase : ".round($c,2). '%';
                $profit_status='high';
            }else{
                
                if($b!=0){
                    $c=(($a-$b)/$b * 100); //decrease percentage
                    //echo "percentage decease : ".round($c,2). '%';
                    $profit_status='low';
                }
            }

            $data['profit']=round($c,2);
            $data['profit_status']=$profit_status;
            $data['profit_value']=$profit_value;//total revenue overall

            /* Profit */
            
            /* sales */
            
            $a=$profits['total_sales_today'];
            $b=$profits['total_sales_yesterday'];

            $diff=($a-$b);

            //echo "difference from yesterday = ".$diff;
            //echo '<br>';

            $sales_status= '';
            $c=0;
            if($diff>0){
                $c=(($a-$b)/$a * 100); //increase percentage
                //echo "percentage increase : ".round($c,2). '%';
                $sales_status='high';
            }else{
                if($b!=0){
                    $c=(($a-$b)/$b * 100); //decrease percentage
                    //echo "percentage decease : ".round($c,2). '%';
                    $sales_status='low';
                }
            }

            $data['total_sales']=$total_sales;//total sales overall
            $data['total_sales_today']=$total_sales_today;//total sales today
            $data['total_sales_yesterday']=$total_sales_yesterday;//total sales yesterday
            $data['sales']=round($c,2);//total sales yesterday
            $data['sales_status']=$sales_status;//total sales yesterday

            /* sales */

            $total_order=$this->Analytics_model->get_order_details('total');
            $total_order_cat=$this->Analytics_model->get_order_details('cat');
            
            //print_r($total_order_cat);
           
            $data['total_order']=$total_order;
            $data['total_order_cat']=$total_order_cat;

            /* Growth */
            $current_yr_total=$this->Analytics_model->getRevenueOnYearBasis_revenue(date('Y'));
            $py=date("Y",strtotime("-1 year"));
            $previous_yr_total=$this->Analytics_model->getRevenueOnYearBasis_revenue($py);

            $data["current_yr_total"]=$current_yr_total;
            $data["previous_yr_total"]=$previous_yr_total;
            $a=$current_yr_total;
            $b=$previous_yr_total;

            $diff=($a-$b);

            $growth_status= '';
            $c=0;
            if($diff>0){
                $c=(($a-$b)/$a * 100); //increase percentage
                //echo "percentage increase : ".round($c,2). '%';
                $growth_status='high';
            }else{
                if($b!=0){
                    $c=(($a-$b)/$b * 100); //decrease percentage
                    //echo "percentage decease : ".round($c,2). '%';
                    $growth_status='low';
                }
            }

            $data['growth']=round($c);
            $data['growth_status']=$growth_status;


            /* Growth */

            $total_payments=$this->Analytics_model->get_overall_payments();
            $data['total_payments']=round($total_payments);

            $total_refund=$this->Analytics_model->refund_requests_for();
            $data['total_refund']=round($total_refund);

            /* Wallet Transactions */

            $wallet_total=$this->Analytics_model->get_wallet_amount_total();
            $wallet_data=$this->Analytics_model->get_wallet_amounts_customer();
            $data['wallet_total']=round($wallet_total);
            $data['wallet_data']=$wallet_data;

            /* Wallet Transactions */

            /* Orders */

            $orders_data=$this->Analytics_model->get_orders_data();
            $data["orders_data"]=$orders_data;

            /* Orders */

            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/analytics/vendor_dashboard.php');
        }else{
            redirect(base_url('Administrator'));
        }
    }
    
    public function dashboard() {
        if($this -> session -> userdata("logged_in")){
            $data['menu_flag']='dashboard';


            /* Profit */

            $profits=$this->Analytics_model->profit_for_today();

            $data['profits_today']=$profits['total_revenue_today']; 
            $data['total_revenue_yesterday']=$profits['total_revenue_yesterday'];
            $profit_value=$profits['total_revenue'];
            $total_sales=$profits['total_sales'];


            $total_sales_today=$profits['total_sales_today'];
            $total_sales_yesterday=$profits['total_sales_yesterday'];

            $diff=($profits['total_revenue_today']-$profits['total_revenue_yesterday']);

            /* profit */
            //print_r($profits);

            $a=$profits['total_revenue_today'];
            $b=$profits['total_revenue_yesterday'];

            //$a=160;//today
            //$b=80; //yesterday

            $diff=($a-$b);

            //echo "difference from yesterday = ".$diff;
            //echo '<br>';

            $profit_status= '';
            $c=0;
            if($diff>0){

                $c=(($a-$b)/$a * 100); //increase percentage
                //echo "percentage increase : ".round($c,2). '%';
                $profit_status='high';
            }else{
                
                if($b!=0){
                    $c=(($a-$b)/$b * 100); //decrease percentage
                    //echo "percentage decease : ".round($c,2). '%';
                    $profit_status='low';
                }
            }

            $data['profit']=round($c,2);
            $data['profit_status']=$profit_status;
            $data['profit_value']=$profit_value;//total revenue overall

            
            

            /* Profit */
            
            /* sales */

            
            $a=$profits['total_sales_today'];
            $b=$profits['total_sales_yesterday'];

            //$a=100;//today
            //$b=80; //yesterday

            $diff=($a-$b);

            //echo "difference from yesterday = ".$diff;
            //echo '<br>';

            $sales_status= '';
            $c=0;
            if($diff>0){
                $c=(($a-$b)/$a * 100); //increase percentage
                //echo "percentage increase : ".round($c,2). '%';
                $sales_status='high';
            }else{
                if($b!=0){
                    $c=(($a-$b)/$b * 100); //decrease percentage
                    //echo "percentage decease : ".round($c,2). '%';
                    $sales_status='low';
                }
            }

            $data['total_sales']=$total_sales;//total sales overall
            $data['total_sales_today']=$total_sales_today;//total sales today
            $data['total_sales_yesterday']=$total_sales_yesterday;//total sales yesterday
            $data['sales']=round($c,2);//total sales yesterday
            $data['sales_status']=$sales_status;//total sales yesterday

            /* sales */

            $total_order=$this->Analytics_model->get_order_details('total');
            $total_order_cat=$this->Analytics_model->get_order_details('cat');
            
            //print_r($total_order_cat);
           
            $data['total_order']=$total_order;
            $data['total_order_cat']=$total_order_cat;


            /* Growth */
            $current_yr_total=$this->Analytics_model->getRevenueOnYearBasis_revenue(date('Y'));
            $py=date("Y",strtotime("-1 year"));
            $previous_yr_total=$this->Analytics_model->getRevenueOnYearBasis_revenue($py);

            $data["current_yr_total"]=$current_yr_total;
            $data["previous_yr_total"]=$previous_yr_total;
            $a=$current_yr_total;
            $b=$previous_yr_total;

            $diff=($a-$b);

            $growth_status= '';
            $c=0;
            if($diff>0){
                $c=(($a-$b)/$a * 100); //increase percentage
                //echo "percentage increase : ".round($c,2). '%';
                $growth_status='high';
            }else{
                if($b!=0){
                    $c=(($a-$b)/$b * 100); //decrease percentage
                    //echo "percentage decease : ".round($c,2). '%';
                    $growth_status='low';
                }
            }

            $data['growth']=round($c);
            $data['growth_status']=$growth_status;


            /* Growth */

            $total_payments=$this->Analytics_model->get_overall_payments();
            $data['total_payments']=round($total_payments);

            $total_refund=$this->Analytics_model->refund_requests_for();
            $data['total_refund']=round($total_refund);

            /* Wallet Transactions */
            $wallet_total=$this->Analytics_model->get_wallet_amount_total();
            $wallet_data=$this->Analytics_model->get_wallet_amounts_customer();
            $data['wallet_total']=round($wallet_total);
            $data['wallet_data']=$wallet_data;

            /* Wallet Transactions */

            /* Orders */
            $orders_data=$this->Analytics_model->get_orders_data();
            $data["orders_data"]=$orders_data;

            /* Orders */


            

            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/analytics/dashboard.php');
        }else{
            redirect(base_url('Administrator'));
        }
    }
    
    public function getVisitorTimeSpent() {

        $report_type = $this -> input -> post('report_type');
        $from_date = $this -> input -> post('from_date');
        $to_date = $this -> input -> post('to_date');
        
        $customer = "";
        
        /* customer intelligence */
        //$from_date='';$to_date='';
            
        $p_data=$this->Analytics_model->get_products_purchase_data($from_date,$to_date);
        $sale_count=array();$sale_amount=array();

        $comb_sku_list=array();
        $comb_sku_count=array();
        $comb_sku_amount=array();

        if(!empty($p_data)){
            foreach($p_data as $data){
                $sale_count[$data->sku_id]=intval($data->total_sales);
                $sale_amount[$data->sku_id]=intval($data->total_revenue);
                $comb_sku_list[]=$data->sku_id;
                $comb_sku_count[]=intval($data->total_sales);
                $comb_sku_amount[]=intval($data->total_revenue);

            }
        }

        if($report_type=="total_time_spent"){

            $dates_arr=displayDates($from_date,$to_date);
            //print_r($dates_arr);
            $user=array();$guest=array();
            $category_arr=array();
            
            //print_r($dates_arr);
            $surf_time_all=array();
            $arr_min_all=array();
            $visitors=array();
            $devices=array();
            $city=array();
            $user_city_based=array();
            $user_type=$this->session->userdata("user_type");
            $a_id=$this->session->userdata("user_id");
            $vendor_sku_ids=get_sku_ids_of_product_vendor($a_id);
           
            if(!empty($vendor_sku_ids)){
                $vendor_sku_ids=array_values($vendor_sku_ids);
            }
            //print_r($vendor_sku_ids);
            //exit;

            if(!empty($dates_arr)){

                foreach($dates_arr as $mY=>$dates){
                    

                    if(is_dir("analytics_user/".$mY)){

                        foreach($dates as $d){

                            $surf_time=array();//date_wise
                            $arr_min=array();
                            $surf_user_type=array();

                            $name="analytics_user/".$mY."/".$d.".txt";

                            if(file_exists($name)){
                                $file = fopen($name,"r");
                                $json_data_arr = json_decode(file_get_contents($name), true);

                                //print_r($json_data_arr);

                                if(!empty($json_data_arr)){
                                    
                                    //array_sum(array_column( $json_data_arr, 'view_count')); 
                                    $sum_guest = 0; $sum_user=0; $single_date_visitors=array();$users=[];

                                    foreach ($json_data_arr as $arr) {

                                        

                                        if(isset($arr['request_page'])){
                                            $request_page=$arr['request_page'];
                                            if(!empty($request_page)){
                                                foreach($request_page as $rq){
                                                    //print_r($rq['request_time']);
                                                    if(isset($rq['request_time'])){

                                    

                                    if($user_type=="vendor"){
                                        //echo 'comming into vendor_type <br>';
                                        /* sku wise visit */
                                            if(isset($rq['request_uri'])){
                                                $url= $rq['request_uri'];
                                                if((preg_match("/item/",$rq['request_uri']))){
                                                    $values = parse_url($url);

                                                    //print_r($values);
                                                    $host = explode('/',$values["path"]);

                                                    if(is_array($host)){
                                                        $sku=strtoupper(end($host));
                                                    }else{
                                                        $sku='';
                                                    }
                                                    if($sku!=''){

                                                            if(in_array($sku,$vendor_sku_ids)){

                                                                if(isset($arr['visitor_id'])){
                                                                    if(!in_array($arr['visitor_id'],$single_date_visitors)){
                        
                                                                        $single_date_visitors[]=$arr['visitor_id'];
                                                                        $devices[]=$arr['device'];
                        
                                                                        $temp_user=array_count_values(array_unique($arr['c_id']));
                                                                       
                                                                        //print_r($temp_user);
                                                                        //echo count($temp_user);
                                                                        $uc=0;
                                                                        if(isset($temp_user['guest']) && count($temp_user)>1){
                                                                            //echo "getting_inside1".count($temp_user);
                                                                            //logged in users
                                                                            $sum_user+=(count($temp_user)-1);//same cookie id but multiple login
                                                                            $uc=(count($temp_user)-1);
                                                                        }elseif(!isset($temp_user['guest']) && count($temp_user)>=1){
                                                                            //echo "getting_inside2".count($temp_user);
                                                                            $sum_user+=count($temp_user);
                                                                            $uc=count($temp_user);
                                                                        }else{
                                                                            
                                                                            if(isset($temp_user['guest']) && count($temp_user)==1){
                                                                                //echo "getting_inside";
                                                                                $sum_guest+=1;
                                                                                $uc=1;
                                                                            }
                                                                            
                                                                        }
                        
                                                                        if(isset($arr['geo_data'])){
                                                                            if($arr['geo_data']!="null"){
                        
                                                                                if(!in_array($arr['geo_data'],$city)){
                                                                                    $city[]=($arr['geo_data']!='') ? $arr['geo_data'] : '';
                                                                                }
                                                                                
                                                                                if(isset($user_city_based[$arr['geo_data']])){
                                                                                    $user_city_based[$arr['geo_data']]=($user_city_based[$arr['geo_data']]+$uc);
                                                                                }else{
                                                                                    $user_city_based[$arr['geo_data']]=$uc;// both guest and user 
                                                                                }
                                                                               
                                                                            }
                                                                        }
                                                                        
                                                                    }
                                                                }
                                                                //echo 'Sku visited : '.$sku .' on '.$d.'<br>';
                                                                foreach($rq['request_time'] as $time){
                                                                    $tim_arr=explode('_',$time);
                                                                    $surf_time[]=$tim_arr[0];
                                                                    $surf_user_type[]=isset($tim_arr[1]) ? $tim_arr[1] : 'guest';
                                                                }

                                                                if(isset($rq['timeSpent'])){

                                                                    $arr_times=array_filter($rq['timeSpent']);
                                                                    if(!empty($arr_times)){
                                                                        $arr_sum=array_sum($arr_times);
                                                                        $minutes=milliseconds_to_minutes($arr_sum);
                                                                        $arr_min[]=$minutes;
                                                                    }
                                                                }

                                                                //echo "date:" 
                                                                //print_r($surf_time);

                                                            }else{
                                                                //echo 'no sku visited';
                                                            }
                                                           
                                                    }
                                                }
                                            }

                                            /* sku wise visit */

                                        }else{

                                            if(isset($arr['visitor_id'])){
                                                if(!in_array($arr['visitor_id'],$single_date_visitors)){
    
                                                    $single_date_visitors[]=$arr['visitor_id'];
                                                    $devices[]=$arr['device'];
    
                                                    $temp_user=array_count_values(array_unique($arr['c_id']));
                                                   
                                                    //print_r($temp_user);
                                                    //echo count($temp_user);
                                                    $uc=0;
                                                    if(isset($temp_user['guest']) && count($temp_user)>1){
                                                        //echo "getting_inside1".count($temp_user);
                                                        //logged in users
                                                        $sum_user+=(count($temp_user)-1);//same cookie id but multiple login
                                                        $uc=(count($temp_user)-1);
                                                    }elseif(!isset($temp_user['guest']) && count($temp_user)>=1){
                                                        //echo "getting_inside2".count($temp_user);
                                                        $sum_user+=count($temp_user);
                                                        $uc=count($temp_user);
                                                    }else{
                                                        
                                                        if(isset($temp_user['guest']) && count($temp_user)==1){
                                                            //echo "getting_inside";
                                                            $sum_guest+=1;
                                                            $uc=1;
                                                        }
                                                        
                                                    }
    
                                                    if(isset($arr['geo_data'])){
                                                        if($arr['geo_data']!="null"){
    
                                                            if(!in_array($arr['geo_data'],$city)){
                                                                $city[]=($arr['geo_data']!='') ? $arr['geo_data'] : '';
                                                            }
                                                            
                                                            if(isset($user_city_based[$arr['geo_data']])){
                                                                $user_city_based[$arr['geo_data']]=($user_city_based[$arr['geo_data']]+$uc);
                                                            }else{
                                                                $user_city_based[$arr['geo_data']]=$uc;// both guest and user 
                                                            }
                                                           
                                                        }
                                                    }
                                                    
                                                }
                                            }
                                            foreach($rq['request_time'] as $time){
                                                $tim_arr=explode('_',$time);
                                                $surf_time[]=$tim_arr[0];
                                                $surf_user_type[]=isset($tim_arr[1]) ? $tim_arr[1] : 'guest';
                                            }

                                            if(isset($rq['timeSpent'])){

                                                $arr_times=array_filter($rq['timeSpent']);
                                                if(!empty($arr_times)){
                                                    $arr_sum=array_sum($arr_times);
                                                    $minutes=milliseconds_to_minutes($arr_sum);
                                                    $arr_min[]=$minutes;
                                                }
                                            }

                                        }
                                              
                                                    }//if request_time
                                                    
                                                }
                                            }

                                        }
                                        
                                        /*if(isset($arr['user_type'])){
                                            if($arr['user_type']=='guest'){
                                                $sum_guest += $arr['view_count'];
                                            }else{
                                                $sum_user += $arr['view_count'];
                                            }
                                        }*/

                                    }

                                    //print_r($single_date_visitors);
                                    //print_r($devices);

                                    $guest[]=$sum_guest;
                                    $user[]=$sum_user;
                                    $category_arr[]=date('d M',strtotime($d));

                                    if(!empty($surf_time)){
                                        //print_r($surf_time);
                                        $min_value = min($surf_time);
                                        $max_value = max($surf_time);
                                        $dif=timestamp_diff($min_value,$max_value);
                                        
                                        //echo "<br>dif =".$dif.'<br>';

                                    }else{
                                        $dif=0;
                                    }

                                    $surf_time_all[]=intval($dif);

                                    if(!empty($arr_min)){
                                        $tot_min=array_sum($arr_min);
                                    }else{
                                        $tot_min=0;
                                    }
                                    $arr_min_all[]=round($tot_min,2);

                                    $visitors[]=count($single_date_visitors);
                                    //print_r($users);
                                    $users_all[]=$users;
                                   
                                }
                            }
                            
                        }/* date foreach */

                    } 
                    
                }

            }
            
            $rr=array_count_values($devices);
            //print_r($city);
            //print_r($user_city_based);
            $d_arr=array();

            foreach($rr as $key=>$r){
                $temp=array("name"=>$key,'y'=>$r);
                $d_arr[]=$temp;
            }

            //print_r($surf_time_all);

            $customer=array('guest'=>$guest,'user'=>$user,'category'=>$category_arr,'surf_time_all'=>$arr_min_all,'visitors'=>$visitors,'devices'=>$d_arr,'cities'=>$city,"city_visitors"=>array_values($user_city_based),"product_sale_count"=>$sale_count,"product_sale_amount"=>$sale_amount,"comb_sku_list"=>$comb_sku_list,"comb_sku_count"=>$comb_sku_count,"comb_sku_amount"=>$comb_sku_amount);
        }

        /* customer intelligence */

       
        echo json_encode($customer);
    }

    public function getCatOrder() {

        $from_date = $this -> input -> post('from_date');
        $to_date = $this -> input -> post('to_date');       
        $particular_year = $this -> input -> post('particular_year');
    
         $temp=[];
         $total_order_cat=$this->Analytics_model->get_order_details('cat');
        if(!empty($total_order_cat)){
        
            foreach($total_order_cat as $dat){

                $drill_data=[];
                $drill_data['name']=$dat->cat_name;
                $drill_data['y']=intval($dat->total_orders);
                //$subcategory_arr=$this->Analytics_model->get_all_sub_category_arr_by_cat($dat['cat_id']);
                array_push($temp,$drill_data);

            }
        }

        echo json_encode($temp);
    }

    public function getRevenueOnYearBasis() {

        $from_date = $this -> input -> post('from_date');
        $to_date = $this -> input -> post('to_date');
        $particular_year = $this -> input -> post('particular_year');

         $temp=[];
         $y=date('Y');
         $current=$this->Analytics_model->getRevenueOnYearBasis(date('Y'));
         $py=date("Y",strtotime("-1 year"));
         $previous=$this->Analytics_model->getRevenueOnYearBasis($py);

         //print_r($current);
         //print_r($previous);
        
        $month_arr=array('Jan'=>0,'Feb'=>0,'Mar'=>0,'Apr'=>0,'May'=>0,'Jun'=>0,'Jul'=>0,'Aug'=>0,'Sep'=>0,'Oct'=>0,'Nov'=>0,'Dec'=>0);
        $current_arr=$month_arr;
        $previous_arr=$month_arr;
        if(!empty($current)){
            
            foreach($current as $dat){
                if(in_array($dat->Short,$month_arr)){
                    $current_arr[$dat->Short]=intval($dat->total_revenue);
                }
            }
        }

        //print_r($previous_arr);

        if(!empty($previous)){
            
            foreach($previous as $dat){
                if(in_array($dat->Short,$month_arr)){
                    $previous_arr[$dat->Short]=intval($dat->total_revenue);
                }
            }
        }

        //print_r($previous_arr);

        $temp=array(array('name'=>$y,'data'=>array_values($current_arr)),array('name'=>$py,'data'=>array_values($previous_arr)));
        
        echo json_encode($temp);
    }
    public function profileRegisterCount() {

        $from_date = $this -> input -> post('from_date');
        $to_date = $this -> input -> post('to_date');
        $particular_year = $this -> input -> post('particular_year');

         $temp=[];
         $y=date('Y');
         $current=$this->Analytics_model->profileRegisterCount(date('Y'));

        $month_arr=array('Jan'=>0,'Feb'=>0,'Mar'=>0,'Apr'=>0,'May'=>0,'Jun'=>0,'Jul'=>0,'Aug'=>0,'Sep'=>0,'Oct'=>0,'Nov'=>0,'Dec'=>0);
        $current_arr=$month_arr;
        if(!empty($current)){
            
            foreach($current as $dat){
                if(in_array($dat->Short,$month_arr)){
                    $current_arr[$dat->Short]=intval($dat->total_register);
                }
            }
        }
        $temp=array_values($current_arr);
        
        echo json_encode($temp);
    }
    public function getProfitAndSales(){
        $from_date = $this -> input -> post('from_date');
        $to_date = $this -> input -> post('to_date');
        $profit_data=$this->Analytics_model->getProfitAndSales($from_date,$to_date);

        $arr=array();
        $total_cost_price=0;
        $total_profit=0;
        $total_sales=0;


        if(!empty($profit_data)){
            $total_cost_price=$profit_data->total_cost_price;
            $total_profit=$profit_data->total_profit;
            $total_sales=$profit_data->total_sales;
        }

        $arr["total_cost_price"]=$total_cost_price;
        $arr["total_profit"]=$total_profit;
        $arr["total_sales"]=$total_sales;
        
        echo json_encode($arr);
    }


}
