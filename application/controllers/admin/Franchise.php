<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Franchise extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('admin/Model_franchise');
		$this->load->model('admin/Inventory');
		$this->load->model('Front_promotions');
		$this->load->library('My_fpdf');
		$this->load->helper('emailsmstemplate');
		$this->load->helper('file');
		$this->load->helper('general');
        // Load form validation library
	}
	public function index(){
		if($this->session->userdata("logged_in")){
			$this->parent_category();	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}		
	}
	
	public function franchises(){		
		if($this->session->userdata("logged_in")){
			
			$data['franchises'] = $this->Model_franchise->franchises();
			$data["menu_flag"]="franchise";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/franchise/franchise_view',$data);			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function edit_franchises_form($id=''){
		if($this->session->userdata("logged_in")){
			if($id!=''){
				//franchise login
			}else{
				$id=$this->input->post("id");
			}
			$data['franchises'] = $this->Model_franchise->franchises();
			$data["get_franchises_data"]=$this->get_franchises_data($id);
			$data["id"]=$id;
			$data["menu_flag"]="shipping_active_links";
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/edit_franchises_view',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function add_franchises(){		
		if($this->session->userdata("logged_in")){
			$name=$this->db->escape_str($this -> input -> post("name"));
			$email=$this->db->escape_str($this -> input -> post("email"));
			$mobile=$this->db->escape_str($this -> input -> post("mobile"));
			$landline=$this->db->escape_str($this -> input -> post("landline"));
			$address1=$this->db->escape_str($this -> input -> post("address1"));
			$address2=$this->db->escape_str($this -> input -> post("address2"));
                        $pincode=$this->db->escape_str($this -> input -> post("pincode"));
                        
			$flag=$this->Model_franchise->check_duplication_name_franchise($email,$mobile,$landline,"");
			if($flag==true){
				echo "exist";
			}else{
                            $password=rand(10000000,99999999);
                            $pass=md5($password);
                            
                            $image="assets/pictures/cust_uploads/user.jpg";
                            
                            $update_flag=$this->Model_franchise->add_franchises($name,$email,$mobile,$landline,$address1,$address2,$pincode,$password,$image);
                            if($update_flag==true){
                                    /*sending mail to franchise */
                                    
                                    $email_stuff_arr=email_stuff();
                                    $str='Your franchise account is ready. Now you are a registered franchise. Your password is <b>'.$password.'</b>. Please login to your panel '.base_url()."Administrator.";//activation link has to be sent

				if($email!=''){
					
					$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>Thank You Note</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color:color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;">  <table cellspacing="0" cellpadding="0" style="min-width:500px;background-color:#ff3366 ;"> <tbody> <tr> <td valign="middle" align="center" height="35" style="border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"> <a style="text-decoration:none;outline:none;display:block" href="" target="_blank"> <span style="font-size:16px;color:#ffffff;line-height:14px">We Know</span></a> </td> <td valign="middle" align="center" height="35" style="border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"> <a style="text-decoration:none;outline:none;display:block" href="" target="_blank"> <span style="font-size:16px;color:#ffffff;line-height:14px">We Understand</span> </a>  </td>  <td valign="middle" align="center" height="35" style="border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;">  <a style="text-decoration:none;outline:none;display:block" href="" target="_blank"> <span style="font-size:16px;color:#ffffff;line-height:14px">We Deliver</span>  </a> </td> </tr> </tbody> </table></center><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer"><tr><table style="max-width:500px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top"> <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi, </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">'.$str.'</p><br><p style="padding:0;margin:0;color:#565656;font-size: 15px;">Warm Regards, <br><b>'.$email_stuff_arr["regardsteam_emailtemplate"].'</b></p><br></td></tr><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%" class="flexibleContainer" style="background-color:#2e2e2e;"><tr><td align="center" valign="top" width="500" class="flexibleContainerCell" style="padding: 0px;"><table border="0" cellspacing="0" width="100%"><tr><td valign="top" bgcolor="#2e2e2e"><div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#fff;text-align:center;line-height:140%;"><p>'.$email_stuff_arr["footercopyrights_emailtemplate"].'<br><span style="color:orange">Developed & Maintained by </span> AxonLabs </p></div></td></tr></table></td></tr></table></td></tr></tbody> </table></tr></table></center> </body></html>';
				
					
					$mail = new PHPMailer;
					/*$mail->isSMTP();
					$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
					$mail->Port = '587';//  live - comment it 
					$mail->Auth = true;//  live - comment it 
					$mail->SMTPAuth = true;
					$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
					$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
					$mail->SMTPSecure = 'tls';*/
					$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
					$mail->FromName = $email_stuff_arr["name_emailtemplate"];
					$mail->addAddress($email);
					$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
					$mail->WordWrap = 50;
					$mail->isHTML(true);
					$mail->Subject = 'franchise Registration';
					$mail->Body    = $usermessage;
					if($email != ''){
                                            @$mail->send();
                                            //$flag=$this->Customer_account->sended_code_to_mail($email,$rand);
                                            //echo $flag;
					}
				}
                                
                                echo "yes";
                                    
                                    /*sending mail to franchise */
                            }else{
                                    echo "no";
                            }
			}
	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_franchises_data($id){
		if($this->session->userdata("logged_in")){
			$franchise_arr=$this->Model_franchise->get_franchises_data($id);
			return $franchise_arr;
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function edit_franchises(){
		
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("edit_id");
			$name=$this->db->escape_str($this -> input -> post("edit_name"));
			$email=$this->db->escape_str($this -> input -> post("edit_email"));
			$mobile=$this->db->escape_str($this -> input -> post("edit_mobile"));
			$landline=$this->db->escape_str($this -> input -> post("edit_landline"));
			
			$email_default=$this->db->escape_str($this -> input -> post("edit_email_default"));
			$mobile_default=$this->db->escape_str($this -> input -> post("edit_mobile_default"));
			$landline_default=$this->db->escape_str($this -> input -> post("edit_landline_default"));
			
			$address1=$this->db->escape_str($this -> input -> post("edit_address1"));
			$address2=$this->db->escape_str($this -> input -> post("edit_address2"));
			$pincode=$this->db->escape_str($this -> input -> post("edit_pincode"));
			$flag_check="";
			if($email_default!=$email || $mobile_default!=$mobile || $landline_default!=$landline){
			$flag_check=$this->Model_franchise->check_duplication_name_franchise($email,$mobile,$landline,$id);
			}
			if($flag_check==true || $flag_check!=""){
				echo "exist";
			}else{
			$update_flag=$this->Model_franchise->edit_franchises($id,$name,$email,$mobile,$landline,$address1,$address2,$pincode);
			
				if($update_flag==true){
					echo true;
				}else{
					echo 0;
				}
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}	
	public function update_franchises_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$active=$this->input->post("active");
			$flag=$this -> Model_franchise-> update_franchises_selected($selected_list,$active);		
			if($flag==true){
				
					echo true;
					
			}else{
				echo 0;
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
			
	}
	public function franchise_change_status() {
		if($this->session->userdata("logged_in")){

			$status = $this->input->post('status');
			$id = $this->input->post('id');
			
			$error_msg='';
			$data=get_franchise_details($id);
				
			if($data->password==''){
				$password=randomPassword();

			}else{
				$password='';
			}
			
			$flag=$this->Model_franchise->franchise_change_status($id,$status,$password);
			
			if($flag){
				/* send mail to franchise */
				
				//print_r($data);


				if(!empty($data)){
					
					$email=$data->email;//franchise email
					$name=$data->name;//franchise email
					
					
					if($email!=''){
									$email_stuff_arr=email_stuff();
									$sub='';
									$msg='';
									if($status=='1'){
										$sub='Profile Approved Successfully';

										//$msg.=$password;
										if($password!=''){
										$msg.='Login Credentials for your accout with Voomet,  <br><br><p>Username:<b>'.$email.'</b><br>Password:<b>'.$password.'</b></p><br><br>';
										}
										$msg.='Thanks for registering with us. Your profile has been <span style="color:green;"><b>Approved<b></span> successfully. ';
									}
									if($status=='2'){
										$sub='Profile is Onhold for Approval';
										$msg.='Thanks for registering with us. Your profile is  <b>Onhold</b>. For further details please contact the Admin. ';
									}
									if($status=='3'){
										$sub='Profile has been Rejected';
										$msg.='Your profile verification has been <span style="color:red;"><b>Rejected</b></span> by the Admin. For further details please contact the Admin. ';
									}
					
				$arr=array();
									$arr['name']=$name ;
									$arr['email']=$email;
									$arr['sub']=$sub;
									$arr['msg']=$msg;
									
									$usermessage=$this->load->view('template/notification_franchise_PROFILE',$arr,true);

									
				$mail = new PHPMailer;
				/*$mail->isSMTP();
				$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
				$mail->Port = '587';//  live - comment it 
				$mail->Auth = true;//  live - comment it 
				$mail->SMTPAuth = true;
				$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
				$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
				$mail->SMTPSecure = 'tls';*/
				$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
				$mail->FromName = $email_stuff_arr["name_emailtemplate"];
				$mail->addAddress($email);
				$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				$mail->Subject = $sub;
				$mail->Body    = $usermessage;
				//echo $email;
				if($email != ''){
					@$mail->send();
										
				}
			}
				/* send mail to franchise */
				}
			}
			
			if($status!=''){
				$error_msg='Status updated successfully';
			}

			echo json_encode(array("flag"=>1,'message'=>$error_msg));
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}

	public function franchises_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_franchise->franchises_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_franchise->franchises_processing($params,"get_recs");
			$sno=0;
			$user_type=$this->session->userdata("user_type");
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				//$row[]='<input type="checkbox" name="common_checkbox" id="common_checkbox" onclick="selectAllFun(this)">';
								$row[]=++$sno;
								$txt='';
				$txt.='<b>'.ucfirst($queryRecordsObj->name).'</b>';
				$txt.='<br>'.$queryRecordsObj->email;
				$txt.='<br>'.$queryRecordsObj->mobile;
				$txt.='<br>'.'<b>Address</b>';
				if($queryRecordsObj->address1!='' || $queryRecordsObj->address2 !='')
				$txt.='<br>'.$queryRecordsObj->address1.' ,'.$queryRecordsObj->address2;
				$txt.='<br>'.$queryRecordsObj->city;
				$txt.='<br>'.$queryRecordsObj->state;
				$txt.='<br>'.$queryRecordsObj->pincode;


				$project_count=$this->Model_franchise->franchises_projects_count($queryRecordsObj->id);

				$client_details='<br><a href="'.base_url().'admin/Franchise/franchise_projects/'.$queryRecordsObj->id.'">'.'Total Projects : '.$project_count.'</a>';

				$row[]=$txt;
				$row[]=$client_details;
										
				$str='';
				
								
				$txt='';
				if($queryRecordsObj->approval_status=='0'){
					$txt.='<span style="color:blue"> Yet to process </span>';
				}else if($queryRecordsObj->approval_status=='1'){
					$txt.='<span style="color:green"> <b>Approved </b></span>'.'<br> <small> On '.date('d/m/Y H:i:s',strtotime($queryRecordsObj->status_updated_on)).'</small>';
				}else if($queryRecordsObj->approval_status=='2'){
					$txt.='On Hold'.'<br> <small> On '.date('d/m/Y H:i:s',strtotime($queryRecordsObj->status_updated_on)).'</small>';;
				}else if($queryRecordsObj->approval_status=='3'){
					$txt.='<span style="color:red"> <b>Rejected</b></span>'.'<br> <small> On '.date('d/m/Y H:i:s',strtotime($queryRecordsObj->status_updated_on)).'</small>';;
				}
	
		
				$str.="<br>Approved Status :".  $txt.'<br>';
				
	
				$row[]=$str;
				$row[]=date('d-m-Y H:i:s',strtotime($queryRecordsObj -> timestamp));
				$link='';       
				if(1){
					$link='<select class="form-control" name="change_status_'.$queryRecordsObj->id.'" id="change_status_'.$queryRecordsObj->id.'" onchange="change_status('.$queryRecordsObj->id.',this.value)"> ';
	
					if($queryRecordsObj->approval_status!='1' || $queryRecordsObj->approval_status!='2' || $queryRecordsObj->approval_status!='3'){
						$link.='<option value="">Change status</option>';
					}
					if($queryRecordsObj->approval_status!='1'){
						$link.='<option value="1">Approve</option>';
					}
					if($queryRecordsObj->approval_status!='2'){
						$link.='<option value="2">On hold</option>';
					}
					if($queryRecordsObj->approval_status!='3'){
						$link.='<option value="3">Reject</option>';
					}
					$link.='</select>';
				}
				
				
				$row[]=$link;
				
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function franchises_count(){
		if($this->session->userdata("logged_in")){
			$count=$this->Model_franchise->franchises_count();
			echo $count;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function franchises_projects_count($id){
		if($this->session->userdata("logged_in")){
			$count=$this->Model_franchise->franchises_projects_count($id);
			echo $count;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function franchise_projects($id){		
		if($this->session->userdata("logged_in")){
			
			$data['franchises'] = $this->Model_franchise->franchises_projects_count($id);
			$data['franchise_details'] = $this->Model_franchise->franchise_details($id);
			$data["menu_flag"]="franchise";
			$data["id"]=$id;

			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/franchise/franchise_projects_view',$data);			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function franchises_projects_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_franchise->franchises_projects_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_franchise->franchises_projects_processing($params,"get_recs");
			$sno=0;
			$user_type=$this->session->userdata("user_type");
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				//$row[]='<input type="checkbox" name="common_checkbox" id="common_checkbox" onclick="selectAllFun(this)">';
								$row[]=++$sno;
								$txt='';
								$txt2='';
								$txt3='';
				$txt.='<b>'.ucfirst($queryRecordsObj->project_name).'</b>';
				$txt.='<br><i>'.ucfirst($queryRecordsObj->project_location).'</i>';

				$txt2.='Name : '.$queryRecordsObj->client_name;
				$txt2.='<br>Email : '.$queryRecordsObj->client_email;
				$txt2.='<br>Mob : '.$queryRecordsObj->client_mobile;
			
				$row[]=$txt;
				$row[]=$txt2;

				$row[]=ucfirst($queryRecordsObj->project_type);

				

				if($queryRecordsObj->project_requirements!=''){
				/*requirements */
				$req_str='<span style="cursor:pointer;" data-toggle="modal" data-target="#Modal_'.$queryRecordsObj->project_id.'">
				<u class="text-info">View all requirements</u>
				</span>
<div class="modal fade" id="Modal_'.$queryRecordsObj->project_id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel_'.$queryRecordsObj->project_id.'">'.$queryRecordsObj->project_name.' - Requirements</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
'.$queryRecordsObj->project_requirements.'
</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>';
				}else{
					$req_str='-';
				}


				if($queryRecordsObj->project_file!=''){

					if(file_exists($queryRecordsObj->project_file)){
						$req_str.='<br>
						<u class="text-info"><a href="'.base_url().$queryRecordsObj->project_file.'" target="_blank"><i class="fa fa-paperclip" aria-hidden="true"></i> Attachment</a></u>';
					}

				}

				$row[]=$req_str;

				/*requirements */
				
				$row[]=$queryRecordsObj->project_deadline;
				$row[]=$queryRecordsObj->project_tentative_budget;
				$row[]=ucfirst($queryRecordsObj->project_status);
				$row[]=date('d/m/Y H:i:s',strtotime($queryRecordsObj->project_last_updated_on));
				
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
}
