<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Recomendations extends CI_Controller {
    private $pagesize = 20;
    function __construct() {
        parent::__construct();
        $this -> load -> model('admin/Recomendations_model');
		$this->load->helper('emailsmstemplate');
    }

    public function index() {

        if ($this -> session -> userdata("logged_in")) {
			$data["menu_flag"]="";
            //$this->create_recmendation();
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/recomendations/index.php');
            
        } else {
            redirect(base_url());
        }
    }
     public function recomendations_processing(){
        if($this->session->userdata("logged_in")){
            
            
            $params = $columns = $totalRecords = $data = array();

    $params = $this->input->post();
    
    $totalRecords=$this->Recomendations_model->recomendations_processing($params,"get_total_num_recs");
    
    $queryRecordsResult=$this->Recomendations_model->recomendations_processing($params,"get_recs");
    
    $i=count($queryRecordsResult);
    $i=0;
    foreach($queryRecordsResult as $queryRecordsObj){


        if($queryRecordsObj->timestamp!=""){
            $created_date=date("D,j M Y H:i", strtotime($queryRecordsObj->timestamp));
        }
        else{
            $start_date="";
        }
        if($queryRecordsObj->inv_id!=""){
            $sku_namesss='with SKUID '.$this->getSKUId($queryRecordsObj->inv_id);
        }else{
            $sku_namesss="";
        }
        
        $row=array();
        $row[]=++$i;
        $row[]='<a href="'.base_url().'admin/Recomendations/preview_receipient_item/'.$queryRecordsObj->recomendation_uid.'">'.$queryRecordsObj->name_of_items.'&nbsp;'.$sku_namesss.'</a>';
        $row[]=$created_date;
        $data[]=$row;
        $row=array();
    }
    $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $data   // total data array
            );
    echo json_encode($json_data);  // send data as json format
    
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
   public function create_recmendation(){
        if($this->session->userdata("logged_in")){
            $this->load->model('admin/promotions_manage_data');
            $data['parent_category']=$this->promotions_manage_data->get_all_parent_category();
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/recomendations/create_recepient.php');
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
    }
   public function getExisting_recomendation(){
       if($this->session->userdata("logged_in")){
            $selected_attr_one=$this->input->post('selected_attr_one');
            $selected_level_one=$this->input->post('selected_level_one');
            $selected_attr_two=$this->input->post('selected_attr_two');
            $selected_level_two=$this->input->post('selected_level_two');
            $selected_attr_three=$this->input->post('selected_attr_three');
            $selected_level_three=$this->input->post('selected_level_three');
            $selected_attr_four=$this->input->post('selected_attr_four');
            $selected_level_four=$this->input->post('selected_level_four');
            $selected_attr_five=$this->input->post('selected_attr_five');
            $selected_level_five=$this->input->post('selected_level_five');
            $products_tagged_for_purchasing=$this->input->post('products_tagged_for_purchasing');
            $sql_check=$this->Recomendations_model->getExisting_recomendation($selected_attr_one,$selected_level_one,$selected_attr_two,$selected_level_two,$selected_attr_three,$selected_level_three,$selected_attr_four,$selected_level_four,$selected_attr_five,$selected_level_five,$products_tagged_for_purchasing);
                if($sql_check!=""){
                    $itemsssss=$sql_check['items'];
                    $skussss=$sql_check['skus'];
                    $sku_rec=$sql_check['recomendation_uid'];
                    $uls="<ul>";
                    foreach($itemsssss as $data){
                      $uls.='<li>'.$data.'</li>';  
                    }
                    $uls.="</ul>";
                    $return_result=array();
                    $return_result['recomendation']=$sku_rec;
                    $return_result['skus']=$skussss;
                    $return_result['uls']=$uls;
                    echo json_encode($return_result);
                }
                else{
                    echo 0;
                }
            }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
   }

    public function getExisting_recomendation_tagged(){
       if($this->session->userdata("logged_in")){
            $selected_attr_one=$this->input->post('selected_attr_one');
            $selected_level_one=$this->input->post('selected_level_one');
            $selected_attr_two=$this->input->post('selected_attr_two');
            $selected_level_two=$this->input->post('selected_level_two');
            $selected_attr_three=$this->input->post('selected_attr_three');
            $selected_level_three=$this->input->post('selected_level_three');
            $selected_attr_four=$this->input->post('selected_attr_four');
            $selected_level_four=$this->input->post('selected_level_four');
            $selected_attr_five=$this->input->post('selected_attr_five');
            $selected_level_five=$this->input->post('selected_level_five');
            $recomenation_uid=$this->input->post('recomenation_uid');
            $products_tagged_for_recomendation=$this->input->post('products_tagged_for_recomendation');
            $sql_check=$this->Recomendations_model->getExisting_recomendation_tagged($selected_attr_one,$selected_level_one,$selected_attr_two,$selected_level_two,$selected_attr_three,$selected_level_three,$selected_attr_four,$selected_level_four,$selected_attr_five,$selected_level_five,$recomenation_uid,$products_tagged_for_recomendation);
            
            //print_r($sql_check); exit;
                if($sql_check){
                    $first_level_ul="";
                    $second_level_ul="";
                    $third_level_ul="";
                    $culprit_skus="";
                    if(array_key_exists('first', $sql_check)){
                        $first_level=$sql_check['first'];
                        $first_level_ul.="<ul class='list-group'> <h4>Already tagged as 1st Degree</h4>";
                        $i=1;
                        foreach($first_level as $data){
                            $first_level_ul.='<li class="list-group-item" style="float:left">'.$this->getSKUId($data['sku_item']).'</li>';
                            $culprit_skus.=$data['id'].'-'.$data['sku_item'];
                            if($i<count($first_level)){
                                $culprit_skus.='+';
                            }
                         $i++;
                        }
                        $first_level_ul.="</ul><br>";
                    }
                    if(array_key_exists('second', $sql_check)){
                        $second_level=$sql_check['second'];
                        $second_level_ul.="<br><ul class='list-group'><h4>Already tagged as 2nd Degree</h4>";
                        $i=1;
                        $culprit_skus.="+";
                        foreach($second_level as $data){
                            $second_level_ul.='<li class="list-group-item" style="float:left">'.$this->getSKUId($data['sku_item']).'</li>';
                            $culprit_skus.=$data['id'].'-'.$data['sku_item'];
                            if($i<count($second_level)){
                                $culprit_skus.='+';
                            }
                            $i++;
                        }
                        $second_level_ul.="</ul><br>";
                    }
                    if(array_key_exists('third', $sql_check)){
                        $third_level=$sql_check['third'];
                        $third_level_ul.="<br><ul class='list-group'><h4>Already tagged as 3rd Degree</h4>";
                        $i=1;
                        $culprit_skus="+";
                        foreach($third_level as $data){
                            $third_level_ul.='<li class="list-group-item" style="float:left">'.$this->getSKUId($data['sku_item']).'</li>';
                            $culprit_skus.=$data['id'].'-'.$data['sku_item'];
                            if($i<count($third_level)){
                                $culprit_skus.='+';
                            }
                            $i++;
                        }
                        $third_level_ul.="</ul><br>";
                    }
                    $return_result=array();
                    $return_result['skus']=$culprit_skus;
                    $return_result['uls']=$first_level_ul.$second_level_ul.$third_level_ul;
                    echo json_encode($return_result);
                    
                    
                    /*$itemsssss=$sql_check['items'];
                    $skussss=$sql_check['skus'];
                    $uls="<ul>";
                    foreach($itemsssss as $data){
                      $uls.='<li>'.$data.'</li>';  
                    }
                    $uls.="</ul>";
                    $return_result=array();
                    $return_result['skus']=$skussss;
                    $return_result['uls']=$uls;
                    echo json_encode($return_result);*/
                }
                else{
                    echo 0;
                }
            }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
   }

   public function select_products_for_receipient(){
       if($this->session->userdata("logged_in")){
            
          //print_r($this->input->post()); exit;
            $products_tagged_for_purchasing="";
            $parent_category="";
            $category="";
            $sub_category="";
            $attributes="";
            $attributes_selector="";
            $name_of_products_tagged_for_purchasing="";
            extract($this->input->post());
            $date = new DateTime();
            $mt=microtime();
            $salt="promo_id";
            $rand=mt_rand(1, 1000000);
            $ts=$date->getTimestamp();
            $str=$salt.$rand.$ts.$mt;
            $recomendation_uid=md5($str);

            if($override_previous_recomendation!='0' && $override_previous_recomendation!='group_delete'){
                $deleted=$this->delete_previous_recomendations($parent_category,$category,$sub_category,$attributes,$attributes_selector,$override_previous_recomendation);
                if(!$deleted){
                    $this->session->set_flashdata('notification','Error While Deleting Previous Recomendations');
                    redirect(base_url().'admin/Recomendations/preview_receipient_item/'.$recomendation_uid);
                }
            }
            if($override_previous_recomendation=='group_delete' && $update_confilct=='0'){
                
                $deleted=$this->group_delete_previous_recomendations($parent_category,$category,$sub_category,$attributes,$attributes_selector);
                if(!$deleted){
                    $this->session->set_flashdata('notification','Error While Deleting Previous Recomendations');
                    redirect(base_url().'admin/Recomendations/preview_receipient_item/'.$recomendation_uid);
                }
            }
            
        if($update_confilct=='0'){
            $added=$this->Recomendations_model->add_receipient($recomendation_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector,$name_of_products_tagged_for_purchasing);
        }
                    
        if($update_confilct!='0'){
            $recomendation_uid=$update_confilct;
            $added=$this->Recomendations_model->update_receipient($recomendation_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector,$name_of_products_tagged_for_purchasing);
        }
            
            if($added){
                $this->session->set_flashdata('notification','Receipient Items Added');
                redirect(base_url().'admin/Recomendations/preview_receipient_item/'.$recomendation_uid);
            }
        }
        else{
            $this->load->view('admin/header');
            $this->load->view('admin/login');
        }
   }

public function group_delete_previous_recomendations($parent_category,$category,$sub_category,$attributes,$attributes_selector){
    return $this->Recomendations_model->group_delete_previous_recomendations($parent_category,$category,$sub_category,$attributes,$attributes_selector);
}

public function delete_previous_recomendations($parent_category,$category,$sub_category,$attributes,$attributes_selector,$override_previous_recomendation){
    return $this->Recomendations_model->delete_previous_recomendations($parent_category,$category,$sub_category,$attributes,$attributes_selector,$override_previous_recomendation);
}
public function preview_receipient_item($recomendation_uid){
    if($this->session->userdata("logged_in")){
        $recomendation_receipient=$this->Recomendations_model->getRecepientdata($recomendation_uid);
        if($recomendation_receipient){
            $data['recomendation_uids']=$recomendation_uid;
            $data['recepient_data']=$recomendation_receipient;
            $data['controller']=$this;
            $data['recepient_data_first_level']=$this->Recomendations_model->getRecepientTaggeddataFirstLevel($recomendation_uid);
            $data['recepient_data_second_level']=$this->Recomendations_model->getRecepientTaggedSecondLevel($recomendation_uid);
            $data['recepient_data_third_level']=$this->Recomendations_model->getRecepientTaggedThirdLevel($recomendation_uid);
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/recomendations/view_recepient.php');
        }else{
            $this->session->set_flashdata('notification','Receipient Item Not Found');
            redirect(base_url().'admin/Recomendations/create_recmendation/');
        }
        
        
    }
    else{
        $this->load->view('admin/header');
        $this->load->view('admin/login');
    }
}

public function getSKUId($inventoryId){
    return $this->Recomendations_model->getSKUId($inventoryId);
    
}
public function getProductName($productId){
    return $this->Recomendations_model->getProductName($productId);
}

public function add_first_level_recomendation($recomendation_uid){
    if($this->session->userdata("logged_in")){
        $recomendation_receipient=$this->Recomendations_model->getRecepientLastdata($recomendation_uid);
        if($recomendation_receipient){
        $data['recomendation_uids']=$recomendation_uid;
        $data['recomendation_items']=$recomendation_receipient;
        $this->load->model('admin/promotions_manage_data');
        $data['parent_category']=$this->promotions_manage_data->get_all_parent_category();
        $this -> load -> view('admin/vmanage',$data);
        $this -> load -> view('admin/recomendations/add_recomendation_first.php');
        }
        else{
            $this->session->set_flashdata('notification','Receipient Item Not Found');
            redirect(base_url().'admin/Recomendations/create_recmendation/');
        }
    }
    else{
        $this->load->view('admin/header');
        $this->load->view('admin/login');
    }
}

public function add_second_level_recomendation($recomendation_uid){
    if($this->session->userdata("logged_in")){
        $recomendation_receipient=$this->Recomendations_model->getRecepientLastdata($recomendation_uid);
        if($recomendation_receipient){
        $data['recomendation_uids']=$recomendation_uid;
        $data['recomendation_items']=$recomendation_receipient;
        $this->load->model('admin/promotions_manage_data');
        $data['parent_category']=$this->promotions_manage_data->get_all_parent_category();
        $this -> load -> view('admin/vmanage',$data);
        $this -> load -> view('admin/recomendations/add_recomendation_second.php');
        }
        else{
            $this->session->set_flashdata('notification','Receipient Item Not Found');
            redirect(base_url().'admin/Recomendations/create_recmendation/');
        }
    }
    else{
        $this->load->view('admin/header');
        $this->load->view('admin/login');
    }
}
public function add_third_level_recomendation($recomendation_uid){
    if($this->session->userdata("logged_in")){
        $recomendation_receipient=$this->Recomendations_model->getRecepientLastdata($recomendation_uid);
        if($recomendation_receipient){
        $data['recomendation_uids']=$recomendation_uid;
        $data['recomendation_items']=$recomendation_receipient;
        $this->load->model('admin/promotions_manage_data');
        $data['parent_category']=$this->promotions_manage_data->get_all_parent_category();
        $this -> load -> view('admin/vmanage',$data);
        $this -> load -> view('admin/recomendations/add_recomendation_third.php');
        }
        else{
            $this->session->set_flashdata('notification','Receipient Item Not Found');
            redirect(base_url().'admin/Recomendations/create_recmendation/');
        }
    }
    else{
        $this->load->view('admin/header');
        $this->load->view('admin/login');
    }
}

public function add_items_tagged_under_recomendations($recomendation_uid){
    if($this->session->userdata("logged_in")){
        //print_r($this->input->post()); exit;
        $products_tagged_for_recomendation="";
        $parent_category="";
        $category="";
        $sub_category="";
        $attributes="";
        $attributes_selector="";
        $level_recomendation="";
        extract($this->input->post());
        if($override_previous_recomendation!='0'){
            $itemsss_arr=explode('+',$override_previous_recomendation);
            foreach($itemsss_arr as $itemsss){
                $item_arr=explode('-',$itemsss);
                $this->delete_group_of_recomendation_level_items_sub_rr($item_arr[0],$item_arr[1]);
            }
            
        }
        
        $added=$this->Recomendations_model->add_items_tagged_under_recomendations($recomendation_uid,$products_tagged_for_recomendation,$parent_category,$category,$sub_category,$attributes,$attributes_selector,$level_recomendation);
            if($added){
                $this->session->set_flashdata('notification',$level_recomendation.' Tagged Items Added');
                redirect(base_url().'admin/Recomendations/preview_receipient_item/'.$recomendation_uid);
            }else{
                $this->session->set_flashdata('notification',$level_recomendation.' Tagged Items Not Added');
                redirect(base_url().'admin/Recomendations/add_first_level_recomendation/'.$recomendation_uid);
            }
        
    }
    else{
        $this->load->view('admin/header');
        $this->load->view('admin/login');
    }
    
}
public function delete_group_of_recomendation_level_items_sub_rr($ids,$sub_item){
    if($this->session->userdata("logged_in")){

        return $this->Recomendations_model->delete_group_of_recomendation_level_items_sub_rr($ids,$sub_item);

    }
    else{
        $this->load->view('admin/header');
        $this->load->view('admin/login');
    }
}

public function delete_group_of_recomendation_level_items(){
    if($this->session->userdata("logged_in")){
        $recomendation_level=$this->input->post('recomendation_level');
        $recomendation_id=$this->input->post('recomendation_id');
        $ids=$this->input->post('ids');
        //echo 1;exit;
        $delete=$this->Recomendations_model->delete_group_of_recomendation_level_items($recomendation_level,$recomendation_id,$ids);
        if($delete){
            echo 1;
        }else{
            echo 0;
        }
    }
    else{
        $this->load->view('admin/header');
        $this->load->view('admin/login');
    }
}
public function delete_group_of_recomendation_level_items_sub(){
    if($this->session->userdata("logged_in")){
        $recomendation_level=$this->input->post('recomendation_level');
        $recomendation_id=$this->input->post('recomendation_id');
        $ids=$this->input->post('ids');
        $sub_item=$this->input->post('sub_item');
        //echo 1;exit;
        $delete=$this->Recomendations_model->delete_group_of_recomendation_level_items_sub($recomendation_level,$recomendation_id,$ids,$sub_item);
        if($delete){
            echo 1;
        }else{
            echo 0;
        }
    }
    else{
        $this->load->view('admin/header');
        $this->load->view('admin/login');
    }
}
public function blah_blah(){
    if($this->session->userdata("logged_in")){
        
    }
    else{
        $this->load->view('admin/header');
        $this->load->view('admin/login');
    }
}
public function edit_receipient($id){
    if($this->session->userdata("logged_in")){
        $recomendation_receipient=$this->Recomendations_model->getRecepientdata($id);
        if($recomendation_receipient){
            $this->load->model('admin/promotions_manage_data');
            $data['recepitent_level']=$this->Recomendations_model->getRecipientData($id);
            $data['parent_category']=$this->promotions_manage_data->get_all_parent_category();
            $data['controller']=$this;
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/recomendations/edit_receipient');
        }
        else{
            $this->session->set_flashdata('notification','Receipient Item Not Found');
            redirect(base_url().'admin/Recomendations');
        }
        
    }
    else{
        $this->load->view('admin/header');
        $this->load->view('admin/login');
    }
}
public function update_receipient_list($id){
    if($this->session->userdata("logged_in")){
        $recomendation_receipient=$this->Recomendations_model->getRecepientdata($id);
        if($recomendation_receipient){
            //print_r($this->input->post()); exit;
            $products_tagged_for_purchasing="";
            $parent_category="";
            $category="";
            $sub_category="";
            $attributes="";
            $attributes_selector="";
            $name_of_products_tagged_for_purchasing="";
            extract($this->input->post());
            $recomendation_uid=$id;
             if($override_previous_recomendation!='0' && $override_previous_recomendation!='group_delete'){
                $deleted=$this->delete_previous_recomendations($parent_category,$category,$sub_category,$attributes,$attributes_selector,$override_previous_recomendation);
                if(!$deleted){
                    $this->session->set_flashdata('notification','Error While Deleting Previous Recomendations');
                    redirect(base_url().'admin/Recomendations/preview_receipient_item/'.$recomendation_uid);
                }
            }

            if($override_previous_recomendation=='group_delete' && $update_confilct=='0'){
                
                $deleted=$this->group_delete_previous_recomendations($parent_category,$category,$sub_category,$attributes,$attributes_selector);
                if(!$deleted){
                    $this->session->set_flashdata('notification','Error While Deleting Previous Recomendations');
                    redirect(base_url().'admin/Recomendations/preview_receipient_item/'.$recomendation_uid);
                }
            }
            if($update_confilct!='0'){
                $recomendation_uid=$update_confilct;
                $added=$this->Recomendations_model->update_receipient($recomendation_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector,$name_of_products_tagged_for_purchasing);
            }
            if($override_previous_recomendation=='0' && $update_confilct=='0'){
                $added=$this->Recomendations_model->update_receipient($recomendation_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector,$name_of_products_tagged_for_purchasing);
            }
            if($override_previous_recomendation!='0' && $update_confilct=='0'){
                $added=$this->Recomendations_model->update_receipient($recomendation_uid,$products_tagged_for_purchasing,$parent_category,$category,$sub_category,$attributes,$attributes_selector,$name_of_products_tagged_for_purchasing);
            }
            
            if($added){
                $this->session->set_flashdata('notification','Receipient Items update');
                redirect(base_url().'admin/Recomendations/preview_receipient_item/'.$recomendation_uid);
            }
            
            
            
        }
        else{
            $this->session->set_flashdata('notification','Receipient Item Not Found');
            redirect(base_url().'admin/Recomendations');
        }
    }
    else{
        $this->load->view('admin/header');
        $this->load->view('admin/login');
    }
}
public function delete_receipient(){
	if($this->session->userdata("logged_in")){
		
		$recomendation_uid=$this->input->post('recomendation_uid');
		$flag=$this->Recomendations_model->delete_receipient($recomendation_uid);
		echo $flag;
		
	}else{
        $this->load->view('admin/header');
        $this->load->view('admin/login');
    }
}
   
}
