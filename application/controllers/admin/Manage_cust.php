<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Manage_cust extends CI_Controller {
        private $pagesize = 20;
	function __construct(){
		parent::__construct();
		$this->load->database();
        $data["controller"]=$this;
		$this->load->model('admin/Model_customers');
        $this->load->helper(array('url','form','general'));
		$this->load->helper('emailsmstemplate');
		$this->load->library('My_PHPMailer');	
	}
	public function index(){
		if($this->session->userdata("logged_in")){
			$data["menu_flag"]="customer_view";
			$this->load->view('admin/vmanage',$data);
			$this->customer_view();	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	function customer_view() {
        $data['customers'] = $this->Model_customers->get_all_customer();
		$this->load->view('admin/vmanage',$data);
		$this->load->view('admin/cust_manage/customers',$data);
    }
	
	
	public function customers_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			
			$totalRecords=$this->Model_customers->customers_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_customers->customers_processing($params,"get_recs");
			$i=1;
			
			foreach($queryRecordsResult as $queryRecordsObj){
				
				$row[]=$i;
				$row[]="<img src='".base_url().$queryRecordsObj->image."' width='50' height='40'>";
				
				$row[]=$queryRecordsObj -> name;
				$row[]=$queryRecordsObj -> email;
				$row[]=$queryRecordsObj -> mobile;
				if($queryRecordsObj -> view==0){
				$row[]='<input type="button" class="btn btn-danger btn-xs btn-block" value="View Profile" onClick="showcustomerprofile('.$queryRecordsObj -> id.')">';
				}
				if($queryRecordsObj -> view==1){
				$row[]='<input type="button" class="btn btn-info btn-xs btn-block" value="View Profile" onClick="showcustomerprofile('.$queryRecordsObj -> id.')">';
				}
				$data[]=$row;	
				$row=array();
				$i++;
				
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function customers_count(){
		if($this->session->userdata("logged_in")){
			$count=$this->Model_customers->customers_count();
			echo $count;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function view($customer_id){
		if($this->session->userdata("logged_in")){
		
			$data["controller"]=$this;
			$view_prof = $this->Model_customers->set_view_customer($customer_id);
			$data["view_prof"]=$view_prof;
			
			$personal_info = $this->Model_customers->get_data_from_customer($customer_id);
			$data["personal_info"]=$personal_info;
			$addresses=$this->Model_customers->get_all_customer_addresses($customer_id);
			$data["addresses"]=$addresses;	

			$wishlist=$this->Model_customers->get_all_products_from_wishlist($customer_id);			
			$data["wishlist"]=$wishlist;
			
			$reviews_and_ratings=$this->Model_customers->get_customer_reviews_and_ratings($customer_id);
			$data["reviews_and_ratings"]=$reviews_and_ratings;

			$wallet_transaction_details=$this->Model_customers->get_wallet_transaction_details($customer_id);	
			$data["wallet_transaction_details"]=$wallet_transaction_details;
			
			$bank_account_number=$this->Model_customers->get_customer_all_bank($customer_id);
			$bank_details=array();
			if(!empty($bank_account_number)){
				foreach($bank_account_number as $account_number){
					$bank_details[]=$this->Model_customers->get_bank_detail($customer_id,$account_number->account_number);
				}
				$data["bank_details"]=$bank_details;
			}else{
				$data["bank_details"]=$bank_details;
			}

			$saved_cards=$this->Model_customers->get_customer_all_cards($customer_id);
			$data["saved_cards"]=$saved_cards;
			
			$wallet_details=$this->Model_customers->get_customer_wallet_summary($customer_id);
			$data["wallet_details"]=$wallet_details;
			
			$data["all_orders"]=$this->my_order($customer_id);
			$data["service_ratings"]=$this->get_service_ratings($customer_id);
			
			$data["skill_material"]=$this->Model_customers->get_additional_customer_info($customer_id);
			$data['deactivate_reason'] = $this->Model_customers->deactivate_reason();
			$data['deactivate_history_details'] = $this->Model_customers->deactivate_history_details($customer_id);
			$data["menu_flag"]="customer_view";
			$data["customer_id"]=$customer_id;
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/cust_manage/customer_details',$data);
		}else{
			redirect(base_url());
		}
	}
	public function get_skill_material_info($sk_mat){
		$get_skill_material_info_arr=$this->Model_customers->get_skill_material_info($sk_mat);
		return $get_skill_material_info_arr;
	}
	public function get_customer_skills_info($skills_in_arr){
		$skills_arr=explode(",",$skills_in_arr);
		$result=$this->Model_customers->get_customer_skills_info($skills_arr);
		$total_skill_arr=array();
		foreach($result as $resarr){
			$total_skill_arr[]=$resarr["skill_value"];
		}
		return implode(", ",$total_skill_arr);
	}	
	public function customer_prefer_material_info($materials_in_arr){
		$materials_arr=explode(",",$materials_in_arr);
		$result=$this->Model_customers->customer_prefer_material_info($materials_arr);
		$total_materials_arr=array();
		foreach($result as $resarr){
			$total_materials_arr[]=$resarr["material"];
		}
		return implode(", ",$total_materials_arr);
	}
	public function my_order($customer_id){
		
			$order_item_id_arr=array();
			$all_orders_gp=array();
			$all_orders_collection=array();
			$all_orders_collection=$this->Model_customers->get_all_orders($customer_id);
			if(!empty($all_orders_collection)){
				foreach($all_orders_collection as $order_statusarr){
					$order_item_id_arr[]=$order_statusarr["order_item_id"];
				}
			}
			
			$all_distinct_orders=$this->Model_customers->get_all_distinct_orders_id($customer_id);
			$all_distinct_orders=array_unique($all_distinct_orders);
			
			foreach($all_distinct_orders as $distinct_ordersObj){
				$all_orders_gp[$distinct_ordersObj][]=$this->Model_customers->all_orders($distinct_ordersObj);
			}		
			
			return $all_orders_gp;

	}
	public function get_all_order_items($id,$order_id){
			return $this->Model_customers->get_all_order_items($id,$order_id);
	}
	public function get_order_placed_date($id){
			return $this->Model_customers->get_order_placed_date($id);
	}
	public function get_order_grand_total($id){
			$grandtotal=$this->Model_customers->get_order_grand_total($id);
			$total_order_val=0;
			foreach($grandtotal as $total){
				$total_order_val+=$total['grandtotal'];
			}
			return $total_order_val;
	}
	public function get_product_name($product_id){
			$product_name=$this->Model_customers->get_product_name($product_id);
			return $product_name;
	}
	public function get_in_completed_table_or_not($order_item_id){
		$result=$this->Model_customers->get_in_completed_table_or_not($order_item_id);
		return $result;
	}
	public function get_in_returned_table_or_not($order_item_id){
		$result=$this->Model_customers->get_in_returned_table_or_not($order_item_id);
		return $result;
	}
	public function get_in_replaced_table_or_not($order_item_id){
		$result=$this->Model_customers->get_in_replaced_table_or_not($order_item_id);
		return $result;
	}
	public function get_return_refund_table_by_order_item_id($order_item_id){
		$return_refund_table=$this->Model_customers->get_return_refund_table_by_order_item_id($order_item_id);
		return $return_refund_table;
	}
	public function get_return_replacement_table_by_order_item_id($order_item_id){
		$return_replacement_table=$this->Model_customers->get_return_replacement_table_by_order_item_id($order_item_id);
		return $return_replacement_table;
	}
	public function get_return_replacement_desired_order_item_id($order_item_id){
		$return_replacement_table=$this->Model_customers->get_return_replacement_desired_order_item_id($order_item_id);
		return $return_replacement_table;
	}
	
	public function get_order_item_status_summary($id){
			$order_item_status=$this->Model_customers->get_order_item_status_summary($id);
			$order_item_status_arr=array_reverse($order_item_status[0]);
			$state_as="";
			$flag=0;
			$btn="";
			foreach($order_item_status_arr as $k => $v){
				
				if($v=='1'){
					
					$flag=1;
					$statuss=explode('_',$k);
					foreach($statuss as $state){
						$state_as.=' '.ucfirst($state);
					}
					if($k=="order_placed"||$k=="order_confirmed"||$k=="order_packed"){
						$str='Order Placed';
					}
					if($k=="order_shipped"){
						$str='Order Shipped';
					}
					if($k=="order_delivered"){
						$str='Order Delivered';
					}
					if($k=="order_cancelled"){
						$str='Order was Cancelled';
					}
					return $state_as." @ ".date_format(date_create($order_item_status_arr['timestamp']), "D, jS M'y");//.'<br><span class="text-color">Order Status:&nbsp;</span>'.$str;
					break;
				}
			}
			
			if($flag=='0'){
				//$btn='<a href="'.base_url().'Account/cancel_order/'.$id.'"<button class="btn btn-default"><i class="fa fa-times" aria-hidden="true"></i> Cancel </button></a>';
				$state_as="order yet to be placed";
				return $state_as." @ ".date_format(date_create($order_item_status_arr['timestamp']), "D, jS M'y");//.'<br><span class="text-color">Order Status:&nbsp;</span>'.$str;
			}
	}
	public function de_activate_customer(){
		$id=$this->input->post("id");
		$block_user=$this->input->post("block_user");
		$deactivate_reason=$this->input->post("deactivate_reason");
		$comments=$this->input->post("comments");
		$flag=$this->Model_customers->de_activate_customer($id,$deactivate_reason,$comments,$block_user);
		echo $flag;
	}
	public function de_activate_customer_acc($customer_id){
		$flag=$this->Model_customers->de_activate_customer_acc($customer_id);
		echo $flag;
	}
	public function activate_customer(){
		$id=$this->input->post("id");
		$reactivate_comments=$this->input->post("reactivate_comments");
		$flag=$this->Model_customers->activate_customer($id,$reactivate_comments);
		echo $flag;
	}
	public function deactivate_history_check(){
		$id=$this->input->post("id");
		$flag=$this->Model_customers->deactivate_history_check($id);
		echo $flag;
	}
	public function get_service_ratings($customer_id){
		
		$rateorderObj=$this->Model_customers->get_service_ratings($customer_id);
		return $rateorderObj;
	}
	
	public function customer_wallet_bank_transfer(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["menu_flag"]="customer_wallet_bank_transfer";
			$wallet_bank_transfer_view=$this -> Model_customers-> update_wallet_bank_transfer_view();
			$data["wallet_bank_transfer_view"]=$wallet_bank_transfer_view;
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/cust_manage/customer_bank_transfer',$data);
		}else{
			redirect(base_url());
		}
	}
	public function get_wallet_transaction_table_by_wallet_transaction_bank($wallet_id,$customer_id){
		$wallet_transaction_table_by_wallet_transaction_bank_arr=$this->Model_customers->get_wallet_transaction_table_by_wallet_transaction_bank($wallet_id,$customer_id);
		return $wallet_transaction_table_by_wallet_transaction_bank_arr;
	}
	public function customer_bank_transfer_processing(){
		
		$params = $columns = $totalRecords = $data = array();

			$params = $this->input->post();
			
			$totalRecords=$this->Model_customers->customer_bank_transfer_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_customers->customer_bank_transfer_processing($params,"get_recs");
			$i=1;
			$i=count($queryRecordsResult);
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]=$i;
				$request_summary='<ul class="list-unstyled">';
				$request_summary.="<li>Customer Name: ".$queryRecordsObj->name."</li>";
				$request_summary.="<li>Return requested date: ".date("D j M, Y - h:i a",strtotime($queryRecordsObj->timestamp))."</li>";
				$request_summary.="<li>Request id: ".$queryRecordsObj->wallet_request_id."</li>";
				$request_summary.="<li>Requested Amount: ".curr_sym.$queryRecordsObj->transaction_amount."</li>";
				$request_summary.='</ul>';
				$row[]=$request_summary;
				$wallet_transaction_table_by_wallet_transaction_bank_arr=$this->get_wallet_transaction_table_by_wallet_transaction_bank($queryRecordsObj->wallet_id,$queryRecordsObj->customer_id);
				$bank_details='<ul class="list-unstyled">';
				$bank_details.='<li>Bank Name : '.$queryRecordsObj->bank_name.'</li>';
				$bank_details.='<li>Branch Name : '.$queryRecordsObj->branch_name.'</li>';
				$bank_details.='<li>City : '.$queryRecordsObj->city.'</li>';
				$bank_details.='<li>IFSC Code : '.$queryRecordsObj->ifsc_code.'</li>';
				$bank_details.='<li>Account Number : '.$queryRecordsObj->account_number.'</li>';
				$bank_details.='<li>Account Holder Name : '.$queryRecordsObj->account_holder_name.'</li>';
				$bank_details.='<li>Mobile Number : '.$queryRecordsObj->mobile_number.'</li>';
				$bank_details.='</ul>';
				$row[]=$bank_details;
				$actions='<ul class="list-unstyled">';
				if($queryRecordsObj->status=="reject"){
					$actions.='<li>This request has been rejected</li>';
				}
				if($queryRecordsObj->status=="accept"){
					$actions.='<li>This request has been processed</li>';
				}
				if($queryRecordsObj->status=="success"){
					$actions.='<li>This request has been successfully transferred</li>';
				}
				if($queryRecordsObj->status=="failure"){
					$actions.='<li>This request has been failed</li>';
				}
				if($queryRecordsObj->status=="pending"){
					$actions.='<li><input type="button" value="Accept" class="btn btn-success btn-xs btn-block" onclick="open_wallet_amount_edit_fun('.$queryRecordsObj->id.')"></li>';
					
					$actions.='<li><input type="button" value="Reject" class="btn btn-danger btn-xs btn-block" onclick="reject_fun('.$queryRecordsObj->id.')"></li>';
					
					$actions.='<li><div id="reject_div_'.$queryRecordsObj->id.'" style="display:none"><form id="reject_form_'.$queryRecordsObj->id.'" ><textarea placeholder="Write comments" id="reject_text_'.$queryRecordsObj->id.'" name="rejected_comments" class="form-control"></textarea>';
					$actions.='<input type="hidden" name="wallet_id" value="'.$queryRecordsObj->wallet_id.'">';
					$actions.='<input type="hidden" name="customer_id" value="'.$queryRecordsObj->customer_id.'">';
					$actions.='<input type="hidden" name="transaction_amount" value="'.$queryRecordsObj->transaction_amount.'">';
					$actions.='<input type="hidden" name="wallet_transaction_bank_id" value="'.$queryRecordsObj->id.'">';
					$actions.='<input type="hidden" name="wallet_request_id" value="'.$queryRecordsObj->wallet_request_id.'">';
					$actions.='<input type="button" value="Submit" class="btn btn-success btn-xs btn-block" onclick="submit_reject_fun('.$queryRecordsObj->id.')"></form></div></li>';
				}
				$allow_update_bank_details = str_replace(' ', '', $queryRecordsObj->allow_update_bank_details);
				
				if(strtolower($allow_update_bank_details)=='updated' && $queryRecordsObj->status!='success'){
					
					$actions.='<li><input type="button" class="btn btn-success btn-xs btn-block" value="Resend" onclick="resend_wallet_bank_tranfer_fun('.$queryRecordsObj->id.')"></li>';
				}
				$actions.='</ul>';
				$row[]=$actions;
				$data[]=$row;
				$row=array();
				$i++;
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format

	}
	public function wallet_transaction_bank_accept(){
		

		$wallet_transaction_bank_id=$this->input->post("wallet_transaction_bank_id");
		$transaction_amount=$this->input->post("transaction_amount");
		$accepted_transaction_amount=$this->input->post("accepted_transaction_amount");
		$accepted_transaction_comments=$this->input->post("accepted_transaction_comments");
		$customer_id=$this->input->post("customer_id");

		$wallet_summary=$this->Model_customers->get_customer_wallet_summary($customer_id);
		foreach($wallet_summary as $summary){
			$wallet_id=$summary->wallet_id;
			$amount=$summary->wallet_amount;
		}

		$change=0;
		if($transaction_amount==$accepted_transaction_amount){
			echo $this->Model_customers->wallet_transaction_bank_accept($wallet_transaction_bank_id,$accepted_transaction_amount,$accepted_transaction_comments); // no change
		}elseif($transaction_amount>$accepted_transaction_amount){
			/*credt to wallet because admin sends less */
			$amt=($transaction_amount-$accepted_transaction_amount);
			$change=1;
			$debit="";
			$credit=$amt;
			$amount=($credit+$amount);
			$wallet_type='Admin Approved to send '.curr_sym.$accepted_transaction_amount;	
		}elseif($transaction_amount<$accepted_transaction_amount){
			/*debit from wallet because admin sends more */
			$amt=($accepted_transaction_amount-$transaction_amount);
			$change=2;
			$debit=$amt;
			$credit="";
			$amount=($amount-$amt);
			$wallet_type='Admin Approved to send '.curr_sym.$accepted_transaction_amount;	
		}
		
		$transaction_details=$accepted_transaction_comments;

		if($change>0){
				
			$wallet_transaction_id=$this->Model_customers->update_customer_wallet($wallet_id,$customer_id,$transaction_details,$debit,$credit,$amount,$wallet_type);
			echo $this->Model_customers->wallet_transaction_bank_accept($wallet_transaction_bank_id,$accepted_transaction_amount,$accepted_transaction_comments); // no change
		}

	}
	
	public function wallet_transaction_bank_reject(){
		
		//print_r($this->input->post());
	
		$customer_id=$this->input->post("customer_id");
		$wallet_id=$this->input->post("wallet_id");
		$transaction_amount=$this->input->post("transaction_amount");
		$wallet_transaction_bank_id=$this->input->post("wallet_transaction_bank_id");
		$wallet_request_id=$this->input->post("wallet_request_id");
		$rejected_comments=$this->input->post("rejected_comments");
		
		$wallet_summary=$this->Model_customers->get_customer_wallet_summary($customer_id);
		foreach($wallet_summary as $summary){
			$wallet_id=$summary->wallet_id;
			$amount=$summary->wallet_amount;
		}
		
		$transaction_details=$rejected_comments;

		$debit="";
		$credit=$transaction_amount;
		$amount=$credit+$amount;
		
		// /* added new  */
		$wallet_type='BankTransfer Request is Rejected';
		// $amount=0;
		// $wallet_transaction_id='';
		// /* added new  */


		$wallet_transaction_id=$this->Model_customers->update_customer_wallet($wallet_id,$customer_id,$transaction_details,$debit,$credit,$amount,$wallet_type);
		
		echo $this->Model_customers->wallet_transaction_bank_reject($wallet_transaction_bank_id,$rejected_comments,$wallet_transaction_id);


		/* Mail notification to the customer */

		$email=get_customer_email($customer_id);
		$email_stuff_arr=email_stuff();

		$str=' <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi, </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">Your Bank transfer request is rejected. Your requested amount is credited to your wallet again. </p>';

		$str.='<h4>Rejection Details</h4>';
		$str.='<p>Requested Amount : '.curr_sym.$transaction_amount.'</p>';
		$str.='<p>Rejection Comments : '.$rejected_comments.'</p>';
		
		$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr>
				
				
		<tr> <td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top">'.$str.'<br></td></tr><tr> <td> <table width="100%" cellspacing="0" cellpadding="0" style="max-width:802px;"> <tbody> <tr> <td valign="top" align="center" width="300" style="background-color:#f9f9f9"> <br><table width="100%" cellspacing="0" cellpadding="0"> <tbody> <tr> <td valign="top" align="left" style="padding:0 10px 15px 20px;margin:0"> <p style="padding:0;margin:0 0 7px 0;font-size:16px;color:#565656">Any Questions?</p><p style="padding:0;margin:0;font-size:11px;color:#565656;line-height:20px">Write to us at '.$email_stuff_arr["fromemail_emailtemplate"].' </p></td></tr></tbody> </table> </td></tr></tbody> </table> </td></tr><tr> <td> <table border="0" cellpadding="0" cellspacing="0" width="100%" class="flexibleContainer" style="background-color:#2e2e2e;"> <tr> <td align="center" valign="top" width="500" class="flexibleContainerCell" style="padding: 0px;"> <table border="0" cellspacing="0" width="100%"> <tr> <td valign="top" bgcolor="#2e2e2e"> <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#fff;text-align:center;line-height:140%;"> <p>'.$email_stuff_arr["footercopyrights_emailtemplate"].'</p></div></td></tr></table> </td></tr></table> </td></tr></tbody> </table> </tr></table> </center></body></html>';
			

		if($email!=''){
			$mail = new PHPMailer;
			/*$mail->isSMTP();
			$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
			$mail->Port = '587';//  live - comment it 
			$mail->Auth = true;//  live - comment it 
			$mail->SMTPAuth = true;
			$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
			$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
			$mail->SMTPSecure = 'tls';*/
			$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
			$mail->FromName = $email_stuff_arr["name_emailtemplate"];
			$mail->addAddress($email);
			$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
			$mail->WordWrap = 50;
			$mail->isHTML(true);

			$sub="Bank Transfer Request is Rejected";

			$mail->Subject = $sub;
			$mail->Body    = $usermessage;
			
			if(!$mail->send()){
				echo 'mail is not sent';
			}
		}
		/* Mail notification to the customer */

	}
	
	public function customer_wallet_bank_transfer_accounts_incoming(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$this->Model_customers->notify_account_bank_transfer_unset();
			$data["menu_flag"]="accounts_banktransfer_active_links";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/cust_manage/customer_wallet_bank_transfer_accounts_incoming',$data);
		}else{
			redirect(base_url());
		}
	}
	
	public function open_wallet_amount_edit_fun(){

		$wallet_transaction_bank_id=$this->input->post("wallet_transaction_bank_id");

		$obj=$this->Model_customers->customer_bank_tranfer($wallet_transaction_bank_id);
		
		?>
		<input type="hidden" class="form-control" name="wallet_transaction_bank_id" value="<?php echo $obj["id"]; ?>"> 
		<input type="hidden" class="form-control" name="customer_id" value="<?php echo $obj["customer_id"]; ?>">
		<input type="hidden" class="form-control" name="transaction_amount" value="<?php echo $obj["transaction_amount"]; ?>">

		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
					<div class="col-md-12">
						<label class="">Transaction Amount <?php echo curr_sym; ?>)</label>
						<input type="text" class="form-control" name="accepted_transaction_amount" placeholder="Enter Accepted amount" value="<?php echo $obj["transaction_amount"]; ?>" required />
					<dv>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
			<div class="form-group">
				
				<div class="col-md-12">
				<div class="col-md-12">

					<textarea type="text" class="form-control" name="accepted_transaction_comments" placeholder="Enter Accepted Comments" value="<?php echo $obj["accepted_transaction_comments"]; ?>" required />
					</div>
				</div>
				</div>
			</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				<input type="submit" class="btn btn-success btn-xs btn-block" value="Submit">
			</div>
		</div>
				   
		<?php
	}
	public function customer_wallet_bank_transfer_accounts_incoming_walletsuccess(){
		$wallet_transaction_bank_id=$this->input->post("wallet_transaction_bank_id");
		$customer_bank_tranfer_accounts_incoming_obj=$this->Model_customers->customer_bank_tranfer_accounts_incoming($wallet_transaction_bank_id);
		?>
		<input type="hidden" class="form-control" name="wallet_transaction_bank_id" value="<?php echo $customer_bank_tranfer_accounts_incoming_obj["id"]; ?>">
		 
		 <input type="hidden" class="form-control" name="customer_id" value="<?php echo $customer_bank_tranfer_accounts_incoming_obj["customer_id"]; ?>">
		 
		 <input type="hidden" class="form-control" name="transaction_amount" value="<?php echo $customer_bank_tranfer_accounts_incoming_obj["transaction_amount"]; ?>">
		 
		 <input type="hidden" class="form-control" name="requested_transaction_amount" value="<?php echo $customer_bank_tranfer_accounts_incoming_obj["requested_transaction_amount"]; ?>">

						<div class="form-group">
							<div class="col-md-12">
								<input type="text" class="form-control" name="transaction_id" placeholder="Enter Transaction ID" required />
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<input type="text" class="form-control" name="transaction_date" id="transaction_date" placeholder="Transaction Date" required />
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<input id="transaction_time" name="transaction_time" type="text" class="time form-control" placeholder="Transaction Time" required />
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<input type="submit" class="btn btn-success btn-xs btn-block" value="Submit">
							</div>
						</div>
				   <script>
				  $(document).ready(function(){
						
						$('#transaction_date').bootstrapMaterialDatePicker
						({
							time: false,
							clearButton: true
						});

						$('#transaction_time').bootstrapMaterialDatePicker
						({
							date: false,
							shortTime: true,
							use24hours: false,
							clearButton: true,
							format: 'hh:mm A'
						});

						$.material.init()
					});
				  </script>
                  
		<?php
	}
	
	public function customer_wallet_bank_transfer_accounts_incoming_walletfailure(){
		
		$wallet_transaction_bank_id=$this->input->post("wallet_transaction_bank_id");
		$customer_bank_tranfer_accounts_incoming_obj=$this->Model_customers->customer_bank_tranfer_accounts_incoming($wallet_transaction_bank_id);
		
		?>
		 <input type="hidden" class="form-control" name="wallet_transaction_bank_id" value="<?php echo $customer_bank_tranfer_accounts_incoming_obj["id"]; ?>">
		 
            
                    
						<div class="form-group">
							<div class="col-md-12">
								<textarea class="form-control" name="refund_fail_comments" required placeholder="Write comments"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<input type="checkbox" name="allow_update_bank_details" value="allow_update_bank_details"> Allow customer to update bank details
							</div>
						</div>
					 <div class="form-group">
							<div class="col-md-12">
								<input type="submit" class="btn btn-danger btn-xs btn-block" value="Submit">
							</div>
						</div>
		<?php
	}
	
	public function customer_bank_tranfer_accounts_incoming_processing(){
		
		$params = $columns = $totalRecords = $data = array();

			$params = $this->input->post();
			
			$totalRecords=$this->Model_customers->customer_bank_tranfer_accounts_incoming_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_customers->customer_bank_tranfer_accounts_incoming_processing($params,"get_recs");
			
			$i=count($queryRecordsResult);
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$request_details='<ul class="list-unstyled">';
				$request_details.='<li>'.$queryRecordsObj->name.'</li>';
				$request_details.='<li>Requested on: '.date("D j M, Y - h:i a",strtotime($queryRecordsObj->timestamp)).'</li>';
				$request_details.='<li>Request id: '.$queryRecordsObj->wallet_request_id.'</li>';
				$request_details.="<li>Requested Amount:".curr_sym.$queryRecordsObj->requested_transaction_amount."</li>";
				$request_details.="<li><b>Transaction Amount:".curr_sym.$queryRecordsObj->accepted_transaction_amount."</li>";
				$request_details.='</ul>';
				$row[]=$request_details;
				$wallet_transaction_table_by_wallet_transaction_bank_arr=$this->get_wallet_transaction_table_by_wallet_transaction_bank($queryRecordsObj->wallet_id,$queryRecordsObj->customer_id);
				$bank_details='<ul class="list-unstyled">';
				$bank_details.='<li>Bank Name : '.$queryRecordsObj->bank_name.'</li>';
				$bank_details.='<li>Branch Name : '.$queryRecordsObj->branch_name.'</li>';
				$bank_details.='<li>City : '.$queryRecordsObj->city.'</li>';
				$bank_details.='<li>IFSC Code : '.$queryRecordsObj->ifsc_code.'</li>';
				$bank_details.='<li>Account Number : '.$queryRecordsObj->account_number.'</li>';
				$bank_details.='<li>Account Holder Name : '.$queryRecordsObj->account_holder_name.'</li>';
				$bank_details.='<li>Mobile Number : '.$queryRecordsObj->mobile_number.'</li>';
				$bank_details.='</ul>';
				$row[]=$bank_details;
				$actions="";
				$actions.='<input type="button" value="Success" class="btn btn-success btn-xs btn-block" data-toggle="modal"  onclick="wallet_bank_success_fun('.$queryRecordsObj->id.')">';
				
					$actions.='<input type="button" value="Fail" class="btn btn-danger btn-xs btn-block" data-toggle="modal"  onclick="wallet_bank_failure_fun('.$queryRecordsObj->id.')">';
				
				$row[]=$actions;
				$data[]=$row;
				$row=array();
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format

	}
	
	public function wallet_transaction_bank_success(){
		$wallet_transaction_bank_id=$this->input->post("wallet_transaction_bank_id");

		$transaction_id=$this->input->post("transaction_id");
		$transaction_date=$this->input->post("transaction_date");
		$transaction_time=$this->input->post("transaction_time");
		$transaction_amount=$this->input->post("transaction_amount");
		$customer_id=$this->input->post("customer_id");

		$requested_transaction_amount=$this->input->post("requested_transaction_amount");;

		echo $this->Model_customers->wallet_transaction_bank_success($wallet_transaction_bank_id,$transaction_id,$transaction_date,$transaction_time,$customer_id,$transaction_amount);


		/* Mail notification to the customer */

		$email=get_customer_email($customer_id);
		$email_stuff_arr=email_stuff();

		$str=' <p style="padding:0;margin:0;font-size:16px;font-weight:bold;"> Hi , </p><br><p style="padding:0;margin:0;color:#565656;line-height:22px;font-size: 15px;">Your requested wallet amount is transferred successfully to your bank with given bank details . </p>';

		$str.='<h4>Transaction Details</h4>';
		$str.='<p>Transaction ID : '.$transaction_id.'</p>';
		$str.='<p>Transaction Date : '.date('j F Y',strtotime($transaction_date)).'</p>';
		$str.='<p>Transaction Time : '.$transaction_time.'</p>';
		$str.='<p>Requested amount : '.curr_sym.$requested_transaction_amount.'</p>';
		$str.='<p>Transaction amount : '.curr_sym.$transaction_amount.'</p>';
		
		$usermessage='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="format-detection" content="telephone=no"/><title>'.$email_stuff_arr["name_emailtemplate"].'</title><style type="text/css">body{font-family: Helvetica,Arial,sans-serif;color: rgb(95, 95, 95);font-size: 15px;}</style></head><body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center style="background-color:#E1E1E1;"> <table border="0" cellpadding="0" cellspacing="0" width="800" class="flexibleContainer"><tr><table style="max-width:800px;border-left:solid 1px #e6e6e6;border-right:solid 1px #e6e6e6;" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="top" width="500" style="padding:0px"><table border="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" align="center" style="padding:10px;"><img src="'.base_url().$email_stuff_arr["emaillogo_emailtemplate"].'"></td></tr></tbody></table></td></tr></tbody></table></td></tr>
				
				
		<tr> <td style="color:#2c2c2c;display:block;line-height:20px;margin:0 auto;clear:both;border-bottom:1px solid #e6e6e6;background-color:#f9f9f9;padding:20px;" align="left" bgcolor="#F9F9F9" valign="top">'.$str.'<br></td></tr><tr> <td> <table width="100%" cellspacing="0" cellpadding="0" style="max-width:802px;"> <tbody> <tr> <td valign="top" align="center" width="300" style="background-color:#f9f9f9"> <br><table width="100%" cellspacing="0" cellpadding="0"> <tbody> <tr> <td valign="top" align="left" style="padding:0 10px 15px 20px;margin:0"> <p style="padding:0;margin:0 0 7px 0;font-size:16px;color:#565656">Any Questions?</p><p style="padding:0;margin:0;font-size:11px;color:#565656;line-height:20px">Write to us at '.$email_stuff_arr["fromemail_emailtemplate"].' </p></td></tr></tbody> </table> </td></tr></tbody> </table> </td></tr><tr> <td> <table border="0" cellpadding="0" cellspacing="0" width="100%" class="flexibleContainer" style="background-color:#2e2e2e;"> <tr> <td align="center" valign="top" width="500" class="flexibleContainerCell" style="padding: 0px;"> <table border="0" cellspacing="0" width="100%"> <tr> <td valign="top" bgcolor="#2e2e2e"> <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#fff;text-align:center;line-height:140%;"> <p>'.$email_stuff_arr["footercopyrights_emailtemplate"].'</p></div></td></tr></table> </td></tr></table> </td></tr></tbody> </table> </tr></table> </center></body></html>';
			

		if($email!=''){
			$mail = new PHPMailer;
			/*$mail->isSMTP();
			$mail->Host = $email_stuff_arr["fromemail_Host"];// live - localhost
			$mail->Port = '587';//  live - comment it 
			$mail->Auth = true;//  live - comment it 
			$mail->SMTPAuth = true;
			$mail->Username = $email_stuff_arr["fromemail_Username"];//  live - comment it 
			$mail->Password = $email_stuff_arr["fromemail_Password"];//  live - comment it 
			$mail->SMTPSecure = 'tls';*/
			$mail->From = $email_stuff_arr["fromemail_emailtemplate"];
			$mail->FromName = $email_stuff_arr["name_emailtemplate"];
			$mail->addAddress($email);
			$mail->addReplyTo( $email_stuff_arr["fromemail_emailtemplate"], $email_stuff_arr["name_emailtemplate"]);
			$mail->WordWrap = 50;
			$mail->isHTML(true);

			$sub="Wallet to bank transfer is successfull";

			$mail->Subject = $sub;
			$mail->Body    = $usermessage;
			
			if(!$mail->send()){
				echo 'mail is not sent';
			}
		}
		/* Mail notification to the customer */

	}
	
	public function customer_wallet_bank_transfer_accounts_success(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["menu_flag"]="accounts_banktransfer_active_links";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/cust_manage/customer_wallet_bank_transfer_accounts_success',$data);
		}else{
			redirect(base_url());
		}
	}
	public function customer_bank_tranfer_accounts_success_processing(){
		
		$params = $columns = $totalRecords = $data = array();

			$params = $this->input->post();
			
			$totalRecords=$this->Model_customers->customer_bank_tranfer_accounts_success_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_customers->customer_bank_tranfer_accounts_success_processing($params,"get_recs");
			
			$i=count($queryRecordsResult);
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$request_details='<ul class="list-unstyled">';
				$request_details.='<li>'.$queryRecordsObj->name.'</li>';
				$request_details.='<li>Requested on: '.date("D j M, Y - h:i a",strtotime($queryRecordsObj->timestamp)).'</li>';
				$request_details.='<li>Request id: '.$queryRecordsObj->wallet_request_id.'</li>';
				
				$request_details.='</ul>';
				$row[]=$request_details;
				$wallet_transaction_table_by_wallet_transaction_bank_arr=$this->get_wallet_transaction_table_by_wallet_transaction_bank($queryRecordsObj->wallet_id,$queryRecordsObj->customer_id);
				$bank_details='<ul class="list-unstyled">';
				$bank_details.='<li>Bank Name : '.$queryRecordsObj->bank_name.'</li>';
				$bank_details.='<li>Branch Name : '.$queryRecordsObj->branch_name.'</li>';
				$bank_details.='<li>City : '.$queryRecordsObj->city.'</li>';
				$bank_details.='<li>IFSC Code : '.$queryRecordsObj->ifsc_code.'</li>';
				$bank_details.='<li>Account Number : '.$queryRecordsObj->account_number.'</li>';
				$bank_details.='<li>Account Holder Name : '.$queryRecordsObj->account_holder_name.'</li>';
				$bank_details.='<li>Mobile Number : '.$queryRecordsObj->mobile_number.'</li>';
				$bank_details.='</ul>';
				$row[]=$bank_details;
				
				$trans_details='';
				$trans_details='<ul class="list-unstyled">';
				$trans_details.="<li>Requested Amount:".curr_sym.$queryRecordsObj->requested_transaction_amount."</li>";
				$trans_details.="<li>Accepted Amount:".curr_sym.$queryRecordsObj->accepted_transaction_amount."</li>";
				$trans_details.="<li><br></li>";
				$trans_details.="<li>Transaction Amount:".curr_sym.$queryRecordsObj->transaction_amount."</li>";
				$trans_details.="<li>Transaction Date:".date("D j M, Y",strtotime($queryRecordsObj->transaction_date))."</li>";
				$trans_details.="<li>Transaction Time:".$queryRecordsObj->transaction_time."</li>";
				$trans_details.='</ul>';

				$row[]=$trans_details;
				$data[]=$row;
				$row=array();
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format

	}
	
	public function wallet_transaction_bank_failure(){
		$wallet_transaction_bank_id=$this->input->post("wallet_transaction_bank_id");
		$refund_fail_comments=$this->input->post("refund_fail_comments");
		$allow_update_bank_details=$this->input->post("allow_update_bank_details");
		
		echo $this->Model_customers->wallet_transaction_bank_failure($wallet_transaction_bank_id,$refund_fail_comments,$allow_update_bank_details);
	}
	
	public function customer_wallet_bank_transfer_accounts_failure(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["menu_flag"]="accounts_banktransfer_active_links";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/cust_manage/customer_wallet_bank_transfer_accounts_failure',$data);
		}else{
			redirect(base_url());
		}
	}
	public function customer_bank_tranfer_accounts_failure_processing(){
		
		$params = $columns = $totalRecords = $data = array();

			$params = $this->input->post();
			
			$totalRecords=$this->Model_customers->customer_bank_tranfer_accounts_failure_processing($params,"get_total_num_recs");
			
			$queryRecordsResult=$this->Model_customers->customer_bank_tranfer_accounts_failure_processing($params,"get_recs");
			
			$i=count($queryRecordsResult);
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$request_details='<ul class="list-unstyled">';
				$request_details.='<li>'.$queryRecordsObj->name.'</li>';
				$request_details.='<li>Requested on: '.date("D j M, Y - h:i a",strtotime($queryRecordsObj->timestamp)).'</li>';
				$request_details.='<li>Request id: '.$queryRecordsObj->wallet_request_id.'</li>';
				$request_details.="<li>Transaction Amount:".$queryRecordsObj->transaction_amount."</li>";
				$request_details.='</ul>';
				$row[]=$request_details;
				$wallet_transaction_table_by_wallet_transaction_bank_arr=$this->get_wallet_transaction_table_by_wallet_transaction_bank($queryRecordsObj->wallet_id,$queryRecordsObj->customer_id);
				$bank_details='<ul class="list-unstyled">';
				$bank_details.='<li>Bank Name : '.$queryRecordsObj->bank_name.'</li>';
				$bank_details.='<li>Branch Name : '.$queryRecordsObj->branch_name.'</li>';
				$bank_details.='<li>City : '.$queryRecordsObj->city.'</li>';
				$bank_details.='<li>IFSC Code : '.$queryRecordsObj->ifsc_code.'</li>';
				$bank_details.='<li>Account Number : '.$queryRecordsObj->account_number.'</li>';
				$bank_details.='<li>Account Holder Name : '.$queryRecordsObj->account_holder_name.'</li>';
				$bank_details.='<li>Mobile Number : '.$queryRecordsObj->mobile_number.'</li>';
				$bank_details.='</ul>';
				$row[]=$bank_details;
				
				$data[]=$row;
				$row=array();
			}
			$json_data = array(
					"draw"            => intval( $params['draw'] ),   
					"recordsTotal"    => intval( $totalRecords ),  
					"recordsFiltered" => intval($totalRecords),
					"data"            => $data   // total data array
					);
			echo json_encode($json_data);  // send data as json format

	}
	public function all_shipped_address($shipping_address_id){
		$shipping_address_arr=$this->Model_customers->all_shipped_address($shipping_address_id);
		return $shipping_address_arr;
	}
	public function returns_replacement_request_orders_accounts_success($order_item_id){
		
		$returns_replacement_request_orders_accounts_success=$this->Model_customers->returns_replacement_request_orders_accounts_success($order_item_id);
		
		return $returns_replacement_request_orders_accounts_success;
	}
	
	public function customer_profile(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data["menu_flag"]="customer_profile";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/cust_manage/customer_profile',$data);
		}else{
			redirect(base_url());
		}
	}
	
	
	
	public function customer_profile_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_customers->customer_profile_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_customers->customer_profile_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->id.'">';
				$row[]=$queryRecordsObj->profile_name;
				$row[]=$queryRecordsObj->sort_order;
				$row[]=$queryRecordsObj->view;
				
				$row[]=$queryRecordsObj -> timestamp;
				
				$row[]='<form action="'.base_url().'admin/Manage_cust/edit_customer_profile_form" method="post"><input type="hidden" value="'.$queryRecordsObj -> id.'" name="id"><input type="submit" class="btn btn-info btn-xs btn-block" value="Edit" ></form>';
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function add_customer_profile() {	
		if($this->session->userdata("logged_in")){
			$profile_name=addslashes($this->input->post("profile_name"));
			$sort_order=$this->input->post("sort_order");
			$view=$this->input->post("view");	
			$flag=$this->Model_customers->add_customer_profile($profile_name,$sort_order,$view);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function edit_customer_profile_form(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("id");
			$data["get_customer_profile_data"]=$this->Model_customers->get_customer_profile_data($id);
			$data["id"]=$id;
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/cust_manage/edit_customer_profile',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function get_customer_profile_data(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("id");
			$logistics_arr=$this->Model_customers->get_customer_profile_data($id);
			echo json_encode($logistics_arr);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function delete_customer_profile_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_customers-> delete_customer_profile_selected($selected_list);			
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function edit_customer_profile(){		
		if($this->session->userdata("logged_in")){
			$profile_name=addslashes($this->input->post("edit_profile_name"));
			$sort_order=$this->input->post("edit_sort_order");
			$view=$this->input->post("edit_view");
			$id=$this->input->post("id");
			$flag=$this->Model_customers->edit_customer_profile($profile_name,$sort_order,$view,$id);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function customer_profile_values(){
		if($this->session->userdata("logged_in")){
			$data["controller"]=$this;
			$data['customer_add_profile_name'] = $this->Model_customers->customer_add_profile_name();
			$data["menu_flag"]="customer_profile_values";
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/cust_manage/customer_profile_values',$data);
		}else{
			redirect(base_url());
		}
	}
	
	public function edit_customer_profile_values_form(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("addl_profile_name_id");
			$profile_value_id=$this->input->post("profile_value_id");
			$data['customer_add_profile_name'] = $this->Model_customers->customer_add_profile_name();
			$data["get_customer_profile_value_data"]=$this->Model_customers->get_customer_profile_value_data($profile_value_id);
			$data["profile_value_id"]=$profile_value_id;
			$data["id"]=$id;
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/cust_manage/edit_customer_profile_values',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	public function customer_profile_values_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_customers->customer_profile_values_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_customers->customer_profile_values_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->profile_value_id.'">';
				$row[]=$queryRecordsObj->profile_value;
				$row[]=$queryRecordsObj->sort_order;
				$row[]=$queryRecordsObj->view;
				
				$row[]=$queryRecordsObj -> timestamp;
				$row[]='<form action="'.base_url().'admin/Manage_cust/edit_customer_profile_values_form" method="post"><input type="hidden" value="'.$queryRecordsObj -> profile_value_id.'" name="profile_value_id"><input type="hidden" value="'.$queryRecordsObj -> addl_profile_name_id.'" name="addl_profile_name_id"><input type="submit" class="btn btn-warning" value="Edit" ></form>';
				
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function add_customer_profile_value() {	
		if($this->session->userdata("logged_in")){
			$profile_value=addslashes($this->input->post("profile_value"));
			$create_sort_order=$this->input->post("create_sort_order");
			$view=$this->input->post("view");
			$profile_name_id=$this->input->post("profile_name_id");
			$flag=$this->Model_customers->add_customer_profile_value($profile_value,$create_sort_order,$view,$profile_name_id);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function get_customer_profile_value_data(){
		if($this->session->userdata("logged_in")){
			$profile_value_id=$this->input->post("profile_value_id");
			$logistics_arr=$this->Model_customers->get_customer_profile_value_data($profile_value_id);
			echo json_encode($logistics_arr);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	public function edit_customer_profile_value(){		
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("profile_value_id");
			$edit_profile_value=addslashes($this->input->post("edit_profile_value"));
			$edit_sort_order=addslashes($this->input->post("edit_sort_order"));
			$edit_profile_name=$this->input->post("edit_profile_name");
			$view=$this->input->post("edit_view");
			
			$flag=$this->Model_customers->edit_customer_profile_value($edit_profile_value,$edit_sort_order,$id,$view,$edit_profile_name);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	
	public function delete_customer_profile_values_selected(){
		if($this->session->userdata("logged_in")){
			$selected_list=$this->input->post("selected_list");
			$flag=$this -> Model_customers-> delete_customer_profile_values_selected($selected_list);			
			if($flag==true){
				echo true;
			}else{
				echo 0;
			}	
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}	
	}
	
	public function requests_count(){
		
		if($this->session->userdata("logged_in")){
			$count=$this->Model_customers->requests_count();
			echo $count;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}

	
	public function design_request() {
		if($this->session->userdata("logged_in")){
			$data["menu_flag"]="design_request";
			//$data['customers'] = $this->Model_customers->get_all_customer();
			$this->load->view('admin/vmanage',$data);
			$this->load->view('admin/cust_manage/customer_design_request',$data);
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
    }
	public function design_request_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			
			$totalRecords=$this->Model_customers->design_request_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_customers->design_request_processing($params,"get_recs");
			$i=1;
			
			foreach($queryRecordsResult as $queryRecordsObj){
				
				$row[]=$i;
				$row[]='<b>'.$queryRecordsObj -> name.'</b><br>';
				
				$text='<b style="color:#ff5a00;">'.ucwords($queryRecordsObj -> design_type).'</b>';

				if(strtolower($queryRecordsObj -> design_type)== 'home'){
					$text.='<br> BHK type : '.$queryRecordsObj -> size_of_home.' BHK ('.$queryRecordsObj ->size_of_home_des.')';
				}
				
				if(strtolower($queryRecordsObj -> design_type)== 'modular kitchen'){
					$text.='<br> Kitchen Layout : '.$queryRecordsObj -> kitchen_layout;
					$text.='<br> Area Of Kitchen : '.$queryRecordsObj -> area_of_kitchen;
					$text.='<br> Kind Of Kitchen Top : '.$queryRecordsObj -> kitchen_top;
				}
				if(strtolower($queryRecordsObj -> design_type)== 'bedroom'){
					
					$text.='<br>Bedroom Layout : '.$queryRecordsObj ->bedroom_layout.'';
					$text.='<br>Bedroom Style : '.$queryRecordsObj ->bedroom_style.'';
					$text.='<br>Area Of Bedroom : '.$queryRecordsObj ->area_of_bedroom.'';
					$text.='<br>Bedroom Utility : '.$queryRecordsObj ->bedroom_utility.'';
				}
				
				if(strtolower($queryRecordsObj -> design_type)== 'office room'){
					
					$ol=$queryRecordsObj ->office_layout;
					$ol = preg_replace('/[,]+/', ', ', ($ol));	
					$text.='<br>Office Layout : '.$ol.'';
					$text.='<br>Office Requirement : '.$queryRecordsObj ->office_requirement.'';
					$text.='<br>Office Type : '.$queryRecordsObj ->office_type.'';
					$text.='<br>Size Of Office : '.$queryRecordsObj ->size_of_office.'';
					$text.='<br>Office Style : '.$queryRecordsObj ->office_style.'';
				}
				
				if(strtolower($queryRecordsObj -> design_type)== 'conference room'){
					
					$conf_room_layout = preg_replace('/[ ,]+/', '<br>', ($queryRecordsObj->conf_room_layout));	

					$ol=$queryRecordsObj ->conf_room_layout;
					$ol = preg_replace('/[ ,]+/', ' ', ($ol));
					$text.='<br>Type : '.$queryRecordsObj ->conf_room_type.'';
					$text.='<br>Size : '.$queryRecordsObj ->size_of_conf_room.'';	
					$text.='<br>Fitting Type : '.$queryRecordsObj ->conf_room_layout.'';
					$text.='<br>Style : '.$queryRecordsObj ->conf_room_style.'';
				}
				
				if(strtolower($queryRecordsObj -> design_type)== 'workstations' || strtolower($queryRecordsObj -> design_type)== 'work stations' ){
					//$workstations_style = preg_replace('/[ ,]+/', '<br>', ($queryRecordsObj->workstations_style));	

					$workstations_style=$queryRecordsObj ->workstations_style;
					$ol = preg_replace('/[ ,]+/', ' ', ($workstations_style));
					$text.='<br>Seating Capacity : '.$queryRecordsObj ->seating_capacity_workstations.'';
					$text.='<br>Size Of Office : '.$queryRecordsObj ->size_of_workstations.'';	
					$text.='<br>Style : '.$workstations_style.'';
					$text.='<br>Material Preference : '.$queryRecordsObj ->workstations_material.'';
				}

				
				$text.='<br>Interior Budget : '.ucfirst($queryRecordsObj -> interior_budget).'';
				
				$row[]=$text;
				$row[]='<i class="fa fa-envelope" aria-hidden="true"></i> '.$queryRecordsObj -> email.'<br><i class="fa fa-phone" aria-hidden="true"></i> '.$queryRecordsObj -> mobile.'<br><i class="fa fa-map-marker" aria-hidden="true"></i> '.$queryRecordsObj -> city .'<br>'.$queryRecordsObj -> state.'<br>'.$queryRecordsObj -> country.'<br>'.$queryRecordsObj->pincode;
				
				$row[]=date('d-m-Y H:i',strtotime($queryRecordsObj -> timestamp));

				$status='';

				if($queryRecordsObj ->status=='0'){
					$status.='<b style="color:red;">Pending Connect</b><br>';
					$status.='<button class="btn btn-warning" onclick="view_status_modal('.$queryRecordsObj -> request_id.')">Update status</button>';
				}else{
					if($queryRecordsObj ->status=='1'){
						$status.='<b class="" style="color:green;">Contacted</b> <br>';
						$status.='<b class="" style="color:green;"><small>'.($queryRecordsObj -> contacted_status).'</small></b> <br>';
						if($queryRecordsObj -> contacted_comments!=''){
							$status.='<br><div class=""><small>Comments :</small> <br>'.($queryRecordsObj -> contacted_comments).'</div> <br>';
						}
					}
				}
				
				$row[]=$status;
				
				$data[]=$row;	
				$row=array();
				$i++;
				
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
			
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
		
	}
	public function update_status_request_design(){
		
		$request_id=$this->input->post("request_id");
		$status=$this->input->post("status");
		$contacted_status=$this->input->post("contacted_status");
		$contacted_comments=$this->input->post("contacted_comments");
		
		$flag=$this->Model_customers->update_status_request_design($request_id,$status,$contacted_status,$contacted_comments);	
		echo $flag;
	}
	
	
}
