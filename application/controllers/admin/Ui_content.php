<?php
class Ui_content extends CI_controller{
    function __construct(){
		parent::__construct();
		$this->load->model('admin/Model_ui');				
		$this->load->model('admin/Model_catalogue');				
		$this->load->model('admin/Site_blog');	
		$this->load->model('admin/Site_newsletter');	
		$this->load->helper('emailsmstemplate');		
	}
	public function index(){
		if($this->session->userdata("logged_in")){
			redirect(base_url().('admin/Admin_manage')); 
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function common_page(){
		if($this->session->userdata("logged_in")){
			$data["menu_flag"]="front_end";
			$data['parent_catagories'] = $this->Model_catalogue->parent_category();		
			$this->banner_and_footer();
		}else{
			$this->logged_out();
		}	
	}
	public function banner_and_footer(){
		if($this->session->userdata("logged_in")){
			$data["menu_flag"]="front_end";		
			$data['data']=$this->Model_ui->get_ui_banner();
			$data['f_data']=$this->Model_ui->get_ui_footer();
			$data['data_obj']=$this->Model_ui->get_ui();
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/ui/content',$data);
		}else{
			$this->logged_out();
		}
	}
	public function add_banner(){
		if($this->session->userdata("logged_in")){
			$this->load->library('upload');
			$data["menu_flag"]="front_end";
			if(!is_dir('assets/data/ui')){
				mkdir('assets/data/ui');
			}
				

			$files = $_FILES;
			$background_image='';
			$background_image=$files['background_image']['name'];
			if($background_image!=""){
				$c_ext = pathinfo($background_image, PATHINFO_EXTENSION);
				$c_filen=rand(10,100000);
				
				$_FILES['background_image']['name']=$c_filen.".".$c_ext;
				$_FILES['background_image']['type']= $files['background_image']['type'];
				$_FILES['background_image']['tmp_name']= $files['background_image']['tmp_name'];
				$_FILES['background_image']['error']= $files['background_image']['error'];
				$_FILES['background_image']['size']= $files['background_image']['size'];  
				$this->upload->initialize($this->set_upload_options_config(1920,601));
				
				if (!$this -> upload -> do_upload("background_image")) {
					echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					$background_image = "assets/data/ui/". $upload_data['file_name'];
				}
			}
			
			$text=$this->db->escape_str($this->input->post("text"));
			if($text!='' && $background_image!=''){
				$flag=$this->Model_ui->add_banner($text,$background_image);
				echo $flag;
			}else{
				echo false;
			}
			
		}else{
			$this->logged_out();
		}
	}
	public function add_footer(){
		if($this->session->userdata("logged_in")){
			$this->load->library('upload');
			$data["menu_flag"]="front_end";
			if(!is_dir('assets/data/ui')){
				mkdir('assets/data/ui');
			}
				
			//print_r($_FILES);
			//exit;
			$files = $_FILES;
			$background_image='';
			$background_image=$files['background_image']['name'];
			if($background_image!=""){
				$c_ext = pathinfo($background_image, PATHINFO_EXTENSION);
				$c_filen=rand(10,100000);
				
				$_FILES['background_image']['name']=$c_filen.".".$c_ext;
				$_FILES['background_image']['type']= $files['background_image']['type'];
				$_FILES['background_image']['tmp_name']= $files['background_image']['tmp_name'];
				$_FILES['background_image']['error']= $files['background_image']['error'];
				$_FILES['background_image']['size']= $files['background_image']['size'];  
				$this->upload->initialize($this->set_upload_options_config(1920,1000));
				
				if (!$this -> upload -> do_upload("background_image")) {
					echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					$background_image = "assets/data/ui/". $upload_data['file_name'];
				}
			}
			
			
			if($background_image!=''){
				$flag=$this->Model_ui->add_footer($background_image);
				echo $flag;
			}else{
				echo false;
			}
			
		}else{
			$this->logged_out();
		}
	}
	
	public function edit_content(){
		if($this->session->userdata("logged_in")){
			$data["menu_flag"]="front_end";
			$id=$this->input->post("id");
			$type=$this->input->post("type");
			$page=$this->input->post("page");
			$data['type']=$type;
			$data['page']=$page;
			$data['data']=$this->Model_ui->get_ui_content($id,$type,$page);

			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/ui/edit_content',$data);
		}else{
			$this->logged_out();
		}	
	}
	public function update_ui_content(){
		
		if($this->session->userdata("logged_in")){
			$id=$this->db->escape_str($this->input->post("id"));
			$type=$this->input->post("type");
			$page=$this->input->post("page");
			
			$this->load->library('upload');
			$files = $_FILES;
			$background_image='';
			$background_image_name=$files['background_image']['name'];
			if($background_image_name!=""){
				$previous_image=$this->input->post("previous_image");
				if(file_exists($previous_image)) {
					unlink($previous_image);			   
				}
					
				$c_ext = pathinfo($background_image_name, PATHINFO_EXTENSION);
				$c_filen=rand(10,100000);
				
				$_FILES['background_image']['name']=$c_filen.".".$c_ext;
				$_FILES['background_image']['type']= $files['background_image']['type'];
				$_FILES['background_image']['tmp_name']= $files['background_image']['tmp_name'];
				$_FILES['background_image']['error']= $files['background_image']['error'];
				$_FILES['background_image']['size']= $files['background_image']['size'];  
				
				if($type=="banner"){
					$this->upload->initialize($this->set_upload_options_config(1920,601));
				}
				if($type=="footer"){
					$this->upload->initialize($this->set_upload_options_config(1920,1000));
				}
				
				if (!$this -> upload -> do_upload("background_image")) {
					echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					$background_image = "assets/data/ui/".$upload_data['file_name'];
				}
			}else{
				$background_image=$this->input->post("previous_image");	  
			}
	
			
			if($type=="banner"){
				$text=$this->db->escape_str($this->input->post("text"));
				if($background_image!=''){
					$flag=$this->Model_ui->update_banner($text,$background_image,$id);
					echo $flag;
				}else{
					echo false;
				}
			}
			if($type=="footer"){
				if($background_image!=''){
					$text='';
					$flag=$this->Model_ui->update_banner($text,$background_image,$id);
					echo $flag;
				}else{
					echo false;
				}
			}
	
		}else{
			$this->logged_out();
		}	
	}
	public function delete_content(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("id");
			$type=$this->input->post("type");
			$page=$this->input->post("page");
			$flag=$this->Model_ui->delete_content($id,$type,$page);
			echo $flag;
		}else{
			$this->logged_out();
		}	
	}
	public function delete_testimonial(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("id");
			$flag=$this->Model_ui->delete_testimonial($id);
			echo $flag;
		}else{
			$this->logged_out();
		}	
	}
	public function banner_category(){
		if($this->session->userdata("logged_in")){
			$data["menu_flag"]="front_end";
			$data['parent_catagory'] = $this->Model_catalogue->parent_category();
			$data['catagories'] = $this->Model_catalogue->category();		
			$data['data_obj']=$this->Model_ui->get_banner_category();
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/ui/banner_category',$data);
		}else{
			$this->logged_out();
		}
	}
	public function banner_category_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_ui->banner_category_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_ui->banner_category_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
			
				$row[]=$queryRecordsObj->cat_name;
				$row[]='<img src="'.base_url().$queryRecordsObj->background_image.'" class="img-responsive" >';

				$row[]=$queryRecordsObj -> cat_text;
				$row[]=$queryRecordsObj -> type;
				
				$row[]=$queryRecordsObj -> timestamp;
				if($queryRecordsObj->active){
				$str='<form action="'.base_url().'admin/Ui_content/edit_banner_category" method="post">	<input type="hidden" value="'.$queryRecordsObj ->id.'" name="id"><input type="hidden" value="'.$queryRecordsObj -> cat_id.'" name="cat_id"><input type="hidden" value="'.$queryRecordsObj -> pcat_id.'" name="pcat_id"><input type="hidden" value="category" name="page"><input type="hidden" value="banner" name="type"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';
				$str.='<button id="multiple_delete_button3" class="btn btn-danger btn-xs btn-block" onclick=delete_content("'.$queryRecordsObj ->id.'","banner","category")>delete</button>';
				$row[]=$str;
				}
				
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
		}else{
			$this->logged_out();
		}
	}	
	public function add_banner_category(){
		if($this->session->userdata("logged_in")){
			
			$cat_id=$this->input->post("cat_id");
			
			$val=$this->Model_ui->check_cat_exists($cat_id);
			if($val!=0){
				echo "exists";
				exit;
			}
			$this->load->library('upload');
			$data["menu_flag"]="front_end";
			if(!is_dir('assets/data/ui')){
				mkdir('assets/data/ui');
			}
				
			//print_r($_FILES);
			//exit;
			$files = $_FILES;
			$background_image='';
			$background_image=$files['background_image']['name'];
			if($background_image!=""){
				$c_ext = pathinfo($background_image, PATHINFO_EXTENSION);
				$c_filen=rand(10,100000);
				
				$_FILES['background_image']['name']=$c_filen.".".$c_ext;
				$_FILES['background_image']['type']= $files['background_image']['type'];
				$_FILES['background_image']['tmp_name']= $files['background_image']['tmp_name'];
				$_FILES['background_image']['error']= $files['background_image']['error'];
				$_FILES['background_image']['size']= $files['background_image']['size'];  
				$this->upload->initialize($this->set_upload_options_config(1920,601));
				
				if (!$this -> upload -> do_upload("background_image")) {
					echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					$background_image = "assets/data/ui/". $upload_data['file_name'];
				}
			}
			
			$cat_text=$this->db->escape_str($this->input->post("cat_text"));
			
			
			if($background_image!=''){
				$flag=$this->Model_ui->add_banner_category($cat_id,$cat_text,$background_image);
				echo $flag;
			}else{
				echo false;
			}
			
		}else{
			$this->logged_out();
		}
	}
	public function edit_banner_category(){
		
		if($this->session->userdata("logged_in")){
			$data["menu_flag"]="front_end";	
			$id=$this->input->post("id");
			$cat_id=$this->input->post("cat_id");
			$pcat_id=$this->input->post("pcat_id");
			$data["id"]=$id;
			$data["cat_id"]=$cat_id;
			$data["pcat_id"]=$pcat_id;
			$page=$this->input->post("page");
			$data['parent_catagory'] = $this->Model_catalogue->parent_category();
			$data['catagories'] = $this->Model_ui->get_category($pcat_id);	
			$data['data']=$this->Model_ui->get_banner_category_content($id,$cat_id);			
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/ui/edit_banner_category',$data);
		}else{
			$this->logged_out();
		}
		
	}
	public function update_banner_category(){
		if($this->session->userdata("logged_in")){
			$this->load->library('upload');
			$data["menu_flag"]="front_end";
			if(!is_dir('assets/data/ui')){
				mkdir('assets/data/ui');
			}			
			//print_r($_FILES);
			//exit;
			$this->load->library('upload');
			$files = $_FILES;
			$background_image='';
			$background_image_name=$files['background_image']['name'];
			if($background_image_name!=""){
				$previous_image=$this->input->post("previous_image");
				if(file_exists($previous_image)) {
					unlink($previous_image);			   
				}
					
				$c_ext = pathinfo($background_image_name, PATHINFO_EXTENSION);
				$c_filen=rand(10,100000);
				
				$_FILES['background_image']['name']=$c_filen.".".$c_ext;
				$_FILES['background_image']['type']= $files['background_image']['type'];
				$_FILES['background_image']['tmp_name']= $files['background_image']['tmp_name'];
				$_FILES['background_image']['error']= $files['background_image']['error'];
				$_FILES['background_image']['size']= $files['background_image']['size'];  

				$this->upload->initialize($this->set_upload_options_config(1920,601));

				if (!$this -> upload -> do_upload("background_image")) {
					echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					$background_image = "assets/data/ui/".$upload_data['file_name'];
				}
			}else{
				$background_image=$this->input->post("previous_image");	  
			}
			
			$cat_text=$this->db->escape_str($this->input->post("cat_text"));
			$cat_id=$this->input->post("cat_id");
			$pcat_id=$this->input->post("pcat_id");
			$id=$this->input->post("id");
			
			if($background_image!=''){
				$flag=$this->Model_ui->update_banner_category($id,$cat_id,$cat_text,$background_image);
				echo $flag;
			}else{
				echo false;
			}
			
		}else{
			$this->logged_out();
		}
	}
	
	public function testimonial(){
		if($this->session->userdata("logged_in")){
			$data["menu_flag"]="front_end";	
			$data['parent_catagory'] = $this->Model_catalogue->parent_category();
			$data['catagories'] = $this->Model_catalogue->category();		
			//$data['data_obj']=$this->Model_ui->testimonial();
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/ui/testimonial',$data);
		}else{
			$this->logged_out();
		}
	}
	public function testimonial_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_ui->testimonial_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_ui->testimonial_processing($params,"get_recs");
			
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				
				if(isset($queryRecordsObj->cat_name)){
					$row[]=($queryRecordsObj->cat_name !='') ? $queryRecordsObj->cat_name : 'Common Page';
				}else{
					$row[]='';
				}
				
				$row[]='<img src="'.base_url().$queryRecordsObj->image.'" class="img-responsive" >';

				$row[]=$queryRecordsObj -> name."<br><small>".$queryRecordsObj->designation."</small>";
				
				$content='';
				$content.='<h4>'.$queryRecordsObj->heading.'</h4>';
				if(strlen($queryRecordsObj->content)>200){
					$content.='<div class="f-row margin-top more">'.str_split($queryRecordsObj->content,200)[0].'<span class="moreellipses">... </span><span class="morecontent"><span>' .substr($queryRecordsObj->content,201). '</span> <a class="morelink cursor_pointer" onclick="morelinkfun(this)">more</a></span></div>';
				}else{
					$content.='<div class="f-row margin-top more">'.$queryRecordsObj->content.'</div><br>';
				}
				
				$content.='<h4>'.$queryRecordsObj->heading2.'</h4>';
				if(strlen($queryRecordsObj->content2)>200){
					$content.='<div class="f-row margin-top more">'.str_split($queryRecordsObj->content2,200)[0].'<span class="moreellipses">... </span><span class="morecontent"><span>' .substr($queryRecordsObj->content2,201). '</span> <a class="morelink cursor_pointer" onclick="morelinkfun(this)">more</a></span></div>';
				}else{
					$content.='<div class="f-row margin-top more">'.$queryRecordsObj->content2.'</div><br>';
				}
				$content.='<h4>'.$queryRecordsObj->heading3.'</h4>';
				if(strlen($queryRecordsObj->content3)>200){
					$content.='<div class="f-row margin-top more">'.str_split($queryRecordsObj->content3,200)[0].'<span class="moreellipses">... </span><span class="morecontent"><span>' .substr($queryRecordsObj->content3,201). '</span> <a class="morelink cursor_pointer" onclick="morelinkfun(this)">more</a></span></div>';
				}else{
					$content.='<div class="f-row margin-top more">'.$queryRecordsObj->content3.'</div><br>';
				}
				
				$row[]=$content;
				
				$row[]=$queryRecordsObj -> timestamp;
				
				$str='<form action="'.base_url().'admin/Ui_content/edit_testimonial" method="post">	<input type="hidden" value="'.$queryRecordsObj ->id.'" name="id"><input type="hidden" value="'.$queryRecordsObj -> cat_id.'" name="cat_id">';
				if(isset($queryRecordsObj->pcat_id)){
					if($queryRecordsObj->cat_id!=''){
						$str.='<input type="hidden" value="'.$queryRecordsObj -> pcat_id.'" name="pcat_id">';
					}else{
						$str.='<input type="hidden" value="" name="pcat_id">';
					}
				}else{
					$str.='<input type="hidden" value="" name="pcat_id">';
				}
				$str.='<input type="hidden" value="'.$queryRecordsObj ->page.'" name="page"><input type="submit" class="btn btn-warning btn-xs btn-block" value="Edit" ></form>';
				$str.='<button id="multiple_delete_button3" class="btn btn-danger btn-xs btn-block" onclick=delete_testimonial("'.$queryRecordsObj ->id.'")>delete</button>';
				
				$row[]=$str;
				
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
		}else{
			$this->logged_out();
		}
	}
	
	public function add_testimonial(){
		if($this->session->userdata("logged_in")){
			
			$cat_id=$this->input->post("cat_id");
			
			$heading=$this->db->escape_str($this->input->post("heading"));
			$content=$this->db->escape_str($this->input->post("content"));
			
			$heading2=$this->db->escape_str($this->input->post("heading2"));
			$content2=$this->db->escape_str($this->input->post("content2"));
			
			$heading3=$this->db->escape_str($this->input->post("heading3"));
			$content3=$this->db->escape_str($this->input->post("content3"));
			
			$name=$this->db->escape_str($this->input->post("name"));
			$designation=$this->db->escape_str($this->input->post("designation"));
			
			if($cat_id!=""){
				$page="category";
			}else{
				$page="common";
			}
			
			$this->load->library('upload');
			$data["menu_flag"]="front_end";
			if(!is_dir('assets/data/testimonial')){
				mkdir('assets/data/testimonial');
			}
			$files = $_FILES;
			$image='';
			$image=$files['image']['name'];
			if($image!=""){
				$c_ext = pathinfo($image, PATHINFO_EXTENSION);
				$c_filen=rand(10,100000);
				
				$_FILES['image']['name']=$c_filen.".".$c_ext;
				$_FILES['image']['type']= $files['image']['type'];
				$_FILES['image']['tmp_name']= $files['image']['tmp_name'];
				$_FILES['image']['error']= $files['image']['error'];
				$_FILES['image']['size']= $files['image']['size'];  
				$this->upload->initialize($this->set_upload_options_config(120,120,"testimonial"));
				
				if (!$this -> upload -> do_upload("image")) {
					echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					$image = "assets/data/testimonial/". $upload_data['file_name'];
				}
			}
			

			if($image!='' && $name!='' && $designation!=''){
				$flag=$this->Model_ui->add_testimonial($cat_id,$image,$name,$designation,$heading,$content,$heading2,$content2,$heading3,$content3,$page);
				echo $flag;
			}else{
				echo false;
			}
			
		}else{
			$this->logged_out();
		}
	}
	public function edit_testimonial(){
		
		if($this->session->userdata("logged_in")){
			$data["menu_flag"]="front_end";	
			$id=$this->input->post("id");
			$cat_id=$this->input->post("cat_id");
			$pcat_id=$this->input->post("pcat_id");
			$page=$this->input->post("page");
			$data["id"]=$id;
			$data["cat_id"]=$cat_id;
			$data["pcat_id"]=$pcat_id;
			
			if($page=="category" && $pcat_id!=''){
				$data['catagories'] = $this->Model_ui->get_category($pcat_id);	
			}
			
			$data['parent_catagory'] = $this->Model_catalogue->parent_category();
			$data['data']=$this->Model_ui->get_testimonial($id);			
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/ui/edit_testimonial',$data);
		}else{
			$this->logged_out();
		}
		
	}
	public function update_testimonial(){
		if($this->session->userdata("logged_in")){
			
			$id=$this->input->post("id");
			$cat_id=$this->input->post("cat_id");
			
			$heading=$this->db->escape_str($this->input->post("heading"));
			$content=$this->db->escape_str($this->input->post("content"));
			
			$heading2=$this->db->escape_str($this->input->post("heading2"));
			$content2=$this->db->escape_str($this->input->post("content2"));
			
			$heading3=$this->db->escape_str($this->input->post("heading3"));
			$content3=$this->db->escape_str($this->input->post("content3"));
			
			$name=$this->db->escape_str($this->input->post("name"));
			$designation=$this->db->escape_str($this->input->post("designation"));
			
			if($cat_id!=""){
				$page="category";
			}else{
				$page="common";
			}
			
			$this->load->library('upload');
			$data["menu_flag"]="front_end";
			if(!is_dir('assets/data/testimonial')){
				mkdir('assets/data/testimonial');
			}
			$files = $_FILES;
			$image='';
			$image=$files['image']['name'];
			if($image!=""){
				$previous_image=$this->input->post("previous_image");
				if(file_exists($previous_image)) {
					unlink($previous_image);			   
				}
				
				$c_ext = pathinfo($image, PATHINFO_EXTENSION);
				$c_filen=rand(10,100000);
				
				$_FILES['image']['name']=$c_filen.".".$c_ext;
				$_FILES['image']['type']= $files['image']['type'];
				$_FILES['image']['tmp_name']= $files['image']['tmp_name'];
				$_FILES['image']['error']= $files['image']['error'];
				$_FILES['image']['size']= $files['image']['size'];  
				$this->upload->initialize($this->set_upload_options_config(120,120,"testimonial"));
				
				if (!$this -> upload -> do_upload("image")) {
					echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					$image = "assets/data/testimonial/". $upload_data['file_name'];
				}
			}else{
				$image=$this->input->post("previous_image");	  
			}
		
			if($image!='' && $name!='' && $designation!=''){
				$flag=$this->Model_ui->update_testimonial($id,$cat_id,$image,$name,$designation,$heading,$content,$heading2,$content2,$heading3,$content3,$page);
				echo $flag;
			}else{
				echo false;
			}
			
		}else{
			$this->logged_out();
		}
	}
	public function site_description(){
		if($this->session->userdata("logged_in")){
			$data["menu_flag"]="front_end";
			$data["data_obj"]=$this->Model_ui->site_description();
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/ui/site_description',$data);
		}else{
			$this->logged_out();
		}
	}
	public function add_site_description(){
		if($this->session->userdata("logged_in")){
			$title=$this->input->post("title");
			$description=$this->db->escape_str($this->input->post("description"));
			$view_order=$this->input->post("view_order");
			$active=$this->input->post("active");
			$this->load->library('upload');
			
			if(!is_dir('assets/data/content_bg')){
				mkdir('assets/data/content_bg');
			}
			$files = $_FILES;
			$background_image='';
			$background_image=$files['background_image']['name'];
			if($background_image!=""){
				
				$c_ext = pathinfo($background_image, PATHINFO_EXTENSION);
				$c_filen=rand(10,100000);
				
				$_FILES['background_image']['name']=$c_filen.".".$c_ext;
				$_FILES['background_image']['type']= $files['background_image']['type'];
				$_FILES['background_image']['tmp_name']= $files['background_image']['tmp_name'];
				$_FILES['background_image']['error']= $files['background_image']['error'];
				$_FILES['background_image']['size']= $files['background_image']['size'];  
				$this->upload->initialize($this->set_upload_options_config(400,400,"content_bg"));
				
				if (!$this -> upload -> do_upload("background_image")) {
					echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					$background_image = "assets/data/content_bg/". $upload_data['file_name'];
				}
			}
			

			if($title!='' && $description!=''){
				$flag=$this->Model_ui->add_site_description($title,$description,$background_image,$view_order,$active);
				echo $flag;
			}else{
				echo false;
			}
			
		}else{
			$this->logged_out();
		}
	}
	public function edit_site_description(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("id");
			$data["menu_flag"]="front_end";
			$data["data"]=$this->Model_ui->get_site_description($id);
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/ui/edit_site_description',$data);
		}else{
			$this->logged_out();
		}
		
	}
	public function update_site_description(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("id");
			$title=$this->input->post("title");
			$description=$this->db->escape_str($this->input->post("description"));
			$view_order=$this->input->post("view_order");
			$active=$this->input->post("active");
			
			$this->load->library('upload');
			$files = $_FILES;
			$background_image='';
			$background_image=$files['background_image']['name'];
			if($background_image!=""){
				$previous_image=$this->input->post("previous_image");
				if(file_exists($previous_image)) {
					unlink($previous_image);			   
				}
				$c_ext = pathinfo($background_image, PATHINFO_EXTENSION);
				$c_filen=rand(10,100000);
				
				$_FILES['background_image']['name']=$c_filen.".".$c_ext;
				$_FILES['background_image']['type']= $files['background_image']['type'];
				$_FILES['background_image']['tmp_name']= $files['background_image']['tmp_name'];
				$_FILES['background_image']['error']= $files['background_image']['error'];
				$_FILES['background_image']['size']= $files['background_image']['size'];  
				$this->upload->initialize($this->set_upload_options_config(400,400,"content_bg"));
				
				if (!$this -> upload -> do_upload("background_image")) {
					echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					$background_image = "assets/data/content_bg/". $upload_data['file_name'];
				}
			}else{
				$background_image=$this->input->post("previous_image");	  
			}
			
			
			$flag=$this->Model_ui->update_site_description($id,$title,$description,$background_image,$view_order,$active);
			echo $flag;
			
			
		}else{
			$this->logged_out();
		}
	}
	public function delete_site_des(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post("id");
			$flag=$this->Model_ui->delete_site_des($id);
			echo $flag;
		}else{
			$this->logged_out();
		}	
	}
	private function set_upload_options_config($width,$height,$path=''){
		$config = array();
		if($path=="testimonial"){
			$config['upload_path'] = './assets/data/testimonial';
		}elseif($path=="content_bg"){
			$config['upload_path'] = './assets/data/content_bg';
		}elseif($path=="shopbycategory"){
			$config['upload_path'] = './assets/data/shopbycategory_images';
		}elseif($path=="shopbyparentcategory"){
			$config['upload_path'] = './assets/data/shopbyparentcategory_images';
		}elseif($path=="shopbybrand"){
			$config['upload_path'] = './assets/data/shopbybrand_images';
		}else{
			$config['upload_path'] = './assets/data/ui';
		}
		
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '1048576';
		$config['max_width'] = $width;
		$config['max_height'] = $height;
		$config['overwrite']     = FALSE;

		return $config;
	}
	public function assign_inventory(){
		if($this->session->userdata("logged_in")){
			$data["menu_flag"]="front_end";
			$data['parent_catagory'] = $this->Model_catalogue->parent_category();
			$data['all_brand_names'] = $this->Model_catalogue->get_all_brand_names();
			$data["data_obj"]=$this->Model_ui->assign_inventory();
			$this -> load -> view('admin/vmanage',$data);
			$this -> load -> view('admin/ui/assign_inventory',$data);
		}else{
			$this->logged_out();
		}		
	}
	public function assign_inv_processing(){
		if($this->session->userdata("logged_in")){
			$params = $columns = $totalRecords = $data = array();
			$params = $this->input->post();
			$totalRecords=$this->Model_ui->assign_inv_processing($params,"get_total_num_recs");	
			$queryRecordsResult=$this->Model_ui->assign_inv_processing($params,"get_recs");
			$section_name_in_page =$params['section_name_in_page'];
			foreach($queryRecordsResult as $queryRecordsObj){
				$row=array();
				$row[]='<input type="checkbox" name="selected_list" value="'.$queryRecordsObj->i_id.'" />';
				
				if($section_name_in_page=="Shop by Category"){
					$ht='<img src="'.base_url().$queryRecordsObj->image_shopbycategory.'" class="img-responsive" width="100" height="122"><br>';
					$row[]=$ht;
				}
				else if($section_name_in_page=="Shop by Parent Category"){
					$ht='<img src="'.base_url().$queryRecordsObj->image_shopbyparentcategory.'" class="img-responsive" width="100" height="122"><br>';
					$row[]=$ht;
				}
				else if($section_name_in_page=="Shop by Brand"){
					$ht='<img src="'.base_url().$queryRecordsObj->image_shopbybrand.'" class="img-responsive" width="100" height="122"><br>';
					$row[]=$ht;
				}
				else{
					$ht='<img src="'.base_url().$queryRecordsObj->thumbnail.'" class="img-responsive" ><br>';
					$ht.="<small>".$queryRecordsObj->attribute_1.' : '.$queryRecordsObj->attribute_1_value.'<br>';
					$ht.=$queryRecordsObj->attribute_2.' : '.$queryRecordsObj->attribute_2_value.'<br><small>';
					
					$row[]=$ht;
				}
				
				
				
				
				$vo=$queryRecordsObj ->view_order.'<a class="cursor_pointer" data-toggle="collapse" data-target="#update_view_order_'.$queryRecordsObj->i_id.'"> <u><i class="fa fa-pencil-square-o" aria-hidden="true"></i></u></a><br>';
				
				$vo.='<div class="f-row collapse" id="update_view_order_'.$queryRecordsObj->i_id.'"><input type="text" value="'.$queryRecordsObj ->view_order.'" name="view_order" id="view_order_'.$queryRecordsObj->i_id.'" class="form-control"><input type="button" class="btn btn-success btn-sm" value="submit" onclick="update_view_order('.$queryRecordsObj->i_id.','.$queryRecordsObj ->view_order.',\''.$section_name_in_page.'\')"></div>';
				$row[]=$vo;
				if($section_name_in_page=="Shop by Category"){
					$brand_id_list=$queryRecordsObj ->brand_id_list;
					$brand_name_arr=[];
					if($brand_id_list!=""){
						$brand_id_list_arr=explode(",",$brand_id_list);
						foreach($brand_id_list_arr as $brand_id){
							$brand_name_arr[]=$this->Model_ui->get_brand_name($brand_id);
						}
						$row[]=implode(", ",$brand_name_arr)." / -";
					}
					else{
						$row[]="- / -";
					}
				}
				else if($section_name_in_page=="Shop by Parent Category"){
					$row[]="- / -";
				}
				else if($section_name_in_page=="Shop by Brand"){
					$row[]="- / -";
				}
				else{
					$brand_name=$this->Model_ui->get_brand_name($queryRecordsObj->brand_id);
					$row[]=$brand_name." / ".$queryRecordsObj ->product_name;
				}
				
				if($section_name_in_page=="Shop by Parent Category"){
					
					$cat_id_list=$queryRecordsObj ->cat_id_list;
					$cat_name_arr=[];
					if($cat_id_list!=""){
						$cat_id_list_arr=explode(",",$cat_id_list);
						foreach($cat_id_list_arr as $cat_id){
							$cat_name_arr[]=$this->Model_ui->get_category_name($cat_id);
						}
						$row[]=$queryRecordsObj ->pcat_name." / ".implode(", ",$cat_name_arr);
					}
					else{
						$row[]=$queryRecordsObj ->pcat_name." / -";
					}
					
					
				}
				else if($section_name_in_page=="Shop by Brand"){
					$row[]="- / -";
				}
				else{
					$row[]=$queryRecordsObj ->cat_name;
				}
				
				$row[]=($queryRecordsObj ->page=="common") ? "index" : $queryRecordsObj ->page;

				//$row[]=$queryRecordsObj -> content;
				
				$row[]=$queryRecordsObj -> timestamp;	
			
				$str='<button id="multiple_delete_button3" class="btn btn-danger btn-xs btn-block" onclick=delete_assigned_inv("'.$queryRecordsObj ->id.'")>delete</button>';
				
				//$row[]=$str;
				
				$data[]=$row;	
			}
			$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data
						);
			echo json_encode($json_data);
		}else{
			$this->logged_out();
		}
	}
	public function add_assign_inventory(){
		if($this->session->userdata("logged_in")){
			$product_id=$this->input->post("products");
			$cat_id=$this->input->post("categories");
			$inventory=$this->input->post("inventory");
			$page=$this->input->post("page");
			$section_name_in_page=$this->input->post("section_name_in_page");
			$flag=$this->Model_ui->add_assign_inventory($product_id,$cat_id,$inventory,$page,$section_name_in_page);
			echo $flag;
		}else{
			$this->logged_out();
		}
	}
	public function update_view_order(){
		
		if($this->session->userdata("logged_in")){
			$view_order=$this->input->post("view_order");
			$id=$this->input->post("id");
			$section_name_in_page=$this->input->post("section_name_in_page");
			
			$flag=$this->Model_ui->update_view_order($id,$view_order,$section_name_in_page);
			echo $flag;
		}else{
			$this->logged_out();
		}
	}
	public function delete_assigned_inventory(){
		if($this->session->userdata("logged_in")){	
			$selected_list=$this->input->post("selected_list");
			$section_name_in_page=$this->input->post("section_name_in_page");
			$flag=$this -> Model_ui->delete_assigned_inventory($selected_list,$section_name_in_page);
			echo $flag;
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	public function show_available_inventory(){
		if($this->session->userdata("logged_in")){	
			$product_id=$this->input->post("product_id");
			$page=$this->input->post("page");
			$result_inventory=$this->Model_ui->show_available_inventory($product_id,$page);
			if(count($result_inventory)!=0){
				?>
				<?php
				foreach($result_inventory as $res){
				?>
					<option value="<?php echo $res->id;?>"><?php echo $res->sku_id;?></option>
					<?php
				}
			}
			else{
				echo count($result_inventory);
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	/* shopbycategory starts */
	public function show_available_brands_under_category(){
		if($this->session->userdata("logged_in")){	
			$category_id=$this->input->post("category_id");
			$result_brand=$this->Model_ui->show_available_brands_under_category($category_id);
			if(count($result_brand)!=0){
				?>
				<?php
				foreach($result_brand as $res){
				?>
					<option value="<?php echo $res->brand_id;?>"><?php echo $res->brand_name;?></option>
					<?php
				}
			}
			else{
				echo count($result_brand);
			}
		}
		else{
			$this->load->view('admin/header');
			$this->load->view('admin/login');
		}
	}
	
	
	public function add_assign_brand_shopbycategory(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("parent_category_shopbycategory");
			$cat_id=$this->input->post("categories_shopbycategory");
			$brand=$this->input->post("brand_shopbycategory");
			$page=$this->input->post("page_shopbycategory");
			$legend1=$this->input->post("legend1_shopbycategory");
			$legend2=$this->input->post("legend2_shopbycategory");
			$legend3=$this->input->post("legend3_shopbycategory");
			$section_name_in_page="Shop by Category";
			
			
			$this->load->library('upload');
			$files = $_FILES;
			
			
			//generate a $thread_id
			$date = new DateTime();
			$mt=microtime();
			$salt="thread_id";
			$rand=mt_rand(1, 1000000);
			$ts=$date->getTimestamp();
			$str=$salt.$rand.$ts.$mt;
			$c_filen=md5($str);
			if(!is_dir('assets/data/shopbycategory_images')){
				mkdir('assets/data/shopbycategory_images');
			}
			
			
			
			$image_shopbycategory='';
			$common_image_name=$files['image_shopbycategory']['name'];
			if($common_image_name!=""){
				$c_ext = pathinfo($common_image_name, PATHINFO_EXTENSION);
				
				
				$_FILES['image_shopbycategory']['name']=$c_filen.".".$c_ext;
				$_FILES['image_shopbycategory']['type']= $files['image_shopbycategory']['type'];
				$_FILES['image_shopbycategory']['tmp_name']= $files['image_shopbycategory']['tmp_name'];
				$_FILES['image_shopbycategory']['error']= $files['image_shopbycategory']['error'];
				$_FILES['image_shopbycategory']['size']= $files['image_shopbycategory']['size'];  
				$this->upload->initialize($this->set_upload_options_config(300,366,"shopbycategory"));
				
				if (!$this -> upload -> do_upload("image_shopbycategory")) {
					echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					print_r($upload_data);
					$image_shopbycategory = "assets/data/shopbycategory_images/". $upload_data['file_name'];
				}
			}
			
			
			$flag=$this->Model_ui->add_assign_brand_shopbycategory($pcat_id,$cat_id,$brand,$page,$section_name_in_page,$legend1,$legend2,$legend3,$image_shopbycategory);
			echo $flag;
		}else{
			$this->logged_out();
		}
	}
	
	//* shopbycategory ends */
	
	
	
	
	
	/* shopbyparentcategory starts */
	
	
	public function add_assign_brand_shopbyparentcategory(){
		if($this->session->userdata("logged_in")){
			$pcat_id=$this->input->post("parent_parentcategory_shopbyparentcategory");
			$cat_id=$this->input->post("categories_shopbyparentcategory");
			
			$page=$this->input->post("page_shopbyparentcategory");
			$legend1=$this->input->post("legend1_shopbyparentcategory");
			$legend2=$this->input->post("legend2_shopbyparentcategory");
			$legend3=$this->input->post("legend3_shopbyparentcategory");
			$section_name_in_page="Shop by Parent Category";
			
			
			$this->load->library('upload');
			$files = $_FILES;
			
			
			//generate a $thread_id
			$date = new DateTime();
			$mt=microtime();
			$salt="thread_id";
			$rand=mt_rand(1, 1000000);
			$ts=$date->getTimestamp();
			$str=$salt.$rand.$ts.$mt;
			$c_filen=md5($str);
			if(!is_dir('assets/data/shopbyparentcategory_images')){
				mkdir('assets/data/shopbyparentcategory_images');
			}
			
			
			
			$image_shopbyparentcategory='';
			$common_image_name=$files['image_shopbyparentcategory']['name'];
			if($common_image_name!=""){
				$c_ext = pathinfo($common_image_name, PATHINFO_EXTENSION);
				
				
				$_FILES['image_shopbyparentcategory']['name']=$c_filen.".".$c_ext;
				$_FILES['image_shopbyparentcategory']['type']= $files['image_shopbyparentcategory']['type'];
				$_FILES['image_shopbyparentcategory']['tmp_name']= $files['image_shopbyparentcategory']['tmp_name'];
				$_FILES['image_shopbyparentcategory']['error']= $files['image_shopbyparentcategory']['error'];
				$_FILES['image_shopbyparentcategory']['size']= $files['image_shopbyparentcategory']['size'];  
				$this->upload->initialize($this->set_upload_options_config(300,366,"shopbyparentcategory"));
				
				if (!$this -> upload -> do_upload("image_shopbyparentcategory")) {
					echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					print_r($upload_data);
					$image_shopbyparentcategory = "assets/data/shopbyparentcategory_images/". $upload_data['file_name'];
				}
			}
			
			
			$flag=$this->Model_ui->add_assign_brand_shopbyparentcategory($pcat_id,$cat_id,$page,$section_name_in_page,$legend1,$legend2,$legend3,$image_shopbyparentcategory);
			echo $flag;
		}else{
			$this->logged_out();
		}
	}
	
	//* shopbyparentcategory ends */
	
	
	
	
	/* shopbybrand starts */
	
	
	public function add_assign_brand_shopbybrand(){
		if($this->session->userdata("logged_in")){
			$brand_name=$this->input->post("brands_shopbybrand");
			
			
			$page=$this->input->post("page_shopbybrand");
			$legend1=$this->input->post("legend1_shopbybrand");
			$legend2=$this->input->post("legend2_shopbybrand");
			$legend3=$this->input->post("legend3_shopbybrand");
			$section_name_in_page="Shop by Brand";
			
			
			$this->load->library('upload');
			$files = $_FILES;
			
			
			//generate a $thread_id
			$date = new DateTime();
			$mt=microtime();
			$salt="thread_id";
			$rand=mt_rand(1, 1000000);
			$ts=$date->getTimestamp();
			$str=$salt.$rand.$ts.$mt;
			$c_filen=md5($str);
			if(!is_dir('assets/data/shopbybrand_images')){
				mkdir('assets/data/shopbybrand_images');
			}
			
			
			
			$image_shopbybrand='';
			$common_image_name=$files['image_shopbybrand']['name'];
			if($common_image_name!=""){
				$c_ext = pathinfo($common_image_name, PATHINFO_EXTENSION);
				
				
				$_FILES['image_shopbybrand']['name']=$c_filen.".".$c_ext;
				$_FILES['image_shopbybrand']['type']= $files['image_shopbybrand']['type'];
				$_FILES['image_shopbybrand']['tmp_name']= $files['image_shopbybrand']['tmp_name'];
				$_FILES['image_shopbybrand']['error']= $files['image_shopbybrand']['error'];
				$_FILES['image_shopbybrand']['size']= $files['image_shopbybrand']['size'];  
				$this->upload->initialize($this->set_upload_options_config(300,366,"shopbybrand"));
				
				if (!$this -> upload -> do_upload("image_shopbybrand")) {
					echo $this -> upload -> display_errors();exit;
				} else {
					$upload_data = $this -> upload -> data();
					print_r($upload_data);
					$image_shopbybrand = "assets/data/shopbybrand_images/". $upload_data['file_name'];
				}
			}
			
			
			$flag=$this->Model_ui->add_assign_brand_shopbybrand($brand_name,$page,$section_name_in_page,$legend1,$legend2,$legend3,$image_shopbybrand);
			echo $flag;
		}else{
			$this->logged_out();
		}
	}
	
	//* shopbybrand ends */
	
	
	
		    /* admin blogs */
	
	public function publish($slug){
		if($this->session->userdata("logged_in")){
			$updated=$this->Site_blog->publish($slug);
			if($updated){
                    $this->session->set_flashdata('info','Blog Published');
                    redirect(base_url()."admin/Ui_content/all_blogs_published");
                }
		}else{
			$this->logged_out();
		}
	}
	public function request_unpublish_cancel($slug){
		if($this->session->userdata("logged_in")){
			$updated=$this->Site_blog->request_unpublish_cancel($slug);
			if($updated){
                    $this->session->set_flashdata('info','Request Rejected');
                    redirect(base_url()."admin/Ui_content/all_blogs_reqest_unpublish");
                }else{
				
				}
		}else{
			$this->logged_out();
		}
	}
	public function request_unpublish_accept($slug){
		if($this->session->userdata("logged_in")){
			$updated=$this->Site_blog->request_unpublish_accept($slug);
			if($updated){
                    $this->session->set_flashdata('info','Request Accepted');
                    redirect(base_url()."admin/Ui_content/all_blogs_reqest_unpublish");
                }
		}else{
			$this->logged_out();
		}
	}
	public function unpublish($slug){
		if($this->session->userdata("logged_in")){
			$updated=$this->Site_blog->unpublish($slug);
			if($updated){
                    $this->session->set_flashdata('info','Blog Published');
                    redirect(base_url()."admin/Ui_content/all_blogs_published");
                }
		}else{
			$this->logged_out();
		}
	}
	public function deleteComment(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post('msg_id');
			$updated=$this->Site_blog->deleteComment($id);
			if($updated){
                    http_response_code(200);
				echo 'true';
                }else{
				http_response_code(400);
			}
		}else{
			$this->logged_out();
		}
	}
	public function publishComment(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post('ids');
			$updated=$this->Site_blog->publishComment($id);
			if($updated){
                    http_response_code(200);
                }else{
				http_response_code(400);
			}
		}else{
			$this->logged_out();
		}
	}
	public function unPublishComment(){
		if($this->session->userdata("logged_in")){
			$id=$this->input->post('ids');
			$updated=$this->Site_blog->unPublishComment($id);
			if($updated){
                    http_response_code(200);
                }else{
				http_response_code(400);
			}
		}else{
			$this->logged_out();
		}
	}
	
    public function  all_blogs_un_published_comments_processing(){
        if($this->session->userdata("logged_in")){
            $params = $columns = $totalRecords = $data = array();
            
            $params = $this->input->post();
            
            $totalRecords=$this->Site_blog->all_blogs_un_published_comments_processing($params,"get_total_num_recs");
            
            $queryRecordsResult=$this->Site_blog->all_blogs_un_published_comments_processing($params,"get_recs");
            
            $i=1;
            foreach($queryRecordsResult as $queryRecordsObj){
 
                $strs=strlen($queryRecordsObj->message) > 50 ? substr($queryRecordsObj->message,0,50)."..." : $queryRecordsObj->message;
                $customer_id=$this->session->userdata("customer_id");
                $row=array();
                $row[]=$i;
                $row[]='<a target="_blank" href="'.base_url().'get_blog_post/'.$queryRecordsObj->slug.'">'.$queryRecordsObj->blog_title.'</a>';
                $row[]='<span messages="'.$queryRecordsObj->message.'" onclick="showFullMessage(this)" style="cursor:pointer">'.$strs.'</span>';
                $row[]=$queryRecordsObj->customer_name;
				$row[]=$queryRecordsObj->timestamp;
                $row[]='<i class="fa fa-trash" msg_id="'.$queryRecordsObj->id.'" onclick="deleteComment(this)" style="cursor:pointer"></i> &nbsp; <i class="fa fa-check-circle-o" msg_id="'.$queryRecordsObj->id.'" onclick="publishComment(this)" style="cursor:pointer"></i>';
				
                $data[]=$row;
                $row=array();
                $i++;
            }
            $json_data = array(
                    "draw"            => intval( $params['draw'] ),   
                    "recordsTotal"    => intval( $totalRecords ),  
                    "recordsFiltered" => intval($totalRecords),
                    "data"            => $data   // total data array
                    );
            echo json_encode($json_data);  // send data as json format
            
        }else{
            $this->logged_out();
        }   
    }
    
    public function  unpublished_comments(){
        if($this->session->userdata("logged_in")){
            $data["menu_flag"]="blog";
            $data['parent_catagories'] = $this->Model_catalogue->parent_category();     
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/ui/un_published_comments',$data);
        }else{
            $this->logged_out();
        }   
    }
    
    public function  all_blogs_published_comments_processing(){
         if($this->session->userdata("logged_in")){
            $params = $columns = $totalRecords = $data = array();
            
            $params = $this->input->post();
            
            $totalRecords=$this->Site_blog->all_blogs_published_comments_processing($params,"get_total_num_recs");
            
            $queryRecordsResult=$this->Site_blog->all_blogs_published_comments_processing($params,"get_recs");
            
            $i=1;
            foreach($queryRecordsResult as $queryRecordsObj){
 
                $strs=strlen($queryRecordsObj->message) > 50 ? substr($queryRecordsObj->message,0,50)."..." : $queryRecordsObj->message;
                $customer_id=$this->session->userdata("customer_id");
                $row=array();
                $row[]=$i;
                $row[]='<a target="_blank" href="'.base_url().'get_blog_post/'.$queryRecordsObj->slug.'">'.$queryRecordsObj->blog_title.'</a>';
                $row[]='<span messages="'.$queryRecordsObj->message.'" onclick="showFullMessage(this)" style="cursor:pointer">'.$strs.'</span>';
                $row[]=$queryRecordsObj->customer_name;
				$row[]=$queryRecordsObj->timestamp;
                $row[]='<i class="fa fa-trash" msg_id="'.$queryRecordsObj->id.'" onclick="deleteComment(this)" style="cursor:pointer"></i> &nbsp; <i class="fa fa-times-circle-o" msg_id="'.$queryRecordsObj->id.'" onclick="unPublishComment(this)" style="cursor:pointer"></i>';
				
                $data[]=$row;
                $row=array();
                $i++;
            }
            $json_data = array(
                    "draw"            => intval( $params['draw'] ),   
                    "recordsTotal"    => intval( $totalRecords ),  
                    "recordsFiltered" => intval($totalRecords),
                    "data"            => $data   // total data array
                    );
            echo json_encode($json_data);  // send data as json format
            
        }else{
            $this->logged_out();
        } 
    }
    
    public function  published_comments(){
        if($this->session->userdata("logged_in")){
            $data["menu_flag"]="blog";
            $data['parent_catagories'] = $this->Model_catalogue->parent_category();     
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/ui/published_comments',$data);
        }else{
            $this->logged_out();
        }   
    }
    
    public function  all_blogs_published_processing(){
        if($this->session->userdata("logged_in")){
            $params = $columns = $totalRecords = $data = array();
            
            $params = $this->input->post();
            
            $totalRecords=$this->Site_blog->all_blogs_published_processing($params,"get_total_num_recs");
            
            $queryRecordsResult=$this->Site_blog->all_blogs_published_processing($params,"get_recs");
            
            $i=1;
            foreach($queryRecordsResult as $queryRecordsObj){
                $status="";
                $writingstatus="";
                if($queryRecordsObj->blogger_request_publish==0 && $queryRecordsObj->admin_publish==0){
                    $writingstatus='<small style="color:#0000ff">Draft </small>';
                }
                if($queryRecordsObj->blogger_request_publish==1 && $queryRecordsObj->admin_publish==0){
                    $writingstatus='<small style="color:#0000ff">Sent to Admin for Publishing</small>';
                }
                $publishstatus="";
                if($queryRecordsObj->blogger_request_publish==1 && $queryRecordsObj->admin_publish==1){
                    $publishstatus='<small style="color:#0000ff">Published by Admin</small>';
                    $writingstatus="";
                }
                $unpublishstatus="";
                if($queryRecordsObj->blogger_request_unpublish==1 && $queryRecordsObj->admin_publish==1){
                    $unpublishstatus='<small style="color:#0000ff">Unpublish request sent to Admin</small>';
                    $publishstatus="";
                    $writingstatus="";
                }
                

                
                if($writingstatus!=""){
                    $status=$writingstatus;
                }
                if($publishstatus!=""){
                    $status=$publishstatus;
                }
                if($unpublishstatus!=""){
                    $status=$unpublishstatus;
                }
                

                
                $customer_id=$this->session->userdata("customer_id");
                $row=array();
                $row[]=$i;
                $row[]=$queryRecordsObj->blog_title;
                $row[]=$queryRecordsObj->topic_sel;
                $row[]=$queryRecordsObj->timestamp;
                $row[]=$status;
                $row[]='<a target="_blank" href="'.base_url().'get_blog_post_preview/'.$queryRecordsObj->slug.'/'.$queryRecordsObj->customer_id.'"><i class="fa fa-eye"></i></a> <a href="'.base_url().'admin/Ui_content/unpublish/'.$queryRecordsObj->slug.'"><small class="label label-warning">Unpublish</small></a>';
				$upload_image='<button class="btn btn-success btn-xs" type="button" onclick="uploadBlogImage(\''.$queryRecordsObj->id.'\')">Upload</button>';
				if($queryRecordsObj->blog_image!=""){
					$upload_image.='<img src="'.base_url().$queryRecordsObj->blog_image.'" class="img-responsive" width="50" height="50">';
				}
				$row[]=$upload_image;
				
				if($queryRecordsObj->post_to_frontend=='1'){
					$row[]='<input type="checkbox" onclick="post_to_frontend_fun(this,\''.$queryRecordsObj->id.'\')" checked>';
				}
				else{
					$row[]='<input type="checkbox" onclick="post_to_frontend_fun(this,\''.$queryRecordsObj->id.'\')">';
				}
                $data[]=$row;
                $row=array();
                $i++;
            }
            $json_data = array(
                    "draw"            => intval( $params['draw'] ),   
                    "recordsTotal"    => intval( $totalRecords ),  
                    "recordsFiltered" => intval($totalRecords),
                    "data"            => $data   // total data array
                    );
            echo json_encode($json_data);  // send data as json format
            
        }else{
            $this->logged_out();
        }   
    }
    
    public function  all_blogs_published(){
        if($this->session->userdata("logged_in")){
            $data["menu_flag"]="blog";
            $data['parent_catagories'] = $this->Model_catalogue->parent_category();     
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/ui/all_blogs_published',$data);
        }else{
            $this->logged_out();
        }   
    }
   
   
    public function  all_blogs_unpublished_processing(){
        if($this->session->userdata("logged_in")){
            $params = $columns = $totalRecords = $data = array();
            $params = $this->input->post();
            
            $totalRecords=$this->Site_blog->all_blogs_unpublished_processing($params,"get_total_num_recs");
            
            $queryRecordsResult=$this->Site_blog->all_blogs_unpublished_processing($params,"get_recs");
            
            $i=1;
            foreach($queryRecordsResult as $queryRecordsObj){
                $status="";
                $writingstatus="";
                if($queryRecordsObj->blogger_request_publish==0 && $queryRecordsObj->admin_publish==0){
                    $writingstatus='<small style="color:#0000ff">Draft </small>';
                }
                if($queryRecordsObj->blogger_request_publish==1 && $queryRecordsObj->admin_publish==0){
                    $writingstatus='<small style="color:#0000ff">Sent to Admin for Publishing</small>';
                }
                $publishstatus="";
                if($queryRecordsObj->blogger_request_publish==1 && $queryRecordsObj->admin_publish==1){
                    $publishstatus='<small style="color:#0000ff">Published by Admin</small>';
                    $writingstatus="";
                }
                $unpublishstatus="";
                if($queryRecordsObj->blogger_request_unpublish==1 && $queryRecordsObj->admin_publish==1){
                    $unpublishstatus='<small style="color:#0000ff">Unpublish request sent to Admin</small>';
                    $publishstatus="";
                    $writingstatus="";
                }
                

                
                if($writingstatus!=""){
                    $status=$writingstatus;
                }
                if($publishstatus!=""){
                    $status=$publishstatus;
                }
                if($unpublishstatus!=""){
                    $status=$unpublishstatus;
                }
                  
                $customer_id=$this->session->userdata("customer_id");
                $row=array();
                $row[]=$i;
                $row[]=$queryRecordsObj->blog_title;
                $row[]=$queryRecordsObj->topic_sel;
                $row[]=$queryRecordsObj->timestamp;
                $row[]=$status;
                $row[]='<a target="_blank" href="'.base_url().'get_blog_post_preview/'.$queryRecordsObj->slug.'/'.$queryRecordsObj->customer_id.'"><i class="fa fa-eye"></i></a> <a href="'.base_url().'admin/Ui_content/publish/'.$queryRecordsObj->slug.'"><small class="label label-warning">Publish</small></a> <a href="'.base_url().'admin/Ui_content/updateblog/'.$queryRecordsObj->blog_id.'"><small class="label label-success">Edit</small></a>';
                $data[]=$row;
                $row=array();
                $i++;
            }
            $json_data = array(
                    "draw"            => intval( $params['draw'] ),   
                    "recordsTotal"    => intval( $totalRecords ),  
                    "recordsFiltered" => intval($totalRecords),
                    "data"            => $data   // total data array
                    );
            echo json_encode($json_data);  // send data as json format
            
        }else{
            $this->logged_out();
        }   
    }
    public function  all_blogs_unpublished(){
        if($this->session->userdata("logged_in")){
            $data["menu_flag"]="blog";
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/ui/all_blogs_unpublished',$data);
            
        }else{
            $this->logged_out();
        }   
    }
    public function  blogs(){
        if($this->session->userdata("logged_in")){
            $data["menu_flag"]="blog";
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/ui/all_blogs_unpublished',$data);
            
        }else{
            $this->logged_out();
        }   
    }
    /* admin blogs ends*/
	public function logged_out(){
		$this->load->view('admin/header');
		$this->load->view('admin/login');
	}
	
	public function add_blog_image(){
		if($this->session->userdata("logged_in")){
            $blog_id_for_image=$this->input->post("blog_id_for_image");
			$file_location="";
			if(count($_FILES["blog_image"])>0){
					if (!file_exists($folder="assets/blog_images/".$blog_id_for_image)) {
						$mask=umask(0);
mkdir($folder,0777);
umask($mask);

					}
					

					$name1=$_FILES["blog_image"]['name'];
					$ext=pathinfo($name1, PATHINFO_EXTENSION);
					$tmp=$_FILES["blog_image"]['tmp_name'];
					
					$folder1=$folder."/".$blog_id_for_image."."."png";
				
					if(move_uploaded_file($tmp,$folder1)){
						$file_family=$_FILES['blog_image']['type'];
						$file_location=$folder1;
						
					}
					 else{
						 $file_location="";
						 $file_family="";
						
						 
					 } 	
				}
				
				$flag=$this->Site_blog->add_blog_image($blog_id_for_image,$file_location);
			echo $flag;
			
            
        }else{
            $this->logged_out();
        }  
	}
	
	public function post_to_frontend(){
		if($this->session->userdata("logged_in")){
            $blog_id=$this->input->post("blog_id");
			$post=$this->input->post("post");
			$flag=$this->Site_blog->post_to_frontend($blog_id,$post);
			echo $flag;
			
            
        }else{
            $this->logged_out();
        }  
	}
	
	 public function update_blog_content(){
            if($this->session->userdata("logged_in")){
                //print_r($this->input->post());exit;
                extract($this->input->post());
                
                $updated=$this->Site_blog->update_blog_content($blog_content,$blog_id,$customer_id);
                if($updated){
                    if($updated==1){
                        http_response_code(200);
                        echo 'Updated Blog';
                    }
                    if($updated==2){
                        http_response_code(200);
                        echo 'You have made no changes';
                    }
                    
                }else{
                    http_response_code(400);
                    echo "Oops! Some error, try after some time.";
                }
            }
            else{
                redirect(base_url());
            }
        }
		public function  all_Site_newsletter(){
        if($this->session->userdata("logged_in")){
            $data["menu_flag"]="newsletter";
            
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/ui/newsletter',$data);
        }else{
            $this->logged_out();
        }   
    }
	public function write_a_blog(){
		if($this->session->userdata("logged_in")){
            $data["blog_topic_result"]=$this->Site_blog->get_blog_topic_data();
			$data["blog_tags_result"]=$this->Site_blog->get_blog_tags_data();
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/ui/write-a-blog',$data);
        }else{
            $this->logged_out();
        }  
	}
	public function updateblog($blog_id){
		if($this->session->userdata("logged_in")){
            $data["blog_id"]=$blog_id;
			$customer_id=$this->session->userdata("user_id");
            $data["blog_data"]=$this->Site_blog->get_all_blog_data($blog_id,$customer_id);
			$data["blog_topic_result"]=$this->Site_blog->get_blog_topic_data();
			$data["blog_tags_result"]=$this->Site_blog->get_blog_tags_data();
            $this -> load -> view('admin/vmanage',$data);
            $this -> load -> view('admin/ui/updateblog',$data);
        }else{
            $this->logged_out();
        }  
	}
	
	
	public function write_a_blog_action(){
		if($this->session->userdata("logged_in")){
          $write_a_blog=$this->input->post("write_a_blog");
            if($write_a_blog=="addinitials"){
				 
				
				$topic_sel=addslashes($this->input->post("topic_sel"));
				$blog_date=addslashes($this->input->post("blog_date"));
				$tag_sel=$this->input->post("tag_sel");
				$tag_sel=json_encode($tag_sel);
				$blog_title=addslashes($this->input->post("blog_title"));
				
				
				
				
				
				$flag=false;
				$date = new DateTime();
                $mt=microtime();
                $salt="blog_id";
                $rand=mt_rand(1, 1000000);
                $ts=$date->getTimestamp();
                $str=$salt.$rand.$ts.$mt;
                $blog_id=md5($str);
				$customer_id=$this->session->userdata("user_id");
				$flag=$this->Site_blog->blog_action_add_initials($blog_date,$topic_sel,$tag_sel,$blog_title,$blog_id,$customer_id);
				
					if($flag==true){
						echo json_encode(array("status"=>"yes","blog_id"=>$blog_id));
					}
					else{
						echo json_encode(array("status"=>"no","reason"=>"unknown"));
					}

			}
			if($write_a_blog=="updateinitials"){
				
				$topic_sel=addslashes($this->input->post("topic_sel"));
				$tag_sel=$this->input->post("tag_sel");
				$tag_sel=addslashes(json_encode($tag_sel));
				$blog_title=addslashes($this->input->post("blog_title"));
				$blog_content=addslashes($this->input->post("blog_content"));
				$flag=false;
				$blog_id=$this->input->post("blog_id");
				$blog_id_for_image=$blog_id;
				$customer_id=$this->session->userdata("user_id");
				
				$blog_image_for_content_exists=addslashes($this->input->post("blog_image_for_content_exists"));
				
				
				$file_location="";
				
		if(!empty($_FILES["blog_image_for_content"]["name"])){
			
				$image_info = getimagesize($_FILES["blog_image_for_content"]["tmp_name"]);
			$image_width = $image_info[0];
			$image_height = $image_info[1];
			if($image_width=="1200" && $image_height=="500"){
				
			}
			else{
				echo json_encode(array("status"=>"no","reason"=>"wrong_dimensions"));
				exit;
			}
			
			if (!file_exists($folder="assets/blog_images")){
					$mask=umask(0);
					mkdir($folder,0777);
					umask($mask);
				}
				if (!file_exists($folder=$folder."/".$blog_id_for_image)){
					$mask=umask(0);
					mkdir($folder,0777);
					umask($mask);
				}
				$name1=$_FILES["blog_image_for_content"]['name'];
				$ext=pathinfo($name1, PATHINFO_EXTENSION);
				$tmp=$_FILES["blog_image_for_content"]['tmp_name'];
				
				$folder1=$folder."/".$blog_id_for_image."."."jpg";
				if(move_uploaded_file($tmp,$folder1)){
					$file_family=$_FILES['blog_image_for_content']['type'];
					$file_location=$folder1;
					
				}else{
					 $file_location="";
					 $file_family="";
				} 	
			}
			
			
			
				$flag=$this->Site_blog->update_initial_blog($topic_sel,$tag_sel,$blog_title,$blog_content,$blog_id,$customer_id,$file_location,$blog_image_for_content_exists);
				if($flag==true){
					echo json_encode(array("status"=>"yes"));
				}
				else{
					echo json_encode(array("status"=>"no","reason"=>"unknown"));
				}
			}
			
			
        }else{
            $this->logged_out();
        }  
	}
	
	
	/** blogs ends ***/
		
		public function all_news_letter_processing(){
			
			if($this->session->userdata("logged_in")){
				$params = $columns = $totalRecords = $data = array();
				
				$params = $this->input->post();
				
				$totalRecords=$this->Site_newsletter->all_news_letter_processing($params,"get_total_num_recs");
				
				$queryRecordsResult=$this->Site_newsletter->all_news_letter_processing($params,"get_recs");
				
				$i=1;
				foreach($queryRecordsResult as $queryRecordsObj){
					
					$row=array();
					$row[]=$i;
					$row[]=$queryRecordsObj->email_address;
				$row[]=$queryRecordsObj->subscribed;
				$row[]=$queryRecordsObj->timestamp;
					$data[]=$row;
					$row=array();
					$i++;
				}
				$json_data = array(
						"draw"            => intval( $params['draw'] ),   
						"recordsTotal"    => intval( $totalRecords ),  
						"recordsFiltered" => intval($totalRecords),
						"data"            => $data   // total data array
						);
				echo json_encode($json_data);  // send data as json format
				
			}else{
				$this->logged_out();
			}   
    
	}
  
}
?>